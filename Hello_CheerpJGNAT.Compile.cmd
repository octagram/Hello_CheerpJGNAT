@echo off

jvm-gnatmake -PHello_CheerpJGNAT.gpr
jvm-gnat bind -PHello_CheerpJGNAT.gpr hello_cheerpjgnat.ali
jvm-gnat link -PHello_CheerpJGNAT.gpr hello_cheerpjgnat.ali
jarmake -o .obj\Hello_CheerpJGNAT.Draft.jar .obj\hello_cheerpjgnat.class .obj\*.class
md .obj\Repack
pushd .obj\Repack
jar -xf ..\Hello_CheerpJGNAT.Draft.jar
del /q ..\Hello_CheerpJGNAT.Draft.jar > nul
jar -cf ..\Hello_CheerpJGNAT.jar jgnat *.class
popd
rd /s /q .obj\Repack > nul
python ..\cheerpj_preview\cheerpjfy.py .obj\Hello_CheerpJGNAT.jar
move .obj\Hello_CheerpJGNAT.jar.js Test\ > nul
move .obj\Hello_CheerpJGNAT.jar Test\ > nul
