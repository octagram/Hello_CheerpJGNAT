pragma Extensions_Allowed (On);
limited with Java.Net.ServerSocket;
limited with Java.Net.Socket;
limited with Java.Nio.Channels.ServerSocketChannel;
limited with Java.Nio.Channels.SocketChannel;
with Java.Lang.Object;

package Com.Oracle.Net.Sdp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function OpenSocket return access Java.Net.Socket.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenServerSocket return access Java.Net.ServerSocket.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenSocketChannel return access Java.Nio.Channels.SocketChannel.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenServerSocketChannel return access Java.Nio.Channels.ServerSocketChannel.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, OpenSocket, "openSocket");
   pragma Import (Java, OpenServerSocket, "openServerSocket");
   pragma Import (Java, OpenSocketChannel, "openSocketChannel");
   pragma Import (Java, OpenServerSocketChannel, "openServerSocketChannel");

end Com.Oracle.Net.Sdp;
pragma Import (Java, Com.Oracle.Net.Sdp, "com.oracle.net.Sdp");
pragma Extensions_Allowed (Off);
