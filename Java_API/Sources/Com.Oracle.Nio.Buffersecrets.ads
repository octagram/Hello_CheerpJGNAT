pragma Extensions_Allowed (On);
limited with Java.Nio.Buffer;
limited with Java.Nio.ByteBuffer;
with Java.Lang.Object;

package Com.Oracle.Nio.BufferSecrets is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Instance return access Com.Oracle.Nio.BufferSecrets.Typ'Class;

   function NewDirectByteBuffer (This : access Typ;
                                 P1_Long : Java.Long;
                                 P2_Int : Java.Int;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                                 return access Java.Nio.ByteBuffer.Typ'Class;

   function Address (This : access Typ;
                     P1_Buffer : access Standard.Java.Nio.Buffer.Typ'Class)
                     return Java.Long;

   function Attachment (This : access Typ;
                        P1_Buffer : access Standard.Java.Nio.Buffer.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   procedure Truncate (This : access Typ;
                       P1_Buffer : access Standard.Java.Nio.Buffer.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Instance, "instance");
   pragma Import (Java, NewDirectByteBuffer, "newDirectByteBuffer");
   pragma Import (Java, Address, "address");
   pragma Import (Java, Attachment, "attachment");
   pragma Import (Java, Truncate, "truncate");

end Com.Oracle.Nio.BufferSecrets;
pragma Import (Java, Com.Oracle.Nio.BufferSecrets, "com.oracle.nio.BufferSecrets");
pragma Extensions_Allowed (Off);
