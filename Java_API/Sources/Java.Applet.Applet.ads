pragma Extensions_Allowed (On);
limited with Java.Applet.AppletContext;
limited with Java.Applet.AppletStub;
limited with Java.Applet.AudioClip;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Locale;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Awt.Panel;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Java.Awt.Image;

package Java.Applet.Applet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Panel.Typ(MenuContainer_I,
                              ImageObserver_I,
                              Serializable_I,
                              Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Applet (This : Ref := null)
                        return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   procedure SetStub (This : access Typ;
                      P1_AppletStub : access Standard.Java.Applet.AppletStub.Typ'Class);

   function IsActive (This : access Typ)
                      return Java.Boolean;

   function GetDocumentBase (This : access Typ)
                             return access Java.Net.URL.Typ'Class;

   function GetCodeBase (This : access Typ)
                         return access Java.Net.URL.Typ'Class;

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function GetAppletContext (This : access Typ)
                              return access Java.Applet.AppletContext.Typ'Class;

   procedure Resize (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int);

   procedure Resize (This : access Typ;
                     P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   procedure ShowStatus (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetImage (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class)
                      return access Java.Awt.Image.Typ'Class;

   function GetImage (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Awt.Image.Typ'Class;

   --  final
   function NewAudioClip (P1_URL : access Standard.Java.Net.URL.Typ'Class)
                          return access Java.Applet.AudioClip.Typ'Class;

   function GetAudioClip (This : access Typ;
                          P1_URL : access Standard.Java.Net.URL.Typ'Class)
                          return access Java.Applet.AudioClip.Typ'Class;

   function GetAudioClip (This : access Typ;
                          P1_URL : access Standard.Java.Net.URL.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Applet.AudioClip.Typ'Class;

   function GetAppletInfo (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   function GetParameterInfo (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   procedure Play (This : access Typ;
                   P1_URL : access Standard.Java.Net.URL.Typ'Class);

   procedure Play (This : access Typ;
                   P1_URL : access Standard.Java.Net.URL.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Init (This : access Typ);

   procedure Start (This : access Typ);

   procedure Stop (This : access Typ);

   procedure Destroy (This : access Typ);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Applet);
   pragma Import (Java, SetStub, "setStub");
   pragma Import (Java, IsActive, "isActive");
   pragma Import (Java, GetDocumentBase, "getDocumentBase");
   pragma Import (Java, GetCodeBase, "getCodeBase");
   pragma Import (Java, GetParameter, "getParameter");
   pragma Import (Java, GetAppletContext, "getAppletContext");
   pragma Import (Java, Resize, "resize");
   pragma Import (Java, ShowStatus, "showStatus");
   pragma Import (Java, GetImage, "getImage");
   pragma Import (Java, NewAudioClip, "newAudioClip");
   pragma Import (Java, GetAudioClip, "getAudioClip");
   pragma Import (Java, GetAppletInfo, "getAppletInfo");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, GetParameterInfo, "getParameterInfo");
   pragma Import (Java, Play, "play");
   pragma Import (Java, Init, "init");
   pragma Import (Java, Start, "start");
   pragma Import (Java, Stop, "stop");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Applet.Applet;
pragma Import (Java, Java.Applet.Applet, "java.applet.Applet");
pragma Extensions_Allowed (Off);
