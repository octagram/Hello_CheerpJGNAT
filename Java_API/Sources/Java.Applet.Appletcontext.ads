pragma Extensions_Allowed (On);
limited with Java.Applet.Applet;
limited with Java.Applet.AudioClip;
limited with Java.Awt.Image;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Enumeration;
limited with Java.Util.Iterator;
with Java.Lang.Object;

package Java.Applet.AppletContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAudioClip (This : access Typ;
                          P1_URL : access Standard.Java.Net.URL.Typ'Class)
                          return access Java.Applet.AudioClip.Typ'Class is abstract;

   function GetImage (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class)
                      return access Java.Awt.Image.Typ'Class is abstract;

   function GetApplet (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Applet.Applet.Typ'Class is abstract;

   function GetApplets (This : access Typ)
                        return access Java.Util.Enumeration.Typ'Class is abstract;

   procedure ShowDocument (This : access Typ;
                           P1_URL : access Standard.Java.Net.URL.Typ'Class) is abstract;

   procedure ShowDocument (This : access Typ;
                           P1_URL : access Standard.Java.Net.URL.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure ShowStatus (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetStream (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   function GetStream (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Io.InputStream.Typ'Class is abstract;

   function GetStreamKeys (This : access Typ)
                           return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAudioClip, "getAudioClip");
   pragma Export (Java, GetImage, "getImage");
   pragma Export (Java, GetApplet, "getApplet");
   pragma Export (Java, GetApplets, "getApplets");
   pragma Export (Java, ShowDocument, "showDocument");
   pragma Export (Java, ShowStatus, "showStatus");
   pragma Export (Java, SetStream, "setStream");
   pragma Export (Java, GetStream, "getStream");
   pragma Export (Java, GetStreamKeys, "getStreamKeys");

end Java.Applet.AppletContext;
pragma Import (Java, Java.Applet.AppletContext, "java.applet.AppletContext");
pragma Extensions_Allowed (Off);
