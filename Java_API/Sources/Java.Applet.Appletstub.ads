pragma Extensions_Allowed (On);
limited with Java.Applet.AppletContext;
limited with Java.Lang.String;
limited with Java.Net.URL;
with Java.Lang.Object;

package Java.Applet.AppletStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsActive (This : access Typ)
                      return Java.Boolean is abstract;

   function GetDocumentBase (This : access Typ)
                             return access Java.Net.URL.Typ'Class is abstract;

   function GetCodeBase (This : access Typ)
                         return access Java.Net.URL.Typ'Class is abstract;

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetAppletContext (This : access Typ)
                              return access Java.Applet.AppletContext.Typ'Class is abstract;

   procedure AppletResize (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsActive, "isActive");
   pragma Export (Java, GetDocumentBase, "getDocumentBase");
   pragma Export (Java, GetCodeBase, "getCodeBase");
   pragma Export (Java, GetParameter, "getParameter");
   pragma Export (Java, GetAppletContext, "getAppletContext");
   pragma Export (Java, AppletResize, "appletResize");

end Java.Applet.AppletStub;
pragma Import (Java, Java.Applet.AppletStub, "java.applet.AppletStub");
pragma Extensions_Allowed (Off);
