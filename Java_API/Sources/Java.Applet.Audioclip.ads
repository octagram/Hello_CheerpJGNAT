pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Applet.AudioClip is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Play (This : access Typ) is abstract;

   procedure loop_K (This : access Typ) is abstract;

   procedure Stop (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Play, "play");
   pragma Export (Java, loop_K, "loop");
   pragma Export (Java, Stop, "stop");

end Java.Applet.AudioClip;
pragma Import (Java, Java.Applet.AudioClip, "java.applet.AudioClip");
pragma Extensions_Allowed (Off);
