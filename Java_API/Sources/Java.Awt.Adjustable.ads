pragma Extensions_Allowed (On);
limited with Java.Awt.Event.AdjustmentListener;
with Java.Lang.Object;

package Java.Awt.Adjustable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOrientation (This : access Typ)
                            return Java.Int is abstract;

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   function GetMinimum (This : access Typ)
                        return Java.Int is abstract;

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   function GetMaximum (This : access Typ)
                        return Java.Int is abstract;

   procedure SetUnitIncrement (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   function GetUnitIncrement (This : access Typ)
                              return Java.Int is abstract;

   procedure SetBlockIncrement (This : access Typ;
                                P1_Int : Java.Int) is abstract;

   function GetBlockIncrement (This : access Typ)
                               return Java.Int is abstract;

   procedure SetVisibleAmount (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   function GetVisibleAmount (This : access Typ)
                              return Java.Int is abstract;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   function GetValue (This : access Typ)
                      return Java.Int is abstract;

   procedure AddAdjustmentListener (This : access Typ;
                                    P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class) is abstract;

   procedure RemoveAdjustmentListener (This : access Typ;
                                       P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   HORIZONTAL : constant Java.Int;

   --  final
   VERTICAL : constant Java.Int;

   --  final
   NO_ORIENTATION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetOrientation, "getOrientation");
   pragma Export (Java, SetMinimum, "setMinimum");
   pragma Export (Java, GetMinimum, "getMinimum");
   pragma Export (Java, SetMaximum, "setMaximum");
   pragma Export (Java, GetMaximum, "getMaximum");
   pragma Export (Java, SetUnitIncrement, "setUnitIncrement");
   pragma Export (Java, GetUnitIncrement, "getUnitIncrement");
   pragma Export (Java, SetBlockIncrement, "setBlockIncrement");
   pragma Export (Java, GetBlockIncrement, "getBlockIncrement");
   pragma Export (Java, SetVisibleAmount, "setVisibleAmount");
   pragma Export (Java, GetVisibleAmount, "getVisibleAmount");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, AddAdjustmentListener, "addAdjustmentListener");
   pragma Export (Java, RemoveAdjustmentListener, "removeAdjustmentListener");
   pragma Import (Java, HORIZONTAL, "HORIZONTAL");
   pragma Import (Java, VERTICAL, "VERTICAL");
   pragma Import (Java, NO_ORIENTATION, "NO_ORIENTATION");

end Java.Awt.Adjustable;
pragma Import (Java, Java.Awt.Adjustable, "java.awt.Adjustable");
pragma Extensions_Allowed (Off);
