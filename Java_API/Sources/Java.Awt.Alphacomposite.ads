pragma Extensions_Allowed (On);
limited with Java.Awt.CompositeContext;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.RenderingHints;
with Java.Awt.Composite;
with Java.Lang.Object;

package Java.Awt.AlphaComposite is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Composite_I : Java.Awt.Composite.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_Int : Java.Int)
                         return access Java.Awt.AlphaComposite.Typ'Class;

   function GetInstance (P1_Int : Java.Int;
                         P2_Float : Java.Float)
                         return access Java.Awt.AlphaComposite.Typ'Class;

   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P3_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.CompositeContext.Typ'Class;

   function GetAlpha (This : access Typ)
                      return Java.Float;

   function GetRule (This : access Typ)
                     return Java.Int;

   function Derive (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Awt.AlphaComposite.Typ'Class;

   function Derive (This : access Typ;
                    P1_Float : Java.Float)
                    return access Java.Awt.AlphaComposite.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CLEAR : constant Java.Int;

   --  final
   SRC : constant Java.Int;

   --  final
   DST : constant Java.Int;

   --  final
   SRC_OVER : constant Java.Int;

   --  final
   DST_OVER : constant Java.Int;

   --  final
   SRC_IN : constant Java.Int;

   --  final
   DST_IN : constant Java.Int;

   --  final
   SRC_OUT : constant Java.Int;

   --  final
   DST_OUT : constant Java.Int;

   --  final
   SRC_ATOP : constant Java.Int;

   --  final
   DST_ATOP : constant Java.Int;

   --  final
   XOR_K : constant Java.Int;

   --  final
   Clear_K : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   Src_K : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   Dst_K : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   SrcOver : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   DstOver : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   SrcIn : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   DstIn : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   SrcOut : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   DstOut : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   SrcAtop : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   DstAtop : access Java.Awt.AlphaComposite.Typ'Class;

   --  final
   Xor_K_K : access Java.Awt.AlphaComposite.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, CreateContext, "createContext");
   pragma Import (Java, GetAlpha, "getAlpha");
   pragma Import (Java, GetRule, "getRule");
   pragma Import (Java, Derive, "derive");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, CLEAR, "CLEAR");
   pragma Import (Java, SRC, "SRC");
   pragma Import (Java, DST, "DST");
   pragma Import (Java, SRC_OVER, "SRC_OVER");
   pragma Import (Java, DST_OVER, "DST_OVER");
   pragma Import (Java, SRC_IN, "SRC_IN");
   pragma Import (Java, DST_IN, "DST_IN");
   pragma Import (Java, SRC_OUT, "SRC_OUT");
   pragma Import (Java, DST_OUT, "DST_OUT");
   pragma Import (Java, SRC_ATOP, "SRC_ATOP");
   pragma Import (Java, DST_ATOP, "DST_ATOP");
   pragma Import (Java, XOR_K, "XOR");
   pragma Import (Java, Clear_K, "Clear");
   pragma Import (Java, Src_K, "Src");
   pragma Import (Java, Dst_K, "Dst");
   pragma Import (Java, SrcOver, "SrcOver");
   pragma Import (Java, DstOver, "DstOver");
   pragma Import (Java, SrcIn, "SrcIn");
   pragma Import (Java, DstIn, "DstIn");
   pragma Import (Java, SrcOut, "SrcOut");
   pragma Import (Java, DstOut, "DstOut");
   pragma Import (Java, SrcAtop, "SrcAtop");
   pragma Import (Java, DstAtop, "DstAtop");
   pragma Import (Java, Xor_K_K, "Xor");

end Java.Awt.AlphaComposite;
pragma Import (Java, Java.Awt.AlphaComposite, "java.awt.AlphaComposite");
pragma Extensions_Allowed (Off);
