pragma Extensions_Allowed (On);
limited with Java.Awt.Event;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Awt.AWTEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Id : Java.Int;
      pragma Import (Java, Id, "id");

      --  protected
      Consumed : Java.Boolean;
      pragma Import (Java, Consumed, "consumed");

   end record;

   function New_AWTEvent (P1_Event : access Standard.Java.Awt.Event.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_AWTEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSource (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetID (This : access Typ)
                   return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  protected
   procedure Consume (This : access Typ);

   --  protected
   function IsConsumed (This : access Typ)
                        return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   COMPONENT_EVENT_MASK : constant Java.Long;

   --  final
   CONTAINER_EVENT_MASK : constant Java.Long;

   --  final
   FOCUS_EVENT_MASK : constant Java.Long;

   --  final
   KEY_EVENT_MASK : constant Java.Long;

   --  final
   MOUSE_EVENT_MASK : constant Java.Long;

   --  final
   MOUSE_MOTION_EVENT_MASK : constant Java.Long;

   --  final
   WINDOW_EVENT_MASK : constant Java.Long;

   --  final
   ACTION_EVENT_MASK : constant Java.Long;

   --  final
   ADJUSTMENT_EVENT_MASK : constant Java.Long;

   --  final
   ITEM_EVENT_MASK : constant Java.Long;

   --  final
   TEXT_EVENT_MASK : constant Java.Long;

   --  final
   INPUT_METHOD_EVENT_MASK : constant Java.Long;

   --  final
   PAINT_EVENT_MASK : constant Java.Long;

   --  final
   INVOCATION_EVENT_MASK : constant Java.Long;

   --  final
   HIERARCHY_EVENT_MASK : constant Java.Long;

   --  final
   HIERARCHY_BOUNDS_EVENT_MASK : constant Java.Long;

   --  final
   MOUSE_WHEEL_EVENT_MASK : constant Java.Long;

   --  final
   WINDOW_STATE_EVENT_MASK : constant Java.Long;

   --  final
   WINDOW_FOCUS_EVENT_MASK : constant Java.Long;

   --  final
   RESERVED_ID_MAX : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AWTEvent);
   pragma Import (Java, SetSource, "setSource");
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, Consume, "consume");
   pragma Import (Java, IsConsumed, "isConsumed");
   pragma Import (Java, COMPONENT_EVENT_MASK, "COMPONENT_EVENT_MASK");
   pragma Import (Java, CONTAINER_EVENT_MASK, "CONTAINER_EVENT_MASK");
   pragma Import (Java, FOCUS_EVENT_MASK, "FOCUS_EVENT_MASK");
   pragma Import (Java, KEY_EVENT_MASK, "KEY_EVENT_MASK");
   pragma Import (Java, MOUSE_EVENT_MASK, "MOUSE_EVENT_MASK");
   pragma Import (Java, MOUSE_MOTION_EVENT_MASK, "MOUSE_MOTION_EVENT_MASK");
   pragma Import (Java, WINDOW_EVENT_MASK, "WINDOW_EVENT_MASK");
   pragma Import (Java, ACTION_EVENT_MASK, "ACTION_EVENT_MASK");
   pragma Import (Java, ADJUSTMENT_EVENT_MASK, "ADJUSTMENT_EVENT_MASK");
   pragma Import (Java, ITEM_EVENT_MASK, "ITEM_EVENT_MASK");
   pragma Import (Java, TEXT_EVENT_MASK, "TEXT_EVENT_MASK");
   pragma Import (Java, INPUT_METHOD_EVENT_MASK, "INPUT_METHOD_EVENT_MASK");
   pragma Import (Java, PAINT_EVENT_MASK, "PAINT_EVENT_MASK");
   pragma Import (Java, INVOCATION_EVENT_MASK, "INVOCATION_EVENT_MASK");
   pragma Import (Java, HIERARCHY_EVENT_MASK, "HIERARCHY_EVENT_MASK");
   pragma Import (Java, HIERARCHY_BOUNDS_EVENT_MASK, "HIERARCHY_BOUNDS_EVENT_MASK");
   pragma Import (Java, MOUSE_WHEEL_EVENT_MASK, "MOUSE_WHEEL_EVENT_MASK");
   pragma Import (Java, WINDOW_STATE_EVENT_MASK, "WINDOW_STATE_EVENT_MASK");
   pragma Import (Java, WINDOW_FOCUS_EVENT_MASK, "WINDOW_FOCUS_EVENT_MASK");
   pragma Import (Java, RESERVED_ID_MAX, "RESERVED_ID_MAX");

end Java.Awt.AWTEvent;
pragma Import (Java, Java.Awt.AWTEvent, "java.awt.AWTEvent");
pragma Extensions_Allowed (Off);
