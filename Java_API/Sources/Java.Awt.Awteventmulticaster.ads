pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.AdjustmentEvent;
limited with Java.Awt.Event.ComponentEvent;
limited with Java.Awt.Event.ContainerEvent;
limited with Java.Awt.Event.FocusEvent;
limited with Java.Awt.Event.HierarchyEvent;
limited with Java.Awt.Event.InputMethodEvent;
limited with Java.Awt.Event.ItemEvent;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Event.MouseWheelEvent;
limited with Java.Awt.Event.TextEvent;
limited with Java.Awt.Event.WindowEvent;
limited with Java.Io.ObjectOutputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
with Java.Awt.Event.ActionListener;
with Java.Awt.Event.AdjustmentListener;
with Java.Awt.Event.ComponentListener;
with Java.Awt.Event.ContainerListener;
with Java.Awt.Event.FocusListener;
with Java.Awt.Event.HierarchyBoundsListener;
with Java.Awt.Event.HierarchyListener;
with Java.Awt.Event.InputMethodListener;
with Java.Awt.Event.ItemListener;
with Java.Awt.Event.KeyListener;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Awt.Event.TextListener;
with Java.Awt.Event.WindowFocusListener;
with Java.Awt.Event.WindowListener;
with Java.Awt.Event.WindowStateListener;
with Java.Lang.Object;

package Java.Awt.AWTEventMulticaster is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ActionListener_I : Java.Awt.Event.ActionListener.Ref;
            AdjustmentListener_I : Java.Awt.Event.AdjustmentListener.Ref;
            ComponentListener_I : Java.Awt.Event.ComponentListener.Ref;
            ContainerListener_I : Java.Awt.Event.ContainerListener.Ref;
            FocusListener_I : Java.Awt.Event.FocusListener.Ref;
            HierarchyBoundsListener_I : Java.Awt.Event.HierarchyBoundsListener.Ref;
            HierarchyListener_I : Java.Awt.Event.HierarchyListener.Ref;
            InputMethodListener_I : Java.Awt.Event.InputMethodListener.Ref;
            ItemListener_I : Java.Awt.Event.ItemListener.Ref;
            KeyListener_I : Java.Awt.Event.KeyListener.Ref;
            MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref;
            TextListener_I : Java.Awt.Event.TextListener.Ref;
            WindowFocusListener_I : Java.Awt.Event.WindowFocusListener.Ref;
            WindowListener_I : Java.Awt.Event.WindowListener.Ref;
            WindowStateListener_I : Java.Awt.Event.WindowStateListener.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      A : access Java.Util.EventListener.Typ'Class;
      pragma Import (Java, A, "a");

      --  protected  final
      B : access Java.Util.EventListener.Typ'Class;
      pragma Import (Java, B, "b");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AWTEventMulticaster (P1_EventListener : access Standard.Java.Util.EventListener.Typ'Class;
                                     P2_EventListener : access Standard.Java.Util.EventListener.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function Remove (This : access Typ;
                    P1_EventListener : access Standard.Java.Util.EventListener.Typ'Class)
                    return access Java.Util.EventListener.Typ'Class;

   procedure ComponentResized (This : access Typ;
                               P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   procedure ComponentMoved (This : access Typ;
                             P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   procedure ComponentShown (This : access Typ;
                             P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   procedure ComponentHidden (This : access Typ;
                              P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   procedure ComponentAdded (This : access Typ;
                             P1_ContainerEvent : access Standard.Java.Awt.Event.ContainerEvent.Typ'Class);

   procedure ComponentRemoved (This : access Typ;
                               P1_ContainerEvent : access Standard.Java.Awt.Event.ContainerEvent.Typ'Class);

   procedure FocusGained (This : access Typ;
                          P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure FocusLost (This : access Typ;
                        P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure KeyTyped (This : access Typ;
                       P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   procedure KeyPressed (This : access Typ;
                         P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   procedure KeyReleased (This : access Typ;
                          P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure WindowOpened (This : access Typ;
                           P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowClosing (This : access Typ;
                            P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowClosed (This : access Typ;
                           P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowIconified (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowDeiconified (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowActivated (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowDeactivated (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowStateChanged (This : access Typ;
                                 P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowGainedFocus (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowLostFocus (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   procedure ItemStateChanged (This : access Typ;
                               P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);

   procedure AdjustmentValueChanged (This : access Typ;
                                     P1_AdjustmentEvent : access Standard.Java.Awt.Event.AdjustmentEvent.Typ'Class);

   procedure TextValueChanged (This : access Typ;
                               P1_TextEvent : access Standard.Java.Awt.Event.TextEvent.Typ'Class);

   procedure InputMethodTextChanged (This : access Typ;
                                     P1_InputMethodEvent : access Standard.Java.Awt.Event.InputMethodEvent.Typ'Class);

   procedure CaretPositionChanged (This : access Typ;
                                   P1_InputMethodEvent : access Standard.Java.Awt.Event.InputMethodEvent.Typ'Class);

   procedure HierarchyChanged (This : access Typ;
                               P1_HierarchyEvent : access Standard.Java.Awt.Event.HierarchyEvent.Typ'Class);

   procedure AncestorMoved (This : access Typ;
                            P1_HierarchyEvent : access Standard.Java.Awt.Event.HierarchyEvent.Typ'Class);

   procedure AncestorResized (This : access Typ;
                              P1_HierarchyEvent : access Standard.Java.Awt.Event.HierarchyEvent.Typ'Class);

   procedure MouseWheelMoved (This : access Typ;
                              P1_MouseWheelEvent : access Standard.Java.Awt.Event.MouseWheelEvent.Typ'Class);

   function Add (P1_ComponentListener : access Standard.Java.Awt.Event.ComponentListener.Typ'Class;
                 P2_ComponentListener : access Standard.Java.Awt.Event.ComponentListener.Typ'Class)
                 return access Java.Awt.Event.ComponentListener.Typ'Class;

   function Add (P1_ContainerListener : access Standard.Java.Awt.Event.ContainerListener.Typ'Class;
                 P2_ContainerListener : access Standard.Java.Awt.Event.ContainerListener.Typ'Class)
                 return access Java.Awt.Event.ContainerListener.Typ'Class;

   function Add (P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class;
                 P2_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class)
                 return access Java.Awt.Event.FocusListener.Typ'Class;

   function Add (P1_KeyListener : access Standard.Java.Awt.Event.KeyListener.Typ'Class;
                 P2_KeyListener : access Standard.Java.Awt.Event.KeyListener.Typ'Class)
                 return access Java.Awt.Event.KeyListener.Typ'Class;

   function Add (P1_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class;
                 P2_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class)
                 return access Java.Awt.Event.MouseListener.Typ'Class;

   function Add (P1_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class;
                 P2_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class)
                 return access Java.Awt.Event.MouseMotionListener.Typ'Class;

   function Add (P1_WindowListener : access Standard.Java.Awt.Event.WindowListener.Typ'Class;
                 P2_WindowListener : access Standard.Java.Awt.Event.WindowListener.Typ'Class)
                 return access Java.Awt.Event.WindowListener.Typ'Class;

   function Add (P1_WindowStateListener : access Standard.Java.Awt.Event.WindowStateListener.Typ'Class;
                 P2_WindowStateListener : access Standard.Java.Awt.Event.WindowStateListener.Typ'Class)
                 return access Java.Awt.Event.WindowStateListener.Typ'Class;

   function Add (P1_WindowFocusListener : access Standard.Java.Awt.Event.WindowFocusListener.Typ'Class;
                 P2_WindowFocusListener : access Standard.Java.Awt.Event.WindowFocusListener.Typ'Class)
                 return access Java.Awt.Event.WindowFocusListener.Typ'Class;

   function Add (P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class;
                 P2_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class)
                 return access Java.Awt.Event.ActionListener.Typ'Class;

   function Add (P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class;
                 P2_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class)
                 return access Java.Awt.Event.ItemListener.Typ'Class;

   function Add (P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class;
                 P2_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class)
                 return access Java.Awt.Event.AdjustmentListener.Typ'Class;

   function Add (P1_TextListener : access Standard.Java.Awt.Event.TextListener.Typ'Class;
                 P2_TextListener : access Standard.Java.Awt.Event.TextListener.Typ'Class)
                 return access Java.Awt.Event.TextListener.Typ'Class;

   function Add (P1_InputMethodListener : access Standard.Java.Awt.Event.InputMethodListener.Typ'Class;
                 P2_InputMethodListener : access Standard.Java.Awt.Event.InputMethodListener.Typ'Class)
                 return access Java.Awt.Event.InputMethodListener.Typ'Class;

   function Add (P1_HierarchyListener : access Standard.Java.Awt.Event.HierarchyListener.Typ'Class;
                 P2_HierarchyListener : access Standard.Java.Awt.Event.HierarchyListener.Typ'Class)
                 return access Java.Awt.Event.HierarchyListener.Typ'Class;

   function Add (P1_HierarchyBoundsListener : access Standard.Java.Awt.Event.HierarchyBoundsListener.Typ'Class;
                 P2_HierarchyBoundsListener : access Standard.Java.Awt.Event.HierarchyBoundsListener.Typ'Class)
                 return access Java.Awt.Event.HierarchyBoundsListener.Typ'Class;

   function Add (P1_MouseWheelListener : access Standard.Java.Awt.Event.MouseWheelListener.Typ'Class;
                 P2_MouseWheelListener : access Standard.Java.Awt.Event.MouseWheelListener.Typ'Class)
                 return access Java.Awt.Event.MouseWheelListener.Typ'Class;

   function Remove (P1_ComponentListener : access Standard.Java.Awt.Event.ComponentListener.Typ'Class;
                    P2_ComponentListener : access Standard.Java.Awt.Event.ComponentListener.Typ'Class)
                    return access Java.Awt.Event.ComponentListener.Typ'Class;

   function Remove (P1_ContainerListener : access Standard.Java.Awt.Event.ContainerListener.Typ'Class;
                    P2_ContainerListener : access Standard.Java.Awt.Event.ContainerListener.Typ'Class)
                    return access Java.Awt.Event.ContainerListener.Typ'Class;

   function Remove (P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class;
                    P2_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class)
                    return access Java.Awt.Event.FocusListener.Typ'Class;

   function Remove (P1_KeyListener : access Standard.Java.Awt.Event.KeyListener.Typ'Class;
                    P2_KeyListener : access Standard.Java.Awt.Event.KeyListener.Typ'Class)
                    return access Java.Awt.Event.KeyListener.Typ'Class;

   function Remove (P1_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class;
                    P2_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class)
                    return access Java.Awt.Event.MouseListener.Typ'Class;

   function Remove (P1_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class;
                    P2_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class)
                    return access Java.Awt.Event.MouseMotionListener.Typ'Class;

   function Remove (P1_WindowListener : access Standard.Java.Awt.Event.WindowListener.Typ'Class;
                    P2_WindowListener : access Standard.Java.Awt.Event.WindowListener.Typ'Class)
                    return access Java.Awt.Event.WindowListener.Typ'Class;

   function Remove (P1_WindowStateListener : access Standard.Java.Awt.Event.WindowStateListener.Typ'Class;
                    P2_WindowStateListener : access Standard.Java.Awt.Event.WindowStateListener.Typ'Class)
                    return access Java.Awt.Event.WindowStateListener.Typ'Class;

   function Remove (P1_WindowFocusListener : access Standard.Java.Awt.Event.WindowFocusListener.Typ'Class;
                    P2_WindowFocusListener : access Standard.Java.Awt.Event.WindowFocusListener.Typ'Class)
                    return access Java.Awt.Event.WindowFocusListener.Typ'Class;

   function Remove (P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class;
                    P2_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class)
                    return access Java.Awt.Event.ActionListener.Typ'Class;

   function Remove (P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class;
                    P2_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class)
                    return access Java.Awt.Event.ItemListener.Typ'Class;

   function Remove (P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class;
                    P2_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class)
                    return access Java.Awt.Event.AdjustmentListener.Typ'Class;

   function Remove (P1_TextListener : access Standard.Java.Awt.Event.TextListener.Typ'Class;
                    P2_TextListener : access Standard.Java.Awt.Event.TextListener.Typ'Class)
                    return access Java.Awt.Event.TextListener.Typ'Class;

   function Remove (P1_InputMethodListener : access Standard.Java.Awt.Event.InputMethodListener.Typ'Class;
                    P2_InputMethodListener : access Standard.Java.Awt.Event.InputMethodListener.Typ'Class)
                    return access Java.Awt.Event.InputMethodListener.Typ'Class;

   function Remove (P1_HierarchyListener : access Standard.Java.Awt.Event.HierarchyListener.Typ'Class;
                    P2_HierarchyListener : access Standard.Java.Awt.Event.HierarchyListener.Typ'Class)
                    return access Java.Awt.Event.HierarchyListener.Typ'Class;

   function Remove (P1_HierarchyBoundsListener : access Standard.Java.Awt.Event.HierarchyBoundsListener.Typ'Class;
                    P2_HierarchyBoundsListener : access Standard.Java.Awt.Event.HierarchyBoundsListener.Typ'Class)
                    return access Java.Awt.Event.HierarchyBoundsListener.Typ'Class;

   function Remove (P1_MouseWheelListener : access Standard.Java.Awt.Event.MouseWheelListener.Typ'Class;
                    P2_MouseWheelListener : access Standard.Java.Awt.Event.MouseWheelListener.Typ'Class)
                    return access Java.Awt.Event.MouseWheelListener.Typ'Class;

   --  protected
   function AddInternal (P1_EventListener : access Standard.Java.Util.EventListener.Typ'Class;
                         P2_EventListener : access Standard.Java.Util.EventListener.Typ'Class)
                         return access Java.Util.EventListener.Typ'Class;

   --  protected
   function RemoveInternal (P1_EventListener : access Standard.Java.Util.EventListener.Typ'Class;
                            P2_EventListener : access Standard.Java.Util.EventListener.Typ'Class)
                            return access Java.Util.EventListener.Typ'Class;

   --  protected
   procedure SaveInternal (This : access Typ;
                           P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Save (P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_EventListener : access Standard.Java.Util.EventListener.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetListeners (P1_EventListener : access Standard.Java.Util.EventListener.Typ'Class;
                          P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AWTEventMulticaster);
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ComponentResized, "componentResized");
   pragma Import (Java, ComponentMoved, "componentMoved");
   pragma Import (Java, ComponentShown, "componentShown");
   pragma Import (Java, ComponentHidden, "componentHidden");
   pragma Import (Java, ComponentAdded, "componentAdded");
   pragma Import (Java, ComponentRemoved, "componentRemoved");
   pragma Import (Java, FocusGained, "focusGained");
   pragma Import (Java, FocusLost, "focusLost");
   pragma Import (Java, KeyTyped, "keyTyped");
   pragma Import (Java, KeyPressed, "keyPressed");
   pragma Import (Java, KeyReleased, "keyReleased");
   pragma Import (Java, MouseClicked, "mouseClicked");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MouseEntered, "mouseEntered");
   pragma Import (Java, MouseExited, "mouseExited");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");
   pragma Import (Java, WindowOpened, "windowOpened");
   pragma Import (Java, WindowClosing, "windowClosing");
   pragma Import (Java, WindowClosed, "windowClosed");
   pragma Import (Java, WindowIconified, "windowIconified");
   pragma Import (Java, WindowDeiconified, "windowDeiconified");
   pragma Import (Java, WindowActivated, "windowActivated");
   pragma Import (Java, WindowDeactivated, "windowDeactivated");
   pragma Import (Java, WindowStateChanged, "windowStateChanged");
   pragma Import (Java, WindowGainedFocus, "windowGainedFocus");
   pragma Import (Java, WindowLostFocus, "windowLostFocus");
   pragma Import (Java, ActionPerformed, "actionPerformed");
   pragma Import (Java, ItemStateChanged, "itemStateChanged");
   pragma Import (Java, AdjustmentValueChanged, "adjustmentValueChanged");
   pragma Import (Java, TextValueChanged, "textValueChanged");
   pragma Import (Java, InputMethodTextChanged, "inputMethodTextChanged");
   pragma Import (Java, CaretPositionChanged, "caretPositionChanged");
   pragma Import (Java, HierarchyChanged, "hierarchyChanged");
   pragma Import (Java, AncestorMoved, "ancestorMoved");
   pragma Import (Java, AncestorResized, "ancestorResized");
   pragma Import (Java, MouseWheelMoved, "mouseWheelMoved");
   pragma Import (Java, Add, "add");
   pragma Import (Java, AddInternal, "addInternal");
   pragma Import (Java, RemoveInternal, "removeInternal");
   pragma Import (Java, SaveInternal, "saveInternal");
   pragma Import (Java, Save, "save");
   pragma Import (Java, GetListeners, "getListeners");

end Java.Awt.AWTEventMulticaster;
pragma Import (Java, Java.Awt.AWTEventMulticaster, "java.awt.AWTEventMulticaster");
pragma Extensions_Allowed (Off);
