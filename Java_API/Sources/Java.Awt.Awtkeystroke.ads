pragma Extensions_Allowed (On);
limited with Java.Awt.Event.KeyEvent;
limited with Java.Lang.Character;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.AWTKeyStroke is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AWTKeyStroke (This : Ref := null)
                              return Ref;

   --  protected
   function New_AWTKeyStroke (P1_Char : Java.Char;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure RegisterSubclass (P1_Class : access Standard.Java.Lang.Class.Typ'Class);

   function GetAWTKeyStroke (P1_Char : Java.Char)
                             return access Java.Awt.AWTKeyStroke.Typ'Class;

   function GetAWTKeyStroke (P1_Character : access Standard.Java.Lang.Character.Typ'Class;
                             P2_Int : Java.Int)
                             return access Java.Awt.AWTKeyStroke.Typ'Class;

   function GetAWTKeyStroke (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Boolean : Java.Boolean)
                             return access Java.Awt.AWTKeyStroke.Typ'Class;

   function GetAWTKeyStroke (P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Java.Awt.AWTKeyStroke.Typ'Class;

   function GetAWTKeyStrokeForEvent (P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class)
                                     return access Java.Awt.AWTKeyStroke.Typ'Class;

   function GetAWTKeyStroke (P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Awt.AWTKeyStroke.Typ'Class;

   --  final
   function GetKeyChar (This : access Typ)
                        return Java.Char;

   --  final
   function GetKeyCode (This : access Typ)
                        return Java.Int;

   --  final
   function GetModifiers (This : access Typ)
                          return Java.Int;

   --  final
   function IsOnKeyRelease (This : access Typ)
                            return Java.Boolean;

   --  final
   function GetKeyEventType (This : access Typ)
                             return Java.Int;

   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.ObjectStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AWTKeyStroke);
   pragma Import (Java, RegisterSubclass, "registerSubclass");
   pragma Import (Java, GetAWTKeyStroke, "getAWTKeyStroke");
   pragma Import (Java, GetAWTKeyStrokeForEvent, "getAWTKeyStrokeForEvent");
   pragma Import (Java, GetKeyChar, "getKeyChar");
   pragma Import (Java, GetKeyCode, "getKeyCode");
   pragma Import (Java, GetModifiers, "getModifiers");
   pragma Import (Java, IsOnKeyRelease, "isOnKeyRelease");
   pragma Import (Java, GetKeyEventType, "getKeyEventType");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ReadResolve, "readResolve");

end Java.Awt.AWTKeyStroke;
pragma Import (Java, Java.Awt.AWTKeyStroke, "java.awt.AWTKeyStroke");
pragma Extensions_Allowed (Off);
