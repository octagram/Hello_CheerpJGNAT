pragma Extensions_Allowed (On);
limited with Java.Awt.Shape;
with Java.Awt.Stroke;
with Java.Lang.Object;

package Java.Awt.BasicStroke is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Stroke_I : Java.Awt.Stroke.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicStroke (P1_Float : Java.Float;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Float : Java.Float;
                             P5_Float_Arr : Java.Float_Arr;
                             P6_Float : Java.Float; 
                             This : Ref := null)
                             return Ref;

   function New_BasicStroke (P1_Float : Java.Float;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Float : Java.Float; 
                             This : Ref := null)
                             return Ref;

   function New_BasicStroke (P1_Float : Java.Float;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_BasicStroke (P1_Float : Java.Float; 
                             This : Ref := null)
                             return Ref;

   function New_BasicStroke (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateStrokedShape (This : access Typ;
                                P1_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                return access Java.Awt.Shape.Typ'Class;

   function GetLineWidth (This : access Typ)
                          return Java.Float;

   function GetEndCap (This : access Typ)
                       return Java.Int;

   function GetLineJoin (This : access Typ)
                         return Java.Int;

   function GetMiterLimit (This : access Typ)
                           return Java.Float;

   function GetDashArray (This : access Typ)
                          return Java.Float_Arr;

   function GetDashPhase (This : access Typ)
                          return Java.Float;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JOIN_MITER : constant Java.Int;

   --  final
   JOIN_ROUND : constant Java.Int;

   --  final
   JOIN_BEVEL : constant Java.Int;

   --  final
   CAP_BUTT : constant Java.Int;

   --  final
   CAP_ROUND : constant Java.Int;

   --  final
   CAP_SQUARE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicStroke);
   pragma Import (Java, CreateStrokedShape, "createStrokedShape");
   pragma Import (Java, GetLineWidth, "getLineWidth");
   pragma Import (Java, GetEndCap, "getEndCap");
   pragma Import (Java, GetLineJoin, "getLineJoin");
   pragma Import (Java, GetMiterLimit, "getMiterLimit");
   pragma Import (Java, GetDashArray, "getDashArray");
   pragma Import (Java, GetDashPhase, "getDashPhase");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, JOIN_MITER, "JOIN_MITER");
   pragma Import (Java, JOIN_ROUND, "JOIN_ROUND");
   pragma Import (Java, JOIN_BEVEL, "JOIN_BEVEL");
   pragma Import (Java, CAP_BUTT, "CAP_BUTT");
   pragma Import (Java, CAP_ROUND, "CAP_ROUND");
   pragma Import (Java, CAP_SQUARE, "CAP_SQUARE");

end Java.Awt.BasicStroke;
pragma Import (Java, Java.Awt.BasicStroke, "java.awt.BasicStroke");
pragma Extensions_Allowed (Off);
