pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
with Java.Awt.LayoutManager2;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.BorderLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BorderLayout (This : Ref := null)
                              return Ref;

   function New_BorderLayout (P1_Int : Java.Int;
                              P2_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHgap (This : access Typ)
                     return Java.Int;

   procedure SetHgap (This : access Typ;
                      P1_Int : Java.Int);

   function GetVgap (This : access Typ)
                     return Java.Int;

   procedure SetVgap (This : access Typ;
                      P1_Int : Java.Int);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetLayoutComponent (This : access Typ;
                                P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                return access Java.Awt.Component.Typ'Class;

   function GetLayoutComponent (This : access Typ;
                                P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                return access Java.Awt.Component.Typ'Class;

   function GetConstraints (This : access Typ;
                            P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return access Java.Lang.Object.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MaximumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function GetLayoutAlignmentX (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   function GetLayoutAlignmentY (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   procedure InvalidateLayout (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NORTH : constant access Java.Lang.String.Typ'Class;

   --  final
   SOUTH : constant access Java.Lang.String.Typ'Class;

   --  final
   EAST : constant access Java.Lang.String.Typ'Class;

   --  final
   WEST : constant access Java.Lang.String.Typ'Class;

   --  final
   CENTER : constant access Java.Lang.String.Typ'Class;

   --  final
   BEFORE_FIRST_LINE : constant access Java.Lang.String.Typ'Class;

   --  final
   AFTER_LAST_LINE : constant access Java.Lang.String.Typ'Class;

   --  final
   BEFORE_LINE_BEGINS : constant access Java.Lang.String.Typ'Class;

   --  final
   AFTER_LINE_ENDS : constant access Java.Lang.String.Typ'Class;

   --  final
   PAGE_START : constant access Java.Lang.String.Typ'Class;

   --  final
   PAGE_END : constant access Java.Lang.String.Typ'Class;

   --  final
   LINE_START : constant access Java.Lang.String.Typ'Class;

   --  final
   LINE_END : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BorderLayout);
   pragma Import (Java, GetHgap, "getHgap");
   pragma Import (Java, SetHgap, "setHgap");
   pragma Import (Java, GetVgap, "getVgap");
   pragma Import (Java, SetVgap, "setVgap");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, GetLayoutComponent, "getLayoutComponent");
   pragma Import (Java, GetConstraints, "getConstraints");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MaximumLayoutSize, "maximumLayoutSize");
   pragma Import (Java, GetLayoutAlignmentX, "getLayoutAlignmentX");
   pragma Import (Java, GetLayoutAlignmentY, "getLayoutAlignmentY");
   pragma Import (Java, InvalidateLayout, "invalidateLayout");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, NORTH, "NORTH");
   pragma Import (Java, SOUTH, "SOUTH");
   pragma Import (Java, EAST, "EAST");
   pragma Import (Java, WEST, "WEST");
   pragma Import (Java, CENTER, "CENTER");
   pragma Import (Java, BEFORE_FIRST_LINE, "BEFORE_FIRST_LINE");
   pragma Import (Java, AFTER_LAST_LINE, "AFTER_LAST_LINE");
   pragma Import (Java, BEFORE_LINE_BEGINS, "BEFORE_LINE_BEGINS");
   pragma Import (Java, AFTER_LINE_ENDS, "AFTER_LINE_ENDS");
   pragma Import (Java, PAGE_START, "PAGE_START");
   pragma Import (Java, PAGE_END, "PAGE_END");
   pragma Import (Java, LINE_START, "LINE_START");
   pragma Import (Java, LINE_END, "LINE_END");

end Java.Awt.BorderLayout;
pragma Import (Java, Java.Awt.BorderLayout, "java.awt.BorderLayout");
pragma Extensions_Allowed (Off);
