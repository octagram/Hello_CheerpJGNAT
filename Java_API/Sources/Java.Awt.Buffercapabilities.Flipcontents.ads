pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.BufferCapabilities.FlipContents is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNDEFINED : access Java.Awt.BufferCapabilities.FlipContents.Typ'Class;

   --  final
   BACKGROUND : access Java.Awt.BufferCapabilities.FlipContents.Typ'Class;

   --  final
   PRIOR : access Java.Awt.BufferCapabilities.FlipContents.Typ'Class;

   --  final
   COPIED : access Java.Awt.BufferCapabilities.FlipContents.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, UNDEFINED, "UNDEFINED");
   pragma Import (Java, BACKGROUND, "BACKGROUND");
   pragma Import (Java, PRIOR, "PRIOR");
   pragma Import (Java, COPIED, "COPIED");

end Java.Awt.BufferCapabilities.FlipContents;
pragma Import (Java, Java.Awt.BufferCapabilities.FlipContents, "java.awt.BufferCapabilities$FlipContents");
pragma Extensions_Allowed (Off);
