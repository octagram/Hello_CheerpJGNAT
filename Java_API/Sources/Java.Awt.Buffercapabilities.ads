pragma Extensions_Allowed (On);
limited with Java.Awt.BufferCapabilities.FlipContents;
limited with Java.Awt.ImageCapabilities;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.BufferCapabilities is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BufferCapabilities (P1_ImageCapabilities : access Standard.Java.Awt.ImageCapabilities.Typ'Class;
                                    P2_ImageCapabilities : access Standard.Java.Awt.ImageCapabilities.Typ'Class;
                                    P3_FlipContents : access Standard.Java.Awt.BufferCapabilities.FlipContents.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFrontBufferCapabilities (This : access Typ)
                                        return access Java.Awt.ImageCapabilities.Typ'Class;

   function GetBackBufferCapabilities (This : access Typ)
                                       return access Java.Awt.ImageCapabilities.Typ'Class;

   function IsPageFlipping (This : access Typ)
                            return Java.Boolean;

   function GetFlipContents (This : access Typ)
                             return access Java.Awt.BufferCapabilities.FlipContents.Typ'Class;

   function IsFullScreenRequired (This : access Typ)
                                  return Java.Boolean;

   function IsMultiBufferAvailable (This : access Typ)
                                    return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BufferCapabilities);
   pragma Import (Java, GetFrontBufferCapabilities, "getFrontBufferCapabilities");
   pragma Import (Java, GetBackBufferCapabilities, "getBackBufferCapabilities");
   pragma Import (Java, IsPageFlipping, "isPageFlipping");
   pragma Import (Java, GetFlipContents, "getFlipContents");
   pragma Import (Java, IsFullScreenRequired, "isFullScreenRequired");
   pragma Import (Java, IsMultiBufferAvailable, "isMultiBufferAvailable");
   pragma Import (Java, Clone, "clone");

end Java.Awt.BufferCapabilities;
pragma Import (Java, Java.Awt.BufferCapabilities, "java.awt.BufferCapabilities");
pragma Extensions_Allowed (Off);
