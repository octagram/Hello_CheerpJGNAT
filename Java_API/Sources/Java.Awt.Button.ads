pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ActionListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Button is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Button (This : Ref := null)
                        return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Button (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetLabel (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetLabel (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessActionEvent (This : access Typ;
                                 P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Button);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetLabel, "getLabel");
   pragma Import (Java, SetLabel, "setLabel");
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessActionEvent, "processActionEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.Button;
pragma Import (Java, Java.Awt.Button, "java.awt.Button");
pragma Extensions_Allowed (Off);
