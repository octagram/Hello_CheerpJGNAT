pragma Extensions_Allowed (On);
limited with Java.Awt.BufferCapabilities;
limited with Java.Awt.Graphics;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Image.BufferStrategy;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Canvas is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Canvas (This : Ref := null)
                        return Ref;

   function New_Canvas (P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure CreateBufferStrategy (This : access Typ;
                                   P1_Int : Java.Int);

   procedure CreateBufferStrategy (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_BufferCapabilities : access Standard.Java.Awt.BufferCapabilities.Typ'Class);
   --  can raise Java.Awt.AWTException.Except

   function GetBufferStrategy (This : access Typ)
                               return access Java.Awt.Image.BufferStrategy.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Canvas);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Update, "update");
   pragma Import (Java, CreateBufferStrategy, "createBufferStrategy");
   pragma Import (Java, GetBufferStrategy, "getBufferStrategy");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.Canvas;
pragma Import (Java, Java.Awt.Canvas, "java.awt.Canvas");
pragma Extensions_Allowed (Off);
