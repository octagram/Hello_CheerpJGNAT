pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.CheckboxGroup;
limited with Java.Awt.Event.ItemEvent;
limited with Java.Awt.Event.ItemListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Checkbox is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Checkbox (This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Checkbox (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Checkbox (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Boolean : Java.Boolean; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Checkbox (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Boolean : Java.Boolean;
                          P3_CheckboxGroup : access Standard.Java.Awt.CheckboxGroup.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Checkbox (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_CheckboxGroup : access Standard.Java.Awt.CheckboxGroup.Typ'Class;
                          P3_Boolean : Java.Boolean; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetLabel (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetLabel (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetState (This : access Typ)
                      return Java.Boolean;

   procedure SetState (This : access Typ;
                       P1_Boolean : Java.Boolean);

   function GetSelectedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetCheckboxGroup (This : access Typ)
                              return access Java.Awt.CheckboxGroup.Typ'Class;

   procedure SetCheckboxGroup (This : access Typ;
                               P1_CheckboxGroup : access Standard.Java.Awt.CheckboxGroup.Typ'Class);

   --  synchronized
   procedure AddItemListener (This : access Typ;
                              P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   --  synchronized
   procedure RemoveItemListener (This : access Typ;
                                 P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   --  synchronized
   function GetItemListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessItemEvent (This : access Typ;
                               P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Checkbox);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetLabel, "getLabel");
   pragma Import (Java, SetLabel, "setLabel");
   pragma Import (Java, GetState, "getState");
   pragma Import (Java, SetState, "setState");
   pragma Import (Java, GetSelectedObjects, "getSelectedObjects");
   pragma Import (Java, GetCheckboxGroup, "getCheckboxGroup");
   pragma Import (Java, SetCheckboxGroup, "setCheckboxGroup");
   pragma Import (Java, AddItemListener, "addItemListener");
   pragma Import (Java, RemoveItemListener, "removeItemListener");
   pragma Import (Java, GetItemListeners, "getItemListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessItemEvent, "processItemEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.Checkbox;
pragma Import (Java, Java.Awt.Checkbox, "java.awt.Checkbox");
pragma Extensions_Allowed (Off);
