pragma Extensions_Allowed (On);
limited with Java.Awt.Checkbox;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.CheckboxGroup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CheckboxGroup (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSelectedCheckbox (This : access Typ)
                                 return access Java.Awt.Checkbox.Typ'Class;

   procedure SetSelectedCheckbox (This : access Typ;
                                  P1_Checkbox : access Standard.Java.Awt.Checkbox.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CheckboxGroup);
   pragma Import (Java, GetSelectedCheckbox, "getSelectedCheckbox");
   pragma Import (Java, SetSelectedCheckbox, "setSelectedCheckbox");
   pragma Import (Java, ToString, "toString");

end Java.Awt.CheckboxGroup;
pragma Import (Java, Java.Awt.CheckboxGroup, "java.awt.CheckboxGroup");
pragma Extensions_Allowed (Off);
