pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Event.ItemEvent;
limited with Java.Awt.Event.ItemListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Choice is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Choice (This : Ref := null)
                        return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetItemCount (This : access Typ)
                          return Java.Int;

   function GetItem (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   procedure Add (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AddItem (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Insert (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure RemoveAll (This : access Typ);

   --  synchronized
   function GetSelectedItem (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetSelectedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetSelectedIndex (This : access Typ)
                              return Java.Int;

   --  synchronized
   procedure select_K (This : access Typ;
                       P1_Int : Java.Int);

   --  synchronized
   procedure select_K (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   procedure AddItemListener (This : access Typ;
                              P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   --  synchronized
   procedure RemoveItemListener (This : access Typ;
                                 P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   --  synchronized
   function GetItemListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessItemEvent (This : access Typ;
                               P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Choice);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetItemCount, "getItemCount");
   pragma Import (Java, GetItem, "getItem");
   pragma Import (Java, Add, "add");
   pragma Import (Java, AddItem, "addItem");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, GetSelectedItem, "getSelectedItem");
   pragma Import (Java, GetSelectedObjects, "getSelectedObjects");
   pragma Import (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Import (Java, select_K, "select");
   pragma Import (Java, AddItemListener, "addItemListener");
   pragma Import (Java, RemoveItemListener, "removeItemListener");
   pragma Import (Java, GetItemListeners, "getItemListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessItemEvent, "processItemEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.Choice;
pragma Import (Java, Java.Awt.Choice, "java.awt.Choice");
pragma Extensions_Allowed (Off);
