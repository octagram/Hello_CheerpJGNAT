pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Color.ColorSpace is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_ColorSpace (P1_Int : Java.Int;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_Int : Java.Int)
                         return access Java.Awt.Color.ColorSpace.Typ'Class;

   function IsCS_sRGB (This : access Typ)
                       return Java.Boolean;

   function ToRGB (This : access Typ;
                   P1_Float_Arr : Java.Float_Arr)
                   return Java.Float_Arr is abstract;

   function FromRGB (This : access Typ;
                     P1_Float_Arr : Java.Float_Arr)
                     return Java.Float_Arr is abstract;

   function ToCIEXYZ (This : access Typ;
                      P1_Float_Arr : Java.Float_Arr)
                      return Java.Float_Arr is abstract;

   function FromCIEXYZ (This : access Typ;
                        P1_Float_Arr : Java.Float_Arr)
                        return Java.Float_Arr is abstract;

   function GetType (This : access Typ)
                     return Java.Int;

   function GetNumComponents (This : access Typ)
                              return Java.Int;

   function GetName (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   function GetMinValue (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Float;

   function GetMaxValue (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Float;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_XYZ : constant Java.Int;

   --  final
   TYPE_Lab : constant Java.Int;

   --  final
   TYPE_Luv : constant Java.Int;

   --  final
   TYPE_YCbCr : constant Java.Int;

   --  final
   TYPE_Yxy : constant Java.Int;

   --  final
   TYPE_RGB : constant Java.Int;

   --  final
   TYPE_GRAY : constant Java.Int;

   --  final
   TYPE_HSV : constant Java.Int;

   --  final
   TYPE_HLS : constant Java.Int;

   --  final
   TYPE_CMYK : constant Java.Int;

   --  final
   TYPE_CMY : constant Java.Int;

   --  final
   TYPE_2CLR : constant Java.Int;

   --  final
   TYPE_3CLR : constant Java.Int;

   --  final
   TYPE_4CLR : constant Java.Int;

   --  final
   TYPE_5CLR : constant Java.Int;

   --  final
   TYPE_6CLR : constant Java.Int;

   --  final
   TYPE_7CLR : constant Java.Int;

   --  final
   TYPE_8CLR : constant Java.Int;

   --  final
   TYPE_9CLR : constant Java.Int;

   --  final
   TYPE_ACLR : constant Java.Int;

   --  final
   TYPE_BCLR : constant Java.Int;

   --  final
   TYPE_CCLR : constant Java.Int;

   --  final
   TYPE_DCLR : constant Java.Int;

   --  final
   TYPE_ECLR : constant Java.Int;

   --  final
   TYPE_FCLR : constant Java.Int;

   --  final
   CS_sRGB : constant Java.Int;

   --  final
   CS_LINEAR_RGB : constant Java.Int;

   --  final
   CS_CIEXYZ : constant Java.Int;

   --  final
   CS_PYCC : constant Java.Int;

   --  final
   CS_GRAY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ColorSpace);
   pragma Export (Java, GetInstance, "getInstance");
   pragma Export (Java, IsCS_sRGB, "isCS_sRGB");
   pragma Export (Java, ToRGB, "toRGB");
   pragma Export (Java, FromRGB, "fromRGB");
   pragma Export (Java, ToCIEXYZ, "toCIEXYZ");
   pragma Export (Java, FromCIEXYZ, "fromCIEXYZ");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetNumComponents, "getNumComponents");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetMinValue, "getMinValue");
   pragma Export (Java, GetMaxValue, "getMaxValue");
   pragma Import (Java, TYPE_XYZ, "TYPE_XYZ");
   pragma Import (Java, TYPE_Lab, "TYPE_Lab");
   pragma Import (Java, TYPE_Luv, "TYPE_Luv");
   pragma Import (Java, TYPE_YCbCr, "TYPE_YCbCr");
   pragma Import (Java, TYPE_Yxy, "TYPE_Yxy");
   pragma Import (Java, TYPE_RGB, "TYPE_RGB");
   pragma Import (Java, TYPE_GRAY, "TYPE_GRAY");
   pragma Import (Java, TYPE_HSV, "TYPE_HSV");
   pragma Import (Java, TYPE_HLS, "TYPE_HLS");
   pragma Import (Java, TYPE_CMYK, "TYPE_CMYK");
   pragma Import (Java, TYPE_CMY, "TYPE_CMY");
   pragma Import (Java, TYPE_2CLR, "TYPE_2CLR");
   pragma Import (Java, TYPE_3CLR, "TYPE_3CLR");
   pragma Import (Java, TYPE_4CLR, "TYPE_4CLR");
   pragma Import (Java, TYPE_5CLR, "TYPE_5CLR");
   pragma Import (Java, TYPE_6CLR, "TYPE_6CLR");
   pragma Import (Java, TYPE_7CLR, "TYPE_7CLR");
   pragma Import (Java, TYPE_8CLR, "TYPE_8CLR");
   pragma Import (Java, TYPE_9CLR, "TYPE_9CLR");
   pragma Import (Java, TYPE_ACLR, "TYPE_ACLR");
   pragma Import (Java, TYPE_BCLR, "TYPE_BCLR");
   pragma Import (Java, TYPE_CCLR, "TYPE_CCLR");
   pragma Import (Java, TYPE_DCLR, "TYPE_DCLR");
   pragma Import (Java, TYPE_ECLR, "TYPE_ECLR");
   pragma Import (Java, TYPE_FCLR, "TYPE_FCLR");
   pragma Import (Java, CS_sRGB, "CS_sRGB");
   pragma Import (Java, CS_LINEAR_RGB, "CS_LINEAR_RGB");
   pragma Import (Java, CS_CIEXYZ, "CS_CIEXYZ");
   pragma Import (Java, CS_PYCC, "CS_PYCC");
   pragma Import (Java, CS_GRAY, "CS_GRAY");

end Java.Awt.Color.ColorSpace;
pragma Import (Java, Java.Awt.Color.ColorSpace, "java.awt.color.ColorSpace");
pragma Extensions_Allowed (Off);
