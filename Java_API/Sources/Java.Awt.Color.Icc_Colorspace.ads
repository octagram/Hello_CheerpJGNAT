pragma Extensions_Allowed (On);
limited with Java.Awt.Color.ICC_Profile;
with Java.Awt.Color.ColorSpace;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Color.ICC_ColorSpace is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Color.ColorSpace.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ICC_ColorSpace (P1_ICC_Profile : access Standard.Java.Awt.Color.ICC_Profile.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetProfile (This : access Typ)
                        return access Java.Awt.Color.ICC_Profile.Typ'Class;

   function ToRGB (This : access Typ;
                   P1_Float_Arr : Java.Float_Arr)
                   return Java.Float_Arr;

   function FromRGB (This : access Typ;
                     P1_Float_Arr : Java.Float_Arr)
                     return Java.Float_Arr;

   function ToCIEXYZ (This : access Typ;
                      P1_Float_Arr : Java.Float_Arr)
                      return Java.Float_Arr;

   function FromCIEXYZ (This : access Typ;
                        P1_Float_Arr : Java.Float_Arr)
                        return Java.Float_Arr;

   function GetMinValue (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Float;

   function GetMaxValue (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Float;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ICC_ColorSpace);
   pragma Import (Java, GetProfile, "getProfile");
   pragma Import (Java, ToRGB, "toRGB");
   pragma Import (Java, FromRGB, "fromRGB");
   pragma Import (Java, ToCIEXYZ, "toCIEXYZ");
   pragma Import (Java, FromCIEXYZ, "fromCIEXYZ");
   pragma Import (Java, GetMinValue, "getMinValue");
   pragma Import (Java, GetMaxValue, "getMaxValue");

end Java.Awt.Color.ICC_ColorSpace;
pragma Import (Java, Java.Awt.Color.ICC_ColorSpace, "java.awt.color.ICC_ColorSpace");
pragma Extensions_Allowed (Off);
