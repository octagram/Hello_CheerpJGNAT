pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Color.ICC_Profile is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Finalize (This : access Typ);

   function GetInstance (P1_Byte_Arr : Java.Byte_Arr)
                         return access Java.Awt.Color.ICC_Profile.Typ'Class;

   function GetInstance (P1_Int : Java.Int)
                         return access Java.Awt.Color.ICC_Profile.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Awt.Color.ICC_Profile.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetInstance (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                         return access Java.Awt.Color.ICC_Profile.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetMajorVersion (This : access Typ)
                             return Java.Int;

   function GetMinorVersion (This : access Typ)
                             return Java.Int;

   function GetProfileClass (This : access Typ)
                             return Java.Int;

   function GetColorSpaceType (This : access Typ)
                               return Java.Int;

   function GetPCSType (This : access Typ)
                        return Java.Int;

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetData (This : access Typ)
                     return Java.Byte_Arr;

   function GetData (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Byte_Arr;

   procedure SetData (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Byte_Arr : Java.Byte_Arr);

   function GetNumComponents (This : access Typ)
                              return Java.Int;

   --  protected
   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.ObjectStreamException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CLASS_INPUT : constant Java.Int;

   --  final
   CLASS_DISPLAY : constant Java.Int;

   --  final
   CLASS_OUTPUT : constant Java.Int;

   --  final
   CLASS_DEVICELINK : constant Java.Int;

   --  final
   CLASS_COLORSPACECONVERSION : constant Java.Int;

   --  final
   CLASS_ABSTRACT : constant Java.Int;

   --  final
   CLASS_NAMEDCOLOR : constant Java.Int;

   --  final
   IcSigXYZData : constant Java.Int;

   --  final
   IcSigLabData : constant Java.Int;

   --  final
   IcSigLuvData : constant Java.Int;

   --  final
   IcSigYCbCrData : constant Java.Int;

   --  final
   IcSigYxyData : constant Java.Int;

   --  final
   IcSigRgbData : constant Java.Int;

   --  final
   IcSigGrayData : constant Java.Int;

   --  final
   IcSigHsvData : constant Java.Int;

   --  final
   IcSigHlsData : constant Java.Int;

   --  final
   IcSigCmykData : constant Java.Int;

   --  final
   IcSigCmyData : constant Java.Int;

   --  final
   IcSigSpace2CLR : constant Java.Int;

   --  final
   IcSigSpace3CLR : constant Java.Int;

   --  final
   IcSigSpace4CLR : constant Java.Int;

   --  final
   IcSigSpace5CLR : constant Java.Int;

   --  final
   IcSigSpace6CLR : constant Java.Int;

   --  final
   IcSigSpace7CLR : constant Java.Int;

   --  final
   IcSigSpace8CLR : constant Java.Int;

   --  final
   IcSigSpace9CLR : constant Java.Int;

   --  final
   IcSigSpaceACLR : constant Java.Int;

   --  final
   IcSigSpaceBCLR : constant Java.Int;

   --  final
   IcSigSpaceCCLR : constant Java.Int;

   --  final
   IcSigSpaceDCLR : constant Java.Int;

   --  final
   IcSigSpaceECLR : constant Java.Int;

   --  final
   IcSigSpaceFCLR : constant Java.Int;

   --  final
   IcSigInputClass : constant Java.Int;

   --  final
   IcSigDisplayClass : constant Java.Int;

   --  final
   IcSigOutputClass : constant Java.Int;

   --  final
   IcSigLinkClass : constant Java.Int;

   --  final
   IcSigAbstractClass : constant Java.Int;

   --  final
   IcSigColorSpaceClass : constant Java.Int;

   --  final
   IcSigNamedColorClass : constant Java.Int;

   --  final
   IcPerceptual : constant Java.Int;

   --  final
   IcRelativeColorimetric : constant Java.Int;

   --  final
   IcMediaRelativeColorimetric : constant Java.Int;

   --  final
   IcSaturation : constant Java.Int;

   --  final
   IcAbsoluteColorimetric : constant Java.Int;

   --  final
   IcICCAbsoluteColorimetric : constant Java.Int;

   --  final
   IcSigHead : constant Java.Int;

   --  final
   IcSigAToB0Tag : constant Java.Int;

   --  final
   IcSigAToB1Tag : constant Java.Int;

   --  final
   IcSigAToB2Tag : constant Java.Int;

   --  final
   IcSigBlueColorantTag : constant Java.Int;

   --  final
   IcSigBlueMatrixColumnTag : constant Java.Int;

   --  final
   IcSigBlueTRCTag : constant Java.Int;

   --  final
   IcSigBToA0Tag : constant Java.Int;

   --  final
   IcSigBToA1Tag : constant Java.Int;

   --  final
   IcSigBToA2Tag : constant Java.Int;

   --  final
   IcSigCalibrationDateTimeTag : constant Java.Int;

   --  final
   IcSigCharTargetTag : constant Java.Int;

   --  final
   IcSigCopyrightTag : constant Java.Int;

   --  final
   IcSigCrdInfoTag : constant Java.Int;

   --  final
   IcSigDeviceMfgDescTag : constant Java.Int;

   --  final
   IcSigDeviceModelDescTag : constant Java.Int;

   --  final
   IcSigDeviceSettingsTag : constant Java.Int;

   --  final
   IcSigGamutTag : constant Java.Int;

   --  final
   IcSigGrayTRCTag : constant Java.Int;

   --  final
   IcSigGreenColorantTag : constant Java.Int;

   --  final
   IcSigGreenMatrixColumnTag : constant Java.Int;

   --  final
   IcSigGreenTRCTag : constant Java.Int;

   --  final
   IcSigLuminanceTag : constant Java.Int;

   --  final
   IcSigMeasurementTag : constant Java.Int;

   --  final
   IcSigMediaBlackPointTag : constant Java.Int;

   --  final
   IcSigMediaWhitePointTag : constant Java.Int;

   --  final
   IcSigNamedColor2Tag : constant Java.Int;

   --  final
   IcSigOutputResponseTag : constant Java.Int;

   --  final
   IcSigPreview0Tag : constant Java.Int;

   --  final
   IcSigPreview1Tag : constant Java.Int;

   --  final
   IcSigPreview2Tag : constant Java.Int;

   --  final
   IcSigProfileDescriptionTag : constant Java.Int;

   --  final
   IcSigProfileSequenceDescTag : constant Java.Int;

   --  final
   IcSigPs2CRD0Tag : constant Java.Int;

   --  final
   IcSigPs2CRD1Tag : constant Java.Int;

   --  final
   IcSigPs2CRD2Tag : constant Java.Int;

   --  final
   IcSigPs2CRD3Tag : constant Java.Int;

   --  final
   IcSigPs2CSATag : constant Java.Int;

   --  final
   IcSigPs2RenderingIntentTag : constant Java.Int;

   --  final
   IcSigRedColorantTag : constant Java.Int;

   --  final
   IcSigRedMatrixColumnTag : constant Java.Int;

   --  final
   IcSigRedTRCTag : constant Java.Int;

   --  final
   IcSigScreeningDescTag : constant Java.Int;

   --  final
   IcSigScreeningTag : constant Java.Int;

   --  final
   IcSigTechnologyTag : constant Java.Int;

   --  final
   IcSigUcrBgTag : constant Java.Int;

   --  final
   IcSigViewingCondDescTag : constant Java.Int;

   --  final
   IcSigViewingConditionsTag : constant Java.Int;

   --  final
   IcSigChromaticityTag : constant Java.Int;

   --  final
   IcSigChromaticAdaptationTag : constant Java.Int;

   --  final
   IcSigColorantOrderTag : constant Java.Int;

   --  final
   IcSigColorantTableTag : constant Java.Int;

   --  final
   IcHdrSize : constant Java.Int;

   --  final
   IcHdrCmmId : constant Java.Int;

   --  final
   IcHdrVersion : constant Java.Int;

   --  final
   IcHdrDeviceClass : constant Java.Int;

   --  final
   IcHdrColorSpace : constant Java.Int;

   --  final
   IcHdrPcs : constant Java.Int;

   --  final
   IcHdrDate : constant Java.Int;

   --  final
   IcHdrMagic : constant Java.Int;

   --  final
   IcHdrPlatform : constant Java.Int;

   --  final
   IcHdrFlags : constant Java.Int;

   --  final
   IcHdrManufacturer : constant Java.Int;

   --  final
   IcHdrModel : constant Java.Int;

   --  final
   IcHdrAttributes : constant Java.Int;

   --  final
   IcHdrRenderingIntent : constant Java.Int;

   --  final
   IcHdrIlluminant : constant Java.Int;

   --  final
   IcHdrCreator : constant Java.Int;

   --  final
   IcHdrProfileID : constant Java.Int;

   --  final
   IcTagType : constant Java.Int;

   --  final
   IcTagReserved : constant Java.Int;

   --  final
   IcCurveCount : constant Java.Int;

   --  final
   IcCurveData : constant Java.Int;

   --  final
   IcXYZNumberX : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Finalize, "finalize");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetMajorVersion, "getMajorVersion");
   pragma Import (Java, GetMinorVersion, "getMinorVersion");
   pragma Import (Java, GetProfileClass, "getProfileClass");
   pragma Import (Java, GetColorSpaceType, "getColorSpaceType");
   pragma Import (Java, GetPCSType, "getPCSType");
   pragma Import (Java, Write, "write");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, SetData, "setData");
   pragma Import (Java, GetNumComponents, "getNumComponents");
   pragma Import (Java, ReadResolve, "readResolve");
   pragma Import (Java, CLASS_INPUT, "CLASS_INPUT");
   pragma Import (Java, CLASS_DISPLAY, "CLASS_DISPLAY");
   pragma Import (Java, CLASS_OUTPUT, "CLASS_OUTPUT");
   pragma Import (Java, CLASS_DEVICELINK, "CLASS_DEVICELINK");
   pragma Import (Java, CLASS_COLORSPACECONVERSION, "CLASS_COLORSPACECONVERSION");
   pragma Import (Java, CLASS_ABSTRACT, "CLASS_ABSTRACT");
   pragma Import (Java, CLASS_NAMEDCOLOR, "CLASS_NAMEDCOLOR");
   pragma Import (Java, IcSigXYZData, "icSigXYZData");
   pragma Import (Java, IcSigLabData, "icSigLabData");
   pragma Import (Java, IcSigLuvData, "icSigLuvData");
   pragma Import (Java, IcSigYCbCrData, "icSigYCbCrData");
   pragma Import (Java, IcSigYxyData, "icSigYxyData");
   pragma Import (Java, IcSigRgbData, "icSigRgbData");
   pragma Import (Java, IcSigGrayData, "icSigGrayData");
   pragma Import (Java, IcSigHsvData, "icSigHsvData");
   pragma Import (Java, IcSigHlsData, "icSigHlsData");
   pragma Import (Java, IcSigCmykData, "icSigCmykData");
   pragma Import (Java, IcSigCmyData, "icSigCmyData");
   pragma Import (Java, IcSigSpace2CLR, "icSigSpace2CLR");
   pragma Import (Java, IcSigSpace3CLR, "icSigSpace3CLR");
   pragma Import (Java, IcSigSpace4CLR, "icSigSpace4CLR");
   pragma Import (Java, IcSigSpace5CLR, "icSigSpace5CLR");
   pragma Import (Java, IcSigSpace6CLR, "icSigSpace6CLR");
   pragma Import (Java, IcSigSpace7CLR, "icSigSpace7CLR");
   pragma Import (Java, IcSigSpace8CLR, "icSigSpace8CLR");
   pragma Import (Java, IcSigSpace9CLR, "icSigSpace9CLR");
   pragma Import (Java, IcSigSpaceACLR, "icSigSpaceACLR");
   pragma Import (Java, IcSigSpaceBCLR, "icSigSpaceBCLR");
   pragma Import (Java, IcSigSpaceCCLR, "icSigSpaceCCLR");
   pragma Import (Java, IcSigSpaceDCLR, "icSigSpaceDCLR");
   pragma Import (Java, IcSigSpaceECLR, "icSigSpaceECLR");
   pragma Import (Java, IcSigSpaceFCLR, "icSigSpaceFCLR");
   pragma Import (Java, IcSigInputClass, "icSigInputClass");
   pragma Import (Java, IcSigDisplayClass, "icSigDisplayClass");
   pragma Import (Java, IcSigOutputClass, "icSigOutputClass");
   pragma Import (Java, IcSigLinkClass, "icSigLinkClass");
   pragma Import (Java, IcSigAbstractClass, "icSigAbstractClass");
   pragma Import (Java, IcSigColorSpaceClass, "icSigColorSpaceClass");
   pragma Import (Java, IcSigNamedColorClass, "icSigNamedColorClass");
   pragma Import (Java, IcPerceptual, "icPerceptual");
   pragma Import (Java, IcRelativeColorimetric, "icRelativeColorimetric");
   pragma Import (Java, IcMediaRelativeColorimetric, "icMediaRelativeColorimetric");
   pragma Import (Java, IcSaturation, "icSaturation");
   pragma Import (Java, IcAbsoluteColorimetric, "icAbsoluteColorimetric");
   pragma Import (Java, IcICCAbsoluteColorimetric, "icICCAbsoluteColorimetric");
   pragma Import (Java, IcSigHead, "icSigHead");
   pragma Import (Java, IcSigAToB0Tag, "icSigAToB0Tag");
   pragma Import (Java, IcSigAToB1Tag, "icSigAToB1Tag");
   pragma Import (Java, IcSigAToB2Tag, "icSigAToB2Tag");
   pragma Import (Java, IcSigBlueColorantTag, "icSigBlueColorantTag");
   pragma Import (Java, IcSigBlueMatrixColumnTag, "icSigBlueMatrixColumnTag");
   pragma Import (Java, IcSigBlueTRCTag, "icSigBlueTRCTag");
   pragma Import (Java, IcSigBToA0Tag, "icSigBToA0Tag");
   pragma Import (Java, IcSigBToA1Tag, "icSigBToA1Tag");
   pragma Import (Java, IcSigBToA2Tag, "icSigBToA2Tag");
   pragma Import (Java, IcSigCalibrationDateTimeTag, "icSigCalibrationDateTimeTag");
   pragma Import (Java, IcSigCharTargetTag, "icSigCharTargetTag");
   pragma Import (Java, IcSigCopyrightTag, "icSigCopyrightTag");
   pragma Import (Java, IcSigCrdInfoTag, "icSigCrdInfoTag");
   pragma Import (Java, IcSigDeviceMfgDescTag, "icSigDeviceMfgDescTag");
   pragma Import (Java, IcSigDeviceModelDescTag, "icSigDeviceModelDescTag");
   pragma Import (Java, IcSigDeviceSettingsTag, "icSigDeviceSettingsTag");
   pragma Import (Java, IcSigGamutTag, "icSigGamutTag");
   pragma Import (Java, IcSigGrayTRCTag, "icSigGrayTRCTag");
   pragma Import (Java, IcSigGreenColorantTag, "icSigGreenColorantTag");
   pragma Import (Java, IcSigGreenMatrixColumnTag, "icSigGreenMatrixColumnTag");
   pragma Import (Java, IcSigGreenTRCTag, "icSigGreenTRCTag");
   pragma Import (Java, IcSigLuminanceTag, "icSigLuminanceTag");
   pragma Import (Java, IcSigMeasurementTag, "icSigMeasurementTag");
   pragma Import (Java, IcSigMediaBlackPointTag, "icSigMediaBlackPointTag");
   pragma Import (Java, IcSigMediaWhitePointTag, "icSigMediaWhitePointTag");
   pragma Import (Java, IcSigNamedColor2Tag, "icSigNamedColor2Tag");
   pragma Import (Java, IcSigOutputResponseTag, "icSigOutputResponseTag");
   pragma Import (Java, IcSigPreview0Tag, "icSigPreview0Tag");
   pragma Import (Java, IcSigPreview1Tag, "icSigPreview1Tag");
   pragma Import (Java, IcSigPreview2Tag, "icSigPreview2Tag");
   pragma Import (Java, IcSigProfileDescriptionTag, "icSigProfileDescriptionTag");
   pragma Import (Java, IcSigProfileSequenceDescTag, "icSigProfileSequenceDescTag");
   pragma Import (Java, IcSigPs2CRD0Tag, "icSigPs2CRD0Tag");
   pragma Import (Java, IcSigPs2CRD1Tag, "icSigPs2CRD1Tag");
   pragma Import (Java, IcSigPs2CRD2Tag, "icSigPs2CRD2Tag");
   pragma Import (Java, IcSigPs2CRD3Tag, "icSigPs2CRD3Tag");
   pragma Import (Java, IcSigPs2CSATag, "icSigPs2CSATag");
   pragma Import (Java, IcSigPs2RenderingIntentTag, "icSigPs2RenderingIntentTag");
   pragma Import (Java, IcSigRedColorantTag, "icSigRedColorantTag");
   pragma Import (Java, IcSigRedMatrixColumnTag, "icSigRedMatrixColumnTag");
   pragma Import (Java, IcSigRedTRCTag, "icSigRedTRCTag");
   pragma Import (Java, IcSigScreeningDescTag, "icSigScreeningDescTag");
   pragma Import (Java, IcSigScreeningTag, "icSigScreeningTag");
   pragma Import (Java, IcSigTechnologyTag, "icSigTechnologyTag");
   pragma Import (Java, IcSigUcrBgTag, "icSigUcrBgTag");
   pragma Import (Java, IcSigViewingCondDescTag, "icSigViewingCondDescTag");
   pragma Import (Java, IcSigViewingConditionsTag, "icSigViewingConditionsTag");
   pragma Import (Java, IcSigChromaticityTag, "icSigChromaticityTag");
   pragma Import (Java, IcSigChromaticAdaptationTag, "icSigChromaticAdaptationTag");
   pragma Import (Java, IcSigColorantOrderTag, "icSigColorantOrderTag");
   pragma Import (Java, IcSigColorantTableTag, "icSigColorantTableTag");
   pragma Import (Java, IcHdrSize, "icHdrSize");
   pragma Import (Java, IcHdrCmmId, "icHdrCmmId");
   pragma Import (Java, IcHdrVersion, "icHdrVersion");
   pragma Import (Java, IcHdrDeviceClass, "icHdrDeviceClass");
   pragma Import (Java, IcHdrColorSpace, "icHdrColorSpace");
   pragma Import (Java, IcHdrPcs, "icHdrPcs");
   pragma Import (Java, IcHdrDate, "icHdrDate");
   pragma Import (Java, IcHdrMagic, "icHdrMagic");
   pragma Import (Java, IcHdrPlatform, "icHdrPlatform");
   pragma Import (Java, IcHdrFlags, "icHdrFlags");
   pragma Import (Java, IcHdrManufacturer, "icHdrManufacturer");
   pragma Import (Java, IcHdrModel, "icHdrModel");
   pragma Import (Java, IcHdrAttributes, "icHdrAttributes");
   pragma Import (Java, IcHdrRenderingIntent, "icHdrRenderingIntent");
   pragma Import (Java, IcHdrIlluminant, "icHdrIlluminant");
   pragma Import (Java, IcHdrCreator, "icHdrCreator");
   pragma Import (Java, IcHdrProfileID, "icHdrProfileID");
   pragma Import (Java, IcTagType, "icTagType");
   pragma Import (Java, IcTagReserved, "icTagReserved");
   pragma Import (Java, IcCurveCount, "icCurveCount");
   pragma Import (Java, IcCurveData, "icCurveData");
   pragma Import (Java, IcXYZNumberX, "icXYZNumberX");

end Java.Awt.Color.ICC_Profile;
pragma Import (Java, Java.Awt.Color.ICC_Profile, "java.awt.color.ICC_Profile");
pragma Extensions_Allowed (Off);
