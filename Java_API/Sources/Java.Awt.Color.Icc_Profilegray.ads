pragma Extensions_Allowed (On);
with Java.Awt.Color.ICC_Profile;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Color.ICC_ProfileGray is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Color.ICC_Profile.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMediaWhitePoint (This : access Typ)
                                return Java.Float_Arr;

   function GetGamma (This : access Typ)
                      return Java.Float;

   function GetTRC (This : access Typ)
                    return Java.Short_Arr;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetMediaWhitePoint, "getMediaWhitePoint");
   pragma Import (Java, GetGamma, "getGamma");
   pragma Import (Java, GetTRC, "getTRC");

end Java.Awt.Color.ICC_ProfileGray;
pragma Import (Java, Java.Awt.Color.ICC_ProfileGray, "java.awt.color.ICC_ProfileGray");
pragma Extensions_Allowed (Off);
