pragma Extensions_Allowed (On);
with Java.Awt.Color.ICC_Profile;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Color.ICC_ProfileRGB is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Color.ICC_Profile.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMediaWhitePoint (This : access Typ)
                                return Java.Float_Arr;

   function GetMatrix (This : access Typ)
                       return Java.Float_Arr_2;

   function GetGamma (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Float;

   function GetTRC (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Short_Arr;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   REDCOMPONENT : constant Java.Int;

   --  final
   GREENCOMPONENT : constant Java.Int;

   --  final
   BLUECOMPONENT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetMediaWhitePoint, "getMediaWhitePoint");
   pragma Import (Java, GetMatrix, "getMatrix");
   pragma Import (Java, GetGamma, "getGamma");
   pragma Import (Java, GetTRC, "getTRC");
   pragma Import (Java, REDCOMPONENT, "REDCOMPONENT");
   pragma Import (Java, GREENCOMPONENT, "GREENCOMPONENT");
   pragma Import (Java, BLUECOMPONENT, "BLUECOMPONENT");

end Java.Awt.Color.ICC_ProfileRGB;
pragma Import (Java, Java.Awt.Color.ICC_ProfileRGB, "java.awt.color.ICC_ProfileRGB");
pragma Extensions_Allowed (Off);
