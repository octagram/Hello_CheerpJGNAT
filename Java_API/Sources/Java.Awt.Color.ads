pragma Extensions_Allowed (On);
limited with Java.Awt.Color.ColorSpace;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.PaintContext;
limited with Java.Awt.Rectangle;
limited with Java.Awt.RenderingHints;
limited with Java.Lang.String;
with Java.Awt.Paint;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Color is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Paint_I : Java.Awt.Paint.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Color (P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   function New_Color (P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   function New_Color (P1_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   function New_Color (P1_Int : Java.Int;
                       P2_Boolean : Java.Boolean; 
                       This : Ref := null)
                       return Ref;

   function New_Color (P1_Float : Java.Float;
                       P2_Float : Java.Float;
                       P3_Float : Java.Float; 
                       This : Ref := null)
                       return Ref;

   function New_Color (P1_Float : Java.Float;
                       P2_Float : Java.Float;
                       P3_Float : Java.Float;
                       P4_Float : Java.Float; 
                       This : Ref := null)
                       return Ref;

   function New_Color (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                       P2_Float_Arr : Java.Float_Arr;
                       P3_Float : Java.Float; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRed (This : access Typ)
                    return Java.Int;

   function GetGreen (This : access Typ)
                      return Java.Int;

   function GetBlue (This : access Typ)
                     return Java.Int;

   function GetAlpha (This : access Typ)
                      return Java.Int;

   function GetRGB (This : access Typ)
                    return Java.Int;

   function Brighter (This : access Typ)
                      return access Java.Awt.Color.Typ'Class;

   function Darker (This : access Typ)
                    return access Java.Awt.Color.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Decode (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Awt.Color.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function GetColor (P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Awt.Color.Typ'Class;

   function GetColor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Color : access Standard.Java.Awt.Color.Typ'Class)
                      return access Java.Awt.Color.Typ'Class;

   function GetColor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int)
                      return access Java.Awt.Color.Typ'Class;

   function HSBtoRGB (P1_Float : Java.Float;
                      P2_Float : Java.Float;
                      P3_Float : Java.Float)
                      return Java.Int;

   function RGBtoHSB (P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Float_Arr : Java.Float_Arr)
                      return Java.Float_Arr;

   function GetHSBColor (P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Float : Java.Float)
                         return access Java.Awt.Color.Typ'Class;

   function GetRGBComponents (This : access Typ;
                              P1_Float_Arr : Java.Float_Arr)
                              return Java.Float_Arr;

   function GetRGBColorComponents (This : access Typ;
                                   P1_Float_Arr : Java.Float_Arr)
                                   return Java.Float_Arr;

   function GetComponents (This : access Typ;
                           P1_Float_Arr : Java.Float_Arr)
                           return Java.Float_Arr;

   function GetColorComponents (This : access Typ;
                                P1_Float_Arr : Java.Float_Arr)
                                return Java.Float_Arr;

   function GetComponents (This : access Typ;
                           P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                           P2_Float_Arr : Java.Float_Arr)
                           return Java.Float_Arr;

   function GetColorComponents (This : access Typ;
                                P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                                P2_Float_Arr : Java.Float_Arr)
                                return Java.Float_Arr;

   function GetColorSpace (This : access Typ)
                           return access Java.Awt.Color.ColorSpace.Typ'Class;

   --  synchronized
   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                           P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                           P4_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                           P5_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.PaintContext.Typ'Class;

   function GetTransparency (This : access Typ)
                             return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   White : access Java.Awt.Color.Typ'Class;

   --  final
   WHITE_K : access Java.Awt.Color.Typ'Class;

   --  final
   LightGray : access Java.Awt.Color.Typ'Class;

   --  final
   LIGHT_GRAY : access Java.Awt.Color.Typ'Class;

   --  final
   Gray : access Java.Awt.Color.Typ'Class;

   --  final
   GRAY_K : access Java.Awt.Color.Typ'Class;

   --  final
   DarkGray : access Java.Awt.Color.Typ'Class;

   --  final
   DARK_GRAY : access Java.Awt.Color.Typ'Class;

   --  final
   Black : access Java.Awt.Color.Typ'Class;

   --  final
   BLACK_K : access Java.Awt.Color.Typ'Class;

   --  final
   Red : access Java.Awt.Color.Typ'Class;

   --  final
   RED_K : access Java.Awt.Color.Typ'Class;

   --  final
   Pink : access Java.Awt.Color.Typ'Class;

   --  final
   PINK_K : access Java.Awt.Color.Typ'Class;

   --  final
   Orange : access Java.Awt.Color.Typ'Class;

   --  final
   ORANGE_K : access Java.Awt.Color.Typ'Class;

   --  final
   Yellow : access Java.Awt.Color.Typ'Class;

   --  final
   YELLOW_K : access Java.Awt.Color.Typ'Class;

   --  final
   Green : access Java.Awt.Color.Typ'Class;

   --  final
   GREEN_K : access Java.Awt.Color.Typ'Class;

   --  final
   Magenta : access Java.Awt.Color.Typ'Class;

   --  final
   MAGENTA_K : access Java.Awt.Color.Typ'Class;

   --  final
   Cyan : access Java.Awt.Color.Typ'Class;

   --  final
   CYAN_K : access Java.Awt.Color.Typ'Class;

   --  final
   Blue : access Java.Awt.Color.Typ'Class;

   --  final
   BLUE_K : access Java.Awt.Color.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Color);
   pragma Import (Java, GetRed, "getRed");
   pragma Import (Java, GetGreen, "getGreen");
   pragma Import (Java, GetBlue, "getBlue");
   pragma Import (Java, GetAlpha, "getAlpha");
   pragma Import (Java, GetRGB, "getRGB");
   pragma Import (Java, Brighter, "brighter");
   pragma Import (Java, Darker, "darker");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Decode, "decode");
   pragma Import (Java, GetColor, "getColor");
   pragma Import (Java, HSBtoRGB, "HSBtoRGB");
   pragma Import (Java, RGBtoHSB, "RGBtoHSB");
   pragma Import (Java, GetHSBColor, "getHSBColor");
   pragma Import (Java, GetRGBComponents, "getRGBComponents");
   pragma Import (Java, GetRGBColorComponents, "getRGBColorComponents");
   pragma Import (Java, GetComponents, "getComponents");
   pragma Import (Java, GetColorComponents, "getColorComponents");
   pragma Import (Java, GetColorSpace, "getColorSpace");
   pragma Import (Java, CreateContext, "createContext");
   pragma Import (Java, GetTransparency, "getTransparency");
   pragma Import (Java, White, "white");
   pragma Import (Java, WHITE_K, "WHITE");
   pragma Import (Java, LightGray, "lightGray");
   pragma Import (Java, LIGHT_GRAY, "LIGHT_GRAY");
   pragma Import (Java, Gray, "gray");
   pragma Import (Java, GRAY_K, "GRAY");
   pragma Import (Java, DarkGray, "darkGray");
   pragma Import (Java, DARK_GRAY, "DARK_GRAY");
   pragma Import (Java, Black, "black");
   pragma Import (Java, BLACK_K, "BLACK");
   pragma Import (Java, Red, "red");
   pragma Import (Java, RED_K, "RED");
   pragma Import (Java, Pink, "pink");
   pragma Import (Java, PINK_K, "PINK");
   pragma Import (Java, Orange, "orange");
   pragma Import (Java, ORANGE_K, "ORANGE");
   pragma Import (Java, Yellow, "yellow");
   pragma Import (Java, YELLOW_K, "YELLOW");
   pragma Import (Java, Green, "green");
   pragma Import (Java, GREEN_K, "GREEN");
   pragma Import (Java, Magenta, "magenta");
   pragma Import (Java, MAGENTA_K, "MAGENTA");
   pragma Import (Java, Cyan, "cyan");
   pragma Import (Java, CYAN_K, "CYAN");
   pragma Import (Java, Blue, "blue");
   pragma Import (Java, BLUE_K, "BLUE");

end Java.Awt.Color;
pragma Import (Java, Java.Awt.Color, "java.awt.Color");
pragma Extensions_Allowed (Off);
