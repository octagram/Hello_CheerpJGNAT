pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Java.Awt.Component.BaselineResizeBehavior is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CONSTANT_ASCENT : access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  final
   CONSTANT_DESCENT : access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  final
   CENTER_OFFSET : access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  final
   OTHER : access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, CONSTANT_ASCENT, "CONSTANT_ASCENT");
   pragma Import (Java, CONSTANT_DESCENT, "CONSTANT_DESCENT");
   pragma Import (Java, CENTER_OFFSET, "CENTER_OFFSET");
   pragma Import (Java, OTHER, "OTHER");

end Java.Awt.Component.BaselineResizeBehavior;
pragma Import (Java, Java.Awt.Component.BaselineResizeBehavior, "java.awt.Component$BaselineResizeBehavior");
pragma Extensions_Allowed (Off);
