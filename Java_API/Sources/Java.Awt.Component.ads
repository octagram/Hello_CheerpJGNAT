pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Color;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.ComponentOrientation;
limited with Java.Awt.Container;
limited with Java.Awt.Cursor;
limited with Java.Awt.Dimension;
limited with Java.Awt.Dnd.DropTarget;
limited with Java.Awt.Event.ComponentEvent;
limited with Java.Awt.Event.ComponentListener;
limited with Java.Awt.Event.FocusEvent;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Event.HierarchyBoundsListener;
limited with Java.Awt.Event.HierarchyEvent;
limited with Java.Awt.Event.HierarchyListener;
limited with Java.Awt.Event.InputMethodEvent;
limited with Java.Awt.Event.InputMethodListener;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.KeyListener;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Event.MouseListener;
limited with Java.Awt.Event.MouseMotionListener;
limited with Java.Awt.Event.MouseWheelEvent;
limited with Java.Awt.Event.MouseWheelListener;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Im.InputContext;
limited with Java.Awt.Im.InputMethodRequests;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Awt.Image.VolatileImage;
limited with Java.Awt.ImageCapabilities;
limited with Java.Awt.MenuComponent;
limited with Java.Awt.Point;
limited with Java.Awt.PopupMenu;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Toolkit;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Io.PrintStream;
limited with Java.Io.PrintWriter;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Java.Util.Locale;
limited with Java.Util.Set;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Awt.Image;

package Java.Awt.Component is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Component (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetParent (This : access Typ)
                       return access Java.Awt.Container.Typ'Class;

   --  synchronized
   procedure SetDropTarget (This : access Typ;
                            P1_DropTarget : access Standard.Java.Awt.Dnd.DropTarget.Typ'Class);

   --  synchronized
   function GetDropTarget (This : access Typ)
                           return access Java.Awt.Dnd.DropTarget.Typ'Class;

   function GetGraphicsConfiguration (This : access Typ)
                                      return access Java.Awt.GraphicsConfiguration.Typ'Class;

   --  final
   function GetTreeLock (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function GetToolkit (This : access Typ)
                        return access Java.Awt.Toolkit.Typ'Class;

   function IsValid (This : access Typ)
                     return Java.Boolean;

   function IsDisplayable (This : access Typ)
                           return Java.Boolean;

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   function GetMousePosition (This : access Typ)
                              return access Java.Awt.Point.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function IsShowing (This : access Typ)
                       return Java.Boolean;

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsDoubleBuffered (This : access Typ)
                              return Java.Boolean;

   procedure EnableInputMethods (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function GetForeground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function IsForegroundSet (This : access Typ)
                             return Java.Boolean;

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function IsBackgroundSet (This : access Typ)
                             return Java.Boolean;

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function IsFontSet (This : access Typ)
                       return Java.Boolean;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class;

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   function GetLocationOnScreen (This : access Typ)
                                 return access Java.Awt.Point.Typ'Class;

   procedure SetLocation (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);

   procedure SetLocation (This : access Typ;
                          P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;

   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int);

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   procedure SetBounds (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure SetBounds (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetX (This : access Typ)
                  return Java.Int;

   function GetY (This : access Typ)
                  return Java.Int;

   function GetWidth (This : access Typ)
                      return Java.Int;

   function GetHeight (This : access Typ)
                       return Java.Int;

   function GetBounds (This : access Typ;
                       P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                       return access Java.Awt.Rectangle.Typ'Class;

   function GetSize (This : access Typ;
                     P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class)
                     return access Java.Awt.Dimension.Typ'Class;

   function GetLocation (This : access Typ;
                         P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                         return access Java.Awt.Point.Typ'Class;

   function IsOpaque (This : access Typ)
                      return Java.Boolean;

   function IsLightweight (This : access Typ)
                           return Java.Boolean;

   procedure SetPreferredSize (This : access Typ;
                               P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function IsPreferredSizeSet (This : access Typ)
                                return Java.Boolean;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   procedure SetMinimumSize (This : access Typ;
                             P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function IsMinimumSizeSet (This : access Typ)
                              return Java.Boolean;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   procedure SetMaximumSize (This : access Typ;
                             P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function IsMaximumSizeSet (This : access Typ)
                              return Java.Boolean;

   function GetMaximumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetAlignmentX (This : access Typ)
                           return Java.Float;

   function GetAlignmentY (This : access Typ)
                           return Java.Float;

   function GetBaseline (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   procedure DoLayout (This : access Typ);

   procedure Validate (This : access Typ);

   procedure Invalidate (This : access Typ);

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetCursor (This : access Typ)
                       return access Java.Awt.Cursor.Typ'Class;

   function IsCursorSet (This : access Typ)
                         return Java.Boolean;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PaintAll (This : access Typ;
                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Repaint (This : access Typ);

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long);

   procedure Repaint (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int);

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int);

   procedure Print (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PrintAll (This : access Typ;
                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function ImageUpdate (This : access Typ;
                         P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int : Java.Int)
                         return Java.Boolean;

   function CreateImage (This : access Typ;
                         P1_ImageProducer : access Standard.Java.Awt.Image.ImageProducer.Typ'Class)
                         return access Java.Awt.Image.Typ'Class;

   function CreateImage (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return access Java.Awt.Image.Typ'Class;

   function CreateVolatileImage (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int)
                                 return access Java.Awt.Image.VolatileImage.Typ'Class;

   function CreateVolatileImage (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_ImageCapabilities : access Standard.Java.Awt.ImageCapabilities.Typ'Class)
                                 return access Java.Awt.Image.VolatileImage.Typ'Class;
   --  can raise Java.Awt.AWTException.Except

   function PrepareImage (This : access Typ;
                          P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                          return Java.Boolean;

   function PrepareImage (This : access Typ;
                          P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                          return Java.Boolean;

   function CheckImage (This : access Typ;
                        P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P2_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                        return Java.Int;

   function CheckImage (This : access Typ;
                        P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                        return Java.Int;

   procedure SetIgnoreRepaint (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function GetIgnoreRepaint (This : access Typ)
                              return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                      return Java.Boolean;

   function GetComponentAt (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Awt.Component.Typ'Class;

   function GetComponentAt (This : access Typ;
                            P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                            return access Java.Awt.Component.Typ'Class;

   --  final
   procedure DispatchEvent (This : access Typ;
                            P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  synchronized
   procedure AddComponentListener (This : access Typ;
                                   P1_ComponentListener : access Standard.Java.Awt.Event.ComponentListener.Typ'Class);

   --  synchronized
   procedure RemoveComponentListener (This : access Typ;
                                      P1_ComponentListener : access Standard.Java.Awt.Event.ComponentListener.Typ'Class);

   --  synchronized
   function GetComponentListeners (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddFocusListener (This : access Typ;
                               P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   --  synchronized
   procedure RemoveFocusListener (This : access Typ;
                                  P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   --  synchronized
   function GetFocusListeners (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   procedure AddHierarchyListener (This : access Typ;
                                   P1_HierarchyListener : access Standard.Java.Awt.Event.HierarchyListener.Typ'Class);

   procedure RemoveHierarchyListener (This : access Typ;
                                      P1_HierarchyListener : access Standard.Java.Awt.Event.HierarchyListener.Typ'Class);

   --  synchronized
   function GetHierarchyListeners (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   procedure AddHierarchyBoundsListener (This : access Typ;
                                         P1_HierarchyBoundsListener : access Standard.Java.Awt.Event.HierarchyBoundsListener.Typ'Class);

   procedure RemoveHierarchyBoundsListener (This : access Typ;
                                            P1_HierarchyBoundsListener : access Standard.Java.Awt.Event.HierarchyBoundsListener.Typ'Class);

   --  synchronized
   function GetHierarchyBoundsListeners (This : access Typ)
                                         return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddKeyListener (This : access Typ;
                             P1_KeyListener : access Standard.Java.Awt.Event.KeyListener.Typ'Class);

   --  synchronized
   procedure RemoveKeyListener (This : access Typ;
                                P1_KeyListener : access Standard.Java.Awt.Event.KeyListener.Typ'Class);

   --  synchronized
   function GetKeyListeners (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddMouseListener (This : access Typ;
                               P1_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class);

   --  synchronized
   procedure RemoveMouseListener (This : access Typ;
                                  P1_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class);

   --  synchronized
   function GetMouseListeners (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddMouseMotionListener (This : access Typ;
                                     P1_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class);

   --  synchronized
   procedure RemoveMouseMotionListener (This : access Typ;
                                        P1_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class);

   --  synchronized
   function GetMouseMotionListeners (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddMouseWheelListener (This : access Typ;
                                    P1_MouseWheelListener : access Standard.Java.Awt.Event.MouseWheelListener.Typ'Class);

   --  synchronized
   procedure RemoveMouseWheelListener (This : access Typ;
                                       P1_MouseWheelListener : access Standard.Java.Awt.Event.MouseWheelListener.Typ'Class);

   --  synchronized
   function GetMouseWheelListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddInputMethodListener (This : access Typ;
                                     P1_InputMethodListener : access Standard.Java.Awt.Event.InputMethodListener.Typ'Class);

   --  synchronized
   procedure RemoveInputMethodListener (This : access Typ;
                                        P1_InputMethodListener : access Standard.Java.Awt.Event.InputMethodListener.Typ'Class);

   --  synchronized
   function GetInputMethodListeners (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetInputMethodRequests (This : access Typ)
                                    return access Java.Awt.Im.InputMethodRequests.Typ'Class;

   function GetInputContext (This : access Typ)
                             return access Java.Awt.Im.InputContext.Typ'Class;

   --  final  protected
   procedure EnableEvents (This : access Typ;
                           P1_Long : Java.Long);

   --  final  protected
   procedure DisableEvents (This : access Typ;
                            P1_Long : Java.Long);

   --  protected
   function CoalesceEvents (This : access Typ;
                            P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class;
                            P2_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class)
                            return access Java.Awt.AWTEvent.Typ'Class;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessComponentEvent (This : access Typ;
                                    P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   --  protected
   procedure ProcessFocusEvent (This : access Typ;
                                P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   --  protected
   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   --  protected
   procedure ProcessMouseEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   --  protected
   procedure ProcessMouseMotionEvent (This : access Typ;
                                      P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   --  protected
   procedure ProcessMouseWheelEvent (This : access Typ;
                                     P1_MouseWheelEvent : access Standard.Java.Awt.Event.MouseWheelEvent.Typ'Class);

   --  protected
   procedure ProcessInputMethodEvent (This : access Typ;
                                      P1_InputMethodEvent : access Standard.Java.Awt.Event.InputMethodEvent.Typ'Class);

   --  protected
   procedure ProcessHierarchyEvent (This : access Typ;
                                    P1_HierarchyEvent : access Standard.Java.Awt.Event.HierarchyEvent.Typ'Class);

   --  protected
   procedure ProcessHierarchyBoundsEvent (This : access Typ;
                                          P1_HierarchyEvent : access Standard.Java.Awt.Event.HierarchyEvent.Typ'Class);

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   function IsFocusable (This : access Typ)
                         return Java.Boolean;

   procedure SetFocusable (This : access Typ;
                           P1_Boolean : Java.Boolean);

   procedure SetFocusTraversalKeys (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Set : access Standard.Java.Util.Set.Typ'Class);

   function GetFocusTraversalKeys (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Util.Set.Typ'Class;

   function AreFocusTraversalKeysSet (This : access Typ;
                                      P1_Int : Java.Int)
                                      return Java.Boolean;

   procedure SetFocusTraversalKeysEnabled (This : access Typ;
                                           P1_Boolean : Java.Boolean);

   function GetFocusTraversalKeysEnabled (This : access Typ)
                                          return Java.Boolean;

   procedure RequestFocus (This : access Typ);

   --  protected
   function RequestFocus (This : access Typ;
                          P1_Boolean : Java.Boolean)
                          return Java.Boolean;

   function RequestFocusInWindow (This : access Typ)
                                  return Java.Boolean;

   --  protected
   function RequestFocusInWindow (This : access Typ;
                                  P1_Boolean : Java.Boolean)
                                  return Java.Boolean;

   procedure TransferFocus (This : access Typ);

   function GetFocusCycleRootAncestor (This : access Typ)
                                       return access Java.Awt.Container.Typ'Class;

   function IsFocusCycleRoot (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                              return Java.Boolean;

   procedure TransferFocusBackward (This : access Typ);

   procedure TransferFocusUpCycle (This : access Typ);

   function HasFocus (This : access Typ)
                      return Java.Boolean;

   function IsFocusOwner (This : access Typ)
                          return Java.Boolean;

   --  synchronized
   procedure Add (This : access Typ;
                  P1_PopupMenu : access Standard.Java.Awt.PopupMenu.Typ'Class);

   --  synchronized
   procedure Remove (This : access Typ;
                     P1_MenuComponent : access Standard.Java.Awt.MenuComponent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure List (This : access Typ);

   procedure List (This : access Typ;
                   P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   procedure List (This : access Typ;
                   P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class;
                   P2_Int : Java.Int);

   procedure List (This : access Typ;
                   P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class);

   procedure List (This : access Typ;
                   P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class;
                   P2_Int : Java.Int);

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function GetPropertyChangeListeners (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Boolean : Java.Boolean;
                                 P3_Boolean : Java.Boolean);

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Byte : Java.Byte;
                                 P3_Byte : Java.Byte);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Char : Java.Char;
                                 P3_Char : Java.Char);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Short : Java.Short;
                                 P3_Short : Java.Short);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Long : Java.Long;
                                 P3_Long : Java.Long);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Float : Java.Float;
                                 P3_Float : Java.Float);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Double : Java.Double;
                                 P3_Double : Java.Double);

   procedure SetComponentOrientation (This : access Typ;
                                      P1_ComponentOrientation : access Standard.Java.Awt.ComponentOrientation.Typ'Class);

   function GetComponentOrientation (This : access Typ)
                                     return access Java.Awt.ComponentOrientation.Typ'Class;

   procedure ApplyComponentOrientation (This : access Typ;
                                        P1_ComponentOrientation : access Standard.Java.Awt.ComponentOrientation.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TOP_ALIGNMENT : constant Java.Float;

   --  final
   CENTER_ALIGNMENT : constant Java.Float;

   --  final
   BOTTOM_ALIGNMENT : constant Java.Float;

   --  final
   LEFT_ALIGNMENT : constant Java.Float;

   --  final
   RIGHT_ALIGNMENT : constant Java.Float;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Component);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetName, "setName");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, SetDropTarget, "setDropTarget");
   pragma Import (Java, GetDropTarget, "getDropTarget");
   pragma Import (Java, GetGraphicsConfiguration, "getGraphicsConfiguration");
   pragma Import (Java, GetTreeLock, "getTreeLock");
   pragma Import (Java, GetToolkit, "getToolkit");
   pragma Import (Java, IsValid, "isValid");
   pragma Import (Java, IsDisplayable, "isDisplayable");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, GetMousePosition, "getMousePosition");
   pragma Import (Java, IsShowing, "isShowing");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, IsDoubleBuffered, "isDoubleBuffered");
   pragma Import (Java, EnableInputMethods, "enableInputMethods");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, SetForeground, "setForeground");
   pragma Import (Java, IsForegroundSet, "isForegroundSet");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, IsBackgroundSet, "isBackgroundSet");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, IsFontSet, "isFontSet");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, SetLocale, "setLocale");
   pragma Import (Java, GetColorModel, "getColorModel");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetLocationOnScreen, "getLocationOnScreen");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, SetBounds, "setBounds");
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, IsOpaque, "isOpaque");
   pragma Import (Java, IsLightweight, "isLightweight");
   pragma Import (Java, SetPreferredSize, "setPreferredSize");
   pragma Import (Java, IsPreferredSizeSet, "isPreferredSizeSet");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, SetMinimumSize, "setMinimumSize");
   pragma Import (Java, IsMinimumSizeSet, "isMinimumSizeSet");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, SetMaximumSize, "setMaximumSize");
   pragma Import (Java, IsMaximumSizeSet, "isMaximumSizeSet");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetAlignmentX, "getAlignmentX");
   pragma Import (Java, GetAlignmentY, "getAlignmentY");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, DoLayout, "doLayout");
   pragma Import (Java, Validate, "validate");
   pragma Import (Java, Invalidate, "invalidate");
   pragma Import (Java, GetGraphics, "getGraphics");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, SetCursor, "setCursor");
   pragma Import (Java, GetCursor, "getCursor");
   pragma Import (Java, IsCursorSet, "isCursorSet");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Update, "update");
   pragma Import (Java, PaintAll, "paintAll");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, Print, "print");
   pragma Import (Java, PrintAll, "printAll");
   pragma Import (Java, ImageUpdate, "imageUpdate");
   pragma Import (Java, CreateImage, "createImage");
   pragma Import (Java, CreateVolatileImage, "createVolatileImage");
   pragma Import (Java, PrepareImage, "prepareImage");
   pragma Import (Java, CheckImage, "checkImage");
   pragma Import (Java, SetIgnoreRepaint, "setIgnoreRepaint");
   pragma Import (Java, GetIgnoreRepaint, "getIgnoreRepaint");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, GetComponentAt, "getComponentAt");
   pragma Import (Java, DispatchEvent, "dispatchEvent");
   pragma Import (Java, AddComponentListener, "addComponentListener");
   pragma Import (Java, RemoveComponentListener, "removeComponentListener");
   pragma Import (Java, GetComponentListeners, "getComponentListeners");
   pragma Import (Java, AddFocusListener, "addFocusListener");
   pragma Import (Java, RemoveFocusListener, "removeFocusListener");
   pragma Import (Java, GetFocusListeners, "getFocusListeners");
   pragma Import (Java, AddHierarchyListener, "addHierarchyListener");
   pragma Import (Java, RemoveHierarchyListener, "removeHierarchyListener");
   pragma Import (Java, GetHierarchyListeners, "getHierarchyListeners");
   pragma Import (Java, AddHierarchyBoundsListener, "addHierarchyBoundsListener");
   pragma Import (Java, RemoveHierarchyBoundsListener, "removeHierarchyBoundsListener");
   pragma Import (Java, GetHierarchyBoundsListeners, "getHierarchyBoundsListeners");
   pragma Import (Java, AddKeyListener, "addKeyListener");
   pragma Import (Java, RemoveKeyListener, "removeKeyListener");
   pragma Import (Java, GetKeyListeners, "getKeyListeners");
   pragma Import (Java, AddMouseListener, "addMouseListener");
   pragma Import (Java, RemoveMouseListener, "removeMouseListener");
   pragma Import (Java, GetMouseListeners, "getMouseListeners");
   pragma Import (Java, AddMouseMotionListener, "addMouseMotionListener");
   pragma Import (Java, RemoveMouseMotionListener, "removeMouseMotionListener");
   pragma Import (Java, GetMouseMotionListeners, "getMouseMotionListeners");
   pragma Import (Java, AddMouseWheelListener, "addMouseWheelListener");
   pragma Import (Java, RemoveMouseWheelListener, "removeMouseWheelListener");
   pragma Import (Java, GetMouseWheelListeners, "getMouseWheelListeners");
   pragma Import (Java, AddInputMethodListener, "addInputMethodListener");
   pragma Import (Java, RemoveInputMethodListener, "removeInputMethodListener");
   pragma Import (Java, GetInputMethodListeners, "getInputMethodListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, GetInputMethodRequests, "getInputMethodRequests");
   pragma Import (Java, GetInputContext, "getInputContext");
   pragma Import (Java, EnableEvents, "enableEvents");
   pragma Import (Java, DisableEvents, "disableEvents");
   pragma Import (Java, CoalesceEvents, "coalesceEvents");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessComponentEvent, "processComponentEvent");
   pragma Import (Java, ProcessFocusEvent, "processFocusEvent");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, ProcessMouseEvent, "processMouseEvent");
   pragma Import (Java, ProcessMouseMotionEvent, "processMouseMotionEvent");
   pragma Import (Java, ProcessMouseWheelEvent, "processMouseWheelEvent");
   pragma Import (Java, ProcessInputMethodEvent, "processInputMethodEvent");
   pragma Import (Java, ProcessHierarchyEvent, "processHierarchyEvent");
   pragma Import (Java, ProcessHierarchyBoundsEvent, "processHierarchyBoundsEvent");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, IsFocusable, "isFocusable");
   pragma Import (Java, SetFocusable, "setFocusable");
   pragma Import (Java, SetFocusTraversalKeys, "setFocusTraversalKeys");
   pragma Import (Java, GetFocusTraversalKeys, "getFocusTraversalKeys");
   pragma Import (Java, AreFocusTraversalKeysSet, "areFocusTraversalKeysSet");
   pragma Import (Java, SetFocusTraversalKeysEnabled, "setFocusTraversalKeysEnabled");
   pragma Import (Java, GetFocusTraversalKeysEnabled, "getFocusTraversalKeysEnabled");
   pragma Import (Java, RequestFocus, "requestFocus");
   pragma Import (Java, RequestFocusInWindow, "requestFocusInWindow");
   pragma Import (Java, TransferFocus, "transferFocus");
   pragma Import (Java, GetFocusCycleRootAncestor, "getFocusCycleRootAncestor");
   pragma Import (Java, IsFocusCycleRoot, "isFocusCycleRoot");
   pragma Import (Java, TransferFocusBackward, "transferFocusBackward");
   pragma Import (Java, TransferFocusUpCycle, "transferFocusUpCycle");
   pragma Import (Java, HasFocus, "hasFocus");
   pragma Import (Java, IsFocusOwner, "isFocusOwner");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, List, "list");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, SetComponentOrientation, "setComponentOrientation");
   pragma Import (Java, GetComponentOrientation, "getComponentOrientation");
   pragma Import (Java, ApplyComponentOrientation, "applyComponentOrientation");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, TOP_ALIGNMENT, "TOP_ALIGNMENT");
   pragma Import (Java, CENTER_ALIGNMENT, "CENTER_ALIGNMENT");
   pragma Import (Java, BOTTOM_ALIGNMENT, "BOTTOM_ALIGNMENT");
   pragma Import (Java, LEFT_ALIGNMENT, "LEFT_ALIGNMENT");
   pragma Import (Java, RIGHT_ALIGNMENT, "RIGHT_ALIGNMENT");

end Java.Awt.Component;
pragma Import (Java, Java.Awt.Component, "java.awt.Component");
pragma Extensions_Allowed (Off);
