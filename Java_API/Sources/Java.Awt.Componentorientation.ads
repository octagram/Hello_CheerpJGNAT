pragma Extensions_Allowed (On);
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.ComponentOrientation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsHorizontal (This : access Typ)
                          return Java.Boolean;

   function IsLeftToRight (This : access Typ)
                           return Java.Boolean;

   function GetOrientation (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                            return access Java.Awt.ComponentOrientation.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LEFT_TO_RIGHT : access Java.Awt.ComponentOrientation.Typ'Class;

   --  final
   RIGHT_TO_LEFT : access Java.Awt.ComponentOrientation.Typ'Class;

   --  final
   UNKNOWN : access Java.Awt.ComponentOrientation.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, IsHorizontal, "isHorizontal");
   pragma Import (Java, IsLeftToRight, "isLeftToRight");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, LEFT_TO_RIGHT, "LEFT_TO_RIGHT");
   pragma Import (Java, RIGHT_TO_LEFT, "RIGHT_TO_LEFT");
   pragma Import (Java, UNKNOWN, "UNKNOWN");

end Java.Awt.ComponentOrientation;
pragma Import (Java, Java.Awt.ComponentOrientation, "java.awt.ComponentOrientation");
pragma Extensions_Allowed (Off);
