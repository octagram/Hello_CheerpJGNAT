pragma Extensions_Allowed (On);
limited with Java.Awt.CompositeContext;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.RenderingHints;
with Java.Lang.Object;

package Java.Awt.Composite is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P3_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.CompositeContext.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateContext, "createContext");

end Java.Awt.Composite;
pragma Import (Java, Java.Awt.Composite, "java.awt.Composite");
pragma Extensions_Allowed (Off);
