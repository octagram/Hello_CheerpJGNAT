pragma Extensions_Allowed (On);
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.WritableRaster;
with Java.Lang.Object;

package Java.Awt.CompositeContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Dispose (This : access Typ) is abstract;

   procedure Compose (This : access Typ;
                      P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                      P2_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                      P3_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Dispose, "dispose");
   pragma Export (Java, Compose, "compose");

end Java.Awt.CompositeContext;
pragma Import (Java, Java.Awt.CompositeContext, "java.awt.CompositeContext");
pragma Extensions_Allowed (Off);
