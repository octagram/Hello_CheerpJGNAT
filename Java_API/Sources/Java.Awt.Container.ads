pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.ComponentOrientation;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ContainerEvent;
limited with Java.Awt.Event.ContainerListener;
limited with Java.Awt.FocusTraversalPolicy;
limited with Java.Awt.Font;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Point;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Io.PrintStream;
limited with Java.Io.PrintWriter;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Java.Util.Set;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Container is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Container (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponentCount (This : access Typ)
                               return Java.Int;

   function GetComponent (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Awt.Component.Typ'Class;

   function GetComponents (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function GetInsets (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   function Add (This : access Typ;
                 P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                 return access Java.Awt.Component.Typ'Class;

   function Add (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                 return access Java.Awt.Component.Typ'Class;

   function Add (This : access Typ;
                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                 P2_Int : Java.Int)
                 return access Java.Awt.Component.Typ'Class;

   procedure SetComponentZOrder (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Int : Java.Int);

   function GetComponentZOrder (This : access Typ;
                                P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return Java.Int;

   procedure Add (This : access Typ;
                  P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Add (This : access Typ;
                  P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P3_Int : Java.Int);

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveAll (This : access Typ);

   function GetLayout (This : access Typ)
                       return access Java.Awt.LayoutManager.Typ'Class;

   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   procedure DoLayout (This : access Typ);

   procedure Invalidate (This : access Typ);

   procedure Validate (This : access Typ);

   --  protected
   procedure ValidateTree (This : access Typ);

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetAlignmentX (This : access Typ)
                           return Java.Float;

   function GetAlignmentY (This : access Typ)
                           return Java.Float;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Print (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PaintComponents (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PrintComponents (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  synchronized
   procedure AddContainerListener (This : access Typ;
                                   P1_ContainerListener : access Standard.Java.Awt.Event.ContainerListener.Typ'Class);

   --  synchronized
   procedure RemoveContainerListener (This : access Typ;
                                      P1_ContainerListener : access Standard.Java.Awt.Event.ContainerListener.Typ'Class);

   --  synchronized
   function GetContainerListeners (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessContainerEvent (This : access Typ;
                                    P1_ContainerEvent : access Standard.Java.Awt.Event.ContainerEvent.Typ'Class);

   function GetComponentAt (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Awt.Component.Typ'Class;

   function GetComponentAt (This : access Typ;
                            P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                            return access Java.Awt.Component.Typ'Class;

   function GetMousePosition (This : access Typ;
                              P1_Boolean : Java.Boolean)
                              return access Java.Awt.Point.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function FindComponentAt (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Java.Awt.Component.Typ'Class;

   function FindComponentAt (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return access Java.Awt.Component.Typ'Class;

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   function IsAncestorOf (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                          return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure List (This : access Typ;
                   P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class;
                   P2_Int : Java.Int);

   procedure List (This : access Typ;
                   P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class;
                   P2_Int : Java.Int);

   procedure SetFocusTraversalKeys (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Set : access Standard.Java.Util.Set.Typ'Class);

   function GetFocusTraversalKeys (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Util.Set.Typ'Class;

   function AreFocusTraversalKeysSet (This : access Typ;
                                      P1_Int : Java.Int)
                                      return Java.Boolean;

   function IsFocusCycleRoot (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                              return Java.Boolean;

   procedure TransferFocusBackward (This : access Typ);

   procedure SetFocusTraversalPolicy (This : access Typ;
                                      P1_FocusTraversalPolicy : access Standard.Java.Awt.FocusTraversalPolicy.Typ'Class);

   function GetFocusTraversalPolicy (This : access Typ)
                                     return access Java.Awt.FocusTraversalPolicy.Typ'Class;

   function IsFocusTraversalPolicySet (This : access Typ)
                                       return Java.Boolean;

   procedure SetFocusCycleRoot (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function IsFocusCycleRoot (This : access Typ)
                              return Java.Boolean;

   --  final
   procedure SetFocusTraversalPolicyProvider (This : access Typ;
                                              P1_Boolean : Java.Boolean);

   --  final
   function IsFocusTraversalPolicyProvider (This : access Typ)
                                            return Java.Boolean;

   procedure TransferFocusDownCycle (This : access Typ);

   procedure ApplyComponentOrientation (This : access Typ;
                                        P1_ComponentOrientation : access Standard.Java.Awt.ComponentOrientation.Typ'Class);

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Container);
   pragma Import (Java, GetComponentCount, "getComponentCount");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, GetComponents, "getComponents");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, Add, "add");
   pragma Import (Java, SetComponentZOrder, "setComponentZOrder");
   pragma Import (Java, GetComponentZOrder, "getComponentZOrder");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, GetLayout, "getLayout");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, DoLayout, "doLayout");
   pragma Import (Java, Invalidate, "invalidate");
   pragma Import (Java, Validate, "validate");
   pragma Import (Java, ValidateTree, "validateTree");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetAlignmentX, "getAlignmentX");
   pragma Import (Java, GetAlignmentY, "getAlignmentY");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Update, "update");
   pragma Import (Java, Print, "print");
   pragma Import (Java, PaintComponents, "paintComponents");
   pragma Import (Java, PrintComponents, "printComponents");
   pragma Import (Java, AddContainerListener, "addContainerListener");
   pragma Import (Java, RemoveContainerListener, "removeContainerListener");
   pragma Import (Java, GetContainerListeners, "getContainerListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessContainerEvent, "processContainerEvent");
   pragma Import (Java, GetComponentAt, "getComponentAt");
   pragma Import (Java, GetMousePosition, "getMousePosition");
   pragma Import (Java, FindComponentAt, "findComponentAt");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, IsAncestorOf, "isAncestorOf");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, List, "list");
   pragma Import (Java, SetFocusTraversalKeys, "setFocusTraversalKeys");
   pragma Import (Java, GetFocusTraversalKeys, "getFocusTraversalKeys");
   pragma Import (Java, AreFocusTraversalKeysSet, "areFocusTraversalKeysSet");
   pragma Import (Java, IsFocusCycleRoot, "isFocusCycleRoot");
   pragma Import (Java, TransferFocusBackward, "transferFocusBackward");
   pragma Import (Java, SetFocusTraversalPolicy, "setFocusTraversalPolicy");
   pragma Import (Java, GetFocusTraversalPolicy, "getFocusTraversalPolicy");
   pragma Import (Java, IsFocusTraversalPolicySet, "isFocusTraversalPolicySet");
   pragma Import (Java, SetFocusCycleRoot, "setFocusCycleRoot");
   pragma Import (Java, SetFocusTraversalPolicyProvider, "setFocusTraversalPolicyProvider");
   pragma Import (Java, IsFocusTraversalPolicyProvider, "isFocusTraversalPolicyProvider");
   pragma Import (Java, TransferFocusDownCycle, "transferFocusDownCycle");
   pragma Import (Java, ApplyComponentOrientation, "applyComponentOrientation");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");

end Java.Awt.Container;
pragma Import (Java, Java.Awt.Container, "java.awt.Container");
pragma Extensions_Allowed (Off);
