pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
with Java.Awt.FocusTraversalPolicy;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.ContainerOrderFocusTraversalPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.FocusTraversalPolicy.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ContainerOrderFocusTraversalPolicy (This : Ref := null)
                                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponentAfter (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                               P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return access Java.Awt.Component.Typ'Class;

   function GetComponentBefore (This : access Typ;
                                P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                                P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return access Java.Awt.Component.Typ'Class;

   function GetFirstComponent (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Component.Typ'Class;

   function GetLastComponent (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                              return access Java.Awt.Component.Typ'Class;

   function GetDefaultComponent (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Component.Typ'Class;

   procedure SetImplicitDownCycleTraversal (This : access Typ;
                                            P1_Boolean : Java.Boolean);

   function GetImplicitDownCycleTraversal (This : access Typ)
                                           return Java.Boolean;

   --  protected
   function accept_K (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                      return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ContainerOrderFocusTraversalPolicy);
   pragma Import (Java, GetComponentAfter, "getComponentAfter");
   pragma Import (Java, GetComponentBefore, "getComponentBefore");
   pragma Import (Java, GetFirstComponent, "getFirstComponent");
   pragma Import (Java, GetLastComponent, "getLastComponent");
   pragma Import (Java, GetDefaultComponent, "getDefaultComponent");
   pragma Import (Java, SetImplicitDownCycleTraversal, "setImplicitDownCycleTraversal");
   pragma Import (Java, GetImplicitDownCycleTraversal, "getImplicitDownCycleTraversal");
   pragma Import (Java, accept_K, "accept");

end Java.Awt.ContainerOrderFocusTraversalPolicy;
pragma Import (Java, Java.Awt.ContainerOrderFocusTraversalPolicy, "java.awt.ContainerOrderFocusTraversalPolicy");
pragma Extensions_Allowed (Off);
