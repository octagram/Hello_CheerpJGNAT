pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Cursor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPredefinedCursor (P1_Int : Java.Int)
                                 return access Java.Awt.Cursor.Typ'Class;

   function GetSystemCustomCursor (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Awt.Cursor.Typ'Class;
   --  can raise Java.Awt.AWTException.Except and
   --  Java.Awt.HeadlessException.Except

   function GetDefaultCursor return access Java.Awt.Cursor.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Cursor (P1_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   --  protected
   function New_Cursor (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function GetType (This : access Typ)
                     return Java.Int;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_CURSOR : constant Java.Int;

   --  final
   CROSSHAIR_CURSOR : constant Java.Int;

   --  final
   TEXT_CURSOR : constant Java.Int;

   --  final
   WAIT_CURSOR : constant Java.Int;

   --  final
   SW_RESIZE_CURSOR : constant Java.Int;

   --  final
   SE_RESIZE_CURSOR : constant Java.Int;

   --  final
   NW_RESIZE_CURSOR : constant Java.Int;

   --  final
   NE_RESIZE_CURSOR : constant Java.Int;

   --  final
   N_RESIZE_CURSOR : constant Java.Int;

   --  final
   S_RESIZE_CURSOR : constant Java.Int;

   --  final
   W_RESIZE_CURSOR : constant Java.Int;

   --  final
   E_RESIZE_CURSOR : constant Java.Int;

   --  final
   HAND_CURSOR : constant Java.Int;

   --  final
   MOVE_CURSOR : constant Java.Int;

   --  protected
   Predefined : Standard.Java.Lang.Object.Ref;

   --  final
   CUSTOM_CURSOR : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetPredefinedCursor, "getPredefinedCursor");
   pragma Import (Java, GetSystemCustomCursor, "getSystemCustomCursor");
   pragma Import (Java, GetDefaultCursor, "getDefaultCursor");
   pragma Java_Constructor (New_Cursor);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, DEFAULT_CURSOR, "DEFAULT_CURSOR");
   pragma Import (Java, CROSSHAIR_CURSOR, "CROSSHAIR_CURSOR");
   pragma Import (Java, TEXT_CURSOR, "TEXT_CURSOR");
   pragma Import (Java, WAIT_CURSOR, "WAIT_CURSOR");
   pragma Import (Java, SW_RESIZE_CURSOR, "SW_RESIZE_CURSOR");
   pragma Import (Java, SE_RESIZE_CURSOR, "SE_RESIZE_CURSOR");
   pragma Import (Java, NW_RESIZE_CURSOR, "NW_RESIZE_CURSOR");
   pragma Import (Java, NE_RESIZE_CURSOR, "NE_RESIZE_CURSOR");
   pragma Import (Java, N_RESIZE_CURSOR, "N_RESIZE_CURSOR");
   pragma Import (Java, S_RESIZE_CURSOR, "S_RESIZE_CURSOR");
   pragma Import (Java, W_RESIZE_CURSOR, "W_RESIZE_CURSOR");
   pragma Import (Java, E_RESIZE_CURSOR, "E_RESIZE_CURSOR");
   pragma Import (Java, HAND_CURSOR, "HAND_CURSOR");
   pragma Import (Java, MOVE_CURSOR, "MOVE_CURSOR");
   pragma Import (Java, Predefined, "predefined");
   pragma Import (Java, CUSTOM_CURSOR, "CUSTOM_CURSOR");

end Java.Awt.Cursor;
pragma Import (Java, Java.Awt.Cursor, "java.awt.Cursor");
pragma Extensions_Allowed (Off);
