pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.ClipboardOwner;
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Awt.Datatransfer.FlavorListener;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.Datatransfer.Clipboard is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Owner : access Java.Awt.Datatransfer.ClipboardOwner.Typ'Class;
      pragma Import (Java, Owner, "owner");

      --  protected
      Contents : access Java.Awt.Datatransfer.Transferable.Typ'Class;
      pragma Import (Java, Contents, "contents");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Clipboard (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure SetContents (This : access Typ;
                          P1_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                          P2_ClipboardOwner : access Standard.Java.Awt.Datatransfer.ClipboardOwner.Typ'Class);

   --  synchronized
   function GetContents (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Awt.Datatransfer.Transferable.Typ'Class;

   function GetAvailableDataFlavors (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   function IsDataFlavorAvailable (This : access Typ;
                                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                   return Java.Boolean;

   function GetData (This : access Typ;
                     P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Awt.Datatransfer.UnsupportedFlavorException.Except and
   --  Java.Io.IOException.Except

   --  synchronized
   procedure AddFlavorListener (This : access Typ;
                                P1_FlavorListener : access Standard.Java.Awt.Datatransfer.FlavorListener.Typ'Class);

   --  synchronized
   procedure RemoveFlavorListener (This : access Typ;
                                   P1_FlavorListener : access Standard.Java.Awt.Datatransfer.FlavorListener.Typ'Class);

   --  synchronized
   function GetFlavorListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Clipboard);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetContents, "setContents");
   pragma Import (Java, GetContents, "getContents");
   pragma Import (Java, GetAvailableDataFlavors, "getAvailableDataFlavors");
   pragma Import (Java, IsDataFlavorAvailable, "isDataFlavorAvailable");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, AddFlavorListener, "addFlavorListener");
   pragma Import (Java, RemoveFlavorListener, "removeFlavorListener");
   pragma Import (Java, GetFlavorListeners, "getFlavorListeners");

end Java.Awt.Datatransfer.Clipboard;
pragma Import (Java, Java.Awt.Datatransfer.Clipboard, "java.awt.datatransfer.Clipboard");
pragma Extensions_Allowed (Off);
