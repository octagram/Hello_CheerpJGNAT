pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.Clipboard;
limited with Java.Awt.Datatransfer.Transferable;
with Java.Lang.Object;

package Java.Awt.Datatransfer.ClipboardOwner is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure LostOwnership (This : access Typ;
                            P1_Clipboard : access Standard.Java.Awt.Datatransfer.Clipboard.Typ'Class;
                            P2_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, LostOwnership, "lostOwnership");

end Java.Awt.Datatransfer.ClipboardOwner;
pragma Import (Java, Java.Awt.Datatransfer.ClipboardOwner, "java.awt.datatransfer.ClipboardOwner");
pragma Extensions_Allowed (Off);
