pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Io.ObjectInput;
limited with Java.Io.ObjectOutput;
limited with Java.Io.Reader;
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
with Java.Io.Externalizable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Datatransfer.DataFlavor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Externalizable_I : Java.Io.Externalizable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   function TryToLoadClass (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                            return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DataFlavor (This : Ref := null)
                            return Ref;

   function New_DataFlavor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_DataFlavor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_DataFlavor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function New_DataFlavor (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   function GetTextPlainUnicodeFlavor return access Java.Awt.Datatransfer.DataFlavor.Typ'Class;

   --  final
   function SelectBestTextFlavor (P1_DataFlavor_Arr : access Java.Awt.Datatransfer.DataFlavor.Arr_Obj)
                                  return access Java.Awt.Datatransfer.DataFlavor.Typ'Class;

   function GetReaderForText (This : access Typ;
                              P1_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class)
                              return access Java.Io.Reader.Typ'Class;
   --  can raise Java.Awt.Datatransfer.UnsupportedFlavorException.Except and
   --  Java.Io.IOException.Except

   function GetMimeType (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetRepresentationClass (This : access Typ)
                                    return access Java.Lang.Class.Typ'Class;

   function GetHumanPresentableName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetPrimaryType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetSubType (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   procedure SetHumanPresentableName (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Match (This : access Typ;
                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                   return Java.Boolean;

   function IsMimeTypeEqual (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return Java.Boolean;

   --  final
   function IsMimeTypeEqual (This : access Typ;
                             P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                             return Java.Boolean;

   function IsMimeTypeSerializedObject (This : access Typ)
                                        return Java.Boolean;

   --  final
   function GetDefaultRepresentationClass (This : access Typ)
                                           return access Java.Lang.Class.Typ'Class;

   --  final
   function GetDefaultRepresentationClassAsString (This : access Typ)
                                                   return access Java.Lang.String.Typ'Class;

   function IsRepresentationClassInputStream (This : access Typ)
                                              return Java.Boolean;

   function IsRepresentationClassReader (This : access Typ)
                                         return Java.Boolean;

   function IsRepresentationClassCharBuffer (This : access Typ)
                                             return Java.Boolean;

   function IsRepresentationClassByteBuffer (This : access Typ)
                                             return Java.Boolean;

   function IsRepresentationClassSerializable (This : access Typ)
                                               return Java.Boolean;

   function IsRepresentationClassRemote (This : access Typ)
                                         return Java.Boolean;

   function IsFlavorSerializedObjectType (This : access Typ)
                                          return Java.Boolean;

   function IsFlavorRemoteObjectType (This : access Typ)
                                      return Java.Boolean;

   function IsFlavorJavaFileListType (This : access Typ)
                                      return Java.Boolean;

   function IsFlavorTextType (This : access Typ)
                              return Java.Boolean;

   --  synchronized
   procedure WriteExternal (This : access Typ;
                            P1_ObjectOutput : access Standard.Java.Io.ObjectOutput.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure ReadExternal (This : access Typ;
                           P1_ObjectInput : access Standard.Java.Io.ObjectInput.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   StringFlavor : access Java.Awt.Datatransfer.DataFlavor.Typ'Class;

   --  final
   ImageFlavor : access Java.Awt.Datatransfer.DataFlavor.Typ'Class;

   --  final
   JavaSerializedObjectMimeType : constant access Java.Lang.String.Typ'Class;

   --  final
   JavaFileListFlavor : access Java.Awt.Datatransfer.DataFlavor.Typ'Class;

   --  final
   JavaJVMLocalObjectMimeType : constant access Java.Lang.String.Typ'Class;

   --  final
   JavaRemoteObjectMimeType : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, TryToLoadClass, "tryToLoadClass");
   pragma Java_Constructor (New_DataFlavor);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetTextPlainUnicodeFlavor, "getTextPlainUnicodeFlavor");
   pragma Import (Java, SelectBestTextFlavor, "selectBestTextFlavor");
   pragma Import (Java, GetReaderForText, "getReaderForText");
   pragma Import (Java, GetMimeType, "getMimeType");
   pragma Import (Java, GetRepresentationClass, "getRepresentationClass");
   pragma Import (Java, GetHumanPresentableName, "getHumanPresentableName");
   pragma Import (Java, GetPrimaryType, "getPrimaryType");
   pragma Import (Java, GetSubType, "getSubType");
   pragma Import (Java, GetParameter, "getParameter");
   pragma Import (Java, SetHumanPresentableName, "setHumanPresentableName");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Match, "match");
   pragma Import (Java, IsMimeTypeEqual, "isMimeTypeEqual");
   pragma Import (Java, IsMimeTypeSerializedObject, "isMimeTypeSerializedObject");
   pragma Import (Java, GetDefaultRepresentationClass, "getDefaultRepresentationClass");
   pragma Import (Java, GetDefaultRepresentationClassAsString, "getDefaultRepresentationClassAsString");
   pragma Import (Java, IsRepresentationClassInputStream, "isRepresentationClassInputStream");
   pragma Import (Java, IsRepresentationClassReader, "isRepresentationClassReader");
   pragma Import (Java, IsRepresentationClassCharBuffer, "isRepresentationClassCharBuffer");
   pragma Import (Java, IsRepresentationClassByteBuffer, "isRepresentationClassByteBuffer");
   pragma Import (Java, IsRepresentationClassSerializable, "isRepresentationClassSerializable");
   pragma Import (Java, IsRepresentationClassRemote, "isRepresentationClassRemote");
   pragma Import (Java, IsFlavorSerializedObjectType, "isFlavorSerializedObjectType");
   pragma Import (Java, IsFlavorRemoteObjectType, "isFlavorRemoteObjectType");
   pragma Import (Java, IsFlavorJavaFileListType, "isFlavorJavaFileListType");
   pragma Import (Java, IsFlavorTextType, "isFlavorTextType");
   pragma Import (Java, WriteExternal, "writeExternal");
   pragma Import (Java, ReadExternal, "readExternal");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, StringFlavor, "stringFlavor");
   pragma Import (Java, ImageFlavor, "imageFlavor");
   pragma Import (Java, JavaSerializedObjectMimeType, "javaSerializedObjectMimeType");
   pragma Import (Java, JavaFileListFlavor, "javaFileListFlavor");
   pragma Import (Java, JavaJVMLocalObjectMimeType, "javaJVMLocalObjectMimeType");
   pragma Import (Java, JavaRemoteObjectMimeType, "javaRemoteObjectMimeType");

end Java.Awt.Datatransfer.DataFlavor;
pragma Import (Java, Java.Awt.Datatransfer.DataFlavor, "java.awt.datatransfer.DataFlavor");
pragma Extensions_Allowed (Off);
