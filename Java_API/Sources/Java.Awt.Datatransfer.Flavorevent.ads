pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.Clipboard;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Awt.Datatransfer.FlavorEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FlavorEvent (P1_Clipboard : access Standard.Java.Awt.Datatransfer.Clipboard.Typ'Class; 
                             This : Ref := null)
                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FlavorEvent);

end Java.Awt.Datatransfer.FlavorEvent;
pragma Import (Java, Java.Awt.Datatransfer.FlavorEvent, "java.awt.datatransfer.FlavorEvent");
pragma Extensions_Allowed (Off);
