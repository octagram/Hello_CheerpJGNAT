pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Lang.String;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Awt.Datatransfer.FlavorMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNativesForFlavors (This : access Typ;
                                  P1_DataFlavor_Arr : access Java.Awt.Datatransfer.DataFlavor.Arr_Obj)
                                  return access Java.Util.Map.Typ'Class is abstract;

   function GetFlavorsForNatives (This : access Typ;
                                  P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                  return access Java.Util.Map.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetNativesForFlavors, "getNativesForFlavors");
   pragma Export (Java, GetFlavorsForNatives, "getFlavorsForNatives");

end Java.Awt.Datatransfer.FlavorMap;
pragma Import (Java, Java.Awt.Datatransfer.FlavorMap, "java.awt.datatransfer.FlavorMap");
pragma Extensions_Allowed (Off);
