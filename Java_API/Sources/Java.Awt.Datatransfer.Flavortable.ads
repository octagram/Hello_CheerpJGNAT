pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Awt.Datatransfer.FlavorMap;
with Java.Lang.Object;

package Java.Awt.Datatransfer.FlavorTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            FlavorMap_I : Java.Awt.Datatransfer.FlavorMap.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNativesForFlavor (This : access Typ;
                                 P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                 return access Java.Util.List.Typ'Class is abstract;

   function GetFlavorsForNative (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Util.List.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetNativesForFlavor, "getNativesForFlavor");
   pragma Export (Java, GetFlavorsForNative, "getFlavorsForNative");

end Java.Awt.Datatransfer.FlavorTable;
pragma Import (Java, Java.Awt.Datatransfer.FlavorTable, "java.awt.datatransfer.FlavorTable");
pragma Extensions_Allowed (Off);
