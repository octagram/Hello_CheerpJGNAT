pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.Clipboard;
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Lang.String;
with Java.Awt.Datatransfer.ClipboardOwner;
with Java.Awt.Datatransfer.Transferable;
with Java.Lang.Object;

package Java.Awt.Datatransfer.StringSelection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ClipboardOwner_I : Java.Awt.Datatransfer.ClipboardOwner.Ref;
            Transferable_I : Java.Awt.Datatransfer.Transferable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringSelection (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransferDataFlavors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function IsDataFlavorSupported (This : access Typ;
                                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                   return Java.Boolean;

   function GetTransferData (This : access Typ;
                             P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Awt.Datatransfer.UnsupportedFlavorException.Except and
   --  Java.Io.IOException.Except

   procedure LostOwnership (This : access Typ;
                            P1_Clipboard : access Standard.Java.Awt.Datatransfer.Clipboard.Typ'Class;
                            P2_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringSelection);
   pragma Import (Java, GetTransferDataFlavors, "getTransferDataFlavors");
   pragma Import (Java, IsDataFlavorSupported, "isDataFlavorSupported");
   pragma Import (Java, GetTransferData, "getTransferData");
   pragma Import (Java, LostOwnership, "lostOwnership");

end Java.Awt.Datatransfer.StringSelection;
pragma Import (Java, Java.Awt.Datatransfer.StringSelection, "java.awt.datatransfer.StringSelection");
pragma Extensions_Allowed (Off);
