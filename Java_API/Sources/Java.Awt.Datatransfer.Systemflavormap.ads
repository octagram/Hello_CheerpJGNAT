pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
with Java.Awt.Datatransfer.FlavorMap;
with Java.Awt.Datatransfer.FlavorTable;
with Java.Lang.Object;

package Java.Awt.Datatransfer.SystemFlavorMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(FlavorMap_I : Java.Awt.Datatransfer.FlavorMap.Ref;
            FlavorTable_I : Java.Awt.Datatransfer.FlavorTable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultFlavorMap return access Java.Awt.Datatransfer.FlavorMap.Typ'Class;

   --  synchronized
   function GetNativesForFlavor (This : access Typ;
                                 P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                 return access Java.Util.List.Typ'Class;

   --  synchronized
   function GetFlavorsForNative (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Util.List.Typ'Class;

   --  synchronized
   function GetNativesForFlavors (This : access Typ;
                                  P1_DataFlavor_Arr : access Java.Awt.Datatransfer.DataFlavor.Arr_Obj)
                                  return access Java.Util.Map.Typ'Class;

   --  synchronized
   function GetFlavorsForNatives (This : access Typ;
                                  P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                  return access Java.Util.Map.Typ'Class;

   --  synchronized
   procedure AddUnencodedNativeForFlavor (This : access Typ;
                                          P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class;
                                          P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   procedure SetNativesForFlavor (This : access Typ;
                                  P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class;
                                  P2_String_Arr : access Java.Lang.String.Arr_Obj);

   --  synchronized
   procedure AddFlavorForUnencodedNative (This : access Typ;
                                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                                          P2_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class);

   --  synchronized
   procedure SetFlavorsForNative (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_DataFlavor_Arr : access Java.Awt.Datatransfer.DataFlavor.Arr_Obj);

   function EncodeJavaMIMEType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function EncodeDataFlavor (P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                              return access Java.Lang.String.Typ'Class;

   function IsJavaMIMEType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Boolean;

   function DecodeJavaMIMEType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function DecodeDataFlavor (P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Awt.Datatransfer.DataFlavor.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDefaultFlavorMap, "getDefaultFlavorMap");
   pragma Import (Java, GetNativesForFlavor, "getNativesForFlavor");
   pragma Import (Java, GetFlavorsForNative, "getFlavorsForNative");
   pragma Import (Java, GetNativesForFlavors, "getNativesForFlavors");
   pragma Import (Java, GetFlavorsForNatives, "getFlavorsForNatives");
   pragma Import (Java, AddUnencodedNativeForFlavor, "addUnencodedNativeForFlavor");
   pragma Import (Java, SetNativesForFlavor, "setNativesForFlavor");
   pragma Import (Java, AddFlavorForUnencodedNative, "addFlavorForUnencodedNative");
   pragma Import (Java, SetFlavorsForNative, "setFlavorsForNative");
   pragma Import (Java, EncodeJavaMIMEType, "encodeJavaMIMEType");
   pragma Import (Java, EncodeDataFlavor, "encodeDataFlavor");
   pragma Import (Java, IsJavaMIMEType, "isJavaMIMEType");
   pragma Import (Java, DecodeJavaMIMEType, "decodeJavaMIMEType");
   pragma Import (Java, DecodeDataFlavor, "decodeDataFlavor");

end Java.Awt.Datatransfer.SystemFlavorMap;
pragma Import (Java, Java.Awt.Datatransfer.SystemFlavorMap, "java.awt.datatransfer.SystemFlavorMap");
pragma Extensions_Allowed (Off);
