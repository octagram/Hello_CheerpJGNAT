pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
with Java.Lang.Object;

package Java.Awt.Datatransfer.Transferable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransferDataFlavors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function IsDataFlavorSupported (This : access Typ;
                                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                   return Java.Boolean is abstract;

   function GetTransferData (This : access Typ;
                             P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                             return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Awt.Datatransfer.UnsupportedFlavorException.Except and
   --  Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTransferDataFlavors, "getTransferDataFlavors");
   pragma Export (Java, IsDataFlavorSupported, "isDataFlavorSupported");
   pragma Export (Java, GetTransferData, "getTransferData");

end Java.Awt.Datatransfer.Transferable;
pragma Import (Java, Java.Awt.Datatransfer.Transferable, "java.awt.datatransfer.Transferable");
pragma Extensions_Allowed (Off);
