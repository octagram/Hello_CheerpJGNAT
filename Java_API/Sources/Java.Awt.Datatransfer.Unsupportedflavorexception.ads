pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Java.Awt.Datatransfer.UnsupportedFlavorException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnsupportedFlavorException (P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.awt.datatransfer.UnsupportedFlavorException");
   pragma Java_Constructor (New_UnsupportedFlavorException);

end Java.Awt.Datatransfer.UnsupportedFlavorException;
pragma Import (Java, Java.Awt.Datatransfer.UnsupportedFlavorException, "java.awt.datatransfer.UnsupportedFlavorException");
pragma Extensions_Allowed (Off);
