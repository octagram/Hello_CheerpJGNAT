pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
with Java.Awt.ContainerOrderFocusTraversalPolicy;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.DefaultFocusTraversalPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.ContainerOrderFocusTraversalPolicy.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultFocusTraversalPolicy (This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function accept_K (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                      return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultFocusTraversalPolicy);
   pragma Import (Java, accept_K, "accept");

end Java.Awt.DefaultFocusTraversalPolicy;
pragma Import (Java, Java.Awt.DefaultFocusTraversalPolicy, "java.awt.DefaultFocusTraversalPolicy");
pragma Extensions_Allowed (Off);
