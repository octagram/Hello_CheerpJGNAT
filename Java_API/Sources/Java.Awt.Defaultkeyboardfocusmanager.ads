pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Event.KeyEvent;
with Java.Awt.KeyEventDispatcher;
with Java.Awt.KeyEventPostProcessor;
with Java.Awt.KeyboardFocusManager;
with Java.Lang.Object;

package Java.Awt.DefaultKeyboardFocusManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeyEventDispatcher_I : Java.Awt.KeyEventDispatcher.Ref;
            KeyEventPostProcessor_I : Java.Awt.KeyEventPostProcessor.Ref)
    is new Java.Awt.KeyboardFocusManager.Typ(KeyEventDispatcher_I,
                                             KeyEventPostProcessor_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultKeyboardFocusManager (This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function DispatchEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class)
                           return Java.Boolean;

   function DispatchKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class)
                              return Java.Boolean;

   function PostProcessKeyEvent (This : access Typ;
                                 P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class)
                                 return Java.Boolean;

   procedure ProcessKeyEvent (This : access Typ;
                              P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                              P2_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   --  protected  synchronized
   procedure EnqueueKeyEvents (This : access Typ;
                               P1_Long : Java.Long;
                               P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected  synchronized
   procedure DequeueKeyEvents (This : access Typ;
                               P1_Long : Java.Long;
                               P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected  synchronized
   procedure DiscardKeyEvents (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure FocusPreviousComponent (This : access Typ;
                                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure FocusNextComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure UpFocusCycle (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure DownFocusCycle (This : access Typ;
                             P1_Container : access Standard.Java.Awt.Container.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultKeyboardFocusManager);
   pragma Import (Java, DispatchEvent, "dispatchEvent");
   pragma Import (Java, DispatchKeyEvent, "dispatchKeyEvent");
   pragma Import (Java, PostProcessKeyEvent, "postProcessKeyEvent");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, EnqueueKeyEvents, "enqueueKeyEvents");
   pragma Import (Java, DequeueKeyEvents, "dequeueKeyEvents");
   pragma Import (Java, DiscardKeyEvents, "discardKeyEvents");
   pragma Import (Java, FocusPreviousComponent, "focusPreviousComponent");
   pragma Import (Java, FocusNextComponent, "focusNextComponent");
   pragma Import (Java, UpFocusCycle, "upFocusCycle");
   pragma Import (Java, DownFocusCycle, "downFocusCycle");

end Java.Awt.DefaultKeyboardFocusManager;
pragma Import (Java, Java.Awt.DefaultKeyboardFocusManager, "java.awt.DefaultKeyboardFocusManager");
pragma Extensions_Allowed (Off);
