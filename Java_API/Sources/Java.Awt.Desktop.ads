pragma Extensions_Allowed (On);
limited with Java.Awt.Desktop.Action;
limited with Java.Io.File;
limited with Java.Net.URI;
with Java.Lang.Object;

package Java.Awt.Desktop is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetDesktop return access Java.Awt.Desktop.Typ'Class;

   function IsDesktopSupported return Java.Boolean;

   function IsSupported (This : access Typ;
                         P1_Action : access Standard.Java.Awt.Desktop.Action.Typ'Class)
                         return Java.Boolean;

   procedure Open (This : access Typ;
                   P1_File : access Standard.Java.Io.File.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Edit (This : access Typ;
                   P1_File : access Standard.Java.Io.File.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Print (This : access Typ;
                    P1_File : access Standard.Java.Io.File.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Browse (This : access Typ;
                     P1_URI : access Standard.Java.Net.URI.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Mail (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Mail (This : access Typ;
                   P1_URI : access Standard.Java.Net.URI.Typ'Class);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDesktop, "getDesktop");
   pragma Import (Java, IsDesktopSupported, "isDesktopSupported");
   pragma Import (Java, IsSupported, "isSupported");
   pragma Import (Java, Open, "open");
   pragma Import (Java, Edit, "edit");
   pragma Import (Java, Print, "print");
   pragma Import (Java, Browse, "browse");
   pragma Import (Java, Mail, "mail");

end Java.Awt.Desktop;
pragma Import (Java, Java.Awt.Desktop, "java.awt.Desktop");
pragma Extensions_Allowed (Off);
