pragma Extensions_Allowed (On);
limited with Java.Awt.Dialog.ModalityType;
limited with Java.Awt.Frame;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Awt.Window;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Dialog is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Window.Typ(MenuContainer_I,
                               ImageObserver_I,
                               Serializable_I,
                               Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Dialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                        P2_Boolean : Java.Boolean; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_Boolean : Java.Boolean; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_Boolean : Java.Boolean;
                        P4_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_Boolean : Java.Boolean; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_Boolean : Java.Boolean;
                        P4_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Window : access Standard.Java.Awt.Window.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                        P2_ModalityType : access Standard.Java.Awt.Dialog.ModalityType.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_ModalityType : access Standard.Java.Awt.Dialog.ModalityType.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Dialog (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_ModalityType : access Standard.Java.Awt.Dialog.ModalityType.Typ'Class;
                        P4_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function IsModal (This : access Typ)
                     return Java.Boolean;

   procedure SetModal (This : access Typ;
                       P1_Boolean : Java.Boolean);

   function GetModalityType (This : access Typ)
                             return access Java.Awt.Dialog.ModalityType.Typ'Class;

   procedure SetModalityType (This : access Typ;
                              P1_ModalityType : access Standard.Java.Awt.Dialog.ModalityType.Typ'Class);

   function GetTitle (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetTitle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure ToBack (This : access Typ);

   function IsResizable (This : access Typ)
                         return Java.Boolean;

   procedure SetResizable (This : access Typ;
                           P1_Boolean : Java.Boolean);

   procedure SetUndecorated (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function IsUndecorated (This : access Typ)
                           return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_MODALITY_TYPE : access Java.Awt.Dialog.ModalityType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Dialog);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, IsModal, "isModal");
   pragma Import (Java, SetModal, "setModal");
   pragma Import (Java, GetModalityType, "getModalityType");
   pragma Import (Java, SetModalityType, "setModalityType");
   pragma Import (Java, GetTitle, "getTitle");
   pragma Import (Java, SetTitle, "setTitle");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, ToBack, "toBack");
   pragma Import (Java, IsResizable, "isResizable");
   pragma Import (Java, SetResizable, "setResizable");
   pragma Import (Java, SetUndecorated, "setUndecorated");
   pragma Import (Java, IsUndecorated, "isUndecorated");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, DEFAULT_MODALITY_TYPE, "DEFAULT_MODALITY_TYPE");

end Java.Awt.Dialog;
pragma Import (Java, Java.Awt.Dialog, "java.awt.Dialog");
pragma Extensions_Allowed (Off);
