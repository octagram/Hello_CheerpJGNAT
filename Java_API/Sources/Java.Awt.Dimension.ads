pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Geom.Dimension2D;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Dimension is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Dimension2D.Typ(Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Width : Java.Int;
      pragma Import (Java, Width, "width");

      Height : Java.Int;
      pragma Import (Java, Height, "height");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Dimension (This : Ref := null)
                           return Ref;

   function New_Dimension (P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Dimension (P1_Int : Java.Int;
                           P2_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetWidth (This : access Typ)
                      return Java.Double;

   function GetHeight (This : access Typ)
                       return Java.Double;

   procedure SetSize (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double);

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Dimension);
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Dimension;
pragma Import (Java, Java.Awt.Dimension, "java.awt.Dimension");
pragma Extensions_Allowed (Off);
