pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.DisplayMode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DisplayMode (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHeight (This : access Typ)
                       return Java.Int;

   function GetWidth (This : access Typ)
                      return Java.Int;

   function GetBitDepth (This : access Typ)
                         return Java.Int;

   function GetRefreshRate (This : access Typ)
                            return Java.Int;

   function Equals (This : access Typ;
                    P1_DisplayMode : access Standard.Java.Awt.DisplayMode.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BIT_DEPTH_MULTI : constant Java.Int;

   --  final
   REFRESH_RATE_UNKNOWN : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DisplayMode);
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetBitDepth, "getBitDepth");
   pragma Import (Java, GetRefreshRate, "getRefreshRate");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, BIT_DEPTH_MULTI, "BIT_DEPTH_MULTI");
   pragma Import (Java, REFRESH_RATE_UNKNOWN, "REFRESH_RATE_UNKNOWN");

end Java.Awt.DisplayMode;
pragma Import (Java, Java.Awt.DisplayMode, "java.awt.DisplayMode");
pragma Extensions_Allowed (Off);
