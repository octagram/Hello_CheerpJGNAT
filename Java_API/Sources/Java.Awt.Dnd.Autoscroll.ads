pragma Extensions_Allowed (On);
limited with Java.Awt.Insets;
limited with Java.Awt.Point;
with Java.Lang.Object;

package Java.Awt.Dnd.Autoscroll is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAutoscrollInsets (This : access Typ)
                                 return access Java.Awt.Insets.Typ'Class is abstract;

   procedure Autoscroll (This : access Typ;
                         P1_Point : access Standard.Java.Awt.Point.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAutoscrollInsets, "getAutoscrollInsets");
   pragma Export (Java, Autoscroll, "autoscroll");

end Java.Awt.Dnd.Autoscroll;
pragma Import (Java, Java.Awt.Dnd.Autoscroll, "java.awt.dnd.Autoscroll");
pragma Extensions_Allowed (Off);
