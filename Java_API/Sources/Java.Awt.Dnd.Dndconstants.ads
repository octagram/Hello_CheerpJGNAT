pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Dnd.DnDConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ACTION_NONE : constant Java.Int;

   --  final
   ACTION_COPY : constant Java.Int;

   --  final
   ACTION_MOVE : constant Java.Int;

   --  final
   ACTION_COPY_OR_MOVE : constant Java.Int;

   --  final
   ACTION_LINK : constant Java.Int;

   --  final
   ACTION_REFERENCE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ACTION_NONE, "ACTION_NONE");
   pragma Import (Java, ACTION_COPY, "ACTION_COPY");
   pragma Import (Java, ACTION_MOVE, "ACTION_MOVE");
   pragma Import (Java, ACTION_COPY_OR_MOVE, "ACTION_COPY_OR_MOVE");
   pragma Import (Java, ACTION_LINK, "ACTION_LINK");
   pragma Import (Java, ACTION_REFERENCE, "ACTION_REFERENCE");

end Java.Awt.Dnd.DnDConstants;
pragma Import (Java, Java.Awt.Dnd.DnDConstants, "java.awt.dnd.DnDConstants");
pragma Extensions_Allowed (Off);
