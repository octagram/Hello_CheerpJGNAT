pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Cursor;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Awt.Dnd.DragGestureRecognizer;
limited with Java.Awt.Dnd.DragSource;
limited with Java.Awt.Dnd.DragSourceListener;
limited with Java.Awt.Event.InputEvent;
limited with Java.Awt.Image;
limited with Java.Awt.Point;
limited with Java.Util.Iterator;
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Awt.Dnd.DragGestureEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DragGestureEvent (P1_DragGestureRecognizer : access Standard.Java.Awt.Dnd.DragGestureRecognizer.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Point : access Standard.Java.Awt.Point.Typ'Class;
                                  P4_List : access Standard.Java.Util.List.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSourceAsDragGestureRecognizer (This : access Typ)
                                              return access Java.Awt.Dnd.DragGestureRecognizer.Typ'Class;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   function GetDragSource (This : access Typ)
                           return access Java.Awt.Dnd.DragSource.Typ'Class;

   function GetDragOrigin (This : access Typ)
                           return access Java.Awt.Point.Typ'Class;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function GetDragAction (This : access Typ)
                           return Java.Int;

   function GetTriggerEvent (This : access Typ)
                             return access Java.Awt.Event.InputEvent.Typ'Class;

   procedure StartDrag (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P2_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   procedure StartDrag (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P2_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                        P3_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   procedure StartDrag (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P2_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P3_Point : access Standard.Java.Awt.Point.Typ'Class;
                        P4_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                        P5_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DragGestureEvent);
   pragma Import (Java, GetSourceAsDragGestureRecognizer, "getSourceAsDragGestureRecognizer");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, GetDragSource, "getDragSource");
   pragma Import (Java, GetDragOrigin, "getDragOrigin");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, GetDragAction, "getDragAction");
   pragma Import (Java, GetTriggerEvent, "getTriggerEvent");
   pragma Import (Java, StartDrag, "startDrag");

end Java.Awt.Dnd.DragGestureEvent;
pragma Import (Java, Java.Awt.Dnd.DragGestureEvent, "java.awt.dnd.DragGestureEvent");
pragma Extensions_Allowed (Off);
