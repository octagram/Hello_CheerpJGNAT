pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dnd.DragGestureListener;
limited with Java.Awt.Dnd.DragSource;
limited with Java.Awt.Event.InputEvent;
limited with Java.Awt.Point;
limited with Java.Util.ArrayList;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DragGestureRecognizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DragSource : access Java.Awt.Dnd.DragSource.Typ'Class;
      pragma Import (Java, DragSource, "dragSource");

      --  protected
      Component : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, Component, "component");

      --  protected
      DragGestureListener : access Java.Awt.Dnd.DragGestureListener.Typ'Class;
      pragma Import (Java, DragGestureListener, "dragGestureListener");

      --  protected
      SourceActions : Java.Int;
      pragma Import (Java, SourceActions, "sourceActions");

      --  protected
      Events : access Java.Util.ArrayList.Typ'Class;
      pragma Import (Java, Events, "events");

   end record;

   --  protected
   function New_DragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class;
                                       P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_DragGestureListener : access Standard.Java.Awt.Dnd.DragGestureListener.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   --  protected
   function New_DragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class;
                                       P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                                       P3_Int : Java.Int; 
                                       This : Ref := null)
                                       return Ref;

   --  protected
   function New_DragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class;
                                       P2_Component : access Standard.Java.Awt.Component.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   --  protected
   function New_DragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure RegisterListeners (This : access Typ) is abstract;

   --  protected
   procedure UnregisterListeners (This : access Typ) is abstract;

   function GetDragSource (This : access Typ)
                           return access Java.Awt.Dnd.DragSource.Typ'Class;

   --  synchronized
   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   --  synchronized
   procedure SetComponent (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  synchronized
   function GetSourceActions (This : access Typ)
                              return Java.Int;

   --  synchronized
   procedure SetSourceActions (This : access Typ;
                               P1_Int : Java.Int);

   function GetTriggerEvent (This : access Typ)
                             return access Java.Awt.Event.InputEvent.Typ'Class;

   procedure ResetRecognizer (This : access Typ);

   --  synchronized
   procedure AddDragGestureListener (This : access Typ;
                                     P1_DragGestureListener : access Standard.Java.Awt.Dnd.DragGestureListener.Typ'Class);
   --  can raise Java.Util.TooManyListenersException.Except

   --  synchronized
   procedure RemoveDragGestureListener (This : access Typ;
                                        P1_DragGestureListener : access Standard.Java.Awt.Dnd.DragGestureListener.Typ'Class);

   --  protected  synchronized
   procedure FireDragGestureRecognized (This : access Typ;
                                        P1_Int : Java.Int;
                                        P2_Point : access Standard.Java.Awt.Point.Typ'Class);

   --  protected  synchronized
   procedure AppendEvent (This : access Typ;
                          P1_InputEvent : access Standard.Java.Awt.Event.InputEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DragGestureRecognizer);
   pragma Export (Java, RegisterListeners, "registerListeners");
   pragma Export (Java, UnregisterListeners, "unregisterListeners");
   pragma Export (Java, GetDragSource, "getDragSource");
   pragma Export (Java, GetComponent, "getComponent");
   pragma Export (Java, SetComponent, "setComponent");
   pragma Export (Java, GetSourceActions, "getSourceActions");
   pragma Export (Java, SetSourceActions, "setSourceActions");
   pragma Export (Java, GetTriggerEvent, "getTriggerEvent");
   pragma Export (Java, ResetRecognizer, "resetRecognizer");
   pragma Export (Java, AddDragGestureListener, "addDragGestureListener");
   pragma Export (Java, RemoveDragGestureListener, "removeDragGestureListener");
   pragma Export (Java, FireDragGestureRecognized, "fireDragGestureRecognized");
   pragma Export (Java, AppendEvent, "appendEvent");

end Java.Awt.Dnd.DragGestureRecognizer;
pragma Import (Java, Java.Awt.Dnd.DragGestureRecognizer, "java.awt.dnd.DragGestureRecognizer");
pragma Extensions_Allowed (Off);
