pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Cursor;
limited with Java.Awt.Datatransfer.FlavorMap;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Awt.Dnd.DragGestureEvent;
limited with Java.Awt.Dnd.DragGestureListener;
limited with Java.Awt.Dnd.DragGestureRecognizer;
limited with Java.Awt.Dnd.DragSourceContext;
limited with Java.Awt.Dnd.DragSourceListener;
limited with Java.Awt.Dnd.DragSourceMotionListener;
limited with Java.Awt.Dnd.Peer.DragSourceContextPeer;
limited with Java.Awt.Image;
limited with Java.Awt.Point;
limited with Java.Lang.Class;
limited with Java.Util.EventListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DragSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultDragSource return access Java.Awt.Dnd.DragSource.Typ'Class;

   function IsDragImageSupported return Java.Boolean;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DragSource (This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   procedure StartDrag (This : access Typ;
                        P1_DragGestureEvent : access Standard.Java.Awt.Dnd.DragGestureEvent.Typ'Class;
                        P2_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P3_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P4_Point : access Standard.Java.Awt.Point.Typ'Class;
                        P5_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                        P6_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class;
                        P7_FlavorMap : access Standard.Java.Awt.Datatransfer.FlavorMap.Typ'Class);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   procedure StartDrag (This : access Typ;
                        P1_DragGestureEvent : access Standard.Java.Awt.Dnd.DragGestureEvent.Typ'Class;
                        P2_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P3_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                        P4_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class;
                        P5_FlavorMap : access Standard.Java.Awt.Datatransfer.FlavorMap.Typ'Class);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   procedure StartDrag (This : access Typ;
                        P1_DragGestureEvent : access Standard.Java.Awt.Dnd.DragGestureEvent.Typ'Class;
                        P2_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P3_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P4_Point : access Standard.Java.Awt.Point.Typ'Class;
                        P5_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                        P6_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   procedure StartDrag (This : access Typ;
                        P1_DragGestureEvent : access Standard.Java.Awt.Dnd.DragGestureEvent.Typ'Class;
                        P2_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P3_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                        P4_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   --  protected
   function CreateDragSourceContext (This : access Typ;
                                     P1_DragSourceContextPeer : access Standard.Java.Awt.Dnd.Peer.DragSourceContextPeer.Typ'Class;
                                     P2_DragGestureEvent : access Standard.Java.Awt.Dnd.DragGestureEvent.Typ'Class;
                                     P3_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                                     P4_Image : access Standard.Java.Awt.Image.Typ'Class;
                                     P5_Point : access Standard.Java.Awt.Point.Typ'Class;
                                     P6_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                                     P7_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class)
                                     return access Java.Awt.Dnd.DragSourceContext.Typ'Class;

   function GetFlavorMap (This : access Typ)
                          return access Java.Awt.Datatransfer.FlavorMap.Typ'Class;

   function CreateDragGestureRecognizer (This : access Typ;
                                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                         P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_DragGestureListener : access Standard.Java.Awt.Dnd.DragGestureListener.Typ'Class)
                                         return access Java.Awt.Dnd.DragGestureRecognizer.Typ'Class;

   function CreateDefaultDragGestureRecognizer (This : access Typ;
                                                P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                                P2_Int : Java.Int;
                                                P3_DragGestureListener : access Standard.Java.Awt.Dnd.DragGestureListener.Typ'Class)
                                                return access Java.Awt.Dnd.DragGestureRecognizer.Typ'Class;

   procedure AddDragSourceListener (This : access Typ;
                                    P1_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);

   procedure RemoveDragSourceListener (This : access Typ;
                                       P1_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);

   function GetDragSourceListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   procedure AddDragSourceMotionListener (This : access Typ;
                                          P1_DragSourceMotionListener : access Standard.Java.Awt.Dnd.DragSourceMotionListener.Typ'Class);

   procedure RemoveDragSourceMotionListener (This : access Typ;
                                             P1_DragSourceMotionListener : access Standard.Java.Awt.Dnd.DragSourceMotionListener.Typ'Class);

   function GetDragSourceMotionListeners (This : access Typ)
                                          return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetDragThreshold return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DefaultCopyDrop : access Java.Awt.Cursor.Typ'Class;

   --  final
   DefaultMoveDrop : access Java.Awt.Cursor.Typ'Class;

   --  final
   DefaultLinkDrop : access Java.Awt.Cursor.Typ'Class;

   --  final
   DefaultCopyNoDrop : access Java.Awt.Cursor.Typ'Class;

   --  final
   DefaultMoveNoDrop : access Java.Awt.Cursor.Typ'Class;

   --  final
   DefaultLinkNoDrop : access Java.Awt.Cursor.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDefaultDragSource, "getDefaultDragSource");
   pragma Import (Java, IsDragImageSupported, "isDragImageSupported");
   pragma Java_Constructor (New_DragSource);
   pragma Import (Java, StartDrag, "startDrag");
   pragma Import (Java, CreateDragSourceContext, "createDragSourceContext");
   pragma Import (Java, GetFlavorMap, "getFlavorMap");
   pragma Import (Java, CreateDragGestureRecognizer, "createDragGestureRecognizer");
   pragma Import (Java, CreateDefaultDragGestureRecognizer, "createDefaultDragGestureRecognizer");
   pragma Import (Java, AddDragSourceListener, "addDragSourceListener");
   pragma Import (Java, RemoveDragSourceListener, "removeDragSourceListener");
   pragma Import (Java, GetDragSourceListeners, "getDragSourceListeners");
   pragma Import (Java, AddDragSourceMotionListener, "addDragSourceMotionListener");
   pragma Import (Java, RemoveDragSourceMotionListener, "removeDragSourceMotionListener");
   pragma Import (Java, GetDragSourceMotionListeners, "getDragSourceMotionListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, GetDragThreshold, "getDragThreshold");
   pragma Import (Java, DefaultCopyDrop, "DefaultCopyDrop");
   pragma Import (Java, DefaultMoveDrop, "DefaultMoveDrop");
   pragma Import (Java, DefaultLinkDrop, "DefaultLinkDrop");
   pragma Import (Java, DefaultCopyNoDrop, "DefaultCopyNoDrop");
   pragma Import (Java, DefaultMoveNoDrop, "DefaultMoveNoDrop");
   pragma Import (Java, DefaultLinkNoDrop, "DefaultLinkNoDrop");

end Java.Awt.Dnd.DragSource;
pragma Import (Java, Java.Awt.Dnd.DragSource, "java.awt.dnd.DragSource");
pragma Extensions_Allowed (Off);
