pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DragSourceDragEvent;
limited with Java.Awt.Dnd.DragSourceDropEvent;
limited with Java.Awt.Dnd.DragSourceEvent;
with Java.Awt.Dnd.DragSourceListener;
with Java.Awt.Dnd.DragSourceMotionListener;
with Java.Lang.Object;

package Java.Awt.Dnd.DragSourceAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DragSourceListener_I : Java.Awt.Dnd.DragSourceListener.Ref;
            DragSourceMotionListener_I : Java.Awt.Dnd.DragSourceMotionListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_DragSourceAdapter (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DragEnter (This : access Typ;
                        P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   procedure DragOver (This : access Typ;
                       P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   procedure DragMouseMoved (This : access Typ;
                             P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   procedure DropActionChanged (This : access Typ;
                                P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   procedure DragExit (This : access Typ;
                       P1_DragSourceEvent : access Standard.Java.Awt.Dnd.DragSourceEvent.Typ'Class);

   procedure DragDropEnd (This : access Typ;
                          P1_DragSourceDropEvent : access Standard.Java.Awt.Dnd.DragSourceDropEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DragSourceAdapter);
   pragma Import (Java, DragEnter, "dragEnter");
   pragma Import (Java, DragOver, "dragOver");
   pragma Import (Java, DragMouseMoved, "dragMouseMoved");
   pragma Import (Java, DropActionChanged, "dropActionChanged");
   pragma Import (Java, DragExit, "dragExit");
   pragma Import (Java, DragDropEnd, "dragDropEnd");

end Java.Awt.Dnd.DragSourceAdapter;
pragma Import (Java, Java.Awt.Dnd.DragSourceAdapter, "java.awt.dnd.DragSourceAdapter");
pragma Extensions_Allowed (Off);
