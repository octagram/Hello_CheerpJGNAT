pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Cursor;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Awt.Dnd.DragGestureEvent;
limited with Java.Awt.Dnd.DragSource;
limited with Java.Awt.Dnd.DragSourceDragEvent;
limited with Java.Awt.Dnd.DragSourceDropEvent;
limited with Java.Awt.Dnd.DragSourceEvent;
limited with Java.Awt.Dnd.Peer.DragSourceContextPeer;
limited with Java.Awt.Image;
limited with Java.Awt.Point;
with Java.Awt.Dnd.DragSourceListener;
with Java.Awt.Dnd.DragSourceMotionListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DragSourceContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DragSourceListener_I : Java.Awt.Dnd.DragSourceListener.Ref;
            DragSourceMotionListener_I : Java.Awt.Dnd.DragSourceMotionListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DragSourceContext (P1_DragSourceContextPeer : access Standard.Java.Awt.Dnd.Peer.DragSourceContextPeer.Typ'Class;
                                   P2_DragGestureEvent : access Standard.Java.Awt.Dnd.DragGestureEvent.Typ'Class;
                                   P3_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                                   P4_Image : access Standard.Java.Awt.Image.Typ'Class;
                                   P5_Point : access Standard.Java.Awt.Point.Typ'Class;
                                   P6_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                                   P7_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDragSource (This : access Typ)
                           return access Java.Awt.Dnd.DragSource.Typ'Class;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   function GetTrigger (This : access Typ)
                        return access Java.Awt.Dnd.DragGestureEvent.Typ'Class;

   function GetSourceActions (This : access Typ)
                              return Java.Int;

   --  synchronized
   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetCursor (This : access Typ)
                       return access Java.Awt.Cursor.Typ'Class;

   --  synchronized
   procedure AddDragSourceListener (This : access Typ;
                                    P1_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);
   --  can raise Java.Util.TooManyListenersException.Except

   --  synchronized
   procedure RemoveDragSourceListener (This : access Typ;
                                       P1_DragSourceListener : access Standard.Java.Awt.Dnd.DragSourceListener.Typ'Class);

   procedure TransferablesFlavorsChanged (This : access Typ);

   procedure DragEnter (This : access Typ;
                        P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   procedure DragOver (This : access Typ;
                       P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   procedure DragExit (This : access Typ;
                       P1_DragSourceEvent : access Standard.Java.Awt.Dnd.DragSourceEvent.Typ'Class);

   procedure DropActionChanged (This : access Typ;
                                P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   procedure DragDropEnd (This : access Typ;
                          P1_DragSourceDropEvent : access Standard.Java.Awt.Dnd.DragSourceDropEvent.Typ'Class);

   procedure DragMouseMoved (This : access Typ;
                             P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class);

   function GetTransferable (This : access Typ)
                             return access Java.Awt.Datatransfer.Transferable.Typ'Class;

   --  protected  synchronized
   procedure UpdateCurrentCursor (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   DEFAULT : constant Java.Int;

   --  protected  final
   ENTER : constant Java.Int;

   --  protected  final
   OVER : constant Java.Int;

   --  protected  final
   CHANGED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DragSourceContext);
   pragma Import (Java, GetDragSource, "getDragSource");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, GetTrigger, "getTrigger");
   pragma Import (Java, GetSourceActions, "getSourceActions");
   pragma Import (Java, SetCursor, "setCursor");
   pragma Import (Java, GetCursor, "getCursor");
   pragma Import (Java, AddDragSourceListener, "addDragSourceListener");
   pragma Import (Java, RemoveDragSourceListener, "removeDragSourceListener");
   pragma Import (Java, TransferablesFlavorsChanged, "transferablesFlavorsChanged");
   pragma Import (Java, DragEnter, "dragEnter");
   pragma Import (Java, DragOver, "dragOver");
   pragma Import (Java, DragExit, "dragExit");
   pragma Import (Java, DropActionChanged, "dropActionChanged");
   pragma Import (Java, DragDropEnd, "dragDropEnd");
   pragma Import (Java, DragMouseMoved, "dragMouseMoved");
   pragma Import (Java, GetTransferable, "getTransferable");
   pragma Import (Java, UpdateCurrentCursor, "updateCurrentCursor");
   pragma Import (Java, DEFAULT, "DEFAULT");
   pragma Import (Java, ENTER, "ENTER");
   pragma Import (Java, OVER, "OVER");
   pragma Import (Java, CHANGED, "CHANGED");

end Java.Awt.Dnd.DragSourceContext;
pragma Import (Java, Java.Awt.Dnd.DragSourceContext, "java.awt.dnd.DragSourceContext");
pragma Extensions_Allowed (Off);
