pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DragSourceContext;
with Java.Awt.Dnd.DragSourceEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DragSourceDragEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Dnd.DragSourceEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DragSourceDragEvent (P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   function New_DragSourceDragEvent (P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTargetActions (This : access Typ)
                              return Java.Int;

   function GetGestureModifiers (This : access Typ)
                                 return Java.Int;

   function GetGestureModifiersEx (This : access Typ)
                                   return Java.Int;

   function GetUserAction (This : access Typ)
                           return Java.Int;

   function GetDropAction (This : access Typ)
                           return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DragSourceDragEvent);
   pragma Import (Java, GetTargetActions, "getTargetActions");
   pragma Import (Java, GetGestureModifiers, "getGestureModifiers");
   pragma Import (Java, GetGestureModifiersEx, "getGestureModifiersEx");
   pragma Import (Java, GetUserAction, "getUserAction");
   pragma Import (Java, GetDropAction, "getDropAction");

end Java.Awt.Dnd.DragSourceDragEvent;
pragma Import (Java, Java.Awt.Dnd.DragSourceDragEvent, "java.awt.dnd.DragSourceDragEvent");
pragma Extensions_Allowed (Off);
