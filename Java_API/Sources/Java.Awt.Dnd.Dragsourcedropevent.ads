pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DragSourceContext;
with Java.Awt.Dnd.DragSourceEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DragSourceDropEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Dnd.DragSourceEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DragSourceDropEvent (P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Boolean : Java.Boolean; 
                                     This : Ref := null)
                                     return Ref;

   function New_DragSourceDropEvent (P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Boolean : Java.Boolean;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   function New_DragSourceDropEvent (P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDropSuccess (This : access Typ)
                            return Java.Boolean;

   function GetDropAction (This : access Typ)
                           return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DragSourceDropEvent);
   pragma Import (Java, GetDropSuccess, "getDropSuccess");
   pragma Import (Java, GetDropAction, "getDropAction");

end Java.Awt.Dnd.DragSourceDropEvent;
pragma Import (Java, Java.Awt.Dnd.DragSourceDropEvent, "java.awt.dnd.DragSourceDropEvent");
pragma Extensions_Allowed (Off);
