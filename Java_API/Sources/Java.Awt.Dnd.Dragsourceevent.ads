pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DragSourceContext;
limited with Java.Awt.Point;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Awt.Dnd.DragSourceEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DragSourceEvent (P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_DragSourceEvent (P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDragSourceContext (This : access Typ)
                                  return access Java.Awt.Dnd.DragSourceContext.Typ'Class;

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   function GetX (This : access Typ)
                  return Java.Int;

   function GetY (This : access Typ)
                  return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DragSourceEvent);
   pragma Import (Java, GetDragSourceContext, "getDragSourceContext");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");

end Java.Awt.Dnd.DragSourceEvent;
pragma Import (Java, Java.Awt.Dnd.DragSourceEvent, "java.awt.dnd.DragSourceEvent");
pragma Extensions_Allowed (Off);
