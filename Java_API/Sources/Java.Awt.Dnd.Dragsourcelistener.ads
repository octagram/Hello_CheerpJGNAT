pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DragSourceDragEvent;
limited with Java.Awt.Dnd.DragSourceDropEvent;
limited with Java.Awt.Dnd.DragSourceEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Dnd.DragSourceListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DragEnter (This : access Typ;
                        P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class) is abstract;

   procedure DragOver (This : access Typ;
                       P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class) is abstract;

   procedure DropActionChanged (This : access Typ;
                                P1_DragSourceDragEvent : access Standard.Java.Awt.Dnd.DragSourceDragEvent.Typ'Class) is abstract;

   procedure DragExit (This : access Typ;
                       P1_DragSourceEvent : access Standard.Java.Awt.Dnd.DragSourceEvent.Typ'Class) is abstract;

   procedure DragDropEnd (This : access Typ;
                          P1_DragSourceDropEvent : access Standard.Java.Awt.Dnd.DragSourceDropEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, DragEnter, "dragEnter");
   pragma Export (Java, DragOver, "dragOver");
   pragma Export (Java, DropActionChanged, "dropActionChanged");
   pragma Export (Java, DragExit, "dragExit");
   pragma Export (Java, DragDropEnd, "dragDropEnd");

end Java.Awt.Dnd.DragSourceListener;
pragma Import (Java, Java.Awt.Dnd.DragSourceListener, "java.awt.dnd.DragSourceListener");
pragma Extensions_Allowed (Off);
