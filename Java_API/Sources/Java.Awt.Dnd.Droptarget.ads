pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Datatransfer.FlavorMap;
limited with Java.Awt.Dnd.DropTargetContext;
limited with Java.Awt.Dnd.DropTargetDragEvent;
limited with Java.Awt.Dnd.DropTargetDropEvent;
limited with Java.Awt.Dnd.DropTargetEvent;
limited with Java.Awt.Peer.ComponentPeer;
limited with Java.Awt.Point;
with Java.Awt.Dnd.DropTargetListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DropTarget is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DropTargetListener_I : Java.Awt.Dnd.DropTargetListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DropTarget (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_DropTargetListener : access Standard.Java.Awt.Dnd.DropTargetListener.Typ'Class;
                            P4_Boolean : Java.Boolean;
                            P5_FlavorMap : access Standard.Java.Awt.Datatransfer.FlavorMap.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_DropTarget (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_DropTargetListener : access Standard.Java.Awt.Dnd.DropTargetListener.Typ'Class;
                            P4_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_DropTarget (This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_DropTarget (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_DropTargetListener : access Standard.Java.Awt.Dnd.DropTargetListener.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_DropTarget (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_DropTargetListener : access Standard.Java.Awt.Dnd.DropTargetListener.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure SetComponent (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  synchronized
   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   procedure SetDefaultActions (This : access Typ;
                                P1_Int : Java.Int);

   function GetDefaultActions (This : access Typ)
                               return Java.Int;

   --  synchronized
   procedure SetActive (This : access Typ;
                        P1_Boolean : Java.Boolean);

   function IsActive (This : access Typ)
                      return Java.Boolean;

   --  synchronized
   procedure AddDropTargetListener (This : access Typ;
                                    P1_DropTargetListener : access Standard.Java.Awt.Dnd.DropTargetListener.Typ'Class);
   --  can raise Java.Util.TooManyListenersException.Except

   --  synchronized
   procedure RemoveDropTargetListener (This : access Typ;
                                       P1_DropTargetListener : access Standard.Java.Awt.Dnd.DropTargetListener.Typ'Class);

   --  synchronized
   procedure DragEnter (This : access Typ;
                        P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class);

   --  synchronized
   procedure DragOver (This : access Typ;
                       P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class);

   --  synchronized
   procedure DropActionChanged (This : access Typ;
                                P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class);

   --  synchronized
   procedure DragExit (This : access Typ;
                       P1_DropTargetEvent : access Standard.Java.Awt.Dnd.DropTargetEvent.Typ'Class);

   --  synchronized
   procedure Drop (This : access Typ;
                   P1_DropTargetDropEvent : access Standard.Java.Awt.Dnd.DropTargetDropEvent.Typ'Class);

   function GetFlavorMap (This : access Typ)
                          return access Java.Awt.Datatransfer.FlavorMap.Typ'Class;

   procedure SetFlavorMap (This : access Typ;
                           P1_FlavorMap : access Standard.Java.Awt.Datatransfer.FlavorMap.Typ'Class);

   procedure AddNotify (This : access Typ;
                        P1_ComponentPeer : access Standard.Java.Awt.Peer.ComponentPeer.Typ'Class);

   procedure RemoveNotify (This : access Typ;
                           P1_ComponentPeer : access Standard.Java.Awt.Peer.ComponentPeer.Typ'Class);

   function GetDropTargetContext (This : access Typ)
                                  return access Java.Awt.Dnd.DropTargetContext.Typ'Class;

   --  protected
   function CreateDropTargetContext (This : access Typ)
                                     return access Java.Awt.Dnd.DropTargetContext.Typ'Class;

   --  protected
   procedure InitializeAutoscrolling (This : access Typ;
                                      P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   --  protected
   procedure UpdateAutoscroll (This : access Typ;
                               P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   --  protected
   procedure ClearAutoscroll (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DropTarget);
   pragma Import (Java, SetComponent, "setComponent");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, SetDefaultActions, "setDefaultActions");
   pragma Import (Java, GetDefaultActions, "getDefaultActions");
   pragma Import (Java, SetActive, "setActive");
   pragma Import (Java, IsActive, "isActive");
   pragma Import (Java, AddDropTargetListener, "addDropTargetListener");
   pragma Import (Java, RemoveDropTargetListener, "removeDropTargetListener");
   pragma Import (Java, DragEnter, "dragEnter");
   pragma Import (Java, DragOver, "dragOver");
   pragma Import (Java, DropActionChanged, "dropActionChanged");
   pragma Import (Java, DragExit, "dragExit");
   pragma Import (Java, Drop, "drop");
   pragma Import (Java, GetFlavorMap, "getFlavorMap");
   pragma Import (Java, SetFlavorMap, "setFlavorMap");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, GetDropTargetContext, "getDropTargetContext");
   pragma Import (Java, CreateDropTargetContext, "createDropTargetContext");
   pragma Import (Java, InitializeAutoscrolling, "initializeAutoscrolling");
   pragma Import (Java, UpdateAutoscroll, "updateAutoscroll");
   pragma Import (Java, ClearAutoscroll, "clearAutoscroll");

end Java.Awt.Dnd.DropTarget;
pragma Import (Java, Java.Awt.Dnd.DropTarget, "java.awt.dnd.DropTarget");
pragma Extensions_Allowed (Off);
