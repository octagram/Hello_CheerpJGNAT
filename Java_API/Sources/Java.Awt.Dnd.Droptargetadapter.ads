pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DropTargetDragEvent;
limited with Java.Awt.Dnd.DropTargetEvent;
with Java.Awt.Dnd.DropTargetListener;
with Java.Lang.Object;

package Java.Awt.Dnd.DropTargetAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DropTargetListener_I : Java.Awt.Dnd.DropTargetListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_DropTargetAdapter (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DragEnter (This : access Typ;
                        P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class);

   procedure DragOver (This : access Typ;
                       P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class);

   procedure DropActionChanged (This : access Typ;
                                P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class);

   procedure DragExit (This : access Typ;
                       P1_DropTargetEvent : access Standard.Java.Awt.Dnd.DropTargetEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DropTargetAdapter);
   pragma Import (Java, DragEnter, "dragEnter");
   pragma Import (Java, DragOver, "dragOver");
   pragma Import (Java, DropActionChanged, "dropActionChanged");
   pragma Import (Java, DragExit, "dragExit");

end Java.Awt.Dnd.DropTargetAdapter;
pragma Import (Java, Java.Awt.Dnd.DropTargetAdapter, "java.awt.dnd.DropTargetAdapter");
pragma Extensions_Allowed (Off);
