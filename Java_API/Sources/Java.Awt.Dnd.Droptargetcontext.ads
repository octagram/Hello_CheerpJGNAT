pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Awt.Dnd.DropTarget;
limited with Java.Awt.Dnd.Peer.DropTargetContextPeer;
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DropTargetContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDropTarget (This : access Typ)
                           return access Java.Awt.Dnd.DropTarget.Typ'Class;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   procedure AddNotify (This : access Typ;
                        P1_DropTargetContextPeer : access Standard.Java.Awt.Dnd.Peer.DropTargetContextPeer.Typ'Class);

   procedure RemoveNotify (This : access Typ);

   --  protected
   procedure SetTargetActions (This : access Typ;
                               P1_Int : Java.Int);

   --  protected
   function GetTargetActions (This : access Typ)
                              return Java.Int;

   procedure DropComplete (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   --  protected
   procedure AcceptDrag (This : access Typ;
                         P1_Int : Java.Int);

   --  protected
   procedure RejectDrag (This : access Typ);

   --  protected
   procedure AcceptDrop (This : access Typ;
                         P1_Int : Java.Int);

   --  protected
   procedure RejectDrop (This : access Typ);

   --  protected
   function GetCurrentDataFlavors (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetCurrentDataFlavorsAsList (This : access Typ)
                                         return access Java.Util.List.Typ'Class;

   --  protected
   function IsDataFlavorSupported (This : access Typ;
                                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                   return Java.Boolean;

   --  protected
   function GetTransferable (This : access Typ)
                             return access Java.Awt.Datatransfer.Transferable.Typ'Class;
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   --  protected
   function CreateTransferableProxy (This : access Typ;
                                     P1_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                                     P2_Boolean : Java.Boolean)
                                     return access Java.Awt.Datatransfer.Transferable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDropTarget, "getDropTarget");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, SetTargetActions, "setTargetActions");
   pragma Import (Java, GetTargetActions, "getTargetActions");
   pragma Import (Java, DropComplete, "dropComplete");
   pragma Import (Java, AcceptDrag, "acceptDrag");
   pragma Import (Java, RejectDrag, "rejectDrag");
   pragma Import (Java, AcceptDrop, "acceptDrop");
   pragma Import (Java, RejectDrop, "rejectDrop");
   pragma Import (Java, GetCurrentDataFlavors, "getCurrentDataFlavors");
   pragma Import (Java, GetCurrentDataFlavorsAsList, "getCurrentDataFlavorsAsList");
   pragma Import (Java, IsDataFlavorSupported, "isDataFlavorSupported");
   pragma Import (Java, GetTransferable, "getTransferable");
   pragma Import (Java, CreateTransferableProxy, "createTransferableProxy");

end Java.Awt.Dnd.DropTargetContext;
pragma Import (Java, Java.Awt.Dnd.DropTargetContext, "java.awt.dnd.DropTargetContext");
pragma Extensions_Allowed (Off);
