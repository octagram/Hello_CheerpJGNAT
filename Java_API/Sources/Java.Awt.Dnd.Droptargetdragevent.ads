pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Awt.Dnd.DropTargetContext;
limited with Java.Awt.Point;
limited with Java.Util.List;
with Java.Awt.Dnd.DropTargetEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.DropTargetDragEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Dnd.DropTargetEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DropTargetDragEvent (P1_DropTargetContext : access Standard.Java.Awt.Dnd.DropTargetContext.Typ'Class;
                                     P2_Point : access Standard.Java.Awt.Point.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   function GetCurrentDataFlavors (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   function GetCurrentDataFlavorsAsList (This : access Typ)
                                         return access Java.Util.List.Typ'Class;

   function IsDataFlavorSupported (This : access Typ;
                                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                   return Java.Boolean;

   function GetSourceActions (This : access Typ)
                              return Java.Int;

   function GetDropAction (This : access Typ)
                           return Java.Int;

   function GetTransferable (This : access Typ)
                             return access Java.Awt.Datatransfer.Transferable.Typ'Class;

   procedure AcceptDrag (This : access Typ;
                         P1_Int : Java.Int);

   procedure RejectDrag (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DropTargetDragEvent);
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetCurrentDataFlavors, "getCurrentDataFlavors");
   pragma Import (Java, GetCurrentDataFlavorsAsList, "getCurrentDataFlavorsAsList");
   pragma Import (Java, IsDataFlavorSupported, "isDataFlavorSupported");
   pragma Import (Java, GetSourceActions, "getSourceActions");
   pragma Import (Java, GetDropAction, "getDropAction");
   pragma Import (Java, GetTransferable, "getTransferable");
   pragma Import (Java, AcceptDrag, "acceptDrag");
   pragma Import (Java, RejectDrag, "rejectDrag");

end Java.Awt.Dnd.DropTargetDragEvent;
pragma Import (Java, Java.Awt.Dnd.DropTargetDragEvent, "java.awt.dnd.DropTargetDragEvent");
pragma Extensions_Allowed (Off);
