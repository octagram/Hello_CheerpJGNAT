pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DropTargetContext;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Awt.Dnd.DropTargetEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Context : access Java.Awt.Dnd.DropTargetContext.Typ'Class;
      pragma Import (Java, Context, "context");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DropTargetEvent (P1_DropTargetContext : access Standard.Java.Awt.Dnd.DropTargetContext.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDropTargetContext (This : access Typ)
                                  return access Java.Awt.Dnd.DropTargetContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DropTargetEvent);
   pragma Import (Java, GetDropTargetContext, "getDropTargetContext");

end Java.Awt.Dnd.DropTargetEvent;
pragma Import (Java, Java.Awt.Dnd.DropTargetEvent, "java.awt.dnd.DropTargetEvent");
pragma Extensions_Allowed (Off);
