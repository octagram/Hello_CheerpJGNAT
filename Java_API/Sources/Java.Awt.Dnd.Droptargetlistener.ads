pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DropTargetDragEvent;
limited with Java.Awt.Dnd.DropTargetDropEvent;
limited with Java.Awt.Dnd.DropTargetEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Dnd.DropTargetListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DragEnter (This : access Typ;
                        P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class) is abstract;

   procedure DragOver (This : access Typ;
                       P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class) is abstract;

   procedure DropActionChanged (This : access Typ;
                                P1_DropTargetDragEvent : access Standard.Java.Awt.Dnd.DropTargetDragEvent.Typ'Class) is abstract;

   procedure DragExit (This : access Typ;
                       P1_DropTargetEvent : access Standard.Java.Awt.Dnd.DropTargetEvent.Typ'Class) is abstract;

   procedure Drop (This : access Typ;
                   P1_DropTargetDropEvent : access Standard.Java.Awt.Dnd.DropTargetDropEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, DragEnter, "dragEnter");
   pragma Export (Java, DragOver, "dragOver");
   pragma Export (Java, DropActionChanged, "dropActionChanged");
   pragma Export (Java, DragExit, "dragExit");
   pragma Export (Java, Drop, "drop");

end Java.Awt.Dnd.DropTargetListener;
pragma Import (Java, Java.Awt.Dnd.DropTargetListener, "java.awt.dnd.DropTargetListener");
pragma Extensions_Allowed (Off);
