pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dnd.DragGestureListener;
limited with Java.Awt.Dnd.DragSource;
limited with Java.Awt.Event.MouseEvent;
with Java.Awt.Dnd.DragGestureRecognizer;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Dnd.MouseDragGestureRecognizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Awt.Dnd.DragGestureRecognizer.Typ(Serializable_I)
      with null record;

   --  protected
   function New_MouseDragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class;
                                            P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                                            P3_Int : Java.Int;
                                            P4_DragGestureListener : access Standard.Java.Awt.Dnd.DragGestureListener.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   --  protected
   function New_MouseDragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class;
                                            P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                                            P3_Int : Java.Int; 
                                            This : Ref := null)
                                            return Ref;

   --  protected
   function New_MouseDragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class;
                                            P2_Component : access Standard.Java.Awt.Component.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   --  protected
   function New_MouseDragGestureRecognizer (P1_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure RegisterListeners (This : access Typ);

   --  protected
   procedure UnregisterListeners (This : access Typ);

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MouseDragGestureRecognizer);
   pragma Import (Java, RegisterListeners, "registerListeners");
   pragma Import (Java, UnregisterListeners, "unregisterListeners");
   pragma Import (Java, MouseClicked, "mouseClicked");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MouseEntered, "mouseEntered");
   pragma Import (Java, MouseExited, "mouseExited");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");

end Java.Awt.Dnd.MouseDragGestureRecognizer;
pragma Import (Java, Java.Awt.Dnd.MouseDragGestureRecognizer, "java.awt.dnd.MouseDragGestureRecognizer");
pragma Extensions_Allowed (Off);
