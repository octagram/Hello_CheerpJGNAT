pragma Extensions_Allowed (On);
limited with Java.Awt.Cursor;
limited with Java.Awt.Dnd.DragSourceContext;
limited with Java.Awt.Image;
limited with Java.Awt.Point;
with Java.Lang.Object;

package Java.Awt.Dnd.Peer.DragSourceContextPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure StartDrag (This : access Typ;
                        P1_DragSourceContext : access Standard.Java.Awt.Dnd.DragSourceContext.Typ'Class;
                        P2_Cursor : access Standard.Java.Awt.Cursor.Typ'Class;
                        P3_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P4_Point : access Standard.Java.Awt.Point.Typ'Class) is abstract;
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   function GetCursor (This : access Typ)
                       return access Java.Awt.Cursor.Typ'Class is abstract;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class) is abstract;
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   procedure TransferablesFlavorsChanged (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, StartDrag, "startDrag");
   pragma Export (Java, GetCursor, "getCursor");
   pragma Export (Java, SetCursor, "setCursor");
   pragma Export (Java, TransferablesFlavorsChanged, "transferablesFlavorsChanged");

end Java.Awt.Dnd.Peer.DragSourceContextPeer;
pragma Import (Java, Java.Awt.Dnd.Peer.DragSourceContextPeer, "java.awt.dnd.peer.DragSourceContextPeer");
pragma Extensions_Allowed (Off);
