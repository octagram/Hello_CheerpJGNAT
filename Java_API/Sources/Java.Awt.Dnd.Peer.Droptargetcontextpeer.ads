pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Awt.Dnd.DropTarget;
with Java.Lang.Object;

package Java.Awt.Dnd.Peer.DropTargetContextPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTargetActions (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   function GetTargetActions (This : access Typ)
                              return Java.Int is abstract;

   function GetDropTarget (This : access Typ)
                           return access Java.Awt.Dnd.DropTarget.Typ'Class is abstract;

   function GetTransferDataFlavors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function GetTransferable (This : access Typ)
                             return access Java.Awt.Datatransfer.Transferable.Typ'Class is abstract;
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   function IsTransferableJVMLocal (This : access Typ)
                                    return Java.Boolean is abstract;

   procedure AcceptDrag (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   procedure RejectDrag (This : access Typ) is abstract;

   procedure AcceptDrop (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   procedure RejectDrop (This : access Typ) is abstract;

   procedure DropComplete (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetTargetActions, "setTargetActions");
   pragma Export (Java, GetTargetActions, "getTargetActions");
   pragma Export (Java, GetDropTarget, "getDropTarget");
   pragma Export (Java, GetTransferDataFlavors, "getTransferDataFlavors");
   pragma Export (Java, GetTransferable, "getTransferable");
   pragma Export (Java, IsTransferableJVMLocal, "isTransferableJVMLocal");
   pragma Export (Java, AcceptDrag, "acceptDrag");
   pragma Export (Java, RejectDrag, "rejectDrag");
   pragma Export (Java, AcceptDrop, "acceptDrop");
   pragma Export (Java, RejectDrop, "rejectDrop");
   pragma Export (Java, DropComplete, "dropComplete");

end Java.Awt.Dnd.Peer.DropTargetContextPeer;
pragma Import (Java, Java.Awt.Dnd.Peer.DropTargetContextPeer, "java.awt.dnd.peer.DropTargetContextPeer");
pragma Extensions_Allowed (Off);
