pragma Extensions_Allowed (On);
limited with Java.Awt.Dnd.DropTarget;
with Java.Lang.Object;

package Java.Awt.Dnd.Peer.DropTargetPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddDropTarget (This : access Typ;
                            P1_DropTarget : access Standard.Java.Awt.Dnd.DropTarget.Typ'Class) is abstract;

   procedure RemoveDropTarget (This : access Typ;
                               P1_DropTarget : access Standard.Java.Awt.Dnd.DropTarget.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddDropTarget, "addDropTarget");
   pragma Export (Java, RemoveDropTarget, "removeDropTarget");

end Java.Awt.Dnd.Peer.DropTargetPeer;
pragma Import (Java, Java.Awt.Dnd.Peer.DropTargetPeer, "java.awt.dnd.peer.DropTargetPeer");
pragma Extensions_Allowed (Off);
