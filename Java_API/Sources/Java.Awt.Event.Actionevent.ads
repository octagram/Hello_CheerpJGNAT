pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.ActionEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActionEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int;
                             P3_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_ActionEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_ActionEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_Long : Java.Long;
                             P5_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetWhen (This : access Typ)
                     return Java.Long;

   function GetModifiers (This : access Typ)
                          return Java.Int;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SHIFT_MASK : constant Java.Int;

   --  final
   CTRL_MASK : constant Java.Int;

   --  final
   META_MASK : constant Java.Int;

   --  final
   ALT_MASK : constant Java.Int;

   --  final
   ACTION_FIRST : constant Java.Int;

   --  final
   ACTION_LAST : constant Java.Int;

   --  final
   ACTION_PERFORMED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActionEvent);
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, GetWhen, "getWhen");
   pragma Import (Java, GetModifiers, "getModifiers");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, SHIFT_MASK, "SHIFT_MASK");
   pragma Import (Java, CTRL_MASK, "CTRL_MASK");
   pragma Import (Java, META_MASK, "META_MASK");
   pragma Import (Java, ALT_MASK, "ALT_MASK");
   pragma Import (Java, ACTION_FIRST, "ACTION_FIRST");
   pragma Import (Java, ACTION_LAST, "ACTION_LAST");
   pragma Import (Java, ACTION_PERFORMED, "ACTION_PERFORMED");

end Java.Awt.Event.ActionEvent;
pragma Import (Java, Java.Awt.Event.ActionEvent, "java.awt.event.ActionEvent");
pragma Extensions_Allowed (Off);
