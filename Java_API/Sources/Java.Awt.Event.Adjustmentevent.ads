pragma Extensions_Allowed (On);
limited with Java.Awt.Adjustable;
limited with Java.Lang.String;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.AdjustmentEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AdjustmentEvent (P1_Adjustable : access Standard.Java.Awt.Adjustable.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_AdjustmentEvent (P1_Adjustable : access Standard.Java.Awt.Adjustable.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAdjustable (This : access Typ)
                           return access Java.Awt.Adjustable.Typ'Class;

   function GetValue (This : access Typ)
                      return Java.Int;

   function GetAdjustmentType (This : access Typ)
                               return Java.Int;

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ADJUSTMENT_FIRST : constant Java.Int;

   --  final
   ADJUSTMENT_LAST : constant Java.Int;

   --  final
   ADJUSTMENT_VALUE_CHANGED : constant Java.Int;

   --  final
   UNIT_INCREMENT : constant Java.Int;

   --  final
   UNIT_DECREMENT : constant Java.Int;

   --  final
   BLOCK_DECREMENT : constant Java.Int;

   --  final
   BLOCK_INCREMENT : constant Java.Int;

   --  final
   TRACK : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AdjustmentEvent);
   pragma Import (Java, GetAdjustable, "getAdjustable");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetAdjustmentType, "getAdjustmentType");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, ADJUSTMENT_FIRST, "ADJUSTMENT_FIRST");
   pragma Import (Java, ADJUSTMENT_LAST, "ADJUSTMENT_LAST");
   pragma Import (Java, ADJUSTMENT_VALUE_CHANGED, "ADJUSTMENT_VALUE_CHANGED");
   pragma Import (Java, UNIT_INCREMENT, "UNIT_INCREMENT");
   pragma Import (Java, UNIT_DECREMENT, "UNIT_DECREMENT");
   pragma Import (Java, BLOCK_DECREMENT, "BLOCK_DECREMENT");
   pragma Import (Java, BLOCK_INCREMENT, "BLOCK_INCREMENT");
   pragma Import (Java, TRACK, "TRACK");

end Java.Awt.Event.AdjustmentEvent;
pragma Import (Java, Java.Awt.Event.AdjustmentEvent, "java.awt.event.AdjustmentEvent");
pragma Extensions_Allowed (Off);
