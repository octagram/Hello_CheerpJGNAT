pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
with Java.Awt.Event.AWTEventListener;
with Java.Lang.Object;
with Java.Util.EventListener;
with Java.Util.EventListenerProxy;

package Java.Awt.Event.AWTEventListenerProxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AWTEventListener_I : Java.Awt.Event.AWTEventListener.Ref;
            EventListener_I : Java.Util.EventListener.Ref)
    is new Java.Util.EventListenerProxy.Typ(EventListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AWTEventListenerProxy (P1_Long : Java.Long;
                                       P2_AWTEventListener : access Standard.Java.Awt.Event.AWTEventListener.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure EventDispatched (This : access Typ;
                              P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   function GetEventMask (This : access Typ)
                          return Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AWTEventListenerProxy);
   pragma Import (Java, EventDispatched, "eventDispatched");
   pragma Import (Java, GetEventMask, "getEventMask");

end Java.Awt.Event.AWTEventListenerProxy;
pragma Import (Java, Java.Awt.Event.AWTEventListenerProxy, "java.awt.event.AWTEventListenerProxy");
pragma Extensions_Allowed (Off);
