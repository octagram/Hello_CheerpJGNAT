pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ComponentEvent;
with Java.Awt.Event.ComponentListener;
with Java.Lang.Object;

package Java.Awt.Event.ComponentAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ComponentListener_I : Java.Awt.Event.ComponentListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ComponentAdapter (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ComponentResized (This : access Typ;
                               P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   procedure ComponentMoved (This : access Typ;
                             P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   procedure ComponentShown (This : access Typ;
                             P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   procedure ComponentHidden (This : access Typ;
                              P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComponentAdapter);
   pragma Import (Java, ComponentResized, "componentResized");
   pragma Import (Java, ComponentMoved, "componentMoved");
   pragma Import (Java, ComponentShown, "componentShown");
   pragma Import (Java, ComponentHidden, "componentHidden");

end Java.Awt.Event.ComponentAdapter;
pragma Import (Java, Java.Awt.Event.ComponentAdapter, "java.awt.event.ComponentAdapter");
pragma Extensions_Allowed (Off);
