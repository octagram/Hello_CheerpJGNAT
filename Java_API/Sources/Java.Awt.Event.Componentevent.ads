pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.ComponentEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ComponentEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   COMPONENT_FIRST : constant Java.Int;

   --  final
   COMPONENT_LAST : constant Java.Int;

   --  final
   COMPONENT_MOVED : constant Java.Int;

   --  final
   COMPONENT_RESIZED : constant Java.Int;

   --  final
   COMPONENT_SHOWN : constant Java.Int;

   --  final
   COMPONENT_HIDDEN : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComponentEvent);
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, COMPONENT_FIRST, "COMPONENT_FIRST");
   pragma Import (Java, COMPONENT_LAST, "COMPONENT_LAST");
   pragma Import (Java, COMPONENT_MOVED, "COMPONENT_MOVED");
   pragma Import (Java, COMPONENT_RESIZED, "COMPONENT_RESIZED");
   pragma Import (Java, COMPONENT_SHOWN, "COMPONENT_SHOWN");
   pragma Import (Java, COMPONENT_HIDDEN, "COMPONENT_HIDDEN");

end Java.Awt.Event.ComponentEvent;
pragma Import (Java, Java.Awt.Event.ComponentEvent, "java.awt.event.ComponentEvent");
pragma Extensions_Allowed (Off);
