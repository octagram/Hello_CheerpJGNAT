pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ComponentEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Event.ComponentListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ComponentResized (This : access Typ;
                               P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class) is abstract;

   procedure ComponentMoved (This : access Typ;
                             P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class) is abstract;

   procedure ComponentShown (This : access Typ;
                             P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class) is abstract;

   procedure ComponentHidden (This : access Typ;
                              P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ComponentResized, "componentResized");
   pragma Export (Java, ComponentMoved, "componentMoved");
   pragma Export (Java, ComponentShown, "componentShown");
   pragma Export (Java, ComponentHidden, "componentHidden");

end Java.Awt.Event.ComponentListener;
pragma Import (Java, Java.Awt.Event.ComponentListener, "java.awt.event.ComponentListener");
pragma Extensions_Allowed (Off);
