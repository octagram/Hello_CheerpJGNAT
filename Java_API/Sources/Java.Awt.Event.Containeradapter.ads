pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ContainerEvent;
with Java.Awt.Event.ContainerListener;
with Java.Lang.Object;

package Java.Awt.Event.ContainerAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ContainerListener_I : Java.Awt.Event.ContainerListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ContainerAdapter (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ComponentAdded (This : access Typ;
                             P1_ContainerEvent : access Standard.Java.Awt.Event.ContainerEvent.Typ'Class);

   procedure ComponentRemoved (This : access Typ;
                               P1_ContainerEvent : access Standard.Java.Awt.Event.ContainerEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ContainerAdapter);
   pragma Import (Java, ComponentAdded, "componentAdded");
   pragma Import (Java, ComponentRemoved, "componentRemoved");

end Java.Awt.Event.ContainerAdapter;
pragma Import (Java, Java.Awt.Event.ContainerAdapter, "java.awt.event.ContainerAdapter");
pragma Extensions_Allowed (Off);
