pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Lang.String;
with Java.Awt.Event.ComponentEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.ContainerEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.ComponentEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ContainerEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Component : access Standard.Java.Awt.Component.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContainer (This : access Typ)
                          return access Java.Awt.Container.Typ'Class;

   function GetChild (This : access Typ)
                      return access Java.Awt.Component.Typ'Class;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CONTAINER_FIRST : constant Java.Int;

   --  final
   CONTAINER_LAST : constant Java.Int;

   --  final
   COMPONENT_ADDED : constant Java.Int;

   --  final
   COMPONENT_REMOVED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ContainerEvent);
   pragma Import (Java, GetContainer, "getContainer");
   pragma Import (Java, GetChild, "getChild");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, CONTAINER_FIRST, "CONTAINER_FIRST");
   pragma Import (Java, CONTAINER_LAST, "CONTAINER_LAST");
   pragma Import (Java, COMPONENT_ADDED, "COMPONENT_ADDED");
   pragma Import (Java, COMPONENT_REMOVED, "COMPONENT_REMOVED");

end Java.Awt.Event.ContainerEvent;
pragma Import (Java, Java.Awt.Event.ContainerEvent, "java.awt.event.ContainerEvent");
pragma Extensions_Allowed (Off);
