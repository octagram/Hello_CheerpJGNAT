pragma Extensions_Allowed (On);
limited with Java.Awt.Event.FocusEvent;
with Java.Awt.Event.FocusListener;
with Java.Lang.Object;

package Java.Awt.Event.FocusAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(FocusListener_I : Java.Awt.Event.FocusListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FocusAdapter (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure FocusGained (This : access Typ;
                          P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure FocusLost (This : access Typ;
                        P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FocusAdapter);
   pragma Import (Java, FocusGained, "focusGained");
   pragma Import (Java, FocusLost, "focusLost");

end Java.Awt.Event.FocusAdapter;
pragma Import (Java, Java.Awt.Event.FocusAdapter, "java.awt.event.FocusAdapter");
pragma Extensions_Allowed (Off);
