pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
with Java.Awt.Event.ComponentEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.FocusEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.ComponentEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FocusEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Boolean : Java.Boolean;
                            P4_Component : access Standard.Java.Awt.Component.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_FocusEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;

   function New_FocusEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsTemporary (This : access Typ)
                         return Java.Boolean;

   function GetOppositeComponent (This : access Typ)
                                  return access Java.Awt.Component.Typ'Class;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FOCUS_FIRST : constant Java.Int;

   --  final
   FOCUS_LAST : constant Java.Int;

   --  final
   FOCUS_GAINED : constant Java.Int;

   --  final
   FOCUS_LOST : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FocusEvent);
   pragma Import (Java, IsTemporary, "isTemporary");
   pragma Import (Java, GetOppositeComponent, "getOppositeComponent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, FOCUS_FIRST, "FOCUS_FIRST");
   pragma Import (Java, FOCUS_LAST, "FOCUS_LAST");
   pragma Import (Java, FOCUS_GAINED, "FOCUS_GAINED");
   pragma Import (Java, FOCUS_LOST, "FOCUS_LOST");

end Java.Awt.Event.FocusEvent;
pragma Import (Java, Java.Awt.Event.FocusEvent, "java.awt.event.FocusEvent");
pragma Extensions_Allowed (Off);
