pragma Extensions_Allowed (On);
limited with Java.Awt.Event.FocusEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Event.FocusListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure FocusGained (This : access Typ;
                          P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class) is abstract;

   procedure FocusLost (This : access Typ;
                        P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, FocusGained, "focusGained");
   pragma Export (Java, FocusLost, "focusLost");

end Java.Awt.Event.FocusListener;
pragma Import (Java, Java.Awt.Event.FocusListener, "java.awt.event.FocusListener");
pragma Extensions_Allowed (Off);
