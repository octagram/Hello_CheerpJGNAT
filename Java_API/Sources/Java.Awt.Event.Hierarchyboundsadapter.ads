pragma Extensions_Allowed (On);
limited with Java.Awt.Event.HierarchyEvent;
with Java.Awt.Event.HierarchyBoundsListener;
with Java.Lang.Object;

package Java.Awt.Event.HierarchyBoundsAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(HierarchyBoundsListener_I : Java.Awt.Event.HierarchyBoundsListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_HierarchyBoundsAdapter (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AncestorMoved (This : access Typ;
                            P1_HierarchyEvent : access Standard.Java.Awt.Event.HierarchyEvent.Typ'Class);

   procedure AncestorResized (This : access Typ;
                              P1_HierarchyEvent : access Standard.Java.Awt.Event.HierarchyEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HierarchyBoundsAdapter);
   pragma Import (Java, AncestorMoved, "ancestorMoved");
   pragma Import (Java, AncestorResized, "ancestorResized");

end Java.Awt.Event.HierarchyBoundsAdapter;
pragma Import (Java, Java.Awt.Event.HierarchyBoundsAdapter, "java.awt.event.HierarchyBoundsAdapter");
pragma Extensions_Allowed (Off);
