pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Lang.String;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.HierarchyEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HierarchyEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P4_Container : access Standard.Java.Awt.Container.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_HierarchyEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P4_Container : access Standard.Java.Awt.Container.Typ'Class;
                                P5_Long : Java.Long; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   function GetChanged (This : access Typ)
                        return access Java.Awt.Component.Typ'Class;

   function GetChangedParent (This : access Typ)
                              return access Java.Awt.Container.Typ'Class;

   function GetChangeFlags (This : access Typ)
                            return Java.Long;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   HIERARCHY_FIRST : constant Java.Int;

   --  final
   HIERARCHY_CHANGED : constant Java.Int;

   --  final
   ANCESTOR_MOVED : constant Java.Int;

   --  final
   ANCESTOR_RESIZED : constant Java.Int;

   --  final
   HIERARCHY_LAST : constant Java.Int;

   --  final
   PARENT_CHANGED : constant Java.Int;

   --  final
   DISPLAYABILITY_CHANGED : constant Java.Int;

   --  final
   SHOWING_CHANGED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HierarchyEvent);
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, GetChanged, "getChanged");
   pragma Import (Java, GetChangedParent, "getChangedParent");
   pragma Import (Java, GetChangeFlags, "getChangeFlags");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, HIERARCHY_FIRST, "HIERARCHY_FIRST");
   pragma Import (Java, HIERARCHY_CHANGED, "HIERARCHY_CHANGED");
   pragma Import (Java, ANCESTOR_MOVED, "ANCESTOR_MOVED");
   pragma Import (Java, ANCESTOR_RESIZED, "ANCESTOR_RESIZED");
   pragma Import (Java, HIERARCHY_LAST, "HIERARCHY_LAST");
   pragma Import (Java, PARENT_CHANGED, "PARENT_CHANGED");
   pragma Import (Java, DISPLAYABILITY_CHANGED, "DISPLAYABILITY_CHANGED");
   pragma Import (Java, SHOWING_CHANGED, "SHOWING_CHANGED");

end Java.Awt.Event.HierarchyEvent;
pragma Import (Java, Java.Awt.Event.HierarchyEvent, "java.awt.event.HierarchyEvent");
pragma Extensions_Allowed (Off);
