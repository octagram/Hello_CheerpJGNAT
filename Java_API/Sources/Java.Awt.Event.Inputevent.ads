pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Event.ComponentEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.InputEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Awt.Event.ComponentEvent.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsShiftDown (This : access Typ)
                         return Java.Boolean;

   function IsControlDown (This : access Typ)
                           return Java.Boolean;

   function IsMetaDown (This : access Typ)
                        return Java.Boolean;

   function IsAltDown (This : access Typ)
                       return Java.Boolean;

   function IsAltGraphDown (This : access Typ)
                            return Java.Boolean;

   function GetWhen (This : access Typ)
                     return Java.Long;

   function GetModifiers (This : access Typ)
                          return Java.Int;

   function GetModifiersEx (This : access Typ)
                            return Java.Int;

   procedure Consume (This : access Typ);

   function IsConsumed (This : access Typ)
                        return Java.Boolean;

   function GetModifiersExText (P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SHIFT_MASK : constant Java.Int;

   --  final
   CTRL_MASK : constant Java.Int;

   --  final
   META_MASK : constant Java.Int;

   --  final
   ALT_MASK : constant Java.Int;

   --  final
   ALT_GRAPH_MASK : constant Java.Int;

   --  final
   BUTTON1_MASK : constant Java.Int;

   --  final
   BUTTON2_MASK : constant Java.Int;

   --  final
   BUTTON3_MASK : constant Java.Int;

   --  final
   SHIFT_DOWN_MASK : constant Java.Int;

   --  final
   CTRL_DOWN_MASK : constant Java.Int;

   --  final
   META_DOWN_MASK : constant Java.Int;

   --  final
   ALT_DOWN_MASK : constant Java.Int;

   --  final
   BUTTON1_DOWN_MASK : constant Java.Int;

   --  final
   BUTTON2_DOWN_MASK : constant Java.Int;

   --  final
   BUTTON3_DOWN_MASK : constant Java.Int;

   --  final
   ALT_GRAPH_DOWN_MASK : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, IsShiftDown, "isShiftDown");
   pragma Import (Java, IsControlDown, "isControlDown");
   pragma Import (Java, IsMetaDown, "isMetaDown");
   pragma Import (Java, IsAltDown, "isAltDown");
   pragma Import (Java, IsAltGraphDown, "isAltGraphDown");
   pragma Import (Java, GetWhen, "getWhen");
   pragma Import (Java, GetModifiers, "getModifiers");
   pragma Import (Java, GetModifiersEx, "getModifiersEx");
   pragma Import (Java, Consume, "consume");
   pragma Import (Java, IsConsumed, "isConsumed");
   pragma Import (Java, GetModifiersExText, "getModifiersExText");
   pragma Import (Java, SHIFT_MASK, "SHIFT_MASK");
   pragma Import (Java, CTRL_MASK, "CTRL_MASK");
   pragma Import (Java, META_MASK, "META_MASK");
   pragma Import (Java, ALT_MASK, "ALT_MASK");
   pragma Import (Java, ALT_GRAPH_MASK, "ALT_GRAPH_MASK");
   pragma Import (Java, BUTTON1_MASK, "BUTTON1_MASK");
   pragma Import (Java, BUTTON2_MASK, "BUTTON2_MASK");
   pragma Import (Java, BUTTON3_MASK, "BUTTON3_MASK");
   pragma Import (Java, SHIFT_DOWN_MASK, "SHIFT_DOWN_MASK");
   pragma Import (Java, CTRL_DOWN_MASK, "CTRL_DOWN_MASK");
   pragma Import (Java, META_DOWN_MASK, "META_DOWN_MASK");
   pragma Import (Java, ALT_DOWN_MASK, "ALT_DOWN_MASK");
   pragma Import (Java, BUTTON1_DOWN_MASK, "BUTTON1_DOWN_MASK");
   pragma Import (Java, BUTTON2_DOWN_MASK, "BUTTON2_DOWN_MASK");
   pragma Import (Java, BUTTON3_DOWN_MASK, "BUTTON3_DOWN_MASK");
   pragma Import (Java, ALT_GRAPH_DOWN_MASK, "ALT_GRAPH_DOWN_MASK");

end Java.Awt.Event.InputEvent;
pragma Import (Java, Java.Awt.Event.InputEvent, "java.awt.event.InputEvent");
pragma Extensions_Allowed (Off);
