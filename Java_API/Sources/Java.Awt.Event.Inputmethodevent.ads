pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Font.TextHitInfo;
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.InputMethodEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InputMethodEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Long : Java.Long;
                                  P4_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                                  P5_Int : Java.Int;
                                  P6_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                  P7_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_InputMethodEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                                  P4_Int : Java.Int;
                                  P5_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                  P6_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_InputMethodEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                  P4_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetText (This : access Typ)
                     return access Java.Text.AttributedCharacterIterator.Typ'Class;

   function GetCommittedCharacterCount (This : access Typ)
                                        return Java.Int;

   function GetCaret (This : access Typ)
                      return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetVisiblePosition (This : access Typ)
                                return access Java.Awt.Font.TextHitInfo.Typ'Class;

   procedure Consume (This : access Typ);

   function IsConsumed (This : access Typ)
                        return Java.Boolean;

   function GetWhen (This : access Typ)
                     return Java.Long;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INPUT_METHOD_FIRST : constant Java.Int;

   --  final
   INPUT_METHOD_TEXT_CHANGED : constant Java.Int;

   --  final
   CARET_POSITION_CHANGED : constant Java.Int;

   --  final
   INPUT_METHOD_LAST : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputMethodEvent);
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, GetCommittedCharacterCount, "getCommittedCharacterCount");
   pragma Import (Java, GetCaret, "getCaret");
   pragma Import (Java, GetVisiblePosition, "getVisiblePosition");
   pragma Import (Java, Consume, "consume");
   pragma Import (Java, IsConsumed, "isConsumed");
   pragma Import (Java, GetWhen, "getWhen");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, INPUT_METHOD_FIRST, "INPUT_METHOD_FIRST");
   pragma Import (Java, INPUT_METHOD_TEXT_CHANGED, "INPUT_METHOD_TEXT_CHANGED");
   pragma Import (Java, CARET_POSITION_CHANGED, "CARET_POSITION_CHANGED");
   pragma Import (Java, INPUT_METHOD_LAST, "INPUT_METHOD_LAST");

end Java.Awt.Event.InputMethodEvent;
pragma Import (Java, Java.Awt.Event.InputMethodEvent, "java.awt.event.InputMethodEvent");
pragma Extensions_Allowed (Off);
