pragma Extensions_Allowed (On);
limited with Java.Awt.Event.InputMethodEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Event.InputMethodListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InputMethodTextChanged (This : access Typ;
                                     P1_InputMethodEvent : access Standard.Java.Awt.Event.InputMethodEvent.Typ'Class) is abstract;

   procedure CaretPositionChanged (This : access Typ;
                                   P1_InputMethodEvent : access Standard.Java.Awt.Event.InputMethodEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, InputMethodTextChanged, "inputMethodTextChanged");
   pragma Export (Java, CaretPositionChanged, "caretPositionChanged");

end Java.Awt.Event.InputMethodListener;
pragma Import (Java, Java.Awt.Event.InputMethodListener, "java.awt.event.InputMethodListener");
pragma Extensions_Allowed (Off);
