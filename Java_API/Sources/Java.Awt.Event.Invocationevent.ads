pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
limited with Java.Lang.Runnable;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Awt.AWTEvent;
with Java.Awt.ActiveEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.InvocationEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ActiveEvent_I : Java.Awt.ActiveEvent.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Runnable : access Java.Lang.Runnable.Typ'Class;
      pragma Import (Java, Runnable, "runnable");

      --  protected
      Notifier : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Notifier, "notifier");

      --  protected
      CatchExceptions : Java.Boolean;
      pragma Import (Java, CatchExceptions, "catchExceptions");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InvocationEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P2_Runnable : access Standard.Java.Lang.Runnable.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_InvocationEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P2_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P4_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   --  protected
   function New_InvocationEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                                 P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P5_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Dispatch (This : access Typ);

   function GetException (This : access Typ)
                          return access Java.Lang.Exception_K.Typ'Class;

   function GetThrowable (This : access Typ)
                          return access Java.Lang.Throwable.Typ'Class;

   function GetWhen (This : access Typ)
                     return Java.Long;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INVOCATION_FIRST : constant Java.Int;

   --  final
   INVOCATION_DEFAULT : constant Java.Int;

   --  final
   INVOCATION_LAST : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InvocationEvent);
   pragma Import (Java, Dispatch, "dispatch");
   pragma Import (Java, GetException, "getException");
   pragma Import (Java, GetThrowable, "getThrowable");
   pragma Import (Java, GetWhen, "getWhen");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, INVOCATION_FIRST, "INVOCATION_FIRST");
   pragma Import (Java, INVOCATION_DEFAULT, "INVOCATION_DEFAULT");
   pragma Import (Java, INVOCATION_LAST, "INVOCATION_LAST");

end Java.Awt.Event.InvocationEvent;
pragma Import (Java, Java.Awt.Event.InvocationEvent, "java.awt.event.InvocationEvent");
pragma Extensions_Allowed (Off);
