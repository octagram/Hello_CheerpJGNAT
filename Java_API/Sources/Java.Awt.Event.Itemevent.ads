pragma Extensions_Allowed (On);
limited with Java.Awt.ItemSelectable;
limited with Java.Lang.String;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.ItemEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ItemEvent (P1_ItemSelectable : access Standard.Java.Awt.ItemSelectable.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P4_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetItemSelectable (This : access Typ)
                               return access Java.Awt.ItemSelectable.Typ'Class;

   function GetItem (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function GetStateChange (This : access Typ)
                            return Java.Int;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ITEM_FIRST : constant Java.Int;

   --  final
   ITEM_LAST : constant Java.Int;

   --  final
   ITEM_STATE_CHANGED : constant Java.Int;

   --  final
   SELECTED : constant Java.Int;

   --  final
   DESELECTED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ItemEvent);
   pragma Import (Java, GetItemSelectable, "getItemSelectable");
   pragma Import (Java, GetItem, "getItem");
   pragma Import (Java, GetStateChange, "getStateChange");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, ITEM_FIRST, "ITEM_FIRST");
   pragma Import (Java, ITEM_LAST, "ITEM_LAST");
   pragma Import (Java, ITEM_STATE_CHANGED, "ITEM_STATE_CHANGED");
   pragma Import (Java, SELECTED, "SELECTED");
   pragma Import (Java, DESELECTED, "DESELECTED");

end Java.Awt.Event.ItemEvent;
pragma Import (Java, Java.Awt.Event.ItemEvent, "java.awt.event.ItemEvent");
pragma Extensions_Allowed (Off);
