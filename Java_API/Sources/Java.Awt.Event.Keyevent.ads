pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
with Java.Awt.Event.InputEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.KeyEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.InputEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KeyEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Long : Java.Long;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Char : Java.Char;
                          P7_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   function New_KeyEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Long : Java.Long;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Char : Java.Char; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKeyCode (This : access Typ)
                        return Java.Int;

   procedure SetKeyCode (This : access Typ;
                         P1_Int : Java.Int);

   function GetKeyChar (This : access Typ)
                        return Java.Char;

   procedure SetKeyChar (This : access Typ;
                         P1_Char : Java.Char);

   function GetKeyLocation (This : access Typ)
                            return Java.Int;

   function GetKeyText (P1_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class;

   function GetKeyModifiersText (P1_Int : Java.Int)
                                 return access Java.Lang.String.Typ'Class;

   function IsActionKey (This : access Typ)
                         return Java.Boolean;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   KEY_FIRST : constant Java.Int;

   --  final
   KEY_LAST : constant Java.Int;

   --  final
   KEY_TYPED : constant Java.Int;

   --  final
   KEY_PRESSED : constant Java.Int;

   --  final
   KEY_RELEASED : constant Java.Int;

   --  final
   VK_ENTER : constant Java.Int;

   --  final
   VK_BACK_SPACE : constant Java.Int;

   --  final
   VK_TAB : constant Java.Int;

   --  final
   VK_CANCEL : constant Java.Int;

   --  final
   VK_CLEAR : constant Java.Int;

   --  final
   VK_SHIFT : constant Java.Int;

   --  final
   VK_CONTROL : constant Java.Int;

   --  final
   VK_ALT : constant Java.Int;

   --  final
   VK_PAUSE : constant Java.Int;

   --  final
   VK_CAPS_LOCK : constant Java.Int;

   --  final
   VK_ESCAPE : constant Java.Int;

   --  final
   VK_SPACE : constant Java.Int;

   --  final
   VK_PAGE_UP : constant Java.Int;

   --  final
   VK_PAGE_DOWN : constant Java.Int;

   --  final
   VK_END : constant Java.Int;

   --  final
   VK_HOME : constant Java.Int;

   --  final
   VK_LEFT : constant Java.Int;

   --  final
   VK_UP : constant Java.Int;

   --  final
   VK_RIGHT : constant Java.Int;

   --  final
   VK_DOWN : constant Java.Int;

   --  final
   VK_COMMA : constant Java.Int;

   --  final
   VK_MINUS : constant Java.Int;

   --  final
   VK_PERIOD : constant Java.Int;

   --  final
   VK_SLASH : constant Java.Int;

   --  final
   VK_0 : constant Java.Int;

   --  final
   VK_1 : constant Java.Int;

   --  final
   VK_2 : constant Java.Int;

   --  final
   VK_3 : constant Java.Int;

   --  final
   VK_4 : constant Java.Int;

   --  final
   VK_5 : constant Java.Int;

   --  final
   VK_6 : constant Java.Int;

   --  final
   VK_7 : constant Java.Int;

   --  final
   VK_8 : constant Java.Int;

   --  final
   VK_9 : constant Java.Int;

   --  final
   VK_SEMICOLON : constant Java.Int;

   --  final
   VK_EQUALS : constant Java.Int;

   --  final
   VK_A : constant Java.Int;

   --  final
   VK_B : constant Java.Int;

   --  final
   VK_C : constant Java.Int;

   --  final
   VK_D : constant Java.Int;

   --  final
   VK_E : constant Java.Int;

   --  final
   VK_F : constant Java.Int;

   --  final
   VK_G : constant Java.Int;

   --  final
   VK_H : constant Java.Int;

   --  final
   VK_I : constant Java.Int;

   --  final
   VK_J : constant Java.Int;

   --  final
   VK_K : constant Java.Int;

   --  final
   VK_L : constant Java.Int;

   --  final
   VK_M : constant Java.Int;

   --  final
   VK_N : constant Java.Int;

   --  final
   VK_O : constant Java.Int;

   --  final
   VK_P : constant Java.Int;

   --  final
   VK_Q : constant Java.Int;

   --  final
   VK_R : constant Java.Int;

   --  final
   VK_S : constant Java.Int;

   --  final
   VK_T : constant Java.Int;

   --  final
   VK_U : constant Java.Int;

   --  final
   VK_V : constant Java.Int;

   --  final
   VK_W : constant Java.Int;

   --  final
   VK_X : constant Java.Int;

   --  final
   VK_Y : constant Java.Int;

   --  final
   VK_Z : constant Java.Int;

   --  final
   VK_OPEN_BRACKET : constant Java.Int;

   --  final
   VK_BACK_SLASH : constant Java.Int;

   --  final
   VK_CLOSE_BRACKET : constant Java.Int;

   --  final
   VK_NUMPAD0 : constant Java.Int;

   --  final
   VK_NUMPAD1 : constant Java.Int;

   --  final
   VK_NUMPAD2 : constant Java.Int;

   --  final
   VK_NUMPAD3 : constant Java.Int;

   --  final
   VK_NUMPAD4 : constant Java.Int;

   --  final
   VK_NUMPAD5 : constant Java.Int;

   --  final
   VK_NUMPAD6 : constant Java.Int;

   --  final
   VK_NUMPAD7 : constant Java.Int;

   --  final
   VK_NUMPAD8 : constant Java.Int;

   --  final
   VK_NUMPAD9 : constant Java.Int;

   --  final
   VK_MULTIPLY : constant Java.Int;

   --  final
   VK_ADD : constant Java.Int;

   --  final
   VK_SEPARATER : constant Java.Int;

   --  final
   VK_SEPARATOR : constant Java.Int;

   --  final
   VK_SUBTRACT : constant Java.Int;

   --  final
   VK_DECIMAL : constant Java.Int;

   --  final
   VK_DIVIDE : constant Java.Int;

   --  final
   VK_DELETE : constant Java.Int;

   --  final
   VK_NUM_LOCK : constant Java.Int;

   --  final
   VK_SCROLL_LOCK : constant Java.Int;

   --  final
   VK_F1 : constant Java.Int;

   --  final
   VK_F2 : constant Java.Int;

   --  final
   VK_F3 : constant Java.Int;

   --  final
   VK_F4 : constant Java.Int;

   --  final
   VK_F5 : constant Java.Int;

   --  final
   VK_F6 : constant Java.Int;

   --  final
   VK_F7 : constant Java.Int;

   --  final
   VK_F8 : constant Java.Int;

   --  final
   VK_F9 : constant Java.Int;

   --  final
   VK_F10 : constant Java.Int;

   --  final
   VK_F11 : constant Java.Int;

   --  final
   VK_F12 : constant Java.Int;

   --  final
   VK_F13 : constant Java.Int;

   --  final
   VK_F14 : constant Java.Int;

   --  final
   VK_F15 : constant Java.Int;

   --  final
   VK_F16 : constant Java.Int;

   --  final
   VK_F17 : constant Java.Int;

   --  final
   VK_F18 : constant Java.Int;

   --  final
   VK_F19 : constant Java.Int;

   --  final
   VK_F20 : constant Java.Int;

   --  final
   VK_F21 : constant Java.Int;

   --  final
   VK_F22 : constant Java.Int;

   --  final
   VK_F23 : constant Java.Int;

   --  final
   VK_F24 : constant Java.Int;

   --  final
   VK_PRINTSCREEN : constant Java.Int;

   --  final
   VK_INSERT : constant Java.Int;

   --  final
   VK_HELP : constant Java.Int;

   --  final
   VK_META : constant Java.Int;

   --  final
   VK_BACK_QUOTE : constant Java.Int;

   --  final
   VK_QUOTE : constant Java.Int;

   --  final
   VK_KP_UP : constant Java.Int;

   --  final
   VK_KP_DOWN : constant Java.Int;

   --  final
   VK_KP_LEFT : constant Java.Int;

   --  final
   VK_KP_RIGHT : constant Java.Int;

   --  final
   VK_DEAD_GRAVE : constant Java.Int;

   --  final
   VK_DEAD_ACUTE : constant Java.Int;

   --  final
   VK_DEAD_CIRCUMFLEX : constant Java.Int;

   --  final
   VK_DEAD_TILDE : constant Java.Int;

   --  final
   VK_DEAD_MACRON : constant Java.Int;

   --  final
   VK_DEAD_BREVE : constant Java.Int;

   --  final
   VK_DEAD_ABOVEDOT : constant Java.Int;

   --  final
   VK_DEAD_DIAERESIS : constant Java.Int;

   --  final
   VK_DEAD_ABOVERING : constant Java.Int;

   --  final
   VK_DEAD_DOUBLEACUTE : constant Java.Int;

   --  final
   VK_DEAD_CARON : constant Java.Int;

   --  final
   VK_DEAD_CEDILLA : constant Java.Int;

   --  final
   VK_DEAD_OGONEK : constant Java.Int;

   --  final
   VK_DEAD_IOTA : constant Java.Int;

   --  final
   VK_DEAD_VOICED_SOUND : constant Java.Int;

   --  final
   VK_DEAD_SEMIVOICED_SOUND : constant Java.Int;

   --  final
   VK_AMPERSAND : constant Java.Int;

   --  final
   VK_ASTERISK : constant Java.Int;

   --  final
   VK_QUOTEDBL : constant Java.Int;

   --  final
   VK_LESS : constant Java.Int;

   --  final
   VK_GREATER : constant Java.Int;

   --  final
   VK_BRACELEFT : constant Java.Int;

   --  final
   VK_BRACERIGHT : constant Java.Int;

   --  final
   VK_AT : constant Java.Int;

   --  final
   VK_COLON : constant Java.Int;

   --  final
   VK_CIRCUMFLEX : constant Java.Int;

   --  final
   VK_DOLLAR : constant Java.Int;

   --  final
   VK_EURO_SIGN : constant Java.Int;

   --  final
   VK_EXCLAMATION_MARK : constant Java.Int;

   --  final
   VK_INVERTED_EXCLAMATION_MARK : constant Java.Int;

   --  final
   VK_LEFT_PARENTHESIS : constant Java.Int;

   --  final
   VK_NUMBER_SIGN : constant Java.Int;

   --  final
   VK_PLUS : constant Java.Int;

   --  final
   VK_RIGHT_PARENTHESIS : constant Java.Int;

   --  final
   VK_UNDERSCORE : constant Java.Int;

   --  final
   VK_WINDOWS : constant Java.Int;

   --  final
   VK_CONTEXT_MENU : constant Java.Int;

   --  final
   VK_FINAL : constant Java.Int;

   --  final
   VK_CONVERT : constant Java.Int;

   --  final
   VK_NONCONVERT : constant Java.Int;

   --  final
   VK_ACCEPT : constant Java.Int;

   --  final
   VK_MODECHANGE : constant Java.Int;

   --  final
   VK_KANA : constant Java.Int;

   --  final
   VK_KANJI : constant Java.Int;

   --  final
   VK_ALPHANUMERIC : constant Java.Int;

   --  final
   VK_KATAKANA : constant Java.Int;

   --  final
   VK_HIRAGANA : constant Java.Int;

   --  final
   VK_FULL_WIDTH : constant Java.Int;

   --  final
   VK_HALF_WIDTH : constant Java.Int;

   --  final
   VK_ROMAN_CHARACTERS : constant Java.Int;

   --  final
   VK_ALL_CANDIDATES : constant Java.Int;

   --  final
   VK_PREVIOUS_CANDIDATE : constant Java.Int;

   --  final
   VK_CODE_INPUT : constant Java.Int;

   --  final
   VK_JAPANESE_KATAKANA : constant Java.Int;

   --  final
   VK_JAPANESE_HIRAGANA : constant Java.Int;

   --  final
   VK_JAPANESE_ROMAN : constant Java.Int;

   --  final
   VK_KANA_LOCK : constant Java.Int;

   --  final
   VK_INPUT_METHOD_ON_OFF : constant Java.Int;

   --  final
   VK_CUT : constant Java.Int;

   --  final
   VK_COPY : constant Java.Int;

   --  final
   VK_PASTE : constant Java.Int;

   --  final
   VK_UNDO : constant Java.Int;

   --  final
   VK_AGAIN : constant Java.Int;

   --  final
   VK_FIND : constant Java.Int;

   --  final
   VK_PROPS : constant Java.Int;

   --  final
   VK_STOP : constant Java.Int;

   --  final
   VK_COMPOSE : constant Java.Int;

   --  final
   VK_ALT_GRAPH : constant Java.Int;

   --  final
   VK_BEGIN : constant Java.Int;

   --  final
   VK_UNDEFINED : constant Java.Int;

   --  final
   CHAR_UNDEFINED : constant Java.Char;

   --  final
   KEY_LOCATION_UNKNOWN : constant Java.Int;

   --  final
   KEY_LOCATION_STANDARD : constant Java.Int;

   --  final
   KEY_LOCATION_LEFT : constant Java.Int;

   --  final
   KEY_LOCATION_RIGHT : constant Java.Int;

   --  final
   KEY_LOCATION_NUMPAD : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyEvent);
   pragma Import (Java, GetKeyCode, "getKeyCode");
   pragma Import (Java, SetKeyCode, "setKeyCode");
   pragma Import (Java, GetKeyChar, "getKeyChar");
   pragma Import (Java, SetKeyChar, "setKeyChar");
   pragma Import (Java, GetKeyLocation, "getKeyLocation");
   pragma Import (Java, GetKeyText, "getKeyText");
   pragma Import (Java, GetKeyModifiersText, "getKeyModifiersText");
   pragma Import (Java, IsActionKey, "isActionKey");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, KEY_FIRST, "KEY_FIRST");
   pragma Import (Java, KEY_LAST, "KEY_LAST");
   pragma Import (Java, KEY_TYPED, "KEY_TYPED");
   pragma Import (Java, KEY_PRESSED, "KEY_PRESSED");
   pragma Import (Java, KEY_RELEASED, "KEY_RELEASED");
   pragma Import (Java, VK_ENTER, "VK_ENTER");
   pragma Import (Java, VK_BACK_SPACE, "VK_BACK_SPACE");
   pragma Import (Java, VK_TAB, "VK_TAB");
   pragma Import (Java, VK_CANCEL, "VK_CANCEL");
   pragma Import (Java, VK_CLEAR, "VK_CLEAR");
   pragma Import (Java, VK_SHIFT, "VK_SHIFT");
   pragma Import (Java, VK_CONTROL, "VK_CONTROL");
   pragma Import (Java, VK_ALT, "VK_ALT");
   pragma Import (Java, VK_PAUSE, "VK_PAUSE");
   pragma Import (Java, VK_CAPS_LOCK, "VK_CAPS_LOCK");
   pragma Import (Java, VK_ESCAPE, "VK_ESCAPE");
   pragma Import (Java, VK_SPACE, "VK_SPACE");
   pragma Import (Java, VK_PAGE_UP, "VK_PAGE_UP");
   pragma Import (Java, VK_PAGE_DOWN, "VK_PAGE_DOWN");
   pragma Import (Java, VK_END, "VK_END");
   pragma Import (Java, VK_HOME, "VK_HOME");
   pragma Import (Java, VK_LEFT, "VK_LEFT");
   pragma Import (Java, VK_UP, "VK_UP");
   pragma Import (Java, VK_RIGHT, "VK_RIGHT");
   pragma Import (Java, VK_DOWN, "VK_DOWN");
   pragma Import (Java, VK_COMMA, "VK_COMMA");
   pragma Import (Java, VK_MINUS, "VK_MINUS");
   pragma Import (Java, VK_PERIOD, "VK_PERIOD");
   pragma Import (Java, VK_SLASH, "VK_SLASH");
   pragma Import (Java, VK_0, "VK_0");
   pragma Import (Java, VK_1, "VK_1");
   pragma Import (Java, VK_2, "VK_2");
   pragma Import (Java, VK_3, "VK_3");
   pragma Import (Java, VK_4, "VK_4");
   pragma Import (Java, VK_5, "VK_5");
   pragma Import (Java, VK_6, "VK_6");
   pragma Import (Java, VK_7, "VK_7");
   pragma Import (Java, VK_8, "VK_8");
   pragma Import (Java, VK_9, "VK_9");
   pragma Import (Java, VK_SEMICOLON, "VK_SEMICOLON");
   pragma Import (Java, VK_EQUALS, "VK_EQUALS");
   pragma Import (Java, VK_A, "VK_A");
   pragma Import (Java, VK_B, "VK_B");
   pragma Import (Java, VK_C, "VK_C");
   pragma Import (Java, VK_D, "VK_D");
   pragma Import (Java, VK_E, "VK_E");
   pragma Import (Java, VK_F, "VK_F");
   pragma Import (Java, VK_G, "VK_G");
   pragma Import (Java, VK_H, "VK_H");
   pragma Import (Java, VK_I, "VK_I");
   pragma Import (Java, VK_J, "VK_J");
   pragma Import (Java, VK_K, "VK_K");
   pragma Import (Java, VK_L, "VK_L");
   pragma Import (Java, VK_M, "VK_M");
   pragma Import (Java, VK_N, "VK_N");
   pragma Import (Java, VK_O, "VK_O");
   pragma Import (Java, VK_P, "VK_P");
   pragma Import (Java, VK_Q, "VK_Q");
   pragma Import (Java, VK_R, "VK_R");
   pragma Import (Java, VK_S, "VK_S");
   pragma Import (Java, VK_T, "VK_T");
   pragma Import (Java, VK_U, "VK_U");
   pragma Import (Java, VK_V, "VK_V");
   pragma Import (Java, VK_W, "VK_W");
   pragma Import (Java, VK_X, "VK_X");
   pragma Import (Java, VK_Y, "VK_Y");
   pragma Import (Java, VK_Z, "VK_Z");
   pragma Import (Java, VK_OPEN_BRACKET, "VK_OPEN_BRACKET");
   pragma Import (Java, VK_BACK_SLASH, "VK_BACK_SLASH");
   pragma Import (Java, VK_CLOSE_BRACKET, "VK_CLOSE_BRACKET");
   pragma Import (Java, VK_NUMPAD0, "VK_NUMPAD0");
   pragma Import (Java, VK_NUMPAD1, "VK_NUMPAD1");
   pragma Import (Java, VK_NUMPAD2, "VK_NUMPAD2");
   pragma Import (Java, VK_NUMPAD3, "VK_NUMPAD3");
   pragma Import (Java, VK_NUMPAD4, "VK_NUMPAD4");
   pragma Import (Java, VK_NUMPAD5, "VK_NUMPAD5");
   pragma Import (Java, VK_NUMPAD6, "VK_NUMPAD6");
   pragma Import (Java, VK_NUMPAD7, "VK_NUMPAD7");
   pragma Import (Java, VK_NUMPAD8, "VK_NUMPAD8");
   pragma Import (Java, VK_NUMPAD9, "VK_NUMPAD9");
   pragma Import (Java, VK_MULTIPLY, "VK_MULTIPLY");
   pragma Import (Java, VK_ADD, "VK_ADD");
   pragma Import (Java, VK_SEPARATER, "VK_SEPARATER");
   pragma Import (Java, VK_SEPARATOR, "VK_SEPARATOR");
   pragma Import (Java, VK_SUBTRACT, "VK_SUBTRACT");
   pragma Import (Java, VK_DECIMAL, "VK_DECIMAL");
   pragma Import (Java, VK_DIVIDE, "VK_DIVIDE");
   pragma Import (Java, VK_DELETE, "VK_DELETE");
   pragma Import (Java, VK_NUM_LOCK, "VK_NUM_LOCK");
   pragma Import (Java, VK_SCROLL_LOCK, "VK_SCROLL_LOCK");
   pragma Import (Java, VK_F1, "VK_F1");
   pragma Import (Java, VK_F2, "VK_F2");
   pragma Import (Java, VK_F3, "VK_F3");
   pragma Import (Java, VK_F4, "VK_F4");
   pragma Import (Java, VK_F5, "VK_F5");
   pragma Import (Java, VK_F6, "VK_F6");
   pragma Import (Java, VK_F7, "VK_F7");
   pragma Import (Java, VK_F8, "VK_F8");
   pragma Import (Java, VK_F9, "VK_F9");
   pragma Import (Java, VK_F10, "VK_F10");
   pragma Import (Java, VK_F11, "VK_F11");
   pragma Import (Java, VK_F12, "VK_F12");
   pragma Import (Java, VK_F13, "VK_F13");
   pragma Import (Java, VK_F14, "VK_F14");
   pragma Import (Java, VK_F15, "VK_F15");
   pragma Import (Java, VK_F16, "VK_F16");
   pragma Import (Java, VK_F17, "VK_F17");
   pragma Import (Java, VK_F18, "VK_F18");
   pragma Import (Java, VK_F19, "VK_F19");
   pragma Import (Java, VK_F20, "VK_F20");
   pragma Import (Java, VK_F21, "VK_F21");
   pragma Import (Java, VK_F22, "VK_F22");
   pragma Import (Java, VK_F23, "VK_F23");
   pragma Import (Java, VK_F24, "VK_F24");
   pragma Import (Java, VK_PRINTSCREEN, "VK_PRINTSCREEN");
   pragma Import (Java, VK_INSERT, "VK_INSERT");
   pragma Import (Java, VK_HELP, "VK_HELP");
   pragma Import (Java, VK_META, "VK_META");
   pragma Import (Java, VK_BACK_QUOTE, "VK_BACK_QUOTE");
   pragma Import (Java, VK_QUOTE, "VK_QUOTE");
   pragma Import (Java, VK_KP_UP, "VK_KP_UP");
   pragma Import (Java, VK_KP_DOWN, "VK_KP_DOWN");
   pragma Import (Java, VK_KP_LEFT, "VK_KP_LEFT");
   pragma Import (Java, VK_KP_RIGHT, "VK_KP_RIGHT");
   pragma Import (Java, VK_DEAD_GRAVE, "VK_DEAD_GRAVE");
   pragma Import (Java, VK_DEAD_ACUTE, "VK_DEAD_ACUTE");
   pragma Import (Java, VK_DEAD_CIRCUMFLEX, "VK_DEAD_CIRCUMFLEX");
   pragma Import (Java, VK_DEAD_TILDE, "VK_DEAD_TILDE");
   pragma Import (Java, VK_DEAD_MACRON, "VK_DEAD_MACRON");
   pragma Import (Java, VK_DEAD_BREVE, "VK_DEAD_BREVE");
   pragma Import (Java, VK_DEAD_ABOVEDOT, "VK_DEAD_ABOVEDOT");
   pragma Import (Java, VK_DEAD_DIAERESIS, "VK_DEAD_DIAERESIS");
   pragma Import (Java, VK_DEAD_ABOVERING, "VK_DEAD_ABOVERING");
   pragma Import (Java, VK_DEAD_DOUBLEACUTE, "VK_DEAD_DOUBLEACUTE");
   pragma Import (Java, VK_DEAD_CARON, "VK_DEAD_CARON");
   pragma Import (Java, VK_DEAD_CEDILLA, "VK_DEAD_CEDILLA");
   pragma Import (Java, VK_DEAD_OGONEK, "VK_DEAD_OGONEK");
   pragma Import (Java, VK_DEAD_IOTA, "VK_DEAD_IOTA");
   pragma Import (Java, VK_DEAD_VOICED_SOUND, "VK_DEAD_VOICED_SOUND");
   pragma Import (Java, VK_DEAD_SEMIVOICED_SOUND, "VK_DEAD_SEMIVOICED_SOUND");
   pragma Import (Java, VK_AMPERSAND, "VK_AMPERSAND");
   pragma Import (Java, VK_ASTERISK, "VK_ASTERISK");
   pragma Import (Java, VK_QUOTEDBL, "VK_QUOTEDBL");
   pragma Import (Java, VK_LESS, "VK_LESS");
   pragma Import (Java, VK_GREATER, "VK_GREATER");
   pragma Import (Java, VK_BRACELEFT, "VK_BRACELEFT");
   pragma Import (Java, VK_BRACERIGHT, "VK_BRACERIGHT");
   pragma Import (Java, VK_AT, "VK_AT");
   pragma Import (Java, VK_COLON, "VK_COLON");
   pragma Import (Java, VK_CIRCUMFLEX, "VK_CIRCUMFLEX");
   pragma Import (Java, VK_DOLLAR, "VK_DOLLAR");
   pragma Import (Java, VK_EURO_SIGN, "VK_EURO_SIGN");
   pragma Import (Java, VK_EXCLAMATION_MARK, "VK_EXCLAMATION_MARK");
   pragma Import (Java, VK_INVERTED_EXCLAMATION_MARK, "VK_INVERTED_EXCLAMATION_MARK");
   pragma Import (Java, VK_LEFT_PARENTHESIS, "VK_LEFT_PARENTHESIS");
   pragma Import (Java, VK_NUMBER_SIGN, "VK_NUMBER_SIGN");
   pragma Import (Java, VK_PLUS, "VK_PLUS");
   pragma Import (Java, VK_RIGHT_PARENTHESIS, "VK_RIGHT_PARENTHESIS");
   pragma Import (Java, VK_UNDERSCORE, "VK_UNDERSCORE");
   pragma Import (Java, VK_WINDOWS, "VK_WINDOWS");
   pragma Import (Java, VK_CONTEXT_MENU, "VK_CONTEXT_MENU");
   pragma Import (Java, VK_FINAL, "VK_FINAL");
   pragma Import (Java, VK_CONVERT, "VK_CONVERT");
   pragma Import (Java, VK_NONCONVERT, "VK_NONCONVERT");
   pragma Import (Java, VK_ACCEPT, "VK_ACCEPT");
   pragma Import (Java, VK_MODECHANGE, "VK_MODECHANGE");
   pragma Import (Java, VK_KANA, "VK_KANA");
   pragma Import (Java, VK_KANJI, "VK_KANJI");
   pragma Import (Java, VK_ALPHANUMERIC, "VK_ALPHANUMERIC");
   pragma Import (Java, VK_KATAKANA, "VK_KATAKANA");
   pragma Import (Java, VK_HIRAGANA, "VK_HIRAGANA");
   pragma Import (Java, VK_FULL_WIDTH, "VK_FULL_WIDTH");
   pragma Import (Java, VK_HALF_WIDTH, "VK_HALF_WIDTH");
   pragma Import (Java, VK_ROMAN_CHARACTERS, "VK_ROMAN_CHARACTERS");
   pragma Import (Java, VK_ALL_CANDIDATES, "VK_ALL_CANDIDATES");
   pragma Import (Java, VK_PREVIOUS_CANDIDATE, "VK_PREVIOUS_CANDIDATE");
   pragma Import (Java, VK_CODE_INPUT, "VK_CODE_INPUT");
   pragma Import (Java, VK_JAPANESE_KATAKANA, "VK_JAPANESE_KATAKANA");
   pragma Import (Java, VK_JAPANESE_HIRAGANA, "VK_JAPANESE_HIRAGANA");
   pragma Import (Java, VK_JAPANESE_ROMAN, "VK_JAPANESE_ROMAN");
   pragma Import (Java, VK_KANA_LOCK, "VK_KANA_LOCK");
   pragma Import (Java, VK_INPUT_METHOD_ON_OFF, "VK_INPUT_METHOD_ON_OFF");
   pragma Import (Java, VK_CUT, "VK_CUT");
   pragma Import (Java, VK_COPY, "VK_COPY");
   pragma Import (Java, VK_PASTE, "VK_PASTE");
   pragma Import (Java, VK_UNDO, "VK_UNDO");
   pragma Import (Java, VK_AGAIN, "VK_AGAIN");
   pragma Import (Java, VK_FIND, "VK_FIND");
   pragma Import (Java, VK_PROPS, "VK_PROPS");
   pragma Import (Java, VK_STOP, "VK_STOP");
   pragma Import (Java, VK_COMPOSE, "VK_COMPOSE");
   pragma Import (Java, VK_ALT_GRAPH, "VK_ALT_GRAPH");
   pragma Import (Java, VK_BEGIN, "VK_BEGIN");
   pragma Import (Java, VK_UNDEFINED, "VK_UNDEFINED");
   pragma Import (Java, CHAR_UNDEFINED, "CHAR_UNDEFINED");
   pragma Import (Java, KEY_LOCATION_UNKNOWN, "KEY_LOCATION_UNKNOWN");
   pragma Import (Java, KEY_LOCATION_STANDARD, "KEY_LOCATION_STANDARD");
   pragma Import (Java, KEY_LOCATION_LEFT, "KEY_LOCATION_LEFT");
   pragma Import (Java, KEY_LOCATION_RIGHT, "KEY_LOCATION_RIGHT");
   pragma Import (Java, KEY_LOCATION_NUMPAD, "KEY_LOCATION_NUMPAD");

end Java.Awt.Event.KeyEvent;
pragma Import (Java, Java.Awt.Event.KeyEvent, "java.awt.event.KeyEvent");
pragma Extensions_Allowed (Off);
