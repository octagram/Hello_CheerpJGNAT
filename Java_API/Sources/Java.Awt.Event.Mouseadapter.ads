pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Event.MouseWheelEvent;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Lang.Object;

package Java.Awt.Event.MouseAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MouseAdapter (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseWheelMoved (This : access Typ;
                              P1_MouseWheelEvent : access Standard.Java.Awt.Event.MouseWheelEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MouseAdapter);
   pragma Import (Java, MouseClicked, "mouseClicked");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MouseEntered, "mouseEntered");
   pragma Import (Java, MouseExited, "mouseExited");
   pragma Import (Java, MouseWheelMoved, "mouseWheelMoved");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");

end Java.Awt.Event.MouseAdapter;
pragma Import (Java, Java.Awt.Event.MouseAdapter, "java.awt.event.MouseAdapter");
pragma Extensions_Allowed (Off);
