pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Point;
limited with Java.Lang.String;
with Java.Awt.Event.InputEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.MouseEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.InputEvent.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLocationOnScreen (This : access Typ)
                                 return access Java.Awt.Point.Typ'Class;

   function GetXOnScreen (This : access Typ)
                          return Java.Int;

   function GetYOnScreen (This : access Typ)
                          return Java.Int;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MouseEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Long : Java.Long;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int;
                            P7_Int : Java.Int;
                            P8_Boolean : Java.Boolean;
                            P9_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_MouseEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Long : Java.Long;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int;
                            P7_Int : Java.Int;
                            P8_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;

   function New_MouseEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Long : Java.Long;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int;
                            P7_Int : Java.Int;
                            P8_Int : Java.Int;
                            P9_Int : Java.Int;
                            P10_Boolean : Java.Boolean;
                            P11_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function GetX (This : access Typ)
                  return Java.Int;

   function GetY (This : access Typ)
                  return Java.Int;

   function GetPoint (This : access Typ)
                      return access Java.Awt.Point.Typ'Class;

   --  synchronized
   procedure TranslatePoint (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int);

   function GetClickCount (This : access Typ)
                           return Java.Int;

   function GetButton (This : access Typ)
                       return Java.Int;

   function IsPopupTrigger (This : access Typ)
                            return Java.Boolean;

   function GetMouseModifiersText (P1_Int : Java.Int)
                                   return access Java.Lang.String.Typ'Class;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MOUSE_FIRST : constant Java.Int;

   --  final
   MOUSE_LAST : constant Java.Int;

   --  final
   MOUSE_CLICKED : constant Java.Int;

   --  final
   MOUSE_PRESSED : constant Java.Int;

   --  final
   MOUSE_RELEASED : constant Java.Int;

   --  final
   MOUSE_MOVED : constant Java.Int;

   --  final
   MOUSE_ENTERED : constant Java.Int;

   --  final
   MOUSE_EXITED : constant Java.Int;

   --  final
   MOUSE_DRAGGED : constant Java.Int;

   --  final
   MOUSE_WHEEL : constant Java.Int;

   --  final
   NOBUTTON : constant Java.Int;

   --  final
   BUTTON1 : constant Java.Int;

   --  final
   BUTTON2 : constant Java.Int;

   --  final
   BUTTON3 : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetLocationOnScreen, "getLocationOnScreen");
   pragma Import (Java, GetXOnScreen, "getXOnScreen");
   pragma Import (Java, GetYOnScreen, "getYOnScreen");
   pragma Java_Constructor (New_MouseEvent);
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetPoint, "getPoint");
   pragma Import (Java, TranslatePoint, "translatePoint");
   pragma Import (Java, GetClickCount, "getClickCount");
   pragma Import (Java, GetButton, "getButton");
   pragma Import (Java, IsPopupTrigger, "isPopupTrigger");
   pragma Import (Java, GetMouseModifiersText, "getMouseModifiersText");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, MOUSE_FIRST, "MOUSE_FIRST");
   pragma Import (Java, MOUSE_LAST, "MOUSE_LAST");
   pragma Import (Java, MOUSE_CLICKED, "MOUSE_CLICKED");
   pragma Import (Java, MOUSE_PRESSED, "MOUSE_PRESSED");
   pragma Import (Java, MOUSE_RELEASED, "MOUSE_RELEASED");
   pragma Import (Java, MOUSE_MOVED, "MOUSE_MOVED");
   pragma Import (Java, MOUSE_ENTERED, "MOUSE_ENTERED");
   pragma Import (Java, MOUSE_EXITED, "MOUSE_EXITED");
   pragma Import (Java, MOUSE_DRAGGED, "MOUSE_DRAGGED");
   pragma Import (Java, MOUSE_WHEEL, "MOUSE_WHEEL");
   pragma Import (Java, NOBUTTON, "NOBUTTON");
   pragma Import (Java, BUTTON1, "BUTTON1");
   pragma Import (Java, BUTTON2, "BUTTON2");
   pragma Import (Java, BUTTON3, "BUTTON3");

end Java.Awt.Event.MouseEvent;
pragma Import (Java, Java.Awt.Event.MouseEvent, "java.awt.event.MouseEvent");
pragma Extensions_Allowed (Off);
