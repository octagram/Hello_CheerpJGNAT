pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Event.MouseListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class) is abstract;

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class) is abstract;

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class) is abstract;

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class) is abstract;

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, MouseClicked, "mouseClicked");
   pragma Export (Java, MousePressed, "mousePressed");
   pragma Export (Java, MouseReleased, "mouseReleased");
   pragma Export (Java, MouseEntered, "mouseEntered");
   pragma Export (Java, MouseExited, "mouseExited");

end Java.Awt.Event.MouseListener;
pragma Import (Java, Java.Awt.Event.MouseListener, "java.awt.event.MouseListener");
pragma Extensions_Allowed (Off);
