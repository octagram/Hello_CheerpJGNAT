pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
with Java.Awt.Event.MouseMotionListener;
with Java.Lang.Object;

package Java.Awt.Event.MouseMotionAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MouseMotionAdapter (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MouseMotionAdapter);
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");

end Java.Awt.Event.MouseMotionAdapter;
pragma Import (Java, Java.Awt.Event.MouseMotionAdapter, "java.awt.event.MouseMotionAdapter");
pragma Extensions_Allowed (Off);
