pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
with Java.Awt.Event.MouseEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.MouseWheelEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.MouseEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MouseWheelEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Long : Java.Long;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Boolean : Java.Boolean;
                                 P9_Int : Java.Int;
                                 P10_Int : Java.Int;
                                 P11_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_MouseWheelEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Long : Java.Long;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Int : Java.Int;
                                 P9_Int : Java.Int;
                                 P10_Boolean : Java.Boolean;
                                 P11_Int : Java.Int;
                                 P12_Int : Java.Int;
                                 P13_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetScrollType (This : access Typ)
                           return Java.Int;

   function GetScrollAmount (This : access Typ)
                             return Java.Int;

   function GetWheelRotation (This : access Typ)
                              return Java.Int;

   function GetUnitsToScroll (This : access Typ)
                              return Java.Int;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WHEEL_UNIT_SCROLL : constant Java.Int;

   --  final
   WHEEL_BLOCK_SCROLL : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MouseWheelEvent);
   pragma Import (Java, GetScrollType, "getScrollType");
   pragma Import (Java, GetScrollAmount, "getScrollAmount");
   pragma Import (Java, GetWheelRotation, "getWheelRotation");
   pragma Import (Java, GetUnitsToScroll, "getUnitsToScroll");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, WHEEL_UNIT_SCROLL, "WHEEL_UNIT_SCROLL");
   pragma Import (Java, WHEEL_BLOCK_SCROLL, "WHEEL_BLOCK_SCROLL");

end Java.Awt.Event.MouseWheelEvent;
pragma Import (Java, Java.Awt.Event.MouseWheelEvent, "java.awt.event.MouseWheelEvent");
pragma Extensions_Allowed (Off);
