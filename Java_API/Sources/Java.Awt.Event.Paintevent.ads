pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
with Java.Awt.Event.ComponentEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.PaintEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.ComponentEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PaintEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUpdateRect (This : access Typ)
                           return access Java.Awt.Rectangle.Typ'Class;

   procedure SetUpdateRect (This : access Typ;
                            P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PAINT_FIRST : constant Java.Int;

   --  final
   PAINT_LAST : constant Java.Int;

   --  final
   PAINT : constant Java.Int;

   --  final
   UPDATE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PaintEvent);
   pragma Import (Java, GetUpdateRect, "getUpdateRect");
   pragma Import (Java, SetUpdateRect, "setUpdateRect");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, PAINT_FIRST, "PAINT_FIRST");
   pragma Import (Java, PAINT_LAST, "PAINT_LAST");
   pragma Import (Java, PAINT, "PAINT");
   pragma Import (Java, UPDATE, "UPDATE");

end Java.Awt.Event.PaintEvent;
pragma Import (Java, Java.Awt.Event.PaintEvent, "java.awt.event.PaintEvent");
pragma Extensions_Allowed (Off);
