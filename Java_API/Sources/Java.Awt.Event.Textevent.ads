pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.TextEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TextEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TEXT_FIRST : constant Java.Int;

   --  final
   TEXT_LAST : constant Java.Int;

   --  final
   TEXT_VALUE_CHANGED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TextEvent);
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, TEXT_FIRST, "TEXT_FIRST");
   pragma Import (Java, TEXT_LAST, "TEXT_LAST");
   pragma Import (Java, TEXT_VALUE_CHANGED, "TEXT_VALUE_CHANGED");

end Java.Awt.Event.TextEvent;
pragma Import (Java, Java.Awt.Event.TextEvent, "java.awt.event.TextEvent");
pragma Extensions_Allowed (Off);
