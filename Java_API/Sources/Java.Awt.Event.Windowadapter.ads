pragma Extensions_Allowed (On);
limited with Java.Awt.Event.WindowEvent;
with Java.Awt.Event.WindowFocusListener;
with Java.Awt.Event.WindowListener;
with Java.Awt.Event.WindowStateListener;
with Java.Lang.Object;

package Java.Awt.Event.WindowAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(WindowFocusListener_I : Java.Awt.Event.WindowFocusListener.Ref;
            WindowListener_I : Java.Awt.Event.WindowListener.Ref;
            WindowStateListener_I : Java.Awt.Event.WindowStateListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_WindowAdapter (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WindowOpened (This : access Typ;
                           P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowClosing (This : access Typ;
                            P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowClosed (This : access Typ;
                           P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowIconified (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowDeiconified (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowActivated (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowDeactivated (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowStateChanged (This : access Typ;
                                 P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowGainedFocus (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   procedure WindowLostFocus (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_WindowAdapter);
   pragma Import (Java, WindowOpened, "windowOpened");
   pragma Import (Java, WindowClosing, "windowClosing");
   pragma Import (Java, WindowClosed, "windowClosed");
   pragma Import (Java, WindowIconified, "windowIconified");
   pragma Import (Java, WindowDeiconified, "windowDeiconified");
   pragma Import (Java, WindowActivated, "windowActivated");
   pragma Import (Java, WindowDeactivated, "windowDeactivated");
   pragma Import (Java, WindowStateChanged, "windowStateChanged");
   pragma Import (Java, WindowGainedFocus, "windowGainedFocus");
   pragma Import (Java, WindowLostFocus, "windowLostFocus");

end Java.Awt.Event.WindowAdapter;
pragma Import (Java, Java.Awt.Event.WindowAdapter, "java.awt.event.WindowAdapter");
pragma Extensions_Allowed (Off);
