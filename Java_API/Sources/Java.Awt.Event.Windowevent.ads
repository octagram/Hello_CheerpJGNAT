pragma Extensions_Allowed (On);
limited with Java.Awt.Window;
limited with Java.Lang.String;
with Java.Awt.Event.ComponentEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event.WindowEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.ComponentEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_WindowEvent (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Window : access Standard.Java.Awt.Window.Typ'Class;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_WindowEvent (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Window : access Standard.Java.Awt.Window.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_WindowEvent (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_WindowEvent (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetWindow (This : access Typ)
                       return access Java.Awt.Window.Typ'Class;

   function GetOppositeWindow (This : access Typ)
                               return access Java.Awt.Window.Typ'Class;

   function GetOldState (This : access Typ)
                         return Java.Int;

   function GetNewState (This : access Typ)
                         return Java.Int;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WINDOW_FIRST : constant Java.Int;

   --  final
   WINDOW_OPENED : constant Java.Int;

   --  final
   WINDOW_CLOSING : constant Java.Int;

   --  final
   WINDOW_CLOSED : constant Java.Int;

   --  final
   WINDOW_ICONIFIED : constant Java.Int;

   --  final
   WINDOW_DEICONIFIED : constant Java.Int;

   --  final
   WINDOW_ACTIVATED : constant Java.Int;

   --  final
   WINDOW_DEACTIVATED : constant Java.Int;

   --  final
   WINDOW_GAINED_FOCUS : constant Java.Int;

   --  final
   WINDOW_LOST_FOCUS : constant Java.Int;

   --  final
   WINDOW_STATE_CHANGED : constant Java.Int;

   --  final
   WINDOW_LAST : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_WindowEvent);
   pragma Import (Java, GetWindow, "getWindow");
   pragma Import (Java, GetOppositeWindow, "getOppositeWindow");
   pragma Import (Java, GetOldState, "getOldState");
   pragma Import (Java, GetNewState, "getNewState");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, WINDOW_FIRST, "WINDOW_FIRST");
   pragma Import (Java, WINDOW_OPENED, "WINDOW_OPENED");
   pragma Import (Java, WINDOW_CLOSING, "WINDOW_CLOSING");
   pragma Import (Java, WINDOW_CLOSED, "WINDOW_CLOSED");
   pragma Import (Java, WINDOW_ICONIFIED, "WINDOW_ICONIFIED");
   pragma Import (Java, WINDOW_DEICONIFIED, "WINDOW_DEICONIFIED");
   pragma Import (Java, WINDOW_ACTIVATED, "WINDOW_ACTIVATED");
   pragma Import (Java, WINDOW_DEACTIVATED, "WINDOW_DEACTIVATED");
   pragma Import (Java, WINDOW_GAINED_FOCUS, "WINDOW_GAINED_FOCUS");
   pragma Import (Java, WINDOW_LOST_FOCUS, "WINDOW_LOST_FOCUS");
   pragma Import (Java, WINDOW_STATE_CHANGED, "WINDOW_STATE_CHANGED");
   pragma Import (Java, WINDOW_LAST, "WINDOW_LAST");

end Java.Awt.Event.WindowEvent;
pragma Import (Java, Java.Awt.Event.WindowEvent, "java.awt.event.WindowEvent");
pragma Extensions_Allowed (Off);
