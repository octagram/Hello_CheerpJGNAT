pragma Extensions_Allowed (On);
limited with Java.Awt.Event.WindowEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Event.WindowListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WindowOpened (This : access Typ;
                           P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;

   procedure WindowClosing (This : access Typ;
                            P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;

   procedure WindowClosed (This : access Typ;
                           P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;

   procedure WindowIconified (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;

   procedure WindowDeiconified (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;

   procedure WindowActivated (This : access Typ;
                              P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;

   procedure WindowDeactivated (This : access Typ;
                                P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WindowOpened, "windowOpened");
   pragma Export (Java, WindowClosing, "windowClosing");
   pragma Export (Java, WindowClosed, "windowClosed");
   pragma Export (Java, WindowIconified, "windowIconified");
   pragma Export (Java, WindowDeiconified, "windowDeiconified");
   pragma Export (Java, WindowActivated, "windowActivated");
   pragma Export (Java, WindowDeactivated, "windowDeactivated");

end Java.Awt.Event.WindowListener;
pragma Import (Java, Java.Awt.Event.WindowListener, "java.awt.event.WindowListener");
pragma Extensions_Allowed (Off);
