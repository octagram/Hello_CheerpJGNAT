pragma Extensions_Allowed (On);
limited with Java.Awt.Event.WindowEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Awt.Event.WindowStateListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WindowStateChanged (This : access Typ;
                                 P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WindowStateChanged, "windowStateChanged");

end Java.Awt.Event.WindowStateListener;
pragma Import (Java, Java.Awt.Event.WindowStateListener, "java.awt.event.WindowStateListener");
pragma Extensions_Allowed (Off);
