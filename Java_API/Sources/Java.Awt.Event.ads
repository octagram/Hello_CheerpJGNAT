pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Event is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Target : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Target, "target");

      when_K : Java.Long;
      pragma Import (Java, when_K, "when");

      Id : Java.Int;
      pragma Import (Java, Id, "id");

      X : Java.Int;
      pragma Import (Java, X, "x");

      Y : Java.Int;
      pragma Import (Java, Y, "y");

      Key : Java.Int;
      pragma Import (Java, Key, "key");

      Modifiers : Java.Int;
      pragma Import (Java, Modifiers, "modifiers");

      ClickCount : Java.Int;
      pragma Import (Java, ClickCount, "clickCount");

      Arg : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Arg, "arg");

      Evt : access Java.Awt.Event.Typ'Class;
      pragma Import (Java, Evt, "evt");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Event (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Long : Java.Long;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int;
                       P8_Object : access Standard.Java.Lang.Object.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_Event (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Long : Java.Long;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   function New_Event (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Object : access Standard.Java.Lang.Object.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Translate (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   function ShiftDown (This : access Typ)
                       return Java.Boolean;

   function ControlDown (This : access Typ)
                         return Java.Boolean;

   function MetaDown (This : access Typ)
                      return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SHIFT_MASK : constant Java.Int;

   --  final
   CTRL_MASK : constant Java.Int;

   --  final
   META_MASK : constant Java.Int;

   --  final
   ALT_MASK : constant Java.Int;

   --  final
   HOME : constant Java.Int;

   --  final
   END_K : constant Java.Int;

   --  final
   PGUP : constant Java.Int;

   --  final
   PGDN : constant Java.Int;

   --  final
   UP : constant Java.Int;

   --  final
   DOWN : constant Java.Int;

   --  final
   LEFT : constant Java.Int;

   --  final
   RIGHT : constant Java.Int;

   --  final
   F1 : constant Java.Int;

   --  final
   F2 : constant Java.Int;

   --  final
   F3 : constant Java.Int;

   --  final
   F4 : constant Java.Int;

   --  final
   F5 : constant Java.Int;

   --  final
   F6 : constant Java.Int;

   --  final
   F7 : constant Java.Int;

   --  final
   F8 : constant Java.Int;

   --  final
   F9 : constant Java.Int;

   --  final
   F10 : constant Java.Int;

   --  final
   F11 : constant Java.Int;

   --  final
   F12 : constant Java.Int;

   --  final
   PRINT_SCREEN : constant Java.Int;

   --  final
   SCROLL_LOCK : constant Java.Int;

   --  final
   CAPS_LOCK : constant Java.Int;

   --  final
   NUM_LOCK : constant Java.Int;

   --  final
   PAUSE : constant Java.Int;

   --  final
   INSERT : constant Java.Int;

   --  final
   ENTER : constant Java.Int;

   --  final
   BACK_SPACE : constant Java.Int;

   --  final
   TAB : constant Java.Int;

   --  final
   ESCAPE : constant Java.Int;

   --  final
   DELETE : constant Java.Int;

   --  final
   WINDOW_DESTROY : constant Java.Int;

   --  final
   WINDOW_EXPOSE : constant Java.Int;

   --  final
   WINDOW_ICONIFY : constant Java.Int;

   --  final
   WINDOW_DEICONIFY : constant Java.Int;

   --  final
   WINDOW_MOVED : constant Java.Int;

   --  final
   KEY_PRESS : constant Java.Int;

   --  final
   KEY_RELEASE : constant Java.Int;

   --  final
   KEY_ACTION : constant Java.Int;

   --  final
   KEY_ACTION_RELEASE : constant Java.Int;

   --  final
   MOUSE_DOWN : constant Java.Int;

   --  final
   MOUSE_UP : constant Java.Int;

   --  final
   MOUSE_MOVE : constant Java.Int;

   --  final
   MOUSE_ENTER : constant Java.Int;

   --  final
   MOUSE_EXIT : constant Java.Int;

   --  final
   MOUSE_DRAG : constant Java.Int;

   --  final
   SCROLL_LINE_UP : constant Java.Int;

   --  final
   SCROLL_LINE_DOWN : constant Java.Int;

   --  final
   SCROLL_PAGE_UP : constant Java.Int;

   --  final
   SCROLL_PAGE_DOWN : constant Java.Int;

   --  final
   SCROLL_ABSOLUTE : constant Java.Int;

   --  final
   SCROLL_BEGIN : constant Java.Int;

   --  final
   SCROLL_END : constant Java.Int;

   --  final
   LIST_SELECT : constant Java.Int;

   --  final
   LIST_DESELECT : constant Java.Int;

   --  final
   ACTION_EVENT : constant Java.Int;

   --  final
   LOAD_FILE : constant Java.Int;

   --  final
   SAVE_FILE : constant Java.Int;

   --  final
   GOT_FOCUS : constant Java.Int;

   --  final
   LOST_FOCUS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Event);
   pragma Import (Java, Translate, "translate");
   pragma Import (Java, ShiftDown, "shiftDown");
   pragma Import (Java, ControlDown, "controlDown");
   pragma Import (Java, MetaDown, "metaDown");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SHIFT_MASK, "SHIFT_MASK");
   pragma Import (Java, CTRL_MASK, "CTRL_MASK");
   pragma Import (Java, META_MASK, "META_MASK");
   pragma Import (Java, ALT_MASK, "ALT_MASK");
   pragma Import (Java, HOME, "HOME");
   pragma Import (Java, END_K, "END");
   pragma Import (Java, PGUP, "PGUP");
   pragma Import (Java, PGDN, "PGDN");
   pragma Import (Java, UP, "UP");
   pragma Import (Java, DOWN, "DOWN");
   pragma Import (Java, LEFT, "LEFT");
   pragma Import (Java, RIGHT, "RIGHT");
   pragma Import (Java, F1, "F1");
   pragma Import (Java, F2, "F2");
   pragma Import (Java, F3, "F3");
   pragma Import (Java, F4, "F4");
   pragma Import (Java, F5, "F5");
   pragma Import (Java, F6, "F6");
   pragma Import (Java, F7, "F7");
   pragma Import (Java, F8, "F8");
   pragma Import (Java, F9, "F9");
   pragma Import (Java, F10, "F10");
   pragma Import (Java, F11, "F11");
   pragma Import (Java, F12, "F12");
   pragma Import (Java, PRINT_SCREEN, "PRINT_SCREEN");
   pragma Import (Java, SCROLL_LOCK, "SCROLL_LOCK");
   pragma Import (Java, CAPS_LOCK, "CAPS_LOCK");
   pragma Import (Java, NUM_LOCK, "NUM_LOCK");
   pragma Import (Java, PAUSE, "PAUSE");
   pragma Import (Java, INSERT, "INSERT");
   pragma Import (Java, ENTER, "ENTER");
   pragma Import (Java, BACK_SPACE, "BACK_SPACE");
   pragma Import (Java, TAB, "TAB");
   pragma Import (Java, ESCAPE, "ESCAPE");
   pragma Import (Java, DELETE, "DELETE");
   pragma Import (Java, WINDOW_DESTROY, "WINDOW_DESTROY");
   pragma Import (Java, WINDOW_EXPOSE, "WINDOW_EXPOSE");
   pragma Import (Java, WINDOW_ICONIFY, "WINDOW_ICONIFY");
   pragma Import (Java, WINDOW_DEICONIFY, "WINDOW_DEICONIFY");
   pragma Import (Java, WINDOW_MOVED, "WINDOW_MOVED");
   pragma Import (Java, KEY_PRESS, "KEY_PRESS");
   pragma Import (Java, KEY_RELEASE, "KEY_RELEASE");
   pragma Import (Java, KEY_ACTION, "KEY_ACTION");
   pragma Import (Java, KEY_ACTION_RELEASE, "KEY_ACTION_RELEASE");
   pragma Import (Java, MOUSE_DOWN, "MOUSE_DOWN");
   pragma Import (Java, MOUSE_UP, "MOUSE_UP");
   pragma Import (Java, MOUSE_MOVE, "MOUSE_MOVE");
   pragma Import (Java, MOUSE_ENTER, "MOUSE_ENTER");
   pragma Import (Java, MOUSE_EXIT, "MOUSE_EXIT");
   pragma Import (Java, MOUSE_DRAG, "MOUSE_DRAG");
   pragma Import (Java, SCROLL_LINE_UP, "SCROLL_LINE_UP");
   pragma Import (Java, SCROLL_LINE_DOWN, "SCROLL_LINE_DOWN");
   pragma Import (Java, SCROLL_PAGE_UP, "SCROLL_PAGE_UP");
   pragma Import (Java, SCROLL_PAGE_DOWN, "SCROLL_PAGE_DOWN");
   pragma Import (Java, SCROLL_ABSOLUTE, "SCROLL_ABSOLUTE");
   pragma Import (Java, SCROLL_BEGIN, "SCROLL_BEGIN");
   pragma Import (Java, SCROLL_END, "SCROLL_END");
   pragma Import (Java, LIST_SELECT, "LIST_SELECT");
   pragma Import (Java, LIST_DESELECT, "LIST_DESELECT");
   pragma Import (Java, ACTION_EVENT, "ACTION_EVENT");
   pragma Import (Java, LOAD_FILE, "LOAD_FILE");
   pragma Import (Java, SAVE_FILE, "SAVE_FILE");
   pragma Import (Java, GOT_FOCUS, "GOT_FOCUS");
   pragma Import (Java, LOST_FOCUS, "LOST_FOCUS");

end Java.Awt.Event;
pragma Import (Java, Java.Awt.Event, "java.awt.Event");
pragma Extensions_Allowed (Off);
