pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Lang.Runnable;
with Java.Lang.Object;

package Java.Awt.EventQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EventQueue (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PostEvent (This : access Typ;
                        P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   function GetNextEvent (This : access Typ)
                          return access Java.Awt.AWTEvent.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   --  synchronized
   function PeekEvent (This : access Typ)
                       return access Java.Awt.AWTEvent.Typ'Class;

   --  synchronized
   function PeekEvent (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Awt.AWTEvent.Typ'Class;

   --  protected
   procedure DispatchEvent (This : access Typ;
                            P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   function GetMostRecentEventTime return Java.Long;

   function GetCurrentEvent return access Java.Awt.AWTEvent.Typ'Class;

   --  synchronized
   procedure Push (This : access Typ;
                   P1_EventQueue : access Standard.Java.Awt.EventQueue.Typ'Class);

   --  protected
   procedure Pop (This : access Typ);
   --  can raise Java.Util.EmptyStackException.Except

   function IsDispatchThread return Java.Boolean;

   procedure InvokeLater (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);

   procedure InvokeAndWait (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Lang.Reflect.InvocationTargetException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EventQueue);
   pragma Import (Java, PostEvent, "postEvent");
   pragma Import (Java, GetNextEvent, "getNextEvent");
   pragma Import (Java, PeekEvent, "peekEvent");
   pragma Import (Java, DispatchEvent, "dispatchEvent");
   pragma Import (Java, GetMostRecentEventTime, "getMostRecentEventTime");
   pragma Import (Java, GetCurrentEvent, "getCurrentEvent");
   pragma Import (Java, Push, "push");
   pragma Import (Java, Pop, "pop");
   pragma Import (Java, IsDispatchThread, "isDispatchThread");
   pragma Import (Java, InvokeLater, "invokeLater");
   pragma Import (Java, InvokeAndWait, "invokeAndWait");

end Java.Awt.EventQueue;
pragma Import (Java, Java.Awt.EventQueue, "java.awt.EventQueue");
pragma Extensions_Allowed (Off);
