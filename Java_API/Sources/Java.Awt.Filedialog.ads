pragma Extensions_Allowed (On);
limited with Java.Awt.Frame;
limited with Java.Io.FilenameFilter;
limited with Java.Lang.String;
with Java.Awt.Dialog;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.FileDialog is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Dialog.Typ(MenuContainer_I,
                               ImageObserver_I,
                               Serializable_I,
                               Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileDialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_FileDialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_FileDialog (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_FileDialog (P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_FileDialog (P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_FileDialog (P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetMode (This : access Typ)
                     return Java.Int;

   procedure SetMode (This : access Typ;
                      P1_Int : Java.Int);

   function GetDirectory (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetDirectory (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetFile (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetFile (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetFilenameFilter (This : access Typ)
                               return access Java.Io.FilenameFilter.Typ'Class;

   --  synchronized
   procedure SetFilenameFilter (This : access Typ;
                                P1_FilenameFilter : access Standard.Java.Io.FilenameFilter.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LOAD : constant Java.Int;

   --  final
   SAVE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileDialog);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetMode, "getMode");
   pragma Import (Java, SetMode, "setMode");
   pragma Import (Java, GetDirectory, "getDirectory");
   pragma Import (Java, SetDirectory, "setDirectory");
   pragma Import (Java, GetFile, "getFile");
   pragma Import (Java, SetFile, "setFile");
   pragma Import (Java, GetFilenameFilter, "getFilenameFilter");
   pragma Import (Java, SetFilenameFilter, "setFilenameFilter");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, LOAD, "LOAD");
   pragma Import (Java, SAVE, "SAVE");

end Java.Awt.FileDialog;
pragma Import (Java, Java.Awt.FileDialog, "java.awt.FileDialog");
pragma Extensions_Allowed (Off);
