pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Window;
with Java.Lang.Object;

package Java.Awt.FocusTraversalPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FocusTraversalPolicy (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponentAfter (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                               P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return access Java.Awt.Component.Typ'Class is abstract;

   function GetComponentBefore (This : access Typ;
                                P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                                P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return access Java.Awt.Component.Typ'Class is abstract;

   function GetFirstComponent (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Component.Typ'Class is abstract;

   function GetLastComponent (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                              return access Java.Awt.Component.Typ'Class is abstract;

   function GetDefaultComponent (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Component.Typ'Class is abstract;

   function GetInitialComponent (This : access Typ;
                                 P1_Window : access Standard.Java.Awt.Window.Typ'Class)
                                 return access Java.Awt.Component.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FocusTraversalPolicy);
   pragma Export (Java, GetComponentAfter, "getComponentAfter");
   pragma Export (Java, GetComponentBefore, "getComponentBefore");
   pragma Export (Java, GetFirstComponent, "getFirstComponent");
   pragma Export (Java, GetLastComponent, "getLastComponent");
   pragma Export (Java, GetDefaultComponent, "getDefaultComponent");
   pragma Export (Java, GetInitialComponent, "getInitialComponent");

end Java.Awt.FocusTraversalPolicy;
pragma Import (Java, Java.Awt.FocusTraversalPolicy, "java.awt.FocusTraversalPolicy");
pragma Extensions_Allowed (Off);
