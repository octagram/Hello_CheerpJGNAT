pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
with Java.Lang.Object;

package Java.Awt.Font.FontRenderContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_FontRenderContext (This : Ref := null)
                                   return Ref;

   function New_FontRenderContext (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                                   P2_Boolean : Java.Boolean;
                                   P3_Boolean : Java.Boolean; 
                                   This : Ref := null)
                                   return Ref;

   function New_FontRenderContext (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                                   P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                   P3_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsTransformed (This : access Typ)
                           return Java.Boolean;

   function GetTransformType (This : access Typ)
                              return Java.Int;

   function GetTransform (This : access Typ)
                          return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function IsAntiAliased (This : access Typ)
                           return Java.Boolean;

   function UsesFractionalMetrics (This : access Typ)
                                   return Java.Boolean;

   function GetAntiAliasingHint (This : access Typ)
                                 return access Java.Lang.Object.Typ'Class;

   function GetFractionalMetricsHint (This : access Typ)
                                      return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FontRenderContext);
   pragma Import (Java, IsTransformed, "isTransformed");
   pragma Import (Java, GetTransformType, "getTransformType");
   pragma Import (Java, GetTransform, "getTransform");
   pragma Import (Java, IsAntiAliased, "isAntiAliased");
   pragma Import (Java, UsesFractionalMetrics, "usesFractionalMetrics");
   pragma Import (Java, GetAntiAliasingHint, "getAntiAliasingHint");
   pragma Import (Java, GetFractionalMetricsHint, "getFractionalMetricsHint");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Awt.Font.FontRenderContext;
pragma Import (Java, Java.Awt.Font.FontRenderContext, "java.awt.font.FontRenderContext");
pragma Extensions_Allowed (Off);
