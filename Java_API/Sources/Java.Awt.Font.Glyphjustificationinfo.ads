pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Font.GlyphJustificationInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  final
      Weight : Java.Float;
      pragma Import (Java, Weight, "weight");

      --  final
      GrowPriority : Java.Int;
      pragma Import (Java, GrowPriority, "growPriority");

      --  final
      GrowAbsorb : Java.Boolean;
      pragma Import (Java, GrowAbsorb, "growAbsorb");

      --  final
      GrowLeftLimit : Java.Float;
      pragma Import (Java, GrowLeftLimit, "growLeftLimit");

      --  final
      GrowRightLimit : Java.Float;
      pragma Import (Java, GrowRightLimit, "growRightLimit");

      --  final
      ShrinkPriority : Java.Int;
      pragma Import (Java, ShrinkPriority, "shrinkPriority");

      --  final
      ShrinkAbsorb : Java.Boolean;
      pragma Import (Java, ShrinkAbsorb, "shrinkAbsorb");

      --  final
      ShrinkLeftLimit : Java.Float;
      pragma Import (Java, ShrinkLeftLimit, "shrinkLeftLimit");

      --  final
      ShrinkRightLimit : Java.Float;
      pragma Import (Java, ShrinkRightLimit, "shrinkRightLimit");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GlyphJustificationInfo (P1_Float : Java.Float;
                                        P2_Boolean : Java.Boolean;
                                        P3_Int : Java.Int;
                                        P4_Float : Java.Float;
                                        P5_Float : Java.Float;
                                        P6_Boolean : Java.Boolean;
                                        P7_Int : Java.Int;
                                        P8_Float : Java.Float;
                                        P9_Float : Java.Float; 
                                        This : Ref := null)
                                        return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PRIORITY_KASHIDA : constant Java.Int;

   --  final
   PRIORITY_WHITESPACE : constant Java.Int;

   --  final
   PRIORITY_INTERCHAR : constant Java.Int;

   --  final
   PRIORITY_NONE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GlyphJustificationInfo);
   pragma Import (Java, PRIORITY_KASHIDA, "PRIORITY_KASHIDA");
   pragma Import (Java, PRIORITY_WHITESPACE, "PRIORITY_WHITESPACE");
   pragma Import (Java, PRIORITY_INTERCHAR, "PRIORITY_INTERCHAR");
   pragma Import (Java, PRIORITY_NONE, "PRIORITY_NONE");

end Java.Awt.Font.GlyphJustificationInfo;
pragma Import (Java, Java.Awt.Font.GlyphJustificationInfo, "java.awt.font.GlyphJustificationInfo");
pragma Extensions_Allowed (Off);
