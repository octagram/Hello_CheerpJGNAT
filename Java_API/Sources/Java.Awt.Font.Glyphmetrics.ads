pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Rectangle2D;
with Java.Lang.Object;

package Java.Awt.Font.GlyphMetrics is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GlyphMetrics (P1_Float : Java.Float;
                              P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                              P3_Byte : Java.Byte; 
                              This : Ref := null)
                              return Ref;

   function New_GlyphMetrics (P1_Boolean : Java.Boolean;
                              P2_Float : Java.Float;
                              P3_Float : Java.Float;
                              P4_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                              P5_Byte : Java.Byte; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAdvance (This : access Typ)
                        return Java.Float;

   function GetAdvanceX (This : access Typ)
                         return Java.Float;

   function GetAdvanceY (This : access Typ)
                         return Java.Float;

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetLSB (This : access Typ)
                    return Java.Float;

   function GetRSB (This : access Typ)
                    return Java.Float;

   function GetType (This : access Typ)
                     return Java.Int;

   function IsStandard (This : access Typ)
                        return Java.Boolean;

   function IsLigature (This : access Typ)
                        return Java.Boolean;

   function IsCombining (This : access Typ)
                         return Java.Boolean;

   function IsComponent (This : access Typ)
                         return Java.Boolean;

   function IsWhitespace (This : access Typ)
                          return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   STANDARD_C : constant Java.Byte;

   --  final
   LIGATURE : constant Java.Byte;

   --  final
   COMBINING : constant Java.Byte;

   --  final
   COMPONENT : constant Java.Byte;

   --  final
   WHITESPACE : constant Java.Byte;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GlyphMetrics);
   pragma Import (Java, GetAdvance, "getAdvance");
   pragma Import (Java, GetAdvanceX, "getAdvanceX");
   pragma Import (Java, GetAdvanceY, "getAdvanceY");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, GetLSB, "getLSB");
   pragma Import (Java, GetRSB, "getRSB");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, IsStandard, "isStandard");
   pragma Import (Java, IsLigature, "isLigature");
   pragma Import (Java, IsCombining, "isCombining");
   pragma Import (Java, IsComponent, "isComponent");
   pragma Import (Java, IsWhitespace, "isWhitespace");
   pragma Import (Java, STANDARD_C, "STANDARD");
   pragma Import (Java, LIGATURE, "LIGATURE");
   pragma Import (Java, COMBINING, "COMBINING");
   pragma Import (Java, COMPONENT, "COMPONENT");
   pragma Import (Java, WHITESPACE, "WHITESPACE");

end Java.Awt.Font.GlyphMetrics;
pragma Import (Java, Java.Awt.Font.GlyphMetrics, "java.awt.font.GlyphMetrics");
pragma Extensions_Allowed (Off);
