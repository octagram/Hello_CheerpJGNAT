pragma Extensions_Allowed (On);
limited with Java.Awt.Font.FontRenderContext;
limited with Java.Awt.Font.GlyphJustificationInfo;
limited with Java.Awt.Font.GlyphMetrics;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Font.GlyphVector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_GlyphVector (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class is abstract;

   function GetFontRenderContext (This : access Typ)
                                  return access Java.Awt.Font.FontRenderContext.Typ'Class is abstract;

   procedure PerformDefaultLayout (This : access Typ) is abstract;

   function GetNumGlyphs (This : access Typ)
                          return Java.Int is abstract;

   function GetGlyphCode (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int is abstract;

   function GetGlyphCodes (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int_Arr : Java.Int_Arr)
                           return Java.Int_Arr is abstract;

   function GetGlyphCharIndex (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   function GetGlyphCharIndices (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int_Arr : Java.Int_Arr)
                                 return Java.Int_Arr;

   function GetLogicalBounds (This : access Typ)
                              return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   function GetVisualBounds (This : access Typ)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   function GetPixelBounds (This : access Typ;
                            P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                            P2_Float : Java.Float;
                            P3_Float : Java.Float)
                            return access Java.Awt.Rectangle.Typ'Class;

   function GetOutline (This : access Typ)
                        return access Java.Awt.Shape.Typ'Class is abstract;

   function GetOutline (This : access Typ;
                        P1_Float : Java.Float;
                        P2_Float : Java.Float)
                        return access Java.Awt.Shape.Typ'Class is abstract;

   function GetGlyphOutline (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Awt.Shape.Typ'Class is abstract;

   function GetGlyphOutline (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Float : Java.Float;
                             P3_Float : Java.Float)
                             return access Java.Awt.Shape.Typ'Class;

   function GetGlyphPosition (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   procedure SetGlyphPosition (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class) is abstract;

   function GetGlyphTransform (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Awt.Geom.AffineTransform.Typ'Class is abstract;

   procedure SetGlyphTransform (This : access Typ;
                                P1_Int : Java.Int;
                                P2_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class) is abstract;

   function GetLayoutFlags (This : access Typ)
                            return Java.Int;

   function GetGlyphPositions (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Float_Arr : Java.Float_Arr)
                               return Java.Float_Arr is abstract;

   function GetGlyphLogicalBounds (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Awt.Shape.Typ'Class is abstract;

   function GetGlyphVisualBounds (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Java.Awt.Shape.Typ'Class is abstract;

   function GetGlyphPixelBounds (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                                 P3_Float : Java.Float;
                                 P4_Float : Java.Float)
                                 return access Java.Awt.Rectangle.Typ'Class;

   function GetGlyphMetrics (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Awt.Font.GlyphMetrics.Typ'Class is abstract;

   function GetGlyphJustificationInfo (This : access Typ;
                                       P1_Int : Java.Int)
                                       return access Java.Awt.Font.GlyphJustificationInfo.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_GlyphVector : access Standard.Java.Awt.Font.GlyphVector.Typ'Class)
                    return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FLAG_HAS_TRANSFORMS : constant Java.Int;

   --  final
   FLAG_HAS_POSITION_ADJUSTMENTS : constant Java.Int;

   --  final
   FLAG_RUN_RTL : constant Java.Int;

   --  final
   FLAG_COMPLEX_GLYPHS : constant Java.Int;

   --  final
   FLAG_MASK : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GlyphVector);
   pragma Export (Java, GetFont, "getFont");
   pragma Export (Java, GetFontRenderContext, "getFontRenderContext");
   pragma Export (Java, PerformDefaultLayout, "performDefaultLayout");
   pragma Export (Java, GetNumGlyphs, "getNumGlyphs");
   pragma Export (Java, GetGlyphCode, "getGlyphCode");
   pragma Export (Java, GetGlyphCodes, "getGlyphCodes");
   pragma Export (Java, GetGlyphCharIndex, "getGlyphCharIndex");
   pragma Export (Java, GetGlyphCharIndices, "getGlyphCharIndices");
   pragma Export (Java, GetLogicalBounds, "getLogicalBounds");
   pragma Export (Java, GetVisualBounds, "getVisualBounds");
   pragma Export (Java, GetPixelBounds, "getPixelBounds");
   pragma Export (Java, GetOutline, "getOutline");
   pragma Export (Java, GetGlyphOutline, "getGlyphOutline");
   pragma Export (Java, GetGlyphPosition, "getGlyphPosition");
   pragma Export (Java, SetGlyphPosition, "setGlyphPosition");
   pragma Export (Java, GetGlyphTransform, "getGlyphTransform");
   pragma Export (Java, SetGlyphTransform, "setGlyphTransform");
   pragma Export (Java, GetLayoutFlags, "getLayoutFlags");
   pragma Export (Java, GetGlyphPositions, "getGlyphPositions");
   pragma Export (Java, GetGlyphLogicalBounds, "getGlyphLogicalBounds");
   pragma Export (Java, GetGlyphVisualBounds, "getGlyphVisualBounds");
   pragma Export (Java, GetGlyphPixelBounds, "getGlyphPixelBounds");
   pragma Export (Java, GetGlyphMetrics, "getGlyphMetrics");
   pragma Export (Java, GetGlyphJustificationInfo, "getGlyphJustificationInfo");
   pragma Export (Java, Equals, "equals");
   pragma Import (Java, FLAG_HAS_TRANSFORMS, "FLAG_HAS_TRANSFORMS");
   pragma Import (Java, FLAG_HAS_POSITION_ADJUSTMENTS, "FLAG_HAS_POSITION_ADJUSTMENTS");
   pragma Import (Java, FLAG_RUN_RTL, "FLAG_RUN_RTL");
   pragma Import (Java, FLAG_COMPLEX_GLYPHS, "FLAG_COMPLEX_GLYPHS");
   pragma Import (Java, FLAG_MASK, "FLAG_MASK");

end Java.Awt.Font.GlyphVector;
pragma Import (Java, Java.Awt.Font.GlyphVector, "java.awt.font.GlyphVector");
pragma Extensions_Allowed (Off);
