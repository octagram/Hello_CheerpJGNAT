pragma Extensions_Allowed (On);
limited with Java.Awt.Font.GlyphJustificationInfo;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.Shape;
with Java.Lang.Object;

package Java.Awt.Font.GraphicAttribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_GraphicAttribute (P1_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAscent (This : access Typ)
                       return Java.Float is abstract;

   function GetDescent (This : access Typ)
                        return Java.Float is abstract;

   function GetAdvance (This : access Typ)
                        return Java.Float is abstract;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetOutline (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                        return access Java.Awt.Shape.Typ'Class;

   procedure Draw (This : access Typ;
                   P1_Graphics2D : access Standard.Java.Awt.Graphics2D.Typ'Class;
                   P2_Float : Java.Float;
                   P3_Float : Java.Float) is abstract;

   --  final
   function GetAlignment (This : access Typ)
                          return Java.Int;

   function GetJustificationInfo (This : access Typ)
                                  return access Java.Awt.Font.GlyphJustificationInfo.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TOP_ALIGNMENT : constant Java.Int;

   --  final
   BOTTOM_ALIGNMENT : constant Java.Int;

   --  final
   ROMAN_BASELINE : constant Java.Int;

   --  final
   CENTER_BASELINE : constant Java.Int;

   --  final
   HANGING_BASELINE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GraphicAttribute);
   pragma Export (Java, GetAscent, "getAscent");
   pragma Export (Java, GetDescent, "getDescent");
   pragma Export (Java, GetAdvance, "getAdvance");
   pragma Export (Java, GetBounds, "getBounds");
   pragma Export (Java, GetOutline, "getOutline");
   pragma Export (Java, Draw, "draw");
   pragma Export (Java, GetAlignment, "getAlignment");
   pragma Export (Java, GetJustificationInfo, "getJustificationInfo");
   pragma Import (Java, TOP_ALIGNMENT, "TOP_ALIGNMENT");
   pragma Import (Java, BOTTOM_ALIGNMENT, "BOTTOM_ALIGNMENT");
   pragma Import (Java, ROMAN_BASELINE, "ROMAN_BASELINE");
   pragma Import (Java, CENTER_BASELINE, "CENTER_BASELINE");
   pragma Import (Java, HANGING_BASELINE, "HANGING_BASELINE");

end Java.Awt.Font.GraphicAttribute;
pragma Import (Java, Java.Awt.Font.GraphicAttribute, "java.awt.font.GraphicAttribute");
pragma Extensions_Allowed (Off);
