pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.Image;
with Java.Awt.Font.GraphicAttribute;
with Java.Lang.Object;

package Java.Awt.Font.ImageGraphicAttribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Font.GraphicAttribute.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImageGraphicAttribute (P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                                       P2_Int : Java.Int; 
                                       This : Ref := null)
                                       return Ref;

   function New_ImageGraphicAttribute (P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Float : Java.Float;
                                       P4_Float : Java.Float; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAscent (This : access Typ)
                       return Java.Float;

   function GetDescent (This : access Typ)
                        return Java.Float;

   function GetAdvance (This : access Typ)
                        return Java.Float;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   procedure Draw (This : access Typ;
                   P1_Graphics2D : access Standard.Java.Awt.Graphics2D.Typ'Class;
                   P2_Float : Java.Float;
                   P3_Float : Java.Float);

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_ImageGraphicAttribute : access Standard.Java.Awt.Font.ImageGraphicAttribute.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageGraphicAttribute);
   pragma Import (Java, GetAscent, "getAscent");
   pragma Import (Java, GetDescent, "getDescent");
   pragma Import (Java, GetAdvance, "getAdvance");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, Draw, "draw");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Java.Awt.Font.ImageGraphicAttribute;
pragma Import (Java, Java.Awt.Font.ImageGraphicAttribute, "java.awt.font.ImageGraphicAttribute");
pragma Extensions_Allowed (Off);
