pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Point2D;
with Java.Lang.Object;

package Java.Awt.Font.LayoutPath is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_LayoutPath (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function PointToPath (This : access Typ;
                         P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                         P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                         return Java.Boolean is abstract;

   procedure PathToPoint (This : access Typ;
                          P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                          P2_Boolean : Java.Boolean;
                          P3_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LayoutPath);
   pragma Export (Java, PointToPath, "pointToPath");
   pragma Export (Java, PathToPoint, "pathToPoint");

end Java.Awt.Font.LayoutPath;
pragma Import (Java, Java.Awt.Font.LayoutPath, "java.awt.font.LayoutPath");
pragma Extensions_Allowed (Off);
