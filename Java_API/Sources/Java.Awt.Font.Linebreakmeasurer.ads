pragma Extensions_Allowed (On);
limited with Java.Awt.Font.FontRenderContext;
limited with Java.Awt.Font.TextLayout;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Text.BreakIterator;
with Java.Lang.Object;

package Java.Awt.Font.LineBreakMeasurer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LineBreakMeasurer (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                                   P2_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_LineBreakMeasurer (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                                   P2_BreakIterator : access Standard.Java.Text.BreakIterator.Typ'Class;
                                   P3_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NextOffset (This : access Typ;
                        P1_Float : Java.Float)
                        return Java.Int;

   function NextOffset (This : access Typ;
                        P1_Float : Java.Float;
                        P2_Int : Java.Int;
                        P3_Boolean : Java.Boolean)
                        return Java.Int;

   function NextLayout (This : access Typ;
                        P1_Float : Java.Float)
                        return access Java.Awt.Font.TextLayout.Typ'Class;

   function NextLayout (This : access Typ;
                        P1_Float : Java.Float;
                        P2_Int : Java.Int;
                        P3_Boolean : Java.Boolean)
                        return access Java.Awt.Font.TextLayout.Typ'Class;

   function GetPosition (This : access Typ)
                         return Java.Int;

   procedure SetPosition (This : access Typ;
                          P1_Int : Java.Int);

   procedure InsertChar (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Int : Java.Int);

   procedure DeleteChar (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LineBreakMeasurer);
   pragma Import (Java, NextOffset, "nextOffset");
   pragma Import (Java, NextLayout, "nextLayout");
   pragma Import (Java, GetPosition, "getPosition");
   pragma Import (Java, SetPosition, "setPosition");
   pragma Import (Java, InsertChar, "insertChar");
   pragma Import (Java, DeleteChar, "deleteChar");

end Java.Awt.Font.LineBreakMeasurer;
pragma Import (Java, Java.Awt.Font.LineBreakMeasurer, "java.awt.font.LineBreakMeasurer");
pragma Extensions_Allowed (Off);
