pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Font.LineMetrics is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_LineMetrics (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNumChars (This : access Typ)
                         return Java.Int is abstract;

   function GetAscent (This : access Typ)
                       return Java.Float is abstract;

   function GetDescent (This : access Typ)
                        return Java.Float is abstract;

   function GetLeading (This : access Typ)
                        return Java.Float is abstract;

   function GetHeight (This : access Typ)
                       return Java.Float is abstract;

   function GetBaselineIndex (This : access Typ)
                              return Java.Int is abstract;

   function GetBaselineOffsets (This : access Typ)
                                return Java.Float_Arr is abstract;

   function GetStrikethroughOffset (This : access Typ)
                                    return Java.Float is abstract;

   function GetStrikethroughThickness (This : access Typ)
                                       return Java.Float is abstract;

   function GetUnderlineOffset (This : access Typ)
                                return Java.Float is abstract;

   function GetUnderlineThickness (This : access Typ)
                                   return Java.Float is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LineMetrics);
   pragma Export (Java, GetNumChars, "getNumChars");
   pragma Export (Java, GetAscent, "getAscent");
   pragma Export (Java, GetDescent, "getDescent");
   pragma Export (Java, GetLeading, "getLeading");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, GetBaselineIndex, "getBaselineIndex");
   pragma Export (Java, GetBaselineOffsets, "getBaselineOffsets");
   pragma Export (Java, GetStrikethroughOffset, "getStrikethroughOffset");
   pragma Export (Java, GetStrikethroughThickness, "getStrikethroughThickness");
   pragma Export (Java, GetUnderlineOffset, "getUnderlineOffset");
   pragma Export (Java, GetUnderlineThickness, "getUnderlineThickness");

end Java.Awt.Font.LineMetrics;
pragma Import (Java, Java.Awt.Font.LineMetrics, "java.awt.font.LineMetrics");
pragma Extensions_Allowed (Off);
