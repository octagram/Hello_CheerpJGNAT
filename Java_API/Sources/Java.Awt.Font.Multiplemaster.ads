pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.Font.MultipleMaster is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNumDesignAxes (This : access Typ)
                              return Java.Int is abstract;

   function GetDesignAxisRanges (This : access Typ)
                                 return Java.Float_Arr is abstract;

   function GetDesignAxisDefaults (This : access Typ)
                                   return Java.Float_Arr is abstract;

   function GetDesignAxisNames (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;

   function DeriveMMFont (This : access Typ;
                          P1_Float_Arr : Java.Float_Arr)
                          return access Java.Awt.Font.Typ'Class is abstract;

   function DeriveMMFont (This : access Typ;
                          P1_Float_Arr : Java.Float_Arr;
                          P2_Float : Java.Float;
                          P3_Float : Java.Float;
                          P4_Float : Java.Float;
                          P5_Float : Java.Float)
                          return access Java.Awt.Font.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetNumDesignAxes, "getNumDesignAxes");
   pragma Export (Java, GetDesignAxisRanges, "getDesignAxisRanges");
   pragma Export (Java, GetDesignAxisDefaults, "getDesignAxisDefaults");
   pragma Export (Java, GetDesignAxisNames, "getDesignAxisNames");
   pragma Export (Java, DeriveMMFont, "deriveMMFont");

end Java.Awt.Font.MultipleMaster;
pragma Import (Java, Java.Awt.Font.MultipleMaster, "java.awt.font.MultipleMaster");
pragma Extensions_Allowed (Off);
