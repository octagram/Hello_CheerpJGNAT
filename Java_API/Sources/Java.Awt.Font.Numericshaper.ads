pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Font.NumericShaper is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetShaper (P1_Int : Java.Int)
                       return access Java.Awt.Font.NumericShaper.Typ'Class;

   function GetContextualShaper (P1_Int : Java.Int)
                                 return access Java.Awt.Font.NumericShaper.Typ'Class;

   function GetContextualShaper (P1_Int : Java.Int;
                                 P2_Int : Java.Int)
                                 return access Java.Awt.Font.NumericShaper.Typ'Class;

   procedure Shape (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);

   procedure Shape (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int);

   function IsContextual (This : access Typ)
                          return Java.Boolean;

   function GetRanges (This : access Typ)
                       return Java.Int;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   EUROPEAN : constant Java.Int;

   --  final
   ARABIC : constant Java.Int;

   --  final
   EASTERN_ARABIC : constant Java.Int;

   --  final
   DEVANAGARI : constant Java.Int;

   --  final
   BENGALI : constant Java.Int;

   --  final
   GURMUKHI : constant Java.Int;

   --  final
   GUJARATI : constant Java.Int;

   --  final
   ORIYA : constant Java.Int;

   --  final
   TAMIL : constant Java.Int;

   --  final
   TELUGU : constant Java.Int;

   --  final
   KANNADA : constant Java.Int;

   --  final
   MALAYALAM : constant Java.Int;

   --  final
   THAI : constant Java.Int;

   --  final
   LAO : constant Java.Int;

   --  final
   TIBETAN : constant Java.Int;

   --  final
   MYANMAR : constant Java.Int;

   --  final
   ETHIOPIC : constant Java.Int;

   --  final
   KHMER : constant Java.Int;

   --  final
   MONGOLIAN : constant Java.Int;

   --  final
   ALL_RANGES : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetShaper, "getShaper");
   pragma Import (Java, GetContextualShaper, "getContextualShaper");
   pragma Import (Java, Shape, "shape");
   pragma Import (Java, IsContextual, "isContextual");
   pragma Import (Java, GetRanges, "getRanges");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, EUROPEAN, "EUROPEAN");
   pragma Import (Java, ARABIC, "ARABIC");
   pragma Import (Java, EASTERN_ARABIC, "EASTERN_ARABIC");
   pragma Import (Java, DEVANAGARI, "DEVANAGARI");
   pragma Import (Java, BENGALI, "BENGALI");
   pragma Import (Java, GURMUKHI, "GURMUKHI");
   pragma Import (Java, GUJARATI, "GUJARATI");
   pragma Import (Java, ORIYA, "ORIYA");
   pragma Import (Java, TAMIL, "TAMIL");
   pragma Import (Java, TELUGU, "TELUGU");
   pragma Import (Java, KANNADA, "KANNADA");
   pragma Import (Java, MALAYALAM, "MALAYALAM");
   pragma Import (Java, THAI, "THAI");
   pragma Import (Java, LAO, "LAO");
   pragma Import (Java, TIBETAN, "TIBETAN");
   pragma Import (Java, MYANMAR, "MYANMAR");
   pragma Import (Java, ETHIOPIC, "ETHIOPIC");
   pragma Import (Java, KHMER, "KHMER");
   pragma Import (Java, MONGOLIAN, "MONGOLIAN");
   pragma Import (Java, ALL_RANGES, "ALL_RANGES");

end Java.Awt.Font.NumericShaper;
pragma Import (Java, Java.Awt.Font.NumericShaper, "java.awt.font.NumericShaper");
pragma Extensions_Allowed (Off);
