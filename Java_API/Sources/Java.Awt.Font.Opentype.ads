pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.Font.OpenType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetVersion (This : access Typ)
                        return Java.Int is abstract;

   function GetFontTable (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Byte_Arr is abstract;

   function GetFontTable (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Byte_Arr is abstract;

   function GetFontTable (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int)
                          return Java.Byte_Arr is abstract;

   function GetFontTable (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int)
                          return Java.Byte_Arr is abstract;

   function GetFontTableSize (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int is abstract;

   function GetFontTableSize (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TAG_CMAP : constant Java.Int;

   --  final
   TAG_HEAD : constant Java.Int;

   --  final
   TAG_NAME : constant Java.Int;

   --  final
   TAG_GLYF : constant Java.Int;

   --  final
   TAG_MAXP : constant Java.Int;

   --  final
   TAG_PREP : constant Java.Int;

   --  final
   TAG_HMTX : constant Java.Int;

   --  final
   TAG_KERN : constant Java.Int;

   --  final
   TAG_HDMX : constant Java.Int;

   --  final
   TAG_LOCA : constant Java.Int;

   --  final
   TAG_POST : constant Java.Int;

   --  final
   TAG_OS2 : constant Java.Int;

   --  final
   TAG_CVT : constant Java.Int;

   --  final
   TAG_GASP : constant Java.Int;

   --  final
   TAG_VDMX : constant Java.Int;

   --  final
   TAG_VMTX : constant Java.Int;

   --  final
   TAG_VHEA : constant Java.Int;

   --  final
   TAG_HHEA : constant Java.Int;

   --  final
   TAG_TYP1 : constant Java.Int;

   --  final
   TAG_BSLN : constant Java.Int;

   --  final
   TAG_GSUB : constant Java.Int;

   --  final
   TAG_DSIG : constant Java.Int;

   --  final
   TAG_FPGM : constant Java.Int;

   --  final
   TAG_FVAR : constant Java.Int;

   --  final
   TAG_GVAR : constant Java.Int;

   --  final
   TAG_CFF : constant Java.Int;

   --  final
   TAG_MMSD : constant Java.Int;

   --  final
   TAG_MMFX : constant Java.Int;

   --  final
   TAG_BASE : constant Java.Int;

   --  final
   TAG_GDEF : constant Java.Int;

   --  final
   TAG_GPOS : constant Java.Int;

   --  final
   TAG_JSTF : constant Java.Int;

   --  final
   TAG_EBDT : constant Java.Int;

   --  final
   TAG_EBLC : constant Java.Int;

   --  final
   TAG_EBSC : constant Java.Int;

   --  final
   TAG_LTSH : constant Java.Int;

   --  final
   TAG_PCLT : constant Java.Int;

   --  final
   TAG_ACNT : constant Java.Int;

   --  final
   TAG_AVAR : constant Java.Int;

   --  final
   TAG_BDAT : constant Java.Int;

   --  final
   TAG_BLOC : constant Java.Int;

   --  final
   TAG_CVAR : constant Java.Int;

   --  final
   TAG_FEAT : constant Java.Int;

   --  final
   TAG_FDSC : constant Java.Int;

   --  final
   TAG_FMTX : constant Java.Int;

   --  final
   TAG_JUST : constant Java.Int;

   --  final
   TAG_LCAR : constant Java.Int;

   --  final
   TAG_MORT : constant Java.Int;

   --  final
   TAG_OPBD : constant Java.Int;

   --  final
   TAG_PROP : constant Java.Int;

   --  final
   TAG_TRAK : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, GetFontTable, "getFontTable");
   pragma Export (Java, GetFontTableSize, "getFontTableSize");
   pragma Import (Java, TAG_CMAP, "TAG_CMAP");
   pragma Import (Java, TAG_HEAD, "TAG_HEAD");
   pragma Import (Java, TAG_NAME, "TAG_NAME");
   pragma Import (Java, TAG_GLYF, "TAG_GLYF");
   pragma Import (Java, TAG_MAXP, "TAG_MAXP");
   pragma Import (Java, TAG_PREP, "TAG_PREP");
   pragma Import (Java, TAG_HMTX, "TAG_HMTX");
   pragma Import (Java, TAG_KERN, "TAG_KERN");
   pragma Import (Java, TAG_HDMX, "TAG_HDMX");
   pragma Import (Java, TAG_LOCA, "TAG_LOCA");
   pragma Import (Java, TAG_POST, "TAG_POST");
   pragma Import (Java, TAG_OS2, "TAG_OS2");
   pragma Import (Java, TAG_CVT, "TAG_CVT");
   pragma Import (Java, TAG_GASP, "TAG_GASP");
   pragma Import (Java, TAG_VDMX, "TAG_VDMX");
   pragma Import (Java, TAG_VMTX, "TAG_VMTX");
   pragma Import (Java, TAG_VHEA, "TAG_VHEA");
   pragma Import (Java, TAG_HHEA, "TAG_HHEA");
   pragma Import (Java, TAG_TYP1, "TAG_TYP1");
   pragma Import (Java, TAG_BSLN, "TAG_BSLN");
   pragma Import (Java, TAG_GSUB, "TAG_GSUB");
   pragma Import (Java, TAG_DSIG, "TAG_DSIG");
   pragma Import (Java, TAG_FPGM, "TAG_FPGM");
   pragma Import (Java, TAG_FVAR, "TAG_FVAR");
   pragma Import (Java, TAG_GVAR, "TAG_GVAR");
   pragma Import (Java, TAG_CFF, "TAG_CFF");
   pragma Import (Java, TAG_MMSD, "TAG_MMSD");
   pragma Import (Java, TAG_MMFX, "TAG_MMFX");
   pragma Import (Java, TAG_BASE, "TAG_BASE");
   pragma Import (Java, TAG_GDEF, "TAG_GDEF");
   pragma Import (Java, TAG_GPOS, "TAG_GPOS");
   pragma Import (Java, TAG_JSTF, "TAG_JSTF");
   pragma Import (Java, TAG_EBDT, "TAG_EBDT");
   pragma Import (Java, TAG_EBLC, "TAG_EBLC");
   pragma Import (Java, TAG_EBSC, "TAG_EBSC");
   pragma Import (Java, TAG_LTSH, "TAG_LTSH");
   pragma Import (Java, TAG_PCLT, "TAG_PCLT");
   pragma Import (Java, TAG_ACNT, "TAG_ACNT");
   pragma Import (Java, TAG_AVAR, "TAG_AVAR");
   pragma Import (Java, TAG_BDAT, "TAG_BDAT");
   pragma Import (Java, TAG_BLOC, "TAG_BLOC");
   pragma Import (Java, TAG_CVAR, "TAG_CVAR");
   pragma Import (Java, TAG_FEAT, "TAG_FEAT");
   pragma Import (Java, TAG_FDSC, "TAG_FDSC");
   pragma Import (Java, TAG_FMTX, "TAG_FMTX");
   pragma Import (Java, TAG_JUST, "TAG_JUST");
   pragma Import (Java, TAG_LCAR, "TAG_LCAR");
   pragma Import (Java, TAG_MORT, "TAG_MORT");
   pragma Import (Java, TAG_OPBD, "TAG_OPBD");
   pragma Import (Java, TAG_PROP, "TAG_PROP");
   pragma Import (Java, TAG_TRAK, "TAG_TRAK");

end Java.Awt.Font.OpenType;
pragma Import (Java, Java.Awt.Font.OpenType, "java.awt.font.OpenType");
pragma Extensions_Allowed (Off);
