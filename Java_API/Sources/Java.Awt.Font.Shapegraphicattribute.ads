pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.Shape;
with Java.Awt.Font.GraphicAttribute;
with Java.Lang.Object;

package Java.Awt.Font.ShapeGraphicAttribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Font.GraphicAttribute.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ShapeGraphicAttribute (P1_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Boolean : Java.Boolean; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAscent (This : access Typ)
                       return Java.Float;

   function GetDescent (This : access Typ)
                        return Java.Float;

   function GetAdvance (This : access Typ)
                        return Java.Float;

   procedure Draw (This : access Typ;
                   P1_Graphics2D : access Standard.Java.Awt.Graphics2D.Typ'Class;
                   P2_Float : Java.Float;
                   P3_Float : Java.Float);

   function GetBounds (This : access Typ)
                       return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetOutline (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                        return access Java.Awt.Shape.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_ShapeGraphicAttribute : access Standard.Java.Awt.Font.ShapeGraphicAttribute.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   STROKE : constant Java.Boolean;

   --  final
   FILL : constant Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ShapeGraphicAttribute);
   pragma Import (Java, GetAscent, "getAscent");
   pragma Import (Java, GetDescent, "getDescent");
   pragma Import (Java, GetAdvance, "getAdvance");
   pragma Import (Java, Draw, "draw");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetOutline, "getOutline");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, STROKE, "STROKE");
   pragma Import (Java, FILL, "FILL");

end Java.Awt.Font.ShapeGraphicAttribute;
pragma Import (Java, Java.Awt.Font.ShapeGraphicAttribute, "java.awt.font.ShapeGraphicAttribute");
pragma Extensions_Allowed (Off);
