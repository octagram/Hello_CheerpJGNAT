pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Float;
limited with Java.Lang.Integer;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Text.AttributedCharacterIterator.Attribute;

package Java.Awt.Font.TextAttribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Text.AttributedCharacterIterator.Attribute.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_TextAttribute (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.InvalidObjectException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FAMILY : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   WEIGHT : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   WEIGHT_EXTRA_LIGHT : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_LIGHT : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_DEMILIGHT : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_REGULAR : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_SEMIBOLD : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_MEDIUM : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_DEMIBOLD : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_BOLD : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_HEAVY : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_EXTRABOLD : access Java.Lang.Float.Typ'Class;

   --  final
   WEIGHT_ULTRABOLD : access Java.Lang.Float.Typ'Class;

   --  final
   WIDTH : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   WIDTH_CONDENSED : access Java.Lang.Float.Typ'Class;

   --  final
   WIDTH_SEMI_CONDENSED : access Java.Lang.Float.Typ'Class;

   --  final
   WIDTH_REGULAR : access Java.Lang.Float.Typ'Class;

   --  final
   WIDTH_SEMI_EXTENDED : access Java.Lang.Float.Typ'Class;

   --  final
   WIDTH_EXTENDED : access Java.Lang.Float.Typ'Class;

   --  final
   POSTURE : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   POSTURE_REGULAR : access Java.Lang.Float.Typ'Class;

   --  final
   POSTURE_OBLIQUE : access Java.Lang.Float.Typ'Class;

   --  final
   SIZE : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   TRANSFORM : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   SUPERSCRIPT : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   SUPERSCRIPT_SUPER : access Java.Lang.Integer.Typ'Class;

   --  final
   SUPERSCRIPT_SUB : access Java.Lang.Integer.Typ'Class;

   --  final
   FONT : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   CHAR_REPLACEMENT : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   FOREGROUND : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   BACKGROUND : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   UNDERLINE : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   UNDERLINE_ON : access Java.Lang.Integer.Typ'Class;

   --  final
   STRIKETHROUGH : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   STRIKETHROUGH_ON : access Java.Lang.Boolean.Typ'Class;

   --  final
   RUN_DIRECTION : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   RUN_DIRECTION_LTR : access Java.Lang.Boolean.Typ'Class;

   --  final
   RUN_DIRECTION_RTL : access Java.Lang.Boolean.Typ'Class;

   --  final
   BIDI_EMBEDDING : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   JUSTIFICATION : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   JUSTIFICATION_FULL : access Java.Lang.Float.Typ'Class;

   --  final
   JUSTIFICATION_NONE : access Java.Lang.Float.Typ'Class;

   --  final
   INPUT_METHOD_HIGHLIGHT : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   INPUT_METHOD_UNDERLINE : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   UNDERLINE_LOW_ONE_PIXEL : access Java.Lang.Integer.Typ'Class;

   --  final
   UNDERLINE_LOW_TWO_PIXEL : access Java.Lang.Integer.Typ'Class;

   --  final
   UNDERLINE_LOW_DOTTED : access Java.Lang.Integer.Typ'Class;

   --  final
   UNDERLINE_LOW_GRAY : access Java.Lang.Integer.Typ'Class;

   --  final
   UNDERLINE_LOW_DASHED : access Java.Lang.Integer.Typ'Class;

   --  final
   SWAP_COLORS : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   SWAP_COLORS_ON : access Java.Lang.Boolean.Typ'Class;

   --  final
   NUMERIC_SHAPING : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   KERNING : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   KERNING_ON : access Java.Lang.Integer.Typ'Class;

   --  final
   LIGATURES : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   LIGATURES_ON : access Java.Lang.Integer.Typ'Class;

   --  final
   TRACKING : access Java.Awt.Font.TextAttribute.Typ'Class;

   --  final
   TRACKING_TIGHT : access Java.Lang.Float.Typ'Class;

   --  final
   TRACKING_LOOSE : access Java.Lang.Float.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TextAttribute);
   pragma Import (Java, ReadResolve, "readResolve");
   pragma Import (Java, FAMILY, "FAMILY");
   pragma Import (Java, WEIGHT, "WEIGHT");
   pragma Import (Java, WEIGHT_EXTRA_LIGHT, "WEIGHT_EXTRA_LIGHT");
   pragma Import (Java, WEIGHT_LIGHT, "WEIGHT_LIGHT");
   pragma Import (Java, WEIGHT_DEMILIGHT, "WEIGHT_DEMILIGHT");
   pragma Import (Java, WEIGHT_REGULAR, "WEIGHT_REGULAR");
   pragma Import (Java, WEIGHT_SEMIBOLD, "WEIGHT_SEMIBOLD");
   pragma Import (Java, WEIGHT_MEDIUM, "WEIGHT_MEDIUM");
   pragma Import (Java, WEIGHT_DEMIBOLD, "WEIGHT_DEMIBOLD");
   pragma Import (Java, WEIGHT_BOLD, "WEIGHT_BOLD");
   pragma Import (Java, WEIGHT_HEAVY, "WEIGHT_HEAVY");
   pragma Import (Java, WEIGHT_EXTRABOLD, "WEIGHT_EXTRABOLD");
   pragma Import (Java, WEIGHT_ULTRABOLD, "WEIGHT_ULTRABOLD");
   pragma Import (Java, WIDTH, "WIDTH");
   pragma Import (Java, WIDTH_CONDENSED, "WIDTH_CONDENSED");
   pragma Import (Java, WIDTH_SEMI_CONDENSED, "WIDTH_SEMI_CONDENSED");
   pragma Import (Java, WIDTH_REGULAR, "WIDTH_REGULAR");
   pragma Import (Java, WIDTH_SEMI_EXTENDED, "WIDTH_SEMI_EXTENDED");
   pragma Import (Java, WIDTH_EXTENDED, "WIDTH_EXTENDED");
   pragma Import (Java, POSTURE, "POSTURE");
   pragma Import (Java, POSTURE_REGULAR, "POSTURE_REGULAR");
   pragma Import (Java, POSTURE_OBLIQUE, "POSTURE_OBLIQUE");
   pragma Import (Java, SIZE, "SIZE");
   pragma Import (Java, TRANSFORM, "TRANSFORM");
   pragma Import (Java, SUPERSCRIPT, "SUPERSCRIPT");
   pragma Import (Java, SUPERSCRIPT_SUPER, "SUPERSCRIPT_SUPER");
   pragma Import (Java, SUPERSCRIPT_SUB, "SUPERSCRIPT_SUB");
   pragma Import (Java, FONT, "FONT");
   pragma Import (Java, CHAR_REPLACEMENT, "CHAR_REPLACEMENT");
   pragma Import (Java, FOREGROUND, "FOREGROUND");
   pragma Import (Java, BACKGROUND, "BACKGROUND");
   pragma Import (Java, UNDERLINE, "UNDERLINE");
   pragma Import (Java, UNDERLINE_ON, "UNDERLINE_ON");
   pragma Import (Java, STRIKETHROUGH, "STRIKETHROUGH");
   pragma Import (Java, STRIKETHROUGH_ON, "STRIKETHROUGH_ON");
   pragma Import (Java, RUN_DIRECTION, "RUN_DIRECTION");
   pragma Import (Java, RUN_DIRECTION_LTR, "RUN_DIRECTION_LTR");
   pragma Import (Java, RUN_DIRECTION_RTL, "RUN_DIRECTION_RTL");
   pragma Import (Java, BIDI_EMBEDDING, "BIDI_EMBEDDING");
   pragma Import (Java, JUSTIFICATION, "JUSTIFICATION");
   pragma Import (Java, JUSTIFICATION_FULL, "JUSTIFICATION_FULL");
   pragma Import (Java, JUSTIFICATION_NONE, "JUSTIFICATION_NONE");
   pragma Import (Java, INPUT_METHOD_HIGHLIGHT, "INPUT_METHOD_HIGHLIGHT");
   pragma Import (Java, INPUT_METHOD_UNDERLINE, "INPUT_METHOD_UNDERLINE");
   pragma Import (Java, UNDERLINE_LOW_ONE_PIXEL, "UNDERLINE_LOW_ONE_PIXEL");
   pragma Import (Java, UNDERLINE_LOW_TWO_PIXEL, "UNDERLINE_LOW_TWO_PIXEL");
   pragma Import (Java, UNDERLINE_LOW_DOTTED, "UNDERLINE_LOW_DOTTED");
   pragma Import (Java, UNDERLINE_LOW_GRAY, "UNDERLINE_LOW_GRAY");
   pragma Import (Java, UNDERLINE_LOW_DASHED, "UNDERLINE_LOW_DASHED");
   pragma Import (Java, SWAP_COLORS, "SWAP_COLORS");
   pragma Import (Java, SWAP_COLORS_ON, "SWAP_COLORS_ON");
   pragma Import (Java, NUMERIC_SHAPING, "NUMERIC_SHAPING");
   pragma Import (Java, KERNING, "KERNING");
   pragma Import (Java, KERNING_ON, "KERNING_ON");
   pragma Import (Java, LIGATURES, "LIGATURES");
   pragma Import (Java, LIGATURES_ON, "LIGATURES_ON");
   pragma Import (Java, TRACKING, "TRACKING");
   pragma Import (Java, TRACKING_TIGHT, "TRACKING_TIGHT");
   pragma Import (Java, TRACKING_LOOSE, "TRACKING_LOOSE");

end Java.Awt.Font.TextAttribute;
pragma Import (Java, Java.Awt.Font.TextAttribute, "java.awt.font.TextAttribute");
pragma Extensions_Allowed (Off);
