pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.Font.TextHitInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCharIndex (This : access Typ)
                          return Java.Int;

   function IsLeadingEdge (This : access Typ)
                           return Java.Boolean;

   function GetInsertionIndex (This : access Typ)
                               return Java.Int;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Leading (P1_Int : Java.Int)
                     return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function Trailing (P1_Int : Java.Int)
                      return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function BeforeOffset (P1_Int : Java.Int)
                          return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function AfterOffset (P1_Int : Java.Int)
                         return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetOtherHit (This : access Typ)
                         return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetOffsetHit (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Awt.Font.TextHitInfo.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetCharIndex, "getCharIndex");
   pragma Import (Java, IsLeadingEdge, "isLeadingEdge");
   pragma Import (Java, GetInsertionIndex, "getInsertionIndex");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Leading, "leading");
   pragma Import (Java, Trailing, "trailing");
   pragma Import (Java, BeforeOffset, "beforeOffset");
   pragma Import (Java, AfterOffset, "afterOffset");
   pragma Import (Java, GetOtherHit, "getOtherHit");
   pragma Import (Java, GetOffsetHit, "getOffsetHit");

end Java.Awt.Font.TextHitInfo;
pragma Import (Java, Java.Awt.Font.TextHitInfo, "java.awt.font.TextHitInfo");
pragma Extensions_Allowed (Off);
