pragma Extensions_Allowed (On);
limited with Java.Awt.Font.TextHitInfo;
with Java.Lang.Object;

package Java.Awt.Font.TextLayout.CaretPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CaretPolicy (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStrongCaret (This : access Typ;
                            P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                            P2_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                            P3_TextLayout : access Standard.Java.Awt.Font.TextLayout.Typ'Class)
                            return access Java.Awt.Font.TextHitInfo.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CaretPolicy);
   pragma Import (Java, GetStrongCaret, "getStrongCaret");

end Java.Awt.Font.TextLayout.CaretPolicy;
pragma Import (Java, Java.Awt.Font.TextLayout.CaretPolicy, "java.awt.font.TextLayout$CaretPolicy");
pragma Extensions_Allowed (Off);
