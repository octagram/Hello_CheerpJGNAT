pragma Extensions_Allowed (On);
limited with Java.Awt.Font.FontRenderContext;
limited with Java.Awt.Font.LayoutPath;
limited with Java.Awt.Font.TextHitInfo;
limited with Java.Awt.Font.TextLayout.CaretPolicy;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Util.Map;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Font.TextLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TextLayout (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Font : access Standard.Java.Awt.Font.Typ'Class;
                            P3_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_TextLayout (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Map : access Standard.Java.Util.Map.Typ'Class;
                            P3_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_TextLayout (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                            P2_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetJustifiedLayout (This : access Typ;
                                P1_Float : Java.Float)
                                return access Java.Awt.Font.TextLayout.Typ'Class;

   --  protected
   procedure HandleJustify (This : access Typ;
                            P1_Float : Java.Float);

   function GetBaseline (This : access Typ)
                         return Java.Byte;

   function GetBaselineOffsets (This : access Typ)
                                return Java.Float_Arr;

   function GetAdvance (This : access Typ)
                        return Java.Float;

   function GetVisibleAdvance (This : access Typ)
                               return Java.Float;

   function GetAscent (This : access Typ)
                       return Java.Float;

   function GetDescent (This : access Typ)
                        return Java.Float;

   function GetLeading (This : access Typ)
                        return Java.Float;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetPixelBounds (This : access Typ;
                            P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                            P2_Float : Java.Float;
                            P3_Float : Java.Float)
                            return access Java.Awt.Rectangle.Typ'Class;

   function IsLeftToRight (This : access Typ)
                           return Java.Boolean;

   function IsVertical (This : access Typ)
                        return Java.Boolean;

   function GetCharacterCount (This : access Typ)
                               return Java.Int;

   function GetCaretInfo (This : access Typ;
                          P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                          P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                          return Java.Float_Arr;

   function GetCaretInfo (This : access Typ;
                          P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                          return Java.Float_Arr;

   function GetNextRightHit (This : access Typ;
                             P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                             return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetNextRightHit (This : access Typ;
                             P1_Int : Java.Int;
                             P2_CaretPolicy : access Standard.Java.Awt.Font.TextLayout.CaretPolicy.Typ'Class)
                             return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetNextRightHit (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetNextLeftHit (This : access Typ;
                            P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                            return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetNextLeftHit (This : access Typ;
                            P1_Int : Java.Int;
                            P2_CaretPolicy : access Standard.Java.Awt.Font.TextLayout.CaretPolicy.Typ'Class)
                            return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetNextLeftHit (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetVisualOtherHit (This : access Typ;
                               P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                               return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function GetCaretShape (This : access Typ;
                           P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                           P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                           return access Java.Awt.Shape.Typ'Class;

   function GetCaretShape (This : access Typ;
                           P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                           return access Java.Awt.Shape.Typ'Class;

   function GetCharacterLevel (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Byte;

   function GetCaretShapes (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                            P3_CaretPolicy : access Standard.Java.Awt.Font.TextLayout.CaretPolicy.Typ'Class)
                            return Standard.Java.Lang.Object.Ref;

   function GetCaretShapes (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                            return Standard.Java.Lang.Object.Ref;

   function GetCaretShapes (This : access Typ;
                            P1_Int : Java.Int)
                            return Standard.Java.Lang.Object.Ref;

   function GetLogicalRangesForVisualSelection (This : access Typ;
                                                P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                                P2_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                                                return Java.Int_Arr;

   function GetVisualHighlightShape (This : access Typ;
                                     P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                     P2_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                     P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                                     return access Java.Awt.Shape.Typ'Class;

   function GetVisualHighlightShape (This : access Typ;
                                     P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                     P2_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                                     return access Java.Awt.Shape.Typ'Class;

   function GetLogicalHighlightShape (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int;
                                      P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                                      return access Java.Awt.Shape.Typ'Class;

   function GetLogicalHighlightShape (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int)
                                      return access Java.Awt.Shape.Typ'Class;

   function GetBlackBoxBounds (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return access Java.Awt.Shape.Typ'Class;

   function HitTestChar (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                         return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function HitTestChar (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float)
                         return access Java.Awt.Font.TextHitInfo.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_TextLayout : access Standard.Java.Awt.Font.TextLayout.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Draw (This : access Typ;
                   P1_Graphics2D : access Standard.Java.Awt.Graphics2D.Typ'Class;
                   P2_Float : Java.Float;
                   P3_Float : Java.Float);

   function GetOutline (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                        return access Java.Awt.Shape.Typ'Class;

   function GetLayoutPath (This : access Typ)
                           return access Java.Awt.Font.LayoutPath.Typ'Class;

   procedure HitToPoint (This : access Typ;
                         P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                         P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_CARET_POLICY : access Java.Awt.Font.TextLayout.CaretPolicy.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TextLayout);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetJustifiedLayout, "getJustifiedLayout");
   pragma Import (Java, HandleJustify, "handleJustify");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineOffsets, "getBaselineOffsets");
   pragma Import (Java, GetAdvance, "getAdvance");
   pragma Import (Java, GetVisibleAdvance, "getVisibleAdvance");
   pragma Import (Java, GetAscent, "getAscent");
   pragma Import (Java, GetDescent, "getDescent");
   pragma Import (Java, GetLeading, "getLeading");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetPixelBounds, "getPixelBounds");
   pragma Import (Java, IsLeftToRight, "isLeftToRight");
   pragma Import (Java, IsVertical, "isVertical");
   pragma Import (Java, GetCharacterCount, "getCharacterCount");
   pragma Import (Java, GetCaretInfo, "getCaretInfo");
   pragma Import (Java, GetNextRightHit, "getNextRightHit");
   pragma Import (Java, GetNextLeftHit, "getNextLeftHit");
   pragma Import (Java, GetVisualOtherHit, "getVisualOtherHit");
   pragma Import (Java, GetCaretShape, "getCaretShape");
   pragma Import (Java, GetCharacterLevel, "getCharacterLevel");
   pragma Import (Java, GetCaretShapes, "getCaretShapes");
   pragma Import (Java, GetLogicalRangesForVisualSelection, "getLogicalRangesForVisualSelection");
   pragma Import (Java, GetVisualHighlightShape, "getVisualHighlightShape");
   pragma Import (Java, GetLogicalHighlightShape, "getLogicalHighlightShape");
   pragma Import (Java, GetBlackBoxBounds, "getBlackBoxBounds");
   pragma Import (Java, HitTestChar, "hitTestChar");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Draw, "draw");
   pragma Import (Java, GetOutline, "getOutline");
   pragma Import (Java, GetLayoutPath, "getLayoutPath");
   pragma Import (Java, HitToPoint, "hitToPoint");
   pragma Import (Java, DEFAULT_CARET_POLICY, "DEFAULT_CARET_POLICY");

end Java.Awt.Font.TextLayout;
pragma Import (Java, Java.Awt.Font.TextLayout, "java.awt.font.TextLayout");
pragma Extensions_Allowed (Off);
