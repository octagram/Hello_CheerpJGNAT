pragma Extensions_Allowed (On);
limited with Java.Awt.Font.FontRenderContext;
limited with Java.Awt.Font.TextLayout;
limited with Java.Text.AttributedCharacterIterator;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Font.TextMeasurer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TextMeasurer (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                              P2_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetLineBreakIndex (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Float : Java.Float)
                               return Java.Int;

   function GetAdvanceBetween (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return Java.Float;

   function GetLayout (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return access Java.Awt.Font.TextLayout.Typ'Class;

   procedure InsertChar (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Int : Java.Int);

   procedure DeleteChar (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TextMeasurer);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetLineBreakIndex, "getLineBreakIndex");
   pragma Import (Java, GetAdvanceBetween, "getAdvanceBetween");
   pragma Import (Java, GetLayout, "getLayout");
   pragma Import (Java, InsertChar, "insertChar");
   pragma Import (Java, DeleteChar, "deleteChar");

end Java.Awt.Font.TextMeasurer;
pragma Import (Java, Java.Awt.Font.TextMeasurer, "java.awt.font.TextMeasurer");
pragma Extensions_Allowed (Off);
