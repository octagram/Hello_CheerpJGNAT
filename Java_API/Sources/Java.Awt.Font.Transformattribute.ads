pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Font.TransformAttribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TransformAttribute (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransform (This : access Typ)
                          return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function IsIdentity (This : access Typ)
                        return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   IDENTITY : access Java.Awt.Font.TransformAttribute.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TransformAttribute);
   pragma Import (Java, GetTransform, "getTransform");
   pragma Import (Java, IsIdentity, "isIdentity");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, IDENTITY, "IDENTITY");

end Java.Awt.Font.TransformAttribute;
pragma Import (Java, Java.Awt.Font.TransformAttribute, "java.awt.font.TransformAttribute");
pragma Extensions_Allowed (Off);
