pragma Extensions_Allowed (On);
limited with Java.Awt.Font.FontRenderContext;
limited with Java.Awt.Font.GlyphVector;
limited with Java.Awt.Font.LineMetrics;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator.Attribute;
limited with Java.Text.CharacterIterator;
limited with Java.Util.Locale;
limited with Java.Util.Map;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Font is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      --  protected
      Style : Java.Int;
      pragma Import (Java, Style, "style");

      --  protected
      Size : Java.Int;
      pragma Import (Java, Size, "size");

      --  protected
      PointSize : Java.Float;
      pragma Import (Java, PointSize, "pointSize");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Font (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int; 
                      This : Ref := null)
                      return Ref;

   function New_Font (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   --  protected
   function New_Font (P1_Font : access Standard.Java.Awt.Font.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFont (P1_Map : access Standard.Java.Util.Map.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function CreateFont (P1_Int : Java.Int;
                        P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                        return access Java.Awt.Font.Typ'Class;
   --  can raise Java.Awt.FontFormatException.Except and
   --  Java.Io.IOException.Except

   function CreateFont (P1_Int : Java.Int;
                        P2_File : access Standard.Java.Io.File.Typ'Class)
                        return access Java.Awt.Font.Typ'Class;
   --  can raise Java.Awt.FontFormatException.Except and
   --  Java.Io.IOException.Except

   function GetTransform (This : access Typ)
                          return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetFamily (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetFamily (This : access Typ;
                       P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function GetPSName (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetFontName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetFontName (This : access Typ;
                         P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function GetStyle (This : access Typ)
                      return Java.Int;

   function GetSize (This : access Typ)
                     return Java.Int;

   function GetSize2D (This : access Typ)
                       return Java.Float;

   function IsPlain (This : access Typ)
                     return Java.Boolean;

   function IsBold (This : access Typ)
                    return Java.Boolean;

   function IsItalic (This : access Typ)
                      return Java.Boolean;

   function IsTransformed (This : access Typ)
                           return Java.Boolean;

   function HasLayoutAttributes (This : access Typ)
                                 return Java.Boolean;

   function GetFont (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function Decode (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Awt.Font.Typ'Class;

   function GetFont (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Font : access Standard.Java.Awt.Font.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetNumGlyphs (This : access Typ)
                          return Java.Int;

   function GetMissingGlyphCode (This : access Typ)
                                 return Java.Int;

   function GetBaselineFor (This : access Typ;
                            P1_Char : Java.Char)
                            return Java.Byte;

   function GetAttributes (This : access Typ)
                           return access Java.Util.Map.Typ'Class;

   function GetAvailableAttributes (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function DeriveFont (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Float : Java.Float)
                        return access Java.Awt.Font.Typ'Class;

   function DeriveFont (This : access Typ;
                        P1_Int : Java.Int;
                        P2_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                        return access Java.Awt.Font.Typ'Class;

   function DeriveFont (This : access Typ;
                        P1_Float : Java.Float)
                        return access Java.Awt.Font.Typ'Class;

   function DeriveFont (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                        return access Java.Awt.Font.Typ'Class;

   function DeriveFont (This : access Typ;
                        P1_Int : Java.Int)
                        return access Java.Awt.Font.Typ'Class;

   function DeriveFont (This : access Typ;
                        P1_Map : access Standard.Java.Util.Map.Typ'Class)
                        return access Java.Awt.Font.Typ'Class;

   function CanDisplay (This : access Typ;
                        P1_Char : Java.Char)
                        return Java.Boolean;

   function CanDisplay (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   function CanDisplayUpTo (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Int;

   function CanDisplayUpTo (This : access Typ;
                            P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return Java.Int;

   function CanDisplayUpTo (This : access Typ;
                            P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return Java.Int;

   function GetItalicAngle (This : access Typ)
                            return Java.Float;

   function HasUniformLineMetrics (This : access Typ)
                                   return Java.Boolean;

   function GetLineMetrics (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetLineMetrics (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetLineMetrics (This : access Typ;
                            P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetLineMetrics (This : access Typ;
                            P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_Char_Arr : Java.Char_Arr;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetMaxCharBounds (This : access Typ;
                              P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class)
                              return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function CreateGlyphVector (This : access Typ;
                               P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Awt.Font.GlyphVector.Typ'Class;

   function CreateGlyphVector (This : access Typ;
                               P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                               P2_Char_Arr : Java.Char_Arr)
                               return access Java.Awt.Font.GlyphVector.Typ'Class;

   function CreateGlyphVector (This : access Typ;
                               P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                               P2_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class)
                               return access Java.Awt.Font.GlyphVector.Typ'Class;

   function CreateGlyphVector (This : access Typ;
                               P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                               P2_Int_Arr : Java.Int_Arr)
                               return access Java.Awt.Font.GlyphVector.Typ'Class;

   function LayoutGlyphVector (This : access Typ;
                               P1_FontRenderContext : access Standard.Java.Awt.Font.FontRenderContext.Typ'Class;
                               P2_Char_Arr : Java.Char_Arr;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int)
                               return access Java.Awt.Font.GlyphVector.Typ'Class;

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Lang.Throwable.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DIALOG : constant access Java.Lang.String.Typ'Class;

   --  final
   DIALOG_INPUT : constant access Java.Lang.String.Typ'Class;

   --  final
   SANS_SERIF : constant access Java.Lang.String.Typ'Class;

   --  final
   SERIF : constant access Java.Lang.String.Typ'Class;

   --  final
   MONOSPACED : constant access Java.Lang.String.Typ'Class;

   --  final
   PLAIN : constant Java.Int;

   --  final
   BOLD : constant Java.Int;

   --  final
   ITALIC : constant Java.Int;

   --  final
   ROMAN_BASELINE : constant Java.Int;

   --  final
   CENTER_BASELINE : constant Java.Int;

   --  final
   HANGING_BASELINE : constant Java.Int;

   --  final
   TRUETYPE_FONT : constant Java.Int;

   --  final
   TYPE1_FONT : constant Java.Int;

   --  final
   LAYOUT_LEFT_TO_RIGHT : constant Java.Int;

   --  final
   LAYOUT_RIGHT_TO_LEFT : constant Java.Int;

   --  final
   LAYOUT_NO_START_CONTEXT : constant Java.Int;

   --  final
   LAYOUT_NO_LIMIT_CONTEXT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Font);
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, CreateFont, "createFont");
   pragma Import (Java, GetTransform, "getTransform");
   pragma Import (Java, GetFamily, "getFamily");
   pragma Import (Java, GetPSName, "getPSName");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetFontName, "getFontName");
   pragma Import (Java, GetStyle, "getStyle");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, GetSize2D, "getSize2D");
   pragma Import (Java, IsPlain, "isPlain");
   pragma Import (Java, IsBold, "isBold");
   pragma Import (Java, IsItalic, "isItalic");
   pragma Import (Java, IsTransformed, "isTransformed");
   pragma Import (Java, HasLayoutAttributes, "hasLayoutAttributes");
   pragma Import (Java, Decode, "decode");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetNumGlyphs, "getNumGlyphs");
   pragma Import (Java, GetMissingGlyphCode, "getMissingGlyphCode");
   pragma Import (Java, GetBaselineFor, "getBaselineFor");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetAvailableAttributes, "getAvailableAttributes");
   pragma Import (Java, DeriveFont, "deriveFont");
   pragma Import (Java, CanDisplay, "canDisplay");
   pragma Import (Java, CanDisplayUpTo, "canDisplayUpTo");
   pragma Import (Java, GetItalicAngle, "getItalicAngle");
   pragma Import (Java, HasUniformLineMetrics, "hasUniformLineMetrics");
   pragma Import (Java, GetLineMetrics, "getLineMetrics");
   pragma Import (Java, GetStringBounds, "getStringBounds");
   pragma Import (Java, GetMaxCharBounds, "getMaxCharBounds");
   pragma Import (Java, CreateGlyphVector, "createGlyphVector");
   pragma Import (Java, LayoutGlyphVector, "layoutGlyphVector");
   pragma Import (Java, Finalize, "finalize");
   pragma Import (Java, DIALOG, "DIALOG");
   pragma Import (Java, DIALOG_INPUT, "DIALOG_INPUT");
   pragma Import (Java, SANS_SERIF, "SANS_SERIF");
   pragma Import (Java, SERIF, "SERIF");
   pragma Import (Java, MONOSPACED, "MONOSPACED");
   pragma Import (Java, PLAIN, "PLAIN");
   pragma Import (Java, BOLD, "BOLD");
   pragma Import (Java, ITALIC, "ITALIC");
   pragma Import (Java, ROMAN_BASELINE, "ROMAN_BASELINE");
   pragma Import (Java, CENTER_BASELINE, "CENTER_BASELINE");
   pragma Import (Java, HANGING_BASELINE, "HANGING_BASELINE");
   pragma Import (Java, TRUETYPE_FONT, "TRUETYPE_FONT");
   pragma Import (Java, TYPE1_FONT, "TYPE1_FONT");
   pragma Import (Java, LAYOUT_LEFT_TO_RIGHT, "LAYOUT_LEFT_TO_RIGHT");
   pragma Import (Java, LAYOUT_RIGHT_TO_LEFT, "LAYOUT_RIGHT_TO_LEFT");
   pragma Import (Java, LAYOUT_NO_START_CONTEXT, "LAYOUT_NO_START_CONTEXT");
   pragma Import (Java, LAYOUT_NO_LIMIT_CONTEXT, "LAYOUT_NO_LIMIT_CONTEXT");

end Java.Awt.Font;
pragma Import (Java, Java.Awt.Font, "java.awt.Font");
pragma Extensions_Allowed (Off);
