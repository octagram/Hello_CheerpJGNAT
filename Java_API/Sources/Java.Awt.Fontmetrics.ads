pragma Extensions_Allowed (On);
limited with Java.Awt.Font;
limited with Java.Awt.Font.FontRenderContext;
limited with Java.Awt.Font.LineMetrics;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Graphics;
limited with Java.Lang.String;
limited with Java.Text.CharacterIterator;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.FontMetrics is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Font : access Java.Awt.Font.Typ'Class;
      pragma Import (Java, Font, "font");

   end record;

   --  protected
   function New_FontMetrics (P1_Font : access Standard.Java.Awt.Font.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   function GetFontRenderContext (This : access Typ)
                                  return access Java.Awt.Font.FontRenderContext.Typ'Class;

   function GetLeading (This : access Typ)
                        return Java.Int;

   function GetAscent (This : access Typ)
                       return Java.Int;

   function GetDescent (This : access Typ)
                        return Java.Int;

   function GetHeight (This : access Typ)
                       return Java.Int;

   function GetMaxAscent (This : access Typ)
                          return Java.Int;

   function GetMaxDescent (This : access Typ)
                           return Java.Int;

   function GetMaxAdvance (This : access Typ)
                           return Java.Int;

   function CharWidth (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int;

   function CharWidth (This : access Typ;
                       P1_Char : Java.Char)
                       return Java.Int;

   function StringWidth (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Int;

   function CharsWidth (This : access Typ;
                        P1_Char_Arr : Java.Char_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int)
                        return Java.Int;

   function BytesWidth (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int)
                        return Java.Int;

   function GetWidths (This : access Typ)
                       return Java.Int_Arr;

   function HasUniformLineMetrics (This : access Typ)
                                   return Java.Boolean;

   function GetLineMetrics (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetLineMetrics (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetLineMetrics (This : access Typ;
                            P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetLineMetrics (This : access Typ;
                            P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                            return access Java.Awt.Font.LineMetrics.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_Char_Arr : Java.Char_Arr;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetStringBounds (This : access Typ;
                             P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                             return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetMaxCharBounds (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                              return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FontMetrics);
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, GetFontRenderContext, "getFontRenderContext");
   pragma Import (Java, GetLeading, "getLeading");
   pragma Import (Java, GetAscent, "getAscent");
   pragma Import (Java, GetDescent, "getDescent");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetMaxAscent, "getMaxAscent");
   pragma Import (Java, GetMaxDescent, "getMaxDescent");
   pragma Import (Java, GetMaxAdvance, "getMaxAdvance");
   pragma Import (Java, CharWidth, "charWidth");
   pragma Import (Java, StringWidth, "stringWidth");
   pragma Import (Java, CharsWidth, "charsWidth");
   pragma Import (Java, BytesWidth, "bytesWidth");
   pragma Import (Java, GetWidths, "getWidths");
   pragma Import (Java, HasUniformLineMetrics, "hasUniformLineMetrics");
   pragma Import (Java, GetLineMetrics, "getLineMetrics");
   pragma Import (Java, GetStringBounds, "getStringBounds");
   pragma Import (Java, GetMaxCharBounds, "getMaxCharBounds");
   pragma Import (Java, ToString, "toString");

end Java.Awt.FontMetrics;
pragma Import (Java, Java.Awt.FontMetrics, "java.awt.FontMetrics");
pragma Extensions_Allowed (Off);
