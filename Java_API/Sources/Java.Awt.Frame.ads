pragma Extensions_Allowed (On);
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.MenuBar;
limited with Java.Awt.MenuComponent;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Awt.Window;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Java.Awt.Image;

package Java.Awt.Frame is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Window.Typ(MenuContainer_I,
                               ImageObserver_I,
                               Serializable_I,
                               Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Frame (This : Ref := null)
                       return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Frame (P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_Frame (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Frame (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetTitle (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetTitle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetIconImage (This : access Typ)
                          return access Java.Awt.Image.Typ'Class;

   procedure SetIconImage (This : access Typ;
                           P1_Image : access Standard.Java.Awt.Image.Typ'Class);

   function GetMenuBar (This : access Typ)
                        return access Java.Awt.MenuBar.Typ'Class;

   procedure SetMenuBar (This : access Typ;
                         P1_MenuBar : access Standard.Java.Awt.MenuBar.Typ'Class);

   function IsResizable (This : access Typ)
                         return Java.Boolean;

   procedure SetResizable (This : access Typ;
                           P1_Boolean : Java.Boolean);

   --  synchronized
   procedure SetState (This : access Typ;
                       P1_Int : Java.Int);

   procedure SetExtendedState (This : access Typ;
                               P1_Int : Java.Int);

   --  synchronized
   function GetState (This : access Typ)
                      return Java.Int;

   function GetExtendedState (This : access Typ)
                              return Java.Int;

   --  synchronized
   procedure SetMaximizedBounds (This : access Typ;
                                 P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetMaximizedBounds (This : access Typ)
                                return access Java.Awt.Rectangle.Typ'Class;

   procedure SetUndecorated (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function IsUndecorated (This : access Typ)
                           return Java.Boolean;

   procedure Remove (This : access Typ;
                     P1_MenuComponent : access Standard.Java.Awt.MenuComponent.Typ'Class);

   procedure RemoveNotify (This : access Typ);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetFrames return Standard.Java.Lang.Object.Ref;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NORMAL : constant Java.Int;

   --  final
   ICONIFIED : constant Java.Int;

   --  final
   MAXIMIZED_HORIZ : constant Java.Int;

   --  final
   MAXIMIZED_VERT : constant Java.Int;

   --  final
   MAXIMIZED_BOTH : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Frame);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetTitle, "getTitle");
   pragma Import (Java, SetTitle, "setTitle");
   pragma Import (Java, GetIconImage, "getIconImage");
   pragma Import (Java, SetIconImage, "setIconImage");
   pragma Import (Java, GetMenuBar, "getMenuBar");
   pragma Import (Java, SetMenuBar, "setMenuBar");
   pragma Import (Java, IsResizable, "isResizable");
   pragma Import (Java, SetResizable, "setResizable");
   pragma Import (Java, SetState, "setState");
   pragma Import (Java, SetExtendedState, "setExtendedState");
   pragma Import (Java, GetState, "getState");
   pragma Import (Java, GetExtendedState, "getExtendedState");
   pragma Import (Java, SetMaximizedBounds, "setMaximizedBounds");
   pragma Import (Java, GetMaximizedBounds, "getMaximizedBounds");
   pragma Import (Java, SetUndecorated, "setUndecorated");
   pragma Import (Java, IsUndecorated, "isUndecorated");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetFrames, "getFrames");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, NORMAL, "NORMAL");
   pragma Import (Java, ICONIFIED, "ICONIFIED");
   pragma Import (Java, MAXIMIZED_HORIZ, "MAXIMIZED_HORIZ");
   pragma Import (Java, MAXIMIZED_VERT, "MAXIMIZED_VERT");
   pragma Import (Java, MAXIMIZED_BOTH, "MAXIMIZED_BOTH");

end Java.Awt.Frame;
pragma Import (Java, Java.Awt.Frame, "java.awt.Frame");
pragma Extensions_Allowed (Off);
