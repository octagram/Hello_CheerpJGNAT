pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Shape;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.AffineTransform is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AffineTransform (This : Ref := null)
                                 return Ref;

   function New_AffineTransform (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_AffineTransform (P1_Float : Java.Float;
                                 P2_Float : Java.Float;
                                 P3_Float : Java.Float;
                                 P4_Float : Java.Float;
                                 P5_Float : Java.Float;
                                 P6_Float : Java.Float; 
                                 This : Ref := null)
                                 return Ref;

   function New_AffineTransform (P1_Float_Arr : Java.Float_Arr; 
                                 This : Ref := null)
                                 return Ref;

   function New_AffineTransform (P1_Double : Java.Double;
                                 P2_Double : Java.Double;
                                 P3_Double : Java.Double;
                                 P4_Double : Java.Double;
                                 P5_Double : Java.Double;
                                 P6_Double : Java.Double; 
                                 This : Ref := null)
                                 return Ref;

   function New_AffineTransform (P1_Double_Arr : Java.Double_Arr; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTranslateInstance (P1_Double : Java.Double;
                                  P2_Double : Java.Double)
                                  return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetRotateInstance (P1_Double : Java.Double)
                               return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetRotateInstance (P1_Double : Java.Double;
                               P2_Double : Java.Double;
                               P3_Double : Java.Double)
                               return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetRotateInstance (P1_Double : Java.Double;
                               P2_Double : Java.Double)
                               return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetRotateInstance (P1_Double : Java.Double;
                               P2_Double : Java.Double;
                               P3_Double : Java.Double;
                               P4_Double : Java.Double)
                               return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetQuadrantRotateInstance (P1_Int : Java.Int)
                                       return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetQuadrantRotateInstance (P1_Int : Java.Int;
                                       P2_Double : Java.Double;
                                       P3_Double : Java.Double)
                                       return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetScaleInstance (P1_Double : Java.Double;
                              P2_Double : Java.Double)
                              return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetShearInstance (P1_Double : Java.Double;
                              P2_Double : Java.Double)
                              return access Java.Awt.Geom.AffineTransform.Typ'Class;

   function GetType (This : access Typ)
                     return Java.Int;

   function GetDeterminant (This : access Typ)
                            return Java.Double;

   procedure GetMatrix (This : access Typ;
                        P1_Double_Arr : Java.Double_Arr);

   function GetScaleX (This : access Typ)
                       return Java.Double;

   function GetScaleY (This : access Typ)
                       return Java.Double;

   function GetShearX (This : access Typ)
                       return Java.Double;

   function GetShearY (This : access Typ)
                       return Java.Double;

   function GetTranslateX (This : access Typ)
                           return Java.Double;

   function GetTranslateY (This : access Typ)
                           return Java.Double;

   procedure Translate (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double);

   procedure Rotate (This : access Typ;
                     P1_Double : Java.Double);

   procedure Rotate (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double;
                     P3_Double : Java.Double);

   procedure Rotate (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double);

   procedure Rotate (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double;
                     P3_Double : Java.Double;
                     P4_Double : Java.Double);

   procedure QuadrantRotate (This : access Typ;
                             P1_Int : Java.Int);

   procedure QuadrantRotate (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Double : Java.Double;
                             P3_Double : Java.Double);

   procedure Scale (This : access Typ;
                    P1_Double : Java.Double;
                    P2_Double : Java.Double);

   procedure Shear (This : access Typ;
                    P1_Double : Java.Double;
                    P2_Double : Java.Double);

   procedure SetToIdentity (This : access Typ);

   procedure SetToTranslation (This : access Typ;
                               P1_Double : Java.Double;
                               P2_Double : Java.Double);

   procedure SetToRotation (This : access Typ;
                            P1_Double : Java.Double);

   procedure SetToRotation (This : access Typ;
                            P1_Double : Java.Double;
                            P2_Double : Java.Double;
                            P3_Double : Java.Double);

   procedure SetToRotation (This : access Typ;
                            P1_Double : Java.Double;
                            P2_Double : Java.Double);

   procedure SetToRotation (This : access Typ;
                            P1_Double : Java.Double;
                            P2_Double : Java.Double;
                            P3_Double : Java.Double;
                            P4_Double : Java.Double);

   procedure SetToQuadrantRotation (This : access Typ;
                                    P1_Int : Java.Int);

   procedure SetToQuadrantRotation (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Double : Java.Double;
                                    P3_Double : Java.Double);

   procedure SetToScale (This : access Typ;
                         P1_Double : Java.Double;
                         P2_Double : Java.Double);

   procedure SetToShear (This : access Typ;
                         P1_Double : Java.Double;
                         P2_Double : Java.Double);

   procedure SetTransform (This : access Typ;
                           P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   procedure SetTransform (This : access Typ;
                           P1_Double : Java.Double;
                           P2_Double : Java.Double;
                           P3_Double : Java.Double;
                           P4_Double : Java.Double;
                           P5_Double : Java.Double;
                           P6_Double : Java.Double);

   procedure Concatenate (This : access Typ;
                          P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   procedure PreConcatenate (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   function CreateInverse (This : access Typ)
                           return access Java.Awt.Geom.AffineTransform.Typ'Class;
   --  can raise Java.Awt.Geom.NoninvertibleTransformException.Except

   procedure Invert (This : access Typ);
   --  can raise Java.Awt.Geom.NoninvertibleTransformException.Except

   function Transform (This : access Typ;
                       P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                       P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                       return access Java.Awt.Geom.Point2D.Typ'Class;

   procedure Transform (This : access Typ;
                        P1_Point2D_Arr : access Java.Awt.Geom.Point2D.Arr_Obj;
                        P2_Int : Java.Int;
                        P3_Point2D_Arr : access Java.Awt.Geom.Point2D.Arr_Obj;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   procedure Transform (This : access Typ;
                        P1_Float_Arr : Java.Float_Arr;
                        P2_Int : Java.Int;
                        P3_Float_Arr : Java.Float_Arr;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   procedure Transform (This : access Typ;
                        P1_Double_Arr : Java.Double_Arr;
                        P2_Int : Java.Int;
                        P3_Double_Arr : Java.Double_Arr;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   procedure Transform (This : access Typ;
                        P1_Float_Arr : Java.Float_Arr;
                        P2_Int : Java.Int;
                        P3_Double_Arr : Java.Double_Arr;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   procedure Transform (This : access Typ;
                        P1_Double_Arr : Java.Double_Arr;
                        P2_Int : Java.Int;
                        P3_Float_Arr : Java.Float_Arr;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   function InverseTransform (This : access Typ;
                              P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                              P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                              return access Java.Awt.Geom.Point2D.Typ'Class;
   --  can raise Java.Awt.Geom.NoninvertibleTransformException.Except

   procedure InverseTransform (This : access Typ;
                               P1_Double_Arr : Java.Double_Arr;
                               P2_Int : Java.Int;
                               P3_Double_Arr : Java.Double_Arr;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int);
   --  can raise Java.Awt.Geom.NoninvertibleTransformException.Except

   function DeltaTransform (This : access Typ;
                            P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                            P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                            return access Java.Awt.Geom.Point2D.Typ'Class;

   procedure DeltaTransform (This : access Typ;
                             P1_Double_Arr : Java.Double_Arr;
                             P2_Int : Java.Int;
                             P3_Double_Arr : Java.Double_Arr;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int);

   function CreateTransformedShape (This : access Typ;
                                    P1_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                    return access Java.Awt.Shape.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IsIdentity (This : access Typ)
                        return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_IDENTITY : constant Java.Int;

   --  final
   TYPE_TRANSLATION : constant Java.Int;

   --  final
   TYPE_UNIFORM_SCALE : constant Java.Int;

   --  final
   TYPE_GENERAL_SCALE : constant Java.Int;

   --  final
   TYPE_MASK_SCALE : constant Java.Int;

   --  final
   TYPE_FLIP : constant Java.Int;

   --  final
   TYPE_QUADRANT_ROTATION : constant Java.Int;

   --  final
   TYPE_GENERAL_ROTATION : constant Java.Int;

   --  final
   TYPE_MASK_ROTATION : constant Java.Int;

   --  final
   TYPE_GENERAL_TRANSFORM : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AffineTransform);
   pragma Import (Java, GetTranslateInstance, "getTranslateInstance");
   pragma Import (Java, GetRotateInstance, "getRotateInstance");
   pragma Import (Java, GetQuadrantRotateInstance, "getQuadrantRotateInstance");
   pragma Import (Java, GetScaleInstance, "getScaleInstance");
   pragma Import (Java, GetShearInstance, "getShearInstance");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetDeterminant, "getDeterminant");
   pragma Import (Java, GetMatrix, "getMatrix");
   pragma Import (Java, GetScaleX, "getScaleX");
   pragma Import (Java, GetScaleY, "getScaleY");
   pragma Import (Java, GetShearX, "getShearX");
   pragma Import (Java, GetShearY, "getShearY");
   pragma Import (Java, GetTranslateX, "getTranslateX");
   pragma Import (Java, GetTranslateY, "getTranslateY");
   pragma Import (Java, Translate, "translate");
   pragma Import (Java, Rotate, "rotate");
   pragma Import (Java, QuadrantRotate, "quadrantRotate");
   pragma Import (Java, Scale, "scale");
   pragma Import (Java, Shear, "shear");
   pragma Import (Java, SetToIdentity, "setToIdentity");
   pragma Import (Java, SetToTranslation, "setToTranslation");
   pragma Import (Java, SetToRotation, "setToRotation");
   pragma Import (Java, SetToQuadrantRotation, "setToQuadrantRotation");
   pragma Import (Java, SetToScale, "setToScale");
   pragma Import (Java, SetToShear, "setToShear");
   pragma Import (Java, SetTransform, "setTransform");
   pragma Import (Java, Concatenate, "concatenate");
   pragma Import (Java, PreConcatenate, "preConcatenate");
   pragma Import (Java, CreateInverse, "createInverse");
   pragma Import (Java, Invert, "invert");
   pragma Import (Java, Transform, "transform");
   pragma Import (Java, InverseTransform, "inverseTransform");
   pragma Import (Java, DeltaTransform, "deltaTransform");
   pragma Import (Java, CreateTransformedShape, "createTransformedShape");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IsIdentity, "isIdentity");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, TYPE_IDENTITY, "TYPE_IDENTITY");
   pragma Import (Java, TYPE_TRANSLATION, "TYPE_TRANSLATION");
   pragma Import (Java, TYPE_UNIFORM_SCALE, "TYPE_UNIFORM_SCALE");
   pragma Import (Java, TYPE_GENERAL_SCALE, "TYPE_GENERAL_SCALE");
   pragma Import (Java, TYPE_MASK_SCALE, "TYPE_MASK_SCALE");
   pragma Import (Java, TYPE_FLIP, "TYPE_FLIP");
   pragma Import (Java, TYPE_QUADRANT_ROTATION, "TYPE_QUADRANT_ROTATION");
   pragma Import (Java, TYPE_GENERAL_ROTATION, "TYPE_GENERAL_ROTATION");
   pragma Import (Java, TYPE_MASK_ROTATION, "TYPE_MASK_ROTATION");
   pragma Import (Java, TYPE_GENERAL_TRANSFORM, "TYPE_GENERAL_TRANSFORM");

end Java.Awt.Geom.AffineTransform;
pragma Import (Java, Java.Awt.Geom.AffineTransform, "java.awt.geom.AffineTransform");
pragma Extensions_Allowed (Off);
