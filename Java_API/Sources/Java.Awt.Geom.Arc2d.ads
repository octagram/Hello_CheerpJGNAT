pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Dimension2D;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
with Java.Awt.Geom.RectangularShape;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Arc2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Awt.Geom.RectangularShape.Typ(Shape_I,
                                                       Cloneable_I)
      with null record;

   --  protected
   function New_Arc2D (P1_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAngleStart (This : access Typ)
                           return Java.Double is abstract;

   function GetAngleExtent (This : access Typ)
                            return Java.Double is abstract;

   function GetArcType (This : access Typ)
                        return Java.Int;

   function GetStartPoint (This : access Typ)
                           return access Java.Awt.Geom.Point2D.Typ'Class;

   function GetEndPoint (This : access Typ)
                         return access Java.Awt.Geom.Point2D.Typ'Class;

   procedure SetArc (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double;
                     P3_Double : Java.Double;
                     P4_Double : Java.Double;
                     P5_Double : Java.Double;
                     P6_Double : Java.Double;
                     P7_Int : Java.Int) is abstract
                     with Export => "setArc", Convention => Java;

   procedure SetArc (This : access Typ;
                     P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                     P2_Dimension2D : access Standard.Java.Awt.Geom.Dimension2D.Typ'Class;
                     P3_Double : Java.Double;
                     P4_Double : Java.Double;
                     P5_Int : Java.Int)
                     with Import => "setArc", Convention => Java;

   procedure SetArc (This : access Typ;
                     P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                     P2_Double : Java.Double;
                     P3_Double : Java.Double;
                     P4_Int : Java.Int)
                     with Import => "setArc", Convention => Java;

   procedure SetArc (This : access Typ;
                     P1_Arc2D : access Standard.Java.Awt.Geom.Arc2D.Typ'Class)
                     with Import => "setArc", Convention => Java;

   procedure SetArcByCenter (This : access Typ;
                             P1_Double : Java.Double;
                             P2_Double : Java.Double;
                             P3_Double : Java.Double;
                             P4_Double : Java.Double;
                             P5_Double : Java.Double;
                             P6_Int : Java.Int);

   procedure SetArcByTangent (This : access Typ;
                              P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                              P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                              P3_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                              P4_Double : Java.Double);

   procedure SetAngleStart (This : access Typ;
                            P1_Double : Java.Double) is abstract
                            with Export => "setAngleStart", Convention => Java;

   procedure SetAngleExtent (This : access Typ;
                             P1_Double : Java.Double) is abstract;

   procedure SetAngleStart (This : access Typ;
                            P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                            with Import => "setAngleStart", Convention => Java;

   procedure SetAngles (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double);

   procedure SetAngles (This : access Typ;
                        P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                        P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class);

   procedure SetArcType (This : access Typ;
                         P1_Int : Java.Int);

   procedure SetFrame (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double);

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   --  protected
   function MakeBounds (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   function ContainsAngle (This : access Typ;
                           P1_Double : Java.Double)
                           return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OPEN : constant Java.Int;

   --  final
   CHORD : constant Java.Int;

   --  final
   PIE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Arc2D);
   pragma Export (Java, GetAngleStart, "getAngleStart");
   pragma Export (Java, GetAngleExtent, "getAngleExtent");
   pragma Import (Java, GetArcType, "getArcType");
   pragma Import (Java, GetStartPoint, "getStartPoint");
   pragma Import (Java, GetEndPoint, "getEndPoint");
   -- pragma Import (Java, SetArc, "setArc");
   pragma Import (Java, SetArcByCenter, "setArcByCenter");
   pragma Import (Java, SetArcByTangent, "setArcByTangent");
   -- pragma Import (Java, SetAngleStart, "setAngleStart");
   pragma Export (Java, SetAngleExtent, "setAngleExtent");
   pragma Import (Java, SetAngles, "setAngles");
   pragma Import (Java, SetArcType, "setArcType");
   pragma Import (Java, SetFrame, "setFrame");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Export (Java, MakeBounds, "makeBounds");
   pragma Import (Java, ContainsAngle, "containsAngle");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetPathIterator, "getPathIterator");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, OPEN, "OPEN");
   pragma Import (Java, CHORD, "CHORD");
   pragma Import (Java, PIE, "PIE");

end Java.Awt.Geom.Arc2D;
pragma Import (Java, Java.Awt.Geom.Arc2D, "java.awt.geom.Arc2D");
pragma Extensions_Allowed (Off);
