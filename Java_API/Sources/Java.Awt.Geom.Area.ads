pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Rectangle;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Area is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Area (This : Ref := null)
                      return Ref;

   function New_Area (P1_Shape : access Standard.Java.Awt.Shape.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Add (This : access Typ;
                  P1_Area : access Standard.Java.Awt.Geom.Area.Typ'Class);

   procedure Subtract (This : access Typ;
                       P1_Area : access Standard.Java.Awt.Geom.Area.Typ'Class);

   procedure Intersect (This : access Typ;
                        P1_Area : access Standard.Java.Awt.Geom.Area.Typ'Class);

   procedure ExclusiveOr (This : access Typ;
                          P1_Area : access Standard.Java.Awt.Geom.Area.Typ'Class);

   procedure Reset (This : access Typ);

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function IsPolygonal (This : access Typ)
                         return Java.Boolean;

   function IsRectangular (This : access Typ)
                           return Java.Boolean;

   function IsSingular (This : access Typ)
                        return Java.Boolean;

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Area : access Standard.Java.Awt.Geom.Area.Typ'Class)
                    return Java.Boolean;

   procedure Transform (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   function CreateTransformedArea (This : access Typ;
                                   P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                                   return access Java.Awt.Geom.Area.Typ'Class;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                        return Java.Boolean;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                             P2_Double : Java.Double)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Area);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Subtract, "subtract");
   pragma Import (Java, Intersect, "intersect");
   pragma Import (Java, ExclusiveOr, "exclusiveOr");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, IsPolygonal, "isPolygonal");
   pragma Import (Java, IsRectangular, "isRectangular");
   pragma Import (Java, IsSingular, "isSingular");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Transform, "transform");
   pragma Import (Java, CreateTransformedArea, "createTransformedArea");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetPathIterator, "getPathIterator");

end Java.Awt.Geom.Area;
pragma Import (Java, Java.Awt.Geom.Area, "java.awt.geom.Area");
pragma Extensions_Allowed (Off);
