pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Dimension2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Dimension2D (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetWidth (This : access Typ)
                      return Java.Double is abstract;

   function GetHeight (This : access Typ)
                       return Java.Double is abstract;

   procedure SetSize (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double) is abstract
                      with Export => "setSize", Convention => Java;

   procedure SetSize (This : access Typ;
                      P1_Dimension2D : access Standard.Java.Awt.Geom.Dimension2D.Typ'Class)
                      with Import => "setSize", Convention => Java;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Dimension2D);
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   -- pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Geom.Dimension2D;
pragma Import (Java, Java.Awt.Geom.Dimension2D, "java.awt.geom.Dimension2D");
pragma Extensions_Allowed (Off);
