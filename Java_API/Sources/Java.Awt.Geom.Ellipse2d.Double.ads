pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Rectangle2D;
with Java.Awt.Geom.Ellipse2D;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Ellipse2D.Double is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Ellipse2D.Typ(Shape_I,
                                       Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      X : Java.Double;
      pragma Import (Java, X, "x");

      Y : Java.Double;
      pragma Import (Java, Y, "y");

      Width : Java.Double;
      pragma Import (Java, Width, "width");

      Height : Java.Double;
      pragma Import (Java, Height, "height");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Double (This : Ref := null)
                        return Ref;

   function New_Double (P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return Java.Double;

   function GetY (This : access Typ)
                  return Java.Double;

   function GetWidth (This : access Typ)
                      return Java.Double;

   function GetHeight (This : access Typ)
                       return Java.Double;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   procedure SetFrame (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double);

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Double);
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, SetFrame, "setFrame");
   pragma Import (Java, GetBounds2D, "getBounds2D");

end Java.Awt.Geom.Ellipse2D.Double;
pragma Import (Java, Java.Awt.Geom.Ellipse2D.Double, "java.awt.geom.Ellipse2D$Double");
pragma Extensions_Allowed (Off);
