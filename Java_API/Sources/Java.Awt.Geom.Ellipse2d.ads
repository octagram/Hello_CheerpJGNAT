pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
with Java.Awt.Geom.RectangularShape;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Ellipse2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Awt.Geom.RectangularShape.Typ(Shape_I,
                                                       Cloneable_I)
      with null record;

   --  protected
   function New_Ellipse2D (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Ellipse2D);
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetPathIterator, "getPathIterator");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Java.Awt.Geom.Ellipse2D;
pragma Import (Java, Java.Awt.Geom.Ellipse2D, "java.awt.geom.Ellipse2D");
pragma Extensions_Allowed (Off);
