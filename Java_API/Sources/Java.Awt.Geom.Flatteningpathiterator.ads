pragma Extensions_Allowed (On);
with Java.Awt.Geom.PathIterator;
with Java.Lang.Object;

package Java.Awt.Geom.FlatteningPathIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PathIterator_I : Java.Awt.Geom.PathIterator.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FlatteningPathIterator (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                                        P2_Double : Java.Double; 
                                        This : Ref := null)
                                        return Ref;

   function New_FlatteningPathIterator (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                                        P2_Double : Java.Double;
                                        P3_Int : Java.Int; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFlatness (This : access Typ)
                         return Java.Double;

   function GetRecursionLimit (This : access Typ)
                               return Java.Int;

   function GetWindingRule (This : access Typ)
                            return Java.Int;

   function IsDone (This : access Typ)
                    return Java.Boolean;

   procedure Next (This : access Typ);

   function CurrentSegment (This : access Typ;
                            P1_Float_Arr : Java.Float_Arr)
                            return Java.Int;

   function CurrentSegment (This : access Typ;
                            P1_Double_Arr : Java.Double_Arr)
                            return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FlatteningPathIterator);
   pragma Import (Java, GetFlatness, "getFlatness");
   pragma Import (Java, GetRecursionLimit, "getRecursionLimit");
   pragma Import (Java, GetWindingRule, "getWindingRule");
   pragma Import (Java, IsDone, "isDone");
   pragma Import (Java, Next, "next");
   pragma Import (Java, CurrentSegment, "currentSegment");

end Java.Awt.Geom.FlatteningPathIterator;
pragma Import (Java, Java.Awt.Geom.FlatteningPathIterator, "java.awt.geom.FlatteningPathIterator");
pragma Extensions_Allowed (Off);
