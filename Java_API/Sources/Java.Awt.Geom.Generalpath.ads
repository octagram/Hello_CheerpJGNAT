pragma Extensions_Allowed (On);
with Java.Awt.Geom.Path2D.Float;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.GeneralPath is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Path2D.Float.Typ(Shape_I,
                                          Serializable_I,
                                          Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GeneralPath (This : Ref := null)
                             return Ref;

   function New_GeneralPath (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_GeneralPath (P1_Int : Java.Int;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_GeneralPath (P1_Shape : access Standard.Java.Awt.Shape.Typ'Class; 
                             This : Ref := null)
                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GeneralPath);

end Java.Awt.Geom.GeneralPath;
pragma Import (Java, Java.Awt.Geom.GeneralPath, "java.awt.geom.GeneralPath");
pragma Extensions_Allowed (Off);
