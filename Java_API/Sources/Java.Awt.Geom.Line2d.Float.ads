pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
with Java.Awt.Geom.Line2D;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Line2D.Float is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Line2D.Typ(Shape_I,
                                    Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      X1 : Java.Float;
      pragma Import (Java, X1, "x1");

      Y1 : Java.Float;
      pragma Import (Java, Y1, "y1");

      X2 : Java.Float;
      pragma Import (Java, X2, "x2");

      Y2 : Java.Float;
      pragma Import (Java, Y2, "y2");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Float (This : Ref := null)
                       return Ref;

   function New_Float (P1_Float : Java.Float;
                       P2_Float : Java.Float;
                       P3_Float : Java.Float;
                       P4_Float : Java.Float; 
                       This : Ref := null)
                       return Ref;

   function New_Float (P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                       P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX1 (This : access Typ)
                   return Java.Double;

   function GetY1 (This : access Typ)
                   return Java.Double;

   function GetP1 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class;

   function GetX2 (This : access Typ)
                   return Java.Double;

   function GetY2 (This : access Typ)
                   return Java.Double;

   function GetP2 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class;

   procedure SetLine (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double);

   procedure SetLine (This : access Typ;
                      P1_Float : Java.Float;
                      P2_Float : Java.Float;
                      P3_Float : Java.Float;
                      P4_Float : Java.Float);

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Float);
   pragma Import (Java, GetX1, "getX1");
   pragma Import (Java, GetY1, "getY1");
   pragma Import (Java, GetP1, "getP1");
   pragma Import (Java, GetX2, "getX2");
   pragma Import (Java, GetY2, "getY2");
   pragma Import (Java, GetP2, "getP2");
   pragma Import (Java, SetLine, "setLine");
   pragma Import (Java, GetBounds2D, "getBounds2D");

end Java.Awt.Geom.Line2D.Float;
pragma Import (Java, Java.Awt.Geom.Line2D.Float, "java.awt.geom.Line2D$Float");
pragma Extensions_Allowed (Off);
