pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Rectangle;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Line2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Line2D (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX1 (This : access Typ)
                   return Java.Double is abstract;

   function GetY1 (This : access Typ)
                   return Java.Double is abstract;

   function GetP1 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   function GetX2 (This : access Typ)
                   return Java.Double is abstract;

   function GetY2 (This : access Typ)
                   return Java.Double is abstract;

   function GetP2 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   procedure SetLine (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double) is abstract with Export => "setLine", Convention => Java;

   procedure SetLine (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                      P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class) with Import => "setLine", Convention => Java;

   procedure SetLine (This : access Typ;
                      P1_Line2D : access Standard.Java.Awt.Geom.Line2D.Typ'Class) with Import => "setLine", Convention => Java;

   function RelativeCCW (P1_Double : Java.Double;
                         P2_Double : Java.Double;
                         P3_Double : Java.Double;
                         P4_Double : Java.Double;
                         P5_Double : Java.Double;
                         P6_Double : Java.Double)
                         return Java.Int;

   function RelativeCCW (This : access Typ;
                         P1_Double : Java.Double;
                         P2_Double : Java.Double)
                         return Java.Int;

   function RelativeCCW (This : access Typ;
                         P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                         return Java.Int;

   function LinesIntersect (P1_Double : Java.Double;
                            P2_Double : Java.Double;
                            P3_Double : Java.Double;
                            P4_Double : Java.Double;
                            P5_Double : Java.Double;
                            P6_Double : Java.Double;
                            P7_Double : Java.Double;
                            P8_Double : Java.Double)
                            return Java.Boolean;

   function IntersectsLine (This : access Typ;
                            P1_Double : Java.Double;
                            P2_Double : Java.Double;
                            P3_Double : Java.Double;
                            P4_Double : Java.Double)
                            return Java.Boolean;

   function IntersectsLine (This : access Typ;
                            P1_Line2D : access Standard.Java.Awt.Geom.Line2D.Typ'Class)
                            return Java.Boolean;

   function PtSegDistSq (P1_Double : Java.Double;
                         P2_Double : Java.Double;
                         P3_Double : Java.Double;
                         P4_Double : Java.Double;
                         P5_Double : Java.Double;
                         P6_Double : Java.Double)
                         return Java.Double;

   function PtSegDist (P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double;
                       P5_Double : Java.Double;
                       P6_Double : Java.Double)
                       return Java.Double;

   function PtSegDistSq (This : access Typ;
                         P1_Double : Java.Double;
                         P2_Double : Java.Double)
                         return Java.Double;

   function PtSegDistSq (This : access Typ;
                         P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                         return Java.Double;

   function PtSegDist (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double)
                       return Java.Double;

   function PtSegDist (This : access Typ;
                       P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                       return Java.Double;

   function PtLineDistSq (P1_Double : Java.Double;
                          P2_Double : Java.Double;
                          P3_Double : Java.Double;
                          P4_Double : Java.Double;
                          P5_Double : Java.Double;
                          P6_Double : Java.Double)
                          return Java.Double;

   function PtLineDist (P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double;
                        P5_Double : Java.Double;
                        P6_Double : Java.Double)
                        return Java.Double;

   function PtLineDistSq (This : access Typ;
                          P1_Double : Java.Double;
                          P2_Double : Java.Double)
                          return Java.Double;

   function PtLineDistSq (This : access Typ;
                          P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                          return Java.Double;

   function PtLineDist (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double)
                        return Java.Double;

   function PtLineDist (This : access Typ;
                        P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                        return Java.Double;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                             P2_Double : Java.Double)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Line2D);
   pragma Export (Java, GetX1, "getX1");
   pragma Export (Java, GetY1, "getY1");
   pragma Export (Java, GetP1, "getP1");
   pragma Export (Java, GetX2, "getX2");
   pragma Export (Java, GetY2, "getY2");
   pragma Export (Java, GetP2, "getP2");
   -- pragma Import (Java, SetLine, "setLine");
   pragma Import (Java, RelativeCCW, "relativeCCW");
   pragma Import (Java, LinesIntersect, "linesIntersect");
   pragma Import (Java, IntersectsLine, "intersectsLine");
   pragma Import (Java, PtSegDistSq, "ptSegDistSq");
   pragma Import (Java, PtSegDist, "ptSegDist");
   pragma Import (Java, PtLineDistSq, "ptLineDistSq");
   pragma Import (Java, PtLineDist, "ptLineDist");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetPathIterator, "getPathIterator");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Geom.Line2D;
pragma Import (Java, Java.Awt.Geom.Line2D, "java.awt.geom.Line2D");
pragma Extensions_Allowed (Off);
