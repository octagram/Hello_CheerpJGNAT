pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Rectangle2D;
with Java.Awt.Geom.Path2D;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Path2D.Double is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Path2D.Typ(Shape_I,
                                    Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Double (This : Ref := null)
                        return Ref;

   function New_Double (P1_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_Double (P1_Int : Java.Int;
                        P2_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_Double (P1_Shape : access Standard.Java.Awt.Shape.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Double (P1_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                        P2_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  synchronized
   procedure MoveTo (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double);

   --  final  synchronized
   procedure LineTo (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double);

   --  final  synchronized
   procedure QuadTo (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double;
                     P3_Double : Java.Double;
                     P4_Double : Java.Double);

   --  final  synchronized
   procedure CurveTo (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double;
                      P5_Double : Java.Double;
                      P6_Double : Java.Double);

   --  final
   procedure Append (This : access Typ;
                     P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                     P2_Boolean : Java.Boolean);

   --  final
   procedure Transform (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   --  final  synchronized
   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   --  final
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Double);
   pragma Import (Java, MoveTo, "moveTo");
   pragma Import (Java, LineTo, "lineTo");
   pragma Import (Java, QuadTo, "quadTo");
   pragma Import (Java, CurveTo, "curveTo");
   pragma Import (Java, Append, "append");
   pragma Import (Java, Transform, "transform");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, GetPathIterator, "getPathIterator");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Geom.Path2D.Double;
pragma Import (Java, Java.Awt.Geom.Path2D.Double, "java.awt.geom.Path2D$Double");
pragma Extensions_Allowed (Off);
