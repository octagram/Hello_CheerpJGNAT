pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Rectangle;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Path2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MoveTo (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double) is abstract;

   procedure LineTo (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double) is abstract;

   procedure QuadTo (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double;
                     P3_Double : Java.Double;
                     P4_Double : Java.Double) is abstract;

   procedure CurveTo (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double;
                      P5_Double : Java.Double;
                      P6_Double : Java.Double) is abstract;

   --  final  synchronized
   procedure ClosePath (This : access Typ);

   --  final
   procedure Append (This : access Typ;
                     P1_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     with Import => "append", Convention => Java;

   procedure Append (This : access Typ;
                     P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                     P2_Boolean : Java.Boolean) is abstract
                     with Export => "append", Convention => Java;

   --  final  synchronized
   function GetWindingRule (This : access Typ)
                            return Java.Int;

   --  final
   procedure SetWindingRule (This : access Typ;
                             P1_Int : Java.Int);

   --  final  synchronized
   function GetCurrentPoint (This : access Typ)
                             return access Java.Awt.Geom.Point2D.Typ'Class;

   --  final  synchronized
   procedure Reset (This : access Typ);

   procedure Transform (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class) is abstract;

   --  final  synchronized
   function CreateTransformedShape (This : access Typ;
                                    P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                                    return access Java.Awt.Shape.Typ'Class;

   --  final
   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   function Contains (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double)
                      return Java.Boolean;

   function Contains (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                      P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Boolean;

   --  final
   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   --  final
   function Contains (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Boolean;

   function Contains (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double;
                      P5_Double : Java.Double)
                      return Java.Boolean;

   function Contains (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                      P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   --  final
   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   --  final
   function Contains (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   function Intersects (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double;
                        P5_Double : Java.Double)
                        return Java.Boolean;

   function Intersects (P1_PathIterator : access Standard.Java.Awt.Geom.PathIterator.Typ'Class;
                        P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                        return Java.Boolean;

   --  final
   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   --  final
   function Intersects (This : access Typ;
                        P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                        return Java.Boolean;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                             P2_Double : Java.Double)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WIND_EVEN_ODD : constant Java.Int;

   --  final
   WIND_NON_ZERO : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, MoveTo, "moveTo");
   pragma Export (Java, LineTo, "lineTo");
   pragma Export (Java, QuadTo, "quadTo");
   pragma Export (Java, CurveTo, "curveTo");
   pragma Import (Java, ClosePath, "closePath");
   -- pragma Import (Java, Append, "append");
   pragma Import (Java, GetWindingRule, "getWindingRule");
   pragma Import (Java, SetWindingRule, "setWindingRule");
   pragma Import (Java, GetCurrentPoint, "getCurrentPoint");
   pragma Import (Java, Reset, "reset");
   pragma Export (Java, Transform, "transform");
   pragma Import (Java, CreateTransformedShape, "createTransformedShape");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetPathIterator, "getPathIterator");
   pragma Export (Java, Clone, "clone");
   pragma Import (Java, WIND_EVEN_ODD, "WIND_EVEN_ODD");
   pragma Import (Java, WIND_NON_ZERO, "WIND_NON_ZERO");

end Java.Awt.Geom.Path2D;
pragma Import (Java, Java.Awt.Geom.Path2D, "java.awt.geom.Path2D");
pragma Extensions_Allowed (Off);
