pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Geom.PathIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetWindingRule (This : access Typ)
                            return Java.Int is abstract;

   function IsDone (This : access Typ)
                    return Java.Boolean is abstract;

   procedure Next (This : access Typ) is abstract;

   function CurrentSegment (This : access Typ;
                            P1_Float_Arr : Java.Float_Arr)
                            return Java.Int is abstract;

   function CurrentSegment (This : access Typ;
                            P1_Double_Arr : Java.Double_Arr)
                            return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WIND_EVEN_ODD : constant Java.Int;

   --  final
   WIND_NON_ZERO : constant Java.Int;

   --  final
   SEG_MOVETO : constant Java.Int;

   --  final
   SEG_LINETO : constant Java.Int;

   --  final
   SEG_QUADTO : constant Java.Int;

   --  final
   SEG_CUBICTO : constant Java.Int;

   --  final
   SEG_CLOSE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetWindingRule, "getWindingRule");
   pragma Export (Java, IsDone, "isDone");
   pragma Export (Java, Next, "next");
   pragma Export (Java, CurrentSegment, "currentSegment");
   pragma Import (Java, WIND_EVEN_ODD, "WIND_EVEN_ODD");
   pragma Import (Java, WIND_NON_ZERO, "WIND_NON_ZERO");
   pragma Import (Java, SEG_MOVETO, "SEG_MOVETO");
   pragma Import (Java, SEG_LINETO, "SEG_LINETO");
   pragma Import (Java, SEG_QUADTO, "SEG_QUADTO");
   pragma Import (Java, SEG_CUBICTO, "SEG_CUBICTO");
   pragma Import (Java, SEG_CLOSE, "SEG_CLOSE");

end Java.Awt.Geom.PathIterator;
pragma Import (Java, Java.Awt.Geom.PathIterator, "java.awt.geom.PathIterator");
pragma Extensions_Allowed (Off);
