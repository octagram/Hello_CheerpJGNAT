pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Geom.Point2D;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Point2D.Double is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Point2D.Typ(Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      X : Java.Double;
      pragma Import (Java, X, "x");

      Y : Java.Double;
      pragma Import (Java, Y, "y");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Double (This : Ref := null)
                        return Ref;

   function New_Double (P1_Double : Java.Double;
                        P2_Double : Java.Double; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return Java.Double;

   function GetY (This : access Typ)
                  return Java.Double;

   procedure SetLocation (This : access Typ;
                          P1_Double : Java.Double;
                          P2_Double : Java.Double);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Double);
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Geom.Point2D.Double;
pragma Import (Java, Java.Awt.Geom.Point2D.Double, "java.awt.geom.Point2D$Double");
pragma Extensions_Allowed (Off);
