pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Point2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Point2D (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return Java.Double is abstract;

   function GetY (This : access Typ)
                  return Java.Double is abstract;

   procedure SetLocation (This : access Typ;
                          P1_Double : Java.Double;
                          P2_Double : Java.Double) is abstract;

   procedure SetLocation (This : access Typ;
                          P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class);

   function DistanceSq (P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Double;

   function Distance (P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Double;

   function DistanceSq (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double)
                        return Java.Double;

   function DistanceSq (This : access Typ;
                        P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                        return Java.Double;

   function Distance (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Double;

   function Distance (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Double;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Point2D);
   pragma Export (Java, GetX, "getX");
   pragma Export (Java, GetY, "getY");
   pragma Export (Java, SetLocation, "setLocation");
   pragma Import (Java, DistanceSq, "distanceSq");
   pragma Import (Java, Distance, "distance");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Java.Awt.Geom.Point2D;
pragma Import (Java, Java.Awt.Geom.Point2D, "java.awt.geom.Point2D");
pragma Extensions_Allowed (Off);
