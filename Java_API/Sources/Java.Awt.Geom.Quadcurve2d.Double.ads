pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
with Java.Awt.Geom.QuadCurve2D;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.QuadCurve2D.Double is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.QuadCurve2D.Typ(Shape_I,
                                         Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      X1 : Java.Double;
      pragma Import (Java, X1, "x1");

      Y1 : Java.Double;
      pragma Import (Java, Y1, "y1");

      Ctrlx : Java.Double;
      pragma Import (Java, Ctrlx, "ctrlx");

      Ctrly : Java.Double;
      pragma Import (Java, Ctrly, "ctrly");

      X2 : Java.Double;
      pragma Import (Java, X2, "x2");

      Y2 : Java.Double;
      pragma Import (Java, Y2, "y2");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Double (This : Ref := null)
                        return Ref;

   function New_Double (P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double;
                        P5_Double : Java.Double;
                        P6_Double : Java.Double; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX1 (This : access Typ)
                   return Java.Double;

   function GetY1 (This : access Typ)
                   return Java.Double;

   function GetP1 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class;

   function GetCtrlX (This : access Typ)
                      return Java.Double;

   function GetCtrlY (This : access Typ)
                      return Java.Double;

   function GetCtrlPt (This : access Typ)
                       return access Java.Awt.Geom.Point2D.Typ'Class;

   function GetX2 (This : access Typ)
                   return Java.Double;

   function GetY2 (This : access Typ)
                   return Java.Double;

   function GetP2 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class;

   procedure SetCurve (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double;
                       P5_Double : Java.Double;
                       P6_Double : Java.Double);

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Double);
   pragma Import (Java, GetX1, "getX1");
   pragma Import (Java, GetY1, "getY1");
   pragma Import (Java, GetP1, "getP1");
   pragma Import (Java, GetCtrlX, "getCtrlX");
   pragma Import (Java, GetCtrlY, "getCtrlY");
   pragma Import (Java, GetCtrlPt, "getCtrlPt");
   pragma Import (Java, GetX2, "getX2");
   pragma Import (Java, GetY2, "getY2");
   pragma Import (Java, GetP2, "getP2");
   pragma Import (Java, SetCurve, "setCurve");
   pragma Import (Java, GetBounds2D, "getBounds2D");

end Java.Awt.Geom.QuadCurve2D.Double;
pragma Import (Java, Java.Awt.Geom.QuadCurve2D.Double, "java.awt.geom.QuadCurve2D$Double");
pragma Extensions_Allowed (Off);
