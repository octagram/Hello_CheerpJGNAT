pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Rectangle;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.QuadCurve2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_QuadCurve2D (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX1 (This : access Typ)
                   return Java.Double is abstract;

   function GetY1 (This : access Typ)
                   return Java.Double is abstract;

   function GetP1 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   function GetCtrlX (This : access Typ)
                      return Java.Double is abstract;

   function GetCtrlY (This : access Typ)
                      return Java.Double is abstract;

   function GetCtrlPt (This : access Typ)
                       return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   function GetX2 (This : access Typ)
                   return Java.Double is abstract;

   function GetY2 (This : access Typ)
                   return Java.Double is abstract;

   function GetP2 (This : access Typ)
                   return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   procedure SetCurve (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double;
                       P5_Double : Java.Double;
                       P6_Double : Java.Double) is abstract
                       with Export => "setCurve", Convention => Java;

   procedure SetCurve (This : access Typ;
                       P1_Double_Arr : Java.Double_Arr;
                       P2_Int : Java.Int)
                       with Import => "setCurve", Convention => Java;

   procedure SetCurve (This : access Typ;
                       P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                       P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                       P3_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                       with Import => "setCurve", Convention => Java;

   procedure SetCurve (This : access Typ;
                       P1_Point2D_Arr : access Java.Awt.Geom.Point2D.Arr_Obj;
                       P2_Int : Java.Int)
                       with Import => "setCurve", Convention => Java;

   procedure SetCurve (This : access Typ;
                       P1_QuadCurve2D : access Standard.Java.Awt.Geom.QuadCurve2D.Typ'Class)
                       with Import => "setCurve", Convention => Java;

   function GetFlatnessSq (P1_Double : Java.Double;
                           P2_Double : Java.Double;
                           P3_Double : Java.Double;
                           P4_Double : Java.Double;
                           P5_Double : Java.Double;
                           P6_Double : Java.Double)
                           return Java.Double;

   function GetFlatness (P1_Double : Java.Double;
                         P2_Double : Java.Double;
                         P3_Double : Java.Double;
                         P4_Double : Java.Double;
                         P5_Double : Java.Double;
                         P6_Double : Java.Double)
                         return Java.Double;

   function GetFlatnessSq (P1_Double_Arr : Java.Double_Arr;
                           P2_Int : Java.Int)
                           return Java.Double;

   function GetFlatness (P1_Double_Arr : Java.Double_Arr;
                         P2_Int : Java.Int)
                         return Java.Double;

   function GetFlatnessSq (This : access Typ)
                           return Java.Double;

   function GetFlatness (This : access Typ)
                         return Java.Double;

   procedure Subdivide (This : access Typ;
                        P1_QuadCurve2D : access Standard.Java.Awt.Geom.QuadCurve2D.Typ'Class;
                        P2_QuadCurve2D : access Standard.Java.Awt.Geom.QuadCurve2D.Typ'Class);

   procedure Subdivide (P1_QuadCurve2D : access Standard.Java.Awt.Geom.QuadCurve2D.Typ'Class;
                        P2_QuadCurve2D : access Standard.Java.Awt.Geom.QuadCurve2D.Typ'Class;
                        P3_QuadCurve2D : access Standard.Java.Awt.Geom.QuadCurve2D.Typ'Class);

   procedure Subdivide (P1_Double_Arr : Java.Double_Arr;
                        P2_Int : Java.Int;
                        P3_Double_Arr : Java.Double_Arr;
                        P4_Int : Java.Int;
                        P5_Double_Arr : Java.Double_Arr;
                        P6_Int : Java.Int);

   function SolveQuadratic (P1_Double_Arr : Java.Double_Arr)
                            return Java.Int;

   function SolveQuadratic (P1_Double_Arr : Java.Double_Arr;
                            P2_Double_Arr : Java.Double_Arr)
                            return Java.Int;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                             P2_Double : Java.Double)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_QuadCurve2D);
   pragma Export (Java, GetX1, "getX1");
   pragma Export (Java, GetY1, "getY1");
   pragma Export (Java, GetP1, "getP1");
   pragma Export (Java, GetCtrlX, "getCtrlX");
   pragma Export (Java, GetCtrlY, "getCtrlY");
   pragma Export (Java, GetCtrlPt, "getCtrlPt");
   pragma Export (Java, GetX2, "getX2");
   pragma Export (Java, GetY2, "getY2");
   pragma Export (Java, GetP2, "getP2");
   -- pragma Import (Java, SetCurve, "setCurve");
   pragma Import (Java, GetFlatnessSq, "getFlatnessSq");
   pragma Import (Java, GetFlatness, "getFlatness");
   pragma Import (Java, Subdivide, "subdivide");
   pragma Import (Java, SolveQuadratic, "solveQuadratic");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetPathIterator, "getPathIterator");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Geom.QuadCurve2D;
pragma Import (Java, Java.Awt.Geom.QuadCurve2D, "java.awt.geom.QuadCurve2D");
pragma Extensions_Allowed (Off);
