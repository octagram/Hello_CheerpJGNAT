pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Geom.Rectangle2D;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Rectangle2D.Float is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Rectangle2D.Typ(Shape_I,
                                         Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      X : Java.Float;
      pragma Import (Java, X, "x");

      Y : Java.Float;
      pragma Import (Java, Y, "y");

      Width : Java.Float;
      pragma Import (Java, Width, "width");

      Height : Java.Float;
      pragma Import (Java, Height, "height");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Float (This : Ref := null)
                       return Ref;

   function New_Float (P1_Float : Java.Float;
                       P2_Float : Java.Float;
                       P3_Float : Java.Float;
                       P4_Float : Java.Float; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return Java.Double;

   function GetY (This : access Typ)
                  return Java.Double;

   function GetWidth (This : access Typ)
                      return Java.Double;

   function GetHeight (This : access Typ)
                       return Java.Double;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   procedure SetRect (This : access Typ;
                      P1_Float : Java.Float;
                      P2_Float : Java.Float;
                      P3_Float : Java.Float;
                      P4_Float : Java.Float);

   procedure SetRect (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double);

   procedure SetRect (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class);

   function Outcode (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double)
                     return Java.Int;

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function CreateIntersection (This : access Typ;
                                P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                                return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function CreateUnion (This : access Typ;
                         P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Float);
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, SetRect, "setRect");
   pragma Import (Java, Outcode, "outcode");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, CreateIntersection, "createIntersection");
   pragma Import (Java, CreateUnion, "createUnion");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Geom.Rectangle2D.Float;
pragma Import (Java, Java.Awt.Geom.Rectangle2D.Float, "java.awt.geom.Rectangle2D$Float");
pragma Extensions_Allowed (Off);
