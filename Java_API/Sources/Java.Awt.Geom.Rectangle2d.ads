pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Line2D;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
with Java.Awt.Geom.RectangularShape;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.Rectangle2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Awt.Geom.RectangularShape.Typ(Shape_I,
                                                       Cloneable_I)
      with null record;

   --  protected
   function New_Rectangle2D (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetRect (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double) is abstract;

   procedure SetRect (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class);

   function IntersectsLine (This : access Typ;
                            P1_Double : Java.Double;
                            P2_Double : Java.Double;
                            P3_Double : Java.Double;
                            P4_Double : Java.Double)
                            return Java.Boolean;

   function IntersectsLine (This : access Typ;
                            P1_Line2D : access Standard.Java.Awt.Geom.Line2D.Typ'Class)
                            return Java.Boolean;

   function Outcode (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double)
                     return Java.Int is abstract;

   function Outcode (This : access Typ;
                     P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                     return Java.Int;

   procedure SetFrame (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double);

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function CreateIntersection (This : access Typ;
                                P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                                return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   procedure Intersect (P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                        P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                        P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class);

   function CreateUnion (This : access Typ;
                         P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   procedure Union (P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                    P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                    P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class);

   procedure Add (This : access Typ;
                  P1_Double : Java.Double;
                  P2_Double : Java.Double);

   procedure Add (This : access Typ;
                  P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class);

   procedure Add (This : access Typ;
                  P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class);

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                             P2_Double : Java.Double)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OUT_LEFT : constant Java.Int;

   --  final
   OUT_TOP : constant Java.Int;

   --  final
   OUT_RIGHT : constant Java.Int;

   --  final
   OUT_BOTTOM : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Rectangle2D);
   pragma Export (Java, SetRect, "setRect");
   pragma Export (Java, IntersectsLine, "intersectsLine");
   pragma Export (Java, Outcode, "outcode");
   pragma Export (Java, SetFrame, "setFrame");
   pragma Export (Java, GetBounds2D, "getBounds2D");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, Intersects, "intersects");
   pragma Export (Java, CreateIntersection, "createIntersection");
   pragma Export (Java, Intersect, "intersect");
   pragma Export (Java, CreateUnion, "createUnion");
   pragma Export (Java, Union, "union");
   pragma Export (Java, Add, "add");
   pragma Export (Java, GetPathIterator, "getPathIterator");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");
   pragma Import (Java, OUT_LEFT, "OUT_LEFT");
   pragma Import (Java, OUT_TOP, "OUT_TOP");
   pragma Import (Java, OUT_RIGHT, "OUT_RIGHT");
   pragma Import (Java, OUT_BOTTOM, "OUT_BOTTOM");

end Java.Awt.Geom.Rectangle2D;
pragma Import (Java, Java.Awt.Geom.Rectangle2D, "java.awt.geom.Rectangle2D");
pragma Extensions_Allowed (Off);
