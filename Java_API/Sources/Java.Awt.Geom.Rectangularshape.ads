pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Dimension2D;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Rectangle;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.RectangularShape is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_RectangularShape (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return Java.Double is abstract;

   function GetY (This : access Typ)
                  return Java.Double is abstract;

   function GetWidth (This : access Typ)
                      return Java.Double is abstract;

   function GetHeight (This : access Typ)
                       return Java.Double is abstract;

   function GetMinX (This : access Typ)
                     return Java.Double;

   function GetMinY (This : access Typ)
                     return Java.Double;

   function GetMaxX (This : access Typ)
                     return Java.Double;

   function GetMaxY (This : access Typ)
                     return Java.Double;

   function GetCenterX (This : access Typ)
                        return Java.Double;

   function GetCenterY (This : access Typ)
                        return Java.Double;

   function GetFrame (This : access Typ)
                      return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function IsEmpty (This : access Typ)
                     return Java.Boolean is abstract;

   procedure SetFrame (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double) is abstract
                       with Export => "setFrame", Convention => Java;

   procedure SetFrame (This : access Typ;
                       P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                       P2_Dimension2D : access Standard.Java.Awt.Geom.Dimension2D.Typ'Class)
                       with Import => "setFrame", Convention => Java;

   procedure SetFrame (This : access Typ;
                       P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                       with Import => "setFrame", Convention => Java;

   procedure SetFrameFromDiagonal (This : access Typ;
                                   P1_Double : Java.Double;
                                   P2_Double : Java.Double;
                                   P3_Double : Java.Double;
                                   P4_Double : Java.Double);

   procedure SetFrameFromDiagonal (This : access Typ;
                                   P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                   P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class);

   procedure SetFrameFromCenter (This : access Typ;
                                 P1_Double : Java.Double;
                                 P2_Double : Java.Double;
                                 P3_Double : Java.Double;
                                 P4_Double : Java.Double);

   procedure SetFrameFromCenter (This : access Typ;
                                 P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                 P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class);

   function Contains (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                             P2_Double : Java.Double)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RectangularShape);
   pragma Export (Java, GetX, "getX");
   pragma Export (Java, GetY, "getY");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Import (Java, GetMinX, "getMinX");
   pragma Import (Java, GetMinY, "getMinY");
   pragma Import (Java, GetMaxX, "getMaxX");
   pragma Import (Java, GetMaxY, "getMaxY");
   pragma Import (Java, GetCenterX, "getCenterX");
   pragma Import (Java, GetCenterY, "getCenterY");
   pragma Import (Java, GetFrame, "getFrame");
   pragma Export (Java, IsEmpty, "isEmpty");
   -- pragma Import (Java, SetFrame, "setFrame");
   pragma Import (Java, SetFrameFromDiagonal, "setFrameFromDiagonal");
   pragma Import (Java, SetFrameFromCenter, "setFrameFromCenter");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetPathIterator, "getPathIterator");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Geom.RectangularShape;
pragma Import (Java, Java.Awt.Geom.RectangularShape, "java.awt.geom.RectangularShape");
pragma Extensions_Allowed (Off);
