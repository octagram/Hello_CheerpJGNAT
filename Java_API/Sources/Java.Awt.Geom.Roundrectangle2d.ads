pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
with Java.Awt.Geom.RectangularShape;
with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Geom.RoundRectangle2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Awt.Geom.RectangularShape.Typ(Shape_I,
                                                       Cloneable_I)
      with null record;

   --  protected
   function New_RoundRectangle2D (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetArcWidth (This : access Typ)
                         return Java.Double is abstract;

   function GetArcHeight (This : access Typ)
                          return Java.Double is abstract;

   procedure SetRoundRect (This : access Typ;
                           P1_Double : Java.Double;
                           P2_Double : Java.Double;
                           P3_Double : Java.Double;
                           P4_Double : Java.Double;
                           P5_Double : Java.Double;
                           P6_Double : Java.Double) is abstract;

   procedure SetRoundRect (This : access Typ;
                           P1_RoundRectangle2D : access Standard.Java.Awt.Geom.RoundRectangle2D.Typ'Class);

   procedure SetFrame (This : access Typ;
                       P1_Double : Java.Double;
                       P2_Double : Java.Double;
                       P3_Double : Java.Double;
                       P4_Double : Java.Double);

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RoundRectangle2D);
   pragma Export (Java, GetArcWidth, "getArcWidth");
   pragma Export (Java, GetArcHeight, "getArcHeight");
   pragma Export (Java, SetRoundRect, "setRoundRect");
   pragma Export (Java, SetFrame, "setFrame");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, Intersects, "intersects");
   pragma Export (Java, GetPathIterator, "getPathIterator");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");

end Java.Awt.Geom.RoundRectangle2D;
pragma Import (Java, Java.Awt.Geom.RoundRectangle2D, "java.awt.geom.RoundRectangle2D");
pragma Extensions_Allowed (Off);
