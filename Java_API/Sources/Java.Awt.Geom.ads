pragma Extensions_Allowed (On);
package Java.Awt.Geom is
   pragma Preelaborate;
end Java.Awt.Geom;
pragma Import (Java, Java.Awt.Geom, "java.awt.geom");
pragma Extensions_Allowed (Off);
