pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.PaintContext;
limited with Java.Awt.Rectangle;
limited with Java.Awt.RenderingHints;
with Java.Awt.Paint;
with Java.Lang.Object;

package Java.Awt.GradientPaint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Paint_I : Java.Awt.Paint.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GradientPaint (P1_Float : Java.Float;
                               P2_Float : Java.Float;
                               P3_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P4_Float : Java.Float;
                               P5_Float : Java.Float;
                               P6_Color : access Standard.Java.Awt.Color.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_GradientPaint (P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                               P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P3_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                               P4_Color : access Standard.Java.Awt.Color.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_GradientPaint (P1_Float : Java.Float;
                               P2_Float : Java.Float;
                               P3_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P4_Float : Java.Float;
                               P5_Float : Java.Float;
                               P6_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P7_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   function New_GradientPaint (P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                               P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P3_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                               P4_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P5_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPoint1 (This : access Typ)
                       return access Java.Awt.Geom.Point2D.Typ'Class;

   function GetColor1 (This : access Typ)
                       return access Java.Awt.Color.Typ'Class;

   function GetPoint2 (This : access Typ)
                       return access Java.Awt.Geom.Point2D.Typ'Class;

   function GetColor2 (This : access Typ)
                       return access Java.Awt.Color.Typ'Class;

   function IsCyclic (This : access Typ)
                      return Java.Boolean;

   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                           P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                           P4_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                           P5_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.PaintContext.Typ'Class;

   function GetTransparency (This : access Typ)
                             return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GradientPaint);
   pragma Import (Java, GetPoint1, "getPoint1");
   pragma Import (Java, GetColor1, "getColor1");
   pragma Import (Java, GetPoint2, "getPoint2");
   pragma Import (Java, GetColor2, "getColor2");
   pragma Import (Java, IsCyclic, "isCyclic");
   pragma Import (Java, CreateContext, "createContext");
   pragma Import (Java, GetTransparency, "getTransparency");

end Java.Awt.GradientPaint;
pragma Import (Java, Java.Awt.GradientPaint, "java.awt.GradientPaint");
pragma Extensions_Allowed (Off);
