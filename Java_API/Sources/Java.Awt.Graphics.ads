pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Image;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.Polygon;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
with Java.Lang.Object;

package Java.Awt.Graphics is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Graphics (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create (This : access Typ)
                    return access Java.Awt.Graphics.Typ'Class is abstract
                    with Export => "create", Convention => Java;

   function Create (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int)
                    return access Java.Awt.Graphics.Typ'Class
                    with Import => "create", Convention => Java;

   procedure Translate (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int) is abstract;

   function GetColor (This : access Typ)
                      return access Java.Awt.Color.Typ'Class is abstract;

   procedure SetColor (This : access Typ;
                       P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   procedure SetPaintMode (This : access Typ) is abstract;

   procedure SetXORMode (This : access Typ;
                         P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class is abstract;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class) is abstract;

   function GetFontMetrics (This : access Typ)
                            return access Java.Awt.FontMetrics.Typ'Class
                            with Import => "getFontMetrics", Convention => Java;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class is abstract
                            with Export => "getFontMetrics", Convention => Java;

   function GetClipBounds (This : access Typ)
                           return access Java.Awt.Rectangle.Typ'Class is abstract
                           with Export => "getClipBounds", Convention => Java;

   procedure ClipRect (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int) is abstract;

   procedure SetClip (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int) is abstract;

   function GetClip (This : access Typ)
                     return access Java.Awt.Shape.Typ'Class is abstract;

   procedure SetClip (This : access Typ;
                      P1_Shape : access Standard.Java.Awt.Shape.Typ'Class) is abstract;

   procedure CopyArea (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int) is abstract;

   procedure DrawLine (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int) is abstract;

   procedure FillRect (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int) is abstract;

   procedure DrawRect (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   procedure ClearRect (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int) is abstract;

   procedure DrawRoundRect (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int) is abstract;

   procedure FillRoundRect (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int) is abstract;

   procedure Draw3DRect (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Boolean : Java.Boolean);

   procedure Fill3DRect (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Boolean : Java.Boolean);

   procedure DrawOval (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int) is abstract;

   procedure FillOval (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int) is abstract;

   procedure DrawArc (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int;
                      P6_Int : Java.Int) is abstract;

   procedure FillArc (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int;
                      P6_Int : Java.Int) is abstract;

   procedure DrawPolyline (This : access Typ;
                           P1_Int_Arr : Java.Int_Arr;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int) is abstract;

   procedure DrawPolygon (This : access Typ;
                          P1_Int_Arr : Java.Int_Arr;
                          P2_Int_Arr : Java.Int_Arr;
                          P3_Int : Java.Int) is abstract
                          with Export => "drawPolygon", Convention => Java;

   procedure DrawPolygon (This : access Typ;
                          P1_Polygon : access Standard.Java.Awt.Polygon.Typ'Class)
                          with Import => "drawPolygon", Convention => Java;

   procedure FillPolygon (This : access Typ;
                          P1_Int_Arr : Java.Int_Arr;
                          P2_Int_Arr : Java.Int_Arr;
                          P3_Int : Java.Int) is abstract
                          with Export => "fillPolygon", Convention => Java;

   procedure FillPolygon (This : access Typ;
                          P1_Polygon : access Standard.Java.Awt.Polygon.Typ'Class)
                          with Import => "fillPolygon", Convention => Java;

   procedure DrawString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;

   procedure DrawString (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;

   procedure DrawChars (This : access Typ;
                        P1_Char_Arr : Java.Char_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   procedure DrawBytes (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean is abstract;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean is abstract;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Color : access Standard.Java.Awt.Color.Typ'Class;
                       P5_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean is abstract;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Color : access Standard.Java.Awt.Color.Typ'Class;
                       P7_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean is abstract;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int;
                       P8_Int : Java.Int;
                       P9_Int : Java.Int;
                       P10_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean is abstract;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int;
                       P8_Int : Java.Int;
                       P9_Int : Java.Int;
                       P10_Color : access Standard.Java.Awt.Color.Typ'Class;
                       P11_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean is abstract;

   procedure Dispose (This : access Typ) is abstract;

   procedure Finalize (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HitClip (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int;
                     P4_Int : Java.Int)
                     return Java.Boolean;

   function GetClipBounds (This : access Typ;
                           P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                           return access Java.Awt.Rectangle.Typ'Class
                           with Import => "getClipBounds", Convention => Java;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Graphics);
   -- pragma Import (Java, Create, "create");
   pragma Export (Java, Translate, "translate");
   pragma Export (Java, GetColor, "getColor");
   pragma Export (Java, SetColor, "setColor");
   pragma Export (Java, SetPaintMode, "setPaintMode");
   pragma Export (Java, SetXORMode, "setXORMode");
   pragma Export (Java, GetFont, "getFont");
   pragma Export (Java, SetFont, "setFont");
   -- pragma Import (Java, GetFontMetrics, "getFontMetrics");
   -- pragma Import (Java, GetClipBounds, "getClipBounds");
   pragma Export (Java, ClipRect, "clipRect");
   pragma Export (Java, SetClip, "setClip");
   pragma Export (Java, GetClip, "getClip");
   pragma Export (Java, CopyArea, "copyArea");
   pragma Export (Java, DrawLine, "drawLine");
   pragma Export (Java, FillRect, "fillRect");
   pragma Import (Java, DrawRect, "drawRect");
   pragma Export (Java, ClearRect, "clearRect");
   pragma Export (Java, DrawRoundRect, "drawRoundRect");
   pragma Export (Java, FillRoundRect, "fillRoundRect");
   pragma Import (Java, Draw3DRect, "draw3DRect");
   pragma Import (Java, Fill3DRect, "fill3DRect");
   pragma Export (Java, DrawOval, "drawOval");
   pragma Export (Java, FillOval, "fillOval");
   pragma Export (Java, DrawArc, "drawArc");
   pragma Export (Java, FillArc, "fillArc");
   pragma Export (Java, DrawPolyline, "drawPolyline");
   -- pragma Import (Java, DrawPolygon, "drawPolygon");
   -- pragma Import (Java, FillPolygon, "fillPolygon");
   pragma Export (Java, DrawString, "drawString");
   pragma Import (Java, DrawChars, "drawChars");
   pragma Import (Java, DrawBytes, "drawBytes");
   pragma Export (Java, DrawImage, "drawImage");
   pragma Export (Java, Dispose, "dispose");
   pragma Import (Java, Finalize, "finalize");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HitClip, "hitClip");

end Java.Awt.Graphics;
pragma Import (Java, Java.Awt.Graphics, "java.awt.Graphics");
pragma Extensions_Allowed (Off);
