pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Composite;
limited with Java.Awt.Font.FontRenderContext;
limited with Java.Awt.Font.GlyphVector;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Image;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.BufferedImageOp;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.Image.Renderable.RenderableImage;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Awt.Paint;
limited with Java.Awt.Rectangle;
limited with Java.Awt.RenderingHints;
limited with Java.Awt.RenderingHints.Key;
limited with Java.Awt.Shape;
limited with Java.Awt.Stroke;
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Util.Map;
with Java.Awt.Graphics;
with Java.Lang.Object;

package Java.Awt.Graphics2D is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Awt.Graphics.Typ
      with null record;

   --  protected
   function New_Graphics2D (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Draw3DRect (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Boolean : Java.Boolean);

   procedure Fill3DRect (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Boolean : Java.Boolean);

   procedure Draw (This : access Typ;
                   P1_Shape : access Standard.Java.Awt.Shape.Typ'Class) is abstract;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                       P3_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean is abstract;

   procedure DrawImage (This : access Typ;
                        P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                        P2_BufferedImageOp : access Standard.Java.Awt.Image.BufferedImageOp.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int) is abstract;

   procedure DrawRenderedImage (This : access Typ;
                                P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class;
                                P2_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class) is abstract;

   procedure DrawRenderableImage (This : access Typ;
                                  P1_RenderableImage : access Standard.Java.Awt.Image.Renderable.RenderableImage.Typ'Class;
                                  P2_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class) is abstract;

   procedure DrawString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;

   procedure DrawString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Float : Java.Float;
                         P3_Float : Java.Float) is abstract;

   procedure DrawString (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;

   procedure DrawString (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Float : Java.Float;
                         P3_Float : Java.Float) is abstract;

   procedure DrawGlyphVector (This : access Typ;
                              P1_GlyphVector : access Standard.Java.Awt.Font.GlyphVector.Typ'Class;
                              P2_Float : Java.Float;
                              P3_Float : Java.Float) is abstract;

   procedure Fill (This : access Typ;
                   P1_Shape : access Standard.Java.Awt.Shape.Typ'Class) is abstract;

   function Hit (This : access Typ;
                 P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                 P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                 P3_Boolean : Java.Boolean)
                 return Java.Boolean is abstract;

   function GetDeviceConfiguration (This : access Typ)
                                    return access Java.Awt.GraphicsConfiguration.Typ'Class is abstract;

   procedure SetComposite (This : access Typ;
                           P1_Composite : access Standard.Java.Awt.Composite.Typ'Class) is abstract;

   procedure SetPaint (This : access Typ;
                       P1_Paint : access Standard.Java.Awt.Paint.Typ'Class) is abstract;

   procedure SetStroke (This : access Typ;
                        P1_Stroke : access Standard.Java.Awt.Stroke.Typ'Class) is abstract;

   procedure SetRenderingHint (This : access Typ;
                               P1_Key : access Standard.Java.Awt.RenderingHints.Key.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetRenderingHint (This : access Typ;
                              P1_Key : access Standard.Java.Awt.RenderingHints.Key.Typ'Class)
                              return access Java.Lang.Object.Typ'Class is abstract;

   procedure SetRenderingHints (This : access Typ;
                                P1_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;

   procedure AddRenderingHints (This : access Typ;
                                P1_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;

   function GetRenderingHints (This : access Typ)
                               return access Java.Awt.RenderingHints.Typ'Class is abstract;

   procedure Translate (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int) is abstract;

   procedure Translate (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double) is abstract;

   procedure Rotate (This : access Typ;
                     P1_Double : Java.Double) is abstract;

   procedure Rotate (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double;
                     P3_Double : Java.Double) is abstract;

   procedure Scale (This : access Typ;
                    P1_Double : Java.Double;
                    P2_Double : Java.Double) is abstract;

   procedure Shear (This : access Typ;
                    P1_Double : Java.Double;
                    P2_Double : Java.Double) is abstract;

   procedure Transform (This : access Typ;
                        P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class) is abstract;

   procedure SetTransform (This : access Typ;
                           P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class) is abstract;

   function GetTransform (This : access Typ)
                          return access Java.Awt.Geom.AffineTransform.Typ'Class is abstract;

   function GetPaint (This : access Typ)
                      return access Java.Awt.Paint.Typ'Class is abstract;

   function GetComposite (This : access Typ)
                          return access Java.Awt.Composite.Typ'Class is abstract;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class is abstract;

   function GetStroke (This : access Typ)
                       return access Java.Awt.Stroke.Typ'Class is abstract;

   procedure Clip (This : access Typ;
                   P1_Shape : access Standard.Java.Awt.Shape.Typ'Class) is abstract;

   function GetFontRenderContext (This : access Typ)
                                  return access Java.Awt.Font.FontRenderContext.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Graphics2D);
   pragma Export (Java, Draw3DRect, "draw3DRect");
   pragma Export (Java, Fill3DRect, "fill3DRect");
   pragma Export (Java, Draw, "draw");
   pragma Export (Java, DrawImage, "drawImage");
   pragma Export (Java, DrawRenderedImage, "drawRenderedImage");
   pragma Export (Java, DrawRenderableImage, "drawRenderableImage");
   pragma Export (Java, DrawString, "drawString");
   pragma Export (Java, DrawGlyphVector, "drawGlyphVector");
   pragma Export (Java, Fill, "fill");
   pragma Export (Java, Hit, "hit");
   pragma Export (Java, GetDeviceConfiguration, "getDeviceConfiguration");
   pragma Export (Java, SetComposite, "setComposite");
   pragma Export (Java, SetPaint, "setPaint");
   pragma Export (Java, SetStroke, "setStroke");
   pragma Export (Java, SetRenderingHint, "setRenderingHint");
   pragma Export (Java, GetRenderingHint, "getRenderingHint");
   pragma Export (Java, SetRenderingHints, "setRenderingHints");
   pragma Export (Java, AddRenderingHints, "addRenderingHints");
   pragma Export (Java, GetRenderingHints, "getRenderingHints");
   pragma Export (Java, Translate, "translate");
   pragma Export (Java, Rotate, "rotate");
   pragma Export (Java, Scale, "scale");
   pragma Export (Java, Shear, "shear");
   pragma Export (Java, Transform, "transform");
   pragma Export (Java, SetTransform, "setTransform");
   pragma Export (Java, GetTransform, "getTransform");
   pragma Export (Java, GetPaint, "getPaint");
   pragma Export (Java, GetComposite, "getComposite");
   pragma Export (Java, SetBackground, "setBackground");
   pragma Export (Java, GetBackground, "getBackground");
   pragma Export (Java, GetStroke, "getStroke");
   pragma Export (Java, Clip, "clip");
   pragma Export (Java, GetFontRenderContext, "getFontRenderContext");

end Java.Awt.Graphics2D;
pragma Import (Java, Java.Awt.Graphics2D, "java.awt.Graphics2D");
pragma Extensions_Allowed (Off);
