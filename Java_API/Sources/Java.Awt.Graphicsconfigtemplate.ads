pragma Extensions_Allowed (On);
limited with Java.Awt.GraphicsConfiguration;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.GraphicsConfigTemplate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_GraphicsConfigTemplate (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBestConfiguration (This : access Typ;
                                  P1_GraphicsConfiguration_Arr : access Java.Awt.GraphicsConfiguration.Arr_Obj)
                                  return access Java.Awt.GraphicsConfiguration.Typ'Class is abstract;

   function IsGraphicsConfigSupported (This : access Typ;
                                       P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class)
                                       return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   REQUIRED : constant Java.Int;

   --  final
   PREFERRED : constant Java.Int;

   --  final
   UNNECESSARY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GraphicsConfigTemplate);
   pragma Export (Java, GetBestConfiguration, "getBestConfiguration");
   pragma Export (Java, IsGraphicsConfigSupported, "isGraphicsConfigSupported");
   pragma Import (Java, REQUIRED, "REQUIRED");
   pragma Import (Java, PREFERRED, "PREFERRED");
   pragma Import (Java, UNNECESSARY, "UNNECESSARY");

end Java.Awt.GraphicsConfigTemplate;
pragma Import (Java, Java.Awt.GraphicsConfigTemplate, "java.awt.GraphicsConfigTemplate");
pragma Extensions_Allowed (Off);
