pragma Extensions_Allowed (On);
limited with Java.Awt.BufferCapabilities;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.GraphicsDevice;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.VolatileImage;
limited with Java.Awt.ImageCapabilities;
limited with Java.Awt.Rectangle;
with Java.Lang.Object;

package Java.Awt.GraphicsConfiguration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_GraphicsConfiguration (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDevice (This : access Typ)
                       return access Java.Awt.GraphicsDevice.Typ'Class is abstract;

   function CreateCompatibleImage (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int)
                                   return access Java.Awt.Image.BufferedImage.Typ'Class is abstract
                                   with Export => "createCompatibleImage", Convention => Java;

   function CreateCompatibleImage (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int)
                                   return access Java.Awt.Image.BufferedImage.Typ'Class
                                   with Import => "createCompatibleImage", Convention => Java;

   function CreateCompatibleVolatileImage (This : access Typ;
                                           P1_Int : Java.Int;
                                           P2_Int : Java.Int)
                                           return access Java.Awt.Image.VolatileImage.Typ'Class;

   function CreateCompatibleVolatileImage (This : access Typ;
                                           P1_Int : Java.Int;
                                           P2_Int : Java.Int;
                                           P3_Int : Java.Int)
                                           return access Java.Awt.Image.VolatileImage.Typ'Class;

   function CreateCompatibleVolatileImage (This : access Typ;
                                           P1_Int : Java.Int;
                                           P2_Int : Java.Int;
                                           P3_ImageCapabilities : access Standard.Java.Awt.ImageCapabilities.Typ'Class)
                                           return access Java.Awt.Image.VolatileImage.Typ'Class;
   --  can raise Java.Awt.AWTException.Except

   function CreateCompatibleVolatileImage (This : access Typ;
                                           P1_Int : Java.Int;
                                           P2_Int : Java.Int;
                                           P3_ImageCapabilities : access Standard.Java.Awt.ImageCapabilities.Typ'Class;
                                           P4_Int : Java.Int)
                                           return access Java.Awt.Image.VolatileImage.Typ'Class;
   --  can raise Java.Awt.AWTException.Except

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class is abstract;

   function GetColorModel (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Awt.Image.ColorModel.Typ'Class is abstract;

   function GetDefaultTransform (This : access Typ)
                                 return access Java.Awt.Geom.AffineTransform.Typ'Class is abstract;

   function GetNormalizingTransform (This : access Typ)
                                     return access Java.Awt.Geom.AffineTransform.Typ'Class is abstract;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class is abstract;

   function GetBufferCapabilities (This : access Typ)
                                   return access Java.Awt.BufferCapabilities.Typ'Class;

   function GetImageCapabilities (This : access Typ)
                                  return access Java.Awt.ImageCapabilities.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GraphicsConfiguration);
   pragma Export (Java, GetDevice, "getDevice");
   -- pragma Import (Java, CreateCompatibleImage, "createCompatibleImage");
   pragma Import (Java, CreateCompatibleVolatileImage, "createCompatibleVolatileImage");
   pragma Export (Java, GetColorModel, "getColorModel");
   pragma Export (Java, GetDefaultTransform, "getDefaultTransform");
   pragma Export (Java, GetNormalizingTransform, "getNormalizingTransform");
   pragma Export (Java, GetBounds, "getBounds");
   pragma Import (Java, GetBufferCapabilities, "getBufferCapabilities");
   pragma Import (Java, GetImageCapabilities, "getImageCapabilities");

end Java.Awt.GraphicsConfiguration;
pragma Import (Java, Java.Awt.GraphicsConfiguration, "java.awt.GraphicsConfiguration");
pragma Extensions_Allowed (Off);
