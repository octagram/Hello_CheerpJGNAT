pragma Extensions_Allowed (On);
limited with Java.Awt.DisplayMode;
limited with Java.Awt.GraphicsConfigTemplate;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Window;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.GraphicsDevice is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_GraphicsDevice (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return Java.Int is abstract;

   function GetIDstring (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetConfigurations (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function GetDefaultConfiguration (This : access Typ)
                                     return access Java.Awt.GraphicsConfiguration.Typ'Class is abstract;

   function GetBestConfiguration (This : access Typ;
                                  P1_GraphicsConfigTemplate : access Standard.Java.Awt.GraphicsConfigTemplate.Typ'Class)
                                  return access Java.Awt.GraphicsConfiguration.Typ'Class;

   function IsFullScreenSupported (This : access Typ)
                                   return Java.Boolean;

   procedure SetFullScreenWindow (This : access Typ;
                                  P1_Window : access Standard.Java.Awt.Window.Typ'Class);

   function GetFullScreenWindow (This : access Typ)
                                 return access Java.Awt.Window.Typ'Class;

   function IsDisplayChangeSupported (This : access Typ)
                                      return Java.Boolean;

   procedure SetDisplayMode (This : access Typ;
                             P1_DisplayMode : access Standard.Java.Awt.DisplayMode.Typ'Class);

   function GetDisplayMode (This : access Typ)
                            return access Java.Awt.DisplayMode.Typ'Class;

   function GetDisplayModes (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetAvailableAcceleratedMemory (This : access Typ)
                                           return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_RASTER_SCREEN : constant Java.Int;

   --  final
   TYPE_PRINTER : constant Java.Int;

   --  final
   TYPE_IMAGE_BUFFER : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GraphicsDevice);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetIDstring, "getIDstring");
   pragma Export (Java, GetConfigurations, "getConfigurations");
   pragma Export (Java, GetDefaultConfiguration, "getDefaultConfiguration");
   pragma Export (Java, GetBestConfiguration, "getBestConfiguration");
   pragma Export (Java, IsFullScreenSupported, "isFullScreenSupported");
   pragma Export (Java, SetFullScreenWindow, "setFullScreenWindow");
   pragma Export (Java, GetFullScreenWindow, "getFullScreenWindow");
   pragma Export (Java, IsDisplayChangeSupported, "isDisplayChangeSupported");
   pragma Export (Java, SetDisplayMode, "setDisplayMode");
   pragma Export (Java, GetDisplayMode, "getDisplayMode");
   pragma Export (Java, GetDisplayModes, "getDisplayModes");
   pragma Export (Java, GetAvailableAcceleratedMemory, "getAvailableAcceleratedMemory");
   pragma Import (Java, TYPE_RASTER_SCREEN, "TYPE_RASTER_SCREEN");
   pragma Import (Java, TYPE_PRINTER, "TYPE_PRINTER");
   pragma Import (Java, TYPE_IMAGE_BUFFER, "TYPE_IMAGE_BUFFER");

end Java.Awt.GraphicsDevice;
pragma Import (Java, Java.Awt.GraphicsDevice, "java.awt.GraphicsDevice");
pragma Extensions_Allowed (Off);
