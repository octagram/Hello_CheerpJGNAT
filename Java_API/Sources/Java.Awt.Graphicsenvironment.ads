pragma Extensions_Allowed (On);
limited with Java.Awt.Font;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.GraphicsDevice;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Lang.Object;

package Java.Awt.GraphicsEnvironment is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_GraphicsEnvironment (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetLocalGraphicsEnvironment return access Java.Awt.GraphicsEnvironment.Typ'Class;

   function IsHeadless return Java.Boolean;

   function IsHeadlessInstance (This : access Typ)
                                return Java.Boolean;

   function GetScreenDevices (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   function GetDefaultScreenDevice (This : access Typ)
                                    return access Java.Awt.GraphicsDevice.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   function CreateGraphics (This : access Typ;
                            P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class)
                            return access Java.Awt.Graphics2D.Typ'Class is abstract;

   function GetAllFonts (This : access Typ)
                         return Standard.Java.Lang.Object.Ref is abstract;

   function GetAvailableFontFamilyNames (This : access Typ)
                                         return Standard.Java.Lang.Object.Ref is abstract;

   function GetAvailableFontFamilyNames (This : access Typ;
                                         P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                         return Standard.Java.Lang.Object.Ref is abstract;

   function RegisterFont (This : access Typ;
                          P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                          return Java.Boolean;

   procedure PreferLocaleFonts (This : access Typ);

   procedure PreferProportionalFonts (This : access Typ);

   function GetCenterPoint (This : access Typ)
                            return access Java.Awt.Point.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function GetMaximumWindowBounds (This : access Typ)
                                    return access Java.Awt.Rectangle.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GraphicsEnvironment);
   pragma Export (Java, GetLocalGraphicsEnvironment, "getLocalGraphicsEnvironment");
   pragma Export (Java, IsHeadless, "isHeadless");
   pragma Export (Java, IsHeadlessInstance, "isHeadlessInstance");
   pragma Export (Java, GetScreenDevices, "getScreenDevices");
   pragma Export (Java, GetDefaultScreenDevice, "getDefaultScreenDevice");
   pragma Export (Java, CreateGraphics, "createGraphics");
   pragma Export (Java, GetAllFonts, "getAllFonts");
   pragma Export (Java, GetAvailableFontFamilyNames, "getAvailableFontFamilyNames");
   pragma Export (Java, RegisterFont, "registerFont");
   pragma Export (Java, PreferLocaleFonts, "preferLocaleFonts");
   pragma Export (Java, PreferProportionalFonts, "preferProportionalFonts");
   pragma Export (Java, GetCenterPoint, "getCenterPoint");
   pragma Export (Java, GetMaximumWindowBounds, "getMaximumWindowBounds");

end Java.Awt.GraphicsEnvironment;
pragma Import (Java, Java.Awt.GraphicsEnvironment, "java.awt.GraphicsEnvironment");
pragma Extensions_Allowed (Off);
