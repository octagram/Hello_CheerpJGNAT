pragma Extensions_Allowed (On);
limited with Java.Awt.Insets;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.GridBagConstraints is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Gridx : Java.Int;
      pragma Import (Java, Gridx, "gridx");

      Gridy : Java.Int;
      pragma Import (Java, Gridy, "gridy");

      Gridwidth : Java.Int;
      pragma Import (Java, Gridwidth, "gridwidth");

      Gridheight : Java.Int;
      pragma Import (Java, Gridheight, "gridheight");

      Weightx : Java.Double;
      pragma Import (Java, Weightx, "weightx");

      Weighty : Java.Double;
      pragma Import (Java, Weighty, "weighty");

      Anchor : Java.Int;
      pragma Import (Java, Anchor, "anchor");

      Fill : Java.Int;
      pragma Import (Java, Fill, "fill");

      Insets : access Java.Awt.Insets.Typ'Class;
      pragma Import (Java, Insets, "insets");

      Ipadx : Java.Int;
      pragma Import (Java, Ipadx, "ipadx");

      Ipady : Java.Int;
      pragma Import (Java, Ipady, "ipady");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GridBagConstraints (This : Ref := null)
                                    return Ref;

   function New_GridBagConstraints (P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Double : Java.Double;
                                    P6_Double : Java.Double;
                                    P7_Int : Java.Int;
                                    P8_Int : Java.Int;
                                    P9_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                                    P10_Int : Java.Int;
                                    P11_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RELATIVE : constant Java.Int;

   --  final
   REMAINDER : constant Java.Int;

   --  final
   NONE : constant Java.Int;

   --  final
   BOTH : constant Java.Int;

   --  final
   HORIZONTAL : constant Java.Int;

   --  final
   VERTICAL : constant Java.Int;

   --  final
   CENTER : constant Java.Int;

   --  final
   NORTH : constant Java.Int;

   --  final
   NORTHEAST : constant Java.Int;

   --  final
   EAST : constant Java.Int;

   --  final
   SOUTHEAST : constant Java.Int;

   --  final
   SOUTH : constant Java.Int;

   --  final
   SOUTHWEST : constant Java.Int;

   --  final
   WEST : constant Java.Int;

   --  final
   NORTHWEST : constant Java.Int;

   --  final
   PAGE_START : constant Java.Int;

   --  final
   PAGE_END : constant Java.Int;

   --  final
   LINE_START : constant Java.Int;

   --  final
   LINE_END : constant Java.Int;

   --  final
   FIRST_LINE_START : constant Java.Int;

   --  final
   FIRST_LINE_END : constant Java.Int;

   --  final
   LAST_LINE_START : constant Java.Int;

   --  final
   LAST_LINE_END : constant Java.Int;

   --  final
   BASELINE : constant Java.Int;

   --  final
   BASELINE_LEADING : constant Java.Int;

   --  final
   BASELINE_TRAILING : constant Java.Int;

   --  final
   ABOVE_BASELINE : constant Java.Int;

   --  final
   ABOVE_BASELINE_LEADING : constant Java.Int;

   --  final
   ABOVE_BASELINE_TRAILING : constant Java.Int;

   --  final
   BELOW_BASELINE : constant Java.Int;

   --  final
   BELOW_BASELINE_LEADING : constant Java.Int;

   --  final
   BELOW_BASELINE_TRAILING : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GridBagConstraints);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, RELATIVE, "RELATIVE");
   pragma Import (Java, REMAINDER, "REMAINDER");
   pragma Import (Java, NONE, "NONE");
   pragma Import (Java, BOTH, "BOTH");
   pragma Import (Java, HORIZONTAL, "HORIZONTAL");
   pragma Import (Java, VERTICAL, "VERTICAL");
   pragma Import (Java, CENTER, "CENTER");
   pragma Import (Java, NORTH, "NORTH");
   pragma Import (Java, NORTHEAST, "NORTHEAST");
   pragma Import (Java, EAST, "EAST");
   pragma Import (Java, SOUTHEAST, "SOUTHEAST");
   pragma Import (Java, SOUTH, "SOUTH");
   pragma Import (Java, SOUTHWEST, "SOUTHWEST");
   pragma Import (Java, WEST, "WEST");
   pragma Import (Java, NORTHWEST, "NORTHWEST");
   pragma Import (Java, PAGE_START, "PAGE_START");
   pragma Import (Java, PAGE_END, "PAGE_END");
   pragma Import (Java, LINE_START, "LINE_START");
   pragma Import (Java, LINE_END, "LINE_END");
   pragma Import (Java, FIRST_LINE_START, "FIRST_LINE_START");
   pragma Import (Java, FIRST_LINE_END, "FIRST_LINE_END");
   pragma Import (Java, LAST_LINE_START, "LAST_LINE_START");
   pragma Import (Java, LAST_LINE_END, "LAST_LINE_END");
   pragma Import (Java, BASELINE, "BASELINE");
   pragma Import (Java, BASELINE_LEADING, "BASELINE_LEADING");
   pragma Import (Java, BASELINE_TRAILING, "BASELINE_TRAILING");
   pragma Import (Java, ABOVE_BASELINE, "ABOVE_BASELINE");
   pragma Import (Java, ABOVE_BASELINE_LEADING, "ABOVE_BASELINE_LEADING");
   pragma Import (Java, ABOVE_BASELINE_TRAILING, "ABOVE_BASELINE_TRAILING");
   pragma Import (Java, BELOW_BASELINE, "BELOW_BASELINE");
   pragma Import (Java, BELOW_BASELINE_LEADING, "BELOW_BASELINE_LEADING");
   pragma Import (Java, BELOW_BASELINE_TRAILING, "BELOW_BASELINE_TRAILING");

end Java.Awt.GridBagConstraints;
pragma Import (Java, Java.Awt.GridBagConstraints, "java.awt.GridBagConstraints");
pragma Extensions_Allowed (Off);
