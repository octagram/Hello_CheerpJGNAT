pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Awt.GridBagConstraints;
limited with Java.Awt.GridBagLayoutInfo;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
with Java.Awt.LayoutManager2;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.GridBagLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Comptable : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, Comptable, "comptable");

      --  protected
      DefaultConstraints : access Java.Awt.GridBagConstraints.Typ'Class;
      pragma Import (Java, DefaultConstraints, "defaultConstraints");

      --  protected
      LayoutInfo : access Java.Awt.GridBagLayoutInfo.Typ'Class;
      pragma Import (Java, LayoutInfo, "layoutInfo");

      ColumnWidths : Java.Int_Arr;
      pragma Import (Java, ColumnWidths, "columnWidths");

      RowHeights : Java.Int_Arr;
      pragma Import (Java, RowHeights, "rowHeights");

      ColumnWeights : Java.Double_Arr;
      pragma Import (Java, ColumnWeights, "columnWeights");

      RowWeights : Java.Double_Arr;
      pragma Import (Java, RowWeights, "rowWeights");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GridBagLayout (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetConstraints (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_GridBagConstraints : access Standard.Java.Awt.GridBagConstraints.Typ'Class);

   function GetConstraints (This : access Typ;
                            P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return access Java.Awt.GridBagConstraints.Typ'Class;

   --  protected
   function LookupConstraints (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return access Java.Awt.GridBagConstraints.Typ'Class;

   function GetLayoutOrigin (This : access Typ)
                             return access Java.Awt.Point.Typ'Class;

   function GetLayoutDimensions (This : access Typ)
                                 return Java.Int_Arr_2;

   function GetLayoutWeights (This : access Typ)
                              return Java.Double_Arr_2;

   function Location (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return access Java.Awt.Point.Typ'Class;

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function MaximumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function GetLayoutAlignmentX (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   function GetLayoutAlignmentY (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   procedure InvalidateLayout (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   function GetLayoutInfo (This : access Typ;
                           P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                           P2_Int : Java.Int)
                           return access Java.Awt.GridBagLayoutInfo.Typ'Class;

   --  protected
   procedure AdjustForGravity (This : access Typ;
                               P1_GridBagConstraints : access Standard.Java.Awt.GridBagConstraints.Typ'Class;
                               P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function GetMinSize (This : access Typ;
                        P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                        P2_GridBagLayoutInfo : access Standard.Java.Awt.GridBagLayoutInfo.Typ'Class)
                        return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure ArrangeGrid (This : access Typ;
                          P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   MAXGRIDSIZE : constant Java.Int;

   --  protected  final
   MINSIZE : constant Java.Int;

   --  protected  final
   PREFERREDSIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GridBagLayout);
   pragma Import (Java, SetConstraints, "setConstraints");
   pragma Import (Java, GetConstraints, "getConstraints");
   pragma Import (Java, LookupConstraints, "lookupConstraints");
   pragma Import (Java, GetLayoutOrigin, "getLayoutOrigin");
   pragma Import (Java, GetLayoutDimensions, "getLayoutDimensions");
   pragma Import (Java, GetLayoutWeights, "getLayoutWeights");
   pragma Import (Java, Location, "location");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, MaximumLayoutSize, "maximumLayoutSize");
   pragma Import (Java, GetLayoutAlignmentX, "getLayoutAlignmentX");
   pragma Import (Java, GetLayoutAlignmentY, "getLayoutAlignmentY");
   pragma Import (Java, InvalidateLayout, "invalidateLayout");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetLayoutInfo, "getLayoutInfo");
   pragma Import (Java, AdjustForGravity, "adjustForGravity");
   pragma Import (Java, GetMinSize, "getMinSize");
   pragma Import (Java, ArrangeGrid, "arrangeGrid");
   pragma Import (Java, MAXGRIDSIZE, "MAXGRIDSIZE");
   pragma Import (Java, MINSIZE, "MINSIZE");
   pragma Import (Java, PREFERREDSIZE, "PREFERREDSIZE");

end Java.Awt.GridBagLayout;
pragma Import (Java, Java.Awt.GridBagLayout, "java.awt.GridBagLayout");
pragma Extensions_Allowed (Off);
