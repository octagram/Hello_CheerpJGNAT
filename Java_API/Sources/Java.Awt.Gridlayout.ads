pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
with Java.Awt.LayoutManager;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.GridLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GridLayout (This : Ref := null)
                            return Ref;

   function New_GridLayout (P1_Int : Java.Int;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_GridLayout (P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRows (This : access Typ)
                     return Java.Int;

   procedure SetRows (This : access Typ;
                      P1_Int : Java.Int);

   function GetColumns (This : access Typ)
                        return Java.Int;

   procedure SetColumns (This : access Typ;
                         P1_Int : Java.Int);

   function GetHgap (This : access Typ)
                     return Java.Int;

   procedure SetHgap (This : access Typ;
                      P1_Int : Java.Int);

   function GetVgap (This : access Typ)
                     return Java.Int;

   procedure SetVgap (This : access Typ;
                      P1_Int : Java.Int);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GridLayout);
   pragma Import (Java, GetRows, "getRows");
   pragma Import (Java, SetRows, "setRows");
   pragma Import (Java, GetColumns, "getColumns");
   pragma Import (Java, SetColumns, "setColumns");
   pragma Import (Java, GetHgap, "getHgap");
   pragma Import (Java, SetHgap, "setHgap");
   pragma Import (Java, GetVgap, "getVgap");
   pragma Import (Java, SetVgap, "setVgap");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, ToString, "toString");

end Java.Awt.GridLayout;
pragma Import (Java, Java.Awt.GridLayout, "java.awt.GridLayout");
pragma Extensions_Allowed (Off);
