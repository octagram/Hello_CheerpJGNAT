pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Component;
limited with Java.Lang.Character.Subset;
limited with Java.Util.Locale;
with Java.Lang.Object;

package Java.Awt.Im.InputContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_InputContext (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance return access Java.Awt.Im.InputContext.Typ'Class;

   function SelectInputMethod (This : access Typ;
                               P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                               return Java.Boolean;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   procedure SetCharacterSubsets (This : access Typ;
                                  P1_Subset_Arr : access Java.Lang.Character.Subset.Arr_Obj);

   procedure SetCompositionEnabled (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function IsCompositionEnabled (This : access Typ)
                                  return Java.Boolean;

   procedure Reconvert (This : access Typ);

   procedure DispatchEvent (This : access Typ;
                            P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   procedure RemoveNotify (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure EndComposition (This : access Typ);

   procedure Dispose (This : access Typ);

   function GetInputMethodControlObject (This : access Typ)
                                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputContext);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, SelectInputMethod, "selectInputMethod");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, SetCharacterSubsets, "setCharacterSubsets");
   pragma Import (Java, SetCompositionEnabled, "setCompositionEnabled");
   pragma Import (Java, IsCompositionEnabled, "isCompositionEnabled");
   pragma Import (Java, Reconvert, "reconvert");
   pragma Import (Java, DispatchEvent, "dispatchEvent");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, EndComposition, "endComposition");
   pragma Import (Java, Dispose, "dispose");
   pragma Import (Java, GetInputMethodControlObject, "getInputMethodControlObject");

end Java.Awt.Im.InputContext;
pragma Import (Java, Java.Awt.Im.InputContext, "java.awt.im.InputContext");
pragma Extensions_Allowed (Off);
