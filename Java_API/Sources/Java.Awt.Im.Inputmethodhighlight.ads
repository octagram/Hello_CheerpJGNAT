pragma Extensions_Allowed (On);
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Awt.Im.InputMethodHighlight is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InputMethodHighlight (P1_Boolean : Java.Boolean;
                                      P2_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   function New_InputMethodHighlight (P1_Boolean : Java.Boolean;
                                      P2_Int : Java.Int;
                                      P3_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   function New_InputMethodHighlight (P1_Boolean : Java.Boolean;
                                      P2_Int : Java.Int;
                                      P3_Int : Java.Int;
                                      P4_Map : access Standard.Java.Util.Map.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   function GetState (This : access Typ)
                      return Java.Int;

   function GetVariation (This : access Typ)
                          return Java.Int;

   function GetStyle (This : access Typ)
                      return access Java.Util.Map.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RAW_TEXT : constant Java.Int;

   --  final
   CONVERTED_TEXT : constant Java.Int;

   --  final
   UNSELECTED_RAW_TEXT_HIGHLIGHT : access Java.Awt.Im.InputMethodHighlight.Typ'Class;

   --  final
   SELECTED_RAW_TEXT_HIGHLIGHT : access Java.Awt.Im.InputMethodHighlight.Typ'Class;

   --  final
   UNSELECTED_CONVERTED_TEXT_HIGHLIGHT : access Java.Awt.Im.InputMethodHighlight.Typ'Class;

   --  final
   SELECTED_CONVERTED_TEXT_HIGHLIGHT : access Java.Awt.Im.InputMethodHighlight.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputMethodHighlight);
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, GetState, "getState");
   pragma Import (Java, GetVariation, "getVariation");
   pragma Import (Java, GetStyle, "getStyle");
   pragma Import (Java, RAW_TEXT, "RAW_TEXT");
   pragma Import (Java, CONVERTED_TEXT, "CONVERTED_TEXT");
   pragma Import (Java, UNSELECTED_RAW_TEXT_HIGHLIGHT, "UNSELECTED_RAW_TEXT_HIGHLIGHT");
   pragma Import (Java, SELECTED_RAW_TEXT_HIGHLIGHT, "SELECTED_RAW_TEXT_HIGHLIGHT");
   pragma Import (Java, UNSELECTED_CONVERTED_TEXT_HIGHLIGHT, "UNSELECTED_CONVERTED_TEXT_HIGHLIGHT");
   pragma Import (Java, SELECTED_CONVERTED_TEXT_HIGHLIGHT, "SELECTED_CONVERTED_TEXT_HIGHLIGHT");

end Java.Awt.Im.InputMethodHighlight;
pragma Import (Java, Java.Awt.Im.InputMethodHighlight, "java.awt.im.InputMethodHighlight");
pragma Extensions_Allowed (Off);
