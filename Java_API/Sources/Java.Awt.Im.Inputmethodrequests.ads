pragma Extensions_Allowed (On);
limited with Java.Awt.Font.TextHitInfo;
limited with Java.Awt.Rectangle;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Text.AttributedCharacterIterator.Attribute;
with Java.Lang.Object;

package Java.Awt.Im.InputMethodRequests is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTextLocation (This : access Typ;
                             P1_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class)
                             return access Java.Awt.Rectangle.Typ'Class is abstract;

   function GetLocationOffset (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return access Java.Awt.Font.TextHitInfo.Typ'Class is abstract;

   function GetInsertPositionOffset (This : access Typ)
                                     return Java.Int is abstract;

   function GetCommittedText (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Attribute_Arr : access Java.Text.AttributedCharacterIterator.Attribute.Arr_Obj)
                              return access Java.Text.AttributedCharacterIterator.Typ'Class is abstract;

   function GetCommittedTextLength (This : access Typ)
                                    return Java.Int is abstract;

   function CancelLatestCommittedText (This : access Typ;
                                       P1_Attribute_Arr : access Java.Text.AttributedCharacterIterator.Attribute.Arr_Obj)
                                       return access Java.Text.AttributedCharacterIterator.Typ'Class is abstract;

   function GetSelectedText (This : access Typ;
                             P1_Attribute_Arr : access Java.Text.AttributedCharacterIterator.Attribute.Arr_Obj)
                             return access Java.Text.AttributedCharacterIterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTextLocation, "getTextLocation");
   pragma Export (Java, GetLocationOffset, "getLocationOffset");
   pragma Export (Java, GetInsertPositionOffset, "getInsertPositionOffset");
   pragma Export (Java, GetCommittedText, "getCommittedText");
   pragma Export (Java, GetCommittedTextLength, "getCommittedTextLength");
   pragma Export (Java, CancelLatestCommittedText, "cancelLatestCommittedText");
   pragma Export (Java, GetSelectedText, "getSelectedText");

end Java.Awt.Im.InputMethodRequests;
pragma Import (Java, Java.Awt.Im.InputMethodRequests, "java.awt.im.InputMethodRequests");
pragma Extensions_Allowed (Off);
