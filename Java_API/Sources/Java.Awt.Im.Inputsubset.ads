pragma Extensions_Allowed (On);
with Java.Lang.Character.Subset;
with Java.Lang.Object;

package Java.Awt.Im.InputSubset is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Character.Subset.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LATIN : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   LATIN_DIGITS : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   TRADITIONAL_HANZI : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   SIMPLIFIED_HANZI : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   KANJI : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   HANJA : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   HALFWIDTH_KATAKANA : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   FULLWIDTH_LATIN : access Java.Awt.Im.InputSubset.Typ'Class;

   --  final
   FULLWIDTH_DIGITS : access Java.Awt.Im.InputSubset.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, LATIN, "LATIN");
   pragma Import (Java, LATIN_DIGITS, "LATIN_DIGITS");
   pragma Import (Java, TRADITIONAL_HANZI, "TRADITIONAL_HANZI");
   pragma Import (Java, SIMPLIFIED_HANZI, "SIMPLIFIED_HANZI");
   pragma Import (Java, KANJI, "KANJI");
   pragma Import (Java, HANJA, "HANJA");
   pragma Import (Java, HALFWIDTH_KATAKANA, "HALFWIDTH_KATAKANA");
   pragma Import (Java, FULLWIDTH_LATIN, "FULLWIDTH_LATIN");
   pragma Import (Java, FULLWIDTH_DIGITS, "FULLWIDTH_DIGITS");

end Java.Awt.Im.InputSubset;
pragma Import (Java, Java.Awt.Im.InputSubset, "java.awt.im.InputSubset");
pragma Extensions_Allowed (Off);
