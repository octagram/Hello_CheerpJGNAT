pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Im.Spi.InputMethodContext;
limited with Java.Awt.Rectangle;
limited with Java.Lang.Character.Subset;
limited with Java.Util.Locale;
with Java.Lang.Object;

package Java.Awt.Im.Spi.InputMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetInputMethodContext (This : access Typ;
                                    P1_InputMethodContext : access Standard.Java.Awt.Im.Spi.InputMethodContext.Typ'Class) is abstract;

   function SetLocale (This : access Typ;
                       P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return Java.Boolean is abstract;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class is abstract;

   procedure SetCharacterSubsets (This : access Typ;
                                  P1_Subset_Arr : access Java.Lang.Character.Subset.Arr_Obj) is abstract;

   procedure SetCompositionEnabled (This : access Typ;
                                    P1_Boolean : Java.Boolean) is abstract;

   function IsCompositionEnabled (This : access Typ)
                                  return Java.Boolean is abstract;

   procedure Reconvert (This : access Typ) is abstract;

   procedure DispatchEvent (This : access Typ;
                            P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class) is abstract;

   procedure NotifyClientWindowChange (This : access Typ;
                                       P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class) is abstract;

   procedure Activate (This : access Typ) is abstract;

   procedure Deactivate (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   procedure HideWindows (This : access Typ) is abstract;

   procedure RemoveNotify (This : access Typ) is abstract;

   procedure EndComposition (This : access Typ) is abstract;

   procedure Dispose (This : access Typ) is abstract;

   function GetControlObject (This : access Typ)
                              return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetInputMethodContext, "setInputMethodContext");
   pragma Export (Java, SetLocale, "setLocale");
   pragma Export (Java, GetLocale, "getLocale");
   pragma Export (Java, SetCharacterSubsets, "setCharacterSubsets");
   pragma Export (Java, SetCompositionEnabled, "setCompositionEnabled");
   pragma Export (Java, IsCompositionEnabled, "isCompositionEnabled");
   pragma Export (Java, Reconvert, "reconvert");
   pragma Export (Java, DispatchEvent, "dispatchEvent");
   pragma Export (Java, NotifyClientWindowChange, "notifyClientWindowChange");
   pragma Export (Java, Activate, "activate");
   pragma Export (Java, Deactivate, "deactivate");
   pragma Export (Java, HideWindows, "hideWindows");
   pragma Export (Java, RemoveNotify, "removeNotify");
   pragma Export (Java, EndComposition, "endComposition");
   pragma Export (Java, Dispose, "dispose");
   pragma Export (Java, GetControlObject, "getControlObject");

end Java.Awt.Im.Spi.InputMethod;
pragma Import (Java, Java.Awt.Im.Spi.InputMethod, "java.awt.im.spi.InputMethod");
pragma Extensions_Allowed (Off);
