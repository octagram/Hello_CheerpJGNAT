pragma Extensions_Allowed (On);
limited with Java.Awt.Font.TextHitInfo;
limited with Java.Awt.Im.Spi.InputMethod;
limited with Java.Awt.Window;
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
limited with Javax.Swing.JFrame;
with Java.Awt.Im.InputMethodRequests;
with Java.Lang.Object;

package Java.Awt.Im.Spi.InputMethodContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            InputMethodRequests_I : Java.Awt.Im.InputMethodRequests.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DispatchInputMethodEvent (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class;
                                       P5_TextHitInfo : access Standard.Java.Awt.Font.TextHitInfo.Typ'Class) is abstract;

   function CreateInputMethodWindow (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_Boolean : Java.Boolean)
                                     return access Java.Awt.Window.Typ'Class is abstract;

   function CreateInputMethodJFrame (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_Boolean : Java.Boolean)
                                     return access Javax.Swing.JFrame.Typ'Class is abstract;

   procedure EnableClientWindowNotification (This : access Typ;
                                             P1_InputMethod : access Standard.Java.Awt.Im.Spi.InputMethod.Typ'Class;
                                             P2_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, DispatchInputMethodEvent, "dispatchInputMethodEvent");
   pragma Export (Java, CreateInputMethodWindow, "createInputMethodWindow");
   pragma Export (Java, CreateInputMethodJFrame, "createInputMethodJFrame");
   pragma Export (Java, EnableClientWindowNotification, "enableClientWindowNotification");

end Java.Awt.Im.Spi.InputMethodContext;
pragma Import (Java, Java.Awt.Im.Spi.InputMethodContext, "java.awt.im.spi.InputMethodContext");
pragma Extensions_Allowed (Off);
