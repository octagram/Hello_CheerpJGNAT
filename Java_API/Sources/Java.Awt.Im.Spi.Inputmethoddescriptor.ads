pragma Extensions_Allowed (On);
limited with Java.Awt.Im.Spi.InputMethod;
limited with Java.Awt.Image;
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Lang.Object;

package Java.Awt.Im.Spi.InputMethodDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAvailableLocales (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Awt.AWTException.Except

   function HasDynamicLocaleList (This : access Typ)
                                  return Java.Boolean is abstract;

   function GetInputMethodDisplayName (This : access Typ;
                                       P1_Locale : access Standard.Java.Util.Locale.Typ'Class;
                                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                       return access Java.Lang.String.Typ'Class is abstract;

   function GetInputMethodIcon (This : access Typ;
                                P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                return access Java.Awt.Image.Typ'Class is abstract;

   function CreateInputMethod (This : access Typ)
                               return access Java.Awt.Im.Spi.InputMethod.Typ'Class is abstract;
   --  can raise Java.Lang.Exception_K.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Export (Java, HasDynamicLocaleList, "hasDynamicLocaleList");
   pragma Export (Java, GetInputMethodDisplayName, "getInputMethodDisplayName");
   pragma Export (Java, GetInputMethodIcon, "getInputMethodIcon");
   pragma Export (Java, CreateInputMethod, "createInputMethod");

end Java.Awt.Im.Spi.InputMethodDescriptor;
pragma Import (Java, Java.Awt.Im.Spi.InputMethodDescriptor, "java.awt.im.spi.InputMethodDescriptor");
pragma Extensions_Allowed (Off);
