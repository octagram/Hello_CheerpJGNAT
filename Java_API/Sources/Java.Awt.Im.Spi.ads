pragma Extensions_Allowed (On);
package Java.Awt.Im.Spi is
   pragma Preelaborate;
end Java.Awt.Im.Spi;
pragma Import (Java, Java.Awt.Im.Spi, "java.awt.im.spi");
pragma Extensions_Allowed (Off);
