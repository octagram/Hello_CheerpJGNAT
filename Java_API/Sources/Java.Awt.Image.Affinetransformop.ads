pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Awt.RenderingHints;
with Java.Awt.Image.BufferedImageOp;
with Java.Awt.Image.RasterOp;
with Java.Lang.Object;

package Java.Awt.Image.AffineTransformOp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(BufferedImageOp_I : Java.Awt.Image.BufferedImageOp.Ref;
            RasterOp_I : Java.Awt.Image.RasterOp.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AffineTransformOp (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                                   P2_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_AffineTransformOp (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetInterpolationType (This : access Typ)
                                  return Java.Int;

   --  final
   function Filter (This : access Typ;
                    P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                    P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class)
                    return access Java.Awt.Image.BufferedImage.Typ'Class;

   --  final
   function Filter (This : access Typ;
                    P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                    P2_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                    return access Java.Awt.Image.WritableRaster.Typ'Class;

   --  final
   function GetBounds2D (This : access Typ;
                         P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   --  final
   function GetBounds2D (This : access Typ;
                         P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function CreateCompatibleDestImage (This : access Typ;
                                       P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                                       P2_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class)
                                       return access Java.Awt.Image.BufferedImage.Typ'Class;

   function CreateCompatibleDestRaster (This : access Typ;
                                        P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                                        return access Java.Awt.Image.WritableRaster.Typ'Class;

   --  final
   function GetPoint2D (This : access Typ;
                        P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                        P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                        return access Java.Awt.Geom.Point2D.Typ'Class;

   --  final
   function GetTransform (This : access Typ)
                          return access Java.Awt.Geom.AffineTransform.Typ'Class;

   --  final
   function GetRenderingHints (This : access Typ)
                               return access Java.Awt.RenderingHints.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_NEAREST_NEIGHBOR : constant Java.Int;

   --  final
   TYPE_BILINEAR : constant Java.Int;

   --  final
   TYPE_BICUBIC : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AffineTransformOp);
   pragma Import (Java, GetInterpolationType, "getInterpolationType");
   pragma Import (Java, Filter, "filter");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, CreateCompatibleDestImage, "createCompatibleDestImage");
   pragma Import (Java, CreateCompatibleDestRaster, "createCompatibleDestRaster");
   pragma Import (Java, GetPoint2D, "getPoint2D");
   pragma Import (Java, GetTransform, "getTransform");
   pragma Import (Java, GetRenderingHints, "getRenderingHints");
   pragma Import (Java, TYPE_NEAREST_NEIGHBOR, "TYPE_NEAREST_NEIGHBOR");
   pragma Import (Java, TYPE_BILINEAR, "TYPE_BILINEAR");
   pragma Import (Java, TYPE_BICUBIC, "TYPE_BICUBIC");

end Java.Awt.Image.AffineTransformOp;
pragma Import (Java, Java.Awt.Image.AffineTransformOp, "java.awt.image.AffineTransformOp");
pragma Extensions_Allowed (Off);
