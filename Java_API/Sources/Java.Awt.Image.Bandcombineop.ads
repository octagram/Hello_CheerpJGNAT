pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Awt.RenderingHints;
with Java.Awt.Image.RasterOp;
with Java.Lang.Object;

package Java.Awt.Image.BandCombineOp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RasterOp_I : Java.Awt.Image.RasterOp.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BandCombineOp (P1_Float_Arr_2 : Java.Float_Arr_2;
                               P2_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetMatrix (This : access Typ)
                       return Java.Float_Arr_2;

   function Filter (This : access Typ;
                    P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                    P2_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                    return access Java.Awt.Image.WritableRaster.Typ'Class;

   --  final
   function GetBounds2D (This : access Typ;
                         P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function CreateCompatibleDestRaster (This : access Typ;
                                        P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                                        return access Java.Awt.Image.WritableRaster.Typ'Class;

   --  final
   function GetPoint2D (This : access Typ;
                        P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                        P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                        return access Java.Awt.Geom.Point2D.Typ'Class;

   --  final
   function GetRenderingHints (This : access Typ)
                               return access Java.Awt.RenderingHints.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BandCombineOp);
   pragma Import (Java, GetMatrix, "getMatrix");
   pragma Import (Java, Filter, "filter");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, CreateCompatibleDestRaster, "createCompatibleDestRaster");
   pragma Import (Java, GetPoint2D, "getPoint2D");
   pragma Import (Java, GetRenderingHints, "getRenderingHints");

end Java.Awt.Image.BandCombineOp;
pragma Import (Java, Java.Awt.Image.BandCombineOp, "java.awt.image.BandCombineOp");
pragma Extensions_Allowed (Off);
