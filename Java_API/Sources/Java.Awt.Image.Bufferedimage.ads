pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Awt.Image.IndexColorModel;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Image.TileObserver;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Java.Util.Vector;
with Java.Awt.Image;
with Java.Awt.Image.WritableRenderedImage;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Image.BufferedImage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transparency_I : Java.Awt.Transparency.Ref;
            WritableRenderedImage_I : Java.Awt.Image.WritableRenderedImage.Ref)
    is new Java.Awt.Image.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BufferedImage (P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   function New_BufferedImage (P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_IndexColorModel : access Standard.Java.Awt.Image.IndexColorModel.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_BufferedImage (P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                               P2_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class;
                               P3_Boolean : Java.Boolean;
                               P4_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return Java.Int;

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class;

   function GetRaster (This : access Typ)
                       return access Java.Awt.Image.WritableRaster.Typ'Class;

   function GetAlphaRaster (This : access Typ)
                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function GetRGB (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return Java.Int;

   function GetRGB (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int;
                    P5_Int_Arr : Java.Int_Arr;
                    P6_Int : Java.Int;
                    P7_Int : Java.Int)
                    return Java.Int_Arr;

   --  synchronized
   procedure SetRGB (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);

   procedure SetRGB (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int;
                     P4_Int : Java.Int;
                     P5_Int_Arr : Java.Int_Arr;
                     P6_Int : Java.Int;
                     P7_Int : Java.Int);

   function GetWidth (This : access Typ)
                      return Java.Int;

   function GetHeight (This : access Typ)
                       return Java.Int;

   function GetWidth (This : access Typ;
                      P1_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                      return Java.Int;

   function GetHeight (This : access Typ;
                       P1_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Int;

   function GetSource (This : access Typ)
                       return access Java.Awt.Image.ImageProducer.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class;

   function CreateGraphics (This : access Typ)
                            return access Java.Awt.Graphics2D.Typ'Class;

   function GetSubimage (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int)
                         return access Java.Awt.Image.BufferedImage.Typ'Class;

   function IsAlphaPremultiplied (This : access Typ)
                                  return Java.Boolean;

   procedure CoerceData (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetSources (This : access Typ)
                        return access Java.Util.Vector.Typ'Class;

   function GetPropertyNames (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetMinX (This : access Typ)
                     return Java.Int;

   function GetMinY (This : access Typ)
                     return Java.Int;

   function GetSampleModel (This : access Typ)
                            return access Java.Awt.Image.SampleModel.Typ'Class;

   function GetNumXTiles (This : access Typ)
                          return Java.Int;

   function GetNumYTiles (This : access Typ)
                          return Java.Int;

   function GetMinTileX (This : access Typ)
                         return Java.Int;

   function GetMinTileY (This : access Typ)
                         return Java.Int;

   function GetTileWidth (This : access Typ)
                          return Java.Int;

   function GetTileHeight (This : access Typ)
                           return Java.Int;

   function GetTileGridXOffset (This : access Typ)
                                return Java.Int;

   function GetTileGridYOffset (This : access Typ)
                                return Java.Int;

   function GetTile (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Java.Awt.Image.Raster.Typ'Class;

   function GetData (This : access Typ)
                     return access Java.Awt.Image.Raster.Typ'Class;

   function GetData (This : access Typ;
                     P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                     return access Java.Awt.Image.Raster.Typ'Class;

   function CopyData (This : access Typ;
                      P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                      return access Java.Awt.Image.WritableRaster.Typ'Class;

   procedure SetData (This : access Typ;
                      P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class);

   procedure AddTileObserver (This : access Typ;
                              P1_TileObserver : access Standard.Java.Awt.Image.TileObserver.Typ'Class);

   procedure RemoveTileObserver (This : access Typ;
                                 P1_TileObserver : access Standard.Java.Awt.Image.TileObserver.Typ'Class);

   function IsTileWritable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Boolean;

   function GetWritableTileIndices (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function HasTileWriters (This : access Typ)
                            return Java.Boolean;

   function GetWritableTile (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Java.Awt.Image.WritableRaster.Typ'Class;

   procedure ReleaseWritableTile (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int);

   function GetTransparency (This : access Typ)
                             return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_CUSTOM : constant Java.Int;

   --  final
   TYPE_INT_RGB : constant Java.Int;

   --  final
   TYPE_INT_ARGB : constant Java.Int;

   --  final
   TYPE_INT_ARGB_PRE : constant Java.Int;

   --  final
   TYPE_INT_BGR : constant Java.Int;

   --  final
   TYPE_3BYTE_BGR : constant Java.Int;

   --  final
   TYPE_4BYTE_ABGR : constant Java.Int;

   --  final
   TYPE_4BYTE_ABGR_PRE : constant Java.Int;

   --  final
   TYPE_USHORT_565_RGB : constant Java.Int;

   --  final
   TYPE_USHORT_555_RGB : constant Java.Int;

   --  final
   TYPE_BYTE_GRAY : constant Java.Int;

   --  final
   TYPE_USHORT_GRAY : constant Java.Int;

   --  final
   TYPE_BYTE_BINARY : constant Java.Int;

   --  final
   TYPE_BYTE_INDEXED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BufferedImage);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetColorModel, "getColorModel");
   pragma Import (Java, GetRaster, "getRaster");
   pragma Import (Java, GetAlphaRaster, "getAlphaRaster");
   pragma Import (Java, GetRGB, "getRGB");
   pragma Import (Java, SetRGB, "setRGB");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetSource, "getSource");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, GetGraphics, "getGraphics");
   pragma Import (Java, CreateGraphics, "createGraphics");
   pragma Import (Java, GetSubimage, "getSubimage");
   pragma Import (Java, IsAlphaPremultiplied, "isAlphaPremultiplied");
   pragma Import (Java, CoerceData, "coerceData");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetSources, "getSources");
   pragma Import (Java, GetPropertyNames, "getPropertyNames");
   pragma Import (Java, GetMinX, "getMinX");
   pragma Import (Java, GetMinY, "getMinY");
   pragma Import (Java, GetSampleModel, "getSampleModel");
   pragma Import (Java, GetNumXTiles, "getNumXTiles");
   pragma Import (Java, GetNumYTiles, "getNumYTiles");
   pragma Import (Java, GetMinTileX, "getMinTileX");
   pragma Import (Java, GetMinTileY, "getMinTileY");
   pragma Import (Java, GetTileWidth, "getTileWidth");
   pragma Import (Java, GetTileHeight, "getTileHeight");
   pragma Import (Java, GetTileGridXOffset, "getTileGridXOffset");
   pragma Import (Java, GetTileGridYOffset, "getTileGridYOffset");
   pragma Import (Java, GetTile, "getTile");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, CopyData, "copyData");
   pragma Import (Java, SetData, "setData");
   pragma Import (Java, AddTileObserver, "addTileObserver");
   pragma Import (Java, RemoveTileObserver, "removeTileObserver");
   pragma Import (Java, IsTileWritable, "isTileWritable");
   pragma Import (Java, GetWritableTileIndices, "getWritableTileIndices");
   pragma Import (Java, HasTileWriters, "hasTileWriters");
   pragma Import (Java, GetWritableTile, "getWritableTile");
   pragma Import (Java, ReleaseWritableTile, "releaseWritableTile");
   pragma Import (Java, GetTransparency, "getTransparency");
   pragma Import (Java, TYPE_CUSTOM, "TYPE_CUSTOM");
   pragma Import (Java, TYPE_INT_RGB, "TYPE_INT_RGB");
   pragma Import (Java, TYPE_INT_ARGB, "TYPE_INT_ARGB");
   pragma Import (Java, TYPE_INT_ARGB_PRE, "TYPE_INT_ARGB_PRE");
   pragma Import (Java, TYPE_INT_BGR, "TYPE_INT_BGR");
   pragma Import (Java, TYPE_3BYTE_BGR, "TYPE_3BYTE_BGR");
   pragma Import (Java, TYPE_4BYTE_ABGR, "TYPE_4BYTE_ABGR");
   pragma Import (Java, TYPE_4BYTE_ABGR_PRE, "TYPE_4BYTE_ABGR_PRE");
   pragma Import (Java, TYPE_USHORT_565_RGB, "TYPE_USHORT_565_RGB");
   pragma Import (Java, TYPE_USHORT_555_RGB, "TYPE_USHORT_555_RGB");
   pragma Import (Java, TYPE_BYTE_GRAY, "TYPE_BYTE_GRAY");
   pragma Import (Java, TYPE_USHORT_GRAY, "TYPE_USHORT_GRAY");
   pragma Import (Java, TYPE_BYTE_BINARY, "TYPE_BYTE_BINARY");
   pragma Import (Java, TYPE_BYTE_INDEXED, "TYPE_BYTE_INDEXED");

end Java.Awt.Image.BufferedImage;
pragma Import (Java, Java.Awt.Image.BufferedImage, "java.awt.image.BufferedImage");
pragma Extensions_Allowed (Off);
