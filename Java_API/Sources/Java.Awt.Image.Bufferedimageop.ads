pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.RenderingHints;
with Java.Lang.Object;

package Java.Awt.Image.BufferedImageOp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Filter (This : access Typ;
                    P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                    P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class)
                    return access Java.Awt.Image.BufferedImage.Typ'Class is abstract;

   function GetBounds2D (This : access Typ;
                         P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   function CreateCompatibleDestImage (This : access Typ;
                                       P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                                       P2_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class)
                                       return access Java.Awt.Image.BufferedImage.Typ'Class is abstract;

   function GetPoint2D (This : access Typ;
                        P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                        P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                        return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   function GetRenderingHints (This : access Typ)
                               return access Java.Awt.RenderingHints.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Filter, "filter");
   pragma Export (Java, GetBounds2D, "getBounds2D");
   pragma Export (Java, CreateCompatibleDestImage, "createCompatibleDestImage");
   pragma Export (Java, GetPoint2D, "getPoint2D");
   pragma Export (Java, GetRenderingHints, "getRenderingHints");

end Java.Awt.Image.BufferedImageOp;
pragma Import (Java, Java.Awt.Image.BufferedImageOp, "java.awt.image.BufferedImageOp");
pragma Extensions_Allowed (Off);
