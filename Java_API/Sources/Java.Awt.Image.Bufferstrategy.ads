pragma Extensions_Allowed (On);
limited with Java.Awt.BufferCapabilities;
limited with Java.Awt.Graphics;
with Java.Lang.Object;

package Java.Awt.Image.BufferStrategy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_BufferStrategy (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCapabilities (This : access Typ)
                             return access Java.Awt.BufferCapabilities.Typ'Class is abstract;

   function GetDrawGraphics (This : access Typ)
                             return access Java.Awt.Graphics.Typ'Class is abstract;

   function ContentsLost (This : access Typ)
                          return Java.Boolean is abstract;

   function ContentsRestored (This : access Typ)
                              return Java.Boolean is abstract;

   procedure Show (This : access Typ) is abstract;

   procedure Dispose (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BufferStrategy);
   pragma Export (Java, GetCapabilities, "getCapabilities");
   pragma Export (Java, GetDrawGraphics, "getDrawGraphics");
   pragma Export (Java, ContentsLost, "contentsLost");
   pragma Export (Java, ContentsRestored, "contentsRestored");
   pragma Export (Java, Show, "show");
   pragma Import (Java, Dispose, "dispose");

end Java.Awt.Image.BufferStrategy;
pragma Import (Java, Java.Awt.Image.BufferStrategy, "java.awt.image.BufferStrategy");
pragma Extensions_Allowed (Off);
