pragma Extensions_Allowed (On);
limited with Java.Awt.Color.ColorSpace;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Lang.String;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Image.ColorModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transparency_I : Java.Awt.Transparency.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Pixel_bits : Java.Int;
      pragma Import (Java, Pixel_bits, "pixel_bits");

      --  protected
      TransferType : Java.Int;
      pragma Import (Java, TransferType, "transferType");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRGBdefault return access Java.Awt.Image.ColorModel.Typ'Class;

   function New_ColorModel (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   --  protected
   function New_ColorModel (P1_Int : Java.Int;
                            P2_Int_Arr : Java.Int_Arr;
                            P3_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                            P4_Boolean : Java.Boolean;
                            P5_Boolean : Java.Boolean;
                            P6_Int : Java.Int;
                            P7_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   --  final
   function HasAlpha (This : access Typ)
                      return Java.Boolean;

   --  final
   function IsAlphaPremultiplied (This : access Typ)
                                  return Java.Boolean;

   --  final
   function GetTransferType (This : access Typ)
                             return Java.Int;

   function GetPixelSize (This : access Typ)
                          return Java.Int;

   function GetComponentSize (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   function GetComponentSize (This : access Typ)
                              return Java.Int_Arr;

   function GetTransparency (This : access Typ)
                             return Java.Int;

   function GetNumComponents (This : access Typ)
                              return Java.Int;

   function GetNumColorComponents (This : access Typ)
                                   return Java.Int;

   function GetRed (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int is abstract;

   function GetGreen (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int is abstract;

   function GetBlue (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int is abstract;

   function GetAlpha (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int is abstract;

   function GetRGB (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   function GetRed (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetGreen (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Int;

   function GetBlue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function GetAlpha (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Int;

   function GetRGB (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetComponents (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   function GetComponents (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   function GetUnnormalizedComponents (This : access Typ;
                                       P1_Float_Arr : Java.Float_Arr;
                                       P2_Int : Java.Int;
                                       P3_Int_Arr : Java.Int_Arr;
                                       P4_Int : Java.Int)
                                       return Java.Int_Arr;

   function GetNormalizedComponents (This : access Typ;
                                     P1_Int_Arr : Java.Int_Arr;
                                     P2_Int : Java.Int;
                                     P3_Float_Arr : Java.Float_Arr;
                                     P4_Int : Java.Int)
                                     return Java.Float_Arr;

   function GetDataElement (This : access Typ;
                            P1_Int_Arr : Java.Int_Arr;
                            P2_Int : Java.Int)
                            return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Int_Arr : Java.Int_Arr;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetDataElement (This : access Typ;
                            P1_Float_Arr : Java.Float_Arr;
                            P2_Int : Java.Int)
                            return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Float_Arr : Java.Float_Arr;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetNormalizedComponents (This : access Typ;
                                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P2_Float_Arr : Java.Float_Arr;
                                     P3_Int : Java.Int)
                                     return Java.Float_Arr;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function GetColorSpace (This : access Typ)
                           return access Java.Awt.Color.ColorSpace.Typ'Class;

   function CoerceData (This : access Typ;
                        P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class;
                        P2_Boolean : Java.Boolean)
                        return access Java.Awt.Image.ColorModel.Typ'Class;

   function IsCompatibleRaster (This : access Typ;
                                P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                                return Java.Boolean;

   function CreateCompatibleWritableRaster (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Int : Java.Int)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateCompatibleSampleModel (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return access Java.Awt.Image.SampleModel.Typ'Class;

   function IsCompatibleSampleModel (This : access Typ;
                                     P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class)
                                     return Java.Boolean;

   procedure Finalize (This : access Typ);

   function GetAlphaRaster (This : access Typ;
                            P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRGBdefault, "getRGBdefault");
   pragma Java_Constructor (New_ColorModel);
   pragma Export (Java, HasAlpha, "hasAlpha");
   pragma Export (Java, IsAlphaPremultiplied, "isAlphaPremultiplied");
   pragma Export (Java, GetTransferType, "getTransferType");
   pragma Export (Java, GetPixelSize, "getPixelSize");
   pragma Export (Java, GetComponentSize, "getComponentSize");
   pragma Export (Java, GetTransparency, "getTransparency");
   pragma Export (Java, GetNumComponents, "getNumComponents");
   pragma Export (Java, GetNumColorComponents, "getNumColorComponents");
   pragma Export (Java, GetRed, "getRed");
   pragma Export (Java, GetGreen, "getGreen");
   pragma Export (Java, GetBlue, "getBlue");
   pragma Export (Java, GetAlpha, "getAlpha");
   pragma Export (Java, GetRGB, "getRGB");
   pragma Export (Java, GetDataElements, "getDataElements");
   pragma Export (Java, GetComponents, "getComponents");
   pragma Export (Java, GetUnnormalizedComponents, "getUnnormalizedComponents");
   pragma Export (Java, GetNormalizedComponents, "getNormalizedComponents");
   pragma Export (Java, GetDataElement, "getDataElement");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, GetColorSpace, "getColorSpace");
   pragma Export (Java, CoerceData, "coerceData");
   pragma Export (Java, IsCompatibleRaster, "isCompatibleRaster");
   pragma Export (Java, CreateCompatibleWritableRaster, "createCompatibleWritableRaster");
   pragma Export (Java, CreateCompatibleSampleModel, "createCompatibleSampleModel");
   pragma Export (Java, IsCompatibleSampleModel, "isCompatibleSampleModel");
   pragma Export (Java, Finalize, "finalize");
   pragma Export (Java, GetAlphaRaster, "getAlphaRaster");
   pragma Export (Java, ToString, "toString");

end Java.Awt.Image.ColorModel;
pragma Import (Java, Java.Awt.Image.ColorModel, "java.awt.image.ColorModel");
pragma Extensions_Allowed (Off);
