pragma Extensions_Allowed (On);
limited with Java.Awt.Color.ColorSpace;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Image.WritableRaster;
with Java.Awt.Image.ColorModel;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Image.ComponentColorModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transparency_I : Java.Awt.Transparency.Ref)
    is new Java.Awt.Image.ColorModel.Typ(Transparency_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ComponentColorModel (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                                     P2_Int_Arr : Java.Int_Arr;
                                     P3_Boolean : Java.Boolean;
                                     P4_Boolean : Java.Boolean;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   function New_ComponentColorModel (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                                     P2_Boolean : Java.Boolean;
                                     P3_Boolean : Java.Boolean;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRed (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   function GetGreen (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;

   function GetBlue (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   function GetAlpha (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;

   function GetRGB (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   function GetRed (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetGreen (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Int;

   function GetBlue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function GetAlpha (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Int;

   function GetRGB (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetComponents (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   function GetComponents (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   function GetUnnormalizedComponents (This : access Typ;
                                       P1_Float_Arr : Java.Float_Arr;
                                       P2_Int : Java.Int;
                                       P3_Int_Arr : Java.Int_Arr;
                                       P4_Int : Java.Int)
                                       return Java.Int_Arr;

   function GetNormalizedComponents (This : access Typ;
                                     P1_Int_Arr : Java.Int_Arr;
                                     P2_Int : Java.Int;
                                     P3_Float_Arr : Java.Float_Arr;
                                     P4_Int : Java.Int)
                                     return Java.Float_Arr;

   function GetDataElement (This : access Typ;
                            P1_Int_Arr : Java.Int_Arr;
                            P2_Int : Java.Int)
                            return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Int_Arr : Java.Int_Arr;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetDataElement (This : access Typ;
                            P1_Float_Arr : Java.Float_Arr;
                            P2_Int : Java.Int)
                            return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Float_Arr : Java.Float_Arr;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetNormalizedComponents (This : access Typ;
                                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P2_Float_Arr : Java.Float_Arr;
                                     P3_Int : Java.Int)
                                     return Java.Float_Arr;

   function CoerceData (This : access Typ;
                        P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class;
                        P2_Boolean : Java.Boolean)
                        return access Java.Awt.Image.ColorModel.Typ'Class;

   function IsCompatibleRaster (This : access Typ;
                                P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                                return Java.Boolean;

   function CreateCompatibleWritableRaster (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Int : Java.Int)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateCompatibleSampleModel (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return access Java.Awt.Image.SampleModel.Typ'Class;

   function IsCompatibleSampleModel (This : access Typ;
                                     P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class)
                                     return Java.Boolean;

   function GetAlphaRaster (This : access Typ;
                            P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComponentColorModel);
   pragma Import (Java, GetRed, "getRed");
   pragma Import (Java, GetGreen, "getGreen");
   pragma Import (Java, GetBlue, "getBlue");
   pragma Import (Java, GetAlpha, "getAlpha");
   pragma Import (Java, GetRGB, "getRGB");
   pragma Import (Java, GetDataElements, "getDataElements");
   pragma Import (Java, GetComponents, "getComponents");
   pragma Import (Java, GetUnnormalizedComponents, "getUnnormalizedComponents");
   pragma Import (Java, GetNormalizedComponents, "getNormalizedComponents");
   pragma Import (Java, GetDataElement, "getDataElement");
   pragma Import (Java, CoerceData, "coerceData");
   pragma Import (Java, IsCompatibleRaster, "isCompatibleRaster");
   pragma Import (Java, CreateCompatibleWritableRaster, "createCompatibleWritableRaster");
   pragma Import (Java, CreateCompatibleSampleModel, "createCompatibleSampleModel");
   pragma Import (Java, IsCompatibleSampleModel, "isCompatibleSampleModel");
   pragma Import (Java, GetAlphaRaster, "getAlphaRaster");
   pragma Import (Java, Equals, "equals");

end Java.Awt.Image.ComponentColorModel;
pragma Import (Java, Java.Awt.Image.ComponentColorModel, "java.awt.image.ComponentColorModel");
pragma Extensions_Allowed (Off);
