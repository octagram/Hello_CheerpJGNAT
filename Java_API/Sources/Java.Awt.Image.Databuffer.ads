pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Image.DataBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DataType : Java.Int;
      pragma Import (Java, DataType, "dataType");

      --  protected
      Banks : Java.Int;
      pragma Import (Java, Banks, "banks");

      --  protected
      Offset : Java.Int;
      pragma Import (Java, Offset, "offset");

      --  protected
      Size : Java.Int;
      pragma Import (Java, Size, "size");

      --  protected
      Offsets : Java.Int_Arr;
      pragma Import (Java, Offsets, "offsets");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDataTypeSize (P1_Int : Java.Int)
                             return Java.Int;

   --  protected
   function New_DataBuffer (P1_Int : Java.Int;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   --  protected
   function New_DataBuffer (P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   --  protected
   function New_DataBuffer (P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   --  protected
   function New_DataBuffer (P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int_Arr : Java.Int_Arr; 
                            This : Ref := null)
                            return Ref;

   function GetDataType (This : access Typ)
                         return Java.Int;

   function GetSize (This : access Typ)
                     return Java.Int;

   function GetOffset (This : access Typ)
                       return Java.Int;

   function GetOffsets (This : access Typ)
                        return Java.Int_Arr;

   function GetNumBanks (This : access Typ)
                         return Java.Int;

   function GetElem (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int
                     with Import => "getElem", Convention => Java;

   function GetElem (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return Java.Int is abstract
                     with Export => "getElem", Convention => Java;

   procedure SetElem (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      with Import => "setElem", Convention => Java;

   procedure SetElem (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int) is abstract
                      with Export => "setElem", Convention => Java;

   function GetElemFloat (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   function GetElemFloat (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return Java.Float;

   procedure SetElemFloat (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Float : Java.Float);

   procedure SetElemFloat (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Float : Java.Float);

   function GetElemDouble (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Double;

   function GetElemDouble (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return Java.Double;

   procedure SetElemDouble (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Double : Java.Double);

   procedure SetElemDouble (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Double : Java.Double);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_BYTE : constant Java.Int;

   --  final
   TYPE_USHORT : constant Java.Int;

   --  final
   TYPE_SHORT : constant Java.Int;

   --  final
   TYPE_INT : constant Java.Int;

   --  final
   TYPE_FLOAT : constant Java.Int;

   --  final
   TYPE_DOUBLE : constant Java.Int;

   --  final
   TYPE_UNDEFINED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDataTypeSize, "getDataTypeSize");
   pragma Java_Constructor (New_DataBuffer);
   pragma Import (Java, GetDataType, "getDataType");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetOffsets, "getOffsets");
   pragma Import (Java, GetNumBanks, "getNumBanks");
   -- pragma Import (Java, GetElem, "getElem");
   -- pragma Import (Java, SetElem, "setElem");
   pragma Import (Java, GetElemFloat, "getElemFloat");
   pragma Import (Java, SetElemFloat, "setElemFloat");
   pragma Import (Java, GetElemDouble, "getElemDouble");
   pragma Import (Java, SetElemDouble, "setElemDouble");
   pragma Import (Java, TYPE_BYTE, "TYPE_BYTE");
   pragma Import (Java, TYPE_USHORT, "TYPE_USHORT");
   pragma Import (Java, TYPE_SHORT, "TYPE_SHORT");
   pragma Import (Java, TYPE_INT, "TYPE_INT");
   pragma Import (Java, TYPE_FLOAT, "TYPE_FLOAT");
   pragma Import (Java, TYPE_DOUBLE, "TYPE_DOUBLE");
   pragma Import (Java, TYPE_UNDEFINED, "TYPE_UNDEFINED");

end Java.Awt.Image.DataBuffer;
pragma Import (Java, Java.Awt.Image.DataBuffer, "java.awt.image.DataBuffer");
pragma Extensions_Allowed (Off);
