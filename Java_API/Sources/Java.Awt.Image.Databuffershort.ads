pragma Extensions_Allowed (On);
with Java.Awt.Image.DataBuffer;
with Java.Lang.Object;

package Java.Awt.Image.DataBufferShort is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Image.DataBuffer.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DataBufferShort (P1_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_DataBufferShort (P1_Int : Java.Int;
                                 P2_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_DataBufferShort (P1_Short_Arr : Java.Short_Arr;
                                 P2_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_DataBufferShort (P1_Short_Arr : Java.Short_Arr;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_DataBufferShort (P1_Short_Arr_2 : Java.Short_Arr_2;
                                 P2_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_DataBufferShort (P1_Short_Arr_2 : Java.Short_Arr_2;
                                 P2_Int : Java.Int;
                                 P3_Int_Arr : Java.Int_Arr; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetData (This : access Typ)
                     return Java.Short_Arr;

   function GetData (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Short_Arr;

   function GetBankData (This : access Typ)
                         return Java.Short_Arr_2;

   function GetElem (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   function GetElem (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return Java.Int;

   procedure SetElem (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int);

   procedure SetElem (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DataBufferShort);
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, GetBankData, "getBankData");
   pragma Import (Java, GetElem, "getElem");
   pragma Import (Java, SetElem, "setElem");

end Java.Awt.Image.DataBufferShort;
pragma Import (Java, Java.Awt.Image.DataBufferShort, "java.awt.image.DataBufferShort");
pragma Extensions_Allowed (Off);
