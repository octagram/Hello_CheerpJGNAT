pragma Extensions_Allowed (On);
limited with Java.Awt.Color.ColorSpace;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Lang.String;
with Java.Awt.Image.PackedColorModel;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Image.DirectColorModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transparency_I : Java.Awt.Transparency.Ref)
    is new Java.Awt.Image.PackedColorModel.Typ(Transparency_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DirectColorModel (P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   function New_DirectColorModel (P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   function New_DirectColorModel (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int;
                                  P7_Boolean : Java.Boolean;
                                  P8_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetRedMask (This : access Typ)
                        return Java.Int;

   --  final
   function GetGreenMask (This : access Typ)
                          return Java.Int;

   --  final
   function GetBlueMask (This : access Typ)
                         return Java.Int;

   --  final
   function GetAlphaMask (This : access Typ)
                          return Java.Int;

   --  final
   function GetRed (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   --  final
   function GetGreen (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;

   --  final
   function GetBlue (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   --  final
   function GetAlpha (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;

   --  final
   function GetRGB (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   function GetRed (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetGreen (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Int;

   function GetBlue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function GetAlpha (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Int;

   function GetRGB (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   --  final
   function GetComponents (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   --  final
   function GetComponents (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   --  final
   function CreateCompatibleWritableRaster (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Int : Java.Int)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function GetDataElement (This : access Typ;
                            P1_Int_Arr : Java.Int_Arr;
                            P2_Int : Java.Int)
                            return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Int_Arr : Java.Int_Arr;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   --  final
   function CoerceData (This : access Typ;
                        P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class;
                        P2_Boolean : Java.Boolean)
                        return access Java.Awt.Image.ColorModel.Typ'Class;

   function IsCompatibleRaster (This : access Typ;
                                P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                                return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DirectColorModel);
   pragma Import (Java, GetRedMask, "getRedMask");
   pragma Import (Java, GetGreenMask, "getGreenMask");
   pragma Import (Java, GetBlueMask, "getBlueMask");
   pragma Import (Java, GetAlphaMask, "getAlphaMask");
   pragma Import (Java, GetRed, "getRed");
   pragma Import (Java, GetGreen, "getGreen");
   pragma Import (Java, GetBlue, "getBlue");
   pragma Import (Java, GetAlpha, "getAlpha");
   pragma Import (Java, GetRGB, "getRGB");
   pragma Import (Java, GetDataElements, "getDataElements");
   pragma Import (Java, GetComponents, "getComponents");
   pragma Import (Java, CreateCompatibleWritableRaster, "createCompatibleWritableRaster");
   pragma Import (Java, GetDataElement, "getDataElement");
   pragma Import (Java, CoerceData, "coerceData");
   pragma Import (Java, IsCompatibleRaster, "isCompatibleRaster");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Image.DirectColorModel;
pragma Import (Java, Java.Awt.Image.DirectColorModel, "java.awt.image.DirectColorModel");
pragma Extensions_Allowed (Off);
