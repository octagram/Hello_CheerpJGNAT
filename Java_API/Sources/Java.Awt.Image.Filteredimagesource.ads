pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ImageConsumer;
limited with Java.Awt.Image.ImageFilter;
with Java.Awt.Image.ImageProducer;
with Java.Lang.Object;

package Java.Awt.Image.FilteredImageSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageProducer_I : Java.Awt.Image.ImageProducer.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FilteredImageSource (P1_ImageProducer : access Standard.Java.Awt.Image.ImageProducer.Typ'Class;
                                     P2_ImageFilter : access Standard.Java.Awt.Image.ImageFilter.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddConsumer (This : access Typ;
                          P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   --  synchronized
   function IsConsumer (This : access Typ;
                        P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class)
                        return Java.Boolean;

   --  synchronized
   procedure RemoveConsumer (This : access Typ;
                             P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   procedure StartProduction (This : access Typ;
                              P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   procedure RequestTopDownLeftRightResend (This : access Typ;
                                            P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FilteredImageSource);
   pragma Import (Java, AddConsumer, "addConsumer");
   pragma Import (Java, IsConsumer, "isConsumer");
   pragma Import (Java, RemoveConsumer, "removeConsumer");
   pragma Import (Java, StartProduction, "startProduction");
   pragma Import (Java, RequestTopDownLeftRightResend, "requestTopDownLeftRightResend");

end Java.Awt.Image.FilteredImageSource;
pragma Import (Java, Java.Awt.Image.FilteredImageSource, "java.awt.image.FilteredImageSource");
pragma Extensions_Allowed (Off);
