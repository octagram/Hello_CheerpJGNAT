pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Util.Hashtable;
with Java.Lang.Object;

package Java.Awt.Image.ImageConsumer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDimensions (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int) is abstract;

   procedure SetProperties (This : access Typ;
                            P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class) is abstract;

   procedure SetColorModel (This : access Typ;
                            P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class) is abstract;

   procedure SetHints (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Byte_Arr : Java.Byte_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int) is abstract;

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Int_Arr : Java.Int_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int) is abstract;

   procedure ImageComplete (This : access Typ;
                            P1_Int : Java.Int) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RANDOMPIXELORDER : constant Java.Int;

   --  final
   TOPDOWNLEFTRIGHT : constant Java.Int;

   --  final
   COMPLETESCANLINES : constant Java.Int;

   --  final
   SINGLEPASS : constant Java.Int;

   --  final
   SINGLEFRAME : constant Java.Int;

   --  final
   IMAGEERROR : constant Java.Int;

   --  final
   SINGLEFRAMEDONE : constant Java.Int;

   --  final
   STATICIMAGEDONE : constant Java.Int;

   --  final
   IMAGEABORTED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetDimensions, "setDimensions");
   pragma Export (Java, SetProperties, "setProperties");
   pragma Export (Java, SetColorModel, "setColorModel");
   pragma Export (Java, SetHints, "setHints");
   pragma Export (Java, SetPixels, "setPixels");
   pragma Export (Java, ImageComplete, "imageComplete");
   pragma Import (Java, RANDOMPIXELORDER, "RANDOMPIXELORDER");
   pragma Import (Java, TOPDOWNLEFTRIGHT, "TOPDOWNLEFTRIGHT");
   pragma Import (Java, COMPLETESCANLINES, "COMPLETESCANLINES");
   pragma Import (Java, SINGLEPASS, "SINGLEPASS");
   pragma Import (Java, SINGLEFRAME, "SINGLEFRAME");
   pragma Import (Java, IMAGEERROR, "IMAGEERROR");
   pragma Import (Java, SINGLEFRAMEDONE, "SINGLEFRAMEDONE");
   pragma Import (Java, STATICIMAGEDONE, "STATICIMAGEDONE");
   pragma Import (Java, IMAGEABORTED, "IMAGEABORTED");

end Java.Awt.Image.ImageConsumer;
pragma Import (Java, Java.Awt.Image.ImageConsumer, "java.awt.image.ImageConsumer");
pragma Extensions_Allowed (Off);
