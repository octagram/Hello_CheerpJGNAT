pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Util.Hashtable;
with Java.Awt.Image.ImageConsumer;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Image.ImageFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageConsumer_I : Java.Awt.Image.ImageConsumer.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Consumer : access Java.Awt.Image.ImageConsumer.Typ'Class;
      pragma Import (Java, Consumer, "consumer");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImageFilter (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFilterInstance (This : access Typ;
                               P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class)
                               return access Java.Awt.Image.ImageFilter.Typ'Class;

   procedure SetDimensions (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);

   procedure SetProperties (This : access Typ;
                            P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class);

   procedure SetColorModel (This : access Typ;
                            P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class);

   procedure SetHints (This : access Typ;
                       P1_Int : Java.Int);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Byte_Arr : Java.Byte_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Int_Arr : Java.Int_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);

   procedure ImageComplete (This : access Typ;
                            P1_Int : Java.Int);

   procedure ResendTopDownLeftRight (This : access Typ;
                                     P1_ImageProducer : access Standard.Java.Awt.Image.ImageProducer.Typ'Class);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageFilter);
   pragma Import (Java, GetFilterInstance, "getFilterInstance");
   pragma Import (Java, SetDimensions, "setDimensions");
   pragma Import (Java, SetProperties, "setProperties");
   pragma Import (Java, SetColorModel, "setColorModel");
   pragma Import (Java, SetHints, "setHints");
   pragma Import (Java, SetPixels, "setPixels");
   pragma Import (Java, ImageComplete, "imageComplete");
   pragma Import (Java, ResendTopDownLeftRight, "resendTopDownLeftRight");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Image.ImageFilter;
pragma Import (Java, Java.Awt.Image.ImageFilter, "java.awt.image.ImageFilter");
pragma Extensions_Allowed (Off);
