pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Image.ImageObserver is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ImageUpdate (This : access Typ;
                         P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int : Java.Int)
                         return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WIDTH : constant Java.Int;

   --  final
   HEIGHT : constant Java.Int;

   --  final
   PROPERTIES : constant Java.Int;

   --  final
   SOMEBITS : constant Java.Int;

   --  final
   FRAMEBITS : constant Java.Int;

   --  final
   ALLBITS : constant Java.Int;

   --  final
   ERROR : constant Java.Int;

   --  final
   ABORT_K : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ImageUpdate, "imageUpdate");
   pragma Import (Java, WIDTH, "WIDTH");
   pragma Import (Java, HEIGHT, "HEIGHT");
   pragma Import (Java, PROPERTIES, "PROPERTIES");
   pragma Import (Java, SOMEBITS, "SOMEBITS");
   pragma Import (Java, FRAMEBITS, "FRAMEBITS");
   pragma Import (Java, ALLBITS, "ALLBITS");
   pragma Import (Java, ERROR, "ERROR");
   pragma Import (Java, ABORT_K, "ABORT");

end Java.Awt.Image.ImageObserver;
pragma Import (Java, Java.Awt.Image.ImageObserver, "java.awt.image.ImageObserver");
pragma Extensions_Allowed (Off);
