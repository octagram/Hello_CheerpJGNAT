pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ImageConsumer;
with Java.Lang.Object;

package Java.Awt.Image.ImageProducer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddConsumer (This : access Typ;
                          P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class) is abstract;

   function IsConsumer (This : access Typ;
                        P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class)
                        return Java.Boolean is abstract;

   procedure RemoveConsumer (This : access Typ;
                             P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class) is abstract;

   procedure StartProduction (This : access Typ;
                              P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class) is abstract;

   procedure RequestTopDownLeftRightResend (This : access Typ;
                                            P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddConsumer, "addConsumer");
   pragma Export (Java, IsConsumer, "isConsumer");
   pragma Export (Java, RemoveConsumer, "removeConsumer");
   pragma Export (Java, StartProduction, "startProduction");
   pragma Export (Java, RequestTopDownLeftRightResend, "requestTopDownLeftRightResend");

end Java.Awt.Image.ImageProducer;
pragma Import (Java, Java.Awt.Image.ImageProducer, "java.awt.image.ImageProducer");
pragma Extensions_Allowed (Off);
