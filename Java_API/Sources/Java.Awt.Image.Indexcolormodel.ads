pragma Extensions_Allowed (On);
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
with Java.Awt.Image.ColorModel;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Image.IndexColorModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transparency_I : Java.Awt.Transparency.Ref)
    is new Java.Awt.Image.ColorModel.Typ(Transparency_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IndexColorModel (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Byte_Arr : Java.Byte_Arr;
                                 P4_Byte_Arr : Java.Byte_Arr;
                                 P5_Byte_Arr : Java.Byte_Arr; 
                                 This : Ref := null)
                                 return Ref;

   function New_IndexColorModel (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Byte_Arr : Java.Byte_Arr;
                                 P4_Byte_Arr : Java.Byte_Arr;
                                 P5_Byte_Arr : Java.Byte_Arr;
                                 P6_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_IndexColorModel (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Byte_Arr : Java.Byte_Arr;
                                 P4_Byte_Arr : Java.Byte_Arr;
                                 P5_Byte_Arr : Java.Byte_Arr;
                                 P6_Byte_Arr : Java.Byte_Arr; 
                                 This : Ref := null)
                                 return Ref;

   function New_IndexColorModel (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Byte_Arr : Java.Byte_Arr;
                                 P4_Int : Java.Int;
                                 P5_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   function New_IndexColorModel (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Byte_Arr : Java.Byte_Arr;
                                 P4_Int : Java.Int;
                                 P5_Boolean : Java.Boolean;
                                 P6_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_IndexColorModel (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int_Arr : Java.Int_Arr;
                                 P4_Int : Java.Int;
                                 P5_Boolean : Java.Boolean;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_IndexColorModel (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int_Arr : Java.Int_Arr;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransparency (This : access Typ)
                             return Java.Int;

   function GetComponentSize (This : access Typ)
                              return Java.Int_Arr;

   --  final
   function GetMapSize (This : access Typ)
                        return Java.Int;

   --  final
   function GetTransparentPixel (This : access Typ)
                                 return Java.Int;

   --  final
   procedure GetReds (This : access Typ;
                      P1_Byte_Arr : Java.Byte_Arr);

   --  final
   procedure GetGreens (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);

   --  final
   procedure GetBlues (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr);

   --  final
   procedure GetAlphas (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);

   --  final
   procedure GetRGBs (This : access Typ;
                      P1_Int_Arr : Java.Int_Arr);

   --  final
   function GetRed (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   --  final
   function GetGreen (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;

   --  final
   function GetBlue (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   --  final
   function GetAlpha (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;

   --  final
   function GetRGB (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   --  synchronized
   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetComponents (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   function GetComponents (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int)
                           return Java.Int_Arr;

   function GetDataElement (This : access Typ;
                            P1_Int_Arr : Java.Int_Arr;
                            P2_Int : Java.Int)
                            return Java.Int;

   function GetDataElements (This : access Typ;
                             P1_Int_Arr : Java.Int_Arr;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function CreateCompatibleWritableRaster (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Int : Java.Int)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function IsCompatibleRaster (This : access Typ;
                                P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                                return Java.Boolean;

   function CreateCompatibleSampleModel (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return access Java.Awt.Image.SampleModel.Typ'Class;

   function IsCompatibleSampleModel (This : access Typ;
                                     P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class)
                                     return Java.Boolean;

   function ConvertToIntDiscrete (This : access Typ;
                                  P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                                  P2_Boolean : Java.Boolean)
                                  return access Java.Awt.Image.BufferedImage.Typ'Class;

   function IsValid (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Boolean;

   function IsValid (This : access Typ)
                     return Java.Boolean;

   function GetValidPixels (This : access Typ)
                            return access Java.Math.BigInteger.Typ'Class;

   procedure Finalize (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IndexColorModel);
   pragma Import (Java, GetTransparency, "getTransparency");
   pragma Import (Java, GetComponentSize, "getComponentSize");
   pragma Import (Java, GetMapSize, "getMapSize");
   pragma Import (Java, GetTransparentPixel, "getTransparentPixel");
   pragma Import (Java, GetReds, "getReds");
   pragma Import (Java, GetGreens, "getGreens");
   pragma Import (Java, GetBlues, "getBlues");
   pragma Import (Java, GetAlphas, "getAlphas");
   pragma Import (Java, GetRGBs, "getRGBs");
   pragma Import (Java, GetRed, "getRed");
   pragma Import (Java, GetGreen, "getGreen");
   pragma Import (Java, GetBlue, "getBlue");
   pragma Import (Java, GetAlpha, "getAlpha");
   pragma Import (Java, GetRGB, "getRGB");
   pragma Import (Java, GetDataElements, "getDataElements");
   pragma Import (Java, GetComponents, "getComponents");
   pragma Import (Java, GetDataElement, "getDataElement");
   pragma Import (Java, CreateCompatibleWritableRaster, "createCompatibleWritableRaster");
   pragma Import (Java, IsCompatibleRaster, "isCompatibleRaster");
   pragma Import (Java, CreateCompatibleSampleModel, "createCompatibleSampleModel");
   pragma Import (Java, IsCompatibleSampleModel, "isCompatibleSampleModel");
   pragma Import (Java, ConvertToIntDiscrete, "convertToIntDiscrete");
   pragma Import (Java, IsValid, "isValid");
   pragma Import (Java, GetValidPixels, "getValidPixels");
   pragma Import (Java, Finalize, "finalize");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Image.IndexColorModel;
pragma Import (Java, Java.Awt.Image.IndexColorModel, "java.awt.image.IndexColorModel");
pragma Extensions_Allowed (Off);
