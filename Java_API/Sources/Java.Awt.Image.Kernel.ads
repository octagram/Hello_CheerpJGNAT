pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Image.Kernel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Kernel (P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Float_Arr : Java.Float_Arr; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetXOrigin (This : access Typ)
                        return Java.Int;

   --  final
   function GetYOrigin (This : access Typ)
                        return Java.Int;

   --  final
   function GetWidth (This : access Typ)
                      return Java.Int;

   --  final
   function GetHeight (This : access Typ)
                       return Java.Int;

   --  final
   function GetKernelData (This : access Typ;
                           P1_Float_Arr : Java.Float_Arr)
                           return Java.Float_Arr;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Kernel);
   pragma Import (Java, GetXOrigin, "getXOrigin");
   pragma Import (Java, GetYOrigin, "getYOrigin");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetKernelData, "getKernelData");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Image.Kernel;
pragma Import (Java, Java.Awt.Image.Kernel, "java.awt.image.Kernel");
pragma Extensions_Allowed (Off);
