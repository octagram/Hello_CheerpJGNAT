pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Image.LookupTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_LookupTable (P1_Int : Java.Int;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNumComponents (This : access Typ)
                              return Java.Int;

   function GetOffset (This : access Typ)
                       return Java.Int;

   function LookupPixel (This : access Typ;
                         P1_Int_Arr : Java.Int_Arr;
                         P2_Int_Arr : Java.Int_Arr)
                         return Java.Int_Arr is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LookupTable);
   pragma Import (Java, GetNumComponents, "getNumComponents");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Export (Java, LookupPixel, "lookupPixel");

end Java.Awt.Image.LookupTable;
pragma Import (Java, Java.Awt.Image.LookupTable, "java.awt.image.LookupTable");
pragma Extensions_Allowed (Off);
