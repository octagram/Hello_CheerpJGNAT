pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.ImageConsumer;
limited with Java.Util.Hashtable;
with Java.Awt.Image.ImageProducer;
with Java.Lang.Object;

package Java.Awt.Image.MemoryImageSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageProducer_I : Java.Awt.Image.ImageProducer.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MemoryImageSource (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                                   P4_Byte_Arr : Java.Byte_Arr;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_MemoryImageSource (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                                   P4_Byte_Arr : Java.Byte_Arr;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int;
                                   P7_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_MemoryImageSource (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                                   P4_Int_Arr : Java.Int_Arr;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_MemoryImageSource (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                                   P4_Int_Arr : Java.Int_Arr;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int;
                                   P7_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_MemoryImageSource (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int_Arr : Java.Int_Arr;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_MemoryImageSource (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int_Arr : Java.Int_Arr;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddConsumer (This : access Typ;
                          P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   --  synchronized
   function IsConsumer (This : access Typ;
                        P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class)
                        return Java.Boolean;

   --  synchronized
   procedure RemoveConsumer (This : access Typ;
                             P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   procedure StartProduction (This : access Typ;
                              P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   procedure RequestTopDownLeftRightResend (This : access Typ;
                                            P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   --  synchronized
   procedure SetAnimated (This : access Typ;
                          P1_Boolean : Java.Boolean);

   --  synchronized
   procedure SetFullBufferUpdates (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   procedure NewPixels (This : access Typ);

   --  synchronized
   procedure NewPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   --  synchronized
   procedure NewPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Boolean : Java.Boolean);

   --  synchronized
   procedure NewPixels (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   --  synchronized
   procedure NewPixels (This : access Typ;
                        P1_Int_Arr : Java.Int_Arr;
                        P2_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MemoryImageSource);
   pragma Import (Java, AddConsumer, "addConsumer");
   pragma Import (Java, IsConsumer, "isConsumer");
   pragma Import (Java, RemoveConsumer, "removeConsumer");
   pragma Import (Java, StartProduction, "startProduction");
   pragma Import (Java, RequestTopDownLeftRightResend, "requestTopDownLeftRightResend");
   pragma Import (Java, SetAnimated, "setAnimated");
   pragma Import (Java, SetFullBufferUpdates, "setFullBufferUpdates");
   pragma Import (Java, NewPixels, "newPixels");

end Java.Awt.Image.MemoryImageSource;
pragma Import (Java, Java.Awt.Image.MemoryImageSource, "java.awt.image.MemoryImageSource");
pragma Extensions_Allowed (Off);
