pragma Extensions_Allowed (On);
limited with Java.Awt.Image.DataBuffer;
with Java.Awt.Image.SampleModel;
with Java.Lang.Object;

package Java.Awt.Image.MultiPixelPackedSampleModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Image.SampleModel.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MultiPixelPackedSampleModel (P1_Int : Java.Int;
                                             P2_Int : Java.Int;
                                             P3_Int : Java.Int;
                                             P4_Int : Java.Int; 
                                             This : Ref := null)
                                             return Ref;

   function New_MultiPixelPackedSampleModel (P1_Int : Java.Int;
                                             P2_Int : Java.Int;
                                             P3_Int : Java.Int;
                                             P4_Int : Java.Int;
                                             P5_Int : Java.Int;
                                             P6_Int : Java.Int; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateCompatibleSampleModel (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return access Java.Awt.Image.SampleModel.Typ'Class;

   function CreateDataBuffer (This : access Typ)
                              return access Java.Awt.Image.DataBuffer.Typ'Class;

   function GetNumDataElements (This : access Typ)
                                return Java.Int;

   function GetSampleSize (This : access Typ)
                           return Java.Int_Arr;

   function GetSampleSize (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;

   function GetOffset (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return Java.Int;

   function GetBitOffset (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;

   function GetScanlineStride (This : access Typ)
                               return Java.Int;

   function GetPixelBitStride (This : access Typ)
                               return Java.Int;

   function GetDataBitOffset (This : access Typ)
                              return Java.Int;

   function GetTransferType (This : access Typ)
                             return Java.Int;

   function CreateSubsetSampleModel (This : access Typ;
                                     P1_Int_Arr : Java.Int_Arr)
                                     return access Java.Awt.Image.SampleModel.Typ'Class;

   function GetSample (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                       return Java.Int;

   procedure SetSample (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetPixel (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int_Arr : Java.Int_Arr;
                      P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                      return Java.Int_Arr;

   procedure SetDataElements (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetPixel (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int_Arr : Java.Int_Arr;
                       P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MultiPixelPackedSampleModel);
   pragma Import (Java, CreateCompatibleSampleModel, "createCompatibleSampleModel");
   pragma Import (Java, CreateDataBuffer, "createDataBuffer");
   pragma Import (Java, GetNumDataElements, "getNumDataElements");
   pragma Import (Java, GetSampleSize, "getSampleSize");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetBitOffset, "getBitOffset");
   pragma Import (Java, GetScanlineStride, "getScanlineStride");
   pragma Import (Java, GetPixelBitStride, "getPixelBitStride");
   pragma Import (Java, GetDataBitOffset, "getDataBitOffset");
   pragma Import (Java, GetTransferType, "getTransferType");
   pragma Import (Java, CreateSubsetSampleModel, "createSubsetSampleModel");
   pragma Import (Java, GetSample, "getSample");
   pragma Import (Java, SetSample, "setSample");
   pragma Import (Java, GetDataElements, "getDataElements");
   pragma Import (Java, GetPixel, "getPixel");
   pragma Import (Java, SetDataElements, "setDataElements");
   pragma Import (Java, SetPixel, "setPixel");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Awt.Image.MultiPixelPackedSampleModel;
pragma Import (Java, Java.Awt.Image.MultiPixelPackedSampleModel, "java.awt.image.MultiPixelPackedSampleModel");
pragma Extensions_Allowed (Off);
