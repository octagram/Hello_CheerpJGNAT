pragma Extensions_Allowed (On);
limited with Java.Awt.Color.ColorSpace;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Image.WritableRaster;
with Java.Awt.Image.ColorModel;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Image.PackedColorModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transparency_I : Java.Awt.Transparency.Ref)
    is abstract new Java.Awt.Image.ColorModel.Typ(Transparency_I)
      with null record;

   function New_PackedColorModel (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int_Arr : Java.Int_Arr;
                                  P4_Int : Java.Int;
                                  P5_Boolean : Java.Boolean;
                                  P6_Int : Java.Int;
                                  P7_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   function New_PackedColorModel (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int;
                                  P7_Boolean : Java.Boolean;
                                  P8_Int : Java.Int;
                                  P9_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetMask (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   --  final
   function GetMasks (This : access Typ)
                      return Java.Int_Arr;

   function CreateCompatibleSampleModel (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return access Java.Awt.Image.SampleModel.Typ'Class;

   function IsCompatibleSampleModel (This : access Typ;
                                     P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class)
                                     return Java.Boolean;

   function GetAlphaRaster (This : access Typ;
                            P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PackedColorModel);
   pragma Import (Java, GetMask, "getMask");
   pragma Import (Java, GetMasks, "getMasks");
   pragma Import (Java, CreateCompatibleSampleModel, "createCompatibleSampleModel");
   pragma Import (Java, IsCompatibleSampleModel, "isCompatibleSampleModel");
   pragma Import (Java, GetAlphaRaster, "getAlphaRaster");
   pragma Import (Java, Equals, "equals");

end Java.Awt.Image.PackedColorModel;
pragma Import (Java, Java.Awt.Image.PackedColorModel, "java.awt.image.PackedColorModel");
pragma Extensions_Allowed (Off);
