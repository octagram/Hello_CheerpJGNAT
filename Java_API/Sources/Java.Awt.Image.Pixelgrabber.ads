pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Util.Hashtable;
with Java.Awt.Image.ImageConsumer;
with Java.Lang.Object;

package Java.Awt.Image.PixelGrabber is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageConsumer_I : Java.Awt.Image.ImageConsumer.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PixelGrabber (P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Int_Arr : Java.Int_Arr;
                              P7_Int : Java.Int;
                              P8_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_PixelGrabber (P1_ImageProducer : access Standard.Java.Awt.Image.ImageProducer.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Int_Arr : Java.Int_Arr;
                              P7_Int : Java.Int;
                              P8_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_PixelGrabber (P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure StartGrabbing (This : access Typ);

   --  synchronized
   procedure AbortGrabbing (This : access Typ);

   function GrabPixels (This : access Typ)
                        return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   --  synchronized
   function GrabPixels (This : access Typ;
                        P1_Long : Java.Long)
                        return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   --  synchronized
   function GetStatus (This : access Typ)
                       return Java.Int;

   --  synchronized
   function GetWidth (This : access Typ)
                      return Java.Int;

   --  synchronized
   function GetHeight (This : access Typ)
                       return Java.Int;

   --  synchronized
   function GetPixels (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class;

   procedure SetDimensions (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);

   procedure SetHints (This : access Typ;
                       P1_Int : Java.Int);

   procedure SetProperties (This : access Typ;
                            P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class);

   procedure SetColorModel (This : access Typ;
                            P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Byte_Arr : Java.Byte_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Int_Arr : Java.Int_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);

   --  synchronized
   procedure ImageComplete (This : access Typ;
                            P1_Int : Java.Int);

   --  synchronized
   function Status (This : access Typ)
                    return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PixelGrabber);
   pragma Import (Java, StartGrabbing, "startGrabbing");
   pragma Import (Java, AbortGrabbing, "abortGrabbing");
   pragma Import (Java, GrabPixels, "grabPixels");
   pragma Import (Java, GetStatus, "getStatus");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetPixels, "getPixels");
   pragma Import (Java, GetColorModel, "getColorModel");
   pragma Import (Java, SetDimensions, "setDimensions");
   pragma Import (Java, SetHints, "setHints");
   pragma Import (Java, SetProperties, "setProperties");
   pragma Import (Java, SetColorModel, "setColorModel");
   pragma Import (Java, SetPixels, "setPixels");
   pragma Import (Java, ImageComplete, "imageComplete");
   pragma Import (Java, Status, "status");

end Java.Awt.Image.PixelGrabber;
pragma Import (Java, Java.Awt.Image.PixelGrabber, "java.awt.image.PixelGrabber");
pragma Extensions_Allowed (Off);
