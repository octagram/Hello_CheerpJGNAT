pragma Extensions_Allowed (On);
limited with Java.Awt.Image.SampleModel;
with Java.Awt.Image.ComponentSampleModel;
with Java.Lang.Object;

package Java.Awt.Image.PixelInterleavedSampleModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Image.ComponentSampleModel.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PixelInterleavedSampleModel (P1_Int : Java.Int;
                                             P2_Int : Java.Int;
                                             P3_Int : Java.Int;
                                             P4_Int : Java.Int;
                                             P5_Int : Java.Int;
                                             P6_Int_Arr : Java.Int_Arr; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateCompatibleSampleModel (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return access Java.Awt.Image.SampleModel.Typ'Class;

   function CreateSubsetSampleModel (This : access Typ;
                                     P1_Int_Arr : Java.Int_Arr)
                                     return access Java.Awt.Image.SampleModel.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PixelInterleavedSampleModel);
   pragma Import (Java, CreateCompatibleSampleModel, "createCompatibleSampleModel");
   pragma Import (Java, CreateSubsetSampleModel, "createSubsetSampleModel");
   pragma Import (Java, HashCode, "hashCode");

end Java.Awt.Image.PixelInterleavedSampleModel;
pragma Import (Java, Java.Awt.Image.PixelInterleavedSampleModel, "java.awt.image.PixelInterleavedSampleModel");
pragma Extensions_Allowed (Off);
