pragma Extensions_Allowed (On);
limited with Java.Awt.Image.DataBuffer;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
with Java.Lang.Object;

package Java.Awt.Image.Raster is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      SampleModel : access Java.Awt.Image.SampleModel.Typ'Class;
      pragma Import (Java, SampleModel, "sampleModel");

      --  protected
      DataBuffer : access Java.Awt.Image.DataBuffer.Typ'Class;
      pragma Import (Java, DataBuffer, "dataBuffer");

      --  protected
      MinX : Java.Int;
      pragma Import (Java, MinX, "minX");

      --  protected
      MinY : Java.Int;
      pragma Import (Java, MinY, "minY");

      --  protected
      Width : Java.Int;
      pragma Import (Java, Width, "width");

      --  protected
      Height : Java.Int;
      pragma Import (Java, Height, "height");

      --  protected
      SampleModelTranslateX : Java.Int;
      pragma Import (Java, SampleModelTranslateX, "sampleModelTranslateX");

      --  protected
      SampleModelTranslateY : Java.Int;
      pragma Import (Java, SampleModelTranslateY, "sampleModelTranslateY");

      --  protected
      NumBands : Java.Int;
      pragma Import (Java, NumBands, "numBands");

      --  protected
      NumDataElements : Java.Int;
      pragma Import (Java, NumDataElements, "numDataElements");

      --  protected
      Parent : access Java.Awt.Image.Raster.Typ'Class;
      pragma Import (Java, Parent, "parent");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateInterleavedRaster (P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Point : access Standard.Java.Awt.Point.Typ'Class)
                                     return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateInterleavedRaster (P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int_Arr : Java.Int_Arr;
                                     P7_Point : access Standard.Java.Awt.Point.Typ'Class)
                                     return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateBandedRaster (P1_Int : Java.Int;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Point : access Standard.Java.Awt.Point.Typ'Class)
                                return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateBandedRaster (P1_Int : Java.Int;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int_Arr : Java.Int_Arr;
                                P6_Int_Arr : Java.Int_Arr;
                                P7_Point : access Standard.Java.Awt.Point.Typ'Class)
                                return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreatePackedRaster (P1_Int : Java.Int;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int_Arr : Java.Int_Arr;
                                P5_Point : access Standard.Java.Awt.Point.Typ'Class)
                                return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreatePackedRaster (P1_Int : Java.Int;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Point : access Standard.Java.Awt.Point.Typ'Class)
                                return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateInterleavedRaster (P1_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int_Arr : Java.Int_Arr;
                                     P7_Point : access Standard.Java.Awt.Point.Typ'Class)
                                     return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateBandedRaster (P1_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int_Arr : Java.Int_Arr;
                                P6_Int_Arr : Java.Int_Arr;
                                P7_Point : access Standard.Java.Awt.Point.Typ'Class)
                                return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreatePackedRaster (P1_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int_Arr : Java.Int_Arr;
                                P6_Point : access Standard.Java.Awt.Point.Typ'Class)
                                return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreatePackedRaster (P1_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Point : access Standard.Java.Awt.Point.Typ'Class)
                                return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateRaster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                          P2_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                          P3_Point : access Standard.Java.Awt.Point.Typ'Class)
                          return access Java.Awt.Image.Raster.Typ'Class;

   function CreateWritableRaster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                                  P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                                  return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateWritableRaster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                                  P2_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                                  P3_Point : access Standard.Java.Awt.Point.Typ'Class)
                                  return access Java.Awt.Image.WritableRaster.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Raster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                        P2_Point : access Standard.Java.Awt.Point.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   --  protected
   function New_Raster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                        P2_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                        P3_Point : access Standard.Java.Awt.Point.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   --  protected
   function New_Raster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                        P2_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                        P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P4_Point : access Standard.Java.Awt.Point.Typ'Class;
                        P5_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function GetParent (This : access Typ)
                       return access Java.Awt.Image.Raster.Typ'Class;

   --  final
   function GetSampleModelTranslateX (This : access Typ)
                                      return Java.Int;

   --  final
   function GetSampleModelTranslateY (This : access Typ)
                                      return Java.Int;

   function CreateCompatibleWritableRaster (This : access Typ)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateCompatibleWritableRaster (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Int : Java.Int)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateCompatibleWritableRaster (This : access Typ;
                                            P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateCompatibleWritableRaster (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Int : Java.Int;
                                            P3_Int : Java.Int;
                                            P4_Int : Java.Int)
                                            return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateTranslatedChild (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int)
                                   return access Java.Awt.Image.Raster.Typ'Class;

   function CreateChild (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int : Java.Int;
                         P7_Int_Arr : Java.Int_Arr)
                         return access Java.Awt.Image.Raster.Typ'Class;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   --  final
   function GetMinX (This : access Typ)
                     return Java.Int;

   --  final
   function GetMinY (This : access Typ)
                     return Java.Int;

   --  final
   function GetWidth (This : access Typ)
                      return Java.Int;

   --  final
   function GetHeight (This : access Typ)
                       return Java.Int;

   --  final
   function GetNumBands (This : access Typ)
                         return Java.Int;

   --  final
   function GetNumDataElements (This : access Typ)
                                return Java.Int;

   --  final
   function GetTransferType (This : access Typ)
                             return Java.Int;

   function GetDataBuffer (This : access Typ)
                           return access Java.Awt.Image.DataBuffer.Typ'Class;

   function GetSampleModel (This : access Typ)
                            return access Java.Awt.Image.SampleModel.Typ'Class;

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetPixel (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int_Arr : Java.Int_Arr)
                      return Java.Int_Arr;

   function GetPixel (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Float_Arr : Java.Float_Arr)
                      return Java.Float_Arr;

   function GetPixel (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Double_Arr : Java.Double_Arr)
                      return Java.Double_Arr;

   function GetPixels (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int_Arr : Java.Int_Arr)
                       return Java.Int_Arr;

   function GetPixels (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Float_Arr : Java.Float_Arr)
                       return Java.Float_Arr;

   function GetPixels (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Double_Arr : Java.Double_Arr)
                       return Java.Double_Arr;

   function GetSample (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int)
                       return Java.Int;

   function GetSampleFloat (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return Java.Float;

   function GetSampleDouble (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int)
                             return Java.Double;

   function GetSamples (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Int_Arr : Java.Int_Arr)
                        return Java.Int_Arr;

   function GetSamples (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Float_Arr : Java.Float_Arr)
                        return Java.Float_Arr;

   function GetSamples (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Double_Arr : Java.Double_Arr)
                        return Java.Double_Arr;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateInterleavedRaster, "createInterleavedRaster");
   pragma Import (Java, CreateBandedRaster, "createBandedRaster");
   pragma Import (Java, CreatePackedRaster, "createPackedRaster");
   pragma Import (Java, CreateRaster, "createRaster");
   pragma Import (Java, CreateWritableRaster, "createWritableRaster");
   pragma Java_Constructor (New_Raster);
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetSampleModelTranslateX, "getSampleModelTranslateX");
   pragma Import (Java, GetSampleModelTranslateY, "getSampleModelTranslateY");
   pragma Import (Java, CreateCompatibleWritableRaster, "createCompatibleWritableRaster");
   pragma Import (Java, CreateTranslatedChild, "createTranslatedChild");
   pragma Import (Java, CreateChild, "createChild");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetMinX, "getMinX");
   pragma Import (Java, GetMinY, "getMinY");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetNumBands, "getNumBands");
   pragma Import (Java, GetNumDataElements, "getNumDataElements");
   pragma Import (Java, GetTransferType, "getTransferType");
   pragma Import (Java, GetDataBuffer, "getDataBuffer");
   pragma Import (Java, GetSampleModel, "getSampleModel");
   pragma Import (Java, GetDataElements, "getDataElements");
   pragma Import (Java, GetPixel, "getPixel");
   pragma Import (Java, GetPixels, "getPixels");
   pragma Import (Java, GetSample, "getSample");
   pragma Import (Java, GetSampleFloat, "getSampleFloat");
   pragma Import (Java, GetSampleDouble, "getSampleDouble");
   pragma Import (Java, GetSamples, "getSamples");

end Java.Awt.Image.Raster;
pragma Import (Java, Java.Awt.Image.Raster, "java.awt.image.Raster");
pragma Extensions_Allowed (Off);
