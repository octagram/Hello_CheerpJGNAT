pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Awt.RenderingHints;
with Java.Lang.Object;

package Java.Awt.Image.RasterOp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Filter (This : access Typ;
                    P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                    P2_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                    return access Java.Awt.Image.WritableRaster.Typ'Class is abstract;

   function GetBounds2D (This : access Typ;
                         P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   function CreateCompatibleDestRaster (This : access Typ;
                                        P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class)
                                        return access Java.Awt.Image.WritableRaster.Typ'Class is abstract;

   function GetPoint2D (This : access Typ;
                        P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                        P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                        return access Java.Awt.Geom.Point2D.Typ'Class is abstract;

   function GetRenderingHints (This : access Typ)
                               return access Java.Awt.RenderingHints.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Filter, "filter");
   pragma Export (Java, GetBounds2D, "getBounds2D");
   pragma Export (Java, CreateCompatibleDestRaster, "createCompatibleDestRaster");
   pragma Export (Java, GetPoint2D, "getPoint2D");
   pragma Export (Java, GetRenderingHints, "getRenderingHints");

end Java.Awt.Image.RasterOp;
pragma Import (Java, Java.Awt.Image.RasterOp, "java.awt.image.RasterOp");
pragma Extensions_Allowed (Off);
