pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.Renderable.ParameterBlock;
limited with Java.Awt.Image.Renderable.RenderContext;
limited with Java.Awt.Image.Renderable.RenderableImage;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Lang.String;
with Java.Awt.Image.Renderable.RenderedImageFactory;
with Java.Lang.Object;

package Java.Awt.Image.Renderable.ContextualRenderedImageFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RenderedImageFactory_I : Java.Awt.Image.Renderable.RenderedImageFactory.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function MapRenderContext (This : access Typ;
                              P1_Int : Java.Int;
                              P2_RenderContext : access Standard.Java.Awt.Image.Renderable.RenderContext.Typ'Class;
                              P3_ParameterBlock : access Standard.Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;
                              P4_RenderableImage : access Standard.Java.Awt.Image.Renderable.RenderableImage.Typ'Class)
                              return access Java.Awt.Image.Renderable.RenderContext.Typ'Class is abstract;

   function Create (This : access Typ;
                    P1_RenderContext : access Standard.Java.Awt.Image.Renderable.RenderContext.Typ'Class;
                    P2_ParameterBlock : access Standard.Java.Awt.Image.Renderable.ParameterBlock.Typ'Class)
                    return access Java.Awt.Image.RenderedImage.Typ'Class is abstract;

   function GetBounds2D (This : access Typ;
                         P1_ParameterBlock : access Standard.Java.Awt.Image.Renderable.ParameterBlock.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class is abstract;

   function GetProperty (This : access Typ;
                         P1_ParameterBlock : access Standard.Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function GetPropertyNames (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function IsDynamic (This : access Typ)
                       return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, MapRenderContext, "mapRenderContext");
   pragma Export (Java, Create, "create");
   pragma Export (Java, GetBounds2D, "getBounds2D");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, GetPropertyNames, "getPropertyNames");
   pragma Export (Java, IsDynamic, "isDynamic");

end Java.Awt.Image.Renderable.ContextualRenderedImageFactory;
pragma Import (Java, Java.Awt.Image.Renderable.ContextualRenderedImageFactory, "java.awt.image.renderable.ContextualRenderedImageFactory");
pragma Extensions_Allowed (Off);
