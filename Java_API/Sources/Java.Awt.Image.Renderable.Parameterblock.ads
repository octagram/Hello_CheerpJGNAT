pragma Extensions_Allowed (On);
limited with Java.Awt.Image.Renderable.RenderableImage;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Lang.Class;
limited with Java.Util.Vector;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Image.Renderable.ParameterBlock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Sources : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Sources, "sources");

      --  protected
      Parameters : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Parameters, "parameters");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ParameterBlock (This : Ref := null)
                                return Ref;

   function New_ParameterBlock (P1_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_ParameterBlock (P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                                P2_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ShallowClone (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function AddSource (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function GetSource (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.Object.Typ'Class;

   function SetSource (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int)
                       return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function GetRenderedSource (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Awt.Image.RenderedImage.Typ'Class;

   function GetRenderableSource (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Java.Awt.Image.Renderable.RenderableImage.Typ'Class;

   function GetNumSources (This : access Typ)
                           return Java.Int;

   function GetSources (This : access Typ)
                        return access Java.Util.Vector.Typ'Class;

   procedure SetSources (This : access Typ;
                         P1_Vector : access Standard.Java.Util.Vector.Typ'Class);

   procedure RemoveSources (This : access Typ);

   function GetNumParameters (This : access Typ)
                              return Java.Int;

   function GetParameters (This : access Typ)
                           return access Java.Util.Vector.Typ'Class;

   procedure SetParameters (This : access Typ;
                            P1_Vector : access Standard.Java.Util.Vector.Typ'Class);

   procedure RemoveParameters (This : access Typ);

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Add (This : access Typ;
                 P1_Byte : Java.Byte)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Add (This : access Typ;
                 P1_Char : Java.Char)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Add (This : access Typ;
                 P1_Short : Java.Short)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Add (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Add (This : access Typ;
                 P1_Long : Java.Long)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Add (This : access Typ;
                 P1_Float : Java.Float)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Add (This : access Typ;
                 P1_Double : Java.Double)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Byte : Java.Byte;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Char : Java.Char;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Short : Java.Short;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Long : Java.Long;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Float : Java.Float;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function Set (This : access Typ;
                 P1_Double : Java.Double;
                 P2_Int : Java.Int)
                 return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function GetObjectParameter (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.Object.Typ'Class;

   function GetByteParameter (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Byte;

   function GetCharParameter (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Char;

   function GetShortParameter (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Short;

   function GetIntParameter (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   function GetLongParameter (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Long;

   function GetFloatParameter (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Float;

   function GetDoubleParameter (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Double;

   function GetParamClasses (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ParameterBlock);
   pragma Import (Java, ShallowClone, "shallowClone");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, AddSource, "addSource");
   pragma Import (Java, GetSource, "getSource");
   pragma Import (Java, SetSource, "setSource");
   pragma Import (Java, GetRenderedSource, "getRenderedSource");
   pragma Import (Java, GetRenderableSource, "getRenderableSource");
   pragma Import (Java, GetNumSources, "getNumSources");
   pragma Import (Java, GetSources, "getSources");
   pragma Import (Java, SetSources, "setSources");
   pragma Import (Java, RemoveSources, "removeSources");
   pragma Import (Java, GetNumParameters, "getNumParameters");
   pragma Import (Java, GetParameters, "getParameters");
   pragma Import (Java, SetParameters, "setParameters");
   pragma Import (Java, RemoveParameters, "removeParameters");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Set, "set");
   pragma Import (Java, GetObjectParameter, "getObjectParameter");
   pragma Import (Java, GetByteParameter, "getByteParameter");
   pragma Import (Java, GetCharParameter, "getCharParameter");
   pragma Import (Java, GetShortParameter, "getShortParameter");
   pragma Import (Java, GetIntParameter, "getIntParameter");
   pragma Import (Java, GetLongParameter, "getLongParameter");
   pragma Import (Java, GetFloatParameter, "getFloatParameter");
   pragma Import (Java, GetDoubleParameter, "getDoubleParameter");
   pragma Import (Java, GetParamClasses, "getParamClasses");

end Java.Awt.Image.Renderable.ParameterBlock;
pragma Import (Java, Java.Awt.Image.Renderable.ParameterBlock, "java.awt.image.renderable.ParameterBlock");
pragma Extensions_Allowed (Off);
