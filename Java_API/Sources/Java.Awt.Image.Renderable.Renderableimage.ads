pragma Extensions_Allowed (On);
limited with Java.Awt.Image.Renderable.RenderContext;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Awt.RenderingHints;
limited with Java.Lang.String;
limited with Java.Util.Vector;
with Java.Lang.Object;

package Java.Awt.Image.Renderable.RenderableImage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSources (This : access Typ)
                        return access Java.Util.Vector.Typ'Class is abstract;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function GetPropertyNames (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function IsDynamic (This : access Typ)
                       return Java.Boolean is abstract;

   function GetWidth (This : access Typ)
                      return Java.Float is abstract;

   function GetHeight (This : access Typ)
                       return Java.Float is abstract;

   function GetMinX (This : access Typ)
                     return Java.Float is abstract;

   function GetMinY (This : access Typ)
                     return Java.Float is abstract;

   function CreateScaledRendering (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                                   return access Java.Awt.Image.RenderedImage.Typ'Class is abstract;

   function CreateDefaultRendering (This : access Typ)
                                    return access Java.Awt.Image.RenderedImage.Typ'Class is abstract;

   function CreateRendering (This : access Typ;
                             P1_RenderContext : access Standard.Java.Awt.Image.Renderable.RenderContext.Typ'Class)
                             return access Java.Awt.Image.RenderedImage.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   HINTS_OBSERVED : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSources, "getSources");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, GetPropertyNames, "getPropertyNames");
   pragma Export (Java, IsDynamic, "isDynamic");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, GetMinX, "getMinX");
   pragma Export (Java, GetMinY, "getMinY");
   pragma Export (Java, CreateScaledRendering, "createScaledRendering");
   pragma Export (Java, CreateDefaultRendering, "createDefaultRendering");
   pragma Export (Java, CreateRendering, "createRendering");
   pragma Import (Java, HINTS_OBSERVED, "HINTS_OBSERVED");

end Java.Awt.Image.Renderable.RenderableImage;
pragma Import (Java, Java.Awt.Image.Renderable.RenderableImage, "java.awt.image.renderable.RenderableImage");
pragma Extensions_Allowed (Off);
