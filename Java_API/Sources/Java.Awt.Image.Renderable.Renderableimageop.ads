pragma Extensions_Allowed (On);
limited with Java.Awt.Image.Renderable.ContextualRenderedImageFactory;
limited with Java.Awt.Image.Renderable.ParameterBlock;
limited with Java.Awt.Image.Renderable.RenderContext;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Awt.RenderingHints;
limited with Java.Lang.String;
limited with Java.Util.Vector;
with Java.Awt.Image.Renderable.RenderableImage;
with Java.Lang.Object;

package Java.Awt.Image.Renderable.RenderableImageOp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RenderableImage_I : Java.Awt.Image.Renderable.RenderableImage.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RenderableImageOp (P1_ContextualRenderedImageFactory : access Standard.Java.Awt.Image.Renderable.ContextualRenderedImageFactory.Typ'Class;
                                   P2_ParameterBlock : access Standard.Java.Awt.Image.Renderable.ParameterBlock.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSources (This : access Typ)
                        return access Java.Util.Vector.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function GetPropertyNames (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function IsDynamic (This : access Typ)
                       return Java.Boolean;

   function GetWidth (This : access Typ)
                      return Java.Float;

   function GetHeight (This : access Typ)
                       return Java.Float;

   function GetMinX (This : access Typ)
                     return Java.Float;

   function GetMinY (This : access Typ)
                     return Java.Float;

   function SetParameterBlock (This : access Typ;
                               P1_ParameterBlock : access Standard.Java.Awt.Image.Renderable.ParameterBlock.Typ'Class)
                               return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function GetParameterBlock (This : access Typ)
                               return access Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;

   function CreateScaledRendering (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                                   return access Java.Awt.Image.RenderedImage.Typ'Class;

   function CreateDefaultRendering (This : access Typ)
                                    return access Java.Awt.Image.RenderedImage.Typ'Class;

   function CreateRendering (This : access Typ;
                             P1_RenderContext : access Standard.Java.Awt.Image.Renderable.RenderContext.Typ'Class)
                             return access Java.Awt.Image.RenderedImage.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RenderableImageOp);
   pragma Import (Java, GetSources, "getSources");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, GetPropertyNames, "getPropertyNames");
   pragma Import (Java, IsDynamic, "isDynamic");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetMinX, "getMinX");
   pragma Import (Java, GetMinY, "getMinY");
   pragma Import (Java, SetParameterBlock, "setParameterBlock");
   pragma Import (Java, GetParameterBlock, "getParameterBlock");
   pragma Import (Java, CreateScaledRendering, "createScaledRendering");
   pragma Import (Java, CreateDefaultRendering, "createDefaultRendering");
   pragma Import (Java, CreateRendering, "createRendering");

end Java.Awt.Image.Renderable.RenderableImageOp;
pragma Import (Java, Java.Awt.Image.Renderable.RenderableImageOp, "java.awt.image.renderable.RenderableImageOp");
pragma Extensions_Allowed (Off);
