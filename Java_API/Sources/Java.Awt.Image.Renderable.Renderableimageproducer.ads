pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ImageConsumer;
limited with Java.Awt.Image.Renderable.RenderContext;
limited with Java.Awt.Image.Renderable.RenderableImage;
with Java.Awt.Image.ImageProducer;
with Java.Lang.Object;
with Java.Lang.Runnable;

package Java.Awt.Image.Renderable.RenderableImageProducer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageProducer_I : Java.Awt.Image.ImageProducer.Ref;
            Runnable_I : Java.Lang.Runnable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RenderableImageProducer (P1_RenderableImage : access Standard.Java.Awt.Image.Renderable.RenderableImage.Typ'Class;
                                         P2_RenderContext : access Standard.Java.Awt.Image.Renderable.RenderContext.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure SetRenderContext (This : access Typ;
                               P1_RenderContext : access Standard.Java.Awt.Image.Renderable.RenderContext.Typ'Class);

   --  synchronized
   procedure AddConsumer (This : access Typ;
                          P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   --  synchronized
   function IsConsumer (This : access Typ;
                        P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class)
                        return Java.Boolean;

   --  synchronized
   procedure RemoveConsumer (This : access Typ;
                             P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   --  synchronized
   procedure StartProduction (This : access Typ;
                              P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   procedure RequestTopDownLeftRightResend (This : access Typ;
                                            P1_ImageConsumer : access Standard.Java.Awt.Image.ImageConsumer.Typ'Class);

   procedure Run (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RenderableImageProducer);
   pragma Import (Java, SetRenderContext, "setRenderContext");
   pragma Import (Java, AddConsumer, "addConsumer");
   pragma Import (Java, IsConsumer, "isConsumer");
   pragma Import (Java, RemoveConsumer, "removeConsumer");
   pragma Import (Java, StartProduction, "startProduction");
   pragma Import (Java, RequestTopDownLeftRightResend, "requestTopDownLeftRightResend");
   pragma Import (Java, Run, "run");

end Java.Awt.Image.Renderable.RenderableImageProducer;
pragma Import (Java, Java.Awt.Image.Renderable.RenderableImageProducer, "java.awt.image.renderable.RenderableImageProducer");
pragma Extensions_Allowed (Off);
