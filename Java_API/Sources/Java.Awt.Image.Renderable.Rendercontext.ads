pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.RenderingHints;
limited with Java.Awt.Shape;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Image.Renderable.RenderContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RenderContext (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                               P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                               P3_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_RenderContext (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_RenderContext (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                               P2_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_RenderContext (P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                               P2_Shape : access Standard.Java.Awt.Shape.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRenderingHints (This : access Typ)
                               return access Java.Awt.RenderingHints.Typ'Class;

   procedure SetRenderingHints (This : access Typ;
                                P1_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class);

   procedure SetTransform (This : access Typ;
                           P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   procedure PreConcatenateTransform (This : access Typ;
                                      P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   procedure ConcatenateTransform (This : access Typ;
                                   P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class);

   function GetTransform (This : access Typ)
                          return access Java.Awt.Geom.AffineTransform.Typ'Class;

   procedure SetAreaOfInterest (This : access Typ;
                                P1_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetAreaOfInterest (This : access Typ)
                               return access Java.Awt.Shape.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RenderContext);
   pragma Import (Java, GetRenderingHints, "getRenderingHints");
   pragma Import (Java, SetRenderingHints, "setRenderingHints");
   pragma Import (Java, SetTransform, "setTransform");
   pragma Import (Java, PreConcatenateTransform, "preConcatenateTransform");
   pragma Import (Java, ConcatenateTransform, "concatenateTransform");
   pragma Import (Java, GetTransform, "getTransform");
   pragma Import (Java, SetAreaOfInterest, "setAreaOfInterest");
   pragma Import (Java, GetAreaOfInterest, "getAreaOfInterest");
   pragma Import (Java, Clone, "clone");

end Java.Awt.Image.Renderable.RenderContext;
pragma Import (Java, Java.Awt.Image.Renderable.RenderContext, "java.awt.image.renderable.RenderContext");
pragma Extensions_Allowed (Off);
