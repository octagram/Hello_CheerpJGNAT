pragma Extensions_Allowed (On);
limited with Java.Awt.Image.Renderable.ParameterBlock;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Awt.RenderingHints;
with Java.Lang.Object;

package Java.Awt.Image.Renderable.RenderedImageFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create (This : access Typ;
                    P1_ParameterBlock : access Standard.Java.Awt.Image.Renderable.ParameterBlock.Typ'Class;
                    P2_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                    return access Java.Awt.Image.RenderedImage.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Create, "create");

end Java.Awt.Image.Renderable.RenderedImageFactory;
pragma Import (Java, Java.Awt.Image.Renderable.RenderedImageFactory, "java.awt.image.renderable.RenderedImageFactory");
pragma Extensions_Allowed (Off);
