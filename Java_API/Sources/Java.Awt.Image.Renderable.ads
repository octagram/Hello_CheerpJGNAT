pragma Extensions_Allowed (On);
package Java.Awt.Image.Renderable is
   pragma Preelaborate;
end Java.Awt.Image.Renderable;
pragma Import (Java, Java.Awt.Image.Renderable, "java.awt.image.renderable");
pragma Extensions_Allowed (Off);
