pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Vector;
with Java.Lang.Object;

package Java.Awt.Image.RenderedImage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSources (This : access Typ)
                        return access Java.Util.Vector.Typ'Class is abstract;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function GetPropertyNames (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class is abstract;

   function GetSampleModel (This : access Typ)
                            return access Java.Awt.Image.SampleModel.Typ'Class is abstract;

   function GetWidth (This : access Typ)
                      return Java.Int is abstract;

   function GetHeight (This : access Typ)
                       return Java.Int is abstract;

   function GetMinX (This : access Typ)
                     return Java.Int is abstract;

   function GetMinY (This : access Typ)
                     return Java.Int is abstract;

   function GetNumXTiles (This : access Typ)
                          return Java.Int is abstract;

   function GetNumYTiles (This : access Typ)
                          return Java.Int is abstract;

   function GetMinTileX (This : access Typ)
                         return Java.Int is abstract;

   function GetMinTileY (This : access Typ)
                         return Java.Int is abstract;

   function GetTileWidth (This : access Typ)
                          return Java.Int is abstract;

   function GetTileHeight (This : access Typ)
                           return Java.Int is abstract;

   function GetTileGridXOffset (This : access Typ)
                                return Java.Int is abstract;

   function GetTileGridYOffset (This : access Typ)
                                return Java.Int is abstract;

   function GetTile (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Java.Awt.Image.Raster.Typ'Class is abstract;

   function GetData (This : access Typ)
                     return access Java.Awt.Image.Raster.Typ'Class is abstract;

   function GetData (This : access Typ;
                     P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                     return access Java.Awt.Image.Raster.Typ'Class is abstract;

   function CopyData (This : access Typ;
                      P1_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class)
                      return access Java.Awt.Image.WritableRaster.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSources, "getSources");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, GetPropertyNames, "getPropertyNames");
   pragma Export (Java, GetColorModel, "getColorModel");
   pragma Export (Java, GetSampleModel, "getSampleModel");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, GetMinX, "getMinX");
   pragma Export (Java, GetMinY, "getMinY");
   pragma Export (Java, GetNumXTiles, "getNumXTiles");
   pragma Export (Java, GetNumYTiles, "getNumYTiles");
   pragma Export (Java, GetMinTileX, "getMinTileX");
   pragma Export (Java, GetMinTileY, "getMinTileY");
   pragma Export (Java, GetTileWidth, "getTileWidth");
   pragma Export (Java, GetTileHeight, "getTileHeight");
   pragma Export (Java, GetTileGridXOffset, "getTileGridXOffset");
   pragma Export (Java, GetTileGridYOffset, "getTileGridYOffset");
   pragma Export (Java, GetTile, "getTile");
   pragma Export (Java, GetData, "getData");
   pragma Export (Java, CopyData, "copyData");

end Java.Awt.Image.RenderedImage;
pragma Import (Java, Java.Awt.Image.RenderedImage, "java.awt.image.RenderedImage");
pragma Extensions_Allowed (Off);
