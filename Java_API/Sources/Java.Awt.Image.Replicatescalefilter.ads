pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Util.Hashtable;
with Java.Awt.Image.ImageConsumer;
with Java.Awt.Image.ImageFilter;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Image.ReplicateScaleFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageConsumer_I : Java.Awt.Image.ImageConsumer.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Image.ImageFilter.Typ(ImageConsumer_I,
                                          Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      SrcWidth : Java.Int;
      pragma Import (Java, SrcWidth, "srcWidth");

      --  protected
      SrcHeight : Java.Int;
      pragma Import (Java, SrcHeight, "srcHeight");

      --  protected
      DestWidth : Java.Int;
      pragma Import (Java, DestWidth, "destWidth");

      --  protected
      DestHeight : Java.Int;
      pragma Import (Java, DestHeight, "destHeight");

      --  protected
      Srcrows : Java.Int_Arr;
      pragma Import (Java, Srcrows, "srcrows");

      --  protected
      Srccols : Java.Int_Arr;
      pragma Import (Java, Srccols, "srccols");

      --  protected
      Outpixbuf : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Outpixbuf, "outpixbuf");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ReplicateScaleFilter (P1_Int : Java.Int;
                                      P2_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetProperties (This : access Typ;
                            P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class);

   procedure SetDimensions (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Byte_Arr : Java.Byte_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Int_Arr : Java.Int_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ReplicateScaleFilter);
   pragma Import (Java, SetProperties, "setProperties");
   pragma Import (Java, SetDimensions, "setDimensions");
   pragma Import (Java, SetPixels, "setPixels");

end Java.Awt.Image.ReplicateScaleFilter;
pragma Import (Java, Java.Awt.Image.ReplicateScaleFilter, "java.awt.image.ReplicateScaleFilter");
pragma Extensions_Allowed (Off);
