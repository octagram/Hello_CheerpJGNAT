pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.IndexColorModel;
with Java.Awt.Image.ImageConsumer;
with Java.Awt.Image.ImageFilter;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Image.RGBImageFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageConsumer_I : Java.Awt.Image.ImageConsumer.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Awt.Image.ImageFilter.Typ(ImageConsumer_I,
                                                   Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Origmodel : access Java.Awt.Image.ColorModel.Typ'Class;
      pragma Import (Java, Origmodel, "origmodel");

      --  protected
      Newmodel : access Java.Awt.Image.ColorModel.Typ'Class;
      pragma Import (Java, Newmodel, "newmodel");

      --  protected
      CanFilterIndexColorModel : Java.Boolean;
      pragma Import (Java, CanFilterIndexColorModel, "canFilterIndexColorModel");

   end record;

   function New_RGBImageFilter (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetColorModel (This : access Typ;
                            P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class);

   procedure SubstituteColorModel (This : access Typ;
                                   P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                                   P2_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class);

   function FilterIndexColorModel (This : access Typ;
                                   P1_IndexColorModel : access Standard.Java.Awt.Image.IndexColorModel.Typ'Class)
                                   return access Java.Awt.Image.IndexColorModel.Typ'Class;

   procedure FilterRGBPixels (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int_Arr : Java.Int_Arr;
                              P6_Int : Java.Int;
                              P7_Int : Java.Int);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Byte_Arr : Java.Byte_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                        P6_Int_Arr : Java.Int_Arr;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int);

   function FilterRGB (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int)
                       return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RGBImageFilter);
   pragma Export (Java, SetColorModel, "setColorModel");
   pragma Export (Java, SubstituteColorModel, "substituteColorModel");
   pragma Export (Java, FilterIndexColorModel, "filterIndexColorModel");
   pragma Export (Java, FilterRGBPixels, "filterRGBPixels");
   pragma Export (Java, SetPixels, "setPixels");
   pragma Export (Java, FilterRGB, "filterRGB");

end Java.Awt.Image.RGBImageFilter;
pragma Import (Java, Java.Awt.Image.RGBImageFilter, "java.awt.image.RGBImageFilter");
pragma Extensions_Allowed (Off);
