pragma Extensions_Allowed (On);
limited with Java.Awt.Image.DataBuffer;
with Java.Lang.Object;

package Java.Awt.Image.SampleModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Width : Java.Int;
      pragma Import (Java, Width, "width");

      --  protected
      Height : Java.Int;
      pragma Import (Java, Height, "height");

      --  protected
      NumBands : Java.Int;
      pragma Import (Java, NumBands, "numBands");

      --  protected
      DataType : Java.Int;
      pragma Import (Java, DataType, "dataType");

   end record;

   function New_SampleModel (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetWidth (This : access Typ)
                      return Java.Int;

   --  final
   function GetHeight (This : access Typ)
                       return Java.Int;

   --  final
   function GetNumBands (This : access Typ)
                         return Java.Int;

   function GetNumDataElements (This : access Typ)
                                return Java.Int is abstract;

   --  final
   function GetDataType (This : access Typ)
                         return Java.Int;

   function GetTransferType (This : access Typ)
                             return Java.Int;

   function GetPixel (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int_Arr : Java.Int_Arr;
                      P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                      return Java.Int_Arr;

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function GetDataElements (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   procedure SetDataElements (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class) is abstract;

   procedure SetDataElements (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   function GetPixel (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Float_Arr : Java.Float_Arr;
                      P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                      return Java.Float_Arr;

   function GetPixel (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Double_Arr : Java.Double_Arr;
                      P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                      return Java.Double_Arr;

   function GetPixels (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int_Arr : Java.Int_Arr;
                       P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                       return Java.Int_Arr;

   function GetPixels (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Float_Arr : Java.Float_Arr;
                       P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                       return Java.Float_Arr;

   function GetPixels (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Double_Arr : Java.Double_Arr;
                       P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                       return Java.Double_Arr;

   function GetSample (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                       return Java.Int is abstract;

   function GetSampleFloat (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                            return Java.Float;

   function GetSampleDouble (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                             return Java.Double;

   function GetSamples (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Int_Arr : Java.Int_Arr;
                        P7_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                        return Java.Int_Arr;

   function GetSamples (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Float_Arr : Java.Float_Arr;
                        P7_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                        return Java.Float_Arr;

   function GetSamples (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Double_Arr : Java.Double_Arr;
                        P7_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class)
                        return Java.Double_Arr;

   procedure SetPixel (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int_Arr : Java.Int_Arr;
                       P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetPixel (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Float_Arr : Java.Float_Arr;
                       P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetPixel (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Double_Arr : Java.Double_Arr;
                       P4_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int_Arr : Java.Int_Arr;
                        P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Float_Arr : Java.Float_Arr;
                        P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Double_Arr : Java.Double_Arr;
                        P6_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetSample (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class) is abstract;

   procedure SetSample (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Float : Java.Float;
                        P5_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetSample (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Double : Java.Double;
                        P5_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetSamples (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int_Arr : Java.Int_Arr;
                         P7_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetSamples (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Float_Arr : Java.Float_Arr;
                         P7_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   procedure SetSamples (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Double_Arr : Java.Double_Arr;
                         P7_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class);

   function CreateCompatibleSampleModel (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return access Java.Awt.Image.SampleModel.Typ'Class is abstract;

   function CreateSubsetSampleModel (This : access Typ;
                                     P1_Int_Arr : Java.Int_Arr)
                                     return access Java.Awt.Image.SampleModel.Typ'Class is abstract;

   function CreateDataBuffer (This : access Typ)
                              return access Java.Awt.Image.DataBuffer.Typ'Class is abstract;

   function GetSampleSize (This : access Typ)
                           return Java.Int_Arr is abstract;

   function GetSampleSize (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SampleModel);
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, GetNumBands, "getNumBands");
   pragma Export (Java, GetNumDataElements, "getNumDataElements");
   pragma Export (Java, GetDataType, "getDataType");
   pragma Export (Java, GetTransferType, "getTransferType");
   pragma Export (Java, GetPixel, "getPixel");
   pragma Export (Java, GetDataElements, "getDataElements");
   pragma Export (Java, SetDataElements, "setDataElements");
   pragma Export (Java, GetPixels, "getPixels");
   pragma Export (Java, GetSample, "getSample");
   pragma Export (Java, GetSampleFloat, "getSampleFloat");
   pragma Export (Java, GetSampleDouble, "getSampleDouble");
   pragma Export (Java, GetSamples, "getSamples");
   pragma Export (Java, SetPixel, "setPixel");
   pragma Export (Java, SetPixels, "setPixels");
   pragma Export (Java, SetSample, "setSample");
   pragma Export (Java, SetSamples, "setSamples");
   pragma Export (Java, CreateCompatibleSampleModel, "createCompatibleSampleModel");
   pragma Export (Java, CreateSubsetSampleModel, "createSubsetSampleModel");
   pragma Export (Java, CreateDataBuffer, "createDataBuffer");
   pragma Export (Java, GetSampleSize, "getSampleSize");

end Java.Awt.Image.SampleModel;
pragma Import (Java, Java.Awt.Image.SampleModel, "java.awt.image.SampleModel");
pragma Extensions_Allowed (Off);
