pragma Extensions_Allowed (On);
with Java.Awt.Image.LookupTable;
with Java.Lang.Object;

package Java.Awt.Image.ShortLookupTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Image.LookupTable.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ShortLookupTable (P1_Int : Java.Int;
                                  P2_Short_Arr_2 : Java.Short_Arr_2; 
                                  This : Ref := null)
                                  return Ref;

   function New_ShortLookupTable (P1_Int : Java.Int;
                                  P2_Short_Arr : Java.Short_Arr; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetTable (This : access Typ)
                      return Java.Short_Arr_2;

   function LookupPixel (This : access Typ;
                         P1_Int_Arr : Java.Int_Arr;
                         P2_Int_Arr : Java.Int_Arr)
                         return Java.Int_Arr;

   function LookupPixel (This : access Typ;
                         P1_Short_Arr : Java.Short_Arr;
                         P2_Short_Arr : Java.Short_Arr)
                         return Java.Short_Arr;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ShortLookupTable);
   pragma Import (Java, GetTable, "getTable");
   pragma Import (Java, LookupPixel, "lookupPixel");

end Java.Awt.Image.ShortLookupTable;
pragma Import (Java, Java.Awt.Image.ShortLookupTable, "java.awt.image.ShortLookupTable");
pragma Extensions_Allowed (Off);
