pragma Extensions_Allowed (On);
limited with Java.Awt.Image.WritableRenderedImage;
with Java.Lang.Object;

package Java.Awt.Image.TileObserver is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure TileUpdate (This : access Typ;
                         P1_WritableRenderedImage : access Standard.Java.Awt.Image.WritableRenderedImage.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, TileUpdate, "tileUpdate");

end Java.Awt.Image.TileObserver;
pragma Import (Java, Java.Awt.Image.TileObserver, "java.awt.image.TileObserver");
pragma Extensions_Allowed (Off);
