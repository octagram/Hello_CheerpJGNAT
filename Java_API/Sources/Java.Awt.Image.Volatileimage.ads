pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Awt.ImageCapabilities;
with Java.Awt.Image;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Image.VolatileImage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transparency_I : Java.Awt.Transparency.Ref)
    is abstract new Java.Awt.Image.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Transparency : Java.Int;
      pragma Import (Java, Transparency, "transparency");

   end record;

   function New_VolatileImage (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSnapshot (This : access Typ)
                         return access Java.Awt.Image.BufferedImage.Typ'Class is abstract;

   function GetWidth (This : access Typ)
                      return Java.Int is abstract;

   function GetHeight (This : access Typ)
                       return Java.Int is abstract;

   function GetSource (This : access Typ)
                       return access Java.Awt.Image.ImageProducer.Typ'Class;

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class;

   function CreateGraphics (This : access Typ)
                            return access Java.Awt.Graphics2D.Typ'Class is abstract;

   function Validate (This : access Typ;
                      P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class)
                      return Java.Int is abstract;

   function ContentsLost (This : access Typ)
                          return Java.Boolean is abstract;

   function GetCapabilities (This : access Typ)
                             return access Java.Awt.ImageCapabilities.Typ'Class is abstract;

   function GetTransparency (This : access Typ)
                             return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   IMAGE_OK : constant Java.Int;

   --  final
   IMAGE_RESTORED : constant Java.Int;

   --  final
   IMAGE_INCOMPATIBLE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_VolatileImage);
   pragma Export (Java, GetSnapshot, "getSnapshot");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, GetSource, "getSource");
   pragma Export (Java, GetGraphics, "getGraphics");
   pragma Export (Java, CreateGraphics, "createGraphics");
   pragma Export (Java, Validate, "validate");
   pragma Export (Java, ContentsLost, "contentsLost");
   pragma Export (Java, GetCapabilities, "getCapabilities");
   pragma Export (Java, GetTransparency, "getTransparency");
   pragma Import (Java, IMAGE_OK, "IMAGE_OK");
   pragma Import (Java, IMAGE_RESTORED, "IMAGE_RESTORED");
   pragma Import (Java, IMAGE_INCOMPATIBLE, "IMAGE_INCOMPATIBLE");

end Java.Awt.Image.VolatileImage;
pragma Import (Java, Java.Awt.Image.VolatileImage, "java.awt.image.VolatileImage");
pragma Extensions_Allowed (Off);
