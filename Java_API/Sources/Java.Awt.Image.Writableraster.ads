pragma Extensions_Allowed (On);
limited with Java.Awt.Image.DataBuffer;
limited with Java.Awt.Image.SampleModel;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
with Java.Awt.Image.Raster;
with Java.Lang.Object;

package Java.Awt.Image.WritableRaster is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Image.Raster.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_WritableRaster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                                P2_Point : access Standard.Java.Awt.Point.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   --  protected
   function New_WritableRaster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                                P2_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                                P3_Point : access Standard.Java.Awt.Point.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   --  protected
   function New_WritableRaster (P1_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class;
                                P2_DataBuffer : access Standard.Java.Awt.Image.DataBuffer.Typ'Class;
                                P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                P4_Point : access Standard.Java.Awt.Point.Typ'Class;
                                P5_WritableRaster : access Standard.Java.Awt.Image.WritableRaster.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetWritableParent (This : access Typ)
                               return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateWritableTranslatedChild (This : access Typ;
                                           P1_Int : Java.Int;
                                           P2_Int : Java.Int)
                                           return access Java.Awt.Image.WritableRaster.Typ'Class;

   function CreateWritableChild (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int_Arr : Java.Int_Arr)
                                 return access Java.Awt.Image.WritableRaster.Typ'Class;

   procedure SetDataElements (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure SetDataElements (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class);

   procedure SetDataElements (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure SetRect (This : access Typ;
                      P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class);

   procedure SetRect (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class);

   procedure SetPixel (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int_Arr : Java.Int_Arr);

   procedure SetPixel (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Float_Arr : Java.Float_Arr);

   procedure SetPixel (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Double_Arr : Java.Double_Arr);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int_Arr : Java.Int_Arr);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Float_Arr : Java.Float_Arr);

   procedure SetPixels (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Double_Arr : Java.Double_Arr);

   procedure SetSample (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure SetSample (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Float : Java.Float);

   procedure SetSample (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Double : Java.Double);

   procedure SetSamples (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int_Arr : Java.Int_Arr);

   procedure SetSamples (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Float_Arr : Java.Float_Arr);

   procedure SetSamples (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Double_Arr : Java.Double_Arr);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_WritableRaster);
   pragma Import (Java, GetWritableParent, "getWritableParent");
   pragma Import (Java, CreateWritableTranslatedChild, "createWritableTranslatedChild");
   pragma Import (Java, CreateWritableChild, "createWritableChild");
   pragma Import (Java, SetDataElements, "setDataElements");
   pragma Import (Java, SetRect, "setRect");
   pragma Import (Java, SetPixel, "setPixel");
   pragma Import (Java, SetPixels, "setPixels");
   pragma Import (Java, SetSample, "setSample");
   pragma Import (Java, SetSamples, "setSamples");

end Java.Awt.Image.WritableRaster;
pragma Import (Java, Java.Awt.Image.WritableRaster, "java.awt.image.WritableRaster");
pragma Extensions_Allowed (Off);
