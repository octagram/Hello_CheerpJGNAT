pragma Extensions_Allowed (On);
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.TileObserver;
limited with Java.Awt.Image.WritableRaster;
limited with Java.Awt.Point;
with Java.Awt.Image.RenderedImage;
with Java.Lang.Object;

package Java.Awt.Image.WritableRenderedImage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RenderedImage_I : Java.Awt.Image.RenderedImage.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddTileObserver (This : access Typ;
                              P1_TileObserver : access Standard.Java.Awt.Image.TileObserver.Typ'Class) is abstract;

   procedure RemoveTileObserver (This : access Typ;
                                 P1_TileObserver : access Standard.Java.Awt.Image.TileObserver.Typ'Class) is abstract;

   function GetWritableTile (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Java.Awt.Image.WritableRaster.Typ'Class is abstract;

   procedure ReleaseWritableTile (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int) is abstract;

   function IsTileWritable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Boolean is abstract;

   function GetWritableTileIndices (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function HasTileWriters (This : access Typ)
                            return Java.Boolean is abstract;

   procedure SetData (This : access Typ;
                      P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddTileObserver, "addTileObserver");
   pragma Export (Java, RemoveTileObserver, "removeTileObserver");
   pragma Export (Java, GetWritableTile, "getWritableTile");
   pragma Export (Java, ReleaseWritableTile, "releaseWritableTile");
   pragma Export (Java, IsTileWritable, "isTileWritable");
   pragma Export (Java, GetWritableTileIndices, "getWritableTileIndices");
   pragma Export (Java, HasTileWriters, "hasTileWriters");
   pragma Export (Java, SetData, "setData");

end Java.Awt.Image.WritableRenderedImage;
pragma Import (Java, Java.Awt.Image.WritableRenderedImage, "java.awt.image.WritableRenderedImage");
pragma Extensions_Allowed (Off);
