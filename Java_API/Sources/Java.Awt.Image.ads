pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Awt.ImageCapabilities;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.Image is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      AccelerationPriority : Java.Float;
      pragma Import (Java, AccelerationPriority, "accelerationPriority");

   end record;

   function New_Image (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetWidth (This : access Typ;
                      P1_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                      return Java.Int is abstract;

   function GetHeight (This : access Typ;
                       P1_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Int is abstract;

   function GetSource (This : access Typ)
                       return access Java.Awt.Image.ImageProducer.Typ'Class is abstract;

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class is abstract;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function GetScaledInstance (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int)
                               return access Java.Awt.Image.Typ'Class;

   procedure Flush (This : access Typ);

   function GetCapabilities (This : access Typ;
                             P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class)
                             return access Java.Awt.ImageCapabilities.Typ'Class;

   procedure SetAccelerationPriority (This : access Typ;
                                      P1_Float : Java.Float);

   function GetAccelerationPriority (This : access Typ)
                                     return Java.Float;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UndefinedProperty : access Java.Lang.Object.Typ'Class;

   --  final
   SCALE_DEFAULT : constant Java.Int;

   --  final
   SCALE_FAST : constant Java.Int;

   --  final
   SCALE_SMOOTH : constant Java.Int;

   --  final
   SCALE_REPLICATE : constant Java.Int;

   --  final
   SCALE_AREA_AVERAGING : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Image);
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, GetSource, "getSource");
   pragma Export (Java, GetGraphics, "getGraphics");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Import (Java, GetScaledInstance, "getScaledInstance");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, GetCapabilities, "getCapabilities");
   pragma Import (Java, SetAccelerationPriority, "setAccelerationPriority");
   pragma Import (Java, GetAccelerationPriority, "getAccelerationPriority");
   pragma Import (Java, UndefinedProperty, "UndefinedProperty");
   pragma Import (Java, SCALE_DEFAULT, "SCALE_DEFAULT");
   pragma Import (Java, SCALE_FAST, "SCALE_FAST");
   pragma Import (Java, SCALE_SMOOTH, "SCALE_SMOOTH");
   pragma Import (Java, SCALE_REPLICATE, "SCALE_REPLICATE");
   pragma Import (Java, SCALE_AREA_AVERAGING, "SCALE_AREA_AVERAGING");

end Java.Awt.Image;
pragma Import (Java, Java.Awt.Image, "java.awt.Image");
pragma Extensions_Allowed (Off);
