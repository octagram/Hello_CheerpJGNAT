pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.ImageCapabilities is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImageCapabilities (P1_Boolean : Java.Boolean; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsAccelerated (This : access Typ)
                           return Java.Boolean;

   function IsTrueVolatile (This : access Typ)
                            return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageCapabilities);
   pragma Import (Java, IsAccelerated, "isAccelerated");
   pragma Import (Java, IsTrueVolatile, "isTrueVolatile");
   pragma Import (Java, Clone, "clone");

end Java.Awt.ImageCapabilities;
pragma Import (Java, Java.Awt.ImageCapabilities, "java.awt.ImageCapabilities");
pragma Extensions_Allowed (Off);
