pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.JobAttributes.DialogType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   COMMON : access Java.Awt.JobAttributes.DialogType.Typ'Class;

   --  final
   NATIVE : access Java.Awt.JobAttributes.DialogType.Typ'Class;

   --  final
   NONE : access Java.Awt.JobAttributes.DialogType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, COMMON, "COMMON");
   pragma Import (Java, NATIVE, "NATIVE");
   pragma Import (Java, NONE, "NONE");

end Java.Awt.JobAttributes.DialogType;
pragma Import (Java, Java.Awt.JobAttributes.DialogType, "java.awt.JobAttributes$DialogType");
pragma Extensions_Allowed (Off);
