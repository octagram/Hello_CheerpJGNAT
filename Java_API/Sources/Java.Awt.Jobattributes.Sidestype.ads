pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.JobAttributes.SidesType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ONE_SIDED : access Java.Awt.JobAttributes.SidesType.Typ'Class;

   --  final
   TWO_SIDED_LONG_EDGE : access Java.Awt.JobAttributes.SidesType.Typ'Class;

   --  final
   TWO_SIDED_SHORT_EDGE : access Java.Awt.JobAttributes.SidesType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ONE_SIDED, "ONE_SIDED");
   pragma Import (Java, TWO_SIDED_LONG_EDGE, "TWO_SIDED_LONG_EDGE");
   pragma Import (Java, TWO_SIDED_SHORT_EDGE, "TWO_SIDED_SHORT_EDGE");

end Java.Awt.JobAttributes.SidesType;
pragma Import (Java, Java.Awt.JobAttributes.SidesType, "java.awt.JobAttributes$SidesType");
pragma Extensions_Allowed (Off);
