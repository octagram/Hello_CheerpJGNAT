pragma Extensions_Allowed (On);
limited with Java.Awt.JobAttributes.DefaultSelectionType;
limited with Java.Awt.JobAttributes.DestinationType;
limited with Java.Awt.JobAttributes.DialogType;
limited with Java.Awt.JobAttributes.MultipleDocumentHandlingType;
limited with Java.Awt.JobAttributes.SidesType;
limited with Java.Lang.String;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.JobAttributes is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JobAttributes (This : Ref := null)
                               return Ref;

   function New_JobAttributes (P1_JobAttributes : access Standard.Java.Awt.JobAttributes.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_JobAttributes (P1_Int : Java.Int;
                               P2_DefaultSelectionType : access Standard.Java.Awt.JobAttributes.DefaultSelectionType.Typ'Class;
                               P3_DestinationType : access Standard.Java.Awt.JobAttributes.DestinationType.Typ'Class;
                               P4_DialogType : access Standard.Java.Awt.JobAttributes.DialogType.Typ'Class;
                               P5_String : access Standard.Java.Lang.String.Typ'Class;
                               P6_Int : Java.Int;
                               P7_Int : Java.Int;
                               P8_MultipleDocumentHandlingType : access Standard.Java.Awt.JobAttributes.MultipleDocumentHandlingType.Typ'Class;
                               P9_Int_Arr_2 : Java.Int_Arr_2;
                               P10_String : access Standard.Java.Lang.String.Typ'Class;
                               P11_SidesType : access Standard.Java.Awt.JobAttributes.SidesType.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   procedure Set (This : access Typ;
                  P1_JobAttributes : access Standard.Java.Awt.JobAttributes.Typ'Class);

   function GetCopies (This : access Typ)
                       return Java.Int;

   procedure SetCopies (This : access Typ;
                        P1_Int : Java.Int);

   procedure SetCopiesToDefault (This : access Typ);

   function GetDefaultSelection (This : access Typ)
                                 return access Java.Awt.JobAttributes.DefaultSelectionType.Typ'Class;

   procedure SetDefaultSelection (This : access Typ;
                                  P1_DefaultSelectionType : access Standard.Java.Awt.JobAttributes.DefaultSelectionType.Typ'Class);

   function GetDestination (This : access Typ)
                            return access Java.Awt.JobAttributes.DestinationType.Typ'Class;

   procedure SetDestination (This : access Typ;
                             P1_DestinationType : access Standard.Java.Awt.JobAttributes.DestinationType.Typ'Class);

   function GetDialog (This : access Typ)
                       return access Java.Awt.JobAttributes.DialogType.Typ'Class;

   procedure SetDialog (This : access Typ;
                        P1_DialogType : access Standard.Java.Awt.JobAttributes.DialogType.Typ'Class);

   function GetFileName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetFileName (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetFromPage (This : access Typ)
                         return Java.Int;

   procedure SetFromPage (This : access Typ;
                          P1_Int : Java.Int);

   function GetMaxPage (This : access Typ)
                        return Java.Int;

   procedure SetMaxPage (This : access Typ;
                         P1_Int : Java.Int);

   function GetMinPage (This : access Typ)
                        return Java.Int;

   procedure SetMinPage (This : access Typ;
                         P1_Int : Java.Int);

   function GetMultipleDocumentHandling (This : access Typ)
                                         return access Java.Awt.JobAttributes.MultipleDocumentHandlingType.Typ'Class;

   procedure SetMultipleDocumentHandling (This : access Typ;
                                          P1_MultipleDocumentHandlingType : access Standard.Java.Awt.JobAttributes.MultipleDocumentHandlingType.Typ'Class);

   procedure SetMultipleDocumentHandlingToDefault (This : access Typ);

   function GetPageRanges (This : access Typ)
                           return Java.Int_Arr_2;

   procedure SetPageRanges (This : access Typ;
                            P1_Int_Arr_2 : Java.Int_Arr_2);

   function GetPrinter (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetPrinter (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSides (This : access Typ)
                      return access Java.Awt.JobAttributes.SidesType.Typ'Class;

   procedure SetSides (This : access Typ;
                       P1_SidesType : access Standard.Java.Awt.JobAttributes.SidesType.Typ'Class);

   procedure SetSidesToDefault (This : access Typ);

   function GetToPage (This : access Typ)
                       return Java.Int;

   procedure SetToPage (This : access Typ;
                        P1_Int : Java.Int);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JobAttributes);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Set, "set");
   pragma Import (Java, GetCopies, "getCopies");
   pragma Import (Java, SetCopies, "setCopies");
   pragma Import (Java, SetCopiesToDefault, "setCopiesToDefault");
   pragma Import (Java, GetDefaultSelection, "getDefaultSelection");
   pragma Import (Java, SetDefaultSelection, "setDefaultSelection");
   pragma Import (Java, GetDestination, "getDestination");
   pragma Import (Java, SetDestination, "setDestination");
   pragma Import (Java, GetDialog, "getDialog");
   pragma Import (Java, SetDialog, "setDialog");
   pragma Import (Java, GetFileName, "getFileName");
   pragma Import (Java, SetFileName, "setFileName");
   pragma Import (Java, GetFromPage, "getFromPage");
   pragma Import (Java, SetFromPage, "setFromPage");
   pragma Import (Java, GetMaxPage, "getMaxPage");
   pragma Import (Java, SetMaxPage, "setMaxPage");
   pragma Import (Java, GetMinPage, "getMinPage");
   pragma Import (Java, SetMinPage, "setMinPage");
   pragma Import (Java, GetMultipleDocumentHandling, "getMultipleDocumentHandling");
   pragma Import (Java, SetMultipleDocumentHandling, "setMultipleDocumentHandling");
   pragma Import (Java, SetMultipleDocumentHandlingToDefault, "setMultipleDocumentHandlingToDefault");
   pragma Import (Java, GetPageRanges, "getPageRanges");
   pragma Import (Java, SetPageRanges, "setPageRanges");
   pragma Import (Java, GetPrinter, "getPrinter");
   pragma Import (Java, SetPrinter, "setPrinter");
   pragma Import (Java, GetSides, "getSides");
   pragma Import (Java, SetSides, "setSides");
   pragma Import (Java, SetSidesToDefault, "setSidesToDefault");
   pragma Import (Java, GetToPage, "getToPage");
   pragma Import (Java, SetToPage, "setToPage");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Awt.JobAttributes;
pragma Import (Java, Java.Awt.JobAttributes, "java.awt.JobAttributes");
pragma Extensions_Allowed (Off);
