pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.FocusTraversalPolicy;
limited with Java.Awt.Window;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Beans.VetoableChangeListener;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Set;
with Java.Awt.KeyEventDispatcher;
with Java.Awt.KeyEventPostProcessor;
with Java.Lang.Object;

package Java.Awt.KeyboardFocusManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeyEventDispatcher_I : Java.Awt.KeyEventDispatcher.Ref;
            KeyEventPostProcessor_I : Java.Awt.KeyEventPostProcessor.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCurrentKeyboardFocusManager return access Java.Awt.KeyboardFocusManager.Typ'Class;

   procedure SetCurrentKeyboardFocusManager (P1_KeyboardFocusManager : access Standard.Java.Awt.KeyboardFocusManager.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   function New_KeyboardFocusManager (This : Ref := null)
                                      return Ref;

   function GetFocusOwner (This : access Typ)
                           return access Java.Awt.Component.Typ'Class;

   --  protected
   function GetGlobalFocusOwner (This : access Typ)
                                 return access Java.Awt.Component.Typ'Class;
   --  can raise Java.Lang.SecurityException.Except

   --  protected
   procedure SetGlobalFocusOwner (This : access Typ;
                                  P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure ClearGlobalFocusOwner (This : access Typ);

   function GetPermanentFocusOwner (This : access Typ)
                                    return access Java.Awt.Component.Typ'Class;

   --  protected
   function GetGlobalPermanentFocusOwner (This : access Typ)
                                          return access Java.Awt.Component.Typ'Class;
   --  can raise Java.Lang.SecurityException.Except

   --  protected
   procedure SetGlobalPermanentFocusOwner (This : access Typ;
                                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetFocusedWindow (This : access Typ)
                              return access Java.Awt.Window.Typ'Class;

   --  protected
   function GetGlobalFocusedWindow (This : access Typ)
                                    return access Java.Awt.Window.Typ'Class;
   --  can raise Java.Lang.SecurityException.Except

   --  protected
   procedure SetGlobalFocusedWindow (This : access Typ;
                                     P1_Window : access Standard.Java.Awt.Window.Typ'Class);

   function GetActiveWindow (This : access Typ)
                             return access Java.Awt.Window.Typ'Class;

   --  protected
   function GetGlobalActiveWindow (This : access Typ)
                                   return access Java.Awt.Window.Typ'Class;
   --  can raise Java.Lang.SecurityException.Except

   --  protected
   procedure SetGlobalActiveWindow (This : access Typ;
                                    P1_Window : access Standard.Java.Awt.Window.Typ'Class);

   --  synchronized
   function GetDefaultFocusTraversalPolicy (This : access Typ)
                                            return access Java.Awt.FocusTraversalPolicy.Typ'Class;

   procedure SetDefaultFocusTraversalPolicy (This : access Typ;
                                             P1_FocusTraversalPolicy : access Standard.Java.Awt.FocusTraversalPolicy.Typ'Class);

   procedure SetDefaultFocusTraversalKeys (This : access Typ;
                                           P1_Int : Java.Int;
                                           P2_Set : access Standard.Java.Util.Set.Typ'Class);

   function GetDefaultFocusTraversalKeys (This : access Typ;
                                          P1_Int : Java.Int)
                                          return access Java.Util.Set.Typ'Class;

   function GetCurrentFocusCycleRoot (This : access Typ)
                                      return access Java.Awt.Container.Typ'Class;

   --  protected
   function GetGlobalCurrentFocusCycleRoot (This : access Typ)
                                            return access Java.Awt.Container.Typ'Class;
   --  can raise Java.Lang.SecurityException.Except

   procedure SetGlobalCurrentFocusCycleRoot (This : access Typ;
                                             P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddVetoableChangeListener (This : access Typ;
                                        P1_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   procedure RemoveVetoableChangeListener (This : access Typ;
                                           P1_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   function GetVetoableChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   procedure AddVetoableChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   procedure RemoveVetoableChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   function GetVetoableChangeListeners (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireVetoableChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   procedure AddKeyEventDispatcher (This : access Typ;
                                    P1_KeyEventDispatcher : access Standard.Java.Awt.KeyEventDispatcher.Typ'Class);

   procedure RemoveKeyEventDispatcher (This : access Typ;
                                       P1_KeyEventDispatcher : access Standard.Java.Awt.KeyEventDispatcher.Typ'Class);

   --  protected  synchronized
   function GetKeyEventDispatchers (This : access Typ)
                                    return access Java.Util.List.Typ'Class;

   procedure AddKeyEventPostProcessor (This : access Typ;
                                       P1_KeyEventPostProcessor : access Standard.Java.Awt.KeyEventPostProcessor.Typ'Class);

   procedure RemoveKeyEventPostProcessor (This : access Typ;
                                          P1_KeyEventPostProcessor : access Standard.Java.Awt.KeyEventPostProcessor.Typ'Class);

   --  protected
   function GetKeyEventPostProcessors (This : access Typ)
                                       return access Java.Util.List.Typ'Class;

   function DispatchEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class)
                           return Java.Boolean is abstract;

   --  final
   procedure RedispatchEvent (This : access Typ;
                              P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                              P2_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   function DispatchKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class)
                              return Java.Boolean is abstract;

   function PostProcessKeyEvent (This : access Typ;
                                 P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class)
                                 return Java.Boolean is abstract;

   procedure ProcessKeyEvent (This : access Typ;
                              P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                              P2_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class) is abstract;

   --  protected
   procedure EnqueueKeyEvents (This : access Typ;
                               P1_Long : Java.Long;
                               P2_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   --  protected
   procedure DequeueKeyEvents (This : access Typ;
                               P1_Long : Java.Long;
                               P2_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   --  protected
   procedure DiscardKeyEvents (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   procedure FocusNextComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   procedure FocusPreviousComponent (This : access Typ;
                                     P1_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   procedure UpFocusCycle (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   procedure DownFocusCycle (This : access Typ;
                             P1_Container : access Standard.Java.Awt.Container.Typ'Class) is abstract;

   --  final
   procedure FocusNextComponent (This : access Typ);

   --  final
   procedure FocusPreviousComponent (This : access Typ);

   --  final
   procedure UpFocusCycle (This : access Typ);

   --  final
   procedure DownFocusCycle (This : access Typ);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FORWARD_TRAVERSAL_KEYS : constant Java.Int;

   --  final
   BACKWARD_TRAVERSAL_KEYS : constant Java.Int;

   --  final
   UP_CYCLE_TRAVERSAL_KEYS : constant Java.Int;

   --  final
   DOWN_CYCLE_TRAVERSAL_KEYS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCurrentKeyboardFocusManager, "getCurrentKeyboardFocusManager");
   pragma Export (Java, SetCurrentKeyboardFocusManager, "setCurrentKeyboardFocusManager");
   pragma Java_Constructor (New_KeyboardFocusManager);
   pragma Export (Java, GetFocusOwner, "getFocusOwner");
   pragma Export (Java, GetGlobalFocusOwner, "getGlobalFocusOwner");
   pragma Export (Java, SetGlobalFocusOwner, "setGlobalFocusOwner");
   pragma Export (Java, ClearGlobalFocusOwner, "clearGlobalFocusOwner");
   pragma Export (Java, GetPermanentFocusOwner, "getPermanentFocusOwner");
   pragma Export (Java, GetGlobalPermanentFocusOwner, "getGlobalPermanentFocusOwner");
   pragma Export (Java, SetGlobalPermanentFocusOwner, "setGlobalPermanentFocusOwner");
   pragma Export (Java, GetFocusedWindow, "getFocusedWindow");
   pragma Export (Java, GetGlobalFocusedWindow, "getGlobalFocusedWindow");
   pragma Export (Java, SetGlobalFocusedWindow, "setGlobalFocusedWindow");
   pragma Export (Java, GetActiveWindow, "getActiveWindow");
   pragma Export (Java, GetGlobalActiveWindow, "getGlobalActiveWindow");
   pragma Export (Java, SetGlobalActiveWindow, "setGlobalActiveWindow");
   pragma Export (Java, GetDefaultFocusTraversalPolicy, "getDefaultFocusTraversalPolicy");
   pragma Export (Java, SetDefaultFocusTraversalPolicy, "setDefaultFocusTraversalPolicy");
   pragma Export (Java, SetDefaultFocusTraversalKeys, "setDefaultFocusTraversalKeys");
   pragma Export (Java, GetDefaultFocusTraversalKeys, "getDefaultFocusTraversalKeys");
   pragma Export (Java, GetCurrentFocusCycleRoot, "getCurrentFocusCycleRoot");
   pragma Export (Java, GetGlobalCurrentFocusCycleRoot, "getGlobalCurrentFocusCycleRoot");
   pragma Export (Java, SetGlobalCurrentFocusCycleRoot, "setGlobalCurrentFocusCycleRoot");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Export (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Export (Java, FirePropertyChange, "firePropertyChange");
   pragma Export (Java, AddVetoableChangeListener, "addVetoableChangeListener");
   pragma Export (Java, RemoveVetoableChangeListener, "removeVetoableChangeListener");
   pragma Export (Java, GetVetoableChangeListeners, "getVetoableChangeListeners");
   pragma Export (Java, FireVetoableChange, "fireVetoableChange");
   pragma Export (Java, AddKeyEventDispatcher, "addKeyEventDispatcher");
   pragma Export (Java, RemoveKeyEventDispatcher, "removeKeyEventDispatcher");
   pragma Export (Java, GetKeyEventDispatchers, "getKeyEventDispatchers");
   pragma Export (Java, AddKeyEventPostProcessor, "addKeyEventPostProcessor");
   pragma Export (Java, RemoveKeyEventPostProcessor, "removeKeyEventPostProcessor");
   pragma Export (Java, GetKeyEventPostProcessors, "getKeyEventPostProcessors");
   pragma Export (Java, DispatchEvent, "dispatchEvent");
   pragma Export (Java, RedispatchEvent, "redispatchEvent");
   pragma Export (Java, DispatchKeyEvent, "dispatchKeyEvent");
   pragma Export (Java, PostProcessKeyEvent, "postProcessKeyEvent");
   pragma Export (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Export (Java, EnqueueKeyEvents, "enqueueKeyEvents");
   pragma Export (Java, DequeueKeyEvents, "dequeueKeyEvents");
   pragma Export (Java, DiscardKeyEvents, "discardKeyEvents");
   pragma Export (Java, FocusNextComponent, "focusNextComponent");
   pragma Export (Java, FocusPreviousComponent, "focusPreviousComponent");
   pragma Export (Java, UpFocusCycle, "upFocusCycle");
   pragma Export (Java, DownFocusCycle, "downFocusCycle");
   pragma Import (Java, FORWARD_TRAVERSAL_KEYS, "FORWARD_TRAVERSAL_KEYS");
   pragma Import (Java, BACKWARD_TRAVERSAL_KEYS, "BACKWARD_TRAVERSAL_KEYS");
   pragma Import (Java, UP_CYCLE_TRAVERSAL_KEYS, "UP_CYCLE_TRAVERSAL_KEYS");
   pragma Import (Java, DOWN_CYCLE_TRAVERSAL_KEYS, "DOWN_CYCLE_TRAVERSAL_KEYS");

end Java.Awt.KeyboardFocusManager;
pragma Import (Java, Java.Awt.KeyboardFocusManager, "java.awt.KeyboardFocusManager");
pragma Extensions_Allowed (Off);
