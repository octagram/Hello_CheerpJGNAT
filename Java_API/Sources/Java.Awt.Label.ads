pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Label is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Label (This : Ref := null)
                       return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Label (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Label (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetAlignment (This : access Typ)
                          return Java.Int;

   --  synchronized
   procedure SetAlignment (This : access Typ;
                           P1_Int : Java.Int);

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LEFT : constant Java.Int;

   --  final
   CENTER : constant Java.Int;

   --  final
   RIGHT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Label);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, SetAlignment, "setAlignment");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, LEFT, "LEFT");
   pragma Import (Java, CENTER, "CENTER");
   pragma Import (Java, RIGHT, "RIGHT");

end Java.Awt.Label;
pragma Import (Java, Java.Awt.Label, "java.awt.Label");
pragma Extensions_Allowed (Off);
