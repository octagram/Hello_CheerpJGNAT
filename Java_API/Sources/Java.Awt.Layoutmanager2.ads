pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
with Java.Awt.LayoutManager;
with Java.Lang.Object;

package Java.Awt.LayoutManager2 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            LayoutManager_I : Java.Awt.LayoutManager.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddLayoutComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function MaximumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class is abstract;

   function GetLayoutAlignmentX (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float is abstract;

   function GetLayoutAlignmentY (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float is abstract;

   procedure InvalidateLayout (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Export (Java, MaximumLayoutSize, "maximumLayoutSize");
   pragma Export (Java, GetLayoutAlignmentX, "getLayoutAlignmentX");
   pragma Export (Java, GetLayoutAlignmentY, "getLayoutAlignmentY");
   pragma Export (Java, InvalidateLayout, "invalidateLayout");

end Java.Awt.LayoutManager2;
pragma Import (Java, Java.Awt.LayoutManager2, "java.awt.LayoutManager2");
pragma Extensions_Allowed (Off);
