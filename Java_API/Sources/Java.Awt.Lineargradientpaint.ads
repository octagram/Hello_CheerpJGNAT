pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.MultipleGradientPaint.ColorSpaceType;
limited with Java.Awt.MultipleGradientPaint.CycleMethod;
limited with Java.Awt.PaintContext;
limited with Java.Awt.Rectangle;
limited with Java.Awt.RenderingHints;
with Java.Awt.MultipleGradientPaint;
with Java.Awt.Paint;
with Java.Lang.Object;

package Java.Awt.LinearGradientPaint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Paint_I : Java.Awt.Paint.Ref)
    is new Java.Awt.MultipleGradientPaint.Typ(Paint_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinearGradientPaint (P1_Float : Java.Float;
                                     P2_Float : Java.Float;
                                     P3_Float : Java.Float;
                                     P4_Float : Java.Float;
                                     P5_Float_Arr : Java.Float_Arr;
                                     P6_Color_Arr : access Java.Awt.Color.Arr_Obj; 
                                     This : Ref := null)
                                     return Ref;

   function New_LinearGradientPaint (P1_Float : Java.Float;
                                     P2_Float : Java.Float;
                                     P3_Float : Java.Float;
                                     P4_Float : Java.Float;
                                     P5_Float_Arr : Java.Float_Arr;
                                     P6_Color_Arr : access Java.Awt.Color.Arr_Obj;
                                     P7_CycleMethod : access Standard.Java.Awt.MultipleGradientPaint.CycleMethod.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_LinearGradientPaint (P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                     P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                     P3_Float_Arr : Java.Float_Arr;
                                     P4_Color_Arr : access Java.Awt.Color.Arr_Obj; 
                                     This : Ref := null)
                                     return Ref;

   function New_LinearGradientPaint (P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                     P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                     P3_Float_Arr : Java.Float_Arr;
                                     P4_Color_Arr : access Java.Awt.Color.Arr_Obj;
                                     P5_CycleMethod : access Standard.Java.Awt.MultipleGradientPaint.CycleMethod.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_LinearGradientPaint (P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                     P2_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class;
                                     P3_Float_Arr : Java.Float_Arr;
                                     P4_Color_Arr : access Java.Awt.Color.Arr_Obj;
                                     P5_CycleMethod : access Standard.Java.Awt.MultipleGradientPaint.CycleMethod.Typ'Class;
                                     P6_ColorSpaceType : access Standard.Java.Awt.MultipleGradientPaint.ColorSpaceType.Typ'Class;
                                     P7_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                           P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                           P4_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                           P5_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.PaintContext.Typ'Class;

   function GetStartPoint (This : access Typ)
                           return access Java.Awt.Geom.Point2D.Typ'Class;

   function GetEndPoint (This : access Typ)
                         return access Java.Awt.Geom.Point2D.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LinearGradientPaint);
   pragma Import (Java, CreateContext, "createContext");
   pragma Import (Java, GetStartPoint, "getStartPoint");
   pragma Import (Java, GetEndPoint, "getEndPoint");

end Java.Awt.LinearGradientPaint;
pragma Import (Java, Java.Awt.LinearGradientPaint, "java.awt.LinearGradientPaint");
pragma Extensions_Allowed (Off);
