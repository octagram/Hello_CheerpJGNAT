pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Event.ItemEvent;
limited with Java.Awt.Event.ItemListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.List is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_List (This : Ref := null)
                      return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_List (P1_Int : Java.Int; 
                      This : Ref := null)
                      return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_List (P1_Int : Java.Int;
                      P2_Boolean : Java.Boolean; 
                      This : Ref := null)
                      return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   function GetItemCount (This : access Typ)
                          return Java.Int;

   function GetItem (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetItems (This : access Typ)
                      return Standard.Java.Lang.Object.Ref;

   procedure Add (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Add (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Int : Java.Int);

   --  synchronized
   procedure ReplaceItem (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int);

   procedure RemoveAll (This : access Typ);

   --  synchronized
   procedure Remove (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   --  synchronized
   function GetSelectedIndex (This : access Typ)
                              return Java.Int;

   --  synchronized
   function GetSelectedIndexes (This : access Typ)
                                return Java.Int_Arr;

   --  synchronized
   function GetSelectedItem (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetSelectedItems (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetSelectedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   procedure select_K (This : access Typ;
                       P1_Int : Java.Int);

   --  synchronized
   procedure Deselect (This : access Typ;
                       P1_Int : Java.Int);

   function IsIndexSelected (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean;

   function GetRows (This : access Typ)
                     return Java.Int;

   function IsMultipleMode (This : access Typ)
                            return Java.Boolean;

   procedure SetMultipleMode (This : access Typ;
                              P1_Boolean : Java.Boolean);

   function GetVisibleIndex (This : access Typ)
                             return Java.Int;

   --  synchronized
   procedure MakeVisible (This : access Typ;
                          P1_Int : Java.Int);

   function GetPreferredSize (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   --  synchronized
   procedure AddItemListener (This : access Typ;
                              P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   --  synchronized
   procedure RemoveItemListener (This : access Typ;
                                 P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   --  synchronized
   function GetItemListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessItemEvent (This : access Typ;
                               P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);

   --  protected
   procedure ProcessActionEvent (This : access Typ;
                                 P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_List);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, GetItemCount, "getItemCount");
   pragma Import (Java, GetItem, "getItem");
   pragma Import (Java, GetItems, "getItems");
   pragma Import (Java, Add, "add");
   pragma Import (Java, ReplaceItem, "replaceItem");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Import (Java, GetSelectedIndexes, "getSelectedIndexes");
   pragma Import (Java, GetSelectedItem, "getSelectedItem");
   pragma Import (Java, GetSelectedItems, "getSelectedItems");
   pragma Import (Java, GetSelectedObjects, "getSelectedObjects");
   pragma Import (Java, select_K, "select");
   pragma Import (Java, Deselect, "deselect");
   pragma Import (Java, IsIndexSelected, "isIndexSelected");
   pragma Import (Java, GetRows, "getRows");
   pragma Import (Java, IsMultipleMode, "isMultipleMode");
   pragma Import (Java, SetMultipleMode, "setMultipleMode");
   pragma Import (Java, GetVisibleIndex, "getVisibleIndex");
   pragma Import (Java, MakeVisible, "makeVisible");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, AddItemListener, "addItemListener");
   pragma Import (Java, RemoveItemListener, "removeItemListener");
   pragma Import (Java, GetItemListeners, "getItemListeners");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessItemEvent, "processItemEvent");
   pragma Import (Java, ProcessActionEvent, "processActionEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.List;
pragma Import (Java, Java.Awt.List, "java.awt.List");
pragma Extensions_Allowed (Off);
