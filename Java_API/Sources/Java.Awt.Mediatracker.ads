pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Image;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.MediaTracker is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MediaTracker (P1_Component : access Standard.Java.Awt.Component.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int);

   --  synchronized
   procedure AddImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   function CheckAll (This : access Typ)
                      return Java.Boolean;

   function CheckAll (This : access Typ;
                      P1_Boolean : Java.Boolean)
                      return Java.Boolean;

   --  synchronized
   function IsErrorAny (This : access Typ)
                        return Java.Boolean;

   --  synchronized
   function GetErrorsAny (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   procedure WaitForAll (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   --  synchronized
   function WaitForAll (This : access Typ;
                        P1_Long : Java.Long)
                        return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   function StatusAll (This : access Typ;
                       P1_Boolean : Java.Boolean)
                       return Java.Int;

   function CheckID (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Boolean;

   function CheckID (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Boolean : Java.Boolean)
                     return Java.Boolean;

   --  synchronized
   function IsErrorID (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Boolean;

   --  synchronized
   function GetErrorsID (This : access Typ;
                         P1_Int : Java.Int)
                         return Standard.Java.Lang.Object.Ref;

   procedure WaitForID (This : access Typ;
                        P1_Int : Java.Int);
   --  can raise Java.Lang.InterruptedException.Except

   --  synchronized
   function WaitForID (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Long : Java.Long)
                       return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   function StatusID (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Boolean : Java.Boolean)
                      return Java.Int;

   --  synchronized
   procedure RemoveImage (This : access Typ;
                          P1_Image : access Standard.Java.Awt.Image.Typ'Class);

   --  synchronized
   procedure RemoveImage (This : access Typ;
                          P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_Int : Java.Int);

   --  synchronized
   procedure RemoveImage (This : access Typ;
                          P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LOADING : constant Java.Int;

   --  final
   ABORTED : constant Java.Int;

   --  final
   ERRORED : constant Java.Int;

   --  final
   COMPLETE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MediaTracker);
   pragma Import (Java, AddImage, "addImage");
   pragma Import (Java, CheckAll, "checkAll");
   pragma Import (Java, IsErrorAny, "isErrorAny");
   pragma Import (Java, GetErrorsAny, "getErrorsAny");
   pragma Import (Java, WaitForAll, "waitForAll");
   pragma Import (Java, StatusAll, "statusAll");
   pragma Import (Java, CheckID, "checkID");
   pragma Import (Java, IsErrorID, "isErrorID");
   pragma Import (Java, GetErrorsID, "getErrorsID");
   pragma Import (Java, WaitForID, "waitForID");
   pragma Import (Java, StatusID, "statusID");
   pragma Import (Java, RemoveImage, "removeImage");
   pragma Import (Java, LOADING, "LOADING");
   pragma Import (Java, ABORTED, "ABORTED");
   pragma Import (Java, ERRORED, "ERRORED");
   pragma Import (Java, COMPLETE, "COMPLETE");

end Java.Awt.MediaTracker;
pragma Import (Java, Java.Awt.MediaTracker, "java.awt.MediaTracker");
pragma Extensions_Allowed (Off);
