pragma Extensions_Allowed (On);
limited with Java.Awt.MenuComponent;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.MenuContainer;
with Java.Awt.MenuItem;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Menu is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.MenuItem.Typ(Serializable_I,
                                 Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Menu (This : Ref := null)
                      return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Menu (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Menu (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Boolean : Java.Boolean; 
                      This : Ref := null)
                      return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   function IsTearOff (This : access Typ)
                       return Java.Boolean;

   function GetItemCount (This : access Typ)
                          return Java.Int;

   function GetItem (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Awt.MenuItem.Typ'Class;

   function Add (This : access Typ;
                 P1_MenuItem : access Standard.Java.Awt.MenuItem.Typ'Class)
                 return access Java.Awt.MenuItem.Typ'Class;

   procedure Add (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Insert (This : access Typ;
                     P1_MenuItem : access Standard.Java.Awt.MenuItem.Typ'Class;
                     P2_Int : Java.Int);

   procedure Insert (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int);

   procedure AddSeparator (This : access Typ);

   procedure InsertSeparator (This : access Typ;
                              P1_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_MenuComponent : access Standard.Java.Awt.MenuComponent.Typ'Class);

   procedure RemoveAll (This : access Typ);

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Menu);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, IsTearOff, "isTearOff");
   pragma Import (Java, GetItemCount, "getItemCount");
   pragma Import (Java, GetItem, "getItem");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, AddSeparator, "addSeparator");
   pragma Import (Java, InsertSeparator, "insertSeparator");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.Menu;
pragma Import (Java, Java.Awt.Menu, "java.awt.Menu");
pragma Extensions_Allowed (Off);
