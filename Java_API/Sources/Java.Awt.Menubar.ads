pragma Extensions_Allowed (On);
limited with Java.Awt.Menu;
limited with Java.Awt.MenuItem;
limited with Java.Awt.MenuShortcut;
limited with Java.Util.Enumeration;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.MenuComponent;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.MenuBar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.MenuComponent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MenuBar (This : Ref := null)
                         return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   function GetHelpMenu (This : access Typ)
                         return access Java.Awt.Menu.Typ'Class;

   procedure SetHelpMenu (This : access Typ;
                          P1_Menu : access Standard.Java.Awt.Menu.Typ'Class);

   function Add (This : access Typ;
                 P1_Menu : access Standard.Java.Awt.Menu.Typ'Class)
                 return access Java.Awt.Menu.Typ'Class;

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_MenuComponent : access Standard.Java.Awt.MenuComponent.Typ'Class);

   function GetMenuCount (This : access Typ)
                          return Java.Int;

   function GetMenu (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Awt.Menu.Typ'Class;

   --  synchronized
   function Shortcuts (This : access Typ)
                       return access Java.Util.Enumeration.Typ'Class;

   function GetShortcutMenuItem (This : access Typ;
                                 P1_MenuShortcut : access Standard.Java.Awt.MenuShortcut.Typ'Class)
                                 return access Java.Awt.MenuItem.Typ'Class;

   procedure DeleteShortcut (This : access Typ;
                             P1_MenuShortcut : access Standard.Java.Awt.MenuShortcut.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MenuBar);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, GetHelpMenu, "getHelpMenu");
   pragma Import (Java, SetHelpMenu, "setHelpMenu");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, GetMenuCount, "getMenuCount");
   pragma Import (Java, GetMenu, "getMenu");
   pragma Import (Java, Shortcuts, "shortcuts");
   pragma Import (Java, GetShortcutMenuItem, "getShortcutMenuItem");
   pragma Import (Java, DeleteShortcut, "deleteShortcut");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.MenuBar;
pragma Import (Java, Java.Awt.MenuBar, "java.awt.MenuBar");
pragma Extensions_Allowed (Off);
