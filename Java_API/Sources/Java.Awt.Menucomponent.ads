pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Font;
limited with Java.Awt.MenuContainer;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.MenuComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MenuComponent (This : Ref := null)
                               return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetParent (This : access Typ)
                       return access Java.Awt.MenuContainer.Typ'Class;

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   procedure RemoveNotify (This : access Typ);

   --  final
   procedure DispatchEvent (This : access Typ;
                            P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final  protected
   function GetTreeLock (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MenuComponent);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetName, "setName");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, DispatchEvent, "dispatchEvent");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetTreeLock, "getTreeLock");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.MenuComponent;
pragma Import (Java, Java.Awt.MenuComponent, "java.awt.MenuComponent");
pragma Extensions_Allowed (Off);
