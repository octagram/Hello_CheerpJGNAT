pragma Extensions_Allowed (On);
limited with Java.Awt.Font;
limited with Java.Awt.MenuComponent;
with Java.Lang.Object;

package Java.Awt.MenuContainer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class is abstract;

   procedure Remove (This : access Typ;
                     P1_MenuComponent : access Standard.Java.Awt.MenuComponent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetFont, "getFont");
   pragma Export (Java, Remove, "remove");

end Java.Awt.MenuContainer;
pragma Import (Java, Java.Awt.MenuContainer, "java.awt.MenuContainer");
pragma Extensions_Allowed (Off);
