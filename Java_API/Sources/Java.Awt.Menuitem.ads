pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.MenuShortcut;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.MenuComponent;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.MenuItem is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.MenuComponent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MenuItem (This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_MenuItem (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_MenuItem (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_MenuShortcut : access Standard.Java.Awt.MenuShortcut.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetLabel (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure SetLabel (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   --  synchronized
   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function GetShortcut (This : access Typ)
                         return access Java.Awt.MenuShortcut.Typ'Class;

   procedure SetShortcut (This : access Typ;
                          P1_MenuShortcut : access Standard.Java.Awt.MenuShortcut.Typ'Class);

   procedure DeleteShortcut (This : access Typ);

   --  final  protected
   procedure EnableEvents (This : access Typ;
                           P1_Long : Java.Long);

   --  final  protected
   procedure DisableEvents (This : access Typ;
                            P1_Long : Java.Long);

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessActionEvent (This : access Typ;
                                 P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MenuItem);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetLabel, "getLabel");
   pragma Import (Java, SetLabel, "setLabel");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, GetShortcut, "getShortcut");
   pragma Import (Java, SetShortcut, "setShortcut");
   pragma Import (Java, DeleteShortcut, "deleteShortcut");
   pragma Import (Java, EnableEvents, "enableEvents");
   pragma Import (Java, DisableEvents, "disableEvents");
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessActionEvent, "processActionEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.MenuItem;
pragma Import (Java, Java.Awt.MenuItem, "java.awt.MenuItem");
pragma Extensions_Allowed (Off);
