pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.MenuShortcut is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MenuShortcut (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_MenuShortcut (P1_Int : Java.Int;
                              P2_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKey (This : access Typ)
                    return Java.Int;

   function UsesShiftModifier (This : access Typ)
                               return Java.Boolean;

   function Equals (This : access Typ;
                    P1_MenuShortcut : access Standard.Java.Awt.MenuShortcut.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MenuShortcut);
   pragma Import (Java, GetKey, "getKey");
   pragma Import (Java, UsesShiftModifier, "usesShiftModifier");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ParamString, "paramString");

end Java.Awt.MenuShortcut;
pragma Import (Java, Java.Awt.MenuShortcut, "java.awt.MenuShortcut");
pragma Extensions_Allowed (Off);
