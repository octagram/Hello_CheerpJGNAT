pragma Extensions_Allowed (On);
limited with Java.Awt.PointerInfo;
with Java.Lang.Object;

package Java.Awt.MouseInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPointerInfo return access Java.Awt.PointerInfo.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function GetNumberOfButtons return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetPointerInfo, "getPointerInfo");
   pragma Import (Java, GetNumberOfButtons, "getNumberOfButtons");

end Java.Awt.MouseInfo;
pragma Import (Java, Java.Awt.MouseInfo, "java.awt.MouseInfo");
pragma Extensions_Allowed (Off);
