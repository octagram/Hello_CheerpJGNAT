pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.MultipleGradientPaint.ColorSpaceType;
limited with Java.Awt.MultipleGradientPaint.CycleMethod;
with Java.Awt.Paint;
with Java.Lang.Object;

package Java.Awt.MultipleGradientPaint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Paint_I : Java.Awt.Paint.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetFractions (This : access Typ)
                          return Java.Float_Arr;

   --  final
   function GetColors (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCycleMethod (This : access Typ)
                            return access Java.Awt.MultipleGradientPaint.CycleMethod.Typ'Class;

   --  final
   function GetColorSpace (This : access Typ)
                           return access Java.Awt.MultipleGradientPaint.ColorSpaceType.Typ'Class;

   --  final
   function GetTransform (This : access Typ)
                          return access Java.Awt.Geom.AffineTransform.Typ'Class;

   --  final
   function GetTransparency (This : access Typ)
                             return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetFractions, "getFractions");
   pragma Import (Java, GetColors, "getColors");
   pragma Import (Java, GetCycleMethod, "getCycleMethod");
   pragma Import (Java, GetColorSpace, "getColorSpace");
   pragma Import (Java, GetTransform, "getTransform");
   pragma Import (Java, GetTransparency, "getTransparency");

end Java.Awt.MultipleGradientPaint;
pragma Import (Java, Java.Awt.MultipleGradientPaint, "java.awt.MultipleGradientPaint");
pragma Extensions_Allowed (Off);
