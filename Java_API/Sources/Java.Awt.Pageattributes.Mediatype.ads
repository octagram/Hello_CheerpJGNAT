pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.PageAttributes.MediaType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ISO_4A0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_2A0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A1 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A2 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A3 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A4 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A5 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A6 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A7 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A8 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_A10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B1 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B2 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B3 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B4 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B5 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B6 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B7 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B8 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B1 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B2 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B3 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B4 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B5 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B6 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B7 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B8 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   JIS_B10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C1 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C2 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C3 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C4 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C5 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C6 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C7 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C8 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_DESIGNATED_LONG : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   EXECUTIVE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   FOLIO : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   INVOICE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   LEDGER : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_LETTER : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_LEGAL : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   QUARTO : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   D : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   E : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_10X15_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_10X14_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_10X13_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_9X12_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_9X11_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_7X9_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_6X9_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_NUMBER_9_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_NUMBER_10_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_NUMBER_11_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_NUMBER_12_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NA_NUMBER_14_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   INVITE_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ITALY_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   MONARCH_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   PERSONAL_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A1 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A2 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A3 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A4 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A5 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A6 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A7 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A8 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   A10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B1 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B2 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B3 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B4 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B4_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B5 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_B5_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B6 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B7 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B8 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   B10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C0 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C0_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C1 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C1_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C2 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C2_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C3 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C3_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C4 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C4_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C5 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C5_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C6 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C6_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C7 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C7_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C8 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C8_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C9_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   C10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_C10_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ISO_DESIGNATED_LONG_ENVELOPE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   STATEMENT : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   TABLOID : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   LETTER : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   NOTE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   LEGAL : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_10X15 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_10X14 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_10X13 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_9X12 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_9X11 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_7X9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_6X9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_9 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_10 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_11 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_12 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_14 : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_INVITE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_ITALY : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_MONARCH : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ENV_PERSONAL : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   INVITE : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   ITALY : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   MONARCH : access Java.Awt.PageAttributes.MediaType.Typ'Class;

   --  final
   PERSONAL : access Java.Awt.PageAttributes.MediaType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ISO_4A0, "ISO_4A0");
   pragma Import (Java, ISO_2A0, "ISO_2A0");
   pragma Import (Java, ISO_A0, "ISO_A0");
   pragma Import (Java, ISO_A1, "ISO_A1");
   pragma Import (Java, ISO_A2, "ISO_A2");
   pragma Import (Java, ISO_A3, "ISO_A3");
   pragma Import (Java, ISO_A4, "ISO_A4");
   pragma Import (Java, ISO_A5, "ISO_A5");
   pragma Import (Java, ISO_A6, "ISO_A6");
   pragma Import (Java, ISO_A7, "ISO_A7");
   pragma Import (Java, ISO_A8, "ISO_A8");
   pragma Import (Java, ISO_A9, "ISO_A9");
   pragma Import (Java, ISO_A10, "ISO_A10");
   pragma Import (Java, ISO_B0, "ISO_B0");
   pragma Import (Java, ISO_B1, "ISO_B1");
   pragma Import (Java, ISO_B2, "ISO_B2");
   pragma Import (Java, ISO_B3, "ISO_B3");
   pragma Import (Java, ISO_B4, "ISO_B4");
   pragma Import (Java, ISO_B5, "ISO_B5");
   pragma Import (Java, ISO_B6, "ISO_B6");
   pragma Import (Java, ISO_B7, "ISO_B7");
   pragma Import (Java, ISO_B8, "ISO_B8");
   pragma Import (Java, ISO_B9, "ISO_B9");
   pragma Import (Java, ISO_B10, "ISO_B10");
   pragma Import (Java, JIS_B0, "JIS_B0");
   pragma Import (Java, JIS_B1, "JIS_B1");
   pragma Import (Java, JIS_B2, "JIS_B2");
   pragma Import (Java, JIS_B3, "JIS_B3");
   pragma Import (Java, JIS_B4, "JIS_B4");
   pragma Import (Java, JIS_B5, "JIS_B5");
   pragma Import (Java, JIS_B6, "JIS_B6");
   pragma Import (Java, JIS_B7, "JIS_B7");
   pragma Import (Java, JIS_B8, "JIS_B8");
   pragma Import (Java, JIS_B9, "JIS_B9");
   pragma Import (Java, JIS_B10, "JIS_B10");
   pragma Import (Java, ISO_C0, "ISO_C0");
   pragma Import (Java, ISO_C1, "ISO_C1");
   pragma Import (Java, ISO_C2, "ISO_C2");
   pragma Import (Java, ISO_C3, "ISO_C3");
   pragma Import (Java, ISO_C4, "ISO_C4");
   pragma Import (Java, ISO_C5, "ISO_C5");
   pragma Import (Java, ISO_C6, "ISO_C6");
   pragma Import (Java, ISO_C7, "ISO_C7");
   pragma Import (Java, ISO_C8, "ISO_C8");
   pragma Import (Java, ISO_C9, "ISO_C9");
   pragma Import (Java, ISO_C10, "ISO_C10");
   pragma Import (Java, ISO_DESIGNATED_LONG, "ISO_DESIGNATED_LONG");
   pragma Import (Java, EXECUTIVE, "EXECUTIVE");
   pragma Import (Java, FOLIO, "FOLIO");
   pragma Import (Java, INVOICE, "INVOICE");
   pragma Import (Java, LEDGER, "LEDGER");
   pragma Import (Java, NA_LETTER, "NA_LETTER");
   pragma Import (Java, NA_LEGAL, "NA_LEGAL");
   pragma Import (Java, QUARTO, "QUARTO");
   pragma Import (Java, A, "A");
   pragma Import (Java, B, "B");
   pragma Import (Java, C, "C");
   pragma Import (Java, D, "D");
   pragma Import (Java, E, "E");
   pragma Import (Java, NA_10X15_ENVELOPE, "NA_10X15_ENVELOPE");
   pragma Import (Java, NA_10X14_ENVELOPE, "NA_10X14_ENVELOPE");
   pragma Import (Java, NA_10X13_ENVELOPE, "NA_10X13_ENVELOPE");
   pragma Import (Java, NA_9X12_ENVELOPE, "NA_9X12_ENVELOPE");
   pragma Import (Java, NA_9X11_ENVELOPE, "NA_9X11_ENVELOPE");
   pragma Import (Java, NA_7X9_ENVELOPE, "NA_7X9_ENVELOPE");
   pragma Import (Java, NA_6X9_ENVELOPE, "NA_6X9_ENVELOPE");
   pragma Import (Java, NA_NUMBER_9_ENVELOPE, "NA_NUMBER_9_ENVELOPE");
   pragma Import (Java, NA_NUMBER_10_ENVELOPE, "NA_NUMBER_10_ENVELOPE");
   pragma Import (Java, NA_NUMBER_11_ENVELOPE, "NA_NUMBER_11_ENVELOPE");
   pragma Import (Java, NA_NUMBER_12_ENVELOPE, "NA_NUMBER_12_ENVELOPE");
   pragma Import (Java, NA_NUMBER_14_ENVELOPE, "NA_NUMBER_14_ENVELOPE");
   pragma Import (Java, INVITE_ENVELOPE, "INVITE_ENVELOPE");
   pragma Import (Java, ITALY_ENVELOPE, "ITALY_ENVELOPE");
   pragma Import (Java, MONARCH_ENVELOPE, "MONARCH_ENVELOPE");
   pragma Import (Java, PERSONAL_ENVELOPE, "PERSONAL_ENVELOPE");
   pragma Import (Java, A0, "A0");
   pragma Import (Java, A1, "A1");
   pragma Import (Java, A2, "A2");
   pragma Import (Java, A3, "A3");
   pragma Import (Java, A4, "A4");
   pragma Import (Java, A5, "A5");
   pragma Import (Java, A6, "A6");
   pragma Import (Java, A7, "A7");
   pragma Import (Java, A8, "A8");
   pragma Import (Java, A9, "A9");
   pragma Import (Java, A10, "A10");
   pragma Import (Java, B0, "B0");
   pragma Import (Java, B1, "B1");
   pragma Import (Java, B2, "B2");
   pragma Import (Java, B3, "B3");
   pragma Import (Java, B4, "B4");
   pragma Import (Java, ISO_B4_ENVELOPE, "ISO_B4_ENVELOPE");
   pragma Import (Java, B5, "B5");
   pragma Import (Java, ISO_B5_ENVELOPE, "ISO_B5_ENVELOPE");
   pragma Import (Java, B6, "B6");
   pragma Import (Java, B7, "B7");
   pragma Import (Java, B8, "B8");
   pragma Import (Java, B9, "B9");
   pragma Import (Java, B10, "B10");
   pragma Import (Java, C0, "C0");
   pragma Import (Java, ISO_C0_ENVELOPE, "ISO_C0_ENVELOPE");
   pragma Import (Java, C1, "C1");
   pragma Import (Java, ISO_C1_ENVELOPE, "ISO_C1_ENVELOPE");
   pragma Import (Java, C2, "C2");
   pragma Import (Java, ISO_C2_ENVELOPE, "ISO_C2_ENVELOPE");
   pragma Import (Java, C3, "C3");
   pragma Import (Java, ISO_C3_ENVELOPE, "ISO_C3_ENVELOPE");
   pragma Import (Java, C4, "C4");
   pragma Import (Java, ISO_C4_ENVELOPE, "ISO_C4_ENVELOPE");
   pragma Import (Java, C5, "C5");
   pragma Import (Java, ISO_C5_ENVELOPE, "ISO_C5_ENVELOPE");
   pragma Import (Java, C6, "C6");
   pragma Import (Java, ISO_C6_ENVELOPE, "ISO_C6_ENVELOPE");
   pragma Import (Java, C7, "C7");
   pragma Import (Java, ISO_C7_ENVELOPE, "ISO_C7_ENVELOPE");
   pragma Import (Java, C8, "C8");
   pragma Import (Java, ISO_C8_ENVELOPE, "ISO_C8_ENVELOPE");
   pragma Import (Java, C9, "C9");
   pragma Import (Java, ISO_C9_ENVELOPE, "ISO_C9_ENVELOPE");
   pragma Import (Java, C10, "C10");
   pragma Import (Java, ISO_C10_ENVELOPE, "ISO_C10_ENVELOPE");
   pragma Import (Java, ISO_DESIGNATED_LONG_ENVELOPE, "ISO_DESIGNATED_LONG_ENVELOPE");
   pragma Import (Java, STATEMENT, "STATEMENT");
   pragma Import (Java, TABLOID, "TABLOID");
   pragma Import (Java, LETTER, "LETTER");
   pragma Import (Java, NOTE, "NOTE");
   pragma Import (Java, LEGAL, "LEGAL");
   pragma Import (Java, ENV_10X15, "ENV_10X15");
   pragma Import (Java, ENV_10X14, "ENV_10X14");
   pragma Import (Java, ENV_10X13, "ENV_10X13");
   pragma Import (Java, ENV_9X12, "ENV_9X12");
   pragma Import (Java, ENV_9X11, "ENV_9X11");
   pragma Import (Java, ENV_7X9, "ENV_7X9");
   pragma Import (Java, ENV_6X9, "ENV_6X9");
   pragma Import (Java, ENV_9, "ENV_9");
   pragma Import (Java, ENV_10, "ENV_10");
   pragma Import (Java, ENV_11, "ENV_11");
   pragma Import (Java, ENV_12, "ENV_12");
   pragma Import (Java, ENV_14, "ENV_14");
   pragma Import (Java, ENV_INVITE, "ENV_INVITE");
   pragma Import (Java, ENV_ITALY, "ENV_ITALY");
   pragma Import (Java, ENV_MONARCH, "ENV_MONARCH");
   pragma Import (Java, ENV_PERSONAL, "ENV_PERSONAL");
   pragma Import (Java, INVITE, "INVITE");
   pragma Import (Java, ITALY, "ITALY");
   pragma Import (Java, MONARCH, "MONARCH");
   pragma Import (Java, PERSONAL, "PERSONAL");

end Java.Awt.PageAttributes.MediaType;
pragma Import (Java, Java.Awt.PageAttributes.MediaType, "java.awt.PageAttributes$MediaType");
pragma Extensions_Allowed (Off);
