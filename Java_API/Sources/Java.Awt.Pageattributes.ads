pragma Extensions_Allowed (On);
limited with Java.Awt.PageAttributes.ColorType;
limited with Java.Awt.PageAttributes.MediaType;
limited with Java.Awt.PageAttributes.OrientationRequestedType;
limited with Java.Awt.PageAttributes.OriginType;
limited with Java.Awt.PageAttributes.PrintQualityType;
limited with Java.Lang.String;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.PageAttributes is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PageAttributes (This : Ref := null)
                                return Ref;

   function New_PageAttributes (P1_PageAttributes : access Standard.Java.Awt.PageAttributes.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_PageAttributes (P1_ColorType : access Standard.Java.Awt.PageAttributes.ColorType.Typ'Class;
                                P2_MediaType : access Standard.Java.Awt.PageAttributes.MediaType.Typ'Class;
                                P3_OrientationRequestedType : access Standard.Java.Awt.PageAttributes.OrientationRequestedType.Typ'Class;
                                P4_OriginType : access Standard.Java.Awt.PageAttributes.OriginType.Typ'Class;
                                P5_PrintQualityType : access Standard.Java.Awt.PageAttributes.PrintQualityType.Typ'Class;
                                P6_Int_Arr : Java.Int_Arr; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   procedure Set (This : access Typ;
                  P1_PageAttributes : access Standard.Java.Awt.PageAttributes.Typ'Class);

   function GetColor (This : access Typ)
                      return access Java.Awt.PageAttributes.ColorType.Typ'Class;

   procedure SetColor (This : access Typ;
                       P1_ColorType : access Standard.Java.Awt.PageAttributes.ColorType.Typ'Class);

   function GetMedia (This : access Typ)
                      return access Java.Awt.PageAttributes.MediaType.Typ'Class;

   procedure SetMedia (This : access Typ;
                       P1_MediaType : access Standard.Java.Awt.PageAttributes.MediaType.Typ'Class);

   procedure SetMediaToDefault (This : access Typ);

   function GetOrientationRequested (This : access Typ)
                                     return access Java.Awt.PageAttributes.OrientationRequestedType.Typ'Class;

   procedure SetOrientationRequested (This : access Typ;
                                      P1_OrientationRequestedType : access Standard.Java.Awt.PageAttributes.OrientationRequestedType.Typ'Class);

   procedure SetOrientationRequested (This : access Typ;
                                      P1_Int : Java.Int);

   procedure SetOrientationRequestedToDefault (This : access Typ);

   function GetOrigin (This : access Typ)
                       return access Java.Awt.PageAttributes.OriginType.Typ'Class;

   procedure SetOrigin (This : access Typ;
                        P1_OriginType : access Standard.Java.Awt.PageAttributes.OriginType.Typ'Class);

   function GetPrintQuality (This : access Typ)
                             return access Java.Awt.PageAttributes.PrintQualityType.Typ'Class;

   procedure SetPrintQuality (This : access Typ;
                              P1_PrintQualityType : access Standard.Java.Awt.PageAttributes.PrintQualityType.Typ'Class);

   procedure SetPrintQuality (This : access Typ;
                              P1_Int : Java.Int);

   procedure SetPrintQualityToDefault (This : access Typ);

   function GetPrinterResolution (This : access Typ)
                                  return Java.Int_Arr;

   procedure SetPrinterResolution (This : access Typ;
                                   P1_Int_Arr : Java.Int_Arr);

   procedure SetPrinterResolution (This : access Typ;
                                   P1_Int : Java.Int);

   procedure SetPrinterResolutionToDefault (This : access Typ);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PageAttributes);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Set, "set");
   pragma Import (Java, GetColor, "getColor");
   pragma Import (Java, SetColor, "setColor");
   pragma Import (Java, GetMedia, "getMedia");
   pragma Import (Java, SetMedia, "setMedia");
   pragma Import (Java, SetMediaToDefault, "setMediaToDefault");
   pragma Import (Java, GetOrientationRequested, "getOrientationRequested");
   pragma Import (Java, SetOrientationRequested, "setOrientationRequested");
   pragma Import (Java, SetOrientationRequestedToDefault, "setOrientationRequestedToDefault");
   pragma Import (Java, GetOrigin, "getOrigin");
   pragma Import (Java, SetOrigin, "setOrigin");
   pragma Import (Java, GetPrintQuality, "getPrintQuality");
   pragma Import (Java, SetPrintQuality, "setPrintQuality");
   pragma Import (Java, SetPrintQualityToDefault, "setPrintQualityToDefault");
   pragma Import (Java, GetPrinterResolution, "getPrinterResolution");
   pragma Import (Java, SetPrinterResolution, "setPrinterResolution");
   pragma Import (Java, SetPrinterResolutionToDefault, "setPrinterResolutionToDefault");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Awt.PageAttributes;
pragma Import (Java, Java.Awt.PageAttributes, "java.awt.PageAttributes");
pragma Extensions_Allowed (Off);
