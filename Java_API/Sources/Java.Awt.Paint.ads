pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.PaintContext;
limited with Java.Awt.Rectangle;
limited with Java.Awt.RenderingHints;
with Java.Awt.Transparency;
with Java.Lang.Object;

package Java.Awt.Paint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Transparency_I : Java.Awt.Transparency.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                           P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                           P4_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                           P5_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.PaintContext.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateContext, "createContext");

end Java.Awt.Paint;
pragma Import (Java, Java.Awt.Paint, "java.awt.Paint");
pragma Extensions_Allowed (Off);
