pragma Extensions_Allowed (On);
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.Raster;
with Java.Lang.Object;

package Java.Awt.PaintContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Dispose (This : access Typ) is abstract;

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class is abstract;

   function GetRaster (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int)
                       return access Java.Awt.Image.Raster.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Dispose, "dispose");
   pragma Export (Java, GetColorModel, "getColorModel");
   pragma Export (Java, GetRaster, "getRaster");

end Java.Awt.PaintContext;
pragma Import (Java, Java.Awt.PaintContext, "java.awt.PaintContext");
pragma Extensions_Allowed (Off);
