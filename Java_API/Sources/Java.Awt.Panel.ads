pragma Extensions_Allowed (On);
limited with Java.Awt.LayoutManager;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Container;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Panel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Container.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Panel (This : Ref := null)
                       return Ref;

   function New_Panel (P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Panel);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.Panel;
pragma Import (Java, Java.Awt.Panel, "java.awt.Panel");
pragma Extensions_Allowed (Off);
