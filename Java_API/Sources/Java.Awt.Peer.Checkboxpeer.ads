pragma Extensions_Allowed (On);
limited with Java.Awt.CheckboxGroup;
limited with Java.Lang.String;
with Java.Awt.Peer.ComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.CheckboxPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ComponentPeer_I : Java.Awt.Peer.ComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetState (This : access Typ;
                       P1_Boolean : Java.Boolean) is abstract;

   procedure SetCheckboxGroup (This : access Typ;
                               P1_CheckboxGroup : access Standard.Java.Awt.CheckboxGroup.Typ'Class) is abstract;

   procedure SetLabel (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetState, "setState");
   pragma Export (Java, SetCheckboxGroup, "setCheckboxGroup");
   pragma Export (Java, SetLabel, "setLabel");

end Java.Awt.Peer.CheckboxPeer;
pragma Import (Java, Java.Awt.Peer.CheckboxPeer, "java.awt.peer.CheckboxPeer");
pragma Extensions_Allowed (Off);
