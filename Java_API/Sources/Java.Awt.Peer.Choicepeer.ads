pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Peer.ComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.ChoicePeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ComponentPeer_I : Java.Awt.Peer.ComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Add (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Int : Java.Int) is abstract;

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int) is abstract;

   procedure RemoveAll (This : access Typ) is abstract;

   procedure select_K (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   procedure AddItem (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Add, "add");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, RemoveAll, "removeAll");
   pragma Export (Java, select_K, "select");
   pragma Export (Java, AddItem, "addItem");

end Java.Awt.Peer.ChoicePeer;
pragma Import (Java, Java.Awt.Peer.ChoicePeer, "java.awt.peer.ChoicePeer");
pragma Extensions_Allowed (Off);
