pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.BufferCapabilities;
limited with Java.Awt.BufferCapabilities.FlipContents;
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.PaintEvent;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Image;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Awt.Image.VolatileImage;
limited with Java.Awt.Peer.ContainerPeer;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Toolkit;
with Java.Lang.Object;

package Java.Awt.Peer.ComponentPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsObscured (This : access Typ)
                        return Java.Boolean is abstract;

   function CanDetermineObscurity (This : access Typ)
                                   return Java.Boolean is abstract;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class) is abstract;

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int) is abstract;

   procedure Print (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class) is abstract;

   procedure SetBounds (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int) is abstract;

   procedure HandleEvent (This : access Typ;
                          P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class) is abstract;

   procedure CoalescePaintEvent (This : access Typ;
                                 P1_PaintEvent : access Standard.Java.Awt.Event.PaintEvent.Typ'Class) is abstract;

   function GetLocationOnScreen (This : access Typ)
                                 return access Java.Awt.Point.Typ'Class is abstract;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class is abstract;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class is abstract;

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class is abstract;

   function GetToolkit (This : access Typ)
                        return access Java.Awt.Toolkit.Typ'Class is abstract;

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class is abstract;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class is abstract;

   procedure Dispose (This : access Typ) is abstract;

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class) is abstract;

   procedure UpdateCursorImmediately (This : access Typ) is abstract;

   function IsFocusable (This : access Typ)
                         return Java.Boolean is abstract;

   function CreateImage (This : access Typ;
                         P1_ImageProducer : access Standard.Java.Awt.Image.ImageProducer.Typ'Class)
                         return access Java.Awt.Image.Typ'Class is abstract;

   function CreateImage (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return access Java.Awt.Image.Typ'Class is abstract;

   function CreateVolatileImage (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int)
                                 return access Java.Awt.Image.VolatileImage.Typ'Class is abstract;

   function PrepareImage (This : access Typ;
                          P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                          return Java.Boolean is abstract;

   function CheckImage (This : access Typ;
                        P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                        return Java.Int is abstract;

   function GetGraphicsConfiguration (This : access Typ)
                                      return access Java.Awt.GraphicsConfiguration.Typ'Class is abstract;

   function HandlesWheelScrolling (This : access Typ)
                                   return Java.Boolean is abstract;

   procedure CreateBuffers (This : access Typ;
                            P1_Int : Java.Int;
                            P2_BufferCapabilities : access Standard.Java.Awt.BufferCapabilities.Typ'Class) is abstract;
   --  can raise Java.Awt.AWTException.Except

   function GetBackBuffer (This : access Typ)
                           return access Java.Awt.Image.Typ'Class is abstract;

   procedure Flip (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Int : Java.Int;
                   P5_FlipContents : access Standard.Java.Awt.BufferCapabilities.FlipContents.Typ'Class) is abstract;

   procedure DestroyBuffers (This : access Typ) is abstract;

   procedure Reparent (This : access Typ;
                       P1_ContainerPeer : access Standard.Java.Awt.Peer.ContainerPeer.Typ'Class) is abstract;

   function IsReparentSupported (This : access Typ)
                                 return Java.Boolean is abstract;

   procedure Layout (This : access Typ) is abstract;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class is abstract;

   function PreferredSize (This : access Typ)
                           return access Java.Awt.Dimension.Typ'Class is abstract;

   function MinimumSize (This : access Typ)
                         return access Java.Awt.Dimension.Typ'Class is abstract;

   procedure Show (This : access Typ) is abstract;

   procedure Hide (This : access Typ) is abstract;

   procedure Enable (This : access Typ) is abstract;

   procedure Disable (This : access Typ) is abstract;

   procedure Reshape (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SET_LOCATION : constant Java.Int;

   --  final
   SET_SIZE : constant Java.Int;

   --  final
   SET_BOUNDS : constant Java.Int;

   --  final
   SET_CLIENT_SIZE : constant Java.Int;

   --  final
   RESET_OPERATION : constant Java.Int;

   --  final
   NO_EMBEDDED_CHECK : constant Java.Int;

   --  final
   DEFAULT_OPERATION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsObscured, "isObscured");
   pragma Export (Java, CanDetermineObscurity, "canDetermineObscurity");
   pragma Export (Java, SetVisible, "setVisible");
   pragma Export (Java, SetEnabled, "setEnabled");
   pragma Export (Java, Paint, "paint");
   pragma Export (Java, Repaint, "repaint");
   pragma Export (Java, Print, "print");
   pragma Export (Java, SetBounds, "setBounds");
   pragma Export (Java, HandleEvent, "handleEvent");
   pragma Export (Java, CoalescePaintEvent, "coalescePaintEvent");
   pragma Export (Java, GetLocationOnScreen, "getLocationOnScreen");
   pragma Export (Java, GetPreferredSize, "getPreferredSize");
   pragma Export (Java, GetMinimumSize, "getMinimumSize");
   pragma Export (Java, GetColorModel, "getColorModel");
   pragma Export (Java, GetToolkit, "getToolkit");
   pragma Export (Java, GetGraphics, "getGraphics");
   pragma Export (Java, GetFontMetrics, "getFontMetrics");
   pragma Export (Java, Dispose, "dispose");
   pragma Export (Java, SetForeground, "setForeground");
   pragma Export (Java, SetBackground, "setBackground");
   pragma Export (Java, SetFont, "setFont");
   pragma Export (Java, UpdateCursorImmediately, "updateCursorImmediately");
   pragma Export (Java, IsFocusable, "isFocusable");
   pragma Export (Java, CreateImage, "createImage");
   pragma Export (Java, CreateVolatileImage, "createVolatileImage");
   pragma Export (Java, PrepareImage, "prepareImage");
   pragma Export (Java, CheckImage, "checkImage");
   pragma Export (Java, GetGraphicsConfiguration, "getGraphicsConfiguration");
   pragma Export (Java, HandlesWheelScrolling, "handlesWheelScrolling");
   pragma Export (Java, CreateBuffers, "createBuffers");
   pragma Export (Java, GetBackBuffer, "getBackBuffer");
   pragma Export (Java, Flip, "flip");
   pragma Export (Java, DestroyBuffers, "destroyBuffers");
   pragma Export (Java, Reparent, "reparent");
   pragma Export (Java, IsReparentSupported, "isReparentSupported");
   pragma Export (Java, Layout, "layout");
   pragma Export (Java, GetBounds, "getBounds");
   pragma Export (Java, PreferredSize, "preferredSize");
   pragma Export (Java, MinimumSize, "minimumSize");
   pragma Export (Java, Show, "show");
   pragma Export (Java, Hide, "hide");
   pragma Export (Java, Enable, "enable");
   pragma Export (Java, Disable, "disable");
   pragma Export (Java, Reshape, "reshape");
   pragma Import (Java, SET_LOCATION, "SET_LOCATION");
   pragma Import (Java, SET_SIZE, "SET_SIZE");
   pragma Import (Java, SET_BOUNDS, "SET_BOUNDS");
   pragma Import (Java, SET_CLIENT_SIZE, "SET_CLIENT_SIZE");
   pragma Import (Java, RESET_OPERATION, "RESET_OPERATION");
   pragma Import (Java, NO_EMBEDDED_CHECK, "NO_EMBEDDED_CHECK");
   pragma Import (Java, DEFAULT_OPERATION, "DEFAULT_OPERATION");

end Java.Awt.Peer.ComponentPeer;
pragma Import (Java, Java.Awt.Peer.ComponentPeer, "java.awt.peer.ComponentPeer");
pragma Extensions_Allowed (Off);
