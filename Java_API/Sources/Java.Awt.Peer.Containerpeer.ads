pragma Extensions_Allowed (On);
limited with Java.Awt.Insets;
with Java.Awt.Peer.ComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.ContainerPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ComponentPeer_I : Java.Awt.Peer.ComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInsets (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class is abstract;

   procedure BeginValidate (This : access Typ) is abstract;

   procedure EndValidate (This : access Typ) is abstract;

   procedure BeginLayout (This : access Typ) is abstract;

   procedure EndLayout (This : access Typ) is abstract;

   function IsPaintPending (This : access Typ)
                            return Java.Boolean is abstract;

   procedure Restack (This : access Typ) is abstract;

   function IsRestackSupported (This : access Typ)
                                return Java.Boolean is abstract;

   function Insets (This : access Typ)
                    return access Java.Awt.Insets.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetInsets, "getInsets");
   pragma Export (Java, BeginValidate, "beginValidate");
   pragma Export (Java, EndValidate, "endValidate");
   pragma Export (Java, BeginLayout, "beginLayout");
   pragma Export (Java, EndLayout, "endLayout");
   pragma Export (Java, IsPaintPending, "isPaintPending");
   pragma Export (Java, Restack, "restack");
   pragma Export (Java, IsRestackSupported, "isRestackSupported");
   pragma Export (Java, Insets, "insets");

end Java.Awt.Peer.ContainerPeer;
pragma Import (Java, Java.Awt.Peer.ContainerPeer, "java.awt.peer.ContainerPeer");
pragma Extensions_Allowed (Off);
