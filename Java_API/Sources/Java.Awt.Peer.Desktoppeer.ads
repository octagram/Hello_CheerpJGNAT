pragma Extensions_Allowed (On);
limited with Java.Awt.Desktop.Action;
limited with Java.Io.File;
limited with Java.Net.URI;
with Java.Lang.Object;

package Java.Awt.Peer.DesktopPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSupported (This : access Typ;
                         P1_Action : access Standard.Java.Awt.Desktop.Action.Typ'Class)
                         return Java.Boolean is abstract;

   procedure Open (This : access Typ;
                   P1_File : access Standard.Java.Io.File.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Edit (This : access Typ;
                   P1_File : access Standard.Java.Io.File.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Print (This : access Typ;
                    P1_File : access Standard.Java.Io.File.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Mail (This : access Typ;
                   P1_URI : access Standard.Java.Net.URI.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Browse (This : access Typ;
                     P1_URI : access Standard.Java.Net.URI.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsSupported, "isSupported");
   pragma Export (Java, Open, "open");
   pragma Export (Java, Edit, "edit");
   pragma Export (Java, Print, "print");
   pragma Export (Java, Mail, "mail");
   pragma Export (Java, Browse, "browse");

end Java.Awt.Peer.DesktopPeer;
pragma Import (Java, Java.Awt.Peer.DesktopPeer, "java.awt.peer.DesktopPeer");
pragma Extensions_Allowed (Off);
