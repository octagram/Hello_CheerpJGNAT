pragma Extensions_Allowed (On);
limited with Java.Io.FilenameFilter;
limited with Java.Lang.String;
with Java.Awt.Peer.DialogPeer;
with Java.Lang.Object;

package Java.Awt.Peer.FileDialogPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DialogPeer_I : Java.Awt.Peer.DialogPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFile (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetDirectory (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetFilenameFilter (This : access Typ;
                                P1_FilenameFilter : access Standard.Java.Io.FilenameFilter.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetFile, "setFile");
   pragma Export (Java, SetDirectory, "setDirectory");
   pragma Export (Java, SetFilenameFilter, "setFilenameFilter");

end Java.Awt.Peer.FileDialogPeer;
pragma Import (Java, Java.Awt.Peer.FileDialogPeer, "java.awt.peer.FileDialogPeer");
pragma Extensions_Allowed (Off);
