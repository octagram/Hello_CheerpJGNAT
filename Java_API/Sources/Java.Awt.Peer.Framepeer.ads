pragma Extensions_Allowed (On);
limited with Java.Awt.MenuBar;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
with Java.Awt.Peer.WindowPeer;
with Java.Lang.Object;

package Java.Awt.Peer.FramePeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            WindowPeer_I : Java.Awt.Peer.WindowPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTitle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetMenuBar (This : access Typ;
                         P1_MenuBar : access Standard.Java.Awt.MenuBar.Typ'Class) is abstract;

   procedure SetResizable (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;

   procedure SetState (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   function GetState (This : access Typ)
                      return Java.Int is abstract;

   procedure SetMaximizedBounds (This : access Typ;
                                 P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class) is abstract;

   procedure SetBoundsPrivate (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int) is abstract;

   function GetBoundsPrivate (This : access Typ)
                              return access Java.Awt.Rectangle.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetTitle, "setTitle");
   pragma Export (Java, SetMenuBar, "setMenuBar");
   pragma Export (Java, SetResizable, "setResizable");
   pragma Export (Java, SetState, "setState");
   pragma Export (Java, GetState, "getState");
   pragma Export (Java, SetMaximizedBounds, "setMaximizedBounds");
   pragma Export (Java, SetBoundsPrivate, "setBoundsPrivate");
   pragma Export (Java, GetBoundsPrivate, "getBoundsPrivate");

end Java.Awt.Peer.FramePeer;
pragma Import (Java, Java.Awt.Peer.FramePeer, "java.awt.peer.FramePeer");
pragma Extensions_Allowed (Off);
