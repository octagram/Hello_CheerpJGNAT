pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Window;
with Java.Lang.Object;

package Java.Awt.Peer.KeyboardFocusManagerPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetCurrentFocusedWindow (This : access Typ;
                                      P1_Window : access Standard.Java.Awt.Window.Typ'Class) is abstract;

   function GetCurrentFocusedWindow (This : access Typ)
                                     return access Java.Awt.Window.Typ'Class is abstract;

   procedure SetCurrentFocusOwner (This : access Typ;
                                   P1_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   function GetCurrentFocusOwner (This : access Typ)
                                  return access Java.Awt.Component.Typ'Class is abstract;

   procedure ClearGlobalFocusOwner (This : access Typ;
                                    P1_Window : access Standard.Java.Awt.Window.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetCurrentFocusedWindow, "setCurrentFocusedWindow");
   pragma Export (Java, GetCurrentFocusedWindow, "getCurrentFocusedWindow");
   pragma Export (Java, SetCurrentFocusOwner, "setCurrentFocusOwner");
   pragma Export (Java, GetCurrentFocusOwner, "getCurrentFocusOwner");
   pragma Export (Java, ClearGlobalFocusOwner, "clearGlobalFocusOwner");

end Java.Awt.Peer.KeyboardFocusManagerPeer;
pragma Import (Java, Java.Awt.Peer.KeyboardFocusManagerPeer, "java.awt.peer.KeyboardFocusManagerPeer");
pragma Extensions_Allowed (Off);
