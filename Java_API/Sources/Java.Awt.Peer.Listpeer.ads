pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
with Java.Awt.Peer.ComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.ListPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ComponentPeer_I : Java.Awt.Peer.ComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSelectedIndexes (This : access Typ)
                                return Java.Int_Arr is abstract;

   procedure Add (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Int : Java.Int) is abstract;

   procedure DelItems (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int) is abstract;

   procedure RemoveAll (This : access Typ) is abstract;

   procedure select_K (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   procedure Deselect (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   procedure MakeVisible (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   procedure SetMultipleMode (This : access Typ;
                              P1_Boolean : Java.Boolean) is abstract;

   function GetPreferredSize (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Awt.Dimension.Typ'Class is abstract;

   function GetMinimumSize (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Awt.Dimension.Typ'Class is abstract;

   procedure AddItem (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int) is abstract;

   procedure Clear (This : access Typ) is abstract;

   procedure SetMultipleSelections (This : access Typ;
                                    P1_Boolean : Java.Boolean) is abstract;

   function PreferredSize (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Awt.Dimension.Typ'Class is abstract;

   function MinimumSize (This : access Typ;
                         P1_Int : Java.Int)
                         return access Java.Awt.Dimension.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSelectedIndexes, "getSelectedIndexes");
   pragma Export (Java, Add, "add");
   pragma Export (Java, DelItems, "delItems");
   pragma Export (Java, RemoveAll, "removeAll");
   pragma Export (Java, select_K, "select");
   pragma Export (Java, Deselect, "deselect");
   pragma Export (Java, MakeVisible, "makeVisible");
   pragma Export (Java, SetMultipleMode, "setMultipleMode");
   pragma Export (Java, GetPreferredSize, "getPreferredSize");
   pragma Export (Java, GetMinimumSize, "getMinimumSize");
   pragma Export (Java, AddItem, "addItem");
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, SetMultipleSelections, "setMultipleSelections");
   pragma Export (Java, PreferredSize, "preferredSize");
   pragma Export (Java, MinimumSize, "minimumSize");

end Java.Awt.Peer.ListPeer;
pragma Import (Java, Java.Awt.Peer.ListPeer, "java.awt.peer.ListPeer");
pragma Extensions_Allowed (Off);
