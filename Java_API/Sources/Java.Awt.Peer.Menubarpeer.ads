pragma Extensions_Allowed (On);
limited with Java.Awt.Menu;
with Java.Awt.Peer.MenuComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.MenuBarPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MenuComponentPeer_I : Java.Awt.Peer.MenuComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddMenu (This : access Typ;
                      P1_Menu : access Standard.Java.Awt.Menu.Typ'Class) is abstract;

   procedure DelMenu (This : access Typ;
                      P1_Int : Java.Int) is abstract;

   procedure AddHelpMenu (This : access Typ;
                          P1_Menu : access Standard.Java.Awt.Menu.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddMenu, "addMenu");
   pragma Export (Java, DelMenu, "delMenu");
   pragma Export (Java, AddHelpMenu, "addHelpMenu");

end Java.Awt.Peer.MenuBarPeer;
pragma Import (Java, Java.Awt.Peer.MenuBarPeer, "java.awt.peer.MenuBarPeer");
pragma Extensions_Allowed (Off);
