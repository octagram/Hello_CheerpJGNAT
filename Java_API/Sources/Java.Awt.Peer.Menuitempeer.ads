pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Peer.MenuComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.MenuItemPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MenuComponentPeer_I : Java.Awt.Peer.MenuComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetLabel (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   procedure Enable (This : access Typ) is abstract;

   procedure Disable (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetLabel, "setLabel");
   pragma Export (Java, SetEnabled, "setEnabled");
   pragma Export (Java, Enable, "enable");
   pragma Export (Java, Disable, "disable");

end Java.Awt.Peer.MenuItemPeer;
pragma Import (Java, Java.Awt.Peer.MenuItemPeer, "java.awt.peer.MenuItemPeer");
pragma Extensions_Allowed (Off);
