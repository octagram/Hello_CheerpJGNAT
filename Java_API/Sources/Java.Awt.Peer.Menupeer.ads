pragma Extensions_Allowed (On);
limited with Java.Awt.MenuItem;
with Java.Awt.Peer.MenuItemPeer;
with Java.Lang.Object;

package Java.Awt.Peer.MenuPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MenuItemPeer_I : Java.Awt.Peer.MenuItemPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddSeparator (This : access Typ) is abstract;

   procedure AddItem (This : access Typ;
                      P1_MenuItem : access Standard.Java.Awt.MenuItem.Typ'Class) is abstract;

   procedure DelItem (This : access Typ;
                      P1_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddSeparator, "addSeparator");
   pragma Export (Java, AddItem, "addItem");
   pragma Export (Java, DelItem, "delItem");

end Java.Awt.Peer.MenuPeer;
pragma Import (Java, Java.Awt.Peer.MenuPeer, "java.awt.peer.MenuPeer");
pragma Extensions_Allowed (Off);
