pragma Extensions_Allowed (On);
limited with Java.Awt.Point;
limited with Java.Awt.Window;
with Java.Lang.Object;

package Java.Awt.Peer.MouseInfoPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function FillPointWithCoords (This : access Typ;
                                 P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                                 return Java.Int is abstract;

   function IsWindowUnderMouse (This : access Typ;
                                P1_Window : access Standard.Java.Awt.Window.Typ'Class)
                                return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, FillPointWithCoords, "fillPointWithCoords");
   pragma Export (Java, IsWindowUnderMouse, "isWindowUnderMouse");

end Java.Awt.Peer.MouseInfoPeer;
pragma Import (Java, Java.Awt.Peer.MouseInfoPeer, "java.awt.peer.MouseInfoPeer");
pragma Extensions_Allowed (Off);
