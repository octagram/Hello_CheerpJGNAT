pragma Extensions_Allowed (On);
limited with Java.Awt.Event;
with Java.Awt.Peer.MenuPeer;
with Java.Lang.Object;

package Java.Awt.Peer.PopupMenuPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MenuPeer_I : Java.Awt.Peer.MenuPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Show (This : access Typ;
                   P1_Event : access Standard.Java.Awt.Event.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Show, "show");

end Java.Awt.Peer.PopupMenuPeer;
pragma Import (Java, Java.Awt.Peer.PopupMenuPeer, "java.awt.peer.PopupMenuPeer");
pragma Extensions_Allowed (Off);
