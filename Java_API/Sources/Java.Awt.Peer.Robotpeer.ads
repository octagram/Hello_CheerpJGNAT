pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
with Java.Lang.Object;

package Java.Awt.Peer.RobotPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseMove (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int) is abstract;

   procedure MousePress (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   procedure MouseRelease (This : access Typ;
                           P1_Int : Java.Int) is abstract;

   procedure MouseWheel (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   procedure KeyPress (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   procedure KeyRelease (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   function GetRGBPixel (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return Java.Int is abstract;

   function GetRGBPixels (This : access Typ;
                          P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                          return Java.Int_Arr is abstract;

   procedure Dispose (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, MouseMove, "mouseMove");
   pragma Export (Java, MousePress, "mousePress");
   pragma Export (Java, MouseRelease, "mouseRelease");
   pragma Export (Java, MouseWheel, "mouseWheel");
   pragma Export (Java, KeyPress, "keyPress");
   pragma Export (Java, KeyRelease, "keyRelease");
   pragma Export (Java, GetRGBPixel, "getRGBPixel");
   pragma Export (Java, GetRGBPixels, "getRGBPixels");
   pragma Export (Java, Dispose, "dispose");

end Java.Awt.Peer.RobotPeer;
pragma Import (Java, Java.Awt.Peer.RobotPeer, "java.awt.peer.RobotPeer");
pragma Extensions_Allowed (Off);
