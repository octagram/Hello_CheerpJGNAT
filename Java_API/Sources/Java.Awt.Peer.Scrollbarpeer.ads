pragma Extensions_Allowed (On);
with Java.Awt.Peer.ComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.ScrollbarPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ComponentPeer_I : Java.Awt.Peer.ComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetValues (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int) is abstract;

   procedure SetLineIncrement (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   procedure SetPageIncrement (This : access Typ;
                               P1_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetValues, "setValues");
   pragma Export (Java, SetLineIncrement, "setLineIncrement");
   pragma Export (Java, SetPageIncrement, "setPageIncrement");

end Java.Awt.Peer.ScrollbarPeer;
pragma Import (Java, Java.Awt.Peer.ScrollbarPeer, "java.awt.peer.ScrollbarPeer");
pragma Extensions_Allowed (Off);
