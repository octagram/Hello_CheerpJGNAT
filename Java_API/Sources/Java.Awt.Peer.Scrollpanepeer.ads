pragma Extensions_Allowed (On);
limited with Java.Awt.Adjustable;
with Java.Awt.Peer.ContainerPeer;
with Java.Lang.Object;

package Java.Awt.Peer.ScrollPanePeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ContainerPeer_I : Java.Awt.Peer.ContainerPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHScrollbarHeight (This : access Typ)
                                 return Java.Int is abstract;

   function GetVScrollbarWidth (This : access Typ)
                                return Java.Int is abstract;

   procedure SetScrollPosition (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int) is abstract;

   procedure ChildResized (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int) is abstract;

   procedure SetUnitIncrement (This : access Typ;
                               P1_Adjustable : access Standard.Java.Awt.Adjustable.Typ'Class;
                               P2_Int : Java.Int) is abstract;

   procedure SetValue (This : access Typ;
                       P1_Adjustable : access Standard.Java.Awt.Adjustable.Typ'Class;
                       P2_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetHScrollbarHeight, "getHScrollbarHeight");
   pragma Export (Java, GetVScrollbarWidth, "getVScrollbarWidth");
   pragma Export (Java, SetScrollPosition, "setScrollPosition");
   pragma Export (Java, ChildResized, "childResized");
   pragma Export (Java, SetUnitIncrement, "setUnitIncrement");
   pragma Export (Java, SetValue, "setValue");

end Java.Awt.Peer.ScrollPanePeer;
pragma Import (Java, Java.Awt.Peer.ScrollPanePeer, "java.awt.peer.ScrollPanePeer");
pragma Extensions_Allowed (Off);
