pragma Extensions_Allowed (On);
limited with Java.Awt.Im.InputMethodRequests;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
with Java.Awt.Peer.ComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.TextComponentPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ComponentPeer_I : Java.Awt.Peer.ComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetEditable (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSelectionStart (This : access Typ)
                               return Java.Int is abstract;

   function GetSelectionEnd (This : access Typ)
                             return Java.Int is abstract;

   procedure select_K (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int) is abstract;

   procedure SetCaretPosition (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   function GetCaretPosition (This : access Typ)
                              return Java.Int is abstract;

   function GetIndexAtPoint (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return Java.Int is abstract;

   function GetCharacterBounds (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Awt.Rectangle.Typ'Class is abstract;

   function FilterEvents (This : access Typ;
                          P1_Long : Java.Long)
                          return Java.Long is abstract;

   function GetInputMethodRequests (This : access Typ)
                                    return access Java.Awt.Im.InputMethodRequests.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetEditable, "setEditable");
   pragma Export (Java, GetText, "getText");
   pragma Export (Java, SetText, "setText");
   pragma Export (Java, GetSelectionStart, "getSelectionStart");
   pragma Export (Java, GetSelectionEnd, "getSelectionEnd");
   pragma Export (Java, select_K, "select");
   pragma Export (Java, SetCaretPosition, "setCaretPosition");
   pragma Export (Java, GetCaretPosition, "getCaretPosition");
   pragma Export (Java, GetIndexAtPoint, "getIndexAtPoint");
   pragma Export (Java, GetCharacterBounds, "getCharacterBounds");
   pragma Export (Java, FilterEvents, "filterEvents");
   pragma Export (Java, GetInputMethodRequests, "getInputMethodRequests");

end Java.Awt.Peer.TextComponentPeer;
pragma Import (Java, Java.Awt.Peer.TextComponentPeer, "java.awt.peer.TextComponentPeer");
pragma Extensions_Allowed (Off);
