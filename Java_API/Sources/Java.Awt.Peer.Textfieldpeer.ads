pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
with Java.Awt.Peer.TextComponentPeer;
with Java.Lang.Object;

package Java.Awt.Peer.TextFieldPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            TextComponentPeer_I : Java.Awt.Peer.TextComponentPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetEchoChar (This : access Typ;
                          P1_Char : Java.Char) is abstract;

   function GetPreferredSize (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Awt.Dimension.Typ'Class is abstract;

   function GetMinimumSize (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Awt.Dimension.Typ'Class is abstract;

   procedure SetEchoCharacter (This : access Typ;
                               P1_Char : Java.Char) is abstract;

   function PreferredSize (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Awt.Dimension.Typ'Class is abstract;

   function MinimumSize (This : access Typ;
                         P1_Int : Java.Int)
                         return access Java.Awt.Dimension.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetEchoChar, "setEchoChar");
   pragma Export (Java, GetPreferredSize, "getPreferredSize");
   pragma Export (Java, GetMinimumSize, "getMinimumSize");
   pragma Export (Java, SetEchoCharacter, "setEchoCharacter");
   pragma Export (Java, PreferredSize, "preferredSize");
   pragma Export (Java, MinimumSize, "minimumSize");

end Java.Awt.Peer.TextFieldPeer;
pragma Import (Java, Java.Awt.Peer.TextFieldPeer, "java.awt.peer.TextFieldPeer");
pragma Extensions_Allowed (Off);
