pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.Peer.TrayIconPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Dispose (This : access Typ) is abstract;

   procedure SetToolTip (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure UpdateImage (This : access Typ) is abstract;

   procedure DisplayMessage (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure ShowPopupMenu (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Dispose, "dispose");
   pragma Export (Java, SetToolTip, "setToolTip");
   pragma Export (Java, UpdateImage, "updateImage");
   pragma Export (Java, DisplayMessage, "displayMessage");
   pragma Export (Java, ShowPopupMenu, "showPopupMenu");

end Java.Awt.Peer.TrayIconPeer;
pragma Import (Java, Java.Awt.Peer.TrayIconPeer, "java.awt.peer.TrayIconPeer");
pragma Extensions_Allowed (Off);
