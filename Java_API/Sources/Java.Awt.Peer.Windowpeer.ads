pragma Extensions_Allowed (On);
limited with Java.Awt.Dialog;
with Java.Awt.Peer.ContainerPeer;
with Java.Lang.Object;

package Java.Awt.Peer.WindowPeer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ContainerPeer_I : Java.Awt.Peer.ContainerPeer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ToFront (This : access Typ) is abstract;

   procedure ToBack (This : access Typ) is abstract;

   procedure UpdateAlwaysOnTopState (This : access Typ) is abstract;

   procedure UpdateFocusableWindowState (This : access Typ) is abstract;

   function RequestWindowFocus (This : access Typ)
                                return Java.Boolean is abstract;

   procedure SetModalBlocked (This : access Typ;
                              P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class;
                              P2_Boolean : Java.Boolean) is abstract;

   procedure UpdateMinimumSize (This : access Typ) is abstract;

   procedure UpdateIconImages (This : access Typ) is abstract;

   procedure SetOpacity (This : access Typ;
                         P1_Float : Java.Float) is abstract;

   procedure SetOpaque (This : access Typ;
                        P1_Boolean : Java.Boolean) is abstract;

   procedure UpdateWindow (This : access Typ) is abstract;

   procedure RepositionSecurityWarning (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ToFront, "toFront");
   pragma Export (Java, ToBack, "toBack");
   pragma Export (Java, UpdateAlwaysOnTopState, "updateAlwaysOnTopState");
   pragma Export (Java, UpdateFocusableWindowState, "updateFocusableWindowState");
   pragma Export (Java, RequestWindowFocus, "requestWindowFocus");
   pragma Export (Java, SetModalBlocked, "setModalBlocked");
   pragma Export (Java, UpdateMinimumSize, "updateMinimumSize");
   pragma Export (Java, UpdateIconImages, "updateIconImages");
   pragma Export (Java, SetOpacity, "setOpacity");
   pragma Export (Java, SetOpaque, "setOpaque");
   pragma Export (Java, UpdateWindow, "updateWindow");
   pragma Export (Java, RepositionSecurityWarning, "repositionSecurityWarning");

end Java.Awt.Peer.WindowPeer;
pragma Import (Java, Java.Awt.Peer.WindowPeer, "java.awt.peer.WindowPeer");
pragma Extensions_Allowed (Off);
