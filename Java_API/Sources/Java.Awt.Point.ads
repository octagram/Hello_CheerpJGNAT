pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Geom.Point2D;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Point is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Point2D.Typ(Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      X : Java.Int;
      pragma Import (Java, X, "x");

      Y : Java.Int;
      pragma Import (Java, Y, "y");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Point (This : Ref := null)
                       return Ref;

   function New_Point (P1_Point : access Standard.Java.Awt.Point.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_Point (P1_Int : Java.Int;
                       P2_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return Java.Double;

   function GetY (This : access Typ)
                  return Java.Double;

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   procedure SetLocation (This : access Typ;
                          P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   procedure SetLocation (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);

   procedure SetLocation (This : access Typ;
                          P1_Double : Java.Double;
                          P2_Double : Java.Double);

   procedure Move (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Int : Java.Int);

   procedure Translate (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Point);
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, Move, "move");
   pragma Import (Java, Translate, "translate");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Point;
pragma Import (Java, Java.Awt.Point, "java.awt.Point");
pragma Extensions_Allowed (Off);
