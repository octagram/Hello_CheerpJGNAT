pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.PathIterator;
limited with Java.Awt.Geom.Point2D;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Polygon is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Npoints : Java.Int;
      pragma Import (Java, Npoints, "npoints");

      Xpoints : Java.Int_Arr;
      pragma Import (Java, Xpoints, "xpoints");

      Ypoints : Java.Int_Arr;
      pragma Import (Java, Ypoints, "ypoints");

      --  protected
      Bounds : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, Bounds, "bounds");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Polygon (This : Ref := null)
                         return Ref;

   function New_Polygon (P1_Int_Arr : Java.Int_Arr;
                         P2_Int_Arr : Java.Int_Arr;
                         P3_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ);

   procedure Invalidate (This : access Typ);

   procedure Translate (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   procedure AddPoint (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int);

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   function Contains (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return Java.Boolean;

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Point2D : access Standard.Java.Awt.Geom.Point2D.Typ'Class)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Double : Java.Double;
                        P2_Double : Java.Double;
                        P3_Double : Java.Double;
                        P4_Double : Java.Double)
                        return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                        return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                      return Java.Boolean;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;

   function GetPathIterator (This : access Typ;
                             P1_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                             P2_Double : Java.Double)
                             return access Java.Awt.Geom.PathIterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Polygon);
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Invalidate, "invalidate");
   pragma Import (Java, Translate, "translate");
   pragma Import (Java, AddPoint, "addPoint");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, GetPathIterator, "getPathIterator");

end Java.Awt.Polygon;
pragma Import (Java, Java.Awt.Polygon, "java.awt.Polygon");
pragma Extensions_Allowed (Off);
