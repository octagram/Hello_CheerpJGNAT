pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Menu;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.PopupMenu is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Menu.Typ(MenuContainer_I,
                             Serializable_I,
                             Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PopupMenu (This : Ref := null)
                           return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_PopupMenu (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParent (This : access Typ)
                       return access Java.Awt.MenuContainer.Typ'Class;

   procedure AddNotify (This : access Typ);

   procedure Show (This : access Typ;
                   P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PopupMenu);
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, Show, "show");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.PopupMenu;
pragma Import (Java, Java.Awt.PopupMenu, "java.awt.PopupMenu");
pragma Extensions_Allowed (Off);
