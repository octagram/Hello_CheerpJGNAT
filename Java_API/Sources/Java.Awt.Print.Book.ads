pragma Extensions_Allowed (On);
limited with Java.Awt.Print.PageFormat;
limited with Java.Awt.Print.Printable;
with Java.Awt.Print.Pageable;
with Java.Lang.Object;

package Java.Awt.Print.Book is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Pageable_I : Java.Awt.Print.Pageable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Book (This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNumberOfPages (This : access Typ)
                              return Java.Int;

   function GetPageFormat (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Awt.Print.PageFormat.Typ'Class;
   --  can raise Java.Lang.IndexOutOfBoundsException.Except

   function GetPrintable (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Awt.Print.Printable.Typ'Class;
   --  can raise Java.Lang.IndexOutOfBoundsException.Except

   procedure SetPage (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Printable : access Standard.Java.Awt.Print.Printable.Typ'Class;
                      P3_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class);
   --  can raise Java.Lang.IndexOutOfBoundsException.Except

   procedure Append (This : access Typ;
                     P1_Printable : access Standard.Java.Awt.Print.Printable.Typ'Class;
                     P2_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class);

   procedure Append (This : access Typ;
                     P1_Printable : access Standard.Java.Awt.Print.Printable.Typ'Class;
                     P2_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class;
                     P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Book);
   pragma Import (Java, GetNumberOfPages, "getNumberOfPages");
   pragma Import (Java, GetPageFormat, "getPageFormat");
   pragma Import (Java, GetPrintable, "getPrintable");
   pragma Import (Java, SetPage, "setPage");
   pragma Import (Java, Append, "append");

end Java.Awt.Print.Book;
pragma Import (Java, Java.Awt.Print.Book, "java.awt.print.Book");
pragma Extensions_Allowed (Off);
