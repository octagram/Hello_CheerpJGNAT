pragma Extensions_Allowed (On);
limited with Java.Awt.Print.PageFormat;
limited with Java.Awt.Print.Printable;
with Java.Lang.Object;

package Java.Awt.Print.Pageable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNumberOfPages (This : access Typ)
                              return Java.Int is abstract;

   function GetPageFormat (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Awt.Print.PageFormat.Typ'Class is abstract;
   --  can raise Java.Lang.IndexOutOfBoundsException.Except

   function GetPrintable (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Awt.Print.Printable.Typ'Class is abstract;
   --  can raise Java.Lang.IndexOutOfBoundsException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNKNOWN_NUMBER_OF_PAGES : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetNumberOfPages, "getNumberOfPages");
   pragma Export (Java, GetPageFormat, "getPageFormat");
   pragma Export (Java, GetPrintable, "getPrintable");
   pragma Import (Java, UNKNOWN_NUMBER_OF_PAGES, "UNKNOWN_NUMBER_OF_PAGES");

end Java.Awt.Print.Pageable;
pragma Import (Java, Java.Awt.Print.Pageable, "java.awt.print.Pageable");
pragma Extensions_Allowed (Off);
