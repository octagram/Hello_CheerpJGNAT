pragma Extensions_Allowed (On);
limited with Java.Awt.Print.Paper;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Print.PageFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PageFormat (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetWidth (This : access Typ)
                      return Java.Double;

   function GetHeight (This : access Typ)
                       return Java.Double;

   function GetImageableX (This : access Typ)
                           return Java.Double;

   function GetImageableY (This : access Typ)
                           return Java.Double;

   function GetImageableWidth (This : access Typ)
                               return Java.Double;

   function GetImageableHeight (This : access Typ)
                                return Java.Double;

   function GetPaper (This : access Typ)
                      return access Java.Awt.Print.Paper.Typ'Class;

   procedure SetPaper (This : access Typ;
                       P1_Paper : access Standard.Java.Awt.Print.Paper.Typ'Class);

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetOrientation (This : access Typ)
                            return Java.Int;

   function GetMatrix (This : access Typ)
                       return Java.Double_Arr;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LANDSCAPE : constant Java.Int;

   --  final
   PORTRAIT : constant Java.Int;

   --  final
   REVERSE_LANDSCAPE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PageFormat);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetImageableX, "getImageableX");
   pragma Import (Java, GetImageableY, "getImageableY");
   pragma Import (Java, GetImageableWidth, "getImageableWidth");
   pragma Import (Java, GetImageableHeight, "getImageableHeight");
   pragma Import (Java, GetPaper, "getPaper");
   pragma Import (Java, SetPaper, "setPaper");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, GetMatrix, "getMatrix");
   pragma Import (Java, LANDSCAPE, "LANDSCAPE");
   pragma Import (Java, PORTRAIT, "PORTRAIT");
   pragma Import (Java, REVERSE_LANDSCAPE, "REVERSE_LANDSCAPE");

end Java.Awt.Print.PageFormat;
pragma Import (Java, Java.Awt.Print.PageFormat, "java.awt.print.PageFormat");
pragma Extensions_Allowed (Off);
