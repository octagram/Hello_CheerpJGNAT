pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Print.Paper is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Paper (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetHeight (This : access Typ)
                       return Java.Double;

   procedure SetSize (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double);

   function GetWidth (This : access Typ)
                      return Java.Double;

   procedure SetImageableArea (This : access Typ;
                               P1_Double : Java.Double;
                               P2_Double : Java.Double;
                               P3_Double : Java.Double;
                               P4_Double : Java.Double);

   function GetImageableX (This : access Typ)
                           return Java.Double;

   function GetImageableY (This : access Typ)
                           return Java.Double;

   function GetImageableWidth (This : access Typ)
                               return Java.Double;

   function GetImageableHeight (This : access Typ)
                                return Java.Double;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Paper);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, SetImageableArea, "setImageableArea");
   pragma Import (Java, GetImageableX, "getImageableX");
   pragma Import (Java, GetImageableY, "getImageableY");
   pragma Import (Java, GetImageableWidth, "getImageableWidth");
   pragma Import (Java, GetImageableHeight, "getImageableHeight");

end Java.Awt.Print.Paper;
pragma Import (Java, Java.Awt.Print.Paper, "java.awt.print.Paper");
pragma Extensions_Allowed (Off);
