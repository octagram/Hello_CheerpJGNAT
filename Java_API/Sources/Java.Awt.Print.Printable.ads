pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Print.PageFormat;
with Java.Lang.Object;

package Java.Awt.Print.Printable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Print (This : access Typ;
                   P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                   P2_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class;
                   P3_Int : Java.Int)
                   return Java.Int is abstract;
   --  can raise Java.Awt.Print.PrinterException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PAGE_EXISTS : constant Java.Int;

   --  final
   NO_SUCH_PAGE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Print, "print");
   pragma Import (Java, PAGE_EXISTS, "PAGE_EXISTS");
   pragma Import (Java, NO_SUCH_PAGE, "NO_SUCH_PAGE");

end Java.Awt.Print.Printable;
pragma Import (Java, Java.Awt.Print.Printable, "java.awt.print.Printable");
pragma Extensions_Allowed (Off);
