pragma Extensions_Allowed (On);
limited with Java.Io.IOException;
limited with Java.Lang.Throwable;
with Java.Awt.Print.PrinterException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.Print.PrinterIOException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Print.PrinterException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrinterIOException (P1_IOException : access Standard.Java.Io.IOException.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIOException (This : access Typ)
                            return access Java.Io.IOException.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.awt.print.PrinterIOException");
   pragma Java_Constructor (New_PrinterIOException);
   pragma Import (Java, GetIOException, "getIOException");
   pragma Import (Java, GetCause, "getCause");

end Java.Awt.Print.PrinterIOException;
pragma Import (Java, Java.Awt.Print.PrinterIOException, "java.awt.print.PrinterIOException");
pragma Extensions_Allowed (Off);
