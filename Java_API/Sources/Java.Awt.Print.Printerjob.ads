pragma Extensions_Allowed (On);
limited with Java.Awt.Print.PageFormat;
limited with Java.Awt.Print.Pageable;
limited with Java.Awt.Print.Printable;
limited with Java.Lang.String;
limited with Javax.Print.Attribute.PrintRequestAttributeSet;
limited with Javax.Print.PrintService;
limited with Javax.Print.StreamPrintServiceFactory;
with Java.Lang.Object;

package Java.Awt.Print.PrinterJob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrinterJob return access Java.Awt.Print.PrinterJob.Typ'Class;

   function LookupPrintServices return Standard.Java.Lang.Object.Ref;

   function LookupStreamPrintServices (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                       return Standard.Java.Lang.Object.Ref;

   function New_PrinterJob (This : Ref := null)
                            return Ref;

   function GetPrintService (This : access Typ)
                             return access Javax.Print.PrintService.Typ'Class;

   procedure SetPrintService (This : access Typ;
                              P1_PrintService : access Standard.Javax.Print.PrintService.Typ'Class);
   --  can raise Java.Awt.Print.PrinterException.Except

   procedure SetPrintable (This : access Typ;
                           P1_Printable : access Standard.Java.Awt.Print.Printable.Typ'Class) is abstract;

   procedure SetPrintable (This : access Typ;
                           P1_Printable : access Standard.Java.Awt.Print.Printable.Typ'Class;
                           P2_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class) is abstract;

   procedure SetPageable (This : access Typ;
                          P1_Pageable : access Standard.Java.Awt.Print.Pageable.Typ'Class) is abstract;
   --  can raise Java.Lang.NullPointerException.Except

   function PrintDialog (This : access Typ)
                         return Java.Boolean is abstract
                         with Export => "printDialog", Convention => Java;
   --  can raise Java.Awt.HeadlessException.Except

   function PrintDialog (This : access Typ;
                         P1_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class)
                         return Java.Boolean
                         with Import => "printDialog", Convention => Java;
   --  can raise Java.Awt.HeadlessException.Except

   function PageDialog (This : access Typ;
                        P1_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class)
                        return access Java.Awt.Print.PageFormat.Typ'Class is abstract
                        with Export => "pageDialog", Convention => Java;
   --  can raise Java.Awt.HeadlessException.Except

   function PageDialog (This : access Typ;
                        P1_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class)
                        return access Java.Awt.Print.PageFormat.Typ'Class
                        with Import => "pageDialog", Convention => Java;
   --  can raise Java.Awt.HeadlessException.Except

   function DefaultPage (This : access Typ;
                         P1_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class)
                         return access Java.Awt.Print.PageFormat.Typ'Class is abstract
                         with Export => "defaultPage", Convention => Java;

   function DefaultPage (This : access Typ)
                         return access Java.Awt.Print.PageFormat.Typ'Class
                         with Import => "defaultPage", Convention => Java;

   function GetPageFormat (This : access Typ;
                           P1_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class)
                           return access Java.Awt.Print.PageFormat.Typ'Class;

   function ValidatePage (This : access Typ;
                          P1_PageFormat : access Standard.Java.Awt.Print.PageFormat.Typ'Class)
                          return access Java.Awt.Print.PageFormat.Typ'Class is abstract;

   procedure Print (This : access Typ) is abstract
                    with Export => "print", Convention => Java;
   --  can raise Java.Awt.Print.PrinterException.Except

   procedure Print (This : access Typ;
                    P1_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class)
                    with Import => "print", Convention => Java;
   --  can raise Java.Awt.Print.PrinterException.Except

   procedure SetCopies (This : access Typ;
                        P1_Int : Java.Int) is abstract;

   function GetCopies (This : access Typ)
                       return Java.Int is abstract;

   function GetUserName (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetJobName (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetJobName (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure Cancel (This : access Typ) is abstract;

   function IsCancelled (This : access Typ)
                         return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetPrinterJob, "getPrinterJob");
   pragma Import (Java, LookupPrintServices, "lookupPrintServices");
   pragma Import (Java, LookupStreamPrintServices, "lookupStreamPrintServices");
   pragma Java_Constructor (New_PrinterJob);
   pragma Import (Java, GetPrintService, "getPrintService");
   pragma Import (Java, SetPrintService, "setPrintService");
   pragma Export (Java, SetPrintable, "setPrintable");
   pragma Export (Java, SetPageable, "setPageable");
   -- pragma Import (Java, PrintDialog, "printDialog");
   -- pragma Import (Java, PageDialog, "pageDialog");
   -- pragma Import (Java, DefaultPage, "defaultPage");
   pragma Import (Java, GetPageFormat, "getPageFormat");
   pragma Export (Java, ValidatePage, "validatePage");
   -- pragma Import (Java, Print, "print");
   pragma Export (Java, SetCopies, "setCopies");
   pragma Export (Java, GetCopies, "getCopies");
   pragma Export (Java, GetUserName, "getUserName");
   pragma Export (Java, SetJobName, "setJobName");
   pragma Export (Java, GetJobName, "getJobName");
   pragma Export (Java, Cancel, "cancel");
   pragma Export (Java, IsCancelled, "isCancelled");

end Java.Awt.Print.PrinterJob;
pragma Import (Java, Java.Awt.Print.PrinterJob, "java.awt.print.PrinterJob");
pragma Extensions_Allowed (Off);
