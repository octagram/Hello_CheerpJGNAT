pragma Extensions_Allowed (On);
package Java.Awt.Print is
   pragma Preelaborate;
end Java.Awt.Print;
pragma Import (Java, Java.Awt.Print, "java.awt.print");
pragma Extensions_Allowed (Off);
