pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
with Java.Lang.Object;

package Java.Awt.PrintJob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_PrintJob (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class is abstract;

   function GetPageDimension (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class is abstract;

   function GetPageResolution (This : access Typ)
                               return Java.Int is abstract;

   function LastPageFirst (This : access Typ)
                           return Java.Boolean is abstract;

   procedure end_K (This : access Typ) is abstract;

   procedure Finalize (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintJob);
   pragma Export (Java, GetGraphics, "getGraphics");
   pragma Export (Java, GetPageDimension, "getPageDimension");
   pragma Export (Java, GetPageResolution, "getPageResolution");
   pragma Export (Java, LastPageFirst, "lastPageFirst");
   pragma Export (Java, end_K, "end");
   pragma Export (Java, Finalize, "finalize");

end Java.Awt.PrintJob;
pragma Import (Java, Java.Awt.PrintJob, "java.awt.PrintJob");
pragma Extensions_Allowed (Off);
