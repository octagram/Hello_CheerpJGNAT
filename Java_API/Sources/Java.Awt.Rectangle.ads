pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Point;
limited with Java.Lang.String;
with Java.Awt.Geom.Rectangle2D;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Awt.Rectangle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Geom.Rectangle2D.Typ(Shape_I,
                                         Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      X : Java.Int;
      pragma Import (Java, X, "x");

      Y : Java.Int;
      pragma Import (Java, Y, "y");

      Width : Java.Int;
      pragma Import (Java, Width, "width");

      Height : Java.Int;
      pragma Import (Java, Height, "height");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Rectangle (This : Ref := null)
                           return Ref;

   function New_Rectangle (P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Rectangle (P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_Rectangle (P1_Int : Java.Int;
                           P2_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_Rectangle (P1_Point : access Standard.Java.Awt.Point.Typ'Class;
                           P2_Dimension : access Standard.Java.Awt.Dimension.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Rectangle (P1_Point : access Standard.Java.Awt.Point.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Rectangle (P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return Java.Double;

   function GetY (This : access Typ)
                  return Java.Double;

   function GetWidth (This : access Typ)
                      return Java.Double;

   function GetHeight (This : access Typ)
                       return Java.Double;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   function GetBounds2D (This : access Typ)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   procedure SetBounds (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure SetBounds (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure SetRect (This : access Typ;
                      P1_Double : Java.Double;
                      P2_Double : Java.Double;
                      P3_Double : Java.Double;
                      P4_Double : Java.Double);

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   procedure SetLocation (This : access Typ;
                          P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   procedure SetLocation (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);

   procedure Translate (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int);

   function Contains (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int)
                      return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                        return Java.Boolean;

   function Intersection (This : access Typ;
                          P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                          return access Java.Awt.Rectangle.Typ'Class;

   function Union (This : access Typ;
                   P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                   return access Java.Awt.Rectangle.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int);

   procedure Add (This : access Typ;
                  P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   procedure Add (This : access Typ;
                  P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure Grow (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Int : Java.Int);

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Outcode (This : access Typ;
                     P1_Double : Java.Double;
                     P2_Double : Java.Double)
                     return Java.Int;

   function CreateIntersection (This : access Typ;
                                P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                                return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function CreateUnion (This : access Typ;
                         P1_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class)
                         return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Rectangle);
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetBounds2D, "getBounds2D");
   pragma Import (Java, SetBounds, "setBounds");
   pragma Import (Java, SetRect, "setRect");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, Translate, "translate");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, Intersection, "intersection");
   pragma Import (Java, Union, "union");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Grow, "grow");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Outcode, "outcode");
   pragma Import (Java, CreateIntersection, "createIntersection");
   pragma Import (Java, CreateUnion, "createUnion");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Rectangle;
pragma Import (Java, Java.Awt.Rectangle, "java.awt.Rectangle");
pragma Extensions_Allowed (Off);
