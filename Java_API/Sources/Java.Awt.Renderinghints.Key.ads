pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.RenderingHints.Key is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Key (P1_Int : Java.Int; 
                     This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsCompatibleValue (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean is abstract;

   --  final  protected
   function IntKey (This : access Typ)
                    return Java.Int;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Key);
   pragma Export (Java, IsCompatibleValue, "isCompatibleValue");
   pragma Export (Java, IntKey, "intKey");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");

end Java.Awt.RenderingHints.Key;
pragma Import (Java, Java.Awt.RenderingHints.Key, "java.awt.RenderingHints$Key");
pragma Extensions_Allowed (Off);
