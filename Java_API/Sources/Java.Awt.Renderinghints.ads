pragma Extensions_Allowed (On);
limited with Java.Awt.RenderingHints.Key;
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Java.Util.Set;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Map;

package Java.Awt.RenderingHints is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RenderingHints (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_RenderingHints (P1_Key : access Standard.Java.Awt.RenderingHints.Key.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Add (This : access Typ;
                  P1_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class);

   procedure Clear (This : access Typ);

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   KEY_ANTIALIASING : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_ANTIALIAS_ON : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_ANTIALIAS_OFF : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_ANTIALIAS_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_RENDERING : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_RENDER_SPEED : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_RENDER_QUALITY : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_RENDER_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_DITHERING : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_DITHER_DISABLE : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_DITHER_ENABLE : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_DITHER_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_TEXT_ANTIALIASING : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_ON : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_OFF : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_GASP : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_LCD_HRGB : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_LCD_HBGR : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_LCD_VRGB : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_TEXT_ANTIALIAS_LCD_VBGR : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_TEXT_LCD_CONTRAST : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   KEY_FRACTIONALMETRICS : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_FRACTIONALMETRICS_OFF : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_FRACTIONALMETRICS_ON : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_FRACTIONALMETRICS_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_INTERPOLATION : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_INTERPOLATION_NEAREST_NEIGHBOR : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_INTERPOLATION_BILINEAR : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_INTERPOLATION_BICUBIC : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_ALPHA_INTERPOLATION : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_ALPHA_INTERPOLATION_SPEED : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_ALPHA_INTERPOLATION_QUALITY : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_ALPHA_INTERPOLATION_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_COLOR_RENDERING : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_COLOR_RENDER_SPEED : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_COLOR_RENDER_QUALITY : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_COLOR_RENDER_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   KEY_STROKE_CONTROL : access Java.Awt.RenderingHints.Key.Typ'Class;

   --  final
   VALUE_STROKE_DEFAULT : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_STROKE_NORMALIZE : access Java.Lang.Object.Typ'Class;

   --  final
   VALUE_STROKE_PURE : access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RenderingHints);
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, KEY_ANTIALIASING, "KEY_ANTIALIASING");
   pragma Import (Java, VALUE_ANTIALIAS_ON, "VALUE_ANTIALIAS_ON");
   pragma Import (Java, VALUE_ANTIALIAS_OFF, "VALUE_ANTIALIAS_OFF");
   pragma Import (Java, VALUE_ANTIALIAS_DEFAULT, "VALUE_ANTIALIAS_DEFAULT");
   pragma Import (Java, KEY_RENDERING, "KEY_RENDERING");
   pragma Import (Java, VALUE_RENDER_SPEED, "VALUE_RENDER_SPEED");
   pragma Import (Java, VALUE_RENDER_QUALITY, "VALUE_RENDER_QUALITY");
   pragma Import (Java, VALUE_RENDER_DEFAULT, "VALUE_RENDER_DEFAULT");
   pragma Import (Java, KEY_DITHERING, "KEY_DITHERING");
   pragma Import (Java, VALUE_DITHER_DISABLE, "VALUE_DITHER_DISABLE");
   pragma Import (Java, VALUE_DITHER_ENABLE, "VALUE_DITHER_ENABLE");
   pragma Import (Java, VALUE_DITHER_DEFAULT, "VALUE_DITHER_DEFAULT");
   pragma Import (Java, KEY_TEXT_ANTIALIASING, "KEY_TEXT_ANTIALIASING");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_ON, "VALUE_TEXT_ANTIALIAS_ON");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_OFF, "VALUE_TEXT_ANTIALIAS_OFF");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_DEFAULT, "VALUE_TEXT_ANTIALIAS_DEFAULT");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_GASP, "VALUE_TEXT_ANTIALIAS_GASP");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_LCD_HRGB, "VALUE_TEXT_ANTIALIAS_LCD_HRGB");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_LCD_HBGR, "VALUE_TEXT_ANTIALIAS_LCD_HBGR");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_LCD_VRGB, "VALUE_TEXT_ANTIALIAS_LCD_VRGB");
   pragma Import (Java, VALUE_TEXT_ANTIALIAS_LCD_VBGR, "VALUE_TEXT_ANTIALIAS_LCD_VBGR");
   pragma Import (Java, KEY_TEXT_LCD_CONTRAST, "KEY_TEXT_LCD_CONTRAST");
   pragma Import (Java, KEY_FRACTIONALMETRICS, "KEY_FRACTIONALMETRICS");
   pragma Import (Java, VALUE_FRACTIONALMETRICS_OFF, "VALUE_FRACTIONALMETRICS_OFF");
   pragma Import (Java, VALUE_FRACTIONALMETRICS_ON, "VALUE_FRACTIONALMETRICS_ON");
   pragma Import (Java, VALUE_FRACTIONALMETRICS_DEFAULT, "VALUE_FRACTIONALMETRICS_DEFAULT");
   pragma Import (Java, KEY_INTERPOLATION, "KEY_INTERPOLATION");
   pragma Import (Java, VALUE_INTERPOLATION_NEAREST_NEIGHBOR, "VALUE_INTERPOLATION_NEAREST_NEIGHBOR");
   pragma Import (Java, VALUE_INTERPOLATION_BILINEAR, "VALUE_INTERPOLATION_BILINEAR");
   pragma Import (Java, VALUE_INTERPOLATION_BICUBIC, "VALUE_INTERPOLATION_BICUBIC");
   pragma Import (Java, KEY_ALPHA_INTERPOLATION, "KEY_ALPHA_INTERPOLATION");
   pragma Import (Java, VALUE_ALPHA_INTERPOLATION_SPEED, "VALUE_ALPHA_INTERPOLATION_SPEED");
   pragma Import (Java, VALUE_ALPHA_INTERPOLATION_QUALITY, "VALUE_ALPHA_INTERPOLATION_QUALITY");
   pragma Import (Java, VALUE_ALPHA_INTERPOLATION_DEFAULT, "VALUE_ALPHA_INTERPOLATION_DEFAULT");
   pragma Import (Java, KEY_COLOR_RENDERING, "KEY_COLOR_RENDERING");
   pragma Import (Java, VALUE_COLOR_RENDER_SPEED, "VALUE_COLOR_RENDER_SPEED");
   pragma Import (Java, VALUE_COLOR_RENDER_QUALITY, "VALUE_COLOR_RENDER_QUALITY");
   pragma Import (Java, VALUE_COLOR_RENDER_DEFAULT, "VALUE_COLOR_RENDER_DEFAULT");
   pragma Import (Java, KEY_STROKE_CONTROL, "KEY_STROKE_CONTROL");
   pragma Import (Java, VALUE_STROKE_DEFAULT, "VALUE_STROKE_DEFAULT");
   pragma Import (Java, VALUE_STROKE_NORMALIZE, "VALUE_STROKE_NORMALIZE");
   pragma Import (Java, VALUE_STROKE_PURE, "VALUE_STROKE_PURE");

end Java.Awt.RenderingHints;
pragma Import (Java, Java.Awt.RenderingHints, "java.awt.RenderingHints");
pragma Extensions_Allowed (Off);
