pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.GraphicsDevice;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.Robot is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Robot (This : Ref := null)
                       return Ref;
   --  can raise Java.Awt.AWTException.Except

   function New_Robot (P1_GraphicsDevice : access Standard.Java.Awt.GraphicsDevice.Typ'Class; 
                       This : Ref := null)
                       return Ref;
   --  can raise Java.Awt.AWTException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure MouseMove (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   --  synchronized
   procedure MousePress (This : access Typ;
                         P1_Int : Java.Int);

   --  synchronized
   procedure MouseRelease (This : access Typ;
                           P1_Int : Java.Int);

   --  synchronized
   procedure MouseWheel (This : access Typ;
                         P1_Int : Java.Int);

   --  synchronized
   procedure KeyPress (This : access Typ;
                       P1_Int : Java.Int);

   --  synchronized
   procedure KeyRelease (This : access Typ;
                         P1_Int : Java.Int);

   --  synchronized
   function GetPixelColor (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Awt.Color.Typ'Class;

   --  synchronized
   function CreateScreenCapture (This : access Typ;
                                 P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                                 return access Java.Awt.Image.BufferedImage.Typ'Class;

   --  synchronized
   function IsAutoWaitForIdle (This : access Typ)
                               return Java.Boolean;

   --  synchronized
   procedure SetAutoWaitForIdle (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   --  synchronized
   function GetAutoDelay (This : access Typ)
                          return Java.Int;

   --  synchronized
   procedure SetAutoDelay (This : access Typ;
                           P1_Int : Java.Int);

   --  synchronized
   procedure delay_K (This : access Typ;
                      P1_Int : Java.Int);

   --  synchronized
   procedure WaitForIdle (This : access Typ);

   --  synchronized
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Robot);
   pragma Import (Java, MouseMove, "mouseMove");
   pragma Import (Java, MousePress, "mousePress");
   pragma Import (Java, MouseRelease, "mouseRelease");
   pragma Import (Java, MouseWheel, "mouseWheel");
   pragma Import (Java, KeyPress, "keyPress");
   pragma Import (Java, KeyRelease, "keyRelease");
   pragma Import (Java, GetPixelColor, "getPixelColor");
   pragma Import (Java, CreateScreenCapture, "createScreenCapture");
   pragma Import (Java, IsAutoWaitForIdle, "isAutoWaitForIdle");
   pragma Import (Java, SetAutoWaitForIdle, "setAutoWaitForIdle");
   pragma Import (Java, GetAutoDelay, "getAutoDelay");
   pragma Import (Java, SetAutoDelay, "setAutoDelay");
   pragma Import (Java, delay_K, "delay");
   pragma Import (Java, WaitForIdle, "waitForIdle");
   pragma Import (Java, ToString, "toString");

end Java.Awt.Robot;
pragma Import (Java, Java.Awt.Robot, "java.awt.Robot");
pragma Extensions_Allowed (Off);
