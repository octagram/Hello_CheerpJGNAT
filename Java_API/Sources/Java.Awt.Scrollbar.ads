pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Event.AdjustmentEvent;
limited with Java.Awt.Event.AdjustmentListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Adjustable;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.Scrollbar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Adjustable_I : Java.Awt.Adjustable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Scrollbar (This : Ref := null)
                           return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Scrollbar (P1_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_Scrollbar (P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int;
                           P5_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   function GetValue (This : access Typ)
                      return Java.Int;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int);

   function GetMinimum (This : access Typ)
                        return Java.Int;

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int);

   function GetMaximum (This : access Typ)
                        return Java.Int;

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int);

   function GetVisibleAmount (This : access Typ)
                              return Java.Int;

   procedure SetVisibleAmount (This : access Typ;
                               P1_Int : Java.Int);

   procedure SetUnitIncrement (This : access Typ;
                               P1_Int : Java.Int);

   function GetUnitIncrement (This : access Typ)
                              return Java.Int;

   procedure SetBlockIncrement (This : access Typ;
                                P1_Int : Java.Int);

   function GetBlockIncrement (This : access Typ)
                               return Java.Int;

   procedure SetValues (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   --  synchronized
   procedure AddAdjustmentListener (This : access Typ;
                                    P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class);

   --  synchronized
   procedure RemoveAdjustmentListener (This : access Typ;
                                       P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class);

   --  synchronized
   function GetAdjustmentListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessAdjustmentEvent (This : access Typ;
                                     P1_AdjustmentEvent : access Standard.Java.Awt.Event.AdjustmentEvent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   HORIZONTAL : constant Java.Int;

   --  final
   VERTICAL : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Scrollbar);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, GetVisibleAmount, "getVisibleAmount");
   pragma Import (Java, SetVisibleAmount, "setVisibleAmount");
   pragma Import (Java, SetUnitIncrement, "setUnitIncrement");
   pragma Import (Java, GetUnitIncrement, "getUnitIncrement");
   pragma Import (Java, SetBlockIncrement, "setBlockIncrement");
   pragma Import (Java, GetBlockIncrement, "getBlockIncrement");
   pragma Import (Java, SetValues, "setValues");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Import (Java, AddAdjustmentListener, "addAdjustmentListener");
   pragma Import (Java, RemoveAdjustmentListener, "removeAdjustmentListener");
   pragma Import (Java, GetAdjustmentListeners, "getAdjustmentListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessAdjustmentEvent, "processAdjustmentEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, HORIZONTAL, "HORIZONTAL");
   pragma Import (Java, VERTICAL, "VERTICAL");

end Java.Awt.Scrollbar;
pragma Import (Java, Java.Awt.Scrollbar, "java.awt.Scrollbar");
pragma Extensions_Allowed (Off);
