pragma Extensions_Allowed (On);
limited with Java.Awt.Adjustable;
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.MouseWheelEvent;
limited with Java.Awt.Graphics;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Point;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Container;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.ScrollPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Container.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ScrollPane (This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_ScrollPane (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   function GetScrollbarDisplayPolicy (This : access Typ)
                                       return Java.Int;

   function GetViewportSize (This : access Typ)
                             return access Java.Awt.Dimension.Typ'Class;

   function GetHScrollbarHeight (This : access Typ)
                                 return Java.Int;

   function GetVScrollbarWidth (This : access Typ)
                                return Java.Int;

   function GetVAdjustable (This : access Typ)
                            return access Java.Awt.Adjustable.Typ'Class;

   function GetHAdjustable (This : access Typ)
                            return access Java.Awt.Adjustable.Typ'Class;

   procedure SetScrollPosition (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int);

   procedure SetScrollPosition (This : access Typ;
                                P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetScrollPosition (This : access Typ)
                               return access Java.Awt.Point.Typ'Class;

   --  final
   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   procedure DoLayout (This : access Typ);

   procedure PrintComponents (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure AddNotify (This : access Typ);

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  protected
   procedure ProcessMouseWheelEvent (This : access Typ;
                                     P1_MouseWheelEvent : access Standard.Java.Awt.Event.MouseWheelEvent.Typ'Class);

   --  protected
   function EventTypeEnabled (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Boolean;

   procedure SetWheelScrollingEnabled (This : access Typ;
                                       P1_Boolean : Java.Boolean);

   function IsWheelScrollingEnabled (This : access Typ)
                                     return Java.Boolean;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SCROLLBARS_AS_NEEDED : constant Java.Int;

   --  final
   SCROLLBARS_ALWAYS : constant Java.Int;

   --  final
   SCROLLBARS_NEVER : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ScrollPane);
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, GetScrollbarDisplayPolicy, "getScrollbarDisplayPolicy");
   pragma Import (Java, GetViewportSize, "getViewportSize");
   pragma Import (Java, GetHScrollbarHeight, "getHScrollbarHeight");
   pragma Import (Java, GetVScrollbarWidth, "getVScrollbarWidth");
   pragma Import (Java, GetVAdjustable, "getVAdjustable");
   pragma Import (Java, GetHAdjustable, "getHAdjustable");
   pragma Import (Java, SetScrollPosition, "setScrollPosition");
   pragma Import (Java, GetScrollPosition, "getScrollPosition");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, DoLayout, "doLayout");
   pragma Import (Java, PrintComponents, "printComponents");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, ProcessMouseWheelEvent, "processMouseWheelEvent");
   pragma Import (Java, EventTypeEnabled, "eventTypeEnabled");
   pragma Import (Java, SetWheelScrollingEnabled, "setWheelScrollingEnabled");
   pragma Import (Java, IsWheelScrollingEnabled, "isWheelScrollingEnabled");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, SCROLLBARS_AS_NEEDED, "SCROLLBARS_AS_NEEDED");
   pragma Import (Java, SCROLLBARS_ALWAYS, "SCROLLBARS_ALWAYS");
   pragma Import (Java, SCROLLBARS_NEVER, "SCROLLBARS_NEVER");

end Java.Awt.ScrollPane;
pragma Import (Java, Java.Awt.ScrollPane, "java.awt.ScrollPane");
pragma Extensions_Allowed (Off);
