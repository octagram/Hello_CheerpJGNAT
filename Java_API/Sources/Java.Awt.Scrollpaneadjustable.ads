pragma Extensions_Allowed (On);
limited with Java.Awt.Event.AdjustmentListener;
limited with Java.Lang.String;
with Java.Awt.Adjustable;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.ScrollPaneAdjustable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Adjustable_I : Java.Awt.Adjustable.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int);

   function GetMinimum (This : access Typ)
                        return Java.Int;

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int);

   function GetMaximum (This : access Typ)
                        return Java.Int;

   --  synchronized
   procedure SetUnitIncrement (This : access Typ;
                               P1_Int : Java.Int);

   function GetUnitIncrement (This : access Typ)
                              return Java.Int;

   --  synchronized
   procedure SetBlockIncrement (This : access Typ;
                                P1_Int : Java.Int);

   function GetBlockIncrement (This : access Typ)
                               return Java.Int;

   procedure SetVisibleAmount (This : access Typ;
                               P1_Int : Java.Int);

   function GetVisibleAmount (This : access Typ)
                              return Java.Int;

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int);

   function GetValue (This : access Typ)
                      return Java.Int;

   --  synchronized
   procedure AddAdjustmentListener (This : access Typ;
                                    P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class);

   --  synchronized
   procedure RemoveAdjustmentListener (This : access Typ;
                                       P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class);

   --  synchronized
   function GetAdjustmentListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetUnitIncrement, "setUnitIncrement");
   pragma Import (Java, GetUnitIncrement, "getUnitIncrement");
   pragma Import (Java, SetBlockIncrement, "setBlockIncrement");
   pragma Import (Java, GetBlockIncrement, "getBlockIncrement");
   pragma Import (Java, SetVisibleAmount, "setVisibleAmount");
   pragma Import (Java, GetVisibleAmount, "getVisibleAmount");
   pragma Import (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, AddAdjustmentListener, "addAdjustmentListener");
   pragma Import (Java, RemoveAdjustmentListener, "removeAdjustmentListener");
   pragma Import (Java, GetAdjustmentListeners, "getAdjustmentListeners");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ParamString, "paramString");

end Java.Awt.ScrollPaneAdjustable;
pragma Import (Java, Java.Awt.ScrollPaneAdjustable, "java.awt.ScrollPaneAdjustable");
pragma Extensions_Allowed (Off);
