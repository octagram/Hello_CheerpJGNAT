pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics2D;
limited with Java.Awt.Rectangle;
limited with Java.Net.URL;
with Java.Lang.Object;

package Java.Awt.SplashScreen is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetSplashScreen return access Java.Awt.SplashScreen.Typ'Class;

   procedure SetImageURL (This : access Typ;
                          P1_URL : access Standard.Java.Net.URL.Typ'Class);
   --  can raise Java.Lang.NullPointerException.Except,
   --  Java.Io.IOException.Except and
   --  Java.Lang.IllegalStateException.Except

   --  synchronized
   function GetImageURL (This : access Typ)
                         return access Java.Net.URL.Typ'Class;
   --  can raise Java.Lang.IllegalStateException.Except

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;
   --  can raise Java.Lang.IllegalStateException.Except

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;
   --  can raise Java.Lang.IllegalStateException.Except

   function CreateGraphics (This : access Typ)
                            return access Java.Awt.Graphics2D.Typ'Class;
   --  can raise Java.Lang.IllegalStateException.Except

   procedure Update (This : access Typ);
   --  can raise Java.Lang.IllegalStateException.Except

   --  synchronized
   procedure Close (This : access Typ);
   --  can raise Java.Lang.IllegalStateException.Except

   function IsVisible (This : access Typ)
                       return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetSplashScreen, "getSplashScreen");
   pragma Import (Java, SetImageURL, "setImageURL");
   pragma Import (Java, GetImageURL, "getImageURL");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, CreateGraphics, "createGraphics");
   pragma Import (Java, Update, "update");
   pragma Import (Java, Close, "close");
   pragma Import (Java, IsVisible, "isVisible");

end Java.Awt.SplashScreen;
pragma Import (Java, Java.Awt.SplashScreen, "java.awt.SplashScreen");
pragma Extensions_Allowed (Off);
