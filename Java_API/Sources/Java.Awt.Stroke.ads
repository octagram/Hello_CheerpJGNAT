pragma Extensions_Allowed (On);
limited with Java.Awt.Shape;
with Java.Lang.Object;

package Java.Awt.Stroke is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateStrokedShape (This : access Typ;
                                P1_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                return access Java.Awt.Shape.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateStrokedShape, "createStrokedShape");

end Java.Awt.Stroke;
pragma Import (Java, Java.Awt.Stroke, "java.awt.Stroke");
pragma Extensions_Allowed (Off);
