pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.PaintContext;
limited with Java.Awt.Rectangle;
limited with Java.Awt.RenderingHints;
limited with Java.Lang.String;
with Java.Awt.Color;
with Java.Awt.Paint;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Awt.SystemColor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Paint_I : Java.Awt.Paint.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Color.Typ(Paint_I,
                              Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRGB (This : access Typ)
                    return Java.Int;

   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                           P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                           P4_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                           P5_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.PaintContext.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DESKTOP : constant Java.Int;

   --  final
   ACTIVE_CAPTION : constant Java.Int;

   --  final
   ACTIVE_CAPTION_TEXT : constant Java.Int;

   --  final
   ACTIVE_CAPTION_BORDER : constant Java.Int;

   --  final
   INACTIVE_CAPTION : constant Java.Int;

   --  final
   INACTIVE_CAPTION_TEXT : constant Java.Int;

   --  final
   INACTIVE_CAPTION_BORDER : constant Java.Int;

   --  final
   WINDOW : constant Java.Int;

   --  final
   WINDOW_BORDER : constant Java.Int;

   --  final
   WINDOW_TEXT : constant Java.Int;

   --  final
   MENU : constant Java.Int;

   --  final
   MENU_TEXT : constant Java.Int;

   --  final
   TEXT : constant Java.Int;

   --  final
   TEXT_TEXT : constant Java.Int;

   --  final
   TEXT_HIGHLIGHT : constant Java.Int;

   --  final
   TEXT_HIGHLIGHT_TEXT : constant Java.Int;

   --  final
   TEXT_INACTIVE_TEXT : constant Java.Int;

   --  final
   CONTROL : constant Java.Int;

   --  final
   CONTROL_TEXT : constant Java.Int;

   --  final
   CONTROL_HIGHLIGHT : constant Java.Int;

   --  final
   CONTROL_LT_HIGHLIGHT : constant Java.Int;

   --  final
   CONTROL_SHADOW : constant Java.Int;

   --  final
   CONTROL_DK_SHADOW : constant Java.Int;

   --  final
   SCROLLBAR : constant Java.Int;

   --  final
   INFO : constant Java.Int;

   --  final
   INFO_TEXT : constant Java.Int;

   --  final
   NUM_COLORS : constant Java.Int;

   --  final
   Desktop_K : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ActiveCaption : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ActiveCaptionText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ActiveCaptionBorder : access Java.Awt.SystemColor.Typ'Class;

   --  final
   InactiveCaption : access Java.Awt.SystemColor.Typ'Class;

   --  final
   InactiveCaptionText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   InactiveCaptionBorder : access Java.Awt.SystemColor.Typ'Class;

   --  final
   Window_K : access Java.Awt.SystemColor.Typ'Class;

   --  final
   WindowBorder : access Java.Awt.SystemColor.Typ'Class;

   --  final
   WindowText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   Menu_K : access Java.Awt.SystemColor.Typ'Class;

   --  final
   MenuText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   Text_K : access Java.Awt.SystemColor.Typ'Class;

   --  final
   TextText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   TextHighlight : access Java.Awt.SystemColor.Typ'Class;

   --  final
   TextHighlightText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   TextInactiveText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   Control_K : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ControlText : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ControlHighlight : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ControlLtHighlight : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ControlShadow : access Java.Awt.SystemColor.Typ'Class;

   --  final
   ControlDkShadow : access Java.Awt.SystemColor.Typ'Class;

   --  final
   Scrollbar_K : access Java.Awt.SystemColor.Typ'Class;

   --  final
   Info_K : access Java.Awt.SystemColor.Typ'Class;

   --  final
   InfoText : access Java.Awt.SystemColor.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetRGB, "getRGB");
   pragma Import (Java, CreateContext, "createContext");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, DESKTOP, "DESKTOP");
   pragma Import (Java, ACTIVE_CAPTION, "ACTIVE_CAPTION");
   pragma Import (Java, ACTIVE_CAPTION_TEXT, "ACTIVE_CAPTION_TEXT");
   pragma Import (Java, ACTIVE_CAPTION_BORDER, "ACTIVE_CAPTION_BORDER");
   pragma Import (Java, INACTIVE_CAPTION, "INACTIVE_CAPTION");
   pragma Import (Java, INACTIVE_CAPTION_TEXT, "INACTIVE_CAPTION_TEXT");
   pragma Import (Java, INACTIVE_CAPTION_BORDER, "INACTIVE_CAPTION_BORDER");
   pragma Import (Java, WINDOW, "WINDOW");
   pragma Import (Java, WINDOW_BORDER, "WINDOW_BORDER");
   pragma Import (Java, WINDOW_TEXT, "WINDOW_TEXT");
   pragma Import (Java, MENU, "MENU");
   pragma Import (Java, MENU_TEXT, "MENU_TEXT");
   pragma Import (Java, TEXT, "TEXT");
   pragma Import (Java, TEXT_TEXT, "TEXT_TEXT");
   pragma Import (Java, TEXT_HIGHLIGHT, "TEXT_HIGHLIGHT");
   pragma Import (Java, TEXT_HIGHLIGHT_TEXT, "TEXT_HIGHLIGHT_TEXT");
   pragma Import (Java, TEXT_INACTIVE_TEXT, "TEXT_INACTIVE_TEXT");
   pragma Import (Java, CONTROL, "CONTROL");
   pragma Import (Java, CONTROL_TEXT, "CONTROL_TEXT");
   pragma Import (Java, CONTROL_HIGHLIGHT, "CONTROL_HIGHLIGHT");
   pragma Import (Java, CONTROL_LT_HIGHLIGHT, "CONTROL_LT_HIGHLIGHT");
   pragma Import (Java, CONTROL_SHADOW, "CONTROL_SHADOW");
   pragma Import (Java, CONTROL_DK_SHADOW, "CONTROL_DK_SHADOW");
   pragma Import (Java, SCROLLBAR, "SCROLLBAR");
   pragma Import (Java, INFO, "INFO");
   pragma Import (Java, INFO_TEXT, "INFO_TEXT");
   pragma Import (Java, NUM_COLORS, "NUM_COLORS");
   pragma Import (Java, Desktop_K, "desktop");
   pragma Import (Java, ActiveCaption, "activeCaption");
   pragma Import (Java, ActiveCaptionText, "activeCaptionText");
   pragma Import (Java, ActiveCaptionBorder, "activeCaptionBorder");
   pragma Import (Java, InactiveCaption, "inactiveCaption");
   pragma Import (Java, InactiveCaptionText, "inactiveCaptionText");
   pragma Import (Java, InactiveCaptionBorder, "inactiveCaptionBorder");
   pragma Import (Java, Window_K, "window");
   pragma Import (Java, WindowBorder, "windowBorder");
   pragma Import (Java, WindowText, "windowText");
   pragma Import (Java, Menu_K, "menu");
   pragma Import (Java, MenuText, "menuText");
   pragma Import (Java, Text_K, "text");
   pragma Import (Java, TextText, "textText");
   pragma Import (Java, TextHighlight, "textHighlight");
   pragma Import (Java, TextHighlightText, "textHighlightText");
   pragma Import (Java, TextInactiveText, "textInactiveText");
   pragma Import (Java, Control_K, "control");
   pragma Import (Java, ControlText, "controlText");
   pragma Import (Java, ControlHighlight, "controlHighlight");
   pragma Import (Java, ControlLtHighlight, "controlLtHighlight");
   pragma Import (Java, ControlShadow, "controlShadow");
   pragma Import (Java, ControlDkShadow, "controlDkShadow");
   pragma Import (Java, Scrollbar_K, "scrollbar");
   pragma Import (Java, Info_K, "info");
   pragma Import (Java, InfoText, "infoText");

end Java.Awt.SystemColor;
pragma Import (Java, Java.Awt.SystemColor, "java.awt.SystemColor");
pragma Extensions_Allowed (Off);
