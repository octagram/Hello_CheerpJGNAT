pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.TrayIcon;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.SystemTray is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSystemTray return access Java.Awt.SystemTray.Typ'Class;

   function IsSupported return Java.Boolean;

   procedure Add (This : access Typ;
                  P1_TrayIcon : access Standard.Java.Awt.TrayIcon.Typ'Class);
   --  can raise Java.Awt.AWTException.Except

   procedure Remove (This : access Typ;
                     P1_TrayIcon : access Standard.Java.Awt.TrayIcon.Typ'Class);

   function GetTrayIcons (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   function GetTrayIconSize (This : access Typ)
                             return access Java.Awt.Dimension.Typ'Class;

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetSystemTray, "getSystemTray");
   pragma Import (Java, IsSupported, "isSupported");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, GetTrayIcons, "getTrayIcons");
   pragma Import (Java, GetTrayIconSize, "getTrayIconSize");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");

end Java.Awt.SystemTray;
pragma Import (Java, Java.Awt.SystemTray, "java.awt.SystemTray");
pragma Extensions_Allowed (Off);
