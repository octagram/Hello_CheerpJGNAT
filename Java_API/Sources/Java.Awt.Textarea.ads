pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Awt.TextComponent;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.TextArea is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.TextComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I,
                                      Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TextArea (This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_TextArea (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_TextArea (P1_Int : Java.Int;
                          P2_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_TextArea (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   function New_TextArea (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Awt.HeadlessException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   procedure Insert (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int);

   procedure Append (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure ReplaceRange (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int);

   function GetRows (This : access Typ)
                     return Java.Int;

   procedure SetRows (This : access Typ;
                      P1_Int : Java.Int);

   function GetColumns (This : access Typ)
                        return Java.Int;

   procedure SetColumns (This : access Typ;
                         P1_Int : Java.Int);

   function GetScrollbarVisibility (This : access Typ)
                                    return Java.Int;

   function GetPreferredSize (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SCROLLBARS_BOTH : constant Java.Int;

   --  final
   SCROLLBARS_VERTICAL_ONLY : constant Java.Int;

   --  final
   SCROLLBARS_HORIZONTAL_ONLY : constant Java.Int;

   --  final
   SCROLLBARS_NONE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TextArea);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Append, "append");
   pragma Import (Java, ReplaceRange, "replaceRange");
   pragma Import (Java, GetRows, "getRows");
   pragma Import (Java, SetRows, "setRows");
   pragma Import (Java, GetColumns, "getColumns");
   pragma Import (Java, SetColumns, "setColumns");
   pragma Import (Java, GetScrollbarVisibility, "getScrollbarVisibility");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, SCROLLBARS_BOTH, "SCROLLBARS_BOTH");
   pragma Import (Java, SCROLLBARS_VERTICAL_ONLY, "SCROLLBARS_VERTICAL_ONLY");
   pragma Import (Java, SCROLLBARS_HORIZONTAL_ONLY, "SCROLLBARS_HORIZONTAL_ONLY");
   pragma Import (Java, SCROLLBARS_NONE, "SCROLLBARS_NONE");

end Java.Awt.TextArea;
pragma Import (Java, Java.Awt.TextArea, "java.awt.TextArea");
pragma Extensions_Allowed (Off);
