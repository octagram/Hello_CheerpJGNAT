pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.Color;
limited with Java.Awt.Event.TextEvent;
limited with Java.Awt.Event.TextListener;
limited with Java.Awt.Im.InputMethodRequests;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Component;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Java.Awt.TextComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Component.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      TextListener : access Java.Awt.Event.TextListener.Typ'Class;
      pragma Import (Java, TextListener, "textListener");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure EnableInputMethods (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   function GetInputMethodRequests (This : access Typ)
                                    return access Java.Awt.Im.InputMethodRequests.Typ'Class;

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   --  synchronized
   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetSelectedText (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function IsEditable (This : access Typ)
                        return Java.Boolean;

   --  synchronized
   procedure SetEditable (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   --  synchronized
   function GetSelectionStart (This : access Typ)
                               return Java.Int;

   --  synchronized
   procedure SetSelectionStart (This : access Typ;
                                P1_Int : Java.Int);

   --  synchronized
   function GetSelectionEnd (This : access Typ)
                             return Java.Int;

   --  synchronized
   procedure SetSelectionEnd (This : access Typ;
                              P1_Int : Java.Int);

   --  synchronized
   procedure select_K (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int);

   --  synchronized
   procedure SelectAll (This : access Typ);

   --  synchronized
   procedure SetCaretPosition (This : access Typ;
                               P1_Int : Java.Int);

   --  synchronized
   function GetCaretPosition (This : access Typ)
                              return Java.Int;

   --  synchronized
   procedure AddTextListener (This : access Typ;
                              P1_TextListener : access Standard.Java.Awt.Event.TextListener.Typ'Class);

   --  synchronized
   procedure RemoveTextListener (This : access Typ;
                                 P1_TextListener : access Standard.Java.Awt.Event.TextListener.Typ'Class);

   --  synchronized
   function GetTextListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessTextEvent (This : access Typ;
                               P1_TextEvent : access Standard.Java.Awt.Event.TextEvent.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, EnableInputMethods, "enableInputMethods");
   pragma Import (Java, GetInputMethodRequests, "getInputMethodRequests");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, GetSelectedText, "getSelectedText");
   pragma Import (Java, IsEditable, "isEditable");
   pragma Import (Java, SetEditable, "setEditable");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, GetSelectionStart, "getSelectionStart");
   pragma Import (Java, SetSelectionStart, "setSelectionStart");
   pragma Import (Java, GetSelectionEnd, "getSelectionEnd");
   pragma Import (Java, SetSelectionEnd, "setSelectionEnd");
   pragma Import (Java, select_K, "select");
   pragma Import (Java, SelectAll, "selectAll");
   pragma Import (Java, SetCaretPosition, "setCaretPosition");
   pragma Import (Java, GetCaretPosition, "getCaretPosition");
   pragma Import (Java, AddTextListener, "addTextListener");
   pragma Import (Java, RemoveTextListener, "removeTextListener");
   pragma Import (Java, GetTextListeners, "getTextListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessTextEvent, "processTextEvent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Java.Awt.TextComponent;
pragma Import (Java, Java.Awt.TextComponent, "java.awt.TextComponent");
pragma Extensions_Allowed (Off);
