pragma Extensions_Allowed (On);
limited with Java.Awt.Geom.AffineTransform;
limited with Java.Awt.Geom.Rectangle2D;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.PaintContext;
limited with Java.Awt.Rectangle;
limited with Java.Awt.RenderingHints;
with Java.Awt.Paint;
with Java.Lang.Object;

package Java.Awt.TexturePaint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Paint_I : Java.Awt.Paint.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TexturePaint (P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                              P2_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetImage (This : access Typ)
                      return access Java.Awt.Image.BufferedImage.Typ'Class;

   function GetAnchorRect (This : access Typ)
                           return access Java.Awt.Geom.Rectangle2D.Typ'Class;

   function CreateContext (This : access Typ;
                           P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                           P3_Rectangle2D : access Standard.Java.Awt.Geom.Rectangle2D.Typ'Class;
                           P4_AffineTransform : access Standard.Java.Awt.Geom.AffineTransform.Typ'Class;
                           P5_RenderingHints : access Standard.Java.Awt.RenderingHints.Typ'Class)
                           return access Java.Awt.PaintContext.Typ'Class;

   function GetTransparency (This : access Typ)
                             return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TexturePaint);
   pragma Import (Java, GetImage, "getImage");
   pragma Import (Java, GetAnchorRect, "getAnchorRect");
   pragma Import (Java, CreateContext, "createContext");
   pragma Import (Java, GetTransparency, "getTransparency");

end Java.Awt.TexturePaint;
pragma Import (Java, Java.Awt.TexturePaint, "java.awt.TexturePaint");
pragma Extensions_Allowed (Off);
