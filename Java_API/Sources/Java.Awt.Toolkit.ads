pragma Extensions_Allowed (On);
limited with Java.Awt.Button;
limited with Java.Awt.Canvas;
limited with Java.Awt.Checkbox;
limited with Java.Awt.CheckboxMenuItem;
limited with Java.Awt.Choice;
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Cursor;
limited with Java.Awt.Datatransfer.Clipboard;
limited with Java.Awt.Desktop;
limited with Java.Awt.Dialog;
limited with Java.Awt.Dialog.ModalExclusionType;
limited with Java.Awt.Dialog.ModalityType;
limited with Java.Awt.Dimension;
limited with Java.Awt.Dnd.DragGestureEvent;
limited with Java.Awt.Dnd.DragGestureListener;
limited with Java.Awt.Dnd.DragGestureRecognizer;
limited with Java.Awt.Dnd.DragSource;
limited with Java.Awt.Dnd.Peer.DragSourceContextPeer;
limited with Java.Awt.Event.AWTEventListener;
limited with Java.Awt.EventQueue;
limited with Java.Awt.FileDialog;
limited with Java.Awt.Frame;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Im.InputMethodHighlight;
limited with Java.Awt.Image;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.Image.ImageProducer;
limited with Java.Awt.Insets;
limited with Java.Awt.JobAttributes;
limited with Java.Awt.Label;
limited with Java.Awt.List;
limited with Java.Awt.Menu;
limited with Java.Awt.MenuBar;
limited with Java.Awt.MenuItem;
limited with Java.Awt.PageAttributes;
limited with Java.Awt.Panel;
limited with Java.Awt.Peer.ButtonPeer;
limited with Java.Awt.Peer.CanvasPeer;
limited with Java.Awt.Peer.CheckboxMenuItemPeer;
limited with Java.Awt.Peer.CheckboxPeer;
limited with Java.Awt.Peer.ChoicePeer;
limited with Java.Awt.Peer.DesktopPeer;
limited with Java.Awt.Peer.DialogPeer;
limited with Java.Awt.Peer.FileDialogPeer;
limited with Java.Awt.Peer.FramePeer;
limited with Java.Awt.Peer.LabelPeer;
limited with Java.Awt.Peer.LightweightPeer;
limited with Java.Awt.Peer.ListPeer;
limited with Java.Awt.Peer.MenuBarPeer;
limited with Java.Awt.Peer.MenuItemPeer;
limited with Java.Awt.Peer.MenuPeer;
limited with Java.Awt.Peer.MouseInfoPeer;
limited with Java.Awt.Peer.PanelPeer;
limited with Java.Awt.Peer.PopupMenuPeer;
limited with Java.Awt.Peer.ScrollPanePeer;
limited with Java.Awt.Peer.ScrollbarPeer;
limited with Java.Awt.Peer.TextAreaPeer;
limited with Java.Awt.Peer.TextFieldPeer;
limited with Java.Awt.Peer.WindowPeer;
limited with Java.Awt.Point;
limited with Java.Awt.PopupMenu;
limited with Java.Awt.PrintJob;
limited with Java.Awt.ScrollPane;
limited with Java.Awt.Scrollbar;
limited with Java.Awt.TextArea;
limited with Java.Awt.TextField;
limited with Java.Awt.Window;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Beans.PropertyChangeSupport;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Map;
limited with Java.Util.Properties;
with Java.Lang.Object;

package Java.Awt.Toolkit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      DesktopProperties : access Java.Util.Map.Typ'Class;
      pragma Import (Java, DesktopProperties, "desktopProperties");

      --  protected  final
      DesktopPropsSupport : access Java.Beans.PropertyChangeSupport.Typ'Class;
      pragma Import (Java, DesktopPropsSupport, "desktopPropsSupport");

   end record;

   function New_Toolkit (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateDesktopPeer (This : access Typ;
                               P1_Desktop : access Standard.Java.Awt.Desktop.Typ'Class)
                               return access Java.Awt.Peer.DesktopPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateButton (This : access Typ;
                          P1_Button : access Standard.Java.Awt.Button.Typ'Class)
                          return access Java.Awt.Peer.ButtonPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateTextField (This : access Typ;
                             P1_TextField : access Standard.Java.Awt.TextField.Typ'Class)
                             return access Java.Awt.Peer.TextFieldPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateLabel (This : access Typ;
                         P1_Label : access Standard.Java.Awt.Label.Typ'Class)
                         return access Java.Awt.Peer.LabelPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateList (This : access Typ;
                        P1_List : access Standard.Java.Awt.List.Typ'Class)
                        return access Java.Awt.Peer.ListPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateCheckbox (This : access Typ;
                            P1_Checkbox : access Standard.Java.Awt.Checkbox.Typ'Class)
                            return access Java.Awt.Peer.CheckboxPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateScrollbar (This : access Typ;
                             P1_Scrollbar : access Standard.Java.Awt.Scrollbar.Typ'Class)
                             return access Java.Awt.Peer.ScrollbarPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateScrollPane (This : access Typ;
                              P1_ScrollPane : access Standard.Java.Awt.ScrollPane.Typ'Class)
                              return access Java.Awt.Peer.ScrollPanePeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateTextArea (This : access Typ;
                            P1_TextArea : access Standard.Java.Awt.TextArea.Typ'Class)
                            return access Java.Awt.Peer.TextAreaPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateChoice (This : access Typ;
                          P1_Choice : access Standard.Java.Awt.Choice.Typ'Class)
                          return access Java.Awt.Peer.ChoicePeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateFrame (This : access Typ;
                         P1_Frame : access Standard.Java.Awt.Frame.Typ'Class)
                         return access Java.Awt.Peer.FramePeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateCanvas (This : access Typ;
                          P1_Canvas : access Standard.Java.Awt.Canvas.Typ'Class)
                          return access Java.Awt.Peer.CanvasPeer.Typ'Class is abstract;

   --  protected
   function CreatePanel (This : access Typ;
                         P1_Panel : access Standard.Java.Awt.Panel.Typ'Class)
                         return access Java.Awt.Peer.PanelPeer.Typ'Class is abstract;

   --  protected
   function CreateWindow (This : access Typ;
                          P1_Window : access Standard.Java.Awt.Window.Typ'Class)
                          return access Java.Awt.Peer.WindowPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateDialog (This : access Typ;
                          P1_Dialog : access Standard.Java.Awt.Dialog.Typ'Class)
                          return access Java.Awt.Peer.DialogPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateMenuBar (This : access Typ;
                           P1_MenuBar : access Standard.Java.Awt.MenuBar.Typ'Class)
                           return access Java.Awt.Peer.MenuBarPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateMenu (This : access Typ;
                        P1_Menu : access Standard.Java.Awt.Menu.Typ'Class)
                        return access Java.Awt.Peer.MenuPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreatePopupMenu (This : access Typ;
                             P1_PopupMenu : access Standard.Java.Awt.PopupMenu.Typ'Class)
                             return access Java.Awt.Peer.PopupMenuPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateMenuItem (This : access Typ;
                            P1_MenuItem : access Standard.Java.Awt.MenuItem.Typ'Class)
                            return access Java.Awt.Peer.MenuItemPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateFileDialog (This : access Typ;
                              P1_FileDialog : access Standard.Java.Awt.FileDialog.Typ'Class)
                              return access Java.Awt.Peer.FileDialogPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateCheckboxMenuItem (This : access Typ;
                                    P1_CheckboxMenuItem : access Standard.Java.Awt.CheckboxMenuItem.Typ'Class)
                                    return access Java.Awt.Peer.CheckboxMenuItemPeer.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function GetMouseInfoPeer (This : access Typ)
                              return access Java.Awt.Peer.MouseInfoPeer.Typ'Class;

   --  protected
   function CreateComponent (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Peer.LightweightPeer.Typ'Class;

   --  protected
   procedure LoadSystemColors (This : access Typ;
                               P1_Int_Arr : Java.Int_Arr);
   --  can raise Java.Awt.HeadlessException.Except

   procedure SetDynamicLayout (This : access Typ;
                               P1_Boolean : Java.Boolean);
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function IsDynamicLayoutSet (This : access Typ)
                                return Java.Boolean;
   --  can raise Java.Awt.HeadlessException.Except

   function IsDynamicLayoutActive (This : access Typ)
                                   return Java.Boolean;
   --  can raise Java.Awt.HeadlessException.Except

   function GetScreenSize (This : access Typ)
                           return access Java.Awt.Dimension.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   function GetScreenResolution (This : access Typ)
                                 return Java.Int is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   function GetScreenInsets (This : access Typ;
                             P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   procedure Sync (This : access Typ) is abstract;

   --  synchronized
   function GetDefaultToolkit return access Java.Awt.Toolkit.Typ'Class;

   function GetImage (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Awt.Image.Typ'Class is abstract;

   function GetImage (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class)
                      return access Java.Awt.Image.Typ'Class is abstract;

   function CreateImage (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Awt.Image.Typ'Class is abstract;

   function CreateImage (This : access Typ;
                         P1_URL : access Standard.Java.Net.URL.Typ'Class)
                         return access Java.Awt.Image.Typ'Class is abstract;

   function PrepareImage (This : access Typ;
                          P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                          return Java.Boolean is abstract;

   function CheckImage (This : access Typ;
                        P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                        return Java.Int is abstract;

   function CreateImage (This : access Typ;
                         P1_ImageProducer : access Standard.Java.Awt.Image.ImageProducer.Typ'Class)
                         return access Java.Awt.Image.Typ'Class is abstract;

   function CreateImage (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr)
                         return access Java.Awt.Image.Typ'Class;

   function CreateImage (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return access Java.Awt.Image.Typ'Class is abstract;

   function GetPrintJob (This : access Typ;
                         P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_Properties : access Standard.Java.Util.Properties.Typ'Class)
                         return access Java.Awt.PrintJob.Typ'Class is abstract;

   function GetPrintJob (This : access Typ;
                         P1_Frame : access Standard.Java.Awt.Frame.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_JobAttributes : access Standard.Java.Awt.JobAttributes.Typ'Class;
                         P4_PageAttributes : access Standard.Java.Awt.PageAttributes.Typ'Class)
                         return access Java.Awt.PrintJob.Typ'Class;

   procedure Beep (This : access Typ) is abstract;

   function GetSystemClipboard (This : access Typ)
                                return access Java.Awt.Datatransfer.Clipboard.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except

   function GetSystemSelection (This : access Typ)
                                return access Java.Awt.Datatransfer.Clipboard.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function GetMenuShortcutKeyMask (This : access Typ)
                                    return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function GetLockingKeyState (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean;
   --  can raise Java.Lang.UnsupportedOperationException.Except

   procedure SetLockingKeyState (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Boolean : Java.Boolean);
   --  can raise Java.Lang.UnsupportedOperationException.Except

   --  protected
   function GetNativeContainer (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return access Java.Awt.Container.Typ'Class;

   function CreateCustomCursor (This : access Typ;
                                P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                                P2_Point : access Standard.Java.Awt.Point.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Awt.Cursor.Typ'Class;
   --  can raise Java.Lang.IndexOutOfBoundsException.Except and
   --  Java.Awt.HeadlessException.Except

   function GetBestCursorSize (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return access Java.Awt.Dimension.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function GetMaximumCursorColors (This : access Typ)
                                    return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function IsFrameStateSupported (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Boolean;
   --  can raise Java.Awt.HeadlessException.Except

   function GetProperty (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   --  final
   function GetSystemEventQueue (This : access Typ)
                                 return access Java.Awt.EventQueue.Typ'Class;

   --  protected
   function GetSystemEventQueueImpl (This : access Typ)
                                     return access Java.Awt.EventQueue.Typ'Class is abstract;

   function CreateDragSourceContextPeer (This : access Typ;
                                         P1_DragGestureEvent : access Standard.Java.Awt.Dnd.DragGestureEvent.Typ'Class)
                                         return access Java.Awt.Dnd.Peer.DragSourceContextPeer.Typ'Class is abstract;
   --  can raise Java.Awt.Dnd.InvalidDnDOperationException.Except

   function CreateDragGestureRecognizer (This : access Typ;
                                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                         P2_DragSource : access Standard.Java.Awt.Dnd.DragSource.Typ'Class;
                                         P3_Component : access Standard.Java.Awt.Component.Typ'Class;
                                         P4_Int : Java.Int;
                                         P5_DragGestureListener : access Standard.Java.Awt.Dnd.DragGestureListener.Typ'Class)
                                         return access Java.Awt.Dnd.DragGestureRecognizer.Typ'Class;

   --  final  synchronized
   function GetDesktopProperty (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.Object.Typ'Class;

   --  final  protected
   procedure SetDesktopProperty (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   function LazilyLoadDesktopProperty (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                                       return access Java.Lang.Object.Typ'Class;

   --  protected
   procedure InitializeDesktopProperties (This : access Typ);

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   function GetPropertyChangeListeners (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;

   function IsAlwaysOnTopSupported (This : access Typ)
                                    return Java.Boolean;

   function IsModalityTypeSupported (This : access Typ;
                                     P1_ModalityType : access Standard.Java.Awt.Dialog.ModalityType.Typ'Class)
                                     return Java.Boolean is abstract;

   function IsModalExclusionTypeSupported (This : access Typ;
                                           P1_ModalExclusionType : access Standard.Java.Awt.Dialog.ModalExclusionType.Typ'Class)
                                           return Java.Boolean is abstract;

   procedure AddAWTEventListener (This : access Typ;
                                  P1_AWTEventListener : access Standard.Java.Awt.Event.AWTEventListener.Typ'Class;
                                  P2_Long : Java.Long);

   procedure RemoveAWTEventListener (This : access Typ;
                                     P1_AWTEventListener : access Standard.Java.Awt.Event.AWTEventListener.Typ'Class);

   function GetAWTEventListeners (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref;

   function GetAWTEventListeners (This : access Typ;
                                  P1_Long : Java.Long)
                                  return Standard.Java.Lang.Object.Ref;

   function MapInputMethodHighlight (This : access Typ;
                                     P1_InputMethodHighlight : access Standard.Java.Awt.Im.InputMethodHighlight.Typ'Class)
                                     return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Awt.HeadlessException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Toolkit);
   pragma Export (Java, CreateDesktopPeer, "createDesktopPeer");
   pragma Export (Java, CreateButton, "createButton");
   pragma Export (Java, CreateTextField, "createTextField");
   pragma Export (Java, CreateLabel, "createLabel");
   pragma Export (Java, CreateList, "createList");
   pragma Export (Java, CreateCheckbox, "createCheckbox");
   pragma Export (Java, CreateScrollbar, "createScrollbar");
   pragma Export (Java, CreateScrollPane, "createScrollPane");
   pragma Export (Java, CreateTextArea, "createTextArea");
   pragma Export (Java, CreateChoice, "createChoice");
   pragma Export (Java, CreateFrame, "createFrame");
   pragma Export (Java, CreateCanvas, "createCanvas");
   pragma Export (Java, CreatePanel, "createPanel");
   pragma Export (Java, CreateWindow, "createWindow");
   pragma Export (Java, CreateDialog, "createDialog");
   pragma Export (Java, CreateMenuBar, "createMenuBar");
   pragma Export (Java, CreateMenu, "createMenu");
   pragma Export (Java, CreatePopupMenu, "createPopupMenu");
   pragma Export (Java, CreateMenuItem, "createMenuItem");
   pragma Export (Java, CreateFileDialog, "createFileDialog");
   pragma Export (Java, CreateCheckboxMenuItem, "createCheckboxMenuItem");
   pragma Export (Java, GetMouseInfoPeer, "getMouseInfoPeer");
   pragma Export (Java, CreateComponent, "createComponent");
   pragma Export (Java, LoadSystemColors, "loadSystemColors");
   pragma Export (Java, SetDynamicLayout, "setDynamicLayout");
   pragma Export (Java, IsDynamicLayoutSet, "isDynamicLayoutSet");
   pragma Export (Java, IsDynamicLayoutActive, "isDynamicLayoutActive");
   pragma Export (Java, GetScreenSize, "getScreenSize");
   pragma Export (Java, GetScreenResolution, "getScreenResolution");
   pragma Export (Java, GetScreenInsets, "getScreenInsets");
   pragma Export (Java, GetColorModel, "getColorModel");
   pragma Export (Java, Sync, "sync");
   pragma Export (Java, GetDefaultToolkit, "getDefaultToolkit");
   pragma Export (Java, GetImage, "getImage");
   pragma Export (Java, CreateImage, "createImage");
   pragma Export (Java, PrepareImage, "prepareImage");
   pragma Export (Java, CheckImage, "checkImage");
   pragma Export (Java, GetPrintJob, "getPrintJob");
   pragma Export (Java, Beep, "beep");
   pragma Export (Java, GetSystemClipboard, "getSystemClipboard");
   pragma Export (Java, GetSystemSelection, "getSystemSelection");
   pragma Export (Java, GetMenuShortcutKeyMask, "getMenuShortcutKeyMask");
   pragma Export (Java, GetLockingKeyState, "getLockingKeyState");
   pragma Export (Java, SetLockingKeyState, "setLockingKeyState");
   pragma Export (Java, GetNativeContainer, "getNativeContainer");
   pragma Export (Java, CreateCustomCursor, "createCustomCursor");
   pragma Export (Java, GetBestCursorSize, "getBestCursorSize");
   pragma Export (Java, GetMaximumCursorColors, "getMaximumCursorColors");
   pragma Export (Java, IsFrameStateSupported, "isFrameStateSupported");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, GetSystemEventQueue, "getSystemEventQueue");
   pragma Export (Java, GetSystemEventQueueImpl, "getSystemEventQueueImpl");
   pragma Export (Java, CreateDragSourceContextPeer, "createDragSourceContextPeer");
   pragma Export (Java, CreateDragGestureRecognizer, "createDragGestureRecognizer");
   pragma Export (Java, GetDesktopProperty, "getDesktopProperty");
   pragma Export (Java, SetDesktopProperty, "setDesktopProperty");
   pragma Export (Java, LazilyLoadDesktopProperty, "lazilyLoadDesktopProperty");
   pragma Export (Java, InitializeDesktopProperties, "initializeDesktopProperties");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Export (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Export (Java, IsAlwaysOnTopSupported, "isAlwaysOnTopSupported");
   pragma Export (Java, IsModalityTypeSupported, "isModalityTypeSupported");
   pragma Export (Java, IsModalExclusionTypeSupported, "isModalExclusionTypeSupported");
   pragma Export (Java, AddAWTEventListener, "addAWTEventListener");
   pragma Export (Java, RemoveAWTEventListener, "removeAWTEventListener");
   pragma Export (Java, GetAWTEventListeners, "getAWTEventListeners");
   pragma Export (Java, MapInputMethodHighlight, "mapInputMethodHighlight");

end Java.Awt.Toolkit;
pragma Import (Java, Java.Awt.Toolkit, "java.awt.Toolkit");
pragma Extensions_Allowed (Off);
