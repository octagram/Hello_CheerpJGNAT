pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Awt.Transparency is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransparency (This : access Typ)
                             return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OPAQUE : constant Java.Int;

   --  final
   BITMASK : constant Java.Int;

   --  final
   TRANSLUCENT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTransparency, "getTransparency");
   pragma Import (Java, OPAQUE, "OPAQUE");
   pragma Import (Java, BITMASK, "BITMASK");
   pragma Import (Java, TRANSLUCENT, "TRANSLUCENT");

end Java.Awt.Transparency;
pragma Import (Java, Java.Awt.Transparency, "java.awt.Transparency");
pragma Extensions_Allowed (Off);
