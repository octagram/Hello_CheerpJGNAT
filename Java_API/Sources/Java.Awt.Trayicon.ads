pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Event.MouseListener;
limited with Java.Awt.Event.MouseMotionListener;
limited with Java.Awt.Image;
limited with Java.Awt.PopupMenu;
limited with Java.Awt.TrayIcon.MessageType;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Awt.TrayIcon is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TrayIcon (P1_Image : access Standard.Java.Awt.Image.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_TrayIcon (P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_TrayIcon (P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_PopupMenu : access Standard.Java.Awt.PopupMenu.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class);

   function GetImage (This : access Typ)
                      return access Java.Awt.Image.Typ'Class;

   procedure SetPopupMenu (This : access Typ;
                           P1_PopupMenu : access Standard.Java.Awt.PopupMenu.Typ'Class);

   function GetPopupMenu (This : access Typ)
                          return access Java.Awt.PopupMenu.Typ'Class;

   procedure SetToolTip (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetToolTip (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetImageAutoSize (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function IsImageAutoSize (This : access Typ)
                             return Java.Boolean;

   --  synchronized
   procedure AddMouseListener (This : access Typ;
                               P1_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class);

   --  synchronized
   procedure RemoveMouseListener (This : access Typ;
                                  P1_MouseListener : access Standard.Java.Awt.Event.MouseListener.Typ'Class);

   --  synchronized
   function GetMouseListeners (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddMouseMotionListener (This : access Typ;
                                     P1_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class);

   --  synchronized
   procedure RemoveMouseMotionListener (This : access Typ;
                                        P1_MouseMotionListener : access Standard.Java.Awt.Event.MouseMotionListener.Typ'Class);

   --  synchronized
   function GetMouseMotionListeners (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   procedure DisplayMessage (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_MessageType : access Standard.Java.Awt.TrayIcon.MessageType.Typ'Class);

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TrayIcon);
   pragma Import (Java, SetImage, "setImage");
   pragma Import (Java, GetImage, "getImage");
   pragma Import (Java, SetPopupMenu, "setPopupMenu");
   pragma Import (Java, GetPopupMenu, "getPopupMenu");
   pragma Import (Java, SetToolTip, "setToolTip");
   pragma Import (Java, GetToolTip, "getToolTip");
   pragma Import (Java, SetImageAutoSize, "setImageAutoSize");
   pragma Import (Java, IsImageAutoSize, "isImageAutoSize");
   pragma Import (Java, AddMouseListener, "addMouseListener");
   pragma Import (Java, RemoveMouseListener, "removeMouseListener");
   pragma Import (Java, GetMouseListeners, "getMouseListeners");
   pragma Import (Java, AddMouseMotionListener, "addMouseMotionListener");
   pragma Import (Java, RemoveMouseMotionListener, "removeMouseMotionListener");
   pragma Import (Java, GetMouseMotionListeners, "getMouseMotionListeners");
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, DisplayMessage, "displayMessage");
   pragma Import (Java, GetSize, "getSize");

end Java.Awt.TrayIcon;
pragma Import (Java, Java.Awt.TrayIcon, "java.awt.TrayIcon");
pragma Extensions_Allowed (Off);
