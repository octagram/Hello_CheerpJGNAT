pragma Extensions_Allowed (On);
limited with Java.Awt.AWTEvent;
limited with Java.Awt.BufferCapabilities;
limited with Java.Awt.Component;
limited with Java.Awt.Cursor;
limited with Java.Awt.Dialog.ModalExclusionType;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.WindowEvent;
limited with Java.Awt.Event.WindowFocusListener;
limited with Java.Awt.Event.WindowListener;
limited with Java.Awt.Event.WindowStateListener;
limited with Java.Awt.Frame;
limited with Java.Awt.Graphics;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.Im.InputContext;
limited with Java.Awt.Image.BufferStrategy;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Toolkit;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Java.Util.List;
limited with Java.Util.Locale;
limited with Java.Util.Set;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Container;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Java.Awt.Image;

package Java.Awt.Window is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Container.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Window (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Window (P1_Window : access Standard.Java.Awt.Window.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Window (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                        P2_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIconImages (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   --  synchronized
   procedure SetIconImages (This : access Typ;
                            P1_List : access Standard.Java.Util.List.Typ'Class);

   procedure SetIconImage (This : access Typ;
                           P1_Image : access Standard.Java.Awt.Image.Typ'Class);

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   procedure Pack (This : access Typ);

   procedure SetMinimumSize (This : access Typ;
                             P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int);

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure Dispose (This : access Typ);

   procedure ToFront (This : access Typ);

   procedure ToBack (This : access Typ);

   function GetToolkit (This : access Typ)
                        return access Java.Awt.Toolkit.Typ'Class;

   --  final
   function GetWarningString (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   function GetInputContext (This : access Typ)
                             return access Java.Awt.Im.InputContext.Typ'Class;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetOwner (This : access Typ)
                      return access Java.Awt.Window.Typ'Class;

   function GetOwnedWindows (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetWindows return Standard.Java.Lang.Object.Ref;

   function GetOwnerlessWindows return Standard.Java.Lang.Object.Ref;

   procedure SetModalExclusionType (This : access Typ;
                                    P1_ModalExclusionType : access Standard.Java.Awt.Dialog.ModalExclusionType.Typ'Class);

   function GetModalExclusionType (This : access Typ)
                                   return access Java.Awt.Dialog.ModalExclusionType.Typ'Class;

   --  synchronized
   procedure AddWindowListener (This : access Typ;
                                P1_WindowListener : access Standard.Java.Awt.Event.WindowListener.Typ'Class);

   --  synchronized
   procedure AddWindowStateListener (This : access Typ;
                                     P1_WindowStateListener : access Standard.Java.Awt.Event.WindowStateListener.Typ'Class);

   --  synchronized
   procedure AddWindowFocusListener (This : access Typ;
                                     P1_WindowFocusListener : access Standard.Java.Awt.Event.WindowFocusListener.Typ'Class);

   --  synchronized
   procedure RemoveWindowListener (This : access Typ;
                                   P1_WindowListener : access Standard.Java.Awt.Event.WindowListener.Typ'Class);

   --  synchronized
   procedure RemoveWindowStateListener (This : access Typ;
                                        P1_WindowStateListener : access Standard.Java.Awt.Event.WindowStateListener.Typ'Class);

   --  synchronized
   procedure RemoveWindowFocusListener (This : access Typ;
                                        P1_WindowFocusListener : access Standard.Java.Awt.Event.WindowFocusListener.Typ'Class);

   --  synchronized
   function GetWindowListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetWindowFocusListeners (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetWindowStateListeners (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ProcessEvent (This : access Typ;
                           P1_AWTEvent : access Standard.Java.Awt.AWTEvent.Typ'Class);

   --  protected
   procedure ProcessWindowEvent (This : access Typ;
                                 P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   --  protected
   procedure ProcessWindowFocusEvent (This : access Typ;
                                      P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   --  protected
   procedure ProcessWindowStateEvent (This : access Typ;
                                      P1_WindowEvent : access Standard.Java.Awt.Event.WindowEvent.Typ'Class);

   --  final
   procedure SetAlwaysOnTop (This : access Typ;
                             P1_Boolean : Java.Boolean);
   --  can raise Java.Lang.SecurityException.Except

   function IsAlwaysOnTopSupported (This : access Typ)
                                    return Java.Boolean;

   --  final
   function IsAlwaysOnTop (This : access Typ)
                           return Java.Boolean;

   function GetFocusOwner (This : access Typ)
                           return access Java.Awt.Component.Typ'Class;

   function GetMostRecentFocusOwner (This : access Typ)
                                     return access Java.Awt.Component.Typ'Class;

   function IsActive (This : access Typ)
                      return Java.Boolean;

   function IsFocused (This : access Typ)
                       return Java.Boolean;

   function GetFocusTraversalKeys (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Util.Set.Typ'Class;

   --  final
   procedure SetFocusCycleRoot (This : access Typ;
                                P1_Boolean : Java.Boolean);

   --  final
   function IsFocusCycleRoot (This : access Typ)
                              return Java.Boolean;

   --  final
   function GetFocusCycleRootAncestor (This : access Typ)
                                       return access Java.Awt.Container.Typ'Class;

   --  final
   function IsFocusableWindow (This : access Typ)
                               return Java.Boolean;

   function GetFocusableWindowState (This : access Typ)
                                     return Java.Boolean;

   procedure SetFocusableWindowState (This : access Typ;
                                      P1_Boolean : Java.Boolean);

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function IsShowing (This : access Typ)
                       return Java.Boolean;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   function GetGraphicsConfiguration (This : access Typ)
                                      return access Java.Awt.GraphicsConfiguration.Typ'Class;

   procedure SetLocationRelativeTo (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure CreateBufferStrategy (This : access Typ;
                                   P1_Int : Java.Int);

   procedure CreateBufferStrategy (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_BufferCapabilities : access Standard.Java.Awt.BufferCapabilities.Typ'Class);
   --  can raise Java.Awt.AWTException.Except

   function GetBufferStrategy (This : access Typ)
                               return access Java.Awt.Image.BufferStrategy.Typ'Class;

   procedure SetLocationByPlatform (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function IsLocationByPlatform (This : access Typ)
                                  return Java.Boolean;

   procedure SetBounds (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure SetBounds (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Window);
   pragma Import (Java, GetIconImages, "getIconImages");
   pragma Import (Java, SetIconImages, "setIconImages");
   pragma Import (Java, SetIconImage, "setIconImage");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, Pack, "pack");
   pragma Import (Java, SetMinimumSize, "setMinimumSize");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, Dispose, "dispose");
   pragma Import (Java, ToFront, "toFront");
   pragma Import (Java, ToBack, "toBack");
   pragma Import (Java, GetToolkit, "getToolkit");
   pragma Import (Java, GetWarningString, "getWarningString");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, GetInputContext, "getInputContext");
   pragma Import (Java, SetCursor, "setCursor");
   pragma Import (Java, GetOwner, "getOwner");
   pragma Import (Java, GetOwnedWindows, "getOwnedWindows");
   pragma Import (Java, GetWindows, "getWindows");
   pragma Import (Java, GetOwnerlessWindows, "getOwnerlessWindows");
   pragma Import (Java, SetModalExclusionType, "setModalExclusionType");
   pragma Import (Java, GetModalExclusionType, "getModalExclusionType");
   pragma Import (Java, AddWindowListener, "addWindowListener");
   pragma Import (Java, AddWindowStateListener, "addWindowStateListener");
   pragma Import (Java, AddWindowFocusListener, "addWindowFocusListener");
   pragma Import (Java, RemoveWindowListener, "removeWindowListener");
   pragma Import (Java, RemoveWindowStateListener, "removeWindowStateListener");
   pragma Import (Java, RemoveWindowFocusListener, "removeWindowFocusListener");
   pragma Import (Java, GetWindowListeners, "getWindowListeners");
   pragma Import (Java, GetWindowFocusListeners, "getWindowFocusListeners");
   pragma Import (Java, GetWindowStateListeners, "getWindowStateListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, ProcessEvent, "processEvent");
   pragma Import (Java, ProcessWindowEvent, "processWindowEvent");
   pragma Import (Java, ProcessWindowFocusEvent, "processWindowFocusEvent");
   pragma Import (Java, ProcessWindowStateEvent, "processWindowStateEvent");
   pragma Import (Java, SetAlwaysOnTop, "setAlwaysOnTop");
   pragma Import (Java, IsAlwaysOnTopSupported, "isAlwaysOnTopSupported");
   pragma Import (Java, IsAlwaysOnTop, "isAlwaysOnTop");
   pragma Import (Java, GetFocusOwner, "getFocusOwner");
   pragma Import (Java, GetMostRecentFocusOwner, "getMostRecentFocusOwner");
   pragma Import (Java, IsActive, "isActive");
   pragma Import (Java, IsFocused, "isFocused");
   pragma Import (Java, GetFocusTraversalKeys, "getFocusTraversalKeys");
   pragma Import (Java, SetFocusCycleRoot, "setFocusCycleRoot");
   pragma Import (Java, IsFocusCycleRoot, "isFocusCycleRoot");
   pragma Import (Java, GetFocusCycleRootAncestor, "getFocusCycleRootAncestor");
   pragma Import (Java, IsFocusableWindow, "isFocusableWindow");
   pragma Import (Java, GetFocusableWindowState, "getFocusableWindowState");
   pragma Import (Java, SetFocusableWindowState, "setFocusableWindowState");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, IsShowing, "isShowing");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, GetGraphicsConfiguration, "getGraphicsConfiguration");
   pragma Import (Java, SetLocationRelativeTo, "setLocationRelativeTo");
   pragma Import (Java, CreateBufferStrategy, "createBufferStrategy");
   pragma Import (Java, GetBufferStrategy, "getBufferStrategy");
   pragma Import (Java, SetLocationByPlatform, "setLocationByPlatform");
   pragma Import (Java, IsLocationByPlatform, "isLocationByPlatform");
   pragma Import (Java, SetBounds, "setBounds");
   pragma Import (Java, Paint, "paint");

end Java.Awt.Window;
pragma Import (Java, Java.Awt.Window, "java.awt.Window");
pragma Extensions_Allowed (Off);
