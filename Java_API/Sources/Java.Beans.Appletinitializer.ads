pragma Extensions_Allowed (On);
limited with Java.Applet.Applet;
limited with Java.Beans.Beancontext.BeanContext;
with Java.Lang.Object;

package Java.Beans.AppletInitializer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Initialize (This : access Typ;
                         P1_Applet : access Standard.Java.Applet.Applet.Typ'Class;
                         P2_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class) is abstract;

   procedure Activate (This : access Typ;
                       P1_Applet : access Standard.Java.Applet.Applet.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Initialize, "initialize");
   pragma Export (Java, Activate, "activate");

end Java.Beans.AppletInitializer;
pragma Import (Java, Java.Beans.AppletInitializer, "java.beans.AppletInitializer");
pragma Extensions_Allowed (Off);
