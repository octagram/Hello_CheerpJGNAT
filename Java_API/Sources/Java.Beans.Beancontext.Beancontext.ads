pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextMembershipListener;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Net.URL;
with Java.Beans.Beancontext.BeanContextChild;
with Java.Beans.DesignMode;
with Java.Beans.Visibility;
with Java.Lang.Object;
with Java.Util.Collection;

package Java.Beans.Beancontext.BeanContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DesignMode_I : Java.Beans.DesignMode.Ref;
            Visibility_I : Java.Beans.Visibility.Ref;
            BeanContextChild_I : Java.Beans.Beancontext.BeanContextChild.Ref;
            Collection_I : Java.Util.Collection.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function InstantiateChild (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function GetResourceAsStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class)
                                 return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetResource (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class)
                         return access Java.Net.URL.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure AddBeanContextMembershipListener (This : access Typ;
                                               P1_BeanContextMembershipListener : access Standard.Java.Beans.Beancontext.BeanContextMembershipListener.Typ'Class) is abstract;

   procedure RemoveBeanContextMembershipListener (This : access Typ;
                                                  P1_BeanContextMembershipListener : access Standard.Java.Beans.Beancontext.BeanContextMembershipListener.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   GlobalHierarchyLock : access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, InstantiateChild, "instantiateChild");
   pragma Export (Java, GetResourceAsStream, "getResourceAsStream");
   pragma Export (Java, GetResource, "getResource");
   pragma Export (Java, AddBeanContextMembershipListener, "addBeanContextMembershipListener");
   pragma Export (Java, RemoveBeanContextMembershipListener, "removeBeanContextMembershipListener");
   pragma Import (Java, GlobalHierarchyLock, "globalHierarchyLock");

end Java.Beans.Beancontext.BeanContext;
pragma Import (Java, Java.Beans.Beancontext.BeanContext, "java.beans.beancontext.BeanContext");
pragma Extensions_Allowed (Off);
