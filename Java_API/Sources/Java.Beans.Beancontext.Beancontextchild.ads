pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContext;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Beans.VetoableChangeListener;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextChild is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetBeanContext (This : access Typ;
                             P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class) is abstract;
   --  can raise Java.Beans.PropertyVetoException.Except

   function GetBeanContext (This : access Typ)
                            return access Java.Beans.Beancontext.BeanContext.Typ'Class is abstract;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure AddVetoableChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class) is abstract;

   procedure RemoveVetoableChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetBeanContext, "setBeanContext");
   pragma Export (Java, GetBeanContext, "getBeanContext");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Export (Java, AddVetoableChangeListener, "addVetoableChangeListener");
   pragma Export (Java, RemoveVetoableChangeListener, "removeVetoableChangeListener");

end Java.Beans.Beancontext.BeanContextChild;
pragma Import (Java, Java.Beans.Beancontext.BeanContextChild, "java.beans.beancontext.BeanContextChild");
pragma Extensions_Allowed (Off);
