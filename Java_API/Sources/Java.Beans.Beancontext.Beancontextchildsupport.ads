pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContext;
limited with Java.Beans.Beancontext.BeanContextServiceAvailableEvent;
limited with Java.Beans.Beancontext.BeanContextServiceRevokedEvent;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Beans.PropertyChangeSupport;
limited with Java.Beans.VetoableChangeListener;
limited with Java.Beans.VetoableChangeSupport;
limited with Java.Lang.String;
with Java.Beans.Beancontext.BeanContextChild;
with Java.Beans.Beancontext.BeanContextServicesListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextChildSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(BeanContextChild_I : Java.Beans.Beancontext.BeanContextChild.Ref;
            BeanContextServicesListener_I : Java.Beans.Beancontext.BeanContextServicesListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      BeanContextChildPeer : access Java.Beans.Beancontext.BeanContextChild.Typ'Class;
      pragma Import (Java, BeanContextChildPeer, "beanContextChildPeer");

      --  protected
      PcSupport : access Java.Beans.PropertyChangeSupport.Typ'Class;
      pragma Import (Java, PcSupport, "pcSupport");

      --  protected
      VcSupport : access Java.Beans.VetoableChangeSupport.Typ'Class;
      pragma Import (Java, VcSupport, "vcSupport");

      --  protected
      BeanContext : access Java.Beans.Beancontext.BeanContext.Typ'Class;
      pragma Import (Java, BeanContext, "beanContext");

      --  protected
      RejectedSetBCOnce : Java.Boolean;
      pragma Import (Java, RejectedSetBCOnce, "rejectedSetBCOnce");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BeanContextChildSupport (This : Ref := null)
                                         return Ref;

   function New_BeanContextChildSupport (P1_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure SetBeanContext (This : access Typ;
                             P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   --  synchronized
   function GetBeanContext (This : access Typ)
                            return access Java.Beans.Beancontext.BeanContext.Typ'Class;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure AddVetoableChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   procedure RemoveVetoableChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   procedure ServiceRevoked (This : access Typ;
                             P1_BeanContextServiceRevokedEvent : access Standard.Java.Beans.Beancontext.BeanContextServiceRevokedEvent.Typ'Class);

   procedure ServiceAvailable (This : access Typ;
                               P1_BeanContextServiceAvailableEvent : access Standard.Java.Beans.Beancontext.BeanContextServiceAvailableEvent.Typ'Class);

   function GetBeanContextChildPeer (This : access Typ)
                                     return access Java.Beans.Beancontext.BeanContextChild.Typ'Class;

   function IsDelegated (This : access Typ)
                         return Java.Boolean;

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure FireVetoableChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   function ValidatePendingSetBeanContext (This : access Typ;
                                           P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class)
                                           return Java.Boolean;

   --  protected
   procedure ReleaseBeanContextResources (This : access Typ);

   --  protected
   procedure InitializeBeanContextResources (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BeanContextChildSupport);
   pragma Import (Java, SetBeanContext, "setBeanContext");
   pragma Import (Java, GetBeanContext, "getBeanContext");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, AddVetoableChangeListener, "addVetoableChangeListener");
   pragma Import (Java, RemoveVetoableChangeListener, "removeVetoableChangeListener");
   pragma Import (Java, ServiceRevoked, "serviceRevoked");
   pragma Import (Java, ServiceAvailable, "serviceAvailable");
   pragma Import (Java, GetBeanContextChildPeer, "getBeanContextChildPeer");
   pragma Import (Java, IsDelegated, "isDelegated");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, FireVetoableChange, "fireVetoableChange");
   pragma Import (Java, ValidatePendingSetBeanContext, "validatePendingSetBeanContext");
   pragma Import (Java, ReleaseBeanContextResources, "releaseBeanContextResources");
   pragma Import (Java, InitializeBeanContextResources, "initializeBeanContextResources");

end Java.Beans.Beancontext.BeanContextChildSupport;
pragma Import (Java, Java.Beans.Beancontext.BeanContextChildSupport, "java.beans.beancontext.BeanContextChildSupport");
pragma Extensions_Allowed (Off);
