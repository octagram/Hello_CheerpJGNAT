pragma Extensions_Allowed (On);
limited with Java.Awt.Container;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextContainerProxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContainer (This : access Typ)
                          return access Java.Awt.Container.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetContainer, "getContainer");

end Java.Beans.Beancontext.BeanContextContainerProxy;
pragma Import (Java, Java.Beans.Beancontext.BeanContextContainerProxy, "java.beans.beancontext.BeanContextContainerProxy");
pragma Extensions_Allowed (Off);
