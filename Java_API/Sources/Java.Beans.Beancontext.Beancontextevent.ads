pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContext;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Beans.Beancontext.BeanContextEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      PropagatedFrom : access Java.Beans.Beancontext.BeanContext.Typ'Class;
      pragma Import (Java, PropagatedFrom, "propagatedFrom");

   end record;

   --  protected
   function New_BeanContextEvent (P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBeanContext (This : access Typ)
                            return access Java.Beans.Beancontext.BeanContext.Typ'Class;

   --  synchronized
   procedure SetPropagatedFrom (This : access Typ;
                                P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class);

   --  synchronized
   function GetPropagatedFrom (This : access Typ)
                               return access Java.Beans.Beancontext.BeanContext.Typ'Class;

   --  synchronized
   function IsPropagated (This : access Typ)
                          return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BeanContextEvent);
   pragma Import (Java, GetBeanContext, "getBeanContext");
   pragma Import (Java, SetPropagatedFrom, "setPropagatedFrom");
   pragma Import (Java, GetPropagatedFrom, "getPropagatedFrom");
   pragma Import (Java, IsPropagated, "isPropagated");

end Java.Beans.Beancontext.BeanContextEvent;
pragma Import (Java, Java.Beans.Beancontext.BeanContextEvent, "java.beans.beancontext.BeanContextEvent");
pragma Extensions_Allowed (Off);
