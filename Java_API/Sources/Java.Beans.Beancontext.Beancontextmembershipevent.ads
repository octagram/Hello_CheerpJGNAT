pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContext;
limited with Java.Util.Collection;
limited with Java.Util.Iterator;
with Java.Beans.Beancontext.BeanContextEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextMembershipEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Beans.Beancontext.BeanContextEvent.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Children : access Java.Util.Collection.Typ'Class;
      pragma Import (Java, Children, "children");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BeanContextMembershipEvent (P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class;
                                            P2_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_BeanContextMembershipEvent (P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class;
                                            P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Size (This : access Typ)
                  return Java.Int;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BeanContextMembershipEvent);
   pragma Import (Java, Size, "size");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Iterator, "iterator");

end Java.Beans.Beancontext.BeanContextMembershipEvent;
pragma Import (Java, Java.Beans.Beancontext.BeanContextMembershipEvent, "java.beans.beancontext.BeanContextMembershipEvent");
pragma Extensions_Allowed (Off);
