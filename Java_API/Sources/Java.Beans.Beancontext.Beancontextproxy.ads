pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextChild;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextProxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBeanContextProxy (This : access Typ)
                                 return access Java.Beans.Beancontext.BeanContextChild.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetBeanContextProxy, "getBeanContextProxy");

end Java.Beans.Beancontext.BeanContextProxy;
pragma Import (Java, Java.Beans.Beancontext.BeanContextProxy, "java.beans.beancontext.BeanContextProxy");
pragma Extensions_Allowed (Off);
