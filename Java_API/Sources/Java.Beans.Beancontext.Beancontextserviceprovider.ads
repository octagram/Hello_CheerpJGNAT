pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextServices;
limited with Java.Lang.Class;
limited with Java.Util.Iterator;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextServiceProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetService (This : access Typ;
                        P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P4_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   procedure ReleaseService (This : access Typ;
                             P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetCurrentServiceSelectors (This : access Typ;
                                        P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class;
                                        P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                                        return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetService, "getService");
   pragma Export (Java, ReleaseService, "releaseService");
   pragma Export (Java, GetCurrentServiceSelectors, "getCurrentServiceSelectors");

end Java.Beans.Beancontext.BeanContextServiceProvider;
pragma Import (Java, Java.Beans.Beancontext.BeanContextServiceProvider, "java.beans.beancontext.BeanContextServiceProvider");
pragma Extensions_Allowed (Off);
