pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextServices;
limited with Java.Lang.Class;
with Java.Beans.Beancontext.BeanContextEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextServiceRevokedEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Beans.Beancontext.BeanContextEvent.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ServiceClass : access Java.Lang.Class.Typ'Class;
      pragma Import (Java, ServiceClass, "serviceClass");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BeanContextServiceRevokedEvent (P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class;
                                                P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                                P3_Boolean : Java.Boolean; 
                                                This : Ref := null)
                                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSourceAsBeanContextServices (This : access Typ)
                                            return access Java.Beans.Beancontext.BeanContextServices.Typ'Class;

   function GetServiceClass (This : access Typ)
                             return access Java.Lang.Class.Typ'Class;

   function IsServiceClass (This : access Typ;
                            P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                            return Java.Boolean;

   function IsCurrentServiceInvalidNow (This : access Typ)
                                        return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BeanContextServiceRevokedEvent);
   pragma Import (Java, GetSourceAsBeanContextServices, "getSourceAsBeanContextServices");
   pragma Import (Java, GetServiceClass, "getServiceClass");
   pragma Import (Java, IsServiceClass, "isServiceClass");
   pragma Import (Java, IsCurrentServiceInvalidNow, "isCurrentServiceInvalidNow");

end Java.Beans.Beancontext.BeanContextServiceRevokedEvent;
pragma Import (Java, Java.Beans.Beancontext.BeanContextServiceRevokedEvent, "java.beans.beancontext.BeanContextServiceRevokedEvent");
pragma Extensions_Allowed (Off);
