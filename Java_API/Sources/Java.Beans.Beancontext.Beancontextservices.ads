pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextChild;
limited with Java.Beans.Beancontext.BeanContextServiceProvider;
limited with Java.Beans.Beancontext.BeanContextServiceRevokedListener;
limited with Java.Lang.Class;
limited with Java.Util.Iterator;
with Java.Beans.Beancontext.BeanContext;
with Java.Beans.Beancontext.BeanContextServicesListener;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextServices is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            BeanContext_I : Java.Beans.Beancontext.BeanContext.Ref;
            BeanContextServicesListener_I : Java.Beans.Beancontext.BeanContextServicesListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddService (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P2_BeanContextServiceProvider : access Standard.Java.Beans.Beancontext.BeanContextServiceProvider.Typ'Class)
                        return Java.Boolean is abstract;

   procedure RevokeService (This : access Typ;
                            P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P2_BeanContextServiceProvider : access Standard.Java.Beans.Beancontext.BeanContextServiceProvider.Typ'Class;
                            P3_Boolean : Java.Boolean) is abstract;

   function HasService (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return Java.Boolean is abstract;

   function GetService (This : access Typ;
                        P1_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P5_BeanContextServiceRevokedListener : access Standard.Java.Beans.Beancontext.BeanContextServiceRevokedListener.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Util.TooManyListenersException.Except

   procedure ReleaseService (This : access Typ;
                             P1_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetCurrentServiceClasses (This : access Typ)
                                      return access Java.Util.Iterator.Typ'Class is abstract;

   function GetCurrentServiceSelectors (This : access Typ;
                                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                        return access Java.Util.Iterator.Typ'Class is abstract;

   procedure AddBeanContextServicesListener (This : access Typ;
                                             P1_BeanContextServicesListener : access Standard.Java.Beans.Beancontext.BeanContextServicesListener.Typ'Class) is abstract;

   procedure RemoveBeanContextServicesListener (This : access Typ;
                                                P1_BeanContextServicesListener : access Standard.Java.Beans.Beancontext.BeanContextServicesListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddService, "addService");
   pragma Export (Java, RevokeService, "revokeService");
   pragma Export (Java, HasService, "hasService");
   pragma Export (Java, GetService, "getService");
   pragma Export (Java, ReleaseService, "releaseService");
   pragma Export (Java, GetCurrentServiceClasses, "getCurrentServiceClasses");
   pragma Export (Java, GetCurrentServiceSelectors, "getCurrentServiceSelectors");
   pragma Export (Java, AddBeanContextServicesListener, "addBeanContextServicesListener");
   pragma Export (Java, RemoveBeanContextServicesListener, "removeBeanContextServicesListener");

end Java.Beans.Beancontext.BeanContextServices;
pragma Import (Java, Java.Beans.Beancontext.BeanContextServices, "java.beans.beancontext.BeanContextServices");
pragma Extensions_Allowed (Off);
