pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextServiceAvailableEvent;
with Java.Beans.Beancontext.BeanContextServiceRevokedListener;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextServicesListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            BeanContextServiceRevokedListener_I : Java.Beans.Beancontext.BeanContextServiceRevokedListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ServiceAvailable (This : access Typ;
                               P1_BeanContextServiceAvailableEvent : access Standard.Java.Beans.Beancontext.BeanContextServiceAvailableEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ServiceAvailable, "serviceAvailable");

end Java.Beans.Beancontext.BeanContextServicesListener;
pragma Import (Java, Java.Beans.Beancontext.BeanContextServicesListener, "java.beans.beancontext.BeanContextServicesListener");
pragma Extensions_Allowed (Off);
