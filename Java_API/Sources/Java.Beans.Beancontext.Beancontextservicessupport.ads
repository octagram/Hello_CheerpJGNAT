pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextServiceAvailableEvent;
limited with Java.Beans.Beancontext.BeanContextServiceProvider;
limited with Java.Beans.Beancontext.BeanContextServiceRevokedEvent;
limited with Java.Beans.Beancontext.BeanContextServiceRevokedListener;
limited with Java.Io.ObjectInputStream;
limited with Java.Io.ObjectOutputStream;
limited with Java.Lang.Class;
limited with Java.Util.ArrayList;
limited with Java.Util.HashMap;
limited with Java.Util.Iterator;
limited with Java.Util.Locale;
with Java.Beans.Beancontext.BeanContext;
with Java.Beans.Beancontext.BeanContextChild;
with Java.Beans.Beancontext.BeanContextServices;
with Java.Beans.Beancontext.BeanContextServicesListener;
with Java.Beans.Beancontext.BeanContextSupport;
with Java.Beans.PropertyChangeListener;
with Java.Beans.VetoableChangeListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextServicesSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            VetoableChangeListener_I : Java.Beans.VetoableChangeListener.Ref;
            BeanContext_I : Java.Beans.Beancontext.BeanContext.Ref;
            BeanContextChild_I : Java.Beans.Beancontext.BeanContextChild.Ref;
            BeanContextServices_I : Java.Beans.Beancontext.BeanContextServices.Ref;
            BeanContextServicesListener_I : Java.Beans.Beancontext.BeanContextServicesListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Beans.Beancontext.BeanContextSupport.Typ(PropertyChangeListener_I,
                                                         VetoableChangeListener_I,
                                                         BeanContext_I,
                                                         BeanContextChild_I,
                                                         BeanContextServicesListener_I,
                                                         Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Services : access Java.Util.HashMap.Typ'Class;
      pragma Import (Java, Services, "services");

      --  protected
      Serializable : Java.Int;
      pragma Import (Java, Serializable, "serializable");

      --  protected
      BcsListeners : access Java.Util.ArrayList.Typ'Class;
      pragma Import (Java, BcsListeners, "bcsListeners");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BeanContextServicesSupport (P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class;
                                            P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                                            P3_Boolean : Java.Boolean;
                                            P4_Boolean : Java.Boolean; 
                                            This : Ref := null)
                                            return Ref;

   function New_BeanContextServicesSupport (P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class;
                                            P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                                            P3_Boolean : Java.Boolean; 
                                            This : Ref := null)
                                            return Ref;

   function New_BeanContextServicesSupport (P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class;
                                            P2_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_BeanContextServicesSupport (P1_BeanContextServices : access Standard.Java.Beans.Beancontext.BeanContextServices.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_BeanContextServicesSupport (This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Initialize (This : access Typ);

   function GetBeanContextServicesPeer (This : access Typ)
                                        return access Java.Beans.Beancontext.BeanContextServices.Typ'Class;

   procedure AddBeanContextServicesListener (This : access Typ;
                                             P1_BeanContextServicesListener : access Standard.Java.Beans.Beancontext.BeanContextServicesListener.Typ'Class);

   procedure RemoveBeanContextServicesListener (This : access Typ;
                                                P1_BeanContextServicesListener : access Standard.Java.Beans.Beancontext.BeanContextServicesListener.Typ'Class);

   function AddService (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P2_BeanContextServiceProvider : access Standard.Java.Beans.Beancontext.BeanContextServiceProvider.Typ'Class)
                        return Java.Boolean;

   --  protected
   function AddService (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P2_BeanContextServiceProvider : access Standard.Java.Beans.Beancontext.BeanContextServiceProvider.Typ'Class;
                        P3_Boolean : Java.Boolean)
                        return Java.Boolean;

   procedure RevokeService (This : access Typ;
                            P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P2_BeanContextServiceProvider : access Standard.Java.Beans.Beancontext.BeanContextServiceProvider.Typ'Class;
                            P3_Boolean : Java.Boolean);

   --  synchronized
   function HasService (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return Java.Boolean;

   function GetService (This : access Typ;
                        P1_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P5_BeanContextServiceRevokedListener : access Standard.Java.Beans.Beancontext.BeanContextServiceRevokedListener.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Util.TooManyListenersException.Except

   procedure ReleaseService (This : access Typ;
                             P1_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetCurrentServiceClasses (This : access Typ)
                                      return access Java.Util.Iterator.Typ'Class;

   function GetCurrentServiceSelectors (This : access Typ;
                                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                        return access Java.Util.Iterator.Typ'Class;

   procedure ServiceAvailable (This : access Typ;
                               P1_BeanContextServiceAvailableEvent : access Standard.Java.Beans.Beancontext.BeanContextServiceAvailableEvent.Typ'Class);

   procedure ServiceRevoked (This : access Typ;
                             P1_BeanContextServiceRevokedEvent : access Standard.Java.Beans.Beancontext.BeanContextServiceRevokedEvent.Typ'Class);

   --  final  protected
   function GetChildBeanContextServicesListener (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                                 return access Java.Beans.Beancontext.BeanContextServicesListener.Typ'Class;

   --  protected  synchronized
   procedure ReleaseBeanContextResources (This : access Typ);

   --  protected  synchronized
   procedure InitializeBeanContextResources (This : access Typ);

   --  final  protected
   procedure FireServiceAdded (This : access Typ;
                               P1_Class : access Standard.Java.Lang.Class.Typ'Class);

   --  final  protected
   procedure FireServiceAdded (This : access Typ;
                               P1_BeanContextServiceAvailableEvent : access Standard.Java.Beans.Beancontext.BeanContextServiceAvailableEvent.Typ'Class);

   --  final  protected
   procedure FireServiceRevoked (This : access Typ;
                                 P1_BeanContextServiceRevokedEvent : access Standard.Java.Beans.Beancontext.BeanContextServiceRevokedEvent.Typ'Class);

   --  final  protected
   procedure FireServiceRevoked (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                 P2_Boolean : Java.Boolean);

   --  protected  synchronized
   procedure BcsPreSerializationHook (This : access Typ;
                                      P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected  synchronized
   procedure BcsPreDeserializationHook (This : access Typ;
                                        P1_ObjectInputStream : access Standard.Java.Io.ObjectInputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BeanContextServicesSupport);
   pragma Import (Java, Initialize, "initialize");
   pragma Import (Java, GetBeanContextServicesPeer, "getBeanContextServicesPeer");
   pragma Import (Java, AddBeanContextServicesListener, "addBeanContextServicesListener");
   pragma Import (Java, RemoveBeanContextServicesListener, "removeBeanContextServicesListener");
   pragma Import (Java, AddService, "addService");
   pragma Import (Java, RevokeService, "revokeService");
   pragma Import (Java, HasService, "hasService");
   pragma Import (Java, GetService, "getService");
   pragma Import (Java, ReleaseService, "releaseService");
   pragma Import (Java, GetCurrentServiceClasses, "getCurrentServiceClasses");
   pragma Import (Java, GetCurrentServiceSelectors, "getCurrentServiceSelectors");
   pragma Import (Java, ServiceAvailable, "serviceAvailable");
   pragma Import (Java, ServiceRevoked, "serviceRevoked");
   pragma Import (Java, GetChildBeanContextServicesListener, "getChildBeanContextServicesListener");
   pragma Import (Java, ReleaseBeanContextResources, "releaseBeanContextResources");
   pragma Import (Java, InitializeBeanContextResources, "initializeBeanContextResources");
   pragma Import (Java, FireServiceAdded, "fireServiceAdded");
   pragma Import (Java, FireServiceRevoked, "fireServiceRevoked");
   pragma Import (Java, BcsPreSerializationHook, "bcsPreSerializationHook");
   pragma Import (Java, BcsPreDeserializationHook, "bcsPreDeserializationHook");

end Java.Beans.Beancontext.BeanContextServicesSupport;
pragma Import (Java, Java.Beans.Beancontext.BeanContextServicesSupport, "java.beans.beancontext.BeanContextServicesSupport");
pragma Extensions_Allowed (Off);
