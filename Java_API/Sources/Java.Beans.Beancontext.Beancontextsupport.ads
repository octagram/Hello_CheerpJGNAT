pragma Extensions_Allowed (On);
limited with Java.Beans.Beancontext.BeanContextMembershipEvent;
limited with Java.Beans.Beancontext.BeanContextMembershipListener;
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Beans.Visibility;
limited with Java.Io.InputStream;
limited with Java.Io.ObjectInputStream;
limited with Java.Io.ObjectOutputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.ArrayList;
limited with Java.Util.Collection;
limited with Java.Util.HashMap;
limited with Java.Util.Iterator;
limited with Java.Util.Locale;
with Java.Beans.Beancontext.BeanContext;
with Java.Beans.Beancontext.BeanContextChild;
with Java.Beans.Beancontext.BeanContextChildSupport;
with Java.Beans.Beancontext.BeanContextServicesListener;
with Java.Beans.PropertyChangeListener;
with Java.Beans.VetoableChangeListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.Beancontext.BeanContextSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            VetoableChangeListener_I : Java.Beans.VetoableChangeListener.Ref;
            BeanContext_I : Java.Beans.Beancontext.BeanContext.Ref;
            BeanContextChild_I : Java.Beans.Beancontext.BeanContextChild.Ref;
            BeanContextServicesListener_I : Java.Beans.Beancontext.BeanContextServicesListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Beans.Beancontext.BeanContextChildSupport.Typ(BeanContextChild_I,
                                                              BeanContextServicesListener_I,
                                                              Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Children : access Java.Util.HashMap.Typ'Class;
      pragma Import (Java, Children, "children");

      --  protected
      BcmListeners : access Java.Util.ArrayList.Typ'Class;
      pragma Import (Java, BcmListeners, "bcmListeners");

      --  protected
      Locale : access Java.Util.Locale.Typ'Class;
      pragma Import (Java, Locale, "locale");

      --  protected
      OkToUseGui : Java.Boolean;
      pragma Import (Java, OkToUseGui, "okToUseGui");

      --  protected
      DesignTime : Java.Boolean;
      pragma Import (Java, DesignTime, "designTime");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BeanContextSupport (P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class;
                                    P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                                    P3_Boolean : Java.Boolean;
                                    P4_Boolean : Java.Boolean; 
                                    This : Ref := null)
                                    return Ref;

   function New_BeanContextSupport (P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class;
                                    P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                                    P3_Boolean : Java.Boolean; 
                                    This : Ref := null)
                                    return Ref;

   function New_BeanContextSupport (P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class;
                                    P2_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_BeanContextSupport (P1_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_BeanContextSupport (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBeanContextPeer (This : access Typ)
                                return access Java.Beans.Beancontext.BeanContext.Typ'Class;

   function InstantiateChild (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  protected
   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean)
                    return Java.Boolean;

   function ContainsAll (This : access Typ;
                         P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                         return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function RemoveAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   function RetainAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   procedure Clear (This : access Typ);

   procedure AddBeanContextMembershipListener (This : access Typ;
                                               P1_BeanContextMembershipListener : access Standard.Java.Beans.Beancontext.BeanContextMembershipListener.Typ'Class);

   procedure RemoveBeanContextMembershipListener (This : access Typ;
                                                  P1_BeanContextMembershipListener : access Standard.Java.Beans.Beancontext.BeanContextMembershipListener.Typ'Class);

   function GetResourceAsStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class)
                                 return access Java.Io.InputStream.Typ'Class;

   function GetResource (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_BeanContextChild : access Standard.Java.Beans.Beancontext.BeanContextChild.Typ'Class)
                         return access Java.Net.URL.Typ'Class;

   --  synchronized
   procedure SetDesignTime (This : access Typ;
                            P1_Boolean : Java.Boolean);

   --  synchronized
   function IsDesignTime (This : access Typ)
                          return Java.Boolean;

   --  synchronized
   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   --  synchronized
   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   --  synchronized
   function NeedsGui (This : access Typ)
                      return Java.Boolean;

   --  synchronized
   procedure DontUseGui (This : access Typ);

   --  synchronized
   procedure OkToUseGui (This : access Typ);

   function AvoidingGui (This : access Typ)
                         return Java.Boolean;

   function IsSerializing (This : access Typ)
                           return Java.Boolean;

   --  protected
   function BcsChildren (This : access Typ)
                         return access Java.Util.Iterator.Typ'Class;

   --  protected
   procedure BcsPreSerializationHook (This : access Typ;
                                      P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure BcsPreDeserializationHook (This : access Typ;
                                        P1_ObjectInputStream : access Standard.Java.Io.ObjectInputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   --  final  protected
   procedure Serialize (This : access Typ;
                        P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class;
                        P2_Collection : access Standard.Java.Util.Collection.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final  protected
   procedure Deserialize (This : access Typ;
                          P1_ObjectInputStream : access Standard.Java.Io.ObjectInputStream.Typ'Class;
                          P2_Collection : access Standard.Java.Util.Collection.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   --  final
   procedure WriteChildren (This : access Typ;
                            P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure ReadChildren (This : access Typ;
                           P1_ObjectInputStream : access Standard.Java.Io.ObjectInputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   procedure VetoableChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  protected
   function ValidatePendingAdd (This : access Typ;
                                P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                return Java.Boolean;

   --  protected
   function ValidatePendingRemove (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return Java.Boolean;

   --  final  protected
   function GetChildVisibility (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                return access Java.Beans.Visibility.Typ'Class;

   --  final  protected
   function GetChildSerializable (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Io.Serializable.Typ'Class;

   --  final  protected
   function GetChildPropertyChangeListener (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                            return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  final  protected
   function GetChildVetoableChangeListener (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                            return access Java.Beans.VetoableChangeListener.Typ'Class;

   --  final  protected
   function GetChildBeanContextMembershipListener (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                                   return access Java.Beans.Beancontext.BeanContextMembershipListener.Typ'Class;

   --  final  protected
   function GetChildBeanContextChild (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                      return access Java.Beans.Beancontext.BeanContextChild.Typ'Class;

   --  final  protected
   procedure FireChildrenAdded (This : access Typ;
                                P1_BeanContextMembershipEvent : access Standard.Java.Beans.Beancontext.BeanContextMembershipEvent.Typ'Class);

   --  final  protected
   procedure FireChildrenRemoved (This : access Typ;
                                  P1_BeanContextMembershipEvent : access Standard.Java.Beans.Beancontext.BeanContextMembershipEvent.Typ'Class);

   --  protected  synchronized
   procedure Initialize (This : access Typ);

   --  final  protected
   function CopyChildren (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   --  final  protected
   function ClassEquals (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BeanContextSupport);
   pragma Import (Java, GetBeanContextPeer, "getBeanContextPeer");
   pragma Import (Java, InstantiateChild, "instantiateChild");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ContainsAll, "containsAll");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, RetainAll, "retainAll");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, AddBeanContextMembershipListener, "addBeanContextMembershipListener");
   pragma Import (Java, RemoveBeanContextMembershipListener, "removeBeanContextMembershipListener");
   pragma Import (Java, GetResourceAsStream, "getResourceAsStream");
   pragma Import (Java, GetResource, "getResource");
   pragma Import (Java, SetDesignTime, "setDesignTime");
   pragma Import (Java, IsDesignTime, "isDesignTime");
   pragma Import (Java, SetLocale, "setLocale");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, NeedsGui, "needsGui");
   pragma Import (Java, DontUseGui, "dontUseGui");
   pragma Import (Java, OkToUseGui, "okToUseGui");
   pragma Import (Java, AvoidingGui, "avoidingGui");
   pragma Import (Java, IsSerializing, "isSerializing");
   pragma Import (Java, BcsChildren, "bcsChildren");
   pragma Import (Java, BcsPreSerializationHook, "bcsPreSerializationHook");
   pragma Import (Java, BcsPreDeserializationHook, "bcsPreDeserializationHook");
   pragma Import (Java, Serialize, "serialize");
   pragma Import (Java, Deserialize, "deserialize");
   pragma Import (Java, WriteChildren, "writeChildren");
   pragma Import (Java, ReadChildren, "readChildren");
   pragma Import (Java, VetoableChange, "vetoableChange");
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, ValidatePendingAdd, "validatePendingAdd");
   pragma Import (Java, ValidatePendingRemove, "validatePendingRemove");
   pragma Import (Java, GetChildVisibility, "getChildVisibility");
   pragma Import (Java, GetChildSerializable, "getChildSerializable");
   pragma Import (Java, GetChildPropertyChangeListener, "getChildPropertyChangeListener");
   pragma Import (Java, GetChildVetoableChangeListener, "getChildVetoableChangeListener");
   pragma Import (Java, GetChildBeanContextMembershipListener, "getChildBeanContextMembershipListener");
   pragma Import (Java, GetChildBeanContextChild, "getChildBeanContextChild");
   pragma Import (Java, FireChildrenAdded, "fireChildrenAdded");
   pragma Import (Java, FireChildrenRemoved, "fireChildrenRemoved");
   pragma Import (Java, Initialize, "initialize");
   pragma Import (Java, CopyChildren, "copyChildren");
   pragma Import (Java, ClassEquals, "classEquals");

end Java.Beans.Beancontext.BeanContextSupport;
pragma Import (Java, Java.Beans.Beancontext.BeanContextSupport, "java.beans.beancontext.BeanContextSupport");
pragma Extensions_Allowed (Off);
