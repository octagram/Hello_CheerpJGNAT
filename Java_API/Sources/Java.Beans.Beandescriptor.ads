pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
with Java.Beans.FeatureDescriptor;
with Java.Lang.Object;

package Java.Beans.BeanDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.FeatureDescriptor.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BeanDescriptor (P1_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_BeanDescriptor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBeanClass (This : access Typ)
                          return access Java.Lang.Class.Typ'Class;

   function GetCustomizerClass (This : access Typ)
                                return access Java.Lang.Class.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BeanDescriptor);
   pragma Import (Java, GetBeanClass, "getBeanClass");
   pragma Import (Java, GetCustomizerClass, "getCustomizerClass");

end Java.Beans.BeanDescriptor;
pragma Import (Java, Java.Beans.BeanDescriptor, "java.beans.BeanDescriptor");
pragma Extensions_Allowed (Off);
