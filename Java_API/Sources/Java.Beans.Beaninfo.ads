pragma Extensions_Allowed (On);
limited with Java.Awt.Image;
limited with Java.Beans.BeanDescriptor;
limited with Java.Beans.EventSetDescriptor;
limited with Java.Beans.MethodDescriptor;
limited with Java.Beans.PropertyDescriptor;
with Java.Lang.Object;

package Java.Beans.BeanInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBeanDescriptor (This : access Typ)
                               return access Java.Beans.BeanDescriptor.Typ'Class is abstract;

   function GetEventSetDescriptors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function GetDefaultEventIndex (This : access Typ)
                                  return Java.Int is abstract;

   function GetPropertyDescriptors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function GetDefaultPropertyIndex (This : access Typ)
                                     return Java.Int is abstract;

   function GetMethodDescriptors (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref is abstract;

   function GetAdditionalBeanInfo (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref is abstract;

   function GetIcon (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Awt.Image.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ICON_COLOR_16x16 : constant Java.Int;

   --  final
   ICON_COLOR_32x32 : constant Java.Int;

   --  final
   ICON_MONO_16x16 : constant Java.Int;

   --  final
   ICON_MONO_32x32 : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetBeanDescriptor, "getBeanDescriptor");
   pragma Export (Java, GetEventSetDescriptors, "getEventSetDescriptors");
   pragma Export (Java, GetDefaultEventIndex, "getDefaultEventIndex");
   pragma Export (Java, GetPropertyDescriptors, "getPropertyDescriptors");
   pragma Export (Java, GetDefaultPropertyIndex, "getDefaultPropertyIndex");
   pragma Export (Java, GetMethodDescriptors, "getMethodDescriptors");
   pragma Export (Java, GetAdditionalBeanInfo, "getAdditionalBeanInfo");
   pragma Export (Java, GetIcon, "getIcon");
   pragma Import (Java, ICON_COLOR_16x16, "ICON_COLOR_16x16");
   pragma Import (Java, ICON_COLOR_32x32, "ICON_COLOR_32x32");
   pragma Import (Java, ICON_MONO_16x16, "ICON_MONO_16x16");
   pragma Import (Java, ICON_MONO_32x32, "ICON_MONO_32x32");

end Java.Beans.BeanInfo;
pragma Import (Java, Java.Beans.BeanInfo, "java.beans.BeanInfo");
pragma Extensions_Allowed (Off);
