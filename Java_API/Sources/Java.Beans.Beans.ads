pragma Extensions_Allowed (On);
limited with Java.Beans.AppletInitializer;
limited with Java.Beans.Beancontext.BeanContext;
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Beans.Beans is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Beans (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Instantiate (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function Instantiate (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function Instantiate (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_BeanContext : access Standard.Java.Beans.Beancontext.BeanContext.Typ'Class;
                         P4_AppletInitializer : access Standard.Java.Beans.AppletInitializer.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function GetInstanceOf (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   function IsInstanceOf (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Java.Boolean;

   function IsDesignTime return Java.Boolean;

   function IsGuiAvailable return Java.Boolean;

   procedure SetDesignTime (P1_Boolean : Java.Boolean);
   --  can raise Java.Lang.SecurityException.Except

   procedure SetGuiAvailable (P1_Boolean : Java.Boolean);
   --  can raise Java.Lang.SecurityException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Beans);
   pragma Import (Java, Instantiate, "instantiate");
   pragma Import (Java, GetInstanceOf, "getInstanceOf");
   pragma Import (Java, IsInstanceOf, "isInstanceOf");
   pragma Import (Java, IsDesignTime, "isDesignTime");
   pragma Import (Java, IsGuiAvailable, "isGuiAvailable");
   pragma Import (Java, SetDesignTime, "setDesignTime");
   pragma Import (Java, SetGuiAvailable, "setGuiAvailable");

end Java.Beans.Beans;
pragma Import (Java, Java.Beans.Beans, "java.beans.Beans");
pragma Extensions_Allowed (Off);
