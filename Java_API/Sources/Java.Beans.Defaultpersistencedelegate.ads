pragma Extensions_Allowed (On);
limited with Java.Beans.Encoder;
limited with Java.Beans.Expression;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Beans.PersistenceDelegate;
with Java.Lang.Object;

package Java.Beans.DefaultPersistenceDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.PersistenceDelegate.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultPersistenceDelegate (This : Ref := null)
                                            return Ref;

   function New_DefaultPersistenceDelegate (P1_String_Arr : access Java.Lang.String.Arr_Obj; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function MutatesTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   --  protected
   function Instantiate (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Encoder : access Standard.Java.Beans.Encoder.Typ'Class)
                         return access Java.Beans.Expression.Typ'Class;

   --  protected
   procedure Initialize (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P4_Encoder : access Standard.Java.Beans.Encoder.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultPersistenceDelegate);
   pragma Import (Java, MutatesTo, "mutatesTo");
   pragma Import (Java, Instantiate, "instantiate");
   pragma Import (Java, Initialize, "initialize");

end Java.Beans.DefaultPersistenceDelegate;
pragma Import (Java, Java.Beans.DefaultPersistenceDelegate, "java.beans.DefaultPersistenceDelegate");
pragma Extensions_Allowed (Off);
