pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Beans.DesignMode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDesignTime (This : access Typ;
                            P1_Boolean : Java.Boolean) is abstract;

   function IsDesignTime (This : access Typ)
                          return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PROPERTYNAME : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetDesignTime, "setDesignTime");
   pragma Export (Java, IsDesignTime, "isDesignTime");
   pragma Import (Java, PROPERTYNAME, "PROPERTYNAME");

end Java.Beans.DesignMode;
pragma Import (Java, Java.Beans.DesignMode, "java.beans.DesignMode");
pragma Extensions_Allowed (Off);
