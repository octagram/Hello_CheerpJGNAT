pragma Extensions_Allowed (On);
limited with Java.Beans.ExceptionListener;
limited with Java.Beans.Expression;
limited with Java.Beans.PersistenceDelegate;
limited with Java.Beans.Statement;
limited with Java.Lang.Class;
with Java.Lang.Object;

package Java.Beans.Encoder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Encoder (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure WriteObject (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure SetExceptionListener (This : access Typ;
                                   P1_ExceptionListener : access Standard.Java.Beans.ExceptionListener.Typ'Class);

   function GetExceptionListener (This : access Typ)
                                  return access Java.Beans.ExceptionListener.Typ'Class;

   function GetPersistenceDelegate (This : access Typ;
                                    P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                    return access Java.Beans.PersistenceDelegate.Typ'Class;

   procedure SetPersistenceDelegate (This : access Typ;
                                     P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                     P2_PersistenceDelegate : access Standard.Java.Beans.PersistenceDelegate.Typ'Class);

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure WriteStatement (This : access Typ;
                             P1_Statement : access Standard.Java.Beans.Statement.Typ'Class);

   procedure WriteExpression (This : access Typ;
                              P1_Expression : access Standard.Java.Beans.Expression.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Encoder);
   pragma Import (Java, WriteObject, "writeObject");
   pragma Import (Java, SetExceptionListener, "setExceptionListener");
   pragma Import (Java, GetExceptionListener, "getExceptionListener");
   pragma Import (Java, GetPersistenceDelegate, "getPersistenceDelegate");
   pragma Import (Java, SetPersistenceDelegate, "setPersistenceDelegate");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Get, "get");
   pragma Import (Java, WriteStatement, "writeStatement");
   pragma Import (Java, WriteExpression, "writeExpression");

end Java.Beans.Encoder;
pragma Import (Java, Java.Beans.Encoder, "java.beans.Encoder");
pragma Extensions_Allowed (Off);
