pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Lang.Reflect.InvocationHandler;

package Java.Beans.EventHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(InvocationHandler_I : Java.Lang.Reflect.InvocationHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EventHandler (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTarget (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function GetAction (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetEventPropertyName (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   function GetListenerMethodName (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   function Invoke (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class;

   function Create (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function Create (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                    P4_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function Create (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                    P4_String : access Standard.Java.Lang.String.Typ'Class;
                    P5_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EventHandler);
   pragma Import (Java, GetTarget, "getTarget");
   pragma Import (Java, GetAction, "getAction");
   pragma Import (Java, GetEventPropertyName, "getEventPropertyName");
   pragma Import (Java, GetListenerMethodName, "getListenerMethodName");
   pragma Import (Java, Invoke, "invoke");
   pragma Import (Java, Create, "create");

end Java.Beans.EventHandler;
pragma Import (Java, Java.Beans.EventHandler, "java.beans.EventHandler");
pragma Extensions_Allowed (Off);
