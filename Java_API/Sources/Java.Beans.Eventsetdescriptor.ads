pragma Extensions_Allowed (On);
limited with Java.Beans.MethodDescriptor;
limited with Java.Lang.Class;
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
with Java.Beans.FeatureDescriptor;
with Java.Lang.Object;

package Java.Beans.EventSetDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.FeatureDescriptor.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EventSetDescriptor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P4_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_EventSetDescriptor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P4_String_Arr : access Java.Lang.String.Arr_Obj;
                                    P5_String : access Standard.Java.Lang.String.Typ'Class;
                                    P6_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_EventSetDescriptor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P4_String_Arr : access Java.Lang.String.Arr_Obj;
                                    P5_String : access Standard.Java.Lang.String.Typ'Class;
                                    P6_String : access Standard.Java.Lang.String.Typ'Class;
                                    P7_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_EventSetDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P3_Method_Arr : access Java.Lang.Reflect.Method.Arr_Obj;
                                    P4_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                    P5_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_EventSetDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P3_Method_Arr : access Java.Lang.Reflect.Method.Arr_Obj;
                                    P4_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                    P5_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                    P6_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_EventSetDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P3_MethodDescriptor_Arr : access Java.Beans.MethodDescriptor.Arr_Obj;
                                    P4_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                    P5_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetListenerType (This : access Typ)
                             return access Java.Lang.Class.Typ'Class;

   --  synchronized
   function GetListenerMethods (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetListenerMethodDescriptors (This : access Typ)
                                          return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetAddListenerMethod (This : access Typ)
                                  return access Java.Lang.Reflect.Method.Typ'Class;

   --  synchronized
   function GetRemoveListenerMethod (This : access Typ)
                                     return access Java.Lang.Reflect.Method.Typ'Class;

   --  synchronized
   function GetGetListenerMethod (This : access Typ)
                                  return access Java.Lang.Reflect.Method.Typ'Class;

   procedure SetUnicast (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsUnicast (This : access Typ)
                       return Java.Boolean;

   procedure SetInDefaultEventSet (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function IsInDefaultEventSet (This : access Typ)
                                 return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EventSetDescriptor);
   pragma Import (Java, GetListenerType, "getListenerType");
   pragma Import (Java, GetListenerMethods, "getListenerMethods");
   pragma Import (Java, GetListenerMethodDescriptors, "getListenerMethodDescriptors");
   pragma Import (Java, GetAddListenerMethod, "getAddListenerMethod");
   pragma Import (Java, GetRemoveListenerMethod, "getRemoveListenerMethod");
   pragma Import (Java, GetGetListenerMethod, "getGetListenerMethod");
   pragma Import (Java, SetUnicast, "setUnicast");
   pragma Import (Java, IsUnicast, "isUnicast");
   pragma Import (Java, SetInDefaultEventSet, "setInDefaultEventSet");
   pragma Import (Java, IsInDefaultEventSet, "isInDefaultEventSet");

end Java.Beans.EventSetDescriptor;
pragma Import (Java, Java.Beans.EventSetDescriptor, "java.beans.EventSetDescriptor");
pragma Extensions_Allowed (Off);
