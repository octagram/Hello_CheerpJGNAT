pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Beans.Statement;
with Java.Lang.Object;

package Java.Beans.Expression is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.Statement.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Expression (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                            This : Ref := null)
                            return Ref;

   function New_Expression (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Expression);
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, ToString, "toString");

end Java.Beans.Expression;
pragma Import (Java, Java.Beans.Expression, "java.beans.Expression");
pragma Extensions_Allowed (Off);
