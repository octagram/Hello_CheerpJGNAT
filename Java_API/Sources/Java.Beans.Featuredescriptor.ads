pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Java.Beans.FeatureDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FeatureDescriptor (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetDisplayName (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetDisplayName (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   function IsExpert (This : access Typ)
                      return Java.Boolean;

   procedure SetExpert (This : access Typ;
                        P1_Boolean : Java.Boolean);

   function IsHidden (This : access Typ)
                      return Java.Boolean;

   procedure SetHidden (This : access Typ;
                        P1_Boolean : Java.Boolean);

   function IsPreferred (This : access Typ)
                         return Java.Boolean;

   procedure SetPreferred (This : access Typ;
                           P1_Boolean : Java.Boolean);

   function GetShortDescription (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   procedure SetShortDescription (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function AttributeNames (This : access Typ)
                            return access Java.Util.Enumeration.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FeatureDescriptor);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetName, "setName");
   pragma Import (Java, GetDisplayName, "getDisplayName");
   pragma Import (Java, SetDisplayName, "setDisplayName");
   pragma Import (Java, IsExpert, "isExpert");
   pragma Import (Java, SetExpert, "setExpert");
   pragma Import (Java, IsHidden, "isHidden");
   pragma Import (Java, SetHidden, "setHidden");
   pragma Import (Java, IsPreferred, "isPreferred");
   pragma Import (Java, SetPreferred, "setPreferred");
   pragma Import (Java, GetShortDescription, "getShortDescription");
   pragma Import (Java, SetShortDescription, "setShortDescription");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, AttributeNames, "attributeNames");

end Java.Beans.FeatureDescriptor;
pragma Import (Java, Java.Beans.FeatureDescriptor, "java.beans.FeatureDescriptor");
pragma Extensions_Allowed (Off);
