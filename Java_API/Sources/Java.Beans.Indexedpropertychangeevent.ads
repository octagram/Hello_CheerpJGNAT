pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Beans.PropertyChangeEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.IndexedPropertyChangeEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Beans.PropertyChangeEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IndexedPropertyChangeEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                                            P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                                            P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                                            P5_Int : Java.Int; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIndex (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IndexedPropertyChangeEvent);
   pragma Import (Java, GetIndex, "getIndex");

end Java.Beans.IndexedPropertyChangeEvent;
pragma Import (Java, Java.Beans.IndexedPropertyChangeEvent, "java.beans.IndexedPropertyChangeEvent");
pragma Extensions_Allowed (Off);
