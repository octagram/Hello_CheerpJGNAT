pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
with Java.Beans.PropertyDescriptor;
with Java.Lang.Object;

package Java.Beans.IndexedPropertyDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.PropertyDescriptor.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IndexedPropertyDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_IndexedPropertyDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                                           P6_String : access Standard.Java.Lang.String.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_IndexedPropertyDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                           P3_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                           P4_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                           P5_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetIndexedReadMethod (This : access Typ)
                                  return access Java.Lang.Reflect.Method.Typ'Class;

   --  synchronized
   procedure SetIndexedReadMethod (This : access Typ;
                                   P1_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class);
   --  can raise Java.Beans.IntrospectionException.Except

   --  synchronized
   function GetIndexedWriteMethod (This : access Typ)
                                   return access Java.Lang.Reflect.Method.Typ'Class;

   --  synchronized
   procedure SetIndexedWriteMethod (This : access Typ;
                                    P1_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class);
   --  can raise Java.Beans.IntrospectionException.Except

   --  synchronized
   function GetIndexedPropertyType (This : access Typ)
                                    return access Java.Lang.Class.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IndexedPropertyDescriptor);
   pragma Import (Java, GetIndexedReadMethod, "getIndexedReadMethod");
   pragma Import (Java, SetIndexedReadMethod, "setIndexedReadMethod");
   pragma Import (Java, GetIndexedWriteMethod, "getIndexedWriteMethod");
   pragma Import (Java, SetIndexedWriteMethod, "setIndexedWriteMethod");
   pragma Import (Java, GetIndexedPropertyType, "getIndexedPropertyType");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Beans.IndexedPropertyDescriptor;
pragma Import (Java, Java.Beans.IndexedPropertyDescriptor, "java.beans.IndexedPropertyDescriptor");
pragma Extensions_Allowed (Off);
