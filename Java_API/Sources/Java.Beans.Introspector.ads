pragma Extensions_Allowed (On);
limited with Java.Beans.BeanInfo;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Beans.Introspector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBeanInfo (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return access Java.Beans.BeanInfo.Typ'Class;
   --  can raise Java.Beans.IntrospectionException.Except

   function GetBeanInfo (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Int : Java.Int)
                         return access Java.Beans.BeanInfo.Typ'Class;
   --  can raise Java.Beans.IntrospectionException.Except

   function GetBeanInfo (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return access Java.Beans.BeanInfo.Typ'Class;
   --  can raise Java.Beans.IntrospectionException.Except

   function Decapitalize (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetBeanInfoSearchPath return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure SetBeanInfoSearchPath (P1_String_Arr : access Java.Lang.String.Arr_Obj);

   procedure FlushCaches ;

   procedure FlushFromCaches (P1_Class : access Standard.Java.Lang.Class.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   USE_ALL_BEANINFO : constant Java.Int;

   --  final
   IGNORE_IMMEDIATE_BEANINFO : constant Java.Int;

   --  final
   IGNORE_ALL_BEANINFO : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetBeanInfo, "getBeanInfo");
   pragma Import (Java, Decapitalize, "decapitalize");
   pragma Import (Java, GetBeanInfoSearchPath, "getBeanInfoSearchPath");
   pragma Import (Java, SetBeanInfoSearchPath, "setBeanInfoSearchPath");
   pragma Import (Java, FlushCaches, "flushCaches");
   pragma Import (Java, FlushFromCaches, "flushFromCaches");
   pragma Import (Java, USE_ALL_BEANINFO, "USE_ALL_BEANINFO");
   pragma Import (Java, IGNORE_IMMEDIATE_BEANINFO, "IGNORE_IMMEDIATE_BEANINFO");
   pragma Import (Java, IGNORE_ALL_BEANINFO, "IGNORE_ALL_BEANINFO");

end Java.Beans.Introspector;
pragma Import (Java, Java.Beans.Introspector, "java.beans.Introspector");
pragma Extensions_Allowed (Off);
