pragma Extensions_Allowed (On);
limited with Java.Beans.ParameterDescriptor;
limited with Java.Lang.Reflect.Method;
with Java.Beans.FeatureDescriptor;
with Java.Lang.Object;

package Java.Beans.MethodDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.FeatureDescriptor.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MethodDescriptor (P1_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_MethodDescriptor (P1_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                  P2_ParameterDescriptor_Arr : access Java.Beans.ParameterDescriptor.Arr_Obj; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetMethod (This : access Typ)
                       return access Java.Lang.Reflect.Method.Typ'Class;

   function GetParameterDescriptors (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MethodDescriptor);
   pragma Import (Java, GetMethod, "getMethod");
   pragma Import (Java, GetParameterDescriptors, "getParameterDescriptors");

end Java.Beans.MethodDescriptor;
pragma Import (Java, Java.Beans.MethodDescriptor, "java.beans.MethodDescriptor");
pragma Extensions_Allowed (Off);
