pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Beans.PropertyChangeEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyChangeEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P4_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPropertyName (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetNewValue (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function GetOldValue (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   procedure SetPropagationId (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetPropagationId (This : access Typ)
                              return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyChangeEvent);
   pragma Import (Java, GetPropertyName, "getPropertyName");
   pragma Import (Java, GetNewValue, "getNewValue");
   pragma Import (Java, GetOldValue, "getOldValue");
   pragma Import (Java, SetPropagationId, "setPropagationId");
   pragma Import (Java, GetPropagationId, "getPropagationId");

end Java.Beans.PropertyChangeEvent;
pragma Import (Java, Java.Beans.PropertyChangeEvent, "java.beans.PropertyChangeEvent");
pragma Extensions_Allowed (Off);
