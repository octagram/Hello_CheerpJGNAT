pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.String;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;
with Java.Util.EventListener;
with Java.Util.EventListenerProxy;

package Java.Beans.PropertyChangeListenerProxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            EventListener_I : Java.Util.EventListener.Ref)
    is new Java.Util.EventListenerProxy.Typ(EventListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyChangeListenerProxy (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                             P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   function GetPropertyName (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyChangeListenerProxy);
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, GetPropertyName, "getPropertyName");

end Java.Beans.PropertyChangeListenerProxy;
pragma Import (Java, Java.Beans.PropertyChangeListenerProxy, "java.beans.PropertyChangeListenerProxy");
pragma Extensions_Allowed (Off);
