pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.PropertyChangeSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyChangeSupport (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Boolean : Java.Boolean;
                                 P3_Boolean : Java.Boolean);

   procedure FirePropertyChange (This : access Typ;
                                 P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   procedure FireIndexedPropertyChange (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P4_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure FireIndexedPropertyChange (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int);

   procedure FireIndexedPropertyChange (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Boolean : Java.Boolean;
                                        P4_Boolean : Java.Boolean);

   --  synchronized
   function HasListeners (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyChangeSupport);
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, FireIndexedPropertyChange, "fireIndexedPropertyChange");
   pragma Import (Java, HasListeners, "hasListeners");

end Java.Beans.PropertyChangeSupport;
pragma Import (Java, Java.Beans.PropertyChangeSupport, "java.beans.PropertyChangeSupport");
pragma Extensions_Allowed (Off);
