pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyEditor;
limited with Java.Lang.Class;
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
with Java.Beans.FeatureDescriptor;
with Java.Lang.Object;

package Java.Beans.PropertyDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.FeatureDescriptor.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_PropertyDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                                    P4_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   function New_PropertyDescriptor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                    P3_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Beans.IntrospectionException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetPropertyType (This : access Typ)
                             return access Java.Lang.Class.Typ'Class;

   --  synchronized
   function GetReadMethod (This : access Typ)
                           return access Java.Lang.Reflect.Method.Typ'Class;

   --  synchronized
   procedure SetReadMethod (This : access Typ;
                            P1_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class);
   --  can raise Java.Beans.IntrospectionException.Except

   --  synchronized
   function GetWriteMethod (This : access Typ)
                            return access Java.Lang.Reflect.Method.Typ'Class;

   --  synchronized
   procedure SetWriteMethod (This : access Typ;
                             P1_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class);
   --  can raise Java.Beans.IntrospectionException.Except

   function IsBound (This : access Typ)
                     return Java.Boolean;

   procedure SetBound (This : access Typ;
                       P1_Boolean : Java.Boolean);

   function IsConstrained (This : access Typ)
                           return Java.Boolean;

   procedure SetConstrained (This : access Typ;
                             P1_Boolean : Java.Boolean);

   procedure SetPropertyEditorClass (This : access Typ;
                                     P1_Class : access Standard.Java.Lang.Class.Typ'Class);

   function GetPropertyEditorClass (This : access Typ)
                                    return access Java.Lang.Class.Typ'Class;

   function CreatePropertyEditor (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Beans.PropertyEditor.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyDescriptor);
   pragma Import (Java, GetPropertyType, "getPropertyType");
   pragma Import (Java, GetReadMethod, "getReadMethod");
   pragma Import (Java, SetReadMethod, "setReadMethod");
   pragma Import (Java, GetWriteMethod, "getWriteMethod");
   pragma Import (Java, SetWriteMethod, "setWriteMethod");
   pragma Import (Java, IsBound, "isBound");
   pragma Import (Java, SetBound, "setBound");
   pragma Import (Java, IsConstrained, "isConstrained");
   pragma Import (Java, SetConstrained, "setConstrained");
   pragma Import (Java, SetPropertyEditorClass, "setPropertyEditorClass");
   pragma Import (Java, GetPropertyEditorClass, "getPropertyEditorClass");
   pragma Import (Java, CreatePropertyEditor, "createPropertyEditor");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Beans.PropertyDescriptor;
pragma Import (Java, Java.Beans.PropertyDescriptor, "java.beans.PropertyDescriptor");
pragma Extensions_Allowed (Off);
