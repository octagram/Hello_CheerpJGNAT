pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Beans.PropertyEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function IsPaintable (This : access Typ)
                         return Java.Boolean is abstract;

   procedure PaintValue (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class) is abstract;

   function GetJavaInitializationString (This : access Typ)
                                         return access Java.Lang.String.Typ'Class is abstract;

   function GetAsText (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAsText (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetTags (This : access Typ)
                     return Standard.Java.Lang.Object.Ref is abstract;

   function GetCustomEditor (This : access Typ)
                             return access Java.Awt.Component.Typ'Class is abstract;

   function SupportsCustomEditor (This : access Typ)
                                  return Java.Boolean is abstract;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, IsPaintable, "isPaintable");
   pragma Export (Java, PaintValue, "paintValue");
   pragma Export (Java, GetJavaInitializationString, "getJavaInitializationString");
   pragma Export (Java, GetAsText, "getAsText");
   pragma Export (Java, SetAsText, "setAsText");
   pragma Export (Java, GetTags, "getTags");
   pragma Export (Java, GetCustomEditor, "getCustomEditor");
   pragma Export (Java, SupportsCustomEditor, "supportsCustomEditor");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");

end Java.Beans.PropertyEditor;
pragma Import (Java, Java.Beans.PropertyEditor, "java.beans.PropertyEditor");
pragma Extensions_Allowed (Off);
