pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyEditor;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Beans.PropertyEditorManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyEditorManager (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure RegisterEditor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class);

   function FindEditor (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Java.Beans.PropertyEditor.Typ'Class;

   --  synchronized
   function GetEditorSearchPath return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure SetEditorSearchPath (P1_String_Arr : access Java.Lang.String.Arr_Obj);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyEditorManager);
   pragma Import (Java, RegisterEditor, "registerEditor");
   pragma Import (Java, FindEditor, "findEditor");
   pragma Import (Java, GetEditorSearchPath, "getEditorSearchPath");
   pragma Import (Java, SetEditorSearchPath, "setEditorSearchPath");

end Java.Beans.PropertyEditorManager;
pragma Import (Java, Java.Beans.PropertyEditorManager, "java.beans.PropertyEditorManager");
pragma Extensions_Allowed (Off);
