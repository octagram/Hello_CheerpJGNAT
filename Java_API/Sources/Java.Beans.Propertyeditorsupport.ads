pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
with Java.Beans.PropertyEditor;
with Java.Lang.Object;

package Java.Beans.PropertyEditorSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyEditor_I : Java.Beans.PropertyEditor.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyEditorSupport (This : Ref := null)
                                       return Ref;

   function New_PropertyEditorSupport (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSource (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   procedure SetSource (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function IsPaintable (This : access Typ)
                         return Java.Boolean;

   procedure PaintValue (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetJavaInitializationString (This : access Typ)
                                         return access Java.Lang.String.Typ'Class;

   function GetAsText (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   procedure SetAsText (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetTags (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function GetCustomEditor (This : access Typ)
                             return access Java.Awt.Component.Typ'Class;

   function SupportsCustomEditor (This : access Typ)
                                  return Java.Boolean;

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure FirePropertyChange (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyEditorSupport);
   pragma Import (Java, GetSource, "getSource");
   pragma Import (Java, SetSource, "setSource");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, IsPaintable, "isPaintable");
   pragma Import (Java, PaintValue, "paintValue");
   pragma Import (Java, GetJavaInitializationString, "getJavaInitializationString");
   pragma Import (Java, GetAsText, "getAsText");
   pragma Import (Java, SetAsText, "setAsText");
   pragma Import (Java, GetTags, "getTags");
   pragma Import (Java, GetCustomEditor, "getCustomEditor");
   pragma Import (Java, SupportsCustomEditor, "supportsCustomEditor");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");

end Java.Beans.PropertyEditorSupport;
pragma Import (Java, Java.Beans.PropertyEditorSupport, "java.beans.PropertyEditorSupport");
pragma Extensions_Allowed (Off);
