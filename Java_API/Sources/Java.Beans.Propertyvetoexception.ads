pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Java.Beans.PropertyVetoException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyVetoException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPropertyChangeEvent (This : access Typ)
                                    return access Java.Beans.PropertyChangeEvent.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.beans.PropertyVetoException");
   pragma Java_Constructor (New_PropertyVetoException);
   pragma Import (Java, GetPropertyChangeEvent, "getPropertyChangeEvent");

end Java.Beans.PropertyVetoException;
pragma Import (Java, Java.Beans.PropertyVetoException, "java.beans.PropertyVetoException");
pragma Extensions_Allowed (Off);
