pragma Extensions_Allowed (On);
limited with Java.Awt.Image;
limited with Java.Beans.BeanDescriptor;
limited with Java.Beans.EventSetDescriptor;
limited with Java.Beans.MethodDescriptor;
limited with Java.Beans.PropertyDescriptor;
limited with Java.Lang.String;
with Java.Beans.BeanInfo;
with Java.Lang.Object;

package Java.Beans.SimpleBeanInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(BeanInfo_I : Java.Beans.BeanInfo.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleBeanInfo (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBeanDescriptor (This : access Typ)
                               return access Java.Beans.BeanDescriptor.Typ'Class;

   function GetPropertyDescriptors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function GetDefaultPropertyIndex (This : access Typ)
                                     return Java.Int;

   function GetEventSetDescriptors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function GetDefaultEventIndex (This : access Typ)
                                  return Java.Int;

   function GetMethodDescriptors (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref;

   function GetAdditionalBeanInfo (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   function GetIcon (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Awt.Image.Typ'Class;

   function LoadImage (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Awt.Image.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleBeanInfo);
   pragma Import (Java, GetBeanDescriptor, "getBeanDescriptor");
   pragma Import (Java, GetPropertyDescriptors, "getPropertyDescriptors");
   pragma Import (Java, GetDefaultPropertyIndex, "getDefaultPropertyIndex");
   pragma Import (Java, GetEventSetDescriptors, "getEventSetDescriptors");
   pragma Import (Java, GetDefaultEventIndex, "getDefaultEventIndex");
   pragma Import (Java, GetMethodDescriptors, "getMethodDescriptors");
   pragma Import (Java, GetAdditionalBeanInfo, "getAdditionalBeanInfo");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, LoadImage, "loadImage");

end Java.Beans.SimpleBeanInfo;
pragma Import (Java, Java.Beans.SimpleBeanInfo, "java.beans.SimpleBeanInfo");
pragma Extensions_Allowed (Off);
