pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Beans.Statement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Statement (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTarget (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function GetMethodName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetArguments (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   procedure Execute (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Statement);
   pragma Import (Java, GetTarget, "getTarget");
   pragma Import (Java, GetMethodName, "getMethodName");
   pragma Import (Java, GetArguments, "getArguments");
   pragma Import (Java, Execute, "execute");
   pragma Import (Java, ToString, "toString");

end Java.Beans.Statement;
pragma Import (Java, Java.Beans.Statement, "java.beans.Statement");
pragma Extensions_Allowed (Off);
