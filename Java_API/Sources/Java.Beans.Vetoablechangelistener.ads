pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Beans.VetoableChangeListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure VetoableChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class) is abstract;
   --  can raise Java.Beans.PropertyVetoException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, VetoableChange, "vetoableChange");

end Java.Beans.VetoableChangeListener;
pragma Import (Java, Java.Beans.VetoableChangeListener, "java.beans.VetoableChangeListener");
pragma Extensions_Allowed (Off);
