pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.String;
with Java.Beans.VetoableChangeListener;
with Java.Lang.Object;
with Java.Util.EventListener;
with Java.Util.EventListenerProxy;

package Java.Beans.VetoableChangeListenerProxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(VetoableChangeListener_I : Java.Beans.VetoableChangeListener.Ref;
            EventListener_I : Java.Util.EventListener.Ref)
    is new Java.Util.EventListenerProxy.Typ(EventListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_VetoableChangeListenerProxy (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                             P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure VetoableChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   function GetPropertyName (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_VetoableChangeListenerProxy);
   pragma Import (Java, VetoableChange, "vetoableChange");
   pragma Import (Java, GetPropertyName, "getPropertyName");

end Java.Beans.VetoableChangeListenerProxy;
pragma Import (Java, Java.Beans.VetoableChangeListenerProxy, "java.beans.VetoableChangeListenerProxy");
pragma Extensions_Allowed (Off);
