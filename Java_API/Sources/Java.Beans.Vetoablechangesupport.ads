pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Beans.VetoableChangeListener;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Beans.VetoableChangeSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_VetoableChangeSupport (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddVetoableChangeListener (This : access Typ;
                                        P1_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   procedure RemoveVetoableChangeListener (This : access Typ;
                                           P1_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   function GetVetoableChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddVetoableChangeListener (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   procedure RemoveVetoableChangeListener (This : access Typ;
                                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   function GetVetoableChangeListeners (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;

   procedure FireVetoableChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   procedure FireVetoableChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int);
   --  can raise Java.Beans.PropertyVetoException.Except

   procedure FireVetoableChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Boolean : Java.Boolean;
                                 P3_Boolean : Java.Boolean);
   --  can raise Java.Beans.PropertyVetoException.Except

   procedure FireVetoableChange (This : access Typ;
                                 P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   --  synchronized
   function HasListeners (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_VetoableChangeSupport);
   pragma Import (Java, AddVetoableChangeListener, "addVetoableChangeListener");
   pragma Import (Java, RemoveVetoableChangeListener, "removeVetoableChangeListener");
   pragma Import (Java, GetVetoableChangeListeners, "getVetoableChangeListeners");
   pragma Import (Java, FireVetoableChange, "fireVetoableChange");
   pragma Import (Java, HasListeners, "hasListeners");

end Java.Beans.VetoableChangeSupport;
pragma Import (Java, Java.Beans.VetoableChangeSupport, "java.beans.VetoableChangeSupport");
pragma Extensions_Allowed (Off);
