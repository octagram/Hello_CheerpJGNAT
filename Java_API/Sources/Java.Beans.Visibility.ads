pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Beans.Visibility is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function NeedsGui (This : access Typ)
                      return Java.Boolean is abstract;

   procedure DontUseGui (This : access Typ) is abstract;

   procedure OkToUseGui (This : access Typ) is abstract;

   function AvoidingGui (This : access Typ)
                         return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NeedsGui, "needsGui");
   pragma Export (Java, DontUseGui, "dontUseGui");
   pragma Export (Java, OkToUseGui, "okToUseGui");
   pragma Export (Java, AvoidingGui, "avoidingGui");

end Java.Beans.Visibility;
pragma Import (Java, Java.Beans.Visibility, "java.beans.Visibility");
pragma Extensions_Allowed (Off);
