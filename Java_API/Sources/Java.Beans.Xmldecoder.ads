pragma Extensions_Allowed (On);
limited with Java.Beans.ExceptionListener;
limited with Java.Io.InputStream;
limited with Java.Lang.ClassLoader;
with Java.Lang.Object;

package Java.Beans.XMLDecoder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XMLDecoder (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_XMLDecoder (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_XMLDecoder (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P3_ExceptionListener : access Standard.Java.Beans.ExceptionListener.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_XMLDecoder (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P3_ExceptionListener : access Standard.Java.Beans.ExceptionListener.Typ'Class;
                            P4_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Close (This : access Typ);

   procedure SetExceptionListener (This : access Typ;
                                   P1_ExceptionListener : access Standard.Java.Beans.ExceptionListener.Typ'Class);

   function GetExceptionListener (This : access Typ)
                                  return access Java.Beans.ExceptionListener.Typ'Class;

   function ReadObject (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   procedure SetOwner (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetOwner (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLDecoder);
   pragma Import (Java, Close, "close");
   pragma Import (Java, SetExceptionListener, "setExceptionListener");
   pragma Import (Java, GetExceptionListener, "getExceptionListener");
   pragma Import (Java, ReadObject, "readObject");
   pragma Import (Java, SetOwner, "setOwner");
   pragma Import (Java, GetOwner, "getOwner");

end Java.Beans.XMLDecoder;
pragma Import (Java, Java.Beans.XMLDecoder, "java.beans.XMLDecoder");
pragma Extensions_Allowed (Off);
