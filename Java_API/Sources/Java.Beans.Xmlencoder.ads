pragma Extensions_Allowed (On);
limited with Java.Beans.Expression;
limited with Java.Beans.Statement;
limited with Java.Io.OutputStream;
with Java.Beans.Encoder;
with Java.Lang.Object;

package Java.Beans.XMLEncoder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Beans.Encoder.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XMLEncoder (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetOwner (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetOwner (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure WriteObject (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure WriteStatement (This : access Typ;
                             P1_Statement : access Standard.Java.Beans.Statement.Typ'Class);

   procedure WriteExpression (This : access Typ;
                              P1_Expression : access Standard.Java.Beans.Expression.Typ'Class);

   procedure Flush (This : access Typ);

   procedure Close (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLEncoder);
   pragma Import (Java, SetOwner, "setOwner");
   pragma Import (Java, GetOwner, "getOwner");
   pragma Import (Java, WriteObject, "writeObject");
   pragma Import (Java, WriteStatement, "writeStatement");
   pragma Import (Java, WriteExpression, "writeExpression");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Java.Beans.XMLEncoder;
pragma Import (Java, Java.Beans.XMLEncoder, "java.beans.XMLEncoder");
pragma Extensions_Allowed (Off);
