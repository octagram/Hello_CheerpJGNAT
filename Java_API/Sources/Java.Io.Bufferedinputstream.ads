pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
with Java.Io.Closeable;
with Java.Io.FilterInputStream;
with Java.Lang.Object;

package Java.Io.BufferedInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.FilterInputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buf : Java.Byte_Arr;
      pragma Import (Java, Buf, "buf");

      --  protected
      Count : Java.Int;
      pragma Import (Java, Count, "count");

      --  protected
      Pos : Java.Int;
      pragma Import (Java, Pos, "pos");

      --  protected
      Markpos : Java.Int;
      pragma Import (Java, Markpos, "markpos");

      --  protected
      Marklimit : Java.Int;
      pragma Import (Java, Marklimit, "marklimit");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BufferedInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_BufferedInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                     P2_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);

   --  synchronized
   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function MarkSupported (This : access Typ)
                           return Java.Boolean;

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BufferedInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, MarkSupported, "markSupported");
   pragma Import (Java, Close, "close");

end Java.Io.BufferedInputStream;
pragma Import (Java, Java.Io.BufferedInputStream, "java.io.BufferedInputStream");
pragma Extensions_Allowed (Off);
