pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
with Java.Io.Closeable;
with Java.Io.FilterOutputStream;
with Java.Io.Flushable;
with Java.Lang.Object;

package Java.Io.BufferedOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.FilterOutputStream.Typ(Closeable_I,
                                          Flushable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buf : Java.Byte_Arr;
      pragma Import (Java, Buf, "buf");

      --  protected
      Count : Java.Int;
      pragma Import (Java, Count, "count");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BufferedOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_BufferedOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                      P2_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BufferedOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Flush, "flush");

end Java.Io.BufferedOutputStream;
pragma Import (Java, Java.Io.BufferedOutputStream, "java.io.BufferedOutputStream");
pragma Extensions_Allowed (Off);
