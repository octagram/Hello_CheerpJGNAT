pragma Extensions_Allowed (On);
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Java.Io.ByteArrayInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buf : Java.Byte_Arr;
      pragma Import (Java, Buf, "buf");

      --  protected
      Pos : Java.Int;
      pragma Import (Java, Pos, "pos");

      --  protected
      Mark : Java.Int;
      pragma Import (Java, Mark, "mark");

      --  protected
      Count : Java.Int;
      pragma Import (Java, Count, "count");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ByteArrayInputStream (P1_Byte_Arr : Java.Byte_Arr; 
                                      This : Ref := null)
                                      return Ref;

   function New_ByteArrayInputStream (P1_Byte_Arr : Java.Byte_Arr;
                                      P2_Int : Java.Int;
                                      P3_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Read (This : access Typ)
                  return Java.Int;

   --  synchronized
   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;

   --  synchronized
   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;

   --  synchronized
   function Available (This : access Typ)
                       return Java.Int;

   function MarkSupported (This : access Typ)
                           return Java.Boolean;

   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);

   --  synchronized
   procedure Reset (This : access Typ);

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ByteArrayInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Available, "available");
   pragma Import (Java, MarkSupported, "markSupported");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Close, "close");

end Java.Io.ByteArrayInputStream;
pragma Import (Java, Java.Io.ByteArrayInputStream, "java.io.ByteArrayInputStream");
pragma Extensions_Allowed (Off);
