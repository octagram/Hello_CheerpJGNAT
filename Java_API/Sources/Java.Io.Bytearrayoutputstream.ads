pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Io.ByteArrayOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.OutputStream.Typ(Closeable_I,
                                    Flushable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buf : Java.Byte_Arr;
      pragma Import (Java, Buf, "buf");

      --  protected
      Count : Java.Int;
      pragma Import (Java, Count, "count");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ByteArrayOutputStream (This : Ref := null)
                                       return Ref;

   function New_ByteArrayOutputStream (P1_Int : Java.Int; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Int : Java.Int);

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);

   --  synchronized
   procedure WriteTo (This : access Typ;
                      P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Reset (This : access Typ);

   --  synchronized
   function ToByteArray (This : access Typ)
                         return Java.Byte_Arr;

   --  synchronized
   function Size (This : access Typ)
                  return Java.Int;

   --  synchronized
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  synchronized
   function ToString (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ByteArrayOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, WriteTo, "writeTo");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, ToByteArray, "toByteArray");
   pragma Import (Java, Size, "size");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Close, "close");

end Java.Io.ByteArrayOutputStream;
pragma Import (Java, Java.Io.ByteArrayOutputStream, "java.io.ByteArrayOutputStream");
pragma Extensions_Allowed (Off);
