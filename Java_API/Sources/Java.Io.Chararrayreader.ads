pragma Extensions_Allowed (On);
with Java.Io.Closeable;
with Java.Io.Reader;
with Java.Lang.Object;
with Java.Lang.Readable;

package Java.Io.CharArrayReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Readable_I : Java.Lang.Readable.Ref)
    is new Java.Io.Reader.Typ(Closeable_I,
                              Readable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buf : Java.Char_Arr;
      pragma Import (Java, Buf, "buf");

      --  protected
      Pos : Java.Int;
      pragma Import (Java, Pos, "pos");

      --  protected
      MarkedPos : Java.Int;
      pragma Import (Java, MarkedPos, "markedPos");

      --  protected
      Count : Java.Int;
      pragma Import (Java, Count, "count");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CharArrayReader (P1_Char_Arr : Java.Char_Arr; 
                                 This : Ref := null)
                                 return Ref;

   function New_CharArrayReader (P1_Char_Arr : Java.Char_Arr;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Char_Arr : Java.Char_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Ready (This : access Typ)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function MarkSupported (This : access Typ)
                           return Java.Boolean;

   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CharArrayReader);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Ready, "ready");
   pragma Import (Java, MarkSupported, "markSupported");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Close, "close");

end Java.Io.CharArrayReader;
pragma Import (Java, Java.Io.CharArrayReader, "java.io.CharArrayReader");
pragma Extensions_Allowed (Off);
