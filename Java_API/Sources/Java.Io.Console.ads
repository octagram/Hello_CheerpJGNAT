pragma Extensions_Allowed (On);
limited with Java.Io.PrintWriter;
limited with Java.Io.Reader;
limited with Java.Lang.String;
with Java.Io.Flushable;
with Java.Lang.Object;

package Java.Io.Console is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Writer (This : access Typ)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Reader (This : access Typ)
                    return access Java.Io.Reader.Typ'Class;

   function Format (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Io.Console.Typ'Class;

   function Printf (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Io.Console.Typ'Class;

   function ReadLine (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                      return access Java.Lang.String.Typ'Class;

   function ReadLine (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ReadPassword (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                          return Java.Char_Arr;

   function ReadPassword (This : access Typ)
                          return Java.Char_Arr;

   procedure Flush (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Writer, "writer");
   pragma Import (Java, Reader, "reader");
   pragma Import (Java, Format, "format");
   pragma Import (Java, Printf, "printf");
   pragma Import (Java, ReadLine, "readLine");
   pragma Import (Java, ReadPassword, "readPassword");
   pragma Import (Java, Flush, "flush");

end Java.Io.Console;
pragma Import (Java, Java.Io.Console, "java.io.Console");
pragma Extensions_Allowed (Off);
