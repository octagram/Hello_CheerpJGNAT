pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Io.DataInput is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   function SkipBytes (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadBoolean (This : access Typ)
                         return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadByte (This : access Typ)
                      return Java.Byte is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedByte (This : access Typ)
                              return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadShort (This : access Typ)
                       return Java.Short is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedShort (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadChar (This : access Typ)
                      return Java.Char is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadInt (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadLong (This : access Typ)
                      return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadFloat (This : access Typ)
                       return Java.Float is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadDouble (This : access Typ)
                        return Java.Double is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadLine (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadUTF (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ReadFully, "readFully");
   pragma Export (Java, SkipBytes, "skipBytes");
   pragma Export (Java, ReadBoolean, "readBoolean");
   pragma Export (Java, ReadByte, "readByte");
   pragma Export (Java, ReadUnsignedByte, "readUnsignedByte");
   pragma Export (Java, ReadShort, "readShort");
   pragma Export (Java, ReadUnsignedShort, "readUnsignedShort");
   pragma Export (Java, ReadChar, "readChar");
   pragma Export (Java, ReadInt, "readInt");
   pragma Export (Java, ReadLong, "readLong");
   pragma Export (Java, ReadFloat, "readFloat");
   pragma Export (Java, ReadDouble, "readDouble");
   pragma Export (Java, ReadLine, "readLine");
   pragma Export (Java, ReadUTF, "readUTF");

end Java.Io.DataInput;
pragma Import (Java, Java.Io.DataInput, "java.io.DataInput");
pragma Extensions_Allowed (Off);
