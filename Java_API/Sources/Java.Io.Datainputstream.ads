pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.DataInput;
with Java.Io.FilterInputStream;
with Java.Lang.Object;

package Java.Io.DataInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            DataInput_I : Java.Io.DataInput.Ref)
    is new Java.Io.FilterInputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DataInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   function SkipBytes (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadBoolean (This : access Typ)
                         return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadByte (This : access Typ)
                      return Java.Byte;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadUnsignedByte (This : access Typ)
                              return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadShort (This : access Typ)
                       return Java.Short;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadUnsignedShort (This : access Typ)
                               return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadChar (This : access Typ)
                      return Java.Char;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadInt (This : access Typ)
                     return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadLong (This : access Typ)
                      return Java.Long;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadFloat (This : access Typ)
                       return Java.Float;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadDouble (This : access Typ)
                        return Java.Double;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadUTF (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadUTF (P1_DataInput : access Standard.Java.Io.DataInput.Typ'Class)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DataInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, ReadFully, "readFully");
   pragma Import (Java, SkipBytes, "skipBytes");
   pragma Import (Java, ReadBoolean, "readBoolean");
   pragma Import (Java, ReadByte, "readByte");
   pragma Import (Java, ReadUnsignedByte, "readUnsignedByte");
   pragma Import (Java, ReadShort, "readShort");
   pragma Import (Java, ReadUnsignedShort, "readUnsignedShort");
   pragma Import (Java, ReadChar, "readChar");
   pragma Import (Java, ReadInt, "readInt");
   pragma Import (Java, ReadLong, "readLong");
   pragma Import (Java, ReadFloat, "readFloat");
   pragma Import (Java, ReadDouble, "readDouble");
   pragma Import (Java, ReadUTF, "readUTF");

end Java.Io.DataInputStream;
pragma Import (Java, Java.Io.DataInputStream, "java.io.DataInputStream");
pragma Extensions_Allowed (Off);
