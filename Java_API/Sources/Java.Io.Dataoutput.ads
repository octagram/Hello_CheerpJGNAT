pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Io.DataOutput is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteBoolean (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteByte (This : access Typ;
                        P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteShort (This : access Typ;
                         P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteChar (This : access Typ;
                        P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteInt (This : access Typ;
                       P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteLong (This : access Typ;
                        P1_Long : Java.Long) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteFloat (This : access Typ;
                         P1_Float : Java.Float) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteDouble (This : access Typ;
                          P1_Double : Java.Double) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteBytes (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteChars (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteUTF (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Write, "write");
   pragma Export (Java, WriteBoolean, "writeBoolean");
   pragma Export (Java, WriteByte, "writeByte");
   pragma Export (Java, WriteShort, "writeShort");
   pragma Export (Java, WriteChar, "writeChar");
   pragma Export (Java, WriteInt, "writeInt");
   pragma Export (Java, WriteLong, "writeLong");
   pragma Export (Java, WriteFloat, "writeFloat");
   pragma Export (Java, WriteDouble, "writeDouble");
   pragma Export (Java, WriteBytes, "writeBytes");
   pragma Export (Java, WriteChars, "writeChars");
   pragma Export (Java, WriteUTF, "writeUTF");

end Java.Io.DataOutput;
pragma Import (Java, Java.Io.DataOutput, "java.io.DataOutput");
pragma Extensions_Allowed (Off);
