pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.DataOutput;
with Java.Io.FilterOutputStream;
with Java.Io.Flushable;
with Java.Lang.Object;

package Java.Io.DataOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            DataOutput_I : Java.Io.DataOutput.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.FilterOutputStream.Typ(Closeable_I,
                                          Flushable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Written : Java.Int;
      pragma Import (Java, Written, "written");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DataOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteBoolean (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteByte (This : access Typ;
                        P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteShort (This : access Typ;
                         P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteChar (This : access Typ;
                        P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteInt (This : access Typ;
                       P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteLong (This : access Typ;
                        P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteFloat (This : access Typ;
                         P1_Float : Java.Float);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteDouble (This : access Typ;
                          P1_Double : Java.Double);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteBytes (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteChars (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteUTF (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final
   function Size (This : access Typ)
                  return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DataOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, WriteBoolean, "writeBoolean");
   pragma Import (Java, WriteByte, "writeByte");
   pragma Import (Java, WriteShort, "writeShort");
   pragma Import (Java, WriteChar, "writeChar");
   pragma Import (Java, WriteInt, "writeInt");
   pragma Import (Java, WriteLong, "writeLong");
   pragma Import (Java, WriteFloat, "writeFloat");
   pragma Import (Java, WriteDouble, "writeDouble");
   pragma Import (Java, WriteBytes, "writeBytes");
   pragma Import (Java, WriteChars, "writeChars");
   pragma Import (Java, WriteUTF, "writeUTF");
   pragma Import (Java, Size, "size");

end Java.Io.DataOutputStream;
pragma Import (Java, Java.Io.DataOutputStream, "java.io.DataOutputStream");
pragma Extensions_Allowed (Off);
