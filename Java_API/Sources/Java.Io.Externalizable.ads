pragma Extensions_Allowed (On);
limited with Java.Io.ObjectInput;
limited with Java.Io.ObjectOutput;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Io.Externalizable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteExternal (This : access Typ;
                            P1_ObjectOutput : access Standard.Java.Io.ObjectOutput.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadExternal (This : access Typ;
                           P1_ObjectInput : access Standard.Java.Io.ObjectInput.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WriteExternal, "writeExternal");
   pragma Export (Java, ReadExternal, "readExternal");

end Java.Io.Externalizable;
pragma Import (Java, Java.Io.Externalizable, "java.io.Externalizable");
pragma Extensions_Allowed (Off);
