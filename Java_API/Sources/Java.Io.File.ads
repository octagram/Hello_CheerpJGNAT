pragma Extensions_Allowed (On);
limited with Java.Io.FileFilter;
limited with Java.Io.FilenameFilter;
limited with Java.Lang.String;
limited with Java.Net.URI;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Io.File is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_File (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   function New_File (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   function New_File (P1_File : access Standard.Java.Io.File.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   function New_File (P1_URI : access Standard.Java.Net.URI.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetParent (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetParentFile (This : access Typ)
                           return access Java.Io.File.Typ'Class;

   function GetPath (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function IsAbsolute (This : access Typ)
                        return Java.Boolean;

   function GetAbsolutePath (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetAbsoluteFile (This : access Typ)
                             return access Java.Io.File.Typ'Class;

   function GetCanonicalPath (This : access Typ)
                              return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetCanonicalFile (This : access Typ)
                              return access Java.Io.File.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function ToURI (This : access Typ)
                   return access Java.Net.URI.Typ'Class;

   function CanRead (This : access Typ)
                     return Java.Boolean;

   function CanWrite (This : access Typ)
                      return Java.Boolean;

   function Exists (This : access Typ)
                    return Java.Boolean;

   function IsDirectory (This : access Typ)
                         return Java.Boolean;

   function IsFile (This : access Typ)
                    return Java.Boolean;

   function IsHidden (This : access Typ)
                      return Java.Boolean;

   function LastModified (This : access Typ)
                          return Java.Long;

   function Length (This : access Typ)
                    return Java.Long;

   function CreateNewFile (This : access Typ)
                           return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function Delete (This : access Typ)
                    return Java.Boolean;

   procedure DeleteOnExit (This : access Typ);

   function List (This : access Typ)
                  return Standard.Java.Lang.Object.Ref;

   function List (This : access Typ;
                  P1_FilenameFilter : access Standard.Java.Io.FilenameFilter.Typ'Class)
                  return Standard.Java.Lang.Object.Ref;

   function ListFiles (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   function ListFiles (This : access Typ;
                       P1_FilenameFilter : access Standard.Java.Io.FilenameFilter.Typ'Class)
                       return Standard.Java.Lang.Object.Ref;

   function ListFiles (This : access Typ;
                       P1_FileFilter : access Standard.Java.Io.FileFilter.Typ'Class)
                       return Standard.Java.Lang.Object.Ref;

   function Mkdir (This : access Typ)
                   return Java.Boolean;

   function Mkdirs (This : access Typ)
                    return Java.Boolean;

   function RenameTo (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class)
                      return Java.Boolean;

   function SetLastModified (This : access Typ;
                             P1_Long : Java.Long)
                             return Java.Boolean;

   function SetReadOnly (This : access Typ)
                         return Java.Boolean;

   function SetWritable (This : access Typ;
                         P1_Boolean : Java.Boolean;
                         P2_Boolean : Java.Boolean)
                         return Java.Boolean;

   function SetWritable (This : access Typ;
                         P1_Boolean : Java.Boolean)
                         return Java.Boolean;

   function SetReadable (This : access Typ;
                         P1_Boolean : Java.Boolean;
                         P2_Boolean : Java.Boolean)
                         return Java.Boolean;

   function SetReadable (This : access Typ;
                         P1_Boolean : Java.Boolean)
                         return Java.Boolean;

   function SetExecutable (This : access Typ;
                           P1_Boolean : Java.Boolean;
                           P2_Boolean : Java.Boolean)
                           return Java.Boolean;

   function SetExecutable (This : access Typ;
                           P1_Boolean : Java.Boolean)
                           return Java.Boolean;

   function CanExecute (This : access Typ)
                        return Java.Boolean;

   function ListRoots return Standard.Java.Lang.Object.Ref;

   function GetTotalSpace (This : access Typ)
                           return Java.Long;

   function GetFreeSpace (This : access Typ)
                          return Java.Long;

   function GetUsableSpace (This : access Typ)
                            return Java.Long;

   function CreateTempFile (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_File : access Standard.Java.Io.File.Typ'Class)
                            return access Java.Io.File.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function CreateTempFile (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Io.File.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function CompareTo (This : access Typ;
                       P1_File : access Standard.Java.Io.File.Typ'Class)
                       return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SeparatorChar : Java.Char;

   --  final
   Separator : access Java.Lang.String.Typ'Class;

   --  final
   PathSeparatorChar : Java.Char;

   --  final
   PathSeparator : access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_File);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetParentFile, "getParentFile");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, IsAbsolute, "isAbsolute");
   pragma Import (Java, GetAbsolutePath, "getAbsolutePath");
   pragma Import (Java, GetAbsoluteFile, "getAbsoluteFile");
   pragma Import (Java, GetCanonicalPath, "getCanonicalPath");
   pragma Import (Java, GetCanonicalFile, "getCanonicalFile");
   pragma Import (Java, ToURI, "toURI");
   pragma Import (Java, CanRead, "canRead");
   pragma Import (Java, CanWrite, "canWrite");
   pragma Import (Java, Exists, "exists");
   pragma Import (Java, IsDirectory, "isDirectory");
   pragma Import (Java, IsFile, "isFile");
   pragma Import (Java, IsHidden, "isHidden");
   pragma Import (Java, LastModified, "lastModified");
   pragma Import (Java, Length, "length");
   pragma Import (Java, CreateNewFile, "createNewFile");
   pragma Import (Java, Delete, "delete");
   pragma Import (Java, DeleteOnExit, "deleteOnExit");
   pragma Import (Java, List, "list");
   pragma Import (Java, ListFiles, "listFiles");
   pragma Import (Java, Mkdir, "mkdir");
   pragma Import (Java, Mkdirs, "mkdirs");
   pragma Import (Java, RenameTo, "renameTo");
   pragma Import (Java, SetLastModified, "setLastModified");
   pragma Import (Java, SetReadOnly, "setReadOnly");
   pragma Import (Java, SetWritable, "setWritable");
   pragma Import (Java, SetReadable, "setReadable");
   pragma Import (Java, SetExecutable, "setExecutable");
   pragma Import (Java, CanExecute, "canExecute");
   pragma Import (Java, ListRoots, "listRoots");
   pragma Import (Java, GetTotalSpace, "getTotalSpace");
   pragma Import (Java, GetFreeSpace, "getFreeSpace");
   pragma Import (Java, GetUsableSpace, "getUsableSpace");
   pragma Import (Java, CreateTempFile, "createTempFile");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SeparatorChar, "separatorChar");
   pragma Import (Java, Separator, "separator");
   pragma Import (Java, PathSeparatorChar, "pathSeparatorChar");
   pragma Import (Java, PathSeparator, "pathSeparator");

end Java.Io.File;
pragma Import (Java, Java.Io.File, "java.io.File");
pragma Extensions_Allowed (Off);
