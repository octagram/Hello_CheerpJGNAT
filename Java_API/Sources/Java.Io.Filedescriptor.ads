pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Io.FileDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileDescriptor (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Valid (This : access Typ)
                   return Java.Boolean;

   procedure Sync (This : access Typ);
   --  can raise Java.Io.SyncFailedException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   in_K : access Java.Io.FileDescriptor.Typ'Class;

   --  final
   out_K : access Java.Io.FileDescriptor.Typ'Class;

   --  final
   Err : access Java.Io.FileDescriptor.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileDescriptor);
   pragma Import (Java, Valid, "valid");
   pragma Import (Java, Sync, "sync");
   pragma Import (Java, in_K, "in");
   pragma Import (Java, out_K, "out");
   pragma Import (Java, Err, "err");

end Java.Io.FileDescriptor;
pragma Import (Java, Java.Io.FileDescriptor, "java.io.FileDescriptor");
pragma Extensions_Allowed (Off);
