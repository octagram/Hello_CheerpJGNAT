pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.FileDescriptor;
limited with Java.Lang.String;
limited with Java.Nio.Channels.FileChannel;
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Java.Io.FileInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileInputStream (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileInputStream (P1_File : access Standard.Java.Io.File.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileInputStream (P1_FileDescriptor : access Standard.Java.Io.FileDescriptor.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  final
   function GetFD (This : access Typ)
                   return access Java.Io.FileDescriptor.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetChannel (This : access Typ)
                        return access Java.Nio.Channels.FileChannel.Typ'Class;

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GetFD, "getFD");
   pragma Import (Java, GetChannel, "getChannel");
   pragma Import (Java, Finalize, "finalize");

end Java.Io.FileInputStream;
pragma Import (Java, Java.Io.FileInputStream, "java.io.FileInputStream");
pragma Extensions_Allowed (Off);
