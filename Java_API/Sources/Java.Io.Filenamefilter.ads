pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Io.FilenameFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function accept_K (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, accept_K, "accept");

end Java.Io.FilenameFilter;
pragma Import (Java, Java.Io.FilenameFilter, "java.io.FilenameFilter");
pragma Extensions_Allowed (Off);
