pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.FileDescriptor;
limited with Java.Lang.String;
limited with Java.Nio.Channels.FileChannel;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Io.FileOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.OutputStream.Typ(Closeable_I,
                                    Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileOutputStream (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileOutputStream (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Boolean : Java.Boolean; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileOutputStream (P1_File : access Standard.Java.Io.File.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileOutputStream (P1_File : access Standard.Java.Io.File.Typ'Class;
                                  P2_Boolean : Java.Boolean; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileOutputStream (P1_FileDescriptor : access Standard.Java.Io.FileDescriptor.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  final
   function GetFD (This : access Typ)
                   return access Java.Io.FileDescriptor.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetChannel (This : access Typ)
                        return access Java.Nio.Channels.FileChannel.Typ'Class;

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GetFD, "getFD");
   pragma Import (Java, GetChannel, "getChannel");
   pragma Import (Java, Finalize, "finalize");

end Java.Io.FileOutputStream;
pragma Import (Java, Java.Io.FileOutputStream, "java.io.FileOutputStream");
pragma Extensions_Allowed (Off);
