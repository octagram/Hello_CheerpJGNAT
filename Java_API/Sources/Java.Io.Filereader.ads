pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.FileDescriptor;
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.InputStreamReader;
with Java.Lang.Object;
with Java.Lang.Readable;

package Java.Io.FileReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Readable_I : Java.Lang.Readable.Ref)
    is new Java.Io.InputStreamReader.Typ(Closeable_I,
                                         Readable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileReader (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileReader (P1_File : access Standard.Java.Io.File.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_FileReader (P1_FileDescriptor : access Standard.Java.Io.FileDescriptor.Typ'Class; 
                            This : Ref := null)
                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileReader);

end Java.Io.FileReader;
pragma Import (Java, Java.Io.FileReader, "java.io.FileReader");
pragma Extensions_Allowed (Off);
