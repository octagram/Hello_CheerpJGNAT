pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.FileDescriptor;
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.OutputStreamWriter;
with Java.Lang.Appendable;
with Java.Lang.Object;

package Java.Io.FileWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref)
    is new Java.Io.OutputStreamWriter.Typ(Closeable_I,
                                          Flushable_I,
                                          Appendable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileWriter (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Io.IOException.Except

   function New_FileWriter (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Io.IOException.Except

   function New_FileWriter (P1_File : access Standard.Java.Io.File.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Io.IOException.Except

   function New_FileWriter (P1_File : access Standard.Java.Io.File.Typ'Class;
                            P2_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;
   --  can raise Java.Io.IOException.Except

   function New_FileWriter (P1_FileDescriptor : access Standard.Java.Io.FileDescriptor.Typ'Class; 
                            This : Ref := null)
                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileWriter);

end Java.Io.FileWriter;
pragma Import (Java, Java.Io.FileWriter, "java.io.FileWriter");
pragma Extensions_Allowed (Off);
