pragma Extensions_Allowed (On);
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Java.Io.FilterInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      in_K : access Java.Io.InputStream.Typ'Class;
      pragma Import (Java, in_K, "in");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_FilterInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);

   --  synchronized
   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function MarkSupported (This : access Typ)
                           return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FilterInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, MarkSupported, "markSupported");

end Java.Io.FilterInputStream;
pragma Import (Java, Java.Io.FilterInputStream, "java.io.FilterInputStream");
pragma Extensions_Allowed (Off);
