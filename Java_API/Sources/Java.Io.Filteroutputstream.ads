pragma Extensions_Allowed (On);
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Io.FilterOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.OutputStream.Typ(Closeable_I,
                                    Flushable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      out_K : access Java.Io.OutputStream.Typ'Class;
      pragma Import (Java, out_K, "out");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FilterOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FilterOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Java.Io.FilterOutputStream;
pragma Import (Java, Java.Io.FilterOutputStream, "java.io.FilterOutputStream");
pragma Extensions_Allowed (Off);
