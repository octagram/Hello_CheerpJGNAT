pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.Writer;
with Java.Lang.Appendable;
with Java.Lang.Object;

package Java.Io.FilterWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref)
    is abstract new Java.Io.Writer.Typ(Closeable_I,
                                       Flushable_I,
                                       Appendable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      out_K : access Java.Io.Writer.Typ'Class;
      pragma Import (Java, out_K, "out");

   end record;

   --  protected
   function New_FilterWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FilterWriter);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Java.Io.FilterWriter;
pragma Import (Java, Java.Io.FilterWriter, "java.io.FilterWriter");
pragma Extensions_Allowed (Off);
