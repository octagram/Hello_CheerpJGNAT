pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Nio.Charset.Charset;
limited with Java.Nio.Charset.CharsetDecoder;
with Java.Io.Closeable;
with Java.Io.Reader;
with Java.Lang.Object;
with Java.Lang.Readable;

package Java.Io.InputStreamReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Readable_I : Java.Lang.Readable.Ref)
    is new Java.Io.Reader.Typ(Closeable_I,
                              Readable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InputStreamReader (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_InputStreamReader (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   function New_InputStreamReader (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                   P2_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_InputStreamReader (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                   P2_CharsetDecoder : access Standard.Java.Nio.Charset.CharsetDecoder.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Char_Arr : Java.Char_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Ready (This : access Typ)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputStreamReader);
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Ready, "ready");
   pragma Import (Java, Close, "close");

end Java.Io.InputStreamReader;
pragma Import (Java, Java.Io.InputStreamReader, "java.io.InputStreamReader");
pragma Extensions_Allowed (Off);
