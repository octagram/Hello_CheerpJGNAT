pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.ObjectStreamException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Io.InvalidClassException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Io.ObjectStreamException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Classname : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Classname, "classname");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InvalidClassException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_InvalidClassException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.io.InvalidClassException");
   pragma Java_Constructor (New_InvalidClassException);
   pragma Import (Java, GetMessage, "getMessage");

end Java.Io.InvalidClassException;
pragma Import (Java, Java.Io.InvalidClassException, "java.io.InvalidClassException");
pragma Extensions_Allowed (Off);
