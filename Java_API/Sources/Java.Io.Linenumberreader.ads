pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
with Java.Io.BufferedReader;
with Java.Io.Closeable;
with Java.Lang.Object;
with Java.Lang.Readable;

package Java.Io.LineNumberReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Readable_I : Java.Lang.Readable.Ref)
    is new Java.Io.BufferedReader.Typ(Closeable_I,
                                      Readable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LineNumberReader (P1_Reader : access Standard.Java.Io.Reader.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_LineNumberReader (P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                  P2_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetLineNumber (This : access Typ;
                            P1_Int : Java.Int);

   function GetLineNumber (This : access Typ)
                           return Java.Int;

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Char_Arr : Java.Char_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadLine (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LineNumberReader);
   pragma Import (Java, SetLineNumber, "setLineNumber");
   pragma Import (Java, GetLineNumber, "getLineNumber");
   pragma Import (Java, Read, "read");
   pragma Import (Java, ReadLine, "readLine");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");

end Java.Io.LineNumberReader;
pragma Import (Java, Java.Io.LineNumberReader, "java.io.LineNumberReader");
pragma Extensions_Allowed (Off);
