pragma Extensions_Allowed (On);
with Java.Io.DataInput;
with Java.Lang.Object;

package Java.Io.ObjectInput is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DataInput_I : Java.Io.DataInput.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ReadObject (This : access Typ)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.ClassNotFoundException.Except and
   --  Java.Io.IOException.Except

   function Read (This : access Typ)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Available (This : access Typ)
                       return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ReadObject, "readObject");
   pragma Export (Java, Read, "read");
   pragma Export (Java, Skip, "skip");
   pragma Export (Java, Available, "available");
   pragma Export (Java, Close, "close");

end Java.Io.ObjectInput;
pragma Import (Java, Java.Io.ObjectInput, "java.io.ObjectInput");
pragma Extensions_Allowed (Off);
