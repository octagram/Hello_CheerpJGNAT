pragma Extensions_Allowed (On);
limited with Java.Io.ObjectStreamClass;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Io.ObjectInputStream.GetField is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_GetField (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetObjectStreamClass (This : access Typ)
                                  return access Java.Io.ObjectStreamClass.Typ'Class is abstract;

   function Defaulted (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Boolean : Java.Boolean)
                 return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Byte : Java.Byte)
                 return Java.Byte is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Char : Java.Char)
                 return Java.Char is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Short : Java.Short)
                 return Java.Short is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Int : Java.Int)
                 return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Long : Java.Long)
                 return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Float : Java.Float)
                 return Java.Float is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Double : Java.Double)
                 return Java.Double is abstract;
   --  can raise Java.Io.IOException.Except

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GetField);
   pragma Export (Java, GetObjectStreamClass, "getObjectStreamClass");
   pragma Export (Java, Defaulted, "defaulted");
   pragma Export (Java, Get, "get");

end Java.Io.ObjectInputStream.GetField;
pragma Import (Java, Java.Io.ObjectInputStream.GetField, "java.io.ObjectInputStream$GetField");
pragma Extensions_Allowed (Off);
