pragma Extensions_Allowed (On);
limited with Java.Io.ObjectInputStream.GetField;
limited with Java.Io.ObjectInputValidation;
limited with Java.Io.ObjectStreamClass;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Io.ObjectInput;
with Java.Io.ObjectStreamConstants;
with Java.Lang.Object;

package Java.Io.ObjectInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            ObjectInput_I : Java.Io.ObjectInput.Ref;
            ObjectStreamConstants_I : Java.Io.ObjectStreamConstants.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjectInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Java.Io.IOException.Except

   --  protected
   function New_ObjectInputStream (This : Ref := null)
                                   return Ref;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function ReadObject (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   --  protected
   function ReadObjectOverride (This : access Typ)
                                return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function ReadUnshared (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   procedure DefaultReadObject (This : access Typ);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function ReadFields (This : access Typ)
                        return access Java.Io.ObjectInputStream.GetField.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   procedure RegisterValidation (This : access Typ;
                                 P1_ObjectInputValidation : access Standard.Java.Io.ObjectInputValidation.Typ'Class;
                                 P2_Int : Java.Int);
   --  can raise Java.Io.NotActiveException.Except and
   --  Java.Io.InvalidObjectException.Except

   --  protected
   function ResolveClass (This : access Typ;
                          P1_ObjectStreamClass : access Standard.Java.Io.ObjectStreamClass.Typ'Class)
                          return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   --  protected
   function ResolveProxyClass (This : access Typ;
                               P1_String_Arr : access Java.Lang.String.Arr_Obj)
                               return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   --  protected
   function ResolveObject (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   function EnableResolveObject (This : access Typ;
                                 P1_Boolean : Java.Boolean)
                                 return Java.Boolean;
   --  can raise Java.Lang.SecurityException.Except

   --  protected
   procedure ReadStreamHeader (This : access Typ);
   --  can raise Java.Io.IOException.Except and
   --  Java.Io.StreamCorruptedException.Except

   --  protected
   function ReadClassDescriptor (This : access Typ)
                                 return access Java.Io.ObjectStreamClass.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function ReadBoolean (This : access Typ)
                         return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function ReadByte (This : access Typ)
                      return Java.Byte;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedByte (This : access Typ)
                              return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadChar (This : access Typ)
                      return Java.Char;
   --  can raise Java.Io.IOException.Except

   function ReadShort (This : access Typ)
                       return Java.Short;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedShort (This : access Typ)
                               return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadInt (This : access Typ)
                     return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadLong (This : access Typ)
                      return Java.Long;
   --  can raise Java.Io.IOException.Except

   function ReadFloat (This : access Typ)
                       return Java.Float;
   --  can raise Java.Io.IOException.Except

   function ReadDouble (This : access Typ)
                        return Java.Double;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function SkipBytes (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadUTF (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjectInputStream);
   pragma Import (Java, ReadObject, "readObject");
   pragma Import (Java, ReadObjectOverride, "readObjectOverride");
   pragma Import (Java, ReadUnshared, "readUnshared");
   pragma Import (Java, DefaultReadObject, "defaultReadObject");
   pragma Import (Java, ReadFields, "readFields");
   pragma Import (Java, RegisterValidation, "registerValidation");
   pragma Import (Java, ResolveClass, "resolveClass");
   pragma Import (Java, ResolveProxyClass, "resolveProxyClass");
   pragma Import (Java, ResolveObject, "resolveObject");
   pragma Import (Java, EnableResolveObject, "enableResolveObject");
   pragma Import (Java, ReadStreamHeader, "readStreamHeader");
   pragma Import (Java, ReadClassDescriptor, "readClassDescriptor");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Close, "close");
   pragma Import (Java, ReadBoolean, "readBoolean");
   pragma Import (Java, ReadByte, "readByte");
   pragma Import (Java, ReadUnsignedByte, "readUnsignedByte");
   pragma Import (Java, ReadChar, "readChar");
   pragma Import (Java, ReadShort, "readShort");
   pragma Import (Java, ReadUnsignedShort, "readUnsignedShort");
   pragma Import (Java, ReadInt, "readInt");
   pragma Import (Java, ReadLong, "readLong");
   pragma Import (Java, ReadFloat, "readFloat");
   pragma Import (Java, ReadDouble, "readDouble");
   pragma Import (Java, ReadFully, "readFully");
   pragma Import (Java, SkipBytes, "skipBytes");
   pragma Import (Java, ReadUTF, "readUTF");

end Java.Io.ObjectInputStream;
pragma Import (Java, Java.Io.ObjectInputStream, "java.io.ObjectInputStream");
pragma Extensions_Allowed (Off);
