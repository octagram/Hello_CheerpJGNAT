pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Io.ObjectInputValidation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ValidateObject (This : access Typ) is abstract;
   --  can raise Java.Io.InvalidObjectException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ValidateObject, "validateObject");

end Java.Io.ObjectInputValidation;
pragma Import (Java, Java.Io.ObjectInputValidation, "java.io.ObjectInputValidation");
pragma Extensions_Allowed (Off);
