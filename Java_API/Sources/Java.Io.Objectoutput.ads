pragma Extensions_Allowed (On);
with Java.Io.DataOutput;
with Java.Lang.Object;

package Java.Io.ObjectOutput is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DataOutput_I : Java.Io.DataOutput.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteObject (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WriteObject, "writeObject");
   pragma Export (Java, Write, "write");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, Close, "close");

end Java.Io.ObjectOutput;
pragma Import (Java, Java.Io.ObjectOutput, "java.io.ObjectOutput");
pragma Extensions_Allowed (Off);
