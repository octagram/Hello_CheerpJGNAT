pragma Extensions_Allowed (On);
limited with Java.Io.ObjectOutputStream.PutField;
limited with Java.Io.ObjectStreamClass;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.ObjectOutput;
with Java.Io.ObjectStreamConstants;
with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Io.ObjectOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            ObjectOutput_I : Java.Io.ObjectOutput.Ref;
            ObjectStreamConstants_I : Java.Io.ObjectStreamConstants.Ref)
    is new Java.Io.OutputStream.Typ(Closeable_I,
                                    Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjectOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Io.IOException.Except

   --  protected
   function New_ObjectOutputStream (This : Ref := null)
                                    return Ref;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure UseProtocolVersion (This : access Typ;
                                 P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteObject (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteObjectOverride (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure WriteUnshared (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure DefaultWriteObject (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function PutFields (This : access Typ)
                       return access Java.Io.ObjectOutputStream.PutField.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure WriteFields (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure AnnotateClass (This : access Typ;
                            P1_Class : access Standard.Java.Lang.Class.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure AnnotateProxyClass (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   function ReplaceObject (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   function EnableReplaceObject (This : access Typ;
                                 P1_Boolean : Java.Boolean)
                                 return Java.Boolean;
   --  can raise Java.Lang.SecurityException.Except

   --  protected
   procedure WriteStreamHeader (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteClassDescriptor (This : access Typ;
                                   P1_ObjectStreamClass : access Standard.Java.Io.ObjectStreamClass.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Drain (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure WriteBoolean (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Io.IOException.Except

   procedure WriteByte (This : access Typ;
                        P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure WriteShort (This : access Typ;
                         P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure WriteChar (This : access Typ;
                        P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure WriteInt (This : access Typ;
                       P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure WriteLong (This : access Typ;
                        P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   procedure WriteFloat (This : access Typ;
                         P1_Float : Java.Float);
   --  can raise Java.Io.IOException.Except

   procedure WriteDouble (This : access Typ;
                          P1_Double : Java.Double);
   --  can raise Java.Io.IOException.Except

   procedure WriteBytes (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure WriteChars (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure WriteUTF (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjectOutputStream);
   pragma Import (Java, UseProtocolVersion, "useProtocolVersion");
   pragma Import (Java, WriteObject, "writeObject");
   pragma Import (Java, WriteObjectOverride, "writeObjectOverride");
   pragma Import (Java, WriteUnshared, "writeUnshared");
   pragma Import (Java, DefaultWriteObject, "defaultWriteObject");
   pragma Import (Java, PutFields, "putFields");
   pragma Import (Java, WriteFields, "writeFields");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, AnnotateClass, "annotateClass");
   pragma Import (Java, AnnotateProxyClass, "annotateProxyClass");
   pragma Import (Java, ReplaceObject, "replaceObject");
   pragma Import (Java, EnableReplaceObject, "enableReplaceObject");
   pragma Import (Java, WriteStreamHeader, "writeStreamHeader");
   pragma Import (Java, WriteClassDescriptor, "writeClassDescriptor");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Drain, "drain");
   pragma Import (Java, Close, "close");
   pragma Import (Java, WriteBoolean, "writeBoolean");
   pragma Import (Java, WriteByte, "writeByte");
   pragma Import (Java, WriteShort, "writeShort");
   pragma Import (Java, WriteChar, "writeChar");
   pragma Import (Java, WriteInt, "writeInt");
   pragma Import (Java, WriteLong, "writeLong");
   pragma Import (Java, WriteFloat, "writeFloat");
   pragma Import (Java, WriteDouble, "writeDouble");
   pragma Import (Java, WriteBytes, "writeBytes");
   pragma Import (Java, WriteChars, "writeChars");
   pragma Import (Java, WriteUTF, "writeUTF");

end Java.Io.ObjectOutputStream;
pragma Import (Java, Java.Io.ObjectOutputStream, "java.io.ObjectOutputStream");
pragma Extensions_Allowed (Off);
