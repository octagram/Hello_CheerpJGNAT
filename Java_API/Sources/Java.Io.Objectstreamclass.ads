pragma Extensions_Allowed (On);
limited with Java.Io.ObjectStreamField;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Io.ObjectStreamClass is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Lookup (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return access Java.Io.ObjectStreamClass.Typ'Class;

   function LookupAny (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Io.ObjectStreamClass.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetSerialVersionUID (This : access Typ)
                                 return Java.Long;

   function ForClass (This : access Typ)
                      return access Java.Lang.Class.Typ'Class;

   function GetFields (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   function GetField (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Io.ObjectStreamField.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NO_FIELDS : Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Lookup, "lookup");
   pragma Import (Java, LookupAny, "lookupAny");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetSerialVersionUID, "getSerialVersionUID");
   pragma Import (Java, ForClass, "forClass");
   pragma Import (Java, GetFields, "getFields");
   pragma Import (Java, GetField, "getField");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, NO_FIELDS, "NO_FIELDS");

end Java.Io.ObjectStreamClass;
pragma Import (Java, Java.Io.ObjectStreamClass, "java.io.ObjectStreamClass");
pragma Extensions_Allowed (Off);
