pragma Extensions_Allowed (On);
limited with Java.Io.SerializablePermission;
with Java.Lang.Object;

package Java.Io.ObjectStreamConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   STREAM_MAGIC : constant Java.Short;

   --  final
   STREAM_VERSION : constant Java.Short;

   --  final
   TC_BASE : constant Java.Byte;

   --  final
   TC_NULL : constant Java.Byte;

   --  final
   TC_REFERENCE : constant Java.Byte;

   --  final
   TC_CLASSDESC : constant Java.Byte;

   --  final
   TC_OBJECT : constant Java.Byte;

   --  final
   TC_STRING : constant Java.Byte;

   --  final
   TC_ARRAY : constant Java.Byte;

   --  final
   TC_CLASS : constant Java.Byte;

   --  final
   TC_BLOCKDATA : constant Java.Byte;

   --  final
   TC_ENDBLOCKDATA : constant Java.Byte;

   --  final
   TC_RESET : constant Java.Byte;

   --  final
   TC_BLOCKDATALONG : constant Java.Byte;

   --  final
   TC_EXCEPTION : constant Java.Byte;

   --  final
   TC_LONGSTRING : constant Java.Byte;

   --  final
   TC_PROXYCLASSDESC : constant Java.Byte;

   --  final
   TC_ENUM : constant Java.Byte;

   --  final
   TC_MAX : constant Java.Byte;

   --  final
   BaseWireHandle : constant Java.Int;

   --  final
   SC_WRITE_METHOD : constant Java.Byte;

   --  final
   SC_BLOCK_DATA : constant Java.Byte;

   --  final
   SC_SERIALIZABLE : constant Java.Byte;

   --  final
   SC_EXTERNALIZABLE : constant Java.Byte;

   --  final
   SC_ENUM : constant Java.Byte;

   --  final
   SUBSTITUTION_PERMISSION : access Java.Io.SerializablePermission.Typ'Class;

   --  final
   SUBCLASS_IMPLEMENTATION_PERMISSION : access Java.Io.SerializablePermission.Typ'Class;

   --  final
   PROTOCOL_VERSION_1 : constant Java.Int;

   --  final
   PROTOCOL_VERSION_2 : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, STREAM_MAGIC, "STREAM_MAGIC");
   pragma Import (Java, STREAM_VERSION, "STREAM_VERSION");
   pragma Import (Java, TC_BASE, "TC_BASE");
   pragma Import (Java, TC_NULL, "TC_NULL");
   pragma Import (Java, TC_REFERENCE, "TC_REFERENCE");
   pragma Import (Java, TC_CLASSDESC, "TC_CLASSDESC");
   pragma Import (Java, TC_OBJECT, "TC_OBJECT");
   pragma Import (Java, TC_STRING, "TC_STRING");
   pragma Import (Java, TC_ARRAY, "TC_ARRAY");
   pragma Import (Java, TC_CLASS, "TC_CLASS");
   pragma Import (Java, TC_BLOCKDATA, "TC_BLOCKDATA");
   pragma Import (Java, TC_ENDBLOCKDATA, "TC_ENDBLOCKDATA");
   pragma Import (Java, TC_RESET, "TC_RESET");
   pragma Import (Java, TC_BLOCKDATALONG, "TC_BLOCKDATALONG");
   pragma Import (Java, TC_EXCEPTION, "TC_EXCEPTION");
   pragma Import (Java, TC_LONGSTRING, "TC_LONGSTRING");
   pragma Import (Java, TC_PROXYCLASSDESC, "TC_PROXYCLASSDESC");
   pragma Import (Java, TC_ENUM, "TC_ENUM");
   pragma Import (Java, TC_MAX, "TC_MAX");
   pragma Import (Java, BaseWireHandle, "baseWireHandle");
   pragma Import (Java, SC_WRITE_METHOD, "SC_WRITE_METHOD");
   pragma Import (Java, SC_BLOCK_DATA, "SC_BLOCK_DATA");
   pragma Import (Java, SC_SERIALIZABLE, "SC_SERIALIZABLE");
   pragma Import (Java, SC_EXTERNALIZABLE, "SC_EXTERNALIZABLE");
   pragma Import (Java, SC_ENUM, "SC_ENUM");
   pragma Import (Java, SUBSTITUTION_PERMISSION, "SUBSTITUTION_PERMISSION");
   pragma Import (Java, SUBCLASS_IMPLEMENTATION_PERMISSION, "SUBCLASS_IMPLEMENTATION_PERMISSION");
   pragma Import (Java, PROTOCOL_VERSION_1, "PROTOCOL_VERSION_1");
   pragma Import (Java, PROTOCOL_VERSION_2, "PROTOCOL_VERSION_2");

end Java.Io.ObjectStreamConstants;
pragma Import (Java, Java.Io.ObjectStreamConstants, "java.io.ObjectStreamConstants");
pragma Extensions_Allowed (Off);
