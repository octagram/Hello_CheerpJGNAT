pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.IOException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Io.ObjectStreamException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Io.IOException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   --  protected
   function New_ObjectStreamException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   --  protected
   function New_ObjectStreamException (This : Ref := null)
                                       return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.io.ObjectStreamException");
   pragma Java_Constructor (New_ObjectStreamException);

end Java.Io.ObjectStreamException;
pragma Import (Java, Java.Io.ObjectStreamException, "java.io.ObjectStreamException");
pragma Extensions_Allowed (Off);
