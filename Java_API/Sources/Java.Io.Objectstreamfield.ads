pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Io.ObjectStreamField is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjectStreamField (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_ObjectStreamField (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                   P3_Boolean : Java.Boolean; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.Class.Typ'Class;

   function GetTypeCode (This : access Typ)
                         return Java.Char;

   function GetTypeString (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetOffset (This : access Typ)
                       return Java.Int;

   --  protected
   procedure SetOffset (This : access Typ;
                        P1_Int : Java.Int);

   function IsPrimitive (This : access Typ)
                         return Java.Boolean;

   function IsUnshared (This : access Typ)
                        return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjectStreamField);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetTypeCode, "getTypeCode");
   pragma Import (Java, GetTypeString, "getTypeString");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, SetOffset, "setOffset");
   pragma Import (Java, IsPrimitive, "isPrimitive");
   pragma Import (Java, IsUnshared, "isUnshared");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, ToString, "toString");

end Java.Io.ObjectStreamField;
pragma Import (Java, Java.Io.ObjectStreamField, "java.io.ObjectStreamField");
pragma Extensions_Allowed (Off);
