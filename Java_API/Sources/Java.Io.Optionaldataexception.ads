pragma Extensions_Allowed (On);
with Java.Io.ObjectStreamException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Io.OptionalDataException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Io.ObjectStreamException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Length : Java.Int;
      pragma Import (Java, Length, "length");

      Eof : Java.Boolean;
      pragma Import (Java, Eof, "eof");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.io.OptionalDataException");

end Java.Io.OptionalDataException;
pragma Import (Java, Java.Io.OptionalDataException, "java.io.OptionalDataException");
pragma Extensions_Allowed (Off);
