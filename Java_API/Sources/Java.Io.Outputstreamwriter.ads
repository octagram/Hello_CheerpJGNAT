pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Nio.Charset.Charset;
limited with Java.Nio.Charset.CharsetEncoder;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.Writer;
with Java.Lang.Appendable;
with Java.Lang.Object;

package Java.Io.OutputStreamWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref)
    is new Java.Io.Writer.Typ(Closeable_I,
                              Flushable_I,
                              Appendable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_OutputStreamWriter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   function New_OutputStreamWriter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_OutputStreamWriter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                    P2_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_OutputStreamWriter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                    P2_CharsetEncoder : access Standard.Java.Nio.Charset.CharsetEncoder.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OutputStreamWriter);
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Java.Io.OutputStreamWriter;
pragma Import (Java, Java.Io.OutputStreamWriter, "java.io.OutputStreamWriter");
pragma Extensions_Allowed (Off);
