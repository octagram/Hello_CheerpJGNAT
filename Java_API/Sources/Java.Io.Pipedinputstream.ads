pragma Extensions_Allowed (On);
limited with Java.Io.PipedOutputStream;
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Java.Io.PipedInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buffer : Java.Byte_Arr;
      pragma Import (Java, Buffer, "buffer");

      --  protected
      in_K : Java.Int;
      pragma Import (Java, in_K, "in");

      --  protected
      out_K : Java.Int;
      pragma Import (Java, out_K, "out");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PipedInputStream (P1_PipedOutputStream : access Standard.Java.Io.PipedOutputStream.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.IOException.Except

   function New_PipedInputStream (P1_PipedOutputStream : access Standard.Java.Io.PipedOutputStream.Typ'Class;
                                  P2_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.IOException.Except

   function New_PipedInputStream (This : Ref := null)
                                  return Ref;

   function New_PipedInputStream (P1_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Connect (This : access Typ;
                      P1_PipedOutputStream : access Standard.Java.Io.PipedOutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected  synchronized
   procedure Receive (This : access Typ;
                      P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   PIPE_SIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PipedInputStream);
   pragma Import (Java, Connect, "connect");
   pragma Import (Java, Receive, "receive");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Close, "close");
   pragma Import (Java, PIPE_SIZE, "PIPE_SIZE");

end Java.Io.PipedInputStream;
pragma Import (Java, Java.Io.PipedInputStream, "java.io.PipedInputStream");
pragma Extensions_Allowed (Off);
