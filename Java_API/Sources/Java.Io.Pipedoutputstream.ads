pragma Extensions_Allowed (On);
limited with Java.Io.PipedInputStream;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Io.PipedOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.OutputStream.Typ(Closeable_I,
                                    Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PipedOutputStream (P1_PipedInputStream : access Standard.Java.Io.PipedInputStream.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Java.Io.IOException.Except

   function New_PipedOutputStream (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Connect (This : access Typ;
                      P1_PipedInputStream : access Standard.Java.Io.PipedInputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PipedOutputStream);
   pragma Import (Java, Connect, "connect");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Java.Io.PipedOutputStream;
pragma Import (Java, Java.Io.PipedOutputStream, "java.io.PipedOutputStream");
pragma Extensions_Allowed (Off);
