pragma Extensions_Allowed (On);
limited with Java.Io.PipedWriter;
with Java.Io.Closeable;
with Java.Io.Reader;
with Java.Lang.Object;
with Java.Lang.Readable;

package Java.Io.PipedReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Readable_I : Java.Lang.Readable.Ref)
    is new Java.Io.Reader.Typ(Closeable_I,
                              Readable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PipedReader (P1_PipedWriter : access Standard.Java.Io.PipedWriter.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except

   function New_PipedReader (P1_PipedWriter : access Standard.Java.Io.PipedWriter.Typ'Class;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except

   function New_PipedReader (This : Ref := null)
                             return Ref;

   function New_PipedReader (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Connect (This : access Typ;
                      P1_PipedWriter : access Standard.Java.Io.PipedWriter.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Read (This : access Typ;
                  P1_Char_Arr : Java.Char_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function Ready (This : access Typ)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PipedReader);
   pragma Import (Java, Connect, "connect");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Ready, "ready");
   pragma Import (Java, Close, "close");

end Java.Io.PipedReader;
pragma Import (Java, Java.Io.PipedReader, "java.io.PipedReader");
pragma Extensions_Allowed (Off);
