pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.OutputStream;
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.Writer;
with Java.Lang.Appendable;
with Java.Lang.Object;

package Java.Io.PrintWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref)
    is new Java.Io.Writer.Typ(Closeable_I,
                              Flushable_I,
                              Appendable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      out_K : access Java.Io.Writer.Typ'Class;
      pragma Import (Java, out_K, "out");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrintWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_PrintWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                             P2_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   function New_PrintWriter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_PrintWriter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                             P2_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   function New_PrintWriter (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_PrintWriter (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.FileNotFoundException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   function New_PrintWriter (P1_File : access Standard.Java.Io.File.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_PrintWriter (P1_File : access Standard.Java.Io.File.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.FileNotFoundException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Flush (This : access Typ);

   procedure Close (This : access Typ);

   function CheckError (This : access Typ)
                        return Java.Boolean;

   --  protected
   procedure SetError (This : access Typ);

   --  protected
   procedure ClearError (This : access Typ);

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);

   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);

   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr);

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Print (This : access Typ;
                    P1_Boolean : Java.Boolean);

   procedure Print (This : access Typ;
                    P1_Char : Java.Char);

   procedure Print (This : access Typ;
                    P1_Int : Java.Int);

   procedure Print (This : access Typ;
                    P1_Long : Java.Long);

   procedure Print (This : access Typ;
                    P1_Float : Java.Float);

   procedure Print (This : access Typ;
                    P1_Double : Java.Double);

   procedure Print (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr);

   procedure Print (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Print (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Println (This : access Typ);

   procedure Println (This : access Typ;
                      P1_Boolean : Java.Boolean);

   procedure Println (This : access Typ;
                      P1_Char : Java.Char);

   procedure Println (This : access Typ;
                      P1_Int : Java.Int);

   procedure Println (This : access Typ;
                      P1_Long : Java.Long);

   procedure Println (This : access Typ;
                      P1_Float : Java.Float);

   procedure Println (This : access Typ;
                      P1_Double : Java.Double);

   procedure Println (This : access Typ;
                      P1_Char_Arr : Java.Char_Arr);

   procedure Println (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Println (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Printf (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Printf (This : access Typ;
                    P1_Locale : access Standard.Java.Util.Locale.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Format (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Format (This : access Typ;
                    P1_Locale : access Standard.Java.Util.Locale.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Io.PrintWriter.Typ'Class;

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintWriter);
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");
   pragma Import (Java, CheckError, "checkError");
   pragma Import (Java, SetError, "setError");
   pragma Import (Java, ClearError, "clearError");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Print, "print");
   pragma Import (Java, Println, "println");
   pragma Import (Java, Printf, "printf");
   pragma Import (Java, Format, "format");
   pragma Import (Java, Append, "append");

end Java.Io.PrintWriter;
pragma Import (Java, Java.Io.PrintWriter, "java.io.PrintWriter");
pragma Extensions_Allowed (Off);
