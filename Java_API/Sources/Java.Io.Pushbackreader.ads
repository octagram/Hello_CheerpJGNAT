pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
with Java.Io.Closeable;
with Java.Io.FilterReader;
with Java.Lang.Object;
with Java.Lang.Readable;

package Java.Io.PushbackReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Readable_I : Java.Lang.Readable.Ref)
    is new Java.Io.FilterReader.Typ(Closeable_I,
                                    Readable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PushbackReader (P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                P2_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_PushbackReader (P1_Reader : access Standard.Java.Io.Reader.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Char_Arr : Java.Char_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Unread (This : access Typ;
                     P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Unread (This : access Typ;
                     P1_Char_Arr : Java.Char_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Unread (This : access Typ;
                     P1_Char_Arr : Java.Char_Arr);
   --  can raise Java.Io.IOException.Except

   function Ready (This : access Typ)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function MarkSupported (This : access Typ)
                           return Java.Boolean;

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PushbackReader);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Unread, "unread");
   pragma Import (Java, Ready, "ready");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, MarkSupported, "markSupported");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Skip, "skip");

end Java.Io.PushbackReader;
pragma Import (Java, Java.Io.PushbackReader, "java.io.PushbackReader");
pragma Extensions_Allowed (Off);
