pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.FileDescriptor;
limited with Java.Lang.String;
limited with Java.Nio.Channels.FileChannel;
with Java.Io.Closeable;
with Java.Io.DataInput;
with Java.Io.DataOutput;
with Java.Lang.Object;

package Java.Io.RandomAccessFile is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            DataInput_I : Java.Io.DataInput.Ref;
            DataOutput_I : Java.Io.DataOutput.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RandomAccessFile (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_RandomAccessFile (P1_File : access Standard.Java.Io.File.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetFD (This : access Typ)
                   return access Java.Io.FileDescriptor.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function GetChannel (This : access Typ)
                        return access Java.Nio.Channels.FileChannel.Typ'Class;

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function SkipBytes (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function GetFilePointer (This : access Typ)
                            return Java.Long;
   --  can raise Java.Io.IOException.Except

   procedure Seek (This : access Typ;
                   P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   function Length (This : access Typ)
                    return Java.Long;
   --  can raise Java.Io.IOException.Except

   procedure SetLength (This : access Typ;
                        P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadBoolean (This : access Typ)
                         return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadByte (This : access Typ)
                      return Java.Byte;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadUnsignedByte (This : access Typ)
                              return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadShort (This : access Typ)
                       return Java.Short;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadUnsignedShort (This : access Typ)
                               return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadChar (This : access Typ)
                      return Java.Char;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadInt (This : access Typ)
                     return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadLong (This : access Typ)
                      return Java.Long;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadFloat (This : access Typ)
                       return Java.Float;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadDouble (This : access Typ)
                        return Java.Double;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadLine (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function ReadUTF (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteBoolean (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteByte (This : access Typ;
                        P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteShort (This : access Typ;
                         P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteChar (This : access Typ;
                        P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteInt (This : access Typ;
                       P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteLong (This : access Typ;
                        P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteFloat (This : access Typ;
                         P1_Float : Java.Float);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteDouble (This : access Typ;
                          P1_Double : Java.Double);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteBytes (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteChars (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure WriteUTF (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RandomAccessFile);
   pragma Import (Java, GetFD, "getFD");
   pragma Import (Java, GetChannel, "getChannel");
   pragma Import (Java, Read, "read");
   pragma Import (Java, ReadFully, "readFully");
   pragma Import (Java, SkipBytes, "skipBytes");
   pragma Import (Java, Write, "write");
   pragma Import (Java, GetFilePointer, "getFilePointer");
   pragma Import (Java, Seek, "seek");
   pragma Import (Java, Length, "length");
   pragma Import (Java, SetLength, "setLength");
   pragma Import (Java, Close, "close");
   pragma Import (Java, ReadBoolean, "readBoolean");
   pragma Import (Java, ReadByte, "readByte");
   pragma Import (Java, ReadUnsignedByte, "readUnsignedByte");
   pragma Import (Java, ReadShort, "readShort");
   pragma Import (Java, ReadUnsignedShort, "readUnsignedShort");
   pragma Import (Java, ReadChar, "readChar");
   pragma Import (Java, ReadInt, "readInt");
   pragma Import (Java, ReadLong, "readLong");
   pragma Import (Java, ReadFloat, "readFloat");
   pragma Import (Java, ReadDouble, "readDouble");
   pragma Import (Java, ReadLine, "readLine");
   pragma Import (Java, ReadUTF, "readUTF");
   pragma Import (Java, WriteBoolean, "writeBoolean");
   pragma Import (Java, WriteByte, "writeByte");
   pragma Import (Java, WriteShort, "writeShort");
   pragma Import (Java, WriteChar, "writeChar");
   pragma Import (Java, WriteInt, "writeInt");
   pragma Import (Java, WriteLong, "writeLong");
   pragma Import (Java, WriteFloat, "writeFloat");
   pragma Import (Java, WriteDouble, "writeDouble");
   pragma Import (Java, WriteBytes, "writeBytes");
   pragma Import (Java, WriteChars, "writeChars");
   pragma Import (Java, WriteUTF, "writeUTF");

end Java.Io.RandomAccessFile;
pragma Import (Java, Java.Io.RandomAccessFile, "java.io.RandomAccessFile");
pragma Extensions_Allowed (Off);
