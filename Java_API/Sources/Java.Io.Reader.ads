pragma Extensions_Allowed (On);
limited with Java.Nio.CharBuffer;
with Java.Io.Closeable;
with Java.Lang.Object;
with Java.Lang.Readable;

package Java.Io.Reader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Readable_I : Java.Lang.Readable.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Lock : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Lock, "lock");

   end record;

   --  protected
   function New_Reader (This : Ref := null)
                        return Ref;

   --  protected
   function New_Reader (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ;
                  P1_CharBuffer : access Standard.Java.Nio.CharBuffer.Typ'Class)
                  return Java.Int
                  with Import => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ)
                  return Java.Int
                  with Import => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Char_Arr : Java.Char_Arr)
                  return Java.Int
                  with Import => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Char_Arr : Java.Char_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int is abstract
                  with Export => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Ready (This : access Typ)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function MarkSupported (This : access Typ)
                           return Java.Boolean;

   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Reader);
   -- pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Ready, "ready");
   pragma Import (Java, MarkSupported, "markSupported");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Export (Java, Close, "close");

end Java.Io.Reader;
pragma Import (Java, Java.Io.Reader, "java.io.Reader");
pragma Extensions_Allowed (Off);
