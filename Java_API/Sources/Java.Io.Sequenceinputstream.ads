pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Java.Io.SequenceInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SequenceInputStream (P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_SequenceInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                     P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SequenceInputStream);
   pragma Import (Java, Available, "available");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Close, "close");

end Java.Io.SequenceInputStream;
pragma Import (Java, Java.Io.SequenceInputStream, "java.io.SequenceInputStream");
pragma Extensions_Allowed (Off);
