pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Io.StreamTokenizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Ttype : Java.Int;
      pragma Import (Java, Ttype, "ttype");

      Sval : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Sval, "sval");

      Nval : Java.Double;
      pragma Import (Java, Nval, "nval");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StreamTokenizer (P1_Reader : access Standard.Java.Io.Reader.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ResetSyntax (This : access Typ);

   procedure WordChars (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   procedure WhitespaceChars (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int);

   procedure OrdinaryChars (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);

   procedure OrdinaryChar (This : access Typ;
                           P1_Int : Java.Int);

   procedure CommentChar (This : access Typ;
                          P1_Int : Java.Int);

   procedure QuoteChar (This : access Typ;
                        P1_Int : Java.Int);

   procedure ParseNumbers (This : access Typ);

   procedure EolIsSignificant (This : access Typ;
                               P1_Boolean : Java.Boolean);

   procedure SlashStarComments (This : access Typ;
                                P1_Boolean : Java.Boolean);

   procedure SlashSlashComments (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   procedure LowerCaseMode (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function NextToken (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure PushBack (This : access Typ);

   function Lineno (This : access Typ)
                    return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TT_EOF : constant Java.Int;

   --  final
   TT_EOL : constant Java.Int;

   --  final
   TT_NUMBER : constant Java.Int;

   --  final
   TT_WORD : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StreamTokenizer);
   pragma Import (Java, ResetSyntax, "resetSyntax");
   pragma Import (Java, WordChars, "wordChars");
   pragma Import (Java, WhitespaceChars, "whitespaceChars");
   pragma Import (Java, OrdinaryChars, "ordinaryChars");
   pragma Import (Java, OrdinaryChar, "ordinaryChar");
   pragma Import (Java, CommentChar, "commentChar");
   pragma Import (Java, QuoteChar, "quoteChar");
   pragma Import (Java, ParseNumbers, "parseNumbers");
   pragma Import (Java, EolIsSignificant, "eolIsSignificant");
   pragma Import (Java, SlashStarComments, "slashStarComments");
   pragma Import (Java, SlashSlashComments, "slashSlashComments");
   pragma Import (Java, LowerCaseMode, "lowerCaseMode");
   pragma Import (Java, NextToken, "nextToken");
   pragma Import (Java, PushBack, "pushBack");
   pragma Import (Java, Lineno, "lineno");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, TT_EOF, "TT_EOF");
   pragma Import (Java, TT_EOL, "TT_EOL");
   pragma Import (Java, TT_NUMBER, "TT_NUMBER");
   pragma Import (Java, TT_WORD, "TT_WORD");

end Java.Io.StreamTokenizer;
pragma Import (Java, Java.Io.StreamTokenizer, "java.io.StreamTokenizer");
pragma Extensions_Allowed (Off);
