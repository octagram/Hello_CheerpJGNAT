pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Java.Io.StringBufferInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buffer : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Buffer, "buffer");

      --  protected
      Pos : Java.Int;
      pragma Import (Java, Pos, "pos");

      --  protected
      Count : Java.Int;
      pragma Import (Java, Count, "count");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringBufferInputStream (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Read (This : access Typ)
                  return Java.Int;

   --  synchronized
   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;

   --  synchronized
   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;

   --  synchronized
   function Available (This : access Typ)
                       return Java.Int;

   --  synchronized
   procedure Reset (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringBufferInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Reset, "reset");

end Java.Io.StringBufferInputStream;
pragma Import (Java, Java.Io.StringBufferInputStream, "java.io.StringBufferInputStream");
pragma Extensions_Allowed (Off);
