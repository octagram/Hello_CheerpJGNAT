pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.Writer;
with Java.Lang.Appendable;
with Java.Lang.Object;

package Java.Io.StringWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref)
    is new Java.Io.Writer.Typ(Closeable_I,
                              Flushable_I,
                              Appendable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringWriter (This : Ref := null)
                              return Ref;

   function New_StringWriter (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);

   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Io.StringWriter.Typ'Class;

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Io.StringWriter.Typ'Class;

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Io.StringWriter.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetBuffer (This : access Typ)
                       return access Java.Lang.StringBuffer.Typ'Class;

   procedure Flush (This : access Typ);

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringWriter);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Append, "append");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetBuffer, "getBuffer");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Java.Io.StringWriter;
pragma Import (Java, Java.Io.StringWriter, "java.io.StringWriter");
pragma Extensions_Allowed (Off);
