pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Appendable;
with Java.Lang.Object;

package Java.Io.Writer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Lock : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Lock, "lock");

   end record;

   --  protected
   function New_Writer (This : Ref := null)
                        return Ref;

   --  protected
   function New_Writer (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int) with Import => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr) with Import => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int) is abstract with Export => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class) with Import => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int) with Import => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Writer);
   -- pragma Import (Java, Write, "write");
   pragma Import (Java, Append, "append");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, Close, "close");

end Java.Io.Writer;
pragma Import (Java, Java.Io.Writer, "java.io.Writer");
pragma Extensions_Allowed (Off);
