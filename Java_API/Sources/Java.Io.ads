pragma Extensions_Allowed (On);
package Java.Io is
   pragma Preelaborate;
end Java.Io;
pragma Import (Java, Java.Io, "java.io");
pragma Extensions_Allowed (Off);
