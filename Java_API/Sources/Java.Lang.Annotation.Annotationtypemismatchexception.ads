pragma Extensions_Allowed (On);
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Java.Lang.Annotation.AnnotationTypeMismatchException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AnnotationTypeMismatchException (P1_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                                 P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                                 This : Ref := null)
                                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Element (This : access Typ)
                     return access Java.Lang.Reflect.Method.Typ'Class;

   function FoundType (This : access Typ)
                       return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.lang.annotation.AnnotationTypeMismatchException");
   pragma Java_Constructor (New_AnnotationTypeMismatchException);
   pragma Import (Java, Element, "element");
   pragma Import (Java, FoundType, "foundType");

end Java.Lang.Annotation.AnnotationTypeMismatchException;
pragma Import (Java, Java.Lang.Annotation.AnnotationTypeMismatchException, "java.lang.annotation.AnnotationTypeMismatchException");
pragma Extensions_Allowed (Off);
