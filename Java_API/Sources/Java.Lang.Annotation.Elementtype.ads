pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Java.Lang.Annotation.ElementType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Annotation.ElementType.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_K : access Java.Lang.Annotation.ElementType.Typ'Class;

   --  final
   FIELD : access Java.Lang.Annotation.ElementType.Typ'Class;

   --  final
   METHOD : access Java.Lang.Annotation.ElementType.Typ'Class;

   --  final
   PARAMETER : access Java.Lang.Annotation.ElementType.Typ'Class;

   --  final
   CONSTRUCTOR : access Java.Lang.Annotation.ElementType.Typ'Class;

   --  final
   LOCAL_VARIABLE : access Java.Lang.Annotation.ElementType.Typ'Class;

   --  final
   ANNOTATION_TYPE : access Java.Lang.Annotation.ElementType.Typ'Class;

   --  final
   PACKAGE_K : access Java.Lang.Annotation.ElementType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, TYPE_K, "TYPE");
   pragma Import (Java, FIELD, "FIELD");
   pragma Import (Java, METHOD, "METHOD");
   pragma Import (Java, PARAMETER, "PARAMETER");
   pragma Import (Java, CONSTRUCTOR, "CONSTRUCTOR");
   pragma Import (Java, LOCAL_VARIABLE, "LOCAL_VARIABLE");
   pragma Import (Java, ANNOTATION_TYPE, "ANNOTATION_TYPE");
   pragma Import (Java, PACKAGE_K, "PACKAGE");

end Java.Lang.Annotation.ElementType;
pragma Import (Java, Java.Lang.Annotation.ElementType, "java.lang.annotation.ElementType");
pragma Extensions_Allowed (Off);
