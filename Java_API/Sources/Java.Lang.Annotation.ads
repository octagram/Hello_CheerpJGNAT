pragma Extensions_Allowed (On);
package Java.Lang.Annotation is
   pragma Preelaborate;
end Java.Lang.Annotation;
pragma Import (Java, Java.Lang.Annotation, "java.lang.annotation");
pragma Extensions_Allowed (Off);
