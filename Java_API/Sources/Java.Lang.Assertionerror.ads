pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Error;
with Java.Lang.Object;

package Java.Lang.AssertionError is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Error.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AssertionError (This : Ref := null)
                                return Ref;

   function New_AssertionError (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_AssertionError (P1_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   function New_AssertionError (P1_Char : Java.Char; 
                                This : Ref := null)
                                return Ref;

   function New_AssertionError (P1_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_AssertionError (P1_Long : Java.Long; 
                                This : Ref := null)
                                return Ref;

   function New_AssertionError (P1_Float : Java.Float; 
                                This : Ref := null)
                                return Ref;

   function New_AssertionError (P1_Double : Java.Double; 
                                This : Ref := null)
                                return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.lang.AssertionError");
   pragma Java_Constructor (New_AssertionError);

end Java.Lang.AssertionError;
pragma Import (Java, Java.Lang.AssertionError, "java.lang.AssertionError");
pragma Extensions_Allowed (Off);
