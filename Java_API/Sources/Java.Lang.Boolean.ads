pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Lang.Boolean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Boolean (P1_Boolean : Java.Boolean; 
                         This : Ref := null)
                         return Ref;

   function New_Boolean (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ParseBoolean (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean;

   function BooleanValue (This : access Typ)
                          return Java.Boolean;

   function ValueOf (P1_Boolean : Java.Boolean)
                     return access Java.Lang.Boolean.Typ'Class;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Boolean.Typ'Class;

   function ToString (P1_Boolean : Java.Boolean)
                      return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function GetBoolean (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class)
                       return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TRUE : access Java.Lang.Boolean.Typ'Class;

   --  final
   FALSE : access Java.Lang.Boolean.Typ'Class;

   --  final
   TYPE_K : access Java.Lang.Class.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Boolean);
   pragma Import (Java, ParseBoolean, "parseBoolean");
   pragma Import (Java, BooleanValue, "booleanValue");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetBoolean, "getBoolean");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, TRUE, "TRUE");
   pragma Import (Java, FALSE, "FALSE");
   pragma Import (Java, TYPE_K, "TYPE");

end Java.Lang.Boolean;
pragma Import (Java, Java.Lang.Boolean, "java.lang.Boolean");
pragma Extensions_Allowed (Off);
