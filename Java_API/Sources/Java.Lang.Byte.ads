pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Number;
with Java.Lang.Object;

package Java.Lang.Byte is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Number.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (P1_Byte : Java.Byte)
                      return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Byte : Java.Byte)
                     return access Java.Lang.Byte.Typ'Class;

   function ParseByte (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Byte;
   --  can raise Java.Lang.NumberFormatException.Except

   function ParseByte (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int)
                       return Java.Byte;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return access Java.Lang.Byte.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Byte.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function Decode (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Byte.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Byte (P1_Byte : Java.Byte; 
                      This : Ref := null)
                      return Ref;

   function New_Byte (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;
   --  can raise Java.Lang.NumberFormatException.Except

   function ByteValue (This : access Typ)
                       return Java.Byte;

   function ShortValue (This : access Typ)
                        return Java.Short;

   function IntValue (This : access Typ)
                      return Java.Int;

   function LongValue (This : access Typ)
                       return Java.Long;

   function FloatValue (This : access Typ)
                        return Java.Float;

   function DoubleValue (This : access Typ)
                         return Java.Double;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_Byte : access Standard.Java.Lang.Byte.Typ'Class)
                       return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MIN_VALUE : constant Java.Byte;

   --  final
   MAX_VALUE : constant Java.Byte;

   --  final
   TYPE_K : access Java.Lang.Class.Typ'Class;

   --  final
   SIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, ParseByte, "parseByte");
   pragma Import (Java, Decode, "decode");
   pragma Java_Constructor (New_Byte);
   pragma Import (Java, ByteValue, "byteValue");
   pragma Import (Java, ShortValue, "shortValue");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, LongValue, "longValue");
   pragma Import (Java, FloatValue, "floatValue");
   pragma Import (Java, DoubleValue, "doubleValue");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, MIN_VALUE, "MIN_VALUE");
   pragma Import (Java, MAX_VALUE, "MAX_VALUE");
   pragma Import (Java, TYPE_K, "TYPE");
   pragma Import (Java, SIZE, "SIZE");

end Java.Lang.Byte;
pragma Import (Java, Java.Lang.Byte, "java.lang.Byte");
pragma Extensions_Allowed (Off);
