pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Character.Subset;
with Java.Lang.Object;

package Java.Lang.Character.UnicodeBlock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Character.Subset.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function of_K (P1_Char : Java.Char)
                  return access Java.Lang.Character.UnicodeBlock.Typ'Class;

   function of_K (P1_Int : Java.Int)
                  return access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   function ForName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Character.UnicodeBlock.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BASIC_LATIN : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LATIN_1_SUPPLEMENT : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LATIN_EXTENDED_A : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LATIN_EXTENDED_B : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   IPA_EXTENSIONS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SPACING_MODIFIER_LETTERS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   COMBINING_DIACRITICAL_MARKS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GREEK : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CYRILLIC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ARMENIAN : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HEBREW : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ARABIC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   DEVANAGARI : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BENGALI : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GURMUKHI : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GUJARATI : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ORIYA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TAMIL : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TELUGU : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   KANNADA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MALAYALAM : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   THAI : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LAO : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TIBETAN : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GEORGIAN : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HANGUL_JAMO : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LATIN_EXTENDED_ADDITIONAL : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GREEK_EXTENDED : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GENERAL_PUNCTUATION : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SUPERSCRIPTS_AND_SUBSCRIPTS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CURRENCY_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   COMBINING_MARKS_FOR_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LETTERLIKE_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   NUMBER_FORMS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ARROWS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MATHEMATICAL_OPERATORS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MISCELLANEOUS_TECHNICAL : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CONTROL_PICTURES : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   OPTICAL_CHARACTER_RECOGNITION : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ENCLOSED_ALPHANUMERICS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BOX_DRAWING : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BLOCK_ELEMENTS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GEOMETRIC_SHAPES : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MISCELLANEOUS_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   DINGBATS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_SYMBOLS_AND_PUNCTUATION : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HIRAGANA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   KATAKANA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BOPOMOFO : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HANGUL_COMPATIBILITY_JAMO : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   KANBUN : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ENCLOSED_CJK_LETTERS_AND_MONTHS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_COMPATIBILITY : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_UNIFIED_IDEOGRAPHS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HANGUL_SYLLABLES : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   PRIVATE_USE_AREA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_COMPATIBILITY_IDEOGRAPHS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ALPHABETIC_PRESENTATION_FORMS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ARABIC_PRESENTATION_FORMS_A : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   COMBINING_HALF_MARKS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_COMPATIBILITY_FORMS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SMALL_FORM_VARIANTS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ARABIC_PRESENTATION_FORMS_B : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HALFWIDTH_AND_FULLWIDTH_FORMS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SPECIALS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SYRIAC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   THAANA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SINHALA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MYANMAR : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   ETHIOPIC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CHEROKEE : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   OGHAM : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   RUNIC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   KHMER : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MONGOLIAN : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BRAILLE_PATTERNS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_RADICALS_SUPPLEMENT : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   KANGXI_RADICALS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   IDEOGRAPHIC_DESCRIPTION_CHARACTERS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BOPOMOFO_EXTENDED : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   YI_SYLLABLES : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   YI_RADICALS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CYRILLIC_SUPPLEMENTARY : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TAGALOG : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HANUNOO : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BUHID : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TAGBANWA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LIMBU : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TAI_LE : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   KHMER_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   PHONETIC_EXTENSIONS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SUPPLEMENTAL_ARROWS_A : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SUPPLEMENTAL_ARROWS_B : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SUPPLEMENTAL_MATHEMATICAL_OPERATORS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MISCELLANEOUS_SYMBOLS_AND_ARROWS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   KATAKANA_PHONETIC_EXTENSIONS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   YIJING_HEXAGRAM_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   VARIATION_SELECTORS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LINEAR_B_SYLLABARY : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LINEAR_B_IDEOGRAMS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   AEGEAN_NUMBERS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   OLD_ITALIC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   GOTHIC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   UGARITIC : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   DESERET : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SHAVIAN : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   OSMANYA : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CYPRIOT_SYLLABARY : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   BYZANTINE_MUSICAL_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MUSICAL_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TAI_XUAN_JING_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   MATHEMATICAL_ALPHANUMERIC_SYMBOLS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   TAGS : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   VARIATION_SELECTORS_SUPPLEMENT : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SUPPLEMENTARY_PRIVATE_USE_AREA_A : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   SUPPLEMENTARY_PRIVATE_USE_AREA_B : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HIGH_SURROGATES : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   HIGH_PRIVATE_USE_SURROGATES : access Java.Lang.Character.UnicodeBlock.Typ'Class;

   --  final
   LOW_SURROGATES : access Java.Lang.Character.UnicodeBlock.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, of_K, "of");
   pragma Import (Java, ForName, "forName");
   pragma Import (Java, BASIC_LATIN, "BASIC_LATIN");
   pragma Import (Java, LATIN_1_SUPPLEMENT, "LATIN_1_SUPPLEMENT");
   pragma Import (Java, LATIN_EXTENDED_A, "LATIN_EXTENDED_A");
   pragma Import (Java, LATIN_EXTENDED_B, "LATIN_EXTENDED_B");
   pragma Import (Java, IPA_EXTENSIONS, "IPA_EXTENSIONS");
   pragma Import (Java, SPACING_MODIFIER_LETTERS, "SPACING_MODIFIER_LETTERS");
   pragma Import (Java, COMBINING_DIACRITICAL_MARKS, "COMBINING_DIACRITICAL_MARKS");
   pragma Import (Java, GREEK, "GREEK");
   pragma Import (Java, CYRILLIC, "CYRILLIC");
   pragma Import (Java, ARMENIAN, "ARMENIAN");
   pragma Import (Java, HEBREW, "HEBREW");
   pragma Import (Java, ARABIC, "ARABIC");
   pragma Import (Java, DEVANAGARI, "DEVANAGARI");
   pragma Import (Java, BENGALI, "BENGALI");
   pragma Import (Java, GURMUKHI, "GURMUKHI");
   pragma Import (Java, GUJARATI, "GUJARATI");
   pragma Import (Java, ORIYA, "ORIYA");
   pragma Import (Java, TAMIL, "TAMIL");
   pragma Import (Java, TELUGU, "TELUGU");
   pragma Import (Java, KANNADA, "KANNADA");
   pragma Import (Java, MALAYALAM, "MALAYALAM");
   pragma Import (Java, THAI, "THAI");
   pragma Import (Java, LAO, "LAO");
   pragma Import (Java, TIBETAN, "TIBETAN");
   pragma Import (Java, GEORGIAN, "GEORGIAN");
   pragma Import (Java, HANGUL_JAMO, "HANGUL_JAMO");
   pragma Import (Java, LATIN_EXTENDED_ADDITIONAL, "LATIN_EXTENDED_ADDITIONAL");
   pragma Import (Java, GREEK_EXTENDED, "GREEK_EXTENDED");
   pragma Import (Java, GENERAL_PUNCTUATION, "GENERAL_PUNCTUATION");
   pragma Import (Java, SUPERSCRIPTS_AND_SUBSCRIPTS, "SUPERSCRIPTS_AND_SUBSCRIPTS");
   pragma Import (Java, CURRENCY_SYMBOLS, "CURRENCY_SYMBOLS");
   pragma Import (Java, COMBINING_MARKS_FOR_SYMBOLS, "COMBINING_MARKS_FOR_SYMBOLS");
   pragma Import (Java, LETTERLIKE_SYMBOLS, "LETTERLIKE_SYMBOLS");
   pragma Import (Java, NUMBER_FORMS, "NUMBER_FORMS");
   pragma Import (Java, ARROWS, "ARROWS");
   pragma Import (Java, MATHEMATICAL_OPERATORS, "MATHEMATICAL_OPERATORS");
   pragma Import (Java, MISCELLANEOUS_TECHNICAL, "MISCELLANEOUS_TECHNICAL");
   pragma Import (Java, CONTROL_PICTURES, "CONTROL_PICTURES");
   pragma Import (Java, OPTICAL_CHARACTER_RECOGNITION, "OPTICAL_CHARACTER_RECOGNITION");
   pragma Import (Java, ENCLOSED_ALPHANUMERICS, "ENCLOSED_ALPHANUMERICS");
   pragma Import (Java, BOX_DRAWING, "BOX_DRAWING");
   pragma Import (Java, BLOCK_ELEMENTS, "BLOCK_ELEMENTS");
   pragma Import (Java, GEOMETRIC_SHAPES, "GEOMETRIC_SHAPES");
   pragma Import (Java, MISCELLANEOUS_SYMBOLS, "MISCELLANEOUS_SYMBOLS");
   pragma Import (Java, DINGBATS, "DINGBATS");
   pragma Import (Java, CJK_SYMBOLS_AND_PUNCTUATION, "CJK_SYMBOLS_AND_PUNCTUATION");
   pragma Import (Java, HIRAGANA, "HIRAGANA");
   pragma Import (Java, KATAKANA, "KATAKANA");
   pragma Import (Java, BOPOMOFO, "BOPOMOFO");
   pragma Import (Java, HANGUL_COMPATIBILITY_JAMO, "HANGUL_COMPATIBILITY_JAMO");
   pragma Import (Java, KANBUN, "KANBUN");
   pragma Import (Java, ENCLOSED_CJK_LETTERS_AND_MONTHS, "ENCLOSED_CJK_LETTERS_AND_MONTHS");
   pragma Import (Java, CJK_COMPATIBILITY, "CJK_COMPATIBILITY");
   pragma Import (Java, CJK_UNIFIED_IDEOGRAPHS, "CJK_UNIFIED_IDEOGRAPHS");
   pragma Import (Java, HANGUL_SYLLABLES, "HANGUL_SYLLABLES");
   pragma Import (Java, PRIVATE_USE_AREA, "PRIVATE_USE_AREA");
   pragma Import (Java, CJK_COMPATIBILITY_IDEOGRAPHS, "CJK_COMPATIBILITY_IDEOGRAPHS");
   pragma Import (Java, ALPHABETIC_PRESENTATION_FORMS, "ALPHABETIC_PRESENTATION_FORMS");
   pragma Import (Java, ARABIC_PRESENTATION_FORMS_A, "ARABIC_PRESENTATION_FORMS_A");
   pragma Import (Java, COMBINING_HALF_MARKS, "COMBINING_HALF_MARKS");
   pragma Import (Java, CJK_COMPATIBILITY_FORMS, "CJK_COMPATIBILITY_FORMS");
   pragma Import (Java, SMALL_FORM_VARIANTS, "SMALL_FORM_VARIANTS");
   pragma Import (Java, ARABIC_PRESENTATION_FORMS_B, "ARABIC_PRESENTATION_FORMS_B");
   pragma Import (Java, HALFWIDTH_AND_FULLWIDTH_FORMS, "HALFWIDTH_AND_FULLWIDTH_FORMS");
   pragma Import (Java, SPECIALS, "SPECIALS");
   pragma Import (Java, SYRIAC, "SYRIAC");
   pragma Import (Java, THAANA, "THAANA");
   pragma Import (Java, SINHALA, "SINHALA");
   pragma Import (Java, MYANMAR, "MYANMAR");
   pragma Import (Java, ETHIOPIC, "ETHIOPIC");
   pragma Import (Java, CHEROKEE, "CHEROKEE");
   pragma Import (Java, UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS, "UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS");
   pragma Import (Java, OGHAM, "OGHAM");
   pragma Import (Java, RUNIC, "RUNIC");
   pragma Import (Java, KHMER, "KHMER");
   pragma Import (Java, MONGOLIAN, "MONGOLIAN");
   pragma Import (Java, BRAILLE_PATTERNS, "BRAILLE_PATTERNS");
   pragma Import (Java, CJK_RADICALS_SUPPLEMENT, "CJK_RADICALS_SUPPLEMENT");
   pragma Import (Java, KANGXI_RADICALS, "KANGXI_RADICALS");
   pragma Import (Java, IDEOGRAPHIC_DESCRIPTION_CHARACTERS, "IDEOGRAPHIC_DESCRIPTION_CHARACTERS");
   pragma Import (Java, BOPOMOFO_EXTENDED, "BOPOMOFO_EXTENDED");
   pragma Import (Java, CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A, "CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A");
   pragma Import (Java, YI_SYLLABLES, "YI_SYLLABLES");
   pragma Import (Java, YI_RADICALS, "YI_RADICALS");
   pragma Import (Java, CYRILLIC_SUPPLEMENTARY, "CYRILLIC_SUPPLEMENTARY");
   pragma Import (Java, TAGALOG, "TAGALOG");
   pragma Import (Java, HANUNOO, "HANUNOO");
   pragma Import (Java, BUHID, "BUHID");
   pragma Import (Java, TAGBANWA, "TAGBANWA");
   pragma Import (Java, LIMBU, "LIMBU");
   pragma Import (Java, TAI_LE, "TAI_LE");
   pragma Import (Java, KHMER_SYMBOLS, "KHMER_SYMBOLS");
   pragma Import (Java, PHONETIC_EXTENSIONS, "PHONETIC_EXTENSIONS");
   pragma Import (Java, MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A, "MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A");
   pragma Import (Java, SUPPLEMENTAL_ARROWS_A, "SUPPLEMENTAL_ARROWS_A");
   pragma Import (Java, SUPPLEMENTAL_ARROWS_B, "SUPPLEMENTAL_ARROWS_B");
   pragma Import (Java, MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B, "MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B");
   pragma Import (Java, SUPPLEMENTAL_MATHEMATICAL_OPERATORS, "SUPPLEMENTAL_MATHEMATICAL_OPERATORS");
   pragma Import (Java, MISCELLANEOUS_SYMBOLS_AND_ARROWS, "MISCELLANEOUS_SYMBOLS_AND_ARROWS");
   pragma Import (Java, KATAKANA_PHONETIC_EXTENSIONS, "KATAKANA_PHONETIC_EXTENSIONS");
   pragma Import (Java, YIJING_HEXAGRAM_SYMBOLS, "YIJING_HEXAGRAM_SYMBOLS");
   pragma Import (Java, VARIATION_SELECTORS, "VARIATION_SELECTORS");
   pragma Import (Java, LINEAR_B_SYLLABARY, "LINEAR_B_SYLLABARY");
   pragma Import (Java, LINEAR_B_IDEOGRAMS, "LINEAR_B_IDEOGRAMS");
   pragma Import (Java, AEGEAN_NUMBERS, "AEGEAN_NUMBERS");
   pragma Import (Java, OLD_ITALIC, "OLD_ITALIC");
   pragma Import (Java, GOTHIC, "GOTHIC");
   pragma Import (Java, UGARITIC, "UGARITIC");
   pragma Import (Java, DESERET, "DESERET");
   pragma Import (Java, SHAVIAN, "SHAVIAN");
   pragma Import (Java, OSMANYA, "OSMANYA");
   pragma Import (Java, CYPRIOT_SYLLABARY, "CYPRIOT_SYLLABARY");
   pragma Import (Java, BYZANTINE_MUSICAL_SYMBOLS, "BYZANTINE_MUSICAL_SYMBOLS");
   pragma Import (Java, MUSICAL_SYMBOLS, "MUSICAL_SYMBOLS");
   pragma Import (Java, TAI_XUAN_JING_SYMBOLS, "TAI_XUAN_JING_SYMBOLS");
   pragma Import (Java, MATHEMATICAL_ALPHANUMERIC_SYMBOLS, "MATHEMATICAL_ALPHANUMERIC_SYMBOLS");
   pragma Import (Java, CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B, "CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B");
   pragma Import (Java, CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT, "CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT");
   pragma Import (Java, TAGS, "TAGS");
   pragma Import (Java, VARIATION_SELECTORS_SUPPLEMENT, "VARIATION_SELECTORS_SUPPLEMENT");
   pragma Import (Java, SUPPLEMENTARY_PRIVATE_USE_AREA_A, "SUPPLEMENTARY_PRIVATE_USE_AREA_A");
   pragma Import (Java, SUPPLEMENTARY_PRIVATE_USE_AREA_B, "SUPPLEMENTARY_PRIVATE_USE_AREA_B");
   pragma Import (Java, HIGH_SURROGATES, "HIGH_SURROGATES");
   pragma Import (Java, HIGH_PRIVATE_USE_SURROGATES, "HIGH_PRIVATE_USE_SURROGATES");
   pragma Import (Java, LOW_SURROGATES, "LOW_SURROGATES");

end Java.Lang.Character.UnicodeBlock;
pragma Import (Java, Java.Lang.Character.UnicodeBlock, "java.lang.Character$UnicodeBlock");
pragma Extensions_Allowed (Off);
