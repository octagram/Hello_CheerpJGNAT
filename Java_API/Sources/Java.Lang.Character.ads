pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Lang.Character is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Character (P1_Char : Java.Char; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ValueOf (P1_Char : Java.Char)
                     return access Java.Lang.Character.Typ'Class;

   function CharValue (This : access Typ)
                       return Java.Char;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Char : Java.Char)
                      return access Java.Lang.String.Typ'Class;

   function IsValidCodePoint (P1_Int : Java.Int)
                              return Java.Boolean;

   function IsSupplementaryCodePoint (P1_Int : Java.Int)
                                      return Java.Boolean;

   function IsHighSurrogate (P1_Char : Java.Char)
                             return Java.Boolean;

   function IsLowSurrogate (P1_Char : Java.Char)
                            return Java.Boolean;

   function IsSurrogatePair (P1_Char : Java.Char;
                             P2_Char : Java.Char)
                             return Java.Boolean;

   function CharCount (P1_Int : Java.Int)
                       return Java.Int;

   function ToCodePoint (P1_Char : Java.Char;
                         P2_Char : Java.Char)
                         return Java.Int;

   function CodePointAt (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;

   function CodePointAt (P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int)
                         return Java.Int;

   function CodePointAt (P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function CodePointBefore (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                             P2_Int : Java.Int)
                             return Java.Int;

   function CodePointBefore (P1_Char_Arr : Java.Char_Arr;
                             P2_Int : Java.Int)
                             return Java.Int;

   function CodePointBefore (P1_Char_Arr : Java.Char_Arr;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int)
                             return Java.Int;

   function ToChars (P1_Int : Java.Int;
                     P2_Char_Arr : Java.Char_Arr;
                     P3_Int : Java.Int)
                     return Java.Int;

   function ToChars (P1_Int : Java.Int)
                     return Java.Char_Arr;

   function CodePointCount (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return Java.Int;

   function CodePointCount (P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return Java.Int;

   function OffsetByCodePoints (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int)
                                return Java.Int;

   function OffsetByCodePoints (P1_Char_Arr : Java.Char_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int)
                                return Java.Int;

   function IsLowerCase (P1_Char : Java.Char)
                         return Java.Boolean;

   function IsLowerCase (P1_Int : Java.Int)
                         return Java.Boolean;

   function IsUpperCase (P1_Char : Java.Char)
                         return Java.Boolean;

   function IsUpperCase (P1_Int : Java.Int)
                         return Java.Boolean;

   function IsTitleCase (P1_Char : Java.Char)
                         return Java.Boolean;

   function IsTitleCase (P1_Int : Java.Int)
                         return Java.Boolean;

   function IsDigit (P1_Char : Java.Char)
                     return Java.Boolean;

   function IsDigit (P1_Int : Java.Int)
                     return Java.Boolean;

   function IsDefined (P1_Char : Java.Char)
                       return Java.Boolean;

   function IsDefined (P1_Int : Java.Int)
                       return Java.Boolean;

   function IsLetter (P1_Char : Java.Char)
                      return Java.Boolean;

   function IsLetter (P1_Int : Java.Int)
                      return Java.Boolean;

   function IsLetterOrDigit (P1_Char : Java.Char)
                             return Java.Boolean;

   function IsLetterOrDigit (P1_Int : Java.Int)
                             return Java.Boolean;

   function IsJavaIdentifierStart (P1_Char : Java.Char)
                                   return Java.Boolean;

   function IsJavaIdentifierStart (P1_Int : Java.Int)
                                   return Java.Boolean;

   function IsJavaIdentifierPart (P1_Char : Java.Char)
                                  return Java.Boolean;

   function IsJavaIdentifierPart (P1_Int : Java.Int)
                                  return Java.Boolean;

   function IsUnicodeIdentifierStart (P1_Char : Java.Char)
                                      return Java.Boolean;

   function IsUnicodeIdentifierStart (P1_Int : Java.Int)
                                      return Java.Boolean;

   function IsUnicodeIdentifierPart (P1_Char : Java.Char)
                                     return Java.Boolean;

   function IsUnicodeIdentifierPart (P1_Int : Java.Int)
                                     return Java.Boolean;

   function IsIdentifierIgnorable (P1_Char : Java.Char)
                                   return Java.Boolean;

   function IsIdentifierIgnorable (P1_Int : Java.Int)
                                   return Java.Boolean;

   function ToLowerCase (P1_Char : Java.Char)
                         return Java.Char;

   function ToLowerCase (P1_Int : Java.Int)
                         return Java.Int;

   function ToUpperCase (P1_Char : Java.Char)
                         return Java.Char;

   function ToUpperCase (P1_Int : Java.Int)
                         return Java.Int;

   function ToTitleCase (P1_Char : Java.Char)
                         return Java.Char;

   function ToTitleCase (P1_Int : Java.Int)
                         return Java.Int;

   function Digit (P1_Char : Java.Char;
                   P2_Int : Java.Int)
                   return Java.Int;

   function Digit (P1_Int : Java.Int;
                   P2_Int : Java.Int)
                   return Java.Int;

   function GetNumericValue (P1_Char : Java.Char)
                             return Java.Int;

   function GetNumericValue (P1_Int : Java.Int)
                             return Java.Int;

   function IsSpaceChar (P1_Char : Java.Char)
                         return Java.Boolean;

   function IsSpaceChar (P1_Int : Java.Int)
                         return Java.Boolean;

   function IsWhitespace (P1_Char : Java.Char)
                          return Java.Boolean;

   function IsWhitespace (P1_Int : Java.Int)
                          return Java.Boolean;

   function IsISOControl (P1_Char : Java.Char)
                          return Java.Boolean;

   function IsISOControl (P1_Int : Java.Int)
                          return Java.Boolean;

   function GetType (P1_Char : Java.Char)
                     return Java.Int;

   function GetType (P1_Int : Java.Int)
                     return Java.Int;

   function ForDigit (P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return Java.Char;

   function GetDirectionality (P1_Char : Java.Char)
                               return Java.Byte;

   function GetDirectionality (P1_Int : Java.Int)
                               return Java.Byte;

   function IsMirrored (P1_Char : Java.Char)
                        return Java.Boolean;

   function IsMirrored (P1_Int : Java.Int)
                        return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_Character : access Standard.Java.Lang.Character.Typ'Class)
                       return Java.Int;

   function ReverseBytes (P1_Char : Java.Char)
                          return Java.Char;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MIN_RADIX : constant Java.Int;

   --  final
   MAX_RADIX : constant Java.Int;

   --  final
   MIN_VALUE : constant Java.Char;

   --  final
   MAX_VALUE : constant Java.Char;

   --  final
   TYPE_K : access Java.Lang.Class.Typ'Class;

   --  final
   UNASSIGNED : constant Java.Byte;

   --  final
   UPPERCASE_LETTER : constant Java.Byte;

   --  final
   LOWERCASE_LETTER : constant Java.Byte;

   --  final
   TITLECASE_LETTER : constant Java.Byte;

   --  final
   MODIFIER_LETTER : constant Java.Byte;

   --  final
   OTHER_LETTER : constant Java.Byte;

   --  final
   NON_SPACING_MARK : constant Java.Byte;

   --  final
   ENCLOSING_MARK : constant Java.Byte;

   --  final
   COMBINING_SPACING_MARK : constant Java.Byte;

   --  final
   DECIMAL_DIGIT_NUMBER : constant Java.Byte;

   --  final
   LETTER_NUMBER : constant Java.Byte;

   --  final
   OTHER_NUMBER : constant Java.Byte;

   --  final
   SPACE_SEPARATOR : constant Java.Byte;

   --  final
   LINE_SEPARATOR : constant Java.Byte;

   --  final
   PARAGRAPH_SEPARATOR : constant Java.Byte;

   --  final
   CONTROL : constant Java.Byte;

   --  final
   FORMAT : constant Java.Byte;

   --  final
   PRIVATE_USE : constant Java.Byte;

   --  final
   SURROGATE : constant Java.Byte;

   --  final
   DASH_PUNCTUATION : constant Java.Byte;

   --  final
   START_PUNCTUATION : constant Java.Byte;

   --  final
   END_PUNCTUATION : constant Java.Byte;

   --  final
   CONNECTOR_PUNCTUATION : constant Java.Byte;

   --  final
   OTHER_PUNCTUATION : constant Java.Byte;

   --  final
   MATH_SYMBOL : constant Java.Byte;

   --  final
   CURRENCY_SYMBOL : constant Java.Byte;

   --  final
   MODIFIER_SYMBOL : constant Java.Byte;

   --  final
   OTHER_SYMBOL : constant Java.Byte;

   --  final
   INITIAL_QUOTE_PUNCTUATION : constant Java.Byte;

   --  final
   FINAL_QUOTE_PUNCTUATION : constant Java.Byte;

   --  final
   DIRECTIONALITY_UNDEFINED : constant Java.Byte;

   --  final
   DIRECTIONALITY_LEFT_TO_RIGHT : constant Java.Byte;

   --  final
   DIRECTIONALITY_RIGHT_TO_LEFT : constant Java.Byte;

   --  final
   DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC : constant Java.Byte;

   --  final
   DIRECTIONALITY_EUROPEAN_NUMBER : constant Java.Byte;

   --  final
   DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR : constant Java.Byte;

   --  final
   DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR : constant Java.Byte;

   --  final
   DIRECTIONALITY_ARABIC_NUMBER : constant Java.Byte;

   --  final
   DIRECTIONALITY_COMMON_NUMBER_SEPARATOR : constant Java.Byte;

   --  final
   DIRECTIONALITY_NONSPACING_MARK : constant Java.Byte;

   --  final
   DIRECTIONALITY_BOUNDARY_NEUTRAL : constant Java.Byte;

   --  final
   DIRECTIONALITY_PARAGRAPH_SEPARATOR : constant Java.Byte;

   --  final
   DIRECTIONALITY_SEGMENT_SEPARATOR : constant Java.Byte;

   --  final
   DIRECTIONALITY_WHITESPACE : constant Java.Byte;

   --  final
   DIRECTIONALITY_OTHER_NEUTRALS : constant Java.Byte;

   --  final
   DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING : constant Java.Byte;

   --  final
   DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE : constant Java.Byte;

   --  final
   DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING : constant Java.Byte;

   --  final
   DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE : constant Java.Byte;

   --  final
   DIRECTIONALITY_POP_DIRECTIONAL_FORMAT : constant Java.Byte;

   --  final
   MIN_HIGH_SURROGATE : constant Java.Char;

   --  final
   MAX_HIGH_SURROGATE : constant Java.Char;

   --  final
   MIN_LOW_SURROGATE : constant Java.Char;

   --  final
   MAX_LOW_SURROGATE : constant Java.Char;

   --  final
   MIN_SURROGATE : constant Java.Char;

   --  final
   MAX_SURROGATE : constant Java.Char;

   --  final
   MIN_SUPPLEMENTARY_CODE_POINT : constant Java.Int;

   --  final
   MIN_CODE_POINT : constant Java.Int;

   --  final
   MAX_CODE_POINT : constant Java.Int;

   --  final
   SIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Character);
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, CharValue, "charValue");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IsValidCodePoint, "isValidCodePoint");
   pragma Import (Java, IsSupplementaryCodePoint, "isSupplementaryCodePoint");
   pragma Import (Java, IsHighSurrogate, "isHighSurrogate");
   pragma Import (Java, IsLowSurrogate, "isLowSurrogate");
   pragma Import (Java, IsSurrogatePair, "isSurrogatePair");
   pragma Import (Java, CharCount, "charCount");
   pragma Import (Java, ToCodePoint, "toCodePoint");
   pragma Import (Java, CodePointAt, "codePointAt");
   pragma Import (Java, CodePointBefore, "codePointBefore");
   pragma Import (Java, ToChars, "toChars");
   pragma Import (Java, CodePointCount, "codePointCount");
   pragma Import (Java, OffsetByCodePoints, "offsetByCodePoints");
   pragma Import (Java, IsLowerCase, "isLowerCase");
   pragma Import (Java, IsUpperCase, "isUpperCase");
   pragma Import (Java, IsTitleCase, "isTitleCase");
   pragma Import (Java, IsDigit, "isDigit");
   pragma Import (Java, IsDefined, "isDefined");
   pragma Import (Java, IsLetter, "isLetter");
   pragma Import (Java, IsLetterOrDigit, "isLetterOrDigit");
   pragma Import (Java, IsJavaIdentifierStart, "isJavaIdentifierStart");
   pragma Import (Java, IsJavaIdentifierPart, "isJavaIdentifierPart");
   pragma Import (Java, IsUnicodeIdentifierStart, "isUnicodeIdentifierStart");
   pragma Import (Java, IsUnicodeIdentifierPart, "isUnicodeIdentifierPart");
   pragma Import (Java, IsIdentifierIgnorable, "isIdentifierIgnorable");
   pragma Import (Java, ToLowerCase, "toLowerCase");
   pragma Import (Java, ToUpperCase, "toUpperCase");
   pragma Import (Java, ToTitleCase, "toTitleCase");
   pragma Import (Java, Digit, "digit");
   pragma Import (Java, GetNumericValue, "getNumericValue");
   pragma Import (Java, IsSpaceChar, "isSpaceChar");
   pragma Import (Java, IsWhitespace, "isWhitespace");
   pragma Import (Java, IsISOControl, "isISOControl");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, ForDigit, "forDigit");
   pragma Import (Java, GetDirectionality, "getDirectionality");
   pragma Import (Java, IsMirrored, "isMirrored");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, ReverseBytes, "reverseBytes");
   pragma Import (Java, MIN_RADIX, "MIN_RADIX");
   pragma Import (Java, MAX_RADIX, "MAX_RADIX");
   pragma Import (Java, MIN_VALUE, "MIN_VALUE");
   pragma Import (Java, MAX_VALUE, "MAX_VALUE");
   pragma Import (Java, TYPE_K, "TYPE");
   pragma Import (Java, UNASSIGNED, "UNASSIGNED");
   pragma Import (Java, UPPERCASE_LETTER, "UPPERCASE_LETTER");
   pragma Import (Java, LOWERCASE_LETTER, "LOWERCASE_LETTER");
   pragma Import (Java, TITLECASE_LETTER, "TITLECASE_LETTER");
   pragma Import (Java, MODIFIER_LETTER, "MODIFIER_LETTER");
   pragma Import (Java, OTHER_LETTER, "OTHER_LETTER");
   pragma Import (Java, NON_SPACING_MARK, "NON_SPACING_MARK");
   pragma Import (Java, ENCLOSING_MARK, "ENCLOSING_MARK");
   pragma Import (Java, COMBINING_SPACING_MARK, "COMBINING_SPACING_MARK");
   pragma Import (Java, DECIMAL_DIGIT_NUMBER, "DECIMAL_DIGIT_NUMBER");
   pragma Import (Java, LETTER_NUMBER, "LETTER_NUMBER");
   pragma Import (Java, OTHER_NUMBER, "OTHER_NUMBER");
   pragma Import (Java, SPACE_SEPARATOR, "SPACE_SEPARATOR");
   pragma Import (Java, LINE_SEPARATOR, "LINE_SEPARATOR");
   pragma Import (Java, PARAGRAPH_SEPARATOR, "PARAGRAPH_SEPARATOR");
   pragma Import (Java, CONTROL, "CONTROL");
   pragma Import (Java, FORMAT, "FORMAT");
   pragma Import (Java, PRIVATE_USE, "PRIVATE_USE");
   pragma Import (Java, SURROGATE, "SURROGATE");
   pragma Import (Java, DASH_PUNCTUATION, "DASH_PUNCTUATION");
   pragma Import (Java, START_PUNCTUATION, "START_PUNCTUATION");
   pragma Import (Java, END_PUNCTUATION, "END_PUNCTUATION");
   pragma Import (Java, CONNECTOR_PUNCTUATION, "CONNECTOR_PUNCTUATION");
   pragma Import (Java, OTHER_PUNCTUATION, "OTHER_PUNCTUATION");
   pragma Import (Java, MATH_SYMBOL, "MATH_SYMBOL");
   pragma Import (Java, CURRENCY_SYMBOL, "CURRENCY_SYMBOL");
   pragma Import (Java, MODIFIER_SYMBOL, "MODIFIER_SYMBOL");
   pragma Import (Java, OTHER_SYMBOL, "OTHER_SYMBOL");
   pragma Import (Java, INITIAL_QUOTE_PUNCTUATION, "INITIAL_QUOTE_PUNCTUATION");
   pragma Import (Java, FINAL_QUOTE_PUNCTUATION, "FINAL_QUOTE_PUNCTUATION");
   pragma Import (Java, DIRECTIONALITY_UNDEFINED, "DIRECTIONALITY_UNDEFINED");
   pragma Import (Java, DIRECTIONALITY_LEFT_TO_RIGHT, "DIRECTIONALITY_LEFT_TO_RIGHT");
   pragma Import (Java, DIRECTIONALITY_RIGHT_TO_LEFT, "DIRECTIONALITY_RIGHT_TO_LEFT");
   pragma Import (Java, DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC, "DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC");
   pragma Import (Java, DIRECTIONALITY_EUROPEAN_NUMBER, "DIRECTIONALITY_EUROPEAN_NUMBER");
   pragma Import (Java, DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR, "DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR");
   pragma Import (Java, DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR, "DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR");
   pragma Import (Java, DIRECTIONALITY_ARABIC_NUMBER, "DIRECTIONALITY_ARABIC_NUMBER");
   pragma Import (Java, DIRECTIONALITY_COMMON_NUMBER_SEPARATOR, "DIRECTIONALITY_COMMON_NUMBER_SEPARATOR");
   pragma Import (Java, DIRECTIONALITY_NONSPACING_MARK, "DIRECTIONALITY_NONSPACING_MARK");
   pragma Import (Java, DIRECTIONALITY_BOUNDARY_NEUTRAL, "DIRECTIONALITY_BOUNDARY_NEUTRAL");
   pragma Import (Java, DIRECTIONALITY_PARAGRAPH_SEPARATOR, "DIRECTIONALITY_PARAGRAPH_SEPARATOR");
   pragma Import (Java, DIRECTIONALITY_SEGMENT_SEPARATOR, "DIRECTIONALITY_SEGMENT_SEPARATOR");
   pragma Import (Java, DIRECTIONALITY_WHITESPACE, "DIRECTIONALITY_WHITESPACE");
   pragma Import (Java, DIRECTIONALITY_OTHER_NEUTRALS, "DIRECTIONALITY_OTHER_NEUTRALS");
   pragma Import (Java, DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING, "DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING");
   pragma Import (Java, DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE, "DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE");
   pragma Import (Java, DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING, "DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING");
   pragma Import (Java, DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE, "DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE");
   pragma Import (Java, DIRECTIONALITY_POP_DIRECTIONAL_FORMAT, "DIRECTIONALITY_POP_DIRECTIONAL_FORMAT");
   pragma Import (Java, MIN_HIGH_SURROGATE, "MIN_HIGH_SURROGATE");
   pragma Import (Java, MAX_HIGH_SURROGATE, "MAX_HIGH_SURROGATE");
   pragma Import (Java, MIN_LOW_SURROGATE, "MIN_LOW_SURROGATE");
   pragma Import (Java, MAX_LOW_SURROGATE, "MAX_LOW_SURROGATE");
   pragma Import (Java, MIN_SURROGATE, "MIN_SURROGATE");
   pragma Import (Java, MAX_SURROGATE, "MAX_SURROGATE");
   pragma Import (Java, MIN_SUPPLEMENTARY_CODE_POINT, "MIN_SUPPLEMENTARY_CODE_POINT");
   pragma Import (Java, MIN_CODE_POINT, "MIN_CODE_POINT");
   pragma Import (Java, MAX_CODE_POINT, "MAX_CODE_POINT");
   pragma Import (Java, SIZE, "SIZE");

end Java.Lang.Character;
pragma Import (Java, Java.Lang.Character, "java.lang.Character");
pragma Extensions_Allowed (Off);
