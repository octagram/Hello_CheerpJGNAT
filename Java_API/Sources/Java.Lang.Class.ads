pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.Annotation.Annotation;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.Package_K;
limited with Java.Lang.Reflect.Constructor;
limited with Java.Lang.Reflect.Field;
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.Reflect.TypeVariable;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Security.ProtectionDomain;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.Reflect.AnnotatedElement;
with Java.Lang.Reflect.GenericDeclaration;
with Java.Lang.Reflect.Type_K;

package Java.Lang.Class is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AnnotatedElement_I : Java.Lang.Reflect.AnnotatedElement.Ref;
            GenericDeclaration_I : Java.Lang.Reflect.GenericDeclaration.Ref;
            Type_K_I : Java.Lang.Reflect.Type_K.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ForName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function ForName (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Boolean : Java.Boolean;
                     P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                     return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function NewInstance (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InstantiationException.Except and
   --  Java.Lang.IllegalAccessException.Except

   function IsInstance (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean;

   function IsAssignableFrom (This : access Typ;
                              P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                              return Java.Boolean;

   function IsInterface (This : access Typ)
                         return Java.Boolean;

   function IsArray (This : access Typ)
                     return Java.Boolean;

   function IsPrimitive (This : access Typ)
                         return Java.Boolean;

   function IsAnnotation (This : access Typ)
                          return Java.Boolean;

   function IsSynthetic (This : access Typ)
                         return Java.Boolean;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetClassLoader (This : access Typ)
                            return access Java.Lang.ClassLoader.Typ'Class;

   function GetTypeParameters (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetSuperclass (This : access Typ)
                           return access Java.Lang.Class.Typ'Class;

   function GetGenericSuperclass (This : access Typ)
                                  return access Java.Lang.Reflect.Type_K.Typ'Class;

   function GetPackage (This : access Typ)
                        return access Java.Lang.Package_K.Typ'Class;

   function GetInterfaces (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function GetGenericInterfaces (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref;

   function GetComponentType (This : access Typ)
                              return access Java.Lang.Class.Typ'Class;

   function GetModifiers (This : access Typ)
                          return Java.Int;

   function GetSigners (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function GetEnclosingMethod (This : access Typ)
                                return access Java.Lang.Reflect.Method.Typ'Class;

   function GetEnclosingConstructor (This : access Typ)
                                     return access Java.Lang.Reflect.Constructor.Typ'Class;

   function GetDeclaringClass (This : access Typ)
                               return access Java.Lang.Class.Typ'Class;

   function GetEnclosingClass (This : access Typ)
                               return access Java.Lang.Class.Typ'Class;

   function GetSimpleName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetCanonicalName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function IsAnonymousClass (This : access Typ)
                              return Java.Boolean;

   function IsLocalClass (This : access Typ)
                          return Java.Boolean;

   function IsMemberClass (This : access Typ)
                           return Java.Boolean;

   function GetClasses (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function GetFields (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Lang.SecurityException.Except

   function GetMethods (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Lang.SecurityException.Except

   function GetConstructors (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Lang.SecurityException.Except

   function GetField (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.Reflect.Field.Typ'Class;
   --  can raise Java.Lang.NoSuchFieldException.Except and
   --  Java.Lang.SecurityException.Except

   function GetMethod (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Class_Arr : access Java.Lang.Class.Arr_Obj)
                       return access Java.Lang.Reflect.Method.Typ'Class;
   --  can raise Java.Lang.NoSuchMethodException.Except and
   --  Java.Lang.SecurityException.Except

   function GetConstructor (This : access Typ;
                            P1_Class_Arr : access Java.Lang.Class.Arr_Obj)
                            return access Java.Lang.Reflect.Constructor.Typ'Class;
   --  can raise Java.Lang.NoSuchMethodException.Except and
   --  Java.Lang.SecurityException.Except

   function GetDeclaredClasses (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Lang.SecurityException.Except

   function GetDeclaredFields (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Lang.SecurityException.Except

   function GetDeclaredMethods (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Lang.SecurityException.Except

   function GetDeclaredConstructors (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Lang.SecurityException.Except

   function GetDeclaredField (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Reflect.Field.Typ'Class;
   --  can raise Java.Lang.NoSuchFieldException.Except and
   --  Java.Lang.SecurityException.Except

   function GetDeclaredMethod (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Class_Arr : access Java.Lang.Class.Arr_Obj)
                               return access Java.Lang.Reflect.Method.Typ'Class;
   --  can raise Java.Lang.NoSuchMethodException.Except and
   --  Java.Lang.SecurityException.Except

   function GetDeclaredConstructor (This : access Typ;
                                    P1_Class_Arr : access Java.Lang.Class.Arr_Obj)
                                    return access Java.Lang.Reflect.Constructor.Typ'Class;
   --  can raise Java.Lang.NoSuchMethodException.Except and
   --  Java.Lang.SecurityException.Except

   function GetResourceAsStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Io.InputStream.Typ'Class;

   function GetResource (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Net.URL.Typ'Class;

   function GetProtectionDomain (This : access Typ)
                                 return access Java.Security.ProtectionDomain.Typ'Class;

   function DesiredAssertionStatus (This : access Typ)
                                    return Java.Boolean;

   function IsEnum (This : access Typ)
                    return Java.Boolean;

   function GetEnumConstants (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function Cast (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;

   function AsSubclass (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Java.Lang.Class.Typ'Class;

   function GetAnnotation (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Annotation.Annotation.Typ'Class;

   function IsAnnotationPresent (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                 return Java.Boolean;

   function GetAnnotations (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetDeclaredAnnotations (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ForName, "forName");
   pragma Import (Java, NewInstance, "newInstance");
   pragma Import (Java, IsInstance, "isInstance");
   pragma Import (Java, IsAssignableFrom, "isAssignableFrom");
   pragma Import (Java, IsInterface, "isInterface");
   pragma Import (Java, IsArray, "isArray");
   pragma Import (Java, IsPrimitive, "isPrimitive");
   pragma Import (Java, IsAnnotation, "isAnnotation");
   pragma Import (Java, IsSynthetic, "isSynthetic");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetClassLoader, "getClassLoader");
   pragma Import (Java, GetTypeParameters, "getTypeParameters");
   pragma Import (Java, GetSuperclass, "getSuperclass");
   pragma Import (Java, GetGenericSuperclass, "getGenericSuperclass");
   pragma Import (Java, GetPackage, "getPackage");
   pragma Import (Java, GetInterfaces, "getInterfaces");
   pragma Import (Java, GetGenericInterfaces, "getGenericInterfaces");
   pragma Import (Java, GetComponentType, "getComponentType");
   pragma Import (Java, GetModifiers, "getModifiers");
   pragma Import (Java, GetSigners, "getSigners");
   pragma Import (Java, GetEnclosingMethod, "getEnclosingMethod");
   pragma Import (Java, GetEnclosingConstructor, "getEnclosingConstructor");
   pragma Import (Java, GetDeclaringClass, "getDeclaringClass");
   pragma Import (Java, GetEnclosingClass, "getEnclosingClass");
   pragma Import (Java, GetSimpleName, "getSimpleName");
   pragma Import (Java, GetCanonicalName, "getCanonicalName");
   pragma Import (Java, IsAnonymousClass, "isAnonymousClass");
   pragma Import (Java, IsLocalClass, "isLocalClass");
   pragma Import (Java, IsMemberClass, "isMemberClass");
   pragma Import (Java, GetClasses, "getClasses");
   pragma Import (Java, GetFields, "getFields");
   pragma Import (Java, GetMethods, "getMethods");
   pragma Import (Java, GetConstructors, "getConstructors");
   pragma Import (Java, GetField, "getField");
   pragma Import (Java, GetMethod, "getMethod");
   pragma Import (Java, GetConstructor, "getConstructor");
   pragma Import (Java, GetDeclaredClasses, "getDeclaredClasses");
   pragma Import (Java, GetDeclaredFields, "getDeclaredFields");
   pragma Import (Java, GetDeclaredMethods, "getDeclaredMethods");
   pragma Import (Java, GetDeclaredConstructors, "getDeclaredConstructors");
   pragma Import (Java, GetDeclaredField, "getDeclaredField");
   pragma Import (Java, GetDeclaredMethod, "getDeclaredMethod");
   pragma Import (Java, GetDeclaredConstructor, "getDeclaredConstructor");
   pragma Import (Java, GetResourceAsStream, "getResourceAsStream");
   pragma Import (Java, GetResource, "getResource");
   pragma Import (Java, GetProtectionDomain, "getProtectionDomain");
   pragma Import (Java, DesiredAssertionStatus, "desiredAssertionStatus");
   pragma Import (Java, IsEnum, "isEnum");
   pragma Import (Java, GetEnumConstants, "getEnumConstants");
   pragma Import (Java, Cast, "cast");
   pragma Import (Java, AsSubclass, "asSubclass");
   pragma Import (Java, GetAnnotation, "getAnnotation");
   pragma Import (Java, IsAnnotationPresent, "isAnnotationPresent");
   pragma Import (Java, GetAnnotations, "getAnnotations");
   pragma Import (Java, GetDeclaredAnnotations, "getDeclaredAnnotations");

end Java.Lang.Class;
pragma Import (Java, Java.Lang.Class, "java.lang.Class");
pragma Extensions_Allowed (Off);
