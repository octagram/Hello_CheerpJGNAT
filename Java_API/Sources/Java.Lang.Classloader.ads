pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.Class;
limited with Java.Lang.Package_K;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Nio.ByteBuffer;
limited with Java.Security.ProtectionDomain;
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Java.Lang.ClassLoader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_ClassLoader (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   --  protected
   function New_ClassLoader (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function LoadClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   --  protected  synchronized
   function LoadClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Boolean : Java.Boolean)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   --  protected
   function FindClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   --  final  protected
   function DefineClass (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Byte_Arr : Java.Byte_Arr;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int)
                         return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassFormatError.Except

   --  final  protected
   function DefineClass (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Byte_Arr : Java.Byte_Arr;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_ProtectionDomain : access Standard.Java.Security.ProtectionDomain.Typ'Class)
                         return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassFormatError.Except

   --  final  protected
   function DefineClass (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class;
                         P3_ProtectionDomain : access Standard.Java.Security.ProtectionDomain.Typ'Class)
                         return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassFormatError.Except

   --  final  protected
   procedure ResolveClass (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class);

   --  final  protected
   function FindSystemClass (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   --  final  protected
   function FindLoadedClass (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.Class.Typ'Class;

   --  final  protected
   procedure SetSigners (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function GetResource (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Net.URL.Typ'Class;

   function GetResources (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Util.Enumeration.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   function FindResource (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Net.URL.Typ'Class;

   --  protected
   function FindResources (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Util.Enumeration.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetSystemResource (P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Net.URL.Typ'Class;

   function GetSystemResources (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Util.Enumeration.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetResourceAsStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Io.InputStream.Typ'Class;

   function GetSystemResourceAsStream (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                       return access Java.Io.InputStream.Typ'Class;

   --  final
   function GetParent (This : access Typ)
                       return access Java.Lang.ClassLoader.Typ'Class;

   function GetSystemClassLoader return access Java.Lang.ClassLoader.Typ'Class;

   --  protected
   function DefinePackage (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                           P6_String : access Standard.Java.Lang.String.Typ'Class;
                           P7_String : access Standard.Java.Lang.String.Typ'Class;
                           P8_URL : access Standard.Java.Net.URL.Typ'Class)
                           return access Java.Lang.Package_K.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  protected
   function GetPackage (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Package_K.Typ'Class;

   --  protected
   function GetPackages (This : access Typ)
                         return Standard.Java.Lang.Object.Ref;

   --  protected
   function FindLibrary (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure SetDefaultAssertionStatus (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   --  synchronized
   procedure SetPackageAssertionStatus (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_Boolean : Java.Boolean);

   --  synchronized
   procedure SetClassAssertionStatus (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Boolean : Java.Boolean);

   --  synchronized
   procedure ClearAssertionStatus (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ClassLoader);
   pragma Import (Java, LoadClass, "loadClass");
   pragma Import (Java, FindClass, "findClass");
   pragma Import (Java, DefineClass, "defineClass");
   pragma Import (Java, ResolveClass, "resolveClass");
   pragma Import (Java, FindSystemClass, "findSystemClass");
   pragma Import (Java, FindLoadedClass, "findLoadedClass");
   pragma Import (Java, SetSigners, "setSigners");
   pragma Import (Java, GetResource, "getResource");
   pragma Import (Java, GetResources, "getResources");
   pragma Import (Java, FindResource, "findResource");
   pragma Import (Java, FindResources, "findResources");
   pragma Import (Java, GetSystemResource, "getSystemResource");
   pragma Import (Java, GetSystemResources, "getSystemResources");
   pragma Import (Java, GetResourceAsStream, "getResourceAsStream");
   pragma Import (Java, GetSystemResourceAsStream, "getSystemResourceAsStream");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetSystemClassLoader, "getSystemClassLoader");
   pragma Import (Java, DefinePackage, "definePackage");
   pragma Import (Java, GetPackage, "getPackage");
   pragma Import (Java, GetPackages, "getPackages");
   pragma Import (Java, FindLibrary, "findLibrary");
   pragma Import (Java, SetDefaultAssertionStatus, "setDefaultAssertionStatus");
   pragma Import (Java, SetPackageAssertionStatus, "setPackageAssertionStatus");
   pragma Import (Java, SetClassAssertionStatus, "setClassAssertionStatus");
   pragma Import (Java, ClearAssertionStatus, "clearAssertionStatus");

end Java.Lang.ClassLoader;
pragma Import (Java, Java.Lang.ClassLoader, "java.lang.ClassLoader");
pragma Extensions_Allowed (Off);
