pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Java.Lang.ClassNotFoundException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ClassNotFoundException (This : Ref := null)
                                        return Ref;

   function New_ClassNotFoundException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   function New_ClassNotFoundException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetException (This : access Typ)
                          return access Java.Lang.Throwable.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.lang.ClassNotFoundException");
   pragma Java_Constructor (New_ClassNotFoundException);
   pragma Import (Java, GetException, "getException");
   pragma Import (Java, GetCause, "getCause");

end Java.Lang.ClassNotFoundException;
pragma Import (Java, Java.Lang.ClassNotFoundException, "java.lang.ClassNotFoundException");
pragma Extensions_Allowed (Off);
