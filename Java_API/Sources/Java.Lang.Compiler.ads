pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Lang.Compiler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CompileClass (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Java.Boolean;

   function CompileClasses (P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Boolean;

   function Command (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;

   procedure Enable ;

   procedure Disable ;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CompileClass, "compileClass");
   pragma Import (Java, CompileClasses, "compileClasses");
   pragma Import (Java, Command, "command");
   pragma Import (Java, Enable, "enable");
   pragma Import (Java, Disable, "disable");

end Java.Lang.Compiler;
pragma Import (Java, Java.Lang.Compiler, "java.lang.Compiler");
pragma Extensions_Allowed (Off);
