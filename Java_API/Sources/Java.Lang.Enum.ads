pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Lang.Enum is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class;

   --  final
   function Ordinal (This : access Typ)
                     return Java.Int;

   --  protected
   function New_Enum (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int; 
                      This : Ref := null)
                      return Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   --  final
   function CompareTo (This : access Typ;
                       P1_Enum : access Standard.Java.Lang.Enum.Typ'Class)
                       return Java.Int;

   --  final
   function GetDeclaringClass (This : access Typ)
                               return access Java.Lang.Class.Typ'Class;

   function ValueOf (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Enum.Typ'Class;

   --  final  protected
   procedure Finalize (This : access Typ);

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Name, "name");
   pragma Import (Java, Ordinal, "ordinal");
   pragma Java_Constructor (New_Enum);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, GetDeclaringClass, "getDeclaringClass");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, Finalize, "finalize");

end Java.Lang.Enum;
pragma Import (Java, Java.Lang.Enum, "java.lang.Enum");
pragma Extensions_Allowed (Off);
