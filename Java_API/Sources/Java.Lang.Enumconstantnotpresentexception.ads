pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Java.Lang.EnumConstantNotPresentException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EnumConstantNotPresentException (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                                 P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                                 This : Ref := null)
                                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function EnumType (This : access Typ)
                      return access Java.Lang.Class.Typ'Class;

   function ConstantName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.lang.EnumConstantNotPresentException");
   pragma Java_Constructor (New_EnumConstantNotPresentException);
   pragma Import (Java, EnumType, "enumType");
   pragma Import (Java, ConstantName, "constantName");

end Java.Lang.EnumConstantNotPresentException;
pragma Import (Java, Java.Lang.EnumConstantNotPresentException, "java.lang.EnumConstantNotPresentException");
pragma Extensions_Allowed (Off);
