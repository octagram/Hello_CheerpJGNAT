pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Number;
with Java.Lang.Object;

package Java.Lang.Float is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Number.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (P1_Float : Java.Float)
                      return access Java.Lang.String.Typ'Class;

   function ToHexString (P1_Float : Java.Float)
                         return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Float.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_Float : Java.Float)
                     return access Java.Lang.Float.Typ'Class;

   function ParseFloat (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Float;
   --  can raise Java.Lang.NumberFormatException.Except

   function IsNaN (P1_Float : Java.Float)
                   return Java.Boolean;

   function IsInfinite (P1_Float : Java.Float)
                        return Java.Boolean;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Float (P1_Float : Java.Float; 
                       This : Ref := null)
                       return Ref;

   function New_Float (P1_Double : Java.Double; 
                       This : Ref := null)
                       return Ref;

   function New_Float (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;
   --  can raise Java.Lang.NumberFormatException.Except

   function IsNaN (This : access Typ)
                   return Java.Boolean;

   function IsInfinite (This : access Typ)
                        return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ByteValue (This : access Typ)
                       return Java.Byte;

   function ShortValue (This : access Typ)
                        return Java.Short;

   function IntValue (This : access Typ)
                      return Java.Int;

   function LongValue (This : access Typ)
                       return Java.Long;

   function FloatValue (This : access Typ)
                        return Java.Float;

   function DoubleValue (This : access Typ)
                         return Java.Double;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function FloatToIntBits (P1_Float : Java.Float)
                            return Java.Int;

   function FloatToRawIntBits (P1_Float : Java.Float)
                               return Java.Int;

   function IntBitsToFloat (P1_Int : Java.Int)
                            return Java.Float;

   function CompareTo (This : access Typ;
                       P1_Float : access Standard.Java.Lang.Float.Typ'Class)
                       return Java.Int;

   function Compare (P1_Float : Java.Float;
                     P2_Float : Java.Float)
                     return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   POSITIVE_INFINITY : constant Java.Float;

   --  final
   NEGATIVE_INFINITY : constant Java.Float;

   --  final
   NaN : constant Java.Float;

   --  final
   MAX_VALUE : constant Java.Float;

   --  final
   MIN_NORMAL : constant Java.Float;

   --  final
   MIN_VALUE : constant Java.Float;

   --  final
   MAX_EXPONENT : constant Java.Int;

   --  final
   MIN_EXPONENT : constant Java.Int;

   --  final
   SIZE : constant Java.Int;

   --  final
   TYPE_K : access Java.Lang.Class.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToHexString, "toHexString");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, ParseFloat, "parseFloat");
   pragma Import (Java, IsNaN, "isNaN");
   pragma Import (Java, IsInfinite, "isInfinite");
   pragma Java_Constructor (New_Float);
   pragma Import (Java, ByteValue, "byteValue");
   pragma Import (Java, ShortValue, "shortValue");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, LongValue, "longValue");
   pragma Import (Java, FloatValue, "floatValue");
   pragma Import (Java, DoubleValue, "doubleValue");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, FloatToIntBits, "floatToIntBits");
   pragma Import (Java, FloatToRawIntBits, "floatToRawIntBits");
   pragma Import (Java, IntBitsToFloat, "intBitsToFloat");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, Compare, "compare");
   pragma Import (Java, POSITIVE_INFINITY, "POSITIVE_INFINITY");
   pragma Import (Java, NEGATIVE_INFINITY, "NEGATIVE_INFINITY");
   pragma Import (Java, NaN, "NaN");
   pragma Import (Java, MAX_VALUE, "MAX_VALUE");
   pragma Import (Java, MIN_NORMAL, "MIN_NORMAL");
   pragma Import (Java, MIN_VALUE, "MIN_VALUE");
   pragma Import (Java, MAX_EXPONENT, "MAX_EXPONENT");
   pragma Import (Java, MIN_EXPONENT, "MIN_EXPONENT");
   pragma Import (Java, SIZE, "SIZE");
   pragma Import (Java, TYPE_K, "TYPE");

end Java.Lang.Float;
pragma Import (Java, Java.Lang.Float, "java.lang.Float");
pragma Extensions_Allowed (Off);
