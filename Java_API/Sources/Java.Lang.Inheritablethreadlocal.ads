pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Lang.ThreadLocal;

package Java.Lang.InheritableThreadLocal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.ThreadLocal.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InheritableThreadLocal (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function ChildValue (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InheritableThreadLocal);
   pragma Import (Java, ChildValue, "childValue");

end Java.Lang.InheritableThreadLocal;
pragma Import (Java, Java.Lang.InheritableThreadLocal, "java.lang.InheritableThreadLocal");
pragma Extensions_Allowed (Off);
