pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
with Java.Lang.Object;

package Java.Lang.Instrument.ClassDefinition is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ClassDefinition (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                 P2_Byte_Arr : Java.Byte_Arr; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefinitionClass (This : access Typ)
                                return access Java.Lang.Class.Typ'Class;

   function GetDefinitionClassFile (This : access Typ)
                                    return Java.Byte_Arr;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ClassDefinition);
   pragma Import (Java, GetDefinitionClass, "getDefinitionClass");
   pragma Import (Java, GetDefinitionClassFile, "getDefinitionClassFile");

end Java.Lang.Instrument.ClassDefinition;
pragma Import (Java, Java.Lang.Instrument.ClassDefinition, "java.lang.instrument.ClassDefinition");
pragma Extensions_Allowed (Off);
