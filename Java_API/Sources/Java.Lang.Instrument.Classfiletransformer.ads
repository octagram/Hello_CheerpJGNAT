pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Security.ProtectionDomain;
with Java.Lang.Object;

package Java.Lang.Instrument.ClassFileTransformer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Transform (This : access Typ;
                       P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                       P4_ProtectionDomain : access Standard.Java.Security.ProtectionDomain.Typ'Class;
                       P5_Byte_Arr : Java.Byte_Arr)
                       return Java.Byte_Arr is abstract;
   --  can raise Java.Lang.Instrument.IllegalClassFormatException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Transform, "transform");

end Java.Lang.Instrument.ClassFileTransformer;
pragma Import (Java, Java.Lang.Instrument.ClassFileTransformer, "java.lang.instrument.ClassFileTransformer");
pragma Extensions_Allowed (Off);
