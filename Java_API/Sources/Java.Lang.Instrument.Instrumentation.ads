pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.Instrument.ClassDefinition;
limited with Java.Lang.Instrument.ClassFileTransformer;
limited with Java.Lang.String;
limited with Java.Util.Jar.JarFile;
with Java.Lang.Object;

package Java.Lang.Instrument.Instrumentation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddTransformer (This : access Typ;
                             P1_ClassFileTransformer : access Standard.Java.Lang.Instrument.ClassFileTransformer.Typ'Class;
                             P2_Boolean : Java.Boolean) is abstract;

   procedure AddTransformer (This : access Typ;
                             P1_ClassFileTransformer : access Standard.Java.Lang.Instrument.ClassFileTransformer.Typ'Class) is abstract;

   function RemoveTransformer (This : access Typ;
                               P1_ClassFileTransformer : access Standard.Java.Lang.Instrument.ClassFileTransformer.Typ'Class)
                               return Java.Boolean is abstract;

   function IsRetransformClassesSupported (This : access Typ)
                                           return Java.Boolean is abstract;

   procedure RetransformClasses (This : access Typ;
                                 P1_Class_Arr : access Java.Lang.Class.Arr_Obj) is abstract;
   --  can raise Java.Lang.Instrument.UnmodifiableClassException.Except

   function IsRedefineClassesSupported (This : access Typ)
                                        return Java.Boolean is abstract;

   procedure RedefineClasses (This : access Typ;
                              P1_ClassDefinition_Arr : access Java.Lang.Instrument.ClassDefinition.Arr_Obj) is abstract;
   --  can raise Java.Lang.ClassNotFoundException.Except and
   --  Java.Lang.Instrument.UnmodifiableClassException.Except

   function IsModifiableClass (This : access Typ;
                               P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                               return Java.Boolean is abstract;

   function GetAllLoadedClasses (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref is abstract;

   function GetInitiatedClasses (This : access Typ;
                                 P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                                 return Standard.Java.Lang.Object.Ref is abstract;

   function GetObjectSize (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Long is abstract;

   procedure AppendToBootstrapClassLoaderSearch (This : access Typ;
                                                 P1_JarFile : access Standard.Java.Util.Jar.JarFile.Typ'Class) is abstract;

   procedure AppendToSystemClassLoaderSearch (This : access Typ;
                                              P1_JarFile : access Standard.Java.Util.Jar.JarFile.Typ'Class) is abstract;

   function IsNativeMethodPrefixSupported (This : access Typ)
                                           return Java.Boolean is abstract;

   procedure SetNativeMethodPrefix (This : access Typ;
                                    P1_ClassFileTransformer : access Standard.Java.Lang.Instrument.ClassFileTransformer.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddTransformer, "addTransformer");
   pragma Export (Java, RemoveTransformer, "removeTransformer");
   pragma Export (Java, IsRetransformClassesSupported, "isRetransformClassesSupported");
   pragma Export (Java, RetransformClasses, "retransformClasses");
   pragma Export (Java, IsRedefineClassesSupported, "isRedefineClassesSupported");
   pragma Export (Java, RedefineClasses, "redefineClasses");
   pragma Export (Java, IsModifiableClass, "isModifiableClass");
   pragma Export (Java, GetAllLoadedClasses, "getAllLoadedClasses");
   pragma Export (Java, GetInitiatedClasses, "getInitiatedClasses");
   pragma Export (Java, GetObjectSize, "getObjectSize");
   pragma Export (Java, AppendToBootstrapClassLoaderSearch, "appendToBootstrapClassLoaderSearch");
   pragma Export (Java, AppendToSystemClassLoaderSearch, "appendToSystemClassLoaderSearch");
   pragma Export (Java, IsNativeMethodPrefixSupported, "isNativeMethodPrefixSupported");
   pragma Export (Java, SetNativeMethodPrefix, "setNativeMethodPrefix");

end Java.Lang.Instrument.Instrumentation;
pragma Import (Java, Java.Lang.Instrument.Instrumentation, "java.lang.instrument.Instrumentation");
pragma Extensions_Allowed (Off);
