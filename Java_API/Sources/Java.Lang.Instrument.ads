pragma Extensions_Allowed (On);
package Java.Lang.Instrument is
   pragma Preelaborate;
end Java.Lang.Instrument;
pragma Import (Java, Java.Lang.Instrument, "java.lang.instrument");
pragma Extensions_Allowed (Off);
