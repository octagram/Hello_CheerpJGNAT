pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Number;
with Java.Lang.Object;

package Java.Lang.Integer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Number.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function ToHexString (P1_Int : Java.Int)
                         return access Java.Lang.String.Typ'Class;

   function ToOctalString (P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;

   function ToBinaryString (P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   function ToString (P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function ParseInt (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int)
                      return Java.Int;
   --  can raise Java.Lang.NumberFormatException.Except

   function ParseInt (P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Int;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_Int : Java.Int)
                     return access Java.Lang.Integer.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Integer (P1_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   function New_Integer (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Lang.NumberFormatException.Except

   function ByteValue (This : access Typ)
                       return Java.Byte;

   function ShortValue (This : access Typ)
                        return Java.Short;

   function IntValue (This : access Typ)
                      return Java.Int;

   function LongValue (This : access Typ)
                       return Java.Long;

   function FloatValue (This : access Typ)
                        return Java.Float;

   function DoubleValue (This : access Typ)
                         return Java.Double;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function GetInteger (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Integer.Typ'Class;

   function GetInteger (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int)
                        return access Java.Lang.Integer.Typ'Class;

   function GetInteger (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                        return access Java.Lang.Integer.Typ'Class;

   function Decode (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function CompareTo (This : access Typ;
                       P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                       return Java.Int;

   function HighestOneBit (P1_Int : Java.Int)
                           return Java.Int;

   function LowestOneBit (P1_Int : Java.Int)
                          return Java.Int;

   function NumberOfLeadingZeros (P1_Int : Java.Int)
                                  return Java.Int;

   function NumberOfTrailingZeros (P1_Int : Java.Int)
                                   return Java.Int;

   function BitCount (P1_Int : Java.Int)
                      return Java.Int;

   function RotateLeft (P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return Java.Int;

   function RotateRight (P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return Java.Int;

   function reverse_K (P1_Int : Java.Int)
                       return Java.Int;

   function Signum (P1_Int : Java.Int)
                    return Java.Int;

   function ReverseBytes (P1_Int : Java.Int)
                          return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MIN_VALUE : constant Java.Int;

   --  final
   MAX_VALUE : constant Java.Int;

   --  final
   TYPE_K : access Java.Lang.Class.Typ'Class;

   --  final
   SIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToHexString, "toHexString");
   pragma Import (Java, ToOctalString, "toOctalString");
   pragma Import (Java, ToBinaryString, "toBinaryString");
   pragma Import (Java, ParseInt, "parseInt");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Java_Constructor (New_Integer);
   pragma Import (Java, ByteValue, "byteValue");
   pragma Import (Java, ShortValue, "shortValue");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, LongValue, "longValue");
   pragma Import (Java, FloatValue, "floatValue");
   pragma Import (Java, DoubleValue, "doubleValue");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetInteger, "getInteger");
   pragma Import (Java, Decode, "decode");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, HighestOneBit, "highestOneBit");
   pragma Import (Java, LowestOneBit, "lowestOneBit");
   pragma Import (Java, NumberOfLeadingZeros, "numberOfLeadingZeros");
   pragma Import (Java, NumberOfTrailingZeros, "numberOfTrailingZeros");
   pragma Import (Java, BitCount, "bitCount");
   pragma Import (Java, RotateLeft, "rotateLeft");
   pragma Import (Java, RotateRight, "rotateRight");
   pragma Import (Java, reverse_K, "reverse");
   pragma Import (Java, Signum, "signum");
   pragma Import (Java, ReverseBytes, "reverseBytes");
   pragma Import (Java, MIN_VALUE, "MIN_VALUE");
   pragma Import (Java, MAX_VALUE, "MAX_VALUE");
   pragma Import (Java, TYPE_K, "TYPE");
   pragma Import (Java, SIZE, "SIZE");

end Java.Lang.Integer;
pragma Import (Java, Java.Lang.Integer, "java.lang.Integer");
pragma Extensions_Allowed (Off);
