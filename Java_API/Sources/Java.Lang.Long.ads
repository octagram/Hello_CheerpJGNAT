pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Number;
with Java.Lang.Object;

package Java.Lang.Long is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Number.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (P1_Long : Java.Long;
                      P2_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function ToHexString (P1_Long : Java.Long)
                         return access Java.Lang.String.Typ'Class;

   function ToOctalString (P1_Long : Java.Long)
                           return access Java.Lang.String.Typ'Class;

   function ToBinaryString (P1_Long : Java.Long)
                            return access Java.Lang.String.Typ'Class;

   function ToString (P1_Long : Java.Long)
                      return access Java.Lang.String.Typ'Class;

   function ParseLong (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int)
                       return Java.Long;
   --  can raise Java.Lang.NumberFormatException.Except

   function ParseLong (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Long;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return access Java.Lang.Long.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Long.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   function ValueOf (P1_Long : Java.Long)
                     return access Java.Lang.Long.Typ'Class;

   function Decode (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Long.Typ'Class;
   --  can raise Java.Lang.NumberFormatException.Except

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Long (P1_Long : Java.Long; 
                      This : Ref := null)
                      return Ref;

   function New_Long (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;
   --  can raise Java.Lang.NumberFormatException.Except

   function ByteValue (This : access Typ)
                       return Java.Byte;

   function ShortValue (This : access Typ)
                        return Java.Short;

   function IntValue (This : access Typ)
                      return Java.Int;

   function LongValue (This : access Typ)
                       return Java.Long;

   function FloatValue (This : access Typ)
                        return Java.Float;

   function DoubleValue (This : access Typ)
                         return Java.Double;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function GetLong (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.Long.Typ'Class;

   function GetLong (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Long : Java.Long)
                     return access Java.Lang.Long.Typ'Class;

   function GetLong (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Long : access Standard.Java.Lang.Long.Typ'Class)
                     return access Java.Lang.Long.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Long : access Standard.Java.Lang.Long.Typ'Class)
                       return Java.Int;

   function HighestOneBit (P1_Long : Java.Long)
                           return Java.Long;

   function LowestOneBit (P1_Long : Java.Long)
                          return Java.Long;

   function NumberOfLeadingZeros (P1_Long : Java.Long)
                                  return Java.Int;

   function NumberOfTrailingZeros (P1_Long : Java.Long)
                                   return Java.Int;

   function BitCount (P1_Long : Java.Long)
                      return Java.Int;

   function RotateLeft (P1_Long : Java.Long;
                        P2_Int : Java.Int)
                        return Java.Long;

   function RotateRight (P1_Long : Java.Long;
                         P2_Int : Java.Int)
                         return Java.Long;

   function reverse_K (P1_Long : Java.Long)
                       return Java.Long;

   function Signum (P1_Long : Java.Long)
                    return Java.Int;

   function ReverseBytes (P1_Long : Java.Long)
                          return Java.Long;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MIN_VALUE : constant Java.Long;

   --  final
   MAX_VALUE : constant Java.Long;

   --  final
   TYPE_K : access Java.Lang.Class.Typ'Class;

   --  final
   SIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToHexString, "toHexString");
   pragma Import (Java, ToOctalString, "toOctalString");
   pragma Import (Java, ToBinaryString, "toBinaryString");
   pragma Import (Java, ParseLong, "parseLong");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, Decode, "decode");
   pragma Java_Constructor (New_Long);
   pragma Import (Java, ByteValue, "byteValue");
   pragma Import (Java, ShortValue, "shortValue");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, LongValue, "longValue");
   pragma Import (Java, FloatValue, "floatValue");
   pragma Import (Java, DoubleValue, "doubleValue");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetLong, "getLong");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, HighestOneBit, "highestOneBit");
   pragma Import (Java, LowestOneBit, "lowestOneBit");
   pragma Import (Java, NumberOfLeadingZeros, "numberOfLeadingZeros");
   pragma Import (Java, NumberOfTrailingZeros, "numberOfTrailingZeros");
   pragma Import (Java, BitCount, "bitCount");
   pragma Import (Java, RotateLeft, "rotateLeft");
   pragma Import (Java, RotateRight, "rotateRight");
   pragma Import (Java, reverse_K, "reverse");
   pragma Import (Java, Signum, "signum");
   pragma Import (Java, ReverseBytes, "reverseBytes");
   pragma Import (Java, MIN_VALUE, "MIN_VALUE");
   pragma Import (Java, MAX_VALUE, "MAX_VALUE");
   pragma Import (Java, TYPE_K, "TYPE");
   pragma Import (Java, SIZE, "SIZE");

end Java.Lang.Long;
pragma Import (Java, Java.Lang.Long, "java.lang.Long");
pragma Extensions_Allowed (Off);
