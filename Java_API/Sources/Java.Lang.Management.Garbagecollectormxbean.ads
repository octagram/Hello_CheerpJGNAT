pragma Extensions_Allowed (On);
with Java.Lang.Management.MemoryManagerMXBean;
with Java.Lang.Object;

package Java.Lang.Management.GarbageCollectorMXBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MemoryManagerMXBean_I : Java.Lang.Management.MemoryManagerMXBean.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCollectionCount (This : access Typ)
                                return Java.Long is abstract;

   function GetCollectionTime (This : access Typ)
                               return Java.Long is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCollectionCount, "getCollectionCount");
   pragma Export (Java, GetCollectionTime, "getCollectionTime");

end Java.Lang.Management.GarbageCollectorMXBean;
pragma Import (Java, Java.Lang.Management.GarbageCollectorMXBean, "java.lang.management.GarbageCollectorMXBean");
pragma Extensions_Allowed (Off);
