pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.Management.ClassLoadingMXBean;
limited with Java.Lang.Management.CompilationMXBean;
limited with Java.Lang.Management.MemoryMXBean;
limited with Java.Lang.Management.OperatingSystemMXBean;
limited with Java.Lang.Management.RuntimeMXBean;
limited with Java.Lang.Management.ThreadMXBean;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.MBeanServerConnection;
with Java.Lang.Object;

package Java.Lang.Management.ManagementFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassLoadingMXBean return access Java.Lang.Management.ClassLoadingMXBean.Typ'Class;

   function GetMemoryMXBean return access Java.Lang.Management.MemoryMXBean.Typ'Class;

   function GetThreadMXBean return access Java.Lang.Management.ThreadMXBean.Typ'Class;

   function GetRuntimeMXBean return access Java.Lang.Management.RuntimeMXBean.Typ'Class;

   function GetCompilationMXBean return access Java.Lang.Management.CompilationMXBean.Typ'Class;

   function GetOperatingSystemMXBean return access Java.Lang.Management.OperatingSystemMXBean.Typ'Class;

   function GetMemoryPoolMXBeans return access Java.Util.List.Typ'Class;

   function GetMemoryManagerMXBeans return access Java.Util.List.Typ'Class;

   function GetGarbageCollectorMXBeans return access Java.Util.List.Typ'Class;

   --  synchronized
   function GetPlatformMBeanServer return access Javax.Management.MBeanServer.Typ'Class;

   function NewPlatformMXBeanProxy (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CLASS_LOADING_MXBEAN_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   COMPILATION_MXBEAN_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   MEMORY_MXBEAN_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   OPERATING_SYSTEM_MXBEAN_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   RUNTIME_MXBEAN_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   THREAD_MXBEAN_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   GARBAGE_COLLECTOR_MXBEAN_DOMAIN_TYPE : constant access Java.Lang.String.Typ'Class;

   --  final
   MEMORY_MANAGER_MXBEAN_DOMAIN_TYPE : constant access Java.Lang.String.Typ'Class;

   --  final
   MEMORY_POOL_MXBEAN_DOMAIN_TYPE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetClassLoadingMXBean, "getClassLoadingMXBean");
   pragma Import (Java, GetMemoryMXBean, "getMemoryMXBean");
   pragma Import (Java, GetThreadMXBean, "getThreadMXBean");
   pragma Import (Java, GetRuntimeMXBean, "getRuntimeMXBean");
   pragma Import (Java, GetCompilationMXBean, "getCompilationMXBean");
   pragma Import (Java, GetOperatingSystemMXBean, "getOperatingSystemMXBean");
   pragma Import (Java, GetMemoryPoolMXBeans, "getMemoryPoolMXBeans");
   pragma Import (Java, GetMemoryManagerMXBeans, "getMemoryManagerMXBeans");
   pragma Import (Java, GetGarbageCollectorMXBeans, "getGarbageCollectorMXBeans");
   pragma Import (Java, GetPlatformMBeanServer, "getPlatformMBeanServer");
   pragma Import (Java, NewPlatformMXBeanProxy, "newPlatformMXBeanProxy");
   pragma Import (Java, CLASS_LOADING_MXBEAN_NAME, "CLASS_LOADING_MXBEAN_NAME");
   pragma Import (Java, COMPILATION_MXBEAN_NAME, "COMPILATION_MXBEAN_NAME");
   pragma Import (Java, MEMORY_MXBEAN_NAME, "MEMORY_MXBEAN_NAME");
   pragma Import (Java, OPERATING_SYSTEM_MXBEAN_NAME, "OPERATING_SYSTEM_MXBEAN_NAME");
   pragma Import (Java, RUNTIME_MXBEAN_NAME, "RUNTIME_MXBEAN_NAME");
   pragma Import (Java, THREAD_MXBEAN_NAME, "THREAD_MXBEAN_NAME");
   pragma Import (Java, GARBAGE_COLLECTOR_MXBEAN_DOMAIN_TYPE, "GARBAGE_COLLECTOR_MXBEAN_DOMAIN_TYPE");
   pragma Import (Java, MEMORY_MANAGER_MXBEAN_DOMAIN_TYPE, "MEMORY_MANAGER_MXBEAN_DOMAIN_TYPE");
   pragma Import (Java, MEMORY_POOL_MXBEAN_DOMAIN_TYPE, "MEMORY_POOL_MXBEAN_DOMAIN_TYPE");

end Java.Lang.Management.ManagementFactory;
pragma Import (Java, Java.Lang.Management.ManagementFactory, "java.lang.management.ManagementFactory");
pragma Extensions_Allowed (Off);
