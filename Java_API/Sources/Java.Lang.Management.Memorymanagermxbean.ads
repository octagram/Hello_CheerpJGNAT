pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Lang.Management.MemoryManagerMXBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;

   function GetMemoryPoolNames (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, GetMemoryPoolNames, "getMemoryPoolNames");

end Java.Lang.Management.MemoryManagerMXBean;
pragma Import (Java, Java.Lang.Management.MemoryManagerMXBean, "java.lang.management.MemoryManagerMXBean");
pragma Extensions_Allowed (Off);
