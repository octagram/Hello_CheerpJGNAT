pragma Extensions_Allowed (On);
limited with Java.Lang.Management.MemoryUsage;
with Java.Lang.Object;

package Java.Lang.Management.MemoryMXBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetObjectPendingFinalizationCount (This : access Typ)
                                               return Java.Int is abstract;

   function GetHeapMemoryUsage (This : access Typ)
                                return access Java.Lang.Management.MemoryUsage.Typ'Class is abstract;

   function GetNonHeapMemoryUsage (This : access Typ)
                                   return access Java.Lang.Management.MemoryUsage.Typ'Class is abstract;

   function IsVerbose (This : access Typ)
                       return Java.Boolean is abstract;

   procedure SetVerbose (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   procedure Gc (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetObjectPendingFinalizationCount, "getObjectPendingFinalizationCount");
   pragma Export (Java, GetHeapMemoryUsage, "getHeapMemoryUsage");
   pragma Export (Java, GetNonHeapMemoryUsage, "getNonHeapMemoryUsage");
   pragma Export (Java, IsVerbose, "isVerbose");
   pragma Export (Java, SetVerbose, "setVerbose");
   pragma Export (Java, Gc, "gc");

end Java.Lang.Management.MemoryMXBean;
pragma Import (Java, Java.Lang.Management.MemoryMXBean, "java.lang.management.MemoryMXBean");
pragma Extensions_Allowed (Off);
