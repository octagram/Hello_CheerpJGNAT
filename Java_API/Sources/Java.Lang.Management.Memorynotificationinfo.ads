pragma Extensions_Allowed (On);
limited with Java.Lang.Management.MemoryUsage;
limited with Java.Lang.String;
limited with Javax.Management.Openmbean.CompositeData;
with Java.Lang.Object;

package Java.Lang.Management.MemoryNotificationInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MemoryNotificationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_MemoryUsage : access Standard.Java.Lang.Management.MemoryUsage.Typ'Class;
                                        P3_Long : Java.Long; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPoolName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetUsage (This : access Typ)
                      return access Java.Lang.Management.MemoryUsage.Typ'Class;

   function GetCount (This : access Typ)
                      return Java.Long;

   function From (P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class)
                  return access Java.Lang.Management.MemoryNotificationInfo.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MEMORY_THRESHOLD_EXCEEDED : constant access Java.Lang.String.Typ'Class;

   --  final
   MEMORY_COLLECTION_THRESHOLD_EXCEEDED : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MemoryNotificationInfo);
   pragma Import (Java, GetPoolName, "getPoolName");
   pragma Import (Java, GetUsage, "getUsage");
   pragma Import (Java, GetCount, "getCount");
   pragma Import (Java, From, "from");
   pragma Import (Java, MEMORY_THRESHOLD_EXCEEDED, "MEMORY_THRESHOLD_EXCEEDED");
   pragma Import (Java, MEMORY_COLLECTION_THRESHOLD_EXCEEDED, "MEMORY_COLLECTION_THRESHOLD_EXCEEDED");

end Java.Lang.Management.MemoryNotificationInfo;
pragma Import (Java, Java.Lang.Management.MemoryNotificationInfo, "java.lang.management.MemoryNotificationInfo");
pragma Extensions_Allowed (Off);
