pragma Extensions_Allowed (On);
limited with Java.Lang.Management.MemoryType;
limited with Java.Lang.Management.MemoryUsage;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Lang.Management.MemoryPoolMXBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetType (This : access Typ)
                     return access Java.Lang.Management.MemoryType.Typ'Class is abstract;

   function GetUsage (This : access Typ)
                      return access Java.Lang.Management.MemoryUsage.Typ'Class is abstract;

   function GetPeakUsage (This : access Typ)
                          return access Java.Lang.Management.MemoryUsage.Typ'Class is abstract;

   procedure ResetPeakUsage (This : access Typ) is abstract;

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;

   function GetMemoryManagerNames (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref is abstract;

   function GetUsageThreshold (This : access Typ)
                               return Java.Long is abstract;

   procedure SetUsageThreshold (This : access Typ;
                                P1_Long : Java.Long) is abstract;

   function IsUsageThresholdExceeded (This : access Typ)
                                      return Java.Boolean is abstract;

   function GetUsageThresholdCount (This : access Typ)
                                    return Java.Long is abstract;

   function IsUsageThresholdSupported (This : access Typ)
                                       return Java.Boolean is abstract;

   function GetCollectionUsageThreshold (This : access Typ)
                                         return Java.Long is abstract;

   procedure SetCollectionUsageThreshold (This : access Typ;
                                          P1_Long : Java.Long) is abstract;

   function IsCollectionUsageThresholdExceeded (This : access Typ)
                                                return Java.Boolean is abstract;

   function GetCollectionUsageThresholdCount (This : access Typ)
                                              return Java.Long is abstract;

   function GetCollectionUsage (This : access Typ)
                                return access Java.Lang.Management.MemoryUsage.Typ'Class is abstract;

   function IsCollectionUsageThresholdSupported (This : access Typ)
                                                 return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetUsage, "getUsage");
   pragma Export (Java, GetPeakUsage, "getPeakUsage");
   pragma Export (Java, ResetPeakUsage, "resetPeakUsage");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, GetMemoryManagerNames, "getMemoryManagerNames");
   pragma Export (Java, GetUsageThreshold, "getUsageThreshold");
   pragma Export (Java, SetUsageThreshold, "setUsageThreshold");
   pragma Export (Java, IsUsageThresholdExceeded, "isUsageThresholdExceeded");
   pragma Export (Java, GetUsageThresholdCount, "getUsageThresholdCount");
   pragma Export (Java, IsUsageThresholdSupported, "isUsageThresholdSupported");
   pragma Export (Java, GetCollectionUsageThreshold, "getCollectionUsageThreshold");
   pragma Export (Java, SetCollectionUsageThreshold, "setCollectionUsageThreshold");
   pragma Export (Java, IsCollectionUsageThresholdExceeded, "isCollectionUsageThresholdExceeded");
   pragma Export (Java, GetCollectionUsageThresholdCount, "getCollectionUsageThresholdCount");
   pragma Export (Java, GetCollectionUsage, "getCollectionUsage");
   pragma Export (Java, IsCollectionUsageThresholdSupported, "isCollectionUsageThresholdSupported");

end Java.Lang.Management.MemoryPoolMXBean;
pragma Import (Java, Java.Lang.Management.MemoryPoolMXBean, "java.lang.management.MemoryPoolMXBean");
pragma Extensions_Allowed (Off);
