pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Openmbean.CompositeData;
with Java.Lang.Object;

package Java.Lang.Management.MemoryUsage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MemoryUsage (P1_Long : Java.Long;
                             P2_Long : Java.Long;
                             P3_Long : Java.Long;
                             P4_Long : Java.Long; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInit (This : access Typ)
                     return Java.Long;

   function GetUsed (This : access Typ)
                     return Java.Long;

   function GetCommitted (This : access Typ)
                          return Java.Long;

   function GetMax (This : access Typ)
                    return Java.Long;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function From (P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class)
                  return access Java.Lang.Management.MemoryUsage.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MemoryUsage);
   pragma Import (Java, GetInit, "getInit");
   pragma Import (Java, GetUsed, "getUsed");
   pragma Import (Java, GetCommitted, "getCommitted");
   pragma Import (Java, GetMax, "getMax");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, From, "from");

end Java.Lang.Management.MemoryUsage;
pragma Import (Java, Java.Lang.Management.MemoryUsage, "java.lang.management.MemoryUsage");
pragma Extensions_Allowed (Off);
