pragma Extensions_Allowed (On);
limited with Java.Lang.StackTraceElement;
limited with Java.Lang.String;
limited with Javax.Management.Openmbean.CompositeData;
with Java.Lang.Management.LockInfo;
with Java.Lang.Object;

package Java.Lang.Management.MonitorInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Management.LockInfo.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MonitorInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_StackTraceElement : access Standard.Java.Lang.StackTraceElement.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLockedStackDepth (This : access Typ)
                                 return Java.Int;

   function GetLockedStackFrame (This : access Typ)
                                 return access Java.Lang.StackTraceElement.Typ'Class;

   function From (P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class)
                  return access Java.Lang.Management.MonitorInfo.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MonitorInfo);
   pragma Import (Java, GetLockedStackDepth, "getLockedStackDepth");
   pragma Import (Java, GetLockedStackFrame, "getLockedStackFrame");
   pragma Import (Java, From, "from");

end Java.Lang.Management.MonitorInfo;
pragma Import (Java, Java.Lang.Management.MonitorInfo, "java.lang.management.MonitorInfo");
pragma Extensions_Allowed (Off);
