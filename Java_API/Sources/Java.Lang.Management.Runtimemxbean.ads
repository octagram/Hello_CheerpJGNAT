pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Lang.Management.RuntimeMXBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetVmName (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function GetVmVendor (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetVmVersion (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetSpecName (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetSpecVendor (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetSpecVersion (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetManagementSpecVersion (This : access Typ)
                                      return access Java.Lang.String.Typ'Class is abstract;

   function GetClassPath (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetLibraryPath (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function IsBootClassPathSupported (This : access Typ)
                                      return Java.Boolean is abstract;

   function GetBootClassPath (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   function GetInputArguments (This : access Typ)
                               return access Java.Util.List.Typ'Class is abstract;

   function GetUptime (This : access Typ)
                       return Java.Long is abstract;

   function GetStartTime (This : access Typ)
                          return Java.Long is abstract;

   function GetSystemProperties (This : access Typ)
                                 return access Java.Util.Map.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetVmName, "getVmName");
   pragma Export (Java, GetVmVendor, "getVmVendor");
   pragma Export (Java, GetVmVersion, "getVmVersion");
   pragma Export (Java, GetSpecName, "getSpecName");
   pragma Export (Java, GetSpecVendor, "getSpecVendor");
   pragma Export (Java, GetSpecVersion, "getSpecVersion");
   pragma Export (Java, GetManagementSpecVersion, "getManagementSpecVersion");
   pragma Export (Java, GetClassPath, "getClassPath");
   pragma Export (Java, GetLibraryPath, "getLibraryPath");
   pragma Export (Java, IsBootClassPathSupported, "isBootClassPathSupported");
   pragma Export (Java, GetBootClassPath, "getBootClassPath");
   pragma Export (Java, GetInputArguments, "getInputArguments");
   pragma Export (Java, GetUptime, "getUptime");
   pragma Export (Java, GetStartTime, "getStartTime");
   pragma Export (Java, GetSystemProperties, "getSystemProperties");

end Java.Lang.Management.RuntimeMXBean;
pragma Import (Java, Java.Lang.Management.RuntimeMXBean, "java.lang.management.RuntimeMXBean");
pragma Extensions_Allowed (Off);
