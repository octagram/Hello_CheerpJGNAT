pragma Extensions_Allowed (On);
limited with Java.Lang.Management.LockInfo;
limited with Java.Lang.Management.MonitorInfo;
limited with Java.Lang.StackTraceElement;
limited with Java.Lang.String;
limited with Java.Lang.Thread.State;
limited with Javax.Management.Openmbean.CompositeData;
with Java.Lang.Object;

package Java.Lang.Management.ThreadInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetThreadId (This : access Typ)
                         return Java.Long;

   function GetThreadName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetThreadState (This : access Typ)
                            return access Java.Lang.Thread.State.Typ'Class;

   function GetBlockedTime (This : access Typ)
                            return Java.Long;

   function GetBlockedCount (This : access Typ)
                             return Java.Long;

   function GetWaitedTime (This : access Typ)
                           return Java.Long;

   function GetWaitedCount (This : access Typ)
                            return Java.Long;

   function GetLockInfo (This : access Typ)
                         return access Java.Lang.Management.LockInfo.Typ'Class;

   function GetLockName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetLockOwnerId (This : access Typ)
                            return Java.Long;

   function GetLockOwnerName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetStackTrace (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function IsSuspended (This : access Typ)
                         return Java.Boolean;

   function IsInNative (This : access Typ)
                        return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function From (P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class)
                  return access Java.Lang.Management.ThreadInfo.Typ'Class;

   function GetLockedMonitors (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetLockedSynchronizers (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetThreadId, "getThreadId");
   pragma Import (Java, GetThreadName, "getThreadName");
   pragma Import (Java, GetThreadState, "getThreadState");
   pragma Import (Java, GetBlockedTime, "getBlockedTime");
   pragma Import (Java, GetBlockedCount, "getBlockedCount");
   pragma Import (Java, GetWaitedTime, "getWaitedTime");
   pragma Import (Java, GetWaitedCount, "getWaitedCount");
   pragma Import (Java, GetLockInfo, "getLockInfo");
   pragma Import (Java, GetLockName, "getLockName");
   pragma Import (Java, GetLockOwnerId, "getLockOwnerId");
   pragma Import (Java, GetLockOwnerName, "getLockOwnerName");
   pragma Import (Java, GetStackTrace, "getStackTrace");
   pragma Import (Java, IsSuspended, "isSuspended");
   pragma Import (Java, IsInNative, "isInNative");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, From, "from");
   pragma Import (Java, GetLockedMonitors, "getLockedMonitors");
   pragma Import (Java, GetLockedSynchronizers, "getLockedSynchronizers");

end Java.Lang.Management.ThreadInfo;
pragma Import (Java, Java.Lang.Management.ThreadInfo, "java.lang.management.ThreadInfo");
pragma Extensions_Allowed (Off);
