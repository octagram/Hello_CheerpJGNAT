pragma Extensions_Allowed (On);
limited with Java.Lang.Management.ThreadInfo;
with Java.Lang.Object;

package Java.Lang.Management.ThreadMXBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetThreadCount (This : access Typ)
                            return Java.Int is abstract;

   function GetPeakThreadCount (This : access Typ)
                                return Java.Int is abstract;

   function GetTotalStartedThreadCount (This : access Typ)
                                        return Java.Long is abstract;

   function GetDaemonThreadCount (This : access Typ)
                                  return Java.Int is abstract;

   function GetAllThreadIds (This : access Typ)
                             return Java.Long_Arr is abstract;

   function GetThreadInfo (This : access Typ;
                           P1_Long : Java.Long)
                           return access Java.Lang.Management.ThreadInfo.Typ'Class is abstract;

   function GetThreadInfo (This : access Typ;
                           P1_Long_Arr : Java.Long_Arr)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function GetThreadInfo (This : access Typ;
                           P1_Long : Java.Long;
                           P2_Int : Java.Int)
                           return access Java.Lang.Management.ThreadInfo.Typ'Class is abstract;

   function GetThreadInfo (This : access Typ;
                           P1_Long_Arr : Java.Long_Arr;
                           P2_Int : Java.Int)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function IsThreadContentionMonitoringSupported (This : access Typ)
                                                   return Java.Boolean is abstract;

   function IsThreadContentionMonitoringEnabled (This : access Typ)
                                                 return Java.Boolean is abstract;

   procedure SetThreadContentionMonitoringEnabled (This : access Typ;
                                                   P1_Boolean : Java.Boolean) is abstract;

   function GetCurrentThreadCpuTime (This : access Typ)
                                     return Java.Long is abstract;

   function GetCurrentThreadUserTime (This : access Typ)
                                      return Java.Long is abstract;

   function GetThreadCpuTime (This : access Typ;
                              P1_Long : Java.Long)
                              return Java.Long is abstract;

   function GetThreadUserTime (This : access Typ;
                               P1_Long : Java.Long)
                               return Java.Long is abstract;

   function IsThreadCpuTimeSupported (This : access Typ)
                                      return Java.Boolean is abstract;

   function IsCurrentThreadCpuTimeSupported (This : access Typ)
                                             return Java.Boolean is abstract;

   function IsThreadCpuTimeEnabled (This : access Typ)
                                    return Java.Boolean is abstract;

   procedure SetThreadCpuTimeEnabled (This : access Typ;
                                      P1_Boolean : Java.Boolean) is abstract;

   function FindMonitorDeadlockedThreads (This : access Typ)
                                          return Java.Long_Arr is abstract;

   procedure ResetPeakThreadCount (This : access Typ) is abstract;

   function FindDeadlockedThreads (This : access Typ)
                                   return Java.Long_Arr is abstract;

   function IsObjectMonitorUsageSupported (This : access Typ)
                                           return Java.Boolean is abstract;

   function IsSynchronizerUsageSupported (This : access Typ)
                                          return Java.Boolean is abstract;

   function GetThreadInfo (This : access Typ;
                           P1_Long_Arr : Java.Long_Arr;
                           P2_Boolean : Java.Boolean;
                           P3_Boolean : Java.Boolean)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function DumpAllThreads (This : access Typ;
                            P1_Boolean : Java.Boolean;
                            P2_Boolean : Java.Boolean)
                            return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetThreadCount, "getThreadCount");
   pragma Export (Java, GetPeakThreadCount, "getPeakThreadCount");
   pragma Export (Java, GetTotalStartedThreadCount, "getTotalStartedThreadCount");
   pragma Export (Java, GetDaemonThreadCount, "getDaemonThreadCount");
   pragma Export (Java, GetAllThreadIds, "getAllThreadIds");
   pragma Export (Java, GetThreadInfo, "getThreadInfo");
   pragma Export (Java, IsThreadContentionMonitoringSupported, "isThreadContentionMonitoringSupported");
   pragma Export (Java, IsThreadContentionMonitoringEnabled, "isThreadContentionMonitoringEnabled");
   pragma Export (Java, SetThreadContentionMonitoringEnabled, "setThreadContentionMonitoringEnabled");
   pragma Export (Java, GetCurrentThreadCpuTime, "getCurrentThreadCpuTime");
   pragma Export (Java, GetCurrentThreadUserTime, "getCurrentThreadUserTime");
   pragma Export (Java, GetThreadCpuTime, "getThreadCpuTime");
   pragma Export (Java, GetThreadUserTime, "getThreadUserTime");
   pragma Export (Java, IsThreadCpuTimeSupported, "isThreadCpuTimeSupported");
   pragma Export (Java, IsCurrentThreadCpuTimeSupported, "isCurrentThreadCpuTimeSupported");
   pragma Export (Java, IsThreadCpuTimeEnabled, "isThreadCpuTimeEnabled");
   pragma Export (Java, SetThreadCpuTimeEnabled, "setThreadCpuTimeEnabled");
   pragma Export (Java, FindMonitorDeadlockedThreads, "findMonitorDeadlockedThreads");
   pragma Export (Java, ResetPeakThreadCount, "resetPeakThreadCount");
   pragma Export (Java, FindDeadlockedThreads, "findDeadlockedThreads");
   pragma Export (Java, IsObjectMonitorUsageSupported, "isObjectMonitorUsageSupported");
   pragma Export (Java, IsSynchronizerUsageSupported, "isSynchronizerUsageSupported");
   pragma Export (Java, DumpAllThreads, "dumpAllThreads");

end Java.Lang.Management.ThreadMXBean;
pragma Import (Java, Java.Lang.Management.ThreadMXBean, "java.lang.management.ThreadMXBean");
pragma Extensions_Allowed (Off);
