pragma Extensions_Allowed (On);
package Java.Lang.Management is
   pragma Preelaborate;
end Java.Lang.Management;
pragma Import (Java, Java.Lang.Management, "java.lang.management");
pragma Extensions_Allowed (Off);
