pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Lang.Math is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Sin (P1_Double : Java.Double)
                 return Java.Double;

   function Cos (P1_Double : Java.Double)
                 return Java.Double;

   function Tan (P1_Double : Java.Double)
                 return Java.Double;

   function Asin (P1_Double : Java.Double)
                  return Java.Double;

   function Acos (P1_Double : Java.Double)
                  return Java.Double;

   function Atan (P1_Double : Java.Double)
                  return Java.Double;

   function ToRadians (P1_Double : Java.Double)
                       return Java.Double;

   function ToDegrees (P1_Double : Java.Double)
                       return Java.Double;

   function Exp (P1_Double : Java.Double)
                 return Java.Double;

   function Log (P1_Double : Java.Double)
                 return Java.Double;

   function Log10 (P1_Double : Java.Double)
                   return Java.Double;

   function Sqrt (P1_Double : Java.Double)
                  return Java.Double;

   function Cbrt (P1_Double : Java.Double)
                  return Java.Double;

   function IEEEremainder (P1_Double : Java.Double;
                           P2_Double : Java.Double)
                           return Java.Double;

   function Ceil (P1_Double : Java.Double)
                  return Java.Double;

   function Floor (P1_Double : Java.Double)
                   return Java.Double;

   function Rint (P1_Double : Java.Double)
                  return Java.Double;

   function Atan2 (P1_Double : Java.Double;
                   P2_Double : Java.Double)
                   return Java.Double;

   function Pow (P1_Double : Java.Double;
                 P2_Double : Java.Double)
                 return Java.Double;

   function Round (P1_Float : Java.Float)
                   return Java.Int;

   function Round (P1_Double : Java.Double)
                   return Java.Long;

   function Random return Java.Double;

   function abs_K (P1_Int : Java.Int)
                   return Java.Int;

   function abs_K (P1_Long : Java.Long)
                   return Java.Long;

   function abs_K (P1_Float : Java.Float)
                   return Java.Float;

   function abs_K (P1_Double : Java.Double)
                   return Java.Double;

   function Max (P1_Int : Java.Int;
                 P2_Int : Java.Int)
                 return Java.Int;

   function Max (P1_Long : Java.Long;
                 P2_Long : Java.Long)
                 return Java.Long;

   function Max (P1_Float : Java.Float;
                 P2_Float : Java.Float)
                 return Java.Float;

   function Max (P1_Double : Java.Double;
                 P2_Double : Java.Double)
                 return Java.Double;

   function Min (P1_Int : Java.Int;
                 P2_Int : Java.Int)
                 return Java.Int;

   function Min (P1_Long : Java.Long;
                 P2_Long : Java.Long)
                 return Java.Long;

   function Min (P1_Float : Java.Float;
                 P2_Float : Java.Float)
                 return Java.Float;

   function Min (P1_Double : Java.Double;
                 P2_Double : Java.Double)
                 return Java.Double;

   function Ulp (P1_Double : Java.Double)
                 return Java.Double;

   function Ulp (P1_Float : Java.Float)
                 return Java.Float;

   function Signum (P1_Double : Java.Double)
                    return Java.Double;

   function Signum (P1_Float : Java.Float)
                    return Java.Float;

   function Sinh (P1_Double : Java.Double)
                  return Java.Double;

   function Cosh (P1_Double : Java.Double)
                  return Java.Double;

   function Tanh (P1_Double : Java.Double)
                  return Java.Double;

   function Hypot (P1_Double : Java.Double;
                   P2_Double : Java.Double)
                   return Java.Double;

   function Expm1 (P1_Double : Java.Double)
                   return Java.Double;

   function Log1p (P1_Double : Java.Double)
                   return Java.Double;

   function CopySign (P1_Double : Java.Double;
                      P2_Double : Java.Double)
                      return Java.Double;

   function CopySign (P1_Float : Java.Float;
                      P2_Float : Java.Float)
                      return Java.Float;

   function GetExponent (P1_Float : Java.Float)
                         return Java.Int;

   function GetExponent (P1_Double : Java.Double)
                         return Java.Int;

   function NextAfter (P1_Double : Java.Double;
                       P2_Double : Java.Double)
                       return Java.Double;

   function NextAfter (P1_Float : Java.Float;
                       P2_Double : Java.Double)
                       return Java.Float;

   function NextUp (P1_Double : Java.Double)
                    return Java.Double;

   function NextUp (P1_Float : Java.Float)
                    return Java.Float;

   function Scalb (P1_Double : Java.Double;
                   P2_Int : Java.Int)
                   return Java.Double;

   function Scalb (P1_Float : Java.Float;
                   P2_Int : Java.Int)
                   return Java.Float;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   E : constant Java.Double;

   --  final
   PI : constant Java.Double;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Sin, "sin");
   pragma Import (Java, Cos, "cos");
   pragma Import (Java, Tan, "tan");
   pragma Import (Java, Asin, "asin");
   pragma Import (Java, Acos, "acos");
   pragma Import (Java, Atan, "atan");
   pragma Import (Java, ToRadians, "toRadians");
   pragma Import (Java, ToDegrees, "toDegrees");
   pragma Import (Java, Exp, "exp");
   pragma Import (Java, Log, "log");
   pragma Import (Java, Log10, "log10");
   pragma Import (Java, Sqrt, "sqrt");
   pragma Import (Java, Cbrt, "cbrt");
   pragma Import (Java, IEEEremainder, "IEEEremainder");
   pragma Import (Java, Ceil, "ceil");
   pragma Import (Java, Floor, "floor");
   pragma Import (Java, Rint, "rint");
   pragma Import (Java, Atan2, "atan2");
   pragma Import (Java, Pow, "pow");
   pragma Import (Java, Round, "round");
   pragma Import (Java, Random, "random");
   pragma Import (Java, abs_K, "abs");
   pragma Import (Java, Max, "max");
   pragma Import (Java, Min, "min");
   pragma Import (Java, Ulp, "ulp");
   pragma Import (Java, Signum, "signum");
   pragma Import (Java, Sinh, "sinh");
   pragma Import (Java, Cosh, "cosh");
   pragma Import (Java, Tanh, "tanh");
   pragma Import (Java, Hypot, "hypot");
   pragma Import (Java, Expm1, "expm1");
   pragma Import (Java, Log1p, "log1p");
   pragma Import (Java, CopySign, "copySign");
   pragma Import (Java, GetExponent, "getExponent");
   pragma Import (Java, NextAfter, "nextAfter");
   pragma Import (Java, NextUp, "nextUp");
   pragma Import (Java, Scalb, "scalb");
   pragma Import (Java, E, "E");
   pragma Import (Java, PI, "PI");

end Java.Lang.Math;
pragma Import (Java, Java.Lang.Math, "java.lang.Math");
pragma Extensions_Allowed (Off);
