pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Lang.Number is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Number (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IntValue (This : access Typ)
                      return Java.Int is abstract;

   function LongValue (This : access Typ)
                       return Java.Long is abstract;

   function FloatValue (This : access Typ)
                        return Java.Float is abstract;

   function DoubleValue (This : access Typ)
                         return Java.Double is abstract;

   function ByteValue (This : access Typ)
                       return Java.Byte;

   function ShortValue (This : access Typ)
                        return Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Number);
   pragma Export (Java, IntValue, "intValue");
   pragma Export (Java, LongValue, "longValue");
   pragma Export (Java, FloatValue, "floatValue");
   pragma Export (Java, DoubleValue, "doubleValue");
   pragma Import (Java, ByteValue, "byteValue");
   pragma Import (Java, ShortValue, "shortValue");

end Java.Lang.Number;
pragma Import (Java, Java.Lang.Number, "java.lang.Number");
pragma Extensions_Allowed (Off);
