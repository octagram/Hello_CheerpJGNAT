pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;

package Java.Lang.Object is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ is tagged limited null record;
   pragma Convention (Java, Typ);
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   
   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Object (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetClass (This : access Typ)
                      return access Java.Lang.Class.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   procedure Notify (This : access Typ);

   --  final
   procedure NotifyAll (This : access Typ);

   --  final
   procedure Wait (This : access Typ;
                   P1_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   procedure Wait (This : access Typ;
                   P1_Long : Java.Long;
                   P2_Int : Java.Int);
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   procedure Wait (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Lang.Throwable.Except
private
   pragma Java_Constructor (New_Object);
   pragma Import (Java, GetClass, "getClass");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Notify, "notify");
   pragma Import (Java, NotifyAll, "notifyAll");
   pragma Import (Java, Wait, "wait");
   pragma Import (Java, Finalize, "finalize");

end Java.Lang.Object;
pragma Import (Java, Java.Lang.Object, "java.lang.Object");
pragma Extensions_Allowed (Off);
