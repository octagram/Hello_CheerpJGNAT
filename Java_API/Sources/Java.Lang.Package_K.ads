pragma Extensions_Allowed (On);
limited with Java.Lang.Annotation.Annotation;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
with Java.Lang.Object;
with Java.Lang.Reflect.AnnotatedElement;

package Java.Lang.Package_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AnnotatedElement_I : Java.Lang.Reflect.AnnotatedElement.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetSpecificationTitle (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   function GetSpecificationVersion (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetSpecificationVendor (This : access Typ)
                                    return access Java.Lang.String.Typ'Class;

   function GetImplementationTitle (This : access Typ)
                                    return access Java.Lang.String.Typ'Class;

   function GetImplementationVersion (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetImplementationVendor (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function IsSealed (This : access Typ)
                      return Java.Boolean;

   function IsSealed (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class)
                      return Java.Boolean;

   function IsCompatibleWith (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Boolean;
   --  can raise Java.Lang.NumberFormatException.Except

   function GetPackage (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Package_K.Typ'Class;

   function GetPackages return Standard.Java.Lang.Object.Ref;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetAnnotation (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Annotation.Annotation.Typ'Class;

   function IsAnnotationPresent (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                 return Java.Boolean;

   function GetAnnotations (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetDeclaredAnnotations (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetSpecificationTitle, "getSpecificationTitle");
   pragma Import (Java, GetSpecificationVersion, "getSpecificationVersion");
   pragma Import (Java, GetSpecificationVendor, "getSpecificationVendor");
   pragma Import (Java, GetImplementationTitle, "getImplementationTitle");
   pragma Import (Java, GetImplementationVersion, "getImplementationVersion");
   pragma Import (Java, GetImplementationVendor, "getImplementationVendor");
   pragma Import (Java, IsSealed, "isSealed");
   pragma Import (Java, IsCompatibleWith, "isCompatibleWith");
   pragma Import (Java, GetPackage, "getPackage");
   pragma Import (Java, GetPackages, "getPackages");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetAnnotation, "getAnnotation");
   pragma Import (Java, IsAnnotationPresent, "isAnnotationPresent");
   pragma Import (Java, GetAnnotations, "getAnnotations");
   pragma Import (Java, GetDeclaredAnnotations, "getDeclaredAnnotations");

end Java.Lang.Package_K;
pragma Import (Java, Java.Lang.Package_K, "java.lang.Package");
pragma Extensions_Allowed (Off);
