pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Lang.Process is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Process (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class is abstract;

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class is abstract;

   function GetErrorStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class is abstract;

   function WaitFor (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function ExitValue (This : access Typ)
                       return Java.Int is abstract;

   procedure Destroy (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Process);
   pragma Export (Java, GetOutputStream, "getOutputStream");
   pragma Export (Java, GetInputStream, "getInputStream");
   pragma Export (Java, GetErrorStream, "getErrorStream");
   pragma Export (Java, WaitFor, "waitFor");
   pragma Export (Java, ExitValue, "exitValue");
   pragma Export (Java, Destroy, "destroy");

end Java.Lang.Process;
pragma Import (Java, Java.Lang.Process, "java.lang.Process");
pragma Extensions_Allowed (Off);
