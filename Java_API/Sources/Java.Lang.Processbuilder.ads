pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.Process;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Lang.ProcessBuilder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ProcessBuilder (P1_List : access Standard.Java.Util.List.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_ProcessBuilder (P1_String_Arr : access Java.Lang.String.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Command (This : access Typ;
                     P1_List : access Standard.Java.Util.List.Typ'Class)
                     return access Java.Lang.ProcessBuilder.Typ'Class;

   function Command (This : access Typ;
                     P1_String_Arr : access Java.Lang.String.Arr_Obj)
                     return access Java.Lang.ProcessBuilder.Typ'Class;

   function Command (This : access Typ)
                     return access Java.Util.List.Typ'Class;

   function Environment (This : access Typ)
                         return access Java.Util.Map.Typ'Class;

   function Directory (This : access Typ)
                       return access Java.Io.File.Typ'Class;

   function Directory (This : access Typ;
                       P1_File : access Standard.Java.Io.File.Typ'Class)
                       return access Java.Lang.ProcessBuilder.Typ'Class;

   function RedirectErrorStream (This : access Typ)
                                 return Java.Boolean;

   function RedirectErrorStream (This : access Typ;
                                 P1_Boolean : Java.Boolean)
                                 return access Java.Lang.ProcessBuilder.Typ'Class;

   function Start (This : access Typ)
                   return access Java.Lang.Process.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ProcessBuilder);
   pragma Import (Java, Command, "command");
   pragma Import (Java, Environment, "environment");
   pragma Import (Java, Directory, "directory");
   pragma Import (Java, RedirectErrorStream, "redirectErrorStream");
   pragma Import (Java, Start, "start");

end Java.Lang.ProcessBuilder;
pragma Import (Java, Java.Lang.ProcessBuilder, "java.lang.ProcessBuilder");
pragma Extensions_Allowed (Off);
