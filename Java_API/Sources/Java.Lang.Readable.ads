pragma Extensions_Allowed (On);
limited with Java.Nio.CharBuffer;
with Java.Lang.Object;

package Java.Lang.Readable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ;
                  P1_CharBuffer : access Standard.Java.Nio.CharBuffer.Typ'Class)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Read, "read");

end Java.Lang.Readable;
pragma Import (Java, Java.Lang.Readable, "java.lang.Readable");
pragma Extensions_Allowed (Off);
