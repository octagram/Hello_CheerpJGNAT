pragma Extensions_Allowed (On);
limited with Java.Lang.Ref.ReferenceQueue;
with Java.Lang.Object;
with Java.Lang.Ref.Reference;

package Java.Lang.Ref.PhantomReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Ref.Reference.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PhantomReference (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                  P2_ReferenceQueue : access Standard.Java.Lang.Ref.ReferenceQueue.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Get, "get");
   pragma Java_Constructor (New_PhantomReference);

end Java.Lang.Ref.PhantomReference;
pragma Import (Java, Java.Lang.Ref.PhantomReference, "java.lang.ref.PhantomReference");
pragma Extensions_Allowed (Off);
