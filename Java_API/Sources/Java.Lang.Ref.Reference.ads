pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Lang.Ref.Reference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   function IsEnqueued (This : access Typ)
                        return Java.Boolean;

   function Enqueue (This : access Typ)
                     return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Get, "get");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, IsEnqueued, "isEnqueued");
   pragma Import (Java, Enqueue, "enqueue");

end Java.Lang.Ref.Reference;
pragma Import (Java, Java.Lang.Ref.Reference, "java.lang.ref.Reference");
pragma Extensions_Allowed (Off);
