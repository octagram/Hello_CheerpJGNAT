pragma Extensions_Allowed (On);
limited with Java.Lang.Ref.Reference;
with Java.Lang.Object;

package Java.Lang.Ref.ReferenceQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ReferenceQueue (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Poll (This : access Typ)
                  return access Java.Lang.Ref.Reference.Typ'Class;

   function Remove (This : access Typ;
                    P1_Long : Java.Long)
                    return access Java.Lang.Ref.Reference.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.InterruptedException.Except

   function Remove (This : access Typ)
                    return access Java.Lang.Ref.Reference.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ReferenceQueue);
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, Remove, "remove");

end Java.Lang.Ref.ReferenceQueue;
pragma Import (Java, Java.Lang.Ref.ReferenceQueue, "java.lang.ref.ReferenceQueue");
pragma Extensions_Allowed (Off);
