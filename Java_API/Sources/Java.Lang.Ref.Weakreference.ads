pragma Extensions_Allowed (On);
limited with Java.Lang.Ref.ReferenceQueue;
with Java.Lang.Object;
with Java.Lang.Ref.Reference;

package Java.Lang.Ref.WeakReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Ref.Reference.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_WeakReference (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_WeakReference (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_ReferenceQueue : access Standard.Java.Lang.Ref.ReferenceQueue.Typ'Class; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_WeakReference);

end Java.Lang.Ref.WeakReference;
pragma Import (Java, Java.Lang.Ref.WeakReference, "java.lang.ref.WeakReference");
pragma Extensions_Allowed (Off);
