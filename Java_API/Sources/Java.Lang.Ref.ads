pragma Extensions_Allowed (On);
package Java.Lang.Ref is
   pragma Preelaborate;
end Java.Lang.Ref;
pragma Import (Java, Java.Lang.Ref, "java.lang.ref");
pragma Extensions_Allowed (Off);
