pragma Extensions_Allowed (On);
limited with Java.Lang.Annotation.Annotation;
limited with Java.Lang.Class;
with Java.Lang.Object;
with Java.Lang.Reflect.AnnotatedElement;

package Java.Lang.Reflect.AccessibleObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AnnotatedElement_I : Java.Lang.Reflect.AnnotatedElement.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetAccessible (P1_AccessibleObject_Arr : access Java.Lang.Reflect.AccessibleObject.Arr_Obj;
                            P2_Boolean : Java.Boolean);
   --  can raise Java.Lang.SecurityException.Except

   procedure SetAccessible (This : access Typ;
                            P1_Boolean : Java.Boolean);
   --  can raise Java.Lang.SecurityException.Except

   function IsAccessible (This : access Typ)
                          return Java.Boolean;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AccessibleObject (This : Ref := null)
                                  return Ref;

   function GetAnnotation (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Annotation.Annotation.Typ'Class;

   function IsAnnotationPresent (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                 return Java.Boolean;

   function GetAnnotations (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetDeclaredAnnotations (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetAccessible, "setAccessible");
   pragma Import (Java, IsAccessible, "isAccessible");
   pragma Java_Constructor (New_AccessibleObject);
   pragma Import (Java, GetAnnotation, "getAnnotation");
   pragma Import (Java, IsAnnotationPresent, "isAnnotationPresent");
   pragma Import (Java, GetAnnotations, "getAnnotations");
   pragma Import (Java, GetDeclaredAnnotations, "getDeclaredAnnotations");

end Java.Lang.Reflect.AccessibleObject;
pragma Import (Java, Java.Lang.Reflect.AccessibleObject, "java.lang.reflect.AccessibleObject");
pragma Extensions_Allowed (Off);
