pragma Extensions_Allowed (On);
limited with Java.Lang.Annotation.Annotation;
limited with Java.Lang.Class;
with Java.Lang.Object;

package Java.Lang.Reflect.AnnotatedElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsAnnotationPresent (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                 return Java.Boolean is abstract;

   function GetAnnotation (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Annotation.Annotation.Typ'Class is abstract;

   function GetAnnotations (This : access Typ)
                            return Standard.Java.Lang.Object.Ref is abstract;

   function GetDeclaredAnnotations (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsAnnotationPresent, "isAnnotationPresent");
   pragma Export (Java, GetAnnotation, "getAnnotation");
   pragma Export (Java, GetAnnotations, "getAnnotations");
   pragma Export (Java, GetDeclaredAnnotations, "getDeclaredAnnotations");

end Java.Lang.Reflect.AnnotatedElement;
pragma Import (Java, Java.Lang.Reflect.AnnotatedElement, "java.lang.reflect.AnnotatedElement");
pragma Extensions_Allowed (Off);
