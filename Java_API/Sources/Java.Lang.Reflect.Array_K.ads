pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
with Java.Lang.Object;

package Java.Lang.Reflect.Array_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Int : Java.Int)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.NegativeArraySizeException.Except

   function NewInstance (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Int_Arr : Java.Int_Arr)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.NegativeArraySizeException.Except

   function GetLength (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function Get (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetBoolean (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Int : Java.Int)
                        return Java.Boolean;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetByte (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Byte;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetChar (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Char;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetShort (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int)
                      return Java.Short;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetInt (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Int : Java.Int)
                    return Java.Int;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetLong (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Long;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetFloat (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int)
                      return Java.Float;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   function GetDouble (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int)
                       return Java.Double;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure Set (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P2_Int : Java.Int;
                  P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetBoolean (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Boolean : Java.Boolean);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetByte (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Byte : Java.Byte);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetChar (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Char : Java.Char);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetShort (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Short : Java.Short);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetInt (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetLong (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Long : Java.Long);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetFloat (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Float : Java.Float);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except

   procedure SetDouble (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Double : Java.Double);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.ArrayIndexOutOfBoundsException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewInstance, "newInstance");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, Get, "get");
   pragma Import (Java, GetBoolean, "getBoolean");
   pragma Import (Java, GetByte, "getByte");
   pragma Import (Java, GetChar, "getChar");
   pragma Import (Java, GetShort, "getShort");
   pragma Import (Java, GetInt, "getInt");
   pragma Import (Java, GetLong, "getLong");
   pragma Import (Java, GetFloat, "getFloat");
   pragma Import (Java, GetDouble, "getDouble");
   pragma Import (Java, Set, "set");
   pragma Import (Java, SetBoolean, "setBoolean");
   pragma Import (Java, SetByte, "setByte");
   pragma Import (Java, SetChar, "setChar");
   pragma Import (Java, SetShort, "setShort");
   pragma Import (Java, SetInt, "setInt");
   pragma Import (Java, SetLong, "setLong");
   pragma Import (Java, SetFloat, "setFloat");
   pragma Import (Java, SetDouble, "setDouble");

end Java.Lang.Reflect.Array_K;
pragma Import (Java, Java.Lang.Reflect.Array_K, "java.lang.reflect.Array");
pragma Extensions_Allowed (Off);
