pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Lang.Reflect.Type_K;

package Java.Lang.Reflect.GenericArrayType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Type_K_I : Java.Lang.Reflect.Type_K.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetGenericComponentType (This : access Typ)
                                     return access Java.Lang.Reflect.Type_K.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetGenericComponentType, "getGenericComponentType");

end Java.Lang.Reflect.GenericArrayType;
pragma Import (Java, Java.Lang.Reflect.GenericArrayType, "java.lang.reflect.GenericArrayType");
pragma Extensions_Allowed (Off);
