pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Java.Lang.Reflect.InvocationTargetException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_InvocationTargetException (This : Ref := null)
                                           return Ref;

   function New_InvocationTargetException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   function New_InvocationTargetException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class;
                                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTargetException (This : access Typ)
                                return access Java.Lang.Throwable.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.lang.reflect.InvocationTargetException");
   pragma Java_Constructor (New_InvocationTargetException);
   pragma Import (Java, GetTargetException, "getTargetException");
   pragma Import (Java, GetCause, "getCause");

end Java.Lang.Reflect.InvocationTargetException;
pragma Import (Java, Java.Lang.Reflect.InvocationTargetException, "java.lang.reflect.InvocationTargetException");
pragma Extensions_Allowed (Off);
