pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Lang.Reflect.Member is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDeclaringClass (This : access Typ)
                               return access Java.Lang.Class.Typ'Class is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetModifiers (This : access Typ)
                          return Java.Int is abstract;

   function IsSynthetic (This : access Typ)
                         return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PUBLIC : constant Java.Int;

   --  final
   DECLARED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDeclaringClass, "getDeclaringClass");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetModifiers, "getModifiers");
   pragma Export (Java, IsSynthetic, "isSynthetic");
   pragma Import (Java, PUBLIC, "PUBLIC");
   pragma Import (Java, DECLARED, "DECLARED");

end Java.Lang.Reflect.Member;
pragma Import (Java, Java.Lang.Reflect.Member, "java.lang.reflect.Member");
pragma Extensions_Allowed (Off);
