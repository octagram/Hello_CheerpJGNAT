pragma Extensions_Allowed (On);
limited with Java.Lang.Annotation.Annotation;
limited with Java.Lang.Class;
limited with Java.Lang.Reflect.TypeVariable;
limited with Java.Lang.Reflect.Type_K;
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Lang.Reflect.AccessibleObject;
with Java.Lang.Reflect.AnnotatedElement;
with Java.Lang.Reflect.GenericDeclaration;
with Java.Lang.Reflect.Member;

package Java.Lang.Reflect.Method is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AnnotatedElement_I : Java.Lang.Reflect.AnnotatedElement.Ref;
            GenericDeclaration_I : Java.Lang.Reflect.GenericDeclaration.Ref;
            Member_I : Java.Lang.Reflect.Member.Ref)
    is new Java.Lang.Reflect.AccessibleObject.Typ(AnnotatedElement_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDeclaringClass (This : access Typ)
                               return access Java.Lang.Class.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetModifiers (This : access Typ)
                          return Java.Int;

   function GetTypeParameters (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetReturnType (This : access Typ)
                           return access Java.Lang.Class.Typ'Class;

   function GetGenericReturnType (This : access Typ)
                                  return access Java.Lang.Reflect.Type_K.Typ'Class;

   function GetParameterTypes (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetGenericParameterTypes (This : access Typ)
                                      return Standard.Java.Lang.Object.Ref;

   function GetExceptionTypes (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetGenericExceptionTypes (This : access Typ)
                                      return Standard.Java.Lang.Object.Ref;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToGenericString (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function Invoke (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.IllegalAccessException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.Reflect.InvocationTargetException.Except

   function IsBridge (This : access Typ)
                      return Java.Boolean;

   function IsVarArgs (This : access Typ)
                       return Java.Boolean;

   function IsSynthetic (This : access Typ)
                         return Java.Boolean;

   function GetAnnotation (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Annotation.Annotation.Typ'Class;

   function GetDeclaredAnnotations (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function GetDefaultValue (This : access Typ)
                             return access Java.Lang.Object.Typ'Class;

   function GetParameterAnnotations (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDeclaringClass, "getDeclaringClass");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetModifiers, "getModifiers");
   pragma Import (Java, GetTypeParameters, "getTypeParameters");
   pragma Import (Java, GetReturnType, "getReturnType");
   pragma Import (Java, GetGenericReturnType, "getGenericReturnType");
   pragma Import (Java, GetParameterTypes, "getParameterTypes");
   pragma Import (Java, GetGenericParameterTypes, "getGenericParameterTypes");
   pragma Import (Java, GetExceptionTypes, "getExceptionTypes");
   pragma Import (Java, GetGenericExceptionTypes, "getGenericExceptionTypes");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToGenericString, "toGenericString");
   pragma Import (Java, Invoke, "invoke");
   pragma Import (Java, IsBridge, "isBridge");
   pragma Import (Java, IsVarArgs, "isVarArgs");
   pragma Import (Java, IsSynthetic, "isSynthetic");
   pragma Import (Java, GetAnnotation, "getAnnotation");
   pragma Import (Java, GetDeclaredAnnotations, "getDeclaredAnnotations");
   pragma Import (Java, GetDefaultValue, "getDefaultValue");
   pragma Import (Java, GetParameterAnnotations, "getParameterAnnotations");

end Java.Lang.Reflect.Method;
pragma Import (Java, Java.Lang.Reflect.Method, "java.lang.reflect.Method");
pragma Extensions_Allowed (Off);
