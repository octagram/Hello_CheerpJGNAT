pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Lang.Reflect.Modifier is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Modifier (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsPublic (P1_Int : Java.Int)
                      return Java.Boolean;

   function IsPrivate (P1_Int : Java.Int)
                       return Java.Boolean;

   function IsProtected (P1_Int : Java.Int)
                         return Java.Boolean;

   function IsStatic (P1_Int : Java.Int)
                      return Java.Boolean;

   function IsFinal (P1_Int : Java.Int)
                     return Java.Boolean;

   function IsSynchronized (P1_Int : Java.Int)
                            return Java.Boolean;

   function IsVolatile (P1_Int : Java.Int)
                        return Java.Boolean;

   function IsTransient (P1_Int : Java.Int)
                         return Java.Boolean;

   function IsNative (P1_Int : Java.Int)
                      return Java.Boolean;

   function IsInterface (P1_Int : Java.Int)
                         return Java.Boolean;

   function IsAbstract (P1_Int : Java.Int)
                        return Java.Boolean;

   function IsStrict (P1_Int : Java.Int)
                      return Java.Boolean;

   function ToString (P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PUBLIC : constant Java.Int;

   --  final
   PRIVATE_K : constant Java.Int;

   --  final
   PROTECTED_K : constant Java.Int;

   --  final
   STATIC : constant Java.Int;

   --  final
   FINAL : constant Java.Int;

   --  final
   SYNCHRONIZED_K : constant Java.Int;

   --  final
   VOLATILE : constant Java.Int;

   --  final
   TRANSIENT : constant Java.Int;

   --  final
   NATIVE : constant Java.Int;

   --  final
   INTERFACE_K : constant Java.Int;

   --  final
   ABSTRACT_K : constant Java.Int;

   --  final
   STRICT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Modifier);
   pragma Import (Java, IsPublic, "isPublic");
   pragma Import (Java, IsPrivate, "isPrivate");
   pragma Import (Java, IsProtected, "isProtected");
   pragma Import (Java, IsStatic, "isStatic");
   pragma Import (Java, IsFinal, "isFinal");
   pragma Import (Java, IsSynchronized, "isSynchronized");
   pragma Import (Java, IsVolatile, "isVolatile");
   pragma Import (Java, IsTransient, "isTransient");
   pragma Import (Java, IsNative, "isNative");
   pragma Import (Java, IsInterface, "isInterface");
   pragma Import (Java, IsAbstract, "isAbstract");
   pragma Import (Java, IsStrict, "isStrict");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, PUBLIC, "PUBLIC");
   pragma Import (Java, PRIVATE_K, "PRIVATE");
   pragma Import (Java, PROTECTED_K, "PROTECTED");
   pragma Import (Java, STATIC, "STATIC");
   pragma Import (Java, FINAL, "FINAL");
   pragma Import (Java, SYNCHRONIZED_K, "SYNCHRONIZED");
   pragma Import (Java, VOLATILE, "VOLATILE");
   pragma Import (Java, TRANSIENT, "TRANSIENT");
   pragma Import (Java, NATIVE, "NATIVE");
   pragma Import (Java, INTERFACE_K, "INTERFACE");
   pragma Import (Java, ABSTRACT_K, "ABSTRACT");
   pragma Import (Java, STRICT, "STRICT");

end Java.Lang.Reflect.Modifier;
pragma Import (Java, Java.Lang.Reflect.Modifier, "java.lang.reflect.Modifier");
pragma Extensions_Allowed (Off);
