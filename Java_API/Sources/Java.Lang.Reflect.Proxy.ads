pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.Reflect.InvocationHandler;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Lang.Reflect.Proxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      H : access Java.Lang.Reflect.InvocationHandler.Typ'Class;
      pragma Import (Java, H, "h");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Proxy (P1_InvocationHandler : access Standard.Java.Lang.Reflect.InvocationHandler.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetProxyClass (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                           P2_Class_Arr : access Java.Lang.Class.Arr_Obj)
                           return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function NewProxyInstance (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                              P2_Class_Arr : access Java.Lang.Class.Arr_Obj;
                              P3_InvocationHandler : access Standard.Java.Lang.Reflect.InvocationHandler.Typ'Class)
                              return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function IsProxyClass (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Java.Boolean;

   function GetInvocationHandler (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Lang.Reflect.InvocationHandler.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Proxy);
   pragma Import (Java, GetProxyClass, "getProxyClass");
   pragma Import (Java, NewProxyInstance, "newProxyInstance");
   pragma Import (Java, IsProxyClass, "isProxyClass");
   pragma Import (Java, GetInvocationHandler, "getInvocationHandler");

end Java.Lang.Reflect.Proxy;
pragma Import (Java, Java.Lang.Reflect.Proxy, "java.lang.reflect.Proxy");
pragma Extensions_Allowed (Off);
