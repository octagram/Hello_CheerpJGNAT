pragma Extensions_Allowed (On);
package Java.Lang.Reflect is
   pragma Preelaborate;
end Java.Lang.Reflect;
pragma Import (Java, Java.Lang.Reflect, "java.lang.reflect");
pragma Extensions_Allowed (Off);
