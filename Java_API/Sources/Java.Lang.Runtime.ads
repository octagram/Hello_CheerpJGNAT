pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.Process;
limited with Java.Lang.String;
limited with Java.Lang.Thread;
with Java.Lang.Object;

package Java.Lang.Runtime is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRuntime return access Java.Lang.Runtime.Typ'Class;

   procedure exit_K (This : access Typ;
                     P1_Int : Java.Int);

   procedure AddShutdownHook (This : access Typ;
                              P1_Thread : access Standard.Java.Lang.Thread.Typ'Class);

   function RemoveShutdownHook (This : access Typ;
                                P1_Thread : access Standard.Java.Lang.Thread.Typ'Class)
                                return Java.Boolean;

   procedure Halt (This : access Typ;
                   P1_Int : Java.Int);

   function Exec (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Java.Lang.Process.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Exec (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String_Arr : access Java.Lang.String.Arr_Obj)
                  return access Java.Lang.Process.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Exec (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String_Arr : access Java.Lang.String.Arr_Obj;
                  P3_File : access Standard.Java.Io.File.Typ'Class)
                  return access Java.Lang.Process.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Exec (This : access Typ;
                  P1_String_Arr : access Java.Lang.String.Arr_Obj)
                  return access Java.Lang.Process.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Exec (This : access Typ;
                  P1_String_Arr : access Java.Lang.String.Arr_Obj;
                  P2_String_Arr : access Java.Lang.String.Arr_Obj)
                  return access Java.Lang.Process.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Exec (This : access Typ;
                  P1_String_Arr : access Java.Lang.String.Arr_Obj;
                  P2_String_Arr : access Java.Lang.String.Arr_Obj;
                  P3_File : access Standard.Java.Io.File.Typ'Class)
                  return access Java.Lang.Process.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function AvailableProcessors (This : access Typ)
                                 return Java.Int;

   function FreeMemory (This : access Typ)
                        return Java.Long;

   function TotalMemory (This : access Typ)
                         return Java.Long;

   function MaxMemory (This : access Typ)
                       return Java.Long;

   procedure Gc (This : access Typ);

   procedure RunFinalization (This : access Typ);

   procedure TraceInstructions (This : access Typ;
                                P1_Boolean : Java.Boolean);

   procedure TraceMethodCalls (This : access Typ;
                               P1_Boolean : Java.Boolean);

   procedure Load (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure LoadLibrary (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetRuntime, "getRuntime");
   pragma Import (Java, exit_K, "exit");
   pragma Import (Java, AddShutdownHook, "addShutdownHook");
   pragma Import (Java, RemoveShutdownHook, "removeShutdownHook");
   pragma Import (Java, Halt, "halt");
   pragma Import (Java, Exec, "exec");
   pragma Import (Java, AvailableProcessors, "availableProcessors");
   pragma Import (Java, FreeMemory, "freeMemory");
   pragma Import (Java, TotalMemory, "totalMemory");
   pragma Import (Java, MaxMemory, "maxMemory");
   pragma Import (Java, Gc, "gc");
   pragma Import (Java, RunFinalization, "runFinalization");
   pragma Import (Java, TraceInstructions, "traceInstructions");
   pragma Import (Java, TraceMethodCalls, "traceMethodCalls");
   pragma Import (Java, Load, "load");
   pragma Import (Java, LoadLibrary, "loadLibrary");

end Java.Lang.Runtime;
pragma Import (Java, Java.Lang.Runtime, "java.lang.Runtime");
pragma Extensions_Allowed (Off);
