pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.BasicPermission;
with Java.Security.Guard;

package Java.Lang.RuntimePermission is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Guard_I : Java.Security.Guard.Ref)
    is new Java.Security.BasicPermission.Typ(Serializable_I,
                                             Guard_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RuntimePermission (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_RuntimePermission (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RuntimePermission);

end Java.Lang.RuntimePermission;
pragma Import (Java, Java.Lang.RuntimePermission, "java.lang.RuntimePermission");
pragma Extensions_Allowed (Off);
