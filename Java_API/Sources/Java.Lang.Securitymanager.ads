pragma Extensions_Allowed (On);
limited with Java.Io.FileDescriptor;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Lang.Thread;
limited with Java.Lang.ThreadGroup;
limited with Java.Net.InetAddress;
limited with Java.Security.Permission;
with Java.Lang.Object;

package Java.Lang.SecurityManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SecurityManager (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetClassContext (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetSecurityContext (This : access Typ)
                                return access Java.Lang.Object.Typ'Class;

   procedure CheckPermission (This : access Typ;
                              P1_Permission : access Standard.Java.Security.Permission.Typ'Class);

   procedure CheckPermission (This : access Typ;
                              P1_Permission : access Standard.Java.Security.Permission.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure CheckCreateClassLoader (This : access Typ);

   procedure CheckAccess (This : access Typ;
                          P1_Thread : access Standard.Java.Lang.Thread.Typ'Class);

   procedure CheckAccess (This : access Typ;
                          P1_ThreadGroup : access Standard.Java.Lang.ThreadGroup.Typ'Class);

   procedure CheckExit (This : access Typ;
                        P1_Int : Java.Int);

   procedure CheckExec (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure CheckLink (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure CheckRead (This : access Typ;
                        P1_FileDescriptor : access Standard.Java.Io.FileDescriptor.Typ'Class);

   procedure CheckRead (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure CheckRead (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure CheckWrite (This : access Typ;
                         P1_FileDescriptor : access Standard.Java.Io.FileDescriptor.Typ'Class);

   procedure CheckWrite (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure CheckDelete (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure CheckConnect (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int);

   procedure CheckConnect (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure CheckListen (This : access Typ;
                          P1_Int : Java.Int);

   procedure CheckAccept (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int);

   procedure CheckMulticast (This : access Typ;
                             P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class);

   procedure CheckPropertiesAccess (This : access Typ);

   procedure CheckPropertyAccess (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   function CheckTopLevelWindow (This : access Typ;
                                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                 return Java.Boolean;

   procedure CheckPrintJobAccess (This : access Typ);

   procedure CheckSystemClipboardAccess (This : access Typ);

   procedure CheckAwtEventQueueAccess (This : access Typ);

   procedure CheckPackageAccess (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure CheckPackageDefinition (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure CheckSetFactory (This : access Typ);

   procedure CheckMemberAccess (This : access Typ;
                                P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                P2_Int : Java.Int);

   procedure CheckSecurityAccess (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetThreadGroup (This : access Typ)
                            return access Java.Lang.ThreadGroup.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SecurityManager);
   pragma Import (Java, GetClassContext, "getClassContext");
   pragma Import (Java, GetSecurityContext, "getSecurityContext");
   pragma Import (Java, CheckPermission, "checkPermission");
   pragma Import (Java, CheckCreateClassLoader, "checkCreateClassLoader");
   pragma Import (Java, CheckAccess, "checkAccess");
   pragma Import (Java, CheckExit, "checkExit");
   pragma Import (Java, CheckExec, "checkExec");
   pragma Import (Java, CheckLink, "checkLink");
   pragma Import (Java, CheckRead, "checkRead");
   pragma Import (Java, CheckWrite, "checkWrite");
   pragma Import (Java, CheckDelete, "checkDelete");
   pragma Import (Java, CheckConnect, "checkConnect");
   pragma Import (Java, CheckListen, "checkListen");
   pragma Import (Java, CheckAccept, "checkAccept");
   pragma Import (Java, CheckMulticast, "checkMulticast");
   pragma Import (Java, CheckPropertiesAccess, "checkPropertiesAccess");
   pragma Import (Java, CheckPropertyAccess, "checkPropertyAccess");
   pragma Import (Java, CheckTopLevelWindow, "checkTopLevelWindow");
   pragma Import (Java, CheckPrintJobAccess, "checkPrintJobAccess");
   pragma Import (Java, CheckSystemClipboardAccess, "checkSystemClipboardAccess");
   pragma Import (Java, CheckAwtEventQueueAccess, "checkAwtEventQueueAccess");
   pragma Import (Java, CheckPackageAccess, "checkPackageAccess");
   pragma Import (Java, CheckPackageDefinition, "checkPackageDefinition");
   pragma Import (Java, CheckSetFactory, "checkSetFactory");
   pragma Import (Java, CheckMemberAccess, "checkMemberAccess");
   pragma Import (Java, CheckSecurityAccess, "checkSecurityAccess");
   pragma Import (Java, GetThreadGroup, "getThreadGroup");

end Java.Lang.SecurityManager;
pragma Import (Java, Java.Lang.SecurityManager, "java.lang.SecurityManager");
pragma Extensions_Allowed (Off);
