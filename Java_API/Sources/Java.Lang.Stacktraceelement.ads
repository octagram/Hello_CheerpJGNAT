pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Lang.StackTraceElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StackTraceElement (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                                   P4_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFileName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetLineNumber (This : access Typ)
                           return Java.Int;

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetMethodName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function IsNativeMethod (This : access Typ)
                            return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StackTraceElement);
   pragma Import (Java, GetFileName, "getFileName");
   pragma Import (Java, GetLineNumber, "getLineNumber");
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetMethodName, "getMethodName");
   pragma Import (Java, IsNativeMethod, "isNativeMethod");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Lang.StackTraceElement;
pragma Import (Java, Java.Lang.StackTraceElement, "java.lang.StackTraceElement");
pragma Extensions_Allowed (Off);
