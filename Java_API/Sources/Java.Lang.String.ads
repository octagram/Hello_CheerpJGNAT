pragma Extensions_Allowed (On);
limited with Java.Lang.StringBuffer;
limited with Java.Lang.StringBuilder;
limited with Java.Nio.Charset.Charset;
limited with Java.Util.Comparator;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.CharSequence;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Lang.String is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            CharSequence_I : Java.Lang.CharSequence.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;
--------------------------------------------------
   -- Java String to Ada String Conversion Routine --
   --------------------------------------------------
type String_Access is access all Standard.String;
   function "+" (S : Ref) return String_Access;
   function "+" (S : Standard.String) return Ref;
   
   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_String (This : Ref := null)
                        return Ref;

   function New_String (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_Char_Arr : Java.Char_Arr; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_Char_Arr : Java.Char_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_Int_Arr : Java.Int_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   function New_String (P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_Byte_Arr : Java.Byte_Arr;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   function New_String (P1_Byte_Arr : Java.Byte_Arr;
                        P2_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_Byte_Arr : Java.Byte_Arr; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_String (P1_StringBuilder : access Standard.Java.Lang.StringBuilder.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Length (This : access Typ)
                    return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function CharAt (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Char;

   function CodePointAt (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function CodePointBefore (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   function CodePointCount (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Int;

   function OffsetByCodePoints (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return Java.Int;

   procedure GetChars (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Char_Arr : Java.Char_Arr;
                       P4_Int : Java.Int);

   function GetBytes (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Byte_Arr;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   function GetBytes (This : access Typ;
                      P1_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class)
                      return Java.Byte_Arr;

   function GetBytes (This : access Typ)
                      return Java.Byte_Arr;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ContentEquals (This : access Typ;
                           P1_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class)
                           return Java.Boolean;

   function ContentEquals (This : access Typ;
                           P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                           return Java.Boolean;

   function EqualsIgnoreCase (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Int;

   function CompareToIgnoreCase (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return Java.Int;

   function RegionMatches (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int)
                           return Java.Boolean;

   function RegionMatches (This : access Typ;
                           P1_Boolean : Java.Boolean;
                           P2_Int : Java.Int;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_Int : Java.Int;
                           P5_Int : Java.Int)
                           return Java.Boolean;

   function StartsWith (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int)
                        return Java.Boolean;

   function StartsWith (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;

   function EndsWith (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function IndexOf (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   function IndexOf (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return Java.Int;

   function IndexOf (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Int;

   function IndexOf (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;

   function Substring (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;

   function Substring (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;

   function SubSequence (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return access Java.Lang.CharSequence.Typ'Class;

   function Concat (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.String.Typ'Class;

   function Replace (This : access Typ;
                     P1_Char : Java.Char;
                     P2_Char : Java.Char)
                     return access Java.Lang.String.Typ'Class;

   function Matches (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Boolean;

   function Contains (This : access Typ;
                      P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                      return Java.Boolean;

   function ReplaceFirst (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function ReplaceAll (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.String.Typ'Class;

   function Replace (This : access Typ;
                     P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                     P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function Split (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Int : Java.Int)
                   return Standard.Java.Lang.Object.Ref;

   function Split (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return Standard.Java.Lang.Object.Ref;

   function ToLowerCase (This : access Typ;
                         P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function ToLowerCase (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function ToUpperCase (This : access Typ;
                         P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function ToUpperCase (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function Trim (This : access Typ)
                  return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToCharArray (This : access Typ)
                         return Java.Char_Arr;

   function Format (P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.String.Typ'Class;

   function Format (P1_Locale : access Standard.Java.Util.Locale.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Char_Arr : Java.Char_Arr)
                     return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Char_Arr : Java.Char_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   function CopyValueOf (P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return access Java.Lang.String.Typ'Class;

   function CopyValueOf (P1_Char_Arr : Java.Char_Arr)
                         return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Boolean : Java.Boolean)
                     return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Char : Java.Char)
                     return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Long : Java.Long)
                     return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Float : Java.Float)
                     return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_Double : Java.Double)
                     return access Java.Lang.String.Typ'Class;

   function Intern (This : access Typ)
                    return access Java.Lang.String.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CASE_INSENSITIVE_ORDER : access Java.Util.Comparator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, "+", "jgnat.adalib.GNAT_libc.to_string");
   pragma Java_Constructor (New_String);
   pragma Import (Java, Length, "length");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, CharAt, "charAt");
   pragma Import (Java, CodePointAt, "codePointAt");
   pragma Import (Java, CodePointBefore, "codePointBefore");
   pragma Import (Java, CodePointCount, "codePointCount");
   pragma Import (Java, OffsetByCodePoints, "offsetByCodePoints");
   pragma Import (Java, GetChars, "getChars");
   pragma Import (Java, GetBytes, "getBytes");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ContentEquals, "contentEquals");
   pragma Import (Java, EqualsIgnoreCase, "equalsIgnoreCase");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, CompareToIgnoreCase, "compareToIgnoreCase");
   pragma Import (Java, RegionMatches, "regionMatches");
   pragma Import (Java, StartsWith, "startsWith");
   pragma Import (Java, EndsWith, "endsWith");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, Substring, "substring");
   pragma Import (Java, SubSequence, "subSequence");
   pragma Import (Java, Concat, "concat");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, Matches, "matches");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ReplaceFirst, "replaceFirst");
   pragma Import (Java, ReplaceAll, "replaceAll");
   pragma Import (Java, Split, "split");
   pragma Import (Java, ToLowerCase, "toLowerCase");
   pragma Import (Java, ToUpperCase, "toUpperCase");
   pragma Import (Java, Trim, "trim");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToCharArray, "toCharArray");
   pragma Import (Java, Format, "format");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, CopyValueOf, "copyValueOf");
   pragma Import (Java, Intern, "intern");
   pragma Import (Java, CASE_INSENSITIVE_ORDER, "CASE_INSENSITIVE_ORDER");

end Java.Lang.String;
pragma Import (Java, Java.Lang.String, "java.lang.String");
pragma Extensions_Allowed (Off);
