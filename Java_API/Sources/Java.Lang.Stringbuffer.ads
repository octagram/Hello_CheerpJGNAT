pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Appendable;
with Java.Lang.CharSequence;
with Java.Lang.Object;

package Java.Lang.StringBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref;
            CharSequence_I : Java.Lang.CharSequence.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringBuffer (This : Ref := null)
                              return Ref;

   function New_StringBuffer (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_StringBuffer (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StringBuffer (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Length (This : access Typ)
                    return Java.Int;

   --  synchronized
   function Capacity (This : access Typ)
                      return Java.Int;

   --  synchronized
   procedure EnsureCapacity (This : access Typ;
                             P1_Int : Java.Int);

   --  synchronized
   procedure TrimToSize (This : access Typ);

   --  synchronized
   procedure SetLength (This : access Typ;
                        P1_Int : Java.Int);

   --  synchronized
   function CharAt (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Char;

   --  synchronized
   function CodePointAt (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   --  synchronized
   function CodePointBefore (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   --  synchronized
   function CodePointCount (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Int;

   --  synchronized
   function OffsetByCodePoints (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return Java.Int;

   --  synchronized
   procedure GetChars (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Char_Arr : Java.Char_Arr;
                       P4_Int : Java.Int);

   --  synchronized
   procedure SetCharAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Char : Java.Char);

   --  synchronized
   function Append (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Boolean : Java.Boolean)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function AppendCodePoint (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Long : Java.Long)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Float : Java.Float)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Append (This : access Typ;
                    P1_Double : Java.Double)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Delete (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function DeleteCharAt (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Replace (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Substring (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;

   --  synchronized
   function SubSequence (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return access Java.Lang.CharSequence.Typ'Class;

   --  synchronized
   function Substring (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;

   --  synchronized
   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Char_Arr : Java.Char_Arr;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Char_Arr : Java.Char_Arr)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Boolean : Java.Boolean)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Char : Java.Char)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Long : Java.Long)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Float : Java.Float)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Double : Java.Double)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function IndexOf (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Int;

   --  synchronized
   function IndexOf (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Int;

   --  synchronized
   function LastIndexOf (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;

   --  synchronized
   function reverse_K (This : access Typ)
                       return access Java.Lang.StringBuffer.Typ'Class;

   --  synchronized
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringBuffer);
   pragma Import (Java, Length, "length");
   pragma Import (Java, Capacity, "capacity");
   pragma Import (Java, EnsureCapacity, "ensureCapacity");
   pragma Import (Java, TrimToSize, "trimToSize");
   pragma Import (Java, SetLength, "setLength");
   pragma Import (Java, CharAt, "charAt");
   pragma Import (Java, CodePointAt, "codePointAt");
   pragma Import (Java, CodePointBefore, "codePointBefore");
   pragma Import (Java, CodePointCount, "codePointCount");
   pragma Import (Java, OffsetByCodePoints, "offsetByCodePoints");
   pragma Import (Java, GetChars, "getChars");
   pragma Import (Java, SetCharAt, "setCharAt");
   pragma Import (Java, Append, "append");
   pragma Import (Java, AppendCodePoint, "appendCodePoint");
   pragma Import (Java, Delete, "delete");
   pragma Import (Java, DeleteCharAt, "deleteCharAt");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, Substring, "substring");
   pragma Import (Java, SubSequence, "subSequence");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, reverse_K, "reverse");
   pragma Import (Java, ToString, "toString");

end Java.Lang.StringBuffer;
pragma Import (Java, Java.Lang.StringBuffer, "java.lang.StringBuffer");
pragma Extensions_Allowed (Off);
