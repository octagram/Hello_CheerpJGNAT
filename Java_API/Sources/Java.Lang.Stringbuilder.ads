pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
with Java.Io.Serializable;
with Java.Lang.Appendable;
with Java.Lang.CharSequence;
with Java.Lang.Object;

package Java.Lang.StringBuilder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref;
            CharSequence_I : Java.Lang.CharSequence.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringBuilder (This : Ref := null)
                               return Ref;

   function New_StringBuilder (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   function New_StringBuilder (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_StringBuilder (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Append (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Boolean : Java.Boolean)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Long : Java.Long)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Float : Java.Float)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Append (This : access Typ;
                    P1_Double : Java.Double)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function AppendCodePoint (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Lang.StringBuilder.Typ'Class;

   function Delete (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function DeleteCharAt (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.StringBuilder.Typ'Class;

   function Replace (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Char_Arr : Java.Char_Arr;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Char_Arr : Java.Char_Arr)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Boolean : Java.Boolean)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Char : Java.Char)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Long : Java.Long)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Float : Java.Float)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function Insert (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Double : Java.Double)
                    return access Java.Lang.StringBuilder.Typ'Class;

   function IndexOf (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Int;

   function IndexOf (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;

   function reverse_K (This : access Typ)
                       return access Java.Lang.StringBuilder.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Substring (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;

   function SubSequence (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return access Java.Lang.CharSequence.Typ'Class;

   function Substring (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;

   procedure SetCharAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Char : Java.Char);

   procedure GetChars (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Char_Arr : Java.Char_Arr;
                       P4_Int : Java.Int);

   function OffsetByCodePoints (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return Java.Int;

   function CodePointCount (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Int;

   function CodePointBefore (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   function CodePointAt (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function CharAt (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Char;

   procedure SetLength (This : access Typ;
                        P1_Int : Java.Int);

   procedure TrimToSize (This : access Typ);

   procedure EnsureCapacity (This : access Typ;
                             P1_Int : Java.Int);

   function Capacity (This : access Typ)
                      return Java.Int;

   function Length (This : access Typ)
                    return Java.Int;

   function Append (This : access Typ;
                    P1_Char : Java.Char)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Append (This : access Typ;
                    P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return access Java.Lang.Appendable.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringBuilder);
   pragma Import (Java, Append, "append");
   pragma Import (Java, AppendCodePoint, "appendCodePoint");
   pragma Import (Java, Delete, "delete");
   pragma Import (Java, DeleteCharAt, "deleteCharAt");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, reverse_K, "reverse");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Substring, "substring");
   pragma Import (Java, SubSequence, "subSequence");
   pragma Import (Java, SetCharAt, "setCharAt");
   pragma Import (Java, GetChars, "getChars");
   pragma Import (Java, OffsetByCodePoints, "offsetByCodePoints");
   pragma Import (Java, CodePointCount, "codePointCount");
   pragma Import (Java, CodePointBefore, "codePointBefore");
   pragma Import (Java, CodePointAt, "codePointAt");
   pragma Import (Java, CharAt, "charAt");
   pragma Import (Java, SetLength, "setLength");
   pragma Import (Java, TrimToSize, "trimToSize");
   pragma Import (Java, EnsureCapacity, "ensureCapacity");
   pragma Import (Java, Capacity, "capacity");
   pragma Import (Java, Length, "length");

end Java.Lang.StringBuilder;
pragma Import (Java, Java.Lang.StringBuilder, "java.lang.StringBuilder");
pragma Extensions_Allowed (Off);
