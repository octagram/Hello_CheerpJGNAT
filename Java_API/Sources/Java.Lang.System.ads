pragma Extensions_Allowed (On);
limited with Java.Io.Console;
limited with Java.Io.InputStream;
limited with Java.Io.PrintStream;
limited with Java.Lang.SecurityManager;
limited with Java.Lang.String;
limited with Java.Nio.Channels.Channel;
limited with Java.Util.Map;
limited with Java.Util.Properties;
with Java.Lang.Object;

package Java.Lang.System is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetIn (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);

   procedure SetOut (P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   procedure SetErr (P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   function Console return access Java.Io.Console.Typ'Class;

   function InheritedChannel return access Java.Nio.Channels.Channel.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure SetSecurityManager (P1_SecurityManager : access Standard.Java.Lang.SecurityManager.Typ'Class);

   function GetSecurityManager return access Java.Lang.SecurityManager.Typ'Class;

   function CurrentTimeMillis return Java.Long;

   function NanoTime return Java.Long;

   procedure Arraycopy (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   function IdentityHashCode (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                              return Java.Int;

   function GetProperties return access Java.Util.Properties.Typ'Class;

   procedure SetProperties (P1_Properties : access Standard.Java.Util.Properties.Typ'Class);

   function GetProperty (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function GetProperty (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function SetProperty (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function ClearProperty (P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.String.Typ'Class;

   function Getenv (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.String.Typ'Class;

   function Getenv return access Java.Util.Map.Typ'Class;

   procedure exit_K (P1_Int : Java.Int);

   procedure Gc ;

   procedure RunFinalization ;

   procedure Load (P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure LoadLibrary (P1_String : access Standard.Java.Lang.String.Typ'Class);

   function MapLibraryName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   in_K : access Java.Io.InputStream.Typ'Class;

   --  final
   out_K : access Java.Io.PrintStream.Typ'Class;

   --  final
   Err : access Java.Io.PrintStream.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetIn, "setIn");
   pragma Import (Java, SetOut, "setOut");
   pragma Import (Java, SetErr, "setErr");
   pragma Import (Java, Console, "console");
   pragma Import (Java, InheritedChannel, "inheritedChannel");
   pragma Import (Java, SetSecurityManager, "setSecurityManager");
   pragma Import (Java, GetSecurityManager, "getSecurityManager");
   pragma Import (Java, CurrentTimeMillis, "currentTimeMillis");
   pragma Import (Java, NanoTime, "nanoTime");
   pragma Import (Java, Arraycopy, "arraycopy");
   pragma Import (Java, IdentityHashCode, "identityHashCode");
   pragma Import (Java, GetProperties, "getProperties");
   pragma Import (Java, SetProperties, "setProperties");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, ClearProperty, "clearProperty");
   pragma Import (Java, Getenv, "getenv");
   pragma Import (Java, exit_K, "exit");
   pragma Import (Java, Gc, "gc");
   pragma Import (Java, RunFinalization, "runFinalization");
   pragma Import (Java, Load, "load");
   pragma Import (Java, LoadLibrary, "loadLibrary");
   pragma Import (Java, MapLibraryName, "mapLibraryName");
   pragma Import (Java, in_K, "in");
   pragma Import (Java, out_K, "out");
   pragma Import (Java, Err, "err");

end Java.Lang.System;
pragma Import (Java, Java.Lang.System, "java.lang.System");
pragma Extensions_Allowed (Off);
