pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.StackTraceElement;
limited with Java.Lang.String;
limited with Java.Lang.Thread.State;
limited with Java.Lang.Thread.UncaughtExceptionHandler;
limited with Java.Lang.ThreadGroup;
limited with Java.Util.Map;
with Java.Lang.Object;
with Java.Lang.Runnable;

package Java.Lang.Thread is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Runnable_I : Java.Lang.Runnable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CurrentThread return access Java.Lang.Thread.Typ'Class;

   procedure Yield ;

   procedure Sleep (P1_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   procedure Sleep (P1_Long : Java.Long;
                    P2_Int : Java.Int);
   --  can raise Java.Lang.InterruptedException.Except

   --  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Thread (This : Ref := null)
                        return Ref;

   function New_Thread (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Thread (P1_ThreadGroup : access Standard.Java.Lang.ThreadGroup.Typ'Class;
                        P2_Runnable : access Standard.Java.Lang.Runnable.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Thread (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Thread (P1_ThreadGroup : access Standard.Java.Lang.ThreadGroup.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Thread (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Thread (P1_ThreadGroup : access Standard.Java.Lang.ThreadGroup.Typ'Class;
                        P2_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Thread (P1_ThreadGroup : access Standard.Java.Lang.ThreadGroup.Typ'Class;
                        P2_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                        P4_Long : Java.Long; 
                        This : Ref := null)
                        return Ref;

   --  synchronized
   procedure Start (This : access Typ);

   procedure Run (This : access Typ);

   procedure Interrupt (This : access Typ);

   function Interrupted return Java.Boolean;

   function IsInterrupted (This : access Typ)
                           return Java.Boolean;

   --  final
   function IsAlive (This : access Typ)
                     return Java.Boolean;

   --  final
   procedure SetPriority (This : access Typ;
                          P1_Int : Java.Int);

   --  final
   function GetPriority (This : access Typ)
                         return Java.Int;

   --  final
   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetThreadGroup (This : access Typ)
                            return access Java.Lang.ThreadGroup.Typ'Class;

   function ActiveCount return Java.Int;

   function Enumerate (P1_Thread_Arr : access Java.Lang.Thread.Arr_Obj)
                       return Java.Int;

   --  final  synchronized
   procedure Join (This : access Typ;
                   P1_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   --  final  synchronized
   procedure Join (This : access Typ;
                   P1_Long : Java.Long;
                   P2_Int : Java.Int);
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   procedure Join (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   procedure DumpStack ;

   --  final
   procedure SetDaemon (This : access Typ;
                        P1_Boolean : Java.Boolean);

   --  final
   function IsDaemon (This : access Typ)
                      return Java.Boolean;

   --  final
   procedure CheckAccess (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetContextClassLoader (This : access Typ)
                                   return access Java.Lang.ClassLoader.Typ'Class;

   procedure SetContextClassLoader (This : access Typ;
                                    P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class);

   function HoldsLock (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   function GetStackTrace (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function GetAllStackTraces return access Java.Util.Map.Typ'Class;

   function GetId (This : access Typ)
                   return Java.Long;

   function GetState (This : access Typ)
                      return access Java.Lang.Thread.State.Typ'Class;

   procedure SetDefaultUncaughtExceptionHandler (P1_UncaughtExceptionHandler : access Standard.Java.Lang.Thread.UncaughtExceptionHandler.Typ'Class);

   function GetDefaultUncaughtExceptionHandler return access Java.Lang.Thread.UncaughtExceptionHandler.Typ'Class;

   function GetUncaughtExceptionHandler (This : access Typ)
                                         return access Java.Lang.Thread.UncaughtExceptionHandler.Typ'Class;

   procedure SetUncaughtExceptionHandler (This : access Typ;
                                          P1_UncaughtExceptionHandler : access Standard.Java.Lang.Thread.UncaughtExceptionHandler.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MIN_PRIORITY : constant Java.Int;

   --  final
   NORM_PRIORITY : constant Java.Int;

   --  final
   MAX_PRIORITY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CurrentThread, "currentThread");
   pragma Import (Java, Yield, "yield");
   pragma Import (Java, Sleep, "sleep");
   pragma Import (Java, Clone, "clone");
   pragma Java_Constructor (New_Thread);
   pragma Import (Java, Start, "start");
   pragma Import (Java, Run, "run");
   pragma Import (Java, Interrupt, "interrupt");
   pragma Import (Java, Interrupted, "interrupted");
   pragma Import (Java, IsInterrupted, "isInterrupted");
   pragma Import (Java, IsAlive, "isAlive");
   pragma Import (Java, SetPriority, "setPriority");
   pragma Import (Java, GetPriority, "getPriority");
   pragma Import (Java, SetName, "setName");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetThreadGroup, "getThreadGroup");
   pragma Import (Java, ActiveCount, "activeCount");
   pragma Import (Java, Enumerate, "enumerate");
   pragma Import (Java, Join, "join");
   pragma Import (Java, DumpStack, "dumpStack");
   pragma Import (Java, SetDaemon, "setDaemon");
   pragma Import (Java, IsDaemon, "isDaemon");
   pragma Import (Java, CheckAccess, "checkAccess");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetContextClassLoader, "getContextClassLoader");
   pragma Import (Java, SetContextClassLoader, "setContextClassLoader");
   pragma Import (Java, HoldsLock, "holdsLock");
   pragma Import (Java, GetStackTrace, "getStackTrace");
   pragma Import (Java, GetAllStackTraces, "getAllStackTraces");
   pragma Import (Java, GetId, "getId");
   pragma Import (Java, GetState, "getState");
   pragma Import (Java, SetDefaultUncaughtExceptionHandler, "setDefaultUncaughtExceptionHandler");
   pragma Import (Java, GetDefaultUncaughtExceptionHandler, "getDefaultUncaughtExceptionHandler");
   pragma Import (Java, GetUncaughtExceptionHandler, "getUncaughtExceptionHandler");
   pragma Import (Java, SetUncaughtExceptionHandler, "setUncaughtExceptionHandler");
   pragma Import (Java, MIN_PRIORITY, "MIN_PRIORITY");
   pragma Import (Java, NORM_PRIORITY, "NORM_PRIORITY");
   pragma Import (Java, MAX_PRIORITY, "MAX_PRIORITY");

end Java.Lang.Thread;
pragma Import (Java, Java.Lang.Thread, "java.lang.Thread");
pragma Extensions_Allowed (Off);
