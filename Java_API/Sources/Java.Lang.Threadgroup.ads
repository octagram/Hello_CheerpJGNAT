pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Lang.Object;
with Java.Lang.Thread.UncaughtExceptionHandler;
with Java.Lang.Thread;

package Java.Lang.ThreadGroup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(UncaughtExceptionHandler_I : Java.Lang.Thread.UncaughtExceptionHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ThreadGroup (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_ThreadGroup (P1_ThreadGroup : access Standard.Java.Lang.ThreadGroup.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetParent (This : access Typ)
                       return access Java.Lang.ThreadGroup.Typ'Class;

   --  final
   function GetMaxPriority (This : access Typ)
                            return Java.Int;

   --  final
   function IsDaemon (This : access Typ)
                      return Java.Boolean;

   --  synchronized
   function IsDestroyed (This : access Typ)
                         return Java.Boolean;

   --  final
   procedure SetDaemon (This : access Typ;
                        P1_Boolean : Java.Boolean);

   --  final
   procedure SetMaxPriority (This : access Typ;
                             P1_Int : Java.Int);

   --  final
   function ParentOf (This : access Typ;
                      P1_ThreadGroup : access Standard.Java.Lang.ThreadGroup.Typ'Class)
                      return Java.Boolean;

   --  final
   procedure CheckAccess (This : access Typ);

   function ActiveCount (This : access Typ)
                         return Java.Int;

   function Enumerate (This : access Typ;
                       P1_Thread_Arr : access Java.Lang.Thread.Arr_Obj)
                       return Java.Int;

   function Enumerate (This : access Typ;
                       P1_Thread_Arr : access Java.Lang.Thread.Arr_Obj;
                       P2_Boolean : Java.Boolean)
                       return Java.Int;

   function ActiveGroupCount (This : access Typ)
                              return Java.Int;

   function Enumerate (This : access Typ;
                       P1_ThreadGroup_Arr : access Java.Lang.ThreadGroup.Arr_Obj)
                       return Java.Int;

   function Enumerate (This : access Typ;
                       P1_ThreadGroup_Arr : access Java.Lang.ThreadGroup.Arr_Obj;
                       P2_Boolean : Java.Boolean)
                       return Java.Int;

   --  final
   procedure Interrupt (This : access Typ);

   --  final
   procedure Destroy (This : access Typ);

   procedure List (This : access Typ);

   procedure UncaughtException (This : access Typ;
                                P1_Thread : access Standard.Java.Lang.Thread.Typ'Class;
                                P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ThreadGroup);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetMaxPriority, "getMaxPriority");
   pragma Import (Java, IsDaemon, "isDaemon");
   pragma Import (Java, IsDestroyed, "isDestroyed");
   pragma Import (Java, SetDaemon, "setDaemon");
   pragma Import (Java, SetMaxPriority, "setMaxPriority");
   pragma Import (Java, ParentOf, "parentOf");
   pragma Import (Java, CheckAccess, "checkAccess");
   pragma Import (Java, ActiveCount, "activeCount");
   pragma Import (Java, Enumerate, "enumerate");
   pragma Import (Java, ActiveGroupCount, "activeGroupCount");
   pragma Import (Java, Interrupt, "interrupt");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, List, "list");
   pragma Import (Java, UncaughtException, "uncaughtException");
   pragma Import (Java, ToString, "toString");

end Java.Lang.ThreadGroup;
pragma Import (Java, Java.Lang.ThreadGroup, "java.lang.ThreadGroup");
pragma Extensions_Allowed (Off);
