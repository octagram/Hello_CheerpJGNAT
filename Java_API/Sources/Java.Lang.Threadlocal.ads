pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Lang.ThreadLocal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function InitialValue (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ThreadLocal (This : Ref := null)
                             return Ref;

   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;

   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Remove (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, InitialValue, "initialValue");
   pragma Java_Constructor (New_ThreadLocal);
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Remove, "remove");

end Java.Lang.ThreadLocal;
pragma Import (Java, Java.Lang.ThreadLocal, "java.lang.ThreadLocal");
pragma Extensions_Allowed (Off);
