pragma Extensions_Allowed (On);
limited with Java.Io.PrintStream;
limited with Java.Io.PrintWriter;
limited with Java.Lang.StackTraceElement;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Lang.Throwable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Throwable (This : Ref := null)
                           return Ref;

   function New_Throwable (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Throwable (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Throwable (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetLocalizedMessage (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;

   --  synchronized
   function InitCause (This : access Typ;
                       P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class)
                       return access Java.Lang.Throwable.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure PrintStackTrace (This : access Typ);

   procedure PrintStackTrace (This : access Typ;
                              P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   procedure PrintStackTrace (This : access Typ;
                              P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class);

   --  synchronized
   function FillInStackTrace (This : access Typ)
                              return access Java.Lang.Throwable.Typ'Class;

   function GetStackTrace (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   procedure SetStackTrace (This : access Typ;
                            P1_StackTraceElement_Arr : access Java.Lang.StackTraceElement.Arr_Obj);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Throwable);
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, GetLocalizedMessage, "getLocalizedMessage");
   pragma Import (Java, GetCause, "getCause");
   pragma Import (Java, InitCause, "initCause");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, PrintStackTrace, "printStackTrace");
   pragma Import (Java, FillInStackTrace, "fillInStackTrace");
   pragma Import (Java, GetStackTrace, "getStackTrace");
   pragma Import (Java, SetStackTrace, "setStackTrace");

end Java.Lang.Throwable;
pragma Import (Java, Java.Lang.Throwable, "java.lang.Throwable");
pragma Extensions_Allowed (Off);
