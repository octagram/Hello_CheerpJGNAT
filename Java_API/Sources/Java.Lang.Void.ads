pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
with Java.Lang.Object;

package Java.Lang.Void is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_K : access Java.Lang.Class.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, TYPE_K, "TYPE");

end Java.Lang.Void;
pragma Import (Java, Java.Lang.Void, "java.lang.Void");
pragma Extensions_Allowed (Off);
