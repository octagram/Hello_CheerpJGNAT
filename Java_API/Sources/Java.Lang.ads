pragma Extensions_Allowed (On);
package Java.Lang is
   pragma Preelaborate;
end Java.Lang;
pragma Import (Java, Java.Lang, "java.lang");
pragma Extensions_Allowed (Off);
