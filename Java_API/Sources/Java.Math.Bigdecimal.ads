pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Math.MathContext;
limited with Java.Math.RoundingMode;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Number;
with Java.Lang.Object;

package Java.Math.BigDecimal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Number.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BigDecimal (P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Char_Arr : Java.Char_Arr; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Char_Arr : Java.Char_Arr;
                            P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Double : Java.Double; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Double : Java.Double;
                            P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                            P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                            P2_Int : Java.Int;
                            P3_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Int : Java.Int;
                            P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Long : Java.Long; 
                            This : Ref := null)
                            return Ref;

   function New_BigDecimal (P1_Long : Java.Long;
                            P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ValueOf (P1_Long : Java.Long;
                     P2_Int : Java.Int)
                     return access Java.Math.BigDecimal.Typ'Class;

   function ValueOf (P1_Long : Java.Long)
                     return access Java.Math.BigDecimal.Typ'Class;

   function ValueOf (P1_Double : Java.Double)
                     return access Java.Math.BigDecimal.Typ'Class;

   function Add (This : access Typ;
                 P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                 return access Java.Math.BigDecimal.Typ'Class;

   function Add (This : access Typ;
                 P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                 P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                 return access Java.Math.BigDecimal.Typ'Class;

   function Subtract (This : access Typ;
                      P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                      return access Java.Math.BigDecimal.Typ'Class;

   function Subtract (This : access Typ;
                      P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                      P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                      return access Java.Math.BigDecimal.Typ'Class;

   function Multiply (This : access Typ;
                      P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                      return access Java.Math.BigDecimal.Typ'Class;

   function Multiply (This : access Typ;
                      P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                      P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                      return access Java.Math.BigDecimal.Typ'Class;

   function Divide (This : access Typ;
                    P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Java.Math.BigDecimal.Typ'Class;

   function Divide (This : access Typ;
                    P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                    P2_Int : Java.Int;
                    P3_RoundingMode : access Standard.Java.Math.RoundingMode.Typ'Class)
                    return access Java.Math.BigDecimal.Typ'Class;

   function Divide (This : access Typ;
                    P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                    P2_Int : Java.Int)
                    return access Java.Math.BigDecimal.Typ'Class;

   function Divide (This : access Typ;
                    P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                    P2_RoundingMode : access Standard.Java.Math.RoundingMode.Typ'Class)
                    return access Java.Math.BigDecimal.Typ'Class;

   function Divide (This : access Typ;
                    P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                    return access Java.Math.BigDecimal.Typ'Class;

   function Divide (This : access Typ;
                    P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                    P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                    return access Java.Math.BigDecimal.Typ'Class;

   function DivideToIntegralValue (This : access Typ;
                                   P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                                   return access Java.Math.BigDecimal.Typ'Class;

   function DivideToIntegralValue (This : access Typ;
                                   P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                                   P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                                   return access Java.Math.BigDecimal.Typ'Class;

   function Remainder (This : access Typ;
                       P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                       return access Java.Math.BigDecimal.Typ'Class;

   function Remainder (This : access Typ;
                       P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                       P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                       return access Java.Math.BigDecimal.Typ'Class;

   function DivideAndRemainder (This : access Typ;
                                P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                                return Standard.Java.Lang.Object.Ref;

   function DivideAndRemainder (This : access Typ;
                                P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                                P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                                return Standard.Java.Lang.Object.Ref;

   function Pow (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Math.BigDecimal.Typ'Class;

   function Pow (This : access Typ;
                 P1_Int : Java.Int;
                 P2_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                 return access Java.Math.BigDecimal.Typ'Class;

   function abs_K (This : access Typ)
                   return access Java.Math.BigDecimal.Typ'Class;

   function abs_K (This : access Typ;
                   P1_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                   return access Java.Math.BigDecimal.Typ'Class;

   function Negate (This : access Typ)
                    return access Java.Math.BigDecimal.Typ'Class;

   function Negate (This : access Typ;
                    P1_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                    return access Java.Math.BigDecimal.Typ'Class;

   function Plus (This : access Typ)
                  return access Java.Math.BigDecimal.Typ'Class;

   function Plus (This : access Typ;
                  P1_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                  return access Java.Math.BigDecimal.Typ'Class;

   function Signum (This : access Typ)
                    return Java.Int;

   function Scale (This : access Typ)
                   return Java.Int;

   function Precision (This : access Typ)
                       return Java.Int;

   function UnscaledValue (This : access Typ)
                           return access Java.Math.BigInteger.Typ'Class;

   function Round (This : access Typ;
                   P1_MathContext : access Standard.Java.Math.MathContext.Typ'Class)
                   return access Java.Math.BigDecimal.Typ'Class;

   function SetScale (This : access Typ;
                      P1_Int : Java.Int;
                      P2_RoundingMode : access Standard.Java.Math.RoundingMode.Typ'Class)
                      return access Java.Math.BigDecimal.Typ'Class;

   function SetScale (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return access Java.Math.BigDecimal.Typ'Class;

   function SetScale (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Math.BigDecimal.Typ'Class;

   function MovePointLeft (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Math.BigDecimal.Typ'Class;

   function MovePointRight (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Math.BigDecimal.Typ'Class;

   function ScaleByPowerOfTen (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Math.BigDecimal.Typ'Class;

   function StripTrailingZeros (This : access Typ)
                                return access Java.Math.BigDecimal.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                       return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Min (This : access Typ;
                 P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                 return access Java.Math.BigDecimal.Typ'Class;

   function Max (This : access Typ;
                 P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                 return access Java.Math.BigDecimal.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToEngineeringString (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function ToPlainString (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function ToBigInteger (This : access Typ)
                          return access Java.Math.BigInteger.Typ'Class;

   function ToBigIntegerExact (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;

   function LongValue (This : access Typ)
                       return Java.Long;

   function LongValueExact (This : access Typ)
                            return Java.Long;

   function IntValue (This : access Typ)
                      return Java.Int;

   function IntValueExact (This : access Typ)
                           return Java.Int;

   function ShortValueExact (This : access Typ)
                             return Java.Short;

   function ByteValueExact (This : access Typ)
                            return Java.Byte;

   function FloatValue (This : access Typ)
                        return Java.Float;

   function DoubleValue (This : access Typ)
                         return Java.Double;

   function Ulp (This : access Typ)
                 return access Java.Math.BigDecimal.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ZERO : access Java.Math.BigDecimal.Typ'Class;

   --  final
   ONE : access Java.Math.BigDecimal.Typ'Class;

   --  final
   TEN : access Java.Math.BigDecimal.Typ'Class;

   --  final
   ROUND_UP : constant Java.Int;

   --  final
   ROUND_DOWN : constant Java.Int;

   --  final
   ROUND_CEILING : constant Java.Int;

   --  final
   ROUND_FLOOR : constant Java.Int;

   --  final
   ROUND_HALF_UP : constant Java.Int;

   --  final
   ROUND_HALF_DOWN : constant Java.Int;

   --  final
   ROUND_HALF_EVEN : constant Java.Int;

   --  final
   ROUND_UNNECESSARY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BigDecimal);
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Subtract, "subtract");
   pragma Import (Java, Multiply, "multiply");
   pragma Import (Java, Divide, "divide");
   pragma Import (Java, DivideToIntegralValue, "divideToIntegralValue");
   pragma Import (Java, Remainder, "remainder");
   pragma Import (Java, DivideAndRemainder, "divideAndRemainder");
   pragma Import (Java, Pow, "pow");
   pragma Import (Java, abs_K, "abs");
   pragma Import (Java, Negate, "negate");
   pragma Import (Java, Plus, "plus");
   pragma Import (Java, Signum, "signum");
   pragma Import (Java, Scale, "scale");
   pragma Import (Java, Precision, "precision");
   pragma Import (Java, UnscaledValue, "unscaledValue");
   pragma Import (Java, Round, "round");
   pragma Import (Java, SetScale, "setScale");
   pragma Import (Java, MovePointLeft, "movePointLeft");
   pragma Import (Java, MovePointRight, "movePointRight");
   pragma Import (Java, ScaleByPowerOfTen, "scaleByPowerOfTen");
   pragma Import (Java, StripTrailingZeros, "stripTrailingZeros");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Min, "min");
   pragma Import (Java, Max, "max");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToEngineeringString, "toEngineeringString");
   pragma Import (Java, ToPlainString, "toPlainString");
   pragma Import (Java, ToBigInteger, "toBigInteger");
   pragma Import (Java, ToBigIntegerExact, "toBigIntegerExact");
   pragma Import (Java, LongValue, "longValue");
   pragma Import (Java, LongValueExact, "longValueExact");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, IntValueExact, "intValueExact");
   pragma Import (Java, ShortValueExact, "shortValueExact");
   pragma Import (Java, ByteValueExact, "byteValueExact");
   pragma Import (Java, FloatValue, "floatValue");
   pragma Import (Java, DoubleValue, "doubleValue");
   pragma Import (Java, Ulp, "ulp");
   pragma Import (Java, ZERO, "ZERO");
   pragma Import (Java, ONE, "ONE");
   pragma Import (Java, TEN, "TEN");
   pragma Import (Java, ROUND_UP, "ROUND_UP");
   pragma Import (Java, ROUND_DOWN, "ROUND_DOWN");
   pragma Import (Java, ROUND_CEILING, "ROUND_CEILING");
   pragma Import (Java, ROUND_FLOOR, "ROUND_FLOOR");
   pragma Import (Java, ROUND_HALF_UP, "ROUND_HALF_UP");
   pragma Import (Java, ROUND_HALF_DOWN, "ROUND_HALF_DOWN");
   pragma Import (Java, ROUND_HALF_EVEN, "ROUND_HALF_EVEN");
   pragma Import (Java, ROUND_UNNECESSARY, "ROUND_UNNECESSARY");

end Java.Math.BigDecimal;
pragma Import (Java, Java.Math.BigDecimal, "java.math.BigDecimal");
pragma Extensions_Allowed (Off);
