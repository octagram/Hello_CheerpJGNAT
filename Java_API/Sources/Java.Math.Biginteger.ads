pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Random;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Number;
with Java.Lang.Object;

package Java.Math.BigInteger is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Number.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BigInteger (P1_Byte_Arr : Java.Byte_Arr; 
                            This : Ref := null)
                            return Ref;

   function New_BigInteger (P1_Int : Java.Int;
                            P2_Byte_Arr : Java.Byte_Arr; 
                            This : Ref := null)
                            return Ref;

   function New_BigInteger (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_BigInteger (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigInteger (P1_Int : Java.Int;
                            P2_Random : access Standard.Java.Util.Random.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_BigInteger (P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Random : access Standard.Java.Util.Random.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ProbablePrime (P1_Int : Java.Int;
                           P2_Random : access Standard.Java.Util.Random.Typ'Class)
                           return access Java.Math.BigInteger.Typ'Class;

   function NextProbablePrime (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;

   function ValueOf (P1_Long : Java.Long)
                     return access Java.Math.BigInteger.Typ'Class;

   function Add (This : access Typ;
                 P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                 return access Java.Math.BigInteger.Typ'Class;

   function Subtract (This : access Typ;
                      P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                      return access Java.Math.BigInteger.Typ'Class;

   function Multiply (This : access Typ;
                      P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                      return access Java.Math.BigInteger.Typ'Class;

   function Divide (This : access Typ;
                    P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                    return access Java.Math.BigInteger.Typ'Class;

   function DivideAndRemainder (This : access Typ;
                                P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                                return Standard.Java.Lang.Object.Ref;

   function Remainder (This : access Typ;
                       P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                       return access Java.Math.BigInteger.Typ'Class;

   function Pow (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Math.BigInteger.Typ'Class;

   function Gcd (This : access Typ;
                 P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                 return access Java.Math.BigInteger.Typ'Class;

   function abs_K (This : access Typ)
                   return access Java.Math.BigInteger.Typ'Class;

   function Negate (This : access Typ)
                    return access Java.Math.BigInteger.Typ'Class;

   function Signum (This : access Typ)
                    return Java.Int;

   function mod_K (This : access Typ;
                   P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                   return access Java.Math.BigInteger.Typ'Class;

   function ModPow (This : access Typ;
                    P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                    P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                    return access Java.Math.BigInteger.Typ'Class;

   function ModInverse (This : access Typ;
                        P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                        return access Java.Math.BigInteger.Typ'Class;

   function ShiftLeft (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Math.BigInteger.Typ'Class;

   function ShiftRight (This : access Typ;
                        P1_Int : Java.Int)
                        return access Java.Math.BigInteger.Typ'Class;

   function and_K (This : access Typ;
                   P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                   return access Java.Math.BigInteger.Typ'Class;

   function or_K (This : access Typ;
                  P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                  return access Java.Math.BigInteger.Typ'Class;

   function xor_K (This : access Typ;
                   P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                   return access Java.Math.BigInteger.Typ'Class;

   function not_K (This : access Typ)
                   return access Java.Math.BigInteger.Typ'Class;

   function AndNot (This : access Typ;
                    P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                    return access Java.Math.BigInteger.Typ'Class;

   function TestBit (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Boolean;

   function SetBit (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Math.BigInteger.Typ'Class;

   function ClearBit (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Math.BigInteger.Typ'Class;

   function FlipBit (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Math.BigInteger.Typ'Class;

   function GetLowestSetBit (This : access Typ)
                             return Java.Int;

   function BitLength (This : access Typ)
                       return Java.Int;

   function BitCount (This : access Typ)
                      return Java.Int;

   function IsProbablePrime (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                       return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Min (This : access Typ;
                 P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                 return access Java.Math.BigInteger.Typ'Class;

   function Max (This : access Typ;
                 P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                 return access Java.Math.BigInteger.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToByteArray (This : access Typ)
                         return Java.Byte_Arr;

   function IntValue (This : access Typ)
                      return Java.Int;

   function LongValue (This : access Typ)
                       return Java.Long;

   function FloatValue (This : access Typ)
                        return Java.Float;

   function DoubleValue (This : access Typ)
                         return Java.Double;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ZERO : access Java.Math.BigInteger.Typ'Class;

   --  final
   ONE : access Java.Math.BigInteger.Typ'Class;

   --  final
   TEN : access Java.Math.BigInteger.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BigInteger);
   pragma Import (Java, ProbablePrime, "probablePrime");
   pragma Import (Java, NextProbablePrime, "nextProbablePrime");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Subtract, "subtract");
   pragma Import (Java, Multiply, "multiply");
   pragma Import (Java, Divide, "divide");
   pragma Import (Java, DivideAndRemainder, "divideAndRemainder");
   pragma Import (Java, Remainder, "remainder");
   pragma Import (Java, Pow, "pow");
   pragma Import (Java, Gcd, "gcd");
   pragma Import (Java, abs_K, "abs");
   pragma Import (Java, Negate, "negate");
   pragma Import (Java, Signum, "signum");
   pragma Import (Java, mod_K, "mod");
   pragma Import (Java, ModPow, "modPow");
   pragma Import (Java, ModInverse, "modInverse");
   pragma Import (Java, ShiftLeft, "shiftLeft");
   pragma Import (Java, ShiftRight, "shiftRight");
   pragma Import (Java, and_K, "and");
   pragma Import (Java, or_K, "or");
   pragma Import (Java, xor_K, "xor");
   pragma Import (Java, not_K, "not");
   pragma Import (Java, AndNot, "andNot");
   pragma Import (Java, TestBit, "testBit");
   pragma Import (Java, SetBit, "setBit");
   pragma Import (Java, ClearBit, "clearBit");
   pragma Import (Java, FlipBit, "flipBit");
   pragma Import (Java, GetLowestSetBit, "getLowestSetBit");
   pragma Import (Java, BitLength, "bitLength");
   pragma Import (Java, BitCount, "bitCount");
   pragma Import (Java, IsProbablePrime, "isProbablePrime");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Min, "min");
   pragma Import (Java, Max, "max");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToByteArray, "toByteArray");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, LongValue, "longValue");
   pragma Import (Java, FloatValue, "floatValue");
   pragma Import (Java, DoubleValue, "doubleValue");
   pragma Import (Java, ZERO, "ZERO");
   pragma Import (Java, ONE, "ONE");
   pragma Import (Java, TEN, "TEN");

end Java.Math.BigInteger;
pragma Import (Java, Java.Math.BigInteger, "java.math.BigInteger");
pragma Extensions_Allowed (Off);
