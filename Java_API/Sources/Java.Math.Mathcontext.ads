pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.RoundingMode;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Math.MathContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MathContext (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_MathContext (P1_Int : Java.Int;
                             P2_RoundingMode : access Standard.Java.Math.RoundingMode.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_MathContext (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrecision (This : access Typ)
                          return Java.Int;

   function GetRoundingMode (This : access Typ)
                             return access Java.Math.RoundingMode.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNLIMITED : access Java.Math.MathContext.Typ'Class;

   --  final
   DECIMAL32 : access Java.Math.MathContext.Typ'Class;

   --  final
   DECIMAL64 : access Java.Math.MathContext.Typ'Class;

   --  final
   DECIMAL128 : access Java.Math.MathContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MathContext);
   pragma Import (Java, GetPrecision, "getPrecision");
   pragma Import (Java, GetRoundingMode, "getRoundingMode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, UNLIMITED, "UNLIMITED");
   pragma Import (Java, DECIMAL32, "DECIMAL32");
   pragma Import (Java, DECIMAL64, "DECIMAL64");
   pragma Import (Java, DECIMAL128, "DECIMAL128");

end Java.Math.MathContext;
pragma Import (Java, Java.Math.MathContext, "java.math.MathContext");
pragma Extensions_Allowed (Off);
