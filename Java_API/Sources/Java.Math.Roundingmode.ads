pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Java.Math.RoundingMode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Math.RoundingMode.Typ'Class;

   function ValueOf (P1_Int : Java.Int)
                     return access Java.Math.RoundingMode.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UP : access Java.Math.RoundingMode.Typ'Class;

   --  final
   DOWN : access Java.Math.RoundingMode.Typ'Class;

   --  final
   CEILING : access Java.Math.RoundingMode.Typ'Class;

   --  final
   FLOOR : access Java.Math.RoundingMode.Typ'Class;

   --  final
   HALF_UP : access Java.Math.RoundingMode.Typ'Class;

   --  final
   HALF_DOWN : access Java.Math.RoundingMode.Typ'Class;

   --  final
   HALF_EVEN : access Java.Math.RoundingMode.Typ'Class;

   --  final
   UNNECESSARY : access Java.Math.RoundingMode.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, UP, "UP");
   pragma Import (Java, DOWN, "DOWN");
   pragma Import (Java, CEILING, "CEILING");
   pragma Import (Java, FLOOR, "FLOOR");
   pragma Import (Java, HALF_UP, "HALF_UP");
   pragma Import (Java, HALF_DOWN, "HALF_DOWN");
   pragma Import (Java, HALF_EVEN, "HALF_EVEN");
   pragma Import (Java, UNNECESSARY, "UNNECESSARY");

end Java.Math.RoundingMode;
pragma Import (Java, Java.Math.RoundingMode, "java.math.RoundingMode");
pragma Extensions_Allowed (Off);
