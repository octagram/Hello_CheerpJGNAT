pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.Authenticator.RequestorType;
limited with Java.Net.InetAddress;
limited with Java.Net.PasswordAuthentication;
limited with Java.Net.URL;
with Java.Lang.Object;

package Java.Net.Authenticator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Authenticator (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure SetDefault (P1_Authenticator : access Standard.Java.Net.Authenticator.Typ'Class);

   function RequestPasswordAuthentication (P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                           P2_Int : Java.Int;
                                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                                           P5_String : access Standard.Java.Lang.String.Typ'Class)
                                           return access Java.Net.PasswordAuthentication.Typ'Class;

   function RequestPasswordAuthentication (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                                           P6_String : access Standard.Java.Lang.String.Typ'Class)
                                           return access Java.Net.PasswordAuthentication.Typ'Class;

   function RequestPasswordAuthentication (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                                           P6_String : access Standard.Java.Lang.String.Typ'Class;
                                           P7_URL : access Standard.Java.Net.URL.Typ'Class;
                                           P8_RequestorType : access Standard.Java.Net.Authenticator.RequestorType.Typ'Class)
                                           return access Java.Net.PasswordAuthentication.Typ'Class;

   --  final  protected
   function GetRequestingHost (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   --  final  protected
   function GetRequestingSite (This : access Typ)
                               return access Java.Net.InetAddress.Typ'Class;

   --  final  protected
   function GetRequestingPort (This : access Typ)
                               return Java.Int;

   --  final  protected
   function GetRequestingProtocol (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   --  final  protected
   function GetRequestingPrompt (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   --  final  protected
   function GetRequestingScheme (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   --  protected
   function GetPasswordAuthentication (This : access Typ)
                                       return access Java.Net.PasswordAuthentication.Typ'Class;

   --  protected
   function GetRequestingURL (This : access Typ)
                              return access Java.Net.URL.Typ'Class;

   --  protected
   function GetRequestorType (This : access Typ)
                              return access Java.Net.Authenticator.RequestorType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Authenticator);
   pragma Import (Java, SetDefault, "setDefault");
   pragma Import (Java, RequestPasswordAuthentication, "requestPasswordAuthentication");
   pragma Import (Java, GetRequestingHost, "getRequestingHost");
   pragma Import (Java, GetRequestingSite, "getRequestingSite");
   pragma Import (Java, GetRequestingPort, "getRequestingPort");
   pragma Import (Java, GetRequestingProtocol, "getRequestingProtocol");
   pragma Import (Java, GetRequestingPrompt, "getRequestingPrompt");
   pragma Import (Java, GetRequestingScheme, "getRequestingScheme");
   pragma Import (Java, GetPasswordAuthentication, "getPasswordAuthentication");
   pragma Import (Java, GetRequestingURL, "getRequestingURL");
   pragma Import (Java, GetRequestorType, "getRequestorType");

end Java.Net.Authenticator;
pragma Import (Java, Java.Net.Authenticator, "java.net.Authenticator");
pragma Extensions_Allowed (Off);
