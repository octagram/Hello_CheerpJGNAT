pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Net.CacheRequest is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CacheRequest (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBody (This : access Typ)
                     return access Java.Io.OutputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure abort_K (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CacheRequest);
   pragma Export (Java, GetBody, "getBody");
   pragma Export (Java, abort_K, "abort");

end Java.Net.CacheRequest;
pragma Import (Java, Java.Net.CacheRequest, "java.net.CacheRequest");
pragma Extensions_Allowed (Off);
