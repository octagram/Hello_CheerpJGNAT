pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Net.CacheResponse is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CacheResponse (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHeaders (This : access Typ)
                        return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetBody (This : access Typ)
                     return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CacheResponse);
   pragma Export (Java, GetHeaders, "getHeaders");
   pragma Export (Java, GetBody, "getBody");

end Java.Net.CacheResponse;
pragma Import (Java, Java.Net.CacheResponse, "java.net.CacheResponse");
pragma Extensions_Allowed (Off);
