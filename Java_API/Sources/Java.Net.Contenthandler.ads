pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Net.URLConnection;
with Java.Lang.Object;

package Java.Net.ContentHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ContentHandler (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContent (This : access Typ;
                        P1_URLConnection : access Standard.Java.Net.URLConnection.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetContent (This : access Typ;
                        P1_URLConnection : access Standard.Java.Net.URLConnection.Typ'Class;
                        P2_Class_Arr : access Java.Lang.Class.Arr_Obj)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ContentHandler);
   pragma Export (Java, GetContent, "getContent");

end Java.Net.ContentHandler;
pragma Import (Java, Java.Net.ContentHandler, "java.net.ContentHandler");
pragma Extensions_Allowed (Off);
