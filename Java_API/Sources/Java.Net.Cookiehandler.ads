pragma Extensions_Allowed (On);
limited with Java.Net.URI;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Net.CookieHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CookieHandler (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetDefault return access Java.Net.CookieHandler.Typ'Class;

   --  synchronized
   procedure SetDefault (P1_CookieHandler : access Standard.Java.Net.CookieHandler.Typ'Class);

   function Get (This : access Typ;
                 P1_URI : access Standard.Java.Net.URI.Typ'Class;
                 P2_Map : access Standard.Java.Util.Map.Typ'Class)
                 return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Put (This : access Typ;
                  P1_URI : access Standard.Java.Net.URI.Typ'Class;
                  P2_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CookieHandler);
   pragma Export (Java, GetDefault, "getDefault");
   pragma Export (Java, SetDefault, "setDefault");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Put, "put");

end Java.Net.CookieHandler;
pragma Import (Java, Java.Net.CookieHandler, "java.net.CookieHandler");
pragma Extensions_Allowed (Off);
