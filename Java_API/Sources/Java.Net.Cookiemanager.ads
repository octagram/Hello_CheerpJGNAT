pragma Extensions_Allowed (On);
limited with Java.Net.CookiePolicy;
limited with Java.Net.CookieStore;
limited with Java.Net.URI;
limited with Java.Util.Map;
with Java.Lang.Object;
with Java.Net.CookieHandler;

package Java.Net.CookieManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Net.CookieHandler.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CookieManager (This : Ref := null)
                               return Ref;

   function New_CookieManager (P1_CookieStore : access Standard.Java.Net.CookieStore.Typ'Class;
                               P2_CookiePolicy : access Standard.Java.Net.CookiePolicy.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetCookiePolicy (This : access Typ;
                              P1_CookiePolicy : access Standard.Java.Net.CookiePolicy.Typ'Class);

   function GetCookieStore (This : access Typ)
                            return access Java.Net.CookieStore.Typ'Class;

   function Get (This : access Typ;
                 P1_URI : access Standard.Java.Net.URI.Typ'Class;
                 P2_Map : access Standard.Java.Util.Map.Typ'Class)
                 return access Java.Util.Map.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure Put (This : access Typ;
                  P1_URI : access Standard.Java.Net.URI.Typ'Class;
                  P2_Map : access Standard.Java.Util.Map.Typ'Class);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CookieManager);
   pragma Import (Java, SetCookiePolicy, "setCookiePolicy");
   pragma Import (Java, GetCookieStore, "getCookieStore");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Put, "put");

end Java.Net.CookieManager;
pragma Import (Java, Java.Net.CookieManager, "java.net.CookieManager");
pragma Extensions_Allowed (Off);
