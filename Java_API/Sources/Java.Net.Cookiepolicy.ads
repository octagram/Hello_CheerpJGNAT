pragma Extensions_Allowed (On);
limited with Java.Net.HttpCookie;
limited with Java.Net.URI;
with Java.Lang.Object;

package Java.Net.CookiePolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ShouldAccept (This : access Typ;
                          P1_URI : access Standard.Java.Net.URI.Typ'Class;
                          P2_HttpCookie : access Standard.Java.Net.HttpCookie.Typ'Class)
                          return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ACCEPT_ALL : access Java.Net.CookiePolicy.Typ'Class;

   --  final
   ACCEPT_NONE : access Java.Net.CookiePolicy.Typ'Class;

   --  final
   ACCEPT_ORIGINAL_SERVER : access Java.Net.CookiePolicy.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ShouldAccept, "shouldAccept");
   pragma Import (Java, ACCEPT_ALL, "ACCEPT_ALL");
   pragma Import (Java, ACCEPT_NONE, "ACCEPT_NONE");
   pragma Import (Java, ACCEPT_ORIGINAL_SERVER, "ACCEPT_ORIGINAL_SERVER");

end Java.Net.CookiePolicy;
pragma Import (Java, Java.Net.CookiePolicy, "java.net.CookiePolicy");
pragma Extensions_Allowed (Off);
