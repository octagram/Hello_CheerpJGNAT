pragma Extensions_Allowed (On);
limited with Java.Net.HttpCookie;
limited with Java.Net.URI;
limited with Java.Util.List;
with Java.Lang.Object;

package Java.Net.CookieStore is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Add (This : access Typ;
                  P1_URI : access Standard.Java.Net.URI.Typ'Class;
                  P2_HttpCookie : access Standard.Java.Net.HttpCookie.Typ'Class) is abstract;

   function Get (This : access Typ;
                 P1_URI : access Standard.Java.Net.URI.Typ'Class)
                 return access Java.Util.List.Typ'Class is abstract;

   function GetCookies (This : access Typ)
                        return access Java.Util.List.Typ'Class is abstract;

   function GetURIs (This : access Typ)
                     return access Java.Util.List.Typ'Class is abstract;

   function Remove (This : access Typ;
                    P1_URI : access Standard.Java.Net.URI.Typ'Class;
                    P2_HttpCookie : access Standard.Java.Net.HttpCookie.Typ'Class)
                    return Java.Boolean is abstract;

   function RemoveAll (This : access Typ)
                       return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Add, "add");
   pragma Export (Java, Get, "get");
   pragma Export (Java, GetCookies, "getCookies");
   pragma Export (Java, GetURIs, "getURIs");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, RemoveAll, "removeAll");

end Java.Net.CookieStore;
pragma Import (Java, Java.Net.CookieStore, "java.net.CookieStore");
pragma Extensions_Allowed (Off);
