pragma Extensions_Allowed (On);
limited with Java.Net.InetAddress;
limited with Java.Net.SocketAddress;
with Java.Lang.Object;

package Java.Net.DatagramPacket is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DatagramPacket (P1_Byte_Arr : Java.Byte_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_DatagramPacket (P1_Byte_Arr : Java.Byte_Arr;
                                P2_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_DatagramPacket (P1_Byte_Arr : Java.Byte_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                P5_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_DatagramPacket (P1_Byte_Arr : Java.Byte_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Net.SocketException.Except

   function New_DatagramPacket (P1_Byte_Arr : Java.Byte_Arr;
                                P2_Int : Java.Int;
                                P3_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                P4_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_DatagramPacket (P1_Byte_Arr : Java.Byte_Arr;
                                P2_Int : Java.Int;
                                P3_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Net.SocketException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetAddress (This : access Typ)
                        return access Java.Net.InetAddress.Typ'Class;

   --  synchronized
   function GetPort (This : access Typ)
                     return Java.Int;

   --  synchronized
   function GetData (This : access Typ)
                     return Java.Byte_Arr;

   --  synchronized
   function GetOffset (This : access Typ)
                       return Java.Int;

   --  synchronized
   function GetLength (This : access Typ)
                       return Java.Int;

   --  synchronized
   procedure SetData (This : access Typ;
                      P1_Byte_Arr : Java.Byte_Arr;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int);

   --  synchronized
   procedure SetAddress (This : access Typ;
                         P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class);

   --  synchronized
   procedure SetPort (This : access Typ;
                      P1_Int : Java.Int);

   --  synchronized
   procedure SetSocketAddress (This : access Typ;
                               P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class);

   --  synchronized
   function GetSocketAddress (This : access Typ)
                              return access Java.Net.SocketAddress.Typ'Class;

   --  synchronized
   procedure SetData (This : access Typ;
                      P1_Byte_Arr : Java.Byte_Arr);

   --  synchronized
   procedure SetLength (This : access Typ;
                        P1_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DatagramPacket);
   pragma Import (Java, GetAddress, "getAddress");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, SetData, "setData");
   pragma Import (Java, SetAddress, "setAddress");
   pragma Import (Java, SetPort, "setPort");
   pragma Import (Java, SetSocketAddress, "setSocketAddress");
   pragma Import (Java, GetSocketAddress, "getSocketAddress");
   pragma Import (Java, SetLength, "setLength");

end Java.Net.DatagramPacket;
pragma Import (Java, Java.Net.DatagramPacket, "java.net.DatagramPacket");
pragma Extensions_Allowed (Off);
