pragma Extensions_Allowed (On);
limited with Java.Net.DatagramPacket;
limited with Java.Net.DatagramSocketImpl;
limited with Java.Net.DatagramSocketImplFactory;
limited with Java.Net.InetAddress;
limited with Java.Net.SocketAddress;
limited with Java.Nio.Channels.DatagramChannel;
with Java.Lang.Object;

package Java.Net.DatagramSocket is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DatagramSocket (This : Ref := null)
                                return Ref;
   --  can raise Java.Net.SocketException.Except

   --  protected
   function New_DatagramSocket (P1_DatagramSocketImpl : access Standard.Java.Net.DatagramSocketImpl.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_DatagramSocket (P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Net.SocketException.Except

   function New_DatagramSocket (P1_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Net.SocketException.Except

   function New_DatagramSocket (P1_Int : Java.Int;
                                P2_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Net.SocketException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Bind (This : access Typ;
                   P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class);
   --  can raise Java.Net.SocketException.Except

   procedure Connect (This : access Typ;
                      P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                      P2_Int : Java.Int);

   procedure Connect (This : access Typ;
                      P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class);
   --  can raise Java.Net.SocketException.Except

   procedure Disconnect (This : access Typ);

   function IsBound (This : access Typ)
                     return Java.Boolean;

   function IsConnected (This : access Typ)
                         return Java.Boolean;

   function GetInetAddress (This : access Typ)
                            return access Java.Net.InetAddress.Typ'Class;

   function GetPort (This : access Typ)
                     return Java.Int;

   function GetRemoteSocketAddress (This : access Typ)
                                    return access Java.Net.SocketAddress.Typ'Class;

   function GetLocalSocketAddress (This : access Typ)
                                   return access Java.Net.SocketAddress.Typ'Class;

   procedure Send (This : access Typ;
                   P1_DatagramPacket : access Standard.Java.Net.DatagramPacket.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Receive (This : access Typ;
                      P1_DatagramPacket : access Standard.Java.Net.DatagramPacket.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetLocalAddress (This : access Typ)
                             return access Java.Net.InetAddress.Typ'Class;

   function GetLocalPort (This : access Typ)
                          return Java.Int;

   --  synchronized
   procedure SetSoTimeout (This : access Typ;
                           P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetSoTimeout (This : access Typ)
                          return Java.Int;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetSendBufferSize (This : access Typ;
                                P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetSendBufferSize (This : access Typ)
                               return Java.Int;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetReceiveBufferSize (This : access Typ;
                                   P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetReceiveBufferSize (This : access Typ)
                                  return Java.Int;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetReuseAddress (This : access Typ;
                              P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetReuseAddress (This : access Typ)
                             return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetBroadcast (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetBroadcast (This : access Typ)
                          return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetTrafficClass (This : access Typ;
                              P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetTrafficClass (This : access Typ)
                             return Java.Int;
   --  can raise Java.Net.SocketException.Except

   procedure Close (This : access Typ);

   function IsClosed (This : access Typ)
                      return Java.Boolean;

   function GetChannel (This : access Typ)
                        return access Java.Nio.Channels.DatagramChannel.Typ'Class;

   --  synchronized
   procedure SetDatagramSocketImplFactory (P1_DatagramSocketImplFactory : access Standard.Java.Net.DatagramSocketImplFactory.Typ'Class);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DatagramSocket);
   pragma Import (Java, Bind, "bind");
   pragma Import (Java, Connect, "connect");
   pragma Import (Java, Disconnect, "disconnect");
   pragma Import (Java, IsBound, "isBound");
   pragma Import (Java, IsConnected, "isConnected");
   pragma Import (Java, GetInetAddress, "getInetAddress");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, GetRemoteSocketAddress, "getRemoteSocketAddress");
   pragma Import (Java, GetLocalSocketAddress, "getLocalSocketAddress");
   pragma Import (Java, Send, "send");
   pragma Import (Java, Receive, "receive");
   pragma Import (Java, GetLocalAddress, "getLocalAddress");
   pragma Import (Java, GetLocalPort, "getLocalPort");
   pragma Import (Java, SetSoTimeout, "setSoTimeout");
   pragma Import (Java, GetSoTimeout, "getSoTimeout");
   pragma Import (Java, SetSendBufferSize, "setSendBufferSize");
   pragma Import (Java, GetSendBufferSize, "getSendBufferSize");
   pragma Import (Java, SetReceiveBufferSize, "setReceiveBufferSize");
   pragma Import (Java, GetReceiveBufferSize, "getReceiveBufferSize");
   pragma Import (Java, SetReuseAddress, "setReuseAddress");
   pragma Import (Java, GetReuseAddress, "getReuseAddress");
   pragma Import (Java, SetBroadcast, "setBroadcast");
   pragma Import (Java, GetBroadcast, "getBroadcast");
   pragma Import (Java, SetTrafficClass, "setTrafficClass");
   pragma Import (Java, GetTrafficClass, "getTrafficClass");
   pragma Import (Java, Close, "close");
   pragma Import (Java, IsClosed, "isClosed");
   pragma Import (Java, GetChannel, "getChannel");
   pragma Import (Java, SetDatagramSocketImplFactory, "setDatagramSocketImplFactory");

end Java.Net.DatagramSocket;
pragma Import (Java, Java.Net.DatagramSocket, "java.net.DatagramSocket");
pragma Extensions_Allowed (Off);
