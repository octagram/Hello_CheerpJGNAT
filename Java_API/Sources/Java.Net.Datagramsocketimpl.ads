pragma Extensions_Allowed (On);
limited with Java.Io.FileDescriptor;
limited with Java.Net.DatagramPacket;
limited with Java.Net.InetAddress;
limited with Java.Net.NetworkInterface;
limited with Java.Net.SocketAddress;
with Java.Lang.Object;
with Java.Net.SocketOptions;

package Java.Net.DatagramSocketImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SocketOptions_I : Java.Net.SocketOptions.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      LocalPort : Java.Int;
      pragma Import (Java, LocalPort, "localPort");

      --  protected
      Fd : access Java.Io.FileDescriptor.Typ'Class;
      pragma Import (Java, Fd, "fd");

   end record;

   function New_DatagramSocketImpl (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Create (This : access Typ) is abstract;
   --  can raise Java.Net.SocketException.Except

   --  protected
   procedure Bind (This : access Typ;
                   P1_Int : Java.Int;
                   P2_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class) is abstract;
   --  can raise Java.Net.SocketException.Except

   --  protected
   procedure Send (This : access Typ;
                   P1_DatagramPacket : access Standard.Java.Net.DatagramPacket.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Connect (This : access Typ;
                      P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                      P2_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  protected
   procedure Disconnect (This : access Typ);

   --  protected
   function Peek (This : access Typ;
                  P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function PeekData (This : access Typ;
                      P1_DatagramPacket : access Standard.Java.Net.DatagramPacket.Typ'Class)
                      return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Receive (This : access Typ;
                      P1_DatagramPacket : access Standard.Java.Net.DatagramPacket.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure SetTimeToLive (This : access Typ;
                            P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetTimeToLive (This : access Typ)
                           return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Join (This : access Typ;
                   P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Leave (This : access Typ;
                    P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure JoinGroup (This : access Typ;
                        P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                        P2_NetworkInterface : access Standard.Java.Net.NetworkInterface.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure LeaveGroup (This : access Typ;
                         P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                         P2_NetworkInterface : access Standard.Java.Net.NetworkInterface.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Close (This : access Typ) is abstract;

   --  protected
   function GetLocalPort (This : access Typ)
                          return Java.Int;

   --  protected
   function GetFileDescriptor (This : access Typ)
                               return access Java.Io.FileDescriptor.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DatagramSocketImpl);
   pragma Export (Java, Create, "create");
   pragma Export (Java, Bind, "bind");
   pragma Export (Java, Send, "send");
   pragma Import (Java, Connect, "connect");
   pragma Import (Java, Disconnect, "disconnect");
   pragma Export (Java, Peek, "peek");
   pragma Export (Java, PeekData, "peekData");
   pragma Export (Java, Receive, "receive");
   pragma Export (Java, SetTimeToLive, "setTimeToLive");
   pragma Export (Java, GetTimeToLive, "getTimeToLive");
   pragma Export (Java, Join, "join");
   pragma Export (Java, Leave, "leave");
   pragma Export (Java, JoinGroup, "joinGroup");
   pragma Export (Java, LeaveGroup, "leaveGroup");
   pragma Export (Java, Close, "close");
   pragma Import (Java, GetLocalPort, "getLocalPort");
   pragma Import (Java, GetFileDescriptor, "getFileDescriptor");

end Java.Net.DatagramSocketImpl;
pragma Import (Java, Java.Net.DatagramSocketImpl, "java.net.DatagramSocketImpl");
pragma Extensions_Allowed (Off);
