pragma Extensions_Allowed (On);
limited with Java.Net.DatagramSocketImpl;
with Java.Lang.Object;

package Java.Net.DatagramSocketImplFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateDatagramSocketImpl (This : access Typ)
                                      return access Java.Net.DatagramSocketImpl.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateDatagramSocketImpl, "createDatagramSocketImpl");

end Java.Net.DatagramSocketImplFactory;
pragma Import (Java, Java.Net.DatagramSocketImplFactory, "java.net.DatagramSocketImplFactory");
pragma Extensions_Allowed (Off);
