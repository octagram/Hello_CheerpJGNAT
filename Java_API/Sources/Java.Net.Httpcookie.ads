pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Net.HttpCookie is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HttpCookie (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Parse (P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Java.Util.List.Typ'Class;

   function HasExpired (This : access Typ)
                        return Java.Boolean;

   procedure SetComment (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetComment (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetCommentURL (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetCommentURL (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   procedure SetDiscard (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function GetDiscard (This : access Typ)
                        return Java.Boolean;

   procedure SetPortlist (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPortlist (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetDomain (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetDomain (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   procedure SetMaxAge (This : access Typ;
                        P1_Long : Java.Long);

   function GetMaxAge (This : access Typ)
                       return Java.Long;

   procedure SetPath (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPath (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetSecure (This : access Typ;
                        P1_Boolean : Java.Boolean);

   function GetSecure (This : access Typ)
                       return Java.Boolean;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetVersion (This : access Typ)
                        return Java.Int;

   procedure SetVersion (This : access Typ;
                         P1_Int : Java.Int);

   function DomainMatches (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HttpCookie);
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, HasExpired, "hasExpired");
   pragma Import (Java, SetComment, "setComment");
   pragma Import (Java, GetComment, "getComment");
   pragma Import (Java, SetCommentURL, "setCommentURL");
   pragma Import (Java, GetCommentURL, "getCommentURL");
   pragma Import (Java, SetDiscard, "setDiscard");
   pragma Import (Java, GetDiscard, "getDiscard");
   pragma Import (Java, SetPortlist, "setPortlist");
   pragma Import (Java, GetPortlist, "getPortlist");
   pragma Import (Java, SetDomain, "setDomain");
   pragma Import (Java, GetDomain, "getDomain");
   pragma Import (Java, SetMaxAge, "setMaxAge");
   pragma Import (Java, GetMaxAge, "getMaxAge");
   pragma Import (Java, SetPath, "setPath");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, SetSecure, "setSecure");
   pragma Import (Java, GetSecure, "getSecure");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetVersion, "getVersion");
   pragma Import (Java, SetVersion, "setVersion");
   pragma Import (Java, DomainMatches, "domainMatches");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Clone, "clone");

end Java.Net.HttpCookie;
pragma Import (Java, Java.Net.HttpCookie, "java.net.HttpCookie");
pragma Extensions_Allowed (Off);
