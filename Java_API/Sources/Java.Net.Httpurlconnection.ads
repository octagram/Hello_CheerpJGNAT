pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Security.Permission;
with Java.Lang.Object;
with Java.Net.URLConnection;

package Java.Net.HttpURLConnection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Net.URLConnection.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Method : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Method, "method");

      --  protected
      ChunkLength : Java.Int;
      pragma Import (Java, ChunkLength, "chunkLength");

      --  protected
      FixedContentLength : Java.Int;
      pragma Import (Java, FixedContentLength, "fixedContentLength");

      --  protected
      ResponseCode : Java.Int;
      pragma Import (Java, ResponseCode, "responseCode");

      --  protected
      ResponseMessage : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ResponseMessage, "responseMessage");

      --  protected
      InstanceFollowRedirects : Java.Boolean;
      pragma Import (Java, InstanceFollowRedirects, "instanceFollowRedirects");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHeaderFieldKey (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class;

   procedure SetFixedLengthStreamingMode (This : access Typ;
                                          P1_Int : Java.Int);

   procedure SetChunkedStreamingMode (This : access Typ;
                                      P1_Int : Java.Int);

   function GetHeaderField (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function New_HttpURLConnection (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   procedure SetFollowRedirects (P1_Boolean : Java.Boolean);

   function GetFollowRedirects return Java.Boolean;

   procedure SetInstanceFollowRedirects (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   function GetInstanceFollowRedirects (This : access Typ)
                                        return Java.Boolean;

   procedure SetRequestMethod (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Net.ProtocolException.Except

   function GetRequestMethod (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetResponseCode (This : access Typ)
                             return Java.Int;
   --  can raise Java.Io.IOException.Except

   function GetResponseMessage (This : access Typ)
                                return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetHeaderFieldDate (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Long : Java.Long)
                                return Java.Long;

   procedure Disconnect (This : access Typ) is abstract;

   function UsingProxy (This : access Typ)
                        return Java.Boolean is abstract;

   function GetPermission (This : access Typ)
                           return access Java.Security.Permission.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetErrorStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   HTTP_OK : constant Java.Int;

   --  final
   HTTP_CREATED : constant Java.Int;

   --  final
   HTTP_ACCEPTED : constant Java.Int;

   --  final
   HTTP_NOT_AUTHORITATIVE : constant Java.Int;

   --  final
   HTTP_NO_CONTENT : constant Java.Int;

   --  final
   HTTP_RESET : constant Java.Int;

   --  final
   HTTP_PARTIAL : constant Java.Int;

   --  final
   HTTP_MULT_CHOICE : constant Java.Int;

   --  final
   HTTP_MOVED_PERM : constant Java.Int;

   --  final
   HTTP_MOVED_TEMP : constant Java.Int;

   --  final
   HTTP_SEE_OTHER : constant Java.Int;

   --  final
   HTTP_NOT_MODIFIED : constant Java.Int;

   --  final
   HTTP_USE_PROXY : constant Java.Int;

   --  final
   HTTP_BAD_REQUEST : constant Java.Int;

   --  final
   HTTP_UNAUTHORIZED : constant Java.Int;

   --  final
   HTTP_PAYMENT_REQUIRED : constant Java.Int;

   --  final
   HTTP_FORBIDDEN : constant Java.Int;

   --  final
   HTTP_NOT_FOUND : constant Java.Int;

   --  final
   HTTP_BAD_METHOD : constant Java.Int;

   --  final
   HTTP_NOT_ACCEPTABLE : constant Java.Int;

   --  final
   HTTP_PROXY_AUTH : constant Java.Int;

   --  final
   HTTP_CLIENT_TIMEOUT : constant Java.Int;

   --  final
   HTTP_CONFLICT : constant Java.Int;

   --  final
   HTTP_GONE : constant Java.Int;

   --  final
   HTTP_LENGTH_REQUIRED : constant Java.Int;

   --  final
   HTTP_PRECON_FAILED : constant Java.Int;

   --  final
   HTTP_ENTITY_TOO_LARGE : constant Java.Int;

   --  final
   HTTP_REQ_TOO_LONG : constant Java.Int;

   --  final
   HTTP_UNSUPPORTED_TYPE : constant Java.Int;

   --  final
   HTTP_INTERNAL_ERROR : constant Java.Int;

   --  final
   HTTP_NOT_IMPLEMENTED : constant Java.Int;

   --  final
   HTTP_BAD_GATEWAY : constant Java.Int;

   --  final
   HTTP_UNAVAILABLE : constant Java.Int;

   --  final
   HTTP_GATEWAY_TIMEOUT : constant Java.Int;

   --  final
   HTTP_VERSION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetHeaderFieldKey, "getHeaderFieldKey");
   pragma Export (Java, SetFixedLengthStreamingMode, "setFixedLengthStreamingMode");
   pragma Export (Java, SetChunkedStreamingMode, "setChunkedStreamingMode");
   pragma Export (Java, GetHeaderField, "getHeaderField");
   pragma Java_Constructor (New_HttpURLConnection);
   pragma Export (Java, SetFollowRedirects, "setFollowRedirects");
   pragma Export (Java, GetFollowRedirects, "getFollowRedirects");
   pragma Export (Java, SetInstanceFollowRedirects, "setInstanceFollowRedirects");
   pragma Export (Java, GetInstanceFollowRedirects, "getInstanceFollowRedirects");
   pragma Export (Java, SetRequestMethod, "setRequestMethod");
   pragma Export (Java, GetRequestMethod, "getRequestMethod");
   pragma Export (Java, GetResponseCode, "getResponseCode");
   pragma Export (Java, GetResponseMessage, "getResponseMessage");
   pragma Export (Java, GetHeaderFieldDate, "getHeaderFieldDate");
   pragma Export (Java, Disconnect, "disconnect");
   pragma Export (Java, UsingProxy, "usingProxy");
   pragma Export (Java, GetPermission, "getPermission");
   pragma Export (Java, GetErrorStream, "getErrorStream");
   pragma Import (Java, HTTP_OK, "HTTP_OK");
   pragma Import (Java, HTTP_CREATED, "HTTP_CREATED");
   pragma Import (Java, HTTP_ACCEPTED, "HTTP_ACCEPTED");
   pragma Import (Java, HTTP_NOT_AUTHORITATIVE, "HTTP_NOT_AUTHORITATIVE");
   pragma Import (Java, HTTP_NO_CONTENT, "HTTP_NO_CONTENT");
   pragma Import (Java, HTTP_RESET, "HTTP_RESET");
   pragma Import (Java, HTTP_PARTIAL, "HTTP_PARTIAL");
   pragma Import (Java, HTTP_MULT_CHOICE, "HTTP_MULT_CHOICE");
   pragma Import (Java, HTTP_MOVED_PERM, "HTTP_MOVED_PERM");
   pragma Import (Java, HTTP_MOVED_TEMP, "HTTP_MOVED_TEMP");
   pragma Import (Java, HTTP_SEE_OTHER, "HTTP_SEE_OTHER");
   pragma Import (Java, HTTP_NOT_MODIFIED, "HTTP_NOT_MODIFIED");
   pragma Import (Java, HTTP_USE_PROXY, "HTTP_USE_PROXY");
   pragma Import (Java, HTTP_BAD_REQUEST, "HTTP_BAD_REQUEST");
   pragma Import (Java, HTTP_UNAUTHORIZED, "HTTP_UNAUTHORIZED");
   pragma Import (Java, HTTP_PAYMENT_REQUIRED, "HTTP_PAYMENT_REQUIRED");
   pragma Import (Java, HTTP_FORBIDDEN, "HTTP_FORBIDDEN");
   pragma Import (Java, HTTP_NOT_FOUND, "HTTP_NOT_FOUND");
   pragma Import (Java, HTTP_BAD_METHOD, "HTTP_BAD_METHOD");
   pragma Import (Java, HTTP_NOT_ACCEPTABLE, "HTTP_NOT_ACCEPTABLE");
   pragma Import (Java, HTTP_PROXY_AUTH, "HTTP_PROXY_AUTH");
   pragma Import (Java, HTTP_CLIENT_TIMEOUT, "HTTP_CLIENT_TIMEOUT");
   pragma Import (Java, HTTP_CONFLICT, "HTTP_CONFLICT");
   pragma Import (Java, HTTP_GONE, "HTTP_GONE");
   pragma Import (Java, HTTP_LENGTH_REQUIRED, "HTTP_LENGTH_REQUIRED");
   pragma Import (Java, HTTP_PRECON_FAILED, "HTTP_PRECON_FAILED");
   pragma Import (Java, HTTP_ENTITY_TOO_LARGE, "HTTP_ENTITY_TOO_LARGE");
   pragma Import (Java, HTTP_REQ_TOO_LONG, "HTTP_REQ_TOO_LONG");
   pragma Import (Java, HTTP_UNSUPPORTED_TYPE, "HTTP_UNSUPPORTED_TYPE");
   pragma Import (Java, HTTP_INTERNAL_ERROR, "HTTP_INTERNAL_ERROR");
   pragma Import (Java, HTTP_NOT_IMPLEMENTED, "HTTP_NOT_IMPLEMENTED");
   pragma Import (Java, HTTP_BAD_GATEWAY, "HTTP_BAD_GATEWAY");
   pragma Import (Java, HTTP_UNAVAILABLE, "HTTP_UNAVAILABLE");
   pragma Import (Java, HTTP_GATEWAY_TIMEOUT, "HTTP_GATEWAY_TIMEOUT");
   pragma Import (Java, HTTP_VERSION, "HTTP_VERSION");

end Java.Net.HttpURLConnection;
pragma Import (Java, Java.Net.HttpURLConnection, "java.net.HttpURLConnection");
pragma Extensions_Allowed (Off);
