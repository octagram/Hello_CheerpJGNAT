pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Net.IDN is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToASCII (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   function ToASCII (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function ToUnicode (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;

   function ToUnicode (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ALLOW_UNASSIGNED : constant Java.Int;

   --  final
   USE_STD3_ASCII_RULES : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToASCII, "toASCII");
   pragma Import (Java, ToUnicode, "toUnicode");
   pragma Import (Java, ALLOW_UNASSIGNED, "ALLOW_UNASSIGNED");
   pragma Import (Java, USE_STD3_ASCII_RULES, "USE_STD3_ASCII_RULES");

end Java.Net.IDN;
pragma Import (Java, Java.Net.IDN, "java.net.IDN");
pragma Extensions_Allowed (Off);
