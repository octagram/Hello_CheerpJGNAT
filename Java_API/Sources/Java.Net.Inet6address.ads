pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.NetworkInterface;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Net.InetAddress;

package Java.Net.Inet6Address is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Net.InetAddress.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetByAddress (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Byte_Arr : Java.Byte_Arr;
                          P3_NetworkInterface : access Standard.Java.Net.NetworkInterface.Typ'Class)
                          return access Java.Net.Inet6Address.Typ'Class;
   --  can raise Java.Net.UnknownHostException.Except

   function GetByAddress (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Byte_Arr : Java.Byte_Arr;
                          P3_Int : Java.Int)
                          return access Java.Net.Inet6Address.Typ'Class;
   --  can raise Java.Net.UnknownHostException.Except

   function IsMulticastAddress (This : access Typ)
                                return Java.Boolean;

   function IsAnyLocalAddress (This : access Typ)
                               return Java.Boolean;

   function IsLoopbackAddress (This : access Typ)
                               return Java.Boolean;

   function IsLinkLocalAddress (This : access Typ)
                                return Java.Boolean;

   function IsSiteLocalAddress (This : access Typ)
                                return Java.Boolean;

   function IsMCGlobal (This : access Typ)
                        return Java.Boolean;

   function IsMCNodeLocal (This : access Typ)
                           return Java.Boolean;

   function IsMCLinkLocal (This : access Typ)
                           return Java.Boolean;

   function IsMCSiteLocal (This : access Typ)
                           return Java.Boolean;

   function IsMCOrgLocal (This : access Typ)
                          return Java.Boolean;

   function GetAddress (This : access Typ)
                        return Java.Byte_Arr;

   function GetScopeId (This : access Typ)
                        return Java.Int;

   function GetScopedInterface (This : access Typ)
                                return access Java.Net.NetworkInterface.Typ'Class;

   function GetHostAddress (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function IsIPv4CompatibleAddress (This : access Typ)
                                     return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetByAddress, "getByAddress");
   pragma Import (Java, IsMulticastAddress, "isMulticastAddress");
   pragma Import (Java, IsAnyLocalAddress, "isAnyLocalAddress");
   pragma Import (Java, IsLoopbackAddress, "isLoopbackAddress");
   pragma Import (Java, IsLinkLocalAddress, "isLinkLocalAddress");
   pragma Import (Java, IsSiteLocalAddress, "isSiteLocalAddress");
   pragma Import (Java, IsMCGlobal, "isMCGlobal");
   pragma Import (Java, IsMCNodeLocal, "isMCNodeLocal");
   pragma Import (Java, IsMCLinkLocal, "isMCLinkLocal");
   pragma Import (Java, IsMCSiteLocal, "isMCSiteLocal");
   pragma Import (Java, IsMCOrgLocal, "isMCOrgLocal");
   pragma Import (Java, GetAddress, "getAddress");
   pragma Import (Java, GetScopeId, "getScopeId");
   pragma Import (Java, GetScopedInterface, "getScopedInterface");
   pragma Import (Java, GetHostAddress, "getHostAddress");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, IsIPv4CompatibleAddress, "isIPv4CompatibleAddress");

end Java.Net.Inet6Address;
pragma Import (Java, Java.Net.Inet6Address, "java.net.Inet6Address");
pragma Extensions_Allowed (Off);
