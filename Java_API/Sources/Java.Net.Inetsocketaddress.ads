pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Net.SocketAddress;

package Java.Net.InetSocketAddress is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Net.SocketAddress.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InetSocketAddress (P1_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_InetSocketAddress (P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_InetSocketAddress (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUnresolved (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Int : Java.Int)
                              return access Java.Net.InetSocketAddress.Typ'Class;

   --  final
   function GetPort (This : access Typ)
                     return Java.Int;

   --  final
   function GetAddress (This : access Typ)
                        return access Java.Net.InetAddress.Typ'Class;

   --  final
   function GetHostName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  final
   function IsUnresolved (This : access Typ)
                          return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InetSocketAddress);
   pragma Import (Java, CreateUnresolved, "createUnresolved");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, GetAddress, "getAddress");
   pragma Import (Java, GetHostName, "getHostName");
   pragma Import (Java, IsUnresolved, "isUnresolved");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Net.InetSocketAddress;
pragma Import (Java, Java.Net.InetSocketAddress, "java.net.InetSocketAddress");
pragma Extensions_Allowed (Off);
