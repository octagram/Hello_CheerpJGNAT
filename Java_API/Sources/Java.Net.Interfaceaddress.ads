pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
with Java.Lang.Object;

package Java.Net.InterfaceAddress is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAddress (This : access Typ)
                        return access Java.Net.InetAddress.Typ'Class;

   function GetBroadcast (This : access Typ)
                          return access Java.Net.InetAddress.Typ'Class;

   function GetNetworkPrefixLength (This : access Typ)
                                    return Java.Short;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetAddress, "getAddress");
   pragma Import (Java, GetBroadcast, "getBroadcast");
   pragma Import (Java, GetNetworkPrefixLength, "getNetworkPrefixLength");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Net.InterfaceAddress;
pragma Import (Java, Java.Net.InterfaceAddress, "java.net.InterfaceAddress");
pragma Extensions_Allowed (Off);
