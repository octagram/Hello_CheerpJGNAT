pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Security.Cert.Certificate;
limited with Java.Util.Jar.Attributes;
limited with Java.Util.Jar.JarEntry;
limited with Java.Util.Jar.JarFile;
limited with Java.Util.Jar.Manifest;
with Java.Lang.Object;
with Java.Net.URLConnection;

package Java.Net.JarURLConnection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Net.URLConnection.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      JarFileURLConnection : access Java.Net.URLConnection.Typ'Class;
      pragma Import (Java, JarFileURLConnection, "jarFileURLConnection");

   end record;

   --  protected
   function New_JarURLConnection (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetJarFileURL (This : access Typ)
                           return access Java.Net.URL.Typ'Class;

   function GetEntryName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetJarFile (This : access Typ)
                        return access Java.Util.Jar.JarFile.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetManifest (This : access Typ)
                         return access Java.Util.Jar.Manifest.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetJarEntry (This : access Typ)
                         return access Java.Util.Jar.JarEntry.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetAttributes (This : access Typ)
                           return access Java.Util.Jar.Attributes.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetMainAttributes (This : access Typ)
                               return access Java.Util.Jar.Attributes.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetCertificates (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JarURLConnection);
   pragma Export (Java, GetJarFileURL, "getJarFileURL");
   pragma Export (Java, GetEntryName, "getEntryName");
   pragma Export (Java, GetJarFile, "getJarFile");
   pragma Export (Java, GetManifest, "getManifest");
   pragma Export (Java, GetJarEntry, "getJarEntry");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetMainAttributes, "getMainAttributes");
   pragma Export (Java, GetCertificates, "getCertificates");

end Java.Net.JarURLConnection;
pragma Import (Java, Java.Net.JarURLConnection, "java.net.JarURLConnection");
pragma Extensions_Allowed (Off);
