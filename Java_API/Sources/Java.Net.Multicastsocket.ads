pragma Extensions_Allowed (On);
limited with Java.Net.InetAddress;
limited with Java.Net.NetworkInterface;
limited with Java.Net.SocketAddress;
with Java.Lang.Object;
with Java.Net.DatagramSocket;

package Java.Net.MulticastSocket is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Net.DatagramSocket.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MulticastSocket (This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.IOException.Except

   function New_MulticastSocket (P1_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.IOException.Except

   function New_MulticastSocket (P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTimeToLive (This : access Typ;
                            P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function GetTimeToLive (This : access Typ)
                           return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure JoinGroup (This : access Typ;
                        P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure LeaveGroup (This : access Typ;
                         P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure JoinGroup (This : access Typ;
                        P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                        P2_NetworkInterface : access Standard.Java.Net.NetworkInterface.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure LeaveGroup (This : access Typ;
                         P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                         P2_NetworkInterface : access Standard.Java.Net.NetworkInterface.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure SetInterface (This : access Typ;
                           P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class);
   --  can raise Java.Net.SocketException.Except

   function GetInterface (This : access Typ)
                          return access Java.Net.InetAddress.Typ'Class;
   --  can raise Java.Net.SocketException.Except

   procedure SetNetworkInterface (This : access Typ;
                                  P1_NetworkInterface : access Standard.Java.Net.NetworkInterface.Typ'Class);
   --  can raise Java.Net.SocketException.Except

   function GetNetworkInterface (This : access Typ)
                                 return access Java.Net.NetworkInterface.Typ'Class;
   --  can raise Java.Net.SocketException.Except

   procedure SetLoopbackMode (This : access Typ;
                              P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   function GetLoopbackMode (This : access Typ)
                             return Java.Boolean;
   --  can raise Java.Net.SocketException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MulticastSocket);
   pragma Import (Java, SetTimeToLive, "setTimeToLive");
   pragma Import (Java, GetTimeToLive, "getTimeToLive");
   pragma Import (Java, JoinGroup, "joinGroup");
   pragma Import (Java, LeaveGroup, "leaveGroup");
   pragma Import (Java, SetInterface, "setInterface");
   pragma Import (Java, GetInterface, "getInterface");
   pragma Import (Java, SetNetworkInterface, "setNetworkInterface");
   pragma Import (Java, GetNetworkInterface, "getNetworkInterface");
   pragma Import (Java, SetLoopbackMode, "setLoopbackMode");
   pragma Import (Java, GetLoopbackMode, "getLoopbackMode");

end Java.Net.MulticastSocket;
pragma Import (Java, Java.Net.MulticastSocket, "java.net.MulticastSocket");
pragma Extensions_Allowed (Off);
