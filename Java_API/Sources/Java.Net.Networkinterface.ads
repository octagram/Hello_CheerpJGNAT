pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
limited with Java.Util.Enumeration;
limited with Java.Util.List;
with Java.Lang.Object;

package Java.Net.NetworkInterface is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetInetAddresses (This : access Typ)
                              return access Java.Util.Enumeration.Typ'Class;

   function GetInterfaceAddresses (This : access Typ)
                                   return access Java.Util.List.Typ'Class;

   function GetSubInterfaces (This : access Typ)
                              return access Java.Util.Enumeration.Typ'Class;

   function GetParent (This : access Typ)
                       return access Java.Net.NetworkInterface.Typ'Class;

   function GetDisplayName (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetByName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Net.NetworkInterface.Typ'Class;
   --  can raise Java.Net.SocketException.Except

   function GetByInetAddress (P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class)
                              return access Java.Net.NetworkInterface.Typ'Class;
   --  can raise Java.Net.SocketException.Except

   function GetNetworkInterfaces return access Java.Util.Enumeration.Typ'Class;
   --  can raise Java.Net.SocketException.Except

   function IsUp (This : access Typ)
                  return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   function IsLoopback (This : access Typ)
                        return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   function IsPointToPoint (This : access Typ)
                            return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   function SupportsMulticast (This : access Typ)
                               return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   function GetHardwareAddress (This : access Typ)
                                return Java.Byte_Arr;
   --  can raise Java.Net.SocketException.Except

   function GetMTU (This : access Typ)
                    return Java.Int;
   --  can raise Java.Net.SocketException.Except

   function IsVirtual (This : access Typ)
                       return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetInetAddresses, "getInetAddresses");
   pragma Import (Java, GetInterfaceAddresses, "getInterfaceAddresses");
   pragma Import (Java, GetSubInterfaces, "getSubInterfaces");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetDisplayName, "getDisplayName");
   pragma Import (Java, GetByName, "getByName");
   pragma Import (Java, GetByInetAddress, "getByInetAddress");
   pragma Import (Java, GetNetworkInterfaces, "getNetworkInterfaces");
   pragma Import (Java, IsUp, "isUp");
   pragma Import (Java, IsLoopback, "isLoopback");
   pragma Import (Java, IsPointToPoint, "isPointToPoint");
   pragma Import (Java, SupportsMulticast, "supportsMulticast");
   pragma Import (Java, GetHardwareAddress, "getHardwareAddress");
   pragma Import (Java, GetMTU, "getMTU");
   pragma Import (Java, IsVirtual, "isVirtual");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Net.NetworkInterface;
pragma Import (Java, Java.Net.NetworkInterface, "java.net.NetworkInterface");
pragma Extensions_Allowed (Off);
