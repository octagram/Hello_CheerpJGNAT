pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Net.PasswordAuthentication is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PasswordAuthentication (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_Char_Arr : Java.Char_Arr; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUserName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetPassword (This : access Typ)
                         return Java.Char_Arr;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PasswordAuthentication);
   pragma Import (Java, GetUserName, "getUserName");
   pragma Import (Java, GetPassword, "getPassword");

end Java.Net.PasswordAuthentication;
pragma Import (Java, Java.Net.PasswordAuthentication, "java.net.PasswordAuthentication");
pragma Extensions_Allowed (Off);
