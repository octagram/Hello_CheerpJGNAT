pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.Proxy.Type_K;
limited with Java.Net.SocketAddress;
with Java.Lang.Object;

package Java.Net.Proxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Proxy (P1_Type_K : access Standard.Java.Net.Proxy.Type_K.Typ'Class;
                       P2_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Java.Net.Proxy.Type_K.Typ'Class;

   function Address (This : access Typ)
                     return access Java.Net.SocketAddress.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NO_PROXY : access Java.Net.Proxy.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Proxy);
   pragma Import (Java, GetType, "type");
   pragma Import (Java, Address, "address");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, NO_PROXY, "NO_PROXY");

end Java.Net.Proxy;
pragma Import (Java, Java.Net.Proxy, "java.net.Proxy");
pragma Extensions_Allowed (Off);
