pragma Extensions_Allowed (On);
limited with Java.Io.IOException;
limited with Java.Net.SocketAddress;
limited with Java.Net.URI;
limited with Java.Util.List;
with Java.Lang.Object;

package Java.Net.ProxySelector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ProxySelector (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefault return access Java.Net.ProxySelector.Typ'Class;

   procedure SetDefault (P1_ProxySelector : access Standard.Java.Net.ProxySelector.Typ'Class);

   function select_K (This : access Typ;
                      P1_URI : access Standard.Java.Net.URI.Typ'Class)
                      return access Java.Util.List.Typ'Class is abstract;

   procedure ConnectFailed (This : access Typ;
                            P1_URI : access Standard.Java.Net.URI.Typ'Class;
                            P2_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                            P3_IOException : access Standard.Java.Io.IOException.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ProxySelector);
   pragma Export (Java, GetDefault, "getDefault");
   pragma Export (Java, SetDefault, "setDefault");
   pragma Export (Java, select_K, "select");
   pragma Export (Java, ConnectFailed, "connectFailed");

end Java.Net.ProxySelector;
pragma Import (Java, Java.Net.ProxySelector, "java.net.ProxySelector");
pragma Extensions_Allowed (Off);
