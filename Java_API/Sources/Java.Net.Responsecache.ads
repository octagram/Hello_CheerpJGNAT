pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.CacheRequest;
limited with Java.Net.CacheResponse;
limited with Java.Net.URI;
limited with Java.Net.URLConnection;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Net.ResponseCache is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ResponseCache (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetDefault return access Java.Net.ResponseCache.Typ'Class;

   --  synchronized
   procedure SetDefault (P1_ResponseCache : access Standard.Java.Net.ResponseCache.Typ'Class);

   function Get (This : access Typ;
                 P1_URI : access Standard.Java.Net.URI.Typ'Class;
                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                 P3_Map : access Standard.Java.Util.Map.Typ'Class)
                 return access Java.Net.CacheResponse.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function Put (This : access Typ;
                 P1_URI : access Standard.Java.Net.URI.Typ'Class;
                 P2_URLConnection : access Standard.Java.Net.URLConnection.Typ'Class)
                 return access Java.Net.CacheRequest.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ResponseCache);
   pragma Export (Java, GetDefault, "getDefault");
   pragma Export (Java, SetDefault, "setDefault");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Put, "put");

end Java.Net.ResponseCache;
pragma Import (Java, Java.Net.ResponseCache, "java.net.ResponseCache");
pragma Extensions_Allowed (Off);
