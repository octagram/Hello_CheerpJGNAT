pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Principal;
limited with Java.Util.List;
with Java.Lang.Object;
with Java.Net.CacheResponse;

package Java.Net.SecureCacheResponse is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Net.CacheResponse.Typ
      with null record;

   function New_SecureCacheResponse (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCipherSuite (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetLocalCertificateChain (This : access Typ)
                                      return access Java.Util.List.Typ'Class is abstract;

   function GetServerCertificateChain (This : access Typ)
                                       return access Java.Util.List.Typ'Class is abstract;
   --  can raise Javax.Net.Ssl.SSLPeerUnverifiedException.Except

   function GetPeerPrincipal (This : access Typ)
                              return access Java.Security.Principal.Typ'Class is abstract;
   --  can raise Javax.Net.Ssl.SSLPeerUnverifiedException.Except

   function GetLocalPrincipal (This : access Typ)
                               return access Java.Security.Principal.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SecureCacheResponse);
   pragma Export (Java, GetCipherSuite, "getCipherSuite");
   pragma Export (Java, GetLocalCertificateChain, "getLocalCertificateChain");
   pragma Export (Java, GetServerCertificateChain, "getServerCertificateChain");
   pragma Export (Java, GetPeerPrincipal, "getPeerPrincipal");
   pragma Export (Java, GetLocalPrincipal, "getLocalPrincipal");

end Java.Net.SecureCacheResponse;
pragma Import (Java, Java.Net.SecureCacheResponse, "java.net.SecureCacheResponse");
pragma Extensions_Allowed (Off);
