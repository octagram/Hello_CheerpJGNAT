pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
limited with Java.Net.Socket;
limited with Java.Net.SocketAddress;
limited with Java.Net.SocketImplFactory;
limited with Java.Nio.Channels.ServerSocketChannel;
with Java.Lang.Object;

package Java.Net.ServerSocket is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ServerSocket (This : Ref := null)
                              return Ref;
   --  can raise Java.Io.IOException.Except

   function New_ServerSocket (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;
   --  can raise Java.Io.IOException.Except

   function New_ServerSocket (P1_Int : Java.Int;
                              P2_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;
   --  can raise Java.Io.IOException.Except

   function New_ServerSocket (P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Bind (This : access Typ;
                   P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Bind (This : access Typ;
                   P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                   P2_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function GetInetAddress (This : access Typ)
                            return access Java.Net.InetAddress.Typ'Class;

   function GetLocalPort (This : access Typ)
                          return Java.Int;

   function GetLocalSocketAddress (This : access Typ)
                                   return access Java.Net.SocketAddress.Typ'Class;

   function accept_K (This : access Typ)
                      return access Java.Net.Socket.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final  protected
   procedure ImplAccept (This : access Typ;
                         P1_Socket : access Standard.Java.Net.Socket.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function GetChannel (This : access Typ)
                        return access Java.Nio.Channels.ServerSocketChannel.Typ'Class;

   function IsBound (This : access Typ)
                     return Java.Boolean;

   function IsClosed (This : access Typ)
                      return Java.Boolean;

   --  synchronized
   procedure SetSoTimeout (This : access Typ;
                           P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetSoTimeout (This : access Typ)
                          return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure SetReuseAddress (This : access Typ;
                              P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   function GetReuseAddress (This : access Typ)
                             return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure SetSocketFactory (P1_SocketImplFactory : access Standard.Java.Net.SocketImplFactory.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure SetReceiveBufferSize (This : access Typ;
                                   P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetReceiveBufferSize (This : access Typ)
                                  return Java.Int;
   --  can raise Java.Net.SocketException.Except

   procedure SetPerformancePreferences (This : access Typ;
                                        P1_Int : Java.Int;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServerSocket);
   pragma Import (Java, Bind, "bind");
   pragma Import (Java, GetInetAddress, "getInetAddress");
   pragma Import (Java, GetLocalPort, "getLocalPort");
   pragma Import (Java, GetLocalSocketAddress, "getLocalSocketAddress");
   pragma Import (Java, accept_K, "accept");
   pragma Import (Java, ImplAccept, "implAccept");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GetChannel, "getChannel");
   pragma Import (Java, IsBound, "isBound");
   pragma Import (Java, IsClosed, "isClosed");
   pragma Import (Java, SetSoTimeout, "setSoTimeout");
   pragma Import (Java, GetSoTimeout, "getSoTimeout");
   pragma Import (Java, SetReuseAddress, "setReuseAddress");
   pragma Import (Java, GetReuseAddress, "getReuseAddress");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SetSocketFactory, "setSocketFactory");
   pragma Import (Java, SetReceiveBufferSize, "setReceiveBufferSize");
   pragma Import (Java, GetReceiveBufferSize, "getReceiveBufferSize");
   pragma Import (Java, SetPerformancePreferences, "setPerformancePreferences");

end Java.Net.ServerSocket;
pragma Import (Java, Java.Net.ServerSocket, "java.net.ServerSocket");
pragma Extensions_Allowed (Off);
