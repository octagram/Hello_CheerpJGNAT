pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
limited with Java.Net.Proxy;
limited with Java.Net.SocketAddress;
limited with Java.Net.SocketImpl;
limited with Java.Net.SocketImplFactory;
limited with Java.Nio.Channels.SocketChannel;
with Java.Lang.Object;

package Java.Net.Socket is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Socket (This : Ref := null)
                        return Ref;

   function New_Socket (P1_Proxy : access Standard.Java.Net.Proxy.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   --  protected
   function New_Socket (P1_SocketImpl : access Standard.Java.Net.SocketImpl.Typ'Class; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Net.SocketException.Except

   function New_Socket (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Net.UnknownHostException.Except and
   --  Java.Io.IOException.Except

   function New_Socket (P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                        P2_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Io.IOException.Except

   function New_Socket (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int;
                        P3_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                        P4_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Io.IOException.Except

   function New_Socket (P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                        P2_Int : Java.Int;
                        P3_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                        P4_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Connect (This : access Typ;
                      P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Connect (This : access Typ;
                      P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                      P2_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Bind (This : access Typ;
                   P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetInetAddress (This : access Typ)
                            return access Java.Net.InetAddress.Typ'Class;

   function GetLocalAddress (This : access Typ)
                             return access Java.Net.InetAddress.Typ'Class;

   function GetPort (This : access Typ)
                     return Java.Int;

   function GetLocalPort (This : access Typ)
                          return Java.Int;

   function GetRemoteSocketAddress (This : access Typ)
                                    return access Java.Net.SocketAddress.Typ'Class;

   function GetLocalSocketAddress (This : access Typ)
                                   return access Java.Net.SocketAddress.Typ'Class;

   function GetChannel (This : access Typ)
                        return access Java.Nio.Channels.SocketChannel.Typ'Class;

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure SetTcpNoDelay (This : access Typ;
                            P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   function GetTcpNoDelay (This : access Typ)
                           return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   procedure SetSoLinger (This : access Typ;
                          P1_Boolean : Java.Boolean;
                          P2_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   function GetSoLinger (This : access Typ)
                         return Java.Int;
   --  can raise Java.Net.SocketException.Except

   procedure SendUrgentData (This : access Typ;
                             P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure SetOOBInline (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   function GetOOBInline (This : access Typ)
                          return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetSoTimeout (This : access Typ;
                           P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetSoTimeout (This : access Typ)
                          return Java.Int;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetSendBufferSize (This : access Typ;
                                P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetSendBufferSize (This : access Typ)
                               return Java.Int;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure SetReceiveBufferSize (This : access Typ;
                                   P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   function GetReceiveBufferSize (This : access Typ)
                                  return Java.Int;
   --  can raise Java.Net.SocketException.Except

   procedure SetKeepAlive (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   function GetKeepAlive (This : access Typ)
                          return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   procedure SetTrafficClass (This : access Typ;
                              P1_Int : Java.Int);
   --  can raise Java.Net.SocketException.Except

   function GetTrafficClass (This : access Typ)
                             return Java.Int;
   --  can raise Java.Net.SocketException.Except

   procedure SetReuseAddress (This : access Typ;
                              P1_Boolean : Java.Boolean);
   --  can raise Java.Net.SocketException.Except

   function GetReuseAddress (This : access Typ)
                             return Java.Boolean;
   --  can raise Java.Net.SocketException.Except

   --  synchronized
   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure ShutdownInput (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure ShutdownOutput (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IsConnected (This : access Typ)
                         return Java.Boolean;

   function IsBound (This : access Typ)
                     return Java.Boolean;

   function IsClosed (This : access Typ)
                      return Java.Boolean;

   function IsInputShutdown (This : access Typ)
                             return Java.Boolean;

   function IsOutputShutdown (This : access Typ)
                              return Java.Boolean;

   --  synchronized
   procedure SetSocketImplFactory (P1_SocketImplFactory : access Standard.Java.Net.SocketImplFactory.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure SetPerformancePreferences (This : access Typ;
                                        P1_Int : Java.Int;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Socket);
   pragma Import (Java, Connect, "connect");
   pragma Import (Java, Bind, "bind");
   pragma Import (Java, GetInetAddress, "getInetAddress");
   pragma Import (Java, GetLocalAddress, "getLocalAddress");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, GetLocalPort, "getLocalPort");
   pragma Import (Java, GetRemoteSocketAddress, "getRemoteSocketAddress");
   pragma Import (Java, GetLocalSocketAddress, "getLocalSocketAddress");
   pragma Import (Java, GetChannel, "getChannel");
   pragma Import (Java, GetInputStream, "getInputStream");
   pragma Import (Java, GetOutputStream, "getOutputStream");
   pragma Import (Java, SetTcpNoDelay, "setTcpNoDelay");
   pragma Import (Java, GetTcpNoDelay, "getTcpNoDelay");
   pragma Import (Java, SetSoLinger, "setSoLinger");
   pragma Import (Java, GetSoLinger, "getSoLinger");
   pragma Import (Java, SendUrgentData, "sendUrgentData");
   pragma Import (Java, SetOOBInline, "setOOBInline");
   pragma Import (Java, GetOOBInline, "getOOBInline");
   pragma Import (Java, SetSoTimeout, "setSoTimeout");
   pragma Import (Java, GetSoTimeout, "getSoTimeout");
   pragma Import (Java, SetSendBufferSize, "setSendBufferSize");
   pragma Import (Java, GetSendBufferSize, "getSendBufferSize");
   pragma Import (Java, SetReceiveBufferSize, "setReceiveBufferSize");
   pragma Import (Java, GetReceiveBufferSize, "getReceiveBufferSize");
   pragma Import (Java, SetKeepAlive, "setKeepAlive");
   pragma Import (Java, GetKeepAlive, "getKeepAlive");
   pragma Import (Java, SetTrafficClass, "setTrafficClass");
   pragma Import (Java, GetTrafficClass, "getTrafficClass");
   pragma Import (Java, SetReuseAddress, "setReuseAddress");
   pragma Import (Java, GetReuseAddress, "getReuseAddress");
   pragma Import (Java, Close, "close");
   pragma Import (Java, ShutdownInput, "shutdownInput");
   pragma Import (Java, ShutdownOutput, "shutdownOutput");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IsConnected, "isConnected");
   pragma Import (Java, IsBound, "isBound");
   pragma Import (Java, IsClosed, "isClosed");
   pragma Import (Java, IsInputShutdown, "isInputShutdown");
   pragma Import (Java, IsOutputShutdown, "isOutputShutdown");
   pragma Import (Java, SetSocketImplFactory, "setSocketImplFactory");
   pragma Import (Java, SetPerformancePreferences, "setPerformancePreferences");

end Java.Net.Socket;
pragma Import (Java, Java.Net.Socket, "java.net.Socket");
pragma Extensions_Allowed (Off);
