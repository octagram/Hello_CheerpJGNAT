pragma Extensions_Allowed (On);
limited with Java.Io.FileDescriptor;
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
limited with Java.Net.SocketAddress;
with Java.Lang.Object;
with Java.Net.SocketOptions;

package Java.Net.SocketImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SocketOptions_I : Java.Net.SocketOptions.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Fd : access Java.Io.FileDescriptor.Typ'Class;
      pragma Import (Java, Fd, "fd");

      --  protected
      Address : access Java.Net.InetAddress.Typ'Class;
      pragma Import (Java, Address, "address");

      --  protected
      Port : Java.Int;
      pragma Import (Java, Port, "port");

      --  protected
      Localport : Java.Int;
      pragma Import (Java, Localport, "localport");

   end record;

   function New_SocketImpl (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Create (This : access Typ;
                     P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Connect (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Connect (This : access Typ;
                      P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                      P2_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Connect (This : access Typ;
                      P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class;
                      P2_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Bind (This : access Typ;
                   P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                   P2_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Listen (This : access Typ;
                     P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure accept_K (This : access Typ;
                       P1_SocketImpl : access Standard.Java.Net.SocketImpl.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function Available (This : access Typ)
                       return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure ShutdownInput (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure ShutdownOutput (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetFileDescriptor (This : access Typ)
                               return access Java.Io.FileDescriptor.Typ'Class;

   --  protected
   function GetInetAddress (This : access Typ)
                            return access Java.Net.InetAddress.Typ'Class;

   --  protected
   function GetPort (This : access Typ)
                     return Java.Int;

   --  protected
   function SupportsUrgentData (This : access Typ)
                                return Java.Boolean;

   --  protected
   procedure SendUrgentData (This : access Typ;
                             P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetLocalPort (This : access Typ)
                          return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   procedure SetPerformancePreferences (This : access Typ;
                                        P1_Int : Java.Int;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SocketImpl);
   pragma Export (Java, Create, "create");
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, Bind, "bind");
   pragma Export (Java, Listen, "listen");
   pragma Export (Java, accept_K, "accept");
   pragma Export (Java, GetInputStream, "getInputStream");
   pragma Export (Java, GetOutputStream, "getOutputStream");
   pragma Export (Java, Available, "available");
   pragma Export (Java, Close, "close");
   pragma Export (Java, ShutdownInput, "shutdownInput");
   pragma Export (Java, ShutdownOutput, "shutdownOutput");
   pragma Export (Java, GetFileDescriptor, "getFileDescriptor");
   pragma Export (Java, GetInetAddress, "getInetAddress");
   pragma Export (Java, GetPort, "getPort");
   pragma Export (Java, SupportsUrgentData, "supportsUrgentData");
   pragma Export (Java, SendUrgentData, "sendUrgentData");
   pragma Export (Java, GetLocalPort, "getLocalPort");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, SetPerformancePreferences, "setPerformancePreferences");

end Java.Net.SocketImpl;
pragma Import (Java, Java.Net.SocketImpl, "java.net.SocketImpl");
pragma Extensions_Allowed (Off);
