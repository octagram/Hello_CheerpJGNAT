pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Net.SocketOptions is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetOption (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Net.SocketException.Except

   function GetOption (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Net.SocketException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TCP_NODELAY : constant Java.Int;

   --  final
   SO_BINDADDR : constant Java.Int;

   --  final
   SO_REUSEADDR : constant Java.Int;

   --  final
   SO_BROADCAST : constant Java.Int;

   --  final
   IP_MULTICAST_IF : constant Java.Int;

   --  final
   IP_MULTICAST_IF2 : constant Java.Int;

   --  final
   IP_MULTICAST_LOOP : constant Java.Int;

   --  final
   IP_TOS : constant Java.Int;

   --  final
   SO_LINGER : constant Java.Int;

   --  final
   SO_TIMEOUT : constant Java.Int;

   --  final
   SO_SNDBUF : constant Java.Int;

   --  final
   SO_RCVBUF : constant Java.Int;

   --  final
   SO_KEEPALIVE : constant Java.Int;

   --  final
   SO_OOBINLINE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetOption, "setOption");
   pragma Export (Java, GetOption, "getOption");
   pragma Import (Java, TCP_NODELAY, "TCP_NODELAY");
   pragma Import (Java, SO_BINDADDR, "SO_BINDADDR");
   pragma Import (Java, SO_REUSEADDR, "SO_REUSEADDR");
   pragma Import (Java, SO_BROADCAST, "SO_BROADCAST");
   pragma Import (Java, IP_MULTICAST_IF, "IP_MULTICAST_IF");
   pragma Import (Java, IP_MULTICAST_IF2, "IP_MULTICAST_IF2");
   pragma Import (Java, IP_MULTICAST_LOOP, "IP_MULTICAST_LOOP");
   pragma Import (Java, IP_TOS, "IP_TOS");
   pragma Import (Java, SO_LINGER, "SO_LINGER");
   pragma Import (Java, SO_TIMEOUT, "SO_TIMEOUT");
   pragma Import (Java, SO_SNDBUF, "SO_SNDBUF");
   pragma Import (Java, SO_RCVBUF, "SO_RCVBUF");
   pragma Import (Java, SO_KEEPALIVE, "SO_KEEPALIVE");
   pragma Import (Java, SO_OOBINLINE, "SO_OOBINLINE");

end Java.Net.SocketOptions;
pragma Import (Java, Java.Net.SocketOptions, "java.net.SocketOptions");
pragma Extensions_Allowed (Off);
