pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.PermissionCollection;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Guard;
with Java.Security.Permission;

package Java.Net.SocketPermission is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Guard_I : Java.Security.Guard.Ref)
    is new Java.Security.Permission.Typ(Serializable_I,
                                        Guard_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SocketPermission (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Implies (This : access Typ;
                     P1_Permission : access Standard.Java.Security.Permission.Typ'Class)
                     return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function GetActions (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function NewPermissionCollection (This : access Typ)
                                     return access Java.Security.PermissionCollection.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SocketPermission);
   pragma Import (Java, Implies, "implies");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, NewPermissionCollection, "newPermissionCollection");

end Java.Net.SocketPermission;
pragma Import (Java, Java.Net.SocketPermission, "java.net.SocketPermission");
pragma Extensions_Allowed (Off);
