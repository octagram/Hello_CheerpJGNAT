pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.InterruptedIOException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Net.SocketTimeoutException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Io.InterruptedIOException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SocketTimeoutException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   function New_SocketTimeoutException (This : Ref := null)
                                        return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.net.SocketTimeoutException");
   pragma Java_Constructor (New_SocketTimeoutException);

end Java.Net.SocketTimeoutException;
pragma Import (Java, Java.Net.SocketTimeoutException, "java.net.SocketTimeoutException");
pragma Extensions_Allowed (Off);
