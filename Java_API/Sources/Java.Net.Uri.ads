pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.URL;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Net.URI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_URI (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.URISyntaxException.Except

   function New_URI (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                     P4_Int : Java.Int;
                     P5_String : access Standard.Java.Lang.String.Typ'Class;
                     P6_String : access Standard.Java.Lang.String.Typ'Class;
                     P7_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.URISyntaxException.Except

   function New_URI (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                     P4_String : access Standard.Java.Lang.String.Typ'Class;
                     P5_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.URISyntaxException.Except

   function New_URI (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                     P4_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.URISyntaxException.Except

   function New_URI (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.URISyntaxException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Net.URI.Typ'Class;

   function ParseServerAuthority (This : access Typ)
                                  return access Java.Net.URI.Typ'Class;
   --  can raise Java.Net.URISyntaxException.Except

   function Normalize (This : access Typ)
                       return access Java.Net.URI.Typ'Class;

   function Resolve (This : access Typ;
                     P1_URI : access Standard.Java.Net.URI.Typ'Class)
                     return access Java.Net.URI.Typ'Class;

   function Resolve (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Net.URI.Typ'Class;

   function Relativize (This : access Typ;
                        P1_URI : access Standard.Java.Net.URI.Typ'Class)
                        return access Java.Net.URI.Typ'Class;

   function ToURL (This : access Typ)
                   return access Java.Net.URL.Typ'Class;
   --  can raise Java.Net.MalformedURLException.Except

   function GetScheme (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function IsAbsolute (This : access Typ)
                        return Java.Boolean;

   function IsOpaque (This : access Typ)
                      return Java.Boolean;

   function GetRawSchemeSpecificPart (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetSchemeSpecificPart (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   function GetRawAuthority (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetAuthority (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetRawUserInfo (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetUserInfo (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetHost (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetPort (This : access Typ)
                     return Java.Int;

   function GetRawPath (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetPath (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetRawQuery (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetQuery (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetRawFragment (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetFragment (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function CompareTo (This : access Typ;
                       P1_URI : access Standard.Java.Net.URI.Typ'Class)
                       return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToASCIIString (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_URI);
   pragma Import (Java, Create, "create");
   pragma Import (Java, ParseServerAuthority, "parseServerAuthority");
   pragma Import (Java, Normalize, "normalize");
   pragma Import (Java, Resolve, "resolve");
   pragma Import (Java, Relativize, "relativize");
   pragma Import (Java, ToURL, "toURL");
   pragma Import (Java, GetScheme, "getScheme");
   pragma Import (Java, IsAbsolute, "isAbsolute");
   pragma Import (Java, IsOpaque, "isOpaque");
   pragma Import (Java, GetRawSchemeSpecificPart, "getRawSchemeSpecificPart");
   pragma Import (Java, GetSchemeSpecificPart, "getSchemeSpecificPart");
   pragma Import (Java, GetRawAuthority, "getRawAuthority");
   pragma Import (Java, GetAuthority, "getAuthority");
   pragma Import (Java, GetRawUserInfo, "getRawUserInfo");
   pragma Import (Java, GetUserInfo, "getUserInfo");
   pragma Import (Java, GetHost, "getHost");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, GetRawPath, "getRawPath");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, GetRawQuery, "getRawQuery");
   pragma Import (Java, GetQuery, "getQuery");
   pragma Import (Java, GetRawFragment, "getRawFragment");
   pragma Import (Java, GetFragment, "getFragment");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToASCIIString, "toASCIIString");

end Java.Net.URI;
pragma Import (Java, Java.Net.URI, "java.net.URI");
pragma Extensions_Allowed (Off);
