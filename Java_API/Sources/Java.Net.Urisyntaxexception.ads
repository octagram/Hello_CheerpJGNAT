pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Java.Net.URISyntaxException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_URISyntaxException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   function New_URISyntaxException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInput (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetReason (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetIndex (This : access Typ)
                      return Java.Int;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.net.URISyntaxException");
   pragma Java_Constructor (New_URISyntaxException);
   pragma Import (Java, GetInput, "getInput");
   pragma Import (Java, GetReason, "getReason");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, GetMessage, "getMessage");

end Java.Net.URISyntaxException;
pragma Import (Java, Java.Net.URISyntaxException, "java.net.URISyntaxException");
pragma Extensions_Allowed (Off);
