pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.Proxy;
limited with Java.Net.URI;
limited with Java.Net.URLConnection;
limited with Java.Net.URLStreamHandler;
limited with Java.Net.URLStreamHandlerFactory;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Net.URL is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_URL (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_Int : Java.Int;
                     P4_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   function New_URL (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   function New_URL (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_Int : Java.Int;
                     P4_String : access Standard.Java.Lang.String.Typ'Class;
                     P5_URLStreamHandler : access Standard.Java.Net.URLStreamHandler.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   function New_URL (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   function New_URL (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   function New_URL (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_URLStreamHandler : access Standard.Java.Net.URLStreamHandler.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Set (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                  P3_Int : Java.Int;
                  P4_String : access Standard.Java.Lang.String.Typ'Class;
                  P5_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure Set (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                  P3_Int : Java.Int;
                  P4_String : access Standard.Java.Lang.String.Typ'Class;
                  P5_String : access Standard.Java.Lang.String.Typ'Class;
                  P6_String : access Standard.Java.Lang.String.Typ'Class;
                  P7_String : access Standard.Java.Lang.String.Typ'Class;
                  P8_String : access Standard.Java.Lang.String.Typ'Class);

   function GetQuery (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetPath (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetUserInfo (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAuthority (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetPort (This : access Typ)
                     return Java.Int;

   function GetDefaultPort (This : access Typ)
                            return Java.Int;

   function GetProtocol (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetHost (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetFile (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetRef (This : access Typ)
                    return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  synchronized
   function HashCode (This : access Typ)
                      return Java.Int;

   function SameFile (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class)
                      return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToExternalForm (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function ToURI (This : access Typ)
                   return access Java.Net.URI.Typ'Class;
   --  can raise Java.Net.URISyntaxException.Except

   function OpenConnection (This : access Typ)
                            return access Java.Net.URLConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenConnection (This : access Typ;
                            P1_Proxy : access Standard.Java.Net.Proxy.Typ'Class)
                            return access Java.Net.URLConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function OpenStream (This : access Typ)
                        return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function GetContent (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function GetContent (This : access Typ;
                        P1_Class_Arr : access Java.Lang.Class.Arr_Obj)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure SetURLStreamHandlerFactory (P1_URLStreamHandlerFactory : access Standard.Java.Net.URLStreamHandlerFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_URL);
   pragma Import (Java, Set, "set");
   pragma Import (Java, GetQuery, "getQuery");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, GetUserInfo, "getUserInfo");
   pragma Import (Java, GetAuthority, "getAuthority");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, GetDefaultPort, "getDefaultPort");
   pragma Import (Java, GetProtocol, "getProtocol");
   pragma Import (Java, GetHost, "getHost");
   pragma Import (Java, GetFile, "getFile");
   pragma Import (Java, GetRef, "getRef");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, SameFile, "sameFile");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToExternalForm, "toExternalForm");
   pragma Import (Java, ToURI, "toURI");
   pragma Import (Java, OpenConnection, "openConnection");
   pragma Import (Java, OpenStream, "openStream");
   pragma Import (Java, GetContent, "getContent");
   pragma Import (Java, SetURLStreamHandlerFactory, "setURLStreamHandlerFactory");

end Java.Net.URL;
pragma Import (Java, Java.Net.URL, "java.net.URL");
pragma Extensions_Allowed (Off);
