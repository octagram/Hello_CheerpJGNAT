pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.Package_K;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Net.URLStreamHandlerFactory;
limited with Java.Security.CodeSource;
limited with Java.Security.PermissionCollection;
limited with Java.Util.Enumeration;
limited with Java.Util.Jar.Manifest;
with Java.Lang.Object;
with Java.Security.SecureClassLoader;

package Java.Net.URLClassLoader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Security.SecureClassLoader.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_URLClassLoader (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                                P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_URLClassLoader (P1_URL_Arr : access Java.Net.URL.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   function New_URLClassLoader (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                                P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                                P3_URLStreamHandlerFactory : access Standard.Java.Net.URLStreamHandlerFactory.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure AddURL (This : access Typ;
                     P1_URL : access Standard.Java.Net.URL.Typ'Class);

   function GetURLs (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   --  protected
   function FindClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   --  protected
   function DefinePackage (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Manifest : access Standard.Java.Util.Jar.Manifest.Typ'Class;
                           P3_URL : access Standard.Java.Net.URL.Typ'Class)
                           return access Java.Lang.Package_K.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function FindResource (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Net.URL.Typ'Class;

   function FindResources (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Util.Enumeration.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetPermissions (This : access Typ;
                            P1_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                            return access Java.Security.PermissionCollection.Typ'Class;

   function NewInstance (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                         P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Java.Net.URLClassLoader.Typ'Class;

   function NewInstance (P1_URL_Arr : access Java.Net.URL.Arr_Obj)
                         return access Java.Net.URLClassLoader.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_URLClassLoader);
   pragma Import (Java, AddURL, "addURL");
   pragma Import (Java, GetURLs, "getURLs");
   pragma Import (Java, FindClass, "findClass");
   pragma Import (Java, DefinePackage, "definePackage");
   pragma Import (Java, FindResource, "findResource");
   pragma Import (Java, FindResources, "findResources");
   pragma Import (Java, GetPermissions, "getPermissions");
   pragma Import (Java, NewInstance, "newInstance");

end Java.Net.URLClassLoader;
pragma Import (Java, Java.Net.URLClassLoader, "java.net.URLClassLoader");
pragma Extensions_Allowed (Off);
