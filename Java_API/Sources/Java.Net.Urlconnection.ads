pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.ContentHandlerFactory;
limited with Java.Net.FileNameMap;
limited with Java.Net.URL;
limited with Java.Security.Permission;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Net.URLConnection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Url : access Java.Net.URL.Typ'Class;
      pragma Import (Java, Url, "url");

      --  protected
      DoInput : Java.Boolean;
      pragma Import (Java, DoInput, "doInput");

      --  protected
      DoOutput : Java.Boolean;
      pragma Import (Java, DoOutput, "doOutput");

      --  protected
      AllowUserInteraction : Java.Boolean;
      pragma Import (Java, AllowUserInteraction, "allowUserInteraction");

      --  protected
      UseCaches : Java.Boolean;
      pragma Import (Java, UseCaches, "useCaches");

      --  protected
      IfModifiedSince : Java.Long;
      pragma Import (Java, IfModifiedSince, "ifModifiedSince");

      --  protected
      Connected : Java.Boolean;
      pragma Import (Java, Connected, "connected");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetFileNameMap return access Java.Net.FileNameMap.Typ'Class;

   procedure SetFileNameMap (P1_FileNameMap : access Standard.Java.Net.FileNameMap.Typ'Class);

   procedure Connect (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure SetConnectTimeout (This : access Typ;
                                P1_Int : Java.Int);

   function GetConnectTimeout (This : access Typ)
                               return Java.Int;

   procedure SetReadTimeout (This : access Typ;
                             P1_Int : Java.Int);

   function GetReadTimeout (This : access Typ)
                            return Java.Int;

   --  protected
   function New_URLConnection (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function GetURL (This : access Typ)
                    return access Java.Net.URL.Typ'Class;

   function GetContentLength (This : access Typ)
                              return Java.Int;

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetContentEncoding (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function GetExpiration (This : access Typ)
                           return Java.Long;

   function GetDate (This : access Typ)
                     return Java.Long;

   function GetLastModified (This : access Typ)
                             return Java.Long;

   function GetHeaderField (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetHeaderFields (This : access Typ)
                             return access Java.Util.Map.Typ'Class;

   function GetHeaderFieldInt (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Int : Java.Int)
                               return Java.Int;

   function GetHeaderFieldDate (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Long : Java.Long)
                                return Java.Long;

   function GetHeaderFieldKey (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class;

   function GetHeaderField (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   function GetContent (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetContent (This : access Typ;
                        P1_Class_Arr : access Java.Lang.Class.Arr_Obj)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetPermission (This : access Typ)
                           return access Java.Security.Permission.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetDoInput (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function GetDoInput (This : access Typ)
                        return Java.Boolean;

   procedure SetDoOutput (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function GetDoOutput (This : access Typ)
                         return Java.Boolean;

   procedure SetAllowUserInteraction (This : access Typ;
                                      P1_Boolean : Java.Boolean);

   function GetAllowUserInteraction (This : access Typ)
                                     return Java.Boolean;

   procedure SetDefaultAllowUserInteraction (P1_Boolean : Java.Boolean);

   function GetDefaultAllowUserInteraction return Java.Boolean;

   procedure SetUseCaches (This : access Typ;
                           P1_Boolean : Java.Boolean);

   function GetUseCaches (This : access Typ)
                          return Java.Boolean;

   procedure SetIfModifiedSince (This : access Typ;
                                 P1_Long : Java.Long);

   function GetIfModifiedSince (This : access Typ)
                                return Java.Long;

   function GetDefaultUseCaches (This : access Typ)
                                 return Java.Boolean;

   procedure SetDefaultUseCaches (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   procedure SetRequestProperty (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AddRequestProperty (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class);

   function GetRequestProperty (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function GetRequestProperties (This : access Typ)
                                  return access Java.Util.Map.Typ'Class;

   --  synchronized
   procedure SetContentHandlerFactory (P1_ContentHandlerFactory : access Standard.Java.Net.ContentHandlerFactory.Typ'Class);

   function GuessContentTypeFromName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Java.Lang.String.Typ'Class;

   function GuessContentTypeFromStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                        return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetFileNameMap, "getFileNameMap");
   pragma Export (Java, SetFileNameMap, "setFileNameMap");
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, SetConnectTimeout, "setConnectTimeout");
   pragma Export (Java, GetConnectTimeout, "getConnectTimeout");
   pragma Export (Java, SetReadTimeout, "setReadTimeout");
   pragma Export (Java, GetReadTimeout, "getReadTimeout");
   pragma Java_Constructor (New_URLConnection);
   pragma Export (Java, GetURL, "getURL");
   pragma Export (Java, GetContentLength, "getContentLength");
   pragma Export (Java, GetContentType, "getContentType");
   pragma Export (Java, GetContentEncoding, "getContentEncoding");
   pragma Export (Java, GetExpiration, "getExpiration");
   pragma Export (Java, GetDate, "getDate");
   pragma Export (Java, GetLastModified, "getLastModified");
   pragma Export (Java, GetHeaderField, "getHeaderField");
   pragma Export (Java, GetHeaderFields, "getHeaderFields");
   pragma Export (Java, GetHeaderFieldInt, "getHeaderFieldInt");
   pragma Export (Java, GetHeaderFieldDate, "getHeaderFieldDate");
   pragma Export (Java, GetHeaderFieldKey, "getHeaderFieldKey");
   pragma Export (Java, GetContent, "getContent");
   pragma Export (Java, GetPermission, "getPermission");
   pragma Export (Java, GetInputStream, "getInputStream");
   pragma Export (Java, GetOutputStream, "getOutputStream");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, SetDoInput, "setDoInput");
   pragma Export (Java, GetDoInput, "getDoInput");
   pragma Export (Java, SetDoOutput, "setDoOutput");
   pragma Export (Java, GetDoOutput, "getDoOutput");
   pragma Export (Java, SetAllowUserInteraction, "setAllowUserInteraction");
   pragma Export (Java, GetAllowUserInteraction, "getAllowUserInteraction");
   pragma Export (Java, SetDefaultAllowUserInteraction, "setDefaultAllowUserInteraction");
   pragma Export (Java, GetDefaultAllowUserInteraction, "getDefaultAllowUserInteraction");
   pragma Export (Java, SetUseCaches, "setUseCaches");
   pragma Export (Java, GetUseCaches, "getUseCaches");
   pragma Export (Java, SetIfModifiedSince, "setIfModifiedSince");
   pragma Export (Java, GetIfModifiedSince, "getIfModifiedSince");
   pragma Export (Java, GetDefaultUseCaches, "getDefaultUseCaches");
   pragma Export (Java, SetDefaultUseCaches, "setDefaultUseCaches");
   pragma Export (Java, SetRequestProperty, "setRequestProperty");
   pragma Export (Java, AddRequestProperty, "addRequestProperty");
   pragma Export (Java, GetRequestProperty, "getRequestProperty");
   pragma Export (Java, GetRequestProperties, "getRequestProperties");
   pragma Export (Java, SetContentHandlerFactory, "setContentHandlerFactory");
   pragma Export (Java, GuessContentTypeFromName, "guessContentTypeFromName");
   pragma Export (Java, GuessContentTypeFromStream, "guessContentTypeFromStream");

end Java.Net.URLConnection;
pragma Import (Java, Java.Net.URLConnection, "java.net.URLConnection");
pragma Extensions_Allowed (Off);
