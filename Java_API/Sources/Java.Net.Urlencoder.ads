pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Net.URLEncoder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Encode (P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.UnsupportedEncodingException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Encode, "encode");

end Java.Net.URLEncoder;
pragma Import (Java, Java.Net.URLEncoder, "java.net.URLEncoder");
pragma Extensions_Allowed (Off);
