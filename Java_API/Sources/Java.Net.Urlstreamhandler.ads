pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
limited with Java.Net.Proxy;
limited with Java.Net.URL;
limited with Java.Net.URLConnection;
with Java.Lang.Object;

package Java.Net.URLStreamHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_URLStreamHandler (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function OpenConnection (This : access Typ;
                            P1_URL : access Standard.Java.Net.URL.Typ'Class)
                            return access Java.Net.URLConnection.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function OpenConnection (This : access Typ;
                            P1_URL : access Standard.Java.Net.URL.Typ'Class;
                            P2_Proxy : access Standard.Java.Net.Proxy.Typ'Class)
                            return access Java.Net.URLConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure ParseURL (This : access Typ;
                       P1_URL : access Standard.Java.Net.URL.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   --  protected
   function GetDefaultPort (This : access Typ)
                            return Java.Int;

   --  protected
   function Equals (This : access Typ;
                    P1_URL : access Standard.Java.Net.URL.Typ'Class;
                    P2_URL : access Standard.Java.Net.URL.Typ'Class)
                    return Java.Boolean;

   --  protected
   function HashCode (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class)
                      return Java.Int;

   --  protected
   function SameFile (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class;
                      P2_URL : access Standard.Java.Net.URL.Typ'Class)
                      return Java.Boolean;

   --  protected  synchronized
   function GetHostAddress (This : access Typ;
                            P1_URL : access Standard.Java.Net.URL.Typ'Class)
                            return access Java.Net.InetAddress.Typ'Class;

   --  protected
   function HostsEqual (This : access Typ;
                        P1_URL : access Standard.Java.Net.URL.Typ'Class;
                        P2_URL : access Standard.Java.Net.URL.Typ'Class)
                        return Java.Boolean;

   --  protected
   function ToExternalForm (This : access Typ;
                            P1_URL : access Standard.Java.Net.URL.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   procedure SetURL (This : access Typ;
                     P1_URL : access Standard.Java.Net.URL.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                     P4_Int : Java.Int;
                     P5_String : access Standard.Java.Lang.String.Typ'Class;
                     P6_String : access Standard.Java.Lang.String.Typ'Class;
                     P7_String : access Standard.Java.Lang.String.Typ'Class;
                     P8_String : access Standard.Java.Lang.String.Typ'Class;
                     P9_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_URLStreamHandler);
   pragma Export (Java, OpenConnection, "openConnection");
   pragma Export (Java, ParseURL, "parseURL");
   pragma Export (Java, GetDefaultPort, "getDefaultPort");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, SameFile, "sameFile");
   pragma Export (Java, GetHostAddress, "getHostAddress");
   pragma Export (Java, HostsEqual, "hostsEqual");
   pragma Export (Java, ToExternalForm, "toExternalForm");
   pragma Export (Java, SetURL, "setURL");

end Java.Net.URLStreamHandler;
pragma Import (Java, Java.Net.URLStreamHandler, "java.net.URLStreamHandler");
pragma Extensions_Allowed (Off);
