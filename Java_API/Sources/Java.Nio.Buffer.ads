pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Nio.Buffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Capacity (This : access Typ)
                      return Java.Int;

   --  final
   function Position (This : access Typ)
                      return Java.Int;

   --  final
   function Position (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Nio.Buffer.Typ'Class;

   --  final
   function Limit (This : access Typ)
                   return Java.Int;

   --  final
   function Limit (This : access Typ;
                   P1_Int : Java.Int)
                   return access Java.Nio.Buffer.Typ'Class;

   --  final
   function Mark (This : access Typ)
                  return access Java.Nio.Buffer.Typ'Class;

   --  final
   function Reset (This : access Typ)
                   return access Java.Nio.Buffer.Typ'Class;

   --  final
   function Clear (This : access Typ)
                   return access Java.Nio.Buffer.Typ'Class;

   --  final
   function Flip (This : access Typ)
                  return access Java.Nio.Buffer.Typ'Class;

   --  final
   function Rewind (This : access Typ)
                    return access Java.Nio.Buffer.Typ'Class;

   --  final
   function Remaining (This : access Typ)
                       return Java.Int;

   --  final
   function HasRemaining (This : access Typ)
                          return Java.Boolean;

   function IsReadOnly (This : access Typ)
                        return Java.Boolean is abstract;

   function HasArray (This : access Typ)
                      return Java.Boolean is abstract;

   function array_K (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function ArrayOffset (This : access Typ)
                         return Java.Int is abstract;

   function IsDirect (This : access Typ)
                      return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Capacity, "capacity");
   pragma Import (Java, Position, "position");
   pragma Import (Java, Limit, "limit");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Flip, "flip");
   pragma Import (Java, Rewind, "rewind");
   pragma Import (Java, Remaining, "remaining");
   pragma Import (Java, HasRemaining, "hasRemaining");
   pragma Export (Java, IsReadOnly, "isReadOnly");
   pragma Export (Java, HasArray, "hasArray");
   pragma Export (Java, array_K, "array");
   pragma Export (Java, ArrayOffset, "arrayOffset");
   pragma Export (Java, IsDirect, "isDirect");

end Java.Nio.Buffer;
pragma Import (Java, Java.Nio.Buffer, "java.nio.Buffer");
pragma Extensions_Allowed (Off);
