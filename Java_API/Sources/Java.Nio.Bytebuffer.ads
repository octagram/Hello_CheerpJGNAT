pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteOrder;
limited with Java.Nio.CharBuffer;
limited with Java.Nio.DoubleBuffer;
limited with Java.Nio.FloatBuffer;
limited with Java.Nio.IntBuffer;
limited with Java.Nio.LongBuffer;
limited with Java.Nio.ShortBuffer;
with Java.Lang.Comparable;
with Java.Lang.Object;
with Java.Nio.Buffer;

package Java.Nio.ByteBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Nio.Buffer.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function AllocateDirect (P1_Int : Java.Int)
                            return access Java.Nio.ByteBuffer.Typ'Class;

   function Allocate (P1_Int : Java.Int)
                      return access Java.Nio.ByteBuffer.Typ'Class;

   function Wrap (P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return access Java.Nio.ByteBuffer.Typ'Class;

   function Wrap (P1_Byte_Arr : Java.Byte_Arr)
                  return access Java.Nio.ByteBuffer.Typ'Class;

   function Slice (This : access Typ)
                   return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function Duplicate (This : access Typ)
                       return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function AsReadOnlyBuffer (This : access Typ)
                              return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function Get (This : access Typ)
                 return Java.Byte is abstract;

   function Put (This : access Typ;
                 P1_Byte : Java.Byte)
                 return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return Java.Byte is abstract;

   function Put (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Byte : Java.Byte)
                 return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_Byte_Arr : Java.Byte_Arr;
                 P2_Int : Java.Int;
                 P3_Int : Java.Int)
                 return access Java.Nio.ByteBuffer.Typ'Class;

   function Get (This : access Typ;
                 P1_Byte_Arr : Java.Byte_Arr)
                 return access Java.Nio.ByteBuffer.Typ'Class;

   function Put (This : access Typ;
                 P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                 return access Java.Nio.ByteBuffer.Typ'Class;

   function Put (This : access Typ;
                 P1_Byte_Arr : Java.Byte_Arr;
                 P2_Int : Java.Int;
                 P3_Int : Java.Int)
                 return access Java.Nio.ByteBuffer.Typ'Class;

   --  final
   function Put (This : access Typ;
                 P1_Byte_Arr : Java.Byte_Arr)
                 return access Java.Nio.ByteBuffer.Typ'Class;

   --  final
   function HasArray (This : access Typ)
                      return Java.Boolean;

   --  final
   function array_K (This : access Typ)
                     return Java.Byte_Arr;

   --  final
   function ArrayOffset (This : access Typ)
                         return Java.Int;

   function Compact (This : access Typ)
                     return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function IsDirect (This : access Typ)
                      return Java.Boolean is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                       return Java.Int;

   --  final
   function Order (This : access Typ)
                   return access Java.Nio.ByteOrder.Typ'Class;

   --  final
   function Order (This : access Typ;
                   P1_ByteOrder : access Standard.Java.Nio.ByteOrder.Typ'Class)
                   return access Java.Nio.ByteBuffer.Typ'Class;

   function GetChar (This : access Typ)
                     return Java.Char is abstract;

   function PutChar (This : access Typ;
                     P1_Char : Java.Char)
                     return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function GetChar (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Char is abstract;

   function PutChar (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Char : Java.Char)
                     return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function AsCharBuffer (This : access Typ)
                          return access Java.Nio.CharBuffer.Typ'Class is abstract;

   function GetShort (This : access Typ)
                      return Java.Short is abstract;

   function PutShort (This : access Typ;
                      P1_Short : Java.Short)
                      return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function GetShort (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Short is abstract;

   function PutShort (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Short : Java.Short)
                      return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function AsShortBuffer (This : access Typ)
                           return access Java.Nio.ShortBuffer.Typ'Class is abstract;

   function GetInt (This : access Typ)
                    return Java.Int is abstract;

   function PutInt (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function GetInt (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int is abstract;

   function PutInt (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function AsIntBuffer (This : access Typ)
                         return access Java.Nio.IntBuffer.Typ'Class is abstract;

   function GetLong (This : access Typ)
                     return Java.Long is abstract;

   function PutLong (This : access Typ;
                     P1_Long : Java.Long)
                     return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function GetLong (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Long is abstract;

   function PutLong (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Long : Java.Long)
                     return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function AsLongBuffer (This : access Typ)
                          return access Java.Nio.LongBuffer.Typ'Class is abstract;

   function GetFloat (This : access Typ)
                      return Java.Float is abstract;

   function PutFloat (This : access Typ;
                      P1_Float : Java.Float)
                      return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function GetFloat (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Float is abstract;

   function PutFloat (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Float : Java.Float)
                      return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function AsFloatBuffer (This : access Typ)
                           return access Java.Nio.FloatBuffer.Typ'Class is abstract;

   function GetDouble (This : access Typ)
                       return Java.Double is abstract;

   function PutDouble (This : access Typ;
                       P1_Double : Java.Double)
                       return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function GetDouble (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Double is abstract;

   function PutDouble (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Double : Java.Double)
                       return access Java.Nio.ByteBuffer.Typ'Class is abstract;

   function AsDoubleBuffer (This : access Typ)
                            return access Java.Nio.DoubleBuffer.Typ'Class is abstract;

   function array_K (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AllocateDirect, "allocateDirect");
   pragma Export (Java, Allocate, "allocate");
   pragma Export (Java, Wrap, "wrap");
   pragma Export (Java, Slice, "slice");
   pragma Export (Java, Duplicate, "duplicate");
   pragma Export (Java, AsReadOnlyBuffer, "asReadOnlyBuffer");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Put, "put");
   pragma Export (Java, HasArray, "hasArray");
   pragma Export (Java, array_K, "array");
   pragma Export (Java, ArrayOffset, "arrayOffset");
   pragma Export (Java, Compact, "compact");
   pragma Export (Java, IsDirect, "isDirect");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, CompareTo, "compareTo");
   pragma Export (Java, Order, "order");
   pragma Export (Java, GetChar, "getChar");
   pragma Export (Java, PutChar, "putChar");
   pragma Export (Java, AsCharBuffer, "asCharBuffer");
   pragma Export (Java, GetShort, "getShort");
   pragma Export (Java, PutShort, "putShort");
   pragma Export (Java, AsShortBuffer, "asShortBuffer");
   pragma Export (Java, GetInt, "getInt");
   pragma Export (Java, PutInt, "putInt");
   pragma Export (Java, AsIntBuffer, "asIntBuffer");
   pragma Export (Java, GetLong, "getLong");
   pragma Export (Java, PutLong, "putLong");
   pragma Export (Java, AsLongBuffer, "asLongBuffer");
   pragma Export (Java, GetFloat, "getFloat");
   pragma Export (Java, PutFloat, "putFloat");
   pragma Export (Java, AsFloatBuffer, "asFloatBuffer");
   pragma Export (Java, GetDouble, "getDouble");
   pragma Export (Java, PutDouble, "putDouble");
   pragma Export (Java, AsDoubleBuffer, "asDoubleBuffer");

end Java.Nio.ByteBuffer;
pragma Import (Java, Java.Nio.ByteBuffer, "java.nio.ByteBuffer");
pragma Extensions_Allowed (Off);
