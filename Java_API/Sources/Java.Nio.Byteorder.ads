pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Nio.ByteOrder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NativeOrder return access Java.Nio.ByteOrder.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BIG_ENDIAN : access Java.Nio.ByteOrder.Typ'Class;

   --  final
   LITTLE_ENDIAN : access Java.Nio.ByteOrder.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NativeOrder, "nativeOrder");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, BIG_ENDIAN, "BIG_ENDIAN");
   pragma Import (Java, LITTLE_ENDIAN, "LITTLE_ENDIAN");

end Java.Nio.ByteOrder;
pragma Import (Java, Java.Nio.ByteOrder, "java.nio.ByteOrder");
pragma Extensions_Allowed (Off);
