pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Nio.Channels.ClosedChannelException;

package Java.Nio.Channels.AsynchronousCloseException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Nio.Channels.ClosedChannelException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AsynchronousCloseException (This : Ref := null)
                                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.nio.channels.AsynchronousCloseException");
   pragma Java_Constructor (New_AsynchronousCloseException);

end Java.Nio.Channels.AsynchronousCloseException;
pragma Import (Java, Java.Nio.Channels.AsynchronousCloseException, "java.nio.channels.AsynchronousCloseException");
pragma Extensions_Allowed (Off);
