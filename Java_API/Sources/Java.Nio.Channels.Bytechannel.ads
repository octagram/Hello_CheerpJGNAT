pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Nio.Channels.ReadableByteChannel;
with Java.Nio.Channels.WritableByteChannel;

package Java.Nio.Channels.ByteChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ReadableByteChannel_I : Java.Nio.Channels.ReadableByteChannel.Ref;
            WritableByteChannel_I : Java.Nio.Channels.WritableByteChannel.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Java.Nio.Channels.ByteChannel;
pragma Import (Java, Java.Nio.Channels.ByteChannel, "java.nio.channels.ByteChannel");
pragma Extensions_Allowed (Off);
