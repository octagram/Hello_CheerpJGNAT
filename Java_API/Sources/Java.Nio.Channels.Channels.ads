pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Java.Nio.Channels.ReadableByteChannel;
limited with Java.Nio.Channels.WritableByteChannel;
limited with Java.Nio.Charset.CharsetDecoder;
limited with Java.Nio.Charset.CharsetEncoder;
with Java.Lang.Object;

package Java.Nio.Channels.Channels is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInputStream (P1_ReadableByteChannel : access Standard.Java.Nio.Channels.ReadableByteChannel.Typ'Class)
                            return access Java.Io.InputStream.Typ'Class;

   function NewOutputStream (P1_WritableByteChannel : access Standard.Java.Nio.Channels.WritableByteChannel.Typ'Class)
                             return access Java.Io.OutputStream.Typ'Class;

   function NewChannel (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                        return access Java.Nio.Channels.ReadableByteChannel.Typ'Class;

   function NewChannel (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                        return access Java.Nio.Channels.WritableByteChannel.Typ'Class;

   function NewReader (P1_ReadableByteChannel : access Standard.Java.Nio.Channels.ReadableByteChannel.Typ'Class;
                       P2_CharsetDecoder : access Standard.Java.Nio.Charset.CharsetDecoder.Typ'Class;
                       P3_Int : Java.Int)
                       return access Java.Io.Reader.Typ'Class;

   function NewReader (P1_ReadableByteChannel : access Standard.Java.Nio.Channels.ReadableByteChannel.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Io.Reader.Typ'Class;

   function NewWriter (P1_WritableByteChannel : access Standard.Java.Nio.Channels.WritableByteChannel.Typ'Class;
                       P2_CharsetEncoder : access Standard.Java.Nio.Charset.CharsetEncoder.Typ'Class;
                       P3_Int : Java.Int)
                       return access Java.Io.Writer.Typ'Class;

   function NewWriter (P1_WritableByteChannel : access Standard.Java.Nio.Channels.WritableByteChannel.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Io.Writer.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewInputStream, "newInputStream");
   pragma Import (Java, NewOutputStream, "newOutputStream");
   pragma Import (Java, NewChannel, "newChannel");
   pragma Import (Java, NewReader, "newReader");
   pragma Import (Java, NewWriter, "newWriter");

end Java.Nio.Channels.Channels;
pragma Import (Java, Java.Nio.Channels.Channels, "java.nio.channels.Channels");
pragma Extensions_Allowed (Off);
