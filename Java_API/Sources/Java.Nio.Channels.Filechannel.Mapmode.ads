pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Nio.Channels.FileChannel.MapMode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   READ_ONLY : access Java.Nio.Channels.FileChannel.MapMode.Typ'Class;

   --  final
   READ_WRITE : access Java.Nio.Channels.FileChannel.MapMode.Typ'Class;

   --  final
   PRIVATE_K : access Java.Nio.Channels.FileChannel.MapMode.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, READ_ONLY, "READ_ONLY");
   pragma Import (Java, READ_WRITE, "READ_WRITE");
   pragma Import (Java, PRIVATE_K, "PRIVATE");

end Java.Nio.Channels.FileChannel.MapMode;
pragma Import (Java, Java.Nio.Channels.FileChannel.MapMode, "java.nio.channels.FileChannel$MapMode");
pragma Extensions_Allowed (Off);
