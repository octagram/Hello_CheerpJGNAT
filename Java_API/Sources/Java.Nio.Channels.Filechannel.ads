pragma Extensions_Allowed (On);
limited with Java.Nio.ByteBuffer;
limited with Java.Nio.Channels.FileChannel.MapMode;
limited with Java.Nio.Channels.FileLock;
limited with Java.Nio.Channels.ReadableByteChannel;
limited with Java.Nio.Channels.WritableByteChannel;
limited with Java.Nio.MappedByteBuffer;
with Java.Lang.Object;
with Java.Nio.Channels.ByteChannel;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.GatheringByteChannel;
with Java.Nio.Channels.InterruptibleChannel;
with Java.Nio.Channels.ScatteringByteChannel;
with Java.Nio.Channels.Spi.AbstractInterruptibleChannel;

package Java.Nio.Channels.FileChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ByteChannel_I : Java.Nio.Channels.ByteChannel.Ref;
            Channel_I : Java.Nio.Channels.Channel.Ref;
            GatheringByteChannel_I : Java.Nio.Channels.GatheringByteChannel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref;
            ScatteringByteChannel_I : Java.Nio.Channels.ScatteringByteChannel.Ref)
    is abstract new Java.Nio.Channels.Spi.AbstractInterruptibleChannel.Typ(Channel_I,
                                                                           InterruptibleChannel_I)
      with null record;

   --  protected
   function New_FileChannel (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ;
                  P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function Read (This : access Typ;
                  P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int)
                   return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function Write (This : access Typ;
                   P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj)
                   return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Position (This : access Typ)
                      return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Position (This : access Typ;
                      P1_Long : Java.Long)
                      return access Java.Nio.Channels.FileChannel.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function Size (This : access Typ)
                  return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Truncate (This : access Typ;
                      P1_Long : Java.Long)
                      return access Java.Nio.Channels.FileChannel.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Force (This : access Typ;
                    P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Io.IOException.Except

   function TransferTo (This : access Typ;
                        P1_Long : Java.Long;
                        P2_Long : Java.Long;
                        P3_WritableByteChannel : access Standard.Java.Nio.Channels.WritableByteChannel.Typ'Class)
                        return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function TransferFrom (This : access Typ;
                          P1_ReadableByteChannel : access Standard.Java.Nio.Channels.ReadableByteChannel.Typ'Class;
                          P2_Long : Java.Long;
                          P3_Long : Java.Long)
                          return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class;
                  P2_Long : Java.Long)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class;
                   P2_Long : Java.Long)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Map (This : access Typ;
                 P1_MapMode : access Standard.Java.Nio.Channels.FileChannel.MapMode.Typ'Class;
                 P2_Long : Java.Long;
                 P3_Long : Java.Long)
                 return access Java.Nio.MappedByteBuffer.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function Lock (This : access Typ;
                  P1_Long : Java.Long;
                  P2_Long : Java.Long;
                  P3_Boolean : Java.Boolean)
                  return access Java.Nio.Channels.FileLock.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function Lock (This : access Typ)
                  return access Java.Nio.Channels.FileLock.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function TryLock (This : access Typ;
                     P1_Long : Java.Long;
                     P2_Long : Java.Long;
                     P3_Boolean : Java.Boolean)
                     return access Java.Nio.Channels.FileLock.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function TryLock (This : access Typ)
                     return access Java.Nio.Channels.FileLock.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileChannel);
   pragma Export (Java, Read, "read");
   pragma Export (Java, Write, "write");
   pragma Export (Java, Position, "position");
   pragma Export (Java, Size, "size");
   pragma Export (Java, Truncate, "truncate");
   pragma Export (Java, Force, "force");
   pragma Export (Java, TransferTo, "transferTo");
   pragma Export (Java, TransferFrom, "transferFrom");
   pragma Export (Java, Map, "map");
   pragma Export (Java, Lock, "lock");
   pragma Export (Java, TryLock, "tryLock");

end Java.Nio.Channels.FileChannel;
pragma Import (Java, Java.Nio.Channels.FileChannel, "java.nio.channels.FileChannel");
pragma Extensions_Allowed (Off);
