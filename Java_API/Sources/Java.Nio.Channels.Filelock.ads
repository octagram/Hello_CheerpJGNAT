pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.Channels.FileChannel;
with Java.Lang.Object;

package Java.Nio.Channels.FileLock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_FileLock (P1_FileChannel : access Standard.Java.Nio.Channels.FileChannel.Typ'Class;
                          P2_Long : Java.Long;
                          P3_Long : Java.Long;
                          P4_Boolean : Java.Boolean; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Channel (This : access Typ)
                     return access Java.Nio.Channels.FileChannel.Typ'Class;

   --  final
   function Position (This : access Typ)
                      return Java.Long;

   --  final
   function Size (This : access Typ)
                  return Java.Long;

   --  final
   function IsShared (This : access Typ)
                      return Java.Boolean;

   --  final
   function Overlaps (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Long : Java.Long)
                      return Java.Boolean;

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;

   procedure Release (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileLock);
   pragma Import (Java, Channel, "channel");
   pragma Import (Java, Position, "position");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsShared, "isShared");
   pragma Import (Java, Overlaps, "overlaps");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, Release, "release");
   pragma Import (Java, ToString, "toString");

end Java.Nio.Channels.FileLock;
pragma Import (Java, Java.Nio.Channels.FileLock, "java.nio.channels.FileLock");
pragma Extensions_Allowed (Off);
