pragma Extensions_Allowed (On);
limited with Java.Nio.ByteBuffer;
with Java.Lang.Object;
with Java.Nio.Channels.WritableByteChannel;

package Java.Nio.Channels.GatheringByteChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            WritableByteChannel_I : Java.Nio.Channels.WritableByteChannel.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Write (This : access Typ;
                   P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int)
                   return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj)
                   return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Write, "write");

end Java.Nio.Channels.GatheringByteChannel;
pragma Import (Java, Java.Nio.Channels.GatheringByteChannel, "java.nio.channels.GatheringByteChannel");
pragma Extensions_Allowed (Off);
