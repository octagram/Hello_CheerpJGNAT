pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Nio.Channels.Channel;

package Java.Nio.Channels.InterruptibleChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Channel_I : Java.Nio.Channels.Channel.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Close, "close");

end Java.Nio.Channels.InterruptibleChannel;
pragma Import (Java, Java.Nio.Channels.InterruptibleChannel, "java.nio.channels.InterruptibleChannel");
pragma Extensions_Allowed (Off);
