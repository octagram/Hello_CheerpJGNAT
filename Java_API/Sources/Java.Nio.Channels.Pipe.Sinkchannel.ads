pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.Spi.SelectorProvider;
with Java.Lang.Object;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.GatheringByteChannel;
with Java.Nio.Channels.InterruptibleChannel;
with Java.Nio.Channels.Spi.AbstractSelectableChannel;
with Java.Nio.Channels.WritableByteChannel;

package Java.Nio.Channels.Pipe.SinkChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Channel_I : Java.Nio.Channels.Channel.Ref;
            GatheringByteChannel_I : Java.Nio.Channels.GatheringByteChannel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref;
            WritableByteChannel_I : Java.Nio.Channels.WritableByteChannel.Ref)
    is abstract new Java.Nio.Channels.Spi.AbstractSelectableChannel.Typ(Channel_I,
                                                                        InterruptibleChannel_I)
      with null record;

   --  protected
   function New_SinkChannel (P1_SelectorProvider : access Standard.Java.Nio.Channels.Spi.SelectorProvider.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function ValidOps (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SinkChannel);
   pragma Import (Java, ValidOps, "validOps");

end Java.Nio.Channels.Pipe.SinkChannel;
pragma Import (Java, Java.Nio.Channels.Pipe.SinkChannel, "java.nio.channels.Pipe$SinkChannel");
pragma Extensions_Allowed (Off);
