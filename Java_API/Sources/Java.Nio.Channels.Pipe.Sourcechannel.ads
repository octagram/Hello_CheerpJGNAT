pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.Spi.SelectorProvider;
with Java.Lang.Object;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.InterruptibleChannel;
with Java.Nio.Channels.ReadableByteChannel;
with Java.Nio.Channels.ScatteringByteChannel;
with Java.Nio.Channels.Spi.AbstractSelectableChannel;

package Java.Nio.Channels.Pipe.SourceChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Channel_I : Java.Nio.Channels.Channel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref;
            ReadableByteChannel_I : Java.Nio.Channels.ReadableByteChannel.Ref;
            ScatteringByteChannel_I : Java.Nio.Channels.ScatteringByteChannel.Ref)
    is abstract new Java.Nio.Channels.Spi.AbstractSelectableChannel.Typ(Channel_I,
                                                                        InterruptibleChannel_I)
      with null record;

   --  protected
   function New_SourceChannel (P1_SelectorProvider : access Standard.Java.Nio.Channels.Spi.SelectorProvider.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function ValidOps (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SourceChannel);
   pragma Import (Java, ValidOps, "validOps");

end Java.Nio.Channels.Pipe.SourceChannel;
pragma Import (Java, Java.Nio.Channels.Pipe.SourceChannel, "java.nio.channels.Pipe$SourceChannel");
pragma Extensions_Allowed (Off);
