pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.Pipe.SinkChannel;
limited with Java.Nio.Channels.Pipe.SourceChannel;
with Java.Lang.Object;

package Java.Nio.Channels.Pipe is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Pipe (This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Source (This : access Typ)
                    return access Java.Nio.Channels.Pipe.SourceChannel.Typ'Class is abstract;

   function Sink (This : access Typ)
                  return access Java.Nio.Channels.Pipe.SinkChannel.Typ'Class is abstract;

   function Open return access Java.Nio.Channels.Pipe.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Pipe);
   pragma Export (Java, Source, "source");
   pragma Export (Java, Sink, "sink");
   pragma Export (Java, Open, "open");

end Java.Nio.Channels.Pipe;
pragma Import (Java, Java.Nio.Channels.Pipe, "java.nio.channels.Pipe");
pragma Extensions_Allowed (Off);
