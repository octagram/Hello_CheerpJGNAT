pragma Extensions_Allowed (On);
limited with Java.Nio.ByteBuffer;
with Java.Lang.Object;
with Java.Nio.Channels.ReadableByteChannel;

package Java.Nio.Channels.ScatteringByteChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ReadableByteChannel_I : Java.Nio.Channels.ReadableByteChannel.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ;
                  P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj)
                  return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Read, "read");

end Java.Nio.Channels.ScatteringByteChannel;
pragma Import (Java, Java.Nio.Channels.ScatteringByteChannel, "java.nio.channels.ScatteringByteChannel");
pragma Extensions_Allowed (Off);
