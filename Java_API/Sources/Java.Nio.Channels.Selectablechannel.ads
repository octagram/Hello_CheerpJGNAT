pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.SelectionKey;
limited with Java.Nio.Channels.Selector;
limited with Java.Nio.Channels.Spi.SelectorProvider;
with Java.Lang.Object;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.InterruptibleChannel;
with Java.Nio.Channels.Spi.AbstractInterruptibleChannel;

package Java.Nio.Channels.SelectableChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Channel_I : Java.Nio.Channels.Channel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref)
    is abstract new Java.Nio.Channels.Spi.AbstractInterruptibleChannel.Typ(Channel_I,
                                                                           InterruptibleChannel_I)
      with null record;

   --  protected
   function New_SelectableChannel (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Provider (This : access Typ)
                      return access Java.Nio.Channels.Spi.SelectorProvider.Typ'Class is abstract;

   function ValidOps (This : access Typ)
                      return Java.Int is abstract;

   function IsRegistered (This : access Typ)
                          return Java.Boolean is abstract;

   function KeyFor (This : access Typ;
                    P1_Selector : access Standard.Java.Nio.Channels.Selector.Typ'Class)
                    return access Java.Nio.Channels.SelectionKey.Typ'Class is abstract;

   function Register (This : access Typ;
                      P1_Selector : access Standard.Java.Nio.Channels.Selector.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Nio.Channels.SelectionKey.Typ'Class is abstract;
   --  can raise Java.Nio.Channels.ClosedChannelException.Except

   --  final
   function Register (This : access Typ;
                      P1_Selector : access Standard.Java.Nio.Channels.Selector.Typ'Class;
                      P2_Int : Java.Int)
                      return access Java.Nio.Channels.SelectionKey.Typ'Class;
   --  can raise Java.Nio.Channels.ClosedChannelException.Except

   function ConfigureBlocking (This : access Typ;
                               P1_Boolean : Java.Boolean)
                               return access Java.Nio.Channels.SelectableChannel.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function IsBlocking (This : access Typ)
                        return Java.Boolean is abstract;

   function BlockingLock (This : access Typ)
                          return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SelectableChannel);
   pragma Export (Java, Provider, "provider");
   pragma Export (Java, ValidOps, "validOps");
   pragma Export (Java, IsRegistered, "isRegistered");
   pragma Export (Java, KeyFor, "keyFor");
   pragma Export (Java, Register, "register");
   pragma Export (Java, ConfigureBlocking, "configureBlocking");
   pragma Export (Java, IsBlocking, "isBlocking");
   pragma Export (Java, BlockingLock, "blockingLock");

end Java.Nio.Channels.SelectableChannel;
pragma Import (Java, Java.Nio.Channels.SelectableChannel, "java.nio.channels.SelectableChannel");
pragma Extensions_Allowed (Off);
