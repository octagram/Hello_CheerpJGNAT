pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.SelectableChannel;
limited with Java.Nio.Channels.Selector;
with Java.Lang.Object;

package Java.Nio.Channels.SelectionKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SelectionKey (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Channel (This : access Typ)
                     return access Java.Nio.Channels.SelectableChannel.Typ'Class is abstract;

   function Selector (This : access Typ)
                      return access Java.Nio.Channels.Selector.Typ'Class is abstract;

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;

   procedure Cancel (This : access Typ) is abstract;

   function InterestOps (This : access Typ)
                         return Java.Int is abstract;

   function InterestOps (This : access Typ;
                         P1_Int : Java.Int)
                         return access Java.Nio.Channels.SelectionKey.Typ'Class is abstract;

   function ReadyOps (This : access Typ)
                      return Java.Int is abstract;

   --  final
   function IsReadable (This : access Typ)
                        return Java.Boolean;

   --  final
   function IsWritable (This : access Typ)
                        return Java.Boolean;

   --  final
   function IsConnectable (This : access Typ)
                           return Java.Boolean;

   --  final
   function IsAcceptable (This : access Typ)
                          return Java.Boolean;

   --  final
   function Attach (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   --  final
   function Attachment (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OP_READ : constant Java.Int;

   --  final
   OP_WRITE : constant Java.Int;

   --  final
   OP_CONNECT : constant Java.Int;

   --  final
   OP_ACCEPT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SelectionKey);
   pragma Export (Java, Channel, "channel");
   pragma Export (Java, Selector, "selector");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, Cancel, "cancel");
   pragma Export (Java, InterestOps, "interestOps");
   pragma Export (Java, ReadyOps, "readyOps");
   pragma Export (Java, IsReadable, "isReadable");
   pragma Export (Java, IsWritable, "isWritable");
   pragma Export (Java, IsConnectable, "isConnectable");
   pragma Export (Java, IsAcceptable, "isAcceptable");
   pragma Export (Java, Attach, "attach");
   pragma Export (Java, Attachment, "attachment");
   pragma Import (Java, OP_READ, "OP_READ");
   pragma Import (Java, OP_WRITE, "OP_WRITE");
   pragma Import (Java, OP_CONNECT, "OP_CONNECT");
   pragma Import (Java, OP_ACCEPT, "OP_ACCEPT");

end Java.Nio.Channels.SelectionKey;
pragma Import (Java, Java.Nio.Channels.SelectionKey, "java.nio.channels.SelectionKey");
pragma Extensions_Allowed (Off);
