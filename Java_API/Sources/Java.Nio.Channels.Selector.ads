pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.Spi.SelectorProvider;
limited with Java.Util.Set;
with Java.Lang.Object;

package Java.Nio.Channels.Selector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Selector (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Open return access Java.Nio.Channels.Selector.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function IsOpen (This : access Typ)
                    return Java.Boolean is abstract;

   function Provider (This : access Typ)
                      return access Java.Nio.Channels.Spi.SelectorProvider.Typ'Class is abstract;

   function Keys (This : access Typ)
                  return access Java.Util.Set.Typ'Class is abstract;

   function SelectedKeys (This : access Typ)
                          return access Java.Util.Set.Typ'Class is abstract;

   function SelectNow (This : access Typ)
                       return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function select_K (This : access Typ;
                      P1_Long : Java.Long)
                      return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function select_K (This : access Typ)
                      return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Wakeup (This : access Typ)
                    return access Java.Nio.Channels.Selector.Typ'Class is abstract;

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Selector);
   pragma Export (Java, Open, "open");
   pragma Export (Java, IsOpen, "isOpen");
   pragma Export (Java, Provider, "provider");
   pragma Export (Java, Keys, "keys");
   pragma Export (Java, SelectedKeys, "selectedKeys");
   pragma Export (Java, SelectNow, "selectNow");
   pragma Export (Java, select_K, "select");
   pragma Export (Java, Wakeup, "wakeup");
   pragma Export (Java, Close, "close");

end Java.Nio.Channels.Selector;
pragma Import (Java, Java.Nio.Channels.Selector, "java.nio.channels.Selector");
pragma Extensions_Allowed (Off);
