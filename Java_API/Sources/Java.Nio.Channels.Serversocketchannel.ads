pragma Extensions_Allowed (On);
limited with Java.Net.ServerSocket;
limited with Java.Nio.Channels.SocketChannel;
limited with Java.Nio.Channels.Spi.SelectorProvider;
with Java.Lang.Object;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.InterruptibleChannel;
with Java.Nio.Channels.Spi.AbstractSelectableChannel;

package Java.Nio.Channels.ServerSocketChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Channel_I : Java.Nio.Channels.Channel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref)
    is abstract new Java.Nio.Channels.Spi.AbstractSelectableChannel.Typ(Channel_I,
                                                                        InterruptibleChannel_I)
      with null record;

   --  protected
   function New_ServerSocketChannel (P1_SelectorProvider : access Standard.Java.Nio.Channels.Spi.SelectorProvider.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Open return access Java.Nio.Channels.ServerSocketChannel.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function ValidOps (This : access Typ)
                      return Java.Int;

   function Socket (This : access Typ)
                    return access Java.Net.ServerSocket.Typ'Class is abstract;

   function accept_K (This : access Typ)
                      return access Java.Nio.Channels.SocketChannel.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServerSocketChannel);
   pragma Export (Java, Open, "open");
   pragma Export (Java, ValidOps, "validOps");
   pragma Export (Java, Socket, "socket");
   pragma Export (Java, accept_K, "accept");

end Java.Nio.Channels.ServerSocketChannel;
pragma Import (Java, Java.Nio.Channels.ServerSocketChannel, "java.nio.channels.ServerSocketChannel");
pragma Extensions_Allowed (Off);
