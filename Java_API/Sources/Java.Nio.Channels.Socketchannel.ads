pragma Extensions_Allowed (On);
limited with Java.Net.Socket;
limited with Java.Net.SocketAddress;
limited with Java.Nio.ByteBuffer;
limited with Java.Nio.Channels.Spi.SelectorProvider;
with Java.Lang.Object;
with Java.Nio.Channels.ByteChannel;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.GatheringByteChannel;
with Java.Nio.Channels.InterruptibleChannel;
with Java.Nio.Channels.ScatteringByteChannel;
with Java.Nio.Channels.Spi.AbstractSelectableChannel;

package Java.Nio.Channels.SocketChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ByteChannel_I : Java.Nio.Channels.ByteChannel.Ref;
            Channel_I : Java.Nio.Channels.Channel.Ref;
            GatheringByteChannel_I : Java.Nio.Channels.GatheringByteChannel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref;
            ScatteringByteChannel_I : Java.Nio.Channels.ScatteringByteChannel.Ref)
    is abstract new Java.Nio.Channels.Spi.AbstractSelectableChannel.Typ(Channel_I,
                                                                        InterruptibleChannel_I)
      with null record;

   --  protected
   function New_SocketChannel (P1_SelectorProvider : access Standard.Java.Nio.Channels.Spi.SelectorProvider.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Open return access Java.Nio.Channels.SocketChannel.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Open (P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class)
                  return access Java.Nio.Channels.SocketChannel.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  final
   function ValidOps (This : access Typ)
                      return Java.Int;

   function Socket (This : access Typ)
                    return access Java.Net.Socket.Typ'Class is abstract;

   function IsConnected (This : access Typ)
                         return Java.Boolean is abstract;

   function IsConnectionPending (This : access Typ)
                                 return Java.Boolean is abstract;

   function Connect (This : access Typ;
                     P1_SocketAddress : access Standard.Java.Net.SocketAddress.Typ'Class)
                     return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function FinishConnect (This : access Typ)
                           return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function Read (This : access Typ;
                  P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int)
                   return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function Write (This : access Typ;
                   P1_ByteBuffer_Arr : access Java.Nio.ByteBuffer.Arr_Obj)
                   return Java.Long;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SocketChannel);
   pragma Export (Java, Open, "open");
   pragma Export (Java, ValidOps, "validOps");
   pragma Export (Java, Socket, "socket");
   pragma Export (Java, IsConnected, "isConnected");
   pragma Export (Java, IsConnectionPending, "isConnectionPending");
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, FinishConnect, "finishConnect");
   pragma Export (Java, Read, "read");
   pragma Export (Java, Write, "write");

end Java.Nio.Channels.SocketChannel;
pragma Import (Java, Java.Nio.Channels.SocketChannel, "java.nio.channels.SocketChannel");
pragma Extensions_Allowed (Off);
