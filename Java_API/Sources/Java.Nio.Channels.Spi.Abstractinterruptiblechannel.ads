pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.InterruptibleChannel;

package Java.Nio.Channels.Spi.AbstractInterruptibleChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Channel_I : Java.Nio.Channels.Channel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_AbstractInterruptibleChannel (This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure ImplCloseChannel (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function IsOpen (This : access Typ)
                    return Java.Boolean;

   --  final  protected
   procedure begin_K (This : access Typ);

   --  final  protected
   procedure end_K (This : access Typ;
                    P1_Boolean : Java.Boolean);
   --  can raise Java.Nio.Channels.AsynchronousCloseException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractInterruptibleChannel);
   pragma Export (Java, Close, "close");
   pragma Export (Java, ImplCloseChannel, "implCloseChannel");
   pragma Export (Java, IsOpen, "isOpen");
   pragma Export (Java, begin_K, "begin");
   pragma Export (Java, end_K, "end");

end Java.Nio.Channels.Spi.AbstractInterruptibleChannel;
pragma Import (Java, Java.Nio.Channels.Spi.AbstractInterruptibleChannel, "java.nio.channels.spi.AbstractInterruptibleChannel");
pragma Extensions_Allowed (Off);
