pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.SelectionKey;
limited with Java.Nio.Channels.Selector;
limited with Java.Nio.Channels.Spi.SelectorProvider;
with Java.Lang.Object;
with Java.Nio.Channels.Channel;
with Java.Nio.Channels.InterruptibleChannel;
with Java.Nio.Channels.SelectableChannel;

package Java.Nio.Channels.Spi.AbstractSelectableChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Channel_I : Java.Nio.Channels.Channel.Ref;
            InterruptibleChannel_I : Java.Nio.Channels.InterruptibleChannel.Ref)
    is abstract new Java.Nio.Channels.SelectableChannel.Typ(Channel_I,
                                                            InterruptibleChannel_I)
      with null record;

   --  protected
   function New_AbstractSelectableChannel (P1_SelectorProvider : access Standard.Java.Nio.Channels.Spi.SelectorProvider.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Provider (This : access Typ)
                      return access Java.Nio.Channels.Spi.SelectorProvider.Typ'Class;

   --  final
   function IsRegistered (This : access Typ)
                          return Java.Boolean;

   --  final
   function KeyFor (This : access Typ;
                    P1_Selector : access Standard.Java.Nio.Channels.Selector.Typ'Class)
                    return access Java.Nio.Channels.SelectionKey.Typ'Class;

   --  final
   function Register (This : access Typ;
                      P1_Selector : access Standard.Java.Nio.Channels.Selector.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Nio.Channels.SelectionKey.Typ'Class;
   --  can raise Java.Nio.Channels.ClosedChannelException.Except

   --  final  protected
   procedure ImplCloseChannel (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure ImplCloseSelectableChannel (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function IsBlocking (This : access Typ)
                        return Java.Boolean;

   --  final
   function BlockingLock (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   --  final
   function ConfigureBlocking (This : access Typ;
                               P1_Boolean : Java.Boolean)
                               return access Java.Nio.Channels.SelectableChannel.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure ImplConfigureBlocking (This : access Typ;
                                    P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractSelectableChannel);
   pragma Export (Java, Provider, "provider");
   pragma Export (Java, IsRegistered, "isRegistered");
   pragma Export (Java, KeyFor, "keyFor");
   pragma Export (Java, Register, "register");
   pragma Export (Java, ImplCloseChannel, "implCloseChannel");
   pragma Export (Java, ImplCloseSelectableChannel, "implCloseSelectableChannel");
   pragma Export (Java, IsBlocking, "isBlocking");
   pragma Export (Java, BlockingLock, "blockingLock");
   pragma Export (Java, ConfigureBlocking, "configureBlocking");
   pragma Export (Java, ImplConfigureBlocking, "implConfigureBlocking");

end Java.Nio.Channels.Spi.AbstractSelectableChannel;
pragma Import (Java, Java.Nio.Channels.Spi.AbstractSelectableChannel, "java.nio.channels.spi.AbstractSelectableChannel");
pragma Extensions_Allowed (Off);
