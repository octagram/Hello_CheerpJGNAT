pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Nio.Channels.SelectionKey;

package Java.Nio.Channels.Spi.AbstractSelectionKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Nio.Channels.SelectionKey.Typ
      with null record;

   --  protected
   function New_AbstractSelectionKey (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function IsValid (This : access Typ)
                     return Java.Boolean;

   --  final
   procedure Cancel (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractSelectionKey);
   pragma Import (Java, IsValid, "isValid");
   pragma Import (Java, Cancel, "cancel");

end Java.Nio.Channels.Spi.AbstractSelectionKey;
pragma Import (Java, Java.Nio.Channels.Spi.AbstractSelectionKey, "java.nio.channels.spi.AbstractSelectionKey");
pragma Extensions_Allowed (Off);
