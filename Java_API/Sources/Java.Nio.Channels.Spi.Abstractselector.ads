pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.SelectionKey;
limited with Java.Nio.Channels.Spi.AbstractSelectableChannel;
limited with Java.Nio.Channels.Spi.AbstractSelectionKey;
limited with Java.Nio.Channels.Spi.SelectorProvider;
limited with Java.Util.Set;
with Java.Lang.Object;
with Java.Nio.Channels.Selector;

package Java.Nio.Channels.Spi.AbstractSelector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Nio.Channels.Selector.Typ
      with null record;

   --  protected
   function New_AbstractSelector (P1_SelectorProvider : access Standard.Java.Nio.Channels.Spi.SelectorProvider.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure ImplCloseSelector (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   --  final
   function IsOpen (This : access Typ)
                    return Java.Boolean;

   --  final
   function Provider (This : access Typ)
                      return access Java.Nio.Channels.Spi.SelectorProvider.Typ'Class;

   --  final  protected
   function CancelledKeys (This : access Typ)
                           return access Java.Util.Set.Typ'Class;

   --  protected
   function Register (This : access Typ;
                      P1_AbstractSelectableChannel : access Standard.Java.Nio.Channels.Spi.AbstractSelectableChannel.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Nio.Channels.SelectionKey.Typ'Class is abstract;

   --  final  protected
   procedure Deregister (This : access Typ;
                         P1_AbstractSelectionKey : access Standard.Java.Nio.Channels.Spi.AbstractSelectionKey.Typ'Class);

   --  final  protected
   procedure begin_K (This : access Typ);

   --  final  protected
   procedure end_K (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractSelector);
   pragma Export (Java, Close, "close");
   pragma Export (Java, ImplCloseSelector, "implCloseSelector");
   pragma Export (Java, IsOpen, "isOpen");
   pragma Export (Java, Provider, "provider");
   pragma Export (Java, CancelledKeys, "cancelledKeys");
   pragma Export (Java, Register, "register");
   pragma Export (Java, Deregister, "deregister");
   pragma Export (Java, begin_K, "begin");
   pragma Export (Java, end_K, "end");

end Java.Nio.Channels.Spi.AbstractSelector;
pragma Import (Java, Java.Nio.Channels.Spi.AbstractSelector, "java.nio.channels.spi.AbstractSelector");
pragma Extensions_Allowed (Off);
