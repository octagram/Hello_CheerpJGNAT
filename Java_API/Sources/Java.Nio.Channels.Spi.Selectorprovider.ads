pragma Extensions_Allowed (On);
limited with Java.Nio.Channels.Channel;
limited with Java.Nio.Channels.DatagramChannel;
limited with Java.Nio.Channels.Pipe;
limited with Java.Nio.Channels.ServerSocketChannel;
limited with Java.Nio.Channels.SocketChannel;
limited with Java.Nio.Channels.Spi.AbstractSelector;
with Java.Lang.Object;

package Java.Nio.Channels.Spi.SelectorProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SelectorProvider (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Provider return access Java.Nio.Channels.Spi.SelectorProvider.Typ'Class;

   function OpenDatagramChannel (This : access Typ)
                                 return access Java.Nio.Channels.DatagramChannel.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function OpenPipe (This : access Typ)
                      return access Java.Nio.Channels.Pipe.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function OpenSelector (This : access Typ)
                          return access Java.Nio.Channels.Spi.AbstractSelector.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function OpenServerSocketChannel (This : access Typ)
                                     return access Java.Nio.Channels.ServerSocketChannel.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function OpenSocketChannel (This : access Typ)
                               return access Java.Nio.Channels.SocketChannel.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function InheritedChannel (This : access Typ)
                              return access Java.Nio.Channels.Channel.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SelectorProvider);
   pragma Export (Java, Provider, "provider");
   pragma Export (Java, OpenDatagramChannel, "openDatagramChannel");
   pragma Export (Java, OpenPipe, "openPipe");
   pragma Export (Java, OpenSelector, "openSelector");
   pragma Export (Java, OpenServerSocketChannel, "openServerSocketChannel");
   pragma Export (Java, OpenSocketChannel, "openSocketChannel");
   pragma Export (Java, InheritedChannel, "inheritedChannel");

end Java.Nio.Channels.Spi.SelectorProvider;
pragma Import (Java, Java.Nio.Channels.Spi.SelectorProvider, "java.nio.channels.spi.SelectorProvider");
pragma Extensions_Allowed (Off);
