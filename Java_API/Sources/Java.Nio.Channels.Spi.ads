pragma Extensions_Allowed (On);
package Java.Nio.Channels.Spi is
   pragma Preelaborate;
end Java.Nio.Channels.Spi;
pragma Import (Java, Java.Nio.Channels.Spi, "java.nio.channels.spi");
pragma Extensions_Allowed (Off);
