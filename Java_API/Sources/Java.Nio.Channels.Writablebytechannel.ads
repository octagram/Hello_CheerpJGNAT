pragma Extensions_Allowed (On);
limited with Java.Nio.ByteBuffer;
with Java.Lang.Object;
with Java.Nio.Channels.Channel;

package Java.Nio.Channels.WritableByteChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Channel_I : Java.Nio.Channels.Channel.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Write (This : access Typ;
                   P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Write, "write");

end Java.Nio.Channels.WritableByteChannel;
pragma Import (Java, Java.Nio.Channels.WritableByteChannel, "java.nio.channels.WritableByteChannel");
pragma Extensions_Allowed (Off);
