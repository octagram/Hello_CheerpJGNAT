pragma Extensions_Allowed (On);
package Java.Nio.Channels is
   pragma Preelaborate;
end Java.Nio.Channels;
pragma Import (Java, Java.Nio.Channels, "java.nio.channels");
pragma Extensions_Allowed (Off);
