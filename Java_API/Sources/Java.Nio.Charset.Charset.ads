pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteBuffer;
limited with Java.Nio.CharBuffer;
limited with Java.Nio.Charset.CharsetDecoder;
limited with Java.Nio.Charset.CharsetEncoder;
limited with Java.Util.Locale;
limited with Java.Util.Set;
limited with Java.Util.SortedMap;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Nio.Charset.Charset is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSupported (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   function ForName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Nio.Charset.Charset.Typ'Class;

   function AvailableCharsets return access Java.Util.SortedMap.Typ'Class;

   function DefaultCharset return access Java.Nio.Charset.Charset.Typ'Class;

   --  protected
   function New_Charset (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String_Arr : access Java.Lang.String.Arr_Obj; 
                         This : Ref := null)
                         return Ref;

   --  final
   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class;

   --  final
   function Aliases (This : access Typ)
                     return access Java.Util.Set.Typ'Class;

   function DisplayName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  final
   function IsRegistered (This : access Typ)
                          return Java.Boolean;

   function DisplayName (This : access Typ;
                         P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function Contains (This : access Typ;
                      P1_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class)
                      return Java.Boolean is abstract;

   function NewDecoder (This : access Typ)
                        return access Java.Nio.Charset.CharsetDecoder.Typ'Class is abstract;

   function NewEncoder (This : access Typ)
                        return access Java.Nio.Charset.CharsetEncoder.Typ'Class is abstract;

   function CanEncode (This : access Typ)
                       return Java.Boolean;

   --  final
   function Decode (This : access Typ;
                    P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                    return access Java.Nio.CharBuffer.Typ'Class;

   --  final
   function Encode (This : access Typ;
                    P1_CharBuffer : access Standard.Java.Nio.CharBuffer.Typ'Class)
                    return access Java.Nio.ByteBuffer.Typ'Class;

   --  final
   function Encode (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Nio.ByteBuffer.Typ'Class;

   --  final
   function CompareTo (This : access Typ;
                       P1_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class)
                       return Java.Int;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsSupported, "isSupported");
   pragma Export (Java, ForName, "forName");
   pragma Export (Java, AvailableCharsets, "availableCharsets");
   pragma Export (Java, DefaultCharset, "defaultCharset");
   pragma Java_Constructor (New_Charset);
   pragma Export (Java, Name, "name");
   pragma Export (Java, Aliases, "aliases");
   pragma Export (Java, DisplayName, "displayName");
   pragma Export (Java, IsRegistered, "isRegistered");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, NewDecoder, "newDecoder");
   pragma Export (Java, NewEncoder, "newEncoder");
   pragma Export (Java, CanEncode, "canEncode");
   pragma Export (Java, Decode, "decode");
   pragma Export (Java, Encode, "encode");
   pragma Export (Java, CompareTo, "compareTo");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, ToString, "toString");

end Java.Nio.Charset.Charset;
pragma Import (Java, Java.Nio.Charset.Charset, "java.nio.charset.Charset");
pragma Extensions_Allowed (Off);
