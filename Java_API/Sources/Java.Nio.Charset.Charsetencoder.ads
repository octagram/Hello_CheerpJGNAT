pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Nio.ByteBuffer;
limited with Java.Nio.CharBuffer;
limited with Java.Nio.Charset.Charset;
limited with Java.Nio.Charset.CoderResult;
limited with Java.Nio.Charset.CodingErrorAction;
with Java.Lang.Object;

package Java.Nio.Charset.CharsetEncoder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_CharsetEncoder (P1_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class;
                                P2_Float : Java.Float;
                                P3_Float : Java.Float;
                                P4_Byte_Arr : Java.Byte_Arr; 
                                This : Ref := null)
                                return Ref;

   --  protected
   function New_CharsetEncoder (P1_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class;
                                P2_Float : Java.Float;
                                P3_Float : Java.Float; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Charset (This : access Typ)
                     return access Java.Nio.Charset.Charset.Typ'Class;

   --  final
   function Replacement (This : access Typ)
                         return Java.Byte_Arr;

   --  final
   function ReplaceWith (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr)
                         return access Java.Nio.Charset.CharsetEncoder.Typ'Class;

   --  protected
   procedure ImplReplaceWith (This : access Typ;
                              P1_Byte_Arr : Java.Byte_Arr);

   function IsLegalReplacement (This : access Typ;
                                P1_Byte_Arr : Java.Byte_Arr)
                                return Java.Boolean;

   function MalformedInputAction (This : access Typ)
                                  return access Java.Nio.Charset.CodingErrorAction.Typ'Class;

   --  final
   function OnMalformedInput (This : access Typ;
                              P1_CodingErrorAction : access Standard.Java.Nio.Charset.CodingErrorAction.Typ'Class)
                              return access Java.Nio.Charset.CharsetEncoder.Typ'Class;

   --  protected
   procedure ImplOnMalformedInput (This : access Typ;
                                   P1_CodingErrorAction : access Standard.Java.Nio.Charset.CodingErrorAction.Typ'Class);

   function UnmappableCharacterAction (This : access Typ)
                                       return access Java.Nio.Charset.CodingErrorAction.Typ'Class;

   --  final
   function OnUnmappableCharacter (This : access Typ;
                                   P1_CodingErrorAction : access Standard.Java.Nio.Charset.CodingErrorAction.Typ'Class)
                                   return access Java.Nio.Charset.CharsetEncoder.Typ'Class;

   --  protected
   procedure ImplOnUnmappableCharacter (This : access Typ;
                                        P1_CodingErrorAction : access Standard.Java.Nio.Charset.CodingErrorAction.Typ'Class);

   --  final
   function AverageBytesPerChar (This : access Typ)
                                 return Java.Float;

   --  final
   function MaxBytesPerChar (This : access Typ)
                             return Java.Float;

   --  final
   function Encode (This : access Typ;
                    P1_CharBuffer : access Standard.Java.Nio.CharBuffer.Typ'Class;
                    P2_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class;
                    P3_Boolean : Java.Boolean)
                    return access Java.Nio.Charset.CoderResult.Typ'Class;

   --  final
   function Flush (This : access Typ;
                   P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                   return access Java.Nio.Charset.CoderResult.Typ'Class;

   --  protected
   function ImplFlush (This : access Typ;
                       P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                       return access Java.Nio.Charset.CoderResult.Typ'Class;

   --  final
   function Reset (This : access Typ)
                   return access Java.Nio.Charset.CharsetEncoder.Typ'Class;

   --  protected
   procedure ImplReset (This : access Typ);

   --  protected
   function EncodeLoop (This : access Typ;
                        P1_CharBuffer : access Standard.Java.Nio.CharBuffer.Typ'Class;
                        P2_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                        return access Java.Nio.Charset.CoderResult.Typ'Class is abstract;

   --  final
   function Encode (This : access Typ;
                    P1_CharBuffer : access Standard.Java.Nio.CharBuffer.Typ'Class)
                    return access Java.Nio.ByteBuffer.Typ'Class;
   --  can raise Java.Nio.Charset.CharacterCodingException.Except

   function CanEncode (This : access Typ;
                       P1_Char : Java.Char)
                       return Java.Boolean;

   function CanEncode (This : access Typ;
                       P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                       return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CharsetEncoder);
   pragma Export (Java, Charset, "charset");
   pragma Export (Java, Replacement, "replacement");
   pragma Export (Java, ReplaceWith, "replaceWith");
   pragma Export (Java, ImplReplaceWith, "implReplaceWith");
   pragma Export (Java, IsLegalReplacement, "isLegalReplacement");
   pragma Export (Java, MalformedInputAction, "malformedInputAction");
   pragma Export (Java, OnMalformedInput, "onMalformedInput");
   pragma Export (Java, ImplOnMalformedInput, "implOnMalformedInput");
   pragma Export (Java, UnmappableCharacterAction, "unmappableCharacterAction");
   pragma Export (Java, OnUnmappableCharacter, "onUnmappableCharacter");
   pragma Export (Java, ImplOnUnmappableCharacter, "implOnUnmappableCharacter");
   pragma Export (Java, AverageBytesPerChar, "averageBytesPerChar");
   pragma Export (Java, MaxBytesPerChar, "maxBytesPerChar");
   pragma Export (Java, Encode, "encode");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, ImplFlush, "implFlush");
   pragma Export (Java, Reset, "reset");
   pragma Export (Java, ImplReset, "implReset");
   pragma Export (Java, EncodeLoop, "encodeLoop");
   pragma Export (Java, CanEncode, "canEncode");

end Java.Nio.Charset.CharsetEncoder;
pragma Import (Java, Java.Nio.Charset.CharsetEncoder, "java.nio.charset.CharsetEncoder");
pragma Extensions_Allowed (Off);
