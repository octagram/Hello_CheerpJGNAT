pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
with Java.Io.Serializable;
with Java.Lang.Error;
with Java.Lang.Object;

package Java.Nio.Charset.CoderMalfunctionError is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Error.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CoderMalfunctionError (P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.nio.charset.CoderMalfunctionError");
   pragma Java_Constructor (New_CoderMalfunctionError);

end Java.Nio.Charset.CoderMalfunctionError;
pragma Import (Java, Java.Nio.Charset.CoderMalfunctionError, "java.nio.charset.CoderMalfunctionError");
pragma Extensions_Allowed (Off);
