pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Nio.Charset.CoderResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IsUnderflow (This : access Typ)
                         return Java.Boolean;

   function IsOverflow (This : access Typ)
                        return Java.Boolean;

   function IsError (This : access Typ)
                     return Java.Boolean;

   function IsMalformed (This : access Typ)
                         return Java.Boolean;

   function IsUnmappable (This : access Typ)
                          return Java.Boolean;

   function Length (This : access Typ)
                    return Java.Int;

   function MalformedForLength (P1_Int : Java.Int)
                                return access Java.Nio.Charset.CoderResult.Typ'Class;

   function UnmappableForLength (P1_Int : Java.Int)
                                 return access Java.Nio.Charset.CoderResult.Typ'Class;

   procedure ThrowException (This : access Typ);
   --  can raise Java.Nio.Charset.CharacterCodingException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNDERFLOW : access Java.Nio.Charset.CoderResult.Typ'Class;

   --  final
   OVERFLOW : access Java.Nio.Charset.CoderResult.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IsUnderflow, "isUnderflow");
   pragma Import (Java, IsOverflow, "isOverflow");
   pragma Import (Java, IsError, "isError");
   pragma Import (Java, IsMalformed, "isMalformed");
   pragma Import (Java, IsUnmappable, "isUnmappable");
   pragma Import (Java, Length, "length");
   pragma Import (Java, MalformedForLength, "malformedForLength");
   pragma Import (Java, UnmappableForLength, "unmappableForLength");
   pragma Import (Java, ThrowException, "throwException");
   pragma Import (Java, UNDERFLOW, "UNDERFLOW");
   pragma Import (Java, OVERFLOW, "OVERFLOW");

end Java.Nio.Charset.CoderResult;
pragma Import (Java, Java.Nio.Charset.CoderResult, "java.nio.charset.CoderResult");
pragma Extensions_Allowed (Off);
