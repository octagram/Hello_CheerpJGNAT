pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Nio.Charset.CodingErrorAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   IGNORE : access Java.Nio.Charset.CodingErrorAction.Typ'Class;

   --  final
   REPLACE : access Java.Nio.Charset.CodingErrorAction.Typ'Class;

   --  final
   REPORT : access Java.Nio.Charset.CodingErrorAction.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IGNORE, "IGNORE");
   pragma Import (Java, REPLACE, "REPLACE");
   pragma Import (Java, REPORT, "REPORT");

end Java.Nio.Charset.CodingErrorAction;
pragma Import (Java, Java.Nio.Charset.CodingErrorAction, "java.nio.charset.CodingErrorAction");
pragma Extensions_Allowed (Off);
