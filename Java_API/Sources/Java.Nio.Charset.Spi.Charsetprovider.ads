pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.Charset.Charset;
limited with Java.Util.Iterator;
with Java.Lang.Object;

package Java.Nio.Charset.Spi.CharsetProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_CharsetProvider (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Charsets (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class is abstract;

   function CharsetForName (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Nio.Charset.Charset.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CharsetProvider);
   pragma Export (Java, Charsets, "charsets");
   pragma Export (Java, CharsetForName, "charsetForName");

end Java.Nio.Charset.Spi.CharsetProvider;
pragma Import (Java, Java.Nio.Charset.Spi.CharsetProvider, "java.nio.charset.spi.CharsetProvider");
pragma Extensions_Allowed (Off);
