pragma Extensions_Allowed (On);
package Java.Nio.Charset.Spi is
   pragma Preelaborate;
end Java.Nio.Charset.Spi;
pragma Import (Java, Java.Nio.Charset.Spi, "java.nio.charset.spi");
pragma Extensions_Allowed (Off);
