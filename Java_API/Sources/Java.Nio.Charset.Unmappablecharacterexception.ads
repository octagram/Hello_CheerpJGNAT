pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Nio.Charset.CharacterCodingException;

package Java.Nio.Charset.UnmappableCharacterException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Nio.Charset.CharacterCodingException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnmappableCharacterException (P1_Int : Java.Int; 
                                              This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInputLength (This : access Typ)
                            return Java.Int;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.nio.charset.UnmappableCharacterException");
   pragma Java_Constructor (New_UnmappableCharacterException);
   pragma Import (Java, GetInputLength, "getInputLength");
   pragma Import (Java, GetMessage, "getMessage");

end Java.Nio.Charset.UnmappableCharacterException;
pragma Import (Java, Java.Nio.Charset.UnmappableCharacterException, "java.nio.charset.UnmappableCharacterException");
pragma Extensions_Allowed (Off);
