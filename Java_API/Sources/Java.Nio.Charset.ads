pragma Extensions_Allowed (On);
package Java.Nio.Charset is
   pragma Preelaborate;
end Java.Nio.Charset;
pragma Import (Java, Java.Nio.Charset, "java.nio.charset");
pragma Extensions_Allowed (Off);
