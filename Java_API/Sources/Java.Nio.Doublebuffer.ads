pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteOrder;
with Java.Lang.Comparable;
with Java.Lang.Object;
with Java.Nio.Buffer;

package Java.Nio.DoubleBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Nio.Buffer.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Allocate (P1_Int : Java.Int)
                      return access Java.Nio.DoubleBuffer.Typ'Class;

   function Wrap (P1_Double_Arr : Java.Double_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return access Java.Nio.DoubleBuffer.Typ'Class;

   function Wrap (P1_Double_Arr : Java.Double_Arr)
                  return access Java.Nio.DoubleBuffer.Typ'Class;

   function Slice (This : access Typ)
                   return access Java.Nio.DoubleBuffer.Typ'Class is abstract;

   function Duplicate (This : access Typ)
                       return access Java.Nio.DoubleBuffer.Typ'Class is abstract;

   function AsReadOnlyBuffer (This : access Typ)
                              return access Java.Nio.DoubleBuffer.Typ'Class is abstract;

   function Get (This : access Typ)
                 return Java.Double is abstract
                 with Export => "get", Convention => Java;

   function Put (This : access Typ;
                 P1_Double : Java.Double)
                 return access Java.Nio.DoubleBuffer.Typ'Class is abstract
                 with Export => "put", Convention => Java;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return Java.Double is abstract
                 with Export => "get", Convention => Java;

   function Put (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Double : Java.Double)
                 return access Java.Nio.DoubleBuffer.Typ'Class is abstract
                 with Export => "put", Convention => Java;

   function Get (This : access Typ;
                 P1_Double_Arr : Java.Double_Arr;
                 P2_Int : Java.Int;
                 P3_Int : Java.Int)
                 return access Java.Nio.DoubleBuffer.Typ'Class
                 with Import => "get", Convention => Java;

   function Get (This : access Typ;
                 P1_Double_Arr : Java.Double_Arr)
                 return access Java.Nio.DoubleBuffer.Typ'Class
                 with Import => "get", Convention => Java;

   function Put (This : access Typ;
                 P1_DoubleBuffer : access Standard.Java.Nio.DoubleBuffer.Typ'Class)
                 return access Java.Nio.DoubleBuffer.Typ'Class
                 with Import => "put", Convention => Java;

   function Put (This : access Typ;
                 P1_Double_Arr : Java.Double_Arr;
                 P2_Int : Java.Int;
                 P3_Int : Java.Int)
                 return access Java.Nio.DoubleBuffer.Typ'Class
                 with Import => "put", Convention => Java;

   --  final
   function Put (This : access Typ;
                 P1_Double_Arr : Java.Double_Arr)
                 return access Java.Nio.DoubleBuffer.Typ'Class
                 with Import => "put", Convention => Java;

   --  final
   function HasArray (This : access Typ)
                      return Java.Boolean;

   --  final
   function array_K (This : access Typ)
                     return Java.Double_Arr;

   --  final
   function ArrayOffset (This : access Typ)
                         return Java.Int;

   function Compact (This : access Typ)
                     return access Java.Nio.DoubleBuffer.Typ'Class is abstract;

   function IsDirect (This : access Typ)
                      return Java.Boolean is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_DoubleBuffer : access Standard.Java.Nio.DoubleBuffer.Typ'Class)
                       return Java.Int;

   function Order (This : access Typ)
                   return access Java.Nio.ByteOrder.Typ'Class is abstract;

   function array_K (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Allocate, "allocate");
   pragma Import (Java, Wrap, "wrap");
   pragma Export (Java, Slice, "slice");
   pragma Export (Java, Duplicate, "duplicate");
   pragma Export (Java, AsReadOnlyBuffer, "asReadOnlyBuffer");
   -- pragma Import (Java, Get, "get");
   -- pragma Import (Java, Put, "put");
   pragma Import (Java, HasArray, "hasArray");
   pragma Import (Java, array_K, "array");
   pragma Import (Java, ArrayOffset, "arrayOffset");
   pragma Export (Java, Compact, "compact");
   pragma Export (Java, IsDirect, "isDirect");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Export (Java, Order, "order");

end Java.Nio.DoubleBuffer;
pragma Import (Java, Java.Nio.DoubleBuffer, "java.nio.DoubleBuffer");
pragma Extensions_Allowed (Off);
