pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteOrder;
with Java.Lang.Comparable;
with Java.Lang.Object;
with Java.Nio.Buffer;

package Java.Nio.FloatBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Nio.Buffer.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Allocate (P1_Int : Java.Int)
                      return access Java.Nio.FloatBuffer.Typ'Class;

   function Wrap (P1_Float_Arr : Java.Float_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return access Java.Nio.FloatBuffer.Typ'Class;

   function Wrap (P1_Float_Arr : Java.Float_Arr)
                  return access Java.Nio.FloatBuffer.Typ'Class;

   function Slice (This : access Typ)
                   return access Java.Nio.FloatBuffer.Typ'Class is abstract;

   function Duplicate (This : access Typ)
                       return access Java.Nio.FloatBuffer.Typ'Class is abstract;

   function AsReadOnlyBuffer (This : access Typ)
                              return access Java.Nio.FloatBuffer.Typ'Class is abstract;

   function Get (This : access Typ)
                 return Java.Float is abstract;

   function Put (This : access Typ;
                 P1_Float : Java.Float)
                 return access Java.Nio.FloatBuffer.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return Java.Float is abstract;

   function Put (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Float : Java.Float)
                 return access Java.Nio.FloatBuffer.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_Float_Arr : Java.Float_Arr;
                 P2_Int : Java.Int;
                 P3_Int : Java.Int)
                 return access Java.Nio.FloatBuffer.Typ'Class;

   function Get (This : access Typ;
                 P1_Float_Arr : Java.Float_Arr)
                 return access Java.Nio.FloatBuffer.Typ'Class;

   function Put (This : access Typ;
                 P1_FloatBuffer : access Standard.Java.Nio.FloatBuffer.Typ'Class)
                 return access Java.Nio.FloatBuffer.Typ'Class;

   function Put (This : access Typ;
                 P1_Float_Arr : Java.Float_Arr;
                 P2_Int : Java.Int;
                 P3_Int : Java.Int)
                 return access Java.Nio.FloatBuffer.Typ'Class;

   --  final
   function Put (This : access Typ;
                 P1_Float_Arr : Java.Float_Arr)
                 return access Java.Nio.FloatBuffer.Typ'Class;

   --  final
   function HasArray (This : access Typ)
                      return Java.Boolean;

   --  final
   function array_K (This : access Typ)
                     return Java.Float_Arr;

   --  final
   function ArrayOffset (This : access Typ)
                         return Java.Int;

   function Compact (This : access Typ)
                     return access Java.Nio.FloatBuffer.Typ'Class is abstract;

   function IsDirect (This : access Typ)
                      return Java.Boolean is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_FloatBuffer : access Standard.Java.Nio.FloatBuffer.Typ'Class)
                       return Java.Int;

   function Order (This : access Typ)
                   return access Java.Nio.ByteOrder.Typ'Class is abstract;

   function array_K (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Allocate, "allocate");
   pragma Export (Java, Wrap, "wrap");
   pragma Export (Java, Slice, "slice");
   pragma Export (Java, Duplicate, "duplicate");
   pragma Export (Java, AsReadOnlyBuffer, "asReadOnlyBuffer");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Put, "put");
   pragma Export (Java, HasArray, "hasArray");
   pragma Export (Java, array_K, "array");
   pragma Export (Java, ArrayOffset, "arrayOffset");
   pragma Export (Java, Compact, "compact");
   pragma Export (Java, IsDirect, "isDirect");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, CompareTo, "compareTo");
   pragma Export (Java, Order, "order");

end Java.Nio.FloatBuffer;
pragma Import (Java, Java.Nio.FloatBuffer, "java.nio.FloatBuffer");
pragma Extensions_Allowed (Off);
