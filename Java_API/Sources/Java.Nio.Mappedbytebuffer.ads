pragma Extensions_Allowed (On);
with Java.Lang.Comparable;
with Java.Lang.Object;
with Java.Nio.ByteBuffer;

package Java.Nio.MappedByteBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Nio.ByteBuffer.Typ(Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function IsLoaded (This : access Typ)
                      return Java.Boolean;

   --  final
   function Load (This : access Typ)
                  return access Java.Nio.MappedByteBuffer.Typ'Class;

   --  final
   function Force (This : access Typ)
                   return access Java.Nio.MappedByteBuffer.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, IsLoaded, "isLoaded");
   pragma Import (Java, Load, "load");
   pragma Import (Java, Force, "force");

end Java.Nio.MappedByteBuffer;
pragma Import (Java, Java.Nio.MappedByteBuffer, "java.nio.MappedByteBuffer");
pragma Extensions_Allowed (Off);
