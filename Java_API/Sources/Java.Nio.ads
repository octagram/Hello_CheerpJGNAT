pragma Extensions_Allowed (On);
package Java.Nio is
   pragma Preelaborate;
end Java.Nio;
pragma Import (Java, Java.Nio, "java.nio");
pragma Extensions_Allowed (Off);
