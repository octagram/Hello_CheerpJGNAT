pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Activation.ActivationDesc;
limited with Java.Rmi.Activation.ActivationID;
limited with Java.Rmi.MarshalledObject;
limited with Java.Rmi.Server.RMIClientSocketFactory;
limited with Java.Rmi.Server.RMIServerSocketFactory;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Remote;
with Java.Rmi.Server.RemoteServer;

package Java.Rmi.Activation.Activatable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Rmi.Server.RemoteServer.Typ(Serializable_I,
                                                     Remote_I)
      with null record;

   --  protected
   function New_Activatable (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                             P3_Boolean : Java.Boolean;
                             P4_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   --  protected
   function New_Activatable (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                             P3_Boolean : Java.Boolean;
                             P4_Int : Java.Int;
                             P5_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class;
                             P6_RMIServerSocketFactory : access Standard.Java.Rmi.Server.RMIServerSocketFactory.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   --  protected
   function New_Activatable (P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Rmi.RemoteException.Except

   --  protected
   function New_Activatable (P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                             P2_Int : Java.Int;
                             P3_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class;
                             P4_RMIServerSocketFactory : access Standard.Java.Rmi.Server.RMIServerSocketFactory.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Rmi.RemoteException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetID (This : access Typ)
                   return access Java.Rmi.Activation.ActivationID.Typ'Class;

   function Register (P1_ActivationDesc : access Standard.Java.Rmi.Activation.ActivationDesc.Typ'Class)
                      return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.Activation.UnknownGroupException.Except,
   --  Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   function Inactive (P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class)
                      return Java.Boolean;
   --  can raise Java.Rmi.Activation.UnknownObjectException.Except,
   --  Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure Unregister (P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class);
   --  can raise Java.Rmi.Activation.UnknownObjectException.Except,
   --  Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   function ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                          P4_Boolean : Java.Boolean;
                          P5_Int : Java.Int)
                          return access Java.Rmi.Activation.ActivationID.Typ'Class;
   --  can raise Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   function ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                          P4_Boolean : Java.Boolean;
                          P5_Int : Java.Int;
                          P6_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class;
                          P7_RMIServerSocketFactory : access Standard.Java.Rmi.Server.RMIServerSocketFactory.Typ'Class)
                          return access Java.Rmi.Activation.ActivationID.Typ'Class;
   --  can raise Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   function ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                          P2_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                          P3_Int : Java.Int)
                          return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                          P2_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                          P3_Int : Java.Int;
                          P4_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class;
                          P5_RMIServerSocketFactory : access Standard.Java.Rmi.Server.RMIServerSocketFactory.Typ'Class)
                          return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function UnexportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                            P2_Boolean : Java.Boolean)
                            return Java.Boolean;
   --  can raise Java.Rmi.NoSuchObjectException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Activatable);
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, Register, "register");
   pragma Import (Java, Inactive, "inactive");
   pragma Import (Java, Unregister, "unregister");
   pragma Import (Java, ExportObject, "exportObject");
   pragma Import (Java, UnexportObject, "unexportObject");

end Java.Rmi.Activation.Activatable;
pragma Import (Java, Java.Rmi.Activation.Activatable, "java.rmi.activation.Activatable");
pragma Extensions_Allowed (Off);
