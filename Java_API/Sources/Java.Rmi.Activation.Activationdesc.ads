pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Activation.ActivationGroupID;
limited with Java.Rmi.MarshalledObject;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Rmi.Activation.ActivationDesc is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActivationDesc (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Rmi.Activation.ActivationException.Except

   function New_ActivationDesc (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                P4_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Rmi.Activation.ActivationException.Except

   function New_ActivationDesc (P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_ActivationDesc (P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                P5_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetGroupID (This : access Typ)
                        return access Java.Rmi.Activation.ActivationGroupID.Typ'Class;

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetLocation (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetData (This : access Typ)
                     return access Java.Rmi.MarshalledObject.Typ'Class;

   function GetRestartMode (This : access Typ)
                            return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActivationDesc);
   pragma Import (Java, GetGroupID, "getGroupID");
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, GetRestartMode, "getRestartMode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Rmi.Activation.ActivationDesc;
pragma Import (Java, Java.Rmi.Activation.ActivationDesc, "java.rmi.activation.ActivationDesc");
pragma Extensions_Allowed (Off);
