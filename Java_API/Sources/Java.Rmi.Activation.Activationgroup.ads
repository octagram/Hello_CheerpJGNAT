pragma Extensions_Allowed (On);
limited with Java.Rmi.Activation.ActivationGroupDesc;
limited with Java.Rmi.Activation.ActivationGroupID;
limited with Java.Rmi.Activation.ActivationID;
limited with Java.Rmi.Activation.ActivationSystem;
limited with Java.Rmi.MarshalledObject;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Activation.ActivationInstantiator;
with Java.Rmi.Remote;
with Java.Rmi.Server.UnicastRemoteObject;

package Java.Rmi.Activation.ActivationGroup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref;
            ActivationInstantiator_I : Java.Rmi.Activation.ActivationInstantiator.Ref)
    is abstract new Java.Rmi.Server.UnicastRemoteObject.Typ(Serializable_I,
                                                            Remote_I)
      with null record;

   --  protected
   function New_ActivationGroup (P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Rmi.RemoteException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function InactiveObject (This : access Typ;
                            P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class)
                            return Java.Boolean;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure ActiveObject (This : access Typ;
                           P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                           P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except

   --  synchronized
   function CreateGroup (P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class;
                         P2_ActivationGroupDesc : access Standard.Java.Rmi.Activation.ActivationGroupDesc.Typ'Class;
                         P3_Long : Java.Long)
                         return access Java.Rmi.Activation.ActivationGroup.Typ'Class;
   --  can raise Java.Rmi.Activation.ActivationException.Except

   --  synchronized
   function CurrentGroupID return access Java.Rmi.Activation.ActivationGroupID.Typ'Class;

   --  synchronized
   procedure SetSystem (P1_ActivationSystem : access Standard.Java.Rmi.Activation.ActivationSystem.Typ'Class);
   --  can raise Java.Rmi.Activation.ActivationException.Except

   --  synchronized
   function GetSystem return access Java.Rmi.Activation.ActivationSystem.Typ'Class;
   --  can raise Java.Rmi.Activation.ActivationException.Except

   --  protected
   procedure ActiveObject (This : access Typ;
                           P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                           P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class);
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except

   --  protected
   procedure InactiveGroup (This : access Typ);
   --  can raise Java.Rmi.Activation.UnknownGroupException.Except and
   --  Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActivationGroup);
   pragma Export (Java, InactiveObject, "inactiveObject");
   pragma Export (Java, ActiveObject, "activeObject");
   pragma Export (Java, CreateGroup, "createGroup");
   pragma Export (Java, CurrentGroupID, "currentGroupID");
   pragma Export (Java, SetSystem, "setSystem");
   pragma Export (Java, GetSystem, "getSystem");
   pragma Export (Java, InactiveGroup, "inactiveGroup");

end Java.Rmi.Activation.ActivationGroup;
pragma Import (Java, Java.Rmi.Activation.ActivationGroup, "java.rmi.activation.ActivationGroup");
pragma Extensions_Allowed (Off);
