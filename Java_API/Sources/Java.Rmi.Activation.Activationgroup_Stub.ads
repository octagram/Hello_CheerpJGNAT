pragma Extensions_Allowed (On);
limited with Java.Rmi.Activation.ActivationDesc;
limited with Java.Rmi.Activation.ActivationID;
limited with Java.Rmi.MarshalledObject;
limited with Java.Rmi.Server.RemoteRef;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Activation.ActivationInstantiator;
with Java.Rmi.Remote;
with Java.Rmi.Server.RemoteStub;

package Java.Rmi.Activation.ActivationGroup_Stub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref;
            ActivationInstantiator_I : Java.Rmi.Activation.ActivationInstantiator.Ref)
    is new Java.Rmi.Server.RemoteStub.Typ(Serializable_I,
                                          Remote_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActivationGroup_Stub (P1_RemoteRef : access Standard.Java.Rmi.Server.RemoteRef.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance (This : access Typ;
                         P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                         P2_ActivationDesc : access Standard.Java.Rmi.Activation.ActivationDesc.Typ'Class)
                         return access Java.Rmi.MarshalledObject.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except and
   --  Java.Rmi.Activation.ActivationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActivationGroup_Stub);
   pragma Import (Java, NewInstance, "newInstance");

end Java.Rmi.Activation.ActivationGroup_Stub;
pragma Import (Java, Java.Rmi.Activation.ActivationGroup_Stub, "java.rmi.activation.ActivationGroup_Stub");
pragma Extensions_Allowed (Off);
