pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Rmi.Activation.ActivationGroupDesc.CommandEnvironment is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CommandEnvironment (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String_Arr : access Java.Lang.String.Arr_Obj; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCommandPath (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetCommandOptions (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CommandEnvironment);
   pragma Import (Java, GetCommandPath, "getCommandPath");
   pragma Import (Java, GetCommandOptions, "getCommandOptions");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Rmi.Activation.ActivationGroupDesc.CommandEnvironment;
pragma Import (Java, Java.Rmi.Activation.ActivationGroupDesc.CommandEnvironment, "java.rmi.activation.ActivationGroupDesc$CommandEnvironment");
pragma Extensions_Allowed (Off);
