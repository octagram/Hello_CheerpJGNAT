pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Activation.ActivationGroupDesc.CommandEnvironment;
limited with Java.Rmi.MarshalledObject;
limited with Java.Util.Properties;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Rmi.Activation.ActivationGroupDesc is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActivationGroupDesc (P1_Properties : access Standard.Java.Util.Properties.Typ'Class;
                                     P2_CommandEnvironment : access Standard.Java.Rmi.Activation.ActivationGroupDesc.CommandEnvironment.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_ActivationGroupDesc (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                     P4_Properties : access Standard.Java.Util.Properties.Typ'Class;
                                     P5_CommandEnvironment : access Standard.Java.Rmi.Activation.ActivationGroupDesc.CommandEnvironment.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetLocation (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetData (This : access Typ)
                     return access Java.Rmi.MarshalledObject.Typ'Class;

   function GetPropertyOverrides (This : access Typ)
                                  return access Java.Util.Properties.Typ'Class;

   function GetCommandEnvironment (This : access Typ)
                                   return access Java.Rmi.Activation.ActivationGroupDesc.CommandEnvironment.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActivationGroupDesc);
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, GetPropertyOverrides, "getPropertyOverrides");
   pragma Import (Java, GetCommandEnvironment, "getCommandEnvironment");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Rmi.Activation.ActivationGroupDesc;
pragma Import (Java, Java.Rmi.Activation.ActivationGroupDesc, "java.rmi.activation.ActivationGroupDesc");
pragma Extensions_Allowed (Off);
