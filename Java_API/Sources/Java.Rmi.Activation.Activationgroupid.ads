pragma Extensions_Allowed (On);
limited with Java.Rmi.Activation.ActivationSystem;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Rmi.Activation.ActivationGroupID is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActivationGroupID (P1_ActivationSystem : access Standard.Java.Rmi.Activation.ActivationSystem.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSystem (This : access Typ)
                       return access Java.Rmi.Activation.ActivationSystem.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActivationGroupID);
   pragma Import (Java, GetSystem, "getSystem");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Java.Rmi.Activation.ActivationGroupID;
pragma Import (Java, Java.Rmi.Activation.ActivationGroupID, "java.rmi.activation.ActivationGroupID");
pragma Extensions_Allowed (Off);
