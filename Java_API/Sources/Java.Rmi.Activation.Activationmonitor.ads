pragma Extensions_Allowed (On);
limited with Java.Rmi.Activation.ActivationGroupID;
limited with Java.Rmi.Activation.ActivationID;
limited with Java.Rmi.MarshalledObject;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Java.Rmi.Activation.ActivationMonitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InactiveObject (This : access Typ;
                             P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class) is abstract;
   --  can raise Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure ActiveObject (This : access Typ;
                           P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                           P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class) is abstract;
   --  can raise Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure InactiveGroup (This : access Typ;
                            P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class;
                            P2_Long : Java.Long) is abstract;
   --  can raise Java.Rmi.Activation.UnknownGroupException.Except and
   --  Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, InactiveObject, "inactiveObject");
   pragma Export (Java, ActiveObject, "activeObject");
   pragma Export (Java, InactiveGroup, "inactiveGroup");

end Java.Rmi.Activation.ActivationMonitor;
pragma Import (Java, Java.Rmi.Activation.ActivationMonitor, "java.rmi.activation.ActivationMonitor");
pragma Extensions_Allowed (Off);
