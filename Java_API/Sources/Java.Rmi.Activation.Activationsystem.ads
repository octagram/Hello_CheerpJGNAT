pragma Extensions_Allowed (On);
limited with Java.Rmi.Activation.ActivationDesc;
limited with Java.Rmi.Activation.ActivationGroupDesc;
limited with Java.Rmi.Activation.ActivationGroupID;
limited with Java.Rmi.Activation.ActivationID;
limited with Java.Rmi.Activation.ActivationInstantiator;
limited with Java.Rmi.Activation.ActivationMonitor;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Java.Rmi.Activation.ActivationSystem is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function RegisterObject (This : access Typ;
                            P1_ActivationDesc : access Standard.Java.Rmi.Activation.ActivationDesc.Typ'Class)
                            return access Java.Rmi.Activation.ActivationID.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownGroupException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure UnregisterObject (This : access Typ;
                               P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class) is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except

   function RegisterGroup (This : access Typ;
                           P1_ActivationGroupDesc : access Standard.Java.Rmi.Activation.ActivationGroupDesc.Typ'Class)
                           return access Java.Rmi.Activation.ActivationGroupID.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   function ActiveGroup (This : access Typ;
                         P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class;
                         P2_ActivationInstantiator : access Standard.Java.Rmi.Activation.ActivationInstantiator.Typ'Class;
                         P3_Long : Java.Long)
                         return access Java.Rmi.Activation.ActivationMonitor.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.UnknownGroupException.Except,
   --  Java.Rmi.Activation.ActivationException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure UnregisterGroup (This : access Typ;
                              P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class) is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownGroupException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure Shutdown (This : access Typ) is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   function SetActivationDesc (This : access Typ;
                               P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                               P2_ActivationDesc : access Standard.Java.Rmi.Activation.ActivationDesc.Typ'Class)
                               return access Java.Rmi.Activation.ActivationDesc.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownObjectException.Except,
   --  Java.Rmi.Activation.UnknownGroupException.Except and
   --  Java.Rmi.RemoteException.Except

   function SetActivationGroupDesc (This : access Typ;
                                    P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class;
                                    P2_ActivationGroupDesc : access Standard.Java.Rmi.Activation.ActivationGroupDesc.Typ'Class)
                                    return access Java.Rmi.Activation.ActivationGroupDesc.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownGroupException.Except and
   --  Java.Rmi.RemoteException.Except

   function GetActivationDesc (This : access Typ;
                               P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class)
                               return access Java.Rmi.Activation.ActivationDesc.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except

   function GetActivationGroupDesc (This : access Typ;
                                    P1_ActivationGroupID : access Standard.Java.Rmi.Activation.ActivationGroupID.Typ'Class)
                                    return access Java.Rmi.Activation.ActivationGroupDesc.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownGroupException.Except and
   --  Java.Rmi.RemoteException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SYSTEM_PORT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, RegisterObject, "registerObject");
   pragma Export (Java, UnregisterObject, "unregisterObject");
   pragma Export (Java, RegisterGroup, "registerGroup");
   pragma Export (Java, ActiveGroup, "activeGroup");
   pragma Export (Java, UnregisterGroup, "unregisterGroup");
   pragma Export (Java, Shutdown, "shutdown");
   pragma Export (Java, SetActivationDesc, "setActivationDesc");
   pragma Export (Java, SetActivationGroupDesc, "setActivationGroupDesc");
   pragma Export (Java, GetActivationDesc, "getActivationDesc");
   pragma Export (Java, GetActivationGroupDesc, "getActivationGroupDesc");
   pragma Import (Java, SYSTEM_PORT, "SYSTEM_PORT");

end Java.Rmi.Activation.ActivationSystem;
pragma Import (Java, Java.Rmi.Activation.ActivationSystem, "java.rmi.activation.ActivationSystem");
pragma Extensions_Allowed (Off);
