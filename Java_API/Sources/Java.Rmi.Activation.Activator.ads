pragma Extensions_Allowed (On);
limited with Java.Rmi.Activation.ActivationID;
limited with Java.Rmi.MarshalledObject;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Java.Rmi.Activation.Activator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Activate (This : access Typ;
                      P1_ActivationID : access Standard.Java.Rmi.Activation.ActivationID.Typ'Class;
                      P2_Boolean : Java.Boolean)
                      return access Java.Rmi.MarshalledObject.Typ'Class is abstract;
   --  can raise Java.Rmi.Activation.ActivationException.Except,
   --  Java.Rmi.Activation.UnknownObjectException.Except and
   --  Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Activate, "activate");

end Java.Rmi.Activation.Activator;
pragma Import (Java, Java.Rmi.Activation.Activator, "java.rmi.activation.Activator");
pragma Extensions_Allowed (Off);
