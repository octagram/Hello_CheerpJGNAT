pragma Extensions_Allowed (On);
limited with Java.Rmi.Dgc.Lease;
limited with Java.Rmi.Dgc.VMID;
limited with Java.Rmi.Server.ObjID;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Java.Rmi.Dgc.DGC is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Dirty (This : access Typ;
                   P1_ObjID_Arr : access Java.Rmi.Server.ObjID.Arr_Obj;
                   P2_Long : Java.Long;
                   P3_Lease : access Standard.Java.Rmi.Dgc.Lease.Typ'Class)
                   return access Java.Rmi.Dgc.Lease.Typ'Class is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   procedure Clean (This : access Typ;
                    P1_ObjID_Arr : access Java.Rmi.Server.ObjID.Arr_Obj;
                    P2_Long : Java.Long;
                    P3_VMID : access Standard.Java.Rmi.Dgc.VMID.Typ'Class;
                    P4_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Dirty, "dirty");
   pragma Export (Java, Clean, "clean");

end Java.Rmi.Dgc.DGC;
pragma Import (Java, Java.Rmi.Dgc.DGC, "java.rmi.dgc.DGC");
pragma Extensions_Allowed (Off);
