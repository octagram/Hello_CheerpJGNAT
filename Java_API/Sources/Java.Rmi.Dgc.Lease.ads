pragma Extensions_Allowed (On);
limited with Java.Rmi.Dgc.VMID;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Rmi.Dgc.Lease is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Lease (P1_VMID : access Standard.Java.Rmi.Dgc.VMID.Typ'Class;
                       P2_Long : Java.Long; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetVMID (This : access Typ)
                     return access Java.Rmi.Dgc.VMID.Typ'Class;

   function GetValue (This : access Typ)
                      return Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Lease);
   pragma Import (Java, GetVMID, "getVMID");
   pragma Import (Java, GetValue, "getValue");

end Java.Rmi.Dgc.Lease;
pragma Import (Java, Java.Rmi.Dgc.Lease, "java.rmi.dgc.Lease");
pragma Extensions_Allowed (Off);
