pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Remote;
with Java.Lang.Object;

package Java.Rmi.Naming is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Lookup (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.NotBoundException.Except,
   --  Java.Net.MalformedURLException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure Bind (P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class);
   --  can raise Java.Rmi.AlreadyBoundException.Except,
   --  Java.Net.MalformedURLException.Except and
   --  Java.Rmi.RemoteException.Except

   procedure Unbind (P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Rmi.RemoteException.Except,
   --  Java.Rmi.NotBoundException.Except and
   --  Java.Net.MalformedURLException.Except

   procedure Rebind (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class);
   --  can raise Java.Rmi.RemoteException.Except and
   --  Java.Net.MalformedURLException.Except

   function List (P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Rmi.RemoteException.Except and
   --  Java.Net.MalformedURLException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Lookup, "lookup");
   pragma Import (Java, Bind, "bind");
   pragma Import (Java, Unbind, "unbind");
   pragma Import (Java, Rebind, "rebind");
   pragma Import (Java, List, "list");

end Java.Rmi.Naming;
pragma Import (Java, Java.Rmi.Naming, "java.rmi.Naming");
pragma Extensions_Allowed (Off);
