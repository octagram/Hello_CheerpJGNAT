pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Registry.Registry;
limited with Java.Rmi.Server.RMIClientSocketFactory;
limited with Java.Rmi.Server.RMIServerSocketFactory;
with Java.Lang.Object;

package Java.Rmi.Registry.LocateRegistry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRegistry return access Java.Rmi.Registry.Registry.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function GetRegistry (P1_Int : Java.Int)
                         return access Java.Rmi.Registry.Registry.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function GetRegistry (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Rmi.Registry.Registry.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function GetRegistry (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int)
                         return access Java.Rmi.Registry.Registry.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function GetRegistry (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int;
                         P3_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class)
                         return access Java.Rmi.Registry.Registry.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function CreateRegistry (P1_Int : Java.Int)
                            return access Java.Rmi.Registry.Registry.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function CreateRegistry (P1_Int : Java.Int;
                            P2_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class;
                            P3_RMIServerSocketFactory : access Standard.Java.Rmi.Server.RMIServerSocketFactory.Typ'Class)
                            return access Java.Rmi.Registry.Registry.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetRegistry, "getRegistry");
   pragma Import (Java, CreateRegistry, "createRegistry");

end Java.Rmi.Registry.LocateRegistry;
pragma Import (Java, Java.Rmi.Registry.LocateRegistry, "java.rmi.registry.LocateRegistry");
pragma Extensions_Allowed (Off);
