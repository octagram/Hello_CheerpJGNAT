pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Java.Rmi.Registry.Registry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Lookup (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Rmi.Remote.Typ'Class is abstract;
   --  can raise Java.Rmi.RemoteException.Except,
   --  Java.Rmi.NotBoundException.Except and
   --  Java.Rmi.AccessException.Except

   procedure Bind (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;
   --  can raise Java.Rmi.RemoteException.Except,
   --  Java.Rmi.AlreadyBoundException.Except and
   --  Java.Rmi.AccessException.Except

   procedure Unbind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Rmi.RemoteException.Except,
   --  Java.Rmi.NotBoundException.Except and
   --  Java.Rmi.AccessException.Except

   procedure Rebind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;
   --  can raise Java.Rmi.RemoteException.Except and
   --  Java.Rmi.AccessException.Except

   function List (This : access Typ)
                  return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Rmi.RemoteException.Except and
   --  Java.Rmi.AccessException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   REGISTRY_PORT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Lookup, "lookup");
   pragma Export (Java, Bind, "bind");
   pragma Export (Java, Unbind, "unbind");
   pragma Export (Java, Rebind, "rebind");
   pragma Export (Java, List, "list");
   pragma Import (Java, REGISTRY_PORT, "REGISTRY_PORT");

end Java.Rmi.Registry.Registry;
pragma Import (Java, Java.Rmi.Registry.Registry, "java.rmi.registry.Registry");
pragma Extensions_Allowed (Off);
