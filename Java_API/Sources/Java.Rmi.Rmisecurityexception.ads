pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.SecurityException;

package Java.Rmi.RMISecurityException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.SecurityException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.rmi.RMISecurityException");

end Java.Rmi.RMISecurityException;
pragma Import (Java, Java.Rmi.RMISecurityException, "java.rmi.RMISecurityException");
pragma Extensions_Allowed (Off);
