pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Lang.SecurityManager;

package Java.Rmi.RMISecurityManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.SecurityManager.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RMISecurityManager (This : Ref := null)
                                    return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMISecurityManager);

end Java.Rmi.RMISecurityManager;
pragma Import (Java, Java.Rmi.RMISecurityManager, "java.rmi.RMISecurityManager");
pragma Extensions_Allowed (Off);
