pragma Extensions_Allowed (On);
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.PrintStream;
with Java.Lang.Appendable;
with Java.Lang.Object;

package Java.Rmi.Server.LogStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            Appendable_I : Java.Lang.Appendable.Ref)
    is new Java.Io.PrintStream.Typ(Closeable_I,
                                   Flushable_I,
                                   Appendable_I)
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SILENT : constant Java.Int;

   --  final
   BRIEF : constant Java.Int;

   --  final
   VERBOSE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SILENT, "SILENT");
   pragma Import (Java, BRIEF, "BRIEF");
   pragma Import (Java, VERBOSE, "VERBOSE");

end Java.Rmi.Server.LogStream;
pragma Import (Java, Java.Rmi.Server.LogStream, "java.rmi.server.LogStream");
pragma Extensions_Allowed (Off);
