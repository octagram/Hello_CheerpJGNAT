pragma Extensions_Allowed (On);
limited with Java.Io.ObjectInput;
limited with Java.Io.ObjectOutput;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Rmi.Server.ObjID is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjID (This : Ref := null)
                       return Ref;

   function New_ObjID (P1_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_ObjectOutput : access Standard.Java.Io.ObjectOutput.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function Read (P1_ObjectInput : access Standard.Java.Io.ObjectInput.Typ'Class)
                  return access Java.Rmi.Server.ObjID.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   REGISTRY_ID : constant Java.Int;

   --  final
   ACTIVATOR_ID : constant Java.Int;

   --  final
   DGC_ID : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjID);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Read, "read");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, REGISTRY_ID, "REGISTRY_ID");
   pragma Import (Java, ACTIVATOR_ID, "ACTIVATOR_ID");
   pragma Import (Java, DGC_ID, "DGC_ID");

end Java.Rmi.Server.ObjID;
pragma Import (Java, Java.Rmi.Server.ObjID, "java.rmi.server.ObjID");
pragma Extensions_Allowed (Off);
