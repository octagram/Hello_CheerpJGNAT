pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Server.RemoteRef;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Java.Rmi.Server.RemoteObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Ref : access Java.Rmi.Server.RemoteRef.Typ'Class;
      pragma Import (Java, Ref, "ref");

   end record;

   --  protected
   function New_RemoteObject (This : Ref := null)
                              return Ref;

   --  protected
   function New_RemoteObject (P1_RemoteRef : access Standard.Java.Rmi.Server.RemoteRef.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRef (This : access Typ)
                    return access Java.Rmi.Server.RemoteRef.Typ'Class;

   function ToStub (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class)
                    return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.NoSuchObjectException.Except

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RemoteObject);
   pragma Import (Java, GetRef, "getRef");
   pragma Import (Java, ToStub, "toStub");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");

end Java.Rmi.Server.RemoteObject;
pragma Import (Java, Java.Rmi.Server.RemoteObject, "java.rmi.server.RemoteObject");
pragma Extensions_Allowed (Off);
