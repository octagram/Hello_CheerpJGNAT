pragma Extensions_Allowed (On);
limited with Java.Lang.Reflect.Method;
limited with Java.Rmi.Server.RemoteRef;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.Reflect.InvocationHandler;
with Java.Rmi.Remote;
with Java.Rmi.Server.RemoteObject;

package Java.Rmi.Server.RemoteObjectInvocationHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            InvocationHandler_I : Java.Lang.Reflect.InvocationHandler.Ref;
            Remote_I : Java.Rmi.Remote.Ref)
    is new Java.Rmi.Server.RemoteObject.Typ(Serializable_I,
                                            Remote_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RemoteObjectInvocationHandler (P1_RemoteRef : access Standard.Java.Rmi.Server.RemoteRef.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Invoke (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Throwable.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RemoteObjectInvocationHandler);
   pragma Import (Java, Invoke, "invoke");

end Java.Rmi.Server.RemoteObjectInvocationHandler;
pragma Import (Java, Java.Rmi.Server.RemoteObjectInvocationHandler, "java.rmi.server.RemoteObjectInvocationHandler");
pragma Extensions_Allowed (Off);
