pragma Extensions_Allowed (On);
limited with Java.Io.ObjectOutput;
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
limited with Java.Rmi.Remote;
with Java.Io.Externalizable;
with Java.Lang.Object;

package Java.Rmi.Server.RemoteRef is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Externalizable_I : Java.Io.Externalizable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Invoke (This : access Typ;
                    P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                    P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P4_Long : Java.Long)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.Exception_K.Except

   function GetRefClass (This : access Typ;
                         P1_ObjectOutput : access Standard.Java.Io.ObjectOutput.Typ'Class)
                         return access Java.Lang.String.Typ'Class is abstract;

   function RemoteHashCode (This : access Typ)
                            return Java.Int is abstract;

   function RemoteEquals (This : access Typ;
                          P1_RemoteRef : access Standard.Java.Rmi.Server.RemoteRef.Typ'Class)
                          return Java.Boolean is abstract;

   function RemoteToString (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;

   --  final
   PackagePrefix : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Invoke, "invoke");
   pragma Export (Java, GetRefClass, "getRefClass");
   pragma Export (Java, RemoteHashCode, "remoteHashCode");
   pragma Export (Java, RemoteEquals, "remoteEquals");
   pragma Export (Java, RemoteToString, "remoteToString");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");
   pragma Import (Java, PackagePrefix, "packagePrefix");

end Java.Rmi.Server.RemoteRef;
pragma Import (Java, Java.Rmi.Server.RemoteRef, "java.rmi.server.RemoteRef");
pragma Extensions_Allowed (Off);
