pragma Extensions_Allowed (On);
limited with Java.Rmi.Server.RemoteRef;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Remote;
with Java.Rmi.Server.RemoteObject;

package Java.Rmi.Server.RemoteStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Rmi.Server.RemoteObject.Typ(Serializable_I,
                                                     Remote_I)
      with null record;

   --  protected
   function New_RemoteStub (This : Ref := null)
                            return Ref;

   --  protected
   function New_RemoteStub (P1_RemoteRef : access Standard.Java.Rmi.Server.RemoteRef.Typ'Class; 
                            This : Ref := null)
                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RemoteStub);

end Java.Rmi.Server.RemoteStub;
pragma Import (Java, Java.Rmi.Server.RemoteStub, "java.rmi.server.RemoteStub");
pragma Extensions_Allowed (Off);
