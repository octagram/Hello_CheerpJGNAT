pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Rmi.Server.RMIClassLoaderSpi;
with Java.Lang.Object;

package Java.Rmi.Server.RMIClassLoader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function LoadClass (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Net.MalformedURLException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function LoadClass (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Net.MalformedURLException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function LoadClass (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Net.MalformedURLException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function LoadProxyClass (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String_Arr : access Java.Lang.String.Arr_Obj;
                            P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                            return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except and
   --  Java.Net.MalformedURLException.Except

   function GetClassLoader (P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.ClassLoader.Typ'Class;
   --  can raise Java.Net.MalformedURLException.Except and
   --  Java.Lang.SecurityException.Except

   function GetClassAnnotation (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function GetDefaultProviderInstance return access Java.Rmi.Server.RMIClassLoaderSpi.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, LoadClass, "loadClass");
   pragma Import (Java, LoadProxyClass, "loadProxyClass");
   pragma Import (Java, GetClassLoader, "getClassLoader");
   pragma Import (Java, GetClassAnnotation, "getClassAnnotation");
   pragma Import (Java, GetDefaultProviderInstance, "getDefaultProviderInstance");

end Java.Rmi.Server.RMIClassLoader;
pragma Import (Java, Java.Rmi.Server.RMIClassLoader, "java.rmi.server.RMIClassLoader");
pragma Extensions_Allowed (Off);
