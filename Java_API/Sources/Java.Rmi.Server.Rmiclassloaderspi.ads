pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Rmi.Server.RMIClassLoaderSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_RMIClassLoaderSpi (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function LoadClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                       return access Java.Lang.Class.Typ'Class is abstract;
   --  can raise Java.Net.MalformedURLException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function LoadProxyClass (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String_Arr : access Java.Lang.String.Arr_Obj;
                            P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                            return access Java.Lang.Class.Typ'Class is abstract;
   --  can raise Java.Net.MalformedURLException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function GetClassLoader (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.ClassLoader.Typ'Class is abstract;
   --  can raise Java.Net.MalformedURLException.Except

   function GetClassAnnotation (This : access Typ;
                                P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMIClassLoaderSpi);
   pragma Export (Java, LoadClass, "loadClass");
   pragma Export (Java, LoadProxyClass, "loadProxyClass");
   pragma Export (Java, GetClassLoader, "getClassLoader");
   pragma Export (Java, GetClassAnnotation, "getClassAnnotation");

end Java.Rmi.Server.RMIClassLoaderSpi;
pragma Import (Java, Java.Rmi.Server.RMIClassLoaderSpi, "java.rmi.server.RMIClassLoaderSpi");
pragma Extensions_Allowed (Off);
