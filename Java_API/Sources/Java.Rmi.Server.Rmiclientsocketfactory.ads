pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.Socket;
with Java.Lang.Object;

package Java.Rmi.Server.RMIClientSocketFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateSocket (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Net.Socket.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateSocket, "createSocket");

end Java.Rmi.Server.RMIClientSocketFactory;
pragma Import (Java, Java.Rmi.Server.RMIClientSocketFactory, "java.rmi.server.RMIClientSocketFactory");
pragma Extensions_Allowed (Off);
