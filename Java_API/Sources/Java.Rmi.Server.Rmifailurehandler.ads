pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
with Java.Lang.Object;

package Java.Rmi.Server.RMIFailureHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Failure (This : access Typ;
                     P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class)
                     return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Failure, "failure");

end Java.Rmi.Server.RMIFailureHandler;
pragma Import (Java, Java.Rmi.Server.RMIFailureHandler, "java.rmi.server.RMIFailureHandler");
pragma Extensions_Allowed (Off);
