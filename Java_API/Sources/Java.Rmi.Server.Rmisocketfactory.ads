pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.ServerSocket;
limited with Java.Net.Socket;
limited with Java.Rmi.Server.RMIFailureHandler;
with Java.Lang.Object;
with Java.Rmi.Server.RMIClientSocketFactory;
with Java.Rmi.Server.RMIServerSocketFactory;

package Java.Rmi.Server.RMISocketFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RMIClientSocketFactory_I : Java.Rmi.Server.RMIClientSocketFactory.Ref;
            RMIServerSocketFactory_I : Java.Rmi.Server.RMIServerSocketFactory.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_RMISocketFactory (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateSocket (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Net.Socket.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function CreateServerSocket (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Net.ServerSocket.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure SetSocketFactory (P1_RMISocketFactory : access Standard.Java.Rmi.Server.RMISocketFactory.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function GetSocketFactory return access Java.Rmi.Server.RMISocketFactory.Typ'Class;

   --  synchronized
   function GetDefaultSocketFactory return access Java.Rmi.Server.RMISocketFactory.Typ'Class;

   --  synchronized
   procedure SetFailureHandler (P1_RMIFailureHandler : access Standard.Java.Rmi.Server.RMIFailureHandler.Typ'Class);

   --  synchronized
   function GetFailureHandler return access Java.Rmi.Server.RMIFailureHandler.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMISocketFactory);
   pragma Export (Java, CreateSocket, "createSocket");
   pragma Export (Java, CreateServerSocket, "createServerSocket");
   pragma Export (Java, SetSocketFactory, "setSocketFactory");
   pragma Export (Java, GetSocketFactory, "getSocketFactory");
   pragma Export (Java, GetDefaultSocketFactory, "getDefaultSocketFactory");
   pragma Export (Java, SetFailureHandler, "setFailureHandler");
   pragma Export (Java, GetFailureHandler, "getFailureHandler");

end Java.Rmi.Server.RMISocketFactory;
pragma Import (Java, Java.Rmi.Server.RMISocketFactory, "java.rmi.server.RMISocketFactory");
pragma Extensions_Allowed (Off);
