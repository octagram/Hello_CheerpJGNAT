pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.CloneNotSupportedException;
with Java.Lang.Object;

package Java.Rmi.Server.ServerCloneException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.CloneNotSupportedException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Detail : access Java.Lang.Exception_K.Typ'Class;
      pragma Import (Java, Detail, "detail");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ServerCloneException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_ServerCloneException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.rmi.server.ServerCloneException");
   pragma Java_Constructor (New_ServerCloneException);
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, GetCause, "getCause");

end Java.Rmi.Server.ServerCloneException;
pragma Import (Java, Java.Rmi.Server.ServerCloneException, "java.rmi.server.ServerCloneException");
pragma Extensions_Allowed (Off);
