pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Remote;
limited with Java.Rmi.Server.RemoteStub;
with Java.Lang.Object;
with Java.Rmi.Server.RemoteRef;

package Java.Rmi.Server.ServerRef is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RemoteRef_I : Java.Rmi.Server.RemoteRef.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ExportObject (This : access Typ;
                          P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Rmi.Server.RemoteStub.Typ'Class is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   function GetClientHost (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Rmi.Server.ServerNotActiveException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ExportObject, "exportObject");
   pragma Export (Java, GetClientHost, "getClientHost");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Java.Rmi.Server.ServerRef;
pragma Import (Java, Java.Rmi.Server.ServerRef, "java.rmi.server.ServerRef");
pragma Extensions_Allowed (Off);
