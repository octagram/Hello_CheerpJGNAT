pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.RemoteException;

package Java.Rmi.Server.SkeletonMismatchException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Rmi.RemoteException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.rmi.server.SkeletonMismatchException");

end Java.Rmi.Server.SkeletonMismatchException;
pragma Import (Java, Java.Rmi.Server.SkeletonMismatchException, "java.rmi.server.SkeletonMismatchException");
pragma Extensions_Allowed (Off);
