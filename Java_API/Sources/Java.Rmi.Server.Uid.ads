pragma Extensions_Allowed (On);
limited with Java.Io.DataInput;
limited with Java.Io.DataOutput;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Rmi.Server.UID is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UID (This : Ref := null)
                     return Ref;

   function New_UID (P1_Short : Java.Short; 
                     This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Write (This : access Typ;
                    P1_DataOutput : access Standard.Java.Io.DataOutput.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function Read (P1_DataInput : access Standard.Java.Io.DataInput.Typ'Class)
                  return access Java.Rmi.Server.UID.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UID);
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Read, "read");

end Java.Rmi.Server.UID;
pragma Import (Java, Java.Rmi.Server.UID, "java.rmi.server.UID");
pragma Extensions_Allowed (Off);
