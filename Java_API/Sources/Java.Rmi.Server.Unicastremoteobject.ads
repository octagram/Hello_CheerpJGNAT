pragma Extensions_Allowed (On);
limited with Java.Rmi.Server.RMIClientSocketFactory;
limited with Java.Rmi.Server.RMIServerSocketFactory;
limited with Java.Rmi.Server.RemoteStub;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Remote;
with Java.Rmi.Server.RemoteServer;

package Java.Rmi.Server.UnicastRemoteObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref)
    is new Java.Rmi.Server.RemoteServer.Typ(Serializable_I,
                                            Remote_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_UnicastRemoteObject (This : Ref := null)
                                     return Ref;
   --  can raise Java.Rmi.RemoteException.Except

   --  protected
   function New_UnicastRemoteObject (P1_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;
   --  can raise Java.Rmi.RemoteException.Except

   --  protected
   function New_UnicastRemoteObject (P1_Int : Java.Int;
                                     P2_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class;
                                     P3_RMIServerSocketFactory : access Standard.Java.Rmi.Server.RMIServerSocketFactory.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;
   --  can raise Java.Rmi.RemoteException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   function ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class)
                          return access Java.Rmi.Server.RemoteStub.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                          P2_Int : Java.Int;
                          P3_RMIClientSocketFactory : access Standard.Java.Rmi.Server.RMIClientSocketFactory.Typ'Class;
                          P4_RMIServerSocketFactory : access Standard.Java.Rmi.Server.RMIServerSocketFactory.Typ'Class)
                          return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function UnexportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                            P2_Boolean : Java.Boolean)
                            return Java.Boolean;
   --  can raise Java.Rmi.NoSuchObjectException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UnicastRemoteObject);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ExportObject, "exportObject");
   pragma Import (Java, UnexportObject, "unexportObject");

end Java.Rmi.Server.UnicastRemoteObject;
pragma Import (Java, Java.Rmi.Server.UnicastRemoteObject, "java.rmi.server.UnicastRemoteObject");
pragma Extensions_Allowed (Off);
