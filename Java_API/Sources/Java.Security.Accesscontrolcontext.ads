pragma Extensions_Allowed (On);
limited with Java.Security.DomainCombiner;
limited with Java.Security.Permission;
limited with Java.Security.ProtectionDomain;
with Java.Lang.Object;

package Java.Security.AccessControlContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AccessControlContext (P1_ProtectionDomain_Arr : access Java.Security.ProtectionDomain.Arr_Obj; 
                                      This : Ref := null)
                                      return Ref;

   function New_AccessControlContext (P1_AccessControlContext : access Standard.Java.Security.AccessControlContext.Typ'Class;
                                      P2_DomainCombiner : access Standard.Java.Security.DomainCombiner.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDomainCombiner (This : access Typ)
                               return access Java.Security.DomainCombiner.Typ'Class;

   procedure CheckPermission (This : access Typ;
                              P1_Permission : access Standard.Java.Security.Permission.Typ'Class);
   --  can raise Java.Security.AccessControlException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessControlContext);
   pragma Import (Java, GetDomainCombiner, "getDomainCombiner");
   pragma Import (Java, CheckPermission, "checkPermission");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Security.AccessControlContext;
pragma Import (Java, Java.Security.AccessControlContext, "java.security.AccessControlContext");
pragma Extensions_Allowed (Off);
