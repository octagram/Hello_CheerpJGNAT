pragma Extensions_Allowed (On);
limited with Java.Security.AccessControlContext;
limited with Java.Security.Permission;
limited with Java.Security.PrivilegedAction;
limited with Java.Security.PrivilegedExceptionAction;
with Java.Lang.Object;

package Java.Security.AccessController is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function DoPrivileged (P1_PrivilegedAction : access Standard.Java.Security.PrivilegedAction.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function DoPrivilegedWithCombiner (P1_PrivilegedAction : access Standard.Java.Security.PrivilegedAction.Typ'Class)
                                      return access Java.Lang.Object.Typ'Class;

   function DoPrivileged (P1_PrivilegedAction : access Standard.Java.Security.PrivilegedAction.Typ'Class;
                          P2_AccessControlContext : access Standard.Java.Security.AccessControlContext.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function DoPrivileged (P1_PrivilegedExceptionAction : access Standard.Java.Security.PrivilegedExceptionAction.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Security.PrivilegedActionException.Except

   function DoPrivilegedWithCombiner (P1_PrivilegedExceptionAction : access Standard.Java.Security.PrivilegedExceptionAction.Typ'Class)
                                      return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Security.PrivilegedActionException.Except

   function DoPrivileged (P1_PrivilegedExceptionAction : access Standard.Java.Security.PrivilegedExceptionAction.Typ'Class;
                          P2_AccessControlContext : access Standard.Java.Security.AccessControlContext.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Security.PrivilegedActionException.Except

   function GetContext return access Java.Security.AccessControlContext.Typ'Class;

   procedure CheckPermission (P1_Permission : access Standard.Java.Security.Permission.Typ'Class);
   --  can raise Java.Security.AccessControlException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, DoPrivileged, "doPrivileged");
   pragma Import (Java, DoPrivilegedWithCombiner, "doPrivilegedWithCombiner");
   pragma Import (Java, GetContext, "getContext");
   pragma Import (Java, CheckPermission, "checkPermission");

end Java.Security.AccessController;
pragma Import (Java, Java.Security.AccessController, "java.security.AccessController");
pragma Extensions_Allowed (Off);
