pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Acl.AclEntry;
limited with Java.Security.Acl.Permission;
limited with Java.Security.Principal;
limited with Java.Util.Enumeration;
with Java.Lang.Object;
with Java.Security.Acl.Owner;

package Java.Security.Acl.Acl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Owner_I : Java.Security.Acl.Owner.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetName (This : access Typ;
                      P1_Principal : access Standard.Java.Security.Principal.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Security.Acl.NotOwnerException.Except

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function AddEntry (This : access Typ;
                      P1_Principal : access Standard.Java.Security.Principal.Typ'Class;
                      P2_AclEntry : access Standard.Java.Security.Acl.AclEntry.Typ'Class)
                      return Java.Boolean is abstract;
   --  can raise Java.Security.Acl.NotOwnerException.Except

   function RemoveEntry (This : access Typ;
                         P1_Principal : access Standard.Java.Security.Principal.Typ'Class;
                         P2_AclEntry : access Standard.Java.Security.Acl.AclEntry.Typ'Class)
                         return Java.Boolean is abstract;
   --  can raise Java.Security.Acl.NotOwnerException.Except

   function GetPermissions (This : access Typ;
                            P1_Principal : access Standard.Java.Security.Principal.Typ'Class)
                            return access Java.Util.Enumeration.Typ'Class is abstract;

   function Entries (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class is abstract;

   function CheckPermission (This : access Typ;
                             P1_Principal : access Standard.Java.Security.Principal.Typ'Class;
                             P2_Permission : access Standard.Java.Security.Acl.Permission.Typ'Class)
                             return Java.Boolean is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, AddEntry, "addEntry");
   pragma Export (Java, RemoveEntry, "removeEntry");
   pragma Export (Java, GetPermissions, "getPermissions");
   pragma Export (Java, Entries, "entries");
   pragma Export (Java, CheckPermission, "checkPermission");
   pragma Export (Java, ToString, "toString");

end Java.Security.Acl.Acl;
pragma Import (Java, Java.Security.Acl.Acl, "java.security.acl.Acl");
pragma Extensions_Allowed (Off);
