pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Acl.Permission;
limited with Java.Security.Principal;
limited with Java.Util.Enumeration;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Security.Acl.AclEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function SetPrincipal (This : access Typ;
                          P1_Principal : access Standard.Java.Security.Principal.Typ'Class)
                          return Java.Boolean is abstract;

   function GetPrincipal (This : access Typ)
                          return access Java.Security.Principal.Typ'Class is abstract;

   procedure SetNegativePermissions (This : access Typ) is abstract;

   function IsNegative (This : access Typ)
                        return Java.Boolean is abstract;

   function AddPermission (This : access Typ;
                           P1_Permission : access Standard.Java.Security.Acl.Permission.Typ'Class)
                           return Java.Boolean is abstract;

   function RemovePermission (This : access Typ;
                              P1_Permission : access Standard.Java.Security.Acl.Permission.Typ'Class)
                              return Java.Boolean is abstract;

   function CheckPermission (This : access Typ;
                             P1_Permission : access Standard.Java.Security.Acl.Permission.Typ'Class)
                             return Java.Boolean is abstract;

   function Permissions (This : access Typ)
                         return access Java.Util.Enumeration.Typ'Class is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetPrincipal, "setPrincipal");
   pragma Export (Java, GetPrincipal, "getPrincipal");
   pragma Export (Java, SetNegativePermissions, "setNegativePermissions");
   pragma Export (Java, IsNegative, "isNegative");
   pragma Export (Java, AddPermission, "addPermission");
   pragma Export (Java, RemovePermission, "removePermission");
   pragma Export (Java, CheckPermission, "checkPermission");
   pragma Export (Java, Permissions, "permissions");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, Clone, "clone");

end Java.Security.Acl.AclEntry;
pragma Import (Java, Java.Security.Acl.AclEntry, "java.security.acl.AclEntry");
pragma Extensions_Allowed (Off);
