pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
with Java.Lang.Object;
with Java.Security.Principal;

package Java.Security.Acl.Group is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Principal_I : Java.Security.Principal.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddMember (This : access Typ;
                       P1_Principal : access Standard.Java.Security.Principal.Typ'Class)
                       return Java.Boolean is abstract;

   function RemoveMember (This : access Typ;
                          P1_Principal : access Standard.Java.Security.Principal.Typ'Class)
                          return Java.Boolean is abstract;

   function IsMember (This : access Typ;
                      P1_Principal : access Standard.Java.Security.Principal.Typ'Class)
                      return Java.Boolean is abstract;

   function Members (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddMember, "addMember");
   pragma Export (Java, RemoveMember, "removeMember");
   pragma Export (Java, IsMember, "isMember");
   pragma Export (Java, Members, "members");

end Java.Security.Acl.Group;
pragma Import (Java, Java.Security.Acl.Group, "java.security.acl.Group");
pragma Extensions_Allowed (Off);
