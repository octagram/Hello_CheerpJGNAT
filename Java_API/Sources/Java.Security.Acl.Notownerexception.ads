pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Java.Security.Acl.NotOwnerException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NotOwnerException (This : Ref := null)
                                   return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.security.acl.NotOwnerException");
   pragma Java_Constructor (New_NotOwnerException);

end Java.Security.Acl.NotOwnerException;
pragma Import (Java, Java.Security.Acl.NotOwnerException, "java.security.acl.NotOwnerException");
pragma Extensions_Allowed (Off);
