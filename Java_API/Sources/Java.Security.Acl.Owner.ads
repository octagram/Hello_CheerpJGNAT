pragma Extensions_Allowed (On);
limited with Java.Security.Principal;
with Java.Lang.Object;

package Java.Security.Acl.Owner is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddOwner (This : access Typ;
                      P1_Principal : access Standard.Java.Security.Principal.Typ'Class;
                      P2_Principal : access Standard.Java.Security.Principal.Typ'Class)
                      return Java.Boolean is abstract;
   --  can raise Java.Security.Acl.NotOwnerException.Except

   function DeleteOwner (This : access Typ;
                         P1_Principal : access Standard.Java.Security.Principal.Typ'Class;
                         P2_Principal : access Standard.Java.Security.Principal.Typ'Class)
                         return Java.Boolean is abstract;
   --  can raise Java.Security.Acl.NotOwnerException.Except and
   --  Java.Security.Acl.LastOwnerException.Except

   function IsOwner (This : access Typ;
                     P1_Principal : access Standard.Java.Security.Principal.Typ'Class)
                     return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddOwner, "addOwner");
   pragma Export (Java, DeleteOwner, "deleteOwner");
   pragma Export (Java, IsOwner, "isOwner");

end Java.Security.Acl.Owner;
pragma Import (Java, Java.Security.Acl.Owner, "java.security.acl.Owner");
pragma Extensions_Allowed (Off);
