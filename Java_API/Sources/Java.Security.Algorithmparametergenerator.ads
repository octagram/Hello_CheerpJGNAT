pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.AlgorithmParameterGeneratorSpi;
limited with Java.Security.AlgorithmParameters;
limited with Java.Security.Provider;
limited with Java.Security.SecureRandom;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;

package Java.Security.AlgorithmParameterGenerator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AlgorithmParameterGenerator (P1_AlgorithmParameterGeneratorSpi : access Standard.Java.Security.AlgorithmParameterGeneratorSpi.Typ'Class;
                                             P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                                             P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.AlgorithmParameterGenerator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.AlgorithmParameterGenerator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.AlgorithmParameterGenerator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   procedure Init (This : access Typ;
                   P1_Int : Java.Int);

   --  final
   procedure Init (This : access Typ;
                   P1_Int : Java.Int;
                   P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class);

   --  final
   procedure Init (This : access Typ;
                   P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   procedure Init (This : access Typ;
                   P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;
                   P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   function GenerateParameters (This : access Typ)
                                return access Java.Security.AlgorithmParameters.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AlgorithmParameterGenerator);
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, Init, "init");
   pragma Import (Java, GenerateParameters, "generateParameters");

end Java.Security.AlgorithmParameterGenerator;
pragma Import (Java, Java.Security.AlgorithmParameterGenerator, "java.security.AlgorithmParameterGenerator");
pragma Extensions_Allowed (Off);
