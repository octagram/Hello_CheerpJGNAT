pragma Extensions_Allowed (On);
limited with Java.Security.AlgorithmParameters;
limited with Java.Security.SecureRandom;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;

package Java.Security.AlgorithmParameterGeneratorSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AlgorithmParameterGeneratorSpi (This : Ref := null)
                                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure EngineInit (This : access Typ;
                         P1_Int : Java.Int;
                         P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class) is abstract;

   --  protected
   procedure EngineInit (This : access Typ;
                         P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;
                         P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class) is abstract;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   --  protected
   function EngineGenerateParameters (This : access Typ)
                                      return access Java.Security.AlgorithmParameters.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AlgorithmParameterGeneratorSpi);
   pragma Export (Java, EngineInit, "engineInit");
   pragma Export (Java, EngineGenerateParameters, "engineGenerateParameters");

end Java.Security.AlgorithmParameterGeneratorSpi;
pragma Import (Java, Java.Security.AlgorithmParameterGeneratorSpi, "java.security.AlgorithmParameterGeneratorSpi");
pragma Extensions_Allowed (Off);
