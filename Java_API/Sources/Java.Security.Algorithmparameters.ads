pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Security.AlgorithmParametersSpi;
limited with Java.Security.Provider;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;

package Java.Security.AlgorithmParameters is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AlgorithmParameters (P1_AlgorithmParametersSpi : access Standard.Java.Security.AlgorithmParametersSpi.Typ'Class;
                                     P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                                     P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.AlgorithmParameters.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.AlgorithmParameters.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.AlgorithmParameters.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   procedure Init (This : access Typ;
                   P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class);
   --  can raise Java.Security.Spec.InvalidParameterSpecException.Except

   --  final
   procedure Init (This : access Typ;
                   P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   --  final
   procedure Init (This : access Typ;
                   P1_Byte_Arr : Java.Byte_Arr;
                   P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  final
   function GetParameterSpec (This : access Typ;
                              P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;
   --  can raise Java.Security.Spec.InvalidParameterSpecException.Except

   --  final
   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr;
   --  can raise Java.Io.IOException.Except

   --  final
   function GetEncoded (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Byte_Arr;
   --  can raise Java.Io.IOException.Except

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AlgorithmParameters);
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, Init, "init");
   pragma Import (Java, GetParameterSpec, "getParameterSpec");
   pragma Import (Java, GetEncoded, "getEncoded");
   pragma Import (Java, ToString, "toString");

end Java.Security.AlgorithmParameters;
pragma Import (Java, Java.Security.AlgorithmParameters, "java.security.AlgorithmParameters");
pragma Extensions_Allowed (Off);
