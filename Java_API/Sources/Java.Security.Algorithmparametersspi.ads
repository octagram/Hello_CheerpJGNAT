pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;

package Java.Security.AlgorithmParametersSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AlgorithmParametersSpi (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure EngineInit (This : access Typ;
                         P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class) is abstract;
   --  can raise Java.Security.Spec.InvalidParameterSpecException.Except

   --  protected
   procedure EngineInit (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure EngineInit (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr;
                         P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function EngineGetParameterSpec (This : access Typ;
                                    P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                    return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class is abstract;
   --  can raise Java.Security.Spec.InvalidParameterSpecException.Except

   --  protected
   function EngineGetEncoded (This : access Typ)
                              return Java.Byte_Arr is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function EngineGetEncoded (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Byte_Arr is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function EngineToString (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AlgorithmParametersSpi);
   pragma Export (Java, EngineInit, "engineInit");
   pragma Export (Java, EngineGetParameterSpec, "engineGetParameterSpec");
   pragma Export (Java, EngineGetEncoded, "engineGetEncoded");
   pragma Export (Java, EngineToString, "engineToString");

end Java.Security.AlgorithmParametersSpi;
pragma Import (Java, Java.Security.AlgorithmParametersSpi, "java.security.AlgorithmParametersSpi");
pragma Extensions_Allowed (Off);
