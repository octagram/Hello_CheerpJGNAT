pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Security.Auth.Callback.CallbackHandler;
limited with Javax.Security.Auth.Subject;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Security.Provider;
with Java.Util.Map;

package Java.Security.AuthProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is abstract new Java.Security.Provider.Typ(Serializable_I,
                                               Cloneable_I,
                                               Map_I)
      with null record;

   --  protected
   function New_AuthProvider (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Double : Java.Double;
                              P3_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Login (This : access Typ;
                    P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                    P2_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class) is abstract;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   procedure Logout (This : access Typ) is abstract;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   procedure SetCallbackHandler (This : access Typ;
                                 P1_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AuthProvider);
   pragma Export (Java, Login, "login");
   pragma Export (Java, Logout, "logout");
   pragma Export (Java, SetCallbackHandler, "setCallbackHandler");

end Java.Security.AuthProvider;
pragma Import (Java, Java.Security.AuthProvider, "java.security.AuthProvider");
pragma Extensions_Allowed (Off);
