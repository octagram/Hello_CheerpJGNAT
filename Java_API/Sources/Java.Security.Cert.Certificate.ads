pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.PublicKey;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.Cert.Certificate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Certificate (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr is abstract;
   --  can raise Java.Security.Cert.CertificateEncodingException.Except

   procedure Verify (This : access Typ;
                     P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class) is abstract;
   --  can raise Java.Security.Cert.CertificateException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except,
   --  Java.Security.InvalidKeyException.Except,
   --  Java.Security.NoSuchProviderException.Except and
   --  Java.Security.SignatureException.Except

   procedure Verify (This : access Typ;
                     P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Security.Cert.CertificateException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except,
   --  Java.Security.InvalidKeyException.Except,
   --  Java.Security.NoSuchProviderException.Except and
   --  Java.Security.SignatureException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function GetPublicKey (This : access Typ)
                          return access Java.Security.PublicKey.Typ'Class is abstract;

   --  protected
   function WriteReplace (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.ObjectStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Certificate);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Export (Java, GetEncoded, "getEncoded");
   pragma Export (Java, Verify, "verify");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, GetPublicKey, "getPublicKey");
   pragma Import (Java, WriteReplace, "writeReplace");

end Java.Security.Cert.Certificate;
pragma Import (Java, Java.Security.Cert.Certificate, "java.security.cert.Certificate");
pragma Extensions_Allowed (Off);
