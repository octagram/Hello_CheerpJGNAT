pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Security.Cert.CRL;
limited with Java.Security.Cert.CertPath;
limited with Java.Security.Cert.Certificate;
limited with Java.Security.Cert.CertificateFactorySpi;
limited with Java.Security.Provider;
limited with Java.Util.Collection;
limited with Java.Util.Iterator;
limited with Java.Util.List;
with Java.Lang.Object;

package Java.Security.Cert.CertificateFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_CertificateFactory (P1_CertificateFactorySpi : access Standard.Java.Security.Cert.CertificateFactorySpi.Typ'Class;
                                    P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                                    P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Cert.CertificateFactory.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   --  final
   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Cert.CertificateFactory.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except and
   --  Java.Security.NoSuchProviderException.Except

   --  final
   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.Cert.CertificateFactory.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GenerateCertificate (This : access Typ;
                                 P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                 return access Java.Security.Cert.Certificate.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   --  final
   function GetCertPathEncodings (This : access Typ)
                                  return access Java.Util.Iterator.Typ'Class;

   --  final
   function GenerateCertPath (This : access Typ;
                              P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                              return access Java.Security.Cert.CertPath.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   --  final
   function GenerateCertPath (This : access Typ;
                              P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Security.Cert.CertPath.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   --  final
   function GenerateCertPath (This : access Typ;
                              P1_List : access Standard.Java.Util.List.Typ'Class)
                              return access Java.Security.Cert.CertPath.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   --  final
   function GenerateCertificates (This : access Typ;
                                  P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                  return access Java.Util.Collection.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   --  final
   function GenerateCRL (This : access Typ;
                         P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                         return access Java.Security.Cert.CRL.Typ'Class;
   --  can raise Java.Security.Cert.CRLException.Except

   --  final
   function GenerateCRLs (This : access Typ;
                          P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                          return access Java.Util.Collection.Typ'Class;
   --  can raise Java.Security.Cert.CRLException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertificateFactory);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GenerateCertificate, "generateCertificate");
   pragma Import (Java, GetCertPathEncodings, "getCertPathEncodings");
   pragma Import (Java, GenerateCertPath, "generateCertPath");
   pragma Import (Java, GenerateCertificates, "generateCertificates");
   pragma Import (Java, GenerateCRL, "generateCRL");
   pragma Import (Java, GenerateCRLs, "generateCRLs");

end Java.Security.Cert.CertificateFactory;
pragma Import (Java, Java.Security.Cert.CertificateFactory, "java.security.cert.CertificateFactory");
pragma Extensions_Allowed (Off);
