pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Security.Cert.CRL;
limited with Java.Security.Cert.CertPath;
limited with Java.Security.Cert.Certificate;
limited with Java.Util.Collection;
limited with Java.Util.Iterator;
limited with Java.Util.List;
with Java.Lang.Object;

package Java.Security.Cert.CertificateFactorySpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CertificateFactorySpi (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function EngineGenerateCertificate (This : access Typ;
                                       P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                       return access Java.Security.Cert.Certificate.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CertificateException.Except

   function EngineGenerateCertPath (This : access Typ;
                                    P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                    return access Java.Security.Cert.CertPath.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   function EngineGenerateCertPath (This : access Typ;
                                    P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Java.Security.Cert.CertPath.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   function EngineGenerateCertPath (This : access Typ;
                                    P1_List : access Standard.Java.Util.List.Typ'Class)
                                    return access Java.Security.Cert.CertPath.Typ'Class;
   --  can raise Java.Security.Cert.CertificateException.Except

   function EngineGetCertPathEncodings (This : access Typ)
                                        return access Java.Util.Iterator.Typ'Class;

   function EngineGenerateCertificates (This : access Typ;
                                        P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                        return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CertificateException.Except

   function EngineGenerateCRL (This : access Typ;
                               P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                               return access Java.Security.Cert.CRL.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CRLException.Except

   function EngineGenerateCRLs (This : access Typ;
                                P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CRLException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertificateFactorySpi);
   pragma Export (Java, EngineGenerateCertificate, "engineGenerateCertificate");
   pragma Export (Java, EngineGenerateCertPath, "engineGenerateCertPath");
   pragma Export (Java, EngineGetCertPathEncodings, "engineGetCertPathEncodings");
   pragma Export (Java, EngineGenerateCertificates, "engineGenerateCertificates");
   pragma Export (Java, EngineGenerateCRL, "engineGenerateCRL");
   pragma Export (Java, EngineGenerateCRLs, "engineGenerateCRLs");

end Java.Security.Cert.CertificateFactorySpi;
pragma Import (Java, Java.Security.Cert.CertificateFactorySpi, "java.security.cert.CertificateFactorySpi");
pragma Extensions_Allowed (Off);
