pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.Cert.CertPath is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_CertPath (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetEncodings (This : access Typ)
                          return access Java.Util.Iterator.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr is abstract;
   --  can raise Java.Security.Cert.CertificateEncodingException.Except

   function GetEncoded (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Byte_Arr is abstract;
   --  can raise Java.Security.Cert.CertificateEncodingException.Except

   function GetCertificates (This : access Typ)
                             return access Java.Util.List.Typ'Class is abstract;

   --  protected
   function WriteReplace (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.ObjectStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertPath);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetEncodings, "getEncodings");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, GetEncoded, "getEncoded");
   pragma Export (Java, GetCertificates, "getCertificates");
   pragma Export (Java, WriteReplace, "writeReplace");

end Java.Security.Cert.CertPath;
pragma Import (Java, Java.Security.Cert.CertPath, "java.security.cert.CertPath");
pragma Extensions_Allowed (Off);
