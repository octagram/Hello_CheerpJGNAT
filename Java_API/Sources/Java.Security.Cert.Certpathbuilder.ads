pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.CertPathBuilderResult;
limited with Java.Security.Cert.CertPathBuilderSpi;
limited with Java.Security.Cert.CertPathParameters;
limited with Java.Security.Provider;
with Java.Lang.Object;

package Java.Security.Cert.CertPathBuilder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_CertPathBuilder (P1_CertPathBuilderSpi : access Standard.Java.Security.Cert.CertPathBuilderSpi.Typ'Class;
                                 P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Cert.CertPathBuilder.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Cert.CertPathBuilder.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.Cert.CertPathBuilder.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function Build (This : access Typ;
                   P1_CertPathParameters : access Standard.Java.Security.Cert.CertPathParameters.Typ'Class)
                   return access Java.Security.Cert.CertPathBuilderResult.Typ'Class;
   --  can raise Java.Security.Cert.CertPathBuilderException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   function GetDefaultType return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertPathBuilder);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, Build, "build");
   pragma Import (Java, GetDefaultType, "getDefaultType");

end Java.Security.Cert.CertPathBuilder;
pragma Import (Java, Java.Security.Cert.CertPathBuilder, "java.security.cert.CertPathBuilder");
pragma Extensions_Allowed (Off);
