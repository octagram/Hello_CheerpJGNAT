pragma Extensions_Allowed (On);
limited with Java.Security.Cert.CertPathBuilderResult;
limited with Java.Security.Cert.CertPathParameters;
with Java.Lang.Object;

package Java.Security.Cert.CertPathBuilderSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CertPathBuilderSpi (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function EngineBuild (This : access Typ;
                         P1_CertPathParameters : access Standard.Java.Security.Cert.CertPathParameters.Typ'Class)
                         return access Java.Security.Cert.CertPathBuilderResult.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CertPathBuilderException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertPathBuilderSpi);
   pragma Export (Java, EngineBuild, "engineBuild");

end Java.Security.Cert.CertPathBuilderSpi;
pragma Import (Java, Java.Security.Cert.CertPathBuilderSpi, "java.security.cert.CertPathBuilderSpi");
pragma Extensions_Allowed (Off);
