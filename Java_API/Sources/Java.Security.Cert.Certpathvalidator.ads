pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.CertPath;
limited with Java.Security.Cert.CertPathParameters;
limited with Java.Security.Cert.CertPathValidatorResult;
limited with Java.Security.Cert.CertPathValidatorSpi;
limited with Java.Security.Provider;
with Java.Lang.Object;

package Java.Security.Cert.CertPathValidator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_CertPathValidator (P1_CertPathValidatorSpi : access Standard.Java.Security.Cert.CertPathValidatorSpi.Typ'Class;
                                   P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                                   P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Cert.CertPathValidator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Cert.CertPathValidator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.Cert.CertPathValidator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function Validate (This : access Typ;
                      P1_CertPath : access Standard.Java.Security.Cert.CertPath.Typ'Class;
                      P2_CertPathParameters : access Standard.Java.Security.Cert.CertPathParameters.Typ'Class)
                      return access Java.Security.Cert.CertPathValidatorResult.Typ'Class;
   --  can raise Java.Security.Cert.CertPathValidatorException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   function GetDefaultType return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertPathValidator);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, Validate, "validate");
   pragma Import (Java, GetDefaultType, "getDefaultType");

end Java.Security.Cert.CertPathValidator;
pragma Import (Java, Java.Security.Cert.CertPathValidator, "java.security.cert.CertPathValidator");
pragma Extensions_Allowed (Off);
