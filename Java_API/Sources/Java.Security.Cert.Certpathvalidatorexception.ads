pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Java.Security.Cert.CertPath;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.GeneralSecurityException;

package Java.Security.Cert.CertPathValidatorException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Security.GeneralSecurityException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CertPathValidatorException (This : Ref := null)
                                            return Ref;

   function New_CertPathValidatorException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_CertPathValidatorException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_CertPathValidatorException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                            P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_CertPathValidatorException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                            P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class;
                                            P3_CertPath : access Standard.Java.Security.Cert.CertPath.Typ'Class;
                                            P4_Int : Java.Int; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCertPath (This : access Typ)
                         return access Java.Security.Cert.CertPath.Typ'Class;

   function GetIndex (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.security.cert.CertPathValidatorException");
   pragma Java_Constructor (New_CertPathValidatorException);
   pragma Import (Java, GetCertPath, "getCertPath");
   pragma Import (Java, GetIndex, "getIndex");

end Java.Security.Cert.CertPathValidatorException;
pragma Import (Java, Java.Security.Cert.CertPathValidatorException, "java.security.cert.CertPathValidatorException");
pragma Extensions_Allowed (Off);
