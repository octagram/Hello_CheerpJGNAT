pragma Extensions_Allowed (On);
limited with Java.Security.Cert.CertPath;
limited with Java.Security.Cert.CertPathParameters;
limited with Java.Security.Cert.CertPathValidatorResult;
with Java.Lang.Object;

package Java.Security.Cert.CertPathValidatorSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CertPathValidatorSpi (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function EngineValidate (This : access Typ;
                            P1_CertPath : access Standard.Java.Security.Cert.CertPath.Typ'Class;
                            P2_CertPathParameters : access Standard.Java.Security.Cert.CertPathParameters.Typ'Class)
                            return access Java.Security.Cert.CertPathValidatorResult.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CertPathValidatorException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertPathValidatorSpi);
   pragma Export (Java, EngineValidate, "engineValidate");

end Java.Security.Cert.CertPathValidatorSpi;
pragma Import (Java, Java.Security.Cert.CertPathValidatorSpi, "java.security.cert.CertPathValidatorSpi");
pragma Extensions_Allowed (Off);
