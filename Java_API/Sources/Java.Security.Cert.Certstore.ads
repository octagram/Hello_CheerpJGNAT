pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.CRLSelector;
limited with Java.Security.Cert.CertSelector;
limited with Java.Security.Cert.CertStoreParameters;
limited with Java.Security.Cert.CertStoreSpi;
limited with Java.Security.Provider;
limited with Java.Util.Collection;
with Java.Lang.Object;

package Java.Security.Cert.CertStore is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_CertStore (P1_CertStoreSpi : access Standard.Java.Security.Cert.CertStoreSpi.Typ'Class;
                           P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_CertStoreParameters : access Standard.Java.Security.Cert.CertStoreParameters.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetCertificates (This : access Typ;
                             P1_CertSelector : access Standard.Java.Security.Cert.CertSelector.Typ'Class)
                             return access Java.Util.Collection.Typ'Class;
   --  can raise Java.Security.Cert.CertStoreException.Except

   --  final
   function GetCRLs (This : access Typ;
                     P1_CRLSelector : access Standard.Java.Security.Cert.CRLSelector.Typ'Class)
                     return access Java.Util.Collection.Typ'Class;
   --  can raise Java.Security.Cert.CertStoreException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_CertStoreParameters : access Standard.Java.Security.Cert.CertStoreParameters.Typ'Class)
                         return access Java.Security.Cert.CertStore.Typ'Class;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except and
   --  Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_CertStoreParameters : access Standard.Java.Security.Cert.CertStoreParameters.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Cert.CertStore.Typ'Class;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_CertStoreParameters : access Standard.Java.Security.Cert.CertStoreParameters.Typ'Class;
                         P3_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.Cert.CertStore.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   function GetCertStoreParameters (This : access Typ)
                                    return access Java.Security.Cert.CertStoreParameters.Typ'Class;

   --  final
   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   function GetDefaultType return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertStore);
   pragma Import (Java, GetCertificates, "getCertificates");
   pragma Import (Java, GetCRLs, "getCRLs");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetCertStoreParameters, "getCertStoreParameters");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetDefaultType, "getDefaultType");

end Java.Security.Cert.CertStore;
pragma Import (Java, Java.Security.Cert.CertStore, "java.security.cert.CertStore");
pragma Extensions_Allowed (Off);
