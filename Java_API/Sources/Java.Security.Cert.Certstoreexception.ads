pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.GeneralSecurityException;

package Java.Security.Cert.CertStoreException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Security.GeneralSecurityException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CertStoreException (This : Ref := null)
                                    return Ref;

   function New_CertStoreException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_CertStoreException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_CertStoreException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.security.cert.CertStoreException");
   pragma Java_Constructor (New_CertStoreException);

end Java.Security.Cert.CertStoreException;
pragma Import (Java, Java.Security.Cert.CertStoreException, "java.security.cert.CertStoreException");
pragma Extensions_Allowed (Off);
