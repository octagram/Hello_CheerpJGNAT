pragma Extensions_Allowed (On);
limited with Java.Security.Cert.CRLSelector;
limited with Java.Security.Cert.CertSelector;
limited with Java.Security.Cert.CertStoreParameters;
limited with Java.Util.Collection;
with Java.Lang.Object;

package Java.Security.Cert.CertStoreSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CertStoreSpi (P1_CertStoreParameters : access Standard.Java.Security.Cert.CertStoreParameters.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function EngineGetCertificates (This : access Typ;
                                   P1_CertSelector : access Standard.Java.Security.Cert.CertSelector.Typ'Class)
                                   return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CertStoreException.Except

   function EngineGetCRLs (This : access Typ;
                           P1_CRLSelector : access Standard.Java.Security.Cert.CRLSelector.Typ'Class)
                           return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Security.Cert.CertStoreException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CertStoreSpi);
   pragma Export (Java, EngineGetCertificates, "engineGetCertificates");
   pragma Export (Java, EngineGetCRLs, "engineGetCRLs");

end Java.Security.Cert.CertStoreSpi;
pragma Import (Java, Java.Security.Cert.CertStoreSpi, "java.security.cert.CertStoreSpi");
pragma Extensions_Allowed (Off);
