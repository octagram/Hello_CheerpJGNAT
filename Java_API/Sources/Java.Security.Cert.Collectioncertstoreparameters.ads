pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
with Java.Lang.Object;
with Java.Security.Cert.CertStoreParameters;

package Java.Security.Cert.CollectionCertStoreParameters is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CertStoreParameters_I : Java.Security.Cert.CertStoreParameters.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CollectionCertStoreParameters (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;

   function New_CollectionCertStoreParameters (This : Ref := null)
                                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCollection (This : access Typ)
                           return access Java.Util.Collection.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CollectionCertStoreParameters);
   pragma Import (Java, GetCollection, "getCollection");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.CollectionCertStoreParameters;
pragma Import (Java, Java.Security.Cert.CollectionCertStoreParameters, "java.security.cert.CollectionCertStoreParameters");
pragma Extensions_Allowed (Off);
