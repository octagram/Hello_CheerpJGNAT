pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.Certificate;
with Java.Lang.Object;

package Java.Security.Cert.CRL is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_CRL (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function IsRevoked (This : access Typ;
                       P1_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class)
                       return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CRL);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, IsRevoked, "isRevoked");

end Java.Security.Cert.CRL;
pragma Import (Java, Java.Security.Cert.CRL, "java.security.cert.CRL");
pragma Extensions_Allowed (Off);
