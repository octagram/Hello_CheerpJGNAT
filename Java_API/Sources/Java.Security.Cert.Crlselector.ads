pragma Extensions_Allowed (On);
limited with Java.Security.Cert.CRL;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Security.Cert.CRLSelector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Match (This : access Typ;
                   P1_CRL : access Standard.Java.Security.Cert.CRL.Typ'Class)
                   return Java.Boolean is abstract;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Match, "match");
   pragma Export (Java, Clone, "clone");

end Java.Security.Cert.CRLSelector;
pragma Import (Java, Java.Security.Cert.CRLSelector, "java.security.cert.CRLSelector");
pragma Extensions_Allowed (Off);
