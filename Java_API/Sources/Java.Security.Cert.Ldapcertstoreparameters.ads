pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Security.Cert.CertStoreParameters;

package Java.Security.Cert.LDAPCertStoreParameters is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CertStoreParameters_I : Java.Security.Cert.CertStoreParameters.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LDAPCertStoreParameters (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Int : Java.Int; 
                                         This : Ref := null)
                                         return Ref;

   function New_LDAPCertStoreParameters (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_LDAPCertStoreParameters (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetServerName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetPort (This : access Typ)
                     return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LDAPCertStoreParameters);
   pragma Import (Java, GetServerName, "getServerName");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.LDAPCertStoreParameters;
pragma Import (Java, Java.Security.Cert.LDAPCertStoreParameters, "java.security.cert.LDAPCertStoreParameters");
pragma Extensions_Allowed (Off);
