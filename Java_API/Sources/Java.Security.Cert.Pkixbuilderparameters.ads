pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.CertSelector;
limited with Java.Security.KeyStore;
limited with Java.Util.Set;
with Java.Lang.Object;
with Java.Security.Cert.CertPathParameters;
with Java.Security.Cert.PKIXParameters;

package Java.Security.Cert.PKIXBuilderParameters is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CertPathParameters_I : Java.Security.Cert.CertPathParameters.Ref)
    is new Java.Security.Cert.PKIXParameters.Typ(CertPathParameters_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PKIXBuilderParameters (P1_Set : access Standard.Java.Util.Set.Typ'Class;
                                       P2_CertSelector : access Standard.Java.Security.Cert.CertSelector.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   function New_PKIXBuilderParameters (P1_KeyStore : access Standard.Java.Security.KeyStore.Typ'Class;
                                       P2_CertSelector : access Standard.Java.Security.Cert.CertSelector.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;
   --  can raise Java.Security.KeyStoreException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMaxPathLength (This : access Typ;
                               P1_Int : Java.Int);

   function GetMaxPathLength (This : access Typ)
                              return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PKIXBuilderParameters);
   pragma Import (Java, SetMaxPathLength, "setMaxPathLength");
   pragma Import (Java, GetMaxPathLength, "getMaxPathLength");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.PKIXBuilderParameters;
pragma Import (Java, Java.Security.Cert.PKIXBuilderParameters, "java.security.cert.PKIXBuilderParameters");
pragma Extensions_Allowed (Off);
