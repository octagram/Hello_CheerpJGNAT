pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.CertPath;
limited with Java.Security.Cert.PolicyNode;
limited with Java.Security.Cert.TrustAnchor;
limited with Java.Security.PublicKey;
with Java.Lang.Object;
with Java.Security.Cert.CertPathBuilderResult;
with Java.Security.Cert.CertPathValidatorResult;
with Java.Security.Cert.PKIXCertPathValidatorResult;

package Java.Security.Cert.PKIXCertPathBuilderResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CertPathBuilderResult_I : Java.Security.Cert.CertPathBuilderResult.Ref;
            CertPathValidatorResult_I : Java.Security.Cert.CertPathValidatorResult.Ref)
    is new Java.Security.Cert.PKIXCertPathValidatorResult.Typ(CertPathValidatorResult_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PKIXCertPathBuilderResult (P1_CertPath : access Standard.Java.Security.Cert.CertPath.Typ'Class;
                                           P2_TrustAnchor : access Standard.Java.Security.Cert.TrustAnchor.Typ'Class;
                                           P3_PolicyNode : access Standard.Java.Security.Cert.PolicyNode.Typ'Class;
                                           P4_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCertPath (This : access Typ)
                         return access Java.Security.Cert.CertPath.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PKIXCertPathBuilderResult);
   pragma Import (Java, GetCertPath, "getCertPath");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.PKIXCertPathBuilderResult;
pragma Import (Java, Java.Security.Cert.PKIXCertPathBuilderResult, "java.security.cert.PKIXCertPathBuilderResult");
pragma Extensions_Allowed (Off);
