pragma Extensions_Allowed (On);
limited with Java.Security.Cert.Certificate;
limited with Java.Util.Collection;
limited with Java.Util.Set;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Security.Cert.PKIXCertPathChecker is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_PKIXCertPathChecker (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Init (This : access Typ;
                   P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Security.Cert.CertPathValidatorException.Except

   function IsForwardCheckingSupported (This : access Typ)
                                        return Java.Boolean is abstract;

   function GetSupportedExtensions (This : access Typ)
                                    return access Java.Util.Set.Typ'Class is abstract;

   procedure Check (This : access Typ;
                    P1_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class;
                    P2_Collection : access Standard.Java.Util.Collection.Typ'Class) is abstract;
   --  can raise Java.Security.Cert.CertPathValidatorException.Except

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PKIXCertPathChecker);
   pragma Export (Java, Init, "init");
   pragma Export (Java, IsForwardCheckingSupported, "isForwardCheckingSupported");
   pragma Export (Java, GetSupportedExtensions, "getSupportedExtensions");
   pragma Export (Java, Check, "check");
   pragma Export (Java, Clone, "clone");

end Java.Security.Cert.PKIXCertPathChecker;
pragma Import (Java, Java.Security.Cert.PKIXCertPathChecker, "java.security.cert.PKIXCertPathChecker");
pragma Extensions_Allowed (Off);
