pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.PolicyNode;
limited with Java.Security.Cert.TrustAnchor;
limited with Java.Security.PublicKey;
with Java.Lang.Object;
with Java.Security.Cert.CertPathValidatorResult;

package Java.Security.Cert.PKIXCertPathValidatorResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CertPathValidatorResult_I : Java.Security.Cert.CertPathValidatorResult.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PKIXCertPathValidatorResult (P1_TrustAnchor : access Standard.Java.Security.Cert.TrustAnchor.Typ'Class;
                                             P2_PolicyNode : access Standard.Java.Security.Cert.PolicyNode.Typ'Class;
                                             P3_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTrustAnchor (This : access Typ)
                            return access Java.Security.Cert.TrustAnchor.Typ'Class;

   function GetPolicyTree (This : access Typ)
                           return access Java.Security.Cert.PolicyNode.Typ'Class;

   function GetPublicKey (This : access Typ)
                          return access Java.Security.PublicKey.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PKIXCertPathValidatorResult);
   pragma Import (Java, GetTrustAnchor, "getTrustAnchor");
   pragma Import (Java, GetPolicyTree, "getPolicyTree");
   pragma Import (Java, GetPublicKey, "getPublicKey");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.PKIXCertPathValidatorResult;
pragma Import (Java, Java.Security.Cert.PKIXCertPathValidatorResult, "java.security.cert.PKIXCertPathValidatorResult");
pragma Extensions_Allowed (Off);
