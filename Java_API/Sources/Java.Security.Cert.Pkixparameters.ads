pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.CertSelector;
limited with Java.Security.Cert.CertStore;
limited with Java.Security.Cert.PKIXCertPathChecker;
limited with Java.Security.KeyStore;
limited with Java.Util.Date;
limited with Java.Util.List;
limited with Java.Util.Set;
with Java.Lang.Object;
with Java.Security.Cert.CertPathParameters;

package Java.Security.Cert.PKIXParameters is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CertPathParameters_I : Java.Security.Cert.CertPathParameters.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PKIXParameters (P1_Set : access Standard.Java.Util.Set.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   function New_PKIXParameters (P1_KeyStore : access Standard.Java.Security.KeyStore.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Security.KeyStoreException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTrustAnchors (This : access Typ)
                             return access Java.Util.Set.Typ'Class;

   procedure SetTrustAnchors (This : access Typ;
                              P1_Set : access Standard.Java.Util.Set.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   function GetInitialPolicies (This : access Typ)
                                return access Java.Util.Set.Typ'Class;

   procedure SetInitialPolicies (This : access Typ;
                                 P1_Set : access Standard.Java.Util.Set.Typ'Class);

   procedure SetCertStores (This : access Typ;
                            P1_List : access Standard.Java.Util.List.Typ'Class);

   procedure AddCertStore (This : access Typ;
                           P1_CertStore : access Standard.Java.Security.Cert.CertStore.Typ'Class);

   function GetCertStores (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   procedure SetRevocationEnabled (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function IsRevocationEnabled (This : access Typ)
                                 return Java.Boolean;

   procedure SetExplicitPolicyRequired (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function IsExplicitPolicyRequired (This : access Typ)
                                      return Java.Boolean;

   procedure SetPolicyMappingInhibited (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function IsPolicyMappingInhibited (This : access Typ)
                                      return Java.Boolean;

   procedure SetAnyPolicyInhibited (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function IsAnyPolicyInhibited (This : access Typ)
                                  return Java.Boolean;

   procedure SetPolicyQualifiersRejected (This : access Typ;
                                          P1_Boolean : Java.Boolean);

   function GetPolicyQualifiersRejected (This : access Typ)
                                         return Java.Boolean;

   function GetDate (This : access Typ)
                     return access Java.Util.Date.Typ'Class;

   procedure SetDate (This : access Typ;
                      P1_Date : access Standard.Java.Util.Date.Typ'Class);

   procedure SetCertPathCheckers (This : access Typ;
                                  P1_List : access Standard.Java.Util.List.Typ'Class);

   function GetCertPathCheckers (This : access Typ)
                                 return access Java.Util.List.Typ'Class;

   procedure AddCertPathChecker (This : access Typ;
                                 P1_PKIXCertPathChecker : access Standard.Java.Security.Cert.PKIXCertPathChecker.Typ'Class);

   function GetSigProvider (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetSigProvider (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetTargetCertConstraints (This : access Typ)
                                      return access Java.Security.Cert.CertSelector.Typ'Class;

   procedure SetTargetCertConstraints (This : access Typ;
                                       P1_CertSelector : access Standard.Java.Security.Cert.CertSelector.Typ'Class);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PKIXParameters);
   pragma Import (Java, GetTrustAnchors, "getTrustAnchors");
   pragma Import (Java, SetTrustAnchors, "setTrustAnchors");
   pragma Import (Java, GetInitialPolicies, "getInitialPolicies");
   pragma Import (Java, SetInitialPolicies, "setInitialPolicies");
   pragma Import (Java, SetCertStores, "setCertStores");
   pragma Import (Java, AddCertStore, "addCertStore");
   pragma Import (Java, GetCertStores, "getCertStores");
   pragma Import (Java, SetRevocationEnabled, "setRevocationEnabled");
   pragma Import (Java, IsRevocationEnabled, "isRevocationEnabled");
   pragma Import (Java, SetExplicitPolicyRequired, "setExplicitPolicyRequired");
   pragma Import (Java, IsExplicitPolicyRequired, "isExplicitPolicyRequired");
   pragma Import (Java, SetPolicyMappingInhibited, "setPolicyMappingInhibited");
   pragma Import (Java, IsPolicyMappingInhibited, "isPolicyMappingInhibited");
   pragma Import (Java, SetAnyPolicyInhibited, "setAnyPolicyInhibited");
   pragma Import (Java, IsAnyPolicyInhibited, "isAnyPolicyInhibited");
   pragma Import (Java, SetPolicyQualifiersRejected, "setPolicyQualifiersRejected");
   pragma Import (Java, GetPolicyQualifiersRejected, "getPolicyQualifiersRejected");
   pragma Import (Java, GetDate, "getDate");
   pragma Import (Java, SetDate, "setDate");
   pragma Import (Java, SetCertPathCheckers, "setCertPathCheckers");
   pragma Import (Java, GetCertPathCheckers, "getCertPathCheckers");
   pragma Import (Java, AddCertPathChecker, "addCertPathChecker");
   pragma Import (Java, GetSigProvider, "getSigProvider");
   pragma Import (Java, SetSigProvider, "setSigProvider");
   pragma Import (Java, GetTargetCertConstraints, "getTargetCertConstraints");
   pragma Import (Java, SetTargetCertConstraints, "setTargetCertConstraints");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.PKIXParameters;
pragma Import (Java, Java.Security.Cert.PKIXParameters, "java.security.cert.PKIXParameters");
pragma Extensions_Allowed (Off);
