pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Java.Util.Set;
with Java.Lang.Object;

package Java.Security.Cert.PolicyNode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParent (This : access Typ)
                       return access Java.Security.Cert.PolicyNode.Typ'Class is abstract;

   function GetChildren (This : access Typ)
                         return access Java.Util.Iterator.Typ'Class is abstract;

   function GetDepth (This : access Typ)
                      return Java.Int is abstract;

   function GetValidPolicy (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetPolicyQualifiers (This : access Typ)
                                 return access Java.Util.Set.Typ'Class is abstract;

   function GetExpectedPolicies (This : access Typ)
                                 return access Java.Util.Set.Typ'Class is abstract;

   function IsCritical (This : access Typ)
                        return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParent, "getParent");
   pragma Export (Java, GetChildren, "getChildren");
   pragma Export (Java, GetDepth, "getDepth");
   pragma Export (Java, GetValidPolicy, "getValidPolicy");
   pragma Export (Java, GetPolicyQualifiers, "getPolicyQualifiers");
   pragma Export (Java, GetExpectedPolicies, "getExpectedPolicies");
   pragma Export (Java, IsCritical, "isCritical");

end Java.Security.Cert.PolicyNode;
pragma Import (Java, Java.Security.Cert.PolicyNode, "java.security.cert.PolicyNode");
pragma Extensions_Allowed (Off);
