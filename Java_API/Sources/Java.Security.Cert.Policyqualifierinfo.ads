pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Security.Cert.PolicyQualifierInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PolicyQualifierInfo (P1_Byte_Arr : Java.Byte_Arr; 
                                     This : Ref := null)
                                     return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetPolicyQualifierId (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   --  final
   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr;

   --  final
   function GetPolicyQualifier (This : access Typ)
                                return Java.Byte_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PolicyQualifierInfo);
   pragma Import (Java, GetPolicyQualifierId, "getPolicyQualifierId");
   pragma Import (Java, GetEncoded, "getEncoded");
   pragma Import (Java, GetPolicyQualifier, "getPolicyQualifier");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.PolicyQualifierInfo;
pragma Import (Java, Java.Security.Cert.PolicyQualifierInfo, "java.security.cert.PolicyQualifierInfo");
pragma Extensions_Allowed (Off);
