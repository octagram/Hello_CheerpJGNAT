pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.X509Certificate;
limited with Java.Security.PublicKey;
limited with Javax.Security.Auth.X500.X500Principal;
with Java.Lang.Object;

package Java.Security.Cert.TrustAnchor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TrustAnchor (P1_X509Certificate : access Standard.Java.Security.Cert.X509Certificate.Typ'Class;
                             P2_Byte_Arr : Java.Byte_Arr; 
                             This : Ref := null)
                             return Ref;

   function New_TrustAnchor (P1_X500Principal : access Standard.Javax.Security.Auth.X500.X500Principal.Typ'Class;
                             P2_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class;
                             P3_Byte_Arr : Java.Byte_Arr; 
                             This : Ref := null)
                             return Ref;

   function New_TrustAnchor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class;
                             P3_Byte_Arr : Java.Byte_Arr; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetTrustedCert (This : access Typ)
                            return access Java.Security.Cert.X509Certificate.Typ'Class;

   --  final
   function GetCA (This : access Typ)
                   return access Javax.Security.Auth.X500.X500Principal.Typ'Class;

   --  final
   function GetCAName (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   --  final
   function GetCAPublicKey (This : access Typ)
                            return access Java.Security.PublicKey.Typ'Class;

   --  final
   function GetNameConstraints (This : access Typ)
                                return Java.Byte_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TrustAnchor);
   pragma Import (Java, GetTrustedCert, "getTrustedCert");
   pragma Import (Java, GetCA, "getCA");
   pragma Import (Java, GetCAName, "getCAName");
   pragma Import (Java, GetCAPublicKey, "getCAPublicKey");
   pragma Import (Java, GetNameConstraints, "getNameConstraints");
   pragma Import (Java, ToString, "toString");

end Java.Security.Cert.TrustAnchor;
pragma Import (Java, Java.Security.Cert.TrustAnchor, "java.security.cert.TrustAnchor");
pragma Extensions_Allowed (Off);
