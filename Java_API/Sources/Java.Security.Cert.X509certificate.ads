pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Security.Principal;
limited with Java.Util.Collection;
limited with Java.Util.Date;
limited with Java.Util.List;
limited with Javax.Security.Auth.X500.X500Principal;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Cert.Certificate;
with Java.Security.Cert.X509Extension;

package Java.Security.Cert.X509Certificate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            X509Extension_I : Java.Security.Cert.X509Extension.Ref)
    is abstract new Java.Security.Cert.Certificate.Typ(Serializable_I)
      with null record;

   --  protected
   function New_X509Certificate (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure CheckValidity (This : access Typ) is abstract;
   --  can raise Java.Security.Cert.CertificateExpiredException.Except and
   --  Java.Security.Cert.CertificateNotYetValidException.Except

   procedure CheckValidity (This : access Typ;
                            P1_Date : access Standard.Java.Util.Date.Typ'Class) is abstract;
   --  can raise Java.Security.Cert.CertificateExpiredException.Except and
   --  Java.Security.Cert.CertificateNotYetValidException.Except

   function GetVersion (This : access Typ)
                        return Java.Int is abstract;

   function GetSerialNumber (This : access Typ)
                             return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetIssuerDN (This : access Typ)
                         return access Java.Security.Principal.Typ'Class is abstract;

   function GetIssuerX500Principal (This : access Typ)
                                    return access Javax.Security.Auth.X500.X500Principal.Typ'Class;

   function GetSubjectDN (This : access Typ)
                          return access Java.Security.Principal.Typ'Class is abstract;

   function GetSubjectX500Principal (This : access Typ)
                                     return access Javax.Security.Auth.X500.X500Principal.Typ'Class;

   function GetNotBefore (This : access Typ)
                          return access Java.Util.Date.Typ'Class is abstract;

   function GetNotAfter (This : access Typ)
                         return access Java.Util.Date.Typ'Class is abstract;

   function GetTBSCertificate (This : access Typ)
                               return Java.Byte_Arr is abstract;
   --  can raise Java.Security.Cert.CertificateEncodingException.Except

   function GetSignature (This : access Typ)
                          return Java.Byte_Arr is abstract;

   function GetSigAlgName (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetSigAlgOID (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetSigAlgParams (This : access Typ)
                             return Java.Byte_Arr is abstract;

   function GetIssuerUniqueID (This : access Typ)
                               return Java.Boolean_Arr is abstract;

   function GetSubjectUniqueID (This : access Typ)
                                return Java.Boolean_Arr is abstract;

   function GetKeyUsage (This : access Typ)
                         return Java.Boolean_Arr is abstract;

   function GetExtendedKeyUsage (This : access Typ)
                                 return access Java.Util.List.Typ'Class;
   --  can raise Java.Security.Cert.CertificateParsingException.Except

   function GetBasicConstraints (This : access Typ)
                                 return Java.Int is abstract;

   function GetSubjectAlternativeNames (This : access Typ)
                                        return access Java.Util.Collection.Typ'Class;
   --  can raise Java.Security.Cert.CertificateParsingException.Except

   function GetIssuerAlternativeNames (This : access Typ)
                                       return access Java.Util.Collection.Typ'Class;
   --  can raise Java.Security.Cert.CertificateParsingException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X509Certificate);
   pragma Export (Java, CheckValidity, "checkValidity");
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, GetSerialNumber, "getSerialNumber");
   pragma Export (Java, GetIssuerDN, "getIssuerDN");
   pragma Import (Java, GetIssuerX500Principal, "getIssuerX500Principal");
   pragma Export (Java, GetSubjectDN, "getSubjectDN");
   pragma Import (Java, GetSubjectX500Principal, "getSubjectX500Principal");
   pragma Export (Java, GetNotBefore, "getNotBefore");
   pragma Export (Java, GetNotAfter, "getNotAfter");
   pragma Export (Java, GetTBSCertificate, "getTBSCertificate");
   pragma Export (Java, GetSignature, "getSignature");
   pragma Export (Java, GetSigAlgName, "getSigAlgName");
   pragma Export (Java, GetSigAlgOID, "getSigAlgOID");
   pragma Export (Java, GetSigAlgParams, "getSigAlgParams");
   pragma Export (Java, GetIssuerUniqueID, "getIssuerUniqueID");
   pragma Export (Java, GetSubjectUniqueID, "getSubjectUniqueID");
   pragma Export (Java, GetKeyUsage, "getKeyUsage");
   pragma Import (Java, GetExtendedKeyUsage, "getExtendedKeyUsage");
   pragma Export (Java, GetBasicConstraints, "getBasicConstraints");
   pragma Import (Java, GetSubjectAlternativeNames, "getSubjectAlternativeNames");
   pragma Import (Java, GetIssuerAlternativeNames, "getIssuerAlternativeNames");

end Java.Security.Cert.X509Certificate;
pragma Import (Java, Java.Security.Cert.X509Certificate, "java.security.cert.X509Certificate");
pragma Extensions_Allowed (Off);
