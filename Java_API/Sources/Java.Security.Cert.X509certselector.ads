pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Security.Cert.Certificate;
limited with Java.Security.Cert.X509Certificate;
limited with Java.Security.PublicKey;
limited with Java.Util.Collection;
limited with Java.Util.Date;
limited with Java.Util.Set;
limited with Javax.Security.Auth.X500.X500Principal;
with Java.Lang.Object;
with Java.Security.Cert.CertSelector;

package Java.Security.Cert.X509CertSelector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CertSelector_I : Java.Security.Cert.CertSelector.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_X509CertSelector (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetCertificate (This : access Typ;
                             P1_X509Certificate : access Standard.Java.Security.Cert.X509Certificate.Typ'Class);

   procedure SetSerialNumber (This : access Typ;
                              P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class);

   procedure SetIssuer (This : access Typ;
                        P1_X500Principal : access Standard.Javax.Security.Auth.X500.X500Principal.Typ'Class);

   procedure SetIssuer (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure SetIssuer (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure SetSubject (This : access Typ;
                         P1_X500Principal : access Standard.Javax.Security.Auth.X500.X500Principal.Typ'Class);

   procedure SetSubject (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure SetSubject (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure SetSubjectKeyIdentifier (This : access Typ;
                                      P1_Byte_Arr : Java.Byte_Arr);

   procedure SetAuthorityKeyIdentifier (This : access Typ;
                                        P1_Byte_Arr : Java.Byte_Arr);

   procedure SetCertificateValid (This : access Typ;
                                  P1_Date : access Standard.Java.Util.Date.Typ'Class);

   procedure SetPrivateKeyValid (This : access Typ;
                                 P1_Date : access Standard.Java.Util.Date.Typ'Class);

   procedure SetSubjectPublicKeyAlgID (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure SetSubjectPublicKey (This : access Typ;
                                  P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class);

   procedure SetSubjectPublicKey (This : access Typ;
                                  P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure SetKeyUsage (This : access Typ;
                          P1_Boolean_Arr : Java.Boolean_Arr);

   procedure SetExtendedKeyUsage (This : access Typ;
                                  P1_Set : access Standard.Java.Util.Set.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure SetMatchAllSubjectAltNames (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   procedure SetSubjectAlternativeNames (This : access Typ;
                                         P1_Collection : access Standard.Java.Util.Collection.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure AddSubjectAlternativeName (This : access Typ;
                                        P1_Int : Java.Int;
                                        P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure AddSubjectAlternativeName (This : access Typ;
                                        P1_Int : Java.Int;
                                        P2_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure SetNameConstraints (This : access Typ;
                                 P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure SetBasicConstraints (This : access Typ;
                                  P1_Int : Java.Int);

   procedure SetPolicy (This : access Typ;
                        P1_Set : access Standard.Java.Util.Set.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure SetPathToNames (This : access Typ;
                             P1_Collection : access Standard.Java.Util.Collection.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure AddPathToName (This : access Typ;
                            P1_Int : Java.Int;
                            P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure AddPathToName (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   function GetCertificate (This : access Typ)
                            return access Java.Security.Cert.X509Certificate.Typ'Class;

   function GetSerialNumber (This : access Typ)
                             return access Java.Math.BigInteger.Typ'Class;

   function GetIssuer (This : access Typ)
                       return access Javax.Security.Auth.X500.X500Principal.Typ'Class;

   function GetIssuerAsString (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   function GetIssuerAsBytes (This : access Typ)
                              return Java.Byte_Arr;
   --  can raise Java.Io.IOException.Except

   function GetSubject (This : access Typ)
                        return access Javax.Security.Auth.X500.X500Principal.Typ'Class;

   function GetSubjectAsString (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function GetSubjectAsBytes (This : access Typ)
                               return Java.Byte_Arr;
   --  can raise Java.Io.IOException.Except

   function GetSubjectKeyIdentifier (This : access Typ)
                                     return Java.Byte_Arr;

   function GetAuthorityKeyIdentifier (This : access Typ)
                                       return Java.Byte_Arr;

   function GetCertificateValid (This : access Typ)
                                 return access Java.Util.Date.Typ'Class;

   function GetPrivateKeyValid (This : access Typ)
                                return access Java.Util.Date.Typ'Class;

   function GetSubjectPublicKeyAlgID (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetSubjectPublicKey (This : access Typ)
                                 return access Java.Security.PublicKey.Typ'Class;

   function GetKeyUsage (This : access Typ)
                         return Java.Boolean_Arr;

   function GetExtendedKeyUsage (This : access Typ)
                                 return access Java.Util.Set.Typ'Class;

   function GetMatchAllSubjectAltNames (This : access Typ)
                                        return Java.Boolean;

   function GetSubjectAlternativeNames (This : access Typ)
                                        return access Java.Util.Collection.Typ'Class;

   function GetNameConstraints (This : access Typ)
                                return Java.Byte_Arr;

   function GetBasicConstraints (This : access Typ)
                                 return Java.Int;

   function GetPolicy (This : access Typ)
                       return access Java.Util.Set.Typ'Class;

   function GetPathToNames (This : access Typ)
                            return access Java.Util.Collection.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Match (This : access Typ;
                   P1_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class)
                   return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X509CertSelector);
   pragma Import (Java, SetCertificate, "setCertificate");
   pragma Import (Java, SetSerialNumber, "setSerialNumber");
   pragma Import (Java, SetIssuer, "setIssuer");
   pragma Import (Java, SetSubject, "setSubject");
   pragma Import (Java, SetSubjectKeyIdentifier, "setSubjectKeyIdentifier");
   pragma Import (Java, SetAuthorityKeyIdentifier, "setAuthorityKeyIdentifier");
   pragma Import (Java, SetCertificateValid, "setCertificateValid");
   pragma Import (Java, SetPrivateKeyValid, "setPrivateKeyValid");
   pragma Import (Java, SetSubjectPublicKeyAlgID, "setSubjectPublicKeyAlgID");
   pragma Import (Java, SetSubjectPublicKey, "setSubjectPublicKey");
   pragma Import (Java, SetKeyUsage, "setKeyUsage");
   pragma Import (Java, SetExtendedKeyUsage, "setExtendedKeyUsage");
   pragma Import (Java, SetMatchAllSubjectAltNames, "setMatchAllSubjectAltNames");
   pragma Import (Java, SetSubjectAlternativeNames, "setSubjectAlternativeNames");
   pragma Import (Java, AddSubjectAlternativeName, "addSubjectAlternativeName");
   pragma Import (Java, SetNameConstraints, "setNameConstraints");
   pragma Import (Java, SetBasicConstraints, "setBasicConstraints");
   pragma Import (Java, SetPolicy, "setPolicy");
   pragma Import (Java, SetPathToNames, "setPathToNames");
   pragma Import (Java, AddPathToName, "addPathToName");
   pragma Import (Java, GetCertificate, "getCertificate");
   pragma Import (Java, GetSerialNumber, "getSerialNumber");
   pragma Import (Java, GetIssuer, "getIssuer");
   pragma Import (Java, GetIssuerAsString, "getIssuerAsString");
   pragma Import (Java, GetIssuerAsBytes, "getIssuerAsBytes");
   pragma Import (Java, GetSubject, "getSubject");
   pragma Import (Java, GetSubjectAsString, "getSubjectAsString");
   pragma Import (Java, GetSubjectAsBytes, "getSubjectAsBytes");
   pragma Import (Java, GetSubjectKeyIdentifier, "getSubjectKeyIdentifier");
   pragma Import (Java, GetAuthorityKeyIdentifier, "getAuthorityKeyIdentifier");
   pragma Import (Java, GetCertificateValid, "getCertificateValid");
   pragma Import (Java, GetPrivateKeyValid, "getPrivateKeyValid");
   pragma Import (Java, GetSubjectPublicKeyAlgID, "getSubjectPublicKeyAlgID");
   pragma Import (Java, GetSubjectPublicKey, "getSubjectPublicKey");
   pragma Import (Java, GetKeyUsage, "getKeyUsage");
   pragma Import (Java, GetExtendedKeyUsage, "getExtendedKeyUsage");
   pragma Import (Java, GetMatchAllSubjectAltNames, "getMatchAllSubjectAltNames");
   pragma Import (Java, GetSubjectAlternativeNames, "getSubjectAlternativeNames");
   pragma Import (Java, GetNameConstraints, "getNameConstraints");
   pragma Import (Java, GetBasicConstraints, "getBasicConstraints");
   pragma Import (Java, GetPolicy, "getPolicy");
   pragma Import (Java, GetPathToNames, "getPathToNames");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Match, "match");
   pragma Import (Java, Clone, "clone");

end Java.Security.Cert.X509CertSelector;
pragma Import (Java, Java.Security.Cert.X509CertSelector, "java.security.cert.X509CertSelector");
pragma Extensions_Allowed (Off);
