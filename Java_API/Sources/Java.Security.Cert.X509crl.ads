pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Security.Cert.X509CRLEntry;
limited with Java.Security.Cert.X509Certificate;
limited with Java.Security.Principal;
limited with Java.Security.PublicKey;
limited with Java.Util.Date;
limited with Java.Util.Set;
limited with Javax.Security.Auth.X500.X500Principal;
with Java.Lang.Object;
with Java.Security.Cert.CRL;
with Java.Security.Cert.X509Extension;

package Java.Security.Cert.X509CRL is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(X509Extension_I : Java.Security.Cert.X509Extension.Ref)
    is abstract new Java.Security.Cert.CRL.Typ
      with null record;

   --  protected
   function New_X509CRL (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr is abstract;
   --  can raise Java.Security.Cert.CRLException.Except

   procedure Verify (This : access Typ;
                     P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class) is abstract;
   --  can raise Java.Security.Cert.CRLException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except,
   --  Java.Security.InvalidKeyException.Except,
   --  Java.Security.NoSuchProviderException.Except and
   --  Java.Security.SignatureException.Except

   procedure Verify (This : access Typ;
                     P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Security.Cert.CRLException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except,
   --  Java.Security.InvalidKeyException.Except,
   --  Java.Security.NoSuchProviderException.Except and
   --  Java.Security.SignatureException.Except

   function GetVersion (This : access Typ)
                        return Java.Int is abstract;

   function GetIssuerDN (This : access Typ)
                         return access Java.Security.Principal.Typ'Class is abstract;

   function GetIssuerX500Principal (This : access Typ)
                                    return access Javax.Security.Auth.X500.X500Principal.Typ'Class;

   function GetThisUpdate (This : access Typ)
                           return access Java.Util.Date.Typ'Class is abstract;

   function GetNextUpdate (This : access Typ)
                           return access Java.Util.Date.Typ'Class is abstract;

   function GetRevokedCertificate (This : access Typ;
                                   P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                                   return access Java.Security.Cert.X509CRLEntry.Typ'Class is abstract;

   function GetRevokedCertificate (This : access Typ;
                                   P1_X509Certificate : access Standard.Java.Security.Cert.X509Certificate.Typ'Class)
                                   return access Java.Security.Cert.X509CRLEntry.Typ'Class;

   function GetRevokedCertificates (This : access Typ)
                                    return access Java.Util.Set.Typ'Class is abstract;

   function GetTBSCertList (This : access Typ)
                            return Java.Byte_Arr is abstract;
   --  can raise Java.Security.Cert.CRLException.Except

   function GetSignature (This : access Typ)
                          return Java.Byte_Arr is abstract;

   function GetSigAlgName (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetSigAlgOID (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetSigAlgParams (This : access Typ)
                             return Java.Byte_Arr is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X509CRL);
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, GetEncoded, "getEncoded");
   pragma Export (Java, Verify, "verify");
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, GetIssuerDN, "getIssuerDN");
   pragma Export (Java, GetIssuerX500Principal, "getIssuerX500Principal");
   pragma Export (Java, GetThisUpdate, "getThisUpdate");
   pragma Export (Java, GetNextUpdate, "getNextUpdate");
   pragma Export (Java, GetRevokedCertificate, "getRevokedCertificate");
   pragma Export (Java, GetRevokedCertificates, "getRevokedCertificates");
   pragma Export (Java, GetTBSCertList, "getTBSCertList");
   pragma Export (Java, GetSignature, "getSignature");
   pragma Export (Java, GetSigAlgName, "getSigAlgName");
   pragma Export (Java, GetSigAlgOID, "getSigAlgOID");
   pragma Export (Java, GetSigAlgParams, "getSigAlgParams");

end Java.Security.Cert.X509CRL;
pragma Import (Java, Java.Security.Cert.X509CRL, "java.security.cert.X509CRL");
pragma Extensions_Allowed (Off);
