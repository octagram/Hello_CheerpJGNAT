pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Util.Date;
limited with Javax.Security.Auth.X500.X500Principal;
with Java.Lang.Object;
with Java.Security.Cert.X509Extension;

package Java.Security.Cert.X509CRLEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(X509Extension_I : Java.Security.Cert.X509Extension.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_X509CRLEntry (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr is abstract;
   --  can raise Java.Security.Cert.CRLException.Except

   function GetSerialNumber (This : access Typ)
                             return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetCertificateIssuer (This : access Typ)
                                  return access Javax.Security.Auth.X500.X500Principal.Typ'Class;

   function GetRevocationDate (This : access Typ)
                               return access Java.Util.Date.Typ'Class is abstract;

   function HasExtensions (This : access Typ)
                           return Java.Boolean is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X509CRLEntry);
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, GetEncoded, "getEncoded");
   pragma Export (Java, GetSerialNumber, "getSerialNumber");
   pragma Export (Java, GetCertificateIssuer, "getCertificateIssuer");
   pragma Export (Java, GetRevocationDate, "getRevocationDate");
   pragma Export (Java, HasExtensions, "hasExtensions");
   pragma Export (Java, ToString, "toString");

end Java.Security.Cert.X509CRLEntry;
pragma Import (Java, Java.Security.Cert.X509CRLEntry, "java.security.cert.X509CRLEntry");
pragma Extensions_Allowed (Off);
