pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Security.Cert.CRL;
limited with Java.Security.Cert.X509Certificate;
limited with Java.Util.Collection;
limited with Java.Util.Date;
limited with Javax.Security.Auth.X500.X500Principal;
with Java.Lang.Object;
with Java.Security.Cert.CRLSelector;

package Java.Security.Cert.X509CRLSelector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CRLSelector_I : Java.Security.Cert.CRLSelector.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_X509CRLSelector (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetIssuers (This : access Typ;
                         P1_Collection : access Standard.Java.Util.Collection.Typ'Class);

   procedure SetIssuerNames (This : access Typ;
                             P1_Collection : access Standard.Java.Util.Collection.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure AddIssuer (This : access Typ;
                        P1_X500Principal : access Standard.Javax.Security.Auth.X500.X500Principal.Typ'Class);

   procedure AddIssuerName (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure AddIssuerName (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure SetMinCRLNumber (This : access Typ;
                              P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class);

   procedure SetMaxCRLNumber (This : access Typ;
                              P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class);

   procedure SetDateAndTime (This : access Typ;
                             P1_Date : access Standard.Java.Util.Date.Typ'Class);

   procedure SetCertificateChecking (This : access Typ;
                                     P1_X509Certificate : access Standard.Java.Security.Cert.X509Certificate.Typ'Class);

   function GetIssuers (This : access Typ)
                        return access Java.Util.Collection.Typ'Class;

   function GetIssuerNames (This : access Typ)
                            return access Java.Util.Collection.Typ'Class;

   function GetMinCRL (This : access Typ)
                       return access Java.Math.BigInteger.Typ'Class;

   function GetMaxCRL (This : access Typ)
                       return access Java.Math.BigInteger.Typ'Class;

   function GetDateAndTime (This : access Typ)
                            return access Java.Util.Date.Typ'Class;

   function GetCertificateChecking (This : access Typ)
                                    return access Java.Security.Cert.X509Certificate.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Match (This : access Typ;
                   P1_CRL : access Standard.Java.Security.Cert.CRL.Typ'Class)
                   return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X509CRLSelector);
   pragma Import (Java, SetIssuers, "setIssuers");
   pragma Import (Java, SetIssuerNames, "setIssuerNames");
   pragma Import (Java, AddIssuer, "addIssuer");
   pragma Import (Java, AddIssuerName, "addIssuerName");
   pragma Import (Java, SetMinCRLNumber, "setMinCRLNumber");
   pragma Import (Java, SetMaxCRLNumber, "setMaxCRLNumber");
   pragma Import (Java, SetDateAndTime, "setDateAndTime");
   pragma Import (Java, SetCertificateChecking, "setCertificateChecking");
   pragma Import (Java, GetIssuers, "getIssuers");
   pragma Import (Java, GetIssuerNames, "getIssuerNames");
   pragma Import (Java, GetMinCRL, "getMinCRL");
   pragma Import (Java, GetMaxCRL, "getMaxCRL");
   pragma Import (Java, GetDateAndTime, "getDateAndTime");
   pragma Import (Java, GetCertificateChecking, "getCertificateChecking");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Match, "match");
   pragma Import (Java, Clone, "clone");

end Java.Security.Cert.X509CRLSelector;
pragma Import (Java, Java.Security.Cert.X509CRLSelector, "java.security.cert.X509CRLSelector");
pragma Extensions_Allowed (Off);
