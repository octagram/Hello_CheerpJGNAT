pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Security.Principal;
limited with Java.Security.PublicKey;
with Java.Lang.Object;

package Java.Security.Certificate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetGuarantor (This : access Typ)
                          return access Java.Security.Principal.Typ'Class is abstract;

   function GetPrincipal (This : access Typ)
                          return access Java.Security.Principal.Typ'Class is abstract;

   function GetPublicKey (This : access Typ)
                          return access Java.Security.PublicKey.Typ'Class is abstract;

   procedure Encode (This : access Typ;
                     P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Security.KeyException.Except and
   --  Java.Io.IOException.Except

   procedure Decode (This : access Typ;
                     P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Security.KeyException.Except and
   --  Java.Io.IOException.Except

   function GetFormat (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function ToString (This : access Typ;
                      P1_Boolean : Java.Boolean)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetGuarantor, "getGuarantor");
   pragma Export (Java, GetPrincipal, "getPrincipal");
   pragma Export (Java, GetPublicKey, "getPublicKey");
   pragma Export (Java, Encode, "encode");
   pragma Export (Java, Decode, "decode");
   pragma Export (Java, GetFormat, "getFormat");
   pragma Export (Java, ToString, "toString");

end Java.Security.Certificate;
pragma Import (Java, Java.Security.Certificate, "java.security.Certificate");
pragma Extensions_Allowed (Off);
