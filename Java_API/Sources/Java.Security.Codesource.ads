pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Security.Cert.Certificate;
limited with Java.Security.CodeSigner;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.CodeSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CodeSource (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                            P2_Certificate_Arr : access Java.Security.Cert.Certificate.Arr_Obj; 
                            This : Ref := null)
                            return Ref;

   function New_CodeSource (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                            P2_CodeSigner_Arr : access Java.Security.CodeSigner.Arr_Obj; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function GetLocation (This : access Typ)
                         return access Java.Net.URL.Typ'Class;

   --  final
   function GetCertificates (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCodeSigners (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function Implies (This : access Typ;
                     P1_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                     return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CodeSource);
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetCertificates, "getCertificates");
   pragma Import (Java, GetCodeSigners, "getCodeSigners");
   pragma Import (Java, Implies, "implies");
   pragma Import (Java, ToString, "toString");

end Java.Security.CodeSource;
pragma Import (Java, Java.Security.CodeSource, "java.security.CodeSource");
pragma Extensions_Allowed (Off);
