pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Security.MessageDigest;
with Java.Io.Closeable;
with Java.Io.FilterInputStream;
with Java.Lang.Object;

package Java.Security.DigestInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.FilterInputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Digest : access Java.Security.MessageDigest.Typ'Class;
      pragma Import (Java, Digest, "digest");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DigestInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                   P2_MessageDigest : access Standard.Java.Security.MessageDigest.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessageDigest (This : access Typ)
                              return access Java.Security.MessageDigest.Typ'Class;

   procedure SetMessageDigest (This : access Typ;
                               P1_MessageDigest : access Standard.Java.Security.MessageDigest.Typ'Class);

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure On (This : access Typ;
                 P1_Boolean : Java.Boolean);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DigestInputStream);
   pragma Import (Java, GetMessageDigest, "getMessageDigest");
   pragma Import (Java, SetMessageDigest, "setMessageDigest");
   pragma Import (Java, Read, "read");
   pragma Import (Java, On, "on");
   pragma Import (Java, ToString, "toString");

end Java.Security.DigestInputStream;
pragma Import (Java, Java.Security.DigestInputStream, "java.security.DigestInputStream");
pragma Extensions_Allowed (Off);
