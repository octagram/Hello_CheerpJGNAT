pragma Extensions_Allowed (On);
limited with Java.Security.ProtectionDomain;
with Java.Lang.Object;

package Java.Security.DomainCombiner is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Combine (This : access Typ;
                     P1_ProtectionDomain_Arr : access Java.Security.ProtectionDomain.Arr_Obj;
                     P2_ProtectionDomain_Arr : access Java.Security.ProtectionDomain.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Combine, "combine");

end Java.Security.DomainCombiner;
pragma Import (Java, Java.Security.DomainCombiner, "java.security.DomainCombiner");
pragma Extensions_Allowed (Off);
