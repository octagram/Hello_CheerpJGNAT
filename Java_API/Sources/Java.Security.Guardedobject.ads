pragma Extensions_Allowed (On);
limited with Java.Security.Guard;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.GuardedObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GuardedObject (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Guard : access Standard.Java.Security.Guard.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetObject (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.SecurityException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GuardedObject);
   pragma Import (Java, GetObject, "getObject");

end Java.Security.GuardedObject;
pragma Import (Java, Java.Security.GuardedObject, "java.security.GuardedObject");
pragma Extensions_Allowed (Off);
