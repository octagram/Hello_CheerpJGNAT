pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Certificate;
limited with Java.Security.IdentityScope;
limited with Java.Security.PublicKey;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Principal;

package Java.Security.Identity is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Principal_I : Java.Security.Principal.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Identity (This : Ref := null)
                          return Ref;

   function New_Identity (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_IdentityScope : access Standard.Java.Security.IdentityScope.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Security.KeyManagementException.Except

   function New_Identity (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetScope (This : access Typ)
                      return access Java.Security.IdentityScope.Typ'Class;

   function GetPublicKey (This : access Typ)
                          return access Java.Security.PublicKey.Typ'Class;

   procedure SetPublicKey (This : access Typ;
                           P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class);
   --  can raise Java.Security.KeyManagementException.Except

   procedure SetInfo (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetInfo (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure AddCertificate (This : access Typ;
                             P1_Certificate : access Standard.Java.Security.Certificate.Typ'Class);
   --  can raise Java.Security.KeyManagementException.Except

   procedure RemoveCertificate (This : access Typ;
                                P1_Certificate : access Standard.Java.Security.Certificate.Typ'Class);
   --  can raise Java.Security.KeyManagementException.Except

   function Certificates (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  protected
   function IdentityEquals (This : access Typ;
                            P1_Identity : access Standard.Java.Security.Identity.Typ'Class)
                            return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ;
                      P1_Boolean : Java.Boolean)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Identity);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetScope, "getScope");
   pragma Import (Java, GetPublicKey, "getPublicKey");
   pragma Import (Java, SetPublicKey, "setPublicKey");
   pragma Import (Java, SetInfo, "setInfo");
   pragma Import (Java, GetInfo, "getInfo");
   pragma Import (Java, AddCertificate, "addCertificate");
   pragma Import (Java, RemoveCertificate, "removeCertificate");
   pragma Import (Java, Certificates, "certificates");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, IdentityEquals, "identityEquals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");

end Java.Security.Identity;
pragma Import (Java, Java.Security.Identity, "java.security.Identity");
pragma Extensions_Allowed (Off);
