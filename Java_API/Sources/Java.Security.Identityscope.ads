pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.PublicKey;
limited with Java.Util.Enumeration;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Identity;
with Java.Security.Principal;

package Java.Security.IdentityScope is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Principal_I : Java.Security.Principal.Ref)
    is abstract new Java.Security.Identity.Typ(Serializable_I,
                                               Principal_I)
      with null record;

   --  protected
   function New_IdentityScope (This : Ref := null)
                               return Ref;

   function New_IdentityScope (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_IdentityScope (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_IdentityScope : access Standard.Java.Security.IdentityScope.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Java.Security.KeyManagementException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSystemScope return access Java.Security.IdentityScope.Typ'Class;

   --  protected
   procedure SetSystemScope (P1_IdentityScope : access Standard.Java.Security.IdentityScope.Typ'Class);

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function GetIdentity (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Identity.Typ'Class is abstract;

   function GetIdentity (This : access Typ;
                         P1_Principal : access Standard.Java.Security.Principal.Typ'Class)
                         return access Java.Security.Identity.Typ'Class;

   function GetIdentity (This : access Typ;
                         P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class)
                         return access Java.Security.Identity.Typ'Class is abstract;

   procedure AddIdentity (This : access Typ;
                          P1_Identity : access Standard.Java.Security.Identity.Typ'Class) is abstract;
   --  can raise Java.Security.KeyManagementException.Except

   procedure RemoveIdentity (This : access Typ;
                             P1_Identity : access Standard.Java.Security.Identity.Typ'Class) is abstract;
   --  can raise Java.Security.KeyManagementException.Except

   function Identities (This : access Typ)
                        return access Java.Util.Enumeration.Typ'Class is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IdentityScope);
   pragma Export (Java, GetSystemScope, "getSystemScope");
   pragma Export (Java, SetSystemScope, "setSystemScope");
   pragma Export (Java, Size, "size");
   pragma Export (Java, GetIdentity, "getIdentity");
   pragma Export (Java, AddIdentity, "addIdentity");
   pragma Export (Java, RemoveIdentity, "removeIdentity");
   pragma Export (Java, Identities, "identities");
   pragma Export (Java, ToString, "toString");

end Java.Security.IdentityScope;
pragma Import (Java, Java.Security.IdentityScope, "java.security.IdentityScope");
pragma Extensions_Allowed (Off);
