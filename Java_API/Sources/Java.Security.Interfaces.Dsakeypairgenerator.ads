pragma Extensions_Allowed (On);
limited with Java.Security.Interfaces.DSAParams;
limited with Java.Security.SecureRandom;
with Java.Lang.Object;

package Java.Security.Interfaces.DSAKeyPairGenerator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Initialize (This : access Typ;
                         P1_DSAParams : access Standard.Java.Security.Interfaces.DSAParams.Typ'Class;
                         P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class) is abstract;
   --  can raise Java.Security.InvalidParameterException.Except

   procedure Initialize (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Boolean : Java.Boolean;
                         P3_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class) is abstract;
   --  can raise Java.Security.InvalidParameterException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Initialize, "initialize");

end Java.Security.Interfaces.DSAKeyPairGenerator;
pragma Import (Java, Java.Security.Interfaces.DSAKeyPairGenerator, "java.security.interfaces.DSAKeyPairGenerator");
pragma Extensions_Allowed (Off);
