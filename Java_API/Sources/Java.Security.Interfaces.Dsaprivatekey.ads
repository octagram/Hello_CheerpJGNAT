pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;
with Java.Security.Interfaces.DSAKey;
with Java.Security.PrivateKey;

package Java.Security.Interfaces.DSAPrivateKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            PrivateKey_I : Java.Security.PrivateKey.Ref;
            DSAKey_I : Java.Security.Interfaces.DSAKey.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetX (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetX, "getX");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Java.Security.Interfaces.DSAPrivateKey;
pragma Import (Java, Java.Security.Interfaces.DSAPrivateKey, "java.security.interfaces.DSAPrivateKey");
pragma Extensions_Allowed (Off);
