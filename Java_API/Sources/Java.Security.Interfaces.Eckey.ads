pragma Extensions_Allowed (On);
limited with Java.Security.Spec.ECParameterSpec;
with Java.Lang.Object;

package Java.Security.Interfaces.ECKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParams (This : access Typ)
                       return access Java.Security.Spec.ECParameterSpec.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParams, "getParams");

end Java.Security.Interfaces.ECKey;
pragma Import (Java, Java.Security.Interfaces.ECKey, "java.security.interfaces.ECKey");
pragma Extensions_Allowed (Off);
