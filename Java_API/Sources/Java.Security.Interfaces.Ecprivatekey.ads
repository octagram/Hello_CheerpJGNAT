pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;
with Java.Security.Interfaces.ECKey;
with Java.Security.PrivateKey;

package Java.Security.Interfaces.ECPrivateKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            PrivateKey_I : Java.Security.PrivateKey.Ref;
            ECKey_I : Java.Security.Interfaces.ECKey.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetS (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetS, "getS");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Java.Security.Interfaces.ECPrivateKey;
pragma Import (Java, Java.Security.Interfaces.ECPrivateKey, "java.security.interfaces.ECPrivateKey");
pragma Extensions_Allowed (Off);
