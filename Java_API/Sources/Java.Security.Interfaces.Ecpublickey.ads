pragma Extensions_Allowed (On);
limited with Java.Security.Spec.ECPoint;
with Java.Lang.Object;
with Java.Security.Interfaces.ECKey;
with Java.Security.PublicKey;

package Java.Security.Interfaces.ECPublicKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            PublicKey_I : Java.Security.PublicKey.Ref;
            ECKey_I : Java.Security.Interfaces.ECKey.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetW (This : access Typ)
                  return access Java.Security.Spec.ECPoint.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetW, "getW");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Java.Security.Interfaces.ECPublicKey;
pragma Import (Java, Java.Security.Interfaces.ECPublicKey, "java.security.interfaces.ECPublicKey");
pragma Extensions_Allowed (Off);
