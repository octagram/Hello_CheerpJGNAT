pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
limited with Java.Security.Spec.RSAOtherPrimeInfo;
with Java.Lang.Object;
with Java.Security.Interfaces.RSAPrivateKey;

package Java.Security.Interfaces.RSAMultiPrimePrivateCrtKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RSAPrivateKey_I : Java.Security.Interfaces.RSAPrivateKey.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublicExponent (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetPrimeP (This : access Typ)
                       return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetPrimeQ (This : access Typ)
                       return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetPrimeExponentP (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetPrimeExponentQ (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetCrtCoefficient (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetOtherPrimeInfo (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPublicExponent, "getPublicExponent");
   pragma Export (Java, GetPrimeP, "getPrimeP");
   pragma Export (Java, GetPrimeQ, "getPrimeQ");
   pragma Export (Java, GetPrimeExponentP, "getPrimeExponentP");
   pragma Export (Java, GetPrimeExponentQ, "getPrimeExponentQ");
   pragma Export (Java, GetCrtCoefficient, "getCrtCoefficient");
   pragma Export (Java, GetOtherPrimeInfo, "getOtherPrimeInfo");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Java.Security.Interfaces.RSAMultiPrimePrivateCrtKey;
pragma Import (Java, Java.Security.Interfaces.RSAMultiPrimePrivateCrtKey, "java.security.interfaces.RSAMultiPrimePrivateCrtKey");
pragma Extensions_Allowed (Off);
