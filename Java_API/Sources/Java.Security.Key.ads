pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.Key is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetFormat (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAlgorithm, "getAlgorithm");
   pragma Export (Java, GetFormat, "getFormat");
   pragma Export (Java, GetEncoded, "getEncoded");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Java.Security.Key;
pragma Import (Java, Java.Security.Key, "java.security.Key");
pragma Extensions_Allowed (Off);
