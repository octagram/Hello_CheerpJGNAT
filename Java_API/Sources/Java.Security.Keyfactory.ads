pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Security.Key;
limited with Java.Security.KeyFactorySpi;
limited with Java.Security.PrivateKey;
limited with Java.Security.Provider;
limited with Java.Security.PublicKey;
limited with Java.Security.Spec.KeySpec;
with Java.Lang.Object;

package Java.Security.KeyFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_KeyFactory (P1_KeyFactorySpi : access Standard.Java.Security.KeyFactorySpi.Typ'Class;
                            P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.KeyFactory.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.KeyFactory.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.KeyFactory.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function GeneratePublic (This : access Typ;
                            P1_KeySpec : access Standard.Java.Security.Spec.KeySpec.Typ'Class)
                            return access Java.Security.PublicKey.Typ'Class;
   --  can raise Java.Security.Spec.InvalidKeySpecException.Except

   --  final
   function GeneratePrivate (This : access Typ;
                             P1_KeySpec : access Standard.Java.Security.Spec.KeySpec.Typ'Class)
                             return access Java.Security.PrivateKey.Typ'Class;
   --  can raise Java.Security.Spec.InvalidKeySpecException.Except

   --  final
   function GetKeySpec (This : access Typ;
                        P1_Key : access Standard.Java.Security.Key.Typ'Class;
                        P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Java.Security.Spec.KeySpec.Typ'Class;
   --  can raise Java.Security.Spec.InvalidKeySpecException.Except

   --  final
   function TranslateKey (This : access Typ;
                          P1_Key : access Standard.Java.Security.Key.Typ'Class)
                          return access Java.Security.Key.Typ'Class;
   --  can raise Java.Security.InvalidKeyException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyFactory);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GeneratePublic, "generatePublic");
   pragma Import (Java, GeneratePrivate, "generatePrivate");
   pragma Import (Java, GetKeySpec, "getKeySpec");
   pragma Import (Java, TranslateKey, "translateKey");

end Java.Security.KeyFactory;
pragma Import (Java, Java.Security.KeyFactory, "java.security.KeyFactory");
pragma Extensions_Allowed (Off);
