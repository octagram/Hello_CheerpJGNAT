pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Security.Key;
limited with Java.Security.PrivateKey;
limited with Java.Security.PublicKey;
limited with Java.Security.Spec.KeySpec;
with Java.Lang.Object;

package Java.Security.KeyFactorySpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_KeyFactorySpi (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function EngineGeneratePublic (This : access Typ;
                                  P1_KeySpec : access Standard.Java.Security.Spec.KeySpec.Typ'Class)
                                  return access Java.Security.PublicKey.Typ'Class is abstract;
   --  can raise Java.Security.Spec.InvalidKeySpecException.Except

   --  protected
   function EngineGeneratePrivate (This : access Typ;
                                   P1_KeySpec : access Standard.Java.Security.Spec.KeySpec.Typ'Class)
                                   return access Java.Security.PrivateKey.Typ'Class is abstract;
   --  can raise Java.Security.Spec.InvalidKeySpecException.Except

   --  protected
   function EngineGetKeySpec (This : access Typ;
                              P1_Key : access Standard.Java.Security.Key.Typ'Class;
                              P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                              return access Java.Security.Spec.KeySpec.Typ'Class is abstract;
   --  can raise Java.Security.Spec.InvalidKeySpecException.Except

   --  protected
   function EngineTranslateKey (This : access Typ;
                                P1_Key : access Standard.Java.Security.Key.Typ'Class)
                                return access Java.Security.Key.Typ'Class is abstract;
   --  can raise Java.Security.InvalidKeyException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyFactorySpi);
   pragma Export (Java, EngineGeneratePublic, "engineGeneratePublic");
   pragma Export (Java, EngineGeneratePrivate, "engineGeneratePrivate");
   pragma Export (Java, EngineGetKeySpec, "engineGetKeySpec");
   pragma Export (Java, EngineTranslateKey, "engineTranslateKey");

end Java.Security.KeyFactorySpi;
pragma Import (Java, Java.Security.KeyFactorySpi, "java.security.KeyFactorySpi");
pragma Extensions_Allowed (Off);
