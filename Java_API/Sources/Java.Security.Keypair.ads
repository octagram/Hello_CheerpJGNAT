pragma Extensions_Allowed (On);
limited with Java.Security.PrivateKey;
limited with Java.Security.PublicKey;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.KeyPair is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KeyPair (P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class;
                         P2_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublic (This : access Typ)
                       return access Java.Security.PublicKey.Typ'Class;

   function GetPrivate (This : access Typ)
                        return access Java.Security.PrivateKey.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyPair);
   pragma Import (Java, GetPublic, "getPublic");
   pragma Import (Java, GetPrivate, "getPrivate");

end Java.Security.KeyPair;
pragma Import (Java, Java.Security.KeyPair, "java.security.KeyPair");
pragma Extensions_Allowed (Off);
