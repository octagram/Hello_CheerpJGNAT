pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.KeyPair;
limited with Java.Security.Provider;
limited with Java.Security.SecureRandom;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;
with Java.Security.KeyPairGeneratorSpi;

package Java.Security.KeyPairGenerator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Security.KeyPairGeneratorSpi.Typ
      with null record;

   --  protected
   function New_KeyPairGenerator (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.KeyPairGenerator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.KeyPairGenerator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.KeyPairGenerator.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   procedure Initialize (This : access Typ;
                         P1_Int : Java.Int);

   procedure Initialize (This : access Typ;
                         P1_Int : Java.Int;
                         P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class);

   procedure Initialize (This : access Typ;
                         P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   procedure Initialize (This : access Typ;
                         P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;
                         P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   function GenKeyPair (This : access Typ)
                        return access Java.Security.KeyPair.Typ'Class;

   function GenerateKeyPair (This : access Typ)
                             return access Java.Security.KeyPair.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyPairGenerator);
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, Initialize, "initialize");
   pragma Import (Java, GenKeyPair, "genKeyPair");
   pragma Import (Java, GenerateKeyPair, "generateKeyPair");

end Java.Security.KeyPairGenerator;
pragma Import (Java, Java.Security.KeyPairGenerator, "java.security.KeyPairGenerator");
pragma Extensions_Allowed (Off);
