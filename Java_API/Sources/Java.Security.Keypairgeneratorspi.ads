pragma Extensions_Allowed (On);
limited with Java.Security.KeyPair;
limited with Java.Security.SecureRandom;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;

package Java.Security.KeyPairGeneratorSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_KeyPairGeneratorSpi (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Initialize (This : access Typ;
                         P1_Int : Java.Int;
                         P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class) is abstract;

   procedure Initialize (This : access Typ;
                         P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;
                         P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   function GenerateKeyPair (This : access Typ)
                             return access Java.Security.KeyPair.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyPairGeneratorSpi);
   pragma Export (Java, Initialize, "initialize");
   pragma Export (Java, GenerateKeyPair, "generateKeyPair");

end Java.Security.KeyPairGeneratorSpi;
pragma Import (Java, Java.Security.KeyPairGeneratorSpi, "java.security.KeyPairGeneratorSpi");
pragma Extensions_Allowed (Off);
