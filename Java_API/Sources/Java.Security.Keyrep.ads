pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.KeyRep.Type_K;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.KeyRep is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KeyRep (P1_Type_K : access Standard.Java.Security.KeyRep.Type_K.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                        P4_Byte_Arr : Java.Byte_Arr; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.ObjectStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyRep);
   pragma Import (Java, ReadResolve, "readResolve");

end Java.Security.KeyRep;
pragma Import (Java, Java.Security.KeyRep, "java.security.KeyRep");
pragma Extensions_Allowed (Off);
