pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.String;
limited with Java.Security.KeyStore.ProtectionParameter;
limited with Java.Security.Provider;
with Java.Lang.Object;

package Java.Security.KeyStore.Builder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Builder (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKeyStore (This : access Typ)
                         return access Java.Security.KeyStore.Typ'Class is abstract;
   --  can raise Java.Security.KeyStoreException.Except

   function GetProtectionParameter (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Java.Security.KeyStore.ProtectionParameter.Typ'Class is abstract;
   --  can raise Java.Security.KeyStoreException.Except

   function NewInstance (P1_KeyStore : access Standard.Java.Security.KeyStore.Typ'Class;
                         P2_ProtectionParameter : access Standard.Java.Security.KeyStore.ProtectionParameter.Typ'Class)
                         return access Java.Security.KeyStore.Builder.Typ'Class;

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                         P3_File : access Standard.Java.Io.File.Typ'Class;
                         P4_ProtectionParameter : access Standard.Java.Security.KeyStore.ProtectionParameter.Typ'Class)
                         return access Java.Security.KeyStore.Builder.Typ'Class;

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                         P3_ProtectionParameter : access Standard.Java.Security.KeyStore.ProtectionParameter.Typ'Class)
                         return access Java.Security.KeyStore.Builder.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Builder);
   pragma Export (Java, GetKeyStore, "getKeyStore");
   pragma Export (Java, GetProtectionParameter, "getProtectionParameter");
   pragma Export (Java, NewInstance, "newInstance");

end Java.Security.KeyStore.Builder;
pragma Import (Java, Java.Security.KeyStore.Builder, "java.security.KeyStore$Builder");
pragma Extensions_Allowed (Off);
