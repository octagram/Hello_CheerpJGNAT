pragma Extensions_Allowed (On);
limited with Javax.Security.Auth.Callback.CallbackHandler;
with Java.Lang.Object;
with Java.Security.KeyStore.ProtectionParameter;

package Java.Security.KeyStore.CallbackHandlerProtection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ProtectionParameter_I : Java.Security.KeyStore.ProtectionParameter.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CallbackHandlerProtection (P1_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCallbackHandler (This : access Typ)
                                return access Javax.Security.Auth.Callback.CallbackHandler.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CallbackHandlerProtection);
   pragma Import (Java, GetCallbackHandler, "getCallbackHandler");

end Java.Security.KeyStore.CallbackHandlerProtection;
pragma Import (Java, Java.Security.KeyStore.CallbackHandlerProtection, "java.security.KeyStore$CallbackHandlerProtection");
pragma Extensions_Allowed (Off);
