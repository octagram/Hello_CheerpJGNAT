pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Security.KeyStore.ProtectionParameter;
with Javax.Security.Auth.Destroyable;

package Java.Security.KeyStore.PasswordProtection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ProtectionParameter_I : Java.Security.KeyStore.ProtectionParameter.Ref;
            Destroyable_I : Javax.Security.Auth.Destroyable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PasswordProtection (P1_Char_Arr : Java.Char_Arr; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetPassword (This : access Typ)
                         return Java.Char_Arr;

   --  synchronized
   procedure Destroy (This : access Typ);
   --  can raise Javax.Security.Auth.DestroyFailedException.Except

   --  synchronized
   function IsDestroyed (This : access Typ)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PasswordProtection);
   pragma Import (Java, GetPassword, "getPassword");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, IsDestroyed, "isDestroyed");

end Java.Security.KeyStore.PasswordProtection;
pragma Import (Java, Java.Security.KeyStore.PasswordProtection, "java.security.KeyStore$PasswordProtection");
pragma Extensions_Allowed (Off);
