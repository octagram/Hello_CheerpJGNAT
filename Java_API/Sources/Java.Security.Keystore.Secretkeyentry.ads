pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Crypto.SecretKey;
with Java.Lang.Object;
with Java.Security.KeyStore.Entry_K;

package Java.Security.KeyStore.SecretKeyEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Entry_K_I : Java.Security.KeyStore.Entry_K.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SecretKeyEntry (P1_SecretKey : access Standard.Javax.Crypto.SecretKey.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSecretKey (This : access Typ)
                          return access Javax.Crypto.SecretKey.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SecretKeyEntry);
   pragma Import (Java, GetSecretKey, "getSecretKey");
   pragma Import (Java, ToString, "toString");

end Java.Security.KeyStore.SecretKeyEntry;
pragma Import (Java, Java.Security.KeyStore.SecretKeyEntry, "java.security.KeyStore$SecretKeyEntry");
pragma Extensions_Allowed (Off);
