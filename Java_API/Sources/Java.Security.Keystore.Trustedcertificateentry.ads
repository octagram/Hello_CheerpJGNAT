pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.Certificate;
with Java.Lang.Object;
with Java.Security.KeyStore.Entry_K;

package Java.Security.KeyStore.TrustedCertificateEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Entry_K_I : Java.Security.KeyStore.Entry_K.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TrustedCertificateEntry (P1_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTrustedCertificate (This : access Typ)
                                   return access Java.Security.Cert.Certificate.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TrustedCertificateEntry);
   pragma Import (Java, GetTrustedCertificate, "getTrustedCertificate");
   pragma Import (Java, ToString, "toString");

end Java.Security.KeyStore.TrustedCertificateEntry;
pragma Import (Java, Java.Security.KeyStore.TrustedCertificateEntry, "java.security.KeyStore$TrustedCertificateEntry");
pragma Extensions_Allowed (Off);
