pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Security.Cert.Certificate;
limited with Java.Security.Key;
limited with Java.Security.KeyStore.Entry_K;
limited with Java.Security.KeyStore.LoadStoreParameter;
limited with Java.Security.KeyStore.ProtectionParameter;
limited with Java.Security.KeyStoreSpi;
limited with Java.Security.Provider;
limited with Java.Util.Date;
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Java.Security.KeyStore is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_KeyStore (P1_KeyStoreSpi : access Standard.Java.Security.KeyStoreSpi.Typ'Class;
                          P2_Provider : access Standard.Java.Security.Provider.Typ'Class;
                          P3_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.KeyStore.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.KeyStore.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.KeyStore.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function GetDefaultType return access Java.Lang.String.Typ'Class;

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetKey (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Char_Arr : Java.Char_Arr)
                    return access Java.Security.Key.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.UnrecoverableKeyException.Except

   --  final
   function GetCertificateChain (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function GetCertificate (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Security.Cert.Certificate.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function GetCreationDate (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Util.Date.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   procedure SetKeyEntry (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Key : access Standard.Java.Security.Key.Typ'Class;
                          P3_Char_Arr : Java.Char_Arr;
                          P4_Certificate_Arr : access Java.Security.Cert.Certificate.Arr_Obj);
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   procedure SetKeyEntry (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Byte_Arr : Java.Byte_Arr;
                          P3_Certificate_Arr : access Java.Security.Cert.Certificate.Arr_Obj);
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   procedure SetCertificateEntry (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class);
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   procedure DeleteEntry (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function Aliases (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function ContainsAlias (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return Java.Boolean;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function Size (This : access Typ)
                  return Java.Int;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function IsKeyEntry (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function IsCertificateEntry (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function GetCertificateAlias (This : access Typ;
                                 P1_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class)
                                 return access Java.Lang.String.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   procedure Store (This : access Typ;
                    P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                    P2_Char_Arr : Java.Char_Arr);
   --  can raise Java.Security.KeyStoreException.Except,
   --  Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   --  final
   procedure Store (This : access Typ;
                    P1_LoadStoreParameter : access Standard.Java.Security.KeyStore.LoadStoreParameter.Typ'Class);
   --  can raise Java.Security.KeyStoreException.Except,
   --  Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   --  final
   procedure Load (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_Char_Arr : Java.Char_Arr);
   --  can raise Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   --  final
   procedure Load (This : access Typ;
                   P1_LoadStoreParameter : access Standard.Java.Security.KeyStore.LoadStoreParameter.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   --  final
   function GetEntry (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_ProtectionParameter : access Standard.Java.Security.KeyStore.ProtectionParameter.Typ'Class)
                      return access Java.Security.KeyStore.Entry_K.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except,
   --  Java.Security.UnrecoverableEntryException.Except and
   --  Java.Security.KeyStoreException.Except

   --  final
   procedure SetEntry (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Entry_K : access Standard.Java.Security.KeyStore.Entry_K.Typ'Class;
                       P3_ProtectionParameter : access Standard.Java.Security.KeyStore.ProtectionParameter.Typ'Class);
   --  can raise Java.Security.KeyStoreException.Except

   --  final
   function EntryInstanceOf (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                             return Java.Boolean;
   --  can raise Java.Security.KeyStoreException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyStore);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetDefaultType, "getDefaultType");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetKey, "getKey");
   pragma Import (Java, GetCertificateChain, "getCertificateChain");
   pragma Import (Java, GetCertificate, "getCertificate");
   pragma Import (Java, GetCreationDate, "getCreationDate");
   pragma Import (Java, SetKeyEntry, "setKeyEntry");
   pragma Import (Java, SetCertificateEntry, "setCertificateEntry");
   pragma Import (Java, DeleteEntry, "deleteEntry");
   pragma Import (Java, Aliases, "aliases");
   pragma Import (Java, ContainsAlias, "containsAlias");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsKeyEntry, "isKeyEntry");
   pragma Import (Java, IsCertificateEntry, "isCertificateEntry");
   pragma Import (Java, GetCertificateAlias, "getCertificateAlias");
   pragma Import (Java, Store, "store");
   pragma Import (Java, Load, "load");
   pragma Import (Java, GetEntry, "getEntry");
   pragma Import (Java, SetEntry, "setEntry");
   pragma Import (Java, EntryInstanceOf, "entryInstanceOf");

end Java.Security.KeyStore;
pragma Import (Java, Java.Security.KeyStore, "java.security.KeyStore");
pragma Extensions_Allowed (Off);
