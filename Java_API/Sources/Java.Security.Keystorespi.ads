pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Security.Cert.Certificate;
limited with Java.Security.Key;
limited with Java.Security.KeyStore.Entry_K;
limited with Java.Security.KeyStore.LoadStoreParameter;
limited with Java.Security.KeyStore.ProtectionParameter;
limited with Java.Util.Date;
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Java.Security.KeyStoreSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_KeyStoreSpi (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function EngineGetKey (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Char_Arr : Java.Char_Arr)
                          return access Java.Security.Key.Typ'Class is abstract;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.UnrecoverableKeyException.Except

   function EngineGetCertificateChain (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                                       return Standard.Java.Lang.Object.Ref is abstract;

   function EngineGetCertificate (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Java.Security.Cert.Certificate.Typ'Class is abstract;

   function EngineGetCreationDate (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Util.Date.Typ'Class is abstract;

   procedure EngineSetKeyEntry (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Key : access Standard.Java.Security.Key.Typ'Class;
                                P3_Char_Arr : Java.Char_Arr;
                                P4_Certificate_Arr : access Java.Security.Cert.Certificate.Arr_Obj) is abstract;
   --  can raise Java.Security.KeyStoreException.Except

   procedure EngineSetKeyEntry (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Byte_Arr : Java.Byte_Arr;
                                P3_Certificate_Arr : access Java.Security.Cert.Certificate.Arr_Obj) is abstract;
   --  can raise Java.Security.KeyStoreException.Except

   procedure EngineSetCertificateEntry (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class) is abstract;
   --  can raise Java.Security.KeyStoreException.Except

   procedure EngineDeleteEntry (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Security.KeyStoreException.Except

   function EngineAliases (This : access Typ)
                           return access Java.Util.Enumeration.Typ'Class is abstract;

   function EngineContainsAlias (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return Java.Boolean is abstract;

   function EngineSize (This : access Typ)
                        return Java.Int is abstract;

   function EngineIsKeyEntry (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Boolean is abstract;

   function EngineIsCertificateEntry (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return Java.Boolean is abstract;

   function EngineGetCertificateAlias (This : access Typ;
                                       P1_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class)
                                       return access Java.Lang.String.Typ'Class is abstract;

   procedure EngineStore (This : access Typ;
                          P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                          P2_Char_Arr : Java.Char_Arr) is abstract;
   --  can raise Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   procedure EngineStore (This : access Typ;
                          P1_LoadStoreParameter : access Standard.Java.Security.KeyStore.LoadStoreParameter.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   procedure EngineLoad (This : access Typ;
                         P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                         P2_Char_Arr : Java.Char_Arr) is abstract;
   --  can raise Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   procedure EngineLoad (This : access Typ;
                         P1_LoadStoreParameter : access Standard.Java.Security.KeyStore.LoadStoreParameter.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.Cert.CertificateException.Except

   function EngineGetEntry (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_ProtectionParameter : access Standard.Java.Security.KeyStore.ProtectionParameter.Typ'Class)
                            return access Java.Security.KeyStore.Entry_K.Typ'Class;
   --  can raise Java.Security.KeyStoreException.Except,
   --  Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.UnrecoverableEntryException.Except

   procedure EngineSetEntry (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Entry_K : access Standard.Java.Security.KeyStore.Entry_K.Typ'Class;
                             P3_ProtectionParameter : access Standard.Java.Security.KeyStore.ProtectionParameter.Typ'Class);
   --  can raise Java.Security.KeyStoreException.Except

   function EngineEntryInstanceOf (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                                   return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyStoreSpi);
   pragma Export (Java, EngineGetKey, "engineGetKey");
   pragma Export (Java, EngineGetCertificateChain, "engineGetCertificateChain");
   pragma Export (Java, EngineGetCertificate, "engineGetCertificate");
   pragma Export (Java, EngineGetCreationDate, "engineGetCreationDate");
   pragma Export (Java, EngineSetKeyEntry, "engineSetKeyEntry");
   pragma Export (Java, EngineSetCertificateEntry, "engineSetCertificateEntry");
   pragma Export (Java, EngineDeleteEntry, "engineDeleteEntry");
   pragma Export (Java, EngineAliases, "engineAliases");
   pragma Export (Java, EngineContainsAlias, "engineContainsAlias");
   pragma Export (Java, EngineSize, "engineSize");
   pragma Export (Java, EngineIsKeyEntry, "engineIsKeyEntry");
   pragma Export (Java, EngineIsCertificateEntry, "engineIsCertificateEntry");
   pragma Export (Java, EngineGetCertificateAlias, "engineGetCertificateAlias");
   pragma Export (Java, EngineStore, "engineStore");
   pragma Export (Java, EngineLoad, "engineLoad");
   pragma Export (Java, EngineGetEntry, "engineGetEntry");
   pragma Export (Java, EngineSetEntry, "engineSetEntry");
   pragma Export (Java, EngineEntryInstanceOf, "engineEntryInstanceOf");

end Java.Security.KeyStoreSpi;
pragma Import (Java, Java.Security.KeyStoreSpi, "java.security.KeyStoreSpi");
pragma Extensions_Allowed (Off);
