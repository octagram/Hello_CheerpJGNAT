pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteBuffer;
limited with Java.Security.Provider;
with Java.Lang.Object;
with Java.Security.MessageDigestSpi;

package Java.Security.MessageDigest is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Security.MessageDigestSpi.Typ
      with null record;

   --  protected
   function New_MessageDigest (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.MessageDigest.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.MessageDigest.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.MessageDigest.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   procedure Update (This : access Typ;
                     P1_Byte : Java.Byte);

   procedure Update (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);

   procedure Update (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr);

   --  final
   procedure Update (This : access Typ;
                     P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class);

   function Digest (This : access Typ)
                    return Java.Byte_Arr;

   function Digest (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return Java.Int;
   --  can raise Java.Security.DigestException.Except

   function Digest (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr)
                    return Java.Byte_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IsEqual (P1_Byte_Arr : Java.Byte_Arr;
                     P2_Byte_Arr : Java.Byte_Arr)
                     return Java.Boolean;

   procedure Reset (This : access Typ);

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function GetDigestLength (This : access Typ)
                             return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MessageDigest);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, Update, "update");
   pragma Import (Java, Digest, "digest");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IsEqual, "isEqual");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GetDigestLength, "getDigestLength");
   pragma Import (Java, Clone, "clone");

end Java.Security.MessageDigest;
pragma Import (Java, Java.Security.MessageDigest, "java.security.MessageDigest");
pragma Extensions_Allowed (Off);
