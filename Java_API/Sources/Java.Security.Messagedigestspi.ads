pragma Extensions_Allowed (On);
limited with Java.Nio.ByteBuffer;
with Java.Lang.Object;

package Java.Security.MessageDigestSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MessageDigestSpi (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function EngineGetDigestLength (This : access Typ)
                                   return Java.Int;

   --  protected
   procedure EngineUpdate (This : access Typ;
                           P1_Byte : Java.Byte) is abstract;

   --  protected
   procedure EngineUpdate (This : access Typ;
                           P1_Byte_Arr : Java.Byte_Arr;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int) is abstract;

   --  protected
   procedure EngineUpdate (This : access Typ;
                           P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class);

   --  protected
   function EngineDigest (This : access Typ)
                          return Java.Byte_Arr is abstract;

   --  protected
   function EngineDigest (This : access Typ;
                          P1_Byte_Arr : Java.Byte_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int)
                          return Java.Int;
   --  can raise Java.Security.DigestException.Except

   --  protected
   procedure EngineReset (This : access Typ) is abstract;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MessageDigestSpi);
   pragma Export (Java, EngineGetDigestLength, "engineGetDigestLength");
   pragma Export (Java, EngineUpdate, "engineUpdate");
   pragma Export (Java, EngineDigest, "engineDigest");
   pragma Export (Java, EngineReset, "engineReset");
   pragma Export (Java, Clone, "clone");

end Java.Security.MessageDigestSpi;
pragma Import (Java, Java.Security.MessageDigestSpi, "java.security.MessageDigestSpi");
pragma Extensions_Allowed (Off);
