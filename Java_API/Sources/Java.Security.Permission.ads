pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.PermissionCollection;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Guard;

package Java.Security.Permission is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Guard_I : Java.Security.Guard.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Permission (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure CheckGuard (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   function Implies (This : access Typ;
                     P1_Permission : access Standard.Java.Security.Permission.Typ'Class)
                     return Java.Boolean is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetActions (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function NewPermissionCollection (This : access Typ)
                                     return access Java.Security.PermissionCollection.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Permission);
   pragma Import (Java, CheckGuard, "checkGuard");
   pragma Export (Java, Implies, "implies");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Import (Java, GetName, "getName");
   pragma Export (Java, GetActions, "getActions");
   pragma Import (Java, NewPermissionCollection, "newPermissionCollection");
   pragma Import (Java, ToString, "toString");

end Java.Security.Permission;
pragma Import (Java, Java.Security.Permission, "java.security.Permission");
pragma Extensions_Allowed (Off);
