pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Permission;
limited with Java.Util.Enumeration;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.PermissionCollection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_PermissionCollection (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Add (This : access Typ;
                  P1_Permission : access Standard.Java.Security.Permission.Typ'Class) is abstract;

   function Implies (This : access Typ;
                     P1_Permission : access Standard.Java.Security.Permission.Typ'Class)
                     return Java.Boolean is abstract;

   function Elements (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class is abstract;

   procedure SetReadOnly (This : access Typ);

   function IsReadOnly (This : access Typ)
                        return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PermissionCollection);
   pragma Export (Java, Add, "add");
   pragma Export (Java, Implies, "implies");
   pragma Export (Java, Elements, "elements");
   pragma Export (Java, SetReadOnly, "setReadOnly");
   pragma Export (Java, IsReadOnly, "isReadOnly");
   pragma Export (Java, ToString, "toString");

end Java.Security.PermissionCollection;
pragma Import (Java, Java.Security.PermissionCollection, "java.security.PermissionCollection");
pragma Extensions_Allowed (Off);
