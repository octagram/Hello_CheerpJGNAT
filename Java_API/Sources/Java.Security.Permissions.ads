pragma Extensions_Allowed (On);
limited with Java.Security.Permission;
limited with Java.Util.Enumeration;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.PermissionCollection;

package Java.Security.Permissions is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Security.PermissionCollection.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Permissions (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Add (This : access Typ;
                  P1_Permission : access Standard.Java.Security.Permission.Typ'Class);

   function Implies (This : access Typ;
                     P1_Permission : access Standard.Java.Security.Permission.Typ'Class)
                     return Java.Boolean;

   function Elements (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Permissions);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Implies, "implies");
   pragma Import (Java, Elements, "elements");

end Java.Security.Permissions;
pragma Import (Java, Java.Security.Permissions, "java.security.Permissions");
pragma Extensions_Allowed (Off);
