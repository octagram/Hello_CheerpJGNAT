pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.CodeSource;
limited with Java.Security.Permission;
limited with Java.Security.PermissionCollection;
limited with Java.Security.Policy.Parameters;
limited with Java.Security.ProtectionDomain;
limited with Java.Security.Provider;
with Java.Lang.Object;

package Java.Security.Policy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Policy (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPolicy return access Java.Security.Policy.Typ'Class;

   procedure SetPolicy (P1_Policy : access Standard.Java.Security.Policy.Typ'Class);

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Parameters : access Standard.Java.Security.Policy.Parameters.Typ'Class)
                         return access Java.Security.Policy.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Parameters : access Standard.Java.Security.Policy.Parameters.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Policy.Typ'Class;
   --  can raise Java.Security.NoSuchProviderException.Except and
   --  Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Parameters : access Standard.Java.Security.Policy.Parameters.Typ'Class;
                         P3_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.Policy.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetParameters (This : access Typ)
                           return access Java.Security.Policy.Parameters.Typ'Class;

   function GetPermissions (This : access Typ;
                            P1_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                            return access Java.Security.PermissionCollection.Typ'Class;

   function GetPermissions (This : access Typ;
                            P1_ProtectionDomain : access Standard.Java.Security.ProtectionDomain.Typ'Class)
                            return access Java.Security.PermissionCollection.Typ'Class;

   function Implies (This : access Typ;
                     P1_ProtectionDomain : access Standard.Java.Security.ProtectionDomain.Typ'Class;
                     P2_Permission : access Standard.Java.Security.Permission.Typ'Class)
                     return Java.Boolean;

   procedure Refresh (This : access Typ);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNSUPPORTED_EMPTY_COLLECTION : access Java.Security.PermissionCollection.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Policy);
   pragma Import (Java, GetPolicy, "getPolicy");
   pragma Import (Java, SetPolicy, "setPolicy");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetParameters, "getParameters");
   pragma Import (Java, GetPermissions, "getPermissions");
   pragma Import (Java, Implies, "implies");
   pragma Import (Java, Refresh, "refresh");
   pragma Import (Java, UNSUPPORTED_EMPTY_COLLECTION, "UNSUPPORTED_EMPTY_COLLECTION");

end Java.Security.Policy;
pragma Import (Java, Java.Security.Policy, "java.security.Policy");
pragma Extensions_Allowed (Off);
