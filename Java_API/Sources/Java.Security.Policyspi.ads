pragma Extensions_Allowed (On);
limited with Java.Security.CodeSource;
limited with Java.Security.Permission;
limited with Java.Security.PermissionCollection;
limited with Java.Security.ProtectionDomain;
with Java.Lang.Object;

package Java.Security.PolicySpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_PolicySpi (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function EngineImplies (This : access Typ;
                           P1_ProtectionDomain : access Standard.Java.Security.ProtectionDomain.Typ'Class;
                           P2_Permission : access Standard.Java.Security.Permission.Typ'Class)
                           return Java.Boolean is abstract;

   --  protected
   procedure EngineRefresh (This : access Typ);

   --  protected
   function EngineGetPermissions (This : access Typ;
                                  P1_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                                  return access Java.Security.PermissionCollection.Typ'Class;

   --  protected
   function EngineGetPermissions (This : access Typ;
                                  P1_ProtectionDomain : access Standard.Java.Security.ProtectionDomain.Typ'Class)
                                  return access Java.Security.PermissionCollection.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PolicySpi);
   pragma Export (Java, EngineImplies, "engineImplies");
   pragma Export (Java, EngineRefresh, "engineRefresh");
   pragma Export (Java, EngineGetPermissions, "engineGetPermissions");

end Java.Security.PolicySpi;
pragma Import (Java, Java.Security.PolicySpi, "java.security.PolicySpi");
pragma Extensions_Allowed (Off);
