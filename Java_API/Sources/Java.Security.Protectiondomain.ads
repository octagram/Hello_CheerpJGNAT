pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Security.CodeSource;
limited with Java.Security.Permission;
limited with Java.Security.PermissionCollection;
limited with Java.Security.Principal;
with Java.Lang.Object;

package Java.Security.ProtectionDomain is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ProtectionDomain (P1_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class;
                                  P2_PermissionCollection : access Standard.Java.Security.PermissionCollection.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_ProtectionDomain (P1_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class;
                                  P2_PermissionCollection : access Standard.Java.Security.PermissionCollection.Typ'Class;
                                  P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                                  P4_Principal_Arr : access Java.Security.Principal.Arr_Obj; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetCodeSource (This : access Typ)
                           return access Java.Security.CodeSource.Typ'Class;

   --  final
   function GetClassLoader (This : access Typ)
                            return access Java.Lang.ClassLoader.Typ'Class;

   --  final
   function GetPrincipals (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   --  final
   function GetPermissions (This : access Typ)
                            return access Java.Security.PermissionCollection.Typ'Class;

   function Implies (This : access Typ;
                     P1_Permission : access Standard.Java.Security.Permission.Typ'Class)
                     return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ProtectionDomain);
   pragma Import (Java, GetCodeSource, "getCodeSource");
   pragma Import (Java, GetClassLoader, "getClassLoader");
   pragma Import (Java, GetPrincipals, "getPrincipals");
   pragma Import (Java, GetPermissions, "getPermissions");
   pragma Import (Java, Implies, "implies");
   pragma Import (Java, ToString, "toString");

end Java.Security.ProtectionDomain;
pragma Import (Java, Java.Security.ProtectionDomain, "java.security.ProtectionDomain");
pragma Extensions_Allowed (Off);
