pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Util.Map;
with Java.Lang.Object;

package Java.Security.Provider.Service is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Service (P1_Provider : access Standard.Java.Security.Provider.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class;
                         P4_String : access Standard.Java.Lang.String.Typ'Class;
                         P5_List : access Standard.Java.Util.List.Typ'Class;
                         P6_Map : access Standard.Java.Util.Map.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function NewInstance (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function SupportsParameter (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Service);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, NewInstance, "newInstance");
   pragma Import (Java, SupportsParameter, "supportsParameter");
   pragma Import (Java, ToString, "toString");

end Java.Security.Provider.Service;
pragma Import (Java, Java.Security.Provider.Service, "java.security.Provider$Service");
pragma Extensions_Allowed (Off);
