pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Security.Provider.Service;
limited with Java.Util.Collection;
limited with Java.Util.Enumeration;
limited with Java.Util.Set;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Map;
with Java.Util.Properties;

package Java.Security.Provider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is abstract new Java.Util.Properties.Typ(Serializable_I,
                                             Cloneable_I,
                                             Map_I)
      with null record;

   --  protected
   function New_Provider (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Double : Java.Double;
                          P3_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetVersion (This : access Typ)
                        return Java.Double;

   function GetInfo (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure Clear (This : access Typ);

   --  synchronized
   procedure Load (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   --  synchronized
   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   --  synchronized
   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Keys (This : access Typ)
                  return access Java.Util.Enumeration.Typ'Class;

   function Elements (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetService (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Security.Provider.Service.Typ'Class;

   --  synchronized
   function GetServices (This : access Typ)
                         return access Java.Util.Set.Typ'Class;

   --  protected  synchronized
   procedure PutService (This : access Typ;
                         P1_Service : access Standard.Java.Security.Provider.Service.Typ'Class);

   --  protected  synchronized
   procedure RemoveService (This : access Typ;
                            P1_Service : access Standard.Java.Security.Provider.Service.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Provider);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetVersion, "getVersion");
   pragma Import (Java, GetInfo, "getInfo");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Load, "load");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Keys, "keys");
   pragma Import (Java, Elements, "elements");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, GetService, "getService");
   pragma Import (Java, GetServices, "getServices");
   pragma Import (Java, PutService, "putService");
   pragma Import (Java, RemoveService, "removeService");

end Java.Security.Provider;
pragma Import (Java, Java.Security.Provider, "java.security.Provider");
pragma Extensions_Allowed (Off);
