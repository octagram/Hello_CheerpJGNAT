pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Security.Key;

package Java.Security.PublicKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Key_I : Java.Security.Key.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Java.Security.PublicKey;
pragma Import (Java, Java.Security.PublicKey, "java.security.PublicKey");
pragma Extensions_Allowed (Off);
