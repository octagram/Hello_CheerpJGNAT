pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Nio.ByteBuffer;
limited with Java.Security.CodeSource;
limited with Java.Security.PermissionCollection;
with Java.Lang.ClassLoader;
with Java.Lang.Object;

package Java.Security.SecureClassLoader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.ClassLoader.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_SecureClassLoader (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   --  protected
   function New_SecureClassLoader (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   function DefineClass (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Byte_Arr : Java.Byte_Arr;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                         return access Java.Lang.Class.Typ'Class;

   --  final  protected
   function DefineClass (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class;
                         P3_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                         return access Java.Lang.Class.Typ'Class;

   --  protected
   function GetPermissions (This : access Typ;
                            P1_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                            return access Java.Security.PermissionCollection.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SecureClassLoader);
   pragma Import (Java, DefineClass, "defineClass");
   pragma Import (Java, GetPermissions, "getPermissions");

end Java.Security.SecureClassLoader;
pragma Import (Java, Java.Security.SecureClassLoader, "java.security.SecureClassLoader");
pragma Extensions_Allowed (Off);
