pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Security.SecureRandomSpi;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.Random;

package Java.Security.SecureRandom is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.Random.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SecureRandom (This : Ref := null)
                              return Ref;

   function New_SecureRandom (P1_Byte_Arr : Java.Byte_Arr; 
                              This : Ref := null)
                              return Ref;

   --  protected
   function New_SecureRandom (P1_SecureRandomSpi : access Standard.Java.Security.SecureRandomSpi.Typ'Class;
                              P2_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.SecureRandom.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.SecureRandom.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.SecureRandom.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure SetSeed (This : access Typ;
                      P1_Byte_Arr : Java.Byte_Arr);

   procedure SetSeed (This : access Typ;
                      P1_Long : Java.Long);

   --  synchronized
   procedure NextBytes (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);

   --  final  protected
   function Next (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Int;

   function GetSeed (P1_Int : Java.Int)
                     return Java.Byte_Arr;

   function GenerateSeed (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Byte_Arr;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SecureRandom);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, SetSeed, "setSeed");
   pragma Import (Java, NextBytes, "nextBytes");
   pragma Import (Java, Next, "next");
   pragma Import (Java, GetSeed, "getSeed");
   pragma Import (Java, GenerateSeed, "generateSeed");

end Java.Security.SecureRandom;
pragma Import (Java, Java.Security.SecureRandom, "java.security.SecureRandom");
pragma Extensions_Allowed (Off);
