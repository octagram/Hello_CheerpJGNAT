pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.SecureRandomSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SecureRandomSpi (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure EngineSetSeed (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr) is abstract;

   --  protected
   procedure EngineNextBytes (This : access Typ;
                              P1_Byte_Arr : Java.Byte_Arr) is abstract;

   --  protected
   function EngineGenerateSeed (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Byte_Arr is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SecureRandomSpi);
   pragma Export (Java, EngineSetSeed, "engineSetSeed");
   pragma Export (Java, EngineNextBytes, "engineNextBytes");
   pragma Export (Java, EngineGenerateSeed, "engineGenerateSeed");

end Java.Security.SecureRandomSpi;
pragma Import (Java, Java.Security.SecureRandomSpi, "java.security.SecureRandomSpi");
pragma Extensions_Allowed (Off);
