pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Util.Map;
limited with Java.Util.Set;
with Java.Lang.Object;

package Java.Security.Security is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function InsertProviderAt (P1_Provider : access Standard.Java.Security.Provider.Typ'Class;
                              P2_Int : Java.Int)
                              return Java.Int;

   function AddProvider (P1_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return Java.Int;

   --  synchronized
   procedure RemoveProvider (P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetProviders return Standard.Java.Lang.Object.Ref;

   function GetProvider (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Provider.Typ'Class;

   function GetProviders (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetProviders (P1_Map : access Standard.Java.Util.Map.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetProperty (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   procedure SetProperty (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class);

   function GetAlgorithms (P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Util.Set.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, InsertProviderAt, "insertProviderAt");
   pragma Import (Java, AddProvider, "addProvider");
   pragma Import (Java, RemoveProvider, "removeProvider");
   pragma Import (Java, GetProviders, "getProviders");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetAlgorithms, "getAlgorithms");

end Java.Security.Security;
pragma Import (Java, Java.Security.Security, "java.security.Security");
pragma Extensions_Allowed (Off);
