pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteBuffer;
limited with Java.Security.AlgorithmParameters;
limited with Java.Security.Cert.Certificate;
limited with Java.Security.PrivateKey;
limited with Java.Security.Provider;
limited with Java.Security.PublicKey;
limited with Java.Security.SecureRandom;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;
with Java.Security.SignatureSpi;

package Java.Security.Signature is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Security.SignatureSpi.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      State : Java.Int;
      pragma Import (Java, State, "state");

   end record;

   --  protected
   function New_Signature (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Signature.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Security.Signature.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Java.Security.Signature.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   --  final
   procedure InitVerify (This : access Typ;
                         P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class);
   --  can raise Java.Security.InvalidKeyException.Except

   --  final
   procedure InitVerify (This : access Typ;
                         P1_Certificate : access Standard.Java.Security.Cert.Certificate.Typ'Class);
   --  can raise Java.Security.InvalidKeyException.Except

   --  final
   procedure InitSign (This : access Typ;
                       P1_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class);
   --  can raise Java.Security.InvalidKeyException.Except

   --  final
   procedure InitSign (This : access Typ;
                       P1_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class;
                       P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class);
   --  can raise Java.Security.InvalidKeyException.Except

   --  final
   function Sign (This : access Typ)
                  return Java.Byte_Arr;
   --  can raise Java.Security.SignatureException.Except

   --  final
   function Sign (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Security.SignatureException.Except

   --  final
   function Verify (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr)
                    return Java.Boolean;
   --  can raise Java.Security.SignatureException.Except

   --  final
   function Verify (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return Java.Boolean;
   --  can raise Java.Security.SignatureException.Except

   --  final
   procedure Update (This : access Typ;
                     P1_Byte : Java.Byte);
   --  can raise Java.Security.SignatureException.Except

   --  final
   procedure Update (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Security.SignatureException.Except

   --  final
   procedure Update (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);
   --  can raise Java.Security.SignatureException.Except

   --  final
   procedure Update (This : access Typ;
                     P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class);
   --  can raise Java.Security.SignatureException.Except

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   procedure SetParameter (This : access Typ;
                           P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   function GetParameters (This : access Typ)
                           return access Java.Security.AlgorithmParameters.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   UNINITIALIZED : constant Java.Int;

   --  protected  final
   SIGN_K : constant Java.Int;

   --  protected  final
   VERIFY_K : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Signature);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, InitVerify, "initVerify");
   pragma Import (Java, InitSign, "initSign");
   pragma Import (Java, Sign, "sign");
   pragma Import (Java, Verify, "verify");
   pragma Import (Java, Update, "update");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SetParameter, "setParameter");
   pragma Import (Java, GetParameters, "getParameters");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, UNINITIALIZED, "UNINITIALIZED");
   pragma Import (Java, SIGN_K, "SIGN");
   pragma Import (Java, VERIFY_K, "VERIFY");

end Java.Security.Signature;
pragma Import (Java, Java.Security.Signature, "java.security.Signature");
pragma Extensions_Allowed (Off);
