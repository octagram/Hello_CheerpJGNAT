pragma Extensions_Allowed (On);
limited with Java.Nio.ByteBuffer;
limited with Java.Security.AlgorithmParameters;
limited with Java.Security.PrivateKey;
limited with Java.Security.PublicKey;
limited with Java.Security.SecureRandom;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;

package Java.Security.SignatureSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      AppRandom : access Java.Security.SecureRandom.Typ'Class;
      pragma Import (Java, AppRandom, "appRandom");

   end record;

   function New_SignatureSpi (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure EngineInitVerify (This : access Typ;
                               P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class) is abstract;
   --  can raise Java.Security.InvalidKeyException.Except

   --  protected
   procedure EngineInitSign (This : access Typ;
                             P1_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class) is abstract;
   --  can raise Java.Security.InvalidKeyException.Except

   --  protected
   procedure EngineInitSign (This : access Typ;
                             P1_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class;
                             P2_SecureRandom : access Standard.Java.Security.SecureRandom.Typ'Class);
   --  can raise Java.Security.InvalidKeyException.Except

   --  protected
   procedure EngineUpdate (This : access Typ;
                           P1_Byte : Java.Byte) is abstract;
   --  can raise Java.Security.SignatureException.Except

   --  protected
   procedure EngineUpdate (This : access Typ;
                           P1_Byte_Arr : Java.Byte_Arr;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int) is abstract;
   --  can raise Java.Security.SignatureException.Except

   --  protected
   procedure EngineUpdate (This : access Typ;
                           P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class);

   --  protected
   function EngineSign (This : access Typ)
                        return Java.Byte_Arr is abstract;
   --  can raise Java.Security.SignatureException.Except

   --  protected
   function EngineSign (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int)
                        return Java.Int;
   --  can raise Java.Security.SignatureException.Except

   --  protected
   function EngineVerify (This : access Typ;
                          P1_Byte_Arr : Java.Byte_Arr)
                          return Java.Boolean is abstract;
   --  can raise Java.Security.SignatureException.Except

   --  protected
   function EngineVerify (This : access Typ;
                          P1_Byte_Arr : Java.Byte_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int)
                          return Java.Boolean;
   --  can raise Java.Security.SignatureException.Except

   --  protected
   procedure EngineSetParameter (This : access Typ;
                                 P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   --  protected
   function EngineGetParameters (This : access Typ)
                                 return access Java.Security.AlgorithmParameters.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SignatureSpi);
   pragma Export (Java, EngineInitVerify, "engineInitVerify");
   pragma Export (Java, EngineInitSign, "engineInitSign");
   pragma Export (Java, EngineUpdate, "engineUpdate");
   pragma Export (Java, EngineSign, "engineSign");
   pragma Export (Java, EngineVerify, "engineVerify");
   pragma Export (Java, EngineSetParameter, "engineSetParameter");
   pragma Export (Java, EngineGetParameters, "engineGetParameters");
   pragma Export (Java, Clone, "clone");

end Java.Security.SignatureSpi;
pragma Import (Java, Java.Security.SignatureSpi, "java.security.SignatureSpi");
pragma Extensions_Allowed (Off);
