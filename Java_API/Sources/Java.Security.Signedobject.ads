pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.PrivateKey;
limited with Java.Security.PublicKey;
limited with Java.Security.Signature;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.SignedObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SignedObject (P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class;
                              P2_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class;
                              P3_Signature : access Standard.Java.Security.Signature.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Java.Io.IOException.Except,
   --  Java.Security.InvalidKeyException.Except and
   --  Java.Security.SignatureException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetObject (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   function GetSignature (This : access Typ)
                          return Java.Byte_Arr;

   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function Verify (This : access Typ;
                    P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class;
                    P2_Signature : access Standard.Java.Security.Signature.Typ'Class)
                    return Java.Boolean;
   --  can raise Java.Security.InvalidKeyException.Except and
   --  Java.Security.SignatureException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SignedObject);
   pragma Import (Java, GetObject, "getObject");
   pragma Import (Java, GetSignature, "getSignature");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, Verify, "verify");

end Java.Security.SignedObject;
pragma Import (Java, Java.Security.SignedObject, "java.security.SignedObject");
pragma Extensions_Allowed (Off);
