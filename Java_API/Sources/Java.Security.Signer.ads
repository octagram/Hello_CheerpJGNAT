pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.IdentityScope;
limited with Java.Security.KeyPair;
limited with Java.Security.PrivateKey;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Identity;
with Java.Security.Principal;

package Java.Security.Signer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Principal_I : Java.Security.Principal.Ref)
    is abstract new Java.Security.Identity.Typ(Serializable_I,
                                               Principal_I)
      with null record;

   --  protected
   function New_Signer (This : Ref := null)
                        return Ref;

   function New_Signer (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Signer (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_IdentityScope : access Standard.Java.Security.IdentityScope.Typ'Class; 
                        This : Ref := null)
                        return Ref;
   --  can raise Java.Security.KeyManagementException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrivateKey (This : access Typ)
                           return access Java.Security.PrivateKey.Typ'Class;

   --  final
   procedure SetKeyPair (This : access Typ;
                         P1_KeyPair : access Standard.Java.Security.KeyPair.Typ'Class);
   --  can raise Java.Security.InvalidParameterException.Except and
   --  Java.Security.KeyException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Signer);
   pragma Import (Java, GetPrivateKey, "getPrivateKey");
   pragma Import (Java, SetKeyPair, "setKeyPair");
   pragma Import (Java, ToString, "toString");

end Java.Security.Signer;
pragma Import (Java, Java.Security.Signer, "java.security.Signer");
pragma Extensions_Allowed (Off);
