pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;
with Java.Security.Interfaces.DSAParams;
with Java.Security.Spec.AlgorithmParameterSpec;

package Java.Security.Spec.DSAParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DSAParams_I : Java.Security.Interfaces.DSAParams.Ref;
            AlgorithmParameterSpec_I : Java.Security.Spec.AlgorithmParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DSAParameterSpec (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                  P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                  P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetP (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;

   function GetQ (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;

   function GetG (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DSAParameterSpec);
   pragma Import (Java, GetP, "getP");
   pragma Import (Java, GetQ, "getQ");
   pragma Import (Java, GetG, "getG");

end Java.Security.Spec.DSAParameterSpec;
pragma Import (Java, Java.Security.Spec.DSAParameterSpec, "java.security.spec.DSAParameterSpec");
pragma Extensions_Allowed (Off);
