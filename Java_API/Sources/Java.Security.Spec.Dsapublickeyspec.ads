pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;
with Java.Security.Spec.KeySpec;

package Java.Security.Spec.DSAPublicKeySpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeySpec_I : Java.Security.Spec.KeySpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DSAPublicKeySpec (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                  P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                  P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                  P4_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetY (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;

   function GetP (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;

   function GetQ (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;

   function GetG (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DSAPublicKeySpec);
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetP, "getP");
   pragma Import (Java, GetQ, "getQ");
   pragma Import (Java, GetG, "getG");

end Java.Security.Spec.DSAPublicKeySpec;
pragma Import (Java, Java.Security.Spec.DSAPublicKeySpec, "java.security.spec.DSAPublicKeySpec");
pragma Extensions_Allowed (Off);
