pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;
with Java.Security.Spec.ECField;

package Java.Security.Spec.ECFieldF2m is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ECField_I : Java.Security.Spec.ECField.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ECFieldF2m (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_ECFieldF2m (P1_Int : Java.Int;
                            P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_ECFieldF2m (P1_Int : Java.Int;
                            P2_Int_Arr : Java.Int_Arr; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFieldSize (This : access Typ)
                          return Java.Int;

   function GetM (This : access Typ)
                  return Java.Int;

   function GetReductionPolynomial (This : access Typ)
                                    return access Java.Math.BigInteger.Typ'Class;

   function GetMidTermsOfReductionPolynomial (This : access Typ)
                                              return Java.Int_Arr;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ECFieldF2m);
   pragma Import (Java, GetFieldSize, "getFieldSize");
   pragma Import (Java, GetM, "getM");
   pragma Import (Java, GetReductionPolynomial, "getReductionPolynomial");
   pragma Import (Java, GetMidTermsOfReductionPolynomial, "getMidTermsOfReductionPolynomial");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Security.Spec.ECFieldF2m;
pragma Import (Java, Java.Security.Spec.ECFieldF2m, "java.security.spec.ECFieldF2m");
pragma Extensions_Allowed (Off);
