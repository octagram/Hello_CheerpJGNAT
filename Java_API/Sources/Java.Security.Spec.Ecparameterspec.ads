pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
limited with Java.Security.Spec.ECPoint;
limited with Java.Security.Spec.EllipticCurve;
with Java.Lang.Object;
with Java.Security.Spec.AlgorithmParameterSpec;

package Java.Security.Spec.ECParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AlgorithmParameterSpec_I : Java.Security.Spec.AlgorithmParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ECParameterSpec (P1_EllipticCurve : access Standard.Java.Security.Spec.EllipticCurve.Typ'Class;
                                 P2_ECPoint : access Standard.Java.Security.Spec.ECPoint.Typ'Class;
                                 P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                 P4_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCurve (This : access Typ)
                      return access Java.Security.Spec.EllipticCurve.Typ'Class;

   function GetGenerator (This : access Typ)
                          return access Java.Security.Spec.ECPoint.Typ'Class;

   function GetOrder (This : access Typ)
                      return access Java.Math.BigInteger.Typ'Class;

   function GetCofactor (This : access Typ)
                         return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ECParameterSpec);
   pragma Import (Java, GetCurve, "getCurve");
   pragma Import (Java, GetGenerator, "getGenerator");
   pragma Import (Java, GetOrder, "getOrder");
   pragma Import (Java, GetCofactor, "getCofactor");

end Java.Security.Spec.ECParameterSpec;
pragma Import (Java, Java.Security.Spec.ECParameterSpec, "java.security.spec.ECParameterSpec");
pragma Extensions_Allowed (Off);
