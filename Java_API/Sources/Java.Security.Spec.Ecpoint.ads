pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;

package Java.Security.Spec.ECPoint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ECPoint (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                         P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAffineX (This : access Typ)
                        return access Java.Math.BigInteger.Typ'Class;

   function GetAffineY (This : access Typ)
                        return access Java.Math.BigInteger.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   POINT_INFINITY : access Java.Security.Spec.ECPoint.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ECPoint);
   pragma Import (Java, GetAffineX, "getAffineX");
   pragma Import (Java, GetAffineY, "getAffineY");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, POINT_INFINITY, "POINT_INFINITY");

end Java.Security.Spec.ECPoint;
pragma Import (Java, Java.Security.Spec.ECPoint, "java.security.spec.ECPoint");
pragma Extensions_Allowed (Off);
