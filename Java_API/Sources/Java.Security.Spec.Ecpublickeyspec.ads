pragma Extensions_Allowed (On);
limited with Java.Security.Spec.ECParameterSpec;
limited with Java.Security.Spec.ECPoint;
with Java.Lang.Object;
with Java.Security.Spec.KeySpec;

package Java.Security.Spec.ECPublicKeySpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeySpec_I : Java.Security.Spec.KeySpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ECPublicKeySpec (P1_ECPoint : access Standard.Java.Security.Spec.ECPoint.Typ'Class;
                                 P2_ECParameterSpec : access Standard.Java.Security.Spec.ECParameterSpec.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetW (This : access Typ)
                  return access Java.Security.Spec.ECPoint.Typ'Class;

   function GetParams (This : access Typ)
                       return access Java.Security.Spec.ECParameterSpec.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ECPublicKeySpec);
   pragma Import (Java, GetW, "getW");
   pragma Import (Java, GetParams, "getParams");

end Java.Security.Spec.ECPublicKeySpec;
pragma Import (Java, Java.Security.Spec.ECPublicKeySpec, "java.security.spec.ECPublicKeySpec");
pragma Extensions_Allowed (Off);
