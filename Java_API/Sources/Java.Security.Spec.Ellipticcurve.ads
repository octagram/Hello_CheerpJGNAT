pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
limited with Java.Security.Spec.ECField;
with Java.Lang.Object;

package Java.Security.Spec.EllipticCurve is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EllipticCurve (P1_ECField : access Standard.Java.Security.Spec.ECField.Typ'Class;
                               P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                               P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_EllipticCurve (P1_ECField : access Standard.Java.Security.Spec.ECField.Typ'Class;
                               P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                               P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                               P4_Byte_Arr : Java.Byte_Arr; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetField (This : access Typ)
                      return access Java.Security.Spec.ECField.Typ'Class;

   function GetA (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;

   function GetB (This : access Typ)
                  return access Java.Math.BigInteger.Typ'Class;

   function GetSeed (This : access Typ)
                     return Java.Byte_Arr;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EllipticCurve);
   pragma Import (Java, GetField, "getField");
   pragma Import (Java, GetA, "getA");
   pragma Import (Java, GetB, "getB");
   pragma Import (Java, GetSeed, "getSeed");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Security.Spec.EllipticCurve;
pragma Import (Java, Java.Security.Spec.EllipticCurve, "java.security.spec.EllipticCurve");
pragma Extensions_Allowed (Off);
