pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Security.Spec.KeySpec;

package Java.Security.Spec.EncodedKeySpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeySpec_I : Java.Security.Spec.KeySpec.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_EncodedKeySpec (P1_Byte_Arr : Java.Byte_Arr; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr;

   function GetFormat (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EncodedKeySpec);
   pragma Export (Java, GetEncoded, "getEncoded");
   pragma Export (Java, GetFormat, "getFormat");

end Java.Security.Spec.EncodedKeySpec;
pragma Import (Java, Java.Security.Spec.EncodedKeySpec, "java.security.spec.EncodedKeySpec");
pragma Extensions_Allowed (Off);
