pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Security.Spec.AlgorithmParameterSpec;

package Java.Security.Spec.MGF1ParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AlgorithmParameterSpec_I : Java.Security.Spec.AlgorithmParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MGF1ParameterSpec (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDigestAlgorithm (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SHA1 : access Java.Security.Spec.MGF1ParameterSpec.Typ'Class;

   --  final
   SHA256 : access Java.Security.Spec.MGF1ParameterSpec.Typ'Class;

   --  final
   SHA384 : access Java.Security.Spec.MGF1ParameterSpec.Typ'Class;

   --  final
   SHA512 : access Java.Security.Spec.MGF1ParameterSpec.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MGF1ParameterSpec);
   pragma Import (Java, GetDigestAlgorithm, "getDigestAlgorithm");
   pragma Import (Java, SHA1, "SHA1");
   pragma Import (Java, SHA256, "SHA256");
   pragma Import (Java, SHA384, "SHA384");
   pragma Import (Java, SHA512, "SHA512");

end Java.Security.Spec.MGF1ParameterSpec;
pragma Import (Java, Java.Security.Spec.MGF1ParameterSpec, "java.security.spec.MGF1ParameterSpec");
pragma Extensions_Allowed (Off);
