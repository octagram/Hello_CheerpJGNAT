pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Security.Spec.AlgorithmParameterSpec;

package Java.Security.Spec.PSSParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AlgorithmParameterSpec_I : Java.Security.Spec.AlgorithmParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PSSParameterSpec (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   function New_PSSParameterSpec (P1_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDigestAlgorithm (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function GetMGFAlgorithm (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetMGFParameters (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;

   function GetSaltLength (This : access Typ)
                           return Java.Int;

   function GetTrailerField (This : access Typ)
                             return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT : access Java.Security.Spec.PSSParameterSpec.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PSSParameterSpec);
   pragma Import (Java, GetDigestAlgorithm, "getDigestAlgorithm");
   pragma Import (Java, GetMGFAlgorithm, "getMGFAlgorithm");
   pragma Import (Java, GetMGFParameters, "getMGFParameters");
   pragma Import (Java, GetSaltLength, "getSaltLength");
   pragma Import (Java, GetTrailerField, "getTrailerField");
   pragma Import (Java, DEFAULT, "DEFAULT");

end Java.Security.Spec.PSSParameterSpec;
pragma Import (Java, Java.Security.Spec.PSSParameterSpec, "java.security.spec.PSSParameterSpec");
pragma Extensions_Allowed (Off);
