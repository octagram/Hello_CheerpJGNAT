pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;
with Java.Security.Spec.AlgorithmParameterSpec;

package Java.Security.Spec.RSAKeyGenParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AlgorithmParameterSpec_I : Java.Security.Spec.AlgorithmParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RSAKeyGenParameterSpec (P1_Int : Java.Int;
                                        P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKeysize (This : access Typ)
                        return Java.Int;

   function GetPublicExponent (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   F0 : access Java.Math.BigInteger.Typ'Class;

   --  final
   F4 : access Java.Math.BigInteger.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RSAKeyGenParameterSpec);
   pragma Import (Java, GetKeysize, "getKeysize");
   pragma Import (Java, GetPublicExponent, "getPublicExponent");
   pragma Import (Java, F0, "F0");
   pragma Import (Java, F4, "F4");

end Java.Security.Spec.RSAKeyGenParameterSpec;
pragma Import (Java, Java.Security.Spec.RSAKeyGenParameterSpec, "java.security.spec.RSAKeyGenParameterSpec");
pragma Extensions_Allowed (Off);
