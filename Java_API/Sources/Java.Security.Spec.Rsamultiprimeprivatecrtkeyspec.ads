pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
limited with Java.Security.Spec.RSAOtherPrimeInfo;
with Java.Lang.Object;
with Java.Security.Spec.KeySpec;
with Java.Security.Spec.RSAPrivateKeySpec;

package Java.Security.Spec.RSAMultiPrimePrivateCrtKeySpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeySpec_I : Java.Security.Spec.KeySpec.Ref)
    is new Java.Security.Spec.RSAPrivateKeySpec.Typ(KeySpec_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RSAMultiPrimePrivateCrtKeySpec (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P4_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P5_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P6_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P7_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P8_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                                P9_RSAOtherPrimeInfo_Arr : access Java.Security.Spec.RSAOtherPrimeInfo.Arr_Obj; 
                                                This : Ref := null)
                                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublicExponent (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;

   function GetPrimeP (This : access Typ)
                       return access Java.Math.BigInteger.Typ'Class;

   function GetPrimeQ (This : access Typ)
                       return access Java.Math.BigInteger.Typ'Class;

   function GetPrimeExponentP (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;

   function GetPrimeExponentQ (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;

   function GetCrtCoefficient (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;

   function GetOtherPrimeInfo (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RSAMultiPrimePrivateCrtKeySpec);
   pragma Import (Java, GetPublicExponent, "getPublicExponent");
   pragma Import (Java, GetPrimeP, "getPrimeP");
   pragma Import (Java, GetPrimeQ, "getPrimeQ");
   pragma Import (Java, GetPrimeExponentP, "getPrimeExponentP");
   pragma Import (Java, GetPrimeExponentQ, "getPrimeExponentQ");
   pragma Import (Java, GetCrtCoefficient, "getCrtCoefficient");
   pragma Import (Java, GetOtherPrimeInfo, "getOtherPrimeInfo");

end Java.Security.Spec.RSAMultiPrimePrivateCrtKeySpec;
pragma Import (Java, Java.Security.Spec.RSAMultiPrimePrivateCrtKeySpec, "java.security.spec.RSAMultiPrimePrivateCrtKeySpec");
pragma Extensions_Allowed (Off);
