pragma Extensions_Allowed (On);
limited with Java.Math.BigInteger;
with Java.Lang.Object;

package Java.Security.Spec.RSAOtherPrimeInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RSAOtherPrimeInfo (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                   P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                   P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetPrime (This : access Typ)
                      return access Java.Math.BigInteger.Typ'Class;

   --  final
   function GetExponent (This : access Typ)
                         return access Java.Math.BigInteger.Typ'Class;

   --  final
   function GetCrtCoefficient (This : access Typ)
                               return access Java.Math.BigInteger.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RSAOtherPrimeInfo);
   pragma Import (Java, GetPrime, "getPrime");
   pragma Import (Java, GetExponent, "getExponent");
   pragma Import (Java, GetCrtCoefficient, "getCrtCoefficient");

end Java.Security.Spec.RSAOtherPrimeInfo;
pragma Import (Java, Java.Security.Spec.RSAOtherPrimeInfo, "java.security.spec.RSAOtherPrimeInfo");
pragma Extensions_Allowed (Off);
