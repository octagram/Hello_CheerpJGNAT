pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Security.Spec.EncodedKeySpec;
with Java.Security.Spec.KeySpec;

package Java.Security.Spec.X509EncodedKeySpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeySpec_I : Java.Security.Spec.KeySpec.Ref)
    is new Java.Security.Spec.EncodedKeySpec.Typ(KeySpec_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_X509EncodedKeySpec (P1_Byte_Arr : Java.Byte_Arr; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr;

   --  final
   function GetFormat (This : access Typ)
                       return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X509EncodedKeySpec);
   pragma Import (Java, GetEncoded, "getEncoded");
   pragma Import (Java, GetFormat, "getFormat");

end Java.Security.Spec.X509EncodedKeySpec;
pragma Import (Java, Java.Security.Spec.X509EncodedKeySpec, "java.security.spec.X509EncodedKeySpec");
pragma Extensions_Allowed (Off);
