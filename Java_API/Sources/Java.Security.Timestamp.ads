pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.CertPath;
limited with Java.Util.Date;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Security.Timestamp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Timestamp (P1_Date : access Standard.Java.Util.Date.Typ'Class;
                           P2_CertPath : access Standard.Java.Security.Cert.CertPath.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTimestamp (This : access Typ)
                          return access Java.Util.Date.Typ'Class;

   function GetSignerCertPath (This : access Typ)
                               return access Java.Security.Cert.CertPath.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Timestamp);
   pragma Import (Java, GetTimestamp, "getTimestamp");
   pragma Import (Java, GetSignerCertPath, "getSignerCertPath");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");

end Java.Security.Timestamp;
pragma Import (Java, Java.Security.Timestamp, "java.security.Timestamp");
pragma Extensions_Allowed (Off);
