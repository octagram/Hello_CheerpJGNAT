pragma Extensions_Allowed (On);
limited with Java.Net.URI;
with Java.Lang.Object;
with Java.Security.Policy.Parameters;
with Javax.Security.Auth.Login.Configuration.Parameters;

package Java.Security.URIParameter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Parameters_I : Java.Security.Policy.Parameters.Ref;
            Configuration_Parameters_I : Javax.Security.Auth.Login.Configuration.Parameters.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_URIParameter (P1_URI : access Standard.Java.Net.URI.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetURI (This : access Typ)
                    return access Java.Net.URI.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_URIParameter);
   pragma Import (Java, GetURI, "getURI");

end Java.Security.URIParameter;
pragma Import (Java, Java.Security.URIParameter, "java.security.URIParameter");
pragma Extensions_Allowed (Off);
