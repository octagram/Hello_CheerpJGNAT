pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Sql.ResultSet;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Sql.Array_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBaseTypeName (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBaseType (This : access Typ)
                         return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ;
                      P1_Map : access Standard.Java.Util.Map.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Map : access Standard.Java.Util.Map.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSet (This : access Typ)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSet (This : access Typ;
                          P1_Map : access Standard.Java.Util.Map.Typ'Class)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSet (This : access Typ;
                          P1_Long : Java.Long;
                          P2_Int : Java.Int)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSet (This : access Typ;
                          P1_Long : Java.Long;
                          P2_Int : Java.Int;
                          P3_Map : access Standard.Java.Util.Map.Typ'Class)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Free (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetBaseTypeName, "getBaseTypeName");
   pragma Export (Java, GetBaseType, "getBaseType");
   pragma Export (Java, GetArray, "getArray");
   pragma Export (Java, GetResultSet, "getResultSet");
   pragma Export (Java, Free, "free");

end Java.Sql.Array_K;
pragma Import (Java, Java.Sql.Array_K, "java.sql.Array");
pragma Extensions_Allowed (Off);
