pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
with Java.Lang.Object;

package Java.Sql.Blob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Length (This : access Typ)
                    return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBytes (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int)
                      return Java.Byte_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBinaryStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Position (This : access Typ;
                      P1_Byte_Arr : Java.Byte_Arr;
                      P2_Long : Java.Long)
                      return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Position (This : access Typ;
                      P1_Blob : access Standard.Java.Sql.Blob.Typ'Class;
                      P2_Long : Java.Long)
                      return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetBytes (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Byte_Arr : Java.Byte_Arr)
                      return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetBytes (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Byte_Arr : Java.Byte_Arr;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int)
                      return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetBinaryStream (This : access Typ;
                             P1_Long : Java.Long)
                             return access Java.Io.OutputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Truncate (This : access Typ;
                       P1_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Free (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBinaryStream (This : access Typ;
                             P1_Long : Java.Long;
                             P2_Long : Java.Long)
                             return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Length, "length");
   pragma Export (Java, GetBytes, "getBytes");
   pragma Export (Java, GetBinaryStream, "getBinaryStream");
   pragma Export (Java, Position, "position");
   pragma Export (Java, SetBytes, "setBytes");
   pragma Export (Java, SetBinaryStream, "setBinaryStream");
   pragma Export (Java, Truncate, "truncate");
   pragma Export (Java, Free, "free");

end Java.Sql.Blob;
pragma Import (Java, Java.Sql.Blob, "java.sql.Blob");
pragma Extensions_Allowed (Off);
