pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
limited with Java.Util.Calendar;
limited with Java.Util.Map;
with Java.Lang.Object;
with Java.Sql.PreparedStatement;

package Java.Sql.CallableStatement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            PreparedStatement_I : Java.Sql.PreparedStatement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure RegisterOutParameter (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure RegisterOutParameter (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function WasNull (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetString (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBoolean (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetByte (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Byte is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetShort (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Short is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetInt (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetLong (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFloat (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Float is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDouble (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Double is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBytes (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Byte_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBigDecimal (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Math.BigDecimal.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Map : access Standard.Java.Util.Map.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRef (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Sql.Ref.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBlob (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Blob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetClob (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Clob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Sql.Array_K.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure RegisterOutParameter (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure RegisterOutParameter (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure RegisterOutParameter (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure RegisterOutParameter (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Int : Java.Int;
                                   P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetURL (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Net.URL.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetURL (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_URL : access Standard.Java.Net.URL.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBoolean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetByte (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Byte : Java.Byte) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetShort (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Short : Java.Short) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetInt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetLong (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFloat (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Float : Java.Float) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDouble (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Double : Java.Double) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBigDecimal (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetString (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBytes (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                             P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                 P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class;
                           P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int;
                      P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetString (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBoolean (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetByte (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Byte is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetShort (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Short is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetInt (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetLong (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFloat (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Float is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDouble (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Double is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBytes (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Byte_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBigDecimal (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Math.BigDecimal.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Map : access Standard.Java.Util.Map.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRef (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Sql.Ref.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBlob (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Blob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetClob (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Clob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Sql.Array_K.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetURL (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Net.URL.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowId (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Sql.RowId.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowId (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Sql.RowId.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetRowId (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_RowId : access Standard.Java.Sql.RowId.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                  P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_NClob : access Standard.Java.Sql.NClob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                      P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                      P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                       P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNClob (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Sql.NClob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Sql.NClob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetSQLXML (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSQLXML (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Sql.SQLXML.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSQLXML (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Sql.SQLXML.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNString (This : access Typ;
                        P1_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNString (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNCharacterStream (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCharacterStream (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCharacterStream (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Blob : access Standard.Java.Sql.Blob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Clob : access Standard.Java.Sql.Clob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                             P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                 P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, RegisterOutParameter, "registerOutParameter");
   pragma Export (Java, WasNull, "wasNull");
   pragma Export (Java, GetString, "getString");
   pragma Export (Java, GetBoolean, "getBoolean");
   pragma Export (Java, GetByte, "getByte");
   pragma Export (Java, GetShort, "getShort");
   pragma Export (Java, GetInt, "getInt");
   pragma Export (Java, GetLong, "getLong");
   pragma Export (Java, GetFloat, "getFloat");
   pragma Export (Java, GetDouble, "getDouble");
   pragma Export (Java, GetBytes, "getBytes");
   pragma Export (Java, GetDate, "getDate");
   pragma Export (Java, GetTime, "getTime");
   pragma Export (Java, GetTimestamp, "getTimestamp");
   pragma Export (Java, GetObject, "getObject");
   pragma Export (Java, GetBigDecimal, "getBigDecimal");
   pragma Export (Java, GetRef, "getRef");
   pragma Export (Java, GetBlob, "getBlob");
   pragma Export (Java, GetClob, "getClob");
   pragma Export (Java, GetArray, "getArray");
   pragma Export (Java, GetURL, "getURL");
   pragma Export (Java, SetURL, "setURL");
   pragma Export (Java, SetNull, "setNull");
   pragma Export (Java, SetBoolean, "setBoolean");
   pragma Export (Java, SetByte, "setByte");
   pragma Export (Java, SetShort, "setShort");
   pragma Export (Java, SetInt, "setInt");
   pragma Export (Java, SetLong, "setLong");
   pragma Export (Java, SetFloat, "setFloat");
   pragma Export (Java, SetDouble, "setDouble");
   pragma Export (Java, SetBigDecimal, "setBigDecimal");
   pragma Export (Java, SetString, "setString");
   pragma Export (Java, SetBytes, "setBytes");
   pragma Export (Java, SetDate, "setDate");
   pragma Export (Java, SetTime, "setTime");
   pragma Export (Java, SetTimestamp, "setTimestamp");
   pragma Export (Java, SetAsciiStream, "setAsciiStream");
   pragma Export (Java, SetBinaryStream, "setBinaryStream");
   pragma Export (Java, SetObject, "setObject");
   pragma Export (Java, SetCharacterStream, "setCharacterStream");
   pragma Export (Java, GetRowId, "getRowId");
   pragma Export (Java, SetRowId, "setRowId");
   pragma Export (Java, SetNString, "setNString");
   pragma Export (Java, SetNCharacterStream, "setNCharacterStream");
   pragma Export (Java, SetNClob, "setNClob");
   pragma Export (Java, SetClob, "setClob");
   pragma Export (Java, SetBlob, "setBlob");
   pragma Export (Java, GetNClob, "getNClob");
   pragma Export (Java, SetSQLXML, "setSQLXML");
   pragma Export (Java, GetSQLXML, "getSQLXML");
   pragma Export (Java, GetNString, "getNString");
   pragma Export (Java, GetNCharacterStream, "getNCharacterStream");
   pragma Export (Java, GetCharacterStream, "getCharacterStream");

end Java.Sql.CallableStatement;
pragma Import (Java, Java.Sql.CallableStatement, "java.sql.CallableStatement");
pragma Extensions_Allowed (Off);
