pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Sql.Clob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Length (This : access Typ)
                    return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSubString (This : access Typ;
                          P1_Long : Java.Long;
                          P2_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCharacterStream (This : access Typ)
                                return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAsciiStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Position (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Long : Java.Long)
                      return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Position (This : access Typ;
                      P1_Clob : access Standard.Java.Sql.Clob.Typ'Class;
                      P2_Long : Java.Long)
                      return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetString (This : access Typ;
                       P1_Long : Java.Long;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetString (This : access Typ;
                       P1_Long : Java.Long;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int)
                       return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetAsciiStream (This : access Typ;
                            P1_Long : Java.Long)
                            return access Java.Io.OutputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetCharacterStream (This : access Typ;
                                P1_Long : Java.Long)
                                return access Java.Io.Writer.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Truncate (This : access Typ;
                       P1_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Free (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCharacterStream (This : access Typ;
                                P1_Long : Java.Long;
                                P2_Long : Java.Long)
                                return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Length, "length");
   pragma Export (Java, GetSubString, "getSubString");
   pragma Export (Java, GetCharacterStream, "getCharacterStream");
   pragma Export (Java, GetAsciiStream, "getAsciiStream");
   pragma Export (Java, Position, "position");
   pragma Export (Java, SetString, "setString");
   pragma Export (Java, SetAsciiStream, "setAsciiStream");
   pragma Export (Java, SetCharacterStream, "setCharacterStream");
   pragma Export (Java, Truncate, "truncate");
   pragma Export (Java, Free, "free");

end Java.Sql.Clob;
pragma Import (Java, Java.Sql.Clob, "java.sql.Clob");
pragma Extensions_Allowed (Off);
