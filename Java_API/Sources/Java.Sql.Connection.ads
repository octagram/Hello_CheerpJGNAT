pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.CallableStatement;
limited with Java.Sql.Clob;
limited with Java.Sql.DatabaseMetaData;
limited with Java.Sql.NClob;
limited with Java.Sql.PreparedStatement;
limited with Java.Sql.SQLWarning;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Savepoint;
limited with Java.Sql.Statement;
limited with Java.Sql.Struct;
limited with Java.Util.Map;
limited with Java.Util.Properties;
with Java.Lang.Object;
with Java.Sql.Wrapper;

package Java.Sql.Connection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Wrapper_I : Java.Sql.Wrapper.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateStatement (This : access Typ)
                             return access Java.Sql.Statement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareStatement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Sql.PreparedStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareCall (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Sql.CallableStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function NativeSQL (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAutoCommit (This : access Typ;
                            P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAutoCommit (This : access Typ)
                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Commit (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsClosed (This : access Typ)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMetaData (This : access Typ)
                         return access Java.Sql.DatabaseMetaData.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetReadOnly (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsReadOnly (This : access Typ)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCatalog (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCatalog (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTransactionIsolation (This : access Typ;
                                      P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTransactionIsolation (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetWarnings (This : access Typ)
                         return access Java.Sql.SQLWarning.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ClearWarnings (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateStatement (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Java.Sql.Statement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareStatement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int)
                              return access Java.Sql.PreparedStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareCall (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return access Java.Sql.CallableStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTypeMap (This : access Typ)
                        return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTypeMap (This : access Typ;
                         P1_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetHoldability (This : access Typ;
                             P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetHoldability (This : access Typ)
                            return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetSavepoint (This : access Typ)
                          return access Java.Sql.Savepoint.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetSavepoint (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Sql.Savepoint.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ;
                       P1_Savepoint : access Standard.Java.Sql.Savepoint.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ReleaseSavepoint (This : access Typ;
                               P1_Savepoint : access Standard.Java.Sql.Savepoint.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateStatement (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int)
                             return access Java.Sql.Statement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareStatement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int)
                              return access Java.Sql.PreparedStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareCall (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int)
                         return access Java.Sql.CallableStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareStatement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Int : Java.Int)
                              return access Java.Sql.PreparedStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareStatement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Int_Arr : Java.Int_Arr)
                              return access Java.Sql.PreparedStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PrepareStatement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String_Arr : access Java.Lang.String.Arr_Obj)
                              return access Java.Sql.PreparedStatement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateClob (This : access Typ)
                        return access Java.Sql.Clob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateBlob (This : access Typ)
                        return access Java.Sql.Blob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateNClob (This : access Typ)
                         return access Java.Sql.NClob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateSQLXML (This : access Typ)
                          return access Java.Sql.SQLXML.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsValid (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClientInfo (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLClientInfoException.Except

   procedure SetClientInfo (This : access Typ;
                            P1_Properties : access Standard.Java.Util.Properties.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLClientInfoException.Except

   function GetClientInfo (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetClientInfo (This : access Typ)
                           return access Java.Util.Properties.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateArrayOf (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                           return access Java.Sql.Array_K.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateStruct (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                          return access Java.Sql.Struct.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TRANSACTION_NONE : constant Java.Int;

   --  final
   TRANSACTION_READ_UNCOMMITTED : constant Java.Int;

   --  final
   TRANSACTION_READ_COMMITTED : constant Java.Int;

   --  final
   TRANSACTION_REPEATABLE_READ : constant Java.Int;

   --  final
   TRANSACTION_SERIALIZABLE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateStatement, "createStatement");
   pragma Export (Java, PrepareStatement, "prepareStatement");
   pragma Export (Java, PrepareCall, "prepareCall");
   pragma Export (Java, NativeSQL, "nativeSQL");
   pragma Export (Java, SetAutoCommit, "setAutoCommit");
   pragma Export (Java, GetAutoCommit, "getAutoCommit");
   pragma Export (Java, Commit, "commit");
   pragma Export (Java, Rollback, "rollback");
   pragma Export (Java, Close, "close");
   pragma Export (Java, IsClosed, "isClosed");
   pragma Export (Java, GetMetaData, "getMetaData");
   pragma Export (Java, SetReadOnly, "setReadOnly");
   pragma Export (Java, IsReadOnly, "isReadOnly");
   pragma Export (Java, SetCatalog, "setCatalog");
   pragma Export (Java, GetCatalog, "getCatalog");
   pragma Export (Java, SetTransactionIsolation, "setTransactionIsolation");
   pragma Export (Java, GetTransactionIsolation, "getTransactionIsolation");
   pragma Export (Java, GetWarnings, "getWarnings");
   pragma Export (Java, ClearWarnings, "clearWarnings");
   pragma Export (Java, GetTypeMap, "getTypeMap");
   pragma Export (Java, SetTypeMap, "setTypeMap");
   pragma Export (Java, SetHoldability, "setHoldability");
   pragma Export (Java, GetHoldability, "getHoldability");
   pragma Export (Java, SetSavepoint, "setSavepoint");
   pragma Export (Java, ReleaseSavepoint, "releaseSavepoint");
   pragma Export (Java, CreateClob, "createClob");
   pragma Export (Java, CreateBlob, "createBlob");
   pragma Export (Java, CreateNClob, "createNClob");
   pragma Export (Java, CreateSQLXML, "createSQLXML");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, SetClientInfo, "setClientInfo");
   pragma Export (Java, GetClientInfo, "getClientInfo");
   pragma Export (Java, CreateArrayOf, "createArrayOf");
   pragma Export (Java, CreateStruct, "createStruct");
   pragma Import (Java, TRANSACTION_NONE, "TRANSACTION_NONE");
   pragma Import (Java, TRANSACTION_READ_UNCOMMITTED, "TRANSACTION_READ_UNCOMMITTED");
   pragma Import (Java, TRANSACTION_READ_COMMITTED, "TRANSACTION_READ_COMMITTED");
   pragma Import (Java, TRANSACTION_REPEATABLE_READ, "TRANSACTION_REPEATABLE_READ");
   pragma Import (Java, TRANSACTION_SERIALIZABLE, "TRANSACTION_SERIALIZABLE");

end Java.Sql.Connection;
pragma Import (Java, Java.Sql.Connection, "java.sql.Connection");
pragma Extensions_Allowed (Off);
