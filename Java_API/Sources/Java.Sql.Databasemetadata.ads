pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Sql.Connection;
limited with Java.Sql.ResultSet;
limited with Java.Sql.RowIdLifetime;
with Java.Lang.Object;
with Java.Sql.Wrapper;

package Java.Sql.DatabaseMetaData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Wrapper_I : Java.Sql.Wrapper.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AllProceduresAreCallable (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function AllTablesAreSelectable (This : access Typ)
                                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetURL (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetUserName (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsReadOnly (This : access Typ)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function NullsAreSortedHigh (This : access Typ)
                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function NullsAreSortedLow (This : access Typ)
                               return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function NullsAreSortedAtStart (This : access Typ)
                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function NullsAreSortedAtEnd (This : access Typ)
                                 return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDatabaseProductName (This : access Typ)
                                    return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDatabaseProductVersion (This : access Typ)
                                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDriverName (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDriverVersion (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDriverMajorVersion (This : access Typ)
                                   return Java.Int is abstract;

   function GetDriverMinorVersion (This : access Typ)
                                   return Java.Int is abstract;

   function UsesLocalFiles (This : access Typ)
                            return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function UsesLocalFilePerTable (This : access Typ)
                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsMixedCaseIdentifiers (This : access Typ)
                                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function StoresUpperCaseIdentifiers (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function StoresLowerCaseIdentifiers (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function StoresMixedCaseIdentifiers (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsMixedCaseQuotedIdentifiers (This : access Typ)
                                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function StoresUpperCaseQuotedIdentifiers (This : access Typ)
                                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function StoresLowerCaseQuotedIdentifiers (This : access Typ)
                                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function StoresMixedCaseQuotedIdentifiers (This : access Typ)
                                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetIdentifierQuoteString (This : access Typ)
                                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSQLKeywords (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNumericFunctions (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetStringFunctions (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSystemFunctions (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimeDateFunctions (This : access Typ)
                                  return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSearchStringEscape (This : access Typ)
                                   return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetExtraNameCharacters (This : access Typ)
                                    return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsAlterTableWithAddColumn (This : access Typ)
                                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsAlterTableWithDropColumn (This : access Typ)
                                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsColumnAliasing (This : access Typ)
                                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function NullPlusNonNullIsNull (This : access Typ)
                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsConvert (This : access Typ)
                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsConvert (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsTableCorrelationNames (This : access Typ)
                                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsDifferentTableCorrelationNames (This : access Typ)
                                                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsExpressionsInOrderBy (This : access Typ)
                                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsOrderByUnrelated (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsGroupBy (This : access Typ)
                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsGroupByUnrelated (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsGroupByBeyondSelect (This : access Typ)
                                         return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsLikeEscapeClause (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsMultipleResultSets (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsMultipleTransactions (This : access Typ)
                                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsNonNullableColumns (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsMinimumSQLGrammar (This : access Typ)
                                       return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCoreSQLGrammar (This : access Typ)
                                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsExtendedSQLGrammar (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsANSI92EntryLevelSQL (This : access Typ)
                                         return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsANSI92IntermediateSQL (This : access Typ)
                                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsANSI92FullSQL (This : access Typ)
                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsIntegrityEnhancementFacility (This : access Typ)
                                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsOuterJoins (This : access Typ)
                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsFullOuterJoins (This : access Typ)
                                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsLimitedOuterJoins (This : access Typ)
                                       return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSchemaTerm (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetProcedureTerm (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCatalogTerm (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsCatalogAtStart (This : access Typ)
                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCatalogSeparator (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSchemasInDataManipulation (This : access Typ)
                                               return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSchemasInProcedureCalls (This : access Typ)
                                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSchemasInTableDefinitions (This : access Typ)
                                               return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSchemasInIndexDefinitions (This : access Typ)
                                               return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSchemasInPrivilegeDefinitions (This : access Typ)
                                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCatalogsInDataManipulation (This : access Typ)
                                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCatalogsInProcedureCalls (This : access Typ)
                                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCatalogsInTableDefinitions (This : access Typ)
                                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCatalogsInIndexDefinitions (This : access Typ)
                                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCatalogsInPrivilegeDefinitions (This : access Typ)
                                                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsPositionedDelete (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsPositionedUpdate (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSelectForUpdate (This : access Typ)
                                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsStoredProcedures (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSubqueriesInComparisons (This : access Typ)
                                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSubqueriesInExists (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSubqueriesInIns (This : access Typ)
                                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSubqueriesInQuantifieds (This : access Typ)
                                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCorrelatedSubqueries (This : access Typ)
                                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsUnion (This : access Typ)
                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsUnionAll (This : access Typ)
                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsOpenCursorsAcrossCommit (This : access Typ)
                                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsOpenCursorsAcrossRollback (This : access Typ)
                                               return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsOpenStatementsAcrossCommit (This : access Typ)
                                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsOpenStatementsAcrossRollback (This : access Typ)
                                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxBinaryLiteralLength (This : access Typ)
                                       return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxCharLiteralLength (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxColumnNameLength (This : access Typ)
                                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxColumnsInGroupBy (This : access Typ)
                                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxColumnsInIndex (This : access Typ)
                                  return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxColumnsInOrderBy (This : access Typ)
                                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxColumnsInSelect (This : access Typ)
                                   return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxColumnsInTable (This : access Typ)
                                  return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxConnections (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxCursorNameLength (This : access Typ)
                                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxIndexLength (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxSchemaNameLength (This : access Typ)
                                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxProcedureNameLength (This : access Typ)
                                       return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxCatalogNameLength (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxRowSize (This : access Typ)
                           return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function DoesMaxRowSizeIncludeBlobs (This : access Typ)
                                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxStatementLength (This : access Typ)
                                   return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxStatements (This : access Typ)
                              return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxTableNameLength (This : access Typ)
                                   return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxTablesInSelect (This : access Typ)
                                  return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxUserNameLength (This : access Typ)
                                  return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDefaultTransactionIsolation (This : access Typ)
                                            return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsTransactions (This : access Typ)
                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsTransactionIsolationLevel (This : access Typ;
                                               P1_Int : Java.Int)
                                               return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsDataDefinitionAndDataManipulationTransactions (This : access Typ)
                                                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsDataManipulationTransactionsOnly (This : access Typ)
                                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function DataDefinitionCausesTransactionCommit (This : access Typ)
                                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function DataDefinitionIgnoredInTransactions (This : access Typ)
                                                 return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetProcedures (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetProcedureColumns (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTables (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class;
                       P4_String_Arr : access Java.Lang.String.Arr_Obj)
                       return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSchemas (This : access Typ)
                        return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCatalogs (This : access Typ)
                         return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTableTypes (This : access Typ)
                           return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumns (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                        P4_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnPrivileges (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTablePrivileges (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBestRowIdentifier (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_String : access Standard.Java.Lang.String.Typ'Class;
                                  P4_Int : Java.Int;
                                  P5_Boolean : Java.Boolean)
                                  return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetVersionColumns (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetPrimaryKeys (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetImportedKeys (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetExportedKeys (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCrossReference (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class;
                               P4_String : access Standard.Java.Lang.String.Typ'Class;
                               P5_String : access Standard.Java.Lang.String.Typ'Class;
                               P6_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTypeInfo (This : access Typ)
                         return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetIndexInfo (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_String : access Standard.Java.Lang.String.Typ'Class;
                          P4_Boolean : Java.Boolean;
                          P5_Boolean : Java.Boolean)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsResultSetType (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsResultSetConcurrency (This : access Typ;
                                          P1_Int : Java.Int;
                                          P2_Int : Java.Int)
                                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function OwnUpdatesAreVisible (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function OwnDeletesAreVisible (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function OwnInsertsAreVisible (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function OthersUpdatesAreVisible (This : access Typ;
                                     P1_Int : Java.Int)
                                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function OthersDeletesAreVisible (This : access Typ;
                                     P1_Int : Java.Int)
                                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function OthersInsertsAreVisible (This : access Typ;
                                     P1_Int : Java.Int)
                                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function UpdatesAreDetected (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function DeletesAreDetected (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function InsertsAreDetected (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsBatchUpdates (This : access Typ)
                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetUDTs (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                     P4_Int_Arr : Java.Int_Arr)
                     return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetConnection (This : access Typ)
                           return access Java.Sql.Connection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsSavepoints (This : access Typ)
                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsNamedParameters (This : access Typ)
                                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsMultipleOpenResults (This : access Typ)
                                         return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsGetGeneratedKeys (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSuperTypes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSuperTables (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAttributes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsResultSetHoldability (This : access Typ;
                                          P1_Int : Java.Int)
                                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSetHoldability (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDatabaseMajorVersion (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDatabaseMinorVersion (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetJDBCMajorVersion (This : access Typ)
                                 return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetJDBCMinorVersion (This : access Typ)
                                 return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSQLStateType (This : access Typ)
                             return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function LocatorsUpdateCopy (This : access Typ)
                                return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsStatementPooling (This : access Typ)
                                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowIdLifetime (This : access Typ)
                              return access Java.Sql.RowIdLifetime.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSchemas (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsStoredFunctionsUsingCallSyntax (This : access Typ)
                                                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function AutoCommitFailureClosesAllResultSets (This : access Typ)
                                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetClientInfoProperties (This : access Typ)
                                     return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFunctions (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFunctionColumns (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ProcedureResultUnknown : constant Java.Int;

   --  final
   ProcedureNoResult : constant Java.Int;

   --  final
   ProcedureReturnsResult : constant Java.Int;

   --  final
   ProcedureColumnUnknown : constant Java.Int;

   --  final
   ProcedureColumnIn : constant Java.Int;

   --  final
   ProcedureColumnInOut : constant Java.Int;

   --  final
   ProcedureColumnOut : constant Java.Int;

   --  final
   ProcedureColumnReturn : constant Java.Int;

   --  final
   ProcedureColumnResult : constant Java.Int;

   --  final
   ProcedureNoNulls : constant Java.Int;

   --  final
   ProcedureNullable : constant Java.Int;

   --  final
   ProcedureNullableUnknown : constant Java.Int;

   --  final
   ColumnNoNulls : constant Java.Int;

   --  final
   ColumnNullable : constant Java.Int;

   --  final
   ColumnNullableUnknown : constant Java.Int;

   --  final
   BestRowTemporary : constant Java.Int;

   --  final
   BestRowTransaction : constant Java.Int;

   --  final
   BestRowSession : constant Java.Int;

   --  final
   BestRowUnknown : constant Java.Int;

   --  final
   BestRowNotPseudo : constant Java.Int;

   --  final
   BestRowPseudo : constant Java.Int;

   --  final
   VersionColumnUnknown : constant Java.Int;

   --  final
   VersionColumnNotPseudo : constant Java.Int;

   --  final
   VersionColumnPseudo : constant Java.Int;

   --  final
   ImportedKeyCascade : constant Java.Int;

   --  final
   ImportedKeyRestrict : constant Java.Int;

   --  final
   ImportedKeySetNull : constant Java.Int;

   --  final
   ImportedKeyNoAction : constant Java.Int;

   --  final
   ImportedKeySetDefault : constant Java.Int;

   --  final
   ImportedKeyInitiallyDeferred : constant Java.Int;

   --  final
   ImportedKeyInitiallyImmediate : constant Java.Int;

   --  final
   ImportedKeyNotDeferrable : constant Java.Int;

   --  final
   TypeNoNulls : constant Java.Int;

   --  final
   TypeNullable : constant Java.Int;

   --  final
   TypeNullableUnknown : constant Java.Int;

   --  final
   TypePredNone : constant Java.Int;

   --  final
   TypePredChar : constant Java.Int;

   --  final
   TypePredBasic : constant Java.Int;

   --  final
   TypeSearchable : constant Java.Int;

   --  final
   TableIndexStatistic : constant Java.Short;

   --  final
   TableIndexClustered : constant Java.Short;

   --  final
   TableIndexHashed : constant Java.Short;

   --  final
   TableIndexOther : constant Java.Short;

   --  final
   AttributeNoNulls : constant Java.Short;

   --  final
   AttributeNullable : constant Java.Short;

   --  final
   AttributeNullableUnknown : constant Java.Short;

   --  final
   SqlStateXOpen : constant Java.Int;

   --  final
   SqlStateSQL : constant Java.Int;

   --  final
   SqlStateSQL99 : constant Java.Int;

   --  final
   FunctionColumnUnknown : constant Java.Int;

   --  final
   FunctionColumnIn : constant Java.Int;

   --  final
   FunctionColumnInOut : constant Java.Int;

   --  final
   FunctionColumnOut : constant Java.Int;

   --  final
   FunctionReturn : constant Java.Int;

   --  final
   FunctionColumnResult : constant Java.Int;

   --  final
   FunctionNoNulls : constant Java.Int;

   --  final
   FunctionNullable : constant Java.Int;

   --  final
   FunctionNullableUnknown : constant Java.Int;

   --  final
   FunctionResultUnknown : constant Java.Int;

   --  final
   FunctionNoTable : constant Java.Int;

   --  final
   FunctionReturnsTable : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AllProceduresAreCallable, "allProceduresAreCallable");
   pragma Export (Java, AllTablesAreSelectable, "allTablesAreSelectable");
   pragma Export (Java, GetURL, "getURL");
   pragma Export (Java, GetUserName, "getUserName");
   pragma Export (Java, IsReadOnly, "isReadOnly");
   pragma Export (Java, NullsAreSortedHigh, "nullsAreSortedHigh");
   pragma Export (Java, NullsAreSortedLow, "nullsAreSortedLow");
   pragma Export (Java, NullsAreSortedAtStart, "nullsAreSortedAtStart");
   pragma Export (Java, NullsAreSortedAtEnd, "nullsAreSortedAtEnd");
   pragma Export (Java, GetDatabaseProductName, "getDatabaseProductName");
   pragma Export (Java, GetDatabaseProductVersion, "getDatabaseProductVersion");
   pragma Export (Java, GetDriverName, "getDriverName");
   pragma Export (Java, GetDriverVersion, "getDriverVersion");
   pragma Export (Java, GetDriverMajorVersion, "getDriverMajorVersion");
   pragma Export (Java, GetDriverMinorVersion, "getDriverMinorVersion");
   pragma Export (Java, UsesLocalFiles, "usesLocalFiles");
   pragma Export (Java, UsesLocalFilePerTable, "usesLocalFilePerTable");
   pragma Export (Java, SupportsMixedCaseIdentifiers, "supportsMixedCaseIdentifiers");
   pragma Export (Java, StoresUpperCaseIdentifiers, "storesUpperCaseIdentifiers");
   pragma Export (Java, StoresLowerCaseIdentifiers, "storesLowerCaseIdentifiers");
   pragma Export (Java, StoresMixedCaseIdentifiers, "storesMixedCaseIdentifiers");
   pragma Export (Java, SupportsMixedCaseQuotedIdentifiers, "supportsMixedCaseQuotedIdentifiers");
   pragma Export (Java, StoresUpperCaseQuotedIdentifiers, "storesUpperCaseQuotedIdentifiers");
   pragma Export (Java, StoresLowerCaseQuotedIdentifiers, "storesLowerCaseQuotedIdentifiers");
   pragma Export (Java, StoresMixedCaseQuotedIdentifiers, "storesMixedCaseQuotedIdentifiers");
   pragma Export (Java, GetIdentifierQuoteString, "getIdentifierQuoteString");
   pragma Export (Java, GetSQLKeywords, "getSQLKeywords");
   pragma Export (Java, GetNumericFunctions, "getNumericFunctions");
   pragma Export (Java, GetStringFunctions, "getStringFunctions");
   pragma Export (Java, GetSystemFunctions, "getSystemFunctions");
   pragma Export (Java, GetTimeDateFunctions, "getTimeDateFunctions");
   pragma Export (Java, GetSearchStringEscape, "getSearchStringEscape");
   pragma Export (Java, GetExtraNameCharacters, "getExtraNameCharacters");
   pragma Export (Java, SupportsAlterTableWithAddColumn, "supportsAlterTableWithAddColumn");
   pragma Export (Java, SupportsAlterTableWithDropColumn, "supportsAlterTableWithDropColumn");
   pragma Export (Java, SupportsColumnAliasing, "supportsColumnAliasing");
   pragma Export (Java, NullPlusNonNullIsNull, "nullPlusNonNullIsNull");
   pragma Export (Java, SupportsConvert, "supportsConvert");
   pragma Export (Java, SupportsTableCorrelationNames, "supportsTableCorrelationNames");
   pragma Export (Java, SupportsDifferentTableCorrelationNames, "supportsDifferentTableCorrelationNames");
   pragma Export (Java, SupportsExpressionsInOrderBy, "supportsExpressionsInOrderBy");
   pragma Export (Java, SupportsOrderByUnrelated, "supportsOrderByUnrelated");
   pragma Export (Java, SupportsGroupBy, "supportsGroupBy");
   pragma Export (Java, SupportsGroupByUnrelated, "supportsGroupByUnrelated");
   pragma Export (Java, SupportsGroupByBeyondSelect, "supportsGroupByBeyondSelect");
   pragma Export (Java, SupportsLikeEscapeClause, "supportsLikeEscapeClause");
   pragma Export (Java, SupportsMultipleResultSets, "supportsMultipleResultSets");
   pragma Export (Java, SupportsMultipleTransactions, "supportsMultipleTransactions");
   pragma Export (Java, SupportsNonNullableColumns, "supportsNonNullableColumns");
   pragma Export (Java, SupportsMinimumSQLGrammar, "supportsMinimumSQLGrammar");
   pragma Export (Java, SupportsCoreSQLGrammar, "supportsCoreSQLGrammar");
   pragma Export (Java, SupportsExtendedSQLGrammar, "supportsExtendedSQLGrammar");
   pragma Export (Java, SupportsANSI92EntryLevelSQL, "supportsANSI92EntryLevelSQL");
   pragma Export (Java, SupportsANSI92IntermediateSQL, "supportsANSI92IntermediateSQL");
   pragma Export (Java, SupportsANSI92FullSQL, "supportsANSI92FullSQL");
   pragma Export (Java, SupportsIntegrityEnhancementFacility, "supportsIntegrityEnhancementFacility");
   pragma Export (Java, SupportsOuterJoins, "supportsOuterJoins");
   pragma Export (Java, SupportsFullOuterJoins, "supportsFullOuterJoins");
   pragma Export (Java, SupportsLimitedOuterJoins, "supportsLimitedOuterJoins");
   pragma Export (Java, GetSchemaTerm, "getSchemaTerm");
   pragma Export (Java, GetProcedureTerm, "getProcedureTerm");
   pragma Export (Java, GetCatalogTerm, "getCatalogTerm");
   pragma Export (Java, IsCatalogAtStart, "isCatalogAtStart");
   pragma Export (Java, GetCatalogSeparator, "getCatalogSeparator");
   pragma Export (Java, SupportsSchemasInDataManipulation, "supportsSchemasInDataManipulation");
   pragma Export (Java, SupportsSchemasInProcedureCalls, "supportsSchemasInProcedureCalls");
   pragma Export (Java, SupportsSchemasInTableDefinitions, "supportsSchemasInTableDefinitions");
   pragma Export (Java, SupportsSchemasInIndexDefinitions, "supportsSchemasInIndexDefinitions");
   pragma Export (Java, SupportsSchemasInPrivilegeDefinitions, "supportsSchemasInPrivilegeDefinitions");
   pragma Export (Java, SupportsCatalogsInDataManipulation, "supportsCatalogsInDataManipulation");
   pragma Export (Java, SupportsCatalogsInProcedureCalls, "supportsCatalogsInProcedureCalls");
   pragma Export (Java, SupportsCatalogsInTableDefinitions, "supportsCatalogsInTableDefinitions");
   pragma Export (Java, SupportsCatalogsInIndexDefinitions, "supportsCatalogsInIndexDefinitions");
   pragma Export (Java, SupportsCatalogsInPrivilegeDefinitions, "supportsCatalogsInPrivilegeDefinitions");
   pragma Export (Java, SupportsPositionedDelete, "supportsPositionedDelete");
   pragma Export (Java, SupportsPositionedUpdate, "supportsPositionedUpdate");
   pragma Export (Java, SupportsSelectForUpdate, "supportsSelectForUpdate");
   pragma Export (Java, SupportsStoredProcedures, "supportsStoredProcedures");
   pragma Export (Java, SupportsSubqueriesInComparisons, "supportsSubqueriesInComparisons");
   pragma Export (Java, SupportsSubqueriesInExists, "supportsSubqueriesInExists");
   pragma Export (Java, SupportsSubqueriesInIns, "supportsSubqueriesInIns");
   pragma Export (Java, SupportsSubqueriesInQuantifieds, "supportsSubqueriesInQuantifieds");
   pragma Export (Java, SupportsCorrelatedSubqueries, "supportsCorrelatedSubqueries");
   pragma Export (Java, SupportsUnion, "supportsUnion");
   pragma Export (Java, SupportsUnionAll, "supportsUnionAll");
   pragma Export (Java, SupportsOpenCursorsAcrossCommit, "supportsOpenCursorsAcrossCommit");
   pragma Export (Java, SupportsOpenCursorsAcrossRollback, "supportsOpenCursorsAcrossRollback");
   pragma Export (Java, SupportsOpenStatementsAcrossCommit, "supportsOpenStatementsAcrossCommit");
   pragma Export (Java, SupportsOpenStatementsAcrossRollback, "supportsOpenStatementsAcrossRollback");
   pragma Export (Java, GetMaxBinaryLiteralLength, "getMaxBinaryLiteralLength");
   pragma Export (Java, GetMaxCharLiteralLength, "getMaxCharLiteralLength");
   pragma Export (Java, GetMaxColumnNameLength, "getMaxColumnNameLength");
   pragma Export (Java, GetMaxColumnsInGroupBy, "getMaxColumnsInGroupBy");
   pragma Export (Java, GetMaxColumnsInIndex, "getMaxColumnsInIndex");
   pragma Export (Java, GetMaxColumnsInOrderBy, "getMaxColumnsInOrderBy");
   pragma Export (Java, GetMaxColumnsInSelect, "getMaxColumnsInSelect");
   pragma Export (Java, GetMaxColumnsInTable, "getMaxColumnsInTable");
   pragma Export (Java, GetMaxConnections, "getMaxConnections");
   pragma Export (Java, GetMaxCursorNameLength, "getMaxCursorNameLength");
   pragma Export (Java, GetMaxIndexLength, "getMaxIndexLength");
   pragma Export (Java, GetMaxSchemaNameLength, "getMaxSchemaNameLength");
   pragma Export (Java, GetMaxProcedureNameLength, "getMaxProcedureNameLength");
   pragma Export (Java, GetMaxCatalogNameLength, "getMaxCatalogNameLength");
   pragma Export (Java, GetMaxRowSize, "getMaxRowSize");
   pragma Export (Java, DoesMaxRowSizeIncludeBlobs, "doesMaxRowSizeIncludeBlobs");
   pragma Export (Java, GetMaxStatementLength, "getMaxStatementLength");
   pragma Export (Java, GetMaxStatements, "getMaxStatements");
   pragma Export (Java, GetMaxTableNameLength, "getMaxTableNameLength");
   pragma Export (Java, GetMaxTablesInSelect, "getMaxTablesInSelect");
   pragma Export (Java, GetMaxUserNameLength, "getMaxUserNameLength");
   pragma Export (Java, GetDefaultTransactionIsolation, "getDefaultTransactionIsolation");
   pragma Export (Java, SupportsTransactions, "supportsTransactions");
   pragma Export (Java, SupportsTransactionIsolationLevel, "supportsTransactionIsolationLevel");
   pragma Export (Java, SupportsDataDefinitionAndDataManipulationTransactions, "supportsDataDefinitionAndDataManipulationTransactions");
   pragma Export (Java, SupportsDataManipulationTransactionsOnly, "supportsDataManipulationTransactionsOnly");
   pragma Export (Java, DataDefinitionCausesTransactionCommit, "dataDefinitionCausesTransactionCommit");
   pragma Export (Java, DataDefinitionIgnoredInTransactions, "dataDefinitionIgnoredInTransactions");
   pragma Export (Java, GetProcedures, "getProcedures");
   pragma Export (Java, GetProcedureColumns, "getProcedureColumns");
   pragma Export (Java, GetTables, "getTables");
   pragma Export (Java, GetSchemas, "getSchemas");
   pragma Export (Java, GetCatalogs, "getCatalogs");
   pragma Export (Java, GetTableTypes, "getTableTypes");
   pragma Export (Java, GetColumns, "getColumns");
   pragma Export (Java, GetColumnPrivileges, "getColumnPrivileges");
   pragma Export (Java, GetTablePrivileges, "getTablePrivileges");
   pragma Export (Java, GetBestRowIdentifier, "getBestRowIdentifier");
   pragma Export (Java, GetVersionColumns, "getVersionColumns");
   pragma Export (Java, GetPrimaryKeys, "getPrimaryKeys");
   pragma Export (Java, GetImportedKeys, "getImportedKeys");
   pragma Export (Java, GetExportedKeys, "getExportedKeys");
   pragma Export (Java, GetCrossReference, "getCrossReference");
   pragma Export (Java, GetTypeInfo, "getTypeInfo");
   pragma Export (Java, GetIndexInfo, "getIndexInfo");
   pragma Export (Java, SupportsResultSetType, "supportsResultSetType");
   pragma Export (Java, SupportsResultSetConcurrency, "supportsResultSetConcurrency");
   pragma Export (Java, OwnUpdatesAreVisible, "ownUpdatesAreVisible");
   pragma Export (Java, OwnDeletesAreVisible, "ownDeletesAreVisible");
   pragma Export (Java, OwnInsertsAreVisible, "ownInsertsAreVisible");
   pragma Export (Java, OthersUpdatesAreVisible, "othersUpdatesAreVisible");
   pragma Export (Java, OthersDeletesAreVisible, "othersDeletesAreVisible");
   pragma Export (Java, OthersInsertsAreVisible, "othersInsertsAreVisible");
   pragma Export (Java, UpdatesAreDetected, "updatesAreDetected");
   pragma Export (Java, DeletesAreDetected, "deletesAreDetected");
   pragma Export (Java, InsertsAreDetected, "insertsAreDetected");
   pragma Export (Java, SupportsBatchUpdates, "supportsBatchUpdates");
   pragma Export (Java, GetUDTs, "getUDTs");
   pragma Export (Java, GetConnection, "getConnection");
   pragma Export (Java, SupportsSavepoints, "supportsSavepoints");
   pragma Export (Java, SupportsNamedParameters, "supportsNamedParameters");
   pragma Export (Java, SupportsMultipleOpenResults, "supportsMultipleOpenResults");
   pragma Export (Java, SupportsGetGeneratedKeys, "supportsGetGeneratedKeys");
   pragma Export (Java, GetSuperTypes, "getSuperTypes");
   pragma Export (Java, GetSuperTables, "getSuperTables");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, SupportsResultSetHoldability, "supportsResultSetHoldability");
   pragma Export (Java, GetResultSetHoldability, "getResultSetHoldability");
   pragma Export (Java, GetDatabaseMajorVersion, "getDatabaseMajorVersion");
   pragma Export (Java, GetDatabaseMinorVersion, "getDatabaseMinorVersion");
   pragma Export (Java, GetJDBCMajorVersion, "getJDBCMajorVersion");
   pragma Export (Java, GetJDBCMinorVersion, "getJDBCMinorVersion");
   pragma Export (Java, GetSQLStateType, "getSQLStateType");
   pragma Export (Java, LocatorsUpdateCopy, "locatorsUpdateCopy");
   pragma Export (Java, SupportsStatementPooling, "supportsStatementPooling");
   pragma Export (Java, GetRowIdLifetime, "getRowIdLifetime");
   pragma Export (Java, SupportsStoredFunctionsUsingCallSyntax, "supportsStoredFunctionsUsingCallSyntax");
   pragma Export (Java, AutoCommitFailureClosesAllResultSets, "autoCommitFailureClosesAllResultSets");
   pragma Export (Java, GetClientInfoProperties, "getClientInfoProperties");
   pragma Export (Java, GetFunctions, "getFunctions");
   pragma Export (Java, GetFunctionColumns, "getFunctionColumns");
   pragma Import (Java, ProcedureResultUnknown, "procedureResultUnknown");
   pragma Import (Java, ProcedureNoResult, "procedureNoResult");
   pragma Import (Java, ProcedureReturnsResult, "procedureReturnsResult");
   pragma Import (Java, ProcedureColumnUnknown, "procedureColumnUnknown");
   pragma Import (Java, ProcedureColumnIn, "procedureColumnIn");
   pragma Import (Java, ProcedureColumnInOut, "procedureColumnInOut");
   pragma Import (Java, ProcedureColumnOut, "procedureColumnOut");
   pragma Import (Java, ProcedureColumnReturn, "procedureColumnReturn");
   pragma Import (Java, ProcedureColumnResult, "procedureColumnResult");
   pragma Import (Java, ProcedureNoNulls, "procedureNoNulls");
   pragma Import (Java, ProcedureNullable, "procedureNullable");
   pragma Import (Java, ProcedureNullableUnknown, "procedureNullableUnknown");
   pragma Import (Java, ColumnNoNulls, "columnNoNulls");
   pragma Import (Java, ColumnNullable, "columnNullable");
   pragma Import (Java, ColumnNullableUnknown, "columnNullableUnknown");
   pragma Import (Java, BestRowTemporary, "bestRowTemporary");
   pragma Import (Java, BestRowTransaction, "bestRowTransaction");
   pragma Import (Java, BestRowSession, "bestRowSession");
   pragma Import (Java, BestRowUnknown, "bestRowUnknown");
   pragma Import (Java, BestRowNotPseudo, "bestRowNotPseudo");
   pragma Import (Java, BestRowPseudo, "bestRowPseudo");
   pragma Import (Java, VersionColumnUnknown, "versionColumnUnknown");
   pragma Import (Java, VersionColumnNotPseudo, "versionColumnNotPseudo");
   pragma Import (Java, VersionColumnPseudo, "versionColumnPseudo");
   pragma Import (Java, ImportedKeyCascade, "importedKeyCascade");
   pragma Import (Java, ImportedKeyRestrict, "importedKeyRestrict");
   pragma Import (Java, ImportedKeySetNull, "importedKeySetNull");
   pragma Import (Java, ImportedKeyNoAction, "importedKeyNoAction");
   pragma Import (Java, ImportedKeySetDefault, "importedKeySetDefault");
   pragma Import (Java, ImportedKeyInitiallyDeferred, "importedKeyInitiallyDeferred");
   pragma Import (Java, ImportedKeyInitiallyImmediate, "importedKeyInitiallyImmediate");
   pragma Import (Java, ImportedKeyNotDeferrable, "importedKeyNotDeferrable");
   pragma Import (Java, TypeNoNulls, "typeNoNulls");
   pragma Import (Java, TypeNullable, "typeNullable");
   pragma Import (Java, TypeNullableUnknown, "typeNullableUnknown");
   pragma Import (Java, TypePredNone, "typePredNone");
   pragma Import (Java, TypePredChar, "typePredChar");
   pragma Import (Java, TypePredBasic, "typePredBasic");
   pragma Import (Java, TypeSearchable, "typeSearchable");
   pragma Import (Java, TableIndexStatistic, "tableIndexStatistic");
   pragma Import (Java, TableIndexClustered, "tableIndexClustered");
   pragma Import (Java, TableIndexHashed, "tableIndexHashed");
   pragma Import (Java, TableIndexOther, "tableIndexOther");
   pragma Import (Java, AttributeNoNulls, "attributeNoNulls");
   pragma Import (Java, AttributeNullable, "attributeNullable");
   pragma Import (Java, AttributeNullableUnknown, "attributeNullableUnknown");
   pragma Import (Java, SqlStateXOpen, "sqlStateXOpen");
   pragma Import (Java, SqlStateSQL, "sqlStateSQL");
   pragma Import (Java, SqlStateSQL99, "sqlStateSQL99");
   pragma Import (Java, FunctionColumnUnknown, "functionColumnUnknown");
   pragma Import (Java, FunctionColumnIn, "functionColumnIn");
   pragma Import (Java, FunctionColumnInOut, "functionColumnInOut");
   pragma Import (Java, FunctionColumnOut, "functionColumnOut");
   pragma Import (Java, FunctionReturn, "functionReturn");
   pragma Import (Java, FunctionColumnResult, "functionColumnResult");
   pragma Import (Java, FunctionNoNulls, "functionNoNulls");
   pragma Import (Java, FunctionNullable, "functionNullable");
   pragma Import (Java, FunctionNullableUnknown, "functionNullableUnknown");
   pragma Import (Java, FunctionResultUnknown, "functionResultUnknown");
   pragma Import (Java, FunctionNoTable, "functionNoTable");
   pragma Import (Java, FunctionReturnsTable, "functionReturnsTable");

end Java.Sql.DatabaseMetaData;
pragma Import (Java, Java.Sql.DatabaseMetaData, "java.sql.DatabaseMetaData");
pragma Extensions_Allowed (Off);
