pragma Extensions_Allowed (On);
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Iterable;
with Java.Lang.Object;
with Java.Sql.SQLWarning;

package Java.Sql.DataTruncation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Iterable_I : Java.Lang.Iterable.Ref)
    is new Java.Sql.SQLWarning.Typ(Serializable_I,
                                   Iterable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DataTruncation (P1_Int : Java.Int;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_DataTruncation (P1_Int : Java.Int;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIndex (This : access Typ)
                      return Java.Int;

   function GetParameter (This : access Typ)
                          return Java.Boolean;

   function GetRead (This : access Typ)
                     return Java.Boolean;

   function GetDataSize (This : access Typ)
                         return Java.Int;

   function GetTransferSize (This : access Typ)
                             return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.sql.DataTruncation");
   pragma Java_Constructor (New_DataTruncation);
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, GetParameter, "getParameter");
   pragma Import (Java, GetRead, "getRead");
   pragma Import (Java, GetDataSize, "getDataSize");
   pragma Import (Java, GetTransferSize, "getTransferSize");

end Java.Sql.DataTruncation;
pragma Import (Java, Java.Sql.DataTruncation, "java.sql.DataTruncation");
pragma Extensions_Allowed (Off);
