pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Sql.Connection;
limited with Java.Sql.DriverPropertyInfo;
limited with Java.Util.Properties;
with Java.Lang.Object;

package Java.Sql.Driver is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Connect (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Properties : access Standard.Java.Util.Properties.Typ'Class)
                     return access Java.Sql.Connection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function AcceptsURL (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetPropertyInfo (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Properties : access Standard.Java.Util.Properties.Typ'Class)
                             return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMajorVersion (This : access Typ)
                             return Java.Int is abstract;

   function GetMinorVersion (This : access Typ)
                             return Java.Int is abstract;

   function JdbcCompliant (This : access Typ)
                           return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, AcceptsURL, "acceptsURL");
   pragma Export (Java, GetPropertyInfo, "getPropertyInfo");
   pragma Export (Java, GetMajorVersion, "getMajorVersion");
   pragma Export (Java, GetMinorVersion, "getMinorVersion");
   pragma Export (Java, JdbcCompliant, "jdbcCompliant");

end Java.Sql.Driver;
pragma Import (Java, Java.Sql.Driver, "java.sql.Driver");
pragma Extensions_Allowed (Off);
