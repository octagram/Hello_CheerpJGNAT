pragma Extensions_Allowed (On);
limited with Java.Io.PrintWriter;
limited with Java.Lang.String;
limited with Java.Sql.Connection;
limited with Java.Sql.Driver;
limited with Java.Util.Enumeration;
limited with Java.Util.Properties;
with Java.Lang.Object;

package Java.Sql.DriverManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLogWriter return access Java.Io.PrintWriter.Typ'Class;

   procedure SetLogWriter (P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class);

   function GetConnection (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Properties : access Standard.Java.Util.Properties.Typ'Class)
                           return access Java.Sql.Connection.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetConnection (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Sql.Connection.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetConnection (P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Sql.Connection.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetDriver (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Sql.Driver.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   --  synchronized
   procedure RegisterDriver (P1_Driver : access Standard.Java.Sql.Driver.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   --  synchronized
   procedure DeregisterDriver (P1_Driver : access Standard.Java.Sql.Driver.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   function GetDrivers return access Java.Util.Enumeration.Typ'Class;

   procedure SetLoginTimeout (P1_Int : Java.Int);

   function GetLoginTimeout return Java.Int;

   procedure Println (P1_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetLogWriter, "getLogWriter");
   pragma Import (Java, SetLogWriter, "setLogWriter");
   pragma Import (Java, GetConnection, "getConnection");
   pragma Import (Java, GetDriver, "getDriver");
   pragma Import (Java, RegisterDriver, "registerDriver");
   pragma Import (Java, DeregisterDriver, "deregisterDriver");
   pragma Import (Java, GetDrivers, "getDrivers");
   pragma Import (Java, SetLoginTimeout, "setLoginTimeout");
   pragma Import (Java, GetLoginTimeout, "getLoginTimeout");
   pragma Import (Java, Println, "println");

end Java.Sql.DriverManager;
pragma Import (Java, Java.Sql.DriverManager, "java.sql.DriverManager");
pragma Extensions_Allowed (Off);
