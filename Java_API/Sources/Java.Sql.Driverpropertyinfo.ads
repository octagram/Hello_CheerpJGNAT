pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Sql.DriverPropertyInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      Description : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Description, "description");

      Required : Java.Boolean;
      pragma Import (Java, Required, "required");

      Value : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Value, "value");

      Choices : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Choices, "choices");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DriverPropertyInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DriverPropertyInfo);

end Java.Sql.DriverPropertyInfo;
pragma Import (Java, Java.Sql.DriverPropertyInfo, "java.sql.DriverPropertyInfo");
pragma Extensions_Allowed (Off);
