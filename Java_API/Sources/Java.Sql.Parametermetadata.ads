pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Sql.Wrapper;

package Java.Sql.ParameterMetaData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Wrapper_I : Java.Sql.Wrapper.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParameterCount (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsNullable (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsSigned (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetPrecision (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetScale (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetParameterType (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetParameterTypeName (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetParameterClassName (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetParameterMode (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ParameterNoNulls : constant Java.Int;

   --  final
   ParameterNullable : constant Java.Int;

   --  final
   ParameterNullableUnknown : constant Java.Int;

   --  final
   ParameterModeUnknown : constant Java.Int;

   --  final
   ParameterModeIn : constant Java.Int;

   --  final
   ParameterModeInOut : constant Java.Int;

   --  final
   ParameterModeOut : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParameterCount, "getParameterCount");
   pragma Export (Java, IsNullable, "isNullable");
   pragma Export (Java, IsSigned, "isSigned");
   pragma Export (Java, GetPrecision, "getPrecision");
   pragma Export (Java, GetScale, "getScale");
   pragma Export (Java, GetParameterType, "getParameterType");
   pragma Export (Java, GetParameterTypeName, "getParameterTypeName");
   pragma Export (Java, GetParameterClassName, "getParameterClassName");
   pragma Export (Java, GetParameterMode, "getParameterMode");
   pragma Import (Java, ParameterNoNulls, "parameterNoNulls");
   pragma Import (Java, ParameterNullable, "parameterNullable");
   pragma Import (Java, ParameterNullableUnknown, "parameterNullableUnknown");
   pragma Import (Java, ParameterModeUnknown, "parameterModeUnknown");
   pragma Import (Java, ParameterModeIn, "parameterModeIn");
   pragma Import (Java, ParameterModeInOut, "parameterModeInOut");
   pragma Import (Java, ParameterModeOut, "parameterModeOut");

end Java.Sql.ParameterMetaData;
pragma Import (Java, Java.Sql.ParameterMetaData, "java.sql.ParameterMetaData");
pragma Extensions_Allowed (Off);
