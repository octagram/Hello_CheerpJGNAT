pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.ResultSetMetaData;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLWarning;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Statement;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
limited with Java.Util.Calendar;
limited with Java.Util.Map;
with Java.Lang.Object;
with Java.Sql.Wrapper;

package Java.Sql.ResultSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Wrapper_I : Java.Sql.Wrapper.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Next (This : access Typ)
                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function WasNull (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetString (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBoolean (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetByte (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Byte is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetShort (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Short is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetInt (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetLong (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFloat (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Float is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDouble (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Double is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBytes (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Byte_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAsciiStream (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBinaryStream (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetString (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBoolean (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetByte (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Byte is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetShort (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Short is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetInt (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetLong (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFloat (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Float is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDouble (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Double is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBytes (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Byte_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAsciiStream (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBinaryStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetWarnings (This : access Typ)
                         return access Java.Sql.SQLWarning.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ClearWarnings (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCursorName (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMetaData (This : access Typ)
                         return access Java.Sql.ResultSetMetaData.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function FindColumn (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCharacterStream (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCharacterStream (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBigDecimal (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Math.BigDecimal.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBigDecimal (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Math.BigDecimal.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsBeforeFirst (This : access Typ)
                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsAfterLast (This : access Typ)
                         return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsFirst (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsLast (This : access Typ)
                    return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure BeforeFirst (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AfterLast (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function First (This : access Typ)
                   return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Last (This : access Typ)
                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRow (This : access Typ)
                    return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Absolute (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Relative (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Previous (This : access Typ)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFetchDirection (This : access Typ;
                                P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFetchDirection (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFetchSize (This : access Typ;
                           P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFetchSize (This : access Typ)
                          return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetType (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetConcurrency (This : access Typ)
                            return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function RowUpdated (This : access Typ)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function RowInserted (This : access Typ)
                         return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function RowDeleted (This : access Typ)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNull (This : access Typ;
                         P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBoolean (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateByte (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Byte : Java.Byte) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateShort (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Short : Java.Short) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateInt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateLong (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateFloat (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Float : Java.Float) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateDouble (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Double : Java.Double) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBigDecimal (This : access Typ;
                               P1_Int : Java.Int;
                               P2_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateString (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBytes (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateDate (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Date : access Standard.Java.Sql.Date.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateTime (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Time : access Standard.Java.Sql.Time.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateTimestamp (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateAsciiStream (This : access Typ;
                                P1_Int : Java.Int;
                                P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBinaryStream (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                 P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateCharacterStream (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                    P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateObject (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateObject (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNull (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBoolean (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateByte (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Byte : Java.Byte) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateShort (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Short : Java.Short) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateInt (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateLong (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateFloat (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Float : Java.Float) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateDouble (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Double : Java.Double) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBigDecimal (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateString (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBytes (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateDate (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Date : access Standard.Java.Sql.Date.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateTime (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Time : access Standard.Java.Sql.Time.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateTimestamp (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateAsciiStream (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBinaryStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                 P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateCharacterStream (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                    P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateObject (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateObject (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure InsertRow (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateRow (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure DeleteRow (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure RefreshRow (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure CancelRowUpdates (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure MoveToInsertRow (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure MoveToCurrentRow (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetStatement (This : access Typ)
                          return access Java.Sql.Statement.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Map : access Standard.Java.Util.Map.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRef (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Sql.Ref.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBlob (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Blob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetClob (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Sql.Clob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Sql.Array_K.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetObject (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Map : access Standard.Java.Util.Map.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRef (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Sql.Ref.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBlob (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Blob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetClob (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Clob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetArray (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Sql.Array_K.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetDate (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTime (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                     return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTimestamp (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                          return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetURL (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Net.URL.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetURL (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Net.URL.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateRef (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Ref : access Standard.Java.Sql.Ref.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateRef (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Ref : access Standard.Java.Sql.Ref.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBlob (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Blob : access Standard.Java.Sql.Blob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBlob (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Blob : access Standard.Java.Sql.Blob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateClob (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Clob : access Standard.Java.Sql.Clob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateClob (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Clob : access Standard.Java.Sql.Clob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateArray (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Array_K : access Standard.Java.Sql.Array_K.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateArray (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Array_K : access Standard.Java.Sql.Array_K.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowId (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Sql.RowId.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowId (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Sql.RowId.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateRowId (This : access Typ;
                          P1_Int : Java.Int;
                          P2_RowId : access Standard.Java.Sql.RowId.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateRowId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_RowId : access Standard.Java.Sql.RowId.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetHoldability (This : access Typ)
                            return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsClosed (This : access Typ)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNString (This : access Typ;
                            P1_Int : Java.Int;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNString (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNClob (This : access Typ;
                          P1_Int : Java.Int;
                          P2_NClob : access Standard.Java.Sql.NClob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNClob (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_NClob : access Standard.Java.Sql.NClob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNClob (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Sql.NClob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Sql.NClob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSQLXML (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Sql.SQLXML.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSQLXML (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Sql.SQLXML.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateSQLXML (This : access Typ;
                           P1_Int : Java.Int;
                           P2_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateSQLXML (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNString (This : access Typ;
                        P1_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNString (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNCharacterStream (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetNCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNCharacterStream (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                     P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNCharacterStream (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                     P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateAsciiStream (This : access Typ;
                                P1_Int : Java.Int;
                                P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBinaryStream (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                 P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateCharacterStream (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                    P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateAsciiStream (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBinaryStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                 P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateCharacterStream (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                    P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBlob (This : access Typ;
                         P1_Int : Java.Int;
                         P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                         P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBlob (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                         P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateClob (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                         P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateClob (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                         P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNClob (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                          P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNClob (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                          P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNCharacterStream (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNCharacterStream (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateAsciiStream (This : access Typ;
                                P1_Int : Java.Int;
                                P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBinaryStream (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateCharacterStream (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateAsciiStream (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBinaryStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateCharacterStream (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBlob (This : access Typ;
                         P1_Int : Java.Int;
                         P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateBlob (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateClob (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateClob (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNClob (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UpdateNClob (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FETCH_FORWARD : constant Java.Int;

   --  final
   FETCH_REVERSE : constant Java.Int;

   --  final
   FETCH_UNKNOWN : constant Java.Int;

   --  final
   TYPE_FORWARD_ONLY : constant Java.Int;

   --  final
   TYPE_SCROLL_INSENSITIVE : constant Java.Int;

   --  final
   TYPE_SCROLL_SENSITIVE : constant Java.Int;

   --  final
   CONCUR_READ_ONLY : constant Java.Int;

   --  final
   CONCUR_UPDATABLE : constant Java.Int;

   --  final
   HOLD_CURSORS_OVER_COMMIT : constant Java.Int;

   --  final
   CLOSE_CURSORS_AT_COMMIT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Next, "next");
   pragma Export (Java, Close, "close");
   pragma Export (Java, WasNull, "wasNull");
   pragma Export (Java, GetString, "getString");
   pragma Export (Java, GetBoolean, "getBoolean");
   pragma Export (Java, GetByte, "getByte");
   pragma Export (Java, GetShort, "getShort");
   pragma Export (Java, GetInt, "getInt");
   pragma Export (Java, GetLong, "getLong");
   pragma Export (Java, GetFloat, "getFloat");
   pragma Export (Java, GetDouble, "getDouble");
   pragma Export (Java, GetBytes, "getBytes");
   pragma Export (Java, GetDate, "getDate");
   pragma Export (Java, GetTime, "getTime");
   pragma Export (Java, GetTimestamp, "getTimestamp");
   pragma Export (Java, GetAsciiStream, "getAsciiStream");
   pragma Export (Java, GetBinaryStream, "getBinaryStream");
   pragma Export (Java, GetWarnings, "getWarnings");
   pragma Export (Java, ClearWarnings, "clearWarnings");
   pragma Export (Java, GetCursorName, "getCursorName");
   pragma Export (Java, GetMetaData, "getMetaData");
   pragma Export (Java, GetObject, "getObject");
   pragma Export (Java, FindColumn, "findColumn");
   pragma Export (Java, GetCharacterStream, "getCharacterStream");
   pragma Export (Java, GetBigDecimal, "getBigDecimal");
   pragma Export (Java, IsBeforeFirst, "isBeforeFirst");
   pragma Export (Java, IsAfterLast, "isAfterLast");
   pragma Export (Java, IsFirst, "isFirst");
   pragma Export (Java, IsLast, "isLast");
   pragma Export (Java, BeforeFirst, "beforeFirst");
   pragma Export (Java, AfterLast, "afterLast");
   pragma Export (Java, First, "first");
   pragma Export (Java, Last, "last");
   pragma Export (Java, GetRow, "getRow");
   pragma Export (Java, Absolute, "absolute");
   pragma Export (Java, Relative, "relative");
   pragma Export (Java, Previous, "previous");
   pragma Export (Java, SetFetchDirection, "setFetchDirection");
   pragma Export (Java, GetFetchDirection, "getFetchDirection");
   pragma Export (Java, SetFetchSize, "setFetchSize");
   pragma Export (Java, GetFetchSize, "getFetchSize");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetConcurrency, "getConcurrency");
   pragma Export (Java, RowUpdated, "rowUpdated");
   pragma Export (Java, RowInserted, "rowInserted");
   pragma Export (Java, RowDeleted, "rowDeleted");
   pragma Export (Java, UpdateNull, "updateNull");
   pragma Export (Java, UpdateBoolean, "updateBoolean");
   pragma Export (Java, UpdateByte, "updateByte");
   pragma Export (Java, UpdateShort, "updateShort");
   pragma Export (Java, UpdateInt, "updateInt");
   pragma Export (Java, UpdateLong, "updateLong");
   pragma Export (Java, UpdateFloat, "updateFloat");
   pragma Export (Java, UpdateDouble, "updateDouble");
   pragma Export (Java, UpdateBigDecimal, "updateBigDecimal");
   pragma Export (Java, UpdateString, "updateString");
   pragma Export (Java, UpdateBytes, "updateBytes");
   pragma Export (Java, UpdateDate, "updateDate");
   pragma Export (Java, UpdateTime, "updateTime");
   pragma Export (Java, UpdateTimestamp, "updateTimestamp");
   pragma Export (Java, UpdateAsciiStream, "updateAsciiStream");
   pragma Export (Java, UpdateBinaryStream, "updateBinaryStream");
   pragma Export (Java, UpdateCharacterStream, "updateCharacterStream");
   pragma Export (Java, UpdateObject, "updateObject");
   pragma Export (Java, InsertRow, "insertRow");
   pragma Export (Java, UpdateRow, "updateRow");
   pragma Export (Java, DeleteRow, "deleteRow");
   pragma Export (Java, RefreshRow, "refreshRow");
   pragma Export (Java, CancelRowUpdates, "cancelRowUpdates");
   pragma Export (Java, MoveToInsertRow, "moveToInsertRow");
   pragma Export (Java, MoveToCurrentRow, "moveToCurrentRow");
   pragma Export (Java, GetStatement, "getStatement");
   pragma Export (Java, GetRef, "getRef");
   pragma Export (Java, GetBlob, "getBlob");
   pragma Export (Java, GetClob, "getClob");
   pragma Export (Java, GetArray, "getArray");
   pragma Export (Java, GetURL, "getURL");
   pragma Export (Java, UpdateRef, "updateRef");
   pragma Export (Java, UpdateBlob, "updateBlob");
   pragma Export (Java, UpdateClob, "updateClob");
   pragma Export (Java, UpdateArray, "updateArray");
   pragma Export (Java, GetRowId, "getRowId");
   pragma Export (Java, UpdateRowId, "updateRowId");
   pragma Export (Java, GetHoldability, "getHoldability");
   pragma Export (Java, IsClosed, "isClosed");
   pragma Export (Java, UpdateNString, "updateNString");
   pragma Export (Java, UpdateNClob, "updateNClob");
   pragma Export (Java, GetNClob, "getNClob");
   pragma Export (Java, GetSQLXML, "getSQLXML");
   pragma Export (Java, UpdateSQLXML, "updateSQLXML");
   pragma Export (Java, GetNString, "getNString");
   pragma Export (Java, GetNCharacterStream, "getNCharacterStream");
   pragma Export (Java, UpdateNCharacterStream, "updateNCharacterStream");
   pragma Import (Java, FETCH_FORWARD, "FETCH_FORWARD");
   pragma Import (Java, FETCH_REVERSE, "FETCH_REVERSE");
   pragma Import (Java, FETCH_UNKNOWN, "FETCH_UNKNOWN");
   pragma Import (Java, TYPE_FORWARD_ONLY, "TYPE_FORWARD_ONLY");
   pragma Import (Java, TYPE_SCROLL_INSENSITIVE, "TYPE_SCROLL_INSENSITIVE");
   pragma Import (Java, TYPE_SCROLL_SENSITIVE, "TYPE_SCROLL_SENSITIVE");
   pragma Import (Java, CONCUR_READ_ONLY, "CONCUR_READ_ONLY");
   pragma Import (Java, CONCUR_UPDATABLE, "CONCUR_UPDATABLE");
   pragma Import (Java, HOLD_CURSORS_OVER_COMMIT, "HOLD_CURSORS_OVER_COMMIT");
   pragma Import (Java, CLOSE_CURSORS_AT_COMMIT, "CLOSE_CURSORS_AT_COMMIT");

end Java.Sql.ResultSet;
pragma Import (Java, Java.Sql.ResultSet, "java.sql.ResultSet");
pragma Extensions_Allowed (Off);
