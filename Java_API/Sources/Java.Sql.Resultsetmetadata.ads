pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Sql.Wrapper;

package Java.Sql.ResultSetMetaData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Wrapper_I : Java.Sql.Wrapper.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetColumnCount (This : access Typ)
                            return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsAutoIncrement (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsCaseSensitive (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsSearchable (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsCurrency (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsNullable (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsSigned (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnDisplaySize (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnLabel (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSchemaName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetPrecision (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetScale (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTableName (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCatalogName (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnType (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnTypeName (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsReadOnly (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsWritable (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsDefinitelyWritable (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnClassName (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ColumnNoNulls : constant Java.Int;

   --  final
   ColumnNullable : constant Java.Int;

   --  final
   ColumnNullableUnknown : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetColumnCount, "getColumnCount");
   pragma Export (Java, IsAutoIncrement, "isAutoIncrement");
   pragma Export (Java, IsCaseSensitive, "isCaseSensitive");
   pragma Export (Java, IsSearchable, "isSearchable");
   pragma Export (Java, IsCurrency, "isCurrency");
   pragma Export (Java, IsNullable, "isNullable");
   pragma Export (Java, IsSigned, "isSigned");
   pragma Export (Java, GetColumnDisplaySize, "getColumnDisplaySize");
   pragma Export (Java, GetColumnLabel, "getColumnLabel");
   pragma Export (Java, GetColumnName, "getColumnName");
   pragma Export (Java, GetSchemaName, "getSchemaName");
   pragma Export (Java, GetPrecision, "getPrecision");
   pragma Export (Java, GetScale, "getScale");
   pragma Export (Java, GetTableName, "getTableName");
   pragma Export (Java, GetCatalogName, "getCatalogName");
   pragma Export (Java, GetColumnType, "getColumnType");
   pragma Export (Java, GetColumnTypeName, "getColumnTypeName");
   pragma Export (Java, IsReadOnly, "isReadOnly");
   pragma Export (Java, IsWritable, "isWritable");
   pragma Export (Java, IsDefinitelyWritable, "isDefinitelyWritable");
   pragma Export (Java, GetColumnClassName, "getColumnClassName");
   pragma Import (Java, ColumnNoNulls, "columnNoNulls");
   pragma Import (Java, ColumnNullable, "columnNullable");
   pragma Import (Java, ColumnNullableUnknown, "columnNullableUnknown");

end Java.Sql.ResultSetMetaData;
pragma Import (Java, Java.Sql.ResultSetMetaData, "java.sql.ResultSetMetaData");
pragma Extensions_Allowed (Off);
