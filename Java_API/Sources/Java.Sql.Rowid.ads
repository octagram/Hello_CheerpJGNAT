pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Sql.RowId is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function GetBytes (This : access Typ)
                      return Java.Byte_Arr is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, GetBytes, "getBytes");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, HashCode, "hashCode");

end Java.Sql.RowId;
pragma Import (Java, Java.Sql.RowId, "java.sql.RowId");
pragma Extensions_Allowed (Off);
