pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Sql.Savepoint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSavepointId (This : access Typ)
                            return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSavepointName (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSavepointId, "getSavepointId");
   pragma Export (Java, GetSavepointName, "getSavepointName");

end Java.Sql.Savepoint;
pragma Import (Java, Java.Sql.Savepoint, "java.sql.Savepoint");
pragma Extensions_Allowed (Off);
