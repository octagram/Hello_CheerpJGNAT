pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Sql.SQLInput;
limited with Java.Sql.SQLOutput;
with Java.Lang.Object;

package Java.Sql.SQLData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSQLTypeName (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ReadSQL (This : access Typ;
                      P1_SQLInput : access Standard.Java.Sql.SQLInput.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteSQL (This : access Typ;
                       P1_SQLOutput : access Standard.Java.Sql.SQLOutput.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSQLTypeName, "getSQLTypeName");
   pragma Export (Java, ReadSQL, "readSQL");
   pragma Export (Java, WriteSQL, "writeSQL");

end Java.Sql.SQLData;
pragma Import (Java, Java.Sql.SQLData, "java.sql.SQLData");
pragma Extensions_Allowed (Off);
