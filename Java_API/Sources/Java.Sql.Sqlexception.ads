pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Iterable;
with Java.Lang.Object;

package Java.Sql.SQLException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Iterable_I : Java.Lang.Iterable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SQLException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_SQLException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SQLException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SQLException (This : Ref := null)
                              return Ref;

   function New_SQLException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SQLException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SQLException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SQLException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSQLState (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetErrorCode (This : access Typ)
                          return Java.Int;

   function GetNextException (This : access Typ)
                              return access Java.Sql.SQLException.Typ'Class;

   procedure SetNextException (This : access Typ;
                               P1_SQLException : access Standard.Java.Sql.SQLException.Typ'Class);

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.sql.SQLException");
   pragma Java_Constructor (New_SQLException);
   pragma Import (Java, GetSQLState, "getSQLState");
   pragma Import (Java, GetErrorCode, "getErrorCode");
   pragma Import (Java, GetNextException, "getNextException");
   pragma Import (Java, SetNextException, "setNextException");
   pragma Import (Java, Iterator, "iterator");

end Java.Sql.SQLException;
pragma Import (Java, Java.Sql.SQLException, "java.sql.SQLException");
pragma Extensions_Allowed (Off);
