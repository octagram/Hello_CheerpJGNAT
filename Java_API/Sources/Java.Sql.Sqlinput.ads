pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
with Java.Lang.Object;

package Java.Sql.SQLInput is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ReadString (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadBoolean (This : access Typ)
                         return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadByte (This : access Typ)
                      return Java.Byte is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadShort (This : access Typ)
                       return Java.Short is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadInt (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadLong (This : access Typ)
                      return Java.Long is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadFloat (This : access Typ)
                       return Java.Float is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadDouble (This : access Typ)
                        return Java.Double is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadBigDecimal (This : access Typ)
                            return access Java.Math.BigDecimal.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadBytes (This : access Typ)
                       return Java.Byte_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadDate (This : access Typ)
                      return access Java.Sql.Date.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadTime (This : access Typ)
                      return access Java.Sql.Time.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadTimestamp (This : access Typ)
                           return access Java.Sql.Timestamp.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadCharacterStream (This : access Typ)
                                 return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadAsciiStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadBinaryStream (This : access Typ)
                              return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadObject (This : access Typ)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadRef (This : access Typ)
                     return access Java.Sql.Ref.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadBlob (This : access Typ)
                      return access Java.Sql.Blob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadClob (This : access Typ)
                      return access Java.Sql.Clob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadArray (This : access Typ)
                       return access Java.Sql.Array_K.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function WasNull (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadURL (This : access Typ)
                     return access Java.Net.URL.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadNClob (This : access Typ)
                       return access Java.Sql.NClob.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadNString (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadSQLXML (This : access Typ)
                        return access Java.Sql.SQLXML.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ReadRowId (This : access Typ)
                       return access Java.Sql.RowId.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ReadString, "readString");
   pragma Export (Java, ReadBoolean, "readBoolean");
   pragma Export (Java, ReadByte, "readByte");
   pragma Export (Java, ReadShort, "readShort");
   pragma Export (Java, ReadInt, "readInt");
   pragma Export (Java, ReadLong, "readLong");
   pragma Export (Java, ReadFloat, "readFloat");
   pragma Export (Java, ReadDouble, "readDouble");
   pragma Export (Java, ReadBigDecimal, "readBigDecimal");
   pragma Export (Java, ReadBytes, "readBytes");
   pragma Export (Java, ReadDate, "readDate");
   pragma Export (Java, ReadTime, "readTime");
   pragma Export (Java, ReadTimestamp, "readTimestamp");
   pragma Export (Java, ReadCharacterStream, "readCharacterStream");
   pragma Export (Java, ReadAsciiStream, "readAsciiStream");
   pragma Export (Java, ReadBinaryStream, "readBinaryStream");
   pragma Export (Java, ReadObject, "readObject");
   pragma Export (Java, ReadRef, "readRef");
   pragma Export (Java, ReadBlob, "readBlob");
   pragma Export (Java, ReadClob, "readClob");
   pragma Export (Java, ReadArray, "readArray");
   pragma Export (Java, WasNull, "wasNull");
   pragma Export (Java, ReadURL, "readURL");
   pragma Export (Java, ReadNClob, "readNClob");
   pragma Export (Java, ReadNString, "readNString");
   pragma Export (Java, ReadSQLXML, "readSQLXML");
   pragma Export (Java, ReadRowId, "readRowId");

end Java.Sql.SQLInput;
pragma Import (Java, Java.Sql.SQLInput, "java.sql.SQLInput");
pragma Extensions_Allowed (Off);
