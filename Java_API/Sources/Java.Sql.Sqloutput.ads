pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLData;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Struct;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
with Java.Lang.Object;

package Java.Sql.SQLOutput is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteString (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBoolean (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteByte (This : access Typ;
                        P1_Byte : Java.Byte) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteShort (This : access Typ;
                         P1_Short : Java.Short) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteInt (This : access Typ;
                       P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteLong (This : access Typ;
                        P1_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteFloat (This : access Typ;
                         P1_Float : Java.Float) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteDouble (This : access Typ;
                          P1_Double : Java.Double) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBigDecimal (This : access Typ;
                              P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBytes (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteDate (This : access Typ;
                        P1_Date : access Standard.Java.Sql.Date.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteTime (This : access Typ;
                        P1_Time : access Standard.Java.Sql.Time.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteTimestamp (This : access Typ;
                             P1_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteCharacterStream (This : access Typ;
                                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteAsciiStream (This : access Typ;
                               P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBinaryStream (This : access Typ;
                                P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteObject (This : access Typ;
                          P1_SQLData : access Standard.Java.Sql.SQLData.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteRef (This : access Typ;
                       P1_Ref : access Standard.Java.Sql.Ref.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBlob (This : access Typ;
                        P1_Blob : access Standard.Java.Sql.Blob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteClob (This : access Typ;
                        P1_Clob : access Standard.Java.Sql.Clob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteStruct (This : access Typ;
                          P1_Struct : access Standard.Java.Sql.Struct.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteArray (This : access Typ;
                         P1_Array_K : access Standard.Java.Sql.Array_K.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteURL (This : access Typ;
                       P1_URL : access Standard.Java.Net.URL.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteNString (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteNClob (This : access Typ;
                         P1_NClob : access Standard.Java.Sql.NClob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteRowId (This : access Typ;
                         P1_RowId : access Standard.Java.Sql.RowId.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteSQLXML (This : access Typ;
                          P1_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WriteString, "writeString");
   pragma Export (Java, WriteBoolean, "writeBoolean");
   pragma Export (Java, WriteByte, "writeByte");
   pragma Export (Java, WriteShort, "writeShort");
   pragma Export (Java, WriteInt, "writeInt");
   pragma Export (Java, WriteLong, "writeLong");
   pragma Export (Java, WriteFloat, "writeFloat");
   pragma Export (Java, WriteDouble, "writeDouble");
   pragma Export (Java, WriteBigDecimal, "writeBigDecimal");
   pragma Export (Java, WriteBytes, "writeBytes");
   pragma Export (Java, WriteDate, "writeDate");
   pragma Export (Java, WriteTime, "writeTime");
   pragma Export (Java, WriteTimestamp, "writeTimestamp");
   pragma Export (Java, WriteCharacterStream, "writeCharacterStream");
   pragma Export (Java, WriteAsciiStream, "writeAsciiStream");
   pragma Export (Java, WriteBinaryStream, "writeBinaryStream");
   pragma Export (Java, WriteObject, "writeObject");
   pragma Export (Java, WriteRef, "writeRef");
   pragma Export (Java, WriteBlob, "writeBlob");
   pragma Export (Java, WriteClob, "writeClob");
   pragma Export (Java, WriteStruct, "writeStruct");
   pragma Export (Java, WriteArray, "writeArray");
   pragma Export (Java, WriteURL, "writeURL");
   pragma Export (Java, WriteNString, "writeNString");
   pragma Export (Java, WriteNClob, "writeNClob");
   pragma Export (Java, WriteRowId, "writeRowId");
   pragma Export (Java, WriteSQLXML, "writeSQLXML");

end Java.Sql.SQLOutput;
pragma Import (Java, Java.Sql.SQLOutput, "java.sql.SQLOutput");
pragma Extensions_Allowed (Off);
