pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Iterable;
with Java.Lang.Object;
with Java.Sql.SQLException;

package Java.Sql.SQLWarning is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Iterable_I : Java.Lang.Iterable.Ref)
    is new Java.Sql.SQLException.Typ(Serializable_I,
                                     Iterable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SQLWarning (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_SQLWarning (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_SQLWarning (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_SQLWarning (This : Ref := null)
                            return Ref;

   function New_SQLWarning (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_SQLWarning (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_SQLWarning (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_SQLWarning (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Int : Java.Int;
                            P4_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNextWarning (This : access Typ)
                            return access Java.Sql.SQLWarning.Typ'Class;

   procedure SetNextWarning (This : access Typ;
                             P1_SQLWarning : access Standard.Java.Sql.SQLWarning.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.sql.SQLWarning");
   pragma Java_Constructor (New_SQLWarning);
   pragma Import (Java, GetNextWarning, "getNextWarning");
   pragma Import (Java, SetNextWarning, "setNextWarning");

end Java.Sql.SQLWarning;
pragma Import (Java, Java.Sql.SQLWarning, "java.sql.SQLWarning");
pragma Extensions_Allowed (Off);
