pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Source;
with Java.Lang.Object;

package Java.Sql.SQLXML is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Free (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetBinaryStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetBinaryStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCharacterStream (This : access Typ)
                                return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetCharacterStream (This : access Typ)
                                return access Java.Io.Writer.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetString (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetString (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSource (This : access Typ;
                       P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Transform.Source.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SetResult (This : access Typ;
                       P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Transform.Result.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Free, "free");
   pragma Export (Java, GetBinaryStream, "getBinaryStream");
   pragma Export (Java, SetBinaryStream, "setBinaryStream");
   pragma Export (Java, GetCharacterStream, "getCharacterStream");
   pragma Export (Java, SetCharacterStream, "setCharacterStream");
   pragma Export (Java, GetString, "getString");
   pragma Export (Java, SetString, "setString");
   pragma Export (Java, GetSource, "getSource");
   pragma Export (Java, SetResult, "setResult");

end Java.Sql.SQLXML;
pragma Import (Java, Java.Sql.SQLXML, "java.sql.SQLXML");
pragma Extensions_Allowed (Off);
