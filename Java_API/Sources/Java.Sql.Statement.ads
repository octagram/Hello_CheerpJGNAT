pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Sql.Connection;
limited with Java.Sql.ResultSet;
limited with Java.Sql.SQLWarning;
with Java.Lang.Object;
with Java.Sql.Wrapper;

package Java.Sql.Statement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Wrapper_I : Java.Sql.Wrapper.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ExecuteQuery (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ExecuteUpdate (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxFieldSize (This : access Typ)
                             return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMaxFieldSize (This : access Typ;
                              P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxRows (This : access Typ)
                        return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMaxRows (This : access Typ;
                         P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetEscapeProcessing (This : access Typ;
                                  P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetQueryTimeout (This : access Typ)
                             return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetQueryTimeout (This : access Typ;
                              P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Cancel (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetWarnings (This : access Typ)
                         return access Java.Sql.SQLWarning.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ClearWarnings (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCursorName (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Execute (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSet (This : access Typ)
                          return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetUpdateCount (This : access Typ)
                            return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMoreResults (This : access Typ)
                            return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFetchDirection (This : access Typ;
                                P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFetchDirection (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFetchSize (This : access Typ;
                           P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFetchSize (This : access Typ)
                          return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSetConcurrency (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSetType (This : access Typ)
                              return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AddBatch (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ClearBatch (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ExecuteBatch (This : access Typ)
                          return Java.Int_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetConnection (This : access Typ)
                           return access Java.Sql.Connection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMoreResults (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetGeneratedKeys (This : access Typ)
                              return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ExecuteUpdate (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int)
                           return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ExecuteUpdate (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int_Arr : Java.Int_Arr)
                           return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ExecuteUpdate (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj)
                           return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Execute (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Execute (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int_Arr : Java.Int_Arr)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Execute (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String_Arr : access Java.Lang.String.Arr_Obj)
                     return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetResultSetHoldability (This : access Typ)
                                     return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsClosed (This : access Typ)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetPoolable (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsPoolable (This : access Typ)
                        return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CLOSE_CURRENT_RESULT : constant Java.Int;

   --  final
   KEEP_CURRENT_RESULT : constant Java.Int;

   --  final
   CLOSE_ALL_RESULTS : constant Java.Int;

   --  final
   SUCCESS_NO_INFO : constant Java.Int;

   --  final
   EXECUTE_FAILED : constant Java.Int;

   --  final
   RETURN_GENERATED_KEYS : constant Java.Int;

   --  final
   NO_GENERATED_KEYS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ExecuteQuery, "executeQuery");
   pragma Export (Java, ExecuteUpdate, "executeUpdate");
   pragma Export (Java, Close, "close");
   pragma Export (Java, GetMaxFieldSize, "getMaxFieldSize");
   pragma Export (Java, SetMaxFieldSize, "setMaxFieldSize");
   pragma Export (Java, GetMaxRows, "getMaxRows");
   pragma Export (Java, SetMaxRows, "setMaxRows");
   pragma Export (Java, SetEscapeProcessing, "setEscapeProcessing");
   pragma Export (Java, GetQueryTimeout, "getQueryTimeout");
   pragma Export (Java, SetQueryTimeout, "setQueryTimeout");
   pragma Export (Java, Cancel, "cancel");
   pragma Export (Java, GetWarnings, "getWarnings");
   pragma Export (Java, ClearWarnings, "clearWarnings");
   pragma Export (Java, SetCursorName, "setCursorName");
   pragma Export (Java, Execute, "execute");
   pragma Export (Java, GetResultSet, "getResultSet");
   pragma Export (Java, GetUpdateCount, "getUpdateCount");
   pragma Export (Java, GetMoreResults, "getMoreResults");
   pragma Export (Java, SetFetchDirection, "setFetchDirection");
   pragma Export (Java, GetFetchDirection, "getFetchDirection");
   pragma Export (Java, SetFetchSize, "setFetchSize");
   pragma Export (Java, GetFetchSize, "getFetchSize");
   pragma Export (Java, GetResultSetConcurrency, "getResultSetConcurrency");
   pragma Export (Java, GetResultSetType, "getResultSetType");
   pragma Export (Java, AddBatch, "addBatch");
   pragma Export (Java, ClearBatch, "clearBatch");
   pragma Export (Java, ExecuteBatch, "executeBatch");
   pragma Export (Java, GetConnection, "getConnection");
   pragma Export (Java, GetGeneratedKeys, "getGeneratedKeys");
   pragma Export (Java, GetResultSetHoldability, "getResultSetHoldability");
   pragma Export (Java, IsClosed, "isClosed");
   pragma Export (Java, SetPoolable, "setPoolable");
   pragma Export (Java, IsPoolable, "isPoolable");
   pragma Import (Java, CLOSE_CURRENT_RESULT, "CLOSE_CURRENT_RESULT");
   pragma Import (Java, KEEP_CURRENT_RESULT, "KEEP_CURRENT_RESULT");
   pragma Import (Java, CLOSE_ALL_RESULTS, "CLOSE_ALL_RESULTS");
   pragma Import (Java, SUCCESS_NO_INFO, "SUCCESS_NO_INFO");
   pragma Import (Java, EXECUTE_FAILED, "EXECUTE_FAILED");
   pragma Import (Java, RETURN_GENERATED_KEYS, "RETURN_GENERATED_KEYS");
   pragma Import (Java, NO_GENERATED_KEYS, "NO_GENERATED_KEYS");

end Java.Sql.Statement;
pragma Import (Java, Java.Sql.Statement, "java.sql.Statement");
pragma Extensions_Allowed (Off);
