pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Sql.Struct is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSQLTypeName (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAttributes (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAttributes (This : access Typ;
                           P1_Map : access Standard.Java.Util.Map.Typ'Class)
                           return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSQLTypeName, "getSQLTypeName");
   pragma Export (Java, GetAttributes, "getAttributes");

end Java.Sql.Struct;
pragma Import (Java, Java.Sql.Struct, "java.sql.Struct");
pragma Extensions_Allowed (Off);
