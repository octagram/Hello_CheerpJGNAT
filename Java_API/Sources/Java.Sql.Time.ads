pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Comparable;
with Java.Lang.Object;
with Java.Util.Date;

package Java.Sql.Time is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Util.Date.Typ(Serializable_I,
                              Cloneable_I,
                              Comparable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Time (P1_Long : Java.Long; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTime (This : access Typ;
                      P1_Long : Java.Long);

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Time.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Time);
   pragma Import (Java, SetTime, "setTime");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, ToString, "toString");

end Java.Sql.Time;
pragma Import (Java, Java.Sql.Time, "java.sql.Time");
pragma Extensions_Allowed (Off);
