pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Comparable;
with Java.Lang.Object;
with Java.Util.Date;

package Java.Sql.Timestamp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Util.Date.Typ(Serializable_I,
                              Cloneable_I,
                              Comparable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Timestamp (P1_Long : Java.Long; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTime (This : access Typ;
                      P1_Long : Java.Long);

   function GetTime (This : access Typ)
                     return Java.Long;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Sql.Timestamp.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetNanos (This : access Typ)
                      return Java.Int;

   procedure SetNanos (This : access Typ;
                       P1_Int : Java.Int);

   function Equals (This : access Typ;
                    P1_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class)
                    return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Before (This : access Typ;
                    P1_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class)
                    return Java.Boolean;

   function After (This : access Typ;
                   P1_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class)
                   return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class)
                       return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Date : access Standard.Java.Util.Date.Typ'Class)
                       return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Timestamp);
   pragma Import (Java, SetTime, "setTime");
   pragma Import (Java, GetTime, "getTime");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetNanos, "getNanos");
   pragma Import (Java, SetNanos, "setNanos");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Before, "before");
   pragma Import (Java, After, "after");
   pragma Import (Java, CompareTo, "compareTo");

end Java.Sql.Timestamp;
pragma Import (Java, Java.Sql.Timestamp, "java.sql.Timestamp");
pragma Extensions_Allowed (Off);
