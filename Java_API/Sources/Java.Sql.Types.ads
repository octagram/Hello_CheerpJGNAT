pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Sql.Types is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BIT : constant Java.Int;

   --  final
   TINYINT : constant Java.Int;

   --  final
   SMALLINT : constant Java.Int;

   --  final
   INTEGER : constant Java.Int;

   --  final
   BIGINT : constant Java.Int;

   --  final
   FLOAT : constant Java.Int;

   --  final
   REAL : constant Java.Int;

   --  final
   DOUBLE : constant Java.Int;

   --  final
   NUMERIC : constant Java.Int;

   --  final
   DECIMAL : constant Java.Int;

   --  final
   CHAR : constant Java.Int;

   --  final
   VARCHAR : constant Java.Int;

   --  final
   LONGVARCHAR : constant Java.Int;

   --  final
   DATE : constant Java.Int;

   --  final
   TIME : constant Java.Int;

   --  final
   TIMESTAMP : constant Java.Int;

   --  final
   BINARY : constant Java.Int;

   --  final
   VARBINARY : constant Java.Int;

   --  final
   LONGVARBINARY : constant Java.Int;

   --  final
   NULL_K : constant Java.Int;

   --  final
   OTHER : constant Java.Int;

   --  final
   JAVA_OBJECT : constant Java.Int;

   --  final
   DISTINCT : constant Java.Int;

   --  final
   STRUCT : constant Java.Int;

   --  final
   ARRAY_K : constant Java.Int;

   --  final
   BLOB : constant Java.Int;

   --  final
   CLOB : constant Java.Int;

   --  final
   REF_K : constant Java.Int;

   --  final
   DATALINK : constant Java.Int;

   --  final
   BOOLEAN : constant Java.Int;

   --  final
   ROWID : constant Java.Int;

   --  final
   NCHAR : constant Java.Int;

   --  final
   NVARCHAR : constant Java.Int;

   --  final
   LONGNVARCHAR : constant Java.Int;

   --  final
   NCLOB : constant Java.Int;

   --  final
   SQLXML : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, BIT, "BIT");
   pragma Import (Java, TINYINT, "TINYINT");
   pragma Import (Java, SMALLINT, "SMALLINT");
   pragma Import (Java, INTEGER, "INTEGER");
   pragma Import (Java, BIGINT, "BIGINT");
   pragma Import (Java, FLOAT, "FLOAT");
   pragma Import (Java, REAL, "REAL");
   pragma Import (Java, DOUBLE, "DOUBLE");
   pragma Import (Java, NUMERIC, "NUMERIC");
   pragma Import (Java, DECIMAL, "DECIMAL");
   pragma Import (Java, CHAR, "CHAR");
   pragma Import (Java, VARCHAR, "VARCHAR");
   pragma Import (Java, LONGVARCHAR, "LONGVARCHAR");
   pragma Import (Java, DATE, "DATE");
   pragma Import (Java, TIME, "TIME");
   pragma Import (Java, TIMESTAMP, "TIMESTAMP");
   pragma Import (Java, BINARY, "BINARY");
   pragma Import (Java, VARBINARY, "VARBINARY");
   pragma Import (Java, LONGVARBINARY, "LONGVARBINARY");
   pragma Import (Java, NULL_K, "NULL");
   pragma Import (Java, OTHER, "OTHER");
   pragma Import (Java, JAVA_OBJECT, "JAVA_OBJECT");
   pragma Import (Java, DISTINCT, "DISTINCT");
   pragma Import (Java, STRUCT, "STRUCT");
   pragma Import (Java, ARRAY_K, "ARRAY");
   pragma Import (Java, BLOB, "BLOB");
   pragma Import (Java, CLOB, "CLOB");
   pragma Import (Java, REF_K, "REF");
   pragma Import (Java, DATALINK, "DATALINK");
   pragma Import (Java, BOOLEAN, "BOOLEAN");
   pragma Import (Java, ROWID, "ROWID");
   pragma Import (Java, NCHAR, "NCHAR");
   pragma Import (Java, NVARCHAR, "NVARCHAR");
   pragma Import (Java, LONGNVARCHAR, "LONGNVARCHAR");
   pragma Import (Java, NCLOB, "NCLOB");
   pragma Import (Java, SQLXML, "SQLXML");

end Java.Sql.Types;
pragma Import (Java, Java.Sql.Types, "java.sql.Types");
pragma Extensions_Allowed (Off);
