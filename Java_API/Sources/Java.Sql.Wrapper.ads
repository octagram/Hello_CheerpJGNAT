pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
with Java.Lang.Object;

package Java.Sql.Wrapper is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Unwrap (This : access Typ;
                    P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsWrapperFor (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Unwrap, "unwrap");
   pragma Export (Java, IsWrapperFor, "isWrapperFor");

end Java.Sql.Wrapper;
pragma Import (Java, Java.Sql.Wrapper, "java.sql.Wrapper");
pragma Extensions_Allowed (Off);
