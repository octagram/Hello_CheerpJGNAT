pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Text.AttributedCharacterIterator.Attribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Attribute (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  protected
   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.InvalidObjectException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LANGUAGE : access Java.Text.AttributedCharacterIterator.Attribute.Typ'Class;

   --  final
   READING : access Java.Text.AttributedCharacterIterator.Attribute.Typ'Class;

   --  final
   INPUT_METHOD_SEGMENT : access Java.Text.AttributedCharacterIterator.Attribute.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Attribute);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, ReadResolve, "readResolve");
   pragma Import (Java, LANGUAGE, "LANGUAGE");
   pragma Import (Java, READING, "READING");
   pragma Import (Java, INPUT_METHOD_SEGMENT, "INPUT_METHOD_SEGMENT");

end Java.Text.AttributedCharacterIterator.Attribute;
pragma Import (Java, Java.Text.AttributedCharacterIterator.Attribute, "java.text.AttributedCharacterIterator$Attribute");
pragma Extensions_Allowed (Off);
