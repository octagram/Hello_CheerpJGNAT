pragma Extensions_Allowed (On);
limited with Java.Text.AttributedCharacterIterator.Attribute;
limited with Java.Util.Map;
limited with Java.Util.Set;
with Java.Lang.Object;
with Java.Text.CharacterIterator;

package Java.Text.AttributedCharacterIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CharacterIterator_I : Java.Text.CharacterIterator.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRunStart (This : access Typ)
                         return Java.Int is abstract;

   function GetRunStart (This : access Typ;
                         P1_Attribute : access Standard.Java.Text.AttributedCharacterIterator.Attribute.Typ'Class)
                         return Java.Int is abstract;

   function GetRunStart (This : access Typ;
                         P1_Set : access Standard.Java.Util.Set.Typ'Class)
                         return Java.Int is abstract;

   function GetRunLimit (This : access Typ)
                         return Java.Int is abstract;

   function GetRunLimit (This : access Typ;
                         P1_Attribute : access Standard.Java.Text.AttributedCharacterIterator.Attribute.Typ'Class)
                         return Java.Int is abstract;

   function GetRunLimit (This : access Typ;
                         P1_Set : access Standard.Java.Util.Set.Typ'Class)
                         return Java.Int is abstract;

   function GetAttributes (This : access Typ)
                           return access Java.Util.Map.Typ'Class is abstract;

   function GetAttribute (This : access Typ;
                          P1_Attribute : access Standard.Java.Text.AttributedCharacterIterator.Attribute.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function GetAllAttributeKeys (This : access Typ)
                                 return access Java.Util.Set.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRunStart, "getRunStart");
   pragma Export (Java, GetRunLimit, "getRunLimit");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, GetAllAttributeKeys, "getAllAttributeKeys");

end Java.Text.AttributedCharacterIterator;
pragma Import (Java, Java.Text.AttributedCharacterIterator, "java.text.AttributedCharacterIterator");
pragma Extensions_Allowed (Off);
