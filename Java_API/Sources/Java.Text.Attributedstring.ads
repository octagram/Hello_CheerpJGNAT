pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Text.AttributedCharacterIterator.Attribute;
limited with Java.Util.Map;
with Java.Lang.Object;

package Java.Text.AttributedString is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributedString (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_AttributedString (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_AttributedString (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_AttributedString (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   function New_AttributedString (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Attribute_Arr : access Java.Text.AttributedCharacterIterator.Attribute.Arr_Obj; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddAttribute (This : access Typ;
                           P1_Attribute : access Standard.Java.Text.AttributedCharacterIterator.Attribute.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddAttribute (This : access Typ;
                           P1_Attribute : access Standard.Java.Text.AttributedCharacterIterator.Attribute.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int);

   procedure AddAttributes (This : access Typ;
                            P1_Map : access Standard.Java.Util.Map.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int);

   function GetIterator (This : access Typ)
                         return access Java.Text.AttributedCharacterIterator.Typ'Class;

   function GetIterator (This : access Typ;
                         P1_Attribute_Arr : access Java.Text.AttributedCharacterIterator.Attribute.Arr_Obj)
                         return access Java.Text.AttributedCharacterIterator.Typ'Class;

   function GetIterator (This : access Typ;
                         P1_Attribute_Arr : access Java.Text.AttributedCharacterIterator.Attribute.Arr_Obj;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return access Java.Text.AttributedCharacterIterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributedString);
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, AddAttributes, "addAttributes");
   pragma Import (Java, GetIterator, "getIterator");

end Java.Text.AttributedString;
pragma Import (Java, Java.Text.AttributedString, "java.text.AttributedString");
pragma Extensions_Allowed (Off);
