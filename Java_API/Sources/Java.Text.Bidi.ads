pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
with Java.Lang.Object;

package Java.Text.Bidi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Bidi (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int; 
                      This : Ref := null)
                      return Ref;

   function New_Bidi (P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   function New_Bidi (P1_Char_Arr : Java.Char_Arr;
                      P2_Int : Java.Int;
                      P3_Byte_Arr : Java.Byte_Arr;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int;
                      P6_Int : Java.Int; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateLineBidi (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Text.Bidi.Typ'Class;

   function IsMixed (This : access Typ)
                     return Java.Boolean;

   function IsLeftToRight (This : access Typ)
                           return Java.Boolean;

   function IsRightToLeft (This : access Typ)
                           return Java.Boolean;

   function GetLength (This : access Typ)
                       return Java.Int;

   function BaseIsLeftToRight (This : access Typ)
                               return Java.Boolean;

   function GetBaseLevel (This : access Typ)
                          return Java.Int;

   function GetLevelAt (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int;

   function GetRunCount (This : access Typ)
                         return Java.Int;

   function GetRunLevel (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function GetRunStart (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function GetRunLimit (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function RequiresBidi (P1_Char_Arr : Java.Char_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int)
                          return Java.Boolean;

   procedure ReorderVisually (P1_Byte_Arr : Java.Byte_Arr;
                              P2_Int : Java.Int;
                              P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DIRECTION_LEFT_TO_RIGHT : constant Java.Int;

   --  final
   DIRECTION_RIGHT_TO_LEFT : constant Java.Int;

   --  final
   DIRECTION_DEFAULT_LEFT_TO_RIGHT : constant Java.Int;

   --  final
   DIRECTION_DEFAULT_RIGHT_TO_LEFT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Bidi);
   pragma Import (Java, CreateLineBidi, "createLineBidi");
   pragma Import (Java, IsMixed, "isMixed");
   pragma Import (Java, IsLeftToRight, "isLeftToRight");
   pragma Import (Java, IsRightToLeft, "isRightToLeft");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, BaseIsLeftToRight, "baseIsLeftToRight");
   pragma Import (Java, GetBaseLevel, "getBaseLevel");
   pragma Import (Java, GetLevelAt, "getLevelAt");
   pragma Import (Java, GetRunCount, "getRunCount");
   pragma Import (Java, GetRunLevel, "getRunLevel");
   pragma Import (Java, GetRunStart, "getRunStart");
   pragma Import (Java, GetRunLimit, "getRunLimit");
   pragma Import (Java, RequiresBidi, "requiresBidi");
   pragma Import (Java, ReorderVisually, "reorderVisually");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, DIRECTION_LEFT_TO_RIGHT, "DIRECTION_LEFT_TO_RIGHT");
   pragma Import (Java, DIRECTION_RIGHT_TO_LEFT, "DIRECTION_RIGHT_TO_LEFT");
   pragma Import (Java, DIRECTION_DEFAULT_LEFT_TO_RIGHT, "DIRECTION_DEFAULT_LEFT_TO_RIGHT");
   pragma Import (Java, DIRECTION_DEFAULT_RIGHT_TO_LEFT, "DIRECTION_DEFAULT_RIGHT_TO_LEFT");

end Java.Text.Bidi;
pragma Import (Java, Java.Text.Bidi, "java.text.Bidi");
pragma Extensions_Allowed (Off);
