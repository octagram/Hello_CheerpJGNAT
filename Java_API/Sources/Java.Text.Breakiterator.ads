pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.CharacterIterator;
limited with Java.Util.Locale;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Text.BreakIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_BreakIterator (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function First (This : access Typ)
                   return Java.Int is abstract;

   function Last (This : access Typ)
                  return Java.Int is abstract;

   function Next (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Int is abstract;

   function Next (This : access Typ)
                  return Java.Int is abstract;

   function Previous (This : access Typ)
                      return Java.Int is abstract;

   function Following (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int is abstract;

   function Preceding (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int;

   function IsBoundary (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   function Current (This : access Typ)
                     return Java.Int is abstract;

   function GetText (This : access Typ)
                     return access Java.Text.CharacterIterator.Typ'Class is abstract;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetText (This : access Typ;
                      P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class) is abstract;

   function GetWordInstance return access Java.Text.BreakIterator.Typ'Class;

   function GetWordInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Text.BreakIterator.Typ'Class;

   function GetLineInstance return access Java.Text.BreakIterator.Typ'Class;

   function GetLineInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Text.BreakIterator.Typ'Class;

   function GetCharacterInstance return access Java.Text.BreakIterator.Typ'Class;

   function GetCharacterInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                  return access Java.Text.BreakIterator.Typ'Class;

   function GetSentenceInstance return access Java.Text.BreakIterator.Typ'Class;

   function GetSentenceInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                 return access Java.Text.BreakIterator.Typ'Class;

   --  synchronized
   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DONE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BreakIterator);
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, First, "first");
   pragma Export (Java, Last, "last");
   pragma Export (Java, Next, "next");
   pragma Export (Java, Previous, "previous");
   pragma Export (Java, Following, "following");
   pragma Export (Java, Preceding, "preceding");
   pragma Export (Java, IsBoundary, "isBoundary");
   pragma Export (Java, Current, "current");
   pragma Export (Java, GetText, "getText");
   pragma Export (Java, SetText, "setText");
   pragma Export (Java, GetWordInstance, "getWordInstance");
   pragma Export (Java, GetLineInstance, "getLineInstance");
   pragma Export (Java, GetCharacterInstance, "getCharacterInstance");
   pragma Export (Java, GetSentenceInstance, "getSentenceInstance");
   pragma Export (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Import (Java, DONE, "DONE");

end Java.Text.BreakIterator;
pragma Import (Java, Java.Text.BreakIterator, "java.text.BreakIterator");
pragma Extensions_Allowed (Off);
