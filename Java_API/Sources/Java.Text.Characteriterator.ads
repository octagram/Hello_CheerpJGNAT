pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Text.CharacterIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function First (This : access Typ)
                   return Java.Char is abstract;

   function Last (This : access Typ)
                  return Java.Char is abstract;

   function Current (This : access Typ)
                     return Java.Char is abstract;

   function Next (This : access Typ)
                  return Java.Char is abstract;

   function Previous (This : access Typ)
                      return Java.Char is abstract;

   function SetIndex (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Char is abstract;

   function GetBeginIndex (This : access Typ)
                           return Java.Int is abstract;

   function GetEndIndex (This : access Typ)
                         return Java.Int is abstract;

   function GetIndex (This : access Typ)
                      return Java.Int is abstract;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DONE : constant Java.Char;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, First, "first");
   pragma Export (Java, Last, "last");
   pragma Export (Java, Current, "current");
   pragma Export (Java, Next, "next");
   pragma Export (Java, Previous, "previous");
   pragma Export (Java, SetIndex, "setIndex");
   pragma Export (Java, GetBeginIndex, "getBeginIndex");
   pragma Export (Java, GetEndIndex, "getEndIndex");
   pragma Export (Java, GetIndex, "getIndex");
   pragma Export (Java, Clone, "clone");
   pragma Import (Java, DONE, "DONE");

end Java.Text.CharacterIterator;
pragma Import (Java, Java.Text.CharacterIterator, "java.text.CharacterIterator");
pragma Extensions_Allowed (Off);
