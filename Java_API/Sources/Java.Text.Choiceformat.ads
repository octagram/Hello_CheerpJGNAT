pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Text.FieldPosition;
limited with Java.Text.ParsePosition;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.NumberFormat;

package Java.Text.ChoiceFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Text.NumberFormat.Typ(Serializable_I,
                                      Cloneable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ApplyPattern (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   function ToPattern (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChoiceFormat (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_ChoiceFormat (P1_Double_Arr : Java.Double_Arr;
                              P2_String_Arr : access Java.Lang.String.Arr_Obj; 
                              This : Ref := null)
                              return Ref;

   procedure SetChoices (This : access Typ;
                         P1_Double_Arr : Java.Double_Arr;
                         P2_String_Arr : access Java.Lang.String.Arr_Obj);

   function GetLimits (This : access Typ)
                       return Java.Double_Arr;

   function GetFormats (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function Format (This : access Typ;
                    P1_Long : Java.Long;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Format (This : access Typ;
                    P1_Double : Java.Double;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                   return access Java.Lang.Number.Typ'Class;

   --  final
   function NextDouble (P1_Double : Java.Double)
                        return Java.Double;

   --  final
   function PreviousDouble (P1_Double : Java.Double)
                            return Java.Double;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function NextDouble (P1_Double : Java.Double;
                        P2_Boolean : Java.Boolean)
                        return Java.Double;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ApplyPattern, "applyPattern");
   pragma Import (Java, ToPattern, "toPattern");
   pragma Java_Constructor (New_ChoiceFormat);
   pragma Import (Java, SetChoices, "setChoices");
   pragma Import (Java, GetLimits, "getLimits");
   pragma Import (Java, GetFormats, "getFormats");
   pragma Import (Java, Format, "format");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, NextDouble, "nextDouble");
   pragma Import (Java, PreviousDouble, "previousDouble");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Java.Text.ChoiceFormat;
pragma Import (Java, Java.Text.ChoiceFormat, "java.text.ChoiceFormat");
pragma Extensions_Allowed (Off);
