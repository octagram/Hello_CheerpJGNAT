pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.CharacterIterator;
with Java.Lang.Object;

package Java.Text.CollationElementIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ);

   function Next (This : access Typ)
                  return Java.Int;

   function Previous (This : access Typ)
                      return Java.Int;

   --  final
   function PrimaryOrder (P1_Int : Java.Int)
                          return Java.Int;

   --  final
   function SecondaryOrder (P1_Int : Java.Int)
                            return Java.Short;

   --  final
   function TertiaryOrder (P1_Int : Java.Int)
                           return Java.Short;

   procedure SetOffset (This : access Typ;
                        P1_Int : Java.Int);

   function GetOffset (This : access Typ)
                       return Java.Int;

   function GetMaxExpansion (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetText (This : access Typ;
                      P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NULLORDER : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Next, "next");
   pragma Import (Java, Previous, "previous");
   pragma Import (Java, PrimaryOrder, "primaryOrder");
   pragma Import (Java, SecondaryOrder, "secondaryOrder");
   pragma Import (Java, TertiaryOrder, "tertiaryOrder");
   pragma Import (Java, SetOffset, "setOffset");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetMaxExpansion, "getMaxExpansion");
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, NULLORDER, "NULLORDER");

end Java.Text.CollationElementIterator;
pragma Import (Java, Java.Text.CollationElementIterator, "java.text.CollationElementIterator");
pragma Extensions_Allowed (Off);
