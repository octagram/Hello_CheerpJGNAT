pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Text.CollationKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CompareTo (This : access Typ;
                       P1_CollationKey : access Standard.Java.Text.CollationKey.Typ'Class)
                       return Java.Int is abstract
                       with Export => "compareTo", Convention => Java;

   function GetSourceString (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function ToByteArray (This : access Typ)
                         return Java.Byte_Arr is abstract;

   --  protected
   function New_CollationKey (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int
                       with Import => "compareTo", Convention => Java;
private
   pragma Convention (Java, Typ);
   -- pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, GetSourceString, "getSourceString");
   pragma Export (Java, ToByteArray, "toByteArray");
   pragma Java_Constructor (New_CollationKey);

end Java.Text.CollationKey;
pragma Import (Java, Java.Text.CollationKey, "java.text.CollationKey");
pragma Extensions_Allowed (Off);
