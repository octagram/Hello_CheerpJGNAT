pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.CollationKey;
limited with Java.Util.Locale;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Comparator;

package Java.Text.Collator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparator_I : Java.Util.Comparator.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetInstance return access Java.Text.Collator.Typ'Class;

   --  synchronized
   function GetInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Text.Collator.Typ'Class;

   function Compare (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Int is abstract with Export => "compare", Convention => Java;

   function Compare (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int with Import => "compare", Convention => Java;

   function GetCollationKey (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Text.CollationKey.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                    return Java.Boolean;

   --  synchronized
   function GetStrength (This : access Typ)
                         return Java.Int;

   --  synchronized
   procedure SetStrength (This : access Typ;
                          P1_Int : Java.Int);

   --  synchronized
   function GetDecomposition (This : access Typ)
                              return Java.Int;

   --  synchronized
   procedure SetDecomposition (This : access Typ;
                               P1_Int : Java.Int);

   --  synchronized
   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   --  protected
   function New_Collator (This : Ref := null)
                          return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PRIMARY : constant Java.Int;

   --  final
   SECONDARY : constant Java.Int;

   --  final
   TERTIARY : constant Java.Int;

   --  final
   IDENTICAL : constant Java.Int;

   --  final
   NO_DECOMPOSITION : constant Java.Int;

   --  final
   CANONICAL_DECOMPOSITION : constant Java.Int;

   --  final
   FULL_DECOMPOSITION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetInstance, "getInstance");
   -- pragma Import (Java, Compare, "compare");
   pragma Export (Java, GetCollationKey, "getCollationKey");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetStrength, "getStrength");
   pragma Import (Java, SetStrength, "setStrength");
   pragma Import (Java, GetDecomposition, "getDecomposition");
   pragma Import (Java, SetDecomposition, "setDecomposition");
   pragma Import (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Import (Java, Clone, "clone");
   pragma Export (Java, HashCode, "hashCode");
   pragma Java_Constructor (New_Collator);
   pragma Import (Java, PRIMARY, "PRIMARY");
   pragma Import (Java, SECONDARY, "SECONDARY");
   pragma Import (Java, TERTIARY, "TERTIARY");
   pragma Import (Java, IDENTICAL, "IDENTICAL");
   pragma Import (Java, NO_DECOMPOSITION, "NO_DECOMPOSITION");
   pragma Import (Java, CANONICAL_DECOMPOSITION, "CANONICAL_DECOMPOSITION");
   pragma Import (Java, FULL_DECOMPOSITION, "FULL_DECOMPOSITION");

end Java.Text.Collator;
pragma Import (Java, Java.Text.Collator, "java.text.Collator");
pragma Extensions_Allowed (Off);
