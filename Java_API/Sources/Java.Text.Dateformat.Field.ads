pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Text.Format.Field;

package Java.Text.DateFormat.Field is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Text.Format.Field.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function OfCalendarField (P1_Int : Java.Int)
                             return access Java.Text.DateFormat.Field.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Field (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   function GetCalendarField (This : access Typ)
                              return Java.Int;

   --  protected
   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.InvalidObjectException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ERA : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   YEAR : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   MONTH : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   DAY_OF_MONTH : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   HOUR_OF_DAY1 : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   HOUR_OF_DAY0 : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   MINUTE : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   SECOND : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   MILLISECOND : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   DAY_OF_WEEK : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   DAY_OF_YEAR : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   DAY_OF_WEEK_IN_MONTH : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   WEEK_OF_YEAR : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   WEEK_OF_MONTH : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   AM_PM : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   HOUR1 : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   HOUR0 : access Java.Text.DateFormat.Field.Typ'Class;

   --  final
   TIME_ZONE : access Java.Text.DateFormat.Field.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, OfCalendarField, "ofCalendarField");
   pragma Java_Constructor (New_Field);
   pragma Import (Java, GetCalendarField, "getCalendarField");
   pragma Import (Java, ReadResolve, "readResolve");
   pragma Import (Java, ERA, "ERA");
   pragma Import (Java, YEAR, "YEAR");
   pragma Import (Java, MONTH, "MONTH");
   pragma Import (Java, DAY_OF_MONTH, "DAY_OF_MONTH");
   pragma Import (Java, HOUR_OF_DAY1, "HOUR_OF_DAY1");
   pragma Import (Java, HOUR_OF_DAY0, "HOUR_OF_DAY0");
   pragma Import (Java, MINUTE, "MINUTE");
   pragma Import (Java, SECOND, "SECOND");
   pragma Import (Java, MILLISECOND, "MILLISECOND");
   pragma Import (Java, DAY_OF_WEEK, "DAY_OF_WEEK");
   pragma Import (Java, DAY_OF_YEAR, "DAY_OF_YEAR");
   pragma Import (Java, DAY_OF_WEEK_IN_MONTH, "DAY_OF_WEEK_IN_MONTH");
   pragma Import (Java, WEEK_OF_YEAR, "WEEK_OF_YEAR");
   pragma Import (Java, WEEK_OF_MONTH, "WEEK_OF_MONTH");
   pragma Import (Java, AM_PM, "AM_PM");
   pragma Import (Java, HOUR1, "HOUR1");
   pragma Import (Java, HOUR0, "HOUR0");
   pragma Import (Java, TIME_ZONE, "TIME_ZONE");

end Java.Text.DateFormat.Field;
pragma Import (Java, Java.Text.DateFormat.Field, "java.text.DateFormat$Field");
pragma Extensions_Allowed (Off);
