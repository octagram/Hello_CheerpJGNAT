pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Text.FieldPosition;
limited with Java.Text.NumberFormat;
limited with Java.Text.ParsePosition;
limited with Java.Util.Calendar;
limited with Java.Util.Date;
limited with Java.Util.Locale;
limited with Java.Util.TimeZone;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.Format;

package Java.Text.DateFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Text.Format.Typ(Serializable_I,
                                         Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Calendar : access Java.Util.Calendar.Typ'Class;
      pragma Import (Java, Calendar, "calendar");

      --  protected
      NumberFormat : access Java.Text.NumberFormat.Typ'Class;
      pragma Import (Java, NumberFormat, "numberFormat");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Format (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Format (This : access Typ;
                    P1_Date : access Standard.Java.Util.Date.Typ'Class;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class is abstract;

   --  final
   function Format (This : access Typ;
                    P1_Date : access Standard.Java.Util.Date.Typ'Class)
                    return access Java.Lang.String.Typ'Class;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Java.Util.Date.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                   return access Java.Util.Date.Typ'Class is abstract;

   function ParseObject (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   --  final
   function GetTimeInstance return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetTimeInstance (P1_Int : Java.Int)
                             return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetTimeInstance (P1_Int : Java.Int;
                             P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetDateInstance return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetDateInstance (P1_Int : Java.Int)
                             return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetDateInstance (P1_Int : Java.Int;
                             P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetDateTimeInstance return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetDateTimeInstance (P1_Int : Java.Int;
                                 P2_Int : Java.Int)
                                 return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetDateTimeInstance (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                 return access Java.Text.DateFormat.Typ'Class;

   --  final
   function GetInstance return access Java.Text.DateFormat.Typ'Class;

   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   procedure SetCalendar (This : access Typ;
                          P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class);

   function GetCalendar (This : access Typ)
                         return access Java.Util.Calendar.Typ'Class;

   procedure SetNumberFormat (This : access Typ;
                              P1_NumberFormat : access Standard.Java.Text.NumberFormat.Typ'Class);

   function GetNumberFormat (This : access Typ)
                             return access Java.Text.NumberFormat.Typ'Class;

   procedure SetTimeZone (This : access Typ;
                          P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class);

   function GetTimeZone (This : access Typ)
                         return access Java.Util.TimeZone.Typ'Class;

   procedure SetLenient (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsLenient (This : access Typ)
                       return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   --  protected
   function New_DateFormat (This : Ref := null)
                            return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ERA_FIELD : constant Java.Int;

   --  final
   YEAR_FIELD : constant Java.Int;

   --  final
   MONTH_FIELD : constant Java.Int;

   --  final
   DATE_FIELD : constant Java.Int;

   --  final
   HOUR_OF_DAY1_FIELD : constant Java.Int;

   --  final
   HOUR_OF_DAY0_FIELD : constant Java.Int;

   --  final
   MINUTE_FIELD : constant Java.Int;

   --  final
   SECOND_FIELD : constant Java.Int;

   --  final
   MILLISECOND_FIELD : constant Java.Int;

   --  final
   DAY_OF_WEEK_FIELD : constant Java.Int;

   --  final
   DAY_OF_YEAR_FIELD : constant Java.Int;

   --  final
   DAY_OF_WEEK_IN_MONTH_FIELD : constant Java.Int;

   --  final
   WEEK_OF_YEAR_FIELD : constant Java.Int;

   --  final
   WEEK_OF_MONTH_FIELD : constant Java.Int;

   --  final
   AM_PM_FIELD : constant Java.Int;

   --  final
   HOUR1_FIELD : constant Java.Int;

   --  final
   HOUR0_FIELD : constant Java.Int;

   --  final
   TIMEZONE_FIELD : constant Java.Int;

   --  final
   FULL : constant Java.Int;

   --  final
   LONG : constant Java.Int;

   --  final
   MEDIUM : constant Java.Int;

   --  final
   SHORT : constant Java.Int;

   --  final
   DEFAULT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Format, "format");
   pragma Export (Java, Parse, "parse");
   pragma Export (Java, ParseObject, "parseObject");
   pragma Export (Java, GetTimeInstance, "getTimeInstance");
   pragma Export (Java, GetDateInstance, "getDateInstance");
   pragma Export (Java, GetDateTimeInstance, "getDateTimeInstance");
   pragma Export (Java, GetInstance, "getInstance");
   pragma Export (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Export (Java, SetCalendar, "setCalendar");
   pragma Export (Java, GetCalendar, "getCalendar");
   pragma Export (Java, SetNumberFormat, "setNumberFormat");
   pragma Export (Java, GetNumberFormat, "getNumberFormat");
   pragma Export (Java, SetTimeZone, "setTimeZone");
   pragma Export (Java, GetTimeZone, "getTimeZone");
   pragma Export (Java, SetLenient, "setLenient");
   pragma Export (Java, IsLenient, "isLenient");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, Clone, "clone");
   pragma Java_Constructor (New_DateFormat);
   pragma Import (Java, ERA_FIELD, "ERA_FIELD");
   pragma Import (Java, YEAR_FIELD, "YEAR_FIELD");
   pragma Import (Java, MONTH_FIELD, "MONTH_FIELD");
   pragma Import (Java, DATE_FIELD, "DATE_FIELD");
   pragma Import (Java, HOUR_OF_DAY1_FIELD, "HOUR_OF_DAY1_FIELD");
   pragma Import (Java, HOUR_OF_DAY0_FIELD, "HOUR_OF_DAY0_FIELD");
   pragma Import (Java, MINUTE_FIELD, "MINUTE_FIELD");
   pragma Import (Java, SECOND_FIELD, "SECOND_FIELD");
   pragma Import (Java, MILLISECOND_FIELD, "MILLISECOND_FIELD");
   pragma Import (Java, DAY_OF_WEEK_FIELD, "DAY_OF_WEEK_FIELD");
   pragma Import (Java, DAY_OF_YEAR_FIELD, "DAY_OF_YEAR_FIELD");
   pragma Import (Java, DAY_OF_WEEK_IN_MONTH_FIELD, "DAY_OF_WEEK_IN_MONTH_FIELD");
   pragma Import (Java, WEEK_OF_YEAR_FIELD, "WEEK_OF_YEAR_FIELD");
   pragma Import (Java, WEEK_OF_MONTH_FIELD, "WEEK_OF_MONTH_FIELD");
   pragma Import (Java, AM_PM_FIELD, "AM_PM_FIELD");
   pragma Import (Java, HOUR1_FIELD, "HOUR1_FIELD");
   pragma Import (Java, HOUR0_FIELD, "HOUR0_FIELD");
   pragma Import (Java, TIMEZONE_FIELD, "TIMEZONE_FIELD");
   pragma Import (Java, FULL, "FULL");
   pragma Import (Java, LONG, "LONG");
   pragma Import (Java, MEDIUM, "MEDIUM");
   pragma Import (Java, SHORT, "SHORT");
   pragma Import (Java, DEFAULT, "DEFAULT");

end Java.Text.DateFormat;
pragma Import (Java, Java.Text.DateFormat, "java.text.DateFormat");
pragma Extensions_Allowed (Off);
