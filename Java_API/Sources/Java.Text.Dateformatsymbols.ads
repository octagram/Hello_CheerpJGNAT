pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Text.DateFormatSymbols is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DateFormatSymbols (This : Ref := null)
                                   return Ref;

   function New_DateFormatSymbols (P1_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   --  final
   function GetInstance return access Java.Text.DateFormatSymbols.Typ'Class;

   --  final
   function GetInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Text.DateFormatSymbols.Typ'Class;

   function GetEras (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   procedure SetEras (This : access Typ;
                      P1_String_Arr : access Java.Lang.String.Arr_Obj);

   function GetMonths (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   procedure SetMonths (This : access Typ;
                        P1_String_Arr : access Java.Lang.String.Arr_Obj);

   function GetShortMonths (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   procedure SetShortMonths (This : access Typ;
                             P1_String_Arr : access Java.Lang.String.Arr_Obj);

   function GetWeekdays (This : access Typ)
                         return Standard.Java.Lang.Object.Ref;

   procedure SetWeekdays (This : access Typ;
                          P1_String_Arr : access Java.Lang.String.Arr_Obj);

   function GetShortWeekdays (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   procedure SetShortWeekdays (This : access Typ;
                               P1_String_Arr : access Java.Lang.String.Arr_Obj);

   function GetAmPmStrings (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   procedure SetAmPmStrings (This : access Typ;
                             P1_String_Arr : access Java.Lang.String.Arr_Obj);

   function GetZoneStrings (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   procedure SetZoneStrings (This : access Typ;
                             P1_String_Arr_2 : access Java.Lang.String.Arr_2_Obj);

   function GetLocalPatternChars (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   procedure SetLocalPatternChars (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DateFormatSymbols);
   pragma Import (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetEras, "getEras");
   pragma Import (Java, SetEras, "setEras");
   pragma Import (Java, GetMonths, "getMonths");
   pragma Import (Java, SetMonths, "setMonths");
   pragma Import (Java, GetShortMonths, "getShortMonths");
   pragma Import (Java, SetShortMonths, "setShortMonths");
   pragma Import (Java, GetWeekdays, "getWeekdays");
   pragma Import (Java, SetWeekdays, "setWeekdays");
   pragma Import (Java, GetShortWeekdays, "getShortWeekdays");
   pragma Import (Java, SetShortWeekdays, "setShortWeekdays");
   pragma Import (Java, GetAmPmStrings, "getAmPmStrings");
   pragma Import (Java, SetAmPmStrings, "setAmPmStrings");
   pragma Import (Java, GetZoneStrings, "getZoneStrings");
   pragma Import (Java, SetZoneStrings, "setZoneStrings");
   pragma Import (Java, GetLocalPatternChars, "getLocalPatternChars");
   pragma Import (Java, SetLocalPatternChars, "setLocalPatternChars");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Java.Text.DateFormatSymbols;
pragma Import (Java, Java.Text.DateFormatSymbols, "java.text.DateFormatSymbols");
pragma Extensions_Allowed (Off);
