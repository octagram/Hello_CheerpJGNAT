pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Math.RoundingMode;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Text.DecimalFormatSymbols;
limited with Java.Text.FieldPosition;
limited with Java.Text.ParsePosition;
limited with Java.Util.Currency;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.NumberFormat;

package Java.Text.DecimalFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Text.NumberFormat.Typ(Serializable_I,
                                      Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DecimalFormat (This : Ref := null)
                               return Ref;

   function New_DecimalFormat (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_DecimalFormat (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_DecimalFormatSymbols : access Standard.Java.Text.DecimalFormatSymbols.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Format (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Format (This : access Typ;
                    P1_Double : Java.Double;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Format (This : access Typ;
                    P1_Long : Java.Long;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function FormatToCharacterIterator (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                       return access Java.Text.AttributedCharacterIterator.Typ'Class;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                   return access Java.Lang.Number.Typ'Class;

   function GetDecimalFormatSymbols (This : access Typ)
                                     return access Java.Text.DecimalFormatSymbols.Typ'Class;

   procedure SetDecimalFormatSymbols (This : access Typ;
                                      P1_DecimalFormatSymbols : access Standard.Java.Text.DecimalFormatSymbols.Typ'Class);

   function GetPositivePrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure SetPositivePrefix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetNegativePrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure SetNegativePrefix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPositiveSuffix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure SetPositiveSuffix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetNegativeSuffix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure SetNegativeSuffix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetMultiplier (This : access Typ)
                           return Java.Int;

   procedure SetMultiplier (This : access Typ;
                            P1_Int : Java.Int);

   function GetGroupingSize (This : access Typ)
                             return Java.Int;

   procedure SetGroupingSize (This : access Typ;
                              P1_Int : Java.Int);

   function IsDecimalSeparatorAlwaysShown (This : access Typ)
                                           return Java.Boolean;

   procedure SetDecimalSeparatorAlwaysShown (This : access Typ;
                                             P1_Boolean : Java.Boolean);

   function IsParseBigDecimal (This : access Typ)
                               return Java.Boolean;

   procedure SetParseBigDecimal (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToPattern (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function ToLocalizedPattern (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure ApplyPattern (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure ApplyLocalizedPattern (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetMaximumIntegerDigits (This : access Typ;
                                      P1_Int : Java.Int);

   procedure SetMinimumIntegerDigits (This : access Typ;
                                      P1_Int : Java.Int);

   procedure SetMaximumFractionDigits (This : access Typ;
                                       P1_Int : Java.Int);

   procedure SetMinimumFractionDigits (This : access Typ;
                                       P1_Int : Java.Int);

   function GetMaximumIntegerDigits (This : access Typ)
                                     return Java.Int;

   function GetMinimumIntegerDigits (This : access Typ)
                                     return Java.Int;

   function GetMaximumFractionDigits (This : access Typ)
                                      return Java.Int;

   function GetMinimumFractionDigits (This : access Typ)
                                      return Java.Int;

   function GetCurrency (This : access Typ)
                         return access Java.Util.Currency.Typ'Class;

   procedure SetCurrency (This : access Typ;
                          P1_Currency : access Standard.Java.Util.Currency.Typ'Class);

   function GetRoundingMode (This : access Typ)
                             return access Java.Math.RoundingMode.Typ'Class;

   procedure SetRoundingMode (This : access Typ;
                              P1_RoundingMode : access Standard.Java.Math.RoundingMode.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DecimalFormat);
   pragma Import (Java, Format, "format");
   pragma Import (Java, FormatToCharacterIterator, "formatToCharacterIterator");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, GetDecimalFormatSymbols, "getDecimalFormatSymbols");
   pragma Import (Java, SetDecimalFormatSymbols, "setDecimalFormatSymbols");
   pragma Import (Java, GetPositivePrefix, "getPositivePrefix");
   pragma Import (Java, SetPositivePrefix, "setPositivePrefix");
   pragma Import (Java, GetNegativePrefix, "getNegativePrefix");
   pragma Import (Java, SetNegativePrefix, "setNegativePrefix");
   pragma Import (Java, GetPositiveSuffix, "getPositiveSuffix");
   pragma Import (Java, SetPositiveSuffix, "setPositiveSuffix");
   pragma Import (Java, GetNegativeSuffix, "getNegativeSuffix");
   pragma Import (Java, SetNegativeSuffix, "setNegativeSuffix");
   pragma Import (Java, GetMultiplier, "getMultiplier");
   pragma Import (Java, SetMultiplier, "setMultiplier");
   pragma Import (Java, GetGroupingSize, "getGroupingSize");
   pragma Import (Java, SetGroupingSize, "setGroupingSize");
   pragma Import (Java, IsDecimalSeparatorAlwaysShown, "isDecimalSeparatorAlwaysShown");
   pragma Import (Java, SetDecimalSeparatorAlwaysShown, "setDecimalSeparatorAlwaysShown");
   pragma Import (Java, IsParseBigDecimal, "isParseBigDecimal");
   pragma Import (Java, SetParseBigDecimal, "setParseBigDecimal");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToPattern, "toPattern");
   pragma Import (Java, ToLocalizedPattern, "toLocalizedPattern");
   pragma Import (Java, ApplyPattern, "applyPattern");
   pragma Import (Java, ApplyLocalizedPattern, "applyLocalizedPattern");
   pragma Import (Java, SetMaximumIntegerDigits, "setMaximumIntegerDigits");
   pragma Import (Java, SetMinimumIntegerDigits, "setMinimumIntegerDigits");
   pragma Import (Java, SetMaximumFractionDigits, "setMaximumFractionDigits");
   pragma Import (Java, SetMinimumFractionDigits, "setMinimumFractionDigits");
   pragma Import (Java, GetMaximumIntegerDigits, "getMaximumIntegerDigits");
   pragma Import (Java, GetMinimumIntegerDigits, "getMinimumIntegerDigits");
   pragma Import (Java, GetMaximumFractionDigits, "getMaximumFractionDigits");
   pragma Import (Java, GetMinimumFractionDigits, "getMinimumFractionDigits");
   pragma Import (Java, GetCurrency, "getCurrency");
   pragma Import (Java, SetCurrency, "setCurrency");
   pragma Import (Java, GetRoundingMode, "getRoundingMode");
   pragma Import (Java, SetRoundingMode, "setRoundingMode");

end Java.Text.DecimalFormat;
pragma Import (Java, Java.Text.DecimalFormat, "java.text.DecimalFormat");
pragma Extensions_Allowed (Off);
