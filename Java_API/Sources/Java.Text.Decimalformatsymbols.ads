pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Currency;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Text.DecimalFormatSymbols is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DecimalFormatSymbols (This : Ref := null)
                                      return Ref;

   function New_DecimalFormatSymbols (P1_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   --  final
   function GetInstance return access Java.Text.DecimalFormatSymbols.Typ'Class;

   --  final
   function GetInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Text.DecimalFormatSymbols.Typ'Class;

   function GetZeroDigit (This : access Typ)
                          return Java.Char;

   procedure SetZeroDigit (This : access Typ;
                           P1_Char : Java.Char);

   function GetGroupingSeparator (This : access Typ)
                                  return Java.Char;

   procedure SetGroupingSeparator (This : access Typ;
                                   P1_Char : Java.Char);

   function GetDecimalSeparator (This : access Typ)
                                 return Java.Char;

   procedure SetDecimalSeparator (This : access Typ;
                                  P1_Char : Java.Char);

   function GetPerMill (This : access Typ)
                        return Java.Char;

   procedure SetPerMill (This : access Typ;
                         P1_Char : Java.Char);

   function GetPercent (This : access Typ)
                        return Java.Char;

   procedure SetPercent (This : access Typ;
                         P1_Char : Java.Char);

   function GetDigit (This : access Typ)
                      return Java.Char;

   procedure SetDigit (This : access Typ;
                       P1_Char : Java.Char);

   function GetPatternSeparator (This : access Typ)
                                 return Java.Char;

   procedure SetPatternSeparator (This : access Typ;
                                  P1_Char : Java.Char);

   function GetInfinity (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetInfinity (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetNaN (This : access Typ)
                    return access Java.Lang.String.Typ'Class;

   procedure SetNaN (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetMinusSign (This : access Typ)
                          return Java.Char;

   procedure SetMinusSign (This : access Typ;
                           P1_Char : Java.Char);

   function GetCurrencySymbol (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure SetCurrencySymbol (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetInternationalCurrencySymbol (This : access Typ)
                                            return access Java.Lang.String.Typ'Class;

   procedure SetInternationalCurrencySymbol (This : access Typ;
                                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetCurrency (This : access Typ)
                         return access Java.Util.Currency.Typ'Class;

   procedure SetCurrency (This : access Typ;
                          P1_Currency : access Standard.Java.Util.Currency.Typ'Class);

   function GetMonetaryDecimalSeparator (This : access Typ)
                                         return Java.Char;

   procedure SetMonetaryDecimalSeparator (This : access Typ;
                                          P1_Char : Java.Char);

   function GetExponentSeparator (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   procedure SetExponentSeparator (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DecimalFormatSymbols);
   pragma Import (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetZeroDigit, "getZeroDigit");
   pragma Import (Java, SetZeroDigit, "setZeroDigit");
   pragma Import (Java, GetGroupingSeparator, "getGroupingSeparator");
   pragma Import (Java, SetGroupingSeparator, "setGroupingSeparator");
   pragma Import (Java, GetDecimalSeparator, "getDecimalSeparator");
   pragma Import (Java, SetDecimalSeparator, "setDecimalSeparator");
   pragma Import (Java, GetPerMill, "getPerMill");
   pragma Import (Java, SetPerMill, "setPerMill");
   pragma Import (Java, GetPercent, "getPercent");
   pragma Import (Java, SetPercent, "setPercent");
   pragma Import (Java, GetDigit, "getDigit");
   pragma Import (Java, SetDigit, "setDigit");
   pragma Import (Java, GetPatternSeparator, "getPatternSeparator");
   pragma Import (Java, SetPatternSeparator, "setPatternSeparator");
   pragma Import (Java, GetInfinity, "getInfinity");
   pragma Import (Java, SetInfinity, "setInfinity");
   pragma Import (Java, GetNaN, "getNaN");
   pragma Import (Java, SetNaN, "setNaN");
   pragma Import (Java, GetMinusSign, "getMinusSign");
   pragma Import (Java, SetMinusSign, "setMinusSign");
   pragma Import (Java, GetCurrencySymbol, "getCurrencySymbol");
   pragma Import (Java, SetCurrencySymbol, "setCurrencySymbol");
   pragma Import (Java, GetInternationalCurrencySymbol, "getInternationalCurrencySymbol");
   pragma Import (Java, SetInternationalCurrencySymbol, "setInternationalCurrencySymbol");
   pragma Import (Java, GetCurrency, "getCurrency");
   pragma Import (Java, SetCurrency, "setCurrency");
   pragma Import (Java, GetMonetaryDecimalSeparator, "getMonetaryDecimalSeparator");
   pragma Import (Java, SetMonetaryDecimalSeparator, "setMonetaryDecimalSeparator");
   pragma Import (Java, GetExponentSeparator, "getExponentSeparator");
   pragma Import (Java, SetExponentSeparator, "setExponentSeparator");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Text.DecimalFormatSymbols;
pragma Import (Java, Java.Text.DecimalFormatSymbols, "java.text.DecimalFormatSymbols");
pragma Extensions_Allowed (Off);
