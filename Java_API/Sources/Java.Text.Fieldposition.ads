pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.Format.Field;
with Java.Lang.Object;

package Java.Text.FieldPosition is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FieldPosition (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   function New_FieldPosition (P1_Field : access Standard.Java.Text.Format.Field.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_FieldPosition (P1_Field : access Standard.Java.Text.Format.Field.Typ'Class;
                               P2_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFieldAttribute (This : access Typ)
                               return access Java.Text.Format.Field.Typ'Class;

   function GetField (This : access Typ)
                      return Java.Int;

   function GetBeginIndex (This : access Typ)
                           return Java.Int;

   function GetEndIndex (This : access Typ)
                         return Java.Int;

   procedure SetBeginIndex (This : access Typ;
                            P1_Int : Java.Int);

   procedure SetEndIndex (This : access Typ;
                          P1_Int : Java.Int);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FieldPosition);
   pragma Import (Java, GetFieldAttribute, "getFieldAttribute");
   pragma Import (Java, GetField, "getField");
   pragma Import (Java, GetBeginIndex, "getBeginIndex");
   pragma Import (Java, GetEndIndex, "getEndIndex");
   pragma Import (Java, SetBeginIndex, "setBeginIndex");
   pragma Import (Java, SetEndIndex, "setEndIndex");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Text.FieldPosition;
pragma Import (Java, Java.Text.FieldPosition, "java.text.FieldPosition");
pragma Extensions_Allowed (Off);
