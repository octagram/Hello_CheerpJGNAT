pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Text.FieldPosition;
limited with Java.Text.ParsePosition;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Text.Format is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Format (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Format (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.String.Typ'Class
                    with Import => "format", Convention => Java;

   function Format (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class is abstract
                    with Export => "format", Convention => Java;

   function FormatToCharacterIterator (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                       return access Java.Text.AttributedCharacterIterator.Typ'Class;

   function ParseObject (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract
                         with Export => "parseObject", Convention => Java;

   function ParseObject (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class
                         with Import => "parseObject", Convention => Java;
   --  can raise Java.Text.ParseException.Except

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Format);
   -- pragma Import (Java, Format, "format");
   pragma Import (Java, FormatToCharacterIterator, "formatToCharacterIterator");
   -- pragma Import (Java, ParseObject, "parseObject");
   pragma Import (Java, Clone, "clone");

end Java.Text.Format;
pragma Import (Java, Java.Text.Format, "java.text.Format");
pragma Extensions_Allowed (Off);
