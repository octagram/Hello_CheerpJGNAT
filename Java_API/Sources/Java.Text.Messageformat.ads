pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Text.FieldPosition;
limited with Java.Text.ParsePosition;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.Format;

package Java.Text.MessageFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Text.Format.Typ(Serializable_I,
                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MessageFormat (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_MessageFormat (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   procedure ApplyPattern (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   function ToPattern (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   procedure SetFormatsByArgumentIndex (This : access Typ;
                                        P1_Format_Arr : access Java.Text.Format.Arr_Obj);

   procedure SetFormats (This : access Typ;
                         P1_Format_Arr : access Java.Text.Format.Arr_Obj);

   procedure SetFormatByArgumentIndex (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_Format : access Standard.Java.Text.Format.Typ'Class);

   procedure SetFormat (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Format : access Standard.Java.Text.Format.Typ'Class);

   function GetFormatsByArgumentIndex (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   function GetFormats (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   --  final
   function Format (This : access Typ;
                    P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function Format (P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.String.Typ'Class;

   --  final
   function Format (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function FormatToCharacterIterator (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                       return access Java.Text.AttributedCharacterIterator.Typ'Class;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                   return Standard.Java.Lang.Object.Ref;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Text.ParseException.Except

   function ParseObject (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MessageFormat);
   pragma Import (Java, SetLocale, "setLocale");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, ApplyPattern, "applyPattern");
   pragma Import (Java, ToPattern, "toPattern");
   pragma Import (Java, SetFormatsByArgumentIndex, "setFormatsByArgumentIndex");
   pragma Import (Java, SetFormats, "setFormats");
   pragma Import (Java, SetFormatByArgumentIndex, "setFormatByArgumentIndex");
   pragma Import (Java, SetFormat, "setFormat");
   pragma Import (Java, GetFormatsByArgumentIndex, "getFormatsByArgumentIndex");
   pragma Import (Java, GetFormats, "getFormats");
   pragma Import (Java, Format, "format");
   pragma Import (Java, FormatToCharacterIterator, "formatToCharacterIterator");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, ParseObject, "parseObject");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Text.MessageFormat;
pragma Import (Java, Java.Text.MessageFormat, "java.text.MessageFormat");
pragma Extensions_Allowed (Off);
