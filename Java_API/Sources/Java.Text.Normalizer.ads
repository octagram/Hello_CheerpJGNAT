pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Text.Normalizer.Form;
with Java.Lang.Object;

package Java.Text.Normalizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Normalize (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                       P2_Form : access Standard.Java.Text.Normalizer.Form.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function IsNormalized (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                          P2_Form : access Standard.Java.Text.Normalizer.Form.Typ'Class)
                          return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Normalize, "normalize");
   pragma Import (Java, IsNormalized, "isNormalized");

end Java.Text.Normalizer;
pragma Import (Java, Java.Text.Normalizer, "java.text.Normalizer");
pragma Extensions_Allowed (Off);
