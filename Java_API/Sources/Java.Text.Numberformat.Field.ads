pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Text.Format.Field;

package Java.Text.NumberFormat.Field is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Text.Format.Field.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Field (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.InvalidObjectException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INTEGER : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   FRACTION : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   EXPONENT : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   DECIMAL_SEPARATOR : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   SIGN : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   GROUPING_SEPARATOR : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   EXPONENT_SYMBOL : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   PERCENT : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   PERMILLE : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   CURRENCY : access Java.Text.NumberFormat.Field.Typ'Class;

   --  final
   EXPONENT_SIGN : access Java.Text.NumberFormat.Field.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Field);
   pragma Import (Java, ReadResolve, "readResolve");
   pragma Import (Java, INTEGER, "INTEGER");
   pragma Import (Java, FRACTION, "FRACTION");
   pragma Import (Java, EXPONENT, "EXPONENT");
   pragma Import (Java, DECIMAL_SEPARATOR, "DECIMAL_SEPARATOR");
   pragma Import (Java, SIGN, "SIGN");
   pragma Import (Java, GROUPING_SEPARATOR, "GROUPING_SEPARATOR");
   pragma Import (Java, EXPONENT_SYMBOL, "EXPONENT_SYMBOL");
   pragma Import (Java, PERCENT, "PERCENT");
   pragma Import (Java, PERMILLE, "PERMILLE");
   pragma Import (Java, CURRENCY, "CURRENCY");
   pragma Import (Java, EXPONENT_SIGN, "EXPONENT_SIGN");

end Java.Text.NumberFormat.Field;
pragma Import (Java, Java.Text.NumberFormat.Field, "java.text.NumberFormat$Field");
pragma Extensions_Allowed (Off);
