pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Math.RoundingMode;
limited with Java.Text.FieldPosition;
limited with Java.Text.ParsePosition;
limited with Java.Util.Currency;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.Format;

package Java.Text.NumberFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Text.Format.Typ(Serializable_I,
                                         Cloneable_I)
      with null record;

   --  protected
   function New_NumberFormat (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Format (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   --  final
   function ParseObject (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   --  final
   function Format (This : access Typ;
                    P1_Double : Java.Double)
                    return access Java.Lang.String.Typ'Class;

   --  final
   function Format (This : access Typ;
                    P1_Long : Java.Long)
                    return access Java.Lang.String.Typ'Class;

   function Format (This : access Typ;
                    P1_Double : Java.Double;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class is abstract;

   function Format (This : access Typ;
                    P1_Long : Java.Long;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class is abstract;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                   return access Java.Lang.Number.Typ'Class is abstract;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Java.Lang.Number.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   function IsParseIntegerOnly (This : access Typ)
                                return Java.Boolean;

   procedure SetParseIntegerOnly (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   --  final
   function GetInstance return access Java.Text.NumberFormat.Typ'Class;

   function GetInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Text.NumberFormat.Typ'Class;

   --  final
   function GetNumberInstance return access Java.Text.NumberFormat.Typ'Class;

   function GetNumberInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                               return access Java.Text.NumberFormat.Typ'Class;

   --  final
   function GetIntegerInstance return access Java.Text.NumberFormat.Typ'Class;

   function GetIntegerInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                return access Java.Text.NumberFormat.Typ'Class;

   --  final
   function GetCurrencyInstance return access Java.Text.NumberFormat.Typ'Class;

   function GetCurrencyInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                 return access Java.Text.NumberFormat.Typ'Class;

   --  final
   function GetPercentInstance return access Java.Text.NumberFormat.Typ'Class;

   function GetPercentInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                return access Java.Text.NumberFormat.Typ'Class;

   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function IsGroupingUsed (This : access Typ)
                            return Java.Boolean;

   procedure SetGroupingUsed (This : access Typ;
                              P1_Boolean : Java.Boolean);

   function GetMaximumIntegerDigits (This : access Typ)
                                     return Java.Int;

   procedure SetMaximumIntegerDigits (This : access Typ;
                                      P1_Int : Java.Int);

   function GetMinimumIntegerDigits (This : access Typ)
                                     return Java.Int;

   procedure SetMinimumIntegerDigits (This : access Typ;
                                      P1_Int : Java.Int);

   function GetMaximumFractionDigits (This : access Typ)
                                      return Java.Int;

   procedure SetMaximumFractionDigits (This : access Typ;
                                       P1_Int : Java.Int);

   function GetMinimumFractionDigits (This : access Typ)
                                      return Java.Int;

   procedure SetMinimumFractionDigits (This : access Typ;
                                       P1_Int : Java.Int);

   function GetCurrency (This : access Typ)
                         return access Java.Util.Currency.Typ'Class;

   procedure SetCurrency (This : access Typ;
                          P1_Currency : access Standard.Java.Util.Currency.Typ'Class);

   function GetRoundingMode (This : access Typ)
                             return access Java.Math.RoundingMode.Typ'Class;

   procedure SetRoundingMode (This : access Typ;
                              P1_RoundingMode : access Standard.Java.Math.RoundingMode.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INTEGER_FIELD : constant Java.Int;

   --  final
   FRACTION_FIELD : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NumberFormat);
   pragma Export (Java, Format, "format");
   pragma Export (Java, ParseObject, "parseObject");
   pragma Export (Java, Parse, "parse");
   pragma Export (Java, IsParseIntegerOnly, "isParseIntegerOnly");
   pragma Export (Java, SetParseIntegerOnly, "setParseIntegerOnly");
   pragma Export (Java, GetInstance, "getInstance");
   pragma Export (Java, GetNumberInstance, "getNumberInstance");
   pragma Export (Java, GetIntegerInstance, "getIntegerInstance");
   pragma Export (Java, GetCurrencyInstance, "getCurrencyInstance");
   pragma Export (Java, GetPercentInstance, "getPercentInstance");
   pragma Export (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, IsGroupingUsed, "isGroupingUsed");
   pragma Export (Java, SetGroupingUsed, "setGroupingUsed");
   pragma Export (Java, GetMaximumIntegerDigits, "getMaximumIntegerDigits");
   pragma Export (Java, SetMaximumIntegerDigits, "setMaximumIntegerDigits");
   pragma Export (Java, GetMinimumIntegerDigits, "getMinimumIntegerDigits");
   pragma Export (Java, SetMinimumIntegerDigits, "setMinimumIntegerDigits");
   pragma Export (Java, GetMaximumFractionDigits, "getMaximumFractionDigits");
   pragma Export (Java, SetMaximumFractionDigits, "setMaximumFractionDigits");
   pragma Export (Java, GetMinimumFractionDigits, "getMinimumFractionDigits");
   pragma Export (Java, SetMinimumFractionDigits, "setMinimumFractionDigits");
   pragma Export (Java, GetCurrency, "getCurrency");
   pragma Export (Java, SetCurrency, "setCurrency");
   pragma Export (Java, GetRoundingMode, "getRoundingMode");
   pragma Export (Java, SetRoundingMode, "setRoundingMode");
   pragma Import (Java, INTEGER_FIELD, "INTEGER_FIELD");
   pragma Import (Java, FRACTION_FIELD, "FRACTION_FIELD");

end Java.Text.NumberFormat;
pragma Import (Java, Java.Text.NumberFormat, "java.text.NumberFormat");
pragma Extensions_Allowed (Off);
