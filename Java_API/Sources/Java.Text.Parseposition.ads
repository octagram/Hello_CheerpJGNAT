pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Text.ParsePosition is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIndex (This : access Typ)
                      return Java.Int;

   procedure SetIndex (This : access Typ;
                       P1_Int : Java.Int);

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ParsePosition (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   procedure SetErrorIndex (This : access Typ;
                            P1_Int : Java.Int);

   function GetErrorIndex (This : access Typ)
                           return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, SetIndex, "setIndex");
   pragma Java_Constructor (New_ParsePosition);
   pragma Import (Java, SetErrorIndex, "setErrorIndex");
   pragma Import (Java, GetErrorIndex, "getErrorIndex");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Text.ParsePosition;
pragma Import (Java, Java.Text.ParsePosition, "java.text.ParsePosition");
pragma Extensions_Allowed (Off);
