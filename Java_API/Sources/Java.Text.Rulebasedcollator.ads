pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.CharacterIterator;
limited with Java.Text.CollationElementIterator;
limited with Java.Text.CollationKey;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.Collator;
with Java.Util.Comparator;

package Java.Text.RuleBasedCollator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparator_I : Java.Util.Comparator.Ref)
    is new Java.Text.Collator.Typ(Cloneable_I,
                                  Comparator_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RuleBasedCollator (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Java.Text.ParseException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRules (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetCollationElementIterator (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                                         return access Java.Text.CollationElementIterator.Typ'Class;

   function GetCollationElementIterator (This : access Typ;
                                         P1_CharacterIterator : access Standard.Java.Text.CharacterIterator.Typ'Class)
                                         return access Java.Text.CollationElementIterator.Typ'Class;

   --  synchronized
   function Compare (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Int;

   --  synchronized
   function GetCollationKey (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Text.CollationKey.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RuleBasedCollator);
   pragma Import (Java, GetRules, "getRules");
   pragma Import (Java, GetCollationElementIterator, "getCollationElementIterator");
   pragma Import (Java, Compare, "compare");
   pragma Import (Java, GetCollationKey, "getCollationKey");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Text.RuleBasedCollator;
pragma Import (Java, Java.Text.RuleBasedCollator, "java.text.RuleBasedCollator");
pragma Extensions_Allowed (Off);
