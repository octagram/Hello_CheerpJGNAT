pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Text.AttributedCharacterIterator;
limited with Java.Text.DateFormatSymbols;
limited with Java.Text.FieldPosition;
limited with Java.Text.ParsePosition;
limited with Java.Util.Date;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.DateFormat;

package Java.Text.SimpleDateFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Text.DateFormat.Typ(Serializable_I,
                                    Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleDateFormat (This : Ref := null)
                                  return Ref;

   function New_SimpleDateFormat (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_SimpleDateFormat (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_SimpleDateFormat (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_DateFormatSymbols : access Standard.Java.Text.DateFormatSymbols.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Set2DigitYearStart (This : access Typ;
                                 P1_Date : access Standard.Java.Util.Date.Typ'Class);

   function Get2DigitYearStart (This : access Typ)
                                return access Java.Util.Date.Typ'Class;

   function Format (This : access Typ;
                    P1_Date : access Standard.Java.Util.Date.Typ'Class;
                    P2_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                    P3_FieldPosition : access Standard.Java.Text.FieldPosition.Typ'Class)
                    return access Java.Lang.StringBuffer.Typ'Class;

   function FormatToCharacterIterator (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                       return access Java.Text.AttributedCharacterIterator.Typ'Class;

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_ParsePosition : access Standard.Java.Text.ParsePosition.Typ'Class)
                   return access Java.Util.Date.Typ'Class;

   function ToPattern (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function ToLocalizedPattern (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure ApplyPattern (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure ApplyLocalizedPattern (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetDateFormatSymbols (This : access Typ)
                                  return access Java.Text.DateFormatSymbols.Typ'Class;

   procedure SetDateFormatSymbols (This : access Typ;
                                   P1_DateFormatSymbols : access Standard.Java.Text.DateFormatSymbols.Typ'Class);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleDateFormat);
   pragma Import (Java, Set2DigitYearStart, "set2DigitYearStart");
   pragma Import (Java, Get2DigitYearStart, "get2DigitYearStart");
   pragma Import (Java, Format, "format");
   pragma Import (Java, FormatToCharacterIterator, "formatToCharacterIterator");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, ToPattern, "toPattern");
   pragma Import (Java, ToLocalizedPattern, "toLocalizedPattern");
   pragma Import (Java, ApplyPattern, "applyPattern");
   pragma Import (Java, ApplyLocalizedPattern, "applyLocalizedPattern");
   pragma Import (Java, GetDateFormatSymbols, "getDateFormatSymbols");
   pragma Import (Java, SetDateFormatSymbols, "setDateFormatSymbols");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Java.Text.SimpleDateFormat;
pragma Import (Java, Java.Text.SimpleDateFormat, "java.text.SimpleDateFormat");
pragma Extensions_Allowed (Off);
