pragma Extensions_Allowed (On);
limited with Java.Text.DateFormat;
limited with Java.Util.Locale;
with Java.Lang.Object;
with Java.Util.Spi.LocaleServiceProvider;

package Java.Text.Spi.DateFormatProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Util.Spi.LocaleServiceProvider.Typ
      with null record;

   --  protected
   function New_DateFormatProvider (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTimeInstance (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Text.DateFormat.Typ'Class is abstract;

   function GetDateInstance (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Text.DateFormat.Typ'Class is abstract;

   function GetDateTimeInstance (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                 return access Java.Text.DateFormat.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DateFormatProvider);
   pragma Export (Java, GetTimeInstance, "getTimeInstance");
   pragma Export (Java, GetDateInstance, "getDateInstance");
   pragma Export (Java, GetDateTimeInstance, "getDateTimeInstance");

end Java.Text.Spi.DateFormatProvider;
pragma Import (Java, Java.Text.Spi.DateFormatProvider, "java.text.spi.DateFormatProvider");
pragma Extensions_Allowed (Off);
