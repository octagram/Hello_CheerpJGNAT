pragma Extensions_Allowed (On);
limited with Java.Text.DecimalFormatSymbols;
limited with Java.Util.Locale;
with Java.Lang.Object;
with Java.Util.Spi.LocaleServiceProvider;

package Java.Text.Spi.DecimalFormatSymbolsProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Util.Spi.LocaleServiceProvider.Typ
      with null record;

   --  protected
   function New_DecimalFormatSymbolsProvider (This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (This : access Typ;
                         P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Text.DecimalFormatSymbols.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DecimalFormatSymbolsProvider);
   pragma Export (Java, GetInstance, "getInstance");

end Java.Text.Spi.DecimalFormatSymbolsProvider;
pragma Import (Java, Java.Text.Spi.DecimalFormatSymbolsProvider, "java.text.spi.DecimalFormatSymbolsProvider");
pragma Extensions_Allowed (Off);
