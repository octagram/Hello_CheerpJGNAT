pragma Extensions_Allowed (On);
limited with Java.Text.NumberFormat;
limited with Java.Util.Locale;
with Java.Lang.Object;
with Java.Util.Spi.LocaleServiceProvider;

package Java.Text.Spi.NumberFormatProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Util.Spi.LocaleServiceProvider.Typ
      with null record;

   --  protected
   function New_NumberFormatProvider (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCurrencyInstance (This : access Typ;
                                 P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                 return access Java.Text.NumberFormat.Typ'Class is abstract;

   function GetIntegerInstance (This : access Typ;
                                P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                return access Java.Text.NumberFormat.Typ'Class is abstract;

   function GetNumberInstance (This : access Typ;
                               P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                               return access Java.Text.NumberFormat.Typ'Class is abstract;

   function GetPercentInstance (This : access Typ;
                                P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                return access Java.Text.NumberFormat.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NumberFormatProvider);
   pragma Export (Java, GetCurrencyInstance, "getCurrencyInstance");
   pragma Export (Java, GetIntegerInstance, "getIntegerInstance");
   pragma Export (Java, GetNumberInstance, "getNumberInstance");
   pragma Export (Java, GetPercentInstance, "getPercentInstance");

end Java.Text.Spi.NumberFormatProvider;
pragma Import (Java, Java.Text.Spi.NumberFormatProvider, "java.text.spi.NumberFormatProvider");
pragma Extensions_Allowed (Off);
