pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Text.CharacterIterator;

package Java.Text.StringCharacterIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CharacterIterator_I : Java.Text.CharacterIterator.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringCharacterIterator (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_StringCharacterIterator (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Int : Java.Int; 
                                         This : Ref := null)
                                         return Ref;

   function New_StringCharacterIterator (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function First (This : access Typ)
                   return Java.Char;

   function Last (This : access Typ)
                  return Java.Char;

   function SetIndex (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Char;

   function Current (This : access Typ)
                     return Java.Char;

   function Next (This : access Typ)
                  return Java.Char;

   function Previous (This : access Typ)
                      return Java.Char;

   function GetBeginIndex (This : access Typ)
                           return Java.Int;

   function GetEndIndex (This : access Typ)
                         return Java.Int;

   function GetIndex (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringCharacterIterator);
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, First, "first");
   pragma Import (Java, Last, "last");
   pragma Import (Java, SetIndex, "setIndex");
   pragma Import (Java, Current, "current");
   pragma Import (Java, Next, "next");
   pragma Import (Java, Previous, "previous");
   pragma Import (Java, GetBeginIndex, "getBeginIndex");
   pragma Import (Java, GetEndIndex, "getEndIndex");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Clone, "clone");

end Java.Text.StringCharacterIterator;
pragma Import (Java, Java.Text.StringCharacterIterator, "java.text.StringCharacterIterator");
pragma Extensions_Allowed (Off);
