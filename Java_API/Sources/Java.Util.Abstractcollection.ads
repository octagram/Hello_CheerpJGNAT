pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
with Java.Lang.Object;
with Java.Util.Collection;

package Java.Util.AbstractCollection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Collection_I : Java.Util.Collection.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_AbstractCollection (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class is abstract;

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ContainsAll (This : access Typ;
                         P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                         return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function RemoveAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   function RetainAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   procedure Clear (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractCollection);
   pragma Export (Java, Iterator, "iterator");
   pragma Export (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ContainsAll, "containsAll");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, RetainAll, "retainAll");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, ToString, "toString");

end Java.Util.AbstractCollection;
pragma Import (Java, Java.Util.AbstractCollection, "java.util.AbstractCollection");
pragma Extensions_Allowed (Off);
