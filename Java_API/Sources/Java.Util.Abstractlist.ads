pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
limited with Java.Util.ListIterator;
with Java.Lang.Object;
with Java.Util.AbstractCollection;
with Java.Util.Collection;
with Java.Util.List;

package Java.Util.AbstractList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Collection_I : Java.Util.Collection.Ref;
            List_I : Java.Util.List.Ref)
    is abstract new Java.Util.AbstractCollection.Typ(Collection_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ModCount : Java.Int;
      pragma Import (Java, ModCount, "modCount");

   end record;

   --  protected
   function New_AbstractList (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Int;

   procedure Clear (This : access Typ);

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function ListIterator (This : access Typ)
                          return access Java.Util.ListIterator.Typ'Class;

   function ListIterator (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Util.ListIterator.Typ'Class;

   function SubList (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Java.Util.List.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   --  protected
   procedure RemoveRange (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractList);
   pragma Import (Java, Add, "add");
   pragma Export (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, ListIterator, "listIterator");
   pragma Import (Java, SubList, "subList");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, RemoveRange, "removeRange");

end Java.Util.AbstractList;
pragma Import (Java, Java.Util.AbstractList, "java.util.AbstractList");
pragma Extensions_Allowed (Off);
