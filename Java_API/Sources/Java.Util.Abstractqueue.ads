pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.AbstractCollection;
with Java.Util.Collection;
with Java.Util.Queue;

package Java.Util.AbstractQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Collection_I : Java.Util.Collection.Ref;
            Queue_I : Java.Util.Queue.Ref)
    is abstract new Java.Util.AbstractCollection.Typ(Collection_I)
      with null record;

   --  protected
   function New_AbstractQueue (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ)
                    return access Java.Lang.Object.Typ'Class;

   function Element (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractQueue);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Element, "element");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, AddAll, "addAll");

end Java.Util.AbstractQueue;
pragma Import (Java, Java.Util.AbstractQueue, "java.util.AbstractQueue");
pragma Extensions_Allowed (Off);
