pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
limited with Java.Util.ListIterator;
with Java.Lang.Object;
with Java.Util.AbstractList;
with Java.Util.Collection;
with Java.Util.List;

package Java.Util.AbstractSequentialList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Collection_I : Java.Util.Collection.Ref;
            List_I : Java.Util.List.Ref)
    is abstract new Java.Util.AbstractList.Typ(Collection_I,
                                               List_I)
      with null record;

   --  protected
   function New_AbstractSequentialList (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function ListIterator (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Util.ListIterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractSequentialList);
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, Iterator, "iterator");
   pragma Export (Java, ListIterator, "listIterator");

end Java.Util.AbstractSequentialList;
pragma Import (Java, Java.Util.AbstractSequentialList, "java.util.AbstractSequentialList");
pragma Extensions_Allowed (Off);
