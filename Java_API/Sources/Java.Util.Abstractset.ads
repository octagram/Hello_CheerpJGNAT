pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.AbstractCollection;
with Java.Util.Collection;
with Java.Util.Set;

package Java.Util.AbstractSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Collection_I : Java.Util.Collection.Ref;
            Set_I : Java.Util.Set.Ref)
    is abstract new Java.Util.AbstractCollection.Typ(Collection_I)
      with null record;

   --  protected
   function New_AbstractSet (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function RemoveAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractSet);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, RemoveAll, "removeAll");

end Java.Util.AbstractSet;
pragma Import (Java, Java.Util.AbstractSet, "java.util.AbstractSet");
pragma Extensions_Allowed (Off);
