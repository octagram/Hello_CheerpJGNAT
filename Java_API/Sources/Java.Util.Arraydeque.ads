pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractCollection;
with Java.Util.Collection;
with Java.Util.Deque;

package Java.Util.ArrayDeque is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Deque_I : Java.Util.Deque.Ref)
    is new Java.Util.AbstractCollection.Typ(Collection_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ArrayDeque (This : Ref := null)
                            return Ref;

   function New_ArrayDeque (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_ArrayDeque (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddFirst (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddLast (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function OfferFirst (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean;

   function OfferLast (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   function RemoveFirst (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function RemoveLast (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   function PollFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function PollLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function GetFirst (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function GetLast (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function PeekFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function PeekLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function RemoveFirstOccurrence (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return Java.Boolean;

   function RemoveLastOccurrence (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return Java.Boolean;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   function Remove (This : access Typ)
                    return access Java.Lang.Object.Typ'Class;

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Element (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   procedure Push (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Pop (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function DescendingIterator (This : access Typ)
                                return access Java.Util.Iterator.Typ'Class;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   procedure Clear (This : access Typ);

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function Clone (This : access Typ)
                   return access Java.Util.ArrayDeque.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ArrayDeque);
   pragma Import (Java, AddFirst, "addFirst");
   pragma Import (Java, AddLast, "addLast");
   pragma Import (Java, OfferFirst, "offerFirst");
   pragma Import (Java, OfferLast, "offerLast");
   pragma Import (Java, RemoveFirst, "removeFirst");
   pragma Import (Java, RemoveLast, "removeLast");
   pragma Import (Java, PollFirst, "pollFirst");
   pragma Import (Java, PollLast, "pollLast");
   pragma Import (Java, GetFirst, "getFirst");
   pragma Import (Java, GetLast, "getLast");
   pragma Import (Java, PeekFirst, "peekFirst");
   pragma Import (Java, PeekLast, "peekLast");
   pragma Import (Java, RemoveFirstOccurrence, "removeFirstOccurrence");
   pragma Import (Java, RemoveLastOccurrence, "removeLastOccurrence");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Offer, "offer");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, Element, "element");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, Push, "push");
   pragma Import (Java, Pop, "pop");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, DescendingIterator, "descendingIterator");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Clone, "clone");

end Java.Util.ArrayDeque;
pragma Import (Java, Java.Util.ArrayDeque, "java.util.ArrayDeque");
pragma Extensions_Allowed (Off);
