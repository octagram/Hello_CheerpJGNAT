pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractList;
with Java.Util.Collection;
with Java.Util.List;
with Java.Util.RandomAccess;

package Java.Util.ArrayList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            List_I : Java.Util.List.Ref;
            RandomAccess_I : Java.Util.RandomAccess.Ref)
    is new Java.Util.AbstractList.Typ(Collection_I,
                                      List_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ArrayList (P1_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_ArrayList (This : Ref := null)
                           return Ref;

   function New_ArrayList (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure TrimToSize (This : access Typ);

   procedure EnsureCapacity (This : access Typ;
                             P1_Int : Java.Int);

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   procedure Clear (This : access Typ);

   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   --  protected
   procedure RemoveRange (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ArrayList);
   pragma Import (Java, TrimToSize, "trimToSize");
   pragma Import (Java, EnsureCapacity, "ensureCapacity");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, RemoveRange, "removeRange");

end Java.Util.ArrayList;
pragma Import (Java, Java.Util.ArrayList, "java.util.ArrayList");
pragma Extensions_Allowed (Off);
