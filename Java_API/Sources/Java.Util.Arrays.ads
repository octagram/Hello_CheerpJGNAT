pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Comparator;
limited with Java.Util.List;
with Java.Lang.Object;

package Java.Util.Arrays is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Sort (P1_Long_Arr : Java.Long_Arr);

   procedure Sort (P1_Long_Arr : Java.Long_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Int_Arr : Java.Int_Arr);

   procedure Sort (P1_Int_Arr : Java.Int_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Short_Arr : Java.Short_Arr);

   procedure Sort (P1_Short_Arr : Java.Short_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Char_Arr : Java.Char_Arr);

   procedure Sort (P1_Char_Arr : Java.Char_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Byte_Arr : Java.Byte_Arr);

   procedure Sort (P1_Byte_Arr : Java.Byte_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Double_Arr : Java.Double_Arr);

   procedure Sort (P1_Double_Arr : Java.Double_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Float_Arr : Java.Float_Arr);

   procedure Sort (P1_Float_Arr : Java.Float_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure Sort (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Sort (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                   P2_Comparator : access Standard.Java.Util.Comparator.Typ'Class);

   procedure Sort (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Comparator : access Standard.Java.Util.Comparator.Typ'Class);

   function BinarySearch (P1_Long_Arr : Java.Long_Arr;
                          P2_Long : Java.Long)
                          return Java.Int;

   function BinarySearch (P1_Long_Arr : Java.Long_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Long : Java.Long)
                          return Java.Int;

   function BinarySearch (P1_Int_Arr : Java.Int_Arr;
                          P2_Int : Java.Int)
                          return Java.Int;

   function BinarySearch (P1_Int_Arr : Java.Int_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int)
                          return Java.Int;

   function BinarySearch (P1_Short_Arr : Java.Short_Arr;
                          P2_Short : Java.Short)
                          return Java.Int;

   function BinarySearch (P1_Short_Arr : Java.Short_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Short : Java.Short)
                          return Java.Int;

   function BinarySearch (P1_Char_Arr : Java.Char_Arr;
                          P2_Char : Java.Char)
                          return Java.Int;

   function BinarySearch (P1_Char_Arr : Java.Char_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Char : Java.Char)
                          return Java.Int;

   function BinarySearch (P1_Byte_Arr : Java.Byte_Arr;
                          P2_Byte : Java.Byte)
                          return Java.Int;

   function BinarySearch (P1_Byte_Arr : Java.Byte_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Byte : Java.Byte)
                          return Java.Int;

   function BinarySearch (P1_Double_Arr : Java.Double_Arr;
                          P2_Double : Java.Double)
                          return Java.Int;

   function BinarySearch (P1_Double_Arr : Java.Double_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Double : Java.Double)
                          return Java.Int;

   function BinarySearch (P1_Float_Arr : Java.Float_Arr;
                          P2_Float : Java.Float)
                          return Java.Int;

   function BinarySearch (P1_Float_Arr : Java.Float_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Float : Java.Float)
                          return Java.Int;

   function BinarySearch (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return Java.Int;

   function BinarySearch (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return Java.Int;

   function BinarySearch (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P3_Comparator : access Standard.Java.Util.Comparator.Typ'Class)
                          return Java.Int;

   function BinarySearch (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P5_Comparator : access Standard.Java.Util.Comparator.Typ'Class)
                          return Java.Int;

   function Equals (P1_Long_Arr : Java.Long_Arr;
                    P2_Long_Arr : Java.Long_Arr)
                    return Java.Boolean;

   function Equals (P1_Int_Arr : Java.Int_Arr;
                    P2_Int_Arr : Java.Int_Arr)
                    return Java.Boolean;

   function Equals (P1_Short_Arr : Java.Short_Arr;
                    P2_Short_Arr : Java.Short_Arr)
                    return Java.Boolean;

   function Equals (P1_Char_Arr : Java.Char_Arr;
                    P2_Char_Arr : Java.Char_Arr)
                    return Java.Boolean;

   function Equals (P1_Byte_Arr : Java.Byte_Arr;
                    P2_Byte_Arr : Java.Byte_Arr)
                    return Java.Boolean;

   function Equals (P1_Boolean_Arr : Java.Boolean_Arr;
                    P2_Boolean_Arr : Java.Boolean_Arr)
                    return Java.Boolean;

   function Equals (P1_Double_Arr : Java.Double_Arr;
                    P2_Double_Arr : Java.Double_Arr)
                    return Java.Boolean;

   function Equals (P1_Float_Arr : Java.Float_Arr;
                    P2_Float_Arr : Java.Float_Arr)
                    return Java.Boolean;

   function Equals (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return Java.Boolean;

   procedure Fill (P1_Long_Arr : Java.Long_Arr;
                   P2_Long : Java.Long);

   procedure Fill (P1_Long_Arr : Java.Long_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Long : Java.Long);

   procedure Fill (P1_Int_Arr : Java.Int_Arr;
                   P2_Int : Java.Int);

   procedure Fill (P1_Int_Arr : Java.Int_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Int : Java.Int);

   procedure Fill (P1_Short_Arr : Java.Short_Arr;
                   P2_Short : Java.Short);

   procedure Fill (P1_Short_Arr : Java.Short_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Short : Java.Short);

   procedure Fill (P1_Char_Arr : Java.Char_Arr;
                   P2_Char : Java.Char);

   procedure Fill (P1_Char_Arr : Java.Char_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Char : Java.Char);

   procedure Fill (P1_Byte_Arr : Java.Byte_Arr;
                   P2_Byte : Java.Byte);

   procedure Fill (P1_Byte_Arr : Java.Byte_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Byte : Java.Byte);

   procedure Fill (P1_Boolean_Arr : Java.Boolean_Arr;
                   P2_Boolean : Java.Boolean);

   procedure Fill (P1_Boolean_Arr : Java.Boolean_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Boolean : Java.Boolean);

   procedure Fill (P1_Double_Arr : Java.Double_Arr;
                   P2_Double : Java.Double);

   procedure Fill (P1_Double_Arr : Java.Double_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Double : Java.Double);

   procedure Fill (P1_Float_Arr : Java.Float_Arr;
                   P2_Float : Java.Float);

   procedure Fill (P1_Float_Arr : Java.Float_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Float : Java.Float);

   procedure Fill (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Fill (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int;
                   P4_Object : access Standard.Java.Lang.Object.Typ'Class);

   function CopyOf (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P2_Int : Java.Int)
                    return Standard.Java.Lang.Object.Ref;

   function CopyOf (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P2_Int : Java.Int;
                    P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return Standard.Java.Lang.Object.Ref;

   function CopyOf (P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int)
                    return Java.Byte_Arr;

   function CopyOf (P1_Short_Arr : Java.Short_Arr;
                    P2_Int : Java.Int)
                    return Java.Short_Arr;

   function CopyOf (P1_Int_Arr : Java.Int_Arr;
                    P2_Int : Java.Int)
                    return Java.Int_Arr;

   function CopyOf (P1_Long_Arr : Java.Long_Arr;
                    P2_Int : Java.Int)
                    return Java.Long_Arr;

   function CopyOf (P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int)
                    return Java.Char_Arr;

   function CopyOf (P1_Float_Arr : Java.Float_Arr;
                    P2_Int : Java.Int)
                    return Java.Float_Arr;

   function CopyOf (P1_Double_Arr : Java.Double_Arr;
                    P2_Int : Java.Int)
                    return Java.Double_Arr;

   function CopyOf (P1_Boolean_Arr : Java.Boolean_Arr;
                    P2_Int : Java.Int)
                    return Java.Boolean_Arr;

   function CopyOfRange (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Standard.Java.Lang.Object.Ref;

   function CopyOfRange (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return Standard.Java.Lang.Object.Ref;

   function CopyOfRange (P1_Byte_Arr : Java.Byte_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Byte_Arr;

   function CopyOfRange (P1_Short_Arr : Java.Short_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Short_Arr;

   function CopyOfRange (P1_Int_Arr : Java.Int_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int_Arr;

   function CopyOfRange (P1_Long_Arr : Java.Long_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Long_Arr;

   function CopyOfRange (P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Char_Arr;

   function CopyOfRange (P1_Float_Arr : Java.Float_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Float_Arr;

   function CopyOfRange (P1_Double_Arr : Java.Double_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Double_Arr;

   function CopyOfRange (P1_Boolean_Arr : Java.Boolean_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Boolean_Arr;

   function AsList (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Util.List.Typ'Class;

   function HashCode (P1_Long_Arr : Java.Long_Arr)
                      return Java.Int;

   function HashCode (P1_Int_Arr : Java.Int_Arr)
                      return Java.Int;

   function HashCode (P1_Short_Arr : Java.Short_Arr)
                      return Java.Int;

   function HashCode (P1_Char_Arr : Java.Char_Arr)
                      return Java.Int;

   function HashCode (P1_Byte_Arr : Java.Byte_Arr)
                      return Java.Int;

   function HashCode (P1_Boolean_Arr : Java.Boolean_Arr)
                      return Java.Int;

   function HashCode (P1_Float_Arr : Java.Float_Arr)
                      return Java.Int;

   function HashCode (P1_Double_Arr : Java.Double_Arr)
                      return Java.Int;

   function HashCode (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                      return Java.Int;

   function DeepHashCode (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                          return Java.Int;

   function DeepEquals (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                        P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                        return Java.Boolean;

   function ToString (P1_Long_Arr : Java.Long_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Int_Arr : Java.Int_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Short_Arr : Java.Short_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Char_Arr : Java.Char_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Byte_Arr : Java.Byte_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Boolean_Arr : Java.Boolean_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Float_Arr : Java.Float_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Double_Arr : Java.Double_Arr)
                      return access Java.Lang.String.Typ'Class;

   function ToString (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                      return access Java.Lang.String.Typ'Class;

   function DeepToString (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                          return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Sort, "sort");
   pragma Import (Java, BinarySearch, "binarySearch");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Fill, "fill");
   pragma Import (Java, CopyOf, "copyOf");
   pragma Import (Java, CopyOfRange, "copyOfRange");
   pragma Import (Java, AsList, "asList");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, DeepHashCode, "deepHashCode");
   pragma Import (Java, DeepEquals, "deepEquals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, DeepToString, "deepToString");

end Java.Util.Arrays;
pragma Import (Java, Java.Util.Arrays, "java.util.Arrays");
pragma Extensions_Allowed (Off);
