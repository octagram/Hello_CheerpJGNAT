pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Util.BitSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BitSet (This : Ref := null)
                        return Ref;

   function New_BitSet (P1_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Flip (This : access Typ;
                   P1_Int : Java.Int);

   procedure Flip (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Int : Java.Int);

   procedure Set (This : access Typ;
                  P1_Int : Java.Int);

   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Boolean : Java.Boolean);

   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int);

   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int;
                  P3_Boolean : Java.Boolean);

   procedure Clear (This : access Typ;
                    P1_Int : Java.Int);

   procedure Clear (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int);

   procedure Clear (This : access Typ);

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return Java.Boolean;

   function Get (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Int : Java.Int)
                 return access Java.Util.BitSet.Typ'Class;

   function NextSetBit (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int;

   function NextClearBit (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;

   function Length (This : access Typ)
                    return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Intersects (This : access Typ;
                        P1_BitSet : access Standard.Java.Util.BitSet.Typ'Class)
                        return Java.Boolean;

   function Cardinality (This : access Typ)
                         return Java.Int;

   procedure and_K (This : access Typ;
                    P1_BitSet : access Standard.Java.Util.BitSet.Typ'Class);

   procedure or_K (This : access Typ;
                   P1_BitSet : access Standard.Java.Util.BitSet.Typ'Class);

   procedure xor_K (This : access Typ;
                    P1_BitSet : access Standard.Java.Util.BitSet.Typ'Class);

   procedure AndNot (This : access Typ;
                     P1_BitSet : access Standard.Java.Util.BitSet.Typ'Class);

   function HashCode (This : access Typ)
                      return Java.Int;

   function Size (This : access Typ)
                  return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BitSet);
   pragma Import (Java, Flip, "flip");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Get, "get");
   pragma Import (Java, NextSetBit, "nextSetBit");
   pragma Import (Java, NextClearBit, "nextClearBit");
   pragma Import (Java, Length, "length");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Intersects, "intersects");
   pragma Import (Java, Cardinality, "cardinality");
   pragma Import (Java, and_K, "and");
   pragma Import (Java, or_K, "or");
   pragma Import (Java, xor_K, "xor");
   pragma Import (Java, AndNot, "andNot");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");

end Java.Util.BitSet;
pragma Import (Java, Java.Util.BitSet, "java.util.BitSet");
pragma Extensions_Allowed (Off);
