pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Date;
limited with Java.Util.Locale;
limited with Java.Util.Map;
limited with Java.Util.TimeZone;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Util.Calendar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Fields : Java.Int_Arr;
      pragma Import (Java, Fields, "fields");

      --  protected
      IsSet : Java.Boolean_Arr;
      pragma Import (Java, IsSet, "isSet");

      --  protected
      Time : Java.Long;
      pragma Import (Java, Time, "time");

      --  protected
      IsTimeSet : Java.Boolean;
      pragma Import (Java, IsTimeSet, "isTimeSet");

      --  protected
      AreFieldsSet : Java.Boolean;
      pragma Import (Java, AreFieldsSet, "areFieldsSet");

   end record;

   --  protected
   function New_Calendar (This : Ref := null)
                          return Ref;

   --  protected
   function New_Calendar (P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class;
                          P2_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance return access Java.Util.Calendar.Typ'Class;

   function GetInstance (P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class)
                         return access Java.Util.Calendar.Typ'Class;

   function GetInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Util.Calendar.Typ'Class;

   function GetInstance (P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class;
                         P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Util.Calendar.Typ'Class;

   --  synchronized
   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ComputeTime (This : access Typ) is abstract;

   --  protected
   procedure ComputeFields (This : access Typ) is abstract;

   --  final
   function GetTime (This : access Typ)
                     return access Java.Util.Date.Typ'Class;

   --  final
   procedure SetTime (This : access Typ;
                      P1_Date : access Standard.Java.Util.Date.Typ'Class);

   function GetTimeInMillis (This : access Typ)
                             return Java.Long;

   procedure SetTimeInMillis (This : access Typ;
                              P1_Long : Java.Long);

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return Java.Int;

   --  final  protected
   function InternalGet (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int);

   --  final
   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int);

   --  final
   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int;
                  P4_Int : Java.Int;
                  P5_Int : Java.Int);

   --  final
   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int;
                  P4_Int : Java.Int;
                  P5_Int : Java.Int;
                  P6_Int : Java.Int);

   --  final
   procedure Clear (This : access Typ);

   --  final
   procedure Clear (This : access Typ;
                    P1_Int : Java.Int);

   --  final
   function IsSet (This : access Typ;
                   P1_Int : Java.Int)
                   return Java.Boolean;

   function GetDisplayName (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetDisplayNames (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Util.Map.Typ'Class;

   --  protected
   procedure Complete (This : access Typ);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Before (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function After (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                       return Java.Int;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int) is abstract;

   procedure Roll (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Boolean : Java.Boolean) is abstract;

   procedure Roll (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Int : Java.Int);

   procedure SetTimeZone (This : access Typ;
                          P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class);

   function GetTimeZone (This : access Typ)
                         return access Java.Util.TimeZone.Typ'Class;

   procedure SetLenient (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsLenient (This : access Typ)
                       return Java.Boolean;

   procedure SetFirstDayOfWeek (This : access Typ;
                                P1_Int : Java.Int);

   function GetFirstDayOfWeek (This : access Typ)
                               return Java.Int;

   procedure SetMinimalDaysInFirstWeek (This : access Typ;
                                        P1_Int : Java.Int);

   function GetMinimalDaysInFirstWeek (This : access Typ)
                                       return Java.Int;

   function GetMinimum (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int is abstract;

   function GetMaximum (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int is abstract;

   function GetGreatestMinimum (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Int is abstract;

   function GetLeastMaximum (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int is abstract;

   function GetActualMinimum (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   function GetActualMaximum (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ERA : constant Java.Int;

   --  final
   YEAR : constant Java.Int;

   --  final
   MONTH : constant Java.Int;

   --  final
   WEEK_OF_YEAR : constant Java.Int;

   --  final
   WEEK_OF_MONTH : constant Java.Int;

   --  final
   DATE : constant Java.Int;

   --  final
   DAY_OF_MONTH : constant Java.Int;

   --  final
   DAY_OF_YEAR : constant Java.Int;

   --  final
   DAY_OF_WEEK : constant Java.Int;

   --  final
   DAY_OF_WEEK_IN_MONTH : constant Java.Int;

   --  final
   AM_PM : constant Java.Int;

   --  final
   HOUR : constant Java.Int;

   --  final
   HOUR_OF_DAY : constant Java.Int;

   --  final
   MINUTE : constant Java.Int;

   --  final
   SECOND : constant Java.Int;

   --  final
   MILLISECOND : constant Java.Int;

   --  final
   ZONE_OFFSET : constant Java.Int;

   --  final
   DST_OFFSET : constant Java.Int;

   --  final
   FIELD_COUNT : constant Java.Int;

   --  final
   SUNDAY : constant Java.Int;

   --  final
   MONDAY : constant Java.Int;

   --  final
   TUESDAY : constant Java.Int;

   --  final
   WEDNESDAY : constant Java.Int;

   --  final
   THURSDAY : constant Java.Int;

   --  final
   FRIDAY : constant Java.Int;

   --  final
   SATURDAY : constant Java.Int;

   --  final
   JANUARY : constant Java.Int;

   --  final
   FEBRUARY : constant Java.Int;

   --  final
   MARCH : constant Java.Int;

   --  final
   APRIL : constant Java.Int;

   --  final
   MAY : constant Java.Int;

   --  final
   JUNE : constant Java.Int;

   --  final
   JULY : constant Java.Int;

   --  final
   AUGUST : constant Java.Int;

   --  final
   SEPTEMBER : constant Java.Int;

   --  final
   OCTOBER : constant Java.Int;

   --  final
   NOVEMBER : constant Java.Int;

   --  final
   DECEMBER : constant Java.Int;

   --  final
   UNDECIMBER : constant Java.Int;

   --  final
   AM : constant Java.Int;

   --  final
   PM : constant Java.Int;

   --  final
   ALL_STYLES : constant Java.Int;

   --  final
   SHORT : constant Java.Int;

   --  final
   LONG : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Calendar);
   pragma Export (Java, GetInstance, "getInstance");
   pragma Export (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Export (Java, ComputeTime, "computeTime");
   pragma Export (Java, ComputeFields, "computeFields");
   pragma Export (Java, GetTime, "getTime");
   pragma Export (Java, SetTime, "setTime");
   pragma Export (Java, GetTimeInMillis, "getTimeInMillis");
   pragma Export (Java, SetTimeInMillis, "setTimeInMillis");
   pragma Export (Java, Get, "get");
   pragma Export (Java, InternalGet, "internalGet");
   pragma Export (Java, Set, "set");
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, IsSet, "isSet");
   pragma Export (Java, GetDisplayName, "getDisplayName");
   pragma Export (Java, GetDisplayNames, "getDisplayNames");
   pragma Export (Java, Complete, "complete");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Before, "before");
   pragma Export (Java, After, "after");
   pragma Export (Java, CompareTo, "compareTo");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Roll, "roll");
   pragma Export (Java, SetTimeZone, "setTimeZone");
   pragma Export (Java, GetTimeZone, "getTimeZone");
   pragma Export (Java, SetLenient, "setLenient");
   pragma Export (Java, IsLenient, "isLenient");
   pragma Export (Java, SetFirstDayOfWeek, "setFirstDayOfWeek");
   pragma Export (Java, GetFirstDayOfWeek, "getFirstDayOfWeek");
   pragma Export (Java, SetMinimalDaysInFirstWeek, "setMinimalDaysInFirstWeek");
   pragma Export (Java, GetMinimalDaysInFirstWeek, "getMinimalDaysInFirstWeek");
   pragma Export (Java, GetMinimum, "getMinimum");
   pragma Export (Java, GetMaximum, "getMaximum");
   pragma Export (Java, GetGreatestMinimum, "getGreatestMinimum");
   pragma Export (Java, GetLeastMaximum, "getLeastMaximum");
   pragma Export (Java, GetActualMinimum, "getActualMinimum");
   pragma Export (Java, GetActualMaximum, "getActualMaximum");
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, ToString, "toString");
   pragma Import (Java, ERA, "ERA");
   pragma Import (Java, YEAR, "YEAR");
   pragma Import (Java, MONTH, "MONTH");
   pragma Import (Java, WEEK_OF_YEAR, "WEEK_OF_YEAR");
   pragma Import (Java, WEEK_OF_MONTH, "WEEK_OF_MONTH");
   pragma Import (Java, DATE, "DATE");
   pragma Import (Java, DAY_OF_MONTH, "DAY_OF_MONTH");
   pragma Import (Java, DAY_OF_YEAR, "DAY_OF_YEAR");
   pragma Import (Java, DAY_OF_WEEK, "DAY_OF_WEEK");
   pragma Import (Java, DAY_OF_WEEK_IN_MONTH, "DAY_OF_WEEK_IN_MONTH");
   pragma Import (Java, AM_PM, "AM_PM");
   pragma Import (Java, HOUR, "HOUR");
   pragma Import (Java, HOUR_OF_DAY, "HOUR_OF_DAY");
   pragma Import (Java, MINUTE, "MINUTE");
   pragma Import (Java, SECOND, "SECOND");
   pragma Import (Java, MILLISECOND, "MILLISECOND");
   pragma Import (Java, ZONE_OFFSET, "ZONE_OFFSET");
   pragma Import (Java, DST_OFFSET, "DST_OFFSET");
   pragma Import (Java, FIELD_COUNT, "FIELD_COUNT");
   pragma Import (Java, SUNDAY, "SUNDAY");
   pragma Import (Java, MONDAY, "MONDAY");
   pragma Import (Java, TUESDAY, "TUESDAY");
   pragma Import (Java, WEDNESDAY, "WEDNESDAY");
   pragma Import (Java, THURSDAY, "THURSDAY");
   pragma Import (Java, FRIDAY, "FRIDAY");
   pragma Import (Java, SATURDAY, "SATURDAY");
   pragma Import (Java, JANUARY, "JANUARY");
   pragma Import (Java, FEBRUARY, "FEBRUARY");
   pragma Import (Java, MARCH, "MARCH");
   pragma Import (Java, APRIL, "APRIL");
   pragma Import (Java, MAY, "MAY");
   pragma Import (Java, JUNE, "JUNE");
   pragma Import (Java, JULY, "JULY");
   pragma Import (Java, AUGUST, "AUGUST");
   pragma Import (Java, SEPTEMBER, "SEPTEMBER");
   pragma Import (Java, OCTOBER, "OCTOBER");
   pragma Import (Java, NOVEMBER, "NOVEMBER");
   pragma Import (Java, DECEMBER, "DECEMBER");
   pragma Import (Java, UNDECIMBER, "UNDECIMBER");
   pragma Import (Java, AM, "AM");
   pragma Import (Java, PM, "PM");
   pragma Import (Java, ALL_STYLES, "ALL_STYLES");
   pragma Import (Java, SHORT, "SHORT");
   pragma Import (Java, LONG, "LONG");

end Java.Util.Calendar;
pragma Import (Java, Java.Util.Calendar, "java.util.Calendar");
pragma Extensions_Allowed (Off);
