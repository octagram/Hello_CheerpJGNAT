pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Util.ArrayList;
limited with Java.Util.Collection;
limited with Java.Util.Comparator;
limited with Java.Util.Deque;
limited with Java.Util.Enumeration;
limited with Java.Util.List;
limited with Java.Util.Map;
limited with Java.Util.Queue;
limited with Java.Util.Random;
limited with Java.Util.Set;
limited with Java.Util.SortedMap;
limited with Java.Util.SortedSet;
with Java.Lang.Object;

package Java.Util.Collections is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Sort (P1_List : access Standard.Java.Util.List.Typ'Class);

   procedure Sort (P1_List : access Standard.Java.Util.List.Typ'Class;
                   P2_Comparator : access Standard.Java.Util.Comparator.Typ'Class);

   function BinarySearch (P1_List : access Standard.Java.Util.List.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return Java.Int;

   function BinarySearch (P1_List : access Standard.Java.Util.List.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P3_Comparator : access Standard.Java.Util.Comparator.Typ'Class)
                          return Java.Int;

   procedure reverse_K (P1_List : access Standard.Java.Util.List.Typ'Class);

   procedure Shuffle (P1_List : access Standard.Java.Util.List.Typ'Class);

   procedure Shuffle (P1_List : access Standard.Java.Util.List.Typ'Class;
                      P2_Random : access Standard.Java.Util.Random.Typ'Class);

   procedure Swap (P1_List : access Standard.Java.Util.List.Typ'Class;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   procedure Fill (P1_List : access Standard.Java.Util.List.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Copy (P1_List : access Standard.Java.Util.List.Typ'Class;
                   P2_List : access Standard.Java.Util.List.Typ'Class);

   function Min (P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Min (P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                 P2_Comparator : access Standard.Java.Util.Comparator.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Max (P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Max (P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                 P2_Comparator : access Standard.Java.Util.Comparator.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Rotate (P1_List : access Standard.Java.Util.List.Typ'Class;
                     P2_Int : Java.Int);

   function ReplaceAll (P1_List : access Standard.Java.Util.List.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean;

   function IndexOfSubList (P1_List : access Standard.Java.Util.List.Typ'Class;
                            P2_List : access Standard.Java.Util.List.Typ'Class)
                            return Java.Int;

   function LastIndexOfSubList (P1_List : access Standard.Java.Util.List.Typ'Class;
                                P2_List : access Standard.Java.Util.List.Typ'Class)
                                return Java.Int;

   function UnmodifiableCollection (P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                                    return access Java.Util.Collection.Typ'Class;

   function UnmodifiableSet (P1_Set : access Standard.Java.Util.Set.Typ'Class)
                             return access Java.Util.Set.Typ'Class;

   function UnmodifiableSortedSet (P1_SortedSet : access Standard.Java.Util.SortedSet.Typ'Class)
                                   return access Java.Util.SortedSet.Typ'Class;

   function UnmodifiableList (P1_List : access Standard.Java.Util.List.Typ'Class)
                              return access Java.Util.List.Typ'Class;

   function UnmodifiableMap (P1_Map : access Standard.Java.Util.Map.Typ'Class)
                             return access Java.Util.Map.Typ'Class;

   function UnmodifiableSortedMap (P1_SortedMap : access Standard.Java.Util.SortedMap.Typ'Class)
                                   return access Java.Util.SortedMap.Typ'Class;

   function SynchronizedCollection (P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                                    return access Java.Util.Collection.Typ'Class;

   function SynchronizedSet (P1_Set : access Standard.Java.Util.Set.Typ'Class)
                             return access Java.Util.Set.Typ'Class;

   function SynchronizedSortedSet (P1_SortedSet : access Standard.Java.Util.SortedSet.Typ'Class)
                                   return access Java.Util.SortedSet.Typ'Class;

   function SynchronizedList (P1_List : access Standard.Java.Util.List.Typ'Class)
                              return access Java.Util.List.Typ'Class;

   function SynchronizedMap (P1_Map : access Standard.Java.Util.Map.Typ'Class)
                             return access Java.Util.Map.Typ'Class;

   function SynchronizedSortedMap (P1_SortedMap : access Standard.Java.Util.SortedMap.Typ'Class)
                                   return access Java.Util.SortedMap.Typ'Class;

   function CheckedCollection (P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                               P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                               return access Java.Util.Collection.Typ'Class;

   function CheckedSet (P1_Set : access Standard.Java.Util.Set.Typ'Class;
                        P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Java.Util.Set.Typ'Class;

   function CheckedSortedSet (P1_SortedSet : access Standard.Java.Util.SortedSet.Typ'Class;
                              P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                              return access Java.Util.SortedSet.Typ'Class;

   function CheckedList (P1_List : access Standard.Java.Util.List.Typ'Class;
                         P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return access Java.Util.List.Typ'Class;

   function CheckedMap (P1_Map : access Standard.Java.Util.Map.Typ'Class;
                        P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Java.Util.Map.Typ'Class;

   function CheckedSortedMap (P1_SortedMap : access Standard.Java.Util.SortedMap.Typ'Class;
                              P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                              P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                              return access Java.Util.SortedMap.Typ'Class;

   --  final
   function EmptySet return access Java.Util.Set.Typ'Class;

   --  final
   function EmptyList return access Java.Util.List.Typ'Class;

   --  final
   function EmptyMap return access Java.Util.Map.Typ'Class;

   function Singleton (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Util.Set.Typ'Class;

   function SingletonList (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Util.List.Typ'Class;

   function SingletonMap (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Util.Map.Typ'Class;

   function NCopies (P1_Int : Java.Int;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.List.Typ'Class;

   function ReverseOrder return access Java.Util.Comparator.Typ'Class;

   function ReverseOrder (P1_Comparator : access Standard.Java.Util.Comparator.Typ'Class)
                          return access Java.Util.Comparator.Typ'Class;

   function Enumeration (P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                         return access Java.Util.Enumeration.Typ'Class;

   function List (P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class)
                  return access Java.Util.ArrayList.Typ'Class;

   function Frequency (P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   function Disjoint (P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                      P2_Collection : access Standard.Java.Util.Collection.Typ'Class)
                      return Java.Boolean;

   function AddAll (P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return Java.Boolean;

   function NewSetFromMap (P1_Map : access Standard.Java.Util.Map.Typ'Class)
                           return access Java.Util.Set.Typ'Class;

   function AsLifoQueue (P1_Deque : access Standard.Java.Util.Deque.Typ'Class)
                         return access Java.Util.Queue.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   EMPTY_SET : access Java.Util.Set.Typ'Class;

   --  final
   EMPTY_LIST : access Java.Util.List.Typ'Class;

   --  final
   EMPTY_MAP : access Java.Util.Map.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Sort, "sort");
   pragma Import (Java, BinarySearch, "binarySearch");
   pragma Import (Java, reverse_K, "reverse");
   pragma Import (Java, Shuffle, "shuffle");
   pragma Import (Java, Swap, "swap");
   pragma Import (Java, Fill, "fill");
   pragma Import (Java, Copy, "copy");
   pragma Import (Java, Min, "min");
   pragma Import (Java, Max, "max");
   pragma Import (Java, Rotate, "rotate");
   pragma Import (Java, ReplaceAll, "replaceAll");
   pragma Import (Java, IndexOfSubList, "indexOfSubList");
   pragma Import (Java, LastIndexOfSubList, "lastIndexOfSubList");
   pragma Import (Java, UnmodifiableCollection, "unmodifiableCollection");
   pragma Import (Java, UnmodifiableSet, "unmodifiableSet");
   pragma Import (Java, UnmodifiableSortedSet, "unmodifiableSortedSet");
   pragma Import (Java, UnmodifiableList, "unmodifiableList");
   pragma Import (Java, UnmodifiableMap, "unmodifiableMap");
   pragma Import (Java, UnmodifiableSortedMap, "unmodifiableSortedMap");
   pragma Import (Java, SynchronizedCollection, "synchronizedCollection");
   pragma Import (Java, SynchronizedSet, "synchronizedSet");
   pragma Import (Java, SynchronizedSortedSet, "synchronizedSortedSet");
   pragma Import (Java, SynchronizedList, "synchronizedList");
   pragma Import (Java, SynchronizedMap, "synchronizedMap");
   pragma Import (Java, SynchronizedSortedMap, "synchronizedSortedMap");
   pragma Import (Java, CheckedCollection, "checkedCollection");
   pragma Import (Java, CheckedSet, "checkedSet");
   pragma Import (Java, CheckedSortedSet, "checkedSortedSet");
   pragma Import (Java, CheckedList, "checkedList");
   pragma Import (Java, CheckedMap, "checkedMap");
   pragma Import (Java, CheckedSortedMap, "checkedSortedMap");
   pragma Import (Java, EmptySet, "emptySet");
   pragma Import (Java, EmptyList, "emptyList");
   pragma Import (Java, EmptyMap, "emptyMap");
   pragma Import (Java, Singleton, "singleton");
   pragma Import (Java, SingletonList, "singletonList");
   pragma Import (Java, SingletonMap, "singletonMap");
   pragma Import (Java, NCopies, "nCopies");
   pragma Import (Java, ReverseOrder, "reverseOrder");
   pragma Import (Java, Enumeration, "enumeration");
   pragma Import (Java, List, "list");
   pragma Import (Java, Frequency, "frequency");
   pragma Import (Java, Disjoint, "disjoint");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, NewSetFromMap, "newSetFromMap");
   pragma Import (Java, AsLifoQueue, "asLifoQueue");
   pragma Import (Java, EMPTY_SET, "EMPTY_SET");
   pragma Import (Java, EMPTY_LIST, "EMPTY_LIST");
   pragma Import (Java, EMPTY_MAP, "EMPTY_MAP");

end Java.Util.Collections;
pragma Import (Java, Java.Util.Collections, "java.util.Collections");
pragma Extensions_Allowed (Off);
