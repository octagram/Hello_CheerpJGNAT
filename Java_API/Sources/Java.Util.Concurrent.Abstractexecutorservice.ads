pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.Future;
limited with Java.Util.Concurrent.RunnableFuture;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.List;
with Java.Lang.Object;
with Java.Util.Concurrent.ExecutorService;

package Java.Util.Concurrent.AbstractExecutorService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ExecutorService_I : Java.Util.Concurrent.ExecutorService.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AbstractExecutorService (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function NewTaskFor (This : access Typ;
                        P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Util.Concurrent.RunnableFuture.Typ'Class;

   --  protected
   function NewTaskFor (This : access Typ;
                        P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                        return access Java.Util.Concurrent.RunnableFuture.Typ'Class;

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   function Submit (This : access Typ;
                    P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   function InvokeAny (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Util.Concurrent.ExecutionException.Except

   function InvokeAny (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                       P2_Long : Java.Long;
                       P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except,
   --  Java.Util.Concurrent.ExecutionException.Except and
   --  Java.Util.Concurrent.TimeoutException.Except

   function InvokeAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return access Java.Util.List.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function InvokeAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                       P2_Long : Java.Long;
                       P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return access Java.Util.List.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractExecutorService);
   pragma Import (Java, NewTaskFor, "newTaskFor");
   pragma Import (Java, Submit, "submit");
   pragma Import (Java, InvokeAny, "invokeAny");
   pragma Import (Java, InvokeAll, "invokeAll");

end Java.Util.Concurrent.AbstractExecutorService;
pragma Import (Java, Java.Util.Concurrent.AbstractExecutorService, "java.util.concurrent.AbstractExecutorService");
pragma Extensions_Allowed (Off);
