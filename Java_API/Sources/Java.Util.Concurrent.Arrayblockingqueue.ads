pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.AbstractQueue;
with Java.Util.Collection;
with Java.Util.Concurrent.BlockingQueue;
with Java.Util.Queue;

package Java.Util.Concurrent.ArrayBlockingQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Queue_I : Java.Util.Queue.Ref;
            BlockingQueue_I : Java.Util.Concurrent.BlockingQueue.Ref)
    is new Java.Util.AbstractQueue.Typ(Collection_I,
                                       Queue_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ArrayBlockingQueue (P1_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   function New_ArrayBlockingQueue (P1_Int : Java.Int;
                                    P2_Boolean : Java.Boolean; 
                                    This : Ref := null)
                                    return Ref;

   function New_ArrayBlockingQueue (P1_Int : Java.Int;
                                    P2_Boolean : Java.Boolean;
                                    P3_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   procedure Put (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.InterruptedException.Except

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P2_Long : Java.Long;
                   P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Take (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ;
                  P1_Long : Java.Long;
                  P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   function RemainingCapacity (This : access Typ)
                               return Java.Int;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Clear (This : access Typ);

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                     return Java.Int;

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ArrayBlockingQueue);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Offer, "offer");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, Take, "take");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, Size, "size");
   pragma Import (Java, RemainingCapacity, "remainingCapacity");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, DrainTo, "drainTo");
   pragma Import (Java, Iterator, "iterator");

end Java.Util.Concurrent.ArrayBlockingQueue;
pragma Import (Java, Java.Util.Concurrent.ArrayBlockingQueue, "java.util.concurrent.ArrayBlockingQueue");
pragma Extensions_Allowed (Off);
