pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicBoolean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AtomicBoolean (P1_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   function New_AtomicBoolean (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Get (This : access Typ)
                 return Java.Boolean;

   --  final
   function CompareAndSet (This : access Typ;
                           P1_Boolean : Java.Boolean;
                           P2_Boolean : Java.Boolean)
                           return Java.Boolean;

   function WeakCompareAndSet (This : access Typ;
                               P1_Boolean : Java.Boolean;
                               P2_Boolean : Java.Boolean)
                               return Java.Boolean;

   --  final
   procedure Set (This : access Typ;
                  P1_Boolean : Java.Boolean);

   --  final
   procedure LazySet (This : access Typ;
                      P1_Boolean : Java.Boolean);

   --  final
   function GetAndSet (This : access Typ;
                       P1_Boolean : Java.Boolean)
                       return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AtomicBoolean);
   pragma Import (Java, Get, "get");
   pragma Import (Java, CompareAndSet, "compareAndSet");
   pragma Import (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Import (Java, Set, "set");
   pragma Import (Java, LazySet, "lazySet");
   pragma Import (Java, GetAndSet, "getAndSet");
   pragma Import (Java, ToString, "toString");

end Java.Util.Concurrent.Atomic.AtomicBoolean;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicBoolean, "java.util.concurrent.atomic.AtomicBoolean");
pragma Extensions_Allowed (Off);
