pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicIntegerFieldUpdater is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewUpdater (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Util.Concurrent.Atomic.AtomicIntegerFieldUpdater.Typ'Class;

   --  protected
   function New_AtomicIntegerFieldUpdater (This : Ref := null)
                                           return Ref;

   function CompareAndSet (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int)
                           return Java.Boolean is abstract;

   function WeakCompareAndSet (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int)
                               return Java.Boolean is abstract;

   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P2_Int : Java.Int) is abstract;

   procedure LazySet (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int) is abstract;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Int is abstract;

   function GetAndSet (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int)
                       return Java.Int;

   function GetAndIncrement (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Int;

   function GetAndDecrement (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Int;

   function GetAndAdd (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int)
                       return Java.Int;

   function IncrementAndGet (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Int;

   function DecrementAndGet (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Int;

   function AddAndGet (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Int : Java.Int)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NewUpdater, "newUpdater");
   pragma Java_Constructor (New_AtomicIntegerFieldUpdater);
   pragma Export (Java, CompareAndSet, "compareAndSet");
   pragma Export (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Export (Java, Set, "set");
   pragma Export (Java, LazySet, "lazySet");
   pragma Export (Java, Get, "get");
   pragma Export (Java, GetAndSet, "getAndSet");
   pragma Export (Java, GetAndIncrement, "getAndIncrement");
   pragma Export (Java, GetAndDecrement, "getAndDecrement");
   pragma Export (Java, GetAndAdd, "getAndAdd");
   pragma Export (Java, IncrementAndGet, "incrementAndGet");
   pragma Export (Java, DecrementAndGet, "decrementAndGet");
   pragma Export (Java, AddAndGet, "addAndGet");

end Java.Util.Concurrent.Atomic.AtomicIntegerFieldUpdater;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicIntegerFieldUpdater, "java.util.concurrent.atomic.AtomicIntegerFieldUpdater");
pragma Extensions_Allowed (Off);
