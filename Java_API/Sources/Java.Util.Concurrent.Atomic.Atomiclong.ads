pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Number;
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicLong is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Number.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AtomicLong (P1_Long : Java.Long; 
                            This : Ref := null)
                            return Ref;

   function New_AtomicLong (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Get (This : access Typ)
                 return Java.Long;

   --  final
   procedure Set (This : access Typ;
                  P1_Long : Java.Long);

   --  final
   procedure LazySet (This : access Typ;
                      P1_Long : Java.Long);

   --  final
   function GetAndSet (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Long;

   --  final
   function CompareAndSet (This : access Typ;
                           P1_Long : Java.Long;
                           P2_Long : Java.Long)
                           return Java.Boolean;

   --  final
   function WeakCompareAndSet (This : access Typ;
                               P1_Long : Java.Long;
                               P2_Long : Java.Long)
                               return Java.Boolean;

   --  final
   function GetAndIncrement (This : access Typ)
                             return Java.Long;

   --  final
   function GetAndDecrement (This : access Typ)
                             return Java.Long;

   --  final
   function GetAndAdd (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Long;

   --  final
   function IncrementAndGet (This : access Typ)
                             return Java.Long;

   --  final
   function DecrementAndGet (This : access Typ)
                             return Java.Long;

   --  final
   function AddAndGet (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Long;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IntValue (This : access Typ)
                      return Java.Int;

   function LongValue (This : access Typ)
                       return Java.Long;

   function FloatValue (This : access Typ)
                        return Java.Float;

   function DoubleValue (This : access Typ)
                         return Java.Double;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AtomicLong);
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, LazySet, "lazySet");
   pragma Import (Java, GetAndSet, "getAndSet");
   pragma Import (Java, CompareAndSet, "compareAndSet");
   pragma Import (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Import (Java, GetAndIncrement, "getAndIncrement");
   pragma Import (Java, GetAndDecrement, "getAndDecrement");
   pragma Import (Java, GetAndAdd, "getAndAdd");
   pragma Import (Java, IncrementAndGet, "incrementAndGet");
   pragma Import (Java, DecrementAndGet, "decrementAndGet");
   pragma Import (Java, AddAndGet, "addAndGet");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, LongValue, "longValue");
   pragma Import (Java, FloatValue, "floatValue");
   pragma Import (Java, DoubleValue, "doubleValue");

end Java.Util.Concurrent.Atomic.AtomicLong;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicLong, "java.util.concurrent.atomic.AtomicLong");
pragma Extensions_Allowed (Off);
