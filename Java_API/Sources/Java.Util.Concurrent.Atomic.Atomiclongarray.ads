pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicLongArray is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AtomicLongArray (P1_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_AtomicLongArray (P1_Long_Arr : Java.Long_Arr; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Length (This : access Typ)
                    return Java.Int;

   --  final
   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return Java.Long;

   --  final
   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Long : Java.Long);

   --  final
   procedure LazySet (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Long : Java.Long);

   --  final
   function GetAndSet (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Long : Java.Long)
                       return Java.Long;

   --  final
   function CompareAndSet (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Long : Java.Long;
                           P3_Long : Java.Long)
                           return Java.Boolean;

   --  final
   function WeakCompareAndSet (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Long : Java.Long;
                               P3_Long : Java.Long)
                               return Java.Boolean;

   --  final
   function GetAndIncrement (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Long;

   --  final
   function GetAndDecrement (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Long;

   --  final
   function GetAndAdd (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Long : Java.Long)
                       return Java.Long;

   --  final
   function IncrementAndGet (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Long;

   --  final
   function DecrementAndGet (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Long;

   function AddAndGet (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Long : Java.Long)
                       return Java.Long;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AtomicLongArray);
   pragma Import (Java, Length, "length");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, LazySet, "lazySet");
   pragma Import (Java, GetAndSet, "getAndSet");
   pragma Import (Java, CompareAndSet, "compareAndSet");
   pragma Import (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Import (Java, GetAndIncrement, "getAndIncrement");
   pragma Import (Java, GetAndDecrement, "getAndDecrement");
   pragma Import (Java, GetAndAdd, "getAndAdd");
   pragma Import (Java, IncrementAndGet, "incrementAndGet");
   pragma Import (Java, DecrementAndGet, "decrementAndGet");
   pragma Import (Java, AddAndGet, "addAndGet");
   pragma Import (Java, ToString, "toString");

end Java.Util.Concurrent.Atomic.AtomicLongArray;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicLongArray, "java.util.concurrent.atomic.AtomicLongArray");
pragma Extensions_Allowed (Off);
