pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicLongFieldUpdater is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewUpdater (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Util.Concurrent.Atomic.AtomicLongFieldUpdater.Typ'Class;

   --  protected
   function New_AtomicLongFieldUpdater (This : Ref := null)
                                        return Ref;

   function CompareAndSet (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Long : Java.Long;
                           P3_Long : Java.Long)
                           return Java.Boolean is abstract;

   function WeakCompareAndSet (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Long : Java.Long;
                               P3_Long : Java.Long)
                               return Java.Boolean is abstract;

   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P2_Long : Java.Long) is abstract;

   procedure LazySet (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Long : Java.Long) is abstract;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Long is abstract;

   function GetAndSet (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Long : Java.Long)
                       return Java.Long;

   function GetAndIncrement (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Long;

   function GetAndDecrement (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Long;

   function GetAndAdd (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Long : Java.Long)
                       return Java.Long;

   function IncrementAndGet (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Long;

   function DecrementAndGet (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Long;

   function AddAndGet (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Long : Java.Long)
                       return Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NewUpdater, "newUpdater");
   pragma Java_Constructor (New_AtomicLongFieldUpdater);
   pragma Export (Java, CompareAndSet, "compareAndSet");
   pragma Export (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Export (Java, Set, "set");
   pragma Export (Java, LazySet, "lazySet");
   pragma Export (Java, Get, "get");
   pragma Export (Java, GetAndSet, "getAndSet");
   pragma Export (Java, GetAndIncrement, "getAndIncrement");
   pragma Export (Java, GetAndDecrement, "getAndDecrement");
   pragma Export (Java, GetAndAdd, "getAndAdd");
   pragma Export (Java, IncrementAndGet, "incrementAndGet");
   pragma Export (Java, DecrementAndGet, "decrementAndGet");
   pragma Export (Java, AddAndGet, "addAndGet");

end Java.Util.Concurrent.Atomic.AtomicLongFieldUpdater;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicLongFieldUpdater, "java.util.concurrent.atomic.AtomicLongFieldUpdater");
pragma Extensions_Allowed (Off);
