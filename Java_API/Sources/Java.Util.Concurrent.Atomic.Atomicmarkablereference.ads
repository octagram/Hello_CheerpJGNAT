pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicMarkableReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AtomicMarkableReference (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                         P2_Boolean : Java.Boolean; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReference (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   function IsMarked (This : access Typ)
                      return Java.Boolean;

   function Get (This : access Typ;
                 P1_Boolean_Arr : Java.Boolean_Arr)
                 return access Java.Lang.Object.Typ'Class;

   function WeakCompareAndSet (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P3_Boolean : Java.Boolean;
                               P4_Boolean : Java.Boolean)
                               return Java.Boolean;

   function CompareAndSet (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Boolean : Java.Boolean;
                           P4_Boolean : Java.Boolean)
                           return Java.Boolean;

   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P2_Boolean : Java.Boolean);

   function AttemptMark (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Boolean : Java.Boolean)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AtomicMarkableReference);
   pragma Import (Java, GetReference, "getReference");
   pragma Import (Java, IsMarked, "isMarked");
   pragma Import (Java, Get, "get");
   pragma Import (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Import (Java, CompareAndSet, "compareAndSet");
   pragma Import (Java, Set, "set");
   pragma Import (Java, AttemptMark, "attemptMark");

end Java.Util.Concurrent.Atomic.AtomicMarkableReference;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicMarkableReference, "java.util.concurrent.atomic.AtomicMarkableReference");
pragma Extensions_Allowed (Off);
