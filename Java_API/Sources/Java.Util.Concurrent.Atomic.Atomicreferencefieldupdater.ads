pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicReferenceFieldUpdater is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewUpdater (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Util.Concurrent.Atomic.AtomicReferenceFieldUpdater.Typ'Class;

   --  protected
   function New_AtomicReferenceFieldUpdater (This : Ref := null)
                                             return Ref;

   function CompareAndSet (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean is abstract;

   function WeakCompareAndSet (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean is abstract;

   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure LazySet (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function GetAndSet (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewUpdater, "newUpdater");
   pragma Java_Constructor (New_AtomicReferenceFieldUpdater);
   pragma Export (Java, CompareAndSet, "compareAndSet");
   pragma Export (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Export (Java, Set, "set");
   pragma Export (Java, LazySet, "lazySet");
   pragma Export (Java, Get, "get");
   pragma Import (Java, GetAndSet, "getAndSet");

end Java.Util.Concurrent.Atomic.AtomicReferenceFieldUpdater;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicReferenceFieldUpdater, "java.util.concurrent.atomic.AtomicReferenceFieldUpdater");
pragma Extensions_Allowed (Off);
