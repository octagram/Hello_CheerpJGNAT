pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.Concurrent.Atomic.AtomicStampedReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AtomicStampedReference (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P2_Int : Java.Int; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReference (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   function GetStamp (This : access Typ)
                      return Java.Int;

   function Get (This : access Typ;
                 P1_Int_Arr : Java.Int_Arr)
                 return access Java.Lang.Object.Typ'Class;

   function WeakCompareAndSet (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int)
                               return Java.Boolean;

   function CompareAndSet (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int)
                           return Java.Boolean;

   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P2_Int : Java.Int);

   function AttemptStamp (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Int : Java.Int)
                          return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AtomicStampedReference);
   pragma Import (Java, GetReference, "getReference");
   pragma Import (Java, GetStamp, "getStamp");
   pragma Import (Java, Get, "get");
   pragma Import (Java, WeakCompareAndSet, "weakCompareAndSet");
   pragma Import (Java, CompareAndSet, "compareAndSet");
   pragma Import (Java, Set, "set");
   pragma Import (Java, AttemptStamp, "attemptStamp");

end Java.Util.Concurrent.Atomic.AtomicStampedReference;
pragma Import (Java, Java.Util.Concurrent.Atomic.AtomicStampedReference, "java.util.concurrent.atomic.AtomicStampedReference");
pragma Extensions_Allowed (Off);
