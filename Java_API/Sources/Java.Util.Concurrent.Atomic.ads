pragma Extensions_Allowed (On);
package Java.Util.Concurrent.Atomic is
   pragma Preelaborate;
end Java.Util.Concurrent.Atomic;
pragma Import (Java, Java.Util.Concurrent.Atomic, "java.util.concurrent.atomic");
pragma Extensions_Allowed (Off);
