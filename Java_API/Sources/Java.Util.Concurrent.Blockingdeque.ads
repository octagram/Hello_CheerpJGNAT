pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.Iterator;
with Java.Lang.Object;
with Java.Util.Concurrent.BlockingQueue;
with Java.Util.Deque;

package Java.Util.Concurrent.BlockingDeque is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Deque_I : Java.Util.Deque.Ref;
            BlockingQueue_I : Java.Util.Concurrent.BlockingQueue.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddFirst (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure AddLast (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function OfferFirst (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean is abstract;

   function OfferLast (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean is abstract;

   procedure PutFirst (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   procedure PutLast (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function OfferFirst (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Long : Java.Long;
                        P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function OfferLast (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Long : Java.Long;
                       P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function TakeFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function TakeLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function PollFirst (This : access Typ;
                       P1_Long : Java.Long;
                       P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function PollLast (This : access Typ;
                      P1_Long : Java.Long;
                      P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function RemoveFirstOccurrence (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return Java.Boolean is abstract;

   function RemoveLastOccurrence (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return Java.Boolean is abstract;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean is abstract;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean is abstract;

   procedure Put (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P2_Long : Java.Long;
                   P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Remove (This : access Typ)
                    return access Java.Lang.Object.Typ'Class is abstract;

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;

   function Take (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ;
                  P1_Long : Java.Long;
                  P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Element (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean is abstract;

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class is abstract;

   procedure Push (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddFirst, "addFirst");
   pragma Export (Java, AddLast, "addLast");
   pragma Export (Java, OfferFirst, "offerFirst");
   pragma Export (Java, OfferLast, "offerLast");
   pragma Export (Java, PutFirst, "putFirst");
   pragma Export (Java, PutLast, "putLast");
   pragma Export (Java, TakeFirst, "takeFirst");
   pragma Export (Java, TakeLast, "takeLast");
   pragma Export (Java, PollFirst, "pollFirst");
   pragma Export (Java, PollLast, "pollLast");
   pragma Export (Java, RemoveFirstOccurrence, "removeFirstOccurrence");
   pragma Export (Java, RemoveLastOccurrence, "removeLastOccurrence");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Offer, "offer");
   pragma Export (Java, Put, "put");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Poll, "poll");
   pragma Export (Java, Take, "take");
   pragma Export (Java, Element, "element");
   pragma Export (Java, Peek, "peek");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, Size, "size");
   pragma Export (Java, Iterator, "iterator");
   pragma Export (Java, Push, "push");

end Java.Util.Concurrent.BlockingDeque;
pragma Import (Java, Java.Util.Concurrent.BlockingDeque, "java.util.concurrent.BlockingDeque");
pragma Extensions_Allowed (Off);
