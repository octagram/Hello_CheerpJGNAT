pragma Extensions_Allowed (On);
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;
with Java.Util.Queue;

package Java.Util.Concurrent.BlockingQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Queue_I : Java.Util.Queue.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean is abstract;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean is abstract;

   procedure Put (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P2_Long : Java.Long;
                   P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Take (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ;
                  P1_Long : Java.Long;
                  P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function RemainingCapacity (This : access Typ)
                               return Java.Int is abstract;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean is abstract;

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                     return Java.Int is abstract;

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Add, "add");
   pragma Export (Java, Offer, "offer");
   pragma Export (Java, Put, "put");
   pragma Export (Java, Take, "take");
   pragma Export (Java, Poll, "poll");
   pragma Export (Java, RemainingCapacity, "remainingCapacity");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, DrainTo, "drainTo");

end Java.Util.Concurrent.BlockingQueue;
pragma Import (Java, Java.Util.Concurrent.BlockingQueue, "java.util.concurrent.BlockingQueue");
pragma Extensions_Allowed (Off);
