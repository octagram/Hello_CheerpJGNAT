pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.Future;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;

package Java.Util.Concurrent.CompletionService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Submit (This : access Typ;
                    P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class is abstract;

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class is abstract;

   function Take (This : access Typ)
                  return access Java.Util.Concurrent.Future.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ)
                  return access Java.Util.Concurrent.Future.Typ'Class is abstract;

   function Poll (This : access Typ;
                  P1_Long : Java.Long;
                  P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                  return access Java.Util.Concurrent.Future.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Submit, "submit");
   pragma Export (Java, Take, "take");
   pragma Export (Java, Poll, "poll");

end Java.Util.Concurrent.CompletionService;
pragma Import (Java, Java.Util.Concurrent.CompletionService, "java.util.concurrent.CompletionService");
pragma Extensions_Allowed (Off);
