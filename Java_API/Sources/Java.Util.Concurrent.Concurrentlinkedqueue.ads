pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.AbstractQueue;
with Java.Util.Collection;
with Java.Util.Queue;

package Java.Util.Concurrent.ConcurrentLinkedQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Queue_I : Java.Util.Queue.Ref)
    is new Java.Util.AbstractQueue.Typ(Collection_I,
                                       Queue_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ConcurrentLinkedQueue (This : Ref := null)
                                       return Ref;

   function New_ConcurrentLinkedQueue (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Size (This : access Typ)
                  return Java.Int;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConcurrentLinkedQueue);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Offer, "offer");
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Iterator, "iterator");

end Java.Util.Concurrent.ConcurrentLinkedQueue;
pragma Import (Java, Java.Util.Concurrent.ConcurrentLinkedQueue, "java.util.concurrent.ConcurrentLinkedQueue");
pragma Extensions_Allowed (Off);
