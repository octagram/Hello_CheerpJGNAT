pragma Extensions_Allowed (On);
limited with Java.Util.NavigableSet;
with Java.Lang.Object;
with Java.Util.Concurrent.ConcurrentMap;
with Java.Util.NavigableMap;

package Java.Util.Concurrent.ConcurrentNavigableMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            NavigableMap_I : Java.Util.NavigableMap.Ref;
            ConcurrentMap_I : Java.Util.Concurrent.ConcurrentMap.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean;
                    P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P4_Boolean : Java.Boolean)
                    return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class is abstract;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class is abstract;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class is abstract;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class is abstract;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class is abstract;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class is abstract;

   function DescendingMap (This : access Typ)
                           return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class is abstract;

   function NavigableKeySet (This : access Typ)
                             return access Java.Util.NavigableSet.Typ'Class is abstract;

   function KeySet (This : access Typ)
                    return access Java.Util.NavigableSet.Typ'Class is abstract;

   function DescendingKeySet (This : access Typ)
                              return access Java.Util.NavigableSet.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SubMap, "subMap");
   pragma Export (Java, HeadMap, "headMap");
   pragma Export (Java, TailMap, "tailMap");
   pragma Export (Java, DescendingMap, "descendingMap");
   pragma Export (Java, NavigableKeySet, "navigableKeySet");
   pragma Export (Java, KeySet, "keySet");
   pragma Export (Java, DescendingKeySet, "descendingKeySet");

end Java.Util.Concurrent.ConcurrentNavigableMap;
pragma Import (Java, Java.Util.Concurrent.ConcurrentNavigableMap, "java.util.concurrent.ConcurrentNavigableMap");
pragma Extensions_Allowed (Off);
