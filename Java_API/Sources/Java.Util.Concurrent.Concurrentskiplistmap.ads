pragma Extensions_Allowed (On);
limited with Java.Util.Collection;
limited with Java.Util.Comparator;
limited with Java.Util.Map.Entry_K;
limited with Java.Util.NavigableMap;
limited with Java.Util.NavigableSet;
limited with Java.Util.Set;
limited with Java.Util.SortedMap;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractMap;
with Java.Util.Concurrent.ConcurrentNavigableMap;
with Java.Util.Map;

package Java.Util.Concurrent.ConcurrentSkipListMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref;
            ConcurrentNavigableMap_I : Java.Util.Concurrent.ConcurrentNavigableMap.Ref)
    is new Java.Util.AbstractMap.Typ(Map_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ConcurrentSkipListMap (This : Ref := null)
                                       return Ref;

   function New_ConcurrentSkipListMap (P1_Comparator : access Standard.Java.Util.Comparator.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_ConcurrentSkipListMap (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_ConcurrentSkipListMap (P1_SortedMap : access Standard.Java.Util.SortedMap.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Util.Concurrent.ConcurrentSkipListMap.Typ'Class;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   procedure Clear (This : access Typ);

   function KeySet (This : access Typ)
                    return access Java.Util.NavigableSet.Typ'Class;

   function NavigableKeySet (This : access Typ)
                             return access Java.Util.NavigableSet.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function DescendingMap (This : access Typ)
                           return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class;

   function DescendingKeySet (This : access Typ)
                              return access Java.Util.NavigableSet.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function PutIfAbsent (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Replace (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean;

   function Replace (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;

   function Comparator (This : access Typ)
                        return access Java.Util.Comparator.Typ'Class;

   function FirstKey (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function LastKey (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean;
                    P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P4_Boolean : Java.Boolean)
                    return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.Concurrent.ConcurrentNavigableMap.Typ'Class;

   function LowerEntry (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Util.Map.Entry_K.Typ'Class;

   function LowerKey (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function FloorEntry (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Util.Map.Entry_K.Typ'Class;

   function FloorKey (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function CeilingEntry (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Util.Map.Entry_K.Typ'Class;

   function CeilingKey (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function HigherEntry (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Util.Map.Entry_K.Typ'Class;

   function HigherKey (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function FirstEntry (This : access Typ)
                        return access Java.Util.Map.Entry_K.Typ'Class;

   function LastEntry (This : access Typ)
                       return access Java.Util.Map.Entry_K.Typ'Class;

   function PollFirstEntry (This : access Typ)
                            return access Java.Util.Map.Entry_K.Typ'Class;

   function PollLastEntry (This : access Typ)
                           return access Java.Util.Map.Entry_K.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.SortedMap.Typ'Class;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableMap.Typ'Class;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableMap.Typ'Class;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean;
                    P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P4_Boolean : Java.Boolean)
                    return access Java.Util.NavigableMap.Typ'Class;

   function DescendingMap (This : access Typ)
                           return access Java.Util.NavigableMap.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConcurrentSkipListMap);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, NavigableKeySet, "navigableKeySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, DescendingMap, "descendingMap");
   pragma Import (Java, DescendingKeySet, "descendingKeySet");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, PutIfAbsent, "putIfAbsent");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, Comparator, "comparator");
   pragma Import (Java, FirstKey, "firstKey");
   pragma Import (Java, LastKey, "lastKey");
   pragma Import (Java, SubMap, "subMap");
   pragma Import (Java, HeadMap, "headMap");
   pragma Import (Java, TailMap, "tailMap");
   pragma Import (Java, LowerEntry, "lowerEntry");
   pragma Import (Java, LowerKey, "lowerKey");
   pragma Import (Java, FloorEntry, "floorEntry");
   pragma Import (Java, FloorKey, "floorKey");
   pragma Import (Java, CeilingEntry, "ceilingEntry");
   pragma Import (Java, CeilingKey, "ceilingKey");
   pragma Import (Java, HigherEntry, "higherEntry");
   pragma Import (Java, HigherKey, "higherKey");
   pragma Import (Java, FirstEntry, "firstEntry");
   pragma Import (Java, LastEntry, "lastEntry");
   pragma Import (Java, PollFirstEntry, "pollFirstEntry");
   pragma Import (Java, PollLastEntry, "pollLastEntry");

end Java.Util.Concurrent.ConcurrentSkipListMap;
pragma Import (Java, Java.Util.Concurrent.ConcurrentSkipListMap, "java.util.concurrent.ConcurrentSkipListMap");
pragma Extensions_Allowed (Off);
