pragma Extensions_Allowed (On);
limited with Java.Util.Comparator;
limited with Java.Util.Iterator;
limited with Java.Util.SortedSet;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractSet;
with Java.Util.Collection;
with Java.Util.NavigableSet;
with Java.Util.Set;

package Java.Util.Concurrent.ConcurrentSkipListSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            NavigableSet_I : Java.Util.NavigableSet.Ref;
            Set_I : Java.Util.Set.Ref)
    is new Java.Util.AbstractSet.Typ(Collection_I,
                                     Set_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ConcurrentSkipListSet (This : Ref := null)
                                       return Ref;

   function New_ConcurrentSkipListSet (P1_Comparator : access Standard.Java.Util.Comparator.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_ConcurrentSkipListSet (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_ConcurrentSkipListSet (P1_SortedSet : access Standard.Java.Util.SortedSet.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Util.Concurrent.ConcurrentSkipListSet.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   procedure Clear (This : access Typ);

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function DescendingIterator (This : access Typ)
                                return access Java.Util.Iterator.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function RemoveAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   function Lower (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   function Floor (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   function Ceiling (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;

   function Higher (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function PollFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function PollLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function Comparator (This : access Typ)
                        return access Java.Util.Comparator.Typ'Class;

   function First (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Last (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function SubSet (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean;
                    P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P4_Boolean : Java.Boolean)
                    return access Java.Util.NavigableSet.Typ'Class;

   function HeadSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableSet.Typ'Class;

   function TailSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableSet.Typ'Class;

   function SubSet (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.NavigableSet.Typ'Class;

   function HeadSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.NavigableSet.Typ'Class;

   function TailSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.NavigableSet.Typ'Class;

   function DescendingSet (This : access Typ)
                           return access Java.Util.NavigableSet.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   function TailSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedSet.Typ'Class;

   function HeadSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedSet.Typ'Class;

   function SubSet (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.SortedSet.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConcurrentSkipListSet);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, DescendingIterator, "descendingIterator");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, Lower, "lower");
   pragma Import (Java, Floor, "floor");
   pragma Import (Java, Ceiling, "ceiling");
   pragma Import (Java, Higher, "higher");
   pragma Import (Java, PollFirst, "pollFirst");
   pragma Import (Java, PollLast, "pollLast");
   pragma Import (Java, Comparator, "comparator");
   pragma Import (Java, First, "first");
   pragma Import (Java, Last, "last");
   pragma Import (Java, SubSet, "subSet");
   pragma Import (Java, HeadSet, "headSet");
   pragma Import (Java, TailSet, "tailSet");
   pragma Import (Java, DescendingSet, "descendingSet");

end Java.Util.Concurrent.ConcurrentSkipListSet;
pragma Import (Java, Java.Util.Concurrent.ConcurrentSkipListSet, "java.util.concurrent.ConcurrentSkipListSet");
pragma Extensions_Allowed (Off);
