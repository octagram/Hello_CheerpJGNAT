pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.AbstractSet;
with Java.Util.Collection;
with Java.Util.Set;

package Java.Util.Concurrent.CopyOnWriteArraySet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Set_I : Java.Util.Set.Ref)
    is new Java.Util.AbstractSet.Typ(Collection_I,
                                     Set_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CopyOnWriteArraySet (This : Ref := null)
                                     return Ref;

   function New_CopyOnWriteArraySet (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   procedure Clear (This : access Typ);

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function ContainsAll (This : access Typ;
                         P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                         return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function RemoveAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   function RetainAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CopyOnWriteArraySet);
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Add, "add");
   pragma Import (Java, ContainsAll, "containsAll");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, RetainAll, "retainAll");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, Equals, "equals");

end Java.Util.Concurrent.CopyOnWriteArraySet;
pragma Import (Java, Java.Util.Concurrent.CopyOnWriteArraySet, "java.util.concurrent.CopyOnWriteArraySet");
pragma Extensions_Allowed (Off);
