pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;

package Java.Util.Concurrent.CountDownLatch is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CountDownLatch (P1_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Await (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   function Await (This : access Typ;
                   P1_Long : Java.Long;
                   P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   procedure CountDown (This : access Typ);

   function GetCount (This : access Typ)
                      return Java.Long;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CountDownLatch);
   pragma Import (Java, Await, "await");
   pragma Import (Java, CountDown, "countDown");
   pragma Import (Java, GetCount, "getCount");
   pragma Import (Java, ToString, "toString");

end Java.Util.Concurrent.CountDownLatch;
pragma Import (Java, Java.Util.Concurrent.CountDownLatch, "java.util.concurrent.CountDownLatch");
pragma Extensions_Allowed (Off);
