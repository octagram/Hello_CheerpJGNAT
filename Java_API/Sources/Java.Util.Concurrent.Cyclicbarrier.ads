pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;

package Java.Util.Concurrent.CyclicBarrier is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CyclicBarrier (P1_Int : Java.Int;
                               P2_Runnable : access Standard.Java.Lang.Runnable.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_CyclicBarrier (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParties (This : access Typ)
                        return Java.Int;

   function Await (This : access Typ)
                   return Java.Int;
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Util.Concurrent.BrokenBarrierException.Except

   function Await (This : access Typ;
                   P1_Long : Java.Long;
                   P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Int;
   --  can raise Java.Lang.InterruptedException.Except,
   --  Java.Util.Concurrent.BrokenBarrierException.Except and
   --  Java.Util.Concurrent.TimeoutException.Except

   function IsBroken (This : access Typ)
                      return Java.Boolean;

   procedure Reset (This : access Typ);

   function GetNumberWaiting (This : access Typ)
                              return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CyclicBarrier);
   pragma Import (Java, GetParties, "getParties");
   pragma Import (Java, Await, "await");
   pragma Import (Java, IsBroken, "isBroken");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, GetNumberWaiting, "getNumberWaiting");

end Java.Util.Concurrent.CyclicBarrier;
pragma Import (Java, Java.Util.Concurrent.CyclicBarrier, "java.util.concurrent.CyclicBarrier");
pragma Extensions_Allowed (Off);
