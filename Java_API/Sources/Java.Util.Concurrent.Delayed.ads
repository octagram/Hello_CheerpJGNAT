pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Util.Concurrent.Delayed is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDelay (This : access Typ;
                      P1_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return Java.Long is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDelay, "getDelay");

end Java.Util.Concurrent.Delayed;
pragma Import (Java, Java.Util.Concurrent.Delayed, "java.util.concurrent.Delayed");
pragma Extensions_Allowed (Off);
