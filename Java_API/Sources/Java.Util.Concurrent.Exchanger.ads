pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;

package Java.Util.Concurrent.Exchanger is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Exchanger (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Exchange (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Exchange (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Long : Java.Long;
                      P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Util.Concurrent.TimeoutException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Exchanger);
   pragma Import (Java, Exchange, "exchange");

end Java.Util.Concurrent.Exchanger;
pragma Import (Java, Java.Util.Concurrent.Exchanger, "java.util.concurrent.Exchanger");
pragma Extensions_Allowed (Off);
