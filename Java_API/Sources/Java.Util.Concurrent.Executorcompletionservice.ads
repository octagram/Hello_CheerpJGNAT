pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Util.Concurrent.BlockingQueue;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.Executor;
limited with Java.Util.Concurrent.Future;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;
with Java.Util.Concurrent.CompletionService;

package Java.Util.Concurrent.ExecutorCompletionService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CompletionService_I : Java.Util.Concurrent.CompletionService.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ExecutorCompletionService (P1_Executor : access Standard.Java.Util.Concurrent.Executor.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   function New_ExecutorCompletionService (P1_Executor : access Standard.Java.Util.Concurrent.Executor.Typ'Class;
                                           P2_BlockingQueue : access Standard.Java.Util.Concurrent.BlockingQueue.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Submit (This : access Typ;
                    P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   function Take (This : access Typ)
                  return access Java.Util.Concurrent.Future.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ)
                  return access Java.Util.Concurrent.Future.Typ'Class;

   function Poll (This : access Typ;
                  P1_Long : Java.Long;
                  P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                  return access Java.Util.Concurrent.Future.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ExecutorCompletionService);
   pragma Import (Java, Submit, "submit");
   pragma Import (Java, Take, "take");
   pragma Import (Java, Poll, "poll");

end Java.Util.Concurrent.ExecutorCompletionService;
pragma Import (Java, Java.Util.Concurrent.ExecutorCompletionService, "java.util.concurrent.ExecutorCompletionService");
pragma Extensions_Allowed (Off);
