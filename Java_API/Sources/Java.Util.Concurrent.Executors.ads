pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Security.PrivilegedAction;
limited with Java.Security.PrivilegedExceptionAction;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.ExecutorService;
limited with Java.Util.Concurrent.ScheduledExecutorService;
limited with Java.Util.Concurrent.ThreadFactory;
with Java.Lang.Object;

package Java.Util.Concurrent.Executors is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewFixedThreadPool (P1_Int : Java.Int)
                                return access Java.Util.Concurrent.ExecutorService.Typ'Class;

   function NewFixedThreadPool (P1_Int : Java.Int;
                                P2_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class)
                                return access Java.Util.Concurrent.ExecutorService.Typ'Class;

   function NewSingleThreadExecutor return access Java.Util.Concurrent.ExecutorService.Typ'Class;

   function NewSingleThreadExecutor (P1_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class)
                                     return access Java.Util.Concurrent.ExecutorService.Typ'Class;

   function NewCachedThreadPool return access Java.Util.Concurrent.ExecutorService.Typ'Class;

   function NewCachedThreadPool (P1_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class)
                                 return access Java.Util.Concurrent.ExecutorService.Typ'Class;

   function NewSingleThreadScheduledExecutor return access Java.Util.Concurrent.ScheduledExecutorService.Typ'Class;

   function NewSingleThreadScheduledExecutor (P1_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class)
                                              return access Java.Util.Concurrent.ScheduledExecutorService.Typ'Class;

   function NewScheduledThreadPool (P1_Int : Java.Int)
                                    return access Java.Util.Concurrent.ScheduledExecutorService.Typ'Class;

   function NewScheduledThreadPool (P1_Int : Java.Int;
                                    P2_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class)
                                    return access Java.Util.Concurrent.ScheduledExecutorService.Typ'Class;

   function UnconfigurableExecutorService (P1_ExecutorService : access Standard.Java.Util.Concurrent.ExecutorService.Typ'Class)
                                           return access Java.Util.Concurrent.ExecutorService.Typ'Class;

   function UnconfigurableScheduledExecutorService (P1_ScheduledExecutorService : access Standard.Java.Util.Concurrent.ScheduledExecutorService.Typ'Class)
                                                    return access Java.Util.Concurrent.ScheduledExecutorService.Typ'Class;

   function DefaultThreadFactory return access Java.Util.Concurrent.ThreadFactory.Typ'Class;

   function PrivilegedThreadFactory return access Java.Util.Concurrent.ThreadFactory.Typ'Class;

   function Callable (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Util.Concurrent.Callable.Typ'Class;

   function Callable (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class)
                      return access Java.Util.Concurrent.Callable.Typ'Class;

   function Callable (P1_PrivilegedAction : access Standard.Java.Security.PrivilegedAction.Typ'Class)
                      return access Java.Util.Concurrent.Callable.Typ'Class;

   function Callable (P1_PrivilegedExceptionAction : access Standard.Java.Security.PrivilegedExceptionAction.Typ'Class)
                      return access Java.Util.Concurrent.Callable.Typ'Class;

   function PrivilegedCallable (P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                                return access Java.Util.Concurrent.Callable.Typ'Class;

   function PrivilegedCallableUsingCurrentClassLoader (P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                                                       return access Java.Util.Concurrent.Callable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewFixedThreadPool, "newFixedThreadPool");
   pragma Import (Java, NewSingleThreadExecutor, "newSingleThreadExecutor");
   pragma Import (Java, NewCachedThreadPool, "newCachedThreadPool");
   pragma Import (Java, NewSingleThreadScheduledExecutor, "newSingleThreadScheduledExecutor");
   pragma Import (Java, NewScheduledThreadPool, "newScheduledThreadPool");
   pragma Import (Java, UnconfigurableExecutorService, "unconfigurableExecutorService");
   pragma Import (Java, UnconfigurableScheduledExecutorService, "unconfigurableScheduledExecutorService");
   pragma Import (Java, DefaultThreadFactory, "defaultThreadFactory");
   pragma Import (Java, PrivilegedThreadFactory, "privilegedThreadFactory");
   pragma Import (Java, Callable, "callable");
   pragma Import (Java, PrivilegedCallable, "privilegedCallable");
   pragma Import (Java, PrivilegedCallableUsingCurrentClassLoader, "privilegedCallableUsingCurrentClassLoader");

end Java.Util.Concurrent.Executors;
pragma Import (Java, Java.Util.Concurrent.Executors, "java.util.concurrent.Executors");
pragma Extensions_Allowed (Off);
