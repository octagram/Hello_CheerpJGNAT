pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.Future;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.List;
with Java.Lang.Object;
with Java.Util.Concurrent.Executor;

package Java.Util.Concurrent.ExecutorService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Executor_I : Java.Util.Concurrent.Executor.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Shutdown (This : access Typ) is abstract;

   function ShutdownNow (This : access Typ)
                         return access Java.Util.List.Typ'Class is abstract;

   function IsShutdown (This : access Typ)
                        return Java.Boolean is abstract;

   function IsTerminated (This : access Typ)
                          return Java.Boolean is abstract;

   function AwaitTermination (This : access Typ;
                              P1_Long : Java.Long;
                              P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                              return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Submit (This : access Typ;
                    P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class is abstract;

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class is abstract;

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class is abstract;

   function InvokeAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return access Java.Util.List.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function InvokeAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                       P2_Long : Java.Long;
                       P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return access Java.Util.List.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function InvokeAny (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Util.Concurrent.ExecutionException.Except

   function InvokeAny (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                       P2_Long : Java.Long;
                       P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except,
   --  Java.Util.Concurrent.ExecutionException.Except and
   --  Java.Util.Concurrent.TimeoutException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Shutdown, "shutdown");
   pragma Export (Java, ShutdownNow, "shutdownNow");
   pragma Export (Java, IsShutdown, "isShutdown");
   pragma Export (Java, IsTerminated, "isTerminated");
   pragma Export (Java, AwaitTermination, "awaitTermination");
   pragma Export (Java, Submit, "submit");
   pragma Export (Java, InvokeAll, "invokeAll");
   pragma Export (Java, InvokeAny, "invokeAny");

end Java.Util.Concurrent.ExecutorService;
pragma Import (Java, Java.Util.Concurrent.ExecutorService, "java.util.concurrent.ExecutorService");
pragma Extensions_Allowed (Off);
