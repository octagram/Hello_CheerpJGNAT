pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;

package Java.Util.Concurrent.Future is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Cancel (This : access Typ;
                    P1_Boolean : Java.Boolean)
                    return Java.Boolean is abstract;

   function IsCancelled (This : access Typ)
                         return Java.Boolean is abstract;

   function IsDone (This : access Typ)
                    return Java.Boolean is abstract;

   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Util.Concurrent.ExecutionException.Except

   function Get (This : access Typ;
                 P1_Long : Java.Long;
                 P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.InterruptedException.Except,
   --  Java.Util.Concurrent.ExecutionException.Except and
   --  Java.Util.Concurrent.TimeoutException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Cancel, "cancel");
   pragma Export (Java, IsCancelled, "isCancelled");
   pragma Export (Java, IsDone, "isDone");
   pragma Export (Java, Get, "get");

end Java.Util.Concurrent.Future;
pragma Import (Java, Java.Util.Concurrent.Future, "java.util.concurrent.Future");
pragma Extensions_Allowed (Off);
