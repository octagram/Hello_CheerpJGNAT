pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Lang.Throwable;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;
with Java.Util.Concurrent.RunnableFuture;

package Java.Util.Concurrent.FutureTask is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RunnableFuture_I : Java.Util.Concurrent.RunnableFuture.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FutureTask (P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_FutureTask (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsCancelled (This : access Typ)
                         return Java.Boolean;

   function IsDone (This : access Typ)
                    return Java.Boolean;

   function Cancel (This : access Typ;
                    P1_Boolean : Java.Boolean)
                    return Java.Boolean;

   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Util.Concurrent.ExecutionException.Except

   function Get (This : access Typ;
                 P1_Long : Java.Long;
                 P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except,
   --  Java.Util.Concurrent.ExecutionException.Except and
   --  Java.Util.Concurrent.TimeoutException.Except

   --  protected
   procedure Done (This : access Typ);

   --  protected
   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   procedure SetException (This : access Typ;
                           P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   procedure Run (This : access Typ);

   --  protected
   function RunAndReset (This : access Typ)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FutureTask);
   pragma Import (Java, IsCancelled, "isCancelled");
   pragma Import (Java, IsDone, "isDone");
   pragma Import (Java, Cancel, "cancel");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Done, "done");
   pragma Import (Java, Set, "set");
   pragma Import (Java, SetException, "setException");
   pragma Import (Java, Run, "run");
   pragma Import (Java, RunAndReset, "runAndReset");

end Java.Util.Concurrent.FutureTask;
pragma Import (Java, Java.Util.Concurrent.FutureTask, "java.util.concurrent.FutureTask");
pragma Extensions_Allowed (Off);
