pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.AbstractQueue;
with Java.Util.Collection;
with Java.Util.Concurrent.BlockingDeque;
with Java.Util.Queue;

package Java.Util.Concurrent.LinkedBlockingDeque is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Queue_I : Java.Util.Queue.Ref;
            BlockingDeque_I : Java.Util.Concurrent.BlockingDeque.Ref)
    is new Java.Util.AbstractQueue.Typ(Collection_I,
                                       Queue_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinkedBlockingDeque (This : Ref := null)
                                     return Ref;

   function New_LinkedBlockingDeque (P1_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   function New_LinkedBlockingDeque (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddFirst (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddLast (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function OfferFirst (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean;

   function OfferLast (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   procedure PutFirst (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.InterruptedException.Except

   procedure PutLast (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.InterruptedException.Except

   function OfferFirst (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Long : Java.Long;
                        P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                        return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   function OfferLast (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Long : Java.Long;
                       P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   function RemoveFirst (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function RemoveLast (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   function PollFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function PollLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function TakeFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function TakeLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function PollFirst (This : access Typ;
                       P1_Long : Java.Long;
                       P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function PollLast (This : access Typ;
                      P1_Long : Java.Long;
                      P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function GetFirst (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function GetLast (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function PeekFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function PeekLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function RemoveFirstOccurrence (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return Java.Boolean;

   function RemoveLastOccurrence (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return Java.Boolean;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   procedure Put (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.InterruptedException.Except

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P2_Long : Java.Long;
                   P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   function Remove (This : access Typ)
                    return access Java.Lang.Object.Typ'Class;

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Take (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ;
                  P1_Long : Java.Long;
                  P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Element (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function RemainingCapacity (This : access Typ)
                               return Java.Int;

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                     return Java.Int;

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;

   procedure Push (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Pop (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Size (This : access Typ)
                  return Java.Int;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Clear (This : access Typ);

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function DescendingIterator (This : access Typ)
                                return access Java.Util.Iterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LinkedBlockingDeque);
   pragma Import (Java, AddFirst, "addFirst");
   pragma Import (Java, AddLast, "addLast");
   pragma Import (Java, OfferFirst, "offerFirst");
   pragma Import (Java, OfferLast, "offerLast");
   pragma Import (Java, PutFirst, "putFirst");
   pragma Import (Java, PutLast, "putLast");
   pragma Import (Java, RemoveFirst, "removeFirst");
   pragma Import (Java, RemoveLast, "removeLast");
   pragma Import (Java, PollFirst, "pollFirst");
   pragma Import (Java, PollLast, "pollLast");
   pragma Import (Java, TakeFirst, "takeFirst");
   pragma Import (Java, TakeLast, "takeLast");
   pragma Import (Java, GetFirst, "getFirst");
   pragma Import (Java, GetLast, "getLast");
   pragma Import (Java, PeekFirst, "peekFirst");
   pragma Import (Java, PeekLast, "peekLast");
   pragma Import (Java, RemoveFirstOccurrence, "removeFirstOccurrence");
   pragma Import (Java, RemoveLastOccurrence, "removeLastOccurrence");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Offer, "offer");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, Take, "take");
   pragma Import (Java, Element, "element");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, RemainingCapacity, "remainingCapacity");
   pragma Import (Java, DrainTo, "drainTo");
   pragma Import (Java, Push, "push");
   pragma Import (Java, Pop, "pop");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, DescendingIterator, "descendingIterator");

end Java.Util.Concurrent.LinkedBlockingDeque;
pragma Import (Java, Java.Util.Concurrent.LinkedBlockingDeque, "java.util.concurrent.LinkedBlockingDeque");
pragma Extensions_Allowed (Off);
