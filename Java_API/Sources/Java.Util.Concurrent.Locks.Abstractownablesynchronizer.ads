pragma Extensions_Allowed (On);
limited with Java.Lang.Thread;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Concurrent.Locks.AbstractOwnableSynchronizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_AbstractOwnableSynchronizer (This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   procedure SetExclusiveOwnerThread (This : access Typ;
                                      P1_Thread : access Standard.Java.Lang.Thread.Typ'Class);

   --  final  protected
   function GetExclusiveOwnerThread (This : access Typ)
                                     return access Java.Lang.Thread.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractOwnableSynchronizer);
   pragma Import (Java, SetExclusiveOwnerThread, "setExclusiveOwnerThread");
   pragma Import (Java, GetExclusiveOwnerThread, "getExclusiveOwnerThread");

end Java.Util.Concurrent.Locks.AbstractOwnableSynchronizer;
pragma Import (Java, Java.Util.Concurrent.Locks.AbstractOwnableSynchronizer, "java.util.concurrent.locks.AbstractOwnableSynchronizer");
pragma Extensions_Allowed (Off);
