pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Thread;
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer.ConditionObject;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.Concurrent.Locks.AbstractOwnableSynchronizer;

package Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Util.Concurrent.Locks.AbstractOwnableSynchronizer.Typ(Serializable_I)
      with null record;

   --  protected
   function New_AbstractQueuedLongSynchronizer (This : Ref := null)
                                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   function GetState (This : access Typ)
                      return Java.Long;

   --  final  protected
   procedure SetState (This : access Typ;
                       P1_Long : Java.Long);

   --  final  protected
   function CompareAndSetState (This : access Typ;
                                P1_Long : Java.Long;
                                P2_Long : Java.Long)
                                return Java.Boolean;

   --  protected
   function TryAcquire (This : access Typ;
                        P1_Long : Java.Long)
                        return Java.Boolean;

   --  protected
   function TryRelease (This : access Typ;
                        P1_Long : Java.Long)
                        return Java.Boolean;

   --  protected
   function TryAcquireShared (This : access Typ;
                              P1_Long : Java.Long)
                              return Java.Long;

   --  protected
   function TryReleaseShared (This : access Typ;
                              P1_Long : Java.Long)
                              return Java.Boolean;

   --  protected
   function IsHeldExclusively (This : access Typ)
                               return Java.Boolean;

   --  final
   procedure Acquire (This : access Typ;
                      P1_Long : Java.Long);

   --  final
   procedure AcquireInterruptibly (This : access Typ;
                                   P1_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   function TryAcquireNanos (This : access Typ;
                             P1_Long : Java.Long;
                             P2_Long : Java.Long)
                             return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   function Release (This : access Typ;
                     P1_Long : Java.Long)
                     return Java.Boolean;

   --  final
   procedure AcquireShared (This : access Typ;
                            P1_Long : Java.Long);

   --  final
   procedure AcquireSharedInterruptibly (This : access Typ;
                                         P1_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   function TryAcquireSharedNanos (This : access Typ;
                                   P1_Long : Java.Long;
                                   P2_Long : Java.Long)
                                   return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   function ReleaseShared (This : access Typ;
                           P1_Long : Java.Long)
                           return Java.Boolean;

   --  final
   function HasQueuedThreads (This : access Typ)
                              return Java.Boolean;

   --  final
   function HasContended (This : access Typ)
                          return Java.Boolean;

   --  final
   function GetFirstQueuedThread (This : access Typ)
                                  return access Java.Lang.Thread.Typ'Class;

   --  final
   function IsQueued (This : access Typ;
                      P1_Thread : access Standard.Java.Lang.Thread.Typ'Class)
                      return Java.Boolean;

   --  final
   function GetQueueLength (This : access Typ)
                            return Java.Int;

   --  final
   function GetQueuedThreads (This : access Typ)
                              return access Java.Util.Collection.Typ'Class;

   --  final
   function GetExclusiveQueuedThreads (This : access Typ)
                                       return access Java.Util.Collection.Typ'Class;

   --  final
   function GetSharedQueuedThreads (This : access Typ)
                                    return access Java.Util.Collection.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   function Owns (This : access Typ;
                  P1_ConditionObject : access Standard.Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer.ConditionObject.Typ'Class)
                  return Java.Boolean;

   --  final
   function HasWaiters (This : access Typ;
                        P1_ConditionObject : access Standard.Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer.ConditionObject.Typ'Class)
                        return Java.Boolean;

   --  final
   function GetWaitQueueLength (This : access Typ;
                                P1_ConditionObject : access Standard.Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer.ConditionObject.Typ'Class)
                                return Java.Int;

   --  final
   function GetWaitingThreads (This : access Typ;
                               P1_ConditionObject : access Standard.Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer.ConditionObject.Typ'Class)
                               return access Java.Util.Collection.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractQueuedLongSynchronizer);
   pragma Import (Java, GetState, "getState");
   pragma Import (Java, SetState, "setState");
   pragma Import (Java, CompareAndSetState, "compareAndSetState");
   pragma Import (Java, TryAcquire, "tryAcquire");
   pragma Import (Java, TryRelease, "tryRelease");
   pragma Import (Java, TryAcquireShared, "tryAcquireShared");
   pragma Import (Java, TryReleaseShared, "tryReleaseShared");
   pragma Import (Java, IsHeldExclusively, "isHeldExclusively");
   pragma Import (Java, Acquire, "acquire");
   pragma Import (Java, AcquireInterruptibly, "acquireInterruptibly");
   pragma Import (Java, TryAcquireNanos, "tryAcquireNanos");
   pragma Import (Java, Release, "release");
   pragma Import (Java, AcquireShared, "acquireShared");
   pragma Import (Java, AcquireSharedInterruptibly, "acquireSharedInterruptibly");
   pragma Import (Java, TryAcquireSharedNanos, "tryAcquireSharedNanos");
   pragma Import (Java, ReleaseShared, "releaseShared");
   pragma Import (Java, HasQueuedThreads, "hasQueuedThreads");
   pragma Import (Java, HasContended, "hasContended");
   pragma Import (Java, GetFirstQueuedThread, "getFirstQueuedThread");
   pragma Import (Java, IsQueued, "isQueued");
   pragma Import (Java, GetQueueLength, "getQueueLength");
   pragma Import (Java, GetQueuedThreads, "getQueuedThreads");
   pragma Import (Java, GetExclusiveQueuedThreads, "getExclusiveQueuedThreads");
   pragma Import (Java, GetSharedQueuedThreads, "getSharedQueuedThreads");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Owns, "owns");
   pragma Import (Java, HasWaiters, "hasWaiters");
   pragma Import (Java, GetWaitQueueLength, "getWaitQueueLength");
   pragma Import (Java, GetWaitingThreads, "getWaitingThreads");

end Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer;
pragma Import (Java, Java.Util.Concurrent.Locks.AbstractQueuedLongSynchronizer, "java.util.concurrent.locks.AbstractQueuedLongSynchronizer");
pragma Extensions_Allowed (Off);
