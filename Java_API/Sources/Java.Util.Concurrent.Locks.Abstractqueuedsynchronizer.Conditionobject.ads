pragma Extensions_Allowed (On);
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.Date;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.Concurrent.Locks.Condition;

package Java.Util.Concurrent.Locks.AbstractQueuedSynchronizer.ConditionObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Condition_I : Java.Util.Concurrent.Locks.Condition.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ConditionObject (P1_AbstractQueuedSynchronizer : access Standard.Java.Util.Concurrent.Locks.AbstractQueuedSynchronizer.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   procedure Signal (This : access Typ);

   --  final
   procedure SignalAll (This : access Typ);

   --  final
   procedure AwaitUninterruptibly (This : access Typ);

   --  final
   procedure Await (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   function AwaitNanos (This : access Typ;
                        P1_Long : Java.Long)
                        return Java.Long;
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   function AwaitUntil (This : access Typ;
                        P1_Date : access Standard.Java.Util.Date.Typ'Class)
                        return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   --  final
   function Await (This : access Typ;
                   P1_Long : Java.Long;
                   P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   --  final  protected
   function HasWaiters (This : access Typ)
                        return Java.Boolean;

   --  final  protected
   function GetWaitQueueLength (This : access Typ)
                                return Java.Int;

   --  final  protected
   function GetWaitingThreads (This : access Typ)
                               return access Java.Util.Collection.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConditionObject);
   pragma Import (Java, Signal, "signal");
   pragma Import (Java, SignalAll, "signalAll");
   pragma Import (Java, AwaitUninterruptibly, "awaitUninterruptibly");
   pragma Import (Java, Await, "await");
   pragma Import (Java, AwaitNanos, "awaitNanos");
   pragma Import (Java, AwaitUntil, "awaitUntil");
   pragma Import (Java, HasWaiters, "hasWaiters");
   pragma Import (Java, GetWaitQueueLength, "getWaitQueueLength");
   pragma Import (Java, GetWaitingThreads, "getWaitingThreads");

end Java.Util.Concurrent.Locks.AbstractQueuedSynchronizer.ConditionObject;
pragma Import (Java, Java.Util.Concurrent.Locks.AbstractQueuedSynchronizer.ConditionObject, "java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject");
pragma Extensions_Allowed (Off);
