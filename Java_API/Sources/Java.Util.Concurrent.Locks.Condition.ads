pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.Date;
with Java.Lang.Object;

package Java.Util.Concurrent.Locks.Condition is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Await (This : access Typ) is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   procedure AwaitUninterruptibly (This : access Typ) is abstract;

   function AwaitNanos (This : access Typ;
                        P1_Long : Java.Long)
                        return Java.Long is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function Await (This : access Typ;
                   P1_Long : Java.Long;
                   P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function AwaitUntil (This : access Typ;
                        P1_Date : access Standard.Java.Util.Date.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   procedure Signal (This : access Typ) is abstract;

   procedure SignalAll (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Await, "await");
   pragma Export (Java, AwaitUninterruptibly, "awaitUninterruptibly");
   pragma Export (Java, AwaitNanos, "awaitNanos");
   pragma Export (Java, AwaitUntil, "awaitUntil");
   pragma Export (Java, Signal, "signal");
   pragma Export (Java, SignalAll, "signalAll");

end Java.Util.Concurrent.Locks.Condition;
pragma Import (Java, Java.Util.Concurrent.Locks.Condition, "java.util.concurrent.locks.Condition");
pragma Extensions_Allowed (Off);
