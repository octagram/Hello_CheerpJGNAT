pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.Locks.Condition;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;

package Java.Util.Concurrent.Locks.Lock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Lock (This : access Typ) is abstract;

   procedure LockInterruptibly (This : access Typ) is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   function TryLock (This : access Typ)
                     return Java.Boolean is abstract;

   function TryLock (This : access Typ;
                     P1_Long : Java.Long;
                     P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                     return Java.Boolean is abstract;
   --  can raise Java.Lang.InterruptedException.Except

   procedure Unlock (This : access Typ) is abstract;

   function NewCondition (This : access Typ)
                          return access Java.Util.Concurrent.Locks.Condition.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Lock, "lock");
   pragma Export (Java, LockInterruptibly, "lockInterruptibly");
   pragma Export (Java, TryLock, "tryLock");
   pragma Export (Java, Unlock, "unlock");
   pragma Export (Java, NewCondition, "newCondition");

end Java.Util.Concurrent.Locks.Lock;
pragma Import (Java, Java.Util.Concurrent.Locks.Lock, "java.util.concurrent.locks.Lock");
pragma Extensions_Allowed (Off);
