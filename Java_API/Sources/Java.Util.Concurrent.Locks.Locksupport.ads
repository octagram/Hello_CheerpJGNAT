pragma Extensions_Allowed (On);
limited with Java.Lang.Thread;
with Java.Lang.Object;

package Java.Util.Concurrent.Locks.LockSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Unpark (P1_Thread : access Standard.Java.Lang.Thread.Typ'Class);

   procedure Park (P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure ParkNanos (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Long : Java.Long);

   procedure ParkUntil (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Long : Java.Long);

   function GetBlocker (P1_Thread : access Standard.Java.Lang.Thread.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   procedure Park ;

   procedure ParkNanos (P1_Long : Java.Long);

   procedure ParkUntil (P1_Long : Java.Long);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Unpark, "unpark");
   pragma Import (Java, Park, "park");
   pragma Import (Java, ParkNanos, "parkNanos");
   pragma Import (Java, ParkUntil, "parkUntil");
   pragma Import (Java, GetBlocker, "getBlocker");

end Java.Util.Concurrent.Locks.LockSupport;
pragma Import (Java, Java.Util.Concurrent.Locks.LockSupport, "java.util.concurrent.locks.LockSupport");
pragma Extensions_Allowed (Off);
