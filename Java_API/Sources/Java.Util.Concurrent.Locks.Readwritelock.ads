pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.Locks.Lock;
with Java.Lang.Object;

package Java.Util.Concurrent.Locks.ReadWriteLock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ReadLock (This : access Typ)
                      return access Java.Util.Concurrent.Locks.Lock.Typ'Class is abstract;

   function WriteLock (This : access Typ)
                       return access Java.Util.Concurrent.Locks.Lock.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ReadLock, "readLock");
   pragma Export (Java, WriteLock, "writeLock");

end Java.Util.Concurrent.Locks.ReadWriteLock;
pragma Import (Java, Java.Util.Concurrent.Locks.ReadWriteLock, "java.util.concurrent.locks.ReadWriteLock");
pragma Extensions_Allowed (Off);
