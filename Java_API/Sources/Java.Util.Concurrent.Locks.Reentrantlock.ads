pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Thread;
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.Locks.Condition;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.Concurrent.Locks.Lock;

package Java.Util.Concurrent.Locks.ReentrantLock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Lock_I : Java.Util.Concurrent.Locks.Lock.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ReentrantLock (This : Ref := null)
                               return Ref;

   function New_ReentrantLock (P1_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Lock (This : access Typ);

   procedure LockInterruptibly (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   function TryLock (This : access Typ)
                     return Java.Boolean;

   function TryLock (This : access Typ;
                     P1_Long : Java.Long;
                     P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                     return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   procedure Unlock (This : access Typ);

   function NewCondition (This : access Typ)
                          return access Java.Util.Concurrent.Locks.Condition.Typ'Class;

   function GetHoldCount (This : access Typ)
                          return Java.Int;

   function IsHeldByCurrentThread (This : access Typ)
                                   return Java.Boolean;

   function IsLocked (This : access Typ)
                      return Java.Boolean;

   --  final
   function IsFair (This : access Typ)
                    return Java.Boolean;

   --  protected
   function GetOwner (This : access Typ)
                      return access Java.Lang.Thread.Typ'Class;

   --  final
   function HasQueuedThreads (This : access Typ)
                              return Java.Boolean;

   --  final
   function HasQueuedThread (This : access Typ;
                             P1_Thread : access Standard.Java.Lang.Thread.Typ'Class)
                             return Java.Boolean;

   --  final
   function GetQueueLength (This : access Typ)
                            return Java.Int;

   --  protected
   function GetQueuedThreads (This : access Typ)
                              return access Java.Util.Collection.Typ'Class;

   function HasWaiters (This : access Typ;
                        P1_Condition : access Standard.Java.Util.Concurrent.Locks.Condition.Typ'Class)
                        return Java.Boolean;

   function GetWaitQueueLength (This : access Typ;
                                P1_Condition : access Standard.Java.Util.Concurrent.Locks.Condition.Typ'Class)
                                return Java.Int;

   --  protected
   function GetWaitingThreads (This : access Typ;
                               P1_Condition : access Standard.Java.Util.Concurrent.Locks.Condition.Typ'Class)
                               return access Java.Util.Collection.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ReentrantLock);
   pragma Import (Java, Lock, "lock");
   pragma Import (Java, LockInterruptibly, "lockInterruptibly");
   pragma Import (Java, TryLock, "tryLock");
   pragma Import (Java, Unlock, "unlock");
   pragma Import (Java, NewCondition, "newCondition");
   pragma Import (Java, GetHoldCount, "getHoldCount");
   pragma Import (Java, IsHeldByCurrentThread, "isHeldByCurrentThread");
   pragma Import (Java, IsLocked, "isLocked");
   pragma Import (Java, IsFair, "isFair");
   pragma Import (Java, GetOwner, "getOwner");
   pragma Import (Java, HasQueuedThreads, "hasQueuedThreads");
   pragma Import (Java, HasQueuedThread, "hasQueuedThread");
   pragma Import (Java, GetQueueLength, "getQueueLength");
   pragma Import (Java, GetQueuedThreads, "getQueuedThreads");
   pragma Import (Java, HasWaiters, "hasWaiters");
   pragma Import (Java, GetWaitQueueLength, "getWaitQueueLength");
   pragma Import (Java, GetWaitingThreads, "getWaitingThreads");
   pragma Import (Java, ToString, "toString");

end Java.Util.Concurrent.Locks.ReentrantLock;
pragma Import (Java, Java.Util.Concurrent.Locks.ReentrantLock, "java.util.concurrent.locks.ReentrantLock");
pragma Extensions_Allowed (Off);
