pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Concurrent.Locks.Condition;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.Concurrent.Locks.Lock;

package Java.Util.Concurrent.Locks.ReentrantReadWriteLock.WriteLock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Lock_I : Java.Util.Concurrent.Locks.Lock.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_WriteLock (P1_ReentrantReadWriteLock : access Standard.Java.Util.Concurrent.Locks.ReentrantReadWriteLock.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Lock (This : access Typ);

   procedure LockInterruptibly (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   function TryLock (This : access Typ)
                     return Java.Boolean;

   function TryLock (This : access Typ;
                     P1_Long : Java.Long;
                     P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                     return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   procedure Unlock (This : access Typ);

   function NewCondition (This : access Typ)
                          return access Java.Util.Concurrent.Locks.Condition.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IsHeldByCurrentThread (This : access Typ)
                                   return Java.Boolean;

   function GetHoldCount (This : access Typ)
                          return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_WriteLock);
   pragma Import (Java, Lock, "lock");
   pragma Import (Java, LockInterruptibly, "lockInterruptibly");
   pragma Import (Java, TryLock, "tryLock");
   pragma Import (Java, Unlock, "unlock");
   pragma Import (Java, NewCondition, "newCondition");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IsHeldByCurrentThread, "isHeldByCurrentThread");
   pragma Import (Java, GetHoldCount, "getHoldCount");

end Java.Util.Concurrent.Locks.ReentrantReadWriteLock.WriteLock;
pragma Import (Java, Java.Util.Concurrent.Locks.ReentrantReadWriteLock.WriteLock, "java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock");
pragma Extensions_Allowed (Off);
