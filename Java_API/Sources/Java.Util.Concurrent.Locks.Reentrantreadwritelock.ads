pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Thread;
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.Locks.Condition;
limited with Java.Util.Concurrent.Locks.Lock;
limited with Java.Util.Concurrent.Locks.ReentrantReadWriteLock.ReadLock;
limited with Java.Util.Concurrent.Locks.ReentrantReadWriteLock.WriteLock;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.Concurrent.Locks.ReadWriteLock;

package Java.Util.Concurrent.Locks.ReentrantReadWriteLock is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ReadWriteLock_I : Java.Util.Concurrent.Locks.ReadWriteLock.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ReentrantReadWriteLock (This : Ref := null)
                                        return Ref;

   function New_ReentrantReadWriteLock (P1_Boolean : Java.Boolean; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetWriteLock (This : access Typ)
                          return access Java.Util.Concurrent.Locks.ReentrantReadWriteLock.WriteLock.Typ'Class;

   function GetReadLock (This : access Typ)
                         return access Java.Util.Concurrent.Locks.ReentrantReadWriteLock.ReadLock.Typ'Class;

   --  final
   function IsFair (This : access Typ)
                    return Java.Boolean;

   --  protected
   function GetOwner (This : access Typ)
                      return access Java.Lang.Thread.Typ'Class;

   function GetReadLockCount (This : access Typ)
                              return Java.Int;

   function IsWriteLocked (This : access Typ)
                           return Java.Boolean;

   function IsWriteLockedByCurrentThread (This : access Typ)
                                          return Java.Boolean;

   function GetWriteHoldCount (This : access Typ)
                               return Java.Int;

   function GetReadHoldCount (This : access Typ)
                              return Java.Int;

   --  protected
   function GetQueuedWriterThreads (This : access Typ)
                                    return access Java.Util.Collection.Typ'Class;

   --  protected
   function GetQueuedReaderThreads (This : access Typ)
                                    return access Java.Util.Collection.Typ'Class;

   --  final
   function HasQueuedThreads (This : access Typ)
                              return Java.Boolean;

   --  final
   function HasQueuedThread (This : access Typ;
                             P1_Thread : access Standard.Java.Lang.Thread.Typ'Class)
                             return Java.Boolean;

   --  final
   function GetQueueLength (This : access Typ)
                            return Java.Int;

   --  protected
   function GetQueuedThreads (This : access Typ)
                              return access Java.Util.Collection.Typ'Class;

   function HasWaiters (This : access Typ;
                        P1_Condition : access Standard.Java.Util.Concurrent.Locks.Condition.Typ'Class)
                        return Java.Boolean;

   function GetWaitQueueLength (This : access Typ;
                                P1_Condition : access Standard.Java.Util.Concurrent.Locks.Condition.Typ'Class)
                                return Java.Int;

   --  protected
   function GetWaitingThreads (This : access Typ;
                               P1_Condition : access Standard.Java.Util.Concurrent.Locks.Condition.Typ'Class)
                               return access Java.Util.Collection.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetWriteLock (This : access Typ)
                          return access Java.Util.Concurrent.Locks.Lock.Typ'Class;

   function GetReadLock (This : access Typ)
                         return access Java.Util.Concurrent.Locks.Lock.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ReentrantReadWriteLock);
   pragma Import (Java, GetWriteLock, "writeLock");
   pragma Import (Java, GetReadLock, "readLock");
   pragma Import (Java, IsFair, "isFair");
   pragma Import (Java, GetOwner, "getOwner");
   pragma Import (Java, GetReadLockCount, "getReadLockCount");
   pragma Import (Java, IsWriteLocked, "isWriteLocked");
   pragma Import (Java, IsWriteLockedByCurrentThread, "isWriteLockedByCurrentThread");
   pragma Import (Java, GetWriteHoldCount, "getWriteHoldCount");
   pragma Import (Java, GetReadHoldCount, "getReadHoldCount");
   pragma Import (Java, GetQueuedWriterThreads, "getQueuedWriterThreads");
   pragma Import (Java, GetQueuedReaderThreads, "getQueuedReaderThreads");
   pragma Import (Java, HasQueuedThreads, "hasQueuedThreads");
   pragma Import (Java, HasQueuedThread, "hasQueuedThread");
   pragma Import (Java, GetQueueLength, "getQueueLength");
   pragma Import (Java, GetQueuedThreads, "getQueuedThreads");
   pragma Import (Java, HasWaiters, "hasWaiters");
   pragma Import (Java, GetWaitQueueLength, "getWaitQueueLength");
   pragma Import (Java, GetWaitingThreads, "getWaitingThreads");
   pragma Import (Java, ToString, "toString");

end Java.Util.Concurrent.Locks.ReentrantReadWriteLock;
pragma Import (Java, Java.Util.Concurrent.Locks.ReentrantReadWriteLock, "java.util.concurrent.locks.ReentrantReadWriteLock");
pragma Extensions_Allowed (Off);
