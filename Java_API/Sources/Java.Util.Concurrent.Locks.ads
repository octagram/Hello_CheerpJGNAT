pragma Extensions_Allowed (On);
package Java.Util.Concurrent.Locks is
   pragma Preelaborate;
end Java.Util.Concurrent.Locks;
pragma Import (Java, Java.Util.Concurrent.Locks, "java.util.concurrent.locks");
pragma Extensions_Allowed (Off);
