pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Lang.Runnable;
with Java.Util.Concurrent.Future;

package Java.Util.Concurrent.RunnableFuture is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Runnable_I : Java.Lang.Runnable.Ref;
            Future_I : Java.Util.Concurrent.Future.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Run (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Run, "run");

end Java.Util.Concurrent.RunnableFuture;
pragma Import (Java, Java.Util.Concurrent.RunnableFuture, "java.util.concurrent.RunnableFuture");
pragma Extensions_Allowed (Off);
