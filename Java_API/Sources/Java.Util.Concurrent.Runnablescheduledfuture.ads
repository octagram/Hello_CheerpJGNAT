pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.Concurrent.RunnableFuture;
with Java.Util.Concurrent.ScheduledFuture;

package Java.Util.Concurrent.RunnableScheduledFuture is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RunnableFuture_I : Java.Util.Concurrent.RunnableFuture.Ref;
            ScheduledFuture_I : Java.Util.Concurrent.ScheduledFuture.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsPeriodic (This : access Typ)
                        return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsPeriodic, "isPeriodic");

end Java.Util.Concurrent.RunnableScheduledFuture;
pragma Import (Java, Java.Util.Concurrent.RunnableScheduledFuture, "java.util.concurrent.RunnableScheduledFuture");
pragma Extensions_Allowed (Off);
