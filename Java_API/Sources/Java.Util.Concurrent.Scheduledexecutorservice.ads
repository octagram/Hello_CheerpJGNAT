pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.ScheduledFuture;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Lang.Object;
with Java.Util.Concurrent.ExecutorService;

package Java.Util.Concurrent.ScheduledExecutorService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ExecutorService_I : Java.Util.Concurrent.ExecutorService.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Schedule (This : access Typ;
                      P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                      P2_Long : Java.Long;
                      P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return access Java.Util.Concurrent.ScheduledFuture.Typ'Class is abstract;

   function Schedule (This : access Typ;
                      P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class;
                      P2_Long : Java.Long;
                      P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return access Java.Util.Concurrent.ScheduledFuture.Typ'Class is abstract;

   function ScheduleAtFixedRate (This : access Typ;
                                 P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                                 P2_Long : Java.Long;
                                 P3_Long : Java.Long;
                                 P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                                 return access Java.Util.Concurrent.ScheduledFuture.Typ'Class is abstract;

   function ScheduleWithFixedDelay (This : access Typ;
                                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                                    P2_Long : Java.Long;
                                    P3_Long : Java.Long;
                                    P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                                    return access Java.Util.Concurrent.ScheduledFuture.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Schedule, "schedule");
   pragma Export (Java, ScheduleAtFixedRate, "scheduleAtFixedRate");
   pragma Export (Java, ScheduleWithFixedDelay, "scheduleWithFixedDelay");

end Java.Util.Concurrent.ScheduledExecutorService;
pragma Import (Java, Java.Util.Concurrent.ScheduledExecutorService, "java.util.concurrent.ScheduledExecutorService");
pragma Extensions_Allowed (Off);
