pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Util.Concurrent.BlockingQueue;
limited with Java.Util.Concurrent.Callable;
limited with Java.Util.Concurrent.Future;
limited with Java.Util.Concurrent.RejectedExecutionHandler;
limited with Java.Util.Concurrent.RunnableScheduledFuture;
limited with Java.Util.Concurrent.ScheduledFuture;
limited with Java.Util.Concurrent.ThreadFactory;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.List;
with Java.Lang.Object;
with Java.Util.Concurrent.ExecutorService;
with Java.Util.Concurrent.ScheduledExecutorService;
with Java.Util.Concurrent.ThreadPoolExecutor;

package Java.Util.Concurrent.ScheduledThreadPoolExecutor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ExecutorService_I : Java.Util.Concurrent.ExecutorService.Ref;
            ScheduledExecutorService_I : Java.Util.Concurrent.ScheduledExecutorService.Ref)
    is new Java.Util.Concurrent.ThreadPoolExecutor.Typ(ExecutorService_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Remove (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class)
                    return Java.Boolean;

   --  protected
   function DecorateTask (This : access Typ;
                          P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                          P2_RunnableScheduledFuture : access Standard.Java.Util.Concurrent.RunnableScheduledFuture.Typ'Class)
                          return access Java.Util.Concurrent.RunnableScheduledFuture.Typ'Class;

   --  protected
   function DecorateTask (This : access Typ;
                          P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class;
                          P2_RunnableScheduledFuture : access Standard.Java.Util.Concurrent.RunnableScheduledFuture.Typ'Class)
                          return access Java.Util.Concurrent.RunnableScheduledFuture.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ScheduledThreadPoolExecutor (P1_Int : Java.Int; 
                                             This : Ref := null)
                                             return Ref;

   function New_ScheduledThreadPoolExecutor (P1_Int : Java.Int;
                                             P2_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   function New_ScheduledThreadPoolExecutor (P1_Int : Java.Int;
                                             P2_RejectedExecutionHandler : access Standard.Java.Util.Concurrent.RejectedExecutionHandler.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   function New_ScheduledThreadPoolExecutor (P1_Int : Java.Int;
                                             P2_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class;
                                             P3_RejectedExecutionHandler : access Standard.Java.Util.Concurrent.RejectedExecutionHandler.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   function Schedule (This : access Typ;
                      P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                      P2_Long : Java.Long;
                      P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return access Java.Util.Concurrent.ScheduledFuture.Typ'Class;

   function Schedule (This : access Typ;
                      P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class;
                      P2_Long : Java.Long;
                      P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                      return access Java.Util.Concurrent.ScheduledFuture.Typ'Class;

   function ScheduleAtFixedRate (This : access Typ;
                                 P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                                 P2_Long : Java.Long;
                                 P3_Long : Java.Long;
                                 P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                                 return access Java.Util.Concurrent.ScheduledFuture.Typ'Class;

   function ScheduleWithFixedDelay (This : access Typ;
                                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                                    P2_Long : Java.Long;
                                    P3_Long : Java.Long;
                                    P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                                    return access Java.Util.Concurrent.ScheduledFuture.Typ'Class;

   procedure Execute (This : access Typ;
                      P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   function Submit (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   function Submit (This : access Typ;
                    P1_Callable : access Standard.Java.Util.Concurrent.Callable.Typ'Class)
                    return access Java.Util.Concurrent.Future.Typ'Class;

   procedure SetContinueExistingPeriodicTasksAfterShutdownPolicy (This : access Typ;
                                                                  P1_Boolean : Java.Boolean);

   function GetContinueExistingPeriodicTasksAfterShutdownPolicy (This : access Typ)
                                                                 return Java.Boolean;

   procedure SetExecuteExistingDelayedTasksAfterShutdownPolicy (This : access Typ;
                                                                P1_Boolean : Java.Boolean);

   function GetExecuteExistingDelayedTasksAfterShutdownPolicy (This : access Typ)
                                                               return Java.Boolean;

   procedure Shutdown (This : access Typ);

   function ShutdownNow (This : access Typ)
                         return access Java.Util.List.Typ'Class;

   function GetQueue (This : access Typ)
                      return access Java.Util.Concurrent.BlockingQueue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, DecorateTask, "decorateTask");
   pragma Java_Constructor (New_ScheduledThreadPoolExecutor);
   pragma Import (Java, Schedule, "schedule");
   pragma Import (Java, ScheduleAtFixedRate, "scheduleAtFixedRate");
   pragma Import (Java, ScheduleWithFixedDelay, "scheduleWithFixedDelay");
   pragma Import (Java, Execute, "execute");
   pragma Import (Java, Submit, "submit");
   pragma Import (Java, SetContinueExistingPeriodicTasksAfterShutdownPolicy, "setContinueExistingPeriodicTasksAfterShutdownPolicy");
   pragma Import (Java, GetContinueExistingPeriodicTasksAfterShutdownPolicy, "getContinueExistingPeriodicTasksAfterShutdownPolicy");
   pragma Import (Java, SetExecuteExistingDelayedTasksAfterShutdownPolicy, "setExecuteExistingDelayedTasksAfterShutdownPolicy");
   pragma Import (Java, GetExecuteExistingDelayedTasksAfterShutdownPolicy, "getExecuteExistingDelayedTasksAfterShutdownPolicy");
   pragma Import (Java, Shutdown, "shutdown");
   pragma Import (Java, ShutdownNow, "shutdownNow");
   pragma Import (Java, GetQueue, "getQueue");

end Java.Util.Concurrent.ScheduledThreadPoolExecutor;
pragma Import (Java, Java.Util.Concurrent.ScheduledThreadPoolExecutor, "java.util.concurrent.ScheduledThreadPoolExecutor");
pragma Extensions_Allowed (Off);
