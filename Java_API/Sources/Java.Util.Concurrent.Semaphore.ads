pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Java.Util.Concurrent.TimeUnit;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Concurrent.Semaphore is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Semaphore (P1_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_Semaphore (P1_Int : Java.Int;
                           P2_Boolean : Java.Boolean; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Acquire (This : access Typ);
   --  can raise Java.Lang.InterruptedException.Except

   procedure AcquireUninterruptibly (This : access Typ);

   function TryAcquire (This : access Typ)
                        return Java.Boolean;

   function TryAcquire (This : access Typ;
                        P1_Long : Java.Long;
                        P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                        return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   procedure Release (This : access Typ);

   procedure Acquire (This : access Typ;
                      P1_Int : Java.Int);
   --  can raise Java.Lang.InterruptedException.Except

   procedure AcquireUninterruptibly (This : access Typ;
                                     P1_Int : Java.Int);

   function TryAcquire (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   function TryAcquire (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Long : Java.Long;
                        P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                        return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   procedure Release (This : access Typ;
                      P1_Int : Java.Int);

   function AvailablePermits (This : access Typ)
                              return Java.Int;

   function DrainPermits (This : access Typ)
                          return Java.Int;

   --  protected
   procedure ReducePermits (This : access Typ;
                            P1_Int : Java.Int);

   function IsFair (This : access Typ)
                    return Java.Boolean;

   --  final
   function HasQueuedThreads (This : access Typ)
                              return Java.Boolean;

   --  final
   function GetQueueLength (This : access Typ)
                            return Java.Int;

   --  protected
   function GetQueuedThreads (This : access Typ)
                              return access Java.Util.Collection.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Semaphore);
   pragma Import (Java, Acquire, "acquire");
   pragma Import (Java, AcquireUninterruptibly, "acquireUninterruptibly");
   pragma Import (Java, TryAcquire, "tryAcquire");
   pragma Import (Java, Release, "release");
   pragma Import (Java, AvailablePermits, "availablePermits");
   pragma Import (Java, DrainPermits, "drainPermits");
   pragma Import (Java, ReducePermits, "reducePermits");
   pragma Import (Java, IsFair, "isFair");
   pragma Import (Java, HasQueuedThreads, "hasQueuedThreads");
   pragma Import (Java, GetQueueLength, "getQueueLength");
   pragma Import (Java, GetQueuedThreads, "getQueuedThreads");
   pragma Import (Java, ToString, "toString");

end Java.Util.Concurrent.Semaphore;
pragma Import (Java, Java.Util.Concurrent.Semaphore, "java.util.concurrent.Semaphore");
pragma Extensions_Allowed (Off);
