pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.AbstractQueue;
with Java.Util.Collection;
with Java.Util.Concurrent.BlockingQueue;
with Java.Util.Queue;

package Java.Util.Concurrent.SynchronousQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Queue_I : Java.Util.Queue.Ref;
            BlockingQueue_I : Java.Util.Concurrent.BlockingQueue.Ref)
    is new Java.Util.AbstractQueue.Typ(Collection_I,
                                       Queue_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SynchronousQueue (This : Ref := null)
                                  return Ref;

   function New_SynchronousQueue (P1_Boolean : Java.Boolean; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Put (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.InterruptedException.Except

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P2_Long : Java.Long;
                   P3_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   function Take (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ;
                  P1_Long : Java.Long;
                  P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Size (This : access Typ)
                  return Java.Int;

   function RemainingCapacity (This : access Typ)
                               return Java.Int;

   procedure Clear (This : access Typ);

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ContainsAll (This : access Typ;
                         P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                         return Java.Boolean;

   function RemoveAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   function RetainAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                     return Java.Int;

   function DrainTo (This : access Typ;
                     P1_Collection : access Standard.Java.Util.Collection.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SynchronousQueue);
   pragma Import (Java, Put, "put");
   pragma Import (Java, Offer, "offer");
   pragma Import (Java, Take, "take");
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Size, "size");
   pragma Import (Java, RemainingCapacity, "remainingCapacity");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ContainsAll, "containsAll");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, RetainAll, "retainAll");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, DrainTo, "drainTo");

end Java.Util.Concurrent.SynchronousQueue;
pragma Import (Java, Java.Util.Concurrent.SynchronousQueue, "java.util.concurrent.SynchronousQueue");
pragma Extensions_Allowed (Off);
