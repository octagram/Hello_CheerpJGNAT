pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
with Java.Lang.Object;
with Java.Util.Concurrent.RejectedExecutionHandler;

package Java.Util.Concurrent.ThreadPoolExecutor.DiscardPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RejectedExecutionHandler_I : Java.Util.Concurrent.RejectedExecutionHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DiscardPolicy (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure RejectedExecution (This : access Typ;
                                P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                                P2_ThreadPoolExecutor : access Standard.Java.Util.Concurrent.ThreadPoolExecutor.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DiscardPolicy);
   pragma Import (Java, RejectedExecution, "rejectedExecution");

end Java.Util.Concurrent.ThreadPoolExecutor.DiscardPolicy;
pragma Import (Java, Java.Util.Concurrent.ThreadPoolExecutor.DiscardPolicy, "java.util.concurrent.ThreadPoolExecutor$DiscardPolicy");
pragma Extensions_Allowed (Off);
