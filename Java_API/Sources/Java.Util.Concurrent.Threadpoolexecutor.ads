pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Lang.Thread;
limited with Java.Lang.Throwable;
limited with Java.Util.Concurrent.BlockingQueue;
limited with Java.Util.Concurrent.RejectedExecutionHandler;
limited with Java.Util.Concurrent.ThreadFactory;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.List;
with Java.Lang.Object;
with Java.Util.Concurrent.AbstractExecutorService;
with Java.Util.Concurrent.ExecutorService;

package Java.Util.Concurrent.ThreadPoolExecutor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ExecutorService_I : Java.Util.Concurrent.ExecutorService.Ref)
    is new Java.Util.Concurrent.AbstractExecutorService.Typ(ExecutorService_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ThreadPoolExecutor (P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Long : Java.Long;
                                    P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class;
                                    P5_BlockingQueue : access Standard.Java.Util.Concurrent.BlockingQueue.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_ThreadPoolExecutor (P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Long : Java.Long;
                                    P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class;
                                    P5_BlockingQueue : access Standard.Java.Util.Concurrent.BlockingQueue.Typ'Class;
                                    P6_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_ThreadPoolExecutor (P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Long : Java.Long;
                                    P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class;
                                    P5_BlockingQueue : access Standard.Java.Util.Concurrent.BlockingQueue.Typ'Class;
                                    P6_RejectedExecutionHandler : access Standard.Java.Util.Concurrent.RejectedExecutionHandler.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_ThreadPoolExecutor (P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Long : Java.Long;
                                    P4_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class;
                                    P5_BlockingQueue : access Standard.Java.Util.Concurrent.BlockingQueue.Typ'Class;
                                    P6_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class;
                                    P7_RejectedExecutionHandler : access Standard.Java.Util.Concurrent.RejectedExecutionHandler.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Execute (This : access Typ;
                      P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);

   procedure Shutdown (This : access Typ);

   function ShutdownNow (This : access Typ)
                         return access Java.Util.List.Typ'Class;

   function IsShutdown (This : access Typ)
                        return Java.Boolean;

   function IsTerminating (This : access Typ)
                           return Java.Boolean;

   function IsTerminated (This : access Typ)
                          return Java.Boolean;

   function AwaitTermination (This : access Typ;
                              P1_Long : Java.Long;
                              P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                              return Java.Boolean;
   --  can raise Java.Lang.InterruptedException.Except

   --  protected
   procedure Finalize (This : access Typ);

   procedure SetThreadFactory (This : access Typ;
                               P1_ThreadFactory : access Standard.Java.Util.Concurrent.ThreadFactory.Typ'Class);

   function GetThreadFactory (This : access Typ)
                              return access Java.Util.Concurrent.ThreadFactory.Typ'Class;

   procedure SetRejectedExecutionHandler (This : access Typ;
                                          P1_RejectedExecutionHandler : access Standard.Java.Util.Concurrent.RejectedExecutionHandler.Typ'Class);

   function GetRejectedExecutionHandler (This : access Typ)
                                         return access Java.Util.Concurrent.RejectedExecutionHandler.Typ'Class;

   procedure SetCorePoolSize (This : access Typ;
                              P1_Int : Java.Int);

   function GetCorePoolSize (This : access Typ)
                             return Java.Int;

   function PrestartCoreThread (This : access Typ)
                                return Java.Boolean;

   function PrestartAllCoreThreads (This : access Typ)
                                    return Java.Int;

   function AllowsCoreThreadTimeOut (This : access Typ)
                                     return Java.Boolean;

   procedure AllowCoreThreadTimeOut (This : access Typ;
                                     P1_Boolean : Java.Boolean);

   procedure SetMaximumPoolSize (This : access Typ;
                                 P1_Int : Java.Int);

   function GetMaximumPoolSize (This : access Typ)
                                return Java.Int;

   procedure SetKeepAliveTime (This : access Typ;
                               P1_Long : Java.Long;
                               P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class);

   function GetKeepAliveTime (This : access Typ;
                              P1_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                              return Java.Long;

   function GetQueue (This : access Typ)
                      return access Java.Util.Concurrent.BlockingQueue.Typ'Class;

   function Remove (This : access Typ;
                    P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class)
                    return Java.Boolean;

   procedure Purge (This : access Typ);

   function GetPoolSize (This : access Typ)
                         return Java.Int;

   function GetActiveCount (This : access Typ)
                            return Java.Int;

   function GetLargestPoolSize (This : access Typ)
                                return Java.Int;

   function GetTaskCount (This : access Typ)
                          return Java.Long;

   function GetCompletedTaskCount (This : access Typ)
                                   return Java.Long;

   --  protected
   procedure BeforeExecute (This : access Typ;
                            P1_Thread : access Standard.Java.Lang.Thread.Typ'Class;
                            P2_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);

   --  protected
   procedure AfterExecute (This : access Typ;
                           P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class;
                           P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   --  protected
   procedure Terminated (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ThreadPoolExecutor);
   pragma Import (Java, Execute, "execute");
   pragma Import (Java, Shutdown, "shutdown");
   pragma Import (Java, ShutdownNow, "shutdownNow");
   pragma Import (Java, IsShutdown, "isShutdown");
   pragma Import (Java, IsTerminating, "isTerminating");
   pragma Import (Java, IsTerminated, "isTerminated");
   pragma Import (Java, AwaitTermination, "awaitTermination");
   pragma Import (Java, Finalize, "finalize");
   pragma Import (Java, SetThreadFactory, "setThreadFactory");
   pragma Import (Java, GetThreadFactory, "getThreadFactory");
   pragma Import (Java, SetRejectedExecutionHandler, "setRejectedExecutionHandler");
   pragma Import (Java, GetRejectedExecutionHandler, "getRejectedExecutionHandler");
   pragma Import (Java, SetCorePoolSize, "setCorePoolSize");
   pragma Import (Java, GetCorePoolSize, "getCorePoolSize");
   pragma Import (Java, PrestartCoreThread, "prestartCoreThread");
   pragma Import (Java, PrestartAllCoreThreads, "prestartAllCoreThreads");
   pragma Import (Java, AllowsCoreThreadTimeOut, "allowsCoreThreadTimeOut");
   pragma Import (Java, AllowCoreThreadTimeOut, "allowCoreThreadTimeOut");
   pragma Import (Java, SetMaximumPoolSize, "setMaximumPoolSize");
   pragma Import (Java, GetMaximumPoolSize, "getMaximumPoolSize");
   pragma Import (Java, SetKeepAliveTime, "setKeepAliveTime");
   pragma Import (Java, GetKeepAliveTime, "getKeepAliveTime");
   pragma Import (Java, GetQueue, "getQueue");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Purge, "purge");
   pragma Import (Java, GetPoolSize, "getPoolSize");
   pragma Import (Java, GetActiveCount, "getActiveCount");
   pragma Import (Java, GetLargestPoolSize, "getLargestPoolSize");
   pragma Import (Java, GetTaskCount, "getTaskCount");
   pragma Import (Java, GetCompletedTaskCount, "getCompletedTaskCount");
   pragma Import (Java, BeforeExecute, "beforeExecute");
   pragma Import (Java, AfterExecute, "afterExecute");
   pragma Import (Java, Terminated, "terminated");

end Java.Util.Concurrent.ThreadPoolExecutor;
pragma Import (Java, Java.Util.Concurrent.ThreadPoolExecutor, "java.util.concurrent.ThreadPoolExecutor");
pragma Extensions_Allowed (Off);
