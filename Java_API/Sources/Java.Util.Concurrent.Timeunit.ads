pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Thread;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Java.Util.Concurrent.TimeUnit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Lang.Enum.Typ(Serializable_I,
                                       Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Util.Concurrent.TimeUnit.Typ'Class;

   function Convert (This : access Typ;
                     P1_Long : Java.Long;
                     P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                     return Java.Long;

   function ToNanos (This : access Typ;
                     P1_Long : Java.Long)
                     return Java.Long;

   function ToMicros (This : access Typ;
                      P1_Long : Java.Long)
                      return Java.Long;

   function ToMillis (This : access Typ;
                      P1_Long : Java.Long)
                      return Java.Long;

   function ToSeconds (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Long;

   function ToMinutes (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Long;

   function ToHours (This : access Typ;
                     P1_Long : Java.Long)
                     return Java.Long;

   function ToDays (This : access Typ;
                    P1_Long : Java.Long)
                    return Java.Long;

   procedure TimedWait (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   procedure TimedJoin (This : access Typ;
                        P1_Thread : access Standard.Java.Lang.Thread.Typ'Class;
                        P2_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   procedure Sleep (This : access Typ;
                    P1_Long : Java.Long);
   --  can raise Java.Lang.InterruptedException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NANOSECONDS : access Java.Util.Concurrent.TimeUnit.Typ'Class;

   --  final
   MICROSECONDS : access Java.Util.Concurrent.TimeUnit.Typ'Class;

   --  final
   MILLISECONDS : access Java.Util.Concurrent.TimeUnit.Typ'Class;

   --  final
   SECONDS : access Java.Util.Concurrent.TimeUnit.Typ'Class;

   --  final
   MINUTES : access Java.Util.Concurrent.TimeUnit.Typ'Class;

   --  final
   HOURS : access Java.Util.Concurrent.TimeUnit.Typ'Class;

   --  final
   DAYS : access Java.Util.Concurrent.TimeUnit.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, Convert, "convert");
   pragma Import (Java, ToNanos, "toNanos");
   pragma Import (Java, ToMicros, "toMicros");
   pragma Import (Java, ToMillis, "toMillis");
   pragma Import (Java, ToSeconds, "toSeconds");
   pragma Import (Java, ToMinutes, "toMinutes");
   pragma Import (Java, ToHours, "toHours");
   pragma Import (Java, ToDays, "toDays");
   pragma Import (Java, TimedWait, "timedWait");
   pragma Import (Java, TimedJoin, "timedJoin");
   pragma Import (Java, Sleep, "sleep");
   pragma Import (Java, NANOSECONDS, "NANOSECONDS");
   pragma Import (Java, MICROSECONDS, "MICROSECONDS");
   pragma Import (Java, MILLISECONDS, "MILLISECONDS");
   pragma Import (Java, SECONDS, "SECONDS");
   pragma Import (Java, MINUTES, "MINUTES");
   pragma Import (Java, HOURS, "HOURS");
   pragma Import (Java, DAYS, "DAYS");

end Java.Util.Concurrent.TimeUnit;
pragma Import (Java, Java.Util.Concurrent.TimeUnit, "java.util.concurrent.TimeUnit");
pragma Extensions_Allowed (Off);
