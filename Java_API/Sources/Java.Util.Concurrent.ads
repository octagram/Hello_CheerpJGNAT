pragma Extensions_Allowed (On);
package Java.Util.Concurrent is
   pragma Preelaborate;
end Java.Util.Concurrent;
pragma Import (Java, Java.Util.Concurrent, "java.util.concurrent");
pragma Extensions_Allowed (Off);
