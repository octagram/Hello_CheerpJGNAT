pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Currency is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Util.Currency.Typ'Class;

   function GetInstance (P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                         return access Java.Util.Currency.Typ'Class;

   function GetCurrencyCode (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetSymbol (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetSymbol (This : access Typ;
                       P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function GetDefaultFractionDigits (This : access Typ)
                                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetCurrencyCode, "getCurrencyCode");
   pragma Import (Java, GetSymbol, "getSymbol");
   pragma Import (Java, GetDefaultFractionDigits, "getDefaultFractionDigits");
   pragma Import (Java, ToString, "toString");

end Java.Util.Currency;
pragma Import (Java, Java.Util.Currency, "java.util.Currency");
pragma Extensions_Allowed (Off);
