pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Util.Date is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Date (This : Ref := null)
                      return Ref;

   function New_Date (P1_Long : Java.Long; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetTime (This : access Typ)
                     return Java.Long;

   procedure SetTime (This : access Typ;
                      P1_Long : Java.Long);

   function Before (This : access Typ;
                    P1_Date : access Standard.Java.Util.Date.Typ'Class)
                    return Java.Boolean;

   function After (This : access Typ;
                   P1_Date : access Standard.Java.Util.Date.Typ'Class)
                   return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_Date : access Standard.Java.Util.Date.Typ'Class)
                       return Java.Int;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Date);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetTime, "getTime");
   pragma Import (Java, SetTime, "setTime");
   pragma Import (Java, Before, "before");
   pragma Import (Java, After, "after");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Java.Util.Date;
pragma Import (Java, Java.Util.Date, "java.util.Date");
pragma Extensions_Allowed (Off);
