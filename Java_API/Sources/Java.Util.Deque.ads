pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Lang.Object;
with Java.Util.Queue;

package Java.Util.Deque is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Queue_I : Java.Util.Queue.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddFirst (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure AddLast (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function OfferFirst (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean is abstract;

   function OfferLast (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean is abstract;

   function RemoveFirst (This : access Typ)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function RemoveLast (This : access Typ)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function PollFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function PollLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function GetFirst (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function GetLast (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function PeekFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function PeekLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function RemoveFirstOccurrence (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return Java.Boolean is abstract;

   function RemoveLastOccurrence (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return Java.Boolean is abstract;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean is abstract;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean is abstract;

   function Remove (This : access Typ)
                    return access Java.Lang.Object.Typ'Class is abstract;

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;

   function Element (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;

   procedure Push (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function Pop (This : access Typ)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean is abstract;

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class is abstract;

   function DescendingIterator (This : access Typ)
                                return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddFirst, "addFirst");
   pragma Export (Java, AddLast, "addLast");
   pragma Export (Java, OfferFirst, "offerFirst");
   pragma Export (Java, OfferLast, "offerLast");
   pragma Export (Java, RemoveFirst, "removeFirst");
   pragma Export (Java, RemoveLast, "removeLast");
   pragma Export (Java, PollFirst, "pollFirst");
   pragma Export (Java, PollLast, "pollLast");
   pragma Export (Java, GetFirst, "getFirst");
   pragma Export (Java, GetLast, "getLast");
   pragma Export (Java, PeekFirst, "peekFirst");
   pragma Export (Java, PeekLast, "peekLast");
   pragma Export (Java, RemoveFirstOccurrence, "removeFirstOccurrence");
   pragma Export (Java, RemoveLastOccurrence, "removeLastOccurrence");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Offer, "offer");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Poll, "poll");
   pragma Export (Java, Element, "element");
   pragma Export (Java, Peek, "peek");
   pragma Export (Java, Push, "push");
   pragma Export (Java, Pop, "pop");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, Size, "size");
   pragma Export (Java, Iterator, "iterator");
   pragma Export (Java, DescendingIterator, "descendingIterator");

end Java.Util.Deque;
pragma Import (Java, Java.Util.Deque, "java.util.Deque");
pragma Extensions_Allowed (Off);
