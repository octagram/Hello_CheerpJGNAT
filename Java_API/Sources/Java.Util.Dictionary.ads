pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Java.Util.Dictionary is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Dictionary (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function IsEmpty (This : access Typ)
                     return Java.Boolean is abstract;

   function Keys (This : access Typ)
                  return access Java.Util.Enumeration.Typ'Class is abstract;

   function Elements (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Dictionary);
   pragma Export (Java, Size, "size");
   pragma Export (Java, IsEmpty, "isEmpty");
   pragma Export (Java, Keys, "keys");
   pragma Export (Java, Elements, "elements");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Put, "put");
   pragma Export (Java, Remove, "remove");

end Java.Util.Dictionary;
pragma Import (Java, Java.Util.Dictionary, "java.util.Dictionary");
pragma Extensions_Allowed (Off);
