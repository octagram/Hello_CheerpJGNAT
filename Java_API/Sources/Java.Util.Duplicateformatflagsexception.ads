pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.IllegalFormatException;

package Java.Util.DuplicateFormatFlagsException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.IllegalFormatException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DuplicateFormatFlagsException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFlags (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.util.DuplicateFormatFlagsException");
   pragma Java_Constructor (New_DuplicateFormatFlagsException);
   pragma Import (Java, GetFlags, "getFlags");
   pragma Import (Java, GetMessage, "getMessage");

end Java.Util.DuplicateFormatFlagsException;
pragma Import (Java, Java.Util.DuplicateFormatFlagsException, "java.util.DuplicateFormatFlagsException");
pragma Extensions_Allowed (Off);
