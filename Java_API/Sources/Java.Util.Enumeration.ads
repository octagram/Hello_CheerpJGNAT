pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.Enumeration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function HasMoreElements (This : access Typ)
                             return Java.Boolean is abstract;

   function NextElement (This : access Typ)
                         return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, HasMoreElements, "hasMoreElements");
   pragma Export (Java, NextElement, "nextElement");

end Java.Util.Enumeration;
pragma Import (Java, Java.Util.Enumeration, "java.util.Enumeration");
pragma Extensions_Allowed (Off);
