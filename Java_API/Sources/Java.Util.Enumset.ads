pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.Enum;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractSet;
with Java.Util.Collection;
with Java.Util.Set;

package Java.Util.EnumSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Set_I : Java.Util.Set.Ref)
    is abstract new Java.Util.AbstractSet.Typ(Collection_I,
                                              Set_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NoneOf (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return access Java.Util.EnumSet.Typ'Class;

   function AllOf (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                   return access Java.Util.EnumSet.Typ'Class;

   function CopyOf (P1_EnumSet : access Standard.Java.Util.EnumSet.Typ'Class)
                    return access Java.Util.EnumSet.Typ'Class;

   function CopyOf (P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return access Java.Util.EnumSet.Typ'Class;

   function ComplementOf (P1_EnumSet : access Standard.Java.Util.EnumSet.Typ'Class)
                          return access Java.Util.EnumSet.Typ'Class;

   function of_K (P1_Enum : access Standard.Java.Lang.Enum.Typ'Class)
                  return access Java.Util.EnumSet.Typ'Class;

   function of_K (P1_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P2_Enum : access Standard.Java.Lang.Enum.Typ'Class)
                  return access Java.Util.EnumSet.Typ'Class;

   function of_K (P1_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P2_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P3_Enum : access Standard.Java.Lang.Enum.Typ'Class)
                  return access Java.Util.EnumSet.Typ'Class;

   function of_K (P1_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P2_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P3_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P4_Enum : access Standard.Java.Lang.Enum.Typ'Class)
                  return access Java.Util.EnumSet.Typ'Class;

   function of_K (P1_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P2_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P3_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P4_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P5_Enum : access Standard.Java.Lang.Enum.Typ'Class)
                  return access Java.Util.EnumSet.Typ'Class;

   function of_K (P1_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                  P2_Enum_Arr : access Java.Lang.Enum.Arr_Obj)
                  return access Java.Util.EnumSet.Typ'Class;

   function range_K (P1_Enum : access Standard.Java.Lang.Enum.Typ'Class;
                     P2_Enum : access Standard.Java.Lang.Enum.Typ'Class)
                     return access Java.Util.EnumSet.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Util.EnumSet.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NoneOf, "noneOf");
   pragma Import (Java, AllOf, "allOf");
   pragma Import (Java, CopyOf, "copyOf");
   pragma Import (Java, ComplementOf, "complementOf");
   pragma Import (Java, of_K, "of");
   pragma Import (Java, range_K, "range");
   pragma Import (Java, Clone, "clone");

end Java.Util.EnumSet;
pragma Import (Java, Java.Util.EnumSet, "java.util.EnumSet");
pragma Extensions_Allowed (Off);
