pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Util.EventListenerProxy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_EventListenerProxy (P1_EventListener : access Standard.Java.Util.EventListener.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetListener (This : access Typ)
                         return access Java.Util.EventListener.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EventListenerProxy);
   pragma Import (Java, GetListener, "getListener");

end Java.Util.EventListenerProxy;
pragma Import (Java, Java.Util.EventListenerProxy, "java.util.EventListenerProxy");
pragma Extensions_Allowed (Off);
