pragma Extensions_Allowed (On);
limited with Java.Util.Formatter;
with Java.Lang.Object;

package Java.Util.Formattable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure FormatTo (This : access Typ;
                       P1_Formatter : access Standard.Java.Util.Formatter.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, FormatTo, "formatTo");

end Java.Util.Formattable;
pragma Import (Java, Java.Util.Formattable, "java.util.Formattable");
pragma Extensions_Allowed (Off);
