pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.FormattableFlags is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LEFT_JUSTIFY : constant Java.Int;

   --  final
   UPPERCASE : constant Java.Int;

   --  final
   ALTERNATE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, LEFT_JUSTIFY, "LEFT_JUSTIFY");
   pragma Import (Java, UPPERCASE, "UPPERCASE");
   pragma Import (Java, ALTERNATE, "ALTERNATE");

end Java.Util.FormattableFlags;
pragma Import (Java, Java.Util.FormattableFlags, "java.util.FormattableFlags");
pragma Extensions_Allowed (Off);
