pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.IOException;
limited with Java.Io.OutputStream;
limited with Java.Io.PrintStream;
limited with Java.Lang.Appendable;
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Object;

package Java.Util.Formatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Formatter (This : Ref := null)
                           return Ref;

   function New_Formatter (P1_Appendable : access Standard.Java.Lang.Appendable.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Formatter (P1_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Formatter (P1_Appendable : access Standard.Java.Lang.Appendable.Typ'Class;
                           P2_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Formatter (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_Formatter (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.FileNotFoundException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   function New_Formatter (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.FileNotFoundException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   function New_Formatter (P1_File : access Standard.Java.Io.File.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_Formatter (P1_File : access Standard.Java.Io.File.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.FileNotFoundException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   function New_Formatter (P1_File : access Standard.Java.Io.File.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.FileNotFoundException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   function New_Formatter (P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Formatter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Formatter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   function New_Formatter (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Locale (This : access Typ)
                    return access Java.Util.Locale.Typ'Class;

   function out_K (This : access Typ)
                   return access Java.Lang.Appendable.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Flush (This : access Typ);

   procedure Close (This : access Typ);

   function IoException (This : access Typ)
                         return access Java.Io.IOException.Typ'Class;

   function Format (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Util.Formatter.Typ'Class;

   function Format (This : access Typ;
                    P1_Locale : access Standard.Java.Util.Locale.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Util.Formatter.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Formatter);
   pragma Import (Java, Locale, "locale");
   pragma Import (Java, out_K, "out");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");
   pragma Import (Java, IoException, "ioException");
   pragma Import (Java, Format, "format");

end Java.Util.Formatter;
pragma Import (Java, Java.Util.Formatter, "java.util.Formatter");
pragma Extensions_Allowed (Off);
