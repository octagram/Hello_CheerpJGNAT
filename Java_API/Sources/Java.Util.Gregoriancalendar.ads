pragma Extensions_Allowed (On);
limited with Java.Util.Date;
limited with Java.Util.Locale;
limited with Java.Util.TimeZone;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Comparable;
with Java.Lang.Object;
with Java.Util.Calendar;

package Java.Util.GregorianCalendar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Util.Calendar.Typ(Serializable_I,
                                  Cloneable_I,
                                  Comparable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GregorianCalendar (This : Ref := null)
                                   return Ref;

   function New_GregorianCalendar (P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_GregorianCalendar (P1_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_GregorianCalendar (P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class;
                                   P2_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_GregorianCalendar (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_GregorianCalendar (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_GregorianCalendar (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetGregorianChange (This : access Typ;
                                 P1_Date : access Standard.Java.Util.Date.Typ'Class);

   --  final
   function GetGregorianChange (This : access Typ)
                                return access Java.Util.Date.Typ'Class;

   function IsLeapYear (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int);

   procedure Roll (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Boolean : Java.Boolean);

   procedure Roll (This : access Typ;
                   P1_Int : Java.Int;
                   P2_Int : Java.Int);

   function GetMinimum (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int;

   function GetMaximum (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int;

   function GetGreatestMinimum (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Int;

   function GetLeastMaximum (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   function GetActualMinimum (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   function GetActualMaximum (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetTimeZone (This : access Typ)
                         return access Java.Util.TimeZone.Typ'Class;

   procedure SetTimeZone (This : access Typ;
                          P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class);

   --  protected
   procedure ComputeFields (This : access Typ);

   --  protected
   procedure ComputeTime (This : access Typ);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BC : constant Java.Int;

   --  final
   AD : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GregorianCalendar);
   pragma Import (Java, SetGregorianChange, "setGregorianChange");
   pragma Import (Java, GetGregorianChange, "getGregorianChange");
   pragma Import (Java, IsLeapYear, "isLeapYear");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Roll, "roll");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, GetGreatestMinimum, "getGreatestMinimum");
   pragma Import (Java, GetLeastMaximum, "getLeastMaximum");
   pragma Import (Java, GetActualMinimum, "getActualMinimum");
   pragma Import (Java, GetActualMaximum, "getActualMaximum");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetTimeZone, "getTimeZone");
   pragma Import (Java, SetTimeZone, "setTimeZone");
   pragma Import (Java, ComputeFields, "computeFields");
   pragma Import (Java, ComputeTime, "computeTime");
   pragma Import (Java, BC, "BC");
   pragma Import (Java, AD, "AD");

end Java.Util.GregorianCalendar;
pragma Import (Java, Java.Util.GregorianCalendar, "java.util.GregorianCalendar");
pragma Extensions_Allowed (Off);
