pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractSet;
with Java.Util.Collection;
with Java.Util.Set;

package Java.Util.HashSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Set_I : Java.Util.Set.Ref)
    is new Java.Util.AbstractSet.Typ(Collection_I,
                                     Set_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HashSet (This : Ref := null)
                         return Ref;

   function New_HashSet (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_HashSet (P1_Int : Java.Int;
                         P2_Float : Java.Float; 
                         This : Ref := null)
                         return Ref;

   function New_HashSet (P1_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   procedure Clear (This : access Typ);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HashSet);
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Clone, "clone");

end Java.Util.HashSet;
pragma Import (Java, Java.Util.HashSet, "java.util.HashSet");
pragma Extensions_Allowed (Off);
