pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Java.Util.Enumeration;
limited with Java.Util.Set;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Dictionary;
with Java.Util.Map;

package Java.Util.Hashtable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Java.Util.Dictionary.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Hashtable (P1_Int : Java.Int;
                           P2_Float : Java.Float; 
                           This : Ref := null)
                           return Ref;

   function New_Hashtable (P1_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_Hashtable (This : Ref := null)
                           return Ref;

   function New_Hashtable (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Size (This : access Typ)
                  return Java.Int;

   --  synchronized
   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   --  synchronized
   function Keys (This : access Typ)
                  return access Java.Util.Enumeration.Typ'Class;

   --  synchronized
   function Elements (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;

   --  synchronized
   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   --  synchronized
   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   --  synchronized
   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   --  protected
   procedure Rehash (This : access Typ);

   --  synchronized
   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   --  synchronized
   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   --  synchronized
   procedure Clear (This : access Typ);

   --  synchronized
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   --  synchronized
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  synchronized
   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Hashtable);
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Keys, "keys");
   pragma Import (Java, Elements, "elements");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Rehash, "rehash");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Java.Util.Hashtable;
pragma Import (Java, Java.Util.Hashtable, "java.util.Hashtable");
pragma Extensions_Allowed (Off);
