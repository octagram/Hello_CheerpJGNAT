pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.IllegalFormatException;

package Java.Util.IllegalFormatConversionException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.IllegalFormatException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IllegalFormatConversionException (P1_Char : Java.Char;
                                                  P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                                  This : Ref := null)
                                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetConversion (This : access Typ)
                           return Java.Char;

   function GetArgumentClass (This : access Typ)
                              return access Java.Lang.Class.Typ'Class;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.util.IllegalFormatConversionException");
   pragma Java_Constructor (New_IllegalFormatConversionException);
   pragma Import (Java, GetConversion, "getConversion");
   pragma Import (Java, GetArgumentClass, "getArgumentClass");
   pragma Import (Java, GetMessage, "getMessage");

end Java.Util.IllegalFormatConversionException;
pragma Import (Java, Java.Util.IllegalFormatConversionException, "java.util.IllegalFormatConversionException");
pragma Extensions_Allowed (Off);
