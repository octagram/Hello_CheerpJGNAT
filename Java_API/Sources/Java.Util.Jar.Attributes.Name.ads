pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Util.Jar.Attributes.Name is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Name (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MANIFEST_VERSION : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   SIGNATURE_VERSION : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   CONTENT_TYPE : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   CLASS_PATH : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   MAIN_CLASS : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   SEALED : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   EXTENSION_LIST : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   EXTENSION_NAME : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   EXTENSION_INSTALLATION : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   IMPLEMENTATION_TITLE : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   IMPLEMENTATION_VERSION : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   IMPLEMENTATION_VENDOR : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   IMPLEMENTATION_VENDOR_ID : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   IMPLEMENTATION_URL : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   SPECIFICATION_TITLE : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   SPECIFICATION_VERSION : access Java.Util.Jar.Attributes.Name.Typ'Class;

   --  final
   SPECIFICATION_VENDOR : access Java.Util.Jar.Attributes.Name.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Name);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, MANIFEST_VERSION, "MANIFEST_VERSION");
   pragma Import (Java, SIGNATURE_VERSION, "SIGNATURE_VERSION");
   pragma Import (Java, CONTENT_TYPE, "CONTENT_TYPE");
   pragma Import (Java, CLASS_PATH, "CLASS_PATH");
   pragma Import (Java, MAIN_CLASS, "MAIN_CLASS");
   pragma Import (Java, SEALED, "SEALED");
   pragma Import (Java, EXTENSION_LIST, "EXTENSION_LIST");
   pragma Import (Java, EXTENSION_NAME, "EXTENSION_NAME");
   pragma Import (Java, EXTENSION_INSTALLATION, "EXTENSION_INSTALLATION");
   pragma Import (Java, IMPLEMENTATION_TITLE, "IMPLEMENTATION_TITLE");
   pragma Import (Java, IMPLEMENTATION_VERSION, "IMPLEMENTATION_VERSION");
   pragma Import (Java, IMPLEMENTATION_VENDOR, "IMPLEMENTATION_VENDOR");
   pragma Import (Java, IMPLEMENTATION_VENDOR_ID, "IMPLEMENTATION_VENDOR_ID");
   pragma Import (Java, IMPLEMENTATION_URL, "IMPLEMENTATION_URL");
   pragma Import (Java, SPECIFICATION_TITLE, "SPECIFICATION_TITLE");
   pragma Import (Java, SPECIFICATION_VERSION, "SPECIFICATION_VERSION");
   pragma Import (Java, SPECIFICATION_VENDOR, "SPECIFICATION_VENDOR");

end Java.Util.Jar.Attributes.Name;
pragma Import (Java, Java.Util.Jar.Attributes.Name, "java.util.jar.Attributes$Name");
pragma Extensions_Allowed (Off);
