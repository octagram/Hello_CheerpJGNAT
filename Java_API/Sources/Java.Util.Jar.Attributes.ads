pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Java.Util.Jar.Attributes.Name;
limited with Java.Util.Set;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Map;

package Java.Util.Jar.Attributes is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Map : access Java.Util.Map.Typ'Class;
      pragma Import (Java, Map, "map");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Attributes (This : Ref := null)
                            return Ref;

   function New_Attributes (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_Attributes (P1_Attributes : access Standard.Java.Util.Jar.Attributes.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function GetValue (This : access Typ;
                      P1_Name : access Standard.Java.Util.Jar.Attributes.Name.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function PutValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   procedure Clear (This : access Typ);

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Attributes);
   pragma Import (Java, Get, "get");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, Put, "put");
   pragma Import (Java, PutValue, "putValue");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Clone, "clone");

end Java.Util.Jar.Attributes;
pragma Import (Java, Java.Util.Jar.Attributes, "java.util.jar.Attributes");
pragma Extensions_Allowed (Off);
