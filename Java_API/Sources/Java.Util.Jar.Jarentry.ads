pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.Certificate;
limited with Java.Security.CodeSigner;
limited with Java.Util.Jar.Attributes;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Zip.ZipEntry;

package Java.Util.Jar.JarEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Util.Zip.ZipEntry.Typ(Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JarEntry (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_JarEntry (P1_ZipEntry : access Standard.Java.Util.Zip.ZipEntry.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_JarEntry (P1_JarEntry : access Standard.Java.Util.Jar.JarEntry.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributes (This : access Typ)
                           return access Java.Util.Jar.Attributes.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetCertificates (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetCodeSigners (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JarEntry);
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetCertificates, "getCertificates");
   pragma Import (Java, GetCodeSigners, "getCodeSigners");

end Java.Util.Jar.JarEntry;
pragma Import (Java, Java.Util.Jar.JarEntry, "java.util.jar.JarEntry");
pragma Extensions_Allowed (Off);
