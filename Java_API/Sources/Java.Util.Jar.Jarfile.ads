pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Jar.JarEntry;
limited with Java.Util.Jar.Manifest;
limited with Java.Util.Zip.ZipEntry;
with Java.Lang.Object;
with Java.Util.Zip.ZipFile;

package Java.Util.Jar.JarFile is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.Zip.ZipFile.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JarFile (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JarFile (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JarFile (P1_File : access Standard.Java.Io.File.Typ'Class; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JarFile (P1_File : access Standard.Java.Io.File.Typ'Class;
                         P2_Boolean : Java.Boolean; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JarFile (P1_File : access Standard.Java.Io.File.Typ'Class;
                         P2_Boolean : Java.Boolean;
                         P3_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetManifest (This : access Typ)
                         return access Java.Util.Jar.Manifest.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetJarEntry (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Util.Jar.JarEntry.Typ'Class;

   function GetEntry (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Util.Zip.ZipEntry.Typ'Class;

   function Entries (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class;

   --  synchronized
   function GetInputStream (This : access Typ;
                            P1_ZipEntry : access Standard.Java.Util.Zip.ZipEntry.Typ'Class)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MANIFEST_NAME : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JarFile);
   pragma Import (Java, GetManifest, "getManifest");
   pragma Import (Java, GetJarEntry, "getJarEntry");
   pragma Import (Java, GetEntry, "getEntry");
   pragma Import (Java, Entries, "entries");
   pragma Import (Java, GetInputStream, "getInputStream");
   pragma Import (Java, MANIFEST_NAME, "MANIFEST_NAME");

end Java.Util.Jar.JarFile;
pragma Import (Java, Java.Util.Jar.JarFile, "java.util.jar.JarFile");
pragma Extensions_Allowed (Off);
