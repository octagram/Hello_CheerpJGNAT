pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Jar.JarEntry;
limited with Java.Util.Jar.Manifest;
limited with Java.Util.Zip.ZipEntry;
with Java.Io.Closeable;
with Java.Lang.Object;
with Java.Util.Zip.ZipInputStream;

package Java.Util.Jar.JarInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Util.Zip.ZipInputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JarInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JarInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                P2_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetManifest (This : access Typ)
                         return access Java.Util.Jar.Manifest.Typ'Class;

   function GetNextEntry (This : access Typ)
                          return access Java.Util.Zip.ZipEntry.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetNextJarEntry (This : access Typ)
                             return access Java.Util.Jar.JarEntry.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   --  protected
   function CreateZipEntry (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Util.Zip.ZipEntry.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JarInputStream);
   pragma Import (Java, GetManifest, "getManifest");
   pragma Import (Java, GetNextEntry, "getNextEntry");
   pragma Import (Java, GetNextJarEntry, "getNextJarEntry");
   pragma Import (Java, Read, "read");
   pragma Import (Java, CreateZipEntry, "createZipEntry");

end Java.Util.Jar.JarInputStream;
pragma Import (Java, Java.Util.Jar.JarInputStream, "java.util.jar.JarInputStream");
pragma Extensions_Allowed (Off);
