pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Util.Jar.Manifest;
limited with Java.Util.Zip.ZipEntry;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Object;
with Java.Util.Zip.ZipOutputStream;

package Java.Util.Jar.JarOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Util.Zip.ZipOutputStream.Typ(Closeable_I,
                                             Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JarOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                 P2_Manifest : access Standard.Java.Util.Jar.Manifest.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JarOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PutNextEntry (This : access Typ;
                           P1_ZipEntry : access Standard.Java.Util.Zip.ZipEntry.Typ'Class);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JarOutputStream);
   pragma Import (Java, PutNextEntry, "putNextEntry");

end Java.Util.Jar.JarOutputStream;
pragma Import (Java, Java.Util.Jar.JarOutputStream, "java.util.jar.JarOutputStream");
pragma Extensions_Allowed (Off);
