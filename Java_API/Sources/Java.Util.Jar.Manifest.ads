pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Util.Jar.Attributes;
limited with Java.Util.Map;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Util.Jar.Manifest is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Manifest (This : Ref := null)
                          return Ref;

   function New_Manifest (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Io.IOException.Except

   function New_Manifest (P1_Manifest : access Standard.Java.Util.Jar.Manifest.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMainAttributes (This : access Typ)
                               return access Java.Util.Jar.Attributes.Typ'Class;

   function GetEntries (This : access Typ)
                        return access Java.Util.Map.Typ'Class;

   function GetAttributes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Util.Jar.Attributes.Typ'Class;

   procedure Clear (This : access Typ);

   procedure Write (This : access Typ;
                    P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Read (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Manifest);
   pragma Import (Java, GetMainAttributes, "getMainAttributes");
   pragma Import (Java, GetEntries, "getEntries");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Clone, "clone");

end Java.Util.Jar.Manifest;
pragma Import (Java, Java.Util.Jar.Manifest, "java.util.jar.Manifest");
pragma Extensions_Allowed (Off);
