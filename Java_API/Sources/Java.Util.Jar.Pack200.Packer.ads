pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Util.Jar.JarFile;
limited with Java.Util.Jar.JarInputStream;
limited with Java.Util.SortedMap;
with Java.Lang.Object;

package Java.Util.Jar.Pack200.Packer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Properties (This : access Typ)
                        return access Java.Util.SortedMap.Typ'Class is abstract;

   procedure Pack (This : access Typ;
                   P1_JarFile : access Standard.Java.Util.Jar.JarFile.Typ'Class;
                   P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Pack (This : access Typ;
                   P1_JarInputStream : access Standard.Java.Util.Jar.JarInputStream.Typ'Class;
                   P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SEGMENT_LIMIT : constant access Java.Lang.String.Typ'Class;

   --  final
   KEEP_FILE_ORDER : constant access Java.Lang.String.Typ'Class;

   --  final
   EFFORT : constant access Java.Lang.String.Typ'Class;

   --  final
   DEFLATE_HINT : constant access Java.Lang.String.Typ'Class;

   --  final
   MODIFICATION_TIME : constant access Java.Lang.String.Typ'Class;

   --  final
   PASS_FILE_PFX : constant access Java.Lang.String.Typ'Class;

   --  final
   UNKNOWN_ATTRIBUTE : constant access Java.Lang.String.Typ'Class;

   --  final
   CLASS_ATTRIBUTE_PFX : constant access Java.Lang.String.Typ'Class;

   --  final
   FIELD_ATTRIBUTE_PFX : constant access Java.Lang.String.Typ'Class;

   --  final
   METHOD_ATTRIBUTE_PFX : constant access Java.Lang.String.Typ'Class;

   --  final
   CODE_ATTRIBUTE_PFX : constant access Java.Lang.String.Typ'Class;

   --  final
   PROGRESS : constant access Java.Lang.String.Typ'Class;

   --  final
   KEEP : constant access Java.Lang.String.Typ'Class;

   --  final
   PASS : constant access Java.Lang.String.Typ'Class;

   --  final
   STRIP : constant access Java.Lang.String.Typ'Class;

   --  final
   ERROR : constant access Java.Lang.String.Typ'Class;

   --  final
   TRUE : constant access Java.Lang.String.Typ'Class;

   --  final
   FALSE : constant access Java.Lang.String.Typ'Class;

   --  final
   LATEST : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Properties, "properties");
   pragma Export (Java, Pack, "pack");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, SEGMENT_LIMIT, "SEGMENT_LIMIT");
   pragma Import (Java, KEEP_FILE_ORDER, "KEEP_FILE_ORDER");
   pragma Import (Java, EFFORT, "EFFORT");
   pragma Import (Java, DEFLATE_HINT, "DEFLATE_HINT");
   pragma Import (Java, MODIFICATION_TIME, "MODIFICATION_TIME");
   pragma Import (Java, PASS_FILE_PFX, "PASS_FILE_PFX");
   pragma Import (Java, UNKNOWN_ATTRIBUTE, "UNKNOWN_ATTRIBUTE");
   pragma Import (Java, CLASS_ATTRIBUTE_PFX, "CLASS_ATTRIBUTE_PFX");
   pragma Import (Java, FIELD_ATTRIBUTE_PFX, "FIELD_ATTRIBUTE_PFX");
   pragma Import (Java, METHOD_ATTRIBUTE_PFX, "METHOD_ATTRIBUTE_PFX");
   pragma Import (Java, CODE_ATTRIBUTE_PFX, "CODE_ATTRIBUTE_PFX");
   pragma Import (Java, PROGRESS, "PROGRESS");
   pragma Import (Java, KEEP, "KEEP");
   pragma Import (Java, PASS, "PASS");
   pragma Import (Java, STRIP, "STRIP");
   pragma Import (Java, ERROR, "ERROR");
   pragma Import (Java, TRUE, "TRUE");
   pragma Import (Java, FALSE, "FALSE");
   pragma Import (Java, LATEST, "LATEST");

end Java.Util.Jar.Pack200.Packer;
pragma Import (Java, Java.Util.Jar.Pack200.Packer, "java.util.jar.Pack200$Packer");
pragma Extensions_Allowed (Off);
