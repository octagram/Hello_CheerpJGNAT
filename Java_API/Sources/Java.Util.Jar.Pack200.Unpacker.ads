pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Jar.JarOutputStream;
limited with Java.Util.SortedMap;
with Java.Lang.Object;

package Java.Util.Jar.Pack200.Unpacker is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Properties (This : access Typ)
                        return access Java.Util.SortedMap.Typ'Class is abstract;

   procedure Unpack (This : access Typ;
                     P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                     P2_JarOutputStream : access Standard.Java.Util.Jar.JarOutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Unpack (This : access Typ;
                     P1_File : access Standard.Java.Io.File.Typ'Class;
                     P2_JarOutputStream : access Standard.Java.Util.Jar.JarOutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   KEEP : constant access Java.Lang.String.Typ'Class;

   --  final
   TRUE : constant access Java.Lang.String.Typ'Class;

   --  final
   FALSE : constant access Java.Lang.String.Typ'Class;

   --  final
   DEFLATE_HINT : constant access Java.Lang.String.Typ'Class;

   --  final
   PROGRESS : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Properties, "properties");
   pragma Export (Java, Unpack, "unpack");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, KEEP, "KEEP");
   pragma Import (Java, TRUE, "TRUE");
   pragma Import (Java, FALSE, "FALSE");
   pragma Import (Java, DEFLATE_HINT, "DEFLATE_HINT");
   pragma Import (Java, PROGRESS, "PROGRESS");

end Java.Util.Jar.Pack200.Unpacker;
pragma Import (Java, Java.Util.Jar.Pack200.Unpacker, "java.util.jar.Pack200$Unpacker");
pragma Extensions_Allowed (Off);
