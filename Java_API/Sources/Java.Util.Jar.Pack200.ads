pragma Extensions_Allowed (On);
limited with Java.Util.Jar.Pack200.Packer;
limited with Java.Util.Jar.Pack200.Unpacker;
with Java.Lang.Object;

package Java.Util.Jar.Pack200 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function NewPacker return access Java.Util.Jar.Pack200.Packer.Typ'Class;

   function NewUnpacker return access Java.Util.Jar.Pack200.Unpacker.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewPacker, "newPacker");
   pragma Import (Java, NewUnpacker, "newUnpacker");

end Java.Util.Jar.Pack200;
pragma Import (Java, Java.Util.Jar.Pack200, "java.util.jar.Pack200");
pragma Extensions_Allowed (Off);
