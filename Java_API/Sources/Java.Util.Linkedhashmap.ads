pragma Extensions_Allowed (On);
limited with Java.Util.Map.Entry_K;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.HashMap;
with Java.Util.Map;

package Java.Util.LinkedHashMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Java.Util.HashMap.Typ(Serializable_I,
                                 Cloneable_I,
                                 Map_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinkedHashMap (P1_Int : Java.Int;
                               P2_Float : Java.Float; 
                               This : Ref := null)
                               return Ref;

   function New_LinkedHashMap (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   function New_LinkedHashMap (This : Ref := null)
                               return Ref;

   function New_LinkedHashMap (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_LinkedHashMap (P1_Int : Java.Int;
                               P2_Float : Java.Float;
                               P3_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   --  protected
   function RemoveEldestEntry (This : access Typ;
                               P1_Entry_K : access Standard.Java.Util.Map.Entry_K.Typ'Class)
                               return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LinkedHashMap);
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, RemoveEldestEntry, "removeEldestEntry");

end Java.Util.LinkedHashMap;
pragma Import (Java, Java.Util.LinkedHashMap, "java.util.LinkedHashMap");
pragma Extensions_Allowed (Off);
