pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Collection;
with Java.Util.HashSet;
with Java.Util.Set;

package Java.Util.LinkedHashSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Set_I : Java.Util.Set.Ref)
    is new Java.Util.HashSet.Typ(Serializable_I,
                                 Cloneable_I,
                                 Collection_I,
                                 Set_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinkedHashSet (P1_Int : Java.Int;
                               P2_Float : Java.Float; 
                               This : Ref := null)
                               return Ref;

   function New_LinkedHashSet (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   function New_LinkedHashSet (This : Ref := null)
                               return Ref;

   function New_LinkedHashSet (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LinkedHashSet);

end Java.Util.LinkedHashSet;
pragma Import (Java, Java.Util.LinkedHashSet, "java.util.LinkedHashSet");
pragma Extensions_Allowed (Off);
