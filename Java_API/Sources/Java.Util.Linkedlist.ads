pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
limited with Java.Util.ListIterator;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractSequentialList;
with Java.Util.Collection;
with Java.Util.Deque;
with Java.Util.List;

package Java.Util.LinkedList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Deque_I : Java.Util.Deque.Ref;
            List_I : Java.Util.List.Ref)
    is new Java.Util.AbstractSequentialList.Typ(Collection_I,
                                                List_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinkedList (This : Ref := null)
                            return Ref;

   function New_LinkedList (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFirst (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function GetLast (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function RemoveFirst (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function RemoveLast (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   procedure AddFirst (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddLast (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function Size (This : access Typ)
                  return Java.Int;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   procedure Clear (This : access Typ);

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Int;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Element (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ)
                    return access Java.Lang.Object.Typ'Class;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   function OfferFirst (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean;

   function OfferLast (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   function PeekFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function PeekLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function PollFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function PollLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure Push (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Pop (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;

   function RemoveFirstOccurrence (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return Java.Boolean;

   function RemoveLastOccurrence (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return Java.Boolean;

   function ListIterator (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Util.ListIterator.Typ'Class;

   function DescendingIterator (This : access Typ)
                                return access Java.Util.Iterator.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LinkedList);
   pragma Import (Java, GetFirst, "getFirst");
   pragma Import (Java, GetLast, "getLast");
   pragma Import (Java, RemoveFirst, "removeFirst");
   pragma Import (Java, RemoveLast, "removeLast");
   pragma Import (Java, AddFirst, "addFirst");
   pragma Import (Java, AddLast, "addLast");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, Element, "element");
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, Offer, "offer");
   pragma Import (Java, OfferFirst, "offerFirst");
   pragma Import (Java, OfferLast, "offerLast");
   pragma Import (Java, PeekFirst, "peekFirst");
   pragma Import (Java, PeekLast, "peekLast");
   pragma Import (Java, PollFirst, "pollFirst");
   pragma Import (Java, PollLast, "pollLast");
   pragma Import (Java, Push, "push");
   pragma Import (Java, Pop, "pop");
   pragma Import (Java, RemoveFirstOccurrence, "removeFirstOccurrence");
   pragma Import (Java, RemoveLastOccurrence, "removeLastOccurrence");
   pragma Import (Java, ListIterator, "listIterator");
   pragma Import (Java, DescendingIterator, "descendingIterator");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToArray, "toArray");

end Java.Util.LinkedList;
pragma Import (Java, Java.Util.LinkedList, "java.util.LinkedList");
pragma Extensions_Allowed (Off);
