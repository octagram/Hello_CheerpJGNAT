pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.Iterator;

package Java.Util.ListIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Iterator_I : Java.Util.Iterator.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function HasNext (This : access Typ)
                     return Java.Boolean is abstract;

   function Next (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;

   function HasPrevious (This : access Typ)
                         return Java.Boolean is abstract;

   function Previous (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function NextIndex (This : access Typ)
                       return Java.Int is abstract;

   function PreviousIndex (This : access Typ)
                           return Java.Int is abstract;

   procedure Remove (This : access Typ) is abstract;

   procedure Set (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure Add (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, HasNext, "hasNext");
   pragma Export (Java, Next, "next");
   pragma Export (Java, HasPrevious, "hasPrevious");
   pragma Export (Java, Previous, "previous");
   pragma Export (Java, NextIndex, "nextIndex");
   pragma Export (Java, PreviousIndex, "previousIndex");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Set, "set");
   pragma Export (Java, Add, "add");

end Java.Util.ListIterator;
pragma Import (Java, Java.Util.ListIterator, "java.util.ListIterator");
pragma Extensions_Allowed (Off);
