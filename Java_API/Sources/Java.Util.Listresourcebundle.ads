pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Set;
with Java.Lang.Object;
with Java.Util.ResourceBundle;

package Java.Util.ListResourceBundle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Util.ResourceBundle.Typ
      with null record;

   function New_ListResourceBundle (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function HandleGetObject (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetKeys (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class;

   --  protected
   function HandleKeySet (This : access Typ)
                          return access Java.Util.Set.Typ'Class;

   --  protected
   function GetContents (This : access Typ)
                         return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ListResourceBundle);
   pragma Export (Java, HandleGetObject, "handleGetObject");
   pragma Export (Java, GetKeys, "getKeys");
   pragma Export (Java, HandleKeySet, "handleKeySet");
   pragma Export (Java, GetContents, "getContents");

end Java.Util.ListResourceBundle;
pragma Import (Java, Java.Util.ListResourceBundle, "java.util.ListResourceBundle");
pragma Extensions_Allowed (Off);
