pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Util.Locale is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Locale (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Locale (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_Locale (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefault return access Java.Util.Locale.Typ'Class;

   --  synchronized
   procedure SetDefault (P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   function GetAvailableLocales return Standard.Java.Lang.Object.Ref;

   function GetISOCountries return Standard.Java.Lang.Object.Ref;

   function GetISOLanguages return Standard.Java.Lang.Object.Ref;

   function GetLanguage (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetCountry (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetVariant (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetISO3Language (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Java.Util.MissingResourceException.Except

   function GetISO3Country (This : access Typ)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Java.Util.MissingResourceException.Except

   --  final
   function GetDisplayLanguage (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function GetDisplayLanguage (This : access Typ;
                                P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   --  final
   function GetDisplayCountry (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   function GetDisplayCountry (This : access Typ;
                               P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                               return access Java.Lang.String.Typ'Class;

   --  final
   function GetDisplayVariant (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   function GetDisplayVariant (This : access Typ;
                               P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                               return access Java.Lang.String.Typ'Class;

   --  final
   function GetDisplayName (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetDisplayName (This : access Typ;
                            P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ENGLISH : access Java.Util.Locale.Typ'Class;

   --  final
   FRENCH : access Java.Util.Locale.Typ'Class;

   --  final
   GERMAN : access Java.Util.Locale.Typ'Class;

   --  final
   ITALIAN : access Java.Util.Locale.Typ'Class;

   --  final
   JAPANESE : access Java.Util.Locale.Typ'Class;

   --  final
   KOREAN : access Java.Util.Locale.Typ'Class;

   --  final
   CHINESE : access Java.Util.Locale.Typ'Class;

   --  final
   SIMPLIFIED_CHINESE : access Java.Util.Locale.Typ'Class;

   --  final
   TRADITIONAL_CHINESE : access Java.Util.Locale.Typ'Class;

   --  final
   FRANCE : access Java.Util.Locale.Typ'Class;

   --  final
   GERMANY : access Java.Util.Locale.Typ'Class;

   --  final
   ITALY : access Java.Util.Locale.Typ'Class;

   --  final
   JAPAN : access Java.Util.Locale.Typ'Class;

   --  final
   KOREA : access Java.Util.Locale.Typ'Class;

   --  final
   CHINA : access Java.Util.Locale.Typ'Class;

   --  final
   PRC : access Java.Util.Locale.Typ'Class;

   --  final
   TAIWAN : access Java.Util.Locale.Typ'Class;

   --  final
   UK : access Java.Util.Locale.Typ'Class;

   --  final
   US : access Java.Util.Locale.Typ'Class;

   --  final
   CANADA : access Java.Util.Locale.Typ'Class;

   --  final
   CANADA_FRENCH : access Java.Util.Locale.Typ'Class;

   --  final
   ROOT : access Java.Util.Locale.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Locale);
   pragma Import (Java, GetDefault, "getDefault");
   pragma Import (Java, SetDefault, "setDefault");
   pragma Import (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Import (Java, GetISOCountries, "getISOCountries");
   pragma Import (Java, GetISOLanguages, "getISOLanguages");
   pragma Import (Java, GetLanguage, "getLanguage");
   pragma Import (Java, GetCountry, "getCountry");
   pragma Import (Java, GetVariant, "getVariant");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetISO3Language, "getISO3Language");
   pragma Import (Java, GetISO3Country, "getISO3Country");
   pragma Import (Java, GetDisplayLanguage, "getDisplayLanguage");
   pragma Import (Java, GetDisplayCountry, "getDisplayCountry");
   pragma Import (Java, GetDisplayVariant, "getDisplayVariant");
   pragma Import (Java, GetDisplayName, "getDisplayName");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ENGLISH, "ENGLISH");
   pragma Import (Java, FRENCH, "FRENCH");
   pragma Import (Java, GERMAN, "GERMAN");
   pragma Import (Java, ITALIAN, "ITALIAN");
   pragma Import (Java, JAPANESE, "JAPANESE");
   pragma Import (Java, KOREAN, "KOREAN");
   pragma Import (Java, CHINESE, "CHINESE");
   pragma Import (Java, SIMPLIFIED_CHINESE, "SIMPLIFIED_CHINESE");
   pragma Import (Java, TRADITIONAL_CHINESE, "TRADITIONAL_CHINESE");
   pragma Import (Java, FRANCE, "FRANCE");
   pragma Import (Java, GERMANY, "GERMANY");
   pragma Import (Java, ITALY, "ITALY");
   pragma Import (Java, JAPAN, "JAPAN");
   pragma Import (Java, KOREA, "KOREA");
   pragma Import (Java, CHINA, "CHINA");
   pragma Import (Java, PRC, "PRC");
   pragma Import (Java, TAIWAN, "TAIWAN");
   pragma Import (Java, UK, "UK");
   pragma Import (Java, US, "US");
   pragma Import (Java, CANADA, "CANADA");
   pragma Import (Java, CANADA_FRENCH, "CANADA_FRENCH");
   pragma Import (Java, ROOT, "ROOT");

end Java.Util.Locale;
pragma Import (Java, Java.Util.Locale, "java.util.Locale");
pragma Extensions_Allowed (Off);
