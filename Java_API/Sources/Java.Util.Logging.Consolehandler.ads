pragma Extensions_Allowed (On);
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;
with Java.Util.Logging.StreamHandler;

package Java.Util.Logging.ConsoleHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.Logging.StreamHandler.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ConsoleHandler (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Publish (This : access Typ;
                      P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class);

   procedure Close (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConsoleHandler);
   pragma Import (Java, Publish, "publish");
   pragma Import (Java, Close, "close");

end Java.Util.Logging.ConsoleHandler;
pragma Import (Java, Java.Util.Logging.ConsoleHandler, "java.util.logging.ConsoleHandler");
pragma Extensions_Allowed (Off);
