pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Util.Logging.ErrorManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ErrorManager (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Error (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class;
                    P3_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   GENERIC_FAILURE : constant Java.Int;

   --  final
   WRITE_FAILURE : constant Java.Int;

   --  final
   FLUSH_FAILURE : constant Java.Int;

   --  final
   CLOSE_FAILURE : constant Java.Int;

   --  final
   OPEN_FAILURE : constant Java.Int;

   --  final
   FORMAT_FAILURE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ErrorManager);
   pragma Import (Java, Error, "error");
   pragma Import (Java, GENERIC_FAILURE, "GENERIC_FAILURE");
   pragma Import (Java, WRITE_FAILURE, "WRITE_FAILURE");
   pragma Import (Java, FLUSH_FAILURE, "FLUSH_FAILURE");
   pragma Import (Java, CLOSE_FAILURE, "CLOSE_FAILURE");
   pragma Import (Java, OPEN_FAILURE, "OPEN_FAILURE");
   pragma Import (Java, FORMAT_FAILURE, "FORMAT_FAILURE");

end Java.Util.Logging.ErrorManager;
pragma Import (Java, Java.Util.Logging.ErrorManager, "java.util.logging.ErrorManager");
pragma Extensions_Allowed (Off);
