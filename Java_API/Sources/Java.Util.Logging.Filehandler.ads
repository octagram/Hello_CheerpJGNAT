pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;
with Java.Util.Logging.StreamHandler;

package Java.Util.Logging.FileHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.Logging.StreamHandler.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileHandler (This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   function New_FileHandler (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   function New_FileHandler (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   function New_FileHandler (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   function New_FileHandler (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Publish (This : access Typ;
                      P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class);

   --  synchronized
   procedure Close (This : access Typ);
   --  can raise Java.Lang.SecurityException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileHandler);
   pragma Import (Java, Publish, "publish");
   pragma Import (Java, Close, "close");

end Java.Util.Logging.FileHandler;
pragma Import (Java, Java.Util.Logging.FileHandler, "java.util.logging.FileHandler");
pragma Extensions_Allowed (Off);
