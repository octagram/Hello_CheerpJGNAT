pragma Extensions_Allowed (On);
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;

package Java.Util.Logging.Filter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsLoggable (This : access Typ;
                        P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class)
                        return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsLoggable, "isLoggable");

end Java.Util.Logging.Filter;
pragma Import (Java, Java.Util.Logging.Filter, "java.util.logging.Filter");
pragma Extensions_Allowed (Off);
