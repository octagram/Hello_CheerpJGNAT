pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Logging.Handler;
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;

package Java.Util.Logging.Formatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Formatter (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Format (This : access Typ;
                    P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class)
                    return access Java.Lang.String.Typ'Class is abstract;

   function GetHead (This : access Typ;
                     P1_Handler : access Standard.Java.Util.Logging.Handler.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetTail (This : access Typ;
                     P1_Handler : access Standard.Java.Util.Logging.Handler.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   --  synchronized
   function FormatMessage (This : access Typ;
                           P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class)
                           return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Formatter);
   pragma Export (Java, Format, "format");
   pragma Import (Java, GetHead, "getHead");
   pragma Import (Java, GetTail, "getTail");
   pragma Import (Java, FormatMessage, "formatMessage");

end Java.Util.Logging.Formatter;
pragma Import (Java, Java.Util.Logging.Formatter, "java.util.logging.Formatter");
pragma Extensions_Allowed (Off);
