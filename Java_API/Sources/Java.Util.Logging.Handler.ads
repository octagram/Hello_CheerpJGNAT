pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
limited with Java.Lang.String;
limited with Java.Util.Logging.ErrorManager;
limited with Java.Util.Logging.Filter;
limited with Java.Util.Logging.Formatter;
limited with Java.Util.Logging.Level;
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;

package Java.Util.Logging.Handler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Handler (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Publish (This : access Typ;
                      P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class) is abstract;

   procedure Flush (This : access Typ) is abstract;

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Lang.SecurityException.Except

   procedure SetFormatter (This : access Typ;
                           P1_Formatter : access Standard.Java.Util.Logging.Formatter.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   function GetFormatter (This : access Typ)
                          return access Java.Util.Logging.Formatter.Typ'Class;

   procedure SetEncoding (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetFilter (This : access Typ;
                        P1_Filter : access Standard.Java.Util.Logging.Filter.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   function GetFilter (This : access Typ)
                       return access Java.Util.Logging.Filter.Typ'Class;

   procedure SetErrorManager (This : access Typ;
                              P1_ErrorManager : access Standard.Java.Util.Logging.ErrorManager.Typ'Class);

   function GetErrorManager (This : access Typ)
                             return access Java.Util.Logging.ErrorManager.Typ'Class;

   --  protected
   procedure ReportError (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class;
                          P3_Int : Java.Int);

   --  synchronized
   procedure SetLevel (This : access Typ;
                       P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   --  synchronized
   function GetLevel (This : access Typ)
                      return access Java.Util.Logging.Level.Typ'Class;

   function IsLoggable (This : access Typ;
                        P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class)
                        return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Handler);
   pragma Export (Java, Publish, "publish");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, Close, "close");
   pragma Import (Java, SetFormatter, "setFormatter");
   pragma Import (Java, GetFormatter, "getFormatter");
   pragma Import (Java, SetEncoding, "setEncoding");
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, SetFilter, "setFilter");
   pragma Import (Java, GetFilter, "getFilter");
   pragma Import (Java, SetErrorManager, "setErrorManager");
   pragma Import (Java, GetErrorManager, "getErrorManager");
   pragma Import (Java, ReportError, "reportError");
   pragma Import (Java, SetLevel, "setLevel");
   pragma Import (Java, GetLevel, "getLevel");
   pragma Import (Java, IsLoggable, "isLoggable");

end Java.Util.Logging.Handler;
pragma Import (Java, Java.Util.Logging.Handler, "java.util.logging.Handler");
pragma Extensions_Allowed (Off);
