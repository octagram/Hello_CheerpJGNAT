pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Logging.Level is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Level (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   --  protected
   function New_Level (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int;
                       P3_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetResourceBundleName (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetLocalizedName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  final
   function IntValue (This : access Typ)
                      return Java.Int;

   --  synchronized
   function Parse (P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Java.Util.Logging.Level.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OFF : access Java.Util.Logging.Level.Typ'Class;

   --  final
   SEVERE : access Java.Util.Logging.Level.Typ'Class;

   --  final
   WARNING : access Java.Util.Logging.Level.Typ'Class;

   --  final
   INFO : access Java.Util.Logging.Level.Typ'Class;

   --  final
   CONFIG : access Java.Util.Logging.Level.Typ'Class;

   --  final
   FINE : access Java.Util.Logging.Level.Typ'Class;

   --  final
   FINER : access Java.Util.Logging.Level.Typ'Class;

   --  final
   FINEST : access Java.Util.Logging.Level.Typ'Class;

   --  final
   ALL_K : access Java.Util.Logging.Level.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Level);
   pragma Import (Java, GetResourceBundleName, "getResourceBundleName");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetLocalizedName, "getLocalizedName");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IntValue, "intValue");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, OFF, "OFF");
   pragma Import (Java, SEVERE, "SEVERE");
   pragma Import (Java, WARNING, "WARNING");
   pragma Import (Java, INFO, "INFO");
   pragma Import (Java, CONFIG, "CONFIG");
   pragma Import (Java, FINE, "FINE");
   pragma Import (Java, FINER, "FINER");
   pragma Import (Java, FINEST, "FINEST");
   pragma Import (Java, ALL_K, "ALL");

end Java.Util.Logging.Level;
pragma Import (Java, Java.Util.Logging.Level, "java.util.logging.Level");
pragma Extensions_Allowed (Off);
