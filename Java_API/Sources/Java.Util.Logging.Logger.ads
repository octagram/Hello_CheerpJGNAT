pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Java.Util.Logging.Filter;
limited with Java.Util.Logging.Handler;
limited with Java.Util.Logging.Level;
limited with Java.Util.Logging.LogRecord;
limited with Java.Util.ResourceBundle;
with Java.Lang.Object;

package Java.Util.Logging.Logger is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Logger (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetLogger (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.Logging.Logger.Typ'Class;

   --  synchronized
   function GetLogger (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.Logging.Logger.Typ'Class;

   function GetAnonymousLogger return access Java.Util.Logging.Logger.Typ'Class;

   --  synchronized
   function GetAnonymousLogger (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Util.Logging.Logger.Typ'Class;

   function GetResourceBundle (This : access Typ)
                               return access Java.Util.ResourceBundle.Typ'Class;

   function GetResourceBundleName (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   procedure SetFilter (This : access Typ;
                        P1_Filter : access Standard.Java.Util.Logging.Filter.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   function GetFilter (This : access Typ)
                       return access Java.Util.Logging.Filter.Typ'Class;

   procedure Log (This : access Typ;
                  P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class);

   procedure Log (This : access Typ;
                  P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Log (This : access Typ;
                  P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                  P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Log (This : access Typ;
                  P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                  P3_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure Log (This : access Typ;
                  P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                  P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   procedure Logp (This : access Typ;
                   P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                   P4_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Logp (This : access Typ;
                   P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                   P4_String : access Standard.Java.Lang.String.Typ'Class;
                   P5_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Logp (This : access Typ;
                   P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                   P4_String : access Standard.Java.Lang.String.Typ'Class;
                   P5_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure Logp (This : access Typ;
                   P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                   P4_String : access Standard.Java.Lang.String.Typ'Class;
                   P5_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   procedure Logrb (This : access Typ;
                    P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                    P4_String : access Standard.Java.Lang.String.Typ'Class;
                    P5_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Logrb (This : access Typ;
                    P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                    P4_String : access Standard.Java.Lang.String.Typ'Class;
                    P5_String : access Standard.Java.Lang.String.Typ'Class;
                    P6_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Logrb (This : access Typ;
                    P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                    P4_String : access Standard.Java.Lang.String.Typ'Class;
                    P5_String : access Standard.Java.Lang.String.Typ'Class;
                    P6_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure Logrb (This : access Typ;
                    P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                    P4_String : access Standard.Java.Lang.String.Typ'Class;
                    P5_String : access Standard.Java.Lang.String.Typ'Class;
                    P6_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   procedure Entering (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Entering (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Entering (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure Exiting (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Exiting (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Throwing (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   procedure Severe (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Warning (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Info (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Config (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Fine (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Finer (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Finest (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetLevel (This : access Typ;
                       P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   function GetLevel (This : access Typ)
                      return access Java.Util.Logging.Level.Typ'Class;

   function IsLoggable (This : access Typ;
                        P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class)
                        return Java.Boolean;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure AddHandler (This : access Typ;
                         P1_Handler : access Standard.Java.Util.Logging.Handler.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   --  synchronized
   procedure RemoveHandler (This : access Typ;
                            P1_Handler : access Standard.Java.Util.Logging.Handler.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   --  synchronized
   function GetHandlers (This : access Typ)
                         return Standard.Java.Lang.Object.Ref;

   procedure SetUseParentHandlers (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetUseParentHandlers (This : access Typ)
                                  return Java.Boolean;

   function GetParent (This : access Typ)
                       return access Java.Util.Logging.Logger.Typ'Class;

   procedure SetParent (This : access Typ;
                        P1_Logger : access Standard.Java.Util.Logging.Logger.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   GLOBAL_LOGGER_NAME : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Logger);
   pragma Import (Java, GetLogger, "getLogger");
   pragma Import (Java, GetAnonymousLogger, "getAnonymousLogger");
   pragma Import (Java, GetResourceBundle, "getResourceBundle");
   pragma Import (Java, GetResourceBundleName, "getResourceBundleName");
   pragma Import (Java, SetFilter, "setFilter");
   pragma Import (Java, GetFilter, "getFilter");
   pragma Import (Java, Log, "log");
   pragma Import (Java, Logp, "logp");
   pragma Import (Java, Logrb, "logrb");
   pragma Import (Java, Entering, "entering");
   pragma Import (Java, Exiting, "exiting");
   pragma Import (Java, Throwing, "throwing");
   pragma Import (Java, Severe, "severe");
   pragma Import (Java, Warning, "warning");
   pragma Import (Java, Info, "info");
   pragma Import (Java, Config, "config");
   pragma Import (Java, Fine, "fine");
   pragma Import (Java, Finer, "finer");
   pragma Import (Java, Finest, "finest");
   pragma Import (Java, SetLevel, "setLevel");
   pragma Import (Java, GetLevel, "getLevel");
   pragma Import (Java, IsLoggable, "isLoggable");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, AddHandler, "addHandler");
   pragma Import (Java, RemoveHandler, "removeHandler");
   pragma Import (Java, GetHandlers, "getHandlers");
   pragma Import (Java, SetUseParentHandlers, "setUseParentHandlers");
   pragma Import (Java, GetUseParentHandlers, "getUseParentHandlers");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GLOBAL_LOGGER_NAME, "GLOBAL_LOGGER_NAME");

end Java.Util.Logging.Logger;
pragma Import (Java, Java.Util.Logging.Logger, "java.util.logging.Logger");
pragma Extensions_Allowed (Off);
