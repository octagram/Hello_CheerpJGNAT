pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Lang.Object;

package Java.Util.Logging.LoggingMXBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLoggerNames (This : access Typ)
                            return access Java.Util.List.Typ'Class is abstract;

   function GetLoggerLevel (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLoggerLevel (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetParentLoggerName (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLoggerNames, "getLoggerNames");
   pragma Export (Java, GetLoggerLevel, "getLoggerLevel");
   pragma Export (Java, SetLoggerLevel, "setLoggerLevel");
   pragma Export (Java, GetParentLoggerName, "getParentLoggerName");

end Java.Util.Logging.LoggingMXBean;
pragma Import (Java, Java.Util.Logging.LoggingMXBean, "java.util.logging.LoggingMXBean");
pragma Extensions_Allowed (Off);
