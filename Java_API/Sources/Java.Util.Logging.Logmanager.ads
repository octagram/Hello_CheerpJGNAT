pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Logging.Logger;
limited with Java.Util.Logging.LoggingMXBean;
with Java.Lang.Object;

package Java.Util.Logging.LogManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_LogManager (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLogManager return access Java.Util.Logging.LogManager.Typ'Class;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   function AddLogger (This : access Typ;
                       P1_Logger : access Standard.Java.Util.Logging.Logger.Typ'Class)
                       return Java.Boolean;

   function GetLogger (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.Logging.Logger.Typ'Class;

   function GetLoggerNames (This : access Typ)
                            return access Java.Util.Enumeration.Typ'Class;

   procedure ReadConfiguration (This : access Typ);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   procedure Reset (This : access Typ);
   --  can raise Java.Lang.SecurityException.Except

   procedure ReadConfiguration (This : access Typ;
                                P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.SecurityException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   procedure CheckAccess (This : access Typ);
   --  can raise Java.Lang.SecurityException.Except

   --  synchronized
   function GetLoggingMXBean return access Java.Util.Logging.LoggingMXBean.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LOGGING_MXBEAN_NAME : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LogManager);
   pragma Import (Java, GetLogManager, "getLogManager");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, AddLogger, "addLogger");
   pragma Import (Java, GetLogger, "getLogger");
   pragma Import (Java, GetLoggerNames, "getLoggerNames");
   pragma Import (Java, ReadConfiguration, "readConfiguration");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, CheckAccess, "checkAccess");
   pragma Import (Java, GetLoggingMXBean, "getLoggingMXBean");
   pragma Import (Java, LOGGING_MXBEAN_NAME, "LOGGING_MXBEAN_NAME");

end Java.Util.Logging.LogManager;
pragma Import (Java, Java.Util.Logging.LogManager, "java.util.logging.LogManager");
pragma Extensions_Allowed (Off);
