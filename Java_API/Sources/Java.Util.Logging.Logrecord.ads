pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Java.Util.Logging.Level;
limited with Java.Util.ResourceBundle;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Logging.LogRecord is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LogRecord (P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLoggerName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   procedure SetLoggerName (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetResourceBundle (This : access Typ)
                               return access Java.Util.ResourceBundle.Typ'Class;

   procedure SetResourceBundle (This : access Typ;
                                P1_ResourceBundle : access Standard.Java.Util.ResourceBundle.Typ'Class);

   function GetResourceBundleName (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   procedure SetResourceBundleName (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetLevel (This : access Typ)
                      return access Java.Util.Logging.Level.Typ'Class;

   procedure SetLevel (This : access Typ;
                       P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class);

   function GetSequenceNumber (This : access Typ)
                               return Java.Long;

   procedure SetSequenceNumber (This : access Typ;
                                P1_Long : Java.Long);

   function GetSourceClassName (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure SetSourceClassName (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSourceMethodName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   procedure SetSourceMethodName (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetMessage (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetParameters (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   procedure SetParameters (This : access Typ;
                            P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function GetThreadID (This : access Typ)
                         return Java.Int;

   procedure SetThreadID (This : access Typ;
                          P1_Int : Java.Int);

   function GetMillis (This : access Typ)
                       return Java.Long;

   procedure SetMillis (This : access Typ;
                        P1_Long : Java.Long);

   function GetThrown (This : access Typ)
                       return access Java.Lang.Throwable.Typ'Class;

   procedure SetThrown (This : access Typ;
                        P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LogRecord);
   pragma Import (Java, GetLoggerName, "getLoggerName");
   pragma Import (Java, SetLoggerName, "setLoggerName");
   pragma Import (Java, GetResourceBundle, "getResourceBundle");
   pragma Import (Java, SetResourceBundle, "setResourceBundle");
   pragma Import (Java, GetResourceBundleName, "getResourceBundleName");
   pragma Import (Java, SetResourceBundleName, "setResourceBundleName");
   pragma Import (Java, GetLevel, "getLevel");
   pragma Import (Java, SetLevel, "setLevel");
   pragma Import (Java, GetSequenceNumber, "getSequenceNumber");
   pragma Import (Java, SetSequenceNumber, "setSequenceNumber");
   pragma Import (Java, GetSourceClassName, "getSourceClassName");
   pragma Import (Java, SetSourceClassName, "setSourceClassName");
   pragma Import (Java, GetSourceMethodName, "getSourceMethodName");
   pragma Import (Java, SetSourceMethodName, "setSourceMethodName");
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, SetMessage, "setMessage");
   pragma Import (Java, GetParameters, "getParameters");
   pragma Import (Java, SetParameters, "setParameters");
   pragma Import (Java, GetThreadID, "getThreadID");
   pragma Import (Java, SetThreadID, "setThreadID");
   pragma Import (Java, GetMillis, "getMillis");
   pragma Import (Java, SetMillis, "setMillis");
   pragma Import (Java, GetThrown, "getThrown");
   pragma Import (Java, SetThrown, "setThrown");

end Java.Util.Logging.LogRecord;
pragma Import (Java, Java.Util.Logging.LogRecord, "java.util.logging.LogRecord");
pragma Extensions_Allowed (Off);
