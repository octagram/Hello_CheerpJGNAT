pragma Extensions_Allowed (On);
limited with Java.Util.Logging.Level;
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;
with Java.Util.Logging.Handler;

package Java.Util.Logging.MemoryHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.Logging.Handler.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MemoryHandler (This : Ref := null)
                               return Ref;

   function New_MemoryHandler (P1_Handler : access Standard.Java.Util.Logging.Handler.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Level : access Standard.Java.Util.Logging.Level.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Publish (This : access Typ;
                      P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class);

   --  synchronized
   procedure Push (This : access Typ);

   procedure Flush (This : access Typ);

   procedure Close (This : access Typ);
   --  can raise Java.Lang.SecurityException.Except

   procedure SetPushLevel (This : access Typ;
                           P1_Level : access Standard.Java.Util.Logging.Level.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   --  synchronized
   function GetPushLevel (This : access Typ)
                          return access Java.Util.Logging.Level.Typ'Class;

   function IsLoggable (This : access Typ;
                        P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class)
                        return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MemoryHandler);
   pragma Import (Java, Publish, "publish");
   pragma Import (Java, Push, "push");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");
   pragma Import (Java, SetPushLevel, "setPushLevel");
   pragma Import (Java, GetPushLevel, "getPushLevel");
   pragma Import (Java, IsLoggable, "isLoggable");

end Java.Util.Logging.MemoryHandler;
pragma Import (Java, Java.Util.Logging.MemoryHandler, "java.util.logging.MemoryHandler");
pragma Extensions_Allowed (Off);
