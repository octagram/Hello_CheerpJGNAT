pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;
with Java.Util.Logging.Formatter;

package Java.Util.Logging.SimpleFormatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.Logging.Formatter.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleFormatter (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Format (This : access Typ;
                    P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class)
                    return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleFormatter);
   pragma Import (Java, Format, "format");

end Java.Util.Logging.SimpleFormatter;
pragma Import (Java, Java.Util.Logging.SimpleFormatter, "java.util.logging.SimpleFormatter");
pragma Extensions_Allowed (Off);
