pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;
with Java.Util.Logging.StreamHandler;

package Java.Util.Logging.SocketHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.Logging.StreamHandler.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SocketHandler (This : Ref := null)
                               return Ref;
   --  can raise Java.Io.IOException.Except

   function New_SocketHandler (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Close (This : access Typ);
   --  can raise Java.Lang.SecurityException.Except

   --  synchronized
   procedure Publish (This : access Typ;
                      P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SocketHandler);
   pragma Import (Java, Close, "close");
   pragma Import (Java, Publish, "publish");

end Java.Util.Logging.SocketHandler;
pragma Import (Java, Java.Util.Logging.SocketHandler, "java.util.logging.SocketHandler");
pragma Extensions_Allowed (Off);
