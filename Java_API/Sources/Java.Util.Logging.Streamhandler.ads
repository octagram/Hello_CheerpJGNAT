pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Util.Logging.Formatter;
limited with Java.Util.Logging.LogRecord;
with Java.Lang.Object;
with Java.Util.Logging.Handler;

package Java.Util.Logging.StreamHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.Logging.Handler.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StreamHandler (This : Ref := null)
                               return Ref;

   function New_StreamHandler (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                               P2_Formatter : access Standard.Java.Util.Logging.Formatter.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected  synchronized
   procedure SetOutputStream (This : access Typ;
                              P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   procedure SetEncoding (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except and
   --  Java.Io.UnsupportedEncodingException.Except

   --  synchronized
   procedure Publish (This : access Typ;
                      P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class);

   function IsLoggable (This : access Typ;
                        P1_LogRecord : access Standard.Java.Util.Logging.LogRecord.Typ'Class)
                        return Java.Boolean;

   --  synchronized
   procedure Flush (This : access Typ);

   --  synchronized
   procedure Close (This : access Typ);
   --  can raise Java.Lang.SecurityException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StreamHandler);
   pragma Import (Java, SetOutputStream, "setOutputStream");
   pragma Import (Java, SetEncoding, "setEncoding");
   pragma Import (Java, Publish, "publish");
   pragma Import (Java, IsLoggable, "isLoggable");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Java.Util.Logging.StreamHandler;
pragma Import (Java, Java.Util.Logging.StreamHandler, "java.util.logging.StreamHandler");
pragma Extensions_Allowed (Off);
