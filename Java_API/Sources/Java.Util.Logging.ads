pragma Extensions_Allowed (On);
package Java.Util.Logging is
   pragma Preelaborate;
end Java.Util.Logging;
pragma Import (Java, Java.Util.Logging, "java.util.logging");
pragma Extensions_Allowed (Off);
