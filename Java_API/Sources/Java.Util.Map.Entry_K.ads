pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.Map.Entry_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKey (This : access Typ)
                    return access Java.Lang.Object.Typ'Class is abstract;

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function SetValue (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetKey, "getKey");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");

end Java.Util.Map.Entry_K;
pragma Import (Java, Java.Util.Map.Entry_K, "java.util.Map$Entry");
pragma Extensions_Allowed (Off);
