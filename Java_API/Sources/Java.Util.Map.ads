pragma Extensions_Allowed (On);
limited with Java.Util.Collection;
limited with Java.Util.Set;
with Java.Lang.Object;

package Java.Util.Map is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function IsEmpty (This : access Typ)
                     return Java.Boolean is abstract;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean is abstract;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean is abstract;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;

   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;

   procedure Clear (This : access Typ) is abstract;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class is abstract;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class is abstract;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Size, "size");
   pragma Export (Java, IsEmpty, "isEmpty");
   pragma Export (Java, ContainsKey, "containsKey");
   pragma Export (Java, ContainsValue, "containsValue");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Put, "put");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, PutAll, "putAll");
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, KeySet, "keySet");
   pragma Export (Java, Values, "values");
   pragma Export (Java, EntrySet, "entrySet");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");

end Java.Util.Map;
pragma Import (Java, Java.Util.Map, "java.util.Map");
pragma Extensions_Allowed (Off);
