pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Java.Util.MissingResourceException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MissingResourceException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                                          P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetKey (This : access Typ)
                    return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.util.MissingResourceException");
   pragma Java_Constructor (New_MissingResourceException);
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetKey, "getKey");

end Java.Util.MissingResourceException;
pragma Import (Java, Java.Util.MissingResourceException, "java.util.MissingResourceException");
pragma Extensions_Allowed (Off);
