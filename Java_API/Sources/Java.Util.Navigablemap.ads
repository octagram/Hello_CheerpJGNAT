pragma Extensions_Allowed (On);
limited with Java.Util.Map.Entry_K;
limited with Java.Util.NavigableSet;
with Java.Lang.Object;
with Java.Util.SortedMap;

package Java.Util.NavigableMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SortedMap_I : Java.Util.SortedMap.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function LowerEntry (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function LowerKey (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function FloorEntry (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function FloorKey (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function CeilingEntry (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function CeilingKey (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function HigherEntry (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function HigherKey (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function FirstEntry (This : access Typ)
                        return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function LastEntry (This : access Typ)
                       return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function PollFirstEntry (This : access Typ)
                            return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function PollLastEntry (This : access Typ)
                           return access Java.Util.Map.Entry_K.Typ'Class is abstract;

   function DescendingMap (This : access Typ)
                           return access Java.Util.NavigableMap.Typ'Class is abstract;

   function NavigableKeySet (This : access Typ)
                             return access Java.Util.NavigableSet.Typ'Class is abstract;

   function DescendingKeySet (This : access Typ)
                              return access Java.Util.NavigableSet.Typ'Class is abstract;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean;
                    P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P4_Boolean : Java.Boolean)
                    return access Java.Util.NavigableMap.Typ'Class is abstract;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableMap.Typ'Class is abstract;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableMap.Typ'Class is abstract;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.SortedMap.Typ'Class is abstract;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class is abstract;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, LowerEntry, "lowerEntry");
   pragma Export (Java, LowerKey, "lowerKey");
   pragma Export (Java, FloorEntry, "floorEntry");
   pragma Export (Java, FloorKey, "floorKey");
   pragma Export (Java, CeilingEntry, "ceilingEntry");
   pragma Export (Java, CeilingKey, "ceilingKey");
   pragma Export (Java, HigherEntry, "higherEntry");
   pragma Export (Java, HigherKey, "higherKey");
   pragma Export (Java, FirstEntry, "firstEntry");
   pragma Export (Java, LastEntry, "lastEntry");
   pragma Export (Java, PollFirstEntry, "pollFirstEntry");
   pragma Export (Java, PollLastEntry, "pollLastEntry");
   pragma Export (Java, DescendingMap, "descendingMap");
   pragma Export (Java, NavigableKeySet, "navigableKeySet");
   pragma Export (Java, DescendingKeySet, "descendingKeySet");
   pragma Export (Java, SubMap, "subMap");
   pragma Export (Java, HeadMap, "headMap");
   pragma Export (Java, TailMap, "tailMap");

end Java.Util.NavigableMap;
pragma Import (Java, Java.Util.NavigableMap, "java.util.NavigableMap");
pragma Extensions_Allowed (Off);
