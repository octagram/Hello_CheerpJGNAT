pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Lang.Object;
with Java.Util.SortedSet;

package Java.Util.NavigableSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SortedSet_I : Java.Util.SortedSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Lower (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function Floor (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function Ceiling (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function Higher (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;

   function PollFirst (This : access Typ)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function PollLast (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class is abstract;

   function DescendingSet (This : access Typ)
                           return access Java.Util.NavigableSet.Typ'Class is abstract;

   function DescendingIterator (This : access Typ)
                                return access Java.Util.Iterator.Typ'Class is abstract;

   function SubSet (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean;
                    P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P4_Boolean : Java.Boolean)
                    return access Java.Util.NavigableSet.Typ'Class is abstract;

   function HeadSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableSet.Typ'Class is abstract;

   function TailSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableSet.Typ'Class is abstract;

   function SubSet (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.SortedSet.Typ'Class is abstract;

   function HeadSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedSet.Typ'Class is abstract;

   function TailSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedSet.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Lower, "lower");
   pragma Export (Java, Floor, "floor");
   pragma Export (Java, Ceiling, "ceiling");
   pragma Export (Java, Higher, "higher");
   pragma Export (Java, PollFirst, "pollFirst");
   pragma Export (Java, PollLast, "pollLast");
   pragma Export (Java, Iterator, "iterator");
   pragma Export (Java, DescendingSet, "descendingSet");
   pragma Export (Java, DescendingIterator, "descendingIterator");
   pragma Export (Java, SubSet, "subSet");
   pragma Export (Java, HeadSet, "headSet");
   pragma Export (Java, TailSet, "tailSet");

end Java.Util.NavigableSet;
pragma Import (Java, Java.Util.NavigableSet, "java.util.NavigableSet");
pragma Extensions_Allowed (Off);
