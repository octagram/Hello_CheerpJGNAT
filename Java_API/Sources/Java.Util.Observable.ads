pragma Extensions_Allowed (On);
limited with Java.Util.Observer;
with Java.Lang.Object;

package Java.Util.Observable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Observable (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddObserver (This : access Typ;
                          P1_Observer : access Standard.Java.Util.Observer.Typ'Class);

   --  synchronized
   procedure DeleteObserver (This : access Typ;
                             P1_Observer : access Standard.Java.Util.Observer.Typ'Class);

   procedure NotifyObservers (This : access Typ);

   procedure NotifyObservers (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  synchronized
   procedure DeleteObservers (This : access Typ);

   --  protected  synchronized
   procedure SetChanged (This : access Typ);

   --  protected  synchronized
   procedure ClearChanged (This : access Typ);

   --  synchronized
   function HasChanged (This : access Typ)
                        return Java.Boolean;

   --  synchronized
   function CountObservers (This : access Typ)
                            return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Observable);
   pragma Import (Java, AddObserver, "addObserver");
   pragma Import (Java, DeleteObserver, "deleteObserver");
   pragma Import (Java, NotifyObservers, "notifyObservers");
   pragma Import (Java, DeleteObservers, "deleteObservers");
   pragma Import (Java, SetChanged, "setChanged");
   pragma Import (Java, ClearChanged, "clearChanged");
   pragma Import (Java, HasChanged, "hasChanged");
   pragma Import (Java, CountObservers, "countObservers");

end Java.Util.Observable;
pragma Import (Java, Java.Util.Observable, "java.util.Observable");
pragma Extensions_Allowed (Off);
