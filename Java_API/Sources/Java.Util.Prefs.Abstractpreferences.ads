pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Util.Prefs.NodeChangeListener;
limited with Java.Util.Prefs.PreferenceChangeListener;
with Java.Lang.Object;
with Java.Util.Prefs.Preferences;

package Java.Util.Prefs.AbstractPreferences is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Util.Prefs.Preferences.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      NewNode : Java.Boolean;
      pragma Import (Java, NewNode, "newNode");

      --  protected  final
      Lock : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Lock, "lock");

   end record;

   --  protected
   function New_AbstractPreferences (P1_AbstractPreferences : access Standard.Java.Util.Prefs.AbstractPreferences.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Put (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class);

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.String.Typ'Class;

   procedure Remove (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Clear (This : access Typ);
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   procedure PutInt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int);

   function GetInt (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Int : Java.Int)
                    return Java.Int;

   procedure PutLong (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Long : Java.Long);

   function GetLong (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Long : Java.Long)
                     return Java.Long;

   procedure PutBoolean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean);

   function GetBoolean (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Boolean : Java.Boolean)
                        return Java.Boolean;

   procedure PutFloat (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Float : Java.Float);

   function GetFloat (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Float : Java.Float)
                      return Java.Float;

   procedure PutDouble (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Double : Java.Double);

   function GetDouble (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Double : Java.Double)
                       return Java.Double;

   procedure PutByteArray (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Byte_Arr : Java.Byte_Arr);

   function GetByteArray (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Byte_Arr : Java.Byte_Arr)
                          return Java.Byte_Arr;

   function Keys (This : access Typ)
                  return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   function ChildrenNames (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  final  protected
   function CachedChildren (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function Parent (This : access Typ)
                    return access Java.Util.Prefs.Preferences.Typ'Class;

   function Node (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Java.Util.Prefs.Preferences.Typ'Class;

   function NodeExists (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   procedure RemoveNode (This : access Typ);
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class;

   function AbsolutePath (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function IsUserNode (This : access Typ)
                        return Java.Boolean;

   procedure AddPreferenceChangeListener (This : access Typ;
                                          P1_PreferenceChangeListener : access Standard.Java.Util.Prefs.PreferenceChangeListener.Typ'Class);

   procedure RemovePreferenceChangeListener (This : access Typ;
                                             P1_PreferenceChangeListener : access Standard.Java.Util.Prefs.PreferenceChangeListener.Typ'Class);

   procedure AddNodeChangeListener (This : access Typ;
                                    P1_NodeChangeListener : access Standard.Java.Util.Prefs.NodeChangeListener.Typ'Class);

   procedure RemoveNodeChangeListener (This : access Typ;
                                       P1_NodeChangeListener : access Standard.Java.Util.Prefs.NodeChangeListener.Typ'Class);

   --  protected
   procedure PutSpi (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   --  protected
   function GetSpi (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.String.Typ'Class is abstract;

   --  protected
   procedure RemoveSpi (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   --  protected
   procedure RemoveNodeSpi (This : access Typ) is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  protected
   function KeysSpi (This : access Typ)
                     return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  protected
   function ChildrenNamesSpi (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  protected
   function GetChild (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Util.Prefs.AbstractPreferences.Typ'Class;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  protected
   function ChildSpi (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Util.Prefs.AbstractPreferences.Typ'Class is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Sync (This : access Typ);
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  protected
   procedure SyncSpi (This : access Typ) is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  protected
   procedure FlushSpi (This : access Typ) is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   --  protected
   function IsRemoved (This : access Typ)
                       return Java.Boolean;

   procedure ExportNode (This : access Typ;
                         P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Util.Prefs.BackingStoreException.Except

   procedure ExportSubtree (This : access Typ;
                            P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Util.Prefs.BackingStoreException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractPreferences);
   pragma Export (Java, Put, "put");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, PutInt, "putInt");
   pragma Export (Java, GetInt, "getInt");
   pragma Export (Java, PutLong, "putLong");
   pragma Export (Java, GetLong, "getLong");
   pragma Export (Java, PutBoolean, "putBoolean");
   pragma Export (Java, GetBoolean, "getBoolean");
   pragma Export (Java, PutFloat, "putFloat");
   pragma Export (Java, GetFloat, "getFloat");
   pragma Export (Java, PutDouble, "putDouble");
   pragma Export (Java, GetDouble, "getDouble");
   pragma Export (Java, PutByteArray, "putByteArray");
   pragma Export (Java, GetByteArray, "getByteArray");
   pragma Export (Java, Keys, "keys");
   pragma Export (Java, ChildrenNames, "childrenNames");
   pragma Export (Java, CachedChildren, "cachedChildren");
   pragma Export (Java, Parent, "parent");
   pragma Export (Java, Node, "node");
   pragma Export (Java, NodeExists, "nodeExists");
   pragma Export (Java, RemoveNode, "removeNode");
   pragma Export (Java, Name, "name");
   pragma Export (Java, AbsolutePath, "absolutePath");
   pragma Export (Java, IsUserNode, "isUserNode");
   pragma Export (Java, AddPreferenceChangeListener, "addPreferenceChangeListener");
   pragma Export (Java, RemovePreferenceChangeListener, "removePreferenceChangeListener");
   pragma Export (Java, AddNodeChangeListener, "addNodeChangeListener");
   pragma Export (Java, RemoveNodeChangeListener, "removeNodeChangeListener");
   pragma Export (Java, PutSpi, "putSpi");
   pragma Export (Java, GetSpi, "getSpi");
   pragma Export (Java, RemoveSpi, "removeSpi");
   pragma Export (Java, RemoveNodeSpi, "removeNodeSpi");
   pragma Export (Java, KeysSpi, "keysSpi");
   pragma Export (Java, ChildrenNamesSpi, "childrenNamesSpi");
   pragma Export (Java, GetChild, "getChild");
   pragma Export (Java, ChildSpi, "childSpi");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, Sync, "sync");
   pragma Export (Java, SyncSpi, "syncSpi");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, FlushSpi, "flushSpi");
   pragma Export (Java, IsRemoved, "isRemoved");
   pragma Export (Java, ExportNode, "exportNode");
   pragma Export (Java, ExportSubtree, "exportSubtree");

end Java.Util.Prefs.AbstractPreferences;
pragma Import (Java, Java.Util.Prefs.AbstractPreferences, "java.util.prefs.AbstractPreferences");
pragma Extensions_Allowed (Off);
