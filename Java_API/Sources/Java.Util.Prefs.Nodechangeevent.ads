pragma Extensions_Allowed (On);
limited with Java.Util.Prefs.Preferences;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Java.Util.Prefs.NodeChangeEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NodeChangeEvent (P1_Preferences : access Standard.Java.Util.Prefs.Preferences.Typ'Class;
                                 P2_Preferences : access Standard.Java.Util.Prefs.Preferences.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParent (This : access Typ)
                       return access Java.Util.Prefs.Preferences.Typ'Class;

   function GetChild (This : access Typ)
                      return access Java.Util.Prefs.Preferences.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NodeChangeEvent);
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetChild, "getChild");

end Java.Util.Prefs.NodeChangeEvent;
pragma Import (Java, Java.Util.Prefs.NodeChangeEvent, "java.util.prefs.NodeChangeEvent");
pragma Extensions_Allowed (Off);
