pragma Extensions_Allowed (On);
limited with Java.Util.Prefs.NodeChangeEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Util.Prefs.NodeChangeListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ChildAdded (This : access Typ;
                         P1_NodeChangeEvent : access Standard.Java.Util.Prefs.NodeChangeEvent.Typ'Class) is abstract;

   procedure ChildRemoved (This : access Typ;
                           P1_NodeChangeEvent : access Standard.Java.Util.Prefs.NodeChangeEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ChildAdded, "childAdded");
   pragma Export (Java, ChildRemoved, "childRemoved");

end Java.Util.Prefs.NodeChangeListener;
pragma Import (Java, Java.Util.Prefs.NodeChangeListener, "java.util.prefs.NodeChangeListener");
pragma Extensions_Allowed (Off);
