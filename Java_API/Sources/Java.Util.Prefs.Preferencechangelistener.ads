pragma Extensions_Allowed (On);
limited with Java.Util.Prefs.PreferenceChangeEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Java.Util.Prefs.PreferenceChangeListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PreferenceChange (This : access Typ;
                               P1_PreferenceChangeEvent : access Standard.Java.Util.Prefs.PreferenceChangeEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, PreferenceChange, "preferenceChange");

end Java.Util.Prefs.PreferenceChangeListener;
pragma Import (Java, Java.Util.Prefs.PreferenceChangeListener, "java.util.prefs.PreferenceChangeListener");
pragma Extensions_Allowed (Off);
