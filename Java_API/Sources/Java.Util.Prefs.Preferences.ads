pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Prefs.NodeChangeListener;
limited with Java.Util.Prefs.PreferenceChangeListener;
with Java.Lang.Object;

package Java.Util.Prefs.Preferences is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function UserNodeForPackage (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                return access Java.Util.Prefs.Preferences.Typ'Class;

   function SystemNodeForPackage (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                  return access Java.Util.Prefs.Preferences.Typ'Class;

   function UserRoot return access Java.Util.Prefs.Preferences.Typ'Class;

   function SystemRoot return access Java.Util.Prefs.Preferences.Typ'Class;

   --  protected
   function New_Preferences (This : Ref := null)
                             return Ref;

   procedure Put (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.String.Typ'Class is abstract;

   procedure Remove (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Clear (This : access Typ) is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   procedure PutInt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int) is abstract;

   function GetInt (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Int : Java.Int)
                    return Java.Int is abstract;

   procedure PutLong (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Long : Java.Long) is abstract;

   function GetLong (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Long : Java.Long)
                     return Java.Long is abstract;

   procedure PutBoolean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;

   function GetBoolean (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Boolean : Java.Boolean)
                        return Java.Boolean is abstract;

   procedure PutFloat (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Float : Java.Float) is abstract;

   function GetFloat (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Float : Java.Float)
                      return Java.Float is abstract;

   procedure PutDouble (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Double : Java.Double) is abstract;

   function GetDouble (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Double : Java.Double)
                       return Java.Double is abstract;

   procedure PutByteArray (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Byte_Arr : Java.Byte_Arr) is abstract;

   function GetByteArray (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Byte_Arr : Java.Byte_Arr)
                          return Java.Byte_Arr is abstract;

   function Keys (This : access Typ)
                  return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   function ChildrenNames (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   function Parent (This : access Typ)
                    return access Java.Util.Prefs.Preferences.Typ'Class is abstract;

   function Node (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Java.Util.Prefs.Preferences.Typ'Class is abstract;

   function NodeExists (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   procedure RemoveNode (This : access Typ) is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class is abstract;

   function AbsolutePath (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function IsUserNode (This : access Typ)
                        return Java.Boolean is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure Flush (This : access Typ) is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   procedure Sync (This : access Typ) is abstract;
   --  can raise Java.Util.Prefs.BackingStoreException.Except

   procedure AddPreferenceChangeListener (This : access Typ;
                                          P1_PreferenceChangeListener : access Standard.Java.Util.Prefs.PreferenceChangeListener.Typ'Class) is abstract;

   procedure RemovePreferenceChangeListener (This : access Typ;
                                             P1_PreferenceChangeListener : access Standard.Java.Util.Prefs.PreferenceChangeListener.Typ'Class) is abstract;

   procedure AddNodeChangeListener (This : access Typ;
                                    P1_NodeChangeListener : access Standard.Java.Util.Prefs.NodeChangeListener.Typ'Class) is abstract;

   procedure RemoveNodeChangeListener (This : access Typ;
                                       P1_NodeChangeListener : access Standard.Java.Util.Prefs.NodeChangeListener.Typ'Class) is abstract;

   procedure ExportNode (This : access Typ;
                         P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Java.Util.Prefs.BackingStoreException.Except

   procedure ExportSubtree (This : access Typ;
                            P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Java.Util.Prefs.BackingStoreException.Except

   procedure ImportPreferences (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Util.Prefs.InvalidPreferencesFormatException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MAX_KEY_LENGTH : constant Java.Int;

   --  final
   MAX_VALUE_LENGTH : constant Java.Int;

   --  final
   MAX_NAME_LENGTH : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, UserNodeForPackage, "userNodeForPackage");
   pragma Export (Java, SystemNodeForPackage, "systemNodeForPackage");
   pragma Export (Java, UserRoot, "userRoot");
   pragma Export (Java, SystemRoot, "systemRoot");
   pragma Java_Constructor (New_Preferences);
   pragma Export (Java, Put, "put");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, PutInt, "putInt");
   pragma Export (Java, GetInt, "getInt");
   pragma Export (Java, PutLong, "putLong");
   pragma Export (Java, GetLong, "getLong");
   pragma Export (Java, PutBoolean, "putBoolean");
   pragma Export (Java, GetBoolean, "getBoolean");
   pragma Export (Java, PutFloat, "putFloat");
   pragma Export (Java, GetFloat, "getFloat");
   pragma Export (Java, PutDouble, "putDouble");
   pragma Export (Java, GetDouble, "getDouble");
   pragma Export (Java, PutByteArray, "putByteArray");
   pragma Export (Java, GetByteArray, "getByteArray");
   pragma Export (Java, Keys, "keys");
   pragma Export (Java, ChildrenNames, "childrenNames");
   pragma Export (Java, Parent, "parent");
   pragma Export (Java, Node, "node");
   pragma Export (Java, NodeExists, "nodeExists");
   pragma Export (Java, RemoveNode, "removeNode");
   pragma Export (Java, Name, "name");
   pragma Export (Java, AbsolutePath, "absolutePath");
   pragma Export (Java, IsUserNode, "isUserNode");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, Sync, "sync");
   pragma Export (Java, AddPreferenceChangeListener, "addPreferenceChangeListener");
   pragma Export (Java, RemovePreferenceChangeListener, "removePreferenceChangeListener");
   pragma Export (Java, AddNodeChangeListener, "addNodeChangeListener");
   pragma Export (Java, RemoveNodeChangeListener, "removeNodeChangeListener");
   pragma Export (Java, ExportNode, "exportNode");
   pragma Export (Java, ExportSubtree, "exportSubtree");
   pragma Export (Java, ImportPreferences, "importPreferences");
   pragma Import (Java, MAX_KEY_LENGTH, "MAX_KEY_LENGTH");
   pragma Import (Java, MAX_VALUE_LENGTH, "MAX_VALUE_LENGTH");
   pragma Import (Java, MAX_NAME_LENGTH, "MAX_NAME_LENGTH");

end Java.Util.Prefs.Preferences;
pragma Import (Java, Java.Util.Prefs.Preferences, "java.util.prefs.Preferences");
pragma Extensions_Allowed (Off);
