pragma Extensions_Allowed (On);
package Java.Util.Prefs is
   pragma Preelaborate;
end Java.Util.Prefs;
pragma Import (Java, Java.Util.Prefs, "java.util.prefs");
pragma Extensions_Allowed (Off);
