pragma Extensions_Allowed (On);
limited with Java.Util.Comparator;
limited with Java.Util.Iterator;
limited with Java.Util.SortedSet;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.AbstractQueue;
with Java.Util.Collection;
with Java.Util.Queue;

package Java.Util.PriorityQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Queue_I : Java.Util.Queue.Ref)
    is new Java.Util.AbstractQueue.Typ(Collection_I,
                                       Queue_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PriorityQueue (This : Ref := null)
                               return Ref;

   function New_PriorityQueue (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   function New_PriorityQueue (P1_Int : Java.Int;
                               P2_Comparator : access Standard.Java.Util.Comparator.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_PriorityQueue (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_PriorityQueue (P1_PriorityQueue : access Standard.Java.Util.PriorityQueue.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_PriorityQueue (P1_SortedSet : access Standard.Java.Util.SortedSet.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   procedure Clear (This : access Typ);

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Comparator (This : access Typ)
                        return access Java.Util.Comparator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PriorityQueue);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Offer, "offer");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Poll, "poll");
   pragma Import (Java, Comparator, "comparator");

end Java.Util.PriorityQueue;
pragma Import (Java, Java.Util.PriorityQueue, "java.util.PriorityQueue");
pragma Extensions_Allowed (Off);
