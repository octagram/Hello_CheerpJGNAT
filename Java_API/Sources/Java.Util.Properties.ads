pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.PrintStream;
limited with Java.Io.PrintWriter;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Set;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Hashtable;
with Java.Util.Map;

package Java.Util.Properties is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Java.Util.Hashtable.Typ(Serializable_I,
                                   Cloneable_I,
                                   Map_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Defaults : access Java.Util.Properties.Typ'Class;
      pragma Import (Java, Defaults, "defaults");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Properties (This : Ref := null)
                            return Ref;

   function New_Properties (P1_Properties : access Standard.Java.Util.Properties.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function SetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   --  synchronized
   procedure Load (This : access Typ;
                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Load (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Store (This : access Typ;
                    P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Store (This : access Typ;
                    P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure LoadFromXML (This : access Typ;
                          P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Util.InvalidPropertiesFormatException.Except

   --  synchronized
   procedure StoreToXML (This : access Typ;
                         P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure StoreToXML (This : access Typ;
                         P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function PropertyNames (This : access Typ)
                           return access Java.Util.Enumeration.Typ'Class;

   function StringPropertyNames (This : access Typ)
                                 return access Java.Util.Set.Typ'Class;

   procedure List (This : access Typ;
                   P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   procedure List (This : access Typ;
                   P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Properties);
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, Load, "load");
   pragma Import (Java, Store, "store");
   pragma Import (Java, LoadFromXML, "loadFromXML");
   pragma Import (Java, StoreToXML, "storeToXML");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, PropertyNames, "propertyNames");
   pragma Import (Java, StringPropertyNames, "stringPropertyNames");
   pragma Import (Java, List, "list");

end Java.Util.Properties;
pragma Import (Java, Java.Util.Properties, "java.util.Properties");
pragma Extensions_Allowed (Off);
