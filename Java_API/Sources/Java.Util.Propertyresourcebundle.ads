pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Set;
with Java.Lang.Object;
with Java.Util.ResourceBundle;

package Java.Util.PropertyResourceBundle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.ResourceBundle.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyResourceBundle (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;
   --  can raise Java.Io.IOException.Except

   function New_PropertyResourceBundle (P1_Reader : access Standard.Java.Io.Reader.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function HandleGetObject (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function GetKeys (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class;

   --  protected
   function HandleKeySet (This : access Typ)
                          return access Java.Util.Set.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyResourceBundle);
   pragma Import (Java, HandleGetObject, "handleGetObject");
   pragma Import (Java, GetKeys, "getKeys");
   pragma Import (Java, HandleKeySet, "handleKeySet");

end Java.Util.PropertyResourceBundle;
pragma Import (Java, Java.Util.PropertyResourceBundle, "java.util.PropertyResourceBundle");
pragma Extensions_Allowed (Off);
