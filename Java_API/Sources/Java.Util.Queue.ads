pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.Collection;

package Java.Util.Queue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Collection_I : Java.Util.Collection.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean is abstract;

   function Offer (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean is abstract;

   function Remove (This : access Typ)
                    return access Java.Lang.Object.Typ'Class is abstract;

   function Poll (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;

   function Element (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Add, "add");
   pragma Export (Java, Offer, "offer");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Poll, "poll");
   pragma Export (Java, Element, "element");
   pragma Export (Java, Peek, "peek");

end Java.Util.Queue;
pragma Import (Java, Java.Util.Queue, "java.util.Queue");
pragma Extensions_Allowed (Off);
