pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Random is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Random (This : Ref := null)
                        return Ref;

   function New_Random (P1_Long : Java.Long; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure SetSeed (This : access Typ;
                      P1_Long : Java.Long);

   --  protected
   function Next (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Int;

   procedure NextBytes (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);

   function NextInt (This : access Typ)
                     return Java.Int;

   function NextInt (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   function NextLong (This : access Typ)
                      return Java.Long;

   function NextBoolean (This : access Typ)
                         return Java.Boolean;

   function NextFloat (This : access Typ)
                       return Java.Float;

   function NextDouble (This : access Typ)
                        return Java.Double;

   --  synchronized
   function NextGaussian (This : access Typ)
                          return Java.Double;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Random);
   pragma Import (Java, SetSeed, "setSeed");
   pragma Import (Java, Next, "next");
   pragma Import (Java, NextBytes, "nextBytes");
   pragma Import (Java, NextInt, "nextInt");
   pragma Import (Java, NextLong, "nextLong");
   pragma Import (Java, NextBoolean, "nextBoolean");
   pragma Import (Java, NextFloat, "nextFloat");
   pragma Import (Java, NextDouble, "nextDouble");
   pragma Import (Java, NextGaussian, "nextGaussian");

end Java.Util.Random;
pragma Import (Java, Java.Util.Random, "java.util.Random");
pragma Extensions_Allowed (Off);
