pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Java.Util.Regex.Pattern;
with Java.Lang.Object;
with Java.Util.Regex.MatchResult;

package Java.Util.Regex.Matcher is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MatchResult_I : Java.Util.Regex.MatchResult.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Pattern (This : access Typ)
                     return access Java.Util.Regex.Pattern.Typ'Class;

   function ToMatchResult (This : access Typ)
                           return access Java.Util.Regex.MatchResult.Typ'Class;

   function UsePattern (This : access Typ;
                        P1_Pattern : access Standard.Java.Util.Regex.Pattern.Typ'Class)
                        return access Java.Util.Regex.Matcher.Typ'Class;

   function Reset (This : access Typ)
                   return access Java.Util.Regex.Matcher.Typ'Class;

   function Reset (This : access Typ;
                   P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                   return access Java.Util.Regex.Matcher.Typ'Class;

   function Start (This : access Typ)
                   return Java.Int;

   function Start (This : access Typ;
                   P1_Int : Java.Int)
                   return Java.Int;

   function end_K (This : access Typ)
                   return Java.Int;

   function end_K (This : access Typ;
                   P1_Int : Java.Int)
                   return Java.Int;

   function Group (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function Group (This : access Typ;
                   P1_Int : Java.Int)
                   return access Java.Lang.String.Typ'Class;

   function GroupCount (This : access Typ)
                        return Java.Int;

   function Matches (This : access Typ)
                     return Java.Boolean;

   function Find (This : access Typ)
                  return Java.Boolean;

   function Find (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Boolean;

   function LookingAt (This : access Typ)
                       return Java.Boolean;

   function QuoteReplacement (P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.String.Typ'Class;

   function AppendReplacement (This : access Typ;
                               P1_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Util.Regex.Matcher.Typ'Class;

   function AppendTail (This : access Typ;
                        P1_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class)
                        return access Java.Lang.StringBuffer.Typ'Class;

   function ReplaceAll (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.String.Typ'Class;

   function ReplaceFirst (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function Region (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Java.Util.Regex.Matcher.Typ'Class;

   function RegionStart (This : access Typ)
                         return Java.Int;

   function RegionEnd (This : access Typ)
                       return Java.Int;

   function HasTransparentBounds (This : access Typ)
                                  return Java.Boolean;

   function UseTransparentBounds (This : access Typ;
                                  P1_Boolean : Java.Boolean)
                                  return access Java.Util.Regex.Matcher.Typ'Class;

   function HasAnchoringBounds (This : access Typ)
                                return Java.Boolean;

   function UseAnchoringBounds (This : access Typ;
                                P1_Boolean : Java.Boolean)
                                return access Java.Util.Regex.Matcher.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HitEnd (This : access Typ)
                    return Java.Boolean;

   function RequireEnd (This : access Typ)
                        return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Pattern, "pattern");
   pragma Import (Java, ToMatchResult, "toMatchResult");
   pragma Import (Java, UsePattern, "usePattern");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Start, "start");
   pragma Import (Java, end_K, "end");
   pragma Import (Java, Group, "group");
   pragma Import (Java, GroupCount, "groupCount");
   pragma Import (Java, Matches, "matches");
   pragma Import (Java, Find, "find");
   pragma Import (Java, LookingAt, "lookingAt");
   pragma Import (Java, QuoteReplacement, "quoteReplacement");
   pragma Import (Java, AppendReplacement, "appendReplacement");
   pragma Import (Java, AppendTail, "appendTail");
   pragma Import (Java, ReplaceAll, "replaceAll");
   pragma Import (Java, ReplaceFirst, "replaceFirst");
   pragma Import (Java, Region, "region");
   pragma Import (Java, RegionStart, "regionStart");
   pragma Import (Java, RegionEnd, "regionEnd");
   pragma Import (Java, HasTransparentBounds, "hasTransparentBounds");
   pragma Import (Java, UseTransparentBounds, "useTransparentBounds");
   pragma Import (Java, HasAnchoringBounds, "hasAnchoringBounds");
   pragma Import (Java, UseAnchoringBounds, "useAnchoringBounds");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HitEnd, "hitEnd");
   pragma Import (Java, RequireEnd, "requireEnd");

end Java.Util.Regex.Matcher;
pragma Import (Java, Java.Util.Regex.Matcher, "java.util.regex.Matcher");
pragma Extensions_Allowed (Off);
