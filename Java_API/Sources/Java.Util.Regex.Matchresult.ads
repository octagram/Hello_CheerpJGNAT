pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Java.Util.Regex.MatchResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Start (This : access Typ)
                   return Java.Int is abstract;

   function Start (This : access Typ;
                   P1_Int : Java.Int)
                   return Java.Int is abstract;

   function end_K (This : access Typ)
                   return Java.Int is abstract;

   function end_K (This : access Typ;
                   P1_Int : Java.Int)
                   return Java.Int is abstract;

   function Group (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function Group (This : access Typ;
                   P1_Int : Java.Int)
                   return access Java.Lang.String.Typ'Class is abstract;

   function GroupCount (This : access Typ)
                        return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Start, "start");
   pragma Export (Java, end_K, "end");
   pragma Export (Java, Group, "group");
   pragma Export (Java, GroupCount, "groupCount");

end Java.Util.Regex.MatchResult;
pragma Import (Java, Java.Util.Regex.MatchResult, "java.util.regex.MatchResult");
pragma Extensions_Allowed (Off);
