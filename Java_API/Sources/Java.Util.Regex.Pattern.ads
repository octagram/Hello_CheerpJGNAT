pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Util.Regex.Matcher;
with Java.Io.Serializable;
with Java.Lang.Object;

package Java.Util.Regex.Pattern is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Compile (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Util.Regex.Pattern.Typ'Class;

   function Compile (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int)
                     return access Java.Util.Regex.Pattern.Typ'Class;

   function Pattern (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Matcher (This : access Typ;
                     P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                     return access Java.Util.Regex.Matcher.Typ'Class;

   function Flags (This : access Typ)
                   return Java.Int;

   function Matches (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                     return Java.Boolean;

   function Split (This : access Typ;
                   P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                   P2_Int : Java.Int)
                   return Standard.Java.Lang.Object.Ref;

   function Split (This : access Typ;
                   P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                   return Standard.Java.Lang.Object.Ref;

   function Quote (P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNIX_LINES : constant Java.Int;

   --  final
   CASE_INSENSITIVE : constant Java.Int;

   --  final
   COMMENTS : constant Java.Int;

   --  final
   MULTILINE : constant Java.Int;

   --  final
   LITERAL : constant Java.Int;

   --  final
   DOTALL : constant Java.Int;

   --  final
   UNICODE_CASE : constant Java.Int;

   --  final
   CANON_EQ : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Compile, "compile");
   pragma Import (Java, Pattern, "pattern");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Matcher, "matcher");
   pragma Import (Java, Flags, "flags");
   pragma Import (Java, Matches, "matches");
   pragma Import (Java, Split, "split");
   pragma Import (Java, Quote, "quote");
   pragma Import (Java, UNIX_LINES, "UNIX_LINES");
   pragma Import (Java, CASE_INSENSITIVE, "CASE_INSENSITIVE");
   pragma Import (Java, COMMENTS, "COMMENTS");
   pragma Import (Java, MULTILINE, "MULTILINE");
   pragma Import (Java, LITERAL, "LITERAL");
   pragma Import (Java, DOTALL, "DOTALL");
   pragma Import (Java, UNICODE_CASE, "UNICODE_CASE");
   pragma Import (Java, CANON_EQ, "CANON_EQ");

end Java.Util.Regex.Pattern;
pragma Import (Java, Java.Util.Regex.Pattern, "java.util.regex.Pattern");
pragma Extensions_Allowed (Off);
