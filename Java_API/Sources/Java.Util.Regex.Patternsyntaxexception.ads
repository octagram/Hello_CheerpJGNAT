pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.IllegalArgumentException;
with Java.Lang.Object;

package Java.Util.Regex.PatternSyntaxException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.IllegalArgumentException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PatternSyntaxException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                                        P3_Int : Java.Int; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIndex (This : access Typ)
                      return Java.Int;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetPattern (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "java.util.regex.PatternSyntaxException");
   pragma Java_Constructor (New_PatternSyntaxException);
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetPattern, "getPattern");
   pragma Import (Java, GetMessage, "getMessage");

end Java.Util.Regex.PatternSyntaxException;
pragma Import (Java, Java.Util.Regex.PatternSyntaxException, "java.util.regex.PatternSyntaxException");
pragma Extensions_Allowed (Off);
