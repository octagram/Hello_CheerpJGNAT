pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Locale;
with Java.Lang.Object;

package Java.Util.ResourceBundle.Control is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Control (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetControl (P1_List : access Standard.Java.Util.List.Typ'Class)
                        return access Java.Util.ResourceBundle.Control.Typ'Class;

   --  final
   function GetNoFallbackControl (P1_List : access Standard.Java.Util.List.Typ'Class)
                                  return access Java.Util.ResourceBundle.Control.Typ'Class;

   function GetFormats (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Util.List.Typ'Class;

   function GetCandidateLocales (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                 return access Java.Util.List.Typ'Class;

   function GetFallbackLocale (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                               return access Java.Util.Locale.Typ'Class;

   function NewBundle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class;
                       P4_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                       P5_Boolean : Java.Boolean)
                       return access Java.Util.ResourceBundle.Typ'Class;
   --  can raise Java.Lang.IllegalAccessException.Except,
   --  Java.Lang.InstantiationException.Except and
   --  Java.Io.IOException.Except

   function GetTimeToLive (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                           return Java.Long;

   function NeedsReload (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class;
                         P4_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                         P5_ResourceBundle : access Standard.Java.Util.ResourceBundle.Typ'Class;
                         P6_Long : Java.Long)
                         return Java.Boolean;

   function ToBundleName (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function ToResourceName (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FORMAT_DEFAULT : access Java.Util.List.Typ'Class;

   --  final
   FORMAT_CLASS : access Java.Util.List.Typ'Class;

   --  final
   FORMAT_PROPERTIES : access Java.Util.List.Typ'Class;

   --  final
   TTL_DONT_CACHE : constant Java.Long;

   --  final
   TTL_NO_EXPIRATION_CONTROL : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Control);
   pragma Import (Java, GetControl, "getControl");
   pragma Import (Java, GetNoFallbackControl, "getNoFallbackControl");
   pragma Import (Java, GetFormats, "getFormats");
   pragma Import (Java, GetCandidateLocales, "getCandidateLocales");
   pragma Import (Java, GetFallbackLocale, "getFallbackLocale");
   pragma Import (Java, NewBundle, "newBundle");
   pragma Import (Java, GetTimeToLive, "getTimeToLive");
   pragma Import (Java, NeedsReload, "needsReload");
   pragma Import (Java, ToBundleName, "toBundleName");
   pragma Import (Java, ToResourceName, "toResourceName");
   pragma Import (Java, FORMAT_DEFAULT, "FORMAT_DEFAULT");
   pragma Import (Java, FORMAT_CLASS, "FORMAT_CLASS");
   pragma Import (Java, FORMAT_PROPERTIES, "FORMAT_PROPERTIES");
   pragma Import (Java, TTL_DONT_CACHE, "TTL_DONT_CACHE");
   pragma Import (Java, TTL_NO_EXPIRATION_CONTROL, "TTL_NO_EXPIRATION_CONTROL");

end Java.Util.ResourceBundle.Control;
pragma Import (Java, Java.Util.ResourceBundle.Control, "java.util.ResourceBundle$Control");
pragma Extensions_Allowed (Off);
