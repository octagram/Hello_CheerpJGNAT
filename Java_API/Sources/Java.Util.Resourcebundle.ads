pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Locale;
limited with Java.Util.ResourceBundle.Control;
limited with Java.Util.Set;
with Java.Lang.Object;

package Java.Util.ResourceBundle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Parent : access Java.Util.ResourceBundle.Typ'Class;
      pragma Import (Java, Parent, "parent");

   end record;

   function New_ResourceBundle (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetString (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   --  final
   function GetStringArray (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Standard.Java.Lang.Object.Ref;

   --  final
   function GetObject (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   --  protected
   procedure SetParent (This : access Typ;
                        P1_ResourceBundle : access Standard.Java.Util.ResourceBundle.Typ'Class);

   --  final
   function GetBundle (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.ResourceBundle.Typ'Class;

   --  final
   function GetBundle (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Control : access Standard.Java.Util.ResourceBundle.Control.Typ'Class)
                       return access Java.Util.ResourceBundle.Typ'Class;

   --  final
   function GetBundle (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Util.ResourceBundle.Typ'Class;

   --  final
   function GetBundle (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                       P3_Control : access Standard.Java.Util.ResourceBundle.Control.Typ'Class)
                       return access Java.Util.ResourceBundle.Typ'Class;

   function GetBundle (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                       P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                       return access Java.Util.ResourceBundle.Typ'Class;

   function GetBundle (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                       P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                       P4_Control : access Standard.Java.Util.ResourceBundle.Control.Typ'Class)
                       return access Java.Util.ResourceBundle.Typ'Class;

   --  final
   procedure ClearCache ;

   --  final
   procedure ClearCache (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class);

   --  protected
   function HandleGetObject (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function GetKeys (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class is abstract;

   function ContainsKey (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   --  protected
   function HandleKeySet (This : access Typ)
                          return access Java.Util.Set.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ResourceBundle);
   pragma Import (Java, GetString, "getString");
   pragma Import (Java, GetStringArray, "getStringArray");
   pragma Import (Java, GetObject, "getObject");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetBundle, "getBundle");
   pragma Import (Java, ClearCache, "clearCache");
   pragma Export (Java, HandleGetObject, "handleGetObject");
   pragma Export (Java, GetKeys, "getKeys");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, HandleKeySet, "handleKeySet");

end Java.Util.ResourceBundle;
pragma Import (Java, Java.Util.ResourceBundle, "java.util.ResourceBundle");
pragma Extensions_Allowed (Off);
