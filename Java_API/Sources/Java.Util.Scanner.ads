pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.IOException;
limited with Java.Io.InputStream;
limited with Java.Lang.Readable;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Math.BigInteger;
limited with Java.Nio.Channels.ReadableByteChannel;
limited with Java.Util.Locale;
limited with Java.Util.Regex.MatchResult;
limited with Java.Util.Regex.Pattern;
with Java.Lang.Object;
with Java.Util.Iterator;

package Java.Util.Scanner is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Iterator_I : Java.Util.Iterator.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Scanner (P1_Readable : access Standard.Java.Lang.Readable.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_Scanner (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_Scanner (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_Scanner (P1_File : access Standard.Java.Io.File.Typ'Class; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_Scanner (P1_File : access Standard.Java.Io.File.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.FileNotFoundException.Except

   function New_Scanner (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_Scanner (P1_ReadableByteChannel : access Standard.Java.Nio.Channels.ReadableByteChannel.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_Scanner (P1_ReadableByteChannel : access Standard.Java.Nio.Channels.ReadableByteChannel.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Close (This : access Typ);

   function IoException (This : access Typ)
                         return access Java.Io.IOException.Typ'Class;

   function Delimiter (This : access Typ)
                       return access Java.Util.Regex.Pattern.Typ'Class;

   function UseDelimiter (This : access Typ;
                          P1_Pattern : access Standard.Java.Util.Regex.Pattern.Typ'Class)
                          return access Java.Util.Scanner.Typ'Class;

   function UseDelimiter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Util.Scanner.Typ'Class;

   function Locale (This : access Typ)
                    return access Java.Util.Locale.Typ'Class;

   function UseLocale (This : access Typ;
                       P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Util.Scanner.Typ'Class;

   function Radix (This : access Typ)
                   return Java.Int;

   function UseRadix (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Util.Scanner.Typ'Class;

   function Match (This : access Typ)
                   return access Java.Util.Regex.MatchResult.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HasNext (This : access Typ)
                     return Java.Boolean;

   function Next (This : access Typ)
                  return access Java.Lang.String.Typ'Class;

   procedure Remove (This : access Typ);

   function HasNext (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Java.Boolean;

   function Next (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Java.Lang.String.Typ'Class;

   function HasNext (This : access Typ;
                     P1_Pattern : access Standard.Java.Util.Regex.Pattern.Typ'Class)
                     return Java.Boolean;

   function Next (This : access Typ;
                  P1_Pattern : access Standard.Java.Util.Regex.Pattern.Typ'Class)
                  return access Java.Lang.String.Typ'Class;

   function HasNextLine (This : access Typ)
                         return Java.Boolean;

   function NextLine (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function FindInLine (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.String.Typ'Class;

   function FindInLine (This : access Typ;
                        P1_Pattern : access Standard.Java.Util.Regex.Pattern.Typ'Class)
                        return access Java.Lang.String.Typ'Class;

   function FindWithinHorizon (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class;

   function FindWithinHorizon (This : access Typ;
                               P1_Pattern : access Standard.Java.Util.Regex.Pattern.Typ'Class;
                               P2_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class;

   function Skip (This : access Typ;
                  P1_Pattern : access Standard.Java.Util.Regex.Pattern.Typ'Class)
                  return access Java.Util.Scanner.Typ'Class;

   function Skip (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Java.Util.Scanner.Typ'Class;

   function HasNextBoolean (This : access Typ)
                            return Java.Boolean;

   function NextBoolean (This : access Typ)
                         return Java.Boolean;

   function HasNextByte (This : access Typ)
                         return Java.Boolean;

   function HasNextByte (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;

   function NextByte (This : access Typ)
                      return Java.Byte;

   function NextByte (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Byte;

   function HasNextShort (This : access Typ)
                          return Java.Boolean;

   function HasNextShort (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Boolean;

   function NextShort (This : access Typ)
                       return Java.Short;

   function NextShort (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Short;

   function HasNextInt (This : access Typ)
                        return Java.Boolean;

   function HasNextInt (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   function NextInt (This : access Typ)
                     return Java.Int;

   function NextInt (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   function HasNextLong (This : access Typ)
                         return Java.Boolean;

   function HasNextLong (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;

   function NextLong (This : access Typ)
                      return Java.Long;

   function NextLong (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Long;

   function HasNextFloat (This : access Typ)
                          return Java.Boolean;

   function NextFloat (This : access Typ)
                       return Java.Float;

   function HasNextDouble (This : access Typ)
                           return Java.Boolean;

   function NextDouble (This : access Typ)
                        return Java.Double;

   function HasNextBigInteger (This : access Typ)
                               return Java.Boolean;

   function HasNextBigInteger (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Boolean;

   function NextBigInteger (This : access Typ)
                            return access Java.Math.BigInteger.Typ'Class;

   function NextBigInteger (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Math.BigInteger.Typ'Class;

   function HasNextBigDecimal (This : access Typ)
                               return Java.Boolean;

   function NextBigDecimal (This : access Typ)
                            return access Java.Math.BigDecimal.Typ'Class;

   function Reset (This : access Typ)
                   return access Java.Util.Scanner.Typ'Class;

   function Next (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Scanner);
   pragma Import (Java, Close, "close");
   pragma Import (Java, IoException, "ioException");
   pragma Import (Java, Delimiter, "delimiter");
   pragma Import (Java, UseDelimiter, "useDelimiter");
   pragma Import (Java, Locale, "locale");
   pragma Import (Java, UseLocale, "useLocale");
   pragma Import (Java, Radix, "radix");
   pragma Import (Java, UseRadix, "useRadix");
   pragma Import (Java, Match, "match");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HasNext, "hasNext");
   pragma Import (Java, Next, "next");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, HasNextLine, "hasNextLine");
   pragma Import (Java, NextLine, "nextLine");
   pragma Import (Java, FindInLine, "findInLine");
   pragma Import (Java, FindWithinHorizon, "findWithinHorizon");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, HasNextBoolean, "hasNextBoolean");
   pragma Import (Java, NextBoolean, "nextBoolean");
   pragma Import (Java, HasNextByte, "hasNextByte");
   pragma Import (Java, NextByte, "nextByte");
   pragma Import (Java, HasNextShort, "hasNextShort");
   pragma Import (Java, NextShort, "nextShort");
   pragma Import (Java, HasNextInt, "hasNextInt");
   pragma Import (Java, NextInt, "nextInt");
   pragma Import (Java, HasNextLong, "hasNextLong");
   pragma Import (Java, NextLong, "nextLong");
   pragma Import (Java, HasNextFloat, "hasNextFloat");
   pragma Import (Java, NextFloat, "nextFloat");
   pragma Import (Java, HasNextDouble, "hasNextDouble");
   pragma Import (Java, NextDouble, "nextDouble");
   pragma Import (Java, HasNextBigInteger, "hasNextBigInteger");
   pragma Import (Java, NextBigInteger, "nextBigInteger");
   pragma Import (Java, HasNextBigDecimal, "hasNextBigDecimal");
   pragma Import (Java, NextBigDecimal, "nextBigDecimal");
   pragma Import (Java, Reset, "reset");

end Java.Util.Scanner;
pragma Import (Java, Java.Util.Scanner, "java.util.Scanner");
pragma Extensions_Allowed (Off);
