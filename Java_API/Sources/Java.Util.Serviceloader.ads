pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Util.Iterator;
with Java.Lang.Iterable;
with Java.Lang.Object;

package Java.Util.ServiceLoader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Iterable_I : Java.Lang.Iterable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reload (This : access Typ);

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function Load (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                  P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                  return access Java.Util.ServiceLoader.Typ'Class;

   function Load (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                  return access Java.Util.ServiceLoader.Typ'Class;

   function LoadInstalled (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Util.ServiceLoader.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Reload, "reload");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, Load, "load");
   pragma Import (Java, LoadInstalled, "loadInstalled");
   pragma Import (Java, ToString, "toString");

end Java.Util.ServiceLoader;
pragma Import (Java, Java.Util.ServiceLoader, "java.util.ServiceLoader");
pragma Extensions_Allowed (Off);
