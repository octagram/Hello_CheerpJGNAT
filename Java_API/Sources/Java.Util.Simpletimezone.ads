pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Date;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.TimeZone;

package Java.Util.SimpleTimeZone is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Util.TimeZone.Typ(Serializable_I,
                                  Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleTimeZone (P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_SimpleTimeZone (P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int;
                                P7_Int : Java.Int;
                                P8_Int : Java.Int;
                                P9_Int : Java.Int;
                                P10_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_SimpleTimeZone (P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int;
                                P7_Int : Java.Int;
                                P8_Int : Java.Int;
                                P9_Int : Java.Int;
                                P10_Int : Java.Int;
                                P11_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_SimpleTimeZone (P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int;
                                P7_Int : Java.Int;
                                P8_Int : Java.Int;
                                P9_Int : Java.Int;
                                P10_Int : Java.Int;
                                P11_Int : Java.Int;
                                P12_Int : Java.Int;
                                P13_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetStartYear (This : access Typ;
                           P1_Int : Java.Int);

   procedure SetStartRule (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int);

   procedure SetStartRule (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int);

   procedure SetStartRule (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int;
                           P5_Boolean : Java.Boolean);

   procedure SetEndRule (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int);

   procedure SetEndRule (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);

   procedure SetEndRule (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Boolean : Java.Boolean);

   function GetOffset (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Int;

   function GetOffset (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int)
                       return Java.Int;

   function GetRawOffset (This : access Typ)
                          return Java.Int;

   procedure SetRawOffset (This : access Typ;
                           P1_Int : Java.Int);

   procedure SetDSTSavings (This : access Typ;
                            P1_Int : Java.Int);

   function GetDSTSavings (This : access Typ)
                           return Java.Int;

   function UseDaylightTime (This : access Typ)
                             return Java.Boolean;

   function InDaylightTime (This : access Typ;
                            P1_Date : access Standard.Java.Util.Date.Typ'Class)
                            return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HasSameRules (This : access Typ;
                          P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class)
                          return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WALL_TIME : constant Java.Int;

   --  final
   STANDARD_TIME : constant Java.Int;

   --  final
   UTC_TIME : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleTimeZone);
   pragma Import (Java, SetStartYear, "setStartYear");
   pragma Import (Java, SetStartRule, "setStartRule");
   pragma Import (Java, SetEndRule, "setEndRule");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetRawOffset, "getRawOffset");
   pragma Import (Java, SetRawOffset, "setRawOffset");
   pragma Import (Java, SetDSTSavings, "setDSTSavings");
   pragma Import (Java, GetDSTSavings, "getDSTSavings");
   pragma Import (Java, UseDaylightTime, "useDaylightTime");
   pragma Import (Java, InDaylightTime, "inDaylightTime");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HasSameRules, "hasSameRules");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, WALL_TIME, "WALL_TIME");
   pragma Import (Java, STANDARD_TIME, "STANDARD_TIME");
   pragma Import (Java, UTC_TIME, "UTC_TIME");

end Java.Util.SimpleTimeZone;
pragma Import (Java, Java.Util.SimpleTimeZone, "java.util.SimpleTimeZone");
pragma Extensions_Allowed (Off);
