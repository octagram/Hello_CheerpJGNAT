pragma Extensions_Allowed (On);
limited with Java.Util.Collection;
limited with Java.Util.Comparator;
limited with Java.Util.Set;
with Java.Lang.Object;
with Java.Util.Map;

package Java.Util.SortedMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Map_I : Java.Util.Map.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Comparator (This : access Typ)
                        return access Java.Util.Comparator.Typ'Class is abstract;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.SortedMap.Typ'Class is abstract;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class is abstract;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class is abstract;

   function FirstKey (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function LastKey (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class is abstract;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class is abstract;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Comparator, "comparator");
   pragma Export (Java, SubMap, "subMap");
   pragma Export (Java, HeadMap, "headMap");
   pragma Export (Java, TailMap, "tailMap");
   pragma Export (Java, FirstKey, "firstKey");
   pragma Export (Java, LastKey, "lastKey");
   pragma Export (Java, KeySet, "keySet");
   pragma Export (Java, Values, "values");
   pragma Export (Java, EntrySet, "entrySet");

end Java.Util.SortedMap;
pragma Import (Java, Java.Util.SortedMap, "java.util.SortedMap");
pragma Extensions_Allowed (Off);
