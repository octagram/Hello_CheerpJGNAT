pragma Extensions_Allowed (On);
limited with Java.Util.Comparator;
with Java.Lang.Object;
with Java.Util.Set;

package Java.Util.SortedSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Set_I : Java.Util.Set.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Comparator (This : access Typ)
                        return access Java.Util.Comparator.Typ'Class is abstract;

   function SubSet (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.SortedSet.Typ'Class is abstract;

   function HeadSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedSet.Typ'Class is abstract;

   function TailSet (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedSet.Typ'Class is abstract;

   function First (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function Last (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Comparator, "comparator");
   pragma Export (Java, SubSet, "subSet");
   pragma Export (Java, HeadSet, "headSet");
   pragma Export (Java, TailSet, "tailSet");
   pragma Export (Java, First, "first");
   pragma Export (Java, Last, "last");

end Java.Util.SortedSet;
pragma Import (Java, Java.Util.SortedSet, "java.util.SortedSet");
pragma Extensions_Allowed (Off);
