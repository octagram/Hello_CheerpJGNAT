pragma Extensions_Allowed (On);
limited with Java.Util.Locale;
with Java.Lang.Object;

package Java.Util.Spi.LocaleServiceProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_LocaleServiceProvider (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAvailableLocales (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LocaleServiceProvider);
   pragma Export (Java, GetAvailableLocales, "getAvailableLocales");

end Java.Util.Spi.LocaleServiceProvider;
pragma Import (Java, Java.Util.Spi.LocaleServiceProvider, "java.util.spi.LocaleServiceProvider");
pragma Extensions_Allowed (Off);
