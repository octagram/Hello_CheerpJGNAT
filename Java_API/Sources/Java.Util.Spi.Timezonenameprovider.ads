pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Lang.Object;
with Java.Util.Spi.LocaleServiceProvider;

package Java.Util.Spi.TimeZoneNameProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Util.Spi.LocaleServiceProvider.Typ
      with null record;

   --  protected
   function New_TimeZoneNameProvider (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDisplayName (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Boolean : Java.Boolean;
                            P3_Int : Java.Int;
                            P4_Locale : access Standard.Java.Util.Locale.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TimeZoneNameProvider);
   pragma Export (Java, GetDisplayName, "getDisplayName");

end Java.Util.Spi.TimeZoneNameProvider;
pragma Import (Java, Java.Util.Spi.TimeZoneNameProvider, "java.util.spi.TimeZoneNameProvider");
pragma Extensions_Allowed (Off);
