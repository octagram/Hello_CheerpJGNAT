pragma Extensions_Allowed (On);
package Java.Util.Spi is
   pragma Preelaborate;
end Java.Util.Spi;
pragma Import (Java, Java.Util.Spi, "java.util.spi");
pragma Extensions_Allowed (Off);
