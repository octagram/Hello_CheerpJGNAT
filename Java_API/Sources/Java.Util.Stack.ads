pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Collection;
with Java.Util.List;
with Java.Util.RandomAccess;
with Java.Util.Vector;

package Java.Util.Stack is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            List_I : Java.Util.List.Ref;
            RandomAccess_I : Java.Util.RandomAccess.Ref)
    is new Java.Util.Vector.Typ(Serializable_I,
                                Cloneable_I,
                                Collection_I,
                                List_I,
                                RandomAccess_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Stack (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Push (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function Pop (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function Peek (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function Empty (This : access Typ)
                   return Java.Boolean;

   --  synchronized
   function Search (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Stack);
   pragma Import (Java, Push, "push");
   pragma Import (Java, Pop, "pop");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, Empty, "empty");
   pragma Import (Java, Search, "search");

end Java.Util.Stack;
pragma Import (Java, Java.Util.Stack, "java.util.Stack");
pragma Extensions_Allowed (Off);
