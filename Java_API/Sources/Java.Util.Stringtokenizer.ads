pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Util.Enumeration;

package Java.Util.StringTokenizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Enumeration_I : Java.Util.Enumeration.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringTokenizer (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   function New_StringTokenizer (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_StringTokenizer (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function HasMoreTokens (This : access Typ)
                           return Java.Boolean;

   function NextToken (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function NextToken (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function HasMoreElements (This : access Typ)
                             return Java.Boolean;

   function NextElement (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function CountTokens (This : access Typ)
                         return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringTokenizer);
   pragma Import (Java, HasMoreTokens, "hasMoreTokens");
   pragma Import (Java, NextToken, "nextToken");
   pragma Import (Java, HasMoreElements, "hasMoreElements");
   pragma Import (Java, NextElement, "nextElement");
   pragma Import (Java, CountTokens, "countTokens");

end Java.Util.StringTokenizer;
pragma Import (Java, Java.Util.StringTokenizer, "java.util.StringTokenizer");
pragma Extensions_Allowed (Off);
