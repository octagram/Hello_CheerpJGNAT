pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Date;
limited with Java.Util.TimerTask;
with Java.Lang.Object;

package Java.Util.Timer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Timer (This : Ref := null)
                       return Ref;

   function New_Timer (P1_Boolean : Java.Boolean; 
                       This : Ref := null)
                       return Ref;

   function New_Timer (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_Timer (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Boolean : Java.Boolean; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Schedule (This : access Typ;
                       P1_TimerTask : access Standard.Java.Util.TimerTask.Typ'Class;
                       P2_Long : Java.Long);

   procedure Schedule (This : access Typ;
                       P1_TimerTask : access Standard.Java.Util.TimerTask.Typ'Class;
                       P2_Date : access Standard.Java.Util.Date.Typ'Class);

   procedure Schedule (This : access Typ;
                       P1_TimerTask : access Standard.Java.Util.TimerTask.Typ'Class;
                       P2_Long : Java.Long;
                       P3_Long : Java.Long);

   procedure Schedule (This : access Typ;
                       P1_TimerTask : access Standard.Java.Util.TimerTask.Typ'Class;
                       P2_Date : access Standard.Java.Util.Date.Typ'Class;
                       P3_Long : Java.Long);

   procedure ScheduleAtFixedRate (This : access Typ;
                                  P1_TimerTask : access Standard.Java.Util.TimerTask.Typ'Class;
                                  P2_Long : Java.Long;
                                  P3_Long : Java.Long);

   procedure ScheduleAtFixedRate (This : access Typ;
                                  P1_TimerTask : access Standard.Java.Util.TimerTask.Typ'Class;
                                  P2_Date : access Standard.Java.Util.Date.Typ'Class;
                                  P3_Long : Java.Long);

   procedure Cancel (This : access Typ);

   function Purge (This : access Typ)
                   return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Timer);
   pragma Import (Java, Schedule, "schedule");
   pragma Import (Java, ScheduleAtFixedRate, "scheduleAtFixedRate");
   pragma Import (Java, Cancel, "cancel");
   pragma Import (Java, Purge, "purge");

end Java.Util.Timer;
pragma Import (Java, Java.Util.Timer, "java.util.Timer");
pragma Extensions_Allowed (Off);
