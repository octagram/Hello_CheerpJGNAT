pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Lang.Runnable;

package Java.Util.TimerTask is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Runnable_I : Java.Lang.Runnable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_TimerTask (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Run (This : access Typ) is abstract;

   function Cancel (This : access Typ)
                    return Java.Boolean;

   function ScheduledExecutionTime (This : access Typ)
                                    return Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TimerTask);
   pragma Export (Java, Run, "run");
   pragma Export (Java, Cancel, "cancel");
   pragma Export (Java, ScheduledExecutionTime, "scheduledExecutionTime");

end Java.Util.TimerTask;
pragma Import (Java, Java.Util.TimerTask, "java.util.TimerTask");
pragma Extensions_Allowed (Off);
