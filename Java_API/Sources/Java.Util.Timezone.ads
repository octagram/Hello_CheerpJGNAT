pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Date;
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Util.TimeZone is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_TimeZone (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOffset (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int)
                       return Java.Int is abstract;

   function GetOffset (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Int;

   procedure SetRawOffset (This : access Typ;
                           P1_Int : Java.Int) is abstract;

   function GetRawOffset (This : access Typ)
                          return Java.Int is abstract;

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   procedure SetID (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  final
   function GetDisplayName (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   --  final
   function GetDisplayName (This : access Typ;
                            P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  final
   function GetDisplayName (This : access Typ;
                            P1_Boolean : Java.Boolean;
                            P2_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   function GetDisplayName (This : access Typ;
                            P1_Boolean : Java.Boolean;
                            P2_Int : Java.Int;
                            P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetDSTSavings (This : access Typ)
                           return Java.Int;

   function UseDaylightTime (This : access Typ)
                             return Java.Boolean is abstract;

   function InDaylightTime (This : access Typ;
                            P1_Date : access Standard.Java.Util.Date.Typ'Class)
                            return Java.Boolean is abstract;

   --  synchronized
   function GetTimeZone (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Util.TimeZone.Typ'Class;

   --  synchronized
   function GetAvailableIDs (P1_Int : Java.Int)
                             return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetAvailableIDs return Standard.Java.Lang.Object.Ref;

   function GetDefault return access Java.Util.TimeZone.Typ'Class;

   procedure SetDefault (P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class);

   function HasSameRules (This : access Typ;
                          P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class)
                          return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SHORT : constant Java.Int;

   --  final
   LONG : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TimeZone);
   pragma Export (Java, GetOffset, "getOffset");
   pragma Export (Java, SetRawOffset, "setRawOffset");
   pragma Export (Java, GetRawOffset, "getRawOffset");
   pragma Export (Java, GetID, "getID");
   pragma Export (Java, SetID, "setID");
   pragma Export (Java, GetDisplayName, "getDisplayName");
   pragma Export (Java, GetDSTSavings, "getDSTSavings");
   pragma Export (Java, UseDaylightTime, "useDaylightTime");
   pragma Export (Java, InDaylightTime, "inDaylightTime");
   pragma Export (Java, GetTimeZone, "getTimeZone");
   pragma Export (Java, GetAvailableIDs, "getAvailableIDs");
   pragma Export (Java, GetDefault, "getDefault");
   pragma Export (Java, SetDefault, "setDefault");
   pragma Export (Java, HasSameRules, "hasSameRules");
   pragma Export (Java, Clone, "clone");
   pragma Import (Java, SHORT, "SHORT");
   pragma Import (Java, LONG, "LONG");

end Java.Util.TimeZone;
pragma Import (Java, Java.Util.TimeZone, "java.util.TimeZone");
pragma Extensions_Allowed (Off);
