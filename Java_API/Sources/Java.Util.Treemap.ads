pragma Extensions_Allowed (On);
limited with Java.Util.Collection;
limited with Java.Util.Comparator;
limited with Java.Util.Map.Entry_K;
limited with Java.Util.NavigableSet;
limited with Java.Util.Set;
limited with Java.Util.SortedMap;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractMap;
with Java.Util.Map;
with Java.Util.NavigableMap;

package Java.Util.TreeMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref;
            NavigableMap_I : Java.Util.NavigableMap.Ref)
    is new Java.Util.AbstractMap.Typ(Map_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeMap (This : Ref := null)
                         return Ref;

   function New_TreeMap (P1_Comparator : access Standard.Java.Util.Comparator.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_TreeMap (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_TreeMap (P1_SortedMap : access Standard.Java.Util.SortedMap.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Size (This : access Typ)
                  return Java.Int;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Comparator (This : access Typ)
                        return access Java.Util.Comparator.Typ'Class;

   function FirstKey (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function LastKey (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function FirstEntry (This : access Typ)
                        return access Java.Util.Map.Entry_K.Typ'Class;

   function LastEntry (This : access Typ)
                       return access Java.Util.Map.Entry_K.Typ'Class;

   function PollFirstEntry (This : access Typ)
                            return access Java.Util.Map.Entry_K.Typ'Class;

   function PollLastEntry (This : access Typ)
                           return access Java.Util.Map.Entry_K.Typ'Class;

   function LowerEntry (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Util.Map.Entry_K.Typ'Class;

   function LowerKey (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function FloorEntry (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Util.Map.Entry_K.Typ'Class;

   function FloorKey (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function CeilingEntry (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Util.Map.Entry_K.Typ'Class;

   function CeilingKey (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function HigherEntry (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Util.Map.Entry_K.Typ'Class;

   function HigherKey (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function NavigableKeySet (This : access Typ)
                             return access Java.Util.NavigableSet.Typ'Class;

   function DescendingKeySet (This : access Typ)
                              return access Java.Util.NavigableSet.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function DescendingMap (This : access Typ)
                           return access Java.Util.NavigableMap.Typ'Class;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Boolean : Java.Boolean;
                    P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P4_Boolean : Java.Boolean)
                    return access Java.Util.NavigableMap.Typ'Class;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableMap.Typ'Class;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Java.Util.NavigableMap.Typ'Class;

   function SubMap (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Util.SortedMap.Typ'Class;

   function HeadMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class;

   function TailMap (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Util.SortedMap.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeMap);
   pragma Import (Java, Size, "size");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Comparator, "comparator");
   pragma Import (Java, FirstKey, "firstKey");
   pragma Import (Java, LastKey, "lastKey");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, FirstEntry, "firstEntry");
   pragma Import (Java, LastEntry, "lastEntry");
   pragma Import (Java, PollFirstEntry, "pollFirstEntry");
   pragma Import (Java, PollLastEntry, "pollLastEntry");
   pragma Import (Java, LowerEntry, "lowerEntry");
   pragma Import (Java, LowerKey, "lowerKey");
   pragma Import (Java, FloorEntry, "floorEntry");
   pragma Import (Java, FloorKey, "floorKey");
   pragma Import (Java, CeilingEntry, "ceilingEntry");
   pragma Import (Java, CeilingKey, "ceilingKey");
   pragma Import (Java, HigherEntry, "higherEntry");
   pragma Import (Java, HigherKey, "higherKey");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, NavigableKeySet, "navigableKeySet");
   pragma Import (Java, DescendingKeySet, "descendingKeySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, DescendingMap, "descendingMap");
   pragma Import (Java, SubMap, "subMap");
   pragma Import (Java, HeadMap, "headMap");
   pragma Import (Java, TailMap, "tailMap");

end Java.Util.TreeMap;
pragma Import (Java, Java.Util.TreeMap, "java.util.TreeMap");
pragma Extensions_Allowed (Off);
