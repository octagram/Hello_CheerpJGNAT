pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Java.Util.UUID is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UUID (P1_Long : Java.Long;
                      P2_Long : Java.Long; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function RandomUUID return access Java.Util.UUID.Typ'Class;

   function NameUUIDFromBytes (P1_Byte_Arr : Java.Byte_Arr)
                               return access Java.Util.UUID.Typ'Class;

   function FromString (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Util.UUID.Typ'Class;

   function GetLeastSignificantBits (This : access Typ)
                                     return Java.Long;

   function GetMostSignificantBits (This : access Typ)
                                    return Java.Long;

   function Version (This : access Typ)
                     return Java.Int;

   function Variant (This : access Typ)
                     return Java.Int;

   function Timestamp (This : access Typ)
                       return Java.Long;

   function ClockSequence (This : access Typ)
                           return Java.Int;

   function Node (This : access Typ)
                  return Java.Long;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function CompareTo (This : access Typ;
                       P1_UUID : access Standard.Java.Util.UUID.Typ'Class)
                       return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UUID);
   pragma Import (Java, RandomUUID, "randomUUID");
   pragma Import (Java, NameUUIDFromBytes, "nameUUIDFromBytes");
   pragma Import (Java, FromString, "fromString");
   pragma Import (Java, GetLeastSignificantBits, "getLeastSignificantBits");
   pragma Import (Java, GetMostSignificantBits, "getMostSignificantBits");
   pragma Import (Java, Version, "version");
   pragma Import (Java, Variant, "variant");
   pragma Import (Java, Timestamp, "timestamp");
   pragma Import (Java, ClockSequence, "clockSequence");
   pragma Import (Java, Node, "node");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, CompareTo, "compareTo");

end Java.Util.UUID;
pragma Import (Java, Java.Util.UUID, "java.util.UUID");
pragma Extensions_Allowed (Off);
