pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.AbstractList;
with Java.Util.Collection;
with Java.Util.List;
with Java.Util.RandomAccess;

package Java.Util.Vector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            List_I : Java.Util.List.Ref;
            RandomAccess_I : Java.Util.RandomAccess.Ref)
    is new Java.Util.AbstractList.Typ(Collection_I,
                                      List_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ElementData : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ElementData, "elementData");

      --  protected
      ElementCount : Java.Int;
      pragma Import (Java, ElementCount, "elementCount");

      --  protected
      CapacityIncrement : Java.Int;
      pragma Import (Java, CapacityIncrement, "capacityIncrement");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Vector (P1_Int : Java.Int;
                        P2_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_Vector (P1_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_Vector (This : Ref := null)
                        return Ref;

   function New_Vector (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure CopyInto (This : access Typ;
                       P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   --  synchronized
   procedure TrimToSize (This : access Typ);

   --  synchronized
   procedure EnsureCapacity (This : access Typ;
                             P1_Int : Java.Int);

   --  synchronized
   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int);

   --  synchronized
   function Capacity (This : access Typ)
                      return Java.Int;

   --  synchronized
   function Size (This : access Typ)
                  return Java.Int;

   --  synchronized
   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Elements (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   --  synchronized
   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;

   --  synchronized
   function LastIndexOf (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Int;

   --  synchronized
   function LastIndexOf (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;

   --  synchronized
   function ElementAt (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function FirstElement (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function LastElement (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   --  synchronized
   procedure SetElementAt (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int : Java.Int);

   --  synchronized
   procedure RemoveElementAt (This : access Typ;
                              P1_Int : Java.Int);

   --  synchronized
   procedure InsertElementAt (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_Int : Java.Int);

   --  synchronized
   procedure AddElement (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  synchronized
   function RemoveElement (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   --  synchronized
   procedure RemoveAllElements (This : access Typ);

   --  synchronized
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function ToArray (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   --  synchronized
   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  synchronized
   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   --  synchronized
   function ContainsAll (This : access Typ;
                         P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                         return Java.Boolean;

   --  synchronized
   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   --  synchronized
   function RemoveAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   --  synchronized
   function RetainAll (This : access Typ;
                       P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                       return Java.Boolean;

   --  synchronized
   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   --  synchronized
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  synchronized
   function HashCode (This : access Typ)
                      return Java.Int;

   --  synchronized
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  synchronized
   function SubList (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Java.Util.List.Typ'Class;

   --  protected  synchronized
   procedure RemoveRange (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Vector);
   pragma Import (Java, CopyInto, "copyInto");
   pragma Import (Java, TrimToSize, "trimToSize");
   pragma Import (Java, EnsureCapacity, "ensureCapacity");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, Capacity, "capacity");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Elements, "elements");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, ElementAt, "elementAt");
   pragma Import (Java, FirstElement, "firstElement");
   pragma Import (Java, LastElement, "lastElement");
   pragma Import (Java, SetElementAt, "setElementAt");
   pragma Import (Java, RemoveElementAt, "removeElementAt");
   pragma Import (Java, InsertElementAt, "insertElementAt");
   pragma Import (Java, AddElement, "addElement");
   pragma Import (Java, RemoveElement, "removeElement");
   pragma Import (Java, RemoveAllElements, "removeAllElements");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, ContainsAll, "containsAll");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, RetainAll, "retainAll");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SubList, "subList");
   pragma Import (Java, RemoveRange, "removeRange");

end Java.Util.Vector;
pragma Import (Java, Java.Util.Vector, "java.util.Vector");
pragma Extensions_Allowed (Off);
