pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.Zip.Checksum;

package Java.Util.Zip.Adler32 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Checksum_I : Java.Util.Zip.Checksum.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Adler32 (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Update (This : access Typ;
                     P1_Int : Java.Int);

   procedure Update (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);

   procedure Update (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr);

   procedure Reset (This : access Typ);

   function GetValue (This : access Typ)
                      return Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Adler32);
   pragma Import (Java, Update, "update");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, GetValue, "getValue");

end Java.Util.Zip.Adler32;
pragma Import (Java, Java.Util.Zip.Adler32, "java.util.zip.Adler32");
pragma Extensions_Allowed (Off);
