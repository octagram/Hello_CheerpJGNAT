pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Util.Zip.Checksum;
with Java.Io.Closeable;
with Java.Io.FilterInputStream;
with Java.Lang.Object;

package Java.Util.Zip.CheckedInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.FilterInputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CheckedInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                    P2_Checksum : access Standard.Java.Util.Zip.Checksum.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function GetChecksum (This : access Typ)
                         return access Java.Util.Zip.Checksum.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CheckedInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, GetChecksum, "getChecksum");

end Java.Util.Zip.CheckedInputStream;
pragma Import (Java, Java.Util.Zip.CheckedInputStream, "java.util.zip.CheckedInputStream");
pragma Extensions_Allowed (Off);
