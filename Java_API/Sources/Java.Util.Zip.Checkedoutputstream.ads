pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Util.Zip.Checksum;
with Java.Io.Closeable;
with Java.Io.FilterOutputStream;
with Java.Io.Flushable;
with Java.Lang.Object;

package Java.Util.Zip.CheckedOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.FilterOutputStream.Typ(Closeable_I,
                                          Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CheckedOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                     P2_Checksum : access Standard.Java.Util.Zip.Checksum.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function GetChecksum (This : access Typ)
                         return access Java.Util.Zip.Checksum.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CheckedOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, GetChecksum, "getChecksum");

end Java.Util.Zip.CheckedOutputStream;
pragma Import (Java, Java.Util.Zip.CheckedOutputStream, "java.util.zip.CheckedOutputStream");
pragma Extensions_Allowed (Off);
