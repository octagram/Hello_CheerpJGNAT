pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.Zip.Checksum is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Update (This : access Typ;
                     P1_Int : Java.Int) is abstract;

   procedure Update (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int) is abstract;

   function GetValue (This : access Typ)
                      return Java.Long is abstract;

   procedure Reset (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Update, "update");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, Reset, "reset");

end Java.Util.Zip.Checksum;
pragma Import (Java, Java.Util.Zip.Checksum, "java.util.zip.Checksum");
pragma Extensions_Allowed (Off);
