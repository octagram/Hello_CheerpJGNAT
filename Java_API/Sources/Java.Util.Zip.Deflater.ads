pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.Zip.Deflater is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Deflater (P1_Int : Java.Int;
                          P2_Boolean : Java.Boolean; 
                          This : Ref := null)
                          return Ref;

   function New_Deflater (P1_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   function New_Deflater (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetInput (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int);

   procedure SetInput (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr);

   procedure SetDictionary (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int);

   procedure SetDictionary (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr);

   procedure SetStrategy (This : access Typ;
                          P1_Int : Java.Int);

   procedure SetLevel (This : access Typ;
                       P1_Int : Java.Int);

   function NeedsInput (This : access Typ)
                        return Java.Boolean;

   procedure Finish (This : access Typ);

   function Finished (This : access Typ)
                      return Java.Boolean;

   function Deflate (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int)
                     return Java.Int;

   function Deflate (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr)
                     return Java.Int;

   function GetAdler (This : access Typ)
                      return Java.Int;

   function GetTotalIn (This : access Typ)
                        return Java.Int;

   function GetBytesRead (This : access Typ)
                          return Java.Long;

   function GetTotalOut (This : access Typ)
                         return Java.Int;

   function GetBytesWritten (This : access Typ)
                             return Java.Long;

   procedure Reset (This : access Typ);

   procedure end_K (This : access Typ);

   --  protected
   procedure Finalize (This : access Typ);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFLATED : constant Java.Int;

   --  final
   NO_COMPRESSION : constant Java.Int;

   --  final
   BEST_SPEED : constant Java.Int;

   --  final
   BEST_COMPRESSION : constant Java.Int;

   --  final
   DEFAULT_COMPRESSION : constant Java.Int;

   --  final
   FILTERED : constant Java.Int;

   --  final
   HUFFMAN_ONLY : constant Java.Int;

   --  final
   DEFAULT_STRATEGY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Deflater);
   pragma Import (Java, SetInput, "setInput");
   pragma Import (Java, SetDictionary, "setDictionary");
   pragma Import (Java, SetStrategy, "setStrategy");
   pragma Import (Java, SetLevel, "setLevel");
   pragma Import (Java, NeedsInput, "needsInput");
   pragma Import (Java, Finish, "finish");
   pragma Import (Java, Finished, "finished");
   pragma Import (Java, Deflate, "deflate");
   pragma Import (Java, GetAdler, "getAdler");
   pragma Import (Java, GetTotalIn, "getTotalIn");
   pragma Import (Java, GetBytesRead, "getBytesRead");
   pragma Import (Java, GetTotalOut, "getTotalOut");
   pragma Import (Java, GetBytesWritten, "getBytesWritten");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, end_K, "end");
   pragma Import (Java, Finalize, "finalize");
   pragma Import (Java, DEFLATED, "DEFLATED");
   pragma Import (Java, NO_COMPRESSION, "NO_COMPRESSION");
   pragma Import (Java, BEST_SPEED, "BEST_SPEED");
   pragma Import (Java, BEST_COMPRESSION, "BEST_COMPRESSION");
   pragma Import (Java, DEFAULT_COMPRESSION, "DEFAULT_COMPRESSION");
   pragma Import (Java, FILTERED, "FILTERED");
   pragma Import (Java, HUFFMAN_ONLY, "HUFFMAN_ONLY");
   pragma Import (Java, DEFAULT_STRATEGY, "DEFAULT_STRATEGY");

end Java.Util.Zip.Deflater;
pragma Import (Java, Java.Util.Zip.Deflater, "java.util.zip.Deflater");
pragma Extensions_Allowed (Off);
