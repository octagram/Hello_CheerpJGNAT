pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Util.Zip.Deflater;
with Java.Io.Closeable;
with Java.Io.FilterOutputStream;
with Java.Io.Flushable;
with Java.Lang.Object;

package Java.Util.Zip.DeflaterOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.FilterOutputStream.Typ(Closeable_I,
                                          Flushable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Def : access Java.Util.Zip.Deflater.Typ'Class;
      pragma Import (Java, Def, "def");

      --  protected
      Buf : Java.Byte_Arr;
      pragma Import (Java, Buf, "buf");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DeflaterOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                      P2_Deflater : access Standard.Java.Util.Zip.Deflater.Typ'Class;
                                      P3_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   function New_DeflaterOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                      P2_Deflater : access Standard.Java.Util.Zip.Deflater.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_DeflaterOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Finish (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Deflate (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DeflaterOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Finish, "finish");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Deflate, "deflate");

end Java.Util.Zip.DeflaterOutputStream;
pragma Import (Java, Java.Util.Zip.DeflaterOutputStream, "java.util.zip.DeflaterOutputStream");
pragma Extensions_Allowed (Off);
