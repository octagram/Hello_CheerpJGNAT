pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Util.Zip.CRC32;
with Java.Io.Closeable;
with Java.Lang.Object;
with Java.Util.Zip.InflaterInputStream;

package Java.Util.Zip.GZIPInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Util.Zip.InflaterInputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Crc : access Java.Util.Zip.CRC32.Typ'Class;
      pragma Import (Java, Crc, "crc");

      --  protected
      Eos : Java.Boolean;
      pragma Import (Java, Eos, "eos");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GZIPInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                 P2_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.IOException.Except

   function New_GZIPInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   GZIP_MAGIC : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GZIPInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GZIP_MAGIC, "GZIP_MAGIC");

end Java.Util.Zip.GZIPInputStream;
pragma Import (Java, Java.Util.Zip.GZIPInputStream, "java.util.zip.GZIPInputStream");
pragma Extensions_Allowed (Off);
