pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Util.Zip.CRC32;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Object;
with Java.Util.Zip.DeflaterOutputStream;

package Java.Util.Zip.GZIPOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Util.Zip.DeflaterOutputStream.Typ(Closeable_I,
                                                  Flushable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Crc : access Java.Util.Zip.CRC32.Typ'Class;
      pragma Import (Java, Crc, "crc");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GZIPOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                  P2_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.IOException.Except

   function New_GZIPOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Finish (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GZIPOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Finish, "finish");

end Java.Util.Zip.GZIPOutputStream;
pragma Import (Java, Java.Util.Zip.GZIPOutputStream, "java.util.zip.GZIPOutputStream");
pragma Extensions_Allowed (Off);
