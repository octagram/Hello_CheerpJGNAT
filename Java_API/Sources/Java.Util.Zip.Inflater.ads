pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Java.Util.Zip.Inflater is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Inflater (P1_Boolean : Java.Boolean; 
                          This : Ref := null)
                          return Ref;

   function New_Inflater (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetInput (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int);

   procedure SetInput (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr);

   procedure SetDictionary (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int);

   procedure SetDictionary (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr);

   function GetRemaining (This : access Typ)
                          return Java.Int;

   function NeedsInput (This : access Typ)
                        return Java.Boolean;

   function NeedsDictionary (This : access Typ)
                             return Java.Boolean;

   function Finished (This : access Typ)
                      return Java.Boolean;

   function Inflate (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int)
                     return Java.Int;
   --  can raise Java.Util.Zip.DataFormatException.Except

   function Inflate (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr)
                     return Java.Int;
   --  can raise Java.Util.Zip.DataFormatException.Except

   function GetAdler (This : access Typ)
                      return Java.Int;

   function GetTotalIn (This : access Typ)
                        return Java.Int;

   function GetBytesRead (This : access Typ)
                          return Java.Long;

   function GetTotalOut (This : access Typ)
                         return Java.Int;

   function GetBytesWritten (This : access Typ)
                             return Java.Long;

   procedure Reset (This : access Typ);

   procedure end_K (This : access Typ);

   --  protected
   procedure Finalize (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Inflater);
   pragma Import (Java, SetInput, "setInput");
   pragma Import (Java, SetDictionary, "setDictionary");
   pragma Import (Java, GetRemaining, "getRemaining");
   pragma Import (Java, NeedsInput, "needsInput");
   pragma Import (Java, NeedsDictionary, "needsDictionary");
   pragma Import (Java, Finished, "finished");
   pragma Import (Java, Inflate, "inflate");
   pragma Import (Java, GetAdler, "getAdler");
   pragma Import (Java, GetTotalIn, "getTotalIn");
   pragma Import (Java, GetBytesRead, "getBytesRead");
   pragma Import (Java, GetTotalOut, "getTotalOut");
   pragma Import (Java, GetBytesWritten, "getBytesWritten");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, end_K, "end");
   pragma Import (Java, Finalize, "finalize");

end Java.Util.Zip.Inflater;
pragma Import (Java, Java.Util.Zip.Inflater, "java.util.zip.Inflater");
pragma Extensions_Allowed (Off);
