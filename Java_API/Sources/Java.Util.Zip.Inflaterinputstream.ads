pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Util.Zip.Inflater;
with Java.Io.Closeable;
with Java.Io.FilterInputStream;
with Java.Lang.Object;

package Java.Util.Zip.InflaterInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.FilterInputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Inf : access Java.Util.Zip.Inflater.Typ'Class;
      pragma Import (Java, Inf, "inf");

      --  protected
      Buf : Java.Byte_Arr;
      pragma Import (Java, Buf, "buf");

      --  protected
      Len : Java.Int;
      pragma Import (Java, Len, "len");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InflaterInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                     P2_Inflater : access Standard.Java.Util.Zip.Inflater.Typ'Class;
                                     P3_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   function New_InflaterInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                     P2_Inflater : access Standard.Java.Util.Zip.Inflater.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_InflaterInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Fill (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function MarkSupported (This : access Typ)
                           return Java.Boolean;

   --  synchronized
   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);

   --  synchronized
   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InflaterInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Fill, "fill");
   pragma Import (Java, MarkSupported, "markSupported");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");

end Java.Util.Zip.InflaterInputStream;
pragma Import (Java, Java.Util.Zip.InflaterInputStream, "java.util.zip.InflaterInputStream");
pragma Extensions_Allowed (Off);
