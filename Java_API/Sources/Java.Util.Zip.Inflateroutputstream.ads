pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Util.Zip.Inflater;
with Java.Io.Closeable;
with Java.Io.FilterOutputStream;
with Java.Io.Flushable;
with Java.Lang.Object;

package Java.Util.Zip.InflaterOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.FilterOutputStream.Typ(Closeable_I,
                                          Flushable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      Inf : access Java.Util.Zip.Inflater.Typ'Class;
      pragma Import (Java, Inf, "inf");

      --  protected  final
      Buf : Java.Byte_Arr;
      pragma Import (Java, Buf, "buf");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InflaterOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_InflaterOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                      P2_Inflater : access Standard.Java.Util.Zip.Inflater.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_InflaterOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                      P2_Inflater : access Standard.Java.Util.Zip.Inflater.Typ'Class;
                                      P3_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Finish (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InflaterOutputStream);
   pragma Import (Java, Close, "close");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Finish, "finish");
   pragma Import (Java, Write, "write");

end Java.Util.Zip.InflaterOutputStream;
pragma Import (Java, Java.Util.Zip.InflaterOutputStream, "java.util.zip.InflaterOutputStream");
pragma Extensions_Allowed (Off);
