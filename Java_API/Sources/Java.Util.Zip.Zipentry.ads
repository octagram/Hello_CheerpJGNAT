pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Java.Util.Zip.ZipEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ZipEntry (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_ZipEntry (P1_ZipEntry : access Standard.Java.Util.Zip.ZipEntry.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetTime (This : access Typ;
                      P1_Long : Java.Long);

   function GetTime (This : access Typ)
                     return Java.Long;

   procedure SetSize (This : access Typ;
                      P1_Long : Java.Long);

   function GetSize (This : access Typ)
                     return Java.Long;

   function GetCompressedSize (This : access Typ)
                               return Java.Long;

   procedure SetCompressedSize (This : access Typ;
                                P1_Long : Java.Long);

   procedure SetCrc (This : access Typ;
                     P1_Long : Java.Long);

   function GetCrc (This : access Typ)
                    return Java.Long;

   procedure SetMethod (This : access Typ;
                        P1_Int : Java.Int);

   function GetMethod (This : access Typ)
                       return Java.Int;

   procedure SetExtra (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr);

   function GetExtra (This : access Typ)
                      return Java.Byte_Arr;

   procedure SetComment (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetComment (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function IsDirectory (This : access Typ)
                         return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   STORED : constant Java.Int;

   --  final
   DEFLATED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ZipEntry);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetTime, "setTime");
   pragma Import (Java, GetTime, "getTime");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, GetCompressedSize, "getCompressedSize");
   pragma Import (Java, SetCompressedSize, "setCompressedSize");
   pragma Import (Java, SetCrc, "setCrc");
   pragma Import (Java, GetCrc, "getCrc");
   pragma Import (Java, SetMethod, "setMethod");
   pragma Import (Java, GetMethod, "getMethod");
   pragma Import (Java, SetExtra, "setExtra");
   pragma Import (Java, GetExtra, "getExtra");
   pragma Import (Java, SetComment, "setComment");
   pragma Import (Java, GetComment, "getComment");
   pragma Import (Java, IsDirectory, "isDirectory");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, STORED, "STORED");
   pragma Import (Java, DEFLATED, "DEFLATED");

end Java.Util.Zip.ZipEntry;
pragma Import (Java, Java.Util.Zip.ZipEntry, "java.util.zip.ZipEntry");
pragma Extensions_Allowed (Off);
