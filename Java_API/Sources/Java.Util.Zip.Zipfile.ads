pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Zip.ZipEntry;
with Java.Lang.Object;

package Java.Util.Zip.ZipFile is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ZipFile (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.IOException.Except

   function New_ZipFile (P1_File : access Standard.Java.Io.File.Typ'Class;
                         P2_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Io.IOException.Except

   function New_ZipFile (P1_File : access Standard.Java.Io.File.Typ'Class; 
                         This : Ref := null)
                         return Ref;
   --  can raise Java.Util.Zip.ZipException.Except and
   --  Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEntry (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Util.Zip.ZipEntry.Typ'Class;

   function GetInputStream (This : access Typ;
                            P1_ZipEntry : access Standard.Java.Util.Zip.ZipEntry.Typ'Class)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function Entries (This : access Typ)
                     return access Java.Util.Enumeration.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OPEN_READ : constant Java.Int;

   --  final
   OPEN_DELETE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ZipFile);
   pragma Import (Java, GetEntry, "getEntry");
   pragma Import (Java, GetInputStream, "getInputStream");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, Entries, "entries");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Finalize, "finalize");
   pragma Import (Java, OPEN_READ, "OPEN_READ");
   pragma Import (Java, OPEN_DELETE, "OPEN_DELETE");

end Java.Util.Zip.ZipFile;
pragma Import (Java, Java.Util.Zip.ZipFile, "java.util.zip.ZipFile");
pragma Extensions_Allowed (Off);
