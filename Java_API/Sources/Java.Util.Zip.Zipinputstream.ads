pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Zip.ZipEntry;
with Java.Io.Closeable;
with Java.Lang.Object;
with Java.Util.Zip.InflaterInputStream;

package Java.Util.Zip.ZipInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Util.Zip.InflaterInputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ZipInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNextEntry (This : access Typ)
                          return access Java.Util.Zip.ZipEntry.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure CloseEntry (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   function CreateZipEntry (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Util.Zip.ZipEntry.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ZipInputStream);
   pragma Import (Java, GetNextEntry, "getNextEntry");
   pragma Import (Java, CloseEntry, "closeEntry");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Close, "close");
   pragma Import (Java, CreateZipEntry, "createZipEntry");

end Java.Util.Zip.ZipInputStream;
pragma Import (Java, Java.Util.Zip.ZipInputStream, "java.util.zip.ZipInputStream");
pragma Extensions_Allowed (Off);
