pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Util.Zip.ZipEntry;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Object;
with Java.Util.Zip.DeflaterOutputStream;

package Java.Util.Zip.ZipOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Util.Zip.DeflaterOutputStream.Typ(Closeable_I,
                                                  Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ZipOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetComment (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetMethod (This : access Typ;
                        P1_Int : Java.Int);

   procedure SetLevel (This : access Typ;
                       P1_Int : Java.Int);

   procedure PutNextEntry (This : access Typ;
                           P1_ZipEntry : access Standard.Java.Util.Zip.ZipEntry.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure CloseEntry (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Finish (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   STORED : constant Java.Int;

   --  final
   DEFLATED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ZipOutputStream);
   pragma Import (Java, SetComment, "setComment");
   pragma Import (Java, SetMethod, "setMethod");
   pragma Import (Java, SetLevel, "setLevel");
   pragma Import (Java, PutNextEntry, "putNextEntry");
   pragma Import (Java, CloseEntry, "closeEntry");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Finish, "finish");
   pragma Import (Java, Close, "close");
   pragma Import (Java, STORED, "STORED");
   pragma Import (Java, DEFLATED, "DEFLATED");

end Java.Util.Zip.ZipOutputStream;
pragma Import (Java, Java.Util.Zip.ZipOutputStream, "java.util.zip.ZipOutputStream");
pragma Extensions_Allowed (Off);
