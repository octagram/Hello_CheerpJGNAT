pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessibleActionCount (This : access Typ)
                                      return Java.Int is abstract;

   function GetAccessibleActionDescription (This : access Typ;
                                            P1_Int : Java.Int)
                                            return access Java.Lang.String.Typ'Class is abstract;

   function DoAccessibleAction (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TOGGLE_EXPAND : access Java.Lang.String.Typ'Class;

   --  final
   INCREMENT : access Java.Lang.String.Typ'Class;

   --  final
   DECREMENT : access Java.Lang.String.Typ'Class;

   --  final
   CLICK : access Java.Lang.String.Typ'Class;

   --  final
   TOGGLE_POPUP : access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAccessibleActionCount, "getAccessibleActionCount");
   pragma Export (Java, GetAccessibleActionDescription, "getAccessibleActionDescription");
   pragma Export (Java, DoAccessibleAction, "doAccessibleAction");
   pragma Import (Java, TOGGLE_EXPAND, "TOGGLE_EXPAND");
   pragma Import (Java, INCREMENT, "INCREMENT");
   pragma Import (Java, DECREMENT, "DECREMENT");
   pragma Import (Java, CLICK, "CLICK");
   pragma Import (Java, TOGGLE_POPUP, "TOGGLE_POPUP");

end Javax.Accessibility.AccessibleAction;
pragma Import (Java, Javax.Accessibility.AccessibleAction, "javax.accessibility.AccessibleAction");
pragma Extensions_Allowed (Off);
