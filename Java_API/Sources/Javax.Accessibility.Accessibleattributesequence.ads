pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.AttributeSet;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleAttributeSequence is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      StartIndex : Java.Int;
      pragma Import (Java, StartIndex, "startIndex");

      EndIndex : Java.Int;
      pragma Import (Java, EndIndex, "endIndex");

      Attributes : access Javax.Swing.Text.AttributeSet.Typ'Class;
      pragma Import (Java, Attributes, "attributes");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AccessibleAttributeSequence (P1_Int : Java.Int;
                                             P2_Int : Java.Int;
                                             P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleAttributeSequence);

end Javax.Accessibility.AccessibleAttributeSequence;
pragma Import (Java, Javax.Accessibility.AccessibleAttributeSequence, "javax.accessibility.AccessibleAttributeSequence");
pragma Extensions_Allowed (Off);
