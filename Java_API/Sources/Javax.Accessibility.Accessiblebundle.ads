pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleBundle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Key : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Key, "key");

   end record;

   function New_AccessibleBundle (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function ToDisplayString (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Lang.String.Typ'Class;

   function ToDisplayString (This : access Typ;
                             P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                             return access Java.Lang.String.Typ'Class;

   function ToDisplayString (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleBundle);
   pragma Import (Java, ToDisplayString, "toDisplayString");
   pragma Import (Java, ToString, "toString");

end Javax.Accessibility.AccessibleBundle;
pragma Import (Java, Javax.Accessibility.AccessibleBundle, "javax.accessibility.AccessibleBundle");
pragma Extensions_Allowed (Off);
