pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Cursor;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Javax.Accessibility.Accessible;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class is abstract;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   function GetForeground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class is abstract;

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   function GetCursor (This : access Typ)
                       return access Java.Awt.Cursor.Typ'Class is abstract;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class) is abstract;

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class is abstract;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class) is abstract;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class is abstract;

   function IsEnabled (This : access Typ)
                       return Java.Boolean is abstract;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   function IsVisible (This : access Typ)
                       return Java.Boolean is abstract;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   function IsShowing (This : access Typ)
                       return Java.Boolean is abstract;

   function Contains (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                      return Java.Boolean is abstract;

   function GetLocationOnScreen (This : access Typ)
                                 return access Java.Awt.Point.Typ'Class is abstract;

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class is abstract;

   procedure SetLocation (This : access Typ;
                          P1_Point : access Standard.Java.Awt.Point.Typ'Class) is abstract;

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class is abstract;

   procedure SetBounds (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class) is abstract;

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class is abstract;

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class) is abstract;

   function GetAccessibleAt (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean is abstract;

   procedure RequestFocus (This : access Typ) is abstract;

   procedure AddFocusListener (This : access Typ;
                               P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class) is abstract;

   procedure RemoveFocusListener (This : access Typ;
                                  P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetBackground, "getBackground");
   pragma Export (Java, SetBackground, "setBackground");
   pragma Export (Java, GetForeground, "getForeground");
   pragma Export (Java, SetForeground, "setForeground");
   pragma Export (Java, GetCursor, "getCursor");
   pragma Export (Java, SetCursor, "setCursor");
   pragma Export (Java, GetFont, "getFont");
   pragma Export (Java, SetFont, "setFont");
   pragma Export (Java, GetFontMetrics, "getFontMetrics");
   pragma Export (Java, IsEnabled, "isEnabled");
   pragma Export (Java, SetEnabled, "setEnabled");
   pragma Export (Java, IsVisible, "isVisible");
   pragma Export (Java, SetVisible, "setVisible");
   pragma Export (Java, IsShowing, "isShowing");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, GetLocationOnScreen, "getLocationOnScreen");
   pragma Export (Java, GetLocation, "getLocation");
   pragma Export (Java, SetLocation, "setLocation");
   pragma Export (Java, GetBounds, "getBounds");
   pragma Export (Java, SetBounds, "setBounds");
   pragma Export (Java, GetSize, "getSize");
   pragma Export (Java, SetSize, "setSize");
   pragma Export (Java, GetAccessibleAt, "getAccessibleAt");
   pragma Export (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Export (Java, RequestFocus, "requestFocus");
   pragma Export (Java, AddFocusListener, "addFocusListener");
   pragma Export (Java, RemoveFocusListener, "removeFocusListener");

end Javax.Accessibility.AccessibleComponent;
pragma Import (Java, Javax.Accessibility.AccessibleComponent, "javax.accessibility.AccessibleComponent");
pragma Extensions_Allowed (Off);
