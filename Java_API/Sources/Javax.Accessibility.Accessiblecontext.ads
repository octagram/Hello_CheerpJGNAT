pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Accessibility.Accessible;
limited with Javax.Accessibility.AccessibleAction;
limited with Javax.Accessibility.AccessibleComponent;
limited with Javax.Accessibility.AccessibleEditableText;
limited with Javax.Accessibility.AccessibleIcon;
limited with Javax.Accessibility.AccessibleRelationSet;
limited with Javax.Accessibility.AccessibleRole;
limited with Javax.Accessibility.AccessibleSelection;
limited with Javax.Accessibility.AccessibleStateSet;
limited with Javax.Accessibility.AccessibleTable;
limited with Javax.Accessibility.AccessibleText;
limited with Javax.Accessibility.AccessibleValue;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      AccessibleParent : access Javax.Accessibility.Accessible.Typ'Class;
      pragma Import (Java, AccessibleParent, "accessibleParent");

      --  protected
      AccessibleName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, AccessibleName, "accessibleName");

      --  protected
      AccessibleDescription : access Java.Lang.String.Typ'Class;
      pragma Import (Java, AccessibleDescription, "accessibleDescription");

   end record;

   function New_AccessibleContext (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessibleName (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure SetAccessibleName (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetAccessibleDescription (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   procedure SetAccessibleDescription (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetAccessibleRole (This : access Typ)
                               return access Javax.Accessibility.AccessibleRole.Typ'Class is abstract;

   function GetAccessibleStateSet (This : access Typ)
                                   return access Javax.Accessibility.AccessibleStateSet.Typ'Class is abstract;

   function GetAccessibleParent (This : access Typ)
                                 return access Javax.Accessibility.Accessible.Typ'Class;

   procedure SetAccessibleParent (This : access Typ;
                                  P1_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class);

   function GetAccessibleIndexInParent (This : access Typ)
                                        return Java.Int is abstract;

   function GetAccessibleChildrenCount (This : access Typ)
                                        return Java.Int is abstract;

   function GetAccessibleChild (This : access Typ;
                                P1_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class is abstract;
   --  can raise Java.Awt.IllegalComponentStateException.Except

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function GetAccessibleAction (This : access Typ)
                                 return access Javax.Accessibility.AccessibleAction.Typ'Class;

   function GetAccessibleComponent (This : access Typ)
                                    return access Javax.Accessibility.AccessibleComponent.Typ'Class;

   function GetAccessibleSelection (This : access Typ)
                                    return access Javax.Accessibility.AccessibleSelection.Typ'Class;

   function GetAccessibleText (This : access Typ)
                               return access Javax.Accessibility.AccessibleText.Typ'Class;

   function GetAccessibleEditableText (This : access Typ)
                                       return access Javax.Accessibility.AccessibleEditableText.Typ'Class;

   function GetAccessibleValue (This : access Typ)
                                return access Javax.Accessibility.AccessibleValue.Typ'Class;

   function GetAccessibleIcon (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetAccessibleRelationSet (This : access Typ)
                                      return access Javax.Accessibility.AccessibleRelationSet.Typ'Class;

   function GetAccessibleTable (This : access Typ)
                                return access Javax.Accessibility.AccessibleTable.Typ'Class;

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ACCESSIBLE_NAME_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_DESCRIPTION_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_STATE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_VALUE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_SELECTION_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_CARET_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_VISIBLE_DATA_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_CHILD_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_ACTIVE_DESCENDANT_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TABLE_CAPTION_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TABLE_SUMMARY_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TABLE_MODEL_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TABLE_ROW_HEADER_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TABLE_ROW_DESCRIPTION_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TABLE_COLUMN_HEADER_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TABLE_COLUMN_DESCRIPTION_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_ACTION_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_HYPERTEXT_OFFSET : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TEXT_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_INVALIDATE_CHILDREN : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_TEXT_ATTRIBUTES_CHANGED : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSIBLE_COMPONENT_BOUNDS_CHANGED : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleContext);
   pragma Import (Java, GetAccessibleName, "getAccessibleName");
   pragma Import (Java, SetAccessibleName, "setAccessibleName");
   pragma Import (Java, GetAccessibleDescription, "getAccessibleDescription");
   pragma Import (Java, SetAccessibleDescription, "setAccessibleDescription");
   pragma Export (Java, GetAccessibleRole, "getAccessibleRole");
   pragma Export (Java, GetAccessibleStateSet, "getAccessibleStateSet");
   pragma Import (Java, GetAccessibleParent, "getAccessibleParent");
   pragma Import (Java, SetAccessibleParent, "setAccessibleParent");
   pragma Export (Java, GetAccessibleIndexInParent, "getAccessibleIndexInParent");
   pragma Export (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Export (Java, GetAccessibleChild, "getAccessibleChild");
   pragma Export (Java, GetLocale, "getLocale");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetAccessibleAction, "getAccessibleAction");
   pragma Import (Java, GetAccessibleComponent, "getAccessibleComponent");
   pragma Import (Java, GetAccessibleSelection, "getAccessibleSelection");
   pragma Import (Java, GetAccessibleText, "getAccessibleText");
   pragma Import (Java, GetAccessibleEditableText, "getAccessibleEditableText");
   pragma Import (Java, GetAccessibleValue, "getAccessibleValue");
   pragma Import (Java, GetAccessibleIcon, "getAccessibleIcon");
   pragma Import (Java, GetAccessibleRelationSet, "getAccessibleRelationSet");
   pragma Import (Java, GetAccessibleTable, "getAccessibleTable");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, ACCESSIBLE_NAME_PROPERTY, "ACCESSIBLE_NAME_PROPERTY");
   pragma Import (Java, ACCESSIBLE_DESCRIPTION_PROPERTY, "ACCESSIBLE_DESCRIPTION_PROPERTY");
   pragma Import (Java, ACCESSIBLE_STATE_PROPERTY, "ACCESSIBLE_STATE_PROPERTY");
   pragma Import (Java, ACCESSIBLE_VALUE_PROPERTY, "ACCESSIBLE_VALUE_PROPERTY");
   pragma Import (Java, ACCESSIBLE_SELECTION_PROPERTY, "ACCESSIBLE_SELECTION_PROPERTY");
   pragma Import (Java, ACCESSIBLE_CARET_PROPERTY, "ACCESSIBLE_CARET_PROPERTY");
   pragma Import (Java, ACCESSIBLE_VISIBLE_DATA_PROPERTY, "ACCESSIBLE_VISIBLE_DATA_PROPERTY");
   pragma Import (Java, ACCESSIBLE_CHILD_PROPERTY, "ACCESSIBLE_CHILD_PROPERTY");
   pragma Import (Java, ACCESSIBLE_ACTIVE_DESCENDANT_PROPERTY, "ACCESSIBLE_ACTIVE_DESCENDANT_PROPERTY");
   pragma Import (Java, ACCESSIBLE_TABLE_CAPTION_CHANGED, "ACCESSIBLE_TABLE_CAPTION_CHANGED");
   pragma Import (Java, ACCESSIBLE_TABLE_SUMMARY_CHANGED, "ACCESSIBLE_TABLE_SUMMARY_CHANGED");
   pragma Import (Java, ACCESSIBLE_TABLE_MODEL_CHANGED, "ACCESSIBLE_TABLE_MODEL_CHANGED");
   pragma Import (Java, ACCESSIBLE_TABLE_ROW_HEADER_CHANGED, "ACCESSIBLE_TABLE_ROW_HEADER_CHANGED");
   pragma Import (Java, ACCESSIBLE_TABLE_ROW_DESCRIPTION_CHANGED, "ACCESSIBLE_TABLE_ROW_DESCRIPTION_CHANGED");
   pragma Import (Java, ACCESSIBLE_TABLE_COLUMN_HEADER_CHANGED, "ACCESSIBLE_TABLE_COLUMN_HEADER_CHANGED");
   pragma Import (Java, ACCESSIBLE_TABLE_COLUMN_DESCRIPTION_CHANGED, "ACCESSIBLE_TABLE_COLUMN_DESCRIPTION_CHANGED");
   pragma Import (Java, ACCESSIBLE_ACTION_PROPERTY, "ACCESSIBLE_ACTION_PROPERTY");
   pragma Import (Java, ACCESSIBLE_HYPERTEXT_OFFSET, "ACCESSIBLE_HYPERTEXT_OFFSET");
   pragma Import (Java, ACCESSIBLE_TEXT_PROPERTY, "ACCESSIBLE_TEXT_PROPERTY");
   pragma Import (Java, ACCESSIBLE_INVALIDATE_CHILDREN, "ACCESSIBLE_INVALIDATE_CHILDREN");
   pragma Import (Java, ACCESSIBLE_TEXT_ATTRIBUTES_CHANGED, "ACCESSIBLE_TEXT_ATTRIBUTES_CHANGED");
   pragma Import (Java, ACCESSIBLE_COMPONENT_BOUNDS_CHANGED, "ACCESSIBLE_COMPONENT_BOUNDS_CHANGED");

end Javax.Accessibility.AccessibleContext;
pragma Import (Java, Javax.Accessibility.AccessibleContext, "javax.accessibility.AccessibleContext");
pragma Extensions_Allowed (Off);
