pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleText;

package Javax.Accessibility.AccessibleEditableText is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AccessibleText_I : Javax.Accessibility.AccessibleText.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTextContents (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure InsertTextAtIndex (This : access Typ;
                                P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTextRange (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure Delete (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int) is abstract;

   procedure Cut (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int) is abstract;

   procedure Paste (This : access Typ;
                    P1_Int : Java.Int) is abstract;

   procedure ReplaceText (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SelectText (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int) is abstract;

   procedure SetAttributes (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetTextContents, "setTextContents");
   pragma Export (Java, InsertTextAtIndex, "insertTextAtIndex");
   pragma Export (Java, GetTextRange, "getTextRange");
   pragma Export (Java, Delete, "delete");
   pragma Export (Java, Cut, "cut");
   pragma Export (Java, Paste, "paste");
   pragma Export (Java, ReplaceText, "replaceText");
   pragma Export (Java, SelectText, "selectText");
   pragma Export (Java, SetAttributes, "setAttributes");

end Javax.Accessibility.AccessibleEditableText;
pragma Import (Java, Javax.Accessibility.AccessibleEditableText, "javax.accessibility.AccessibleEditableText");
pragma Extensions_Allowed (Off);
