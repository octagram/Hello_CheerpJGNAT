pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleKeyBinding;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleComponent;

package Javax.Accessibility.AccessibleExtendedComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AccessibleComponent_I : Javax.Accessibility.AccessibleComponent.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetToolTipText (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetTitledBorderText (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   function GetAccessibleKeyBinding (This : access Typ)
                                     return access Javax.Accessibility.AccessibleKeyBinding.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetToolTipText, "getToolTipText");
   pragma Export (Java, GetTitledBorderText, "getTitledBorderText");
   pragma Export (Java, GetAccessibleKeyBinding, "getAccessibleKeyBinding");

end Javax.Accessibility.AccessibleExtendedComponent;
pragma Import (Java, Javax.Accessibility.AccessibleExtendedComponent, "javax.accessibility.AccessibleExtendedComponent");
pragma Extensions_Allowed (Off);
