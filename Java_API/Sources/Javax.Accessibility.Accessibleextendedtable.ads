pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Accessibility.AccessibleTable;

package Javax.Accessibility.AccessibleExtendedTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AccessibleTable_I : Javax.Accessibility.AccessibleTable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessibleRow (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int is abstract;

   function GetAccessibleColumn (This : access Typ;
                                 P1_Int : Java.Int)
                                 return Java.Int is abstract;

   function GetAccessibleIndex (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAccessibleRow, "getAccessibleRow");
   pragma Export (Java, GetAccessibleColumn, "getAccessibleColumn");
   pragma Export (Java, GetAccessibleIndex, "getAccessibleIndex");

end Javax.Accessibility.AccessibleExtendedTable;
pragma Import (Java, Javax.Accessibility.AccessibleExtendedTable, "javax.accessibility.AccessibleExtendedTable");
pragma Extensions_Allowed (Off);
