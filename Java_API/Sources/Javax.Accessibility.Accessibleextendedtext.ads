pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleTextSequence;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleExtendedText is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTextRange (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetTextSequenceAt (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return access Javax.Accessibility.AccessibleTextSequence.Typ'Class is abstract;

   function GetTextSequenceAfter (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int)
                                  return access Javax.Accessibility.AccessibleTextSequence.Typ'Class is abstract;

   function GetTextSequenceBefore (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int)
                                   return access Javax.Accessibility.AccessibleTextSequence.Typ'Class is abstract;

   function GetTextBounds (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Awt.Rectangle.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LINE : constant Java.Int;

   --  final
   ATTRIBUTE_RUN : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTextRange, "getTextRange");
   pragma Export (Java, GetTextSequenceAt, "getTextSequenceAt");
   pragma Export (Java, GetTextSequenceAfter, "getTextSequenceAfter");
   pragma Export (Java, GetTextSequenceBefore, "getTextSequenceBefore");
   pragma Export (Java, GetTextBounds, "getTextBounds");
   pragma Import (Java, LINE, "LINE");
   pragma Import (Java, ATTRIBUTE_RUN, "ATTRIBUTE_RUN");

end Javax.Accessibility.AccessibleExtendedText;
pragma Import (Java, Javax.Accessibility.AccessibleExtendedText, "javax.accessibility.AccessibleExtendedText");
pragma Extensions_Allowed (Off);
