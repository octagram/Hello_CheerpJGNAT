pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleAction;

package Javax.Accessibility.AccessibleHyperlink is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AccessibleAction_I : Javax.Accessibility.AccessibleAction.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AccessibleHyperlink (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;

   function GetAccessibleActionCount (This : access Typ)
                                      return Java.Int is abstract;

   function DoAccessibleAction (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean is abstract;

   function GetAccessibleActionDescription (This : access Typ;
                                            P1_Int : Java.Int)
                                            return access Java.Lang.String.Typ'Class is abstract;

   function GetAccessibleActionObject (This : access Typ;
                                       P1_Int : Java.Int)
                                       return access Java.Lang.Object.Typ'Class is abstract;

   function GetAccessibleActionAnchor (This : access Typ;
                                       P1_Int : Java.Int)
                                       return access Java.Lang.Object.Typ'Class is abstract;

   function GetStartIndex (This : access Typ)
                           return Java.Int is abstract;

   function GetEndIndex (This : access Typ)
                         return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleHyperlink);
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, GetAccessibleActionCount, "getAccessibleActionCount");
   pragma Export (Java, DoAccessibleAction, "doAccessibleAction");
   pragma Export (Java, GetAccessibleActionDescription, "getAccessibleActionDescription");
   pragma Export (Java, GetAccessibleActionObject, "getAccessibleActionObject");
   pragma Export (Java, GetAccessibleActionAnchor, "getAccessibleActionAnchor");
   pragma Export (Java, GetStartIndex, "getStartIndex");
   pragma Export (Java, GetEndIndex, "getEndIndex");

end Javax.Accessibility.AccessibleHyperlink;
pragma Import (Java, Javax.Accessibility.AccessibleHyperlink, "javax.accessibility.AccessibleHyperlink");
pragma Extensions_Allowed (Off);
