pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleBundle;

package Javax.Accessibility.AccessibleRelation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Accessibility.AccessibleBundle.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AccessibleRelation (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_AccessibleRelation (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_AccessibleRelation (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKey (This : access Typ)
                    return access Java.Lang.String.Typ'Class;

   function GetTarget (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   procedure SetTarget (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure SetTarget (This : access Typ;
                        P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LABEL_FOR : access Java.Lang.String.Typ'Class;

   --  final
   LABELED_BY : access Java.Lang.String.Typ'Class;

   --  final
   MEMBER_OF : access Java.Lang.String.Typ'Class;

   --  final
   CONTROLLER_FOR : access Java.Lang.String.Typ'Class;

   --  final
   CONTROLLED_BY : access Java.Lang.String.Typ'Class;

   --  final
   FLOWS_TO : constant access Java.Lang.String.Typ'Class;

   --  final
   FLOWS_FROM : constant access Java.Lang.String.Typ'Class;

   --  final
   SUBWINDOW_OF : constant access Java.Lang.String.Typ'Class;

   --  final
   PARENT_WINDOW_OF : constant access Java.Lang.String.Typ'Class;

   --  final
   EMBEDS : constant access Java.Lang.String.Typ'Class;

   --  final
   EMBEDDED_BY : constant access Java.Lang.String.Typ'Class;

   --  final
   CHILD_NODE_OF : constant access Java.Lang.String.Typ'Class;

   --  final
   LABEL_FOR_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   LABELED_BY_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   MEMBER_OF_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CONTROLLER_FOR_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CONTROLLED_BY_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FLOWS_TO_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FLOWS_FROM_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SUBWINDOW_OF_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   PARENT_WINDOW_OF_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   EMBEDS_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   EMBEDDED_BY_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CHILD_NODE_OF_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleRelation);
   pragma Import (Java, GetKey, "getKey");
   pragma Import (Java, GetTarget, "getTarget");
   pragma Import (Java, SetTarget, "setTarget");
   pragma Import (Java, LABEL_FOR, "LABEL_FOR");
   pragma Import (Java, LABELED_BY, "LABELED_BY");
   pragma Import (Java, MEMBER_OF, "MEMBER_OF");
   pragma Import (Java, CONTROLLER_FOR, "CONTROLLER_FOR");
   pragma Import (Java, CONTROLLED_BY, "CONTROLLED_BY");
   pragma Import (Java, FLOWS_TO, "FLOWS_TO");
   pragma Import (Java, FLOWS_FROM, "FLOWS_FROM");
   pragma Import (Java, SUBWINDOW_OF, "SUBWINDOW_OF");
   pragma Import (Java, PARENT_WINDOW_OF, "PARENT_WINDOW_OF");
   pragma Import (Java, EMBEDS, "EMBEDS");
   pragma Import (Java, EMBEDDED_BY, "EMBEDDED_BY");
   pragma Import (Java, CHILD_NODE_OF, "CHILD_NODE_OF");
   pragma Import (Java, LABEL_FOR_PROPERTY, "LABEL_FOR_PROPERTY");
   pragma Import (Java, LABELED_BY_PROPERTY, "LABELED_BY_PROPERTY");
   pragma Import (Java, MEMBER_OF_PROPERTY, "MEMBER_OF_PROPERTY");
   pragma Import (Java, CONTROLLER_FOR_PROPERTY, "CONTROLLER_FOR_PROPERTY");
   pragma Import (Java, CONTROLLED_BY_PROPERTY, "CONTROLLED_BY_PROPERTY");
   pragma Import (Java, FLOWS_TO_PROPERTY, "FLOWS_TO_PROPERTY");
   pragma Import (Java, FLOWS_FROM_PROPERTY, "FLOWS_FROM_PROPERTY");
   pragma Import (Java, SUBWINDOW_OF_PROPERTY, "SUBWINDOW_OF_PROPERTY");
   pragma Import (Java, PARENT_WINDOW_OF_PROPERTY, "PARENT_WINDOW_OF_PROPERTY");
   pragma Import (Java, EMBEDS_PROPERTY, "EMBEDS_PROPERTY");
   pragma Import (Java, EMBEDDED_BY_PROPERTY, "EMBEDDED_BY_PROPERTY");
   pragma Import (Java, CHILD_NODE_OF_PROPERTY, "CHILD_NODE_OF_PROPERTY");

end Javax.Accessibility.AccessibleRelation;
pragma Import (Java, Javax.Accessibility.AccessibleRelation, "javax.accessibility.AccessibleRelation");
pragma Extensions_Allowed (Off);
