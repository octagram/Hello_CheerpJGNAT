pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Accessibility.AccessibleRelation;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleRelationSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Relations : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Relations, "relations");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AccessibleRelationSet (This : Ref := null)
                                       return Ref;

   function New_AccessibleRelationSet (P1_AccessibleRelation_Arr : access Javax.Accessibility.AccessibleRelation.Arr_Obj; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_AccessibleRelation : access Standard.Javax.Accessibility.AccessibleRelation.Typ'Class)
                 return Java.Boolean;

   procedure AddAll (This : access Typ;
                     P1_AccessibleRelation_Arr : access Javax.Accessibility.AccessibleRelation.Arr_Obj);

   function Remove (This : access Typ;
                    P1_AccessibleRelation : access Standard.Javax.Accessibility.AccessibleRelation.Typ'Class)
                    return Java.Boolean;

   procedure Clear (This : access Typ);

   function Size (This : access Typ)
                  return Java.Int;

   function Contains (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Boolean;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Accessibility.AccessibleRelation.Typ'Class;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleRelationSet);
   pragma Import (Java, Add, "add");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Get, "get");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, ToString, "toString");

end Javax.Accessibility.AccessibleRelationSet;
pragma Import (Java, Javax.Accessibility.AccessibleRelationSet, "javax.accessibility.AccessibleRelationSet");
pragma Extensions_Allowed (Off);
