pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.ListResourceBundle;

package Javax.Accessibility.AccessibleResourceBundle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Util.ListResourceBundle.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AccessibleResourceBundle (This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContents (This : access Typ)
                         return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleResourceBundle);
   pragma Import (Java, GetContents, "getContents");

end Javax.Accessibility.AccessibleResourceBundle;
pragma Import (Java, Javax.Accessibility.AccessibleResourceBundle, "javax.accessibility.AccessibleResourceBundle");
pragma Extensions_Allowed (Off);
