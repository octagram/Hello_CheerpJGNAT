pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleBundle;

package Javax.Accessibility.AccessibleRole is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Accessibility.AccessibleBundle.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AccessibleRole (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ALERT : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   COLUMN_HEADER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   CANVAS : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   COMBO_BOX : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   DESKTOP_ICON : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   HTML_CONTAINER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   INTERNAL_FRAME : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   DESKTOP_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   OPTION_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   WINDOW : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   FRAME : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   DIALOG : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   COLOR_CHOOSER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   DIRECTORY_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   FILE_CHOOSER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   FILLER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   HYPERLINK : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   ICON : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   LABEL : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   ROOT_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   GLASS_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   LAYERED_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   LIST : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   LIST_ITEM : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   MENU_BAR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   POPUP_MENU : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   MENU : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   MENU_ITEM : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   SEPARATOR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PAGE_TAB_LIST : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PAGE_TAB : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PANEL : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PROGRESS_BAR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PASSWORD_TEXT : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PUSH_BUTTON : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   TOGGLE_BUTTON : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   CHECK_BOX : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   RADIO_BUTTON : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   ROW_HEADER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   SCROLL_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   SCROLL_BAR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   VIEWPORT : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   SLIDER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   SPLIT_PANE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   TABLE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   TEXT : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   TREE : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   TOOL_BAR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   TOOL_TIP : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   AWT_COMPONENT : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   SWING_COMPONENT : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   UNKNOWN : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   STATUS_BAR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   DATE_EDITOR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   SPIN_BOX : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   FONT_CHOOSER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   GROUP_BOX : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   HEADER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   FOOTER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PARAGRAPH : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   RULER : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   EDITBAR : access Javax.Accessibility.AccessibleRole.Typ'Class;

   --  final
   PROGRESS_MONITOR : access Javax.Accessibility.AccessibleRole.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleRole);
   pragma Import (Java, ALERT, "ALERT");
   pragma Import (Java, COLUMN_HEADER, "COLUMN_HEADER");
   pragma Import (Java, CANVAS, "CANVAS");
   pragma Import (Java, COMBO_BOX, "COMBO_BOX");
   pragma Import (Java, DESKTOP_ICON, "DESKTOP_ICON");
   pragma Import (Java, HTML_CONTAINER, "HTML_CONTAINER");
   pragma Import (Java, INTERNAL_FRAME, "INTERNAL_FRAME");
   pragma Import (Java, DESKTOP_PANE, "DESKTOP_PANE");
   pragma Import (Java, OPTION_PANE, "OPTION_PANE");
   pragma Import (Java, WINDOW, "WINDOW");
   pragma Import (Java, FRAME, "FRAME");
   pragma Import (Java, DIALOG, "DIALOG");
   pragma Import (Java, COLOR_CHOOSER, "COLOR_CHOOSER");
   pragma Import (Java, DIRECTORY_PANE, "DIRECTORY_PANE");
   pragma Import (Java, FILE_CHOOSER, "FILE_CHOOSER");
   pragma Import (Java, FILLER, "FILLER");
   pragma Import (Java, HYPERLINK, "HYPERLINK");
   pragma Import (Java, ICON, "ICON");
   pragma Import (Java, LABEL, "LABEL");
   pragma Import (Java, ROOT_PANE, "ROOT_PANE");
   pragma Import (Java, GLASS_PANE, "GLASS_PANE");
   pragma Import (Java, LAYERED_PANE, "LAYERED_PANE");
   pragma Import (Java, LIST, "LIST");
   pragma Import (Java, LIST_ITEM, "LIST_ITEM");
   pragma Import (Java, MENU_BAR, "MENU_BAR");
   pragma Import (Java, POPUP_MENU, "POPUP_MENU");
   pragma Import (Java, MENU, "MENU");
   pragma Import (Java, MENU_ITEM, "MENU_ITEM");
   pragma Import (Java, SEPARATOR, "SEPARATOR");
   pragma Import (Java, PAGE_TAB_LIST, "PAGE_TAB_LIST");
   pragma Import (Java, PAGE_TAB, "PAGE_TAB");
   pragma Import (Java, PANEL, "PANEL");
   pragma Import (Java, PROGRESS_BAR, "PROGRESS_BAR");
   pragma Import (Java, PASSWORD_TEXT, "PASSWORD_TEXT");
   pragma Import (Java, PUSH_BUTTON, "PUSH_BUTTON");
   pragma Import (Java, TOGGLE_BUTTON, "TOGGLE_BUTTON");
   pragma Import (Java, CHECK_BOX, "CHECK_BOX");
   pragma Import (Java, RADIO_BUTTON, "RADIO_BUTTON");
   pragma Import (Java, ROW_HEADER, "ROW_HEADER");
   pragma Import (Java, SCROLL_PANE, "SCROLL_PANE");
   pragma Import (Java, SCROLL_BAR, "SCROLL_BAR");
   pragma Import (Java, VIEWPORT, "VIEWPORT");
   pragma Import (Java, SLIDER, "SLIDER");
   pragma Import (Java, SPLIT_PANE, "SPLIT_PANE");
   pragma Import (Java, TABLE, "TABLE");
   pragma Import (Java, TEXT, "TEXT");
   pragma Import (Java, TREE, "TREE");
   pragma Import (Java, TOOL_BAR, "TOOL_BAR");
   pragma Import (Java, TOOL_TIP, "TOOL_TIP");
   pragma Import (Java, AWT_COMPONENT, "AWT_COMPONENT");
   pragma Import (Java, SWING_COMPONENT, "SWING_COMPONENT");
   pragma Import (Java, UNKNOWN, "UNKNOWN");
   pragma Import (Java, STATUS_BAR, "STATUS_BAR");
   pragma Import (Java, DATE_EDITOR, "DATE_EDITOR");
   pragma Import (Java, SPIN_BOX, "SPIN_BOX");
   pragma Import (Java, FONT_CHOOSER, "FONT_CHOOSER");
   pragma Import (Java, GROUP_BOX, "GROUP_BOX");
   pragma Import (Java, HEADER, "HEADER");
   pragma Import (Java, FOOTER, "FOOTER");
   pragma Import (Java, PARAGRAPH, "PARAGRAPH");
   pragma Import (Java, RULER, "RULER");
   pragma Import (Java, EDITBAR, "EDITBAR");
   pragma Import (Java, PROGRESS_MONITOR, "PROGRESS_MONITOR");

end Javax.Accessibility.AccessibleRole;
pragma Import (Java, Javax.Accessibility.AccessibleRole, "javax.accessibility.AccessibleRole");
pragma Extensions_Allowed (Off);
