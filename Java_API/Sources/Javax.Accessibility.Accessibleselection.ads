pragma Extensions_Allowed (On);
limited with Javax.Accessibility.Accessible;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleSelection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessibleSelectionCount (This : access Typ)
                                         return Java.Int is abstract;

   function GetAccessibleSelection (This : access Typ;
                                    P1_Int : Java.Int)
                                    return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   function IsAccessibleChildSelected (This : access Typ;
                                       P1_Int : Java.Int)
                                       return Java.Boolean is abstract;

   procedure AddAccessibleSelection (This : access Typ;
                                     P1_Int : Java.Int) is abstract;

   procedure RemoveAccessibleSelection (This : access Typ;
                                        P1_Int : Java.Int) is abstract;

   procedure ClearAccessibleSelection (This : access Typ) is abstract;

   procedure SelectAllAccessibleSelection (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAccessibleSelectionCount, "getAccessibleSelectionCount");
   pragma Export (Java, GetAccessibleSelection, "getAccessibleSelection");
   pragma Export (Java, IsAccessibleChildSelected, "isAccessibleChildSelected");
   pragma Export (Java, AddAccessibleSelection, "addAccessibleSelection");
   pragma Export (Java, RemoveAccessibleSelection, "removeAccessibleSelection");
   pragma Export (Java, ClearAccessibleSelection, "clearAccessibleSelection");
   pragma Export (Java, SelectAllAccessibleSelection, "selectAllAccessibleSelection");

end Javax.Accessibility.AccessibleSelection;
pragma Import (Java, Javax.Accessibility.AccessibleSelection, "javax.accessibility.AccessibleSelection");
pragma Extensions_Allowed (Off);
