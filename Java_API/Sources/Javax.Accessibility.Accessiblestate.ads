pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleBundle;

package Javax.Accessibility.AccessibleState is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Accessibility.AccessibleBundle.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AccessibleState (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ACTIVE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   PRESSED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   ARMED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   BUSY : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   CHECKED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   EDITABLE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   EXPANDABLE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   COLLAPSED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   EXPANDED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   ENABLED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   FOCUSABLE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   FOCUSED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   ICONIFIED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   MODAL : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   OPAQUE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   RESIZABLE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   MULTISELECTABLE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   SELECTABLE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   SELECTED : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   SHOWING : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   VISIBLE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   VERTICAL : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   HORIZONTAL : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   SINGLE_LINE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   MULTI_LINE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   TRANSIENT : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   MANAGES_DESCENDANTS : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   INDETERMINATE : access Javax.Accessibility.AccessibleState.Typ'Class;

   --  final
   TRUNCATED : access Javax.Accessibility.AccessibleState.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleState);
   pragma Import (Java, ACTIVE, "ACTIVE");
   pragma Import (Java, PRESSED, "PRESSED");
   pragma Import (Java, ARMED, "ARMED");
   pragma Import (Java, BUSY, "BUSY");
   pragma Import (Java, CHECKED, "CHECKED");
   pragma Import (Java, EDITABLE, "EDITABLE");
   pragma Import (Java, EXPANDABLE, "EXPANDABLE");
   pragma Import (Java, COLLAPSED, "COLLAPSED");
   pragma Import (Java, EXPANDED, "EXPANDED");
   pragma Import (Java, ENABLED, "ENABLED");
   pragma Import (Java, FOCUSABLE, "FOCUSABLE");
   pragma Import (Java, FOCUSED, "FOCUSED");
   pragma Import (Java, ICONIFIED, "ICONIFIED");
   pragma Import (Java, MODAL, "MODAL");
   pragma Import (Java, OPAQUE, "OPAQUE");
   pragma Import (Java, RESIZABLE, "RESIZABLE");
   pragma Import (Java, MULTISELECTABLE, "MULTISELECTABLE");
   pragma Import (Java, SELECTABLE, "SELECTABLE");
   pragma Import (Java, SELECTED, "SELECTED");
   pragma Import (Java, SHOWING, "SHOWING");
   pragma Import (Java, VISIBLE, "VISIBLE");
   pragma Import (Java, VERTICAL, "VERTICAL");
   pragma Import (Java, HORIZONTAL, "HORIZONTAL");
   pragma Import (Java, SINGLE_LINE, "SINGLE_LINE");
   pragma Import (Java, MULTI_LINE, "MULTI_LINE");
   pragma Import (Java, TRANSIENT, "TRANSIENT");
   pragma Import (Java, MANAGES_DESCENDANTS, "MANAGES_DESCENDANTS");
   pragma Import (Java, INDETERMINATE, "INDETERMINATE");
   pragma Import (Java, TRUNCATED, "TRUNCATED");

end Javax.Accessibility.AccessibleState;
pragma Import (Java, Javax.Accessibility.AccessibleState, "javax.accessibility.AccessibleState");
pragma Extensions_Allowed (Off);
