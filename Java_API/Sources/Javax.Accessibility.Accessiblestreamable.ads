pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Io.InputStream;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleStreamable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMimeTypes (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   function GetStream (This : access Typ;
                       P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                       return access Java.Io.InputStream.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMimeTypes, "getMimeTypes");
   pragma Export (Java, GetStream, "getStream");

end Javax.Accessibility.AccessibleStreamable;
pragma Import (Java, Javax.Accessibility.AccessibleStreamable, "javax.accessibility.AccessibleStreamable");
pragma Extensions_Allowed (Off);
