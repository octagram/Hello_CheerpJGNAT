pragma Extensions_Allowed (On);
limited with Javax.Accessibility.Accessible;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessibleCaption (This : access Typ)
                                  return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   procedure SetAccessibleCaption (This : access Typ;
                                   P1_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class) is abstract;

   function GetAccessibleSummary (This : access Typ)
                                  return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   procedure SetAccessibleSummary (This : access Typ;
                                   P1_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class) is abstract;

   function GetAccessibleRowCount (This : access Typ)
                                   return Java.Int is abstract;

   function GetAccessibleColumnCount (This : access Typ)
                                      return Java.Int is abstract;

   function GetAccessibleAt (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   function GetAccessibleRowExtentAt (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int)
                                      return Java.Int is abstract;

   function GetAccessibleColumnExtentAt (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return Java.Int is abstract;

   function GetAccessibleRowHeader (This : access Typ)
                                    return access Javax.Accessibility.AccessibleTable.Typ'Class is abstract;

   procedure SetAccessibleRowHeader (This : access Typ;
                                     P1_AccessibleTable : access Standard.Javax.Accessibility.AccessibleTable.Typ'Class) is abstract;

   function GetAccessibleColumnHeader (This : access Typ)
                                       return access Javax.Accessibility.AccessibleTable.Typ'Class is abstract;

   procedure SetAccessibleColumnHeader (This : access Typ;
                                        P1_AccessibleTable : access Standard.Javax.Accessibility.AccessibleTable.Typ'Class) is abstract;

   function GetAccessibleRowDescription (This : access Typ;
                                         P1_Int : Java.Int)
                                         return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   procedure SetAccessibleRowDescription (This : access Typ;
                                          P1_Int : Java.Int;
                                          P2_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class) is abstract;

   function GetAccessibleColumnDescription (This : access Typ;
                                            P1_Int : Java.Int)
                                            return access Javax.Accessibility.Accessible.Typ'Class is abstract;

   procedure SetAccessibleColumnDescription (This : access Typ;
                                             P1_Int : Java.Int;
                                             P2_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class) is abstract;

   function IsAccessibleSelected (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int)
                                  return Java.Boolean is abstract;

   function IsAccessibleRowSelected (This : access Typ;
                                     P1_Int : Java.Int)
                                     return Java.Boolean is abstract;

   function IsAccessibleColumnSelected (This : access Typ;
                                        P1_Int : Java.Int)
                                        return Java.Boolean is abstract;

   function GetSelectedAccessibleRows (This : access Typ)
                                       return Java.Int_Arr is abstract;

   function GetSelectedAccessibleColumns (This : access Typ)
                                          return Java.Int_Arr is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAccessibleCaption, "getAccessibleCaption");
   pragma Export (Java, SetAccessibleCaption, "setAccessibleCaption");
   pragma Export (Java, GetAccessibleSummary, "getAccessibleSummary");
   pragma Export (Java, SetAccessibleSummary, "setAccessibleSummary");
   pragma Export (Java, GetAccessibleRowCount, "getAccessibleRowCount");
   pragma Export (Java, GetAccessibleColumnCount, "getAccessibleColumnCount");
   pragma Export (Java, GetAccessibleAt, "getAccessibleAt");
   pragma Export (Java, GetAccessibleRowExtentAt, "getAccessibleRowExtentAt");
   pragma Export (Java, GetAccessibleColumnExtentAt, "getAccessibleColumnExtentAt");
   pragma Export (Java, GetAccessibleRowHeader, "getAccessibleRowHeader");
   pragma Export (Java, SetAccessibleRowHeader, "setAccessibleRowHeader");
   pragma Export (Java, GetAccessibleColumnHeader, "getAccessibleColumnHeader");
   pragma Export (Java, SetAccessibleColumnHeader, "setAccessibleColumnHeader");
   pragma Export (Java, GetAccessibleRowDescription, "getAccessibleRowDescription");
   pragma Export (Java, SetAccessibleRowDescription, "setAccessibleRowDescription");
   pragma Export (Java, GetAccessibleColumnDescription, "getAccessibleColumnDescription");
   pragma Export (Java, SetAccessibleColumnDescription, "setAccessibleColumnDescription");
   pragma Export (Java, IsAccessibleSelected, "isAccessibleSelected");
   pragma Export (Java, IsAccessibleRowSelected, "isAccessibleRowSelected");
   pragma Export (Java, IsAccessibleColumnSelected, "isAccessibleColumnSelected");
   pragma Export (Java, GetSelectedAccessibleRows, "getSelectedAccessibleRows");
   pragma Export (Java, GetSelectedAccessibleColumns, "getSelectedAccessibleColumns");

end Javax.Accessibility.AccessibleTable;
pragma Import (Java, Javax.Accessibility.AccessibleTable, "javax.accessibility.AccessibleTable");
pragma Extensions_Allowed (Off);
