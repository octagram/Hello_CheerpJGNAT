pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Accessibility.AccessibleTableModelChange is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return Java.Int is abstract;

   function GetFirstRow (This : access Typ)
                         return Java.Int is abstract;

   function GetLastRow (This : access Typ)
                        return Java.Int is abstract;

   function GetFirstColumn (This : access Typ)
                            return Java.Int is abstract;

   function GetLastColumn (This : access Typ)
                           return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INSERT : constant Java.Int;

   --  final
   UPDATE : constant Java.Int;

   --  final
   DELETE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetFirstRow, "getFirstRow");
   pragma Export (Java, GetLastRow, "getLastRow");
   pragma Export (Java, GetFirstColumn, "getFirstColumn");
   pragma Export (Java, GetLastColumn, "getLastColumn");
   pragma Import (Java, INSERT, "INSERT");
   pragma Import (Java, UPDATE, "UPDATE");
   pragma Import (Java, DELETE, "DELETE");

end Javax.Accessibility.AccessibleTableModelChange;
pragma Import (Java, Javax.Accessibility.AccessibleTableModelChange, "javax.accessibility.AccessibleTableModelChange");
pragma Extensions_Allowed (Off);
