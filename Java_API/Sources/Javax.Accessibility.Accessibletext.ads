pragma Extensions_Allowed (On);
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleText is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIndexAtPoint (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return Java.Int is abstract;

   function GetCharacterBounds (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Awt.Rectangle.Typ'Class is abstract;

   function GetCharCount (This : access Typ)
                          return Java.Int is abstract;

   function GetCaretPosition (This : access Typ)
                              return Java.Int is abstract;

   function GetAtIndex (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class is abstract;

   function GetAfterIndex (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetBeforeIndex (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetCharacterAttribute (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function GetSelectionStart (This : access Typ)
                               return Java.Int is abstract;

   function GetSelectionEnd (This : access Typ)
                             return Java.Int is abstract;

   function GetSelectedText (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CHARACTER : constant Java.Int;

   --  final
   WORD : constant Java.Int;

   --  final
   SENTENCE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetIndexAtPoint, "getIndexAtPoint");
   pragma Export (Java, GetCharacterBounds, "getCharacterBounds");
   pragma Export (Java, GetCharCount, "getCharCount");
   pragma Export (Java, GetCaretPosition, "getCaretPosition");
   pragma Export (Java, GetAtIndex, "getAtIndex");
   pragma Export (Java, GetAfterIndex, "getAfterIndex");
   pragma Export (Java, GetBeforeIndex, "getBeforeIndex");
   pragma Export (Java, GetCharacterAttribute, "getCharacterAttribute");
   pragma Export (Java, GetSelectionStart, "getSelectionStart");
   pragma Export (Java, GetSelectionEnd, "getSelectionEnd");
   pragma Export (Java, GetSelectedText, "getSelectedText");
   pragma Import (Java, CHARACTER, "CHARACTER");
   pragma Import (Java, WORD, "WORD");
   pragma Import (Java, SENTENCE, "SENTENCE");

end Javax.Accessibility.AccessibleText;
pragma Import (Java, Javax.Accessibility.AccessibleText, "javax.accessibility.AccessibleText");
pragma Extensions_Allowed (Off);
