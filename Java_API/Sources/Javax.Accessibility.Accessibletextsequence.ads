pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleTextSequence is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      StartIndex : Java.Int;
      pragma Import (Java, StartIndex, "startIndex");

      EndIndex : Java.Int;
      pragma Import (Java, EndIndex, "endIndex");

      Text : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Text, "text");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AccessibleTextSequence (P1_Int : Java.Int;
                                        P2_Int : Java.Int;
                                        P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleTextSequence);

end Javax.Accessibility.AccessibleTextSequence;
pragma Import (Java, Javax.Accessibility.AccessibleTextSequence, "javax.accessibility.AccessibleTextSequence");
pragma Extensions_Allowed (Off);
