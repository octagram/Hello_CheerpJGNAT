pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
with Java.Lang.Object;

package Javax.Accessibility.AccessibleValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCurrentAccessibleValue (This : access Typ)
                                       return access Java.Lang.Number.Typ'Class is abstract;

   function SetCurrentAccessibleValue (This : access Typ;
                                       P1_Number : access Standard.Java.Lang.Number.Typ'Class)
                                       return Java.Boolean is abstract;

   function GetMinimumAccessibleValue (This : access Typ)
                                       return access Java.Lang.Number.Typ'Class is abstract;

   function GetMaximumAccessibleValue (This : access Typ)
                                       return access Java.Lang.Number.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCurrentAccessibleValue, "getCurrentAccessibleValue");
   pragma Export (Java, SetCurrentAccessibleValue, "setCurrentAccessibleValue");
   pragma Export (Java, GetMinimumAccessibleValue, "getMinimumAccessibleValue");
   pragma Export (Java, GetMaximumAccessibleValue, "getMaximumAccessibleValue");

end Javax.Accessibility.AccessibleValue;
pragma Import (Java, Javax.Accessibility.AccessibleValue, "javax.accessibility.AccessibleValue");
pragma Extensions_Allowed (Off);
