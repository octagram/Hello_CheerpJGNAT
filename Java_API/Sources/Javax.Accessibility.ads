pragma Extensions_Allowed (On);
package Javax.Accessibility is
   pragma Preelaborate;
end Javax.Accessibility;
pragma Import (Java, Javax.Accessibility, "javax.accessibility");
pragma Extensions_Allowed (Off);
