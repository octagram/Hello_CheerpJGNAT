pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Awt.Datatransfer.DataFlavor;
with Java.Io.Externalizable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Activation.ActivationDataFlavor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Externalizable_I : Java.Io.Externalizable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Datatransfer.DataFlavor.Typ(Externalizable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActivationDataFlavor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                                      P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_ActivationDataFlavor (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_ActivationDataFlavor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMimeType (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetRepresentationClass (This : access Typ)
                                    return access Java.Lang.Class.Typ'Class;

   function GetHumanPresentableName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   procedure SetHumanPresentableName (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function Equals (This : access Typ;
                    P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                    return Java.Boolean;

   function IsMimeTypeEqual (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActivationDataFlavor);
   pragma Import (Java, GetMimeType, "getMimeType");
   pragma Import (Java, GetRepresentationClass, "getRepresentationClass");
   pragma Import (Java, GetHumanPresentableName, "getHumanPresentableName");
   pragma Import (Java, SetHumanPresentableName, "setHumanPresentableName");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, IsMimeTypeEqual, "isMimeTypeEqual");

end Javax.Activation.ActivationDataFlavor;
pragma Import (Java, Javax.Activation.ActivationDataFlavor, "javax.activation.ActivationDataFlavor");
pragma Extensions_Allowed (Off);
