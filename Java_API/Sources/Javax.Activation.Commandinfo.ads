pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Javax.Activation.DataHandler;
with Java.Lang.Object;

package Javax.Activation.CommandInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CommandInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCommandName (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetCommandClass (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetCommandObject (This : access Typ;
                              P1_DataHandler : access Standard.Javax.Activation.DataHandler.Typ'Class;
                              P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                              return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CommandInfo);
   pragma Import (Java, GetCommandName, "getCommandName");
   pragma Import (Java, GetCommandClass, "getCommandClass");
   pragma Import (Java, GetCommandObject, "getCommandObject");

end Javax.Activation.CommandInfo;
pragma Import (Java, Javax.Activation.CommandInfo, "javax.activation.CommandInfo");
pragma Extensions_Allowed (Off);
