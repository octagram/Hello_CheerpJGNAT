pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Activation.CommandInfo;
limited with Javax.Activation.DataContentHandler;
limited with Javax.Activation.DataSource;
with Java.Lang.Object;

package Javax.Activation.CommandMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CommandMap (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetDefaultCommandMap return access Javax.Activation.CommandMap.Typ'Class;

   --  synchronized
   procedure SetDefaultCommandMap (P1_CommandMap : access Standard.Javax.Activation.CommandMap.Typ'Class);

   function GetPreferredCommands (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return Standard.Java.Lang.Object.Ref is abstract;

   function GetPreferredCommands (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_DataSource : access Standard.Javax.Activation.DataSource.Typ'Class)
                                  return Standard.Java.Lang.Object.Ref;

   function GetAllCommands (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Standard.Java.Lang.Object.Ref is abstract;

   function GetAllCommands (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_DataSource : access Standard.Javax.Activation.DataSource.Typ'Class)
                            return Standard.Java.Lang.Object.Ref;

   function GetCommand (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Activation.CommandInfo.Typ'Class is abstract;

   function GetCommand (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_DataSource : access Standard.Javax.Activation.DataSource.Typ'Class)
                        return access Javax.Activation.CommandInfo.Typ'Class;

   function CreateDataContentHandler (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Javax.Activation.DataContentHandler.Typ'Class is abstract;

   function CreateDataContentHandler (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_DataSource : access Standard.Javax.Activation.DataSource.Typ'Class)
                                      return access Javax.Activation.DataContentHandler.Typ'Class;

   function GetMimeTypes (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CommandMap);
   pragma Export (Java, GetDefaultCommandMap, "getDefaultCommandMap");
   pragma Export (Java, SetDefaultCommandMap, "setDefaultCommandMap");
   pragma Export (Java, GetPreferredCommands, "getPreferredCommands");
   pragma Export (Java, GetAllCommands, "getAllCommands");
   pragma Export (Java, GetCommand, "getCommand");
   pragma Export (Java, CreateDataContentHandler, "createDataContentHandler");
   pragma Export (Java, GetMimeTypes, "getMimeTypes");

end Javax.Activation.CommandMap;
pragma Import (Java, Javax.Activation.CommandMap, "javax.activation.CommandMap");
pragma Extensions_Allowed (Off);
