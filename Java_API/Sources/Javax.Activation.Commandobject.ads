pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Activation.DataHandler;
with Java.Lang.Object;

package Javax.Activation.CommandObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetCommandContext (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_DataHandler : access Standard.Javax.Activation.DataHandler.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetCommandContext, "setCommandContext");

end Javax.Activation.CommandObject;
pragma Import (Java, Javax.Activation.CommandObject, "javax.activation.CommandObject");
pragma Extensions_Allowed (Off);
