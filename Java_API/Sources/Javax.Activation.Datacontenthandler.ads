pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Javax.Activation.DataSource;
with Java.Lang.Object;

package Javax.Activation.DataContentHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransferDataFlavors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function GetTransferData (This : access Typ;
                             P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class;
                             P2_DataSource : access Standard.Javax.Activation.DataSource.Typ'Class)
                             return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Awt.Datatransfer.UnsupportedFlavorException.Except and
   --  Java.Io.IOException.Except

   function GetContent (This : access Typ;
                        P1_DataSource : access Standard.Javax.Activation.DataSource.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteTo (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTransferDataFlavors, "getTransferDataFlavors");
   pragma Export (Java, GetTransferData, "getTransferData");
   pragma Export (Java, GetContent, "getContent");
   pragma Export (Java, WriteTo, "writeTo");

end Javax.Activation.DataContentHandler;
pragma Import (Java, Javax.Activation.DataContentHandler, "javax.activation.DataContentHandler");
pragma Extensions_Allowed (Off);
