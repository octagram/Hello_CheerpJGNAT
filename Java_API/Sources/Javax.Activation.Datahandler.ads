pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Activation.CommandInfo;
limited with Javax.Activation.CommandMap;
limited with Javax.Activation.DataContentHandlerFactory;
limited with Javax.Activation.DataSource;
with Java.Awt.Datatransfer.Transferable;
with Java.Lang.Object;

package Javax.Activation.DataHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transferable_I : Java.Awt.Datatransfer.Transferable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DataHandler (P1_DataSource : access Standard.Javax.Activation.DataSource.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_DataHandler (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_DataHandler (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDataSource (This : access Typ)
                           return access Javax.Activation.DataSource.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure WriteTo (This : access Typ;
                      P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function GetTransferDataFlavors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function IsDataFlavorSupported (This : access Typ;
                                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                   return Java.Boolean;

   function GetTransferData (This : access Typ;
                             P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Awt.Datatransfer.UnsupportedFlavorException.Except and
   --  Java.Io.IOException.Except

   --  synchronized
   procedure SetCommandMap (This : access Typ;
                            P1_CommandMap : access Standard.Javax.Activation.CommandMap.Typ'Class);

   function GetPreferredCommands (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref;

   function GetAllCommands (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetCommand (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Activation.CommandInfo.Typ'Class;

   function GetContent (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetBean (This : access Typ;
                     P1_CommandInfo : access Standard.Javax.Activation.CommandInfo.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;

   --  synchronized
   procedure SetDataContentHandlerFactory (P1_DataContentHandlerFactory : access Standard.Javax.Activation.DataContentHandlerFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DataHandler);
   pragma Import (Java, GetDataSource, "getDataSource");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetContentType, "getContentType");
   pragma Import (Java, GetInputStream, "getInputStream");
   pragma Import (Java, WriteTo, "writeTo");
   pragma Import (Java, GetOutputStream, "getOutputStream");
   pragma Import (Java, GetTransferDataFlavors, "getTransferDataFlavors");
   pragma Import (Java, IsDataFlavorSupported, "isDataFlavorSupported");
   pragma Import (Java, GetTransferData, "getTransferData");
   pragma Import (Java, SetCommandMap, "setCommandMap");
   pragma Import (Java, GetPreferredCommands, "getPreferredCommands");
   pragma Import (Java, GetAllCommands, "getAllCommands");
   pragma Import (Java, GetCommand, "getCommand");
   pragma Import (Java, GetContent, "getContent");
   pragma Import (Java, GetBean, "getBean");
   pragma Import (Java, SetDataContentHandlerFactory, "setDataContentHandlerFactory");

end Javax.Activation.DataHandler;
pragma Import (Java, Javax.Activation.DataHandler, "javax.activation.DataHandler");
pragma Extensions_Allowed (Off);
