pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Activation.DataSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetInputStream, "getInputStream");
   pragma Export (Java, GetOutputStream, "getOutputStream");
   pragma Export (Java, GetContentType, "getContentType");
   pragma Export (Java, GetName, "getName");

end Javax.Activation.DataSource;
pragma Import (Java, Javax.Activation.DataSource, "javax.activation.DataSource");
pragma Extensions_Allowed (Off);
