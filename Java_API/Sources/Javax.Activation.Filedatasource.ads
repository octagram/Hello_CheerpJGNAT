pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Javax.Activation.FileTypeMap;
with Java.Lang.Object;
with Javax.Activation.DataSource;

package Javax.Activation.FileDataSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DataSource_I : Javax.Activation.DataSource.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileDataSource (P1_File : access Standard.Java.Io.File.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_FileDataSource (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetFile (This : access Typ)
                     return access Java.Io.File.Typ'Class;

   procedure SetFileTypeMap (This : access Typ;
                             P1_FileTypeMap : access Standard.Javax.Activation.FileTypeMap.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileDataSource);
   pragma Import (Java, GetInputStream, "getInputStream");
   pragma Import (Java, GetOutputStream, "getOutputStream");
   pragma Import (Java, GetContentType, "getContentType");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetFile, "getFile");
   pragma Import (Java, SetFileTypeMap, "setFileTypeMap");

end Javax.Activation.FileDataSource;
pragma Import (Java, Javax.Activation.FileDataSource, "javax.activation.FileDataSource");
pragma Extensions_Allowed (Off);
