pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Activation.FileTypeMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FileTypeMap (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContentType (This : access Typ;
                            P1_File : access Standard.Java.Io.File.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetContentType (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;

   --  synchronized
   procedure SetDefaultFileTypeMap (P1_FileTypeMap : access Standard.Javax.Activation.FileTypeMap.Typ'Class);

   --  synchronized
   function GetDefaultFileTypeMap return access Javax.Activation.FileTypeMap.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileTypeMap);
   pragma Export (Java, GetContentType, "getContentType");
   pragma Import (Java, SetDefaultFileTypeMap, "setDefaultFileTypeMap");
   pragma Import (Java, GetDefaultFileTypeMap, "getDefaultFileTypeMap");

end Javax.Activation.FileTypeMap;
pragma Import (Java, Javax.Activation.FileTypeMap, "javax.activation.FileTypeMap");
pragma Extensions_Allowed (Off);
