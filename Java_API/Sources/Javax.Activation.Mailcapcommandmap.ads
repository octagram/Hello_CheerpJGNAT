pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Javax.Activation.CommandInfo;
limited with Javax.Activation.DataContentHandler;
with Java.Lang.Object;
with Javax.Activation.CommandMap;

package Javax.Activation.MailcapCommandMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Activation.CommandMap.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MailcapCommandMap (This : Ref := null)
                                   return Ref;

   function New_MailcapCommandMap (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Java.Io.IOException.Except

   function New_MailcapCommandMap (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetPreferredCommands (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetAllCommands (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetCommand (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Activation.CommandInfo.Typ'Class;

   --  synchronized
   procedure AddMailcap (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   function CreateDataContentHandler (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Javax.Activation.DataContentHandler.Typ'Class;

   --  synchronized
   function GetMimeTypes (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetNativeCommands (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MailcapCommandMap);
   pragma Import (Java, GetPreferredCommands, "getPreferredCommands");
   pragma Import (Java, GetAllCommands, "getAllCommands");
   pragma Import (Java, GetCommand, "getCommand");
   pragma Import (Java, AddMailcap, "addMailcap");
   pragma Import (Java, CreateDataContentHandler, "createDataContentHandler");
   pragma Import (Java, GetMimeTypes, "getMimeTypes");
   pragma Import (Java, GetNativeCommands, "getNativeCommands");

end Javax.Activation.MailcapCommandMap;
pragma Import (Java, Javax.Activation.MailcapCommandMap, "javax.activation.MailcapCommandMap");
pragma Extensions_Allowed (Off);
