pragma Extensions_Allowed (On);
limited with Java.Io.ObjectInput;
limited with Java.Io.ObjectOutput;
limited with Java.Lang.String;
limited with Javax.Activation.MimeTypeParameterList;
with Java.Io.Externalizable;
with Java.Lang.Object;

package Javax.Activation.MimeType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Externalizable_I : Java.Io.Externalizable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MimeType (This : Ref := null)
                          return Ref;

   function New_MimeType (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Javax.Activation.MimeTypeParseException.Except

   function New_MimeType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Javax.Activation.MimeTypeParseException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrimaryType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetPrimaryType (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Activation.MimeTypeParseException.Except

   function GetSubType (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetSubType (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Activation.MimeTypeParseException.Except

   function GetParameters (This : access Typ)
                           return access Javax.Activation.MimeTypeParameterList.Typ'Class;

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   procedure SetParameter (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveParameter (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetBaseType (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function Match (This : access Typ;
                   P1_MimeType : access Standard.Javax.Activation.MimeType.Typ'Class)
                   return Java.Boolean;

   function Match (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return Java.Boolean;
   --  can raise Javax.Activation.MimeTypeParseException.Except

   procedure WriteExternal (This : access Typ;
                            P1_ObjectOutput : access Standard.Java.Io.ObjectOutput.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure ReadExternal (This : access Typ;
                           P1_ObjectInput : access Standard.Java.Io.ObjectInput.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MimeType);
   pragma Import (Java, GetPrimaryType, "getPrimaryType");
   pragma Import (Java, SetPrimaryType, "setPrimaryType");
   pragma Import (Java, GetSubType, "getSubType");
   pragma Import (Java, SetSubType, "setSubType");
   pragma Import (Java, GetParameters, "getParameters");
   pragma Import (Java, GetParameter, "getParameter");
   pragma Import (Java, SetParameter, "setParameter");
   pragma Import (Java, RemoveParameter, "removeParameter");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetBaseType, "getBaseType");
   pragma Import (Java, Match, "match");
   pragma Import (Java, WriteExternal, "writeExternal");
   pragma Import (Java, ReadExternal, "readExternal");

end Javax.Activation.MimeType;
pragma Import (Java, Javax.Activation.MimeType, "javax.activation.MimeType");
pragma Extensions_Allowed (Off);
