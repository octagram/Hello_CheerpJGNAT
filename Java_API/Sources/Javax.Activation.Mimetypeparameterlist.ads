pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Javax.Activation.MimeTypeParameterList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MimeTypeParameterList (This : Ref := null)
                                       return Ref;

   function New_MimeTypeParameterList (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;
   --  can raise Javax.Activation.MimeTypeParseException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Activation.MimeTypeParseException.Except

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.String.Typ'Class;

   procedure Set (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetNames (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MimeTypeParameterList);
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, GetNames, "getNames");
   pragma Import (Java, ToString, "toString");

end Javax.Activation.MimeTypeParameterList;
pragma Import (Java, Javax.Activation.MimeTypeParameterList, "javax.activation.MimeTypeParameterList");
pragma Extensions_Allowed (Off);
