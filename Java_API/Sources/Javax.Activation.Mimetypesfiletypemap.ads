pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Activation.FileTypeMap;

package Javax.Activation.MimetypesFileTypeMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Activation.FileTypeMap.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MimetypesFileTypeMap (This : Ref := null)
                                      return Ref;

   function New_MimetypesFileTypeMap (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;
   --  can raise Java.Io.IOException.Except

   function New_MimetypesFileTypeMap (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddMimeTypes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetContentType (This : access Typ;
                            P1_File : access Standard.Java.Io.File.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetContentType (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MimetypesFileTypeMap);
   pragma Import (Java, AddMimeTypes, "addMimeTypes");
   pragma Import (Java, GetContentType, "getContentType");

end Javax.Activation.MimetypesFileTypeMap;
pragma Import (Java, Javax.Activation.MimetypesFileTypeMap, "javax.activation.MimetypesFileTypeMap");
pragma Extensions_Allowed (Off);
