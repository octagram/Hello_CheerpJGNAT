pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Net.URL;
with Java.Lang.Object;
with Javax.Activation.DataSource;

package Javax.Activation.URLDataSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DataSource_I : Javax.Activation.DataSource.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_URLDataSource (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetURL (This : access Typ)
                    return access Java.Net.URL.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_URLDataSource);
   pragma Import (Java, GetContentType, "getContentType");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetInputStream, "getInputStream");
   pragma Import (Java, GetOutputStream, "getOutputStream");
   pragma Import (Java, GetURL, "getURL");

end Javax.Activation.URLDataSource;
pragma Import (Java, Javax.Activation.URLDataSource, "javax.activation.URLDataSource");
pragma Extensions_Allowed (Off);
