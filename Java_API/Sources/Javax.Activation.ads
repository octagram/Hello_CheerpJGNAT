pragma Extensions_Allowed (On);
package Javax.Activation is
   pragma Preelaborate;
end Javax.Activation;
pragma Import (Java, Javax.Activation, "javax.activation");
pragma Extensions_Allowed (Off);
