pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.RemoteException;

package Javax.Activity.ActivityCompletedException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Rmi.RemoteException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActivityCompletedException (This : Ref := null)
                                            return Ref;

   function New_ActivityCompletedException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_ActivityCompletedException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_ActivityCompletedException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                            P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.activity.ActivityCompletedException");
   pragma Java_Constructor (New_ActivityCompletedException);

end Javax.Activity.ActivityCompletedException;
pragma Import (Java, Javax.Activity.ActivityCompletedException, "javax.activity.ActivityCompletedException");
pragma Extensions_Allowed (Off);
