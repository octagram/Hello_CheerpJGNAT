pragma Extensions_Allowed (On);
package Javax.Activity is
   pragma Preelaborate;
end Javax.Activity;
pragma Import (Java, Javax.Activity, "javax.activity");
pragma Extensions_Allowed (Off);
