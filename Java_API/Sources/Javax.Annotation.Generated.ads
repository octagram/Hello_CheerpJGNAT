pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Annotation.Generated is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Standard.Java.Lang.Object.Ref is abstract;

   function Date (This : access Typ)
                  return access Java.Lang.String.Typ'Class is abstract;

   function Comments (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Value, "value");
   pragma Export (Java, Date, "date");
   pragma Export (Java, Comments, "comments");

end Javax.Annotation.Generated;
pragma Import (Java, Javax.Annotation.Generated, "javax.annotation.Generated");
pragma Extensions_Allowed (Off);
