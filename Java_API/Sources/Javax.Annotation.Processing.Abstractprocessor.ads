pragma Extensions_Allowed (On);
limited with Java.Lang.Iterable;
limited with Java.Lang.String;
limited with Java.Util.Set;
limited with Javax.Annotation.Processing.ProcessingEnvironment;
limited with Javax.Annotation.Processing.RoundEnvironment;
limited with Javax.Lang.Model.Element.AnnotationMirror;
limited with Javax.Lang.Model.Element.Element;
limited with Javax.Lang.Model.Element.ExecutableElement;
limited with Javax.Lang.Model.SourceVersion;
with Java.Lang.Object;
with Javax.Annotation.Processing.Processor;

package Javax.Annotation.Processing.AbstractProcessor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Processor_I : Javax.Annotation.Processing.Processor.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ProcessingEnv : access Javax.Annotation.Processing.ProcessingEnvironment.Typ'Class;
      pragma Import (Java, ProcessingEnv, "processingEnv");

   end record;

   --  protected
   function New_AbstractProcessor (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSupportedOptions (This : access Typ)
                                 return access Java.Util.Set.Typ'Class;

   function GetSupportedAnnotationTypes (This : access Typ)
                                         return access Java.Util.Set.Typ'Class;

   function GetSupportedSourceVersion (This : access Typ)
                                       return access Javax.Lang.Model.SourceVersion.Typ'Class;

   --  synchronized
   procedure Init (This : access Typ;
                   P1_ProcessingEnvironment : access Standard.Javax.Annotation.Processing.ProcessingEnvironment.Typ'Class);

   function Process (This : access Typ;
                     P1_Set : access Standard.Java.Util.Set.Typ'Class;
                     P2_RoundEnvironment : access Standard.Javax.Annotation.Processing.RoundEnvironment.Typ'Class)
                     return Java.Boolean is abstract;

   function GetCompletions (This : access Typ;
                            P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                            P2_AnnotationMirror : access Standard.Javax.Lang.Model.Element.AnnotationMirror.Typ'Class;
                            P3_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                            P4_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.Iterable.Typ'Class;

   --  protected  synchronized
   function IsInitialized (This : access Typ)
                           return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractProcessor);
   pragma Export (Java, GetSupportedOptions, "getSupportedOptions");
   pragma Export (Java, GetSupportedAnnotationTypes, "getSupportedAnnotationTypes");
   pragma Export (Java, GetSupportedSourceVersion, "getSupportedSourceVersion");
   pragma Export (Java, Init, "init");
   pragma Export (Java, Process, "process");
   pragma Export (Java, GetCompletions, "getCompletions");
   pragma Export (Java, IsInitialized, "isInitialized");

end Javax.Annotation.Processing.AbstractProcessor;
pragma Import (Java, Javax.Annotation.Processing.AbstractProcessor, "javax.annotation.processing.AbstractProcessor");
pragma Extensions_Allowed (Off);
