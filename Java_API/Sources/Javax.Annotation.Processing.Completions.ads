pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Annotation.Processing.Completion;
with Java.Lang.Object;

package Javax.Annotation.Processing.Completions is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function of_K (P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Javax.Annotation.Processing.Completion.Typ'Class;

   function of_K (P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Javax.Annotation.Processing.Completion.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, of_K, "of");

end Javax.Annotation.Processing.Completions;
pragma Import (Java, Javax.Annotation.Processing.Completions, "javax.annotation.processing.Completions");
pragma Extensions_Allowed (Off);
