pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Javax.Lang.Model.Element.Element;
limited with Javax.Tools.FileObject;
limited with Javax.Tools.JavaFileManager.Location;
limited with Javax.Tools.JavaFileObject;
with Java.Lang.Object;

package Javax.Annotation.Processing.Filer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateSourceFile (This : access Typ;
                              P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                              P2_Element_Arr : access Javax.Lang.Model.Element.Element.Arr_Obj)
                              return access Javax.Tools.JavaFileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function CreateClassFile (This : access Typ;
                             P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                             P2_Element_Arr : access Javax.Lang.Model.Element.Element.Arr_Obj)
                             return access Javax.Tools.JavaFileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function CreateResource (This : access Typ;
                            P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                            P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                            P3_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                            P4_Element_Arr : access Javax.Lang.Model.Element.Element.Arr_Obj)
                            return access Javax.Tools.FileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetResource (This : access Typ;
                         P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                         P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                         P3_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                         return access Javax.Tools.FileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateSourceFile, "createSourceFile");
   pragma Export (Java, CreateClassFile, "createClassFile");
   pragma Export (Java, CreateResource, "createResource");
   pragma Export (Java, GetResource, "getResource");

end Javax.Annotation.Processing.Filer;
pragma Import (Java, Javax.Annotation.Processing.Filer, "javax.annotation.processing.Filer");
pragma Extensions_Allowed (Off);
