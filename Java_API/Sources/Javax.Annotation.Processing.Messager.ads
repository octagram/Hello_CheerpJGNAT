pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Javax.Lang.Model.Element.AnnotationMirror;
limited with Javax.Lang.Model.Element.AnnotationValue;
limited with Javax.Lang.Model.Element.Element;
limited with Javax.Tools.Diagnostic.Kind;
with Java.Lang.Object;

package Javax.Annotation.Processing.Messager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PrintMessage (This : access Typ;
                           P1_Kind : access Standard.Javax.Tools.Diagnostic.Kind.Typ'Class;
                           P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class) is abstract;

   procedure PrintMessage (This : access Typ;
                           P1_Kind : access Standard.Javax.Tools.Diagnostic.Kind.Typ'Class;
                           P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                           P3_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class) is abstract;

   procedure PrintMessage (This : access Typ;
                           P1_Kind : access Standard.Javax.Tools.Diagnostic.Kind.Typ'Class;
                           P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                           P3_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                           P4_AnnotationMirror : access Standard.Javax.Lang.Model.Element.AnnotationMirror.Typ'Class) is abstract;

   procedure PrintMessage (This : access Typ;
                           P1_Kind : access Standard.Javax.Tools.Diagnostic.Kind.Typ'Class;
                           P2_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class;
                           P3_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                           P4_AnnotationMirror : access Standard.Javax.Lang.Model.Element.AnnotationMirror.Typ'Class;
                           P5_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, PrintMessage, "printMessage");

end Javax.Annotation.Processing.Messager;
pragma Import (Java, Javax.Annotation.Processing.Messager, "javax.annotation.processing.Messager");
pragma Extensions_Allowed (Off);
