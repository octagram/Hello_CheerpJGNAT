pragma Extensions_Allowed (On);
limited with Java.Util.Locale;
limited with Java.Util.Map;
limited with Javax.Annotation.Processing.Filer;
limited with Javax.Annotation.Processing.Messager;
limited with Javax.Lang.Model.SourceVersion;
limited with Javax.Lang.Model.Util.Elements;
limited with Javax.Lang.Model.Util.Types;
with Java.Lang.Object;

package Javax.Annotation.Processing.ProcessingEnvironment is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOptions (This : access Typ)
                        return access Java.Util.Map.Typ'Class is abstract;

   function GetMessager (This : access Typ)
                         return access Javax.Annotation.Processing.Messager.Typ'Class is abstract;

   function GetFiler (This : access Typ)
                      return access Javax.Annotation.Processing.Filer.Typ'Class is abstract;

   function GetElementUtils (This : access Typ)
                             return access Javax.Lang.Model.Util.Elements.Typ'Class is abstract;

   function GetTypeUtils (This : access Typ)
                          return access Javax.Lang.Model.Util.Types.Typ'Class is abstract;

   function GetSourceVersion (This : access Typ)
                              return access Javax.Lang.Model.SourceVersion.Typ'Class is abstract;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetOptions, "getOptions");
   pragma Export (Java, GetMessager, "getMessager");
   pragma Export (Java, GetFiler, "getFiler");
   pragma Export (Java, GetElementUtils, "getElementUtils");
   pragma Export (Java, GetTypeUtils, "getTypeUtils");
   pragma Export (Java, GetSourceVersion, "getSourceVersion");
   pragma Export (Java, GetLocale, "getLocale");

end Javax.Annotation.Processing.ProcessingEnvironment;
pragma Import (Java, Javax.Annotation.Processing.ProcessingEnvironment, "javax.annotation.processing.ProcessingEnvironment");
pragma Extensions_Allowed (Off);
