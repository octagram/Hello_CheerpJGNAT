pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Util.Set;
limited with Javax.Lang.Model.Element.TypeElement;
with Java.Lang.Object;

package Javax.Annotation.Processing.RoundEnvironment is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ProcessingOver (This : access Typ)
                            return Java.Boolean is abstract;

   function ErrorRaised (This : access Typ)
                         return Java.Boolean is abstract;

   function GetRootElements (This : access Typ)
                             return access Java.Util.Set.Typ'Class is abstract;

   function GetElementsAnnotatedWith (This : access Typ;
                                      P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class)
                                      return access Java.Util.Set.Typ'Class is abstract;

   function GetElementsAnnotatedWith (This : access Typ;
                                      P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                      return access Java.Util.Set.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ProcessingOver, "processingOver");
   pragma Export (Java, ErrorRaised, "errorRaised");
   pragma Export (Java, GetRootElements, "getRootElements");
   pragma Export (Java, GetElementsAnnotatedWith, "getElementsAnnotatedWith");

end Javax.Annotation.Processing.RoundEnvironment;
pragma Import (Java, Javax.Annotation.Processing.RoundEnvironment, "javax.annotation.processing.RoundEnvironment");
pragma Extensions_Allowed (Off);
