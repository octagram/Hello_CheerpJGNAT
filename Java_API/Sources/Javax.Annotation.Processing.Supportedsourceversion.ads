pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.SourceVersion;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Annotation.Processing.SupportedSourceVersion is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return access Javax.Lang.Model.SourceVersion.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Value, "value");

end Javax.Annotation.Processing.SupportedSourceVersion;
pragma Import (Java, Javax.Annotation.Processing.SupportedSourceVersion, "javax.annotation.processing.SupportedSourceVersion");
pragma Extensions_Allowed (Off);
