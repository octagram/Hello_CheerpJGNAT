pragma Extensions_Allowed (On);
package Javax.Annotation.Processing is
   pragma Preelaborate;
end Javax.Annotation.Processing;
pragma Import (Java, Javax.Annotation.Processing, "javax.annotation.processing");
pragma Extensions_Allowed (Off);
