pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Annotation.Resource.AuthenticationType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Annotation.Resource.AuthenticationType.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CONTAINER : access Javax.Annotation.Resource.AuthenticationType.Typ'Class;

   --  final
   APPLICATION : access Javax.Annotation.Resource.AuthenticationType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, CONTAINER, "CONTAINER");
   pragma Import (Java, APPLICATION, "APPLICATION");

end Javax.Annotation.Resource.AuthenticationType;
pragma Import (Java, Javax.Annotation.Resource.AuthenticationType, "javax.annotation.Resource$AuthenticationType");
pragma Extensions_Allowed (Off);
