pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Annotation.Resource.AuthenticationType;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Annotation.Resource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class is abstract;

   function type_K (This : access Typ)
                    return access Java.Lang.Class.Typ'Class is abstract;

   function GetAuthenticationType (This : access Typ)
                                   return access Javax.Annotation.Resource.AuthenticationType.Typ'Class is abstract;

   function Shareable (This : access Typ)
                       return Java.Boolean is abstract;

   function MappedName (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function Description (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Name, "name");
   pragma Export (Java, type_K, "type");
   pragma Export (Java, GetAuthenticationType, "authenticationType");
   pragma Export (Java, Shareable, "shareable");
   pragma Export (Java, MappedName, "mappedName");
   pragma Export (Java, Description, "description");

end Javax.Annotation.Resource;
pragma Import (Java, Javax.Annotation.Resource, "javax.annotation.Resource");
pragma Extensions_Allowed (Off);
