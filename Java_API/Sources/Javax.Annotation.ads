pragma Extensions_Allowed (On);
package Javax.Annotation is
   pragma Preelaborate;
end Javax.Annotation;
pragma Import (Java, Javax.Annotation, "javax.annotation");
pragma Extensions_Allowed (Off);
