pragma Extensions_Allowed (On);
package Javax.Crypto is
   pragma Preelaborate;
end Javax.Crypto;
pragma Import (Java, Javax.Crypto, "javax.crypto");
pragma Extensions_Allowed (Off);
