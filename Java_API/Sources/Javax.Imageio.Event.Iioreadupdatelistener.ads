pragma Extensions_Allowed (On);
limited with Java.Awt.Image.BufferedImage;
limited with Javax.Imageio.ImageReader;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Imageio.Event.IIOReadUpdateListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PassStarted (This : access Typ;
                          P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class;
                          P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int;
                          P7_Int : Java.Int;
                          P8_Int : Java.Int;
                          P9_Int : Java.Int;
                          P10_Int_Arr : Java.Int_Arr) is abstract;

   procedure ImageUpdate (This : access Typ;
                          P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class;
                          P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int;
                          P7_Int : Java.Int;
                          P8_Int : Java.Int;
                          P9_Int_Arr : Java.Int_Arr) is abstract;

   procedure PassComplete (This : access Typ;
                           P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class;
                           P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class) is abstract;

   procedure ThumbnailPassStarted (This : access Typ;
                                   P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class;
                                   P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int;
                                   P7_Int : Java.Int;
                                   P8_Int : Java.Int;
                                   P9_Int : Java.Int;
                                   P10_Int_Arr : Java.Int_Arr) is abstract;

   procedure ThumbnailUpdate (This : access Typ;
                              P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class;
                              P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Int : Java.Int;
                              P7_Int : Java.Int;
                              P8_Int : Java.Int;
                              P9_Int_Arr : Java.Int_Arr) is abstract;

   procedure ThumbnailPassComplete (This : access Typ;
                                    P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class;
                                    P2_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, PassStarted, "passStarted");
   pragma Export (Java, ImageUpdate, "imageUpdate");
   pragma Export (Java, PassComplete, "passComplete");
   pragma Export (Java, ThumbnailPassStarted, "thumbnailPassStarted");
   pragma Export (Java, ThumbnailUpdate, "thumbnailUpdate");
   pragma Export (Java, ThumbnailPassComplete, "thumbnailPassComplete");

end Javax.Imageio.Event.IIOReadUpdateListener;
pragma Import (Java, Javax.Imageio.Event.IIOReadUpdateListener, "javax.imageio.event.IIOReadUpdateListener");
pragma Extensions_Allowed (Off);
