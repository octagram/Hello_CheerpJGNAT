pragma Extensions_Allowed (On);
limited with Javax.Imageio.ImageWriter;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Imageio.Event.IIOWriteProgressListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ImageStarted (This : access Typ;
                           P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class;
                           P2_Int : Java.Int) is abstract;

   procedure ImageProgress (This : access Typ;
                            P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class;
                            P2_Float : Java.Float) is abstract;

   procedure ImageComplete (This : access Typ;
                            P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class) is abstract;

   procedure ThumbnailStarted (This : access Typ;
                               P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure ThumbnailProgress (This : access Typ;
                                P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class;
                                P2_Float : Java.Float) is abstract;

   procedure ThumbnailComplete (This : access Typ;
                                P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class) is abstract;

   procedure WriteAborted (This : access Typ;
                           P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ImageStarted, "imageStarted");
   pragma Export (Java, ImageProgress, "imageProgress");
   pragma Export (Java, ImageComplete, "imageComplete");
   pragma Export (Java, ThumbnailStarted, "thumbnailStarted");
   pragma Export (Java, ThumbnailProgress, "thumbnailProgress");
   pragma Export (Java, ThumbnailComplete, "thumbnailComplete");
   pragma Export (Java, WriteAborted, "writeAborted");

end Javax.Imageio.Event.IIOWriteProgressListener;
pragma Import (Java, Javax.Imageio.Event.IIOWriteProgressListener, "javax.imageio.event.IIOWriteProgressListener");
pragma Extensions_Allowed (Off);
