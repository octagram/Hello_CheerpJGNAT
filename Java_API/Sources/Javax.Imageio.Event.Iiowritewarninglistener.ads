pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Imageio.ImageWriter;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Imageio.Event.IIOWriteWarningListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WarningOccurred (This : access Typ;
                              P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class;
                              P2_Int : Java.Int;
                              P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WarningOccurred, "warningOccurred");

end Javax.Imageio.Event.IIOWriteWarningListener;
pragma Import (Java, Javax.Imageio.Event.IIOWriteWarningListener, "javax.imageio.event.IIOWriteWarningListener");
pragma Extensions_Allowed (Off);
