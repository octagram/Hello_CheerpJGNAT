pragma Extensions_Allowed (On);
package Javax.Imageio.Event is
   pragma Preelaborate;
end Javax.Imageio.Event;
pragma Import (Java, Javax.Imageio.Event, "javax.imageio.event");
pragma Extensions_Allowed (Off);
