pragma Extensions_Allowed (On);
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Util.List;
limited with Javax.Imageio.Metadata.IIOMetadata;
with Java.Lang.Object;

package Javax.Imageio.IIOImage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Image : access Java.Awt.Image.RenderedImage.Typ'Class;
      pragma Import (Java, Image, "image");

      --  protected
      Raster : access Java.Awt.Image.Raster.Typ'Class;
      pragma Import (Java, Raster, "raster");

      --  protected
      Thumbnails : access Java.Util.List.Typ'Class;
      pragma Import (Java, Thumbnails, "thumbnails");

      --  protected
      Metadata : access Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
      pragma Import (Java, Metadata, "metadata");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IIOImage (P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class;
                          P2_List : access Standard.Java.Util.List.Typ'Class;
                          P3_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_IIOImage (P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                          P2_List : access Standard.Java.Util.List.Typ'Class;
                          P3_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRenderedImage (This : access Typ)
                              return access Java.Awt.Image.RenderedImage.Typ'Class;

   procedure SetRenderedImage (This : access Typ;
                               P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class);

   function HasRaster (This : access Typ)
                       return Java.Boolean;

   function GetRaster (This : access Typ)
                       return access Java.Awt.Image.Raster.Typ'Class;

   procedure SetRaster (This : access Typ;
                        P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class);

   function GetNumThumbnails (This : access Typ)
                              return Java.Int;

   function GetThumbnail (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Awt.Image.BufferedImage.Typ'Class;

   function GetThumbnails (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   procedure SetThumbnails (This : access Typ;
                            P1_List : access Standard.Java.Util.List.Typ'Class);

   function GetMetadata (This : access Typ)
                         return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class;

   procedure SetMetadata (This : access Typ;
                          P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IIOImage);
   pragma Import (Java, GetRenderedImage, "getRenderedImage");
   pragma Import (Java, SetRenderedImage, "setRenderedImage");
   pragma Import (Java, HasRaster, "hasRaster");
   pragma Import (Java, GetRaster, "getRaster");
   pragma Import (Java, SetRaster, "setRaster");
   pragma Import (Java, GetNumThumbnails, "getNumThumbnails");
   pragma Import (Java, GetThumbnail, "getThumbnail");
   pragma Import (Java, GetThumbnails, "getThumbnails");
   pragma Import (Java, SetThumbnails, "setThumbnails");
   pragma Import (Java, GetMetadata, "getMetadata");
   pragma Import (Java, SetMetadata, "setMetadata");

end Javax.Imageio.IIOImage;
pragma Import (Java, Javax.Imageio.IIOImage, "javax.imageio.IIOImage");
pragma Extensions_Allowed (Off);
