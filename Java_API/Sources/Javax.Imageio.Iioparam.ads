pragma Extensions_Allowed (On);
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Javax.Imageio.IIOParamController;
limited with Javax.Imageio.ImageTypeSpecifier;
with Java.Lang.Object;

package Javax.Imageio.IIOParam is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      SourceRegion : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, SourceRegion, "sourceRegion");

      --  protected
      SourceXSubsampling : Java.Int;
      pragma Import (Java, SourceXSubsampling, "sourceXSubsampling");

      --  protected
      SourceYSubsampling : Java.Int;
      pragma Import (Java, SourceYSubsampling, "sourceYSubsampling");

      --  protected
      SubsamplingXOffset : Java.Int;
      pragma Import (Java, SubsamplingXOffset, "subsamplingXOffset");

      --  protected
      SubsamplingYOffset : Java.Int;
      pragma Import (Java, SubsamplingYOffset, "subsamplingYOffset");

      --  protected
      SourceBands : Java.Int_Arr;
      pragma Import (Java, SourceBands, "sourceBands");

      --  protected
      DestinationType : access Javax.Imageio.ImageTypeSpecifier.Typ'Class;
      pragma Import (Java, DestinationType, "destinationType");

      --  protected
      DestinationOffset : access Java.Awt.Point.Typ'Class;
      pragma Import (Java, DestinationOffset, "destinationOffset");

      --  protected
      DefaultController : access Javax.Imageio.IIOParamController.Typ'Class;
      pragma Import (Java, DefaultController, "defaultController");

      --  protected
      Controller : access Javax.Imageio.IIOParamController.Typ'Class;
      pragma Import (Java, Controller, "controller");

   end record;

   --  protected
   function New_IIOParam (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSourceRegion (This : access Typ;
                              P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetSourceRegion (This : access Typ)
                             return access Java.Awt.Rectangle.Typ'Class;

   procedure SetSourceSubsampling (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int);

   function GetSourceXSubsampling (This : access Typ)
                                   return Java.Int;

   function GetSourceYSubsampling (This : access Typ)
                                   return Java.Int;

   function GetSubsamplingXOffset (This : access Typ)
                                   return Java.Int;

   function GetSubsamplingYOffset (This : access Typ)
                                   return Java.Int;

   procedure SetSourceBands (This : access Typ;
                             P1_Int_Arr : Java.Int_Arr);

   function GetSourceBands (This : access Typ)
                            return Java.Int_Arr;

   procedure SetDestinationType (This : access Typ;
                                 P1_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class);

   function GetDestinationType (This : access Typ)
                                return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   procedure SetDestinationOffset (This : access Typ;
                                   P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetDestinationOffset (This : access Typ)
                                  return access Java.Awt.Point.Typ'Class;

   procedure SetController (This : access Typ;
                            P1_IIOParamController : access Standard.Javax.Imageio.IIOParamController.Typ'Class);

   function GetController (This : access Typ)
                           return access Javax.Imageio.IIOParamController.Typ'Class;

   function GetDefaultController (This : access Typ)
                                  return access Javax.Imageio.IIOParamController.Typ'Class;

   function HasController (This : access Typ)
                           return Java.Boolean;

   function ActivateController (This : access Typ)
                                return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IIOParam);
   pragma Import (Java, SetSourceRegion, "setSourceRegion");
   pragma Import (Java, GetSourceRegion, "getSourceRegion");
   pragma Import (Java, SetSourceSubsampling, "setSourceSubsampling");
   pragma Import (Java, GetSourceXSubsampling, "getSourceXSubsampling");
   pragma Import (Java, GetSourceYSubsampling, "getSourceYSubsampling");
   pragma Import (Java, GetSubsamplingXOffset, "getSubsamplingXOffset");
   pragma Import (Java, GetSubsamplingYOffset, "getSubsamplingYOffset");
   pragma Import (Java, SetSourceBands, "setSourceBands");
   pragma Import (Java, GetSourceBands, "getSourceBands");
   pragma Import (Java, SetDestinationType, "setDestinationType");
   pragma Import (Java, GetDestinationType, "getDestinationType");
   pragma Import (Java, SetDestinationOffset, "setDestinationOffset");
   pragma Import (Java, GetDestinationOffset, "getDestinationOffset");
   pragma Import (Java, SetController, "setController");
   pragma Import (Java, GetController, "getController");
   pragma Import (Java, GetDefaultController, "getDefaultController");
   pragma Import (Java, HasController, "hasController");
   pragma Import (Java, ActivateController, "activateController");

end Javax.Imageio.IIOParam;
pragma Import (Java, Javax.Imageio.IIOParam, "javax.imageio.IIOParam");
pragma Extensions_Allowed (Off);
