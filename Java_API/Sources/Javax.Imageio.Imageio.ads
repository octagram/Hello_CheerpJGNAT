pragma Extensions_Allowed (On);
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Iterator;
limited with Javax.Imageio.ImageReader;
limited with Javax.Imageio.ImageTypeSpecifier;
limited with Javax.Imageio.ImageWriter;
limited with Javax.Imageio.Stream.ImageInputStream;
limited with Javax.Imageio.Stream.ImageOutputStream;
with Java.Lang.Object;

package Javax.Imageio.ImageIO is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ScanForPlugins ;

   procedure SetUseCache (P1_Boolean : Java.Boolean);

   function GetUseCache return Java.Boolean;

   procedure SetCacheDirectory (P1_File : access Standard.Java.Io.File.Typ'Class);

   function GetCacheDirectory return access Java.Io.File.Typ'Class;

   function CreateImageInputStream (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                    return access Javax.Imageio.Stream.ImageInputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function CreateImageOutputStream (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                     return access Javax.Imageio.Stream.ImageOutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetReaderFormatNames return Standard.Java.Lang.Object.Ref;

   function GetReaderMIMETypes return Standard.Java.Lang.Object.Ref;

   function GetReaderFileSuffixes return Standard.Java.Lang.Object.Ref;

   function GetImageReaders (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Util.Iterator.Typ'Class;

   function GetImageReadersByFormatName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                         return access Java.Util.Iterator.Typ'Class;

   function GetImageReadersBySuffix (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return access Java.Util.Iterator.Typ'Class;

   function GetImageReadersByMIMEType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                       return access Java.Util.Iterator.Typ'Class;

   function GetWriterFormatNames return Standard.Java.Lang.Object.Ref;

   function GetWriterMIMETypes return Standard.Java.Lang.Object.Ref;

   function GetWriterFileSuffixes return Standard.Java.Lang.Object.Ref;

   function GetImageWritersByFormatName (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                         return access Java.Util.Iterator.Typ'Class;

   function GetImageWritersBySuffix (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return access Java.Util.Iterator.Typ'Class;

   function GetImageWritersByMIMEType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                       return access Java.Util.Iterator.Typ'Class;

   function GetImageWriter (P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class)
                            return access Javax.Imageio.ImageWriter.Typ'Class;

   function GetImageReader (P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class)
                            return access Javax.Imageio.ImageReader.Typ'Class;

   function GetImageWriters (P1_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Util.Iterator.Typ'Class;

   function GetImageTranscoders (P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class;
                                 P2_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class)
                                 return access Java.Util.Iterator.Typ'Class;

   function Read (P1_File : access Standard.Java.Io.File.Typ'Class)
                  return access Java.Awt.Image.BufferedImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Read (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                  return access Java.Awt.Image.BufferedImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Read (P1_URL : access Standard.Java.Net.URL.Typ'Class)
                  return access Java.Awt.Image.BufferedImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Read (P1_ImageInputStream : access Standard.Javax.Imageio.Stream.ImageInputStream.Typ'Class)
                  return access Java.Awt.Image.BufferedImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Write (P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_ImageOutputStream : access Standard.Javax.Imageio.Stream.ImageOutputStream.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function Write (P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_File : access Standard.Java.Io.File.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function Write (P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                   P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ScanForPlugins, "scanForPlugins");
   pragma Import (Java, SetUseCache, "setUseCache");
   pragma Import (Java, GetUseCache, "getUseCache");
   pragma Import (Java, SetCacheDirectory, "setCacheDirectory");
   pragma Import (Java, GetCacheDirectory, "getCacheDirectory");
   pragma Import (Java, CreateImageInputStream, "createImageInputStream");
   pragma Import (Java, CreateImageOutputStream, "createImageOutputStream");
   pragma Import (Java, GetReaderFormatNames, "getReaderFormatNames");
   pragma Import (Java, GetReaderMIMETypes, "getReaderMIMETypes");
   pragma Import (Java, GetReaderFileSuffixes, "getReaderFileSuffixes");
   pragma Import (Java, GetImageReaders, "getImageReaders");
   pragma Import (Java, GetImageReadersByFormatName, "getImageReadersByFormatName");
   pragma Import (Java, GetImageReadersBySuffix, "getImageReadersBySuffix");
   pragma Import (Java, GetImageReadersByMIMEType, "getImageReadersByMIMEType");
   pragma Import (Java, GetWriterFormatNames, "getWriterFormatNames");
   pragma Import (Java, GetWriterMIMETypes, "getWriterMIMETypes");
   pragma Import (Java, GetWriterFileSuffixes, "getWriterFileSuffixes");
   pragma Import (Java, GetImageWritersByFormatName, "getImageWritersByFormatName");
   pragma Import (Java, GetImageWritersBySuffix, "getImageWritersBySuffix");
   pragma Import (Java, GetImageWritersByMIMEType, "getImageWritersByMIMEType");
   pragma Import (Java, GetImageWriter, "getImageWriter");
   pragma Import (Java, GetImageReader, "getImageReader");
   pragma Import (Java, GetImageWriters, "getImageWriters");
   pragma Import (Java, GetImageTranscoders, "getImageTranscoders");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Write, "write");

end Javax.Imageio.ImageIO;
pragma Import (Java, Javax.Imageio.ImageIO, "javax.imageio.ImageIO");
pragma Extensions_Allowed (Off);
