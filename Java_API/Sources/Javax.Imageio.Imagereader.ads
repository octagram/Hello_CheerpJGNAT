pragma Extensions_Allowed (On);
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Java.Util.List;
limited with Java.Util.Locale;
limited with Java.Util.Set;
limited with Javax.Imageio.Event.IIOReadProgressListener;
limited with Javax.Imageio.Event.IIOReadUpdateListener;
limited with Javax.Imageio.Event.IIOReadWarningListener;
limited with Javax.Imageio.IIOImage;
limited with Javax.Imageio.ImageReadParam;
limited with Javax.Imageio.ImageTypeSpecifier;
limited with Javax.Imageio.Metadata.IIOMetadata;
limited with Javax.Imageio.Spi.ImageReaderSpi;
with Java.Lang.Object;

package Javax.Imageio.ImageReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      OriginatingProvider : access Javax.Imageio.Spi.ImageReaderSpi.Typ'Class;
      pragma Import (Java, OriginatingProvider, "originatingProvider");

      --  protected
      Input : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Input, "input");

      --  protected
      SeekForwardOnly : Java.Boolean;
      pragma Import (Java, SeekForwardOnly, "seekForwardOnly");

      --  protected
      IgnoreMetadata : Java.Boolean;
      pragma Import (Java, IgnoreMetadata, "ignoreMetadata");

      --  protected
      MinIndex : Java.Int;
      pragma Import (Java, MinIndex, "minIndex");

      --  protected
      AvailableLocales : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, AvailableLocales, "availableLocales");

      --  protected
      Locale : access Java.Util.Locale.Typ'Class;
      pragma Import (Java, Locale, "locale");

      --  protected
      WarningListeners : access Java.Util.List.Typ'Class;
      pragma Import (Java, WarningListeners, "warningListeners");

      --  protected
      WarningLocales : access Java.Util.List.Typ'Class;
      pragma Import (Java, WarningLocales, "warningLocales");

      --  protected
      ProgressListeners : access Java.Util.List.Typ'Class;
      pragma Import (Java, ProgressListeners, "progressListeners");

      --  protected
      UpdateListeners : access Java.Util.List.Typ'Class;
      pragma Import (Java, UpdateListeners, "updateListeners");

   end record;

   --  protected
   function New_ImageReader (P1_ImageReaderSpi : access Standard.Javax.Imageio.Spi.ImageReaderSpi.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFormatName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetOriginatingProvider (This : access Typ)
                                    return access Javax.Imageio.Spi.ImageReaderSpi.Typ'Class;

   procedure SetInput (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Boolean : Java.Boolean;
                       P3_Boolean : Java.Boolean);

   procedure SetInput (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Boolean : Java.Boolean);

   procedure SetInput (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetInput (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function IsSeekForwardOnly (This : access Typ)
                               return Java.Boolean;

   function IsIgnoringMetadata (This : access Typ)
                                return Java.Boolean;

   function GetMinIndex (This : access Typ)
                         return Java.Int;

   function GetAvailableLocales (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   function GetNumImages (This : access Typ;
                          P1_Boolean : Java.Boolean)
                          return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function GetWidth (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function GetHeight (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function IsRandomAccessEasy (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function GetAspectRatio (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;
   --  can raise Java.Io.IOException.Except

   function GetRawImageType (This : access Typ;
                             P1_Int : Java.Int)
                             return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetImageTypes (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Util.Iterator.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetDefaultReadParam (This : access Typ)
                                 return access Javax.Imageio.ImageReadParam.Typ'Class;

   function GetStreamMetadata (This : access Typ)
                               return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract with Export => "getStreamMetadata", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function GetStreamMetadata (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Set : access Standard.Java.Util.Set.Typ'Class)
                               return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class with Import => "getStreamMetadata", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function GetImageMetadata (This : access Typ;
                              P1_Int : Java.Int)
                              return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract with Export => "getImageMetadata", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function GetImageMetadata (This : access Typ;
                              P1_Int : Java.Int;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Set : access Standard.Java.Util.Set.Typ'Class)
                              return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class with Import => "getImageMetadata", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Int : Java.Int)
                  return access Java.Awt.Image.BufferedImage.Typ'Class with Import => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Int : Java.Int;
                  P2_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class)
                  return access Java.Awt.Image.BufferedImage.Typ'Class is abstract with Export => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function ReadAll (This : access Typ;
                     P1_Int : Java.Int;
                     P2_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class)
                     return access Javax.Imageio.IIOImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function ReadAll (This : access Typ;
                     P1_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                     return access Java.Util.Iterator.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function CanReadRaster (This : access Typ)
                           return Java.Boolean;

   function ReadRaster (This : access Typ;
                        P1_Int : Java.Int;
                        P2_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class)
                        return access Java.Awt.Image.Raster.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function IsImageTiled (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function GetTileWidth (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;
   --  can raise Java.Io.IOException.Except

   function GetTileHeight (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;
   --  can raise Java.Io.IOException.Except

   function GetTileGridXOffset (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Int;
   --  can raise Java.Io.IOException.Except

   function GetTileGridYOffset (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadTile (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int)
                      return access Java.Awt.Image.BufferedImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function ReadTileRaster (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return access Java.Awt.Image.Raster.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function ReadAsRenderedImage (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class)
                                 return access Java.Awt.Image.RenderedImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function ReaderSupportsThumbnails (This : access Typ)
                                      return Java.Boolean;

   function HasThumbnails (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function GetNumThumbnails (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;
   --  can raise Java.Io.IOException.Except

   function GetThumbnailWidth (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return Java.Int;
   --  can raise Java.Io.IOException.Except

   function GetThumbnailHeight (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadThumbnail (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Awt.Image.BufferedImage.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure abort_K (This : access Typ);

   --  protected  synchronized
   function AbortRequested (This : access Typ)
                            return Java.Boolean;

   --  protected  synchronized
   procedure ClearAbortRequest (This : access Typ);

   procedure AddIIOReadWarningListener (This : access Typ;
                                        P1_IIOReadWarningListener : access Standard.Javax.Imageio.Event.IIOReadWarningListener.Typ'Class);

   procedure RemoveIIOReadWarningListener (This : access Typ;
                                           P1_IIOReadWarningListener : access Standard.Javax.Imageio.Event.IIOReadWarningListener.Typ'Class);

   procedure RemoveAllIIOReadWarningListeners (This : access Typ);

   procedure AddIIOReadProgressListener (This : access Typ;
                                         P1_IIOReadProgressListener : access Standard.Javax.Imageio.Event.IIOReadProgressListener.Typ'Class);

   procedure RemoveIIOReadProgressListener (This : access Typ;
                                            P1_IIOReadProgressListener : access Standard.Javax.Imageio.Event.IIOReadProgressListener.Typ'Class);

   procedure RemoveAllIIOReadProgressListeners (This : access Typ);

   procedure AddIIOReadUpdateListener (This : access Typ;
                                       P1_IIOReadUpdateListener : access Standard.Javax.Imageio.Event.IIOReadUpdateListener.Typ'Class);

   procedure RemoveIIOReadUpdateListener (This : access Typ;
                                          P1_IIOReadUpdateListener : access Standard.Javax.Imageio.Event.IIOReadUpdateListener.Typ'Class);

   procedure RemoveAllIIOReadUpdateListeners (This : access Typ);

   --  protected
   procedure ProcessSequenceStarted (This : access Typ;
                                     P1_Int : Java.Int);

   --  protected
   procedure ProcessSequenceComplete (This : access Typ);

   --  protected
   procedure ProcessImageStarted (This : access Typ;
                                  P1_Int : Java.Int);

   --  protected
   procedure ProcessImageProgress (This : access Typ;
                                   P1_Float : Java.Float);

   --  protected
   procedure ProcessImageComplete (This : access Typ);

   --  protected
   procedure ProcessThumbnailStarted (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int);

   --  protected
   procedure ProcessThumbnailProgress (This : access Typ;
                                       P1_Float : Java.Float);

   --  protected
   procedure ProcessThumbnailComplete (This : access Typ);

   --  protected
   procedure ProcessReadAborted (This : access Typ);

   --  protected
   procedure ProcessPassStarted (This : access Typ;
                                 P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Int : Java.Int;
                                 P9_Int_Arr : Java.Int_Arr);

   --  protected
   procedure ProcessImageUpdate (This : access Typ;
                                 P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Int_Arr : Java.Int_Arr);

   --  protected
   procedure ProcessPassComplete (This : access Typ;
                                  P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class);

   --  protected
   procedure ProcessThumbnailPassStarted (This : access Typ;
                                          P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                                          P2_Int : Java.Int;
                                          P3_Int : Java.Int;
                                          P4_Int : Java.Int;
                                          P5_Int : Java.Int;
                                          P6_Int : Java.Int;
                                          P7_Int : Java.Int;
                                          P8_Int : Java.Int;
                                          P9_Int_Arr : Java.Int_Arr);

   --  protected
   procedure ProcessThumbnailUpdate (This : access Typ;
                                     P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int;
                                     P7_Int : Java.Int;
                                     P8_Int_Arr : Java.Int_Arr);

   --  protected
   procedure ProcessThumbnailPassComplete (This : access Typ;
                                           P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class);

   --  protected
   procedure ProcessWarningOccurred (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure ProcessWarningOccurred (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Reset (This : access Typ);

   procedure Dispose (This : access Typ);

   --  protected
   function GetSourceRegion (P1_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int)
                             return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   procedure ComputeRegions (P1_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class;
                             P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                             P6_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure CheckReadParamBandSettings (P1_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int);

   --  protected
   function GetDestination (P1_ImageReadParam : access Standard.Javax.Imageio.ImageReadParam.Typ'Class;
                            P2_Iterator : access Standard.Java.Util.Iterator.Typ'Class;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int)
                            return access Java.Awt.Image.BufferedImage.Typ'Class;
   --  can raise Javax.Imageio.IIOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageReader);
   pragma Import (Java, GetFormatName, "getFormatName");
   pragma Import (Java, GetOriginatingProvider, "getOriginatingProvider");
   pragma Import (Java, SetInput, "setInput");
   pragma Import (Java, GetInput, "getInput");
   pragma Import (Java, IsSeekForwardOnly, "isSeekForwardOnly");
   pragma Import (Java, IsIgnoringMetadata, "isIgnoringMetadata");
   pragma Import (Java, GetMinIndex, "getMinIndex");
   pragma Import (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Import (Java, SetLocale, "setLocale");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Export (Java, GetNumImages, "getNumImages");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Import (Java, IsRandomAccessEasy, "isRandomAccessEasy");
   pragma Import (Java, GetAspectRatio, "getAspectRatio");
   pragma Import (Java, GetRawImageType, "getRawImageType");
   pragma Export (Java, GetImageTypes, "getImageTypes");
   pragma Import (Java, GetDefaultReadParam, "getDefaultReadParam");
   -- pragma Import (Java, GetStreamMetadata, "getStreamMetadata");
   -- pragma Import (Java, GetImageMetadata, "getImageMetadata");
   -- pragma Import (Java, Read, "read");
   pragma Import (Java, ReadAll, "readAll");
   pragma Import (Java, CanReadRaster, "canReadRaster");
   pragma Import (Java, ReadRaster, "readRaster");
   pragma Import (Java, IsImageTiled, "isImageTiled");
   pragma Import (Java, GetTileWidth, "getTileWidth");
   pragma Import (Java, GetTileHeight, "getTileHeight");
   pragma Import (Java, GetTileGridXOffset, "getTileGridXOffset");
   pragma Import (Java, GetTileGridYOffset, "getTileGridYOffset");
   pragma Import (Java, ReadTile, "readTile");
   pragma Import (Java, ReadTileRaster, "readTileRaster");
   pragma Import (Java, ReadAsRenderedImage, "readAsRenderedImage");
   pragma Import (Java, ReaderSupportsThumbnails, "readerSupportsThumbnails");
   pragma Import (Java, HasThumbnails, "hasThumbnails");
   pragma Import (Java, GetNumThumbnails, "getNumThumbnails");
   pragma Import (Java, GetThumbnailWidth, "getThumbnailWidth");
   pragma Import (Java, GetThumbnailHeight, "getThumbnailHeight");
   pragma Import (Java, ReadThumbnail, "readThumbnail");
   pragma Import (Java, abort_K, "abort");
   pragma Import (Java, AbortRequested, "abortRequested");
   pragma Import (Java, ClearAbortRequest, "clearAbortRequest");
   pragma Import (Java, AddIIOReadWarningListener, "addIIOReadWarningListener");
   pragma Import (Java, RemoveIIOReadWarningListener, "removeIIOReadWarningListener");
   pragma Import (Java, RemoveAllIIOReadWarningListeners, "removeAllIIOReadWarningListeners");
   pragma Import (Java, AddIIOReadProgressListener, "addIIOReadProgressListener");
   pragma Import (Java, RemoveIIOReadProgressListener, "removeIIOReadProgressListener");
   pragma Import (Java, RemoveAllIIOReadProgressListeners, "removeAllIIOReadProgressListeners");
   pragma Import (Java, AddIIOReadUpdateListener, "addIIOReadUpdateListener");
   pragma Import (Java, RemoveIIOReadUpdateListener, "removeIIOReadUpdateListener");
   pragma Import (Java, RemoveAllIIOReadUpdateListeners, "removeAllIIOReadUpdateListeners");
   pragma Import (Java, ProcessSequenceStarted, "processSequenceStarted");
   pragma Import (Java, ProcessSequenceComplete, "processSequenceComplete");
   pragma Import (Java, ProcessImageStarted, "processImageStarted");
   pragma Import (Java, ProcessImageProgress, "processImageProgress");
   pragma Import (Java, ProcessImageComplete, "processImageComplete");
   pragma Import (Java, ProcessThumbnailStarted, "processThumbnailStarted");
   pragma Import (Java, ProcessThumbnailProgress, "processThumbnailProgress");
   pragma Import (Java, ProcessThumbnailComplete, "processThumbnailComplete");
   pragma Import (Java, ProcessReadAborted, "processReadAborted");
   pragma Import (Java, ProcessPassStarted, "processPassStarted");
   pragma Import (Java, ProcessImageUpdate, "processImageUpdate");
   pragma Import (Java, ProcessPassComplete, "processPassComplete");
   pragma Import (Java, ProcessThumbnailPassStarted, "processThumbnailPassStarted");
   pragma Import (Java, ProcessThumbnailUpdate, "processThumbnailUpdate");
   pragma Import (Java, ProcessThumbnailPassComplete, "processThumbnailPassComplete");
   pragma Import (Java, ProcessWarningOccurred, "processWarningOccurred");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Dispose, "dispose");
   pragma Import (Java, GetSourceRegion, "getSourceRegion");
   pragma Import (Java, ComputeRegions, "computeRegions");
   pragma Import (Java, CheckReadParamBandSettings, "checkReadParamBandSettings");
   pragma Import (Java, GetDestination, "getDestination");

end Javax.Imageio.ImageReader;
pragma Import (Java, Javax.Imageio.ImageReader, "javax.imageio.ImageReader");
pragma Extensions_Allowed (Off);
