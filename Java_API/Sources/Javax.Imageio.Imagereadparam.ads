pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Image.BufferedImage;
limited with Javax.Imageio.ImageTypeSpecifier;
with Java.Lang.Object;
with Javax.Imageio.IIOParam;

package Javax.Imageio.ImageReadParam is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Imageio.IIOParam.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      CanSetSourceRenderSize : Java.Boolean;
      pragma Import (Java, CanSetSourceRenderSize, "canSetSourceRenderSize");

      --  protected
      SourceRenderSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, SourceRenderSize, "sourceRenderSize");

      --  protected
      Destination : access Java.Awt.Image.BufferedImage.Typ'Class;
      pragma Import (Java, Destination, "destination");

      --  protected
      DestinationBands : Java.Int_Arr;
      pragma Import (Java, DestinationBands, "destinationBands");

      --  protected
      MinProgressivePass : Java.Int;
      pragma Import (Java, MinProgressivePass, "minProgressivePass");

      --  protected
      NumProgressivePasses : Java.Int;
      pragma Import (Java, NumProgressivePasses, "numProgressivePasses");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImageReadParam (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDestinationType (This : access Typ;
                                 P1_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class);

   procedure SetDestination (This : access Typ;
                             P1_BufferedImage : access Standard.Java.Awt.Image.BufferedImage.Typ'Class);

   function GetDestination (This : access Typ)
                            return access Java.Awt.Image.BufferedImage.Typ'Class;

   procedure SetDestinationBands (This : access Typ;
                                  P1_Int_Arr : Java.Int_Arr);

   function GetDestinationBands (This : access Typ)
                                 return Java.Int_Arr;

   function CanSetSourceRenderSize (This : access Typ)
                                    return Java.Boolean;

   procedure SetSourceRenderSize (This : access Typ;
                                  P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);
   --  can raise Java.Lang.UnsupportedOperationException.Except

   function GetSourceRenderSize (This : access Typ)
                                 return access Java.Awt.Dimension.Typ'Class;

   procedure SetSourceProgressivePasses (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int);

   function GetSourceMinProgressivePass (This : access Typ)
                                         return Java.Int;

   function GetSourceMaxProgressivePass (This : access Typ)
                                         return Java.Int;

   function GetSourceNumProgressivePasses (This : access Typ)
                                           return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageReadParam);
   pragma Import (Java, SetDestinationType, "setDestinationType");
   pragma Import (Java, SetDestination, "setDestination");
   pragma Import (Java, GetDestination, "getDestination");
   pragma Import (Java, SetDestinationBands, "setDestinationBands");
   pragma Import (Java, GetDestinationBands, "getDestinationBands");
   pragma Import (Java, CanSetSourceRenderSize, "canSetSourceRenderSize");
   pragma Import (Java, SetSourceRenderSize, "setSourceRenderSize");
   pragma Import (Java, GetSourceRenderSize, "getSourceRenderSize");
   pragma Import (Java, SetSourceProgressivePasses, "setSourceProgressivePasses");
   pragma Import (Java, GetSourceMinProgressivePass, "getSourceMinProgressivePass");
   pragma Import (Java, GetSourceMaxProgressivePass, "getSourceMaxProgressivePass");
   pragma Import (Java, GetSourceNumProgressivePasses, "getSourceNumProgressivePasses");

end Javax.Imageio.ImageReadParam;
pragma Import (Java, Javax.Imageio.ImageReadParam, "javax.imageio.ImageReadParam");
pragma Extensions_Allowed (Off);
