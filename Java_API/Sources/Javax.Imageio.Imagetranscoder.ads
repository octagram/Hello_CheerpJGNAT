pragma Extensions_Allowed (On);
limited with Javax.Imageio.ImageTypeSpecifier;
limited with Javax.Imageio.ImageWriteParam;
limited with Javax.Imageio.Metadata.IIOMetadata;
with Java.Lang.Object;

package Javax.Imageio.ImageTranscoder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ConvertStreamMetadata (This : access Typ;
                                   P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                   P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class)
                                   return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract;

   function ConvertImageMetadata (This : access Typ;
                                  P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                  P2_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                                  P3_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class)
                                  return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ConvertStreamMetadata, "convertStreamMetadata");
   pragma Export (Java, ConvertImageMetadata, "convertImageMetadata");

end Javax.Imageio.ImageTranscoder;
pragma Import (Java, Javax.Imageio.ImageTranscoder, "javax.imageio.ImageTranscoder");
pragma Extensions_Allowed (Off);
