pragma Extensions_Allowed (On);
limited with Java.Awt.Color.ColorSpace;
limited with Java.Awt.Image.BufferedImage;
limited with Java.Awt.Image.ColorModel;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Awt.Image.SampleModel;
with Java.Lang.Object;

package Javax.Imageio.ImageTypeSpecifier is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ColorModel : access Java.Awt.Image.ColorModel.Typ'Class;
      pragma Import (Java, ColorModel, "colorModel");

      --  protected
      SampleModel : access Java.Awt.Image.SampleModel.Typ'Class;
      pragma Import (Java, SampleModel, "sampleModel");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImageTypeSpecifier (P1_ColorModel : access Standard.Java.Awt.Image.ColorModel.Typ'Class;
                                    P2_SampleModel : access Standard.Java.Awt.Image.SampleModel.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_ImageTypeSpecifier (P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreatePacked (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int;
                          P7_Boolean : Java.Boolean)
                          return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function CreateInterleaved (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                               P2_Int_Arr : Java.Int_Arr;
                               P3_Int : Java.Int;
                               P4_Boolean : Java.Boolean;
                               P5_Boolean : Java.Boolean)
                               return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function CreateBanded (P1_ColorSpace : access Standard.Java.Awt.Color.ColorSpace.Typ'Class;
                          P2_Int_Arr : Java.Int_Arr;
                          P3_Int_Arr : Java.Int_Arr;
                          P4_Int : Java.Int;
                          P5_Boolean : Java.Boolean;
                          P6_Boolean : Java.Boolean)
                          return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function CreateGrayscale (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Boolean : Java.Boolean)
                             return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function CreateGrayscale (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Boolean : Java.Boolean;
                             P4_Boolean : Java.Boolean)
                             return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function CreateIndexed (P1_Byte_Arr : Java.Byte_Arr;
                           P2_Byte_Arr : Java.Byte_Arr;
                           P3_Byte_Arr : Java.Byte_Arr;
                           P4_Byte_Arr : Java.Byte_Arr;
                           P5_Int : Java.Int;
                           P6_Int : Java.Int)
                           return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function CreateFromBufferedImageType (P1_Int : Java.Int)
                                         return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function CreateFromRenderedImage (P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class)
                                     return access Javax.Imageio.ImageTypeSpecifier.Typ'Class;

   function GetBufferedImageType (This : access Typ)
                                  return Java.Int;

   function GetNumComponents (This : access Typ)
                              return Java.Int;

   function GetNumBands (This : access Typ)
                         return Java.Int;

   function GetBitsPerBand (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Int;

   function GetSampleModel (This : access Typ)
                            return access Java.Awt.Image.SampleModel.Typ'Class;

   function GetSampleModel (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Awt.Image.SampleModel.Typ'Class;

   function GetColorModel (This : access Typ)
                           return access Java.Awt.Image.ColorModel.Typ'Class;

   function CreateBufferedImage (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int)
                                 return access Java.Awt.Image.BufferedImage.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageTypeSpecifier);
   pragma Import (Java, CreatePacked, "createPacked");
   pragma Import (Java, CreateInterleaved, "createInterleaved");
   pragma Import (Java, CreateBanded, "createBanded");
   pragma Import (Java, CreateGrayscale, "createGrayscale");
   pragma Import (Java, CreateIndexed, "createIndexed");
   pragma Import (Java, CreateFromBufferedImageType, "createFromBufferedImageType");
   pragma Import (Java, CreateFromRenderedImage, "createFromRenderedImage");
   pragma Import (Java, GetBufferedImageType, "getBufferedImageType");
   pragma Import (Java, GetNumComponents, "getNumComponents");
   pragma Import (Java, GetNumBands, "getNumBands");
   pragma Import (Java, GetBitsPerBand, "getBitsPerBand");
   pragma Import (Java, GetSampleModel, "getSampleModel");
   pragma Import (Java, GetColorModel, "getColorModel");
   pragma Import (Java, CreateBufferedImage, "createBufferedImage");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Imageio.ImageTypeSpecifier;
pragma Import (Java, Javax.Imageio.ImageTypeSpecifier, "javax.imageio.ImageTypeSpecifier");
pragma Extensions_Allowed (Off);
