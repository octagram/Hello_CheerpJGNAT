pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
limited with Java.Util.Locale;
with Java.Lang.Object;
with Javax.Imageio.IIOParam;

package Javax.Imageio.ImageWriteParam is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Imageio.IIOParam.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      CanWriteTiles : Java.Boolean;
      pragma Import (Java, CanWriteTiles, "canWriteTiles");

      --  protected
      TilingMode : Java.Int;
      pragma Import (Java, TilingMode, "tilingMode");

      --  protected
      PreferredTileSizes : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, PreferredTileSizes, "preferredTileSizes");

      --  protected
      TilingSet : Java.Boolean;
      pragma Import (Java, TilingSet, "tilingSet");

      --  protected
      TileWidth : Java.Int;
      pragma Import (Java, TileWidth, "tileWidth");

      --  protected
      TileHeight : Java.Int;
      pragma Import (Java, TileHeight, "tileHeight");

      --  protected
      CanOffsetTiles : Java.Boolean;
      pragma Import (Java, CanOffsetTiles, "canOffsetTiles");

      --  protected
      TileGridXOffset : Java.Int;
      pragma Import (Java, TileGridXOffset, "tileGridXOffset");

      --  protected
      TileGridYOffset : Java.Int;
      pragma Import (Java, TileGridYOffset, "tileGridYOffset");

      --  protected
      CanWriteProgressive : Java.Boolean;
      pragma Import (Java, CanWriteProgressive, "canWriteProgressive");

      --  protected
      ProgressiveMode : Java.Int;
      pragma Import (Java, ProgressiveMode, "progressiveMode");

      --  protected
      CanWriteCompressed : Java.Boolean;
      pragma Import (Java, CanWriteCompressed, "canWriteCompressed");

      --  protected
      CompressionMode : Java.Int;
      pragma Import (Java, CompressionMode, "compressionMode");

      --  protected
      CompressionTypes : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, CompressionTypes, "compressionTypes");

      --  protected
      CompressionType : access Java.Lang.String.Typ'Class;
      pragma Import (Java, CompressionType, "compressionType");

      --  protected
      CompressionQuality : Java.Float;
      pragma Import (Java, CompressionQuality, "compressionQuality");

      --  protected
      Locale : access Java.Util.Locale.Typ'Class;
      pragma Import (Java, Locale, "locale");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ImageWriteParam (This : Ref := null)
                                 return Ref;

   function New_ImageWriteParam (P1_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   function CanWriteTiles (This : access Typ)
                           return Java.Boolean;

   function CanOffsetTiles (This : access Typ)
                            return Java.Boolean;

   procedure SetTilingMode (This : access Typ;
                            P1_Int : Java.Int);

   function GetTilingMode (This : access Typ)
                           return Java.Int;

   function GetPreferredTileSizes (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   procedure SetTiling (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure UnsetTiling (This : access Typ);

   function GetTileWidth (This : access Typ)
                          return Java.Int;

   function GetTileHeight (This : access Typ)
                           return Java.Int;

   function GetTileGridXOffset (This : access Typ)
                                return Java.Int;

   function GetTileGridYOffset (This : access Typ)
                                return Java.Int;

   function CanWriteProgressive (This : access Typ)
                                 return Java.Boolean;

   procedure SetProgressiveMode (This : access Typ;
                                 P1_Int : Java.Int);

   function GetProgressiveMode (This : access Typ)
                                return Java.Int;

   function CanWriteCompressed (This : access Typ)
                                return Java.Boolean;

   procedure SetCompressionMode (This : access Typ;
                                 P1_Int : Java.Int);

   function GetCompressionMode (This : access Typ)
                                return Java.Int;

   function GetCompressionTypes (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   procedure SetCompressionType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetCompressionType (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure UnsetCompression (This : access Typ);

   function GetLocalizedCompressionTypeName (This : access Typ)
                                             return access Java.Lang.String.Typ'Class;

   function IsCompressionLossless (This : access Typ)
                                   return Java.Boolean;

   procedure SetCompressionQuality (This : access Typ;
                                    P1_Float : Java.Float);

   function GetCompressionQuality (This : access Typ)
                                   return Java.Float;

   function GetBitRate (This : access Typ;
                        P1_Float : Java.Float)
                        return Java.Float;

   function GetCompressionQualityDescriptions (This : access Typ)
                                               return Standard.Java.Lang.Object.Ref;

   function GetCompressionQualityValues (This : access Typ)
                                         return Java.Float_Arr;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MODE_DISABLED : constant Java.Int;

   --  final
   MODE_DEFAULT : constant Java.Int;

   --  final
   MODE_EXPLICIT : constant Java.Int;

   --  final
   MODE_COPY_FROM_METADATA : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageWriteParam);
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, CanWriteTiles, "canWriteTiles");
   pragma Import (Java, CanOffsetTiles, "canOffsetTiles");
   pragma Import (Java, SetTilingMode, "setTilingMode");
   pragma Import (Java, GetTilingMode, "getTilingMode");
   pragma Import (Java, GetPreferredTileSizes, "getPreferredTileSizes");
   pragma Import (Java, SetTiling, "setTiling");
   pragma Import (Java, UnsetTiling, "unsetTiling");
   pragma Import (Java, GetTileWidth, "getTileWidth");
   pragma Import (Java, GetTileHeight, "getTileHeight");
   pragma Import (Java, GetTileGridXOffset, "getTileGridXOffset");
   pragma Import (Java, GetTileGridYOffset, "getTileGridYOffset");
   pragma Import (Java, CanWriteProgressive, "canWriteProgressive");
   pragma Import (Java, SetProgressiveMode, "setProgressiveMode");
   pragma Import (Java, GetProgressiveMode, "getProgressiveMode");
   pragma Import (Java, CanWriteCompressed, "canWriteCompressed");
   pragma Import (Java, SetCompressionMode, "setCompressionMode");
   pragma Import (Java, GetCompressionMode, "getCompressionMode");
   pragma Import (Java, GetCompressionTypes, "getCompressionTypes");
   pragma Import (Java, SetCompressionType, "setCompressionType");
   pragma Import (Java, GetCompressionType, "getCompressionType");
   pragma Import (Java, UnsetCompression, "unsetCompression");
   pragma Import (Java, GetLocalizedCompressionTypeName, "getLocalizedCompressionTypeName");
   pragma Import (Java, IsCompressionLossless, "isCompressionLossless");
   pragma Import (Java, SetCompressionQuality, "setCompressionQuality");
   pragma Import (Java, GetCompressionQuality, "getCompressionQuality");
   pragma Import (Java, GetBitRate, "getBitRate");
   pragma Import (Java, GetCompressionQualityDescriptions, "getCompressionQualityDescriptions");
   pragma Import (Java, GetCompressionQualityValues, "getCompressionQualityValues");
   pragma Import (Java, MODE_DISABLED, "MODE_DISABLED");
   pragma Import (Java, MODE_DEFAULT, "MODE_DEFAULT");
   pragma Import (Java, MODE_EXPLICIT, "MODE_EXPLICIT");
   pragma Import (Java, MODE_COPY_FROM_METADATA, "MODE_COPY_FROM_METADATA");

end Javax.Imageio.ImageWriteParam;
pragma Import (Java, Javax.Imageio.ImageWriteParam, "javax.imageio.ImageWriteParam");
pragma Extensions_Allowed (Off);
