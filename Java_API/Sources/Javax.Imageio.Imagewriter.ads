pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Image.Raster;
limited with Java.Awt.Image.RenderedImage;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Locale;
limited with Javax.Imageio.Event.IIOWriteProgressListener;
limited with Javax.Imageio.Event.IIOWriteWarningListener;
limited with Javax.Imageio.IIOImage;
limited with Javax.Imageio.ImageTypeSpecifier;
limited with Javax.Imageio.ImageWriteParam;
limited with Javax.Imageio.Metadata.IIOMetadata;
limited with Javax.Imageio.Spi.ImageWriterSpi;
with Java.Lang.Object;
with Javax.Imageio.ImageTranscoder;

package Javax.Imageio.ImageWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageTranscoder_I : Javax.Imageio.ImageTranscoder.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      OriginatingProvider : access Javax.Imageio.Spi.ImageWriterSpi.Typ'Class;
      pragma Import (Java, OriginatingProvider, "originatingProvider");

      --  protected
      Output : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Output, "output");

      --  protected
      AvailableLocales : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, AvailableLocales, "availableLocales");

      --  protected
      Locale : access Java.Util.Locale.Typ'Class;
      pragma Import (Java, Locale, "locale");

      --  protected
      WarningListeners : access Java.Util.List.Typ'Class;
      pragma Import (Java, WarningListeners, "warningListeners");

      --  protected
      WarningLocales : access Java.Util.List.Typ'Class;
      pragma Import (Java, WarningLocales, "warningLocales");

      --  protected
      ProgressListeners : access Java.Util.List.Typ'Class;
      pragma Import (Java, ProgressListeners, "progressListeners");

   end record;

   --  protected
   function New_ImageWriter (P1_ImageWriterSpi : access Standard.Javax.Imageio.Spi.ImageWriterSpi.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOriginatingProvider (This : access Typ)
                                    return access Javax.Imageio.Spi.ImageWriterSpi.Typ'Class;

   procedure SetOutput (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetOutput (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function GetAvailableLocales (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   function GetDefaultWriteParam (This : access Typ)
                                  return access Javax.Imageio.ImageWriteParam.Typ'Class;

   function GetDefaultStreamMetadata (This : access Typ;
                                      P1_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class)
                                      return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract;

   function GetDefaultImageMetadata (This : access Typ;
                                     P1_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                                     P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class)
                                     return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract;

   function ConvertStreamMetadata (This : access Typ;
                                   P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                   P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class)
                                   return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract;

   function ConvertImageMetadata (This : access Typ;
                                  P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                  P2_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                                  P3_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class)
                                  return access Javax.Imageio.Metadata.IIOMetadata.Typ'Class is abstract;

   function GetNumThumbnailsSupported (This : access Typ;
                                       P1_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                                       P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class;
                                       P3_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                       P4_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class)
                                       return Java.Int;

   function GetPreferredThumbnailSizes (This : access Typ;
                                        P1_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                                        P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class;
                                        P3_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                        P4_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class)
                                        return Standard.Java.Lang.Object.Ref;

   function CanWriteRasters (This : access Typ)
                             return Java.Boolean;

   procedure Write (This : access Typ;
                    P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                    P2_IIOImage : access Standard.Javax.Imageio.IIOImage.Typ'Class;
                    P3_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class) is abstract
                    with Export => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_IIOImage : access Standard.Javax.Imageio.IIOImage.Typ'Class)
                    with Import => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class)
                    with Import => "write", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function CanWriteSequence (This : access Typ)
                              return Java.Boolean;

   procedure PrepareWriteSequence (This : access Typ;
                                   P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure WriteToSequence (This : access Typ;
                              P1_IIOImage : access Standard.Javax.Imageio.IIOImage.Typ'Class;
                              P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure EndWriteSequence (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function CanReplaceStreamMetadata (This : access Typ)
                                      return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure ReplaceStreamMetadata (This : access Typ;
                                    P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function CanReplaceImageMetadata (This : access Typ;
                                     P1_Int : Java.Int)
                                     return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure ReplaceImageMetadata (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function CanInsertImage (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure WriteInsert (This : access Typ;
                          P1_Int : Java.Int;
                          P2_IIOImage : access Standard.Javax.Imageio.IIOImage.Typ'Class;
                          P3_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function CanRemoveImage (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure RemoveImage (This : access Typ;
                          P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function CanWriteEmpty (This : access Typ)
                           return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure PrepareWriteEmpty (This : access Typ;
                                P1_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                P2_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                P6_List : access Standard.Java.Util.List.Typ'Class;
                                P7_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure EndWriteEmpty (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function CanInsertEmpty (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure PrepareInsertEmpty (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_IIOMetadata : access Standard.Javax.Imageio.Metadata.IIOMetadata.Typ'Class;
                                 P6_List : access Standard.Java.Util.List.Typ'Class;
                                 P7_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure EndInsertEmpty (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function CanReplacePixels (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   procedure PrepareReplacePixels (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure ReplacePixels (This : access Typ;
                            P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class;
                            P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure ReplacePixels (This : access Typ;
                            P1_Raster : access Standard.Java.Awt.Image.Raster.Typ'Class;
                            P2_ImageWriteParam : access Standard.Javax.Imageio.ImageWriteParam.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure EndReplacePixels (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure abort_K (This : access Typ);

   --  protected  synchronized
   function AbortRequested (This : access Typ)
                            return Java.Boolean;

   --  protected  synchronized
   procedure ClearAbortRequest (This : access Typ);

   procedure AddIIOWriteWarningListener (This : access Typ;
                                         P1_IIOWriteWarningListener : access Standard.Javax.Imageio.Event.IIOWriteWarningListener.Typ'Class);

   procedure RemoveIIOWriteWarningListener (This : access Typ;
                                            P1_IIOWriteWarningListener : access Standard.Javax.Imageio.Event.IIOWriteWarningListener.Typ'Class);

   procedure RemoveAllIIOWriteWarningListeners (This : access Typ);

   procedure AddIIOWriteProgressListener (This : access Typ;
                                          P1_IIOWriteProgressListener : access Standard.Javax.Imageio.Event.IIOWriteProgressListener.Typ'Class);

   procedure RemoveIIOWriteProgressListener (This : access Typ;
                                             P1_IIOWriteProgressListener : access Standard.Javax.Imageio.Event.IIOWriteProgressListener.Typ'Class);

   procedure RemoveAllIIOWriteProgressListeners (This : access Typ);

   --  protected
   procedure ProcessImageStarted (This : access Typ;
                                  P1_Int : Java.Int);

   --  protected
   procedure ProcessImageProgress (This : access Typ;
                                   P1_Float : Java.Float);

   --  protected
   procedure ProcessImageComplete (This : access Typ);

   --  protected
   procedure ProcessThumbnailStarted (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int);

   --  protected
   procedure ProcessThumbnailProgress (This : access Typ;
                                       P1_Float : Java.Float);

   --  protected
   procedure ProcessThumbnailComplete (This : access Typ);

   --  protected
   procedure ProcessWriteAborted (This : access Typ);

   --  protected
   procedure ProcessWarningOccurred (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure ProcessWarningOccurred (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Reset (This : access Typ);

   procedure Dispose (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageWriter);
   pragma Import (Java, GetOriginatingProvider, "getOriginatingProvider");
   pragma Import (Java, SetOutput, "setOutput");
   pragma Import (Java, GetOutput, "getOutput");
   pragma Import (Java, GetAvailableLocales, "getAvailableLocales");
   pragma Import (Java, SetLocale, "setLocale");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, GetDefaultWriteParam, "getDefaultWriteParam");
   pragma Export (Java, GetDefaultStreamMetadata, "getDefaultStreamMetadata");
   pragma Export (Java, GetDefaultImageMetadata, "getDefaultImageMetadata");
   pragma Export (Java, ConvertStreamMetadata, "convertStreamMetadata");
   pragma Export (Java, ConvertImageMetadata, "convertImageMetadata");
   pragma Import (Java, GetNumThumbnailsSupported, "getNumThumbnailsSupported");
   pragma Import (Java, GetPreferredThumbnailSizes, "getPreferredThumbnailSizes");
   pragma Import (Java, CanWriteRasters, "canWriteRasters");
   -- pragma Import (Java, Write, "write");
   pragma Import (Java, CanWriteSequence, "canWriteSequence");
   pragma Import (Java, PrepareWriteSequence, "prepareWriteSequence");
   pragma Import (Java, WriteToSequence, "writeToSequence");
   pragma Import (Java, EndWriteSequence, "endWriteSequence");
   pragma Import (Java, CanReplaceStreamMetadata, "canReplaceStreamMetadata");
   pragma Import (Java, ReplaceStreamMetadata, "replaceStreamMetadata");
   pragma Import (Java, CanReplaceImageMetadata, "canReplaceImageMetadata");
   pragma Import (Java, ReplaceImageMetadata, "replaceImageMetadata");
   pragma Import (Java, CanInsertImage, "canInsertImage");
   pragma Import (Java, WriteInsert, "writeInsert");
   pragma Import (Java, CanRemoveImage, "canRemoveImage");
   pragma Import (Java, RemoveImage, "removeImage");
   pragma Import (Java, CanWriteEmpty, "canWriteEmpty");
   pragma Import (Java, PrepareWriteEmpty, "prepareWriteEmpty");
   pragma Import (Java, EndWriteEmpty, "endWriteEmpty");
   pragma Import (Java, CanInsertEmpty, "canInsertEmpty");
   pragma Import (Java, PrepareInsertEmpty, "prepareInsertEmpty");
   pragma Import (Java, EndInsertEmpty, "endInsertEmpty");
   pragma Import (Java, CanReplacePixels, "canReplacePixels");
   pragma Import (Java, PrepareReplacePixels, "prepareReplacePixels");
   pragma Import (Java, ReplacePixels, "replacePixels");
   pragma Import (Java, EndReplacePixels, "endReplacePixels");
   pragma Import (Java, abort_K, "abort");
   pragma Import (Java, AbortRequested, "abortRequested");
   pragma Import (Java, ClearAbortRequest, "clearAbortRequest");
   pragma Import (Java, AddIIOWriteWarningListener, "addIIOWriteWarningListener");
   pragma Import (Java, RemoveIIOWriteWarningListener, "removeIIOWriteWarningListener");
   pragma Import (Java, RemoveAllIIOWriteWarningListeners, "removeAllIIOWriteWarningListeners");
   pragma Import (Java, AddIIOWriteProgressListener, "addIIOWriteProgressListener");
   pragma Import (Java, RemoveIIOWriteProgressListener, "removeIIOWriteProgressListener");
   pragma Import (Java, RemoveAllIIOWriteProgressListeners, "removeAllIIOWriteProgressListeners");
   pragma Import (Java, ProcessImageStarted, "processImageStarted");
   pragma Import (Java, ProcessImageProgress, "processImageProgress");
   pragma Import (Java, ProcessImageComplete, "processImageComplete");
   pragma Import (Java, ProcessThumbnailStarted, "processThumbnailStarted");
   pragma Import (Java, ProcessThumbnailProgress, "processThumbnailProgress");
   pragma Import (Java, ProcessThumbnailComplete, "processThumbnailComplete");
   pragma Import (Java, ProcessWriteAborted, "processWriteAborted");
   pragma Import (Java, ProcessWarningOccurred, "processWarningOccurred");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, Dispose, "dispose");

end Javax.Imageio.ImageWriter;
pragma Import (Java, Javax.Imageio.ImageWriter, "javax.imageio.ImageWriter");
pragma Extensions_Allowed (Off);
