pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Org.W3c.Dom.Node;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Imageio.IIOException;

package Javax.Imageio.Metadata.IIOInvalidTreeException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Imageio.IIOException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      OffendingNode : access Org.W3c.Dom.Node.Typ'Class;
      pragma Import (Java, OffendingNode, "offendingNode");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IIOInvalidTreeException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_IIOInvalidTreeException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class;
                                         P3_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOffendingNode (This : access Typ)
                              return access Org.W3c.Dom.Node.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.imageio.metadata.IIOInvalidTreeException");
   pragma Java_Constructor (New_IIOInvalidTreeException);
   pragma Import (Java, GetOffendingNode, "getOffendingNode");

end Javax.Imageio.Metadata.IIOInvalidTreeException;
pragma Import (Java, Javax.Imageio.Metadata.IIOInvalidTreeException, "javax.imageio.metadata.IIOInvalidTreeException");
pragma Extensions_Allowed (Off);
