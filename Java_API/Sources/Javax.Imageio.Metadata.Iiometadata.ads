pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Imageio.Metadata.IIOMetadataController;
limited with Javax.Imageio.Metadata.IIOMetadataFormat;
limited with Javax.Imageio.Metadata.IIOMetadataNode;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Javax.Imageio.Metadata.IIOMetadata is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      StandardFormatSupported : Java.Boolean;
      pragma Import (Java, StandardFormatSupported, "standardFormatSupported");

      --  protected
      NativeMetadataFormatName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, NativeMetadataFormatName, "nativeMetadataFormatName");

      --  protected
      NativeMetadataFormatClassName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, NativeMetadataFormatClassName, "nativeMetadataFormatClassName");

      --  protected
      ExtraMetadataFormatNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ExtraMetadataFormatNames, "extraMetadataFormatNames");

      --  protected
      ExtraMetadataFormatClassNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ExtraMetadataFormatClassNames, "extraMetadataFormatClassNames");

      --  protected
      DefaultController : access Javax.Imageio.Metadata.IIOMetadataController.Typ'Class;
      pragma Import (Java, DefaultController, "defaultController");

      --  protected
      Controller : access Javax.Imageio.Metadata.IIOMetadataController.Typ'Class;
      pragma Import (Java, Controller, "controller");

   end record;

   --  protected
   function New_IIOMetadata (This : Ref := null)
                             return Ref;

   --  protected
   function New_IIOMetadata (P1_Boolean : Java.Boolean;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_String_Arr : access Java.Lang.String.Arr_Obj;
                             P5_String_Arr : access Java.Lang.String.Arr_Obj; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsStandardMetadataFormatSupported (This : access Typ)
                                               return Java.Boolean;

   function IsReadOnly (This : access Typ)
                        return Java.Boolean is abstract;

   function GetNativeMetadataFormatName (This : access Typ)
                                         return access Java.Lang.String.Typ'Class;

   function GetExtraMetadataFormatNames (This : access Typ)
                                         return Standard.Java.Lang.Object.Ref;

   function GetMetadataFormatNames (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   function GetMetadataFormat (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Javax.Imageio.Metadata.IIOMetadataFormat.Typ'Class;

   function GetAsTree (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Org.W3c.Dom.Node.Typ'Class is abstract;

   procedure MergeTree (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Javax.Imageio.Metadata.IIOInvalidTreeException.Except

   --  protected
   function GetStandardChromaNode (This : access Typ)
                                   return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  protected
   function GetStandardCompressionNode (This : access Typ)
                                        return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  protected
   function GetStandardDataNode (This : access Typ)
                                 return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  protected
   function GetStandardDimensionNode (This : access Typ)
                                      return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  protected
   function GetStandardDocumentNode (This : access Typ)
                                     return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  protected
   function GetStandardTextNode (This : access Typ)
                                 return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  protected
   function GetStandardTileNode (This : access Typ)
                                 return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  protected
   function GetStandardTransparencyNode (This : access Typ)
                                         return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   --  final  protected
   function GetStandardTree (This : access Typ)
                             return access Javax.Imageio.Metadata.IIOMetadataNode.Typ'Class;

   procedure SetFromTree (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);
   --  can raise Javax.Imageio.Metadata.IIOInvalidTreeException.Except

   procedure Reset (This : access Typ) is abstract;

   procedure SetController (This : access Typ;
                            P1_IIOMetadataController : access Standard.Javax.Imageio.Metadata.IIOMetadataController.Typ'Class);

   function GetController (This : access Typ)
                           return access Javax.Imageio.Metadata.IIOMetadataController.Typ'Class;

   function GetDefaultController (This : access Typ)
                                  return access Javax.Imageio.Metadata.IIOMetadataController.Typ'Class;

   function HasController (This : access Typ)
                           return Java.Boolean;

   function ActivateController (This : access Typ)
                                return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IIOMetadata);
   pragma Import (Java, IsStandardMetadataFormatSupported, "isStandardMetadataFormatSupported");
   pragma Export (Java, IsReadOnly, "isReadOnly");
   pragma Import (Java, GetNativeMetadataFormatName, "getNativeMetadataFormatName");
   pragma Import (Java, GetExtraMetadataFormatNames, "getExtraMetadataFormatNames");
   pragma Import (Java, GetMetadataFormatNames, "getMetadataFormatNames");
   pragma Import (Java, GetMetadataFormat, "getMetadataFormat");
   pragma Export (Java, GetAsTree, "getAsTree");
   pragma Export (Java, MergeTree, "mergeTree");
   pragma Import (Java, GetStandardChromaNode, "getStandardChromaNode");
   pragma Import (Java, GetStandardCompressionNode, "getStandardCompressionNode");
   pragma Import (Java, GetStandardDataNode, "getStandardDataNode");
   pragma Import (Java, GetStandardDimensionNode, "getStandardDimensionNode");
   pragma Import (Java, GetStandardDocumentNode, "getStandardDocumentNode");
   pragma Import (Java, GetStandardTextNode, "getStandardTextNode");
   pragma Import (Java, GetStandardTileNode, "getStandardTileNode");
   pragma Import (Java, GetStandardTransparencyNode, "getStandardTransparencyNode");
   pragma Import (Java, GetStandardTree, "getStandardTree");
   pragma Import (Java, SetFromTree, "setFromTree");
   pragma Export (Java, Reset, "reset");
   pragma Import (Java, SetController, "setController");
   pragma Import (Java, GetController, "getController");
   pragma Import (Java, GetDefaultController, "getDefaultController");
   pragma Import (Java, HasController, "hasController");
   pragma Import (Java, ActivateController, "activateController");

end Javax.Imageio.Metadata.IIOMetadata;
pragma Import (Java, Javax.Imageio.Metadata.IIOMetadata, "javax.imageio.metadata.IIOMetadata");
pragma Extensions_Allowed (Off);
