pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.Comparable;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Imageio.ImageTypeSpecifier;
with Java.Lang.Object;

package Javax.Imageio.Metadata.IIOMetadataFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRootName (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function CanNodeAppear (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class)
                           return Java.Boolean is abstract;

   function GetElementMinChildren (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Java.Int is abstract;

   function GetElementMaxChildren (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Java.Int is abstract;

   function GetElementDescription (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                   return access Java.Lang.String.Typ'Class is abstract;

   function GetChildPolicy (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Int is abstract;

   function GetChildNames (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function GetAttributeNames (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function GetAttributeValueType (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Java.Int is abstract;

   function GetAttributeDataType (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return Java.Int is abstract;

   function IsAttributeRequired (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                                 return Java.Boolean is abstract;

   function GetAttributeDefaultValue (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeEnumerations (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                                      return Standard.Java.Lang.Object.Ref is abstract;

   function GetAttributeMinValue (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeMaxValue (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeListMinLength (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                                       return Java.Int is abstract;

   function GetAttributeListMaxLength (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                                       return Java.Int is abstract;

   function GetAttributeDescription (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                     return access Java.Lang.String.Typ'Class is abstract;

   function GetObjectValueType (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int is abstract;

   function GetObjectClass (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.Class.Typ'Class is abstract;

   function GetObjectDefaultValue (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class is abstract;

   function GetObjectEnumerations (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Standard.Java.Lang.Object.Ref is abstract;

   function GetObjectMinValue (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.Comparable.Typ'Class is abstract;

   function GetObjectMaxValue (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.Comparable.Typ'Class is abstract;

   function GetObjectArrayMinLength (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return Java.Int is abstract;

   function GetObjectArrayMaxLength (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CHILD_POLICY_EMPTY : constant Java.Int;

   --  final
   CHILD_POLICY_ALL : constant Java.Int;

   --  final
   CHILD_POLICY_SOME : constant Java.Int;

   --  final
   CHILD_POLICY_CHOICE : constant Java.Int;

   --  final
   CHILD_POLICY_SEQUENCE : constant Java.Int;

   --  final
   CHILD_POLICY_REPEAT : constant Java.Int;

   --  final
   CHILD_POLICY_MAX : constant Java.Int;

   --  final
   VALUE_NONE : constant Java.Int;

   --  final
   VALUE_ARBITRARY : constant Java.Int;

   --  final
   VALUE_RANGE : constant Java.Int;

   --  final
   VALUE_RANGE_MIN_INCLUSIVE_MASK : constant Java.Int;

   --  final
   VALUE_RANGE_MAX_INCLUSIVE_MASK : constant Java.Int;

   --  final
   VALUE_RANGE_MIN_INCLUSIVE : constant Java.Int;

   --  final
   VALUE_RANGE_MAX_INCLUSIVE : constant Java.Int;

   --  final
   VALUE_RANGE_MIN_MAX_INCLUSIVE : constant Java.Int;

   --  final
   VALUE_ENUMERATION : constant Java.Int;

   --  final
   VALUE_LIST : constant Java.Int;

   --  final
   DATATYPE_STRING : constant Java.Int;

   --  final
   DATATYPE_BOOLEAN : constant Java.Int;

   --  final
   DATATYPE_INTEGER : constant Java.Int;

   --  final
   DATATYPE_FLOAT : constant Java.Int;

   --  final
   DATATYPE_DOUBLE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRootName, "getRootName");
   pragma Export (Java, CanNodeAppear, "canNodeAppear");
   pragma Export (Java, GetElementMinChildren, "getElementMinChildren");
   pragma Export (Java, GetElementMaxChildren, "getElementMaxChildren");
   pragma Export (Java, GetElementDescription, "getElementDescription");
   pragma Export (Java, GetChildPolicy, "getChildPolicy");
   pragma Export (Java, GetChildNames, "getChildNames");
   pragma Export (Java, GetAttributeNames, "getAttributeNames");
   pragma Export (Java, GetAttributeValueType, "getAttributeValueType");
   pragma Export (Java, GetAttributeDataType, "getAttributeDataType");
   pragma Export (Java, IsAttributeRequired, "isAttributeRequired");
   pragma Export (Java, GetAttributeDefaultValue, "getAttributeDefaultValue");
   pragma Export (Java, GetAttributeEnumerations, "getAttributeEnumerations");
   pragma Export (Java, GetAttributeMinValue, "getAttributeMinValue");
   pragma Export (Java, GetAttributeMaxValue, "getAttributeMaxValue");
   pragma Export (Java, GetAttributeListMinLength, "getAttributeListMinLength");
   pragma Export (Java, GetAttributeListMaxLength, "getAttributeListMaxLength");
   pragma Export (Java, GetAttributeDescription, "getAttributeDescription");
   pragma Export (Java, GetObjectValueType, "getObjectValueType");
   pragma Export (Java, GetObjectClass, "getObjectClass");
   pragma Export (Java, GetObjectDefaultValue, "getObjectDefaultValue");
   pragma Export (Java, GetObjectEnumerations, "getObjectEnumerations");
   pragma Export (Java, GetObjectMinValue, "getObjectMinValue");
   pragma Export (Java, GetObjectMaxValue, "getObjectMaxValue");
   pragma Export (Java, GetObjectArrayMinLength, "getObjectArrayMinLength");
   pragma Export (Java, GetObjectArrayMaxLength, "getObjectArrayMaxLength");
   pragma Import (Java, CHILD_POLICY_EMPTY, "CHILD_POLICY_EMPTY");
   pragma Import (Java, CHILD_POLICY_ALL, "CHILD_POLICY_ALL");
   pragma Import (Java, CHILD_POLICY_SOME, "CHILD_POLICY_SOME");
   pragma Import (Java, CHILD_POLICY_CHOICE, "CHILD_POLICY_CHOICE");
   pragma Import (Java, CHILD_POLICY_SEQUENCE, "CHILD_POLICY_SEQUENCE");
   pragma Import (Java, CHILD_POLICY_REPEAT, "CHILD_POLICY_REPEAT");
   pragma Import (Java, CHILD_POLICY_MAX, "CHILD_POLICY_MAX");
   pragma Import (Java, VALUE_NONE, "VALUE_NONE");
   pragma Import (Java, VALUE_ARBITRARY, "VALUE_ARBITRARY");
   pragma Import (Java, VALUE_RANGE, "VALUE_RANGE");
   pragma Import (Java, VALUE_RANGE_MIN_INCLUSIVE_MASK, "VALUE_RANGE_MIN_INCLUSIVE_MASK");
   pragma Import (Java, VALUE_RANGE_MAX_INCLUSIVE_MASK, "VALUE_RANGE_MAX_INCLUSIVE_MASK");
   pragma Import (Java, VALUE_RANGE_MIN_INCLUSIVE, "VALUE_RANGE_MIN_INCLUSIVE");
   pragma Import (Java, VALUE_RANGE_MAX_INCLUSIVE, "VALUE_RANGE_MAX_INCLUSIVE");
   pragma Import (Java, VALUE_RANGE_MIN_MAX_INCLUSIVE, "VALUE_RANGE_MIN_MAX_INCLUSIVE");
   pragma Import (Java, VALUE_ENUMERATION, "VALUE_ENUMERATION");
   pragma Import (Java, VALUE_LIST, "VALUE_LIST");
   pragma Import (Java, DATATYPE_STRING, "DATATYPE_STRING");
   pragma Import (Java, DATATYPE_BOOLEAN, "DATATYPE_BOOLEAN");
   pragma Import (Java, DATATYPE_INTEGER, "DATATYPE_INTEGER");
   pragma Import (Java, DATATYPE_FLOAT, "DATATYPE_FLOAT");
   pragma Import (Java, DATATYPE_DOUBLE, "DATATYPE_DOUBLE");

end Javax.Imageio.Metadata.IIOMetadataFormat;
pragma Import (Java, Javax.Imageio.Metadata.IIOMetadataFormat, "javax.imageio.metadata.IIOMetadataFormat");
pragma Extensions_Allowed (Off);
