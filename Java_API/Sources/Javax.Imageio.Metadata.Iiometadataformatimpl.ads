pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.Comparable;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Locale;
limited with Javax.Imageio.ImageTypeSpecifier;
with Java.Lang.Object;
with Javax.Imageio.Metadata.IIOMetadataFormat;

package Javax.Imageio.Metadata.IIOMetadataFormatImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IIOMetadataFormat_I : Javax.Imageio.Metadata.IIOMetadataFormat.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_IIOMetadataFormatImpl (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_Int : Java.Int; 
                                       This : Ref := null)
                                       return Ref;

   function New_IIOMetadataFormatImpl (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure SetResourceBaseName (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function GetResourceBaseName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   --  protected
   procedure AddElement (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_Int : Java.Int);

   --  protected
   procedure AddElement (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int);

   --  protected
   procedure AddChildElement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure RemoveElement (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure AddAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Boolean : Java.Boolean;
                           P5_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure AddAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Boolean : Java.Boolean;
                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                           P6_List : access Standard.Java.Util.List.Typ'Class);

   --  protected
   procedure AddAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Boolean : Java.Boolean;
                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                           P6_String : access Standard.Java.Lang.String.Typ'Class;
                           P7_String : access Standard.Java.Lang.String.Typ'Class;
                           P8_Boolean : Java.Boolean;
                           P9_Boolean : Java.Boolean);

   --  protected
   procedure AddAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Boolean : Java.Boolean;
                           P5_Int : Java.Int;
                           P6_Int : Java.Int);

   --  protected
   procedure AddBooleanAttribute (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_Boolean : Java.Boolean;
                                  P4_Boolean : Java.Boolean);

   --  protected
   procedure RemoveAttribute (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure AddObjectValue (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P3_Boolean : Java.Boolean;
                             P4_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   procedure AddObjectValue (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P3_Boolean : Java.Boolean;
                             P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P5_List : access Standard.Java.Util.List.Typ'Class);

   --  protected
   procedure AddObjectValue (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Comparable : access Standard.Java.Lang.Comparable.Typ'Class;
                             P5_Comparable : access Standard.Java.Lang.Comparable.Typ'Class;
                             P6_Boolean : Java.Boolean;
                             P7_Boolean : Java.Boolean);

   --  protected
   procedure AddObjectValue (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int);

   --  protected
   procedure RemoveObjectValue (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetRootName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function CanNodeAppear (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class)
                           return Java.Boolean is abstract;

   function GetElementMinChildren (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Java.Int;

   function GetElementMaxChildren (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Java.Int;

   function GetElementDescription (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                   return access Java.Lang.String.Typ'Class;

   function GetChildPolicy (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Int;

   function GetChildNames (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return Standard.Java.Lang.Object.Ref;

   function GetAttributeNames (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return Standard.Java.Lang.Object.Ref;

   function GetAttributeValueType (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Java.Int;

   function GetAttributeDataType (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return Java.Int;

   function IsAttributeRequired (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                                 return Java.Boolean;

   function GetAttributeDefaultValue (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Java.Lang.String.Typ'Class;

   function GetAttributeEnumerations (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                                      return Standard.Java.Lang.Object.Ref;

   function GetAttributeMinValue (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Java.Lang.String.Typ'Class;

   function GetAttributeMaxValue (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Java.Lang.String.Typ'Class;

   function GetAttributeListMinLength (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                                       return Java.Int;

   function GetAttributeListMaxLength (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                                       return Java.Int;

   function GetAttributeDescription (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                     return access Java.Lang.String.Typ'Class;

   function GetObjectValueType (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int;

   function GetObjectClass (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.Class.Typ'Class;

   function GetObjectDefaultValue (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class;

   function GetObjectEnumerations (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return Standard.Java.Lang.Object.Ref;

   function GetObjectMinValue (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.Comparable.Typ'Class;

   function GetObjectMaxValue (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.Comparable.Typ'Class;

   function GetObjectArrayMinLength (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return Java.Int;

   function GetObjectArrayMaxLength (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return Java.Int;

   function GetStandardFormatInstance return access Javax.Imageio.Metadata.IIOMetadataFormat.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   StandardMetadataFormatName : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IIOMetadataFormatImpl);
   pragma Import (Java, SetResourceBaseName, "setResourceBaseName");
   pragma Import (Java, GetResourceBaseName, "getResourceBaseName");
   pragma Import (Java, AddElement, "addElement");
   pragma Import (Java, AddChildElement, "addChildElement");
   pragma Import (Java, RemoveElement, "removeElement");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, AddBooleanAttribute, "addBooleanAttribute");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, AddObjectValue, "addObjectValue");
   pragma Import (Java, RemoveObjectValue, "removeObjectValue");
   pragma Import (Java, GetRootName, "getRootName");
   pragma Export (Java, CanNodeAppear, "canNodeAppear");
   pragma Import (Java, GetElementMinChildren, "getElementMinChildren");
   pragma Import (Java, GetElementMaxChildren, "getElementMaxChildren");
   pragma Import (Java, GetElementDescription, "getElementDescription");
   pragma Import (Java, GetChildPolicy, "getChildPolicy");
   pragma Import (Java, GetChildNames, "getChildNames");
   pragma Import (Java, GetAttributeNames, "getAttributeNames");
   pragma Import (Java, GetAttributeValueType, "getAttributeValueType");
   pragma Import (Java, GetAttributeDataType, "getAttributeDataType");
   pragma Import (Java, IsAttributeRequired, "isAttributeRequired");
   pragma Import (Java, GetAttributeDefaultValue, "getAttributeDefaultValue");
   pragma Import (Java, GetAttributeEnumerations, "getAttributeEnumerations");
   pragma Import (Java, GetAttributeMinValue, "getAttributeMinValue");
   pragma Import (Java, GetAttributeMaxValue, "getAttributeMaxValue");
   pragma Import (Java, GetAttributeListMinLength, "getAttributeListMinLength");
   pragma Import (Java, GetAttributeListMaxLength, "getAttributeListMaxLength");
   pragma Import (Java, GetAttributeDescription, "getAttributeDescription");
   pragma Import (Java, GetObjectValueType, "getObjectValueType");
   pragma Import (Java, GetObjectClass, "getObjectClass");
   pragma Import (Java, GetObjectDefaultValue, "getObjectDefaultValue");
   pragma Import (Java, GetObjectEnumerations, "getObjectEnumerations");
   pragma Import (Java, GetObjectMinValue, "getObjectMinValue");
   pragma Import (Java, GetObjectMaxValue, "getObjectMaxValue");
   pragma Import (Java, GetObjectArrayMinLength, "getObjectArrayMinLength");
   pragma Import (Java, GetObjectArrayMaxLength, "getObjectArrayMaxLength");
   pragma Import (Java, GetStandardFormatInstance, "getStandardFormatInstance");
   pragma Import (Java, StandardMetadataFormatName, "standardMetadataFormatName");

end Javax.Imageio.Metadata.IIOMetadataFormatImpl;
pragma Import (Java, Javax.Imageio.Metadata.IIOMetadataFormatImpl, "javax.imageio.metadata.IIOMetadataFormatImpl");
pragma Extensions_Allowed (Off);
