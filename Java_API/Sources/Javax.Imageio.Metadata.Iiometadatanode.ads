pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Attr;
limited with Org.W3c.Dom.Document;
limited with Org.W3c.Dom.NamedNodeMap;
limited with Org.W3c.Dom.Node;
limited with Org.W3c.Dom.TypeInfo;
limited with Org.W3c.Dom.UserDataHandler;
with Java.Lang.Object;
with Org.W3c.Dom.Element;
with Org.W3c.Dom.NodeList;

package Javax.Imageio.Metadata.IIOMetadataNode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Element_I : Org.W3c.Dom.Element.Ref;
            NodeList_I : Org.W3c.Dom.NodeList.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IIOMetadataNode (This : Ref := null)
                                 return Ref;

   function New_IIOMetadataNode (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNodeName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetNodeValue (This : access Typ)
                          return access Java.Lang.String.Typ'Class;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetNodeValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetNodeType (This : access Typ)
                         return Java.Short;

   function GetParentNode (This : access Typ)
                           return access Org.W3c.Dom.Node.Typ'Class;

   function GetChildNodes (This : access Typ)
                           return access Org.W3c.Dom.NodeList.Typ'Class;

   function GetFirstChild (This : access Typ)
                           return access Org.W3c.Dom.Node.Typ'Class;

   function GetLastChild (This : access Typ)
                          return access Org.W3c.Dom.Node.Typ'Class;

   function GetPreviousSibling (This : access Typ)
                                return access Org.W3c.Dom.Node.Typ'Class;

   function GetNextSibling (This : access Typ)
                            return access Org.W3c.Dom.Node.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Org.W3c.Dom.NamedNodeMap.Typ'Class;

   function GetOwnerDocument (This : access Typ)
                              return access Org.W3c.Dom.Document.Typ'Class;

   function InsertBefore (This : access Typ;
                          P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                          P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                          return access Org.W3c.Dom.Node.Typ'Class;

   function ReplaceChild (This : access Typ;
                          P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                          P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                          return access Org.W3c.Dom.Node.Typ'Class;

   function RemoveChild (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                         return access Org.W3c.Dom.Node.Typ'Class;

   function AppendChild (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                         return access Org.W3c.Dom.Node.Typ'Class;

   function HasChildNodes (This : access Typ)
                           return Java.Boolean;

   function CloneNode (This : access Typ;
                       P1_Boolean : Java.Boolean)
                       return access Org.W3c.Dom.Node.Typ'Class;

   procedure Normalize (This : access Typ);

   function IsSupported (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   function GetNamespaceURI (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPrefix (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   procedure SetPrefix (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetLocalName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetTagName (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function GetAttributeNS (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   procedure SetAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetAttributeNS (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveAttribute (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveAttributeNS (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class);

   function GetAttributeNode (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.W3c.Dom.Attr.Typ'Class;

   function GetAttributeNodeNS (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Org.W3c.Dom.Attr.Typ'Class;

   function SetAttributeNode (This : access Typ;
                              P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class)
                              return access Org.W3c.Dom.Attr.Typ'Class;
   --  can raise Org.W3c.Dom.DOMException.Except

   function SetAttributeNodeNS (This : access Typ;
                                P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class)
                                return access Org.W3c.Dom.Attr.Typ'Class;

   function RemoveAttributeNode (This : access Typ;
                                 P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class)
                                 return access Org.W3c.Dom.Attr.Typ'Class;

   function GetElementsByTagName (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Org.W3c.Dom.NodeList.Typ'Class;

   function GetElementsByTagNameNS (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Org.W3c.Dom.NodeList.Typ'Class;

   function HasAttributes (This : access Typ)
                           return Java.Boolean;

   function HasAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean;

   function HasAttributeNS (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Boolean;

   function GetLength (This : access Typ)
                       return Java.Int;

   function Item (This : access Typ;
                  P1_Int : Java.Int)
                  return access Org.W3c.Dom.Node.Typ'Class;

   function GetUserObject (This : access Typ)
                           return access Java.Lang.Object.Typ'Class;

   procedure SetUserObject (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure SetIdAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Boolean : Java.Boolean);
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetIdAttributeNS (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Boolean : Java.Boolean);
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetIdAttributeNode (This : access Typ;
                                 P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class;
                                 P2_Boolean : Java.Boolean);
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSchemaTypeInfo (This : access Typ)
                               return access Org.W3c.Dom.TypeInfo.Typ'Class;

   function SetUserData (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P3_UserDataHandler : access Standard.Org.W3c.Dom.UserDataHandler.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function GetUserData (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function IsSameNode (This : access Typ;
                        P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                        return Java.Boolean;

   function IsEqualNode (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                         return Java.Boolean;

   function LookupNamespaceURI (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function IsDefaultNamespace (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean;

   function LookupPrefix (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function GetTextContent (This : access Typ)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetTextContent (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.W3c.Dom.DOMException.Except

   function CompareDocumentPosition (This : access Typ;
                                     P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                                     return Java.Short;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBaseURI (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IIOMetadataNode);
   pragma Import (Java, GetNodeName, "getNodeName");
   pragma Import (Java, GetNodeValue, "getNodeValue");
   pragma Import (Java, SetNodeValue, "setNodeValue");
   pragma Import (Java, GetNodeType, "getNodeType");
   pragma Import (Java, GetParentNode, "getParentNode");
   pragma Import (Java, GetChildNodes, "getChildNodes");
   pragma Import (Java, GetFirstChild, "getFirstChild");
   pragma Import (Java, GetLastChild, "getLastChild");
   pragma Import (Java, GetPreviousSibling, "getPreviousSibling");
   pragma Import (Java, GetNextSibling, "getNextSibling");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetOwnerDocument, "getOwnerDocument");
   pragma Import (Java, InsertBefore, "insertBefore");
   pragma Import (Java, ReplaceChild, "replaceChild");
   pragma Import (Java, RemoveChild, "removeChild");
   pragma Import (Java, AppendChild, "appendChild");
   pragma Import (Java, HasChildNodes, "hasChildNodes");
   pragma Import (Java, CloneNode, "cloneNode");
   pragma Import (Java, Normalize, "normalize");
   pragma Import (Java, IsSupported, "isSupported");
   pragma Import (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Import (Java, GetPrefix, "getPrefix");
   pragma Import (Java, SetPrefix, "setPrefix");
   pragma Import (Java, GetLocalName, "getLocalName");
   pragma Import (Java, GetTagName, "getTagName");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetAttributeNS, "getAttributeNS");
   pragma Import (Java, SetAttribute, "setAttribute");
   pragma Import (Java, SetAttributeNS, "setAttributeNS");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, RemoveAttributeNS, "removeAttributeNS");
   pragma Import (Java, GetAttributeNode, "getAttributeNode");
   pragma Import (Java, GetAttributeNodeNS, "getAttributeNodeNS");
   pragma Import (Java, SetAttributeNode, "setAttributeNode");
   pragma Import (Java, SetAttributeNodeNS, "setAttributeNodeNS");
   pragma Import (Java, RemoveAttributeNode, "removeAttributeNode");
   pragma Import (Java, GetElementsByTagName, "getElementsByTagName");
   pragma Import (Java, GetElementsByTagNameNS, "getElementsByTagNameNS");
   pragma Import (Java, HasAttributes, "hasAttributes");
   pragma Import (Java, HasAttribute, "hasAttribute");
   pragma Import (Java, HasAttributeNS, "hasAttributeNS");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, Item, "item");
   pragma Import (Java, GetUserObject, "getUserObject");
   pragma Import (Java, SetUserObject, "setUserObject");
   pragma Import (Java, SetIdAttribute, "setIdAttribute");
   pragma Import (Java, SetIdAttributeNS, "setIdAttributeNS");
   pragma Import (Java, SetIdAttributeNode, "setIdAttributeNode");
   pragma Import (Java, GetSchemaTypeInfo, "getSchemaTypeInfo");
   pragma Import (Java, SetUserData, "setUserData");
   pragma Import (Java, GetUserData, "getUserData");
   pragma Import (Java, GetFeature, "getFeature");
   pragma Import (Java, IsSameNode, "isSameNode");
   pragma Import (Java, IsEqualNode, "isEqualNode");
   pragma Import (Java, LookupNamespaceURI, "lookupNamespaceURI");
   pragma Import (Java, IsDefaultNamespace, "isDefaultNamespace");
   pragma Import (Java, LookupPrefix, "lookupPrefix");
   pragma Import (Java, GetTextContent, "getTextContent");
   pragma Import (Java, SetTextContent, "setTextContent");
   pragma Import (Java, CompareDocumentPosition, "compareDocumentPosition");
   pragma Import (Java, GetBaseURI, "getBaseURI");

end Javax.Imageio.Metadata.IIOMetadataNode;
pragma Import (Java, Javax.Imageio.Metadata.IIOMetadataNode, "javax.imageio.metadata.IIOMetadataNode");
pragma Extensions_Allowed (Off);
