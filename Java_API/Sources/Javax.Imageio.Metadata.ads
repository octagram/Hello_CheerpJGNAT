pragma Extensions_Allowed (On);
package Javax.Imageio.Metadata is
   pragma Preelaborate;
end Javax.Imageio.Metadata;
pragma Import (Java, Javax.Imageio.Metadata, "javax.imageio.metadata");
pragma Extensions_Allowed (Off);
