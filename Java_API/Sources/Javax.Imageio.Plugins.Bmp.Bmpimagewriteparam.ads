pragma Extensions_Allowed (On);
limited with Java.Util.Locale;
with Java.Lang.Object;
with Javax.Imageio.ImageWriteParam;

package Javax.Imageio.Plugins.Bmp.BMPImageWriteParam is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Imageio.ImageWriteParam.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BMPImageWriteParam (P1_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_BMPImageWriteParam (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTopDown (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsTopDown (This : access Typ)
                       return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BMPImageWriteParam);
   pragma Import (Java, SetTopDown, "setTopDown");
   pragma Import (Java, IsTopDown, "isTopDown");

end Javax.Imageio.Plugins.Bmp.BMPImageWriteParam;
pragma Import (Java, Javax.Imageio.Plugins.Bmp.BMPImageWriteParam, "javax.imageio.plugins.bmp.BMPImageWriteParam");
pragma Extensions_Allowed (Off);
