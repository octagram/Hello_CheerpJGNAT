pragma Extensions_Allowed (On);
package Javax.Imageio.Plugins.Bmp is
   pragma Preelaborate;
end Javax.Imageio.Plugins.Bmp;
pragma Import (Java, Javax.Imageio.Plugins.Bmp, "javax.imageio.plugins.bmp");
pragma Extensions_Allowed (Off);
