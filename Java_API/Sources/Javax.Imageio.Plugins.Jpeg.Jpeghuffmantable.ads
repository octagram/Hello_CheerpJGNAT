pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JPEGHuffmanTable (P1_Short_Arr : Java.Short_Arr;
                                  P2_Short_Arr : Java.Short_Arr; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLengths (This : access Typ)
                        return Java.Short_Arr;

   function GetValues (This : access Typ)
                       return Java.Short_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   StdDCLuminance : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Typ'Class;

   --  final
   StdDCChrominance : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Typ'Class;

   --  final
   StdACLuminance : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Typ'Class;

   --  final
   StdACChrominance : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JPEGHuffmanTable);
   pragma Import (Java, GetLengths, "getLengths");
   pragma Import (Java, GetValues, "getValues");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, StdDCLuminance, "StdDCLuminance");
   pragma Import (Java, StdDCChrominance, "StdDCChrominance");
   pragma Import (Java, StdACLuminance, "StdACLuminance");
   pragma Import (Java, StdACChrominance, "StdACChrominance");

end Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable;
pragma Import (Java, Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable, "javax.imageio.plugins.jpeg.JPEGHuffmanTable");
pragma Extensions_Allowed (Off);
