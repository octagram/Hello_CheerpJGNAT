pragma Extensions_Allowed (On);
limited with Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable;
limited with Javax.Imageio.Plugins.Jpeg.JPEGQTable;
with Java.Lang.Object;
with Javax.Imageio.ImageReadParam;

package Javax.Imageio.Plugins.Jpeg.JPEGImageReadParam is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Imageio.ImageReadParam.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JPEGImageReadParam (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function AreTablesSet (This : access Typ)
                          return Java.Boolean;

   procedure SetDecodeTables (This : access Typ;
                              P1_JPEGQTable_Arr : access Javax.Imageio.Plugins.Jpeg.JPEGQTable.Arr_Obj;
                              P2_JPEGHuffmanTable_Arr : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Arr_Obj;
                              P3_JPEGHuffmanTable_Arr : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Arr_Obj);

   procedure UnsetDecodeTables (This : access Typ);

   function GetQTables (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function GetDCHuffmanTables (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetACHuffmanTables (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JPEGImageReadParam);
   pragma Import (Java, AreTablesSet, "areTablesSet");
   pragma Import (Java, SetDecodeTables, "setDecodeTables");
   pragma Import (Java, UnsetDecodeTables, "unsetDecodeTables");
   pragma Import (Java, GetQTables, "getQTables");
   pragma Import (Java, GetDCHuffmanTables, "getDCHuffmanTables");
   pragma Import (Java, GetACHuffmanTables, "getACHuffmanTables");

end Javax.Imageio.Plugins.Jpeg.JPEGImageReadParam;
pragma Import (Java, Javax.Imageio.Plugins.Jpeg.JPEGImageReadParam, "javax.imageio.plugins.jpeg.JPEGImageReadParam");
pragma Extensions_Allowed (Off);
