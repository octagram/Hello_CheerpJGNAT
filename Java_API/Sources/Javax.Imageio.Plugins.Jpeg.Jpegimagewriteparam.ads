pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable;
limited with Javax.Imageio.Plugins.Jpeg.JPEGQTable;
with Java.Lang.Object;
with Javax.Imageio.ImageWriteParam;

package Javax.Imageio.Plugins.Jpeg.JPEGImageWriteParam is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Imageio.ImageWriteParam.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JPEGImageWriteParam (P1_Locale : access Standard.Java.Util.Locale.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure UnsetCompression (This : access Typ);

   function IsCompressionLossless (This : access Typ)
                                   return Java.Boolean;

   function GetCompressionQualityDescriptions (This : access Typ)
                                               return Standard.Java.Lang.Object.Ref;

   function GetCompressionQualityValues (This : access Typ)
                                         return Java.Float_Arr;

   function AreTablesSet (This : access Typ)
                          return Java.Boolean;

   procedure SetEncodeTables (This : access Typ;
                              P1_JPEGQTable_Arr : access Javax.Imageio.Plugins.Jpeg.JPEGQTable.Arr_Obj;
                              P2_JPEGHuffmanTable_Arr : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Arr_Obj;
                              P3_JPEGHuffmanTable_Arr : access Javax.Imageio.Plugins.Jpeg.JPEGHuffmanTable.Arr_Obj);

   procedure UnsetEncodeTables (This : access Typ);

   function GetQTables (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function GetDCHuffmanTables (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetACHuffmanTables (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   procedure SetOptimizeHuffmanTables (This : access Typ;
                                       P1_Boolean : Java.Boolean);

   function GetOptimizeHuffmanTables (This : access Typ)
                                      return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JPEGImageWriteParam);
   pragma Import (Java, UnsetCompression, "unsetCompression");
   pragma Import (Java, IsCompressionLossless, "isCompressionLossless");
   pragma Import (Java, GetCompressionQualityDescriptions, "getCompressionQualityDescriptions");
   pragma Import (Java, GetCompressionQualityValues, "getCompressionQualityValues");
   pragma Import (Java, AreTablesSet, "areTablesSet");
   pragma Import (Java, SetEncodeTables, "setEncodeTables");
   pragma Import (Java, UnsetEncodeTables, "unsetEncodeTables");
   pragma Import (Java, GetQTables, "getQTables");
   pragma Import (Java, GetDCHuffmanTables, "getDCHuffmanTables");
   pragma Import (Java, GetACHuffmanTables, "getACHuffmanTables");
   pragma Import (Java, SetOptimizeHuffmanTables, "setOptimizeHuffmanTables");
   pragma Import (Java, GetOptimizeHuffmanTables, "getOptimizeHuffmanTables");

end Javax.Imageio.Plugins.Jpeg.JPEGImageWriteParam;
pragma Import (Java, Javax.Imageio.Plugins.Jpeg.JPEGImageWriteParam, "javax.imageio.plugins.jpeg.JPEGImageWriteParam");
pragma Extensions_Allowed (Off);
