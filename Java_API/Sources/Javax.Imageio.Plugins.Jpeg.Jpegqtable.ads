pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Imageio.Plugins.Jpeg.JPEGQTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JPEGQTable (P1_Int_Arr : Java.Int_Arr; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTable (This : access Typ)
                      return Java.Int_Arr;

   function GetScaledInstance (This : access Typ;
                               P1_Float : Java.Float;
                               P2_Boolean : Java.Boolean)
                               return access Javax.Imageio.Plugins.Jpeg.JPEGQTable.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   K1Luminance : access Javax.Imageio.Plugins.Jpeg.JPEGQTable.Typ'Class;

   --  final
   K1Div2Luminance : access Javax.Imageio.Plugins.Jpeg.JPEGQTable.Typ'Class;

   --  final
   K2Chrominance : access Javax.Imageio.Plugins.Jpeg.JPEGQTable.Typ'Class;

   --  final
   K2Div2Chrominance : access Javax.Imageio.Plugins.Jpeg.JPEGQTable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JPEGQTable);
   pragma Import (Java, GetTable, "getTable");
   pragma Import (Java, GetScaledInstance, "getScaledInstance");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, K1Luminance, "K1Luminance");
   pragma Import (Java, K1Div2Luminance, "K1Div2Luminance");
   pragma Import (Java, K2Chrominance, "K2Chrominance");
   pragma Import (Java, K2Div2Chrominance, "K2Div2Chrominance");

end Javax.Imageio.Plugins.Jpeg.JPEGQTable;
pragma Import (Java, Javax.Imageio.Plugins.Jpeg.JPEGQTable, "javax.imageio.plugins.jpeg.JPEGQTable");
pragma Extensions_Allowed (Off);
