pragma Extensions_Allowed (On);
package Javax.Imageio.Plugins.Jpeg is
   pragma Preelaborate;
end Javax.Imageio.Plugins.Jpeg;
pragma Import (Java, Javax.Imageio.Plugins.Jpeg, "javax.imageio.plugins.jpeg");
pragma Extensions_Allowed (Off);
