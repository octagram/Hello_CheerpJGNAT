pragma Extensions_Allowed (On);
package Javax.Imageio.Plugins is
   pragma Preelaborate;
end Javax.Imageio.Plugins;
pragma Import (Java, Javax.Imageio.Plugins, "javax.imageio.plugins");
pragma Extensions_Allowed (Off);
