pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Imageio.Spi.ServiceRegistry;

package Javax.Imageio.Spi.IIORegistry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Imageio.Spi.ServiceRegistry.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultInstance return access Javax.Imageio.Spi.IIORegistry.Typ'Class;

   procedure RegisterApplicationClasspathSpis (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDefaultInstance, "getDefaultInstance");
   pragma Import (Java, RegisterApplicationClasspathSpis, "registerApplicationClasspathSpis");

end Javax.Imageio.Spi.IIORegistry;
pragma Import (Java, Javax.Imageio.Spi.IIORegistry, "javax.imageio.spi.IIORegistry");
pragma Extensions_Allowed (Off);
