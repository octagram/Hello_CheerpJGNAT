pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Imageio.Spi.ServiceRegistry;
with Java.Lang.Object;
with Javax.Imageio.Spi.RegisterableService;

package Javax.Imageio.Spi.IIOServiceProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RegisterableService_I : Javax.Imageio.Spi.RegisterableService.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      VendorName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, VendorName, "vendorName");

      --  protected
      Version : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Version, "version");

   end record;

   function New_IIOServiceProvider (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_IIOServiceProvider (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure OnRegistration (This : access Typ;
                             P1_ServiceRegistry : access Standard.Javax.Imageio.Spi.ServiceRegistry.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class);

   procedure OnDeregistration (This : access Typ;
                               P1_ServiceRegistry : access Standard.Javax.Imageio.Spi.ServiceRegistry.Typ'Class;
                               P2_Class : access Standard.Java.Lang.Class.Typ'Class);

   function GetVendorName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ;
                            P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IIOServiceProvider);
   pragma Export (Java, OnRegistration, "onRegistration");
   pragma Export (Java, OnDeregistration, "onDeregistration");
   pragma Export (Java, GetVendorName, "getVendorName");
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, GetDescription, "getDescription");

end Javax.Imageio.Spi.IIOServiceProvider;
pragma Import (Java, Javax.Imageio.Spi.IIOServiceProvider, "javax.imageio.spi.IIOServiceProvider");
pragma Extensions_Allowed (Off);
