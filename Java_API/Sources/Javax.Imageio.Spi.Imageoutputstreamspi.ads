pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Imageio.Stream.ImageOutputStream;
with Java.Lang.Object;
with Javax.Imageio.Spi.IIOServiceProvider;
with Javax.Imageio.Spi.RegisterableService;

package Javax.Imageio.Spi.ImageOutputStreamSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RegisterableService_I : Javax.Imageio.Spi.RegisterableService.Ref)
    is abstract new Javax.Imageio.Spi.IIOServiceProvider.Typ(RegisterableService_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      OutputClass : access Java.Lang.Class.Typ'Class;
      pragma Import (Java, OutputClass, "outputClass");

   end record;

   --  protected
   function New_ImageOutputStreamSpi (This : Ref := null)
                                      return Ref;

   function New_ImageOutputStreamSpi (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                                      P3_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOutputClass (This : access Typ)
                            return access Java.Lang.Class.Typ'Class;

   function CanUseCacheFile (This : access Typ)
                             return Java.Boolean;

   function NeedsCacheFile (This : access Typ)
                            return Java.Boolean;

   function CreateOutputStreamInstance (This : access Typ;
                                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P2_Boolean : Java.Boolean;
                                        P3_File : access Standard.Java.Io.File.Typ'Class)
                                        return access Javax.Imageio.Stream.ImageOutputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function CreateOutputStreamInstance (This : access Typ;
                                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                        return access Javax.Imageio.Stream.ImageOutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageOutputStreamSpi);
   pragma Export (Java, GetOutputClass, "getOutputClass");
   pragma Export (Java, CanUseCacheFile, "canUseCacheFile");
   pragma Export (Java, NeedsCacheFile, "needsCacheFile");
   pragma Export (Java, CreateOutputStreamInstance, "createOutputStreamInstance");

end Javax.Imageio.Spi.ImageOutputStreamSpi;
pragma Import (Java, Javax.Imageio.Spi.ImageOutputStreamSpi, "javax.imageio.spi.ImageOutputStreamSpi");
pragma Extensions_Allowed (Off);
