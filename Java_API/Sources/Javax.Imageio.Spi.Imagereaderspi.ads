pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Imageio.ImageReader;
with Java.Lang.Object;
with Javax.Imageio.Spi.ImageReaderWriterSpi;
with Javax.Imageio.Spi.RegisterableService;

package Javax.Imageio.Spi.ImageReaderSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RegisterableService_I : Javax.Imageio.Spi.RegisterableService.Ref)
    is abstract new Javax.Imageio.Spi.ImageReaderWriterSpi.Typ(RegisterableService_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      InputTypes : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, InputTypes, "inputTypes");

      --  protected
      WriterSpiNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, WriterSpiNames, "writerSpiNames");

   end record;

   --  protected
   function New_ImageReaderSpi (This : Ref := null)
                                return Ref;

   function New_ImageReaderSpi (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String_Arr : access Java.Lang.String.Arr_Obj;
                                P4_String_Arr : access Java.Lang.String.Arr_Obj;
                                P5_String_Arr : access Java.Lang.String.Arr_Obj;
                                P6_String : access Standard.Java.Lang.String.Typ'Class;
                                P7_Class_Arr : access Java.Lang.Class.Arr_Obj;
                                P8_String_Arr : access Java.Lang.String.Arr_Obj;
                                P9_Boolean : Java.Boolean;
                                P10_String : access Standard.Java.Lang.String.Typ'Class;
                                P11_String : access Standard.Java.Lang.String.Typ'Class;
                                P12_String_Arr : access Java.Lang.String.Arr_Obj;
                                P13_String_Arr : access Java.Lang.String.Arr_Obj;
                                P14_Boolean : Java.Boolean;
                                P15_String : access Standard.Java.Lang.String.Typ'Class;
                                P16_String : access Standard.Java.Lang.String.Typ'Class;
                                P17_String_Arr : access Java.Lang.String.Arr_Obj;
                                P18_String_Arr : access Java.Lang.String.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInputTypes (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function CanDecodeInput (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function CreateReaderInstance (This : access Typ)
                                  return access Javax.Imageio.ImageReader.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function CreateReaderInstance (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Javax.Imageio.ImageReader.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function IsOwnReader (This : access Typ;
                         P1_ImageReader : access Standard.Javax.Imageio.ImageReader.Typ'Class)
                         return Java.Boolean;

   function GetImageWriterSpiNames (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   STANDARD_INPUT_TYPE : Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageReaderSpi);
   pragma Export (Java, GetInputTypes, "getInputTypes");
   pragma Export (Java, CanDecodeInput, "canDecodeInput");
   pragma Export (Java, CreateReaderInstance, "createReaderInstance");
   pragma Export (Java, IsOwnReader, "isOwnReader");
   pragma Export (Java, GetImageWriterSpiNames, "getImageWriterSpiNames");
   pragma Import (Java, STANDARD_INPUT_TYPE, "STANDARD_INPUT_TYPE");

end Javax.Imageio.Spi.ImageReaderSpi;
pragma Import (Java, Javax.Imageio.Spi.ImageReaderSpi, "javax.imageio.spi.ImageReaderSpi");
pragma Extensions_Allowed (Off);
