pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Imageio.Metadata.IIOMetadataFormat;
with Java.Lang.Object;
with Javax.Imageio.Spi.IIOServiceProvider;
with Javax.Imageio.Spi.RegisterableService;

package Javax.Imageio.Spi.ImageReaderWriterSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RegisterableService_I : Javax.Imageio.Spi.RegisterableService.Ref)
    is abstract new Javax.Imageio.Spi.IIOServiceProvider.Typ(RegisterableService_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Names : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Names, "names");

      --  protected
      Suffixes : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Suffixes, "suffixes");

      --  protected
      MIMETypes : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, MIMETypes, "MIMETypes");

      --  protected
      PluginClassName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, PluginClassName, "pluginClassName");

      --  protected
      SupportsStandardStreamMetadataFormat : Java.Boolean;
      pragma Import (Java, SupportsStandardStreamMetadataFormat, "supportsStandardStreamMetadataFormat");

      --  protected
      NativeStreamMetadataFormatName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, NativeStreamMetadataFormatName, "nativeStreamMetadataFormatName");

      --  protected
      NativeStreamMetadataFormatClassName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, NativeStreamMetadataFormatClassName, "nativeStreamMetadataFormatClassName");

      --  protected
      ExtraStreamMetadataFormatNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ExtraStreamMetadataFormatNames, "extraStreamMetadataFormatNames");

      --  protected
      ExtraStreamMetadataFormatClassNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ExtraStreamMetadataFormatClassNames, "extraStreamMetadataFormatClassNames");

      --  protected
      SupportsStandardImageMetadataFormat : Java.Boolean;
      pragma Import (Java, SupportsStandardImageMetadataFormat, "supportsStandardImageMetadataFormat");

      --  protected
      NativeImageMetadataFormatName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, NativeImageMetadataFormatName, "nativeImageMetadataFormatName");

      --  protected
      NativeImageMetadataFormatClassName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, NativeImageMetadataFormatClassName, "nativeImageMetadataFormatClassName");

      --  protected
      ExtraImageMetadataFormatNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ExtraImageMetadataFormatNames, "extraImageMetadataFormatNames");

      --  protected
      ExtraImageMetadataFormatClassNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ExtraImageMetadataFormatClassNames, "extraImageMetadataFormatClassNames");

   end record;

   function New_ImageReaderWriterSpi (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                                      P3_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P4_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P5_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P6_String : access Standard.Java.Lang.String.Typ'Class;
                                      P7_Boolean : Java.Boolean;
                                      P8_String : access Standard.Java.Lang.String.Typ'Class;
                                      P9_String : access Standard.Java.Lang.String.Typ'Class;
                                      P10_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P11_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P12_Boolean : Java.Boolean;
                                      P13_String : access Standard.Java.Lang.String.Typ'Class;
                                      P14_String : access Standard.Java.Lang.String.Typ'Class;
                                      P15_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P16_String_Arr : access Java.Lang.String.Arr_Obj; 
                                      This : Ref := null)
                                      return Ref;

   function New_ImageReaderWriterSpi (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFormatNames (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetFileSuffixes (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetMIMETypes (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   function GetPluginClassName (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function IsStandardStreamMetadataFormatSupported (This : access Typ)
                                                     return Java.Boolean;

   function GetNativeStreamMetadataFormatName (This : access Typ)
                                               return access Java.Lang.String.Typ'Class;

   function GetExtraStreamMetadataFormatNames (This : access Typ)
                                               return Standard.Java.Lang.Object.Ref;

   function IsStandardImageMetadataFormatSupported (This : access Typ)
                                                    return Java.Boolean;

   function GetNativeImageMetadataFormatName (This : access Typ)
                                              return access Java.Lang.String.Typ'Class;

   function GetExtraImageMetadataFormatNames (This : access Typ)
                                              return Standard.Java.Lang.Object.Ref;

   function GetStreamMetadataFormat (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return access Javax.Imageio.Metadata.IIOMetadataFormat.Typ'Class;

   function GetImageMetadataFormat (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Javax.Imageio.Metadata.IIOMetadataFormat.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageReaderWriterSpi);
   pragma Import (Java, GetFormatNames, "getFormatNames");
   pragma Import (Java, GetFileSuffixes, "getFileSuffixes");
   pragma Import (Java, GetMIMETypes, "getMIMETypes");
   pragma Import (Java, GetPluginClassName, "getPluginClassName");
   pragma Import (Java, IsStandardStreamMetadataFormatSupported, "isStandardStreamMetadataFormatSupported");
   pragma Import (Java, GetNativeStreamMetadataFormatName, "getNativeStreamMetadataFormatName");
   pragma Import (Java, GetExtraStreamMetadataFormatNames, "getExtraStreamMetadataFormatNames");
   pragma Import (Java, IsStandardImageMetadataFormatSupported, "isStandardImageMetadataFormatSupported");
   pragma Import (Java, GetNativeImageMetadataFormatName, "getNativeImageMetadataFormatName");
   pragma Import (Java, GetExtraImageMetadataFormatNames, "getExtraImageMetadataFormatNames");
   pragma Import (Java, GetStreamMetadataFormat, "getStreamMetadataFormat");
   pragma Import (Java, GetImageMetadataFormat, "getImageMetadataFormat");

end Javax.Imageio.Spi.ImageReaderWriterSpi;
pragma Import (Java, Javax.Imageio.Spi.ImageReaderWriterSpi, "javax.imageio.spi.ImageReaderWriterSpi");
pragma Extensions_Allowed (Off);
