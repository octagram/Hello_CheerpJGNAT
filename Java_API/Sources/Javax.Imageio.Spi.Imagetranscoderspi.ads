pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Imageio.ImageTranscoder;
with Java.Lang.Object;
with Javax.Imageio.Spi.IIOServiceProvider;
with Javax.Imageio.Spi.RegisterableService;

package Javax.Imageio.Spi.ImageTranscoderSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RegisterableService_I : Javax.Imageio.Spi.RegisterableService.Ref)
    is abstract new Javax.Imageio.Spi.IIOServiceProvider.Typ(RegisterableService_I)
      with null record;

   --  protected
   function New_ImageTranscoderSpi (This : Ref := null)
                                    return Ref;

   function New_ImageTranscoderSpi (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReaderServiceProviderName (This : access Typ)
                                          return access Java.Lang.String.Typ'Class is abstract;

   function GetWriterServiceProviderName (This : access Typ)
                                          return access Java.Lang.String.Typ'Class is abstract;

   function CreateTranscoderInstance (This : access Typ)
                                      return access Javax.Imageio.ImageTranscoder.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageTranscoderSpi);
   pragma Export (Java, GetReaderServiceProviderName, "getReaderServiceProviderName");
   pragma Export (Java, GetWriterServiceProviderName, "getWriterServiceProviderName");
   pragma Export (Java, CreateTranscoderInstance, "createTranscoderInstance");

end Javax.Imageio.Spi.ImageTranscoderSpi;
pragma Import (Java, Javax.Imageio.Spi.ImageTranscoderSpi, "javax.imageio.spi.ImageTranscoderSpi");
pragma Extensions_Allowed (Off);
