pragma Extensions_Allowed (On);
limited with Java.Awt.Image.RenderedImage;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Imageio.ImageTypeSpecifier;
limited with Javax.Imageio.ImageWriter;
with Java.Lang.Object;
with Javax.Imageio.Spi.ImageReaderWriterSpi;
with Javax.Imageio.Spi.RegisterableService;

package Javax.Imageio.Spi.ImageWriterSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RegisterableService_I : Javax.Imageio.Spi.RegisterableService.Ref)
    is abstract new Javax.Imageio.Spi.ImageReaderWriterSpi.Typ(RegisterableService_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      OutputTypes : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, OutputTypes, "outputTypes");

      --  protected
      ReaderSpiNames : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ReaderSpiNames, "readerSpiNames");

   end record;

   --  protected
   function New_ImageWriterSpi (This : Ref := null)
                                return Ref;

   function New_ImageWriterSpi (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String_Arr : access Java.Lang.String.Arr_Obj;
                                P4_String_Arr : access Java.Lang.String.Arr_Obj;
                                P5_String_Arr : access Java.Lang.String.Arr_Obj;
                                P6_String : access Standard.Java.Lang.String.Typ'Class;
                                P7_Class_Arr : access Java.Lang.Class.Arr_Obj;
                                P8_String_Arr : access Java.Lang.String.Arr_Obj;
                                P9_Boolean : Java.Boolean;
                                P10_String : access Standard.Java.Lang.String.Typ'Class;
                                P11_String : access Standard.Java.Lang.String.Typ'Class;
                                P12_String_Arr : access Java.Lang.String.Arr_Obj;
                                P13_String_Arr : access Java.Lang.String.Arr_Obj;
                                P14_Boolean : Java.Boolean;
                                P15_String : access Standard.Java.Lang.String.Typ'Class;
                                P16_String : access Standard.Java.Lang.String.Typ'Class;
                                P17_String_Arr : access Java.Lang.String.Arr_Obj;
                                P18_String_Arr : access Java.Lang.String.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsFormatLossless (This : access Typ)
                              return Java.Boolean;

   function GetOutputTypes (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function CanEncodeImage (This : access Typ;
                            P1_ImageTypeSpecifier : access Standard.Javax.Imageio.ImageTypeSpecifier.Typ'Class)
                            return Java.Boolean is abstract;

   function CanEncodeImage (This : access Typ;
                            P1_RenderedImage : access Standard.Java.Awt.Image.RenderedImage.Typ'Class)
                            return Java.Boolean;

   function CreateWriterInstance (This : access Typ)
                                  return access Javax.Imageio.ImageWriter.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function CreateWriterInstance (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Javax.Imageio.ImageWriter.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function IsOwnWriter (This : access Typ;
                         P1_ImageWriter : access Standard.Javax.Imageio.ImageWriter.Typ'Class)
                         return Java.Boolean;

   function GetImageReaderSpiNames (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   STANDARD_OUTPUT_TYPE : Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageWriterSpi);
   pragma Export (Java, IsFormatLossless, "isFormatLossless");
   pragma Export (Java, GetOutputTypes, "getOutputTypes");
   pragma Export (Java, CanEncodeImage, "canEncodeImage");
   pragma Export (Java, CreateWriterInstance, "createWriterInstance");
   pragma Export (Java, IsOwnWriter, "isOwnWriter");
   pragma Export (Java, GetImageReaderSpiNames, "getImageReaderSpiNames");
   pragma Import (Java, STANDARD_OUTPUT_TYPE, "STANDARD_OUTPUT_TYPE");

end Javax.Imageio.Spi.ImageWriterSpi;
pragma Import (Java, Javax.Imageio.Spi.ImageWriterSpi, "javax.imageio.spi.ImageWriterSpi");
pragma Extensions_Allowed (Off);
