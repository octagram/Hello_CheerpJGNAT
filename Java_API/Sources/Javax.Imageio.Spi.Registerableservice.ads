pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Javax.Imageio.Spi.ServiceRegistry;
with Java.Lang.Object;

package Javax.Imageio.Spi.RegisterableService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure OnRegistration (This : access Typ;
                             P1_ServiceRegistry : access Standard.Javax.Imageio.Spi.ServiceRegistry.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class) is abstract;

   procedure OnDeregistration (This : access Typ;
                               P1_ServiceRegistry : access Standard.Javax.Imageio.Spi.ServiceRegistry.Typ'Class;
                               P2_Class : access Standard.Java.Lang.Class.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, OnRegistration, "onRegistration");
   pragma Export (Java, OnDeregistration, "onDeregistration");

end Javax.Imageio.Spi.RegisterableService;
pragma Import (Java, Javax.Imageio.Spi.RegisterableService, "javax.imageio.spi.RegisterableService");
pragma Extensions_Allowed (Off);
