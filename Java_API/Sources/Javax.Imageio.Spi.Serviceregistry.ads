pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Util.Iterator;
limited with Javax.Imageio.Spi.ServiceRegistry.Filter;
with Java.Lang.Object;

package Javax.Imageio.Spi.ServiceRegistry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ServiceRegistry (P1_Iterator : access Standard.Java.Util.Iterator.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function LookupProviders (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                             return access Java.Util.Iterator.Typ'Class;

   function LookupProviders (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                             return access Java.Util.Iterator.Typ'Class;

   function GetCategories (This : access Typ)
                           return access Java.Util.Iterator.Typ'Class;

   function RegisterServiceProvider (This : access Typ;
                                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                                     return Java.Boolean;

   procedure RegisterServiceProvider (This : access Typ;
                                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RegisterServiceProviders (This : access Typ;
                                       P1_Iterator : access Standard.Java.Util.Iterator.Typ'Class);

   function DeregisterServiceProvider (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                                       return Java.Boolean;

   procedure DeregisterServiceProvider (This : access Typ;
                                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function GetServiceProviders (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                 P2_Boolean : Java.Boolean)
                                 return access Java.Util.Iterator.Typ'Class;

   function GetServiceProviders (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                 P2_Filter : access Standard.Javax.Imageio.Spi.ServiceRegistry.Filter.Typ'Class;
                                 P3_Boolean : Java.Boolean)
                                 return access Java.Util.Iterator.Typ'Class;

   function GetServiceProviderByClass (This : access Typ;
                                       P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                       return access Java.Lang.Object.Typ'Class;

   function SetOrdering (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function UnsetOrdering (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   procedure DeregisterAll (This : access Typ;
                            P1_Class : access Standard.Java.Lang.Class.Typ'Class);

   procedure DeregisterAll (This : access Typ);

   procedure Finalize (This : access Typ);
   --  can raise Java.Lang.Throwable.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServiceRegistry);
   pragma Import (Java, LookupProviders, "lookupProviders");
   pragma Import (Java, GetCategories, "getCategories");
   pragma Import (Java, RegisterServiceProvider, "registerServiceProvider");
   pragma Import (Java, RegisterServiceProviders, "registerServiceProviders");
   pragma Import (Java, DeregisterServiceProvider, "deregisterServiceProvider");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, GetServiceProviders, "getServiceProviders");
   pragma Import (Java, GetServiceProviderByClass, "getServiceProviderByClass");
   pragma Import (Java, SetOrdering, "setOrdering");
   pragma Import (Java, UnsetOrdering, "unsetOrdering");
   pragma Import (Java, DeregisterAll, "deregisterAll");
   pragma Import (Java, Finalize, "finalize");

end Javax.Imageio.Spi.ServiceRegistry;
pragma Import (Java, Javax.Imageio.Spi.ServiceRegistry, "javax.imageio.spi.ServiceRegistry");
pragma Extensions_Allowed (Off);
