pragma Extensions_Allowed (On);
package Javax.Imageio.Spi is
   pragma Preelaborate;
end Javax.Imageio.Spi;
pragma Import (Java, Javax.Imageio.Spi, "javax.imageio.spi");
pragma Extensions_Allowed (Off);
