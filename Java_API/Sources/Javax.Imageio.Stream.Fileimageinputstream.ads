pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.RandomAccessFile;
with Java.Lang.Object;
with Javax.Imageio.Stream.ImageInputStream;
with Javax.Imageio.Stream.ImageInputStreamImpl;

package Javax.Imageio.Stream.FileImageInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageInputStream_I : Javax.Imageio.Stream.ImageInputStream.Ref)
    is new Javax.Imageio.Stream.ImageInputStreamImpl.Typ(ImageInputStream_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileImageInputStream (P1_File : access Standard.Java.Io.File.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;
   --  can raise Java.Io.FileNotFoundException.Except and
   --  Java.Io.IOException.Except

   function New_FileImageInputStream (P1_RandomAccessFile : access Standard.Java.Io.RandomAccessFile.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Length (This : access Typ)
                    return Java.Long;

   procedure Seek (This : access Typ;
                   P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Lang.Throwable.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileImageInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Length, "length");
   pragma Import (Java, Seek, "seek");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Finalize, "finalize");

end Javax.Imageio.Stream.FileImageInputStream;
pragma Import (Java, Javax.Imageio.Stream.FileImageInputStream, "javax.imageio.stream.FileImageInputStream");
pragma Extensions_Allowed (Off);
