pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Imageio.Stream.IIOByteBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IIOByteBuffer (P1_Byte_Arr : Java.Byte_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetData (This : access Typ)
                     return Java.Byte_Arr;

   procedure SetData (This : access Typ;
                      P1_Byte_Arr : Java.Byte_Arr);

   function GetOffset (This : access Typ)
                       return Java.Int;

   procedure SetOffset (This : access Typ;
                        P1_Int : Java.Int);

   function GetLength (This : access Typ)
                       return Java.Int;

   procedure SetLength (This : access Typ;
                        P1_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IIOByteBuffer);
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, SetData, "setData");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, SetOffset, "setOffset");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, SetLength, "setLength");

end Javax.Imageio.Stream.IIOByteBuffer;
pragma Import (Java, Javax.Imageio.Stream.IIOByteBuffer, "javax.imageio.stream.IIOByteBuffer");
pragma Extensions_Allowed (Off);
