pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteOrder;
limited with Javax.Imageio.Stream.IIOByteBuffer;
with Java.Io.DataInput;
with Java.Lang.Object;

package Javax.Imageio.Stream.ImageInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DataInput_I : Java.Io.DataInput.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetByteOrder (This : access Typ;
                           P1_ByteOrder : access Standard.Java.Nio.ByteOrder.Typ'Class) is abstract;

   function GetByteOrder (This : access Typ)
                          return access Java.Nio.ByteOrder.Typ'Class is abstract;

   function Read (This : access Typ)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadBytes (This : access Typ;
                        P1_IIOByteBuffer : access Standard.Javax.Imageio.Stream.IIOByteBuffer.Typ'Class;
                        P2_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadBoolean (This : access Typ)
                         return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadByte (This : access Typ)
                      return Java.Byte is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedByte (This : access Typ)
                              return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadShort (This : access Typ)
                       return Java.Short is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedShort (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadChar (This : access Typ)
                      return Java.Char is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadInt (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedInt (This : access Typ)
                             return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadLong (This : access Typ)
                      return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadFloat (This : access Typ)
                       return Java.Float is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadDouble (This : access Typ)
                        return Java.Double is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadLine (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadUTF (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Short_Arr : Java.Short_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Char_Arr : Java.Char_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Int_Arr : Java.Int_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Long_Arr : Java.Long_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Float_Arr : Java.Float_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Double_Arr : Java.Double_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   function GetStreamPosition (This : access Typ)
                               return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function GetBitOffset (This : access Typ)
                          return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   procedure SetBitOffset (This : access Typ;
                           P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadBit (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function ReadBits (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function Length (This : access Typ)
                    return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   function SkipBytes (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function SkipBytes (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Long is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Seek (This : access Typ;
                   P1_Long : Java.Long) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Mark (This : access Typ) is abstract;

   procedure Reset (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure FlushBefore (This : access Typ;
                          P1_Long : Java.Long) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   function GetFlushedPosition (This : access Typ)
                                return Java.Long is abstract;

   function IsCached (This : access Typ)
                      return Java.Boolean is abstract;

   function IsCachedMemory (This : access Typ)
                            return Java.Boolean is abstract;

   function IsCachedFile (This : access Typ)
                          return Java.Boolean is abstract;

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetByteOrder, "setByteOrder");
   pragma Export (Java, GetByteOrder, "getByteOrder");
   pragma Export (Java, Read, "read");
   pragma Export (Java, ReadBytes, "readBytes");
   pragma Export (Java, ReadBoolean, "readBoolean");
   pragma Export (Java, ReadByte, "readByte");
   pragma Export (Java, ReadUnsignedByte, "readUnsignedByte");
   pragma Export (Java, ReadShort, "readShort");
   pragma Export (Java, ReadUnsignedShort, "readUnsignedShort");
   pragma Export (Java, ReadChar, "readChar");
   pragma Export (Java, ReadInt, "readInt");
   pragma Export (Java, ReadUnsignedInt, "readUnsignedInt");
   pragma Export (Java, ReadLong, "readLong");
   pragma Export (Java, ReadFloat, "readFloat");
   pragma Export (Java, ReadDouble, "readDouble");
   pragma Export (Java, ReadLine, "readLine");
   pragma Export (Java, ReadUTF, "readUTF");
   pragma Export (Java, ReadFully, "readFully");
   pragma Export (Java, GetStreamPosition, "getStreamPosition");
   pragma Export (Java, GetBitOffset, "getBitOffset");
   pragma Export (Java, SetBitOffset, "setBitOffset");
   pragma Export (Java, ReadBit, "readBit");
   pragma Export (Java, ReadBits, "readBits");
   pragma Export (Java, Length, "length");
   pragma Export (Java, SkipBytes, "skipBytes");
   pragma Export (Java, Seek, "seek");
   pragma Export (Java, Mark, "mark");
   pragma Export (Java, Reset, "reset");
   pragma Export (Java, FlushBefore, "flushBefore");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, GetFlushedPosition, "getFlushedPosition");
   pragma Export (Java, IsCached, "isCached");
   pragma Export (Java, IsCachedMemory, "isCachedMemory");
   pragma Export (Java, IsCachedFile, "isCachedFile");
   pragma Export (Java, Close, "close");

end Javax.Imageio.Stream.ImageInputStream;
pragma Import (Java, Javax.Imageio.Stream.ImageInputStream, "javax.imageio.stream.ImageInputStream");
pragma Extensions_Allowed (Off);
