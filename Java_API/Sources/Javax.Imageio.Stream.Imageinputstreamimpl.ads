pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteOrder;
limited with Javax.Imageio.Stream.IIOByteBuffer;
with Java.Lang.Object;
with Javax.Imageio.Stream.ImageInputStream;

package Javax.Imageio.Stream.ImageInputStreamImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageInputStream_I : Javax.Imageio.Stream.ImageInputStream.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ByteOrder : access Java.Nio.ByteOrder.Typ'Class;
      pragma Import (Java, ByteOrder, "byteOrder");

      --  protected
      StreamPos : Java.Long;
      pragma Import (Java, StreamPos, "streamPos");

      --  protected
      BitOffset : Java.Int;
      pragma Import (Java, BitOffset, "bitOffset");

      --  protected
      FlushedPos : Java.Long;
      pragma Import (Java, FlushedPos, "flushedPos");

   end record;

   function New_ImageInputStreamImpl (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   procedure CheckClosed (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure SetByteOrder (This : access Typ;
                           P1_ByteOrder : access Standard.Java.Nio.ByteOrder.Typ'Class);

   function GetByteOrder (This : access Typ)
                          return access Java.Nio.ByteOrder.Typ'Class;

   function Read (This : access Typ)
                  return Java.Int is abstract
                  with Export => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int
                  with Import => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int is abstract
                  with Export => "read", Convention => Java;
   --  can raise Java.Io.IOException.Except

   procedure ReadBytes (This : access Typ;
                        P1_IIOByteBuffer : access Standard.Javax.Imageio.Stream.IIOByteBuffer.Typ'Class;
                        P2_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function ReadBoolean (This : access Typ)
                         return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function ReadByte (This : access Typ)
                      return Java.Byte;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedByte (This : access Typ)
                              return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadShort (This : access Typ)
                       return Java.Short;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedShort (This : access Typ)
                               return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadChar (This : access Typ)
                      return Java.Char;
   --  can raise Java.Io.IOException.Except

   function ReadInt (This : access Typ)
                     return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadUnsignedInt (This : access Typ)
                             return Java.Long;
   --  can raise Java.Io.IOException.Except

   function ReadLong (This : access Typ)
                      return Java.Long;
   --  can raise Java.Io.IOException.Except

   function ReadFloat (This : access Typ)
                       return Java.Float;
   --  can raise Java.Io.IOException.Except

   function ReadDouble (This : access Typ)
                        return Java.Double;
   --  can raise Java.Io.IOException.Except

   function ReadLine (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function ReadUTF (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Short_Arr : Java.Short_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Char_Arr : Java.Char_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Int_Arr : Java.Int_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Long_Arr : Java.Long_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Float_Arr : Java.Float_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure ReadFully (This : access Typ;
                        P1_Double_Arr : Java.Double_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function GetStreamPosition (This : access Typ)
                               return Java.Long;
   --  can raise Java.Io.IOException.Except

   function GetBitOffset (This : access Typ)
                          return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure SetBitOffset (This : access Typ;
                           P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function ReadBit (This : access Typ)
                     return Java.Int;
   --  can raise Java.Io.IOException.Except

   function ReadBits (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Length (This : access Typ)
                    return Java.Long;

   function SkipBytes (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   function SkipBytes (This : access Typ;
                       P1_Long : Java.Long)
                       return Java.Long;
   --  can raise Java.Io.IOException.Except

   procedure Seek (This : access Typ;
                   P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   procedure Mark (This : access Typ);

   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure FlushBefore (This : access Typ;
                          P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function GetFlushedPosition (This : access Typ)
                                return Java.Long;

   function IsCached (This : access Typ)
                      return Java.Boolean;

   function IsCachedMemory (This : access Typ)
                            return Java.Boolean;

   function IsCachedFile (This : access Typ)
                          return Java.Boolean;

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Lang.Throwable.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageInputStreamImpl);
   pragma Import (Java, CheckClosed, "checkClosed");
   pragma Import (Java, SetByteOrder, "setByteOrder");
   pragma Import (Java, GetByteOrder, "getByteOrder");
   -- pragma Import (Java, Read, "read");
   pragma Import (Java, ReadBytes, "readBytes");
   pragma Import (Java, ReadBoolean, "readBoolean");
   pragma Import (Java, ReadByte, "readByte");
   pragma Import (Java, ReadUnsignedByte, "readUnsignedByte");
   pragma Import (Java, ReadShort, "readShort");
   pragma Import (Java, ReadUnsignedShort, "readUnsignedShort");
   pragma Import (Java, ReadChar, "readChar");
   pragma Import (Java, ReadInt, "readInt");
   pragma Import (Java, ReadUnsignedInt, "readUnsignedInt");
   pragma Import (Java, ReadLong, "readLong");
   pragma Import (Java, ReadFloat, "readFloat");
   pragma Import (Java, ReadDouble, "readDouble");
   pragma Import (Java, ReadLine, "readLine");
   pragma Import (Java, ReadUTF, "readUTF");
   pragma Import (Java, ReadFully, "readFully");
   pragma Import (Java, GetStreamPosition, "getStreamPosition");
   pragma Import (Java, GetBitOffset, "getBitOffset");
   pragma Import (Java, SetBitOffset, "setBitOffset");
   pragma Import (Java, ReadBit, "readBit");
   pragma Import (Java, ReadBits, "readBits");
   pragma Import (Java, Length, "length");
   pragma Import (Java, SkipBytes, "skipBytes");
   pragma Import (Java, Seek, "seek");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, FlushBefore, "flushBefore");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, GetFlushedPosition, "getFlushedPosition");
   pragma Import (Java, IsCached, "isCached");
   pragma Import (Java, IsCachedMemory, "isCachedMemory");
   pragma Import (Java, IsCachedFile, "isCachedFile");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Finalize, "finalize");

end Javax.Imageio.Stream.ImageInputStreamImpl;
pragma Import (Java, Javax.Imageio.Stream.ImageInputStreamImpl, "javax.imageio.stream.ImageInputStreamImpl");
pragma Extensions_Allowed (Off);
