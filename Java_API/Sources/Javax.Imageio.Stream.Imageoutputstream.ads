pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.DataOutput;
with Java.Lang.Object;
with Javax.Imageio.Stream.ImageInputStream;

package Javax.Imageio.Stream.ImageOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DataOutput_I : Java.Io.DataOutput.Ref;
            ImageInputStream_I : Javax.Imageio.Stream.ImageInputStream.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteBoolean (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteByte (This : access Typ;
                        P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteShort (This : access Typ;
                         P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteChar (This : access Typ;
                        P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteInt (This : access Typ;
                       P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteLong (This : access Typ;
                        P1_Long : Java.Long) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteFloat (This : access Typ;
                         P1_Float : Java.Float) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteDouble (This : access Typ;
                          P1_Double : Java.Double) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteBytes (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteChars (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteUTF (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteShorts (This : access Typ;
                          P1_Short_Arr : Java.Short_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteChars (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteInts (This : access Typ;
                        P1_Int_Arr : Java.Int_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteLongs (This : access Typ;
                         P1_Long_Arr : Java.Long_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteFloats (This : access Typ;
                          P1_Float_Arr : Java.Float_Arr;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteDoubles (This : access Typ;
                           P1_Double_Arr : Java.Double_Arr;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteBit (This : access Typ;
                       P1_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure WriteBits (This : access Typ;
                        P1_Long : Java.Long;
                        P2_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure FlushBefore (This : access Typ;
                          P1_Long : Java.Long) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Write, "write");
   pragma Export (Java, WriteBoolean, "writeBoolean");
   pragma Export (Java, WriteByte, "writeByte");
   pragma Export (Java, WriteShort, "writeShort");
   pragma Export (Java, WriteChar, "writeChar");
   pragma Export (Java, WriteInt, "writeInt");
   pragma Export (Java, WriteLong, "writeLong");
   pragma Export (Java, WriteFloat, "writeFloat");
   pragma Export (Java, WriteDouble, "writeDouble");
   pragma Export (Java, WriteBytes, "writeBytes");
   pragma Export (Java, WriteChars, "writeChars");
   pragma Export (Java, WriteUTF, "writeUTF");
   pragma Export (Java, WriteShorts, "writeShorts");
   pragma Export (Java, WriteInts, "writeInts");
   pragma Export (Java, WriteLongs, "writeLongs");
   pragma Export (Java, WriteFloats, "writeFloats");
   pragma Export (Java, WriteDoubles, "writeDoubles");
   pragma Export (Java, WriteBit, "writeBit");
   pragma Export (Java, WriteBits, "writeBits");
   pragma Export (Java, FlushBefore, "flushBefore");

end Javax.Imageio.Stream.ImageOutputStream;
pragma Import (Java, Javax.Imageio.Stream.ImageOutputStream, "javax.imageio.stream.ImageOutputStream");
pragma Extensions_Allowed (Off);
