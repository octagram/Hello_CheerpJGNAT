pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
with Java.Lang.Object;
with Javax.Imageio.Stream.ImageInputStream;
with Javax.Imageio.Stream.ImageInputStreamImpl;

package Javax.Imageio.Stream.MemoryCacheImageInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageInputStream_I : Javax.Imageio.Stream.ImageInputStream.Ref)
    is new Javax.Imageio.Stream.ImageInputStreamImpl.Typ(ImageInputStream_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MemoryCacheImageInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure FlushBefore (This : access Typ;
                          P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except

   function IsCached (This : access Typ)
                      return Java.Boolean;

   function IsCachedFile (This : access Typ)
                          return Java.Boolean;

   function IsCachedMemory (This : access Typ)
                            return Java.Boolean;

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Finalize (This : access Typ);
   --  can raise Java.Lang.Throwable.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MemoryCacheImageInputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, FlushBefore, "flushBefore");
   pragma Import (Java, IsCached, "isCached");
   pragma Import (Java, IsCachedFile, "isCachedFile");
   pragma Import (Java, IsCachedMemory, "isCachedMemory");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Finalize, "finalize");

end Javax.Imageio.Stream.MemoryCacheImageInputStream;
pragma Import (Java, Javax.Imageio.Stream.MemoryCacheImageInputStream, "javax.imageio.stream.MemoryCacheImageInputStream");
pragma Extensions_Allowed (Off);
