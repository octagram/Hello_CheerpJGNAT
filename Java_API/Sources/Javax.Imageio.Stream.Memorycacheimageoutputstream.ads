pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
with Java.Lang.Object;
with Javax.Imageio.Stream.ImageInputStream;
with Javax.Imageio.Stream.ImageOutputStream;
with Javax.Imageio.Stream.ImageOutputStreamImpl;

package Javax.Imageio.Stream.MemoryCacheImageOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageInputStream_I : Javax.Imageio.Stream.ImageInputStream.Ref;
            ImageOutputStream_I : Javax.Imageio.Stream.ImageOutputStream.Ref)
    is new Javax.Imageio.Stream.ImageOutputStreamImpl.Typ(ImageInputStream_I,
                                                          ImageOutputStream_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MemoryCacheImageOutputStream (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   function Length (This : access Typ)
                    return Java.Long;

   function IsCached (This : access Typ)
                      return Java.Boolean;

   function IsCachedFile (This : access Typ)
                          return Java.Boolean;

   function IsCachedMemory (This : access Typ)
                            return Java.Boolean;

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure FlushBefore (This : access Typ;
                          P1_Long : Java.Long);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MemoryCacheImageOutputStream);
   pragma Import (Java, Read, "read");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Length, "length");
   pragma Import (Java, IsCached, "isCached");
   pragma Import (Java, IsCachedFile, "isCachedFile");
   pragma Import (Java, IsCachedMemory, "isCachedMemory");
   pragma Import (Java, Close, "close");
   pragma Import (Java, FlushBefore, "flushBefore");

end Javax.Imageio.Stream.MemoryCacheImageOutputStream;
pragma Import (Java, Javax.Imageio.Stream.MemoryCacheImageOutputStream, "javax.imageio.stream.MemoryCacheImageOutputStream");
pragma Extensions_Allowed (Off);
