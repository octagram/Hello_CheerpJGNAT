pragma Extensions_Allowed (On);
package Javax.Imageio.Stream is
   pragma Preelaborate;
end Javax.Imageio.Stream;
pragma Import (Java, Javax.Imageio.Stream, "javax.imageio.stream");
pragma Extensions_Allowed (Off);
