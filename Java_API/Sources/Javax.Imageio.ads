pragma Extensions_Allowed (On);
package Javax.Imageio is
   pragma Preelaborate;
end Javax.Imageio;
pragma Import (Java, Javax.Imageio, "javax.imageio");
pragma Extensions_Allowed (Off);
