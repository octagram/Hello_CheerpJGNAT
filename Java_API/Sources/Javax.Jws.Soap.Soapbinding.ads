pragma Extensions_Allowed (On);
limited with Javax.Jws.Soap.SOAPBinding.ParameterStyle;
limited with Javax.Jws.Soap.SOAPBinding.Style;
limited with Javax.Jws.Soap.SOAPBinding.Use_K;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Jws.Soap.SOAPBinding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStyle (This : access Typ)
                      return access Javax.Jws.Soap.SOAPBinding.Style.Typ'Class is abstract;

   function GetUse (This : access Typ)
                    return access Javax.Jws.Soap.SOAPBinding.Use_K.Typ'Class is abstract;

   function GetParameterStyle (This : access Typ)
                               return access Javax.Jws.Soap.SOAPBinding.ParameterStyle.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetStyle, "style");
   pragma Export (Java, GetUse, "use");
   pragma Export (Java, GetParameterStyle, "parameterStyle");

end Javax.Jws.Soap.SOAPBinding;
pragma Import (Java, Javax.Jws.Soap.SOAPBinding, "javax.jws.soap.SOAPBinding");
pragma Extensions_Allowed (Off);
