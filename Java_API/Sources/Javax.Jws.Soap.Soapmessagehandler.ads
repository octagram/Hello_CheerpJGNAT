pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Jws.Soap.InitParam;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Jws.Soap.SOAPMessageHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class is abstract;

   function ClassName (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function InitParams (This : access Typ)
                        return Standard.Java.Lang.Object.Ref is abstract;

   function Roles (This : access Typ)
                   return Standard.Java.Lang.Object.Ref is abstract;

   function Headers (This : access Typ)
                     return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Name, "name");
   pragma Export (Java, ClassName, "className");
   pragma Export (Java, InitParams, "initParams");
   pragma Export (Java, Roles, "roles");
   pragma Export (Java, Headers, "headers");

end Javax.Jws.Soap.SOAPMessageHandler;
pragma Import (Java, Javax.Jws.Soap.SOAPMessageHandler, "javax.jws.soap.SOAPMessageHandler");
pragma Extensions_Allowed (Off);
