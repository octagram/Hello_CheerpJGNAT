pragma Extensions_Allowed (On);
package Javax.Jws.Soap is
   pragma Preelaborate;
end Javax.Jws.Soap;
pragma Import (Java, Javax.Jws.Soap, "javax.jws.soap");
pragma Extensions_Allowed (Off);
