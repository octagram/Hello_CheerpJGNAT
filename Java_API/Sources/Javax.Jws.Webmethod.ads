pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Jws.WebMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function OperationName (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function Action (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   function Exclude (This : access Typ)
                     return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, OperationName, "operationName");
   pragma Export (Java, Action, "action");
   pragma Export (Java, Exclude, "exclude");

end Javax.Jws.WebMethod;
pragma Import (Java, Javax.Jws.WebMethod, "javax.jws.WebMethod");
pragma Extensions_Allowed (Off);
