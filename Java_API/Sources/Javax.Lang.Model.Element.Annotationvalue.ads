pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Lang.Model.Element.AnnotationValueVisitor;
with Java.Lang.Object;

package Javax.Lang.Model.Element.AnnotationValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function accept_K (This : access Typ;
                      P1_AnnotationValueVisitor : access Standard.Javax.Lang.Model.Element.AnnotationValueVisitor.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, accept_K, "accept");

end Javax.Lang.Model.Element.AnnotationValue;
pragma Import (Java, Javax.Lang.Model.Element.AnnotationValue, "javax.lang.model.element.AnnotationValue");
pragma Extensions_Allowed (Off);
