pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Lang.Model.Element.AnnotationMirror;
limited with Javax.Lang.Model.Element.AnnotationValue;
limited with Javax.Lang.Model.Element.VariableElement;
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Lang.Object;

package Javax.Lang.Model.Element.AnnotationValueVisitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Visit (This : access Typ;
                   P1_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function Visit (This : access Typ;
                   P1_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function VisitBoolean (This : access Typ;
                          P1_Boolean : Java.Boolean;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function VisitByte (This : access Typ;
                       P1_Byte : Java.Byte;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function VisitChar (This : access Typ;
                       P1_Char : Java.Char;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function VisitDouble (This : access Typ;
                         P1_Double : Java.Double;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function VisitFloat (This : access Typ;
                        P1_Float : Java.Float;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function VisitInt (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function VisitLong (This : access Typ;
                       P1_Long : Java.Long;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function VisitShort (This : access Typ;
                        P1_Short : Java.Short;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function VisitString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function VisitType (This : access Typ;
                       P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function VisitEnumConstant (This : access Typ;
                               P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Java.Lang.Object.Typ'Class is abstract;

   function VisitAnnotation (This : access Typ;
                             P1_AnnotationMirror : access Standard.Javax.Lang.Model.Element.AnnotationMirror.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function VisitArray (This : access Typ;
                        P1_List : access Standard.Java.Util.List.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function VisitUnknown (This : access Typ;
                          P1_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Visit, "visit");
   pragma Export (Java, VisitBoolean, "visitBoolean");
   pragma Export (Java, VisitByte, "visitByte");
   pragma Export (Java, VisitChar, "visitChar");
   pragma Export (Java, VisitDouble, "visitDouble");
   pragma Export (Java, VisitFloat, "visitFloat");
   pragma Export (Java, VisitInt, "visitInt");
   pragma Export (Java, VisitLong, "visitLong");
   pragma Export (Java, VisitShort, "visitShort");
   pragma Export (Java, VisitString, "visitString");
   pragma Export (Java, VisitType, "visitType");
   pragma Export (Java, VisitEnumConstant, "visitEnumConstant");
   pragma Export (Java, VisitAnnotation, "visitAnnotation");
   pragma Export (Java, VisitArray, "visitArray");
   pragma Export (Java, VisitUnknown, "visitUnknown");

end Javax.Lang.Model.Element.AnnotationValueVisitor;
pragma Import (Java, Javax.Lang.Model.Element.AnnotationValueVisitor, "javax.lang.model.element.AnnotationValueVisitor");
pragma Extensions_Allowed (Off);
