pragma Extensions_Allowed (On);
limited with Java.Lang.Annotation.Annotation;
limited with Java.Lang.Class;
limited with Java.Util.List;
limited with Java.Util.Set;
limited with Javax.Lang.Model.Element.ElementKind;
limited with Javax.Lang.Model.Element.ElementVisitor;
limited with Javax.Lang.Model.Element.Name;
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Lang.Object;

package Javax.Lang.Model.Element.Element is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AsType (This : access Typ)
                    return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class is abstract;

   function GetKind (This : access Typ)
                     return access Javax.Lang.Model.Element.ElementKind.Typ'Class is abstract;

   function GetAnnotationMirrors (This : access Typ)
                                  return access Java.Util.List.Typ'Class is abstract;

   function GetAnnotation (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Annotation.Annotation.Typ'Class is abstract;

   function GetModifiers (This : access Typ)
                          return access Java.Util.Set.Typ'Class is abstract;

   function GetSimpleName (This : access Typ)
                           return access Javax.Lang.Model.Element.Name.Typ'Class is abstract;

   function GetEnclosingElement (This : access Typ)
                                 return access Javax.Lang.Model.Element.Element.Typ'Class is abstract;

   function GetEnclosedElements (This : access Typ)
                                 return access Java.Util.List.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function accept_K (This : access Typ;
                      P1_ElementVisitor : access Standard.Javax.Lang.Model.Element.ElementVisitor.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AsType, "asType");
   pragma Export (Java, GetKind, "getKind");
   pragma Export (Java, GetAnnotationMirrors, "getAnnotationMirrors");
   pragma Export (Java, GetAnnotation, "getAnnotation");
   pragma Export (Java, GetModifiers, "getModifiers");
   pragma Export (Java, GetSimpleName, "getSimpleName");
   pragma Export (Java, GetEnclosingElement, "getEnclosingElement");
   pragma Export (Java, GetEnclosedElements, "getEnclosedElements");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, accept_K, "accept");

end Javax.Lang.Model.Element.Element;
pragma Import (Java, Javax.Lang.Model.Element.Element, "javax.lang.model.element.Element");
pragma Extensions_Allowed (Off);
