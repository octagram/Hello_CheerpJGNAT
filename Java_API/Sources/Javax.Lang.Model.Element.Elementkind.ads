pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Lang.Model.Element.ElementKind is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   function IsClass (This : access Typ)
                     return Java.Boolean;

   function IsInterface (This : access Typ)
                         return Java.Boolean;

   function IsField (This : access Typ)
                     return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PACKAGE_K : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   ENUM : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   CLASS : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   ANNOTATION_TYPE : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   INTERFACE_K : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   ENUM_CONSTANT : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   FIELD : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   PARAMETER : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   LOCAL_VARIABLE : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   EXCEPTION_PARAMETER : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   METHOD : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   CONSTRUCTOR : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   STATIC_INIT : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   INSTANCE_INIT : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   TYPE_PARAMETER : access Javax.Lang.Model.Element.ElementKind.Typ'Class;

   --  final
   OTHER : access Javax.Lang.Model.Element.ElementKind.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, IsClass, "isClass");
   pragma Import (Java, IsInterface, "isInterface");
   pragma Import (Java, IsField, "isField");
   pragma Import (Java, PACKAGE_K, "PACKAGE");
   pragma Import (Java, ENUM, "ENUM");
   pragma Import (Java, CLASS, "CLASS");
   pragma Import (Java, ANNOTATION_TYPE, "ANNOTATION_TYPE");
   pragma Import (Java, INTERFACE_K, "INTERFACE");
   pragma Import (Java, ENUM_CONSTANT, "ENUM_CONSTANT");
   pragma Import (Java, FIELD, "FIELD");
   pragma Import (Java, PARAMETER, "PARAMETER");
   pragma Import (Java, LOCAL_VARIABLE, "LOCAL_VARIABLE");
   pragma Import (Java, EXCEPTION_PARAMETER, "EXCEPTION_PARAMETER");
   pragma Import (Java, METHOD, "METHOD");
   pragma Import (Java, CONSTRUCTOR, "CONSTRUCTOR");
   pragma Import (Java, STATIC_INIT, "STATIC_INIT");
   pragma Import (Java, INSTANCE_INIT, "INSTANCE_INIT");
   pragma Import (Java, TYPE_PARAMETER, "TYPE_PARAMETER");
   pragma Import (Java, OTHER, "OTHER");

end Javax.Lang.Model.Element.ElementKind;
pragma Import (Java, Javax.Lang.Model.Element.ElementKind, "javax.lang.model.element.ElementKind");
pragma Extensions_Allowed (Off);
