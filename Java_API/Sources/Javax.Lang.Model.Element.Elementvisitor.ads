pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.Element.Element;
limited with Javax.Lang.Model.Element.ExecutableElement;
limited with Javax.Lang.Model.Element.PackageElement;
limited with Javax.Lang.Model.Element.TypeElement;
limited with Javax.Lang.Model.Element.TypeParameterElement;
limited with Javax.Lang.Model.Element.VariableElement;
with Java.Lang.Object;

package Javax.Lang.Model.Element.ElementVisitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Visit (This : access Typ;
                   P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function Visit (This : access Typ;
                   P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function VisitPackage (This : access Typ;
                          P1_PackageElement : access Standard.Javax.Lang.Model.Element.PackageElement.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function VisitType (This : access Typ;
                       P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function VisitVariable (This : access Typ;
                           P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class is abstract;

   function VisitExecutable (This : access Typ;
                             P1_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function VisitTypeParameter (This : access Typ;
                                P1_TypeParameterElement : access Standard.Javax.Lang.Model.Element.TypeParameterElement.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                return access Java.Lang.Object.Typ'Class is abstract;

   function VisitUnknown (This : access Typ;
                          P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Visit, "visit");
   pragma Export (Java, VisitPackage, "visitPackage");
   pragma Export (Java, VisitType, "visitType");
   pragma Export (Java, VisitVariable, "visitVariable");
   pragma Export (Java, VisitExecutable, "visitExecutable");
   pragma Export (Java, VisitTypeParameter, "visitTypeParameter");
   pragma Export (Java, VisitUnknown, "visitUnknown");

end Javax.Lang.Model.Element.ElementVisitor;
pragma Import (Java, Javax.Lang.Model.Element.ElementVisitor, "javax.lang.model.element.ElementVisitor");
pragma Extensions_Allowed (Off);
