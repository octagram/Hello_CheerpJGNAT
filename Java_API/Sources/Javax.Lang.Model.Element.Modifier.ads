pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Lang.Model.Element.Modifier is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Lang.Model.Element.Modifier.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PUBLIC : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   PROTECTED_K : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   PRIVATE_K : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   ABSTRACT_K : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   STATIC : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   FINAL : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   TRANSIENT : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   VOLATILE : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   SYNCHRONIZED_K : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   NATIVE : access Javax.Lang.Model.Element.Modifier.Typ'Class;

   --  final
   STRICTFP : access Javax.Lang.Model.Element.Modifier.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, PUBLIC, "PUBLIC");
   pragma Import (Java, PROTECTED_K, "PROTECTED");
   pragma Import (Java, PRIVATE_K, "PRIVATE");
   pragma Import (Java, ABSTRACT_K, "ABSTRACT");
   pragma Import (Java, STATIC, "STATIC");
   pragma Import (Java, FINAL, "FINAL");
   pragma Import (Java, TRANSIENT, "TRANSIENT");
   pragma Import (Java, VOLATILE, "VOLATILE");
   pragma Import (Java, SYNCHRONIZED_K, "SYNCHRONIZED");
   pragma Import (Java, NATIVE, "NATIVE");
   pragma Import (Java, STRICTFP, "STRICTFP");

end Javax.Lang.Model.Element.Modifier;
pragma Import (Java, Javax.Lang.Model.Element.Modifier, "javax.lang.model.element.Modifier");
pragma Extensions_Allowed (Off);
