pragma Extensions_Allowed (On);
with Java.Lang.CharSequence;
with Java.Lang.Object;

package Javax.Lang.Model.Element.Name is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CharSequence_I : Java.Lang.CharSequence.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ContentEquals (This : access Typ;
                           P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                           return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ContentEquals, "contentEquals");

end Javax.Lang.Model.Element.Name;
pragma Import (Java, Javax.Lang.Model.Element.Name, "javax.lang.model.element.Name");
pragma Extensions_Allowed (Off);
