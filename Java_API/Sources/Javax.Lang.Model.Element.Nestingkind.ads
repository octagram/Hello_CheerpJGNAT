pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Lang.Model.Element.NestingKind is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Lang.Model.Element.NestingKind.Typ'Class;

   function IsNested (This : access Typ)
                      return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TOP_LEVEL : access Javax.Lang.Model.Element.NestingKind.Typ'Class;

   --  final
   MEMBER : access Javax.Lang.Model.Element.NestingKind.Typ'Class;

   --  final
   LOCAL : access Javax.Lang.Model.Element.NestingKind.Typ'Class;

   --  final
   ANONYMOUS : access Javax.Lang.Model.Element.NestingKind.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, IsNested, "isNested");
   pragma Import (Java, TOP_LEVEL, "TOP_LEVEL");
   pragma Import (Java, MEMBER, "MEMBER");
   pragma Import (Java, LOCAL, "LOCAL");
   pragma Import (Java, ANONYMOUS, "ANONYMOUS");

end Javax.Lang.Model.Element.NestingKind;
pragma Import (Java, Javax.Lang.Model.Element.NestingKind, "javax.lang.model.element.NestingKind");
pragma Extensions_Allowed (Off);
