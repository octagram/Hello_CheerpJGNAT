pragma Extensions_Allowed (On);
limited with Java.Util.List;
limited with Javax.Lang.Model.Element.Name;
limited with Javax.Lang.Model.Element.NestingKind;
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Lang.Object;
with Javax.Lang.Model.Element.Element;

package Javax.Lang.Model.Element.TypeElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Element_I : Javax.Lang.Model.Element.Element.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNestingKind (This : access Typ)
                            return access Javax.Lang.Model.Element.NestingKind.Typ'Class is abstract;

   function GetQualifiedName (This : access Typ)
                              return access Javax.Lang.Model.Element.Name.Typ'Class is abstract;

   function GetSuperclass (This : access Typ)
                           return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class is abstract;

   function GetInterfaces (This : access Typ)
                           return access Java.Util.List.Typ'Class is abstract;

   function GetTypeParameters (This : access Typ)
                               return access Java.Util.List.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetNestingKind, "getNestingKind");
   pragma Export (Java, GetQualifiedName, "getQualifiedName");
   pragma Export (Java, GetSuperclass, "getSuperclass");
   pragma Export (Java, GetInterfaces, "getInterfaces");
   pragma Export (Java, GetTypeParameters, "getTypeParameters");

end Javax.Lang.Model.Element.TypeElement;
pragma Import (Java, Javax.Lang.Model.Element.TypeElement, "javax.lang.model.element.TypeElement");
pragma Extensions_Allowed (Off);
