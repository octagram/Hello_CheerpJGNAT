pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.Element.AnnotationValue;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Javax.Lang.Model.Element.UnknownAnnotationValueException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnknownAnnotationValueException (P1_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class;
                                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                                 This : Ref := null)
                                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUnknownAnnotationValue (This : access Typ)
                                       return access Javax.Lang.Model.Element.AnnotationValue.Typ'Class;

   function GetArgument (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.lang.model.element.UnknownAnnotationValueException");
   pragma Java_Constructor (New_UnknownAnnotationValueException);
   pragma Import (Java, GetUnknownAnnotationValue, "getUnknownAnnotationValue");
   pragma Import (Java, GetArgument, "getArgument");

end Javax.Lang.Model.Element.UnknownAnnotationValueException;
pragma Import (Java, Javax.Lang.Model.Element.UnknownAnnotationValueException, "javax.lang.model.element.UnknownAnnotationValueException");
pragma Extensions_Allowed (Off);
