pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.Element.Element;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Javax.Lang.Model.Element.UnknownElementException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnknownElementException (P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                                         P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUnknownElement (This : access Typ)
                               return access Javax.Lang.Model.Element.Element.Typ'Class;

   function GetArgument (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.lang.model.element.UnknownElementException");
   pragma Java_Constructor (New_UnknownElementException);
   pragma Import (Java, GetUnknownElement, "getUnknownElement");
   pragma Import (Java, GetArgument, "getArgument");

end Javax.Lang.Model.Element.UnknownElementException;
pragma Import (Java, Javax.Lang.Model.Element.UnknownElementException, "javax.lang.model.element.UnknownElementException");
pragma Extensions_Allowed (Off);
