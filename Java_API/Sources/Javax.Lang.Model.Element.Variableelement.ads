pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Lang.Model.Element.Element;

package Javax.Lang.Model.Element.VariableElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Element_I : Javax.Lang.Model.Element.Element.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetConstantValue (This : access Typ)
                              return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetConstantValue, "getConstantValue");

end Javax.Lang.Model.Element.VariableElement;
pragma Import (Java, Javax.Lang.Model.Element.VariableElement, "javax.lang.model.element.VariableElement");
pragma Extensions_Allowed (Off);
