pragma Extensions_Allowed (On);
package Javax.Lang.Model.Element is
   pragma Preelaborate;
end Javax.Lang.Model.Element;
pragma Import (Java, Javax.Lang.Model.Element, "javax.lang.model.element");
pragma Extensions_Allowed (Off);
