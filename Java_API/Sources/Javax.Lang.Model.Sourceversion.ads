pragma Extensions_Allowed (On);
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Lang.Model.SourceVersion is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Lang.Model.SourceVersion.Typ'Class;

   function Latest return access Javax.Lang.Model.SourceVersion.Typ'Class;

   function LatestSupported return access Javax.Lang.Model.SourceVersion.Typ'Class;

   function IsIdentifier (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                          return Java.Boolean;

   function IsName (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                    return Java.Boolean;

   function IsKeyword (P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                       return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RELEASE_0 : access Javax.Lang.Model.SourceVersion.Typ'Class;

   --  final
   RELEASE_1 : access Javax.Lang.Model.SourceVersion.Typ'Class;

   --  final
   RELEASE_2 : access Javax.Lang.Model.SourceVersion.Typ'Class;

   --  final
   RELEASE_3 : access Javax.Lang.Model.SourceVersion.Typ'Class;

   --  final
   RELEASE_4 : access Javax.Lang.Model.SourceVersion.Typ'Class;

   --  final
   RELEASE_5 : access Javax.Lang.Model.SourceVersion.Typ'Class;

   --  final
   RELEASE_6 : access Javax.Lang.Model.SourceVersion.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, Latest, "latest");
   pragma Import (Java, LatestSupported, "latestSupported");
   pragma Import (Java, IsIdentifier, "isIdentifier");
   pragma Import (Java, IsName, "isName");
   pragma Import (Java, IsKeyword, "isKeyword");
   pragma Import (Java, RELEASE_0, "RELEASE_0");
   pragma Import (Java, RELEASE_1, "RELEASE_1");
   pragma Import (Java, RELEASE_2, "RELEASE_2");
   pragma Import (Java, RELEASE_3, "RELEASE_3");
   pragma Import (Java, RELEASE_4, "RELEASE_4");
   pragma Import (Java, RELEASE_5, "RELEASE_5");
   pragma Import (Java, RELEASE_6, "RELEASE_6");

end Javax.Lang.Model.SourceVersion;
pragma Import (Java, Javax.Lang.Model.SourceVersion, "javax.lang.model.SourceVersion");
pragma Extensions_Allowed (Off);
