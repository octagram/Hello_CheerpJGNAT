pragma Extensions_Allowed (On);
limited with Java.Util.List;
limited with Javax.Lang.Model.Element.Element;
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Lang.Object;
with Javax.Lang.Model.type_K.ReferenceType;

package Javax.Lang.Model.type_K.DeclaredType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ReferenceType_I : Javax.Lang.Model.type_K.ReferenceType.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AsElement (This : access Typ)
                       return access Javax.Lang.Model.Element.Element.Typ'Class is abstract;

   function GetEnclosingType (This : access Typ)
                              return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class is abstract;

   function GetTypeArguments (This : access Typ)
                              return access Java.Util.List.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AsElement, "asElement");
   pragma Export (Java, GetEnclosingType, "getEnclosingType");
   pragma Export (Java, GetTypeArguments, "getTypeArguments");

end Javax.Lang.Model.type_K.DeclaredType;
pragma Import (Java, Javax.Lang.Model.type_K.DeclaredType, "javax.lang.model.type.DeclaredType");
pragma Extensions_Allowed (Off);
