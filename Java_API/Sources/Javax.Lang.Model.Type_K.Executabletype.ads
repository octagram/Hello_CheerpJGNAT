pragma Extensions_Allowed (On);
limited with Java.Util.List;
with Java.Lang.Object;
with Javax.Lang.Model.type_K.TypeMirror;

package Javax.Lang.Model.type_K.ExecutableType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            TypeMirror_I : Javax.Lang.Model.type_K.TypeMirror.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTypeVariables (This : access Typ)
                              return access Java.Util.List.Typ'Class is abstract;

   function GetReturnType (This : access Typ)
                           return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class is abstract;

   function GetParameterTypes (This : access Typ)
                               return access Java.Util.List.Typ'Class is abstract;

   function GetThrownTypes (This : access Typ)
                            return access Java.Util.List.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTypeVariables, "getTypeVariables");
   pragma Export (Java, GetReturnType, "getReturnType");
   pragma Export (Java, GetParameterTypes, "getParameterTypes");
   pragma Export (Java, GetThrownTypes, "getThrownTypes");

end Javax.Lang.Model.type_K.ExecutableType;
pragma Import (Java, Javax.Lang.Model.type_K.ExecutableType, "javax.lang.model.type.ExecutableType");
pragma Extensions_Allowed (Off);
