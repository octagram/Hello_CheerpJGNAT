pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Javax.Lang.Model.type_K.MirroredTypeException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MirroredTypeException (P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTypeMirror (This : access Typ)
                           return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.lang.model.type.MirroredTypeException");
   pragma Java_Constructor (New_MirroredTypeException);
   pragma Import (Java, GetTypeMirror, "getTypeMirror");

end Javax.Lang.Model.type_K.MirroredTypeException;
pragma Import (Java, Javax.Lang.Model.type_K.MirroredTypeException, "javax.lang.model.type.MirroredTypeException");
pragma Extensions_Allowed (Off);
