pragma Extensions_Allowed (On);
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Javax.Lang.Model.type_K.MirroredTypesException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MirroredTypesException (P1_List : access Standard.Java.Util.List.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTypeMirrors (This : access Typ)
                            return access Java.Util.List.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.lang.model.type.MirroredTypesException");
   pragma Java_Constructor (New_MirroredTypesException);
   pragma Import (Java, GetTypeMirrors, "getTypeMirrors");

end Javax.Lang.Model.type_K.MirroredTypesException;
pragma Import (Java, Javax.Lang.Model.type_K.MirroredTypesException, "javax.lang.model.type.MirroredTypesException");
pragma Extensions_Allowed (Off);
