pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Lang.Model.type_K.TypeMirror;

package Javax.Lang.Model.type_K.ReferenceType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            TypeMirror_I : Javax.Lang.Model.type_K.TypeMirror.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Javax.Lang.Model.type_K.ReferenceType;
pragma Import (Java, Javax.Lang.Model.type_K.ReferenceType, "javax.lang.model.type.ReferenceType");
pragma Extensions_Allowed (Off);
