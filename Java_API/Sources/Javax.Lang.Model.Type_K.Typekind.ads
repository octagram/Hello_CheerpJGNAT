pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Lang.Model.type_K.TypeKind is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   function IsPrimitive (This : access Typ)
                         return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BOOLEAN : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   BYTE : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   SHORT : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   INT : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   LONG : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   CHAR : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   FLOAT : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   DOUBLE : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   VOID : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   NONE : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   NULL_K : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   ARRAY_K : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   DECLARED : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   ERROR : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   TYPEVAR : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   WILDCARD : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   PACKAGE_K : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   EXECUTABLE : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;

   --  final
   OTHER : access Javax.Lang.Model.type_K.TypeKind.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, IsPrimitive, "isPrimitive");
   pragma Import (Java, BOOLEAN, "BOOLEAN");
   pragma Import (Java, BYTE, "BYTE");
   pragma Import (Java, SHORT, "SHORT");
   pragma Import (Java, INT, "INT");
   pragma Import (Java, LONG, "LONG");
   pragma Import (Java, CHAR, "CHAR");
   pragma Import (Java, FLOAT, "FLOAT");
   pragma Import (Java, DOUBLE, "DOUBLE");
   pragma Import (Java, VOID, "VOID");
   pragma Import (Java, NONE, "NONE");
   pragma Import (Java, NULL_K, "NULL");
   pragma Import (Java, ARRAY_K, "ARRAY");
   pragma Import (Java, DECLARED, "DECLARED");
   pragma Import (Java, ERROR, "ERROR");
   pragma Import (Java, TYPEVAR, "TYPEVAR");
   pragma Import (Java, WILDCARD, "WILDCARD");
   pragma Import (Java, PACKAGE_K, "PACKAGE");
   pragma Import (Java, EXECUTABLE, "EXECUTABLE");
   pragma Import (Java, OTHER, "OTHER");

end Javax.Lang.Model.type_K.TypeKind;
pragma Import (Java, Javax.Lang.Model.type_K.TypeKind, "javax.lang.model.type.TypeKind");
pragma Extensions_Allowed (Off);
