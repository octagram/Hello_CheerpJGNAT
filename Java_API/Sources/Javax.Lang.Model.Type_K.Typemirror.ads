pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Lang.Model.type_K.TypeKind;
limited with Javax.Lang.Model.type_K.TypeVisitor;
with Java.Lang.Object;

package Javax.Lang.Model.type_K.TypeMirror is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKind (This : access Typ)
                     return access Javax.Lang.Model.type_K.TypeKind.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function accept_K (This : access Typ;
                      P1_TypeVisitor : access Standard.Javax.Lang.Model.type_K.TypeVisitor.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetKind, "getKind");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, accept_K, "accept");

end Javax.Lang.Model.type_K.TypeMirror;
pragma Import (Java, Javax.Lang.Model.type_K.TypeMirror, "javax.lang.model.type.TypeMirror");
pragma Extensions_Allowed (Off);
