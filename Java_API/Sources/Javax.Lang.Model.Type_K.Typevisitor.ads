pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.type_K.ArrayType;
limited with Javax.Lang.Model.type_K.DeclaredType;
limited with Javax.Lang.Model.type_K.ErrorType;
limited with Javax.Lang.Model.type_K.ExecutableType;
limited with Javax.Lang.Model.type_K.NoType;
limited with Javax.Lang.Model.type_K.NullType;
limited with Javax.Lang.Model.type_K.PrimitiveType;
limited with Javax.Lang.Model.type_K.TypeMirror;
limited with Javax.Lang.Model.type_K.TypeVariable;
limited with Javax.Lang.Model.type_K.WildcardType;
with Java.Lang.Object;

package Javax.Lang.Model.type_K.TypeVisitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Visit (This : access Typ;
                   P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function Visit (This : access Typ;
                   P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function VisitPrimitive (This : access Typ;
                            P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return access Java.Lang.Object.Typ'Class is abstract;

   function VisitNull (This : access Typ;
                       P1_NullType : access Standard.Javax.Lang.Model.type_K.NullType.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function VisitArray (This : access Typ;
                        P1_ArrayType : access Standard.Javax.Lang.Model.type_K.ArrayType.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function VisitDeclared (This : access Typ;
                           P1_DeclaredType : access Standard.Javax.Lang.Model.type_K.DeclaredType.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class is abstract;

   function VisitError (This : access Typ;
                        P1_ErrorType : access Standard.Javax.Lang.Model.type_K.ErrorType.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function VisitTypeVariable (This : access Typ;
                               P1_TypeVariable : access Standard.Javax.Lang.Model.type_K.TypeVariable.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Java.Lang.Object.Typ'Class is abstract;

   function VisitWildcard (This : access Typ;
                           P1_WildcardType : access Standard.Javax.Lang.Model.type_K.WildcardType.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class is abstract;

   function VisitExecutable (This : access Typ;
                             P1_ExecutableType : access Standard.Javax.Lang.Model.type_K.ExecutableType.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function VisitNoType (This : access Typ;
                         P1_NoType : access Standard.Javax.Lang.Model.type_K.NoType.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function VisitUnknown (This : access Typ;
                          P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Visit, "visit");
   pragma Export (Java, VisitPrimitive, "visitPrimitive");
   pragma Export (Java, VisitNull, "visitNull");
   pragma Export (Java, VisitArray, "visitArray");
   pragma Export (Java, VisitDeclared, "visitDeclared");
   pragma Export (Java, VisitError, "visitError");
   pragma Export (Java, VisitTypeVariable, "visitTypeVariable");
   pragma Export (Java, VisitWildcard, "visitWildcard");
   pragma Export (Java, VisitExecutable, "visitExecutable");
   pragma Export (Java, VisitNoType, "visitNoType");
   pragma Export (Java, VisitUnknown, "visitUnknown");

end Javax.Lang.Model.type_K.TypeVisitor;
pragma Import (Java, Javax.Lang.Model.type_K.TypeVisitor, "javax.lang.model.type.TypeVisitor");
pragma Extensions_Allowed (Off);
