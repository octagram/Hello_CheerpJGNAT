pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Javax.Lang.Model.type_K.UnknownTypeException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnknownTypeException (P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                                      P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUnknownType (This : access Typ)
                            return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class;

   function GetArgument (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.lang.model.type.UnknownTypeException");
   pragma Java_Constructor (New_UnknownTypeException);
   pragma Import (Java, GetUnknownType, "getUnknownType");
   pragma Import (Java, GetArgument, "getArgument");

end Javax.Lang.Model.type_K.UnknownTypeException;
pragma Import (Java, Javax.Lang.Model.type_K.UnknownTypeException, "javax.lang.model.type.UnknownTypeException");
pragma Extensions_Allowed (Off);
