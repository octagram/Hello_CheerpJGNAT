pragma Extensions_Allowed (On);
package Javax.Lang.Model.type_K is
   pragma Preelaborate;
end Javax.Lang.Model.type_K;
pragma Import (Java, Javax.Lang.Model.type_K, "javax.lang.model.type");
pragma Extensions_Allowed (Off);
