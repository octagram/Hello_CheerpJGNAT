pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.Element.AnnotationValue;
with Java.Lang.Object;
with Javax.Lang.Model.Element.AnnotationValueVisitor;

package Javax.Lang.Model.Util.AbstractAnnotationValueVisitor6 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AnnotationValueVisitor_I : Javax.Lang.Model.Element.AnnotationValueVisitor.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_AbstractAnnotationValueVisitor6 (This : Ref := null)
                                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Visit (This : access Typ;
                   P1_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   --  final
   function Visit (This : access Typ;
                   P1_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   function VisitUnknown (This : access Typ;
                          P1_AnnotationValue : access Standard.Javax.Lang.Model.Element.AnnotationValue.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractAnnotationValueVisitor6);
   pragma Import (Java, Visit, "visit");
   pragma Import (Java, VisitUnknown, "visitUnknown");

end Javax.Lang.Model.Util.AbstractAnnotationValueVisitor6;
pragma Import (Java, Javax.Lang.Model.Util.AbstractAnnotationValueVisitor6, "javax.lang.model.util.AbstractAnnotationValueVisitor6");
pragma Extensions_Allowed (Off);
