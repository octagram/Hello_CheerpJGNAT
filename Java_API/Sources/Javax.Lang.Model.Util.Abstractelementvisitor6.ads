pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.Element.Element;
with Java.Lang.Object;
with Javax.Lang.Model.Element.ElementVisitor;

package Javax.Lang.Model.Util.AbstractElementVisitor6 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ElementVisitor_I : Javax.Lang.Model.Element.ElementVisitor.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_AbstractElementVisitor6 (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Visit (This : access Typ;
                   P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   --  final
   function Visit (This : access Typ;
                   P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   function VisitUnknown (This : access Typ;
                          P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractElementVisitor6);
   pragma Import (Java, Visit, "visit");
   pragma Import (Java, VisitUnknown, "visitUnknown");

end Javax.Lang.Model.Util.AbstractElementVisitor6;
pragma Import (Java, Javax.Lang.Model.Util.AbstractElementVisitor6, "javax.lang.model.util.AbstractElementVisitor6");
pragma Extensions_Allowed (Off);
