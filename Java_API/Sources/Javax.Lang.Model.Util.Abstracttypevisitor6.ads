pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Lang.Object;
with Javax.Lang.Model.type_K.TypeVisitor;

package Javax.Lang.Model.Util.AbstractTypeVisitor6 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TypeVisitor_I : Javax.Lang.Model.type_K.TypeVisitor.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_AbstractTypeVisitor6 (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Visit (This : access Typ;
                   P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   --  final
   function Visit (This : access Typ;
                   P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                   return access Java.Lang.Object.Typ'Class;

   function VisitUnknown (This : access Typ;
                          P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractTypeVisitor6);
   pragma Import (Java, Visit, "visit");
   pragma Import (Java, VisitUnknown, "visitUnknown");

end Javax.Lang.Model.Util.AbstractTypeVisitor6;
pragma Import (Java, Javax.Lang.Model.Util.AbstractTypeVisitor6, "javax.lang.model.util.AbstractTypeVisitor6");
pragma Extensions_Allowed (Off);
