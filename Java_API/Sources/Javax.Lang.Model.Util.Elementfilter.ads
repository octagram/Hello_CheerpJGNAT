pragma Extensions_Allowed (On);
limited with Java.Lang.Iterable;
limited with Java.Util.List;
limited with Java.Util.Set;
with Java.Lang.Object;

package Javax.Lang.Model.Util.ElementFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function FieldsIn (P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                      return access Java.Util.List.Typ'Class;

   function FieldsIn (P1_Set : access Standard.Java.Util.Set.Typ'Class)
                      return access Java.Util.Set.Typ'Class;

   function ConstructorsIn (P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                            return access Java.Util.List.Typ'Class;

   function ConstructorsIn (P1_Set : access Standard.Java.Util.Set.Typ'Class)
                            return access Java.Util.Set.Typ'Class;

   function MethodsIn (P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                       return access Java.Util.List.Typ'Class;

   function MethodsIn (P1_Set : access Standard.Java.Util.Set.Typ'Class)
                       return access Java.Util.Set.Typ'Class;

   function TypesIn (P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                     return access Java.Util.List.Typ'Class;

   function TypesIn (P1_Set : access Standard.Java.Util.Set.Typ'Class)
                     return access Java.Util.Set.Typ'Class;

   function PackagesIn (P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                        return access Java.Util.List.Typ'Class;

   function PackagesIn (P1_Set : access Standard.Java.Util.Set.Typ'Class)
                        return access Java.Util.Set.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, FieldsIn, "fieldsIn");
   pragma Import (Java, ConstructorsIn, "constructorsIn");
   pragma Import (Java, MethodsIn, "methodsIn");
   pragma Import (Java, TypesIn, "typesIn");
   pragma Import (Java, PackagesIn, "packagesIn");

end Javax.Lang.Model.Util.ElementFilter;
pragma Import (Java, Javax.Lang.Model.Util.ElementFilter, "javax.lang.model.util.ElementFilter");
pragma Extensions_Allowed (Off);
