pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.Element.ExecutableElement;
limited with Javax.Lang.Model.Element.PackageElement;
limited with Javax.Lang.Model.Element.TypeElement;
limited with Javax.Lang.Model.Element.TypeParameterElement;
limited with Javax.Lang.Model.Element.VariableElement;
with Java.Lang.Object;
with Javax.Lang.Model.Element.ElementVisitor;
with Javax.Lang.Model.Util.SimpleElementVisitor6;

package Javax.Lang.Model.Util.ElementKindVisitor6 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ElementVisitor_I : Javax.Lang.Model.Element.ElementVisitor.Ref)
    is new Javax.Lang.Model.Util.SimpleElementVisitor6.Typ(ElementVisitor_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ElementKindVisitor6 (This : Ref := null)
                                     return Ref;

   --  protected
   function New_ElementKindVisitor6 (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function VisitPackage (This : access Typ;
                          P1_PackageElement : access Standard.Javax.Lang.Model.Element.PackageElement.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function VisitType (This : access Typ;
                       P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function VisitTypeAsAnnotationType (This : access Typ;
                                       P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                       return access Java.Lang.Object.Typ'Class;

   function VisitTypeAsClass (This : access Typ;
                              P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                              return access Java.Lang.Object.Typ'Class;

   function VisitTypeAsEnum (This : access Typ;
                             P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function VisitTypeAsInterface (This : access Typ;
                                  P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Lang.Object.Typ'Class;

   function VisitVariable (This : access Typ;
                           P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   function VisitVariableAsEnumConstant (This : access Typ;
                                         P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                         return access Java.Lang.Object.Typ'Class;

   function VisitVariableAsExceptionParameter (This : access Typ;
                                               P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                               return access Java.Lang.Object.Typ'Class;

   function VisitVariableAsField (This : access Typ;
                                  P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Lang.Object.Typ'Class;

   function VisitVariableAsLocalVariable (This : access Typ;
                                          P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                          return access Java.Lang.Object.Typ'Class;

   function VisitVariableAsParameter (This : access Typ;
                                      P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                      return access Java.Lang.Object.Typ'Class;

   function VisitExecutable (This : access Typ;
                             P1_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function VisitExecutableAsConstructor (This : access Typ;
                                          P1_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                          return access Java.Lang.Object.Typ'Class;

   function VisitExecutableAsInstanceInit (This : access Typ;
                                           P1_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                           return access Java.Lang.Object.Typ'Class;

   function VisitExecutableAsMethod (This : access Typ;
                                     P1_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                     return access Java.Lang.Object.Typ'Class;

   function VisitExecutableAsStaticInit (This : access Typ;
                                         P1_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                         return access Java.Lang.Object.Typ'Class;

   function VisitTypeParameter (This : access Typ;
                                P1_TypeParameterElement : access Standard.Javax.Lang.Model.Element.TypeParameterElement.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ElementKindVisitor6);
   pragma Import (Java, VisitPackage, "visitPackage");
   pragma Import (Java, VisitType, "visitType");
   pragma Import (Java, VisitTypeAsAnnotationType, "visitTypeAsAnnotationType");
   pragma Import (Java, VisitTypeAsClass, "visitTypeAsClass");
   pragma Import (Java, VisitTypeAsEnum, "visitTypeAsEnum");
   pragma Import (Java, VisitTypeAsInterface, "visitTypeAsInterface");
   pragma Import (Java, VisitVariable, "visitVariable");
   pragma Import (Java, VisitVariableAsEnumConstant, "visitVariableAsEnumConstant");
   pragma Import (Java, VisitVariableAsExceptionParameter, "visitVariableAsExceptionParameter");
   pragma Import (Java, VisitVariableAsField, "visitVariableAsField");
   pragma Import (Java, VisitVariableAsLocalVariable, "visitVariableAsLocalVariable");
   pragma Import (Java, VisitVariableAsParameter, "visitVariableAsParameter");
   pragma Import (Java, VisitExecutable, "visitExecutable");
   pragma Import (Java, VisitExecutableAsConstructor, "visitExecutableAsConstructor");
   pragma Import (Java, VisitExecutableAsInstanceInit, "visitExecutableAsInstanceInit");
   pragma Import (Java, VisitExecutableAsMethod, "visitExecutableAsMethod");
   pragma Import (Java, VisitExecutableAsStaticInit, "visitExecutableAsStaticInit");
   pragma Import (Java, VisitTypeParameter, "visitTypeParameter");

end Javax.Lang.Model.Util.ElementKindVisitor6;
pragma Import (Java, Javax.Lang.Model.Util.ElementKindVisitor6, "javax.lang.model.util.ElementKindVisitor6");
pragma Extensions_Allowed (Off);
