pragma Extensions_Allowed (On);
limited with Java.Io.Writer;
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
limited with Javax.Lang.Model.Element.AnnotationMirror;
limited with Javax.Lang.Model.Element.Element;
limited with Javax.Lang.Model.Element.ExecutableElement;
limited with Javax.Lang.Model.Element.Name;
limited with Javax.Lang.Model.Element.PackageElement;
limited with Javax.Lang.Model.Element.TypeElement;
with Java.Lang.Object;

package Javax.Lang.Model.Util.Elements is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPackageElement (This : access Typ;
                               P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                               return access Javax.Lang.Model.Element.PackageElement.Typ'Class is abstract;

   function GetTypeElement (This : access Typ;
                            P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                            return access Javax.Lang.Model.Element.TypeElement.Typ'Class is abstract;

   function GetElementValuesWithDefaults (This : access Typ;
                                          P1_AnnotationMirror : access Standard.Javax.Lang.Model.Element.AnnotationMirror.Typ'Class)
                                          return access Java.Util.Map.Typ'Class is abstract;

   function GetDocComment (This : access Typ;
                           P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                           return access Java.Lang.String.Typ'Class is abstract;

   function IsDeprecated (This : access Typ;
                          P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                          return Java.Boolean is abstract;

   function GetBinaryName (This : access Typ;
                           P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class)
                           return access Javax.Lang.Model.Element.Name.Typ'Class is abstract;

   function GetPackageOf (This : access Typ;
                          P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                          return access Javax.Lang.Model.Element.PackageElement.Typ'Class is abstract;

   function GetAllMembers (This : access Typ;
                           P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class)
                           return access Java.Util.List.Typ'Class is abstract;

   function GetAllAnnotationMirrors (This : access Typ;
                                     P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                                     return access Java.Util.List.Typ'Class is abstract;

   function Hides (This : access Typ;
                   P1_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class;
                   P2_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                   return Java.Boolean is abstract;

   function Overrides (This : access Typ;
                       P1_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                       P2_ExecutableElement : access Standard.Javax.Lang.Model.Element.ExecutableElement.Typ'Class;
                       P3_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class)
                       return Java.Boolean is abstract;

   function GetConstantExpression (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return access Java.Lang.String.Typ'Class is abstract;

   procedure PrintElements (This : access Typ;
                            P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                            P2_Element_Arr : access Javax.Lang.Model.Element.Element.Arr_Obj) is abstract;

   function GetName (This : access Typ;
                     P1_CharSequence : access Standard.Java.Lang.CharSequence.Typ'Class)
                     return access Javax.Lang.Model.Element.Name.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPackageElement, "getPackageElement");
   pragma Export (Java, GetTypeElement, "getTypeElement");
   pragma Export (Java, GetElementValuesWithDefaults, "getElementValuesWithDefaults");
   pragma Export (Java, GetDocComment, "getDocComment");
   pragma Export (Java, IsDeprecated, "isDeprecated");
   pragma Export (Java, GetBinaryName, "getBinaryName");
   pragma Export (Java, GetPackageOf, "getPackageOf");
   pragma Export (Java, GetAllMembers, "getAllMembers");
   pragma Export (Java, GetAllAnnotationMirrors, "getAllAnnotationMirrors");
   pragma Export (Java, Hides, "hides");
   pragma Export (Java, Overrides, "overrides");
   pragma Export (Java, GetConstantExpression, "getConstantExpression");
   pragma Export (Java, PrintElements, "printElements");
   pragma Export (Java, GetName, "getName");

end Javax.Lang.Model.Util.Elements;
pragma Import (Java, Javax.Lang.Model.Util.Elements, "javax.lang.model.util.Elements");
pragma Extensions_Allowed (Off);
