pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Lang.Model.Element.AnnotationMirror;
limited with Javax.Lang.Model.Element.VariableElement;
limited with Javax.Lang.Model.type_K.TypeMirror;
with Java.Lang.Object;
with Javax.Lang.Model.Element.AnnotationValueVisitor;
with Javax.Lang.Model.Util.AbstractAnnotationValueVisitor6;

package Javax.Lang.Model.Util.SimpleAnnotationValueVisitor6 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AnnotationValueVisitor_I : Javax.Lang.Model.Element.AnnotationValueVisitor.Ref)
    is new Javax.Lang.Model.Util.AbstractAnnotationValueVisitor6.Typ(AnnotationValueVisitor_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      DEFAULT_VALUE : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, DEFAULT_VALUE, "DEFAULT_VALUE");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_SimpleAnnotationValueVisitor6 (This : Ref := null)
                                               return Ref;

   --  protected
   function New_SimpleAnnotationValueVisitor6 (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function DefaultAction (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   function VisitBoolean (This : access Typ;
                          P1_Boolean : Java.Boolean;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function VisitByte (This : access Typ;
                       P1_Byte : Java.Byte;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function VisitChar (This : access Typ;
                       P1_Char : Java.Char;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function VisitDouble (This : access Typ;
                         P1_Double : Java.Double;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function VisitFloat (This : access Typ;
                        P1_Float : Java.Float;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function VisitInt (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function VisitLong (This : access Typ;
                       P1_Long : Java.Long;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function VisitShort (This : access Typ;
                        P1_Short : Java.Short;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function VisitString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function VisitType (This : access Typ;
                       P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function VisitEnumConstant (This : access Typ;
                               P1_VariableElement : access Standard.Javax.Lang.Model.Element.VariableElement.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Java.Lang.Object.Typ'Class;

   function VisitAnnotation (This : access Typ;
                             P1_AnnotationMirror : access Standard.Javax.Lang.Model.Element.AnnotationMirror.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function VisitArray (This : access Typ;
                        P1_List : access Standard.Java.Util.List.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleAnnotationValueVisitor6);
   pragma Import (Java, DefaultAction, "defaultAction");
   pragma Import (Java, VisitBoolean, "visitBoolean");
   pragma Import (Java, VisitByte, "visitByte");
   pragma Import (Java, VisitChar, "visitChar");
   pragma Import (Java, VisitDouble, "visitDouble");
   pragma Import (Java, VisitFloat, "visitFloat");
   pragma Import (Java, VisitInt, "visitInt");
   pragma Import (Java, VisitLong, "visitLong");
   pragma Import (Java, VisitShort, "visitShort");
   pragma Import (Java, VisitString, "visitString");
   pragma Import (Java, VisitType, "visitType");
   pragma Import (Java, VisitEnumConstant, "visitEnumConstant");
   pragma Import (Java, VisitAnnotation, "visitAnnotation");
   pragma Import (Java, VisitArray, "visitArray");

end Javax.Lang.Model.Util.SimpleAnnotationValueVisitor6;
pragma Import (Java, Javax.Lang.Model.Util.SimpleAnnotationValueVisitor6, "javax.lang.model.util.SimpleAnnotationValueVisitor6");
pragma Extensions_Allowed (Off);
