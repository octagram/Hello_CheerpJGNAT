pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.type_K.ArrayType;
limited with Javax.Lang.Model.type_K.DeclaredType;
limited with Javax.Lang.Model.type_K.ErrorType;
limited with Javax.Lang.Model.type_K.ExecutableType;
limited with Javax.Lang.Model.type_K.NoType;
limited with Javax.Lang.Model.type_K.NullType;
limited with Javax.Lang.Model.type_K.PrimitiveType;
limited with Javax.Lang.Model.type_K.TypeMirror;
limited with Javax.Lang.Model.type_K.TypeVariable;
limited with Javax.Lang.Model.type_K.WildcardType;
with Java.Lang.Object;
with Javax.Lang.Model.Util.AbstractTypeVisitor6;
with Javax.Lang.Model.type_K.TypeVisitor;

package Javax.Lang.Model.Util.SimpleTypeVisitor6 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TypeVisitor_I : Javax.Lang.Model.type_K.TypeVisitor.Ref)
    is new Javax.Lang.Model.Util.AbstractTypeVisitor6.Typ(TypeVisitor_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      DEFAULT_VALUE : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, DEFAULT_VALUE, "DEFAULT_VALUE");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_SimpleTypeVisitor6 (This : Ref := null)
                                    return Ref;

   --  protected
   function New_SimpleTypeVisitor6 (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function DefaultAction (This : access Typ;
                           P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   function VisitPrimitive (This : access Typ;
                            P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return access Java.Lang.Object.Typ'Class;

   function VisitNull (This : access Typ;
                       P1_NullType : access Standard.Javax.Lang.Model.type_K.NullType.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function VisitArray (This : access Typ;
                        P1_ArrayType : access Standard.Javax.Lang.Model.type_K.ArrayType.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function VisitDeclared (This : access Typ;
                           P1_DeclaredType : access Standard.Javax.Lang.Model.type_K.DeclaredType.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   function VisitError (This : access Typ;
                        P1_ErrorType : access Standard.Javax.Lang.Model.type_K.ErrorType.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function VisitTypeVariable (This : access Typ;
                               P1_TypeVariable : access Standard.Javax.Lang.Model.type_K.TypeVariable.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Java.Lang.Object.Typ'Class;

   function VisitWildcard (This : access Typ;
                           P1_WildcardType : access Standard.Javax.Lang.Model.type_K.WildcardType.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   function VisitExecutable (This : access Typ;
                             P1_ExecutableType : access Standard.Javax.Lang.Model.type_K.ExecutableType.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;

   function VisitNoType (This : access Typ;
                         P1_NoType : access Standard.Javax.Lang.Model.type_K.NoType.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleTypeVisitor6);
   pragma Import (Java, DefaultAction, "defaultAction");
   pragma Import (Java, VisitPrimitive, "visitPrimitive");
   pragma Import (Java, VisitNull, "visitNull");
   pragma Import (Java, VisitArray, "visitArray");
   pragma Import (Java, VisitDeclared, "visitDeclared");
   pragma Import (Java, VisitError, "visitError");
   pragma Import (Java, VisitTypeVariable, "visitTypeVariable");
   pragma Import (Java, VisitWildcard, "visitWildcard");
   pragma Import (Java, VisitExecutable, "visitExecutable");
   pragma Import (Java, VisitNoType, "visitNoType");

end Javax.Lang.Model.Util.SimpleTypeVisitor6;
pragma Import (Java, Javax.Lang.Model.Util.SimpleTypeVisitor6, "javax.lang.model.util.SimpleTypeVisitor6");
pragma Extensions_Allowed (Off);
