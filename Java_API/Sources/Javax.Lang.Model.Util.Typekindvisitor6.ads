pragma Extensions_Allowed (On);
limited with Javax.Lang.Model.type_K.NoType;
limited with Javax.Lang.Model.type_K.PrimitiveType;
with Java.Lang.Object;
with Javax.Lang.Model.Util.SimpleTypeVisitor6;
with Javax.Lang.Model.type_K.TypeVisitor;

package Javax.Lang.Model.Util.TypeKindVisitor6 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TypeVisitor_I : Javax.Lang.Model.type_K.TypeVisitor.Ref)
    is new Javax.Lang.Model.Util.SimpleTypeVisitor6.Typ(TypeVisitor_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_TypeKindVisitor6 (This : Ref := null)
                                  return Ref;

   --  protected
   function New_TypeKindVisitor6 (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function VisitPrimitive (This : access Typ;
                            P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsBoolean (This : access Typ;
                                     P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                     return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsByte (This : access Typ;
                                  P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsShort (This : access Typ;
                                   P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsInt (This : access Typ;
                                 P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                 return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsLong (This : access Typ;
                                  P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsChar (This : access Typ;
                                  P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsFloat (This : access Typ;
                                   P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                   P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class;

   function VisitPrimitiveAsDouble (This : access Typ;
                                    P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class;
                                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                    return access Java.Lang.Object.Typ'Class;

   function VisitNoType (This : access Typ;
                         P1_NoType : access Standard.Javax.Lang.Model.type_K.NoType.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function VisitNoTypeAsVoid (This : access Typ;
                               P1_NoType : access Standard.Javax.Lang.Model.type_K.NoType.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Java.Lang.Object.Typ'Class;

   function VisitNoTypeAsPackage (This : access Typ;
                                  P1_NoType : access Standard.Javax.Lang.Model.type_K.NoType.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                  return access Java.Lang.Object.Typ'Class;

   function VisitNoTypeAsNone (This : access Typ;
                               P1_NoType : access Standard.Javax.Lang.Model.type_K.NoType.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TypeKindVisitor6);
   pragma Import (Java, VisitPrimitive, "visitPrimitive");
   pragma Import (Java, VisitPrimitiveAsBoolean, "visitPrimitiveAsBoolean");
   pragma Import (Java, VisitPrimitiveAsByte, "visitPrimitiveAsByte");
   pragma Import (Java, VisitPrimitiveAsShort, "visitPrimitiveAsShort");
   pragma Import (Java, VisitPrimitiveAsInt, "visitPrimitiveAsInt");
   pragma Import (Java, VisitPrimitiveAsLong, "visitPrimitiveAsLong");
   pragma Import (Java, VisitPrimitiveAsChar, "visitPrimitiveAsChar");
   pragma Import (Java, VisitPrimitiveAsFloat, "visitPrimitiveAsFloat");
   pragma Import (Java, VisitPrimitiveAsDouble, "visitPrimitiveAsDouble");
   pragma Import (Java, VisitNoType, "visitNoType");
   pragma Import (Java, VisitNoTypeAsVoid, "visitNoTypeAsVoid");
   pragma Import (Java, VisitNoTypeAsPackage, "visitNoTypeAsPackage");
   pragma Import (Java, VisitNoTypeAsNone, "visitNoTypeAsNone");

end Javax.Lang.Model.Util.TypeKindVisitor6;
pragma Import (Java, Javax.Lang.Model.Util.TypeKindVisitor6, "javax.lang.model.util.TypeKindVisitor6");
pragma Extensions_Allowed (Off);
