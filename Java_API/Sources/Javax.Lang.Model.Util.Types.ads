pragma Extensions_Allowed (On);
limited with Java.Util.List;
limited with Javax.Lang.Model.Element.Element;
limited with Javax.Lang.Model.Element.TypeElement;
limited with Javax.Lang.Model.type_K.ArrayType;
limited with Javax.Lang.Model.type_K.DeclaredType;
limited with Javax.Lang.Model.type_K.ExecutableType;
limited with Javax.Lang.Model.type_K.NoType;
limited with Javax.Lang.Model.type_K.NullType;
limited with Javax.Lang.Model.type_K.PrimitiveType;
limited with Javax.Lang.Model.type_K.TypeKind;
limited with Javax.Lang.Model.type_K.TypeMirror;
limited with Javax.Lang.Model.type_K.WildcardType;
with Java.Lang.Object;

package Javax.Lang.Model.Util.Types is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AsElement (This : access Typ;
                       P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                       return access Javax.Lang.Model.Element.Element.Typ'Class is abstract;

   function IsSameType (This : access Typ;
                        P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                        P2_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                        return Java.Boolean is abstract;

   function IsSubtype (This : access Typ;
                       P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                       P2_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                       return Java.Boolean is abstract;

   function IsAssignable (This : access Typ;
                          P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                          P2_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                          return Java.Boolean is abstract;

   function Contains (This : access Typ;
                      P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                      P2_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                      return Java.Boolean is abstract;

   function IsSubsignature (This : access Typ;
                            P1_ExecutableType : access Standard.Javax.Lang.Model.type_K.ExecutableType.Typ'Class;
                            P2_ExecutableType : access Standard.Javax.Lang.Model.type_K.ExecutableType.Typ'Class)
                            return Java.Boolean is abstract;

   function DirectSupertypes (This : access Typ;
                              P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                              return access Java.Util.List.Typ'Class is abstract;

   function Erasure (This : access Typ;
                     P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                     return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class is abstract;

   function BoxedClass (This : access Typ;
                        P1_PrimitiveType : access Standard.Javax.Lang.Model.type_K.PrimitiveType.Typ'Class)
                        return access Javax.Lang.Model.Element.TypeElement.Typ'Class is abstract;

   function UnboxedType (This : access Typ;
                         P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                         return access Javax.Lang.Model.type_K.PrimitiveType.Typ'Class is abstract;

   function Capture (This : access Typ;
                     P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                     return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class is abstract;

   function GetPrimitiveType (This : access Typ;
                              P1_TypeKind : access Standard.Javax.Lang.Model.type_K.TypeKind.Typ'Class)
                              return access Javax.Lang.Model.type_K.PrimitiveType.Typ'Class is abstract;

   function GetNullType (This : access Typ)
                         return access Javax.Lang.Model.type_K.NullType.Typ'Class is abstract;

   function GetNoType (This : access Typ;
                       P1_TypeKind : access Standard.Javax.Lang.Model.type_K.TypeKind.Typ'Class)
                       return access Javax.Lang.Model.type_K.NoType.Typ'Class is abstract;

   function GetArrayType (This : access Typ;
                          P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                          return access Javax.Lang.Model.type_K.ArrayType.Typ'Class is abstract;

   function GetWildcardType (This : access Typ;
                             P1_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class;
                             P2_TypeMirror : access Standard.Javax.Lang.Model.type_K.TypeMirror.Typ'Class)
                             return access Javax.Lang.Model.type_K.WildcardType.Typ'Class is abstract;

   function GetDeclaredType (This : access Typ;
                             P1_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                             P2_TypeMirror_Arr : access Javax.Lang.Model.type_K.TypeMirror.Arr_Obj)
                             return access Javax.Lang.Model.type_K.DeclaredType.Typ'Class is abstract;

   function GetDeclaredType (This : access Typ;
                             P1_DeclaredType : access Standard.Javax.Lang.Model.type_K.DeclaredType.Typ'Class;
                             P2_TypeElement : access Standard.Javax.Lang.Model.Element.TypeElement.Typ'Class;
                             P3_TypeMirror_Arr : access Javax.Lang.Model.type_K.TypeMirror.Arr_Obj)
                             return access Javax.Lang.Model.type_K.DeclaredType.Typ'Class is abstract;

   function AsMemberOf (This : access Typ;
                        P1_DeclaredType : access Standard.Javax.Lang.Model.type_K.DeclaredType.Typ'Class;
                        P2_Element : access Standard.Javax.Lang.Model.Element.Element.Typ'Class)
                        return access Javax.Lang.Model.type_K.TypeMirror.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AsElement, "asElement");
   pragma Export (Java, IsSameType, "isSameType");
   pragma Export (Java, IsSubtype, "isSubtype");
   pragma Export (Java, IsAssignable, "isAssignable");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, IsSubsignature, "isSubsignature");
   pragma Export (Java, DirectSupertypes, "directSupertypes");
   pragma Export (Java, Erasure, "erasure");
   pragma Export (Java, BoxedClass, "boxedClass");
   pragma Export (Java, UnboxedType, "unboxedType");
   pragma Export (Java, Capture, "capture");
   pragma Export (Java, GetPrimitiveType, "getPrimitiveType");
   pragma Export (Java, GetNullType, "getNullType");
   pragma Export (Java, GetNoType, "getNoType");
   pragma Export (Java, GetArrayType, "getArrayType");
   pragma Export (Java, GetWildcardType, "getWildcardType");
   pragma Export (Java, GetDeclaredType, "getDeclaredType");
   pragma Export (Java, AsMemberOf, "asMemberOf");

end Javax.Lang.Model.Util.Types;
pragma Import (Java, Javax.Lang.Model.Util.Types, "javax.lang.model.util.Types");
pragma Extensions_Allowed (Off);
