pragma Extensions_Allowed (On);
package Javax.Lang.Model.Util is
   pragma Preelaborate;
end Javax.Lang.Model.Util;
pragma Import (Java, Javax.Lang.Model.Util, "javax.lang.model.util");
pragma Extensions_Allowed (Off);
