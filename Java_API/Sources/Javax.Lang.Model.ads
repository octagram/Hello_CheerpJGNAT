pragma Extensions_Allowed (On);
package Javax.Lang.Model is
   pragma Preelaborate;
end Javax.Lang.Model;
pragma Import (Java, Javax.Lang.Model, "javax.lang.model");
pragma Extensions_Allowed (Off);
