pragma Extensions_Allowed (On);
package Javax.Lang is
   pragma Preelaborate;
end Javax.Lang;
pragma Import (Java, Javax.Lang, "javax.lang");
pragma Extensions_Allowed (Off);
