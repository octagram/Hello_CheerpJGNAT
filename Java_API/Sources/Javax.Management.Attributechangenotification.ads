pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Notification;

package Javax.Management.AttributeChangeNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Notification.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributeChangeNotification (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                             P2_Long : Java.Long;
                                             P3_Long : Java.Long;
                                             P4_String : access Standard.Java.Lang.String.Typ'Class;
                                             P5_String : access Standard.Java.Lang.String.Typ'Class;
                                             P6_String : access Standard.Java.Lang.String.Typ'Class;
                                             P7_Object : access Standard.Java.Lang.Object.Typ'Class;
                                             P8_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributeName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetAttributeType (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetOldValue (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   function GetNewValue (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ATTRIBUTE_CHANGE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributeChangeNotification);
   pragma Import (Java, GetAttributeName, "getAttributeName");
   pragma Import (Java, GetAttributeType, "getAttributeType");
   pragma Import (Java, GetOldValue, "getOldValue");
   pragma Import (Java, GetNewValue, "getNewValue");
   pragma Import (Java, ATTRIBUTE_CHANGE, "ATTRIBUTE_CHANGE");

end Javax.Management.AttributeChangeNotification;
pragma Import (Java, Javax.Management.AttributeChangeNotification, "javax.management.AttributeChangeNotification");
pragma Extensions_Allowed (Off);
