pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Management.Notification;
with Java.Lang.Object;
with Javax.Management.NotificationFilter;

package Javax.Management.AttributeChangeNotificationFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(NotificationFilter_I : Javax.Management.NotificationFilter.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributeChangeNotificationFilter (This : Ref := null)
                                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function IsNotificationEnabled (This : access Typ;
                                   P1_Notification : access Standard.Javax.Management.Notification.Typ'Class)
                                   return Java.Boolean;

   --  synchronized
   procedure EnableAttribute (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   procedure DisableAttribute (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   procedure DisableAllAttributes (This : access Typ);

   --  synchronized
   function GetEnabledAttributes (This : access Typ)
                                  return access Java.Util.Vector.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributeChangeNotificationFilter);
   pragma Import (Java, IsNotificationEnabled, "isNotificationEnabled");
   pragma Import (Java, EnableAttribute, "enableAttribute");
   pragma Import (Java, DisableAttribute, "disableAttribute");
   pragma Import (Java, DisableAllAttributes, "disableAllAttributes");
   pragma Import (Java, GetEnabledAttributes, "getEnabledAttributes");

end Javax.Management.AttributeChangeNotificationFilter;
pragma Import (Java, Javax.Management.AttributeChangeNotificationFilter, "javax.management.AttributeChangeNotificationFilter");
pragma Extensions_Allowed (Off);
