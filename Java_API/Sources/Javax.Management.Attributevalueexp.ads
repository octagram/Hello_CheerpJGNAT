pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.ValueExp;

package Javax.Management.AttributeValueExp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ValueExp_I : Javax.Management.ValueExp.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributeValueExp (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributeName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function Apply (This : access Typ;
                   P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                   return access Javax.Management.ValueExp.Typ'Class;
   --  can raise Javax.Management.BadStringOperationException.Except,
   --  Javax.Management.BadBinaryOpValueExpException.Except,
   --  Javax.Management.BadAttributeValueExpException.Except and
   --  Javax.Management.InvalidApplicationException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetMBeanServer (This : access Typ;
                             P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class);

   --  protected
   function GetAttribute (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributeValueExp);
   pragma Import (Java, GetAttributeName, "getAttributeName");
   pragma Import (Java, Apply, "apply");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SetMBeanServer, "setMBeanServer");
   pragma Import (Java, GetAttribute, "getAttribute");

end Javax.Management.AttributeValueExp;
pragma Import (Java, Javax.Management.AttributeValueExp, "javax.management.AttributeValueExp");
pragma Extensions_Allowed (Off);
