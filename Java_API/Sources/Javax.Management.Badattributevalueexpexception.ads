pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Management.BadAttributeValueExpException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BadAttributeValueExpException (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.management.BadAttributeValueExpException");
   pragma Java_Constructor (New_BadAttributeValueExpException);
   pragma Import (Java, ToString, "toString");

end Javax.Management.BadAttributeValueExpException;
pragma Import (Java, Javax.Management.BadAttributeValueExpException, "javax.management.BadAttributeValueExpException");
pragma Extensions_Allowed (Off);
