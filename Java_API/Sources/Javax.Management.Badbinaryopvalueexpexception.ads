pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.ValueExp;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Management.BadBinaryOpValueExpException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BadBinaryOpValueExpException (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetExp (This : access Typ)
                    return access Javax.Management.ValueExp.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.management.BadBinaryOpValueExpException");
   pragma Java_Constructor (New_BadBinaryOpValueExpException);
   pragma Import (Java, GetExp, "getExp");
   pragma Import (Java, ToString, "toString");

end Javax.Management.BadBinaryOpValueExpException;
pragma Import (Java, Javax.Management.BadBinaryOpValueExpException, "javax.management.BadBinaryOpValueExpException");
pragma Extensions_Allowed (Off);
