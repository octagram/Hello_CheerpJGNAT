pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Management.DefaultLoaderRepository is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultLoaderRepository (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function LoadClass (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function LoadClassWithout (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultLoaderRepository);
   pragma Import (Java, LoadClass, "loadClass");
   pragma Import (Java, LoadClassWithout, "loadClassWithout");

end Javax.Management.DefaultLoaderRepository;
pragma Import (Java, Javax.Management.DefaultLoaderRepository, "javax.management.DefaultLoaderRepository");
pragma Extensions_Allowed (Off);
