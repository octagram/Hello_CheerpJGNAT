pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Management.Descriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFieldValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   procedure SetField (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   function GetFields (This : access Typ)
                       return Standard.Java.Lang.Object.Ref is abstract;

   function GetFieldNames (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function GetFieldValues (This : access Typ;
                            P1_String_Arr : access Java.Lang.String.Arr_Obj)
                            return Standard.Java.Lang.Object.Ref is abstract;

   procedure RemoveField (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetFields (This : access Typ;
                        P1_String_Arr : access Java.Lang.String.Arr_Obj;
                        P2_Object_Arr : access Java.Lang.Object.Arr_Obj) is abstract;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetFieldValue, "getFieldValue");
   pragma Export (Java, SetField, "setField");
   pragma Export (Java, GetFields, "getFields");
   pragma Export (Java, GetFieldNames, "getFieldNames");
   pragma Export (Java, GetFieldValues, "getFieldValues");
   pragma Export (Java, RemoveField, "removeField");
   pragma Export (Java, SetFields, "setFields");
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");

end Javax.Management.Descriptor;
pragma Import (Java, Javax.Management.Descriptor, "javax.management.Descriptor");
pragma Extensions_Allowed (Off);
