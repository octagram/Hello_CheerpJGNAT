pragma Extensions_Allowed (On);
limited with Javax.Management.Descriptor;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;

package Javax.Management.DescriptorAccess is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDescriptor (This : access Typ;
                            P1_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetDescriptor, "setDescriptor");

end Javax.Management.DescriptorAccess;
pragma Import (Java, Javax.Management.DescriptorAccess, "javax.management.DescriptorAccess");
pragma Extensions_Allowed (Off);
