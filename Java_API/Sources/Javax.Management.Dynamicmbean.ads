pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Attribute;
limited with Javax.Management.AttributeList;
limited with Javax.Management.MBeanInfo;
with Java.Lang.Object;

package Javax.Management.DynamicMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   procedure SetAttribute (This : access Typ;
                           P1_Attribute : access Standard.Javax.Management.Attribute.Typ'Class) is abstract;
   --  can raise Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InvalidAttributeValueException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetAttributes (This : access Typ;
                           P1_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Management.AttributeList.Typ'Class is abstract;

   function SetAttributes (This : access Typ;
                           P1_AttributeList : access Standard.Javax.Management.AttributeList.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class is abstract;

   function Invoke (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P3_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetMBeanInfo (This : access Typ)
                          return access Javax.Management.MBeanInfo.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, SetAttribute, "setAttribute");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, SetAttributes, "setAttributes");
   pragma Export (Java, Invoke, "invoke");
   pragma Export (Java, GetMBeanInfo, "getMBeanInfo");

end Javax.Management.DynamicMBean;
pragma Import (Java, Javax.Management.DynamicMBean, "javax.management.DynamicMBean");
pragma Extensions_Allowed (Off);
