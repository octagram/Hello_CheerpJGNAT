pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
with Java.Lang.Object;
with Javax.Management.Descriptor;

package Javax.Management.ImmutableDescriptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Descriptor_I : Javax.Management.Descriptor.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImmutableDescriptor (P1_String_Arr : access Java.Lang.String.Arr_Obj;
                                     P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                     This : Ref := null)
                                     return Ref;

   function New_ImmutableDescriptor (P1_String_Arr : access Java.Lang.String.Arr_Obj; 
                                     This : Ref := null)
                                     return Ref;

   function New_ImmutableDescriptor (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Union (P1_Descriptor_Arr : access Javax.Management.Descriptor.Arr_Obj)
                   return access Javax.Management.ImmutableDescriptor.Typ'Class;

   --  final
   function GetFieldValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   --  final
   function GetFields (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   --  final
   function GetFieldValues (This : access Typ;
                            P1_String_Arr : access Java.Lang.String.Arr_Obj)
                            return Standard.Java.Lang.Object.Ref;

   --  final
   function GetFieldNames (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IsValid (This : access Typ)
                     return Java.Boolean;

   function Clone (This : access Typ)
                   return access Javax.Management.Descriptor.Typ'Class;

   --  final
   procedure SetFields (This : access Typ;
                        P1_String_Arr : access Java.Lang.String.Arr_Obj;
                        P2_Object_Arr : access Java.Lang.Object.Arr_Obj);
   --  can raise Javax.Management.RuntimeOperationsException.Except

   --  final
   procedure SetField (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.RuntimeOperationsException.Except

   --  final
   procedure RemoveField (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   EMPTY_DESCRIPTOR : access Javax.Management.ImmutableDescriptor.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImmutableDescriptor);
   pragma Import (Java, Union, "union");
   pragma Import (Java, GetFieldValue, "getFieldValue");
   pragma Import (Java, GetFields, "getFields");
   pragma Import (Java, GetFieldValues, "getFieldValues");
   pragma Import (Java, GetFieldNames, "getFieldNames");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, IsValid, "isValid");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, SetFields, "setFields");
   pragma Import (Java, SetField, "setField");
   pragma Import (Java, RemoveField, "removeField");
   pragma Import (Java, EMPTY_DESCRIPTOR, "EMPTY_DESCRIPTOR");

end Javax.Management.ImmutableDescriptor;
pragma Import (Java, Javax.Management.ImmutableDescriptor, "javax.management.ImmutableDescriptor");
pragma Extensions_Allowed (Off);
