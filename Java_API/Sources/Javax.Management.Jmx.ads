pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Management.MBeanServerConnection;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;

package Javax.Management.JMX is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewMBeanProxy (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                           P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;

   function NewMBeanProxy (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                           P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                           P4_Boolean : Java.Boolean)
                           return access Java.Lang.Object.Typ'Class;

   function NewMXBeanProxy (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                            P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                            P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                            return access Java.Lang.Object.Typ'Class;

   function NewMXBeanProxy (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                            P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                            P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P4_Boolean : Java.Boolean)
                            return access Java.Lang.Object.Typ'Class;

   function IsMXBeanInterface (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                               return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_VALUE_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   IMMUTABLE_INFO_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   INTERFACE_CLASS_NAME_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   LEGAL_VALUES_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   MAX_VALUE_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   MIN_VALUE_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   MXBEAN_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   OPEN_TYPE_FIELD : constant access Java.Lang.String.Typ'Class;

   --  final
   ORIGINAL_TYPE_FIELD : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewMBeanProxy, "newMBeanProxy");
   pragma Import (Java, NewMXBeanProxy, "newMXBeanProxy");
   pragma Import (Java, IsMXBeanInterface, "isMXBeanInterface");
   pragma Import (Java, DEFAULT_VALUE_FIELD, "DEFAULT_VALUE_FIELD");
   pragma Import (Java, IMMUTABLE_INFO_FIELD, "IMMUTABLE_INFO_FIELD");
   pragma Import (Java, INTERFACE_CLASS_NAME_FIELD, "INTERFACE_CLASS_NAME_FIELD");
   pragma Import (Java, LEGAL_VALUES_FIELD, "LEGAL_VALUES_FIELD");
   pragma Import (Java, MAX_VALUE_FIELD, "MAX_VALUE_FIELD");
   pragma Import (Java, MIN_VALUE_FIELD, "MIN_VALUE_FIELD");
   pragma Import (Java, MXBEAN_FIELD, "MXBEAN_FIELD");
   pragma Import (Java, OPEN_TYPE_FIELD, "OPEN_TYPE_FIELD");
   pragma Import (Java, ORIGINAL_TYPE_FIELD, "ORIGINAL_TYPE_FIELD");

end Javax.Management.JMX;
pragma Import (Java, Javax.Management.JMX, "javax.management.JMX");
pragma Extensions_Allowed (Off);
