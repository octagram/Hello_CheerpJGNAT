pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Management.Loading.ClassLoaderRepository is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function LoadClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class is abstract;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function LoadClassWithout (This : access Typ;
                              P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Class.Typ'Class is abstract;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function LoadClassBefore (This : access Typ;
                             P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.Class.Typ'Class is abstract;
   --  can raise Java.Lang.ClassNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, LoadClass, "loadClass");
   pragma Export (Java, LoadClassWithout, "loadClassWithout");
   pragma Export (Java, LoadClassBefore, "loadClassBefore");

end Javax.Management.Loading.ClassLoaderRepository;
pragma Import (Java, Javax.Management.Loading.ClassLoaderRepository, "javax.management.loading.ClassLoaderRepository");
pragma Extensions_Allowed (Off);
