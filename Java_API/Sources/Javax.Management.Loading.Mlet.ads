pragma Extensions_Allowed (On);
limited with Java.Io.ObjectInput;
limited with Java.Io.ObjectOutput;
limited with Java.Lang.Boolean;
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Net.URLStreamHandlerFactory;
limited with Java.Util.Set;
limited with Javax.Management.Loading.ClassLoaderRepository;
limited with Javax.Management.Loading.MLetContent;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Io.Externalizable;
with Java.Lang.Object;
with Java.Net.URLClassLoader;
with Javax.Management.Loading.MLetMBean;
with Javax.Management.MBeanRegistration;

package Javax.Management.Loading.MLet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Externalizable_I : Java.Io.Externalizable.Ref;
            MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            MLetMBean_I : Javax.Management.Loading.MLetMBean.Ref)
    is new Java.Net.URLClassLoader.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MLet (This : Ref := null)
                      return Ref;

   function New_MLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj; 
                      This : Ref := null)
                      return Ref;

   function New_MLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                      P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   function New_MLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                      P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                      P3_URLStreamHandlerFactory : access Standard.Java.Net.URLStreamHandlerFactory.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   function New_MLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                      P2_Boolean : Java.Boolean; 
                      This : Ref := null)
                      return Ref;

   function New_MLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                      P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                      P3_Boolean : Java.Boolean; 
                      This : Ref := null)
                      return Ref;

   function New_MLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                      P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                      P3_URLStreamHandlerFactory : access Standard.Java.Net.URLStreamHandlerFactory.Typ'Class;
                      P4_Boolean : Java.Boolean; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddURL (This : access Typ;
                     P1_URL : access Standard.Java.Net.URL.Typ'Class);

   procedure AddURL (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.ServiceNotFoundException.Except

   function GetURLs (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function GetMBeansFromURL (This : access Typ;
                              P1_URL : access Standard.Java.Net.URL.Typ'Class)
                              return access Java.Util.Set.Typ'Class;
   --  can raise Javax.Management.ServiceNotFoundException.Except

   function GetMBeansFromURL (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Util.Set.Typ'Class;
   --  can raise Javax.Management.ServiceNotFoundException.Except

   --  synchronized
   function GetLibraryDirectory (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure SetLibraryDirectory (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);

   procedure WriteExternal (This : access Typ;
                            P1_ObjectOutput : access Standard.Java.Io.ObjectOutput.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.UnsupportedOperationException.Except

   procedure ReadExternal (This : access Typ;
                           P1_ObjectInput : access Standard.Java.Io.ObjectInput.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Java.Lang.ClassNotFoundException.Except and
   --  Java.Lang.UnsupportedOperationException.Except

   --  synchronized
   function LoadClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_ClassLoaderRepository : access Standard.Javax.Management.Loading.ClassLoaderRepository.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   --  protected
   function FindClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   --  protected
   function FindLibrary (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   --  protected
   function Check (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_URL : access Standard.Java.Net.URL.Typ'Class;
                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                   P4_MLetContent : access Standard.Javax.Management.Loading.MLetContent.Typ'Class)
                   return access Java.Net.URL.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MLet);
   pragma Import (Java, AddURL, "addURL");
   pragma Import (Java, GetURLs, "getURLs");
   pragma Import (Java, GetMBeansFromURL, "getMBeansFromURL");
   pragma Import (Java, GetLibraryDirectory, "getLibraryDirectory");
   pragma Import (Java, SetLibraryDirectory, "setLibraryDirectory");
   pragma Import (Java, PreRegister, "preRegister");
   pragma Import (Java, PostRegister, "postRegister");
   pragma Import (Java, PreDeregister, "preDeregister");
   pragma Import (Java, PostDeregister, "postDeregister");
   pragma Import (Java, WriteExternal, "writeExternal");
   pragma Import (Java, ReadExternal, "readExternal");
   pragma Import (Java, LoadClass, "loadClass");
   pragma Import (Java, FindClass, "findClass");
   pragma Import (Java, FindLibrary, "findLibrary");
   pragma Import (Java, Check, "check");

end Javax.Management.Loading.MLet;
pragma Import (Java, Javax.Management.Loading.MLet, "javax.management.loading.MLet");
pragma Extensions_Allowed (Off);
