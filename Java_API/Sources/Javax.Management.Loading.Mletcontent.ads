pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.List;
limited with Java.Util.Map;
with Java.Lang.Object;

package Javax.Management.Loading.MLetContent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MLetContent (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                             P2_Map : access Standard.Java.Util.Map.Typ'Class;
                             P3_List : access Standard.Java.Util.List.Typ'Class;
                             P4_List : access Standard.Java.Util.List.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributes (This : access Typ)
                           return access Java.Util.Map.Typ'Class;

   function GetDocumentBase (This : access Typ)
                             return access Java.Net.URL.Typ'Class;

   function GetCodeBase (This : access Typ)
                         return access Java.Net.URL.Typ'Class;

   function GetJarFiles (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetCode (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetSerializedObject (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetParameterTypes (This : access Typ)
                               return access Java.Util.List.Typ'Class;

   function GetParameterValues (This : access Typ)
                                return access Java.Util.List.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MLetContent);
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetDocumentBase, "getDocumentBase");
   pragma Import (Java, GetCodeBase, "getCodeBase");
   pragma Import (Java, GetJarFiles, "getJarFiles");
   pragma Import (Java, GetCode, "getCode");
   pragma Import (Java, GetSerializedObject, "getSerializedObject");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetVersion, "getVersion");
   pragma Import (Java, GetParameterTypes, "getParameterTypes");
   pragma Import (Java, GetParameterValues, "getParameterValues");

end Javax.Management.Loading.MLetContent;
pragma Import (Java, Javax.Management.Loading.MLetContent, "javax.management.loading.MLetContent");
pragma Extensions_Allowed (Off);
