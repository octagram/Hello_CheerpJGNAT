pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Enumeration;
limited with Java.Util.Set;
with Java.Lang.Object;

package Javax.Management.Loading.MLetMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMBeansFromURL (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Util.Set.Typ'Class is abstract;
   --  can raise Javax.Management.ServiceNotFoundException.Except

   function GetMBeansFromURL (This : access Typ;
                              P1_URL : access Standard.Java.Net.URL.Typ'Class)
                              return access Java.Util.Set.Typ'Class is abstract;
   --  can raise Javax.Management.ServiceNotFoundException.Except

   procedure AddURL (This : access Typ;
                     P1_URL : access Standard.Java.Net.URL.Typ'Class) is abstract;

   procedure AddURL (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.ServiceNotFoundException.Except

   function GetURLs (This : access Typ)
                     return Standard.Java.Lang.Object.Ref is abstract;

   function GetResource (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Net.URL.Typ'Class is abstract;

   function GetResourceAsStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Io.InputStream.Typ'Class is abstract;

   function GetResources (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Util.Enumeration.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetLibraryDirectory (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLibraryDirectory (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMBeansFromURL, "getMBeansFromURL");
   pragma Export (Java, AddURL, "addURL");
   pragma Export (Java, GetURLs, "getURLs");
   pragma Export (Java, GetResource, "getResource");
   pragma Export (Java, GetResourceAsStream, "getResourceAsStream");
   pragma Export (Java, GetResources, "getResources");
   pragma Export (Java, GetLibraryDirectory, "getLibraryDirectory");
   pragma Export (Java, SetLibraryDirectory, "setLibraryDirectory");

end Javax.Management.Loading.MLetMBean;
pragma Import (Java, Javax.Management.Loading.MLetMBean, "javax.management.loading.MLetMBean");
pragma Extensions_Allowed (Off);
