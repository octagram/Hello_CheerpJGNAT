pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Net.URL;
limited with Java.Net.URLStreamHandlerFactory;
with Java.Io.Externalizable;
with Java.Lang.Object;
with Javax.Management.Loading.MLet;
with Javax.Management.Loading.MLetMBean;
with Javax.Management.Loading.PrivateClassLoader;
with Javax.Management.MBeanRegistration;

package Javax.Management.Loading.PrivateMLet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Externalizable_I : Java.Io.Externalizable.Ref;
            MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            MLetMBean_I : Javax.Management.Loading.MLetMBean.Ref;
            PrivateClassLoader_I : Javax.Management.Loading.PrivateClassLoader.Ref)
    is new Javax.Management.Loading.MLet.Typ(Externalizable_I,
                                             MBeanRegistration_I,
                                             MLetMBean_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrivateMLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                             P2_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   function New_PrivateMLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                             P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                             P3_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   function New_PrivateMLet (P1_URL_Arr : access Java.Net.URL.Arr_Obj;
                             P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                             P3_URLStreamHandlerFactory : access Standard.Java.Net.URLStreamHandlerFactory.Typ'Class;
                             P4_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrivateMLet);

end Javax.Management.Loading.PrivateMLet;
pragma Import (Java, Javax.Management.Loading.PrivateMLet, "javax.management.loading.PrivateMLet");
pragma Extensions_Allowed (Off);
