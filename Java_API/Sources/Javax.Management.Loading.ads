pragma Extensions_Allowed (On);
package Javax.Management.Loading is
   pragma Preelaborate;
end Javax.Management.Loading;
pragma Import (Java, Javax.Management.Loading, "javax.management.loading");
pragma Extensions_Allowed (Off);
