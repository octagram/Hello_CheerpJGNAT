pragma Extensions_Allowed (On);
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;
with Javax.Management.MBeanFeatureInfo;

package Javax.Management.MBeanAttributeInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref)
    is new Javax.Management.MBeanFeatureInfo.Typ(Serializable_I,
                                                 DescriptorRead_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanAttributeInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                                    P4_Boolean : Java.Boolean;
                                    P5_Boolean : Java.Boolean;
                                    P6_Boolean : Java.Boolean; 
                                    This : Ref := null)
                                    return Ref;

   function New_MBeanAttributeInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                                    P4_Boolean : Java.Boolean;
                                    P5_Boolean : Java.Boolean;
                                    P6_Boolean : Java.Boolean;
                                    P7_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_MBeanAttributeInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                    P4_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Javax.Management.IntrospectionException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function IsReadable (This : access Typ)
                        return Java.Boolean;

   function IsWritable (This : access Typ)
                        return Java.Boolean;

   function IsIs (This : access Typ)
                  return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanAttributeInfo);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, IsReadable, "isReadable");
   pragma Import (Java, IsWritable, "isWritable");
   pragma Import (Java, IsIs, "isIs");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Management.MBeanAttributeInfo;
pragma Import (Java, Javax.Management.MBeanAttributeInfo, "javax.management.MBeanAttributeInfo");
pragma Extensions_Allowed (Off);
