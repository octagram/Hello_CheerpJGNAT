pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;

package Javax.Management.MBeanFeatureInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      --  protected
      Description : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Description, "description");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanFeatureInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_MBeanFeatureInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetDescriptor (This : access Typ)
                           return access Javax.Management.Descriptor.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanFeatureInfo);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetDescriptor, "getDescriptor");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Management.MBeanFeatureInfo;
pragma Import (Java, Javax.Management.MBeanFeatureInfo, "javax.management.MBeanFeatureInfo");
pragma Extensions_Allowed (Off);
