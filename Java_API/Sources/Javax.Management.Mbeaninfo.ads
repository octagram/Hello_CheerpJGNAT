pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
limited with Javax.Management.MBeanAttributeInfo;
limited with Javax.Management.MBeanConstructorInfo;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.MBeanOperationInfo;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;

package Javax.Management.MBeanInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_MBeanAttributeInfo_Arr : access Javax.Management.MBeanAttributeInfo.Arr_Obj;
                           P4_MBeanConstructorInfo_Arr : access Javax.Management.MBeanConstructorInfo.Arr_Obj;
                           P5_MBeanOperationInfo_Arr : access Javax.Management.MBeanOperationInfo.Arr_Obj;
                           P6_MBeanNotificationInfo_Arr : access Javax.Management.MBeanNotificationInfo.Arr_Obj; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function New_MBeanInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_MBeanAttributeInfo_Arr : access Javax.Management.MBeanAttributeInfo.Arr_Obj;
                           P4_MBeanConstructorInfo_Arr : access Javax.Management.MBeanConstructorInfo.Arr_Obj;
                           P5_MBeanOperationInfo_Arr : access Javax.Management.MBeanOperationInfo.Arr_Obj;
                           P6_MBeanNotificationInfo_Arr : access Javax.Management.MBeanNotificationInfo.Arr_Obj;
                           P7_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetAttributes (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function GetOperations (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function GetConstructors (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetNotifications (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetDescriptor (This : access Typ)
                           return access Javax.Management.Descriptor.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanInfo);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetOperations, "getOperations");
   pragma Import (Java, GetConstructors, "getConstructors");
   pragma Import (Java, GetNotifications, "getNotifications");
   pragma Import (Java, GetDescriptor, "getDescriptor");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Management.MBeanInfo;
pragma Import (Java, Javax.Management.MBeanInfo, "javax.management.MBeanInfo");
pragma Extensions_Allowed (Off);
