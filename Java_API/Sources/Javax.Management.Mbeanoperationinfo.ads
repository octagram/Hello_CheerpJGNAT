pragma Extensions_Allowed (On);
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
limited with Javax.Management.MBeanParameterInfo;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;
with Javax.Management.MBeanFeatureInfo;

package Javax.Management.MBeanOperationInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref)
    is new Javax.Management.MBeanFeatureInfo.Typ(Serializable_I,
                                                 DescriptorRead_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanOperationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_MBeanOperationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_MBeanParameterInfo_Arr : access Javax.Management.MBeanParameterInfo.Arr_Obj;
                                    P4_String : access Standard.Java.Lang.String.Typ'Class;
                                    P5_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   function New_MBeanOperationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_MBeanParameterInfo_Arr : access Javax.Management.MBeanParameterInfo.Arr_Obj;
                                    P4_String : access Standard.Java.Lang.String.Typ'Class;
                                    P5_Int : Java.Int;
                                    P6_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetReturnType (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetSignature (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   function GetImpact (This : access Typ)
                       return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INFO : constant Java.Int;

   --  final
   ACTION : constant Java.Int;

   --  final
   ACTION_INFO : constant Java.Int;

   --  final
   UNKNOWN : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanOperationInfo);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetReturnType, "getReturnType");
   pragma Import (Java, GetSignature, "getSignature");
   pragma Import (Java, GetImpact, "getImpact");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, INFO, "INFO");
   pragma Import (Java, ACTION, "ACTION");
   pragma Import (Java, ACTION_INFO, "ACTION_INFO");
   pragma Import (Java, UNKNOWN, "UNKNOWN");

end Javax.Management.MBeanOperationInfo;
pragma Import (Java, Javax.Management.MBeanOperationInfo, "javax.management.MBeanOperationInfo");
pragma Extensions_Allowed (Off);
