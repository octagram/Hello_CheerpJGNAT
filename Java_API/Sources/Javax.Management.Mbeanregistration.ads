pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;

package Javax.Management.MBeanRegistration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class is abstract;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class) is abstract;

   procedure PreDeregister (This : access Typ) is abstract;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, PreRegister, "preRegister");
   pragma Export (Java, PostRegister, "postRegister");
   pragma Export (Java, PreDeregister, "preDeregister");
   pragma Export (Java, PostDeregister, "postDeregister");

end Javax.Management.MBeanRegistration;
pragma Import (Java, Javax.Management.MBeanRegistration, "javax.management.MBeanRegistration");
pragma Extensions_Allowed (Off);
