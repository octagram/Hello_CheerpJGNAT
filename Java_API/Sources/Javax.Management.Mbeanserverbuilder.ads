pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.MBeanServerDelegate;
with Java.Lang.Object;

package Javax.Management.MBeanServerBuilder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanServerBuilder (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewMBeanServerDelegate (This : access Typ)
                                    return access Javax.Management.MBeanServerDelegate.Typ'Class;

   function NewMBeanServer (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                            P3_MBeanServerDelegate : access Standard.Javax.Management.MBeanServerDelegate.Typ'Class)
                            return access Javax.Management.MBeanServer.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanServerBuilder);
   pragma Import (Java, NewMBeanServerDelegate, "newMBeanServerDelegate");
   pragma Import (Java, NewMBeanServer, "newMBeanServer");

end Javax.Management.MBeanServerBuilder;
pragma Import (Java, Javax.Management.MBeanServerBuilder, "javax.management.MBeanServerBuilder");
pragma Extensions_Allowed (Off);
