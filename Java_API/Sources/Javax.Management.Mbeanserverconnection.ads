pragma Extensions_Allowed (On);
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Util.Set;
limited with Javax.Management.Attribute;
limited with Javax.Management.AttributeList;
limited with Javax.Management.MBeanInfo;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
limited with Javax.Management.ObjectInstance;
limited with Javax.Management.ObjectName;
limited with Javax.Management.QueryExp;
with Java.Lang.Object;

package Javax.Management.MBeanServerConnection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                         P4_String_Arr : access Java.Lang.String.Arr_Obj)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P4_Object_Arr : access Java.Lang.Object.Arr_Obj;
                         P5_String_Arr : access Java.Lang.String.Arr_Obj)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure UnregisterMBean (This : access Typ;
                              P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanRegistrationException.Except and
   --  Java.Io.IOException.Except

   function GetObjectInstance (This : access Typ;
                               P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                               return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   function QueryMBeans (This : access Typ;
                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P2_QueryExp : access Standard.Javax.Management.QueryExp.Typ'Class)
                         return access Java.Util.Set.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function QueryNames (This : access Typ;
                        P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                        P2_QueryExp : access Standard.Javax.Management.QueryExp.Typ'Class)
                        return access Java.Util.Set.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function IsRegistered (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                          return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function GetMBeanCount (This : access Typ)
                           return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetAttribute (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function GetAttributes (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Management.AttributeList.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   procedure SetAttribute (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_Attribute : access Standard.Javax.Management.Attribute.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InvalidAttributeValueException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function SetAttributes (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_AttributeList : access Standard.Javax.Management.AttributeList.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function Invoke (This : access Typ;
                    P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P4_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function GetDefaultDomain (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetDomains (This : access Typ)
                        return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Io.IOException.Except

   procedure AddNotificationListener (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P2_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                      P3_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                      P4_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure AddNotificationListener (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P3_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                      P4_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ListenerNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P3_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                         P4_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ListenerNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ListenerNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                         P3_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                         P4_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ListenerNotFoundException.Except and
   --  Java.Io.IOException.Except

   function GetMBeanInfo (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                          return access Javax.Management.MBeanInfo.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.IntrospectionException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function IsInstanceOf (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateMBean, "createMBean");
   pragma Export (Java, UnregisterMBean, "unregisterMBean");
   pragma Export (Java, GetObjectInstance, "getObjectInstance");
   pragma Export (Java, QueryMBeans, "queryMBeans");
   pragma Export (Java, QueryNames, "queryNames");
   pragma Export (Java, IsRegistered, "isRegistered");
   pragma Export (Java, GetMBeanCount, "getMBeanCount");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, SetAttribute, "setAttribute");
   pragma Export (Java, SetAttributes, "setAttributes");
   pragma Export (Java, Invoke, "invoke");
   pragma Export (Java, GetDefaultDomain, "getDefaultDomain");
   pragma Export (Java, GetDomains, "getDomains");
   pragma Export (Java, AddNotificationListener, "addNotificationListener");
   pragma Export (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Export (Java, GetMBeanInfo, "getMBeanInfo");
   pragma Export (Java, IsInstanceOf, "isInstanceOf");

end Javax.Management.MBeanServerConnection;
pragma Import (Java, Javax.Management.MBeanServerConnection, "javax.management.MBeanServerConnection");
pragma Extensions_Allowed (Off);
