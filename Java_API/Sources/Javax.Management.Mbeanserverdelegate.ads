pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.Notification;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.MBeanServerDelegateMBean;
with Javax.Management.NotificationEmitter;

package Javax.Management.MBeanServerDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanServerDelegateMBean_I : Javax.Management.MBeanServerDelegateMBean.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanServerDelegate (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetMBeanServerId (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetSpecificationName (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   function GetSpecificationVersion (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetSpecificationVendor (This : access Typ)
                                    return access Java.Lang.String.Typ'Class;

   function GetImplementationName (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   function GetImplementationVersion (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetImplementationVendor (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure AddNotificationListener (This : access Typ;
                                      P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                      P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                      P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                         P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                         P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   --  synchronized
   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure SendNotification (This : access Typ;
                               P1_Notification : access Standard.Javax.Management.Notification.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DELEGATE_NAME : access Javax.Management.ObjectName.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanServerDelegate);
   pragma Import (Java, GetMBeanServerId, "getMBeanServerId");
   pragma Import (Java, GetSpecificationName, "getSpecificationName");
   pragma Import (Java, GetSpecificationVersion, "getSpecificationVersion");
   pragma Import (Java, GetSpecificationVendor, "getSpecificationVendor");
   pragma Import (Java, GetImplementationName, "getImplementationName");
   pragma Import (Java, GetImplementationVersion, "getImplementationVersion");
   pragma Import (Java, GetImplementationVendor, "getImplementationVendor");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");
   pragma Import (Java, AddNotificationListener, "addNotificationListener");
   pragma Import (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Import (Java, SendNotification, "sendNotification");
   pragma Import (Java, DELEGATE_NAME, "DELEGATE_NAME");

end Javax.Management.MBeanServerDelegate;
pragma Import (Java, Javax.Management.MBeanServerDelegate, "javax.management.MBeanServerDelegate");
pragma Extensions_Allowed (Off);
