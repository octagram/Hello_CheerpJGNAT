pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Management.MBeanServerDelegateMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMBeanServerId (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   function GetSpecificationName (This : access Typ)
                                  return access Java.Lang.String.Typ'Class is abstract;

   function GetSpecificationVersion (This : access Typ)
                                     return access Java.Lang.String.Typ'Class is abstract;

   function GetSpecificationVendor (This : access Typ)
                                    return access Java.Lang.String.Typ'Class is abstract;

   function GetImplementationName (This : access Typ)
                                   return access Java.Lang.String.Typ'Class is abstract;

   function GetImplementationVersion (This : access Typ)
                                      return access Java.Lang.String.Typ'Class is abstract;

   function GetImplementationVendor (This : access Typ)
                                     return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMBeanServerId, "getMBeanServerId");
   pragma Export (Java, GetSpecificationName, "getSpecificationName");
   pragma Export (Java, GetSpecificationVersion, "getSpecificationVersion");
   pragma Export (Java, GetSpecificationVendor, "getSpecificationVendor");
   pragma Export (Java, GetImplementationName, "getImplementationName");
   pragma Export (Java, GetImplementationVersion, "getImplementationVersion");
   pragma Export (Java, GetImplementationVendor, "getImplementationVendor");

end Javax.Management.MBeanServerDelegateMBean;
pragma Import (Java, Javax.Management.MBeanServerDelegateMBean, "javax.management.MBeanServerDelegateMBean");
pragma Extensions_Allowed (Off);
