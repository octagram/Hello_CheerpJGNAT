pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.ArrayList;
limited with Javax.Management.Loading.ClassLoaderRepository;
limited with Javax.Management.MBeanServer;
with Java.Lang.Object;

package Javax.Management.MBeanServerFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ReleaseMBeanServer (P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class);

   function CreateMBeanServer return access Javax.Management.MBeanServer.Typ'Class;

   function CreateMBeanServer (P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Javax.Management.MBeanServer.Typ'Class;

   function NewMBeanServer return access Javax.Management.MBeanServer.Typ'Class;

   function NewMBeanServer (P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Javax.Management.MBeanServer.Typ'Class;

   --  synchronized
   function FindMBeanServer (P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Util.ArrayList.Typ'Class;

   function GetClassLoaderRepository (P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class)
                                      return access Javax.Management.Loading.ClassLoaderRepository.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ReleaseMBeanServer, "releaseMBeanServer");
   pragma Import (Java, CreateMBeanServer, "createMBeanServer");
   pragma Import (Java, NewMBeanServer, "newMBeanServer");
   pragma Import (Java, FindMBeanServer, "findMBeanServer");
   pragma Import (Java, GetClassLoaderRepository, "getClassLoaderRepository");

end Javax.Management.MBeanServerFactory;
pragma Import (Java, Javax.Management.MBeanServerFactory, "javax.management.MBeanServerFactory");
pragma Extensions_Allowed (Off);
