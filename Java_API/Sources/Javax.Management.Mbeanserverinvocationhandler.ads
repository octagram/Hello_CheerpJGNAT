pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.Reflect.Method;
limited with Javax.Management.MBeanServerConnection;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Java.Lang.Reflect.InvocationHandler;

package Javax.Management.MBeanServerInvocationHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(InvocationHandler_I : Java.Lang.Reflect.InvocationHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanServerInvocationHandler (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                                              P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   function New_MBeanServerInvocationHandler (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                                              P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                              P3_Boolean : Java.Boolean; 
                                              This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMBeanServerConnection (This : access Typ)
                                      return access Javax.Management.MBeanServerConnection.Typ'Class;

   function GetObjectName (This : access Typ)
                           return access Javax.Management.ObjectName.Typ'Class;

   function IsMXBean (This : access Typ)
                      return Java.Boolean;

   function NewProxyInstance (P1_MBeanServerConnection : access Standard.Javax.Management.MBeanServerConnection.Typ'Class;
                              P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                              P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                              P4_Boolean : Java.Boolean)
                              return access Java.Lang.Object.Typ'Class;

   function Invoke (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Throwable.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanServerInvocationHandler);
   pragma Import (Java, GetMBeanServerConnection, "getMBeanServerConnection");
   pragma Import (Java, GetObjectName, "getObjectName");
   pragma Import (Java, IsMXBean, "isMXBean");
   pragma Import (Java, NewProxyInstance, "newProxyInstance");
   pragma Import (Java, Invoke, "invoke");

end Javax.Management.MBeanServerInvocationHandler;
pragma Import (Java, Javax.Management.MBeanServerInvocationHandler, "javax.management.MBeanServerInvocationHandler");
pragma Extensions_Allowed (Off);
