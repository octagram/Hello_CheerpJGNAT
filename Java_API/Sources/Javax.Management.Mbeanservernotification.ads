pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.ObjectName;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Notification;

package Javax.Management.MBeanServerNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Notification.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanServerNotification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                         P3_Long : Java.Long;
                                         P4_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMBeanName (This : access Typ)
                          return access Javax.Management.ObjectName.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   REGISTRATION_NOTIFICATION : constant access Java.Lang.String.Typ'Class;

   --  final
   UNREGISTRATION_NOTIFICATION : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanServerNotification);
   pragma Import (Java, GetMBeanName, "getMBeanName");
   pragma Import (Java, REGISTRATION_NOTIFICATION, "REGISTRATION_NOTIFICATION");
   pragma Import (Java, UNREGISTRATION_NOTIFICATION, "UNREGISTRATION_NOTIFICATION");

end Javax.Management.MBeanServerNotification;
pragma Import (Java, Javax.Management.MBeanServerNotification, "javax.management.MBeanServerNotification");
pragma Extensions_Allowed (Off);
