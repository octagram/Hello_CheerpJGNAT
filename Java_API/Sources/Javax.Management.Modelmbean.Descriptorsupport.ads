pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Management.Descriptor;

package Javax.Management.Modelmbean.DescriptorSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Descriptor_I : Javax.Management.Descriptor.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DescriptorSupport (This : Ref := null)
                                   return Ref;

   function New_DescriptorSupport (P1_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function New_DescriptorSupport (P1_DescriptorSupport : access Standard.Javax.Management.Modelmbean.DescriptorSupport.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_DescriptorSupport (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Javax.Management.Modelmbean.XMLParseException.Except

   function New_DescriptorSupport (P1_String_Arr : access Java.Lang.String.Arr_Obj;
                                   P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   function New_DescriptorSupport (P1_String_Arr : access Java.Lang.String.Arr_Obj; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetFieldValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   --  synchronized
   procedure SetField (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.RuntimeOperationsException.Except

   --  synchronized
   function GetFields (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetFieldNames (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetFieldValues (This : access Typ;
                            P1_String_Arr : access Java.Lang.String.Arr_Obj)
                            return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure SetFields (This : access Typ;
                        P1_String_Arr : access Java.Lang.String.Arr_Obj;
                        P2_Object_Arr : access Java.Lang.Object.Arr_Obj);
   --  can raise Javax.Management.RuntimeOperationsException.Except

   --  synchronized
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   --  synchronized
   procedure RemoveField (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  synchronized
   function HashCode (This : access Typ)
                      return Java.Int;

   --  synchronized
   function IsValid (This : access Typ)
                     return Java.Boolean;
   --  can raise Javax.Management.RuntimeOperationsException.Except

   --  synchronized
   function ToXMLString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  synchronized
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DescriptorSupport);
   pragma Import (Java, GetFieldValue, "getFieldValue");
   pragma Import (Java, SetField, "setField");
   pragma Import (Java, GetFields, "getFields");
   pragma Import (Java, GetFieldNames, "getFieldNames");
   pragma Import (Java, GetFieldValues, "getFieldValues");
   pragma Import (Java, SetFields, "setFields");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, RemoveField, "removeField");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, IsValid, "isValid");
   pragma Import (Java, ToXMLString, "toXMLString");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Modelmbean.DescriptorSupport;
pragma Import (Java, Javax.Management.Modelmbean.DescriptorSupport, "javax.management.modelmbean.DescriptorSupport");
pragma Extensions_Allowed (Off);
