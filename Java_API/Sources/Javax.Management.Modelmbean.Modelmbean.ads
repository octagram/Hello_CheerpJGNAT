pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Modelmbean.ModelMBeanInfo;
with Java.Lang.Object;
with Javax.Management.DynamicMBean;
with Javax.Management.Modelmbean.ModelMBeanNotificationBroadcaster;
with Javax.Management.PersistentMBean;

package Javax.Management.Modelmbean.ModelMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynamicMBean_I : Javax.Management.DynamicMBean.Ref;
            PersistentMBean_I : Javax.Management.PersistentMBean.Ref;
            ModelMBeanNotificationBroadcaster_I : Javax.Management.Modelmbean.ModelMBeanNotificationBroadcaster.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetModelMBeanInfo (This : access Typ;
                                P1_ModelMBeanInfo : access Standard.Javax.Management.Modelmbean.ModelMBeanInfo.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SetManagedResource (This : access Typ;
                                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.Modelmbean.InvalidTargetObjectTypeException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetModelMBeanInfo, "setModelMBeanInfo");
   pragma Export (Java, SetManagedResource, "setManagedResource");

end Javax.Management.Modelmbean.ModelMBean;
pragma Import (Java, Javax.Management.Modelmbean.ModelMBean, "javax.management.modelmbean.ModelMBean");
pragma Extensions_Allowed (Off);
