pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
limited with Javax.Management.MBeanAttributeInfo;
limited with Javax.Management.MBeanConstructorInfo;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.MBeanOperationInfo;
limited with Javax.Management.Modelmbean.ModelMBeanAttributeInfo;
limited with Javax.Management.Modelmbean.ModelMBeanNotificationInfo;
limited with Javax.Management.Modelmbean.ModelMBeanOperationInfo;
with Java.Lang.Object;

package Javax.Management.Modelmbean.ModelMBeanInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDescriptors (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SetDescriptors (This : access Typ;
                             P1_Descriptor_Arr : access Javax.Management.Descriptor.Arr_Obj) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetDescriptor (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Management.Descriptor.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SetDescriptor (This : access Typ;
                            P1_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetMBeanDescriptor (This : access Typ)
                                return access Javax.Management.Descriptor.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SetMBeanDescriptor (This : access Typ;
                                 P1_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Management.Modelmbean.ModelMBeanAttributeInfo.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetOperation (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Management.Modelmbean.ModelMBeanOperationInfo.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Management.Modelmbean.ModelMBeanNotificationInfo.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function GetAttributes (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetConstructors (This : access Typ)
                             return Standard.Java.Lang.Object.Ref is abstract;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetNotifications (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function GetOperations (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDescriptors, "getDescriptors");
   pragma Export (Java, SetDescriptors, "setDescriptors");
   pragma Export (Java, GetDescriptor, "getDescriptor");
   pragma Export (Java, SetDescriptor, "setDescriptor");
   pragma Export (Java, GetMBeanDescriptor, "getMBeanDescriptor");
   pragma Export (Java, SetMBeanDescriptor, "setMBeanDescriptor");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, GetOperation, "getOperation");
   pragma Export (Java, GetNotification, "getNotification");
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetClassName, "getClassName");
   pragma Export (Java, GetConstructors, "getConstructors");
   pragma Export (Java, GetDescription, "getDescription");
   pragma Export (Java, GetNotifications, "getNotifications");
   pragma Export (Java, GetOperations, "getOperations");

end Javax.Management.Modelmbean.ModelMBeanInfo;
pragma Import (Java, Javax.Management.Modelmbean.ModelMBeanInfo, "javax.management.modelmbean.ModelMBeanInfo");
pragma Extensions_Allowed (Off);
