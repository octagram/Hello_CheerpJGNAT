pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
limited with Javax.Management.Modelmbean.ModelMBeanAttributeInfo;
limited with Javax.Management.Modelmbean.ModelMBeanConstructorInfo;
limited with Javax.Management.Modelmbean.ModelMBeanNotificationInfo;
limited with Javax.Management.Modelmbean.ModelMBeanOperationInfo;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;
with Javax.Management.MBeanInfo;
with Javax.Management.Modelmbean.ModelMBeanInfo;

package Javax.Management.Modelmbean.ModelMBeanInfoSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref;
            ModelMBeanInfo_I : Javax.Management.Modelmbean.ModelMBeanInfo.Ref)
    is new Javax.Management.MBeanInfo.Typ(Serializable_I,
                                          Cloneable_I,
                                          DescriptorRead_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ModelMBeanInfoSupport (P1_ModelMBeanInfo : access Standard.Javax.Management.Modelmbean.ModelMBeanInfo.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_ModelMBeanInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                                       P3_ModelMBeanAttributeInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanAttributeInfo.Arr_Obj;
                                       P4_ModelMBeanConstructorInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanConstructorInfo.Arr_Obj;
                                       P5_ModelMBeanOperationInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanOperationInfo.Arr_Obj;
                                       P6_ModelMBeanNotificationInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanNotificationInfo.Arr_Obj; 
                                       This : Ref := null)
                                       return Ref;

   function New_ModelMBeanInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                                       P3_ModelMBeanAttributeInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanAttributeInfo.Arr_Obj;
                                       P4_ModelMBeanConstructorInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanConstructorInfo.Arr_Obj;
                                       P5_ModelMBeanOperationInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanOperationInfo.Arr_Obj;
                                       P6_ModelMBeanNotificationInfo_Arr : access Javax.Management.Modelmbean.ModelMBeanNotificationInfo.Arr_Obj;
                                       P7_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetDescriptors (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Standard.Java.Lang.Object.Ref;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SetDescriptors (This : access Typ;
                             P1_Descriptor_Arr : access Javax.Management.Descriptor.Arr_Obj);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetDescriptor (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Management.Descriptor.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetDescriptor (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Management.Descriptor.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SetDescriptor (This : access Typ;
                            P1_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Management.Modelmbean.ModelMBeanAttributeInfo.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetOperation (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Management.Modelmbean.ModelMBeanOperationInfo.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetConstructor (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Javax.Management.Modelmbean.ModelMBeanConstructorInfo.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Management.Modelmbean.ModelMBeanNotificationInfo.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetDescriptor (This : access Typ)
                           return access Javax.Management.Descriptor.Typ'Class;

   function GetMBeanDescriptor (This : access Typ)
                                return access Javax.Management.Descriptor.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except

   procedure SetMBeanDescriptor (This : access Typ;
                                 P1_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ModelMBeanInfoSupport);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetDescriptors, "getDescriptors");
   pragma Import (Java, SetDescriptors, "setDescriptors");
   pragma Import (Java, GetDescriptor, "getDescriptor");
   pragma Import (Java, SetDescriptor, "setDescriptor");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetOperation, "getOperation");
   pragma Import (Java, GetConstructor, "getConstructor");
   pragma Import (Java, GetNotification, "getNotification");
   pragma Import (Java, GetMBeanDescriptor, "getMBeanDescriptor");
   pragma Import (Java, SetMBeanDescriptor, "setMBeanDescriptor");

end Javax.Management.Modelmbean.ModelMBeanInfoSupport;
pragma Import (Java, Javax.Management.Modelmbean.ModelMBeanInfoSupport, "javax.management.modelmbean.ModelMBeanInfoSupport");
pragma Extensions_Allowed (Off);
