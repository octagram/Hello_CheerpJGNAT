pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Attribute;
limited with Javax.Management.AttributeChangeNotification;
limited with Javax.Management.Notification;
limited with Javax.Management.NotificationListener;
with Java.Lang.Object;
with Javax.Management.NotificationBroadcaster;

package Javax.Management.Modelmbean.ModelMBeanNotificationBroadcaster is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            NotificationBroadcaster_I : Javax.Management.NotificationBroadcaster.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SendNotification (This : access Typ;
                               P1_Notification : access Standard.Javax.Management.Notification.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SendNotification (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SendAttributeChangeNotification (This : access Typ;
                                              P1_AttributeChangeNotification : access Standard.Javax.Management.AttributeChangeNotification.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SendAttributeChangeNotification (This : access Typ;
                                              P1_Attribute : access Standard.Javax.Management.Attribute.Typ'Class;
                                              P2_Attribute : access Standard.Javax.Management.Attribute.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure AddAttributeChangeNotificationListener (This : access Typ;
                                                     P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                                     P3_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Java.Lang.IllegalArgumentException.Except

   procedure RemoveAttributeChangeNotificationListener (This : access Typ;
                                                        P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Javax.Management.ListenerNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SendNotification, "sendNotification");
   pragma Export (Java, SendAttributeChangeNotification, "sendAttributeChangeNotification");
   pragma Export (Java, AddAttributeChangeNotificationListener, "addAttributeChangeNotificationListener");
   pragma Export (Java, RemoveAttributeChangeNotificationListener, "removeAttributeChangeNotificationListener");

end Javax.Management.Modelmbean.ModelMBeanNotificationBroadcaster;
pragma Import (Java, Javax.Management.Modelmbean.ModelMBeanNotificationBroadcaster, "javax.management.modelmbean.ModelMBeanNotificationBroadcaster");
pragma Extensions_Allowed (Off);
