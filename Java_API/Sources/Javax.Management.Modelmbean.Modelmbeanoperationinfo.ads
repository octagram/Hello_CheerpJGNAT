pragma Extensions_Allowed (On);
limited with Java.Lang.Reflect.Method;
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
limited with Javax.Management.MBeanParameterInfo;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Management.DescriptorAccess;
with Javax.Management.DescriptorRead;
with Javax.Management.MBeanOperationInfo;

package Javax.Management.Modelmbean.ModelMBeanOperationInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DescriptorAccess_I : Javax.Management.DescriptorAccess.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref)
    is new Javax.Management.MBeanOperationInfo.Typ(Serializable_I,
                                                   Cloneable_I,
                                                   DescriptorRead_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ModelMBeanOperationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_ModelMBeanOperationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                                         P3_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_ModelMBeanOperationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                                         P3_MBeanParameterInfo_Arr : access Javax.Management.MBeanParameterInfo.Arr_Obj;
                                         P4_String : access Standard.Java.Lang.String.Typ'Class;
                                         P5_Int : Java.Int; 
                                         This : Ref := null)
                                         return Ref;

   function New_ModelMBeanOperationInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                                         P3_MBeanParameterInfo_Arr : access Javax.Management.MBeanParameterInfo.Arr_Obj;
                                         P4_String : access Standard.Java.Lang.String.Typ'Class;
                                         P5_Int : Java.Int;
                                         P6_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_ModelMBeanOperationInfo (P1_ModelMBeanOperationInfo : access Standard.Javax.Management.Modelmbean.ModelMBeanOperationInfo.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetDescriptor (This : access Typ)
                           return access Javax.Management.Descriptor.Typ'Class;

   procedure SetDescriptor (This : access Typ;
                            P1_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ModelMBeanOperationInfo);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetDescriptor, "getDescriptor");
   pragma Import (Java, SetDescriptor, "setDescriptor");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Modelmbean.ModelMBeanOperationInfo;
pragma Import (Java, Javax.Management.Modelmbean.ModelMBeanOperationInfo, "javax.management.modelmbean.ModelMBeanOperationInfo");
pragma Extensions_Allowed (Off);
