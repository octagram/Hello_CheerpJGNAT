pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.String;
limited with Javax.Management.Attribute;
limited with Javax.Management.AttributeChangeNotification;
limited with Javax.Management.AttributeList;
limited with Javax.Management.Loading.ClassLoaderRepository;
limited with Javax.Management.MBeanInfo;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.Modelmbean.ModelMBeanInfo;
limited with Javax.Management.Notification;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.Modelmbean.ModelMBean;
with Javax.Management.NotificationEmitter;

package Javax.Management.Modelmbean.RequiredModelMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            ModelMBean_I : Javax.Management.Modelmbean.ModelMBean.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RequiredModelMBean (This : Ref := null)
                                    return Ref;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function New_RequiredModelMBean (P1_ModelMBeanInfo : access Standard.Javax.Management.Modelmbean.ModelMBeanInfo.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetModelMBeanInfo (This : access Typ;
                                P1_ModelMBeanInfo : access Standard.Javax.Management.Modelmbean.ModelMBeanInfo.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SetManagedResource (This : access Typ;
                                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.Modelmbean.InvalidTargetObjectTypeException.Except

   procedure Load (This : access Typ);
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Javax.Management.InstanceNotFoundException.Except

   procedure Store (This : access Typ);
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Javax.Management.InstanceNotFoundException.Except

   function GetMBeanInfo (This : access Typ)
                          return access Javax.Management.MBeanInfo.Typ'Class;

   function Invoke (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P3_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetAttributes (This : access Typ;
                           P1_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Management.AttributeList.Typ'Class;

   procedure SetAttribute (This : access Typ;
                           P1_Attribute : access Standard.Javax.Management.Attribute.Typ'Class);
   --  can raise Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InvalidAttributeValueException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function SetAttributes (This : access Typ;
                           P1_AttributeList : access Standard.Javax.Management.AttributeList.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class;

   procedure AddNotificationListener (This : access Typ;
                                      P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                      P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                      P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                         P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                         P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure SendNotification (This : access Typ;
                               P1_Notification : access Standard.Javax.Management.Notification.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SendNotification (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   procedure AddAttributeChangeNotificationListener (This : access Typ;
                                                     P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                                     P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Java.Lang.IllegalArgumentException.Except

   procedure RemoveAttributeChangeNotificationListener (This : access Typ;
                                                        P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                        P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Javax.Management.ListenerNotFoundException.Except

   procedure SendAttributeChangeNotification (This : access Typ;
                                              P1_AttributeChangeNotification : access Standard.Javax.Management.AttributeChangeNotification.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   procedure SendAttributeChangeNotification (This : access Typ;
                                              P1_Attribute : access Standard.Javax.Management.Attribute.Typ'Class;
                                              P2_Attribute : access Standard.Javax.Management.Attribute.Typ'Class);
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.RuntimeOperationsException.Except

   --  protected
   function GetClassLoaderRepository (This : access Typ)
                                      return access Javax.Management.Loading.ClassLoaderRepository.Typ'Class;

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RequiredModelMBean);
   pragma Import (Java, SetModelMBeanInfo, "setModelMBeanInfo");
   pragma Import (Java, SetManagedResource, "setManagedResource");
   pragma Import (Java, Load, "load");
   pragma Import (Java, Store, "store");
   pragma Import (Java, GetMBeanInfo, "getMBeanInfo");
   pragma Import (Java, Invoke, "invoke");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, SetAttribute, "setAttribute");
   pragma Import (Java, SetAttributes, "setAttributes");
   pragma Import (Java, AddNotificationListener, "addNotificationListener");
   pragma Import (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Import (Java, SendNotification, "sendNotification");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");
   pragma Import (Java, AddAttributeChangeNotificationListener, "addAttributeChangeNotificationListener");
   pragma Import (Java, RemoveAttributeChangeNotificationListener, "removeAttributeChangeNotificationListener");
   pragma Import (Java, SendAttributeChangeNotification, "sendAttributeChangeNotification");
   pragma Import (Java, GetClassLoaderRepository, "getClassLoaderRepository");
   pragma Import (Java, PreRegister, "preRegister");
   pragma Import (Java, PostRegister, "postRegister");
   pragma Import (Java, PreDeregister, "preDeregister");
   pragma Import (Java, PostDeregister, "postDeregister");

end Javax.Management.Modelmbean.RequiredModelMBean;
pragma Import (Java, Javax.Management.Modelmbean.RequiredModelMBean, "javax.management.modelmbean.RequiredModelMBean");
pragma Extensions_Allowed (Off);
