pragma Extensions_Allowed (On);
package Javax.Management.Modelmbean is
   pragma Preelaborate;
end Javax.Management.Modelmbean;
pragma Import (Java, Javax.Management.Modelmbean, "javax.management.modelmbean");
pragma Extensions_Allowed (Off);
