pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.Monitor.MonitorMBean;

package Javax.Management.Monitor.CounterMonitorMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MonitorMBean_I : Javax.Management.Monitor.MonitorMBean.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDerivedGauge (This : access Typ;
                             P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                             return access Java.Lang.Number.Typ'Class is abstract;

   function GetDerivedGaugeTimeStamp (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                                      return Java.Long is abstract;

   function GetThreshold (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                          return access Java.Lang.Number.Typ'Class is abstract;

   function GetInitThreshold (This : access Typ)
                              return access Java.Lang.Number.Typ'Class is abstract;

   procedure SetInitThreshold (This : access Typ;
                               P1_Number : access Standard.Java.Lang.Number.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetOffset (This : access Typ)
                       return access Java.Lang.Number.Typ'Class is abstract;

   procedure SetOffset (This : access Typ;
                        P1_Number : access Standard.Java.Lang.Number.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetModulus (This : access Typ)
                        return access Java.Lang.Number.Typ'Class is abstract;

   procedure SetModulus (This : access Typ;
                         P1_Number : access Standard.Java.Lang.Number.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetNotify (This : access Typ)
                       return Java.Boolean is abstract;

   procedure SetNotify (This : access Typ;
                        P1_Boolean : Java.Boolean) is abstract;

   function GetDifferenceMode (This : access Typ)
                               return Java.Boolean is abstract;

   procedure SetDifferenceMode (This : access Typ;
                                P1_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDerivedGauge, "getDerivedGauge");
   pragma Export (Java, GetDerivedGaugeTimeStamp, "getDerivedGaugeTimeStamp");
   pragma Export (Java, GetThreshold, "getThreshold");
   pragma Export (Java, GetInitThreshold, "getInitThreshold");
   pragma Export (Java, SetInitThreshold, "setInitThreshold");
   pragma Export (Java, GetOffset, "getOffset");
   pragma Export (Java, SetOffset, "setOffset");
   pragma Export (Java, GetModulus, "getModulus");
   pragma Export (Java, SetModulus, "setModulus");
   pragma Export (Java, GetNotify, "getNotify");
   pragma Export (Java, SetNotify, "setNotify");
   pragma Export (Java, GetDifferenceMode, "getDifferenceMode");
   pragma Export (Java, SetDifferenceMode, "setDifferenceMode");

end Javax.Management.Monitor.CounterMonitorMBean;
pragma Import (Java, Javax.Management.Monitor.CounterMonitorMBean, "javax.management.monitor.CounterMonitorMBean");
pragma Extensions_Allowed (Off);
