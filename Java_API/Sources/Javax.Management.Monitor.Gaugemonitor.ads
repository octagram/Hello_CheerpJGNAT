pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.Monitor.GaugeMonitorMBean;
with Javax.Management.Monitor.Monitor;
with Javax.Management.Monitor.MonitorMBean;
with Javax.Management.NotificationEmitter;

package Javax.Management.Monitor.GaugeMonitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            GaugeMonitorMBean_I : Javax.Management.Monitor.GaugeMonitorMBean.Ref;
            MonitorMBean_I : Javax.Management.Monitor.MonitorMBean.Ref)
    is new Javax.Management.Monitor.Monitor.Typ(MBeanRegistration_I,
                                                NotificationEmitter_I,
                                                MonitorMBean_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GaugeMonitor (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Start (This : access Typ);

   --  synchronized
   procedure Stop (This : access Typ);

   --  synchronized
   function GetDerivedGauge (This : access Typ;
                             P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                             return access Java.Lang.Number.Typ'Class;

   --  synchronized
   function GetDerivedGaugeTimeStamp (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                                      return Java.Long;

   --  synchronized
   function GetHighThreshold (This : access Typ)
                              return access Java.Lang.Number.Typ'Class;

   --  synchronized
   function GetLowThreshold (This : access Typ)
                             return access Java.Lang.Number.Typ'Class;

   --  synchronized
   procedure SetThresholds (This : access Typ;
                            P1_Number : access Standard.Java.Lang.Number.Typ'Class;
                            P2_Number : access Standard.Java.Lang.Number.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function GetNotifyHigh (This : access Typ)
                           return Java.Boolean;

   --  synchronized
   procedure SetNotifyHigh (This : access Typ;
                            P1_Boolean : Java.Boolean);

   --  synchronized
   function GetNotifyLow (This : access Typ)
                          return Java.Boolean;

   --  synchronized
   procedure SetNotifyLow (This : access Typ;
                           P1_Boolean : Java.Boolean);

   --  synchronized
   function GetDifferenceMode (This : access Typ)
                               return Java.Boolean;

   --  synchronized
   procedure SetDifferenceMode (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   function GetDerivedGauge (This : access Typ;
                             P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GaugeMonitor);
   pragma Import (Java, Start, "start");
   pragma Import (Java, Stop, "stop");
   pragma Import (Java, GetDerivedGauge, "getDerivedGauge");
   pragma Import (Java, GetDerivedGaugeTimeStamp, "getDerivedGaugeTimeStamp");
   pragma Import (Java, GetHighThreshold, "getHighThreshold");
   pragma Import (Java, GetLowThreshold, "getLowThreshold");
   pragma Import (Java, SetThresholds, "setThresholds");
   pragma Import (Java, GetNotifyHigh, "getNotifyHigh");
   pragma Import (Java, SetNotifyHigh, "setNotifyHigh");
   pragma Import (Java, GetNotifyLow, "getNotifyLow");
   pragma Import (Java, SetNotifyLow, "setNotifyLow");
   pragma Import (Java, GetDifferenceMode, "getDifferenceMode");
   pragma Import (Java, SetDifferenceMode, "setDifferenceMode");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");

end Javax.Management.Monitor.GaugeMonitor;
pragma Import (Java, Javax.Management.Monitor.GaugeMonitor, "javax.management.monitor.GaugeMonitor");
pragma Extensions_Allowed (Off);
