pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.Monitor.MonitorMBean;

package Javax.Management.Monitor.GaugeMonitorMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MonitorMBean_I : Javax.Management.Monitor.MonitorMBean.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDerivedGauge (This : access Typ;
                             P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                             return access Java.Lang.Number.Typ'Class is abstract;

   function GetDerivedGaugeTimeStamp (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                                      return Java.Long is abstract;

   function GetHighThreshold (This : access Typ)
                              return access Java.Lang.Number.Typ'Class is abstract;

   function GetLowThreshold (This : access Typ)
                             return access Java.Lang.Number.Typ'Class is abstract;

   procedure SetThresholds (This : access Typ;
                            P1_Number : access Standard.Java.Lang.Number.Typ'Class;
                            P2_Number : access Standard.Java.Lang.Number.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetNotifyHigh (This : access Typ)
                           return Java.Boolean is abstract;

   procedure SetNotifyHigh (This : access Typ;
                            P1_Boolean : Java.Boolean) is abstract;

   function GetNotifyLow (This : access Typ)
                          return Java.Boolean is abstract;

   procedure SetNotifyLow (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;

   function GetDifferenceMode (This : access Typ)
                               return Java.Boolean is abstract;

   procedure SetDifferenceMode (This : access Typ;
                                P1_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDerivedGauge, "getDerivedGauge");
   pragma Export (Java, GetDerivedGaugeTimeStamp, "getDerivedGaugeTimeStamp");
   pragma Export (Java, GetHighThreshold, "getHighThreshold");
   pragma Export (Java, GetLowThreshold, "getLowThreshold");
   pragma Export (Java, SetThresholds, "setThresholds");
   pragma Export (Java, GetNotifyHigh, "getNotifyHigh");
   pragma Export (Java, SetNotifyHigh, "setNotifyHigh");
   pragma Export (Java, GetNotifyLow, "getNotifyLow");
   pragma Export (Java, SetNotifyLow, "setNotifyLow");
   pragma Export (Java, GetDifferenceMode, "getDifferenceMode");
   pragma Export (Java, SetDifferenceMode, "setDifferenceMode");

end Javax.Management.Monitor.GaugeMonitorMBean;
pragma Import (Java, Javax.Management.Monitor.GaugeMonitorMBean, "javax.management.monitor.GaugeMonitorMBean");
pragma Extensions_Allowed (Off);
