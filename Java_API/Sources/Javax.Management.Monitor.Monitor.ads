pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.String;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.Monitor.MonitorMBean;
with Javax.Management.NotificationBroadcasterSupport;
with Javax.Management.NotificationEmitter;

package Javax.Management.Monitor.Monitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            MonitorMBean_I : Javax.Management.Monitor.MonitorMBean.Ref)
    is abstract new Javax.Management.NotificationBroadcasterSupport.Typ(NotificationEmitter_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ElementCount : Java.Int;
      pragma Import (Java, ElementCount, "elementCount");

      --  protected
      AlreadyNotifieds : Java.Int_Arr;
      pragma Import (Java, AlreadyNotifieds, "alreadyNotifieds");

      --  protected
      Server : access Javax.Management.MBeanServer.Typ'Class;
      pragma Import (Java, Server, "server");

   end record;

   function New_Monitor (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);

   procedure Start (This : access Typ) is abstract;

   procedure Stop (This : access Typ) is abstract;

   --  synchronized
   procedure AddObservedObject (This : access Typ;
                                P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   procedure RemoveObservedObject (This : access Typ;
                                   P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class);

   --  synchronized
   function ContainsObservedObject (This : access Typ;
                                    P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                                    return Java.Boolean;

   --  synchronized
   function GetObservedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function GetObservedAttribute (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   procedure SetObservedAttribute (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function GetGranularityPeriod (This : access Typ)
                                  return Java.Long;

   --  synchronized
   procedure SetGranularityPeriod (This : access Typ;
                                   P1_Long : Java.Long);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function IsActive (This : access Typ)
                      return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   CapacityIncrement : constant Java.Int;

   --  protected  final
   RESET_FLAGS_ALREADY_NOTIFIED : constant Java.Int;

   --  protected  final
   OBSERVED_OBJECT_ERROR_NOTIFIED : constant Java.Int;

   --  protected  final
   OBSERVED_ATTRIBUTE_ERROR_NOTIFIED : constant Java.Int;

   --  protected  final
   OBSERVED_ATTRIBUTE_TYPE_ERROR_NOTIFIED : constant Java.Int;

   --  protected  final
   RUNTIME_ERROR_NOTIFIED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Monitor);
   pragma Export (Java, PreRegister, "preRegister");
   pragma Export (Java, PostRegister, "postRegister");
   pragma Export (Java, PreDeregister, "preDeregister");
   pragma Export (Java, PostDeregister, "postDeregister");
   pragma Export (Java, Start, "start");
   pragma Export (Java, Stop, "stop");
   pragma Export (Java, AddObservedObject, "addObservedObject");
   pragma Export (Java, RemoveObservedObject, "removeObservedObject");
   pragma Export (Java, ContainsObservedObject, "containsObservedObject");
   pragma Export (Java, GetObservedObjects, "getObservedObjects");
   pragma Export (Java, GetObservedAttribute, "getObservedAttribute");
   pragma Export (Java, SetObservedAttribute, "setObservedAttribute");
   pragma Export (Java, GetGranularityPeriod, "getGranularityPeriod");
   pragma Export (Java, SetGranularityPeriod, "setGranularityPeriod");
   pragma Export (Java, IsActive, "isActive");
   pragma Import (Java, CapacityIncrement, "capacityIncrement");
   pragma Import (Java, RESET_FLAGS_ALREADY_NOTIFIED, "RESET_FLAGS_ALREADY_NOTIFIED");
   pragma Import (Java, OBSERVED_OBJECT_ERROR_NOTIFIED, "OBSERVED_OBJECT_ERROR_NOTIFIED");
   pragma Import (Java, OBSERVED_ATTRIBUTE_ERROR_NOTIFIED, "OBSERVED_ATTRIBUTE_ERROR_NOTIFIED");
   pragma Import (Java, OBSERVED_ATTRIBUTE_TYPE_ERROR_NOTIFIED, "OBSERVED_ATTRIBUTE_TYPE_ERROR_NOTIFIED");
   pragma Import (Java, RUNTIME_ERROR_NOTIFIED, "RUNTIME_ERROR_NOTIFIED");

end Javax.Management.Monitor.Monitor;
pragma Import (Java, Javax.Management.Monitor.Monitor, "javax.management.monitor.Monitor");
pragma Extensions_Allowed (Off);
