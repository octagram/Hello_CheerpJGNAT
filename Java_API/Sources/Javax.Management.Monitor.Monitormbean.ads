pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;

package Javax.Management.Monitor.MonitorMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Start (This : access Typ) is abstract;

   procedure Stop (This : access Typ) is abstract;

   procedure AddObservedObject (This : access Typ;
                                P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure RemoveObservedObject (This : access Typ;
                                   P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class) is abstract;

   function ContainsObservedObject (This : access Typ;
                                    P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                                    return Java.Boolean is abstract;

   function GetObservedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;

   function GetObservedAttribute (This : access Typ)
                                  return access Java.Lang.String.Typ'Class is abstract;

   procedure SetObservedAttribute (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetGranularityPeriod (This : access Typ)
                                  return Java.Long is abstract;

   procedure SetGranularityPeriod (This : access Typ;
                                   P1_Long : Java.Long) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function IsActive (This : access Typ)
                      return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Start, "start");
   pragma Export (Java, Stop, "stop");
   pragma Export (Java, AddObservedObject, "addObservedObject");
   pragma Export (Java, RemoveObservedObject, "removeObservedObject");
   pragma Export (Java, ContainsObservedObject, "containsObservedObject");
   pragma Export (Java, GetObservedObjects, "getObservedObjects");
   pragma Export (Java, GetObservedAttribute, "getObservedAttribute");
   pragma Export (Java, SetObservedAttribute, "setObservedAttribute");
   pragma Export (Java, GetGranularityPeriod, "getGranularityPeriod");
   pragma Export (Java, SetGranularityPeriod, "setGranularityPeriod");
   pragma Export (Java, IsActive, "isActive");

end Javax.Management.Monitor.MonitorMBean;
pragma Import (Java, Javax.Management.Monitor.MonitorMBean, "javax.management.monitor.MonitorMBean");
pragma Extensions_Allowed (Off);
