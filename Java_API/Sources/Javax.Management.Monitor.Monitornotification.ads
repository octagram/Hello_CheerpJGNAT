pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.ObjectName;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Notification;

package Javax.Management.Monitor.MonitorNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Notification.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetObservedObject (This : access Typ)
                               return access Javax.Management.ObjectName.Typ'Class;

   function GetObservedAttribute (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   function GetDerivedGauge (This : access Typ)
                             return access Java.Lang.Object.Typ'Class;

   function GetTrigger (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OBSERVED_OBJECT_ERROR : constant access Java.Lang.String.Typ'Class;

   --  final
   OBSERVED_ATTRIBUTE_ERROR : constant access Java.Lang.String.Typ'Class;

   --  final
   OBSERVED_ATTRIBUTE_TYPE_ERROR : constant access Java.Lang.String.Typ'Class;

   --  final
   THRESHOLD_ERROR : constant access Java.Lang.String.Typ'Class;

   --  final
   RUNTIME_ERROR : constant access Java.Lang.String.Typ'Class;

   --  final
   THRESHOLD_VALUE_EXCEEDED : constant access Java.Lang.String.Typ'Class;

   --  final
   THRESHOLD_HIGH_VALUE_EXCEEDED : constant access Java.Lang.String.Typ'Class;

   --  final
   THRESHOLD_LOW_VALUE_EXCEEDED : constant access Java.Lang.String.Typ'Class;

   --  final
   STRING_TO_COMPARE_VALUE_MATCHED : constant access Java.Lang.String.Typ'Class;

   --  final
   STRING_TO_COMPARE_VALUE_DIFFERED : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetObservedObject, "getObservedObject");
   pragma Import (Java, GetObservedAttribute, "getObservedAttribute");
   pragma Import (Java, GetDerivedGauge, "getDerivedGauge");
   pragma Import (Java, GetTrigger, "getTrigger");
   pragma Import (Java, OBSERVED_OBJECT_ERROR, "OBSERVED_OBJECT_ERROR");
   pragma Import (Java, OBSERVED_ATTRIBUTE_ERROR, "OBSERVED_ATTRIBUTE_ERROR");
   pragma Import (Java, OBSERVED_ATTRIBUTE_TYPE_ERROR, "OBSERVED_ATTRIBUTE_TYPE_ERROR");
   pragma Import (Java, THRESHOLD_ERROR, "THRESHOLD_ERROR");
   pragma Import (Java, RUNTIME_ERROR, "RUNTIME_ERROR");
   pragma Import (Java, THRESHOLD_VALUE_EXCEEDED, "THRESHOLD_VALUE_EXCEEDED");
   pragma Import (Java, THRESHOLD_HIGH_VALUE_EXCEEDED, "THRESHOLD_HIGH_VALUE_EXCEEDED");
   pragma Import (Java, THRESHOLD_LOW_VALUE_EXCEEDED, "THRESHOLD_LOW_VALUE_EXCEEDED");
   pragma Import (Java, STRING_TO_COMPARE_VALUE_MATCHED, "STRING_TO_COMPARE_VALUE_MATCHED");
   pragma Import (Java, STRING_TO_COMPARE_VALUE_DIFFERED, "STRING_TO_COMPARE_VALUE_DIFFERED");

end Javax.Management.Monitor.MonitorNotification;
pragma Import (Java, Javax.Management.Monitor.MonitorNotification, "javax.management.monitor.MonitorNotification");
pragma Extensions_Allowed (Off);
