pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.Monitor.Monitor;
with Javax.Management.Monitor.MonitorMBean;
with Javax.Management.Monitor.StringMonitorMBean;
with Javax.Management.NotificationEmitter;

package Javax.Management.Monitor.StringMonitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            MonitorMBean_I : Javax.Management.Monitor.MonitorMBean.Ref;
            StringMonitorMBean_I : Javax.Management.Monitor.StringMonitorMBean.Ref)
    is new Javax.Management.Monitor.Monitor.Typ(MBeanRegistration_I,
                                                NotificationEmitter_I,
                                                MonitorMBean_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringMonitor (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure Start (This : access Typ);

   --  synchronized
   procedure Stop (This : access Typ);

   --  synchronized
   function GetDerivedGauge (This : access Typ;
                             P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                             return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetDerivedGaugeTimeStamp (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                                      return Java.Long;

   --  synchronized
   function GetStringToCompare (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   --  synchronized
   procedure SetStringToCompare (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function GetNotifyMatch (This : access Typ)
                            return Java.Boolean;

   --  synchronized
   procedure SetNotifyMatch (This : access Typ;
                             P1_Boolean : Java.Boolean);

   --  synchronized
   function GetNotifyDiffer (This : access Typ)
                             return Java.Boolean;

   --  synchronized
   procedure SetNotifyDiffer (This : access Typ;
                              P1_Boolean : Java.Boolean);

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   function GetDerivedGauge (This : access Typ;
                             P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringMonitor);
   pragma Import (Java, Start, "start");
   pragma Import (Java, Stop, "stop");
   pragma Import (Java, GetDerivedGauge, "getDerivedGauge");
   pragma Import (Java, GetDerivedGaugeTimeStamp, "getDerivedGaugeTimeStamp");
   pragma Import (Java, GetStringToCompare, "getStringToCompare");
   pragma Import (Java, SetStringToCompare, "setStringToCompare");
   pragma Import (Java, GetNotifyMatch, "getNotifyMatch");
   pragma Import (Java, SetNotifyMatch, "setNotifyMatch");
   pragma Import (Java, GetNotifyDiffer, "getNotifyDiffer");
   pragma Import (Java, SetNotifyDiffer, "setNotifyDiffer");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");

end Javax.Management.Monitor.StringMonitor;
pragma Import (Java, Javax.Management.Monitor.StringMonitor, "javax.management.monitor.StringMonitor");
pragma Extensions_Allowed (Off);
