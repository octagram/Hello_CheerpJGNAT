pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.Monitor.MonitorMBean;

package Javax.Management.Monitor.StringMonitorMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MonitorMBean_I : Javax.Management.Monitor.MonitorMBean.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDerivedGauge (This : access Typ;
                             P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetDerivedGaugeTimeStamp (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                                      return Java.Long is abstract;

   function GetStringToCompare (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetStringToCompare (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetNotifyMatch (This : access Typ)
                            return Java.Boolean is abstract;

   procedure SetNotifyMatch (This : access Typ;
                             P1_Boolean : Java.Boolean) is abstract;

   function GetNotifyDiffer (This : access Typ)
                             return Java.Boolean is abstract;

   procedure SetNotifyDiffer (This : access Typ;
                              P1_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDerivedGauge, "getDerivedGauge");
   pragma Export (Java, GetDerivedGaugeTimeStamp, "getDerivedGaugeTimeStamp");
   pragma Export (Java, GetStringToCompare, "getStringToCompare");
   pragma Export (Java, SetStringToCompare, "setStringToCompare");
   pragma Export (Java, GetNotifyMatch, "getNotifyMatch");
   pragma Export (Java, SetNotifyMatch, "setNotifyMatch");
   pragma Export (Java, GetNotifyDiffer, "getNotifyDiffer");
   pragma Export (Java, SetNotifyDiffer, "setNotifyDiffer");

end Javax.Management.Monitor.StringMonitorMBean;
pragma Import (Java, Javax.Management.Monitor.StringMonitorMBean, "javax.management.monitor.StringMonitorMBean");
pragma Extensions_Allowed (Off);
