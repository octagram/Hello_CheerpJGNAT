pragma Extensions_Allowed (On);
package Javax.Management.Monitor is
   pragma Preelaborate;
end Javax.Management.Monitor;
pragma Import (Java, Javax.Management.Monitor, "javax.management.monitor");
pragma Extensions_Allowed (Off);
