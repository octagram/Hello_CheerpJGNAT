pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Management.Notification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Source_Field : access Java.Lang.Object.Typ'Class; -- "Source" conflicts with declaration at java.util.eventobject.ads:35
      pragma Import (Java, Source_Field, "source");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Notification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_Long : Java.Long; 
                              This : Ref := null)
                              return Ref;

   function New_Notification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_Long : Java.Long;
                              P4_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_Notification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_Long : Java.Long;
                              P4_Long : Java.Long; 
                              This : Ref := null)
                              return Ref;

   function New_Notification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_Long : Java.Long;
                              P4_Long : Java.Long;
                              P5_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSource (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetSequenceNumber (This : access Typ)
                               return Java.Long;

   procedure SetSequenceNumber (This : access Typ;
                                P1_Long : Java.Long);

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetTimeStamp (This : access Typ)
                          return Java.Long;

   procedure SetTimeStamp (This : access Typ;
                           P1_Long : Java.Long);

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetUserData (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   procedure SetUserData (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Notification);
   pragma Import (Java, SetSource, "setSource");
   pragma Import (Java, GetSequenceNumber, "getSequenceNumber");
   pragma Import (Java, SetSequenceNumber, "setSequenceNumber");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetTimeStamp, "getTimeStamp");
   pragma Import (Java, SetTimeStamp, "setTimeStamp");
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, GetUserData, "getUserData");
   pragma Import (Java, SetUserData, "setUserData");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Notification;
pragma Import (Java, Javax.Management.Notification, "javax.management.Notification");
pragma Extensions_Allowed (Off);
