pragma Extensions_Allowed (On);
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
with Java.Lang.Object;

package Javax.Management.NotificationBroadcaster is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotificationListener (This : access Typ;
                                      P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                      P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                      P3_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class) is abstract;
   --  can raise Javax.Management.ListenerNotFoundException.Except

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddNotificationListener, "addNotificationListener");
   pragma Export (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Export (Java, GetNotificationInfo, "getNotificationInfo");

end Javax.Management.NotificationBroadcaster;
pragma Import (Java, Javax.Management.NotificationBroadcaster, "javax.management.NotificationBroadcaster");
pragma Extensions_Allowed (Off);
