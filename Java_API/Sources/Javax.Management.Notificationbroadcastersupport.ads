pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.Executor;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.Notification;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
with Java.Lang.Object;
with Javax.Management.NotificationEmitter;

package Javax.Management.NotificationBroadcasterSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NotificationBroadcasterSupport (This : Ref := null)
                                                return Ref;

   function New_NotificationBroadcasterSupport (P1_Executor : access Standard.Java.Util.Concurrent.Executor.Typ'Class; 
                                                This : Ref := null)
                                                return Ref;

   function New_NotificationBroadcasterSupport (P1_MBeanNotificationInfo_Arr : access Javax.Management.MBeanNotificationInfo.Arr_Obj; 
                                                This : Ref := null)
                                                return Ref;

   function New_NotificationBroadcasterSupport (P1_Executor : access Standard.Java.Util.Concurrent.Executor.Typ'Class;
                                                P2_MBeanNotificationInfo_Arr : access Javax.Management.MBeanNotificationInfo.Arr_Obj; 
                                                This : Ref := null)
                                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotificationListener (This : access Typ;
                                      P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                      P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                      P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                         P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                         P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   procedure SendNotification (This : access Typ;
                               P1_Notification : access Standard.Javax.Management.Notification.Typ'Class);

   --  protected
   procedure HandleNotification (This : access Typ;
                                 P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                 P2_Notification : access Standard.Javax.Management.Notification.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NotificationBroadcasterSupport);
   pragma Import (Java, AddNotificationListener, "addNotificationListener");
   pragma Import (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");
   pragma Import (Java, SendNotification, "sendNotification");
   pragma Import (Java, HandleNotification, "handleNotification");

end Javax.Management.NotificationBroadcasterSupport;
pragma Import (Java, Javax.Management.NotificationBroadcasterSupport, "javax.management.NotificationBroadcasterSupport");
pragma Extensions_Allowed (Off);
