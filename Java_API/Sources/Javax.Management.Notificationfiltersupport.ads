pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Management.Notification;
with Java.Lang.Object;
with Javax.Management.NotificationFilter;

package Javax.Management.NotificationFilterSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(NotificationFilter_I : Javax.Management.NotificationFilter.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NotificationFilterSupport (This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function IsNotificationEnabled (This : access Typ;
                                   P1_Notification : access Standard.Javax.Management.Notification.Typ'Class)
                                   return Java.Boolean;

   --  synchronized
   procedure EnableType (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   procedure DisableType (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   procedure DisableAllTypes (This : access Typ);

   --  synchronized
   function GetEnabledTypes (This : access Typ)
                             return access Java.Util.Vector.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NotificationFilterSupport);
   pragma Import (Java, IsNotificationEnabled, "isNotificationEnabled");
   pragma Import (Java, EnableType, "enableType");
   pragma Import (Java, DisableType, "disableType");
   pragma Import (Java, DisableAllTypes, "disableAllTypes");
   pragma Import (Java, GetEnabledTypes, "getEnabledTypes");

end Javax.Management.NotificationFilterSupport;
pragma Import (Java, Javax.Management.NotificationFilterSupport, "javax.management.NotificationFilterSupport");
pragma Extensions_Allowed (Off);
