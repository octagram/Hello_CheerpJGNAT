pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.ObjectName;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.ObjectInstance is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjectInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Javax.Management.MalformedObjectNameException.Except

   function New_ObjectInstance (P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function GetObjectName (This : access Typ)
                           return access Javax.Management.ObjectName.Typ'Class;

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjectInstance);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, GetObjectName, "getObjectName");
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, ToString, "toString");

end Javax.Management.ObjectInstance;
pragma Import (Java, Javax.Management.ObjectInstance, "javax.management.ObjectInstance");
pragma Extensions_Allowed (Off);
