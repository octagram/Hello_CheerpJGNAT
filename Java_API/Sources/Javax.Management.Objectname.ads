pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Management.MBeanServer;
with Java.Lang.Comparable;
with Java.Lang.Object;
with Javax.Management.QueryExp;

package Javax.Management.ObjectName is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Comparable_I : Java.Lang.Comparable.Ref;
            QueryExp_I : Javax.Management.QueryExp.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Javax.Management.MalformedObjectNameException.Except and
   --  Java.Lang.NullPointerException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Javax.Management.MalformedObjectNameException.Except and
   --  Java.Lang.NullPointerException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Javax.Management.MalformedObjectNameException.Except and
   --  Java.Lang.NullPointerException.Except

   function GetInstance (P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.NullPointerException.Except

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjectName (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Management.MalformedObjectNameException.Except and
   --  Java.Lang.NullPointerException.Except

   function New_ObjectName (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Management.MalformedObjectNameException.Except and
   --  Java.Lang.NullPointerException.Except

   function New_ObjectName (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Management.MalformedObjectNameException.Except and
   --  Java.Lang.NullPointerException.Except

   function IsPattern (This : access Typ)
                       return Java.Boolean;

   function IsDomainPattern (This : access Typ)
                             return Java.Boolean;

   function IsPropertyPattern (This : access Typ)
                               return Java.Boolean;

   function IsPropertyListPattern (This : access Typ)
                                   return Java.Boolean;

   function IsPropertyValuePattern (This : access Typ)
                                    return Java.Boolean;

   function IsPropertyValuePattern (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                                    return Java.Boolean;
   --  can raise Java.Lang.NullPointerException.Except and
   --  Java.Lang.IllegalArgumentException.Except

   function GetCanonicalName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function GetDomain (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetKeyProperty (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Java.Lang.NullPointerException.Except

   function GetKeyPropertyList (This : access Typ)
                                return access Java.Util.Hashtable.Typ'Class;

   function GetKeyPropertyListString (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetCanonicalKeyPropertyListString (This : access Typ)
                                               return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Quote (P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Java.Lang.String.Typ'Class;
   --  can raise Java.Lang.NullPointerException.Except

   function Unquote (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.NullPointerException.Except

   function Apply (This : access Typ;
                   P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Lang.NullPointerException.Except

   procedure SetMBeanServer (This : access Typ;
                             P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class);

   function CompareTo (This : access Typ;
                       P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                       return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WILDCARD : access Javax.Management.ObjectName.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Java_Constructor (New_ObjectName);
   pragma Import (Java, IsPattern, "isPattern");
   pragma Import (Java, IsDomainPattern, "isDomainPattern");
   pragma Import (Java, IsPropertyPattern, "isPropertyPattern");
   pragma Import (Java, IsPropertyListPattern, "isPropertyListPattern");
   pragma Import (Java, IsPropertyValuePattern, "isPropertyValuePattern");
   pragma Import (Java, GetCanonicalName, "getCanonicalName");
   pragma Import (Java, GetDomain, "getDomain");
   pragma Import (Java, GetKeyProperty, "getKeyProperty");
   pragma Import (Java, GetKeyPropertyList, "getKeyPropertyList");
   pragma Import (Java, GetKeyPropertyListString, "getKeyPropertyListString");
   pragma Import (Java, GetCanonicalKeyPropertyListString, "getCanonicalKeyPropertyListString");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Quote, "quote");
   pragma Import (Java, Unquote, "unquote");
   pragma Import (Java, Apply, "apply");
   pragma Import (Java, SetMBeanServer, "setMBeanServer");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, WILDCARD, "WILDCARD");

end Javax.Management.ObjectName;
pragma Import (Java, Javax.Management.ObjectName, "javax.management.ObjectName");
pragma Extensions_Allowed (Off);
