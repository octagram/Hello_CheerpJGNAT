pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Management.Openmbean.SimpleType;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Openmbean.OpenType;

package Javax.Management.Openmbean.ArrayType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Openmbean.OpenType.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ArrayType (P1_Int : Java.Int;
                           P2_OpenType : access Standard.Javax.Management.Openmbean.OpenType.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   function New_ArrayType (P1_SimpleType : access Standard.Javax.Management.Openmbean.SimpleType.Typ'Class;
                           P2_Boolean : Java.Boolean; 
                           This : Ref := null)
                           return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDimension (This : access Typ)
                          return Java.Int;

   function GetElementOpenType (This : access Typ)
                                return access Javax.Management.Openmbean.OpenType.Typ'Class;

   function IsPrimitiveArray (This : access Typ)
                              return Java.Boolean;

   function IsValue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetArrayType (P1_OpenType : access Standard.Javax.Management.Openmbean.OpenType.Typ'Class)
                          return access Javax.Management.Openmbean.ArrayType.Typ'Class;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   function GetPrimitiveArrayType (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                   return access Javax.Management.Openmbean.ArrayType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ArrayType);
   pragma Import (Java, GetDimension, "getDimension");
   pragma Import (Java, GetElementOpenType, "getElementOpenType");
   pragma Import (Java, IsPrimitiveArray, "isPrimitiveArray");
   pragma Import (Java, IsValue, "isValue");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetArrayType, "getArrayType");
   pragma Import (Java, GetPrimitiveArrayType, "getPrimitiveArrayType");

end Javax.Management.Openmbean.ArrayType;
pragma Import (Java, Javax.Management.Openmbean.ArrayType, "javax.management.openmbean.ArrayType");
pragma Extensions_Allowed (Off);
