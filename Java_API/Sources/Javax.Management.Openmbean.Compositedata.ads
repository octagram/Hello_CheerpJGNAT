pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Javax.Management.Openmbean.CompositeType;
with Java.Lang.Object;

package Javax.Management.Openmbean.CompositeData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCompositeType (This : access Typ)
                              return access Javax.Management.Openmbean.CompositeType.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function GetAll (This : access Typ;
                    P1_String_Arr : access Java.Lang.String.Arr_Obj)
                    return Standard.Java.Lang.Object.Ref is abstract;

   function ContainsKey (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean is abstract;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean is abstract;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCompositeType, "getCompositeType");
   pragma Export (Java, Get, "get");
   pragma Export (Java, GetAll, "getAll");
   pragma Export (Java, ContainsKey, "containsKey");
   pragma Export (Java, ContainsValue, "containsValue");
   pragma Export (Java, Values, "values");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");

end Javax.Management.Openmbean.CompositeData;
pragma Import (Java, Javax.Management.Openmbean.CompositeData, "javax.management.openmbean.CompositeData");
pragma Extensions_Allowed (Off);
