pragma Extensions_Allowed (On);
limited with Java.Lang.Reflect.Method;
limited with Javax.Management.Openmbean.CompositeData;
with Java.Lang.Object;
with Java.Lang.Reflect.InvocationHandler;

package Javax.Management.Openmbean.CompositeDataInvocationHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(InvocationHandler_I : Java.Lang.Reflect.InvocationHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CompositeDataInvocationHandler (P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class; 
                                                This : Ref := null)
                                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCompositeData (This : access Typ)
                              return access Javax.Management.Openmbean.CompositeData.Typ'Class;

   function Invoke (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Method : access Standard.Java.Lang.Reflect.Method.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Throwable.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompositeDataInvocationHandler);
   pragma Import (Java, GetCompositeData, "getCompositeData");
   pragma Import (Java, Invoke, "invoke");

end Javax.Management.Openmbean.CompositeDataInvocationHandler;
pragma Import (Java, Javax.Management.Openmbean.CompositeDataInvocationHandler, "javax.management.openmbean.CompositeDataInvocationHandler");
pragma Extensions_Allowed (Off);
