pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Java.Util.Map;
limited with Javax.Management.Openmbean.CompositeType;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Openmbean.CompositeData;

package Javax.Management.Openmbean.CompositeDataSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            CompositeData_I : Javax.Management.Openmbean.CompositeData.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CompositeDataSupport (P1_CompositeType : access Standard.Javax.Management.Openmbean.CompositeType.Typ'Class;
                                      P2_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P3_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                      This : Ref := null)
                                      return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   function New_CompositeDataSupport (P1_CompositeType : access Standard.Javax.Management.Openmbean.CompositeType.Typ'Class;
                                      P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCompositeType (This : access Typ)
                              return access Javax.Management.Openmbean.CompositeType.Typ'Class;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function GetAll (This : access Typ;
                    P1_String_Arr : access Java.Lang.String.Arr_Obj)
                    return Standard.Java.Lang.Object.Ref;

   function ContainsKey (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompositeDataSupport);
   pragma Import (Java, GetCompositeType, "getCompositeType");
   pragma Import (Java, Get, "get");
   pragma Import (Java, GetAll, "getAll");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, Values, "values");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Openmbean.CompositeDataSupport;
pragma Import (Java, Javax.Management.Openmbean.CompositeDataSupport, "javax.management.openmbean.CompositeDataSupport");
pragma Extensions_Allowed (Off);
