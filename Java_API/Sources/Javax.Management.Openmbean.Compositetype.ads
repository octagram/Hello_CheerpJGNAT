pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Set;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Openmbean.OpenType;

package Javax.Management.Openmbean.CompositeType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Openmbean.OpenType.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CompositeType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_String_Arr : access Java.Lang.String.Arr_Obj;
                               P4_String_Arr : access Java.Lang.String.Arr_Obj;
                               P5_OpenType_Arr : access Javax.Management.Openmbean.OpenType.Arr_Obj; 
                               This : Ref := null)
                               return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function ContainsKey (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   function GetDescription (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Management.Openmbean.OpenType.Typ'Class;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function IsValue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompositeType);
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, IsValue, "isValue");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Openmbean.CompositeType;
pragma Import (Java, Javax.Management.Openmbean.CompositeType, "javax.management.openmbean.CompositeType");
pragma Extensions_Allowed (Off);
