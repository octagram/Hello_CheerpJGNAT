pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Management.Openmbean.OpenMBeanParameterInfo;

package Javax.Management.Openmbean.OpenMBeanAttributeInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            OpenMBeanParameterInfo_I : Javax.Management.Openmbean.OpenMBeanParameterInfo.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsReadable (This : access Typ)
                        return Java.Boolean is abstract;

   function IsWritable (This : access Typ)
                        return Java.Boolean is abstract;

   function IsIs (This : access Typ)
                  return Java.Boolean is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsReadable, "isReadable");
   pragma Export (Java, IsWritable, "isWritable");
   pragma Export (Java, IsIs, "isIs");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");

end Javax.Management.Openmbean.OpenMBeanAttributeInfo;
pragma Import (Java, Javax.Management.Openmbean.OpenMBeanAttributeInfo, "javax.management.openmbean.OpenMBeanAttributeInfo");
pragma Extensions_Allowed (Off);
