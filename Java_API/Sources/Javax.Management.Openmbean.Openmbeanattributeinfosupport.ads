pragma Extensions_Allowed (On);
limited with Java.Lang.Comparable;
limited with Java.Lang.String;
limited with Java.Util.Set;
limited with Javax.Management.Descriptor;
limited with Javax.Management.Openmbean.OpenType;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;
with Javax.Management.MBeanAttributeInfo;
with Javax.Management.Openmbean.OpenMBeanAttributeInfo;

package Javax.Management.Openmbean.OpenMBeanAttributeInfoSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref;
            OpenMBeanAttributeInfo_I : Javax.Management.Openmbean.OpenMBeanAttributeInfo.Ref)
    is new Javax.Management.MBeanAttributeInfo.Typ(Serializable_I,
                                                   Cloneable_I,
                                                   DescriptorRead_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_OpenMBeanAttributeInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                                               P3_OpenType : access Standard.Javax.Management.Openmbean.OpenType.Typ'Class;
                                               P4_Boolean : Java.Boolean;
                                               P5_Boolean : Java.Boolean;
                                               P6_Boolean : Java.Boolean; 
                                               This : Ref := null)
                                               return Ref;

   function New_OpenMBeanAttributeInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                                               P3_OpenType : access Standard.Javax.Management.Openmbean.OpenType.Typ'Class;
                                               P4_Boolean : Java.Boolean;
                                               P5_Boolean : Java.Boolean;
                                               P6_Boolean : Java.Boolean;
                                               P7_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;

   function New_OpenMBeanAttributeInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                                               P3_OpenType : access Standard.Javax.Management.Openmbean.OpenType.Typ'Class;
                                               P4_Boolean : Java.Boolean;
                                               P5_Boolean : Java.Boolean;
                                               P6_Boolean : Java.Boolean;
                                               P7_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   function New_OpenMBeanAttributeInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                                               P3_OpenType : access Standard.Javax.Management.Openmbean.OpenType.Typ'Class;
                                               P4_Boolean : Java.Boolean;
                                               P5_Boolean : Java.Boolean;
                                               P6_Boolean : Java.Boolean;
                                               P7_Object : access Standard.Java.Lang.Object.Typ'Class;
                                               P8_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                               This : Ref := null)
                                               return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   function New_OpenMBeanAttributeInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                                               P3_OpenType : access Standard.Javax.Management.Openmbean.OpenType.Typ'Class;
                                               P4_Boolean : Java.Boolean;
                                               P5_Boolean : Java.Boolean;
                                               P6_Boolean : Java.Boolean;
                                               P7_Object : access Standard.Java.Lang.Object.Typ'Class;
                                               P8_Comparable : access Standard.Java.Lang.Comparable.Typ'Class;
                                               P9_Comparable : access Standard.Java.Lang.Comparable.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOpenType (This : access Typ)
                         return access Javax.Management.Openmbean.OpenType.Typ'Class;

   function GetDefaultValue (This : access Typ)
                             return access Java.Lang.Object.Typ'Class;

   function GetLegalValues (This : access Typ)
                            return access Java.Util.Set.Typ'Class;

   function GetMinValue (This : access Typ)
                         return access Java.Lang.Comparable.Typ'Class;

   function GetMaxValue (This : access Typ)
                         return access Java.Lang.Comparable.Typ'Class;

   function HasDefaultValue (This : access Typ)
                             return Java.Boolean;

   function HasLegalValues (This : access Typ)
                            return Java.Boolean;

   function HasMinValue (This : access Typ)
                         return Java.Boolean;

   function HasMaxValue (This : access Typ)
                         return Java.Boolean;

   function IsValue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OpenMBeanAttributeInfoSupport);
   pragma Import (Java, GetOpenType, "getOpenType");
   pragma Import (Java, GetDefaultValue, "getDefaultValue");
   pragma Import (Java, GetLegalValues, "getLegalValues");
   pragma Import (Java, GetMinValue, "getMinValue");
   pragma Import (Java, GetMaxValue, "getMaxValue");
   pragma Import (Java, HasDefaultValue, "hasDefaultValue");
   pragma Import (Java, HasLegalValues, "hasLegalValues");
   pragma Import (Java, HasMinValue, "hasMinValue");
   pragma Import (Java, HasMaxValue, "hasMaxValue");
   pragma Import (Java, IsValue, "isValue");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Openmbean.OpenMBeanAttributeInfoSupport;
pragma Import (Java, Javax.Management.Openmbean.OpenMBeanAttributeInfoSupport, "javax.management.openmbean.OpenMBeanAttributeInfoSupport");
pragma Extensions_Allowed (Off);
