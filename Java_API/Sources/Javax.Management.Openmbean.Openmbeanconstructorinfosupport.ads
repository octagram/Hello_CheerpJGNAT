pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Descriptor;
limited with Javax.Management.Openmbean.OpenMBeanParameterInfo;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Management.DescriptorRead;
with Javax.Management.MBeanConstructorInfo;
with Javax.Management.Openmbean.OpenMBeanConstructorInfo;

package Javax.Management.Openmbean.OpenMBeanConstructorInfoSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DescriptorRead_I : Javax.Management.DescriptorRead.Ref;
            OpenMBeanConstructorInfo_I : Javax.Management.Openmbean.OpenMBeanConstructorInfo.Ref)
    is new Javax.Management.MBeanConstructorInfo.Typ(Serializable_I,
                                                     Cloneable_I,
                                                     DescriptorRead_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_OpenMBeanConstructorInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                                 P3_OpenMBeanParameterInfo_Arr : access Javax.Management.Openmbean.OpenMBeanParameterInfo.Arr_Obj; 
                                                 This : Ref := null)
                                                 return Ref;

   function New_OpenMBeanConstructorInfoSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                                 P3_OpenMBeanParameterInfo_Arr : access Javax.Management.Openmbean.OpenMBeanParameterInfo.Arr_Obj;
                                                 P4_Descriptor : access Standard.Javax.Management.Descriptor.Typ'Class; 
                                                 This : Ref := null)
                                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OpenMBeanConstructorInfoSupport);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Openmbean.OpenMBeanConstructorInfoSupport;
pragma Import (Java, Javax.Management.Openmbean.OpenMBeanConstructorInfoSupport, "javax.management.openmbean.OpenMBeanConstructorInfoSupport");
pragma Extensions_Allowed (Off);
