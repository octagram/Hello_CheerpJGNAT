pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.MBeanAttributeInfo;
limited with Javax.Management.MBeanConstructorInfo;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.MBeanOperationInfo;
with Java.Lang.Object;

package Javax.Management.Openmbean.OpenMBeanInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributes (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function GetOperations (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function GetConstructors (This : access Typ)
                             return Standard.Java.Lang.Object.Ref is abstract;

   function GetNotifications (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetClassName, "getClassName");
   pragma Export (Java, GetDescription, "getDescription");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetOperations, "getOperations");
   pragma Export (Java, GetConstructors, "getConstructors");
   pragma Export (Java, GetNotifications, "getNotifications");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");

end Javax.Management.Openmbean.OpenMBeanInfo;
pragma Import (Java, Javax.Management.Openmbean.OpenMBeanInfo, "javax.management.openmbean.OpenMBeanInfo");
pragma Extensions_Allowed (Off);
