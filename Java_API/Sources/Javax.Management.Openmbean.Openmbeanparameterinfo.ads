pragma Extensions_Allowed (On);
limited with Java.Lang.Comparable;
limited with Java.Lang.String;
limited with Java.Util.Set;
limited with Javax.Management.Openmbean.OpenType;
with Java.Lang.Object;

package Javax.Management.Openmbean.OpenMBeanParameterInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetOpenType (This : access Typ)
                         return access Javax.Management.Openmbean.OpenType.Typ'Class is abstract;

   function GetDefaultValue (This : access Typ)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function GetLegalValues (This : access Typ)
                            return access Java.Util.Set.Typ'Class is abstract;

   function GetMinValue (This : access Typ)
                         return access Java.Lang.Comparable.Typ'Class is abstract;

   function GetMaxValue (This : access Typ)
                         return access Java.Lang.Comparable.Typ'Class is abstract;

   function HasDefaultValue (This : access Typ)
                             return Java.Boolean is abstract;

   function HasLegalValues (This : access Typ)
                            return Java.Boolean is abstract;

   function HasMinValue (This : access Typ)
                         return Java.Boolean is abstract;

   function HasMaxValue (This : access Typ)
                         return Java.Boolean is abstract;

   function IsValue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDescription, "getDescription");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetOpenType, "getOpenType");
   pragma Export (Java, GetDefaultValue, "getDefaultValue");
   pragma Export (Java, GetLegalValues, "getLegalValues");
   pragma Export (Java, GetMinValue, "getMinValue");
   pragma Export (Java, GetMaxValue, "getMaxValue");
   pragma Export (Java, HasDefaultValue, "hasDefaultValue");
   pragma Export (Java, HasLegalValues, "hasLegalValues");
   pragma Export (Java, HasMinValue, "hasMinValue");
   pragma Export (Java, HasMaxValue, "hasMaxValue");
   pragma Export (Java, IsValue, "isValue");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");

end Javax.Management.Openmbean.OpenMBeanParameterInfo;
pragma Import (Java, Javax.Management.Openmbean.OpenMBeanParameterInfo, "javax.management.openmbean.OpenMBeanParameterInfo");
pragma Extensions_Allowed (Off);
