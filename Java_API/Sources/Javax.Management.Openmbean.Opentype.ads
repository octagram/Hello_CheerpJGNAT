pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Openmbean.OpenType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_OpenType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetTypeName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function IsArray (This : access Typ)
                     return Java.Boolean;

   function IsValue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ALLOWED_CLASSNAMES_LIST : access Java.Util.List.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OpenType);
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetTypeName, "getTypeName");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, IsArray, "isArray");
   pragma Export (Java, IsValue, "isValue");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");
   pragma Import (Java, ALLOWED_CLASSNAMES_LIST, "ALLOWED_CLASSNAMES_LIST");

end Javax.Management.Openmbean.OpenType;
pragma Import (Java, Javax.Management.Openmbean.OpenType, "javax.management.openmbean.OpenType");
pragma Extensions_Allowed (Off);
