pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Openmbean.OpenType;

package Javax.Management.Openmbean.SimpleType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Openmbean.OpenType.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsValue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ReadResolve (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.ObjectStreamException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   VOID : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   BOOLEAN : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   CHARACTER : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   BYTE : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   SHORT : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   INTEGER : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   LONG : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   FLOAT : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   DOUBLE : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   STRING : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   BIGDECIMAL : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   BIGINTEGER : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   DATE : access Javax.Management.Openmbean.SimpleType.Typ'Class;

   --  final
   OBJECTNAME : access Javax.Management.Openmbean.SimpleType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, IsValue, "isValue");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ReadResolve, "readResolve");
   pragma Import (Java, VOID, "VOID");
   pragma Import (Java, BOOLEAN, "BOOLEAN");
   pragma Import (Java, CHARACTER, "CHARACTER");
   pragma Import (Java, BYTE, "BYTE");
   pragma Import (Java, SHORT, "SHORT");
   pragma Import (Java, INTEGER, "INTEGER");
   pragma Import (Java, LONG, "LONG");
   pragma Import (Java, FLOAT, "FLOAT");
   pragma Import (Java, DOUBLE, "DOUBLE");
   pragma Import (Java, STRING, "STRING");
   pragma Import (Java, BIGDECIMAL, "BIGDECIMAL");
   pragma Import (Java, BIGINTEGER, "BIGINTEGER");
   pragma Import (Java, DATE, "DATE");
   pragma Import (Java, OBJECTNAME, "OBJECTNAME");

end Javax.Management.Openmbean.SimpleType;
pragma Import (Java, Javax.Management.Openmbean.SimpleType, "javax.management.openmbean.SimpleType");
pragma Extensions_Allowed (Off);
