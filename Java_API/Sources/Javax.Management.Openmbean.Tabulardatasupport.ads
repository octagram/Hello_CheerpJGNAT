pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Java.Util.Set;
limited with Javax.Management.Openmbean.CompositeData;
limited with Javax.Management.Openmbean.TabularType;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Map;
with Javax.Management.Openmbean.TabularData;

package Javax.Management.Openmbean.TabularDataSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref;
            TabularData_I : Javax.Management.Openmbean.TabularData.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TabularDataSupport (P1_TabularType : access Standard.Javax.Management.Openmbean.TabularType.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_TabularDataSupport (P1_TabularType : access Standard.Javax.Management.Openmbean.TabularType.Typ'Class;
                                    P2_Int : Java.Int;
                                    P3_Float : Java.Float; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTabularType (This : access Typ)
                            return access Javax.Management.Openmbean.TabularType.Typ'Class;

   function CalculateIndex (This : access Typ;
                            P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class)
                            return Standard.Java.Lang.Object.Ref;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function ContainsKey (This : access Typ;
                         P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                         return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class)
                           return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Get (This : access Typ;
                 P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                 return access Javax.Management.Openmbean.CompositeData.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Put (This : access Typ;
                  P1_CompositeData : access Standard.Javax.Management.Openmbean.CompositeData.Typ'Class);

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                    return access Javax.Management.Openmbean.CompositeData.Typ'Class;

   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   procedure PutAll (This : access Typ;
                     P1_CompositeData_Arr : access Javax.Management.Openmbean.CompositeData.Arr_Obj);

   procedure Clear (This : access Typ);

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TabularDataSupport);
   pragma Import (Java, GetTabularType, "getTabularType");
   pragma Import (Java, CalculateIndex, "calculateIndex");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Openmbean.TabularDataSupport;
pragma Import (Java, Javax.Management.Openmbean.TabularDataSupport, "javax.management.openmbean.TabularDataSupport");
pragma Extensions_Allowed (Off);
