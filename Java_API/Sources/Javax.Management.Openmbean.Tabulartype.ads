pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Management.Openmbean.CompositeType;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Openmbean.OpenType;

package Javax.Management.Openmbean.TabularType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Openmbean.OpenType.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TabularType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_CompositeType : access Standard.Javax.Management.Openmbean.CompositeType.Typ'Class;
                             P4_String_Arr : access Java.Lang.String.Arr_Obj; 
                             This : Ref := null)
                             return Ref;
   --  can raise Javax.Management.Openmbean.OpenDataException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRowType (This : access Typ)
                        return access Javax.Management.Openmbean.CompositeType.Typ'Class;

   function GetIndexNames (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   function IsValue (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TabularType);
   pragma Import (Java, GetRowType, "getRowType");
   pragma Import (Java, GetIndexNames, "getIndexNames");
   pragma Import (Java, IsValue, "isValue");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Openmbean.TabularType;
pragma Import (Java, Javax.Management.Openmbean.TabularType, "javax.management.openmbean.TabularType");
pragma Extensions_Allowed (Off);
