pragma Extensions_Allowed (On);
package Javax.Management.Openmbean is
   pragma Preelaborate;
end Javax.Management.Openmbean;
pragma Import (Java, Javax.Management.Openmbean, "javax.management.openmbean");
pragma Extensions_Allowed (Off);
