pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Management.PersistentMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Load (This : access Typ) is abstract;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Javax.Management.InstanceNotFoundException.Except

   procedure Store (This : access Typ) is abstract;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.RuntimeOperationsException.Except and
   --  Javax.Management.InstanceNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Load, "load");
   pragma Export (Java, Store, "store");

end Javax.Management.PersistentMBean;
pragma Import (Java, Javax.Management.PersistentMBean, "javax.management.PersistentMBean");
pragma Extensions_Allowed (Off);
