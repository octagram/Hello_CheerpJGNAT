pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Java.Lang.String;
limited with Javax.Management.AttributeValueExp;
limited with Javax.Management.QueryExp;
limited with Javax.Management.StringValueExp;
limited with Javax.Management.ValueExp;
with Java.Lang.Object;

package Javax.Management.Query is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Query (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function and_K (P1_QueryExp : access Standard.Javax.Management.QueryExp.Typ'Class;
                   P2_QueryExp : access Standard.Javax.Management.QueryExp.Typ'Class)
                   return access Javax.Management.QueryExp.Typ'Class;

   function or_K (P1_QueryExp : access Standard.Javax.Management.QueryExp.Typ'Class;
                  P2_QueryExp : access Standard.Javax.Management.QueryExp.Typ'Class)
                  return access Javax.Management.QueryExp.Typ'Class;

   function Gt (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                return access Javax.Management.QueryExp.Typ'Class;

   function Geq (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                 P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                 return access Javax.Management.QueryExp.Typ'Class;

   function Leq (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                 P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                 return access Javax.Management.QueryExp.Typ'Class;

   function Lt (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                return access Javax.Management.QueryExp.Typ'Class;

   function Eq (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                return access Javax.Management.QueryExp.Typ'Class;

   function Between (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                     P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                     P3_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                     return access Javax.Management.QueryExp.Typ'Class;

   function Match (P1_AttributeValueExp : access Standard.Javax.Management.AttributeValueExp.Typ'Class;
                   P2_StringValueExp : access Standard.Javax.Management.StringValueExp.Typ'Class)
                   return access Javax.Management.QueryExp.Typ'Class;

   function Attr (P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Javax.Management.AttributeValueExp.Typ'Class;

   function Attr (P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Javax.Management.AttributeValueExp.Typ'Class;

   function Classattr return access Javax.Management.AttributeValueExp.Typ'Class;

   function not_K (P1_QueryExp : access Standard.Javax.Management.QueryExp.Typ'Class)
                   return access Javax.Management.QueryExp.Typ'Class;

   function in_K (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                  P2_ValueExp_Arr : access Javax.Management.ValueExp.Arr_Obj)
                  return access Javax.Management.QueryExp.Typ'Class;

   function Value (P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Javax.Management.StringValueExp.Typ'Class;

   function Value (P1_Number : access Standard.Java.Lang.Number.Typ'Class)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Value (P1_Int : Java.Int)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Value (P1_Long : Java.Long)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Value (P1_Float : Java.Float)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Value (P1_Double : Java.Double)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Value (P1_Boolean : Java.Boolean)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Plus (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                  P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                  return access Javax.Management.ValueExp.Typ'Class;

   function Times (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                   P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Minus (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                   P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                   return access Javax.Management.ValueExp.Typ'Class;

   function Div (P1_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class;
                 P2_ValueExp : access Standard.Javax.Management.ValueExp.Typ'Class)
                 return access Javax.Management.ValueExp.Typ'Class;

   function InitialSubString (P1_AttributeValueExp : access Standard.Javax.Management.AttributeValueExp.Typ'Class;
                              P2_StringValueExp : access Standard.Javax.Management.StringValueExp.Typ'Class)
                              return access Javax.Management.QueryExp.Typ'Class;

   function AnySubString (P1_AttributeValueExp : access Standard.Javax.Management.AttributeValueExp.Typ'Class;
                          P2_StringValueExp : access Standard.Javax.Management.StringValueExp.Typ'Class)
                          return access Javax.Management.QueryExp.Typ'Class;

   function FinalSubString (P1_AttributeValueExp : access Standard.Javax.Management.AttributeValueExp.Typ'Class;
                            P2_StringValueExp : access Standard.Javax.Management.StringValueExp.Typ'Class)
                            return access Javax.Management.QueryExp.Typ'Class;

   function IsInstanceOf (P1_StringValueExp : access Standard.Javax.Management.StringValueExp.Typ'Class)
                          return access Javax.Management.QueryExp.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   GT_K : constant Java.Int;

   --  final
   LT_K : constant Java.Int;

   --  final
   GE : constant Java.Int;

   --  final
   LE : constant Java.Int;

   --  final
   EQ_K : constant Java.Int;

   --  final
   PLUS_K : constant Java.Int;

   --  final
   MINUS_K : constant Java.Int;

   --  final
   TIMES_K : constant Java.Int;

   --  final
   DIV_K : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Query);
   pragma Import (Java, and_K, "and");
   pragma Import (Java, or_K, "or");
   pragma Import (Java, Gt, "gt");
   pragma Import (Java, Geq, "geq");
   pragma Import (Java, Leq, "leq");
   pragma Import (Java, Lt, "lt");
   pragma Import (Java, Eq, "eq");
   pragma Import (Java, Between, "between");
   pragma Import (Java, Match, "match");
   pragma Import (Java, Attr, "attr");
   pragma Import (Java, Classattr, "classattr");
   pragma Import (Java, not_K, "not");
   pragma Import (Java, in_K, "in");
   pragma Import (Java, Value, "value");
   pragma Import (Java, Plus, "plus");
   pragma Import (Java, Times, "times");
   pragma Import (Java, Minus, "minus");
   pragma Import (Java, Div, "div");
   pragma Import (Java, InitialSubString, "initialSubString");
   pragma Import (Java, AnySubString, "anySubString");
   pragma Import (Java, FinalSubString, "finalSubString");
   pragma Import (Java, IsInstanceOf, "isInstanceOf");
   pragma Import (Java, GT_K, "GT");
   pragma Import (Java, LT_K, "LT");
   pragma Import (Java, GE, "GE");
   pragma Import (Java, LE, "LE");
   pragma Import (Java, EQ_K, "EQ");
   pragma Import (Java, PLUS_K, "PLUS");
   pragma Import (Java, MINUS_K, "MINUS");
   pragma Import (Java, TIMES_K, "TIMES");
   pragma Import (Java, DIV_K, "DIV");

end Javax.Management.Query;
pragma Import (Java, Javax.Management.Query, "javax.management.Query");
pragma Extensions_Allowed (Off);
