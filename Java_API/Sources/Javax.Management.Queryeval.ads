pragma Extensions_Allowed (On);
limited with Javax.Management.MBeanServer;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.QueryEval is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_QueryEval (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMBeanServer (This : access Typ;
                             P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class);

   function GetMBeanServer return access Javax.Management.MBeanServer.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_QueryEval);
   pragma Import (Java, SetMBeanServer, "setMBeanServer");
   pragma Import (Java, GetMBeanServer, "getMBeanServer");

end Javax.Management.QueryEval;
pragma Import (Java, Javax.Management.QueryEval, "javax.management.QueryEval");
pragma Extensions_Allowed (Off);
