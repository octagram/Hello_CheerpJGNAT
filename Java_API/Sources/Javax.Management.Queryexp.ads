pragma Extensions_Allowed (On);
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.QueryExp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Apply (This : access Typ;
                   P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                   return Java.Boolean is abstract;
   --  can raise Javax.Management.BadStringOperationException.Except,
   --  Javax.Management.BadBinaryOpValueExpException.Except,
   --  Javax.Management.BadAttributeValueExpException.Except and
   --  Javax.Management.InvalidApplicationException.Except

   procedure SetMBeanServer (This : access Typ;
                             P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Apply, "apply");
   pragma Export (Java, SetMBeanServer, "setMBeanServer");

end Javax.Management.QueryExp;
pragma Import (Java, Javax.Management.QueryExp, "javax.management.QueryExp");
pragma Extensions_Allowed (Off);
