pragma Extensions_Allowed (On);
limited with Java.Util.Vector;
limited with Javax.Management.Notification;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.NotificationFilter;
with Javax.Management.NotificationFilterSupport;

package Javax.Management.Relation.MBeanServerNotificationFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(NotificationFilter_I : Javax.Management.NotificationFilter.Ref)
    is new Javax.Management.NotificationFilterSupport.Typ(NotificationFilter_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MBeanServerNotificationFilter (This : Ref := null)
                                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure DisableAllObjectNames (This : access Typ);

   --  synchronized
   procedure DisableObjectName (This : access Typ;
                                P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   procedure EnableAllObjectNames (This : access Typ);

   --  synchronized
   procedure EnableObjectName (This : access Typ;
                               P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function GetEnabledObjectNames (This : access Typ)
                                   return access Java.Util.Vector.Typ'Class;

   --  synchronized
   function GetDisabledObjectNames (This : access Typ)
                                    return access Java.Util.Vector.Typ'Class;

   --  synchronized
   function IsNotificationEnabled (This : access Typ;
                                   P1_Notification : access Standard.Javax.Management.Notification.Typ'Class)
                                   return Java.Boolean;
   --  can raise Java.Lang.IllegalArgumentException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MBeanServerNotificationFilter);
   pragma Import (Java, DisableAllObjectNames, "disableAllObjectNames");
   pragma Import (Java, DisableObjectName, "disableObjectName");
   pragma Import (Java, EnableAllObjectNames, "enableAllObjectNames");
   pragma Import (Java, EnableObjectName, "enableObjectName");
   pragma Import (Java, GetEnabledObjectNames, "getEnabledObjectNames");
   pragma Import (Java, GetDisabledObjectNames, "getDisabledObjectNames");
   pragma Import (Java, IsNotificationEnabled, "isNotificationEnabled");

end Javax.Management.Relation.MBeanServerNotificationFilter;
pragma Import (Java, Javax.Management.Relation.MBeanServerNotificationFilter, "javax.management.relation.MBeanServerNotificationFilter");
pragma Extensions_Allowed (Off);
