pragma Extensions_Allowed (On);
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
limited with Javax.Management.ObjectName;
limited with Javax.Management.Relation.Role;
limited with Javax.Management.Relation.RoleList;
limited with Javax.Management.Relation.RoleResult;
with Java.Lang.Object;

package Javax.Management.Relation.Relation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRole (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Util.List.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except and
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function GetRoles (This : access Typ;
                      P1_String_Arr : access Java.Lang.String.Arr_Obj)
                      return access Javax.Management.Relation.RoleResult.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function GetRoleCardinality (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RoleNotFoundException.Except

   function GetAllRoles (This : access Typ)
                         return access Javax.Management.Relation.RoleResult.Typ'Class is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function RetrieveAllRoles (This : access Typ)
                              return access Javax.Management.Relation.RoleList.Typ'Class is abstract;

   procedure SetRole (This : access Typ;
                      P1_Role : access Standard.Javax.Management.Relation.Role.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRoleValueException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function SetRoles (This : access Typ;
                      P1_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class)
                      return access Javax.Management.Relation.RoleResult.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure HandleMBeanUnregistration (This : access Typ;
                                        P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRoleValueException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetReferencedMBeans (This : access Typ)
                                 return access Java.Util.Map.Typ'Class is abstract;

   function GetRelationTypeName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   function GetRelationServiceName (This : access Typ)
                                    return access Javax.Management.ObjectName.Typ'Class is abstract;

   function GetRelationId (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRole, "getRole");
   pragma Export (Java, GetRoles, "getRoles");
   pragma Export (Java, GetRoleCardinality, "getRoleCardinality");
   pragma Export (Java, GetAllRoles, "getAllRoles");
   pragma Export (Java, RetrieveAllRoles, "retrieveAllRoles");
   pragma Export (Java, SetRole, "setRole");
   pragma Export (Java, SetRoles, "setRoles");
   pragma Export (Java, HandleMBeanUnregistration, "handleMBeanUnregistration");
   pragma Export (Java, GetReferencedMBeans, "getReferencedMBeans");
   pragma Export (Java, GetRelationTypeName, "getRelationTypeName");
   pragma Export (Java, GetRelationServiceName, "getRelationServiceName");
   pragma Export (Java, GetRelationId, "getRelationId");

end Javax.Management.Relation.Relation;
pragma Import (Java, Javax.Management.Relation.Relation, "javax.management.relation.Relation");
pragma Extensions_Allowed (Off);
