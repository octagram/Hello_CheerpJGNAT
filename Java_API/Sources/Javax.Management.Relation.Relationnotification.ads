pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Management.ObjectName;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Notification;

package Javax.Management.Relation.RelationNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Notification.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RelationNotification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                      P3_Long : Java.Long;
                                      P4_Long : Java.Long;
                                      P5_String : access Standard.Java.Lang.String.Typ'Class;
                                      P6_String : access Standard.Java.Lang.String.Typ'Class;
                                      P7_String : access Standard.Java.Lang.String.Typ'Class;
                                      P8_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P9_List : access Standard.Java.Util.List.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function New_RelationNotification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                      P3_Long : Java.Long;
                                      P4_Long : Java.Long;
                                      P5_String : access Standard.Java.Lang.String.Typ'Class;
                                      P6_String : access Standard.Java.Lang.String.Typ'Class;
                                      P7_String : access Standard.Java.Lang.String.Typ'Class;
                                      P8_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P9_String : access Standard.Java.Lang.String.Typ'Class;
                                      P10_List : access Standard.Java.Util.List.Typ'Class;
                                      P11_List : access Standard.Java.Util.List.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRelationId (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetRelationTypeName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetObjectName (This : access Typ)
                           return access Javax.Management.ObjectName.Typ'Class;

   function GetMBeansToUnregister (This : access Typ)
                                   return access Java.Util.List.Typ'Class;

   function GetRoleName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetOldRoleValue (This : access Typ)
                             return access Java.Util.List.Typ'Class;

   function GetNewRoleValue (This : access Typ)
                             return access Java.Util.List.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RELATION_BASIC_CREATION : constant access Java.Lang.String.Typ'Class;

   --  final
   RELATION_MBEAN_CREATION : constant access Java.Lang.String.Typ'Class;

   --  final
   RELATION_BASIC_UPDATE : constant access Java.Lang.String.Typ'Class;

   --  final
   RELATION_MBEAN_UPDATE : constant access Java.Lang.String.Typ'Class;

   --  final
   RELATION_BASIC_REMOVAL : constant access Java.Lang.String.Typ'Class;

   --  final
   RELATION_MBEAN_REMOVAL : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RelationNotification);
   pragma Import (Java, GetRelationId, "getRelationId");
   pragma Import (Java, GetRelationTypeName, "getRelationTypeName");
   pragma Import (Java, GetObjectName, "getObjectName");
   pragma Import (Java, GetMBeansToUnregister, "getMBeansToUnregister");
   pragma Import (Java, GetRoleName, "getRoleName");
   pragma Import (Java, GetOldRoleValue, "getOldRoleValue");
   pragma Import (Java, GetNewRoleValue, "getNewRoleValue");
   pragma Import (Java, RELATION_BASIC_CREATION, "RELATION_BASIC_CREATION");
   pragma Import (Java, RELATION_MBEAN_CREATION, "RELATION_MBEAN_CREATION");
   pragma Import (Java, RELATION_BASIC_UPDATE, "RELATION_BASIC_UPDATE");
   pragma Import (Java, RELATION_MBEAN_UPDATE, "RELATION_MBEAN_UPDATE");
   pragma Import (Java, RELATION_BASIC_REMOVAL, "RELATION_BASIC_REMOVAL");
   pragma Import (Java, RELATION_MBEAN_REMOVAL, "RELATION_MBEAN_REMOVAL");

end Javax.Management.Relation.RelationNotification;
pragma Import (Java, Javax.Management.Relation.RelationNotification, "javax.management.relation.RelationNotification");
pragma Extensions_Allowed (Off);
