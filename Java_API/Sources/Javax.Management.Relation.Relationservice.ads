pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.Notification;
limited with Javax.Management.ObjectName;
limited with Javax.Management.Relation.RelationType;
limited with Javax.Management.Relation.Role;
limited with Javax.Management.Relation.RoleInfo;
limited with Javax.Management.Relation.RoleList;
limited with Javax.Management.Relation.RoleResult;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.NotificationBroadcasterSupport;
with Javax.Management.NotificationEmitter;
with Javax.Management.NotificationListener;
with Javax.Management.Relation.RelationServiceMBean;

package Javax.Management.Relation.RelationService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            NotificationListener_I : Javax.Management.NotificationListener.Ref;
            RelationServiceMBean_I : Javax.Management.Relation.RelationServiceMBean.Ref)
    is new Javax.Management.NotificationBroadcasterSupport.Typ(NotificationEmitter_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RelationService (P1_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure IsActive (This : access Typ);
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);

   function GetPurgeFlag (This : access Typ)
                          return Java.Boolean;

   procedure SetPurgeFlag (This : access Typ;
                           P1_Boolean : Java.Boolean);

   procedure CreateRelationType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_RoleInfo_Arr : access Javax.Management.Relation.RoleInfo.Arr_Obj);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.InvalidRelationTypeException.Except

   procedure AddRelationType (This : access Typ;
                              P1_RelationType : access Standard.Javax.Management.Relation.RelationType.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.InvalidRelationTypeException.Except

   function GetAllRelationTypeNames (This : access Typ)
                                     return access Java.Util.List.Typ'Class;

   function GetRoleInfos (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Util.List.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   function GetRoleInfo (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.Relation.RoleInfo.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.RoleInfoNotFoundException.Except

   procedure RemoveRelationType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   procedure CreateRelation (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class);
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRelationIdException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.InvalidRoleValueException.Except

   procedure AddRelation (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.NoSuchMethodException.Except,
   --  Javax.Management.Relation.InvalidRelationIdException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRelationServiceException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except and
   --  Javax.Management.Relation.InvalidRoleValueException.Except

   function IsRelationMBean (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function IsRelation (This : access Typ;
                        P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                        return access Java.Lang.String.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function HasRelation (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Boolean.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetAllRelationIds (This : access Typ)
                               return access Java.Util.List.Typ'Class;

   function CheckRoleReading (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   function CheckRoleWriting (This : access Typ;
                              P1_Role : access Standard.Javax.Management.Relation.Role.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Boolean : access Standard.Java.Lang.Boolean.Typ'Class)
                              return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   procedure SendRelationCreationNotification (This : access Typ;
                                               P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure SendRoleUpdateNotification (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class;
                                         P3_List : access Standard.Java.Util.List.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure SendRelationRemovalNotification (This : access Typ;
                                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                                              P2_List : access Standard.Java.Util.List.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure UpdateRoleMap (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class;
                            P3_List : access Standard.Java.Util.List.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure RemoveRelation (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure PurgeRelations (This : access Typ);
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function FindReferencingRelations (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                                      P3_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Java.Util.Map.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function FindAssociatedMBeans (This : access Typ;
                                  P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Java.Util.Map.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function FindRelationsOfType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Util.List.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   function GetRole (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Util.List.Typ'Class;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except and
   --  Javax.Management.Relation.RoleNotFoundException.Except

   function GetRoles (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String_Arr : access Java.Lang.String.Arr_Obj)
                      return access Javax.Management.Relation.RoleResult.Typ'Class;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetAllRoles (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.Relation.RoleResult.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except and
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function GetRoleCardinality (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except and
   --  Javax.Management.Relation.RoleNotFoundException.Except

   procedure SetRole (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class);
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except and
   --  Javax.Management.Relation.InvalidRoleValueException.Except

   function SetRoles (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class)
                      return access Javax.Management.Relation.RoleResult.Typ'Class;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetReferencedMBeans (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Util.Map.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetRelationTypeName (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Lang.String.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure HandleNotification (This : access Typ;
                                 P1_Notification : access Standard.Javax.Management.Notification.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RelationService);
   pragma Import (Java, IsActive, "isActive");
   pragma Import (Java, PreRegister, "preRegister");
   pragma Import (Java, PostRegister, "postRegister");
   pragma Import (Java, PreDeregister, "preDeregister");
   pragma Import (Java, PostDeregister, "postDeregister");
   pragma Import (Java, GetPurgeFlag, "getPurgeFlag");
   pragma Import (Java, SetPurgeFlag, "setPurgeFlag");
   pragma Import (Java, CreateRelationType, "createRelationType");
   pragma Import (Java, AddRelationType, "addRelationType");
   pragma Import (Java, GetAllRelationTypeNames, "getAllRelationTypeNames");
   pragma Import (Java, GetRoleInfos, "getRoleInfos");
   pragma Import (Java, GetRoleInfo, "getRoleInfo");
   pragma Import (Java, RemoveRelationType, "removeRelationType");
   pragma Import (Java, CreateRelation, "createRelation");
   pragma Import (Java, AddRelation, "addRelation");
   pragma Import (Java, IsRelationMBean, "isRelationMBean");
   pragma Import (Java, IsRelation, "isRelation");
   pragma Import (Java, HasRelation, "hasRelation");
   pragma Import (Java, GetAllRelationIds, "getAllRelationIds");
   pragma Import (Java, CheckRoleReading, "checkRoleReading");
   pragma Import (Java, CheckRoleWriting, "checkRoleWriting");
   pragma Import (Java, SendRelationCreationNotification, "sendRelationCreationNotification");
   pragma Import (Java, SendRoleUpdateNotification, "sendRoleUpdateNotification");
   pragma Import (Java, SendRelationRemovalNotification, "sendRelationRemovalNotification");
   pragma Import (Java, UpdateRoleMap, "updateRoleMap");
   pragma Import (Java, RemoveRelation, "removeRelation");
   pragma Import (Java, PurgeRelations, "purgeRelations");
   pragma Import (Java, FindReferencingRelations, "findReferencingRelations");
   pragma Import (Java, FindAssociatedMBeans, "findAssociatedMBeans");
   pragma Import (Java, FindRelationsOfType, "findRelationsOfType");
   pragma Import (Java, GetRole, "getRole");
   pragma Import (Java, GetRoles, "getRoles");
   pragma Import (Java, GetAllRoles, "getAllRoles");
   pragma Import (Java, GetRoleCardinality, "getRoleCardinality");
   pragma Import (Java, SetRole, "setRole");
   pragma Import (Java, SetRoles, "setRoles");
   pragma Import (Java, GetReferencedMBeans, "getReferencedMBeans");
   pragma Import (Java, GetRelationTypeName, "getRelationTypeName");
   pragma Import (Java, HandleNotification, "handleNotification");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");

end Javax.Management.Relation.RelationService;
pragma Import (Java, Javax.Management.Relation.RelationService, "javax.management.relation.RelationService");
pragma Extensions_Allowed (Off);
