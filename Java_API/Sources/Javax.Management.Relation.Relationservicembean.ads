pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
limited with Javax.Management.ObjectName;
limited with Javax.Management.Relation.RelationType;
limited with Javax.Management.Relation.Role;
limited with Javax.Management.Relation.RoleInfo;
limited with Javax.Management.Relation.RoleList;
limited with Javax.Management.Relation.RoleResult;
with Java.Lang.Object;

package Javax.Management.Relation.RelationServiceMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure IsActive (This : access Typ) is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function GetPurgeFlag (This : access Typ)
                          return Java.Boolean is abstract;

   procedure SetPurgeFlag (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;

   procedure CreateRelationType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_RoleInfo_Arr : access Javax.Management.Relation.RoleInfo.Arr_Obj) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.InvalidRelationTypeException.Except

   procedure AddRelationType (This : access Typ;
                              P1_RelationType : access Standard.Javax.Management.Relation.RelationType.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.InvalidRelationTypeException.Except

   function GetAllRelationTypeNames (This : access Typ)
                                     return access Java.Util.List.Typ'Class is abstract;

   function GetRoleInfos (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Util.List.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   function GetRoleInfo (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.Relation.RoleInfo.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.RoleInfoNotFoundException.Except

   procedure RemoveRelationType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   procedure CreateRelation (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class) is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRelationIdException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.InvalidRoleValueException.Except

   procedure AddRelation (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.NoSuchMethodException.Except,
   --  Javax.Management.Relation.InvalidRelationIdException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRelationServiceException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except and
   --  Javax.Management.Relation.InvalidRoleValueException.Except

   function IsRelationMBean (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Management.ObjectName.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function IsRelation (This : access Typ;
                        P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function HasRelation (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Boolean.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetAllRelationIds (This : access Typ)
                               return access Java.Util.List.Typ'Class is abstract;

   function CheckRoleReading (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   function CheckRoleWriting (This : access Typ;
                              P1_Role : access Standard.Javax.Management.Relation.Role.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Boolean : access Standard.Java.Lang.Boolean.Typ'Class)
                              return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   procedure SendRelationCreationNotification (This : access Typ;
                                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure SendRoleUpdateNotification (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class;
                                         P3_List : access Standard.Java.Util.List.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure SendRelationRemovalNotification (This : access Typ;
                                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                                              P2_List : access Standard.Java.Util.List.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure UpdateRoleMap (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class;
                            P3_List : access Standard.Java.Util.List.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure RemoveRelation (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure PurgeRelations (This : access Typ) is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function FindReferencingRelations (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                                      P3_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function FindAssociatedMBeans (This : access Typ;
                                  P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function FindRelationsOfType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Util.List.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   function GetRole (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Util.List.Typ'Class is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except and
   --  Javax.Management.Relation.RoleNotFoundException.Except

   function GetRoles (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String_Arr : access Java.Lang.String.Arr_Obj)
                      return access Javax.Management.Relation.RoleResult.Typ'Class is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetAllRoles (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.Relation.RoleResult.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except and
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function GetRoleCardinality (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except and
   --  Javax.Management.Relation.RoleNotFoundException.Except

   procedure SetRole (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class) is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationNotFoundException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRoleValueException.Except and
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except

   function SetRoles (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class)
                      return access Javax.Management.Relation.RoleResult.Typ'Class is abstract;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetReferencedMBeans (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetRelationTypeName (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsActive, "isActive");
   pragma Export (Java, GetPurgeFlag, "getPurgeFlag");
   pragma Export (Java, SetPurgeFlag, "setPurgeFlag");
   pragma Export (Java, CreateRelationType, "createRelationType");
   pragma Export (Java, AddRelationType, "addRelationType");
   pragma Export (Java, GetAllRelationTypeNames, "getAllRelationTypeNames");
   pragma Export (Java, GetRoleInfos, "getRoleInfos");
   pragma Export (Java, GetRoleInfo, "getRoleInfo");
   pragma Export (Java, RemoveRelationType, "removeRelationType");
   pragma Export (Java, CreateRelation, "createRelation");
   pragma Export (Java, AddRelation, "addRelation");
   pragma Export (Java, IsRelationMBean, "isRelationMBean");
   pragma Export (Java, IsRelation, "isRelation");
   pragma Export (Java, HasRelation, "hasRelation");
   pragma Export (Java, GetAllRelationIds, "getAllRelationIds");
   pragma Export (Java, CheckRoleReading, "checkRoleReading");
   pragma Export (Java, CheckRoleWriting, "checkRoleWriting");
   pragma Export (Java, SendRelationCreationNotification, "sendRelationCreationNotification");
   pragma Export (Java, SendRoleUpdateNotification, "sendRoleUpdateNotification");
   pragma Export (Java, SendRelationRemovalNotification, "sendRelationRemovalNotification");
   pragma Export (Java, UpdateRoleMap, "updateRoleMap");
   pragma Export (Java, RemoveRelation, "removeRelation");
   pragma Export (Java, PurgeRelations, "purgeRelations");
   pragma Export (Java, FindReferencingRelations, "findReferencingRelations");
   pragma Export (Java, FindAssociatedMBeans, "findAssociatedMBeans");
   pragma Export (Java, FindRelationsOfType, "findRelationsOfType");
   pragma Export (Java, GetRole, "getRole");
   pragma Export (Java, GetRoles, "getRoles");
   pragma Export (Java, GetAllRoles, "getAllRoles");
   pragma Export (Java, GetRoleCardinality, "getRoleCardinality");
   pragma Export (Java, SetRole, "setRole");
   pragma Export (Java, SetRoles, "setRoles");
   pragma Export (Java, GetReferencedMBeans, "getReferencedMBeans");
   pragma Export (Java, GetRelationTypeName, "getRelationTypeName");

end Javax.Management.Relation.RelationServiceMBean;
pragma Import (Java, Javax.Management.Relation.RelationServiceMBean, "javax.management.relation.RelationServiceMBean");
pragma Extensions_Allowed (Off);
