pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Java.Util.Map;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
limited with Javax.Management.Relation.Role;
limited with Javax.Management.Relation.RoleList;
limited with Javax.Management.Relation.RoleResult;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.Relation.RelationSupportMBean;

package Javax.Management.Relation.RelationSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            RelationSupportMBean_I : Javax.Management.Relation.RelationSupportMBean.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RelationSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Javax.Management.Relation.InvalidRoleValueException.Except and
   --  Java.Lang.IllegalArgumentException.Except

   function New_RelationSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                 P3_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                                 P4_String : access Standard.Java.Lang.String.Typ'Class;
                                 P5_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Javax.Management.Relation.InvalidRoleValueException.Except and
   --  Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRole (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Util.List.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except and
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function GetRoles (This : access Typ;
                      P1_String_Arr : access Java.Lang.String.Arr_Obj)
                      return access Javax.Management.Relation.RoleResult.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function GetAllRoles (This : access Typ)
                         return access Javax.Management.Relation.RoleResult.Typ'Class;
   --  can raise Javax.Management.Relation.RelationServiceNotRegisteredException.Except

   function RetrieveAllRoles (This : access Typ)
                              return access Javax.Management.Relation.RoleList.Typ'Class;

   function GetRoleCardinality (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RoleNotFoundException.Except

   procedure SetRole (This : access Typ;
                      P1_Role : access Standard.Javax.Management.Relation.Role.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRoleValueException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function SetRoles (This : access Typ;
                      P1_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class)
                      return access Javax.Management.Relation.RoleResult.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   procedure HandleMBeanUnregistration (This : access Typ;
                                        P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                        P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.RoleNotFoundException.Except,
   --  Javax.Management.Relation.InvalidRoleValueException.Except,
   --  Javax.Management.Relation.RelationServiceNotRegisteredException.Except,
   --  Javax.Management.Relation.RelationTypeNotFoundException.Except and
   --  Javax.Management.Relation.RelationNotFoundException.Except

   function GetReferencedMBeans (This : access Typ)
                                 return access Java.Util.Map.Typ'Class;

   function GetRelationTypeName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetRelationServiceName (This : access Typ)
                                    return access Javax.Management.ObjectName.Typ'Class;

   function GetRelationId (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);

   function IsInRelationService (This : access Typ)
                                 return access Java.Lang.Boolean.Typ'Class;

   procedure SetRelationServiceManagementFlag (This : access Typ;
                                               P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RelationSupport);
   pragma Import (Java, GetRole, "getRole");
   pragma Import (Java, GetRoles, "getRoles");
   pragma Import (Java, GetAllRoles, "getAllRoles");
   pragma Import (Java, RetrieveAllRoles, "retrieveAllRoles");
   pragma Import (Java, GetRoleCardinality, "getRoleCardinality");
   pragma Import (Java, SetRole, "setRole");
   pragma Import (Java, SetRoles, "setRoles");
   pragma Import (Java, HandleMBeanUnregistration, "handleMBeanUnregistration");
   pragma Import (Java, GetReferencedMBeans, "getReferencedMBeans");
   pragma Import (Java, GetRelationTypeName, "getRelationTypeName");
   pragma Import (Java, GetRelationServiceName, "getRelationServiceName");
   pragma Import (Java, GetRelationId, "getRelationId");
   pragma Import (Java, PreRegister, "preRegister");
   pragma Import (Java, PostRegister, "postRegister");
   pragma Import (Java, PreDeregister, "preDeregister");
   pragma Import (Java, PostDeregister, "postDeregister");
   pragma Import (Java, IsInRelationService, "isInRelationService");
   pragma Import (Java, SetRelationServiceManagementFlag, "setRelationServiceManagementFlag");

end Javax.Management.Relation.RelationSupport;
pragma Import (Java, Javax.Management.Relation.RelationSupport, "javax.management.relation.RelationSupport");
pragma Extensions_Allowed (Off);
