pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
with Java.Lang.Object;
with Javax.Management.Relation.Relation;

package Javax.Management.Relation.RelationSupportMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Relation_I : Javax.Management.Relation.Relation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsInRelationService (This : access Typ)
                                 return access Java.Lang.Boolean.Typ'Class is abstract;

   procedure SetRelationServiceManagementFlag (This : access Typ;
                                               P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsInRelationService, "isInRelationService");
   pragma Export (Java, SetRelationServiceManagementFlag, "setRelationServiceManagementFlag");

end Javax.Management.Relation.RelationSupportMBean;
pragma Import (Java, Javax.Management.Relation.RelationSupportMBean, "javax.management.relation.RelationSupportMBean");
pragma Extensions_Allowed (Off);
