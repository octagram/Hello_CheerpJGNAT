pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Management.Relation.RoleInfo;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Relation.RelationType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRelationTypeName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   function GetRoleInfos (This : access Typ)
                          return access Java.Util.List.Typ'Class is abstract;

   function GetRoleInfo (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.Relation.RoleInfo.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RoleInfoNotFoundException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRelationTypeName, "getRelationTypeName");
   pragma Export (Java, GetRoleInfos, "getRoleInfos");
   pragma Export (Java, GetRoleInfo, "getRoleInfo");

end Javax.Management.Relation.RelationType;
pragma Import (Java, Javax.Management.Relation.RelationType, "javax.management.relation.RelationType");
pragma Extensions_Allowed (Off);
