pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Relation.RelationException;

package Javax.Management.Relation.RelationTypeNotFoundException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Relation.RelationException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RelationTypeNotFoundException (This : Ref := null)
                                               return Ref;

   function New_RelationTypeNotFoundException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                               This : Ref := null)
                                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.management.relation.RelationTypeNotFoundException");
   pragma Java_Constructor (New_RelationTypeNotFoundException);

end Javax.Management.Relation.RelationTypeNotFoundException;
pragma Import (Java, Javax.Management.Relation.RelationTypeNotFoundException, "javax.management.relation.RelationTypeNotFoundException");
pragma Extensions_Allowed (Off);
