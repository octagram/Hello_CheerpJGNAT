pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Management.Relation.RoleInfo;
with Java.Lang.Object;
with Javax.Management.Relation.RelationType;

package Javax.Management.Relation.RelationTypeSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RelationType_I : Javax.Management.Relation.RelationType.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RelationTypeSupport (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_RoleInfo_Arr : access Javax.Management.Relation.RoleInfo.Arr_Obj; 
                                     This : Ref := null)
                                     return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.InvalidRelationTypeException.Except

   --  protected
   function New_RelationTypeSupport (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRelationTypeName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetRoleInfos (This : access Typ)
                          return access Java.Util.List.Typ'Class;

   function GetRoleInfo (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Management.Relation.RoleInfo.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.RoleInfoNotFoundException.Except

   --  protected
   procedure AddRoleInfo (This : access Typ;
                          P1_RoleInfo : access Standard.Javax.Management.Relation.RoleInfo.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Javax.Management.Relation.InvalidRelationTypeException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RelationTypeSupport);
   pragma Import (Java, GetRelationTypeName, "getRelationTypeName");
   pragma Import (Java, GetRoleInfos, "getRoleInfos");
   pragma Import (Java, GetRoleInfo, "getRoleInfo");
   pragma Import (Java, AddRoleInfo, "addRoleInfo");

end Javax.Management.Relation.RelationTypeSupport;
pragma Import (Java, Javax.Management.Relation.RelationTypeSupport, "javax.management.relation.RelationTypeSupport");
pragma Extensions_Allowed (Off);
