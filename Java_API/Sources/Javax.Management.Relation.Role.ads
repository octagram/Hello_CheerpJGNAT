pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Relation.Role is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Role (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_List : access Standard.Java.Util.List.Typ'Class; 
                      This : Ref := null)
                      return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRoleName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetRoleValue (This : access Typ)
                          return access Java.Util.List.Typ'Class;

   procedure SetRoleName (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure SetRoleValue (This : access Typ;
                           P1_List : access Standard.Java.Util.List.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function RoleValueToString (P1_List : access Standard.Java.Util.List.Typ'Class)
                               return access Java.Lang.String.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Role);
   pragma Import (Java, GetRoleName, "getRoleName");
   pragma Import (Java, GetRoleValue, "getRoleValue");
   pragma Import (Java, SetRoleName, "setRoleName");
   pragma Import (Java, SetRoleValue, "setRoleValue");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, RoleValueToString, "roleValueToString");

end Javax.Management.Relation.Role;
pragma Import (Java, Javax.Management.Relation.Role, "javax.management.relation.Role");
pragma Extensions_Allowed (Off);
