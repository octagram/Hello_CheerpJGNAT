pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Relation.RoleInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RoleInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_Boolean : Java.Boolean;
                          P4_Boolean : Java.Boolean;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int;
                          P7_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Javax.Management.Relation.InvalidRoleInfoException.Except,
   --  Java.Lang.ClassNotFoundException.Except and
   --  Javax.Management.NotCompliantMBeanException.Except

   function New_RoleInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_Boolean : Java.Boolean;
                          P4_Boolean : Java.Boolean; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Java.Lang.ClassNotFoundException.Except and
   --  Javax.Management.NotCompliantMBeanException.Except

   function New_RoleInfo (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except,
   --  Java.Lang.ClassNotFoundException.Except and
   --  Javax.Management.NotCompliantMBeanException.Except

   function New_RoleInfo (P1_RoleInfo : access Standard.Javax.Management.Relation.RoleInfo.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function IsReadable (This : access Typ)
                        return Java.Boolean;

   function IsWritable (This : access Typ)
                        return Java.Boolean;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetMinDegree (This : access Typ)
                          return Java.Int;

   function GetMaxDegree (This : access Typ)
                          return Java.Int;

   function GetRefMBeanClassName (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   function CheckMinDegree (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Boolean;

   function CheckMaxDegree (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ROLE_CARDINALITY_INFINITY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RoleInfo);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, IsReadable, "isReadable");
   pragma Import (Java, IsWritable, "isWritable");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetMinDegree, "getMinDegree");
   pragma Import (Java, GetMaxDegree, "getMaxDegree");
   pragma Import (Java, GetRefMBeanClassName, "getRefMBeanClassName");
   pragma Import (Java, CheckMinDegree, "checkMinDegree");
   pragma Import (Java, CheckMaxDegree, "checkMaxDegree");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ROLE_CARDINALITY_INFINITY, "ROLE_CARDINALITY_INFINITY");

end Javax.Management.Relation.RoleInfo;
pragma Import (Java, Javax.Management.Relation.RoleInfo, "javax.management.relation.RoleInfo");
pragma Extensions_Allowed (Off);
