pragma Extensions_Allowed (On);
limited with Javax.Management.Relation.Role;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.ArrayList;
with Java.Util.Collection;
with Java.Util.List;
with Java.Util.RandomAccess;

package Javax.Management.Relation.RoleList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            List_I : Java.Util.List.Ref;
            RandomAccess_I : Java.Util.RandomAccess.Ref)
    is new Java.Util.ArrayList.Typ(Serializable_I,
                                   Cloneable_I,
                                   Collection_I,
                                   List_I,
                                   RandomAccess_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RoleList (This : Ref := null)
                          return Ref;

   function New_RoleList (P1_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   function New_RoleList (P1_List : access Standard.Java.Util.List.Typ'Class; 
                          This : Ref := null)
                          return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function AsList (This : access Typ)
                    return access Java.Util.List.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Role : access Standard.Javax.Management.Relation.Role.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.IndexOutOfBoundsException.Except

   procedure Set (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Role : access Standard.Javax.Management.Relation.Role.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.IndexOutOfBoundsException.Except

   function AddAll (This : access Typ;
                    P1_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class)
                    return Java.Boolean;
   --  can raise Java.Lang.IndexOutOfBoundsException.Except

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class)
                    return Java.Boolean;
   --  can raise Java.Lang.IllegalArgumentException.Except and
   --  Java.Lang.IndexOutOfBoundsException.Except

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function AddAll (This : access Typ;
                    P1_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Collection : access Standard.Java.Util.Collection.Typ'Class)
                    return Java.Boolean;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RoleList);
   pragma Import (Java, AsList, "asList");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Set, "set");
   pragma Import (Java, AddAll, "addAll");

end Javax.Management.Relation.RoleList;
pragma Import (Java, Javax.Management.Relation.RoleList, "javax.management.relation.RoleList");
pragma Extensions_Allowed (Off);
