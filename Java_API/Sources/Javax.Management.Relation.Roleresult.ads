pragma Extensions_Allowed (On);
limited with Javax.Management.Relation.RoleList;
limited with Javax.Management.Relation.RoleUnresolvedList;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Relation.RoleResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RoleResult (P1_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class;
                            P2_RoleUnresolvedList : access Standard.Javax.Management.Relation.RoleUnresolvedList.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRoles (This : access Typ)
                      return access Javax.Management.Relation.RoleList.Typ'Class;

   function GetRolesUnresolved (This : access Typ)
                                return access Javax.Management.Relation.RoleUnresolvedList.Typ'Class;

   procedure SetRoles (This : access Typ;
                       P1_RoleList : access Standard.Javax.Management.Relation.RoleList.Typ'Class);

   procedure SetRolesUnresolved (This : access Typ;
                                 P1_RoleUnresolvedList : access Standard.Javax.Management.Relation.RoleUnresolvedList.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RoleResult);
   pragma Import (Java, GetRoles, "getRoles");
   pragma Import (Java, GetRolesUnresolved, "getRolesUnresolved");
   pragma Import (Java, SetRoles, "setRoles");
   pragma Import (Java, SetRolesUnresolved, "setRolesUnresolved");

end Javax.Management.Relation.RoleResult;
pragma Import (Java, Javax.Management.Relation.RoleResult, "javax.management.relation.RoleResult");
pragma Extensions_Allowed (Off);
