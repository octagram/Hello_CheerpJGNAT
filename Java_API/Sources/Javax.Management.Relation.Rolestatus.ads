pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Management.Relation.RoleStatus is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RoleStatus (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsRoleStatus (P1_Int : Java.Int)
                          return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NO_ROLE_WITH_NAME : constant Java.Int;

   --  final
   ROLE_NOT_READABLE : constant Java.Int;

   --  final
   ROLE_NOT_WRITABLE : constant Java.Int;

   --  final
   LESS_THAN_MIN_ROLE_DEGREE : constant Java.Int;

   --  final
   MORE_THAN_MAX_ROLE_DEGREE : constant Java.Int;

   --  final
   REF_MBEAN_OF_INCORRECT_CLASS : constant Java.Int;

   --  final
   REF_MBEAN_NOT_REGISTERED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RoleStatus);
   pragma Import (Java, IsRoleStatus, "isRoleStatus");
   pragma Import (Java, NO_ROLE_WITH_NAME, "NO_ROLE_WITH_NAME");
   pragma Import (Java, ROLE_NOT_READABLE, "ROLE_NOT_READABLE");
   pragma Import (Java, ROLE_NOT_WRITABLE, "ROLE_NOT_WRITABLE");
   pragma Import (Java, LESS_THAN_MIN_ROLE_DEGREE, "LESS_THAN_MIN_ROLE_DEGREE");
   pragma Import (Java, MORE_THAN_MAX_ROLE_DEGREE, "MORE_THAN_MAX_ROLE_DEGREE");
   pragma Import (Java, REF_MBEAN_OF_INCORRECT_CLASS, "REF_MBEAN_OF_INCORRECT_CLASS");
   pragma Import (Java, REF_MBEAN_NOT_REGISTERED, "REF_MBEAN_NOT_REGISTERED");

end Javax.Management.Relation.RoleStatus;
pragma Import (Java, Javax.Management.Relation.RoleStatus, "javax.management.relation.RoleStatus");
pragma Extensions_Allowed (Off);
