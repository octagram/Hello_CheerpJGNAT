pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Relation.RoleUnresolved is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RoleUnresolved (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_List : access Standard.Java.Util.List.Typ'Class;
                                P3_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRoleName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetRoleValue (This : access Typ)
                          return access Java.Util.List.Typ'Class;

   function GetProblemType (This : access Typ)
                            return Java.Int;

   procedure SetRoleName (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure SetRoleValue (This : access Typ;
                           P1_List : access Standard.Java.Util.List.Typ'Class);

   procedure SetProblemType (This : access Typ;
                             P1_Int : Java.Int);
   --  can raise Java.Lang.IllegalArgumentException.Except

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RoleUnresolved);
   pragma Import (Java, GetRoleName, "getRoleName");
   pragma Import (Java, GetRoleValue, "getRoleValue");
   pragma Import (Java, GetProblemType, "getProblemType");
   pragma Import (Java, SetRoleName, "setRoleName");
   pragma Import (Java, SetRoleValue, "setRoleValue");
   pragma Import (Java, SetProblemType, "setProblemType");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Relation.RoleUnresolved;
pragma Import (Java, Javax.Management.Relation.RoleUnresolved, "javax.management.relation.RoleUnresolved");
pragma Extensions_Allowed (Off);
