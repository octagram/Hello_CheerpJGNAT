pragma Extensions_Allowed (On);
package Javax.Management.Relation is
   pragma Preelaborate;
end Javax.Management.Relation;
pragma Import (Java, Javax.Management.Relation, "javax.management.relation");
pragma Extensions_Allowed (Off);
