pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Notification;

package Javax.Management.Remote.JMXConnectionNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Notification.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JMXConnectionNotification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                                           P4_Long : Java.Long;
                                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                                           P6_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetConnectionId (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OPENED : constant access Java.Lang.String.Typ'Class;

   --  final
   CLOSED : constant access Java.Lang.String.Typ'Class;

   --  final
   FAILED : constant access Java.Lang.String.Typ'Class;

   --  final
   NOTIFS_LOST : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JMXConnectionNotification);
   pragma Import (Java, GetConnectionId, "getConnectionId");
   pragma Import (Java, OPENED, "OPENED");
   pragma Import (Java, CLOSED, "CLOSED");
   pragma Import (Java, FAILED, "FAILED");
   pragma Import (Java, NOTIFS_LOST, "NOTIFS_LOST");

end Javax.Management.Remote.JMXConnectionNotification;
pragma Import (Java, Javax.Management.Remote.JMXConnectionNotification, "javax.management.remote.JMXConnectionNotification");
pragma Extensions_Allowed (Off);
