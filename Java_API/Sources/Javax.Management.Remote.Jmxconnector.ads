pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Management.MBeanServerConnection;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
limited with Javax.Security.Auth.Subject;
with Java.Io.Closeable;
with Java.Lang.Object;

package Javax.Management.Remote.JMXConnector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Closeable_I : Java.Io.Closeable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Connect (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Connect (This : access Typ;
                      P1_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   function GetMBeanServerConnection (This : access Typ)
                                      return access Javax.Management.MBeanServerConnection.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetMBeanServerConnection (This : access Typ;
                                      P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                                      return access Javax.Management.MBeanServerConnection.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure AddConnectionNotificationListener (This : access Typ;
                                                P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                                P3_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure RemoveConnectionNotificationListener (This : access Typ;
                                                   P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class) is abstract;
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure RemoveConnectionNotificationListener (This : access Typ;
                                                   P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                   P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                                   P3_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Management.ListenerNotFoundException.Except

   function GetConnectionId (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CREDENTIALS : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, GetMBeanServerConnection, "getMBeanServerConnection");
   pragma Export (Java, Close, "close");
   pragma Export (Java, AddConnectionNotificationListener, "addConnectionNotificationListener");
   pragma Export (Java, RemoveConnectionNotificationListener, "removeConnectionNotificationListener");
   pragma Export (Java, GetConnectionId, "getConnectionId");
   pragma Import (Java, CREDENTIALS, "CREDENTIALS");

end Javax.Management.Remote.JMXConnector;
pragma Import (Java, Javax.Management.Remote.JMXConnector, "javax.management.remote.JMXConnector");
pragma Extensions_Allowed (Off);
