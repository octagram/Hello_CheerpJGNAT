pragma Extensions_Allowed (On);
limited with Java.Util.Map;
limited with Javax.Management.Remote.JMXConnector;
limited with Javax.Management.Remote.JMXServiceURL;
with Java.Lang.Object;

package Javax.Management.Remote.JMXConnectorProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewJMXConnector (This : access Typ;
                             P1_JMXServiceURL : access Standard.Javax.Management.Remote.JMXServiceURL.Typ'Class;
                             P2_Map : access Standard.Java.Util.Map.Typ'Class)
                             return access Javax.Management.Remote.JMXConnector.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NewJMXConnector, "newJMXConnector");

end Javax.Management.Remote.JMXConnectorProvider;
pragma Import (Java, Javax.Management.Remote.JMXConnectorProvider, "javax.management.remote.JMXConnectorProvider");
pragma Extensions_Allowed (Off);
