pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
limited with Javax.Management.Remote.JMXConnector;
limited with Javax.Management.Remote.MBeanServerForwarder;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.NotificationBroadcasterSupport;
with Javax.Management.NotificationEmitter;
with Javax.Management.Remote.JMXAddressable;
with Javax.Management.Remote.JMXConnectorServerMBean;

package Javax.Management.Remote.JMXConnectorServer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            JMXAddressable_I : Javax.Management.Remote.JMXAddressable.Ref;
            JMXConnectorServerMBean_I : Javax.Management.Remote.JMXConnectorServerMBean.Ref)
    is abstract new Javax.Management.NotificationBroadcasterSupport.Typ(NotificationEmitter_I)
      with null record;

   function New_JMXConnectorServer (This : Ref := null)
                                    return Ref;

   function New_JMXConnectorServer (P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetMBeanServer (This : access Typ)
                            return access Javax.Management.MBeanServer.Typ'Class;

   --  synchronized
   procedure SetMBeanServerForwarder (This : access Typ;
                                      P1_MBeanServerForwarder : access Standard.Javax.Management.Remote.MBeanServerForwarder.Typ'Class);

   function GetConnectionIds (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function ToJMXConnector (This : access Typ;
                            P1_Map : access Standard.Java.Util.Map.Typ'Class)
                            return access Javax.Management.Remote.JMXConnector.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure ConnectionOpened (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   procedure ConnectionClosed (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   procedure ConnectionFailed (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  synchronized
   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   --  synchronized
   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   AUTHENTICATOR : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JMXConnectorServer);
   pragma Import (Java, GetMBeanServer, "getMBeanServer");
   pragma Import (Java, SetMBeanServerForwarder, "setMBeanServerForwarder");
   pragma Import (Java, GetConnectionIds, "getConnectionIds");
   pragma Import (Java, ToJMXConnector, "toJMXConnector");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");
   pragma Import (Java, ConnectionOpened, "connectionOpened");
   pragma Import (Java, ConnectionClosed, "connectionClosed");
   pragma Import (Java, ConnectionFailed, "connectionFailed");
   pragma Import (Java, PreRegister, "preRegister");
   pragma Import (Java, PostRegister, "postRegister");
   pragma Import (Java, PreDeregister, "preDeregister");
   pragma Import (Java, PostDeregister, "postDeregister");
   pragma Import (Java, AUTHENTICATOR, "AUTHENTICATOR");

end Javax.Management.Remote.JMXConnectorServer;
pragma Import (Java, Javax.Management.Remote.JMXConnectorServer, "javax.management.remote.JMXConnectorServer");
pragma Extensions_Allowed (Off);
