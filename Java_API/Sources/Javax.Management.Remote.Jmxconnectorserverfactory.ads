pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.Remote.JMXConnectorServer;
limited with Javax.Management.Remote.JMXServiceURL;
with Java.Lang.Object;

package Javax.Management.Remote.JMXConnectorServerFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewJMXConnectorServer (P1_JMXServiceURL : access Standard.Javax.Management.Remote.JMXServiceURL.Typ'Class;
                                   P2_Map : access Standard.Java.Util.Map.Typ'Class;
                                   P3_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class)
                                   return access Javax.Management.Remote.JMXConnectorServer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_CLASS_LOADER : constant access Java.Lang.String.Typ'Class;

   --  final
   DEFAULT_CLASS_LOADER_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   PROTOCOL_PROVIDER_PACKAGES : constant access Java.Lang.String.Typ'Class;

   --  final
   PROTOCOL_PROVIDER_CLASS_LOADER : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewJMXConnectorServer, "newJMXConnectorServer");
   pragma Import (Java, DEFAULT_CLASS_LOADER, "DEFAULT_CLASS_LOADER");
   pragma Import (Java, DEFAULT_CLASS_LOADER_NAME, "DEFAULT_CLASS_LOADER_NAME");
   pragma Import (Java, PROTOCOL_PROVIDER_PACKAGES, "PROTOCOL_PROVIDER_PACKAGES");
   pragma Import (Java, PROTOCOL_PROVIDER_CLASS_LOADER, "PROTOCOL_PROVIDER_CLASS_LOADER");

end Javax.Management.Remote.JMXConnectorServerFactory;
pragma Import (Java, Javax.Management.Remote.JMXConnectorServerFactory, "javax.management.remote.JMXConnectorServerFactory");
pragma Extensions_Allowed (Off);
