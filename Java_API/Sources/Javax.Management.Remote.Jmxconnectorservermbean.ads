pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Management.Remote.JMXConnector;
limited with Javax.Management.Remote.JMXServiceURL;
limited with Javax.Management.Remote.MBeanServerForwarder;
with Java.Lang.Object;

package Javax.Management.Remote.JMXConnectorServerMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Start (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Stop (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   function IsActive (This : access Typ)
                      return Java.Boolean is abstract;

   procedure SetMBeanServerForwarder (This : access Typ;
                                      P1_MBeanServerForwarder : access Standard.Javax.Management.Remote.MBeanServerForwarder.Typ'Class) is abstract;

   function GetConnectionIds (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function GetAddress (This : access Typ)
                        return access Javax.Management.Remote.JMXServiceURL.Typ'Class is abstract;

   function GetAttributes (This : access Typ)
                           return access Java.Util.Map.Typ'Class is abstract;

   function ToJMXConnector (This : access Typ;
                            P1_Map : access Standard.Java.Util.Map.Typ'Class)
                            return access Javax.Management.Remote.JMXConnector.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Start, "start");
   pragma Export (Java, Stop, "stop");
   pragma Export (Java, IsActive, "isActive");
   pragma Export (Java, SetMBeanServerForwarder, "setMBeanServerForwarder");
   pragma Export (Java, GetConnectionIds, "getConnectionIds");
   pragma Export (Java, GetAddress, "getAddress");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, ToJMXConnector, "toJMXConnector");

end Javax.Management.Remote.JMXConnectorServerMBean;
pragma Import (Java, Javax.Management.Remote.JMXConnectorServerMBean, "javax.management.remote.JMXConnectorServerMBean");
pragma Extensions_Allowed (Off);
