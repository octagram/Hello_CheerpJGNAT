pragma Extensions_Allowed (On);
limited with Java.Lang.Error;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.IOException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Remote.JMXServerErrorException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Io.IOException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JMXServerErrorException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Error : access Standard.Java.Lang.Error.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.management.remote.JMXServerErrorException");
   pragma Java_Constructor (New_JMXServerErrorException);
   pragma Import (Java, GetCause, "getCause");

end Javax.Management.Remote.JMXServerErrorException;
pragma Import (Java, Javax.Management.Remote.JMXServerErrorException, "javax.management.remote.JMXServerErrorException");
pragma Extensions_Allowed (Off);
