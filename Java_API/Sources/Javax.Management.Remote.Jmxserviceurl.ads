pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Remote.JMXServiceURL is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JMXServiceURL (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   function New_JMXServiceURL (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   function New_JMXServiceURL (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Int : Java.Int;
                               P4_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Java.Net.MalformedURLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetProtocol (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetHost (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetPort (This : access Typ)
                     return Java.Int;

   function GetURLPath (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JMXServiceURL);
   pragma Import (Java, GetProtocol, "getProtocol");
   pragma Import (Java, GetHost, "getHost");
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, GetURLPath, "getURLPath");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Management.Remote.JMXServiceURL;
pragma Import (Java, Javax.Management.Remote.JMXServiceURL, "javax.management.remote.JMXServiceURL");
pragma Extensions_Allowed (Off);
