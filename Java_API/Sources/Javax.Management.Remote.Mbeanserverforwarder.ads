pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Management.MBeanServer;

package Javax.Management.Remote.MBeanServerForwarder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MBeanServer_I : Javax.Management.MBeanServer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMBeanServer (This : access Typ)
                            return access Javax.Management.MBeanServer.Typ'Class is abstract;

   procedure SetMBeanServer (This : access Typ;
                             P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMBeanServer, "getMBeanServer");
   pragma Export (Java, SetMBeanServer, "setMBeanServer");

end Javax.Management.Remote.MBeanServerForwarder;
pragma Import (Java, Javax.Management.Remote.MBeanServerForwarder, "javax.management.remote.MBeanServerForwarder");
pragma Extensions_Allowed (Off);
