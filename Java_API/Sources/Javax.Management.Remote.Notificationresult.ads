pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Remote.TargetedNotification;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Remote.NotificationResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NotificationResult (P1_Long : Java.Long;
                                    P2_Long : Java.Long;
                                    P3_TargetedNotification_Arr : access Javax.Management.Remote.TargetedNotification.Arr_Obj; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEarliestSequenceNumber (This : access Typ)
                                       return Java.Long;

   function GetNextSequenceNumber (This : access Typ)
                                   return Java.Long;

   function GetTargetedNotifications (This : access Typ)
                                      return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NotificationResult);
   pragma Import (Java, GetEarliestSequenceNumber, "getEarliestSequenceNumber");
   pragma Import (Java, GetNextSequenceNumber, "getNextSequenceNumber");
   pragma Import (Java, GetTargetedNotifications, "getTargetedNotifications");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Remote.NotificationResult;
pragma Import (Java, Javax.Management.Remote.NotificationResult, "javax.management.remote.NotificationResult");
pragma Extensions_Allowed (Off);
