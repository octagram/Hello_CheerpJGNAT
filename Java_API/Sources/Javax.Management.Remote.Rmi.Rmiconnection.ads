pragma Extensions_Allowed (On);
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Rmi.MarshalledObject;
limited with Java.Util.Set;
limited with Javax.Management.AttributeList;
limited with Javax.Management.MBeanInfo;
limited with Javax.Management.ObjectInstance;
limited with Javax.Management.ObjectName;
limited with Javax.Management.Remote.NotificationResult;
limited with Javax.Security.Auth.Subject;
with Java.Io.Closeable;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Javax.Management.Remote.Rmi.RMIConnection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Closeable_I : Java.Io.Closeable.Ref;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetConnectionId (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P4_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                         P4_String_Arr : access Java.Lang.String.Arr_Obj;
                         P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                         P5_String_Arr : access Java.Lang.String.Arr_Obj;
                         P6_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.ReflectionException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure UnregisterMBean (This : access Typ;
                              P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                              P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanRegistrationException.Except and
   --  Java.Io.IOException.Except

   function GetObjectInstance (This : access Typ;
                               P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                               P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                               return access Javax.Management.ObjectInstance.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   function QueryMBeans (This : access Typ;
                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                         P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Java.Util.Set.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function QueryNames (This : access Typ;
                        P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                        P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                        P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                        return access Java.Util.Set.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function IsRegistered (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return Java.Boolean is abstract;
   --  can raise Java.Io.IOException.Except

   function GetMBeanCount (This : access Typ;
                           P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                           return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetAttribute (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.MBeanException.Except,
   --  Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function GetAttributes (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj;
                           P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   procedure SetAttribute (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                           P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InvalidAttributeValueException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function SetAttributes (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                           P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function Invoke (This : access Typ;
                    P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                    P4_String_Arr : access Java.Lang.String.Arr_Obj;
                    P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function GetDefaultDomain (This : access Typ;
                              P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                              return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetDomains (This : access Typ;
                        P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                        return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Io.IOException.Except

   function GetMBeanInfo (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return access Javax.Management.MBeanInfo.Typ'Class is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.IntrospectionException.Except,
   --  Javax.Management.ReflectionException.Except and
   --  Java.Io.IOException.Except

   function IsInstanceOf (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return Java.Boolean is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure AddNotificationListener (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                      P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                      P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ListenerNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                         P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                         P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ListenerNotFoundException.Except and
   --  Java.Io.IOException.Except

   function AddNotificationListeners (This : access Typ;
                                      P1_ObjectName_Arr : access Javax.Management.ObjectName.Arr_Obj;
                                      P2_MarshalledObject_Arr : access Java.Rmi.MarshalledObject.Arr_Obj;
                                      P3_Subject_Arr : access Javax.Security.Auth.Subject.Arr_Obj)
                                      return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RemoveNotificationListeners (This : access Typ;
                                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                          P2_Integer_Arr : access Java.Lang.Integer.Arr_Obj;
                                          P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.ListenerNotFoundException.Except and
   --  Java.Io.IOException.Except

   function FetchNotifications (This : access Typ;
                                P1_Long : Java.Long;
                                P2_Int : Java.Int;
                                P3_Long : Java.Long)
                                return access Javax.Management.Remote.NotificationResult.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetConnectionId, "getConnectionId");
   pragma Export (Java, Close, "close");
   pragma Export (Java, CreateMBean, "createMBean");
   pragma Export (Java, UnregisterMBean, "unregisterMBean");
   pragma Export (Java, GetObjectInstance, "getObjectInstance");
   pragma Export (Java, QueryMBeans, "queryMBeans");
   pragma Export (Java, QueryNames, "queryNames");
   pragma Export (Java, IsRegistered, "isRegistered");
   pragma Export (Java, GetMBeanCount, "getMBeanCount");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, SetAttribute, "setAttribute");
   pragma Export (Java, SetAttributes, "setAttributes");
   pragma Export (Java, Invoke, "invoke");
   pragma Export (Java, GetDefaultDomain, "getDefaultDomain");
   pragma Export (Java, GetDomains, "getDomains");
   pragma Export (Java, GetMBeanInfo, "getMBeanInfo");
   pragma Export (Java, IsInstanceOf, "isInstanceOf");
   pragma Export (Java, AddNotificationListener, "addNotificationListener");
   pragma Export (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Export (Java, AddNotificationListeners, "addNotificationListeners");
   pragma Export (Java, RemoveNotificationListeners, "removeNotificationListeners");
   pragma Export (Java, FetchNotifications, "fetchNotifications");

end Javax.Management.Remote.Rmi.RMIConnection;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIConnection, "javax.management.remote.rmi.RMIConnection");
pragma Extensions_Allowed (Off);
