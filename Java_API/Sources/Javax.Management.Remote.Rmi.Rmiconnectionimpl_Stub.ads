pragma Extensions_Allowed (On);
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Rmi.MarshalledObject;
limited with Java.Rmi.Server.RemoteRef;
limited with Java.Util.Set;
limited with Javax.Management.AttributeList;
limited with Javax.Management.MBeanInfo;
limited with Javax.Management.ObjectInstance;
limited with Javax.Management.ObjectName;
limited with Javax.Management.Remote.NotificationResult;
limited with Javax.Security.Auth.Subject;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Remote;
with Java.Rmi.Server.RemoteStub;
with Javax.Management.Remote.Rmi.RMIConnection;

package Javax.Management.Remote.Rmi.RMIConnectionImpl_Stub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref;
            RMIConnection_I : Javax.Management.Remote.Rmi.RMIConnection.Ref)
    is new Java.Rmi.Server.RemoteStub.Typ(Serializable_I,
                                          Remote_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RMIConnectionImpl_Stub (P1_RemoteRef : access Standard.Java.Rmi.Server.RemoteRef.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotificationListener (This : access Typ;
                                      P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                      P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                      P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                      P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Management.InstanceNotFoundException.Except

   function AddNotificationListeners (This : access Typ;
                                      P1_ObjectName_Arr : access Javax.Management.ObjectName.Arr_Obj;
                                      P2_MarshalledObject_Arr : access Java.Rmi.MarshalledObject.Arr_Obj;
                                      P3_Subject_Arr : access Javax.Security.Auth.Subject.Arr_Obj)
                                      return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Management.InstanceNotFoundException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                         P4_String_Arr : access Java.Lang.String.Arr_Obj;
                         P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                         P5_String_Arr : access Java.Lang.String.Arr_Obj;
                         P6_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P4_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function CreateMBean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Javax.Management.ObjectInstance.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceAlreadyExistsException.Except,
   --  Javax.Management.MBeanException.Except,
   --  Javax.Management.MBeanRegistrationException.Except,
   --  Javax.Management.NotCompliantMBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function FetchNotifications (This : access Typ;
                                P1_Long : Java.Long;
                                P2_Int : Java.Int;
                                P3_Long : Java.Long)
                                return access Javax.Management.Remote.NotificationResult.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetAttribute (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetAttributes (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj;
                           P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetConnectionId (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetDefaultDomain (This : access Typ;
                              P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                              return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetDomains (This : access Typ;
                        P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                        return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Io.IOException.Except

   function GetMBeanCount (This : access Typ;
                           P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                           return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetMBeanInfo (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return access Javax.Management.MBeanInfo.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.IntrospectionException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetObjectInstance (This : access Typ;
                               P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                               P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                               return access Javax.Management.ObjectInstance.Typ'Class;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Management.InstanceNotFoundException.Except

   function Invoke (This : access Typ;
                    P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                    P4_String_Arr : access Java.Lang.String.Arr_Obj;
                    P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function IsInstanceOf (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return Java.Boolean;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Management.InstanceNotFoundException.Except

   function IsRegistered (This : access Typ;
                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                          P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                          return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   function QueryMBeans (This : access Typ;
                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                         P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                         P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                         return access Java.Util.Set.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function QueryNames (This : access Typ;
                        P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                        P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                        P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                        return access Java.Util.Set.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P3_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                         P4_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                                         P5_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.ListenerNotFoundException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                         P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.ListenerNotFoundException.Except

   procedure RemoveNotificationListeners (This : access Typ;
                                          P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                                          P2_Integer_Arr : access Java.Lang.Integer.Arr_Obj;
                                          P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.ListenerNotFoundException.Except

   procedure SetAttribute (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                           P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InstanceNotFoundException.Except,
   --  Javax.Management.InvalidAttributeValueException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function SetAttributes (This : access Typ;
                           P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                           P2_MarshalledObject : access Standard.Java.Rmi.MarshalledObject.Typ'Class;
                           P3_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class;
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.ReflectionException.Except

   procedure UnregisterMBean (This : access Typ;
                              P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class;
                              P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class);
   --  can raise Java.Io.IOException.Except,
   --  Javax.Management.InstanceNotFoundException.Except and
   --  Javax.Management.MBeanRegistrationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMIConnectionImpl_Stub);
   pragma Import (Java, AddNotificationListener, "addNotificationListener");
   pragma Import (Java, AddNotificationListeners, "addNotificationListeners");
   pragma Import (Java, Close, "close");
   pragma Import (Java, CreateMBean, "createMBean");
   pragma Import (Java, FetchNotifications, "fetchNotifications");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetConnectionId, "getConnectionId");
   pragma Import (Java, GetDefaultDomain, "getDefaultDomain");
   pragma Import (Java, GetDomains, "getDomains");
   pragma Import (Java, GetMBeanCount, "getMBeanCount");
   pragma Import (Java, GetMBeanInfo, "getMBeanInfo");
   pragma Import (Java, GetObjectInstance, "getObjectInstance");
   pragma Import (Java, Invoke, "invoke");
   pragma Import (Java, IsInstanceOf, "isInstanceOf");
   pragma Import (Java, IsRegistered, "isRegistered");
   pragma Import (Java, QueryMBeans, "queryMBeans");
   pragma Import (Java, QueryNames, "queryNames");
   pragma Import (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Import (Java, RemoveNotificationListeners, "removeNotificationListeners");
   pragma Import (Java, SetAttribute, "setAttribute");
   pragma Import (Java, SetAttributes, "setAttributes");
   pragma Import (Java, UnregisterMBean, "unregisterMBean");

end Javax.Management.Remote.Rmi.RMIConnectionImpl_Stub;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIConnectionImpl_Stub, "javax.management.remote.rmi.RMIConnectionImpl_Stub");
pragma Extensions_Allowed (Off);
