pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Management.MBeanServerConnection;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
limited with Javax.Management.Remote.JMXServiceURL;
limited with Javax.Management.Remote.Rmi.RMIServer;
limited with Javax.Security.Auth.Subject;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Remote.JMXAddressable;
with Javax.Management.Remote.JMXConnector;

package Javax.Management.Remote.Rmi.RMIConnector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            JMXAddressable_I : Javax.Management.Remote.JMXAddressable.Ref;
            JMXConnector_I : Javax.Management.Remote.JMXConnector.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RMIConnector (P1_JMXServiceURL : access Standard.Javax.Management.Remote.JMXServiceURL.Typ'Class;
                              P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_RMIConnector (P1_RMIServer : access Standard.Javax.Management.Remote.Rmi.RMIServer.Typ'Class;
                              P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetAddress (This : access Typ)
                        return access Javax.Management.Remote.JMXServiceURL.Typ'Class;

   procedure Connect (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Connect (This : access Typ;
                      P1_Map : access Standard.Java.Util.Map.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function GetConnectionId (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function GetMBeanServerConnection (This : access Typ)
                                      return access Javax.Management.MBeanServerConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function GetMBeanServerConnection (This : access Typ;
                                      P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                                      return access Javax.Management.MBeanServerConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure AddConnectionNotificationListener (This : access Typ;
                                                P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                                P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveConnectionNotificationListener (This : access Typ;
                                                   P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure RemoveConnectionNotificationListener (This : access Typ;
                                                   P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                                   P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                                   P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   --  synchronized
   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMIConnector);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetAddress, "getAddress");
   pragma Import (Java, Connect, "connect");
   pragma Import (Java, GetConnectionId, "getConnectionId");
   pragma Import (Java, GetMBeanServerConnection, "getMBeanServerConnection");
   pragma Import (Java, AddConnectionNotificationListener, "addConnectionNotificationListener");
   pragma Import (Java, RemoveConnectionNotificationListener, "removeConnectionNotificationListener");
   pragma Import (Java, Close, "close");

end Javax.Management.Remote.Rmi.RMIConnector;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIConnector, "javax.management.remote.rmi.RMIConnector");
pragma Extensions_Allowed (Off);
