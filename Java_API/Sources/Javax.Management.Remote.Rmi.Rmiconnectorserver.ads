pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.Remote.JMXConnector;
limited with Javax.Management.Remote.JMXServiceURL;
limited with Javax.Management.Remote.MBeanServerForwarder;
limited with Javax.Management.Remote.Rmi.RMIServerImpl;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.NotificationEmitter;
with Javax.Management.Remote.JMXAddressable;
with Javax.Management.Remote.JMXConnectorServer;
with Javax.Management.Remote.JMXConnectorServerMBean;

package Javax.Management.Remote.Rmi.RMIConnectorServer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            JMXAddressable_I : Javax.Management.Remote.JMXAddressable.Ref;
            JMXConnectorServerMBean_I : Javax.Management.Remote.JMXConnectorServerMBean.Ref)
    is new Javax.Management.Remote.JMXConnectorServer.Typ(MBeanRegistration_I,
                                                          NotificationEmitter_I,
                                                          JMXAddressable_I,
                                                          JMXConnectorServerMBean_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RMIConnectorServer (P1_JMXServiceURL : access Standard.Javax.Management.Remote.JMXServiceURL.Typ'Class;
                                    P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Io.IOException.Except

   function New_RMIConnectorServer (P1_JMXServiceURL : access Standard.Javax.Management.Remote.JMXServiceURL.Typ'Class;
                                    P2_Map : access Standard.Java.Util.Map.Typ'Class;
                                    P3_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Io.IOException.Except

   function New_RMIConnectorServer (P1_JMXServiceURL : access Standard.Javax.Management.Remote.JMXServiceURL.Typ'Class;
                                    P2_Map : access Standard.Java.Util.Map.Typ'Class;
                                    P3_RMIServerImpl : access Standard.Javax.Management.Remote.Rmi.RMIServerImpl.Typ'Class;
                                    P4_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToJMXConnector (This : access Typ;
                            P1_Map : access Standard.Java.Util.Map.Typ'Class)
                            return access Javax.Management.Remote.JMXConnector.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Start (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Stop (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   function IsActive (This : access Typ)
                      return Java.Boolean;

   function GetAddress (This : access Typ)
                        return access Javax.Management.Remote.JMXServiceURL.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Java.Util.Map.Typ'Class;

   --  synchronized
   procedure SetMBeanServerForwarder (This : access Typ;
                                      P1_MBeanServerForwarder : access Standard.Javax.Management.Remote.MBeanServerForwarder.Typ'Class);

   --  protected
   procedure ConnectionOpened (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   procedure ConnectionClosed (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   procedure ConnectionFailed (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JNDI_REBIND_ATTRIBUTE : constant access Java.Lang.String.Typ'Class;

   --  final
   RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE : constant access Java.Lang.String.Typ'Class;

   --  final
   RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMIConnectorServer);
   pragma Import (Java, ToJMXConnector, "toJMXConnector");
   pragma Import (Java, Start, "start");
   pragma Import (Java, Stop, "stop");
   pragma Import (Java, IsActive, "isActive");
   pragma Import (Java, GetAddress, "getAddress");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, SetMBeanServerForwarder, "setMBeanServerForwarder");
   pragma Import (Java, ConnectionOpened, "connectionOpened");
   pragma Import (Java, ConnectionClosed, "connectionClosed");
   pragma Import (Java, ConnectionFailed, "connectionFailed");
   pragma Import (Java, JNDI_REBIND_ATTRIBUTE, "JNDI_REBIND_ATTRIBUTE");
   pragma Import (Java, RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE, "RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE");
   pragma Import (Java, RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE, "RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE");

end Javax.Management.Remote.Rmi.RMIConnectorServer;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIConnectorServer, "javax.management.remote.rmi.RMIConnectorServer");
pragma Extensions_Allowed (Off);
