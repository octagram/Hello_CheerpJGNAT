pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Remote;
limited with Java.Util.Map;
limited with Javax.Management.Remote.Rmi.RMIConnection;
limited with Javax.Security.Auth.Subject;
with Java.Io.Closeable;
with Java.Lang.Object;
with Javax.Management.Remote.Rmi.RMIServer;
with Javax.Management.Remote.Rmi.RMIServerImpl;

package Javax.Management.Remote.Rmi.RMIIIOPServerImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            RMIServer_I : Javax.Management.Remote.Rmi.RMIServer.Ref)
    is new Javax.Management.Remote.Rmi.RMIServerImpl.Typ(Closeable_I,
                                                         RMIServer_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RMIIIOPServerImpl (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Export (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetProtocol (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function ToStub (This : access Typ)
                    return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   function MakeClient (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                        return access Javax.Management.Remote.Rmi.RMIConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure CloseClient (This : access Typ;
                          P1_RMIConnection : access Standard.Javax.Management.Remote.Rmi.RMIConnection.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure CloseServer (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMIIIOPServerImpl);
   pragma Import (Java, Export, "export");
   pragma Import (Java, GetProtocol, "getProtocol");
   pragma Import (Java, ToStub, "toStub");
   pragma Import (Java, MakeClient, "makeClient");
   pragma Import (Java, CloseClient, "closeClient");
   pragma Import (Java, CloseServer, "closeServer");

end Javax.Management.Remote.Rmi.RMIIIOPServerImpl;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIIIOPServerImpl, "javax.management.remote.rmi.RMIIIOPServerImpl");
pragma Extensions_Allowed (Off);
