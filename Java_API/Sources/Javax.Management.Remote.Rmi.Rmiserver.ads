pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Remote.Rmi.RMIConnection;
with Java.Lang.Object;
with Java.Rmi.Remote;

package Javax.Management.Remote.Rmi.RMIServer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Remote_I : Java.Rmi.Remote.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   function NewClient (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Javax.Management.Remote.Rmi.RMIConnection.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, NewClient, "newClient");

end Javax.Management.Remote.Rmi.RMIServer;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIServer, "javax.management.remote.rmi.RMIServer");
pragma Extensions_Allowed (Off);
