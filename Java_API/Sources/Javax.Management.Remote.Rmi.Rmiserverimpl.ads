pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Rmi.Remote;
limited with Java.Util.Map;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.Remote.Rmi.RMIConnection;
limited with Javax.Security.Auth.Subject;
with Java.Io.Closeable;
with Java.Lang.Object;
with Javax.Management.Remote.Rmi.RMIServer;

package Javax.Management.Remote.Rmi.RMIServerImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            RMIServer_I : Javax.Management.Remote.Rmi.RMIServer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_RMIServerImpl (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Export (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   function ToStub (This : access Typ)
                    return access Java.Rmi.Remote.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure SetDefaultClassLoader (This : access Typ;
                                    P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class);

   --  synchronized
   function GetDefaultClassLoader (This : access Typ)
                                   return access Java.Lang.ClassLoader.Typ'Class;

   --  synchronized
   procedure SetMBeanServer (This : access Typ;
                             P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class);

   --  synchronized
   function GetMBeanServer (This : access Typ)
                            return access Javax.Management.MBeanServer.Typ'Class;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function NewClient (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Javax.Management.Remote.Rmi.RMIConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   function MakeClient (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class)
                        return access Javax.Management.Remote.Rmi.RMIConnection.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure CloseClient (This : access Typ;
                          P1_RMIConnection : access Standard.Javax.Management.Remote.Rmi.RMIConnection.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetProtocol (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   --  protected
   procedure ClientClosed (This : access Typ;
                           P1_RMIConnection : access Standard.Javax.Management.Remote.Rmi.RMIConnection.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure CloseServer (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMIServerImpl);
   pragma Export (Java, Export, "export");
   pragma Export (Java, ToStub, "toStub");
   pragma Export (Java, SetDefaultClassLoader, "setDefaultClassLoader");
   pragma Export (Java, GetDefaultClassLoader, "getDefaultClassLoader");
   pragma Export (Java, SetMBeanServer, "setMBeanServer");
   pragma Export (Java, GetMBeanServer, "getMBeanServer");
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, NewClient, "newClient");
   pragma Export (Java, MakeClient, "makeClient");
   pragma Export (Java, CloseClient, "closeClient");
   pragma Export (Java, GetProtocol, "getProtocol");
   pragma Export (Java, ClientClosed, "clientClosed");
   pragma Export (Java, Close, "close");
   pragma Export (Java, CloseServer, "closeServer");

end Javax.Management.Remote.Rmi.RMIServerImpl;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIServerImpl, "javax.management.remote.rmi.RMIServerImpl");
pragma Extensions_Allowed (Off);
