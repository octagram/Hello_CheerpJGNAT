pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Server.RemoteRef;
limited with Javax.Management.Remote.Rmi.RMIConnection;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Remote;
with Java.Rmi.Server.RemoteStub;
with Javax.Management.Remote.Rmi.RMIServer;

package Javax.Management.Remote.Rmi.RMIServerImpl_Stub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Remote_I : Java.Rmi.Remote.Ref;
            RMIServer_I : Javax.Management.Remote.Rmi.RMIServer.Ref)
    is new Java.Rmi.Server.RemoteStub.Typ(Serializable_I,
                                          Remote_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RMIServerImpl_Stub (P1_RemoteRef : access Standard.Java.Rmi.Server.RemoteRef.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function NewClient (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Javax.Management.Remote.Rmi.RMIConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RMIServerImpl_Stub);
   pragma Import (Java, GetVersion, "getVersion");
   pragma Import (Java, NewClient, "newClient");

end Javax.Management.Remote.Rmi.RMIServerImpl_Stub;
pragma Import (Java, Javax.Management.Remote.Rmi.RMIServerImpl_Stub, "javax.management.remote.rmi.RMIServerImpl_Stub");
pragma Extensions_Allowed (Off);
