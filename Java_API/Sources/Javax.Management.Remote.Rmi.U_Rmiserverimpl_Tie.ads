pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Rmi.Remote;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.Portable.ResponseHandler;
with Java.Lang.Object;
with Javax.Rmi.CORBA.Tie;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA_2_3.Portable.ObjectImpl;

package Javax.Management.Remote.Rmi.U_RMIServerImpl_Tie is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Tie_I : Javax.Rmi.CORBA.Tie.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is new Org.Omg.CORBA_2_3.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_RMIServerImpl_Tie (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;

   function U_Invoke (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class;
                      P3_ResponseHandler : access Standard.Org.Omg.CORBA.Portable.ResponseHandler.Typ'Class)
                      return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
   --  can raise Org.Omg.CORBA.SystemException.Except

   procedure Deactivate (This : access Typ);

   function GetTarget (This : access Typ)
                       return access Java.Rmi.Remote.Typ'Class;

   function Orb (This : access Typ)
                 return access Org.Omg.CORBA.ORB.Typ'Class;

   procedure Orb (This : access Typ;
                  P1_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class);

   procedure SetTarget (This : access Typ;
                        P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class);

   function ThisObject (This : access Typ)
                        return access Org.Omg.CORBA.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_RMIServerImpl_Tie);
   pragma Import (Java, U_Ids, "_ids");
   pragma Import (Java, U_Invoke, "_invoke");
   pragma Import (Java, Deactivate, "deactivate");
   pragma Import (Java, GetTarget, "getTarget");
   pragma Import (Java, Orb, "orb");
   pragma Import (Java, SetTarget, "setTarget");
   pragma Import (Java, ThisObject, "thisObject");

end Javax.Management.Remote.Rmi.U_RMIServerImpl_Tie;
pragma Import (Java, Javax.Management.Remote.Rmi.U_RMIServerImpl_Tie, "javax.management.remote.rmi._RMIServerImpl_Tie");
pragma Extensions_Allowed (Off);
