pragma Extensions_Allowed (On);
package Javax.Management.Remote.Rmi is
   pragma Preelaborate;
end Javax.Management.Remote.Rmi;
pragma Import (Java, Javax.Management.Remote.Rmi, "javax.management.remote.rmi");
pragma Extensions_Allowed (Off);
