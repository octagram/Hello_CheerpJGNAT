pragma Extensions_Allowed (On);
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Javax.Management.Notification;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.Remote.TargetedNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TargetedNotification (P1_Notification : access Standard.Javax.Management.Notification.Typ'Class;
                                      P2_Integer : access Standard.Java.Lang.Integer.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNotification (This : access Typ)
                             return access Javax.Management.Notification.Typ'Class;

   function GetListenerID (This : access Typ)
                           return access Java.Lang.Integer.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TargetedNotification);
   pragma Import (Java, GetNotification, "getNotification");
   pragma Import (Java, GetListenerID, "getListenerID");
   pragma Import (Java, ToString, "toString");

end Javax.Management.Remote.TargetedNotification;
pragma Import (Java, Javax.Management.Remote.TargetedNotification, "javax.management.remote.TargetedNotification");
pragma Extensions_Allowed (Off);
