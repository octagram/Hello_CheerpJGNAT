pragma Extensions_Allowed (On);
package Javax.Management.Remote is
   pragma Preelaborate;
end Javax.Management.Remote;
pragma Import (Java, Javax.Management.Remote, "javax.management.remote");
pragma Extensions_Allowed (Off);
