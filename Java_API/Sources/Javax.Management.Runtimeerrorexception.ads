pragma Extensions_Allowed (On);
limited with Java.Lang.Error;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.JMRuntimeException;

package Javax.Management.RuntimeErrorException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.JMRuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RuntimeErrorException (P1_Error : access Standard.Java.Lang.Error.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_RuntimeErrorException (P1_Error : access Standard.Java.Lang.Error.Typ'Class;
                                       P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTargetError (This : access Typ)
                            return access Java.Lang.Error.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.management.RuntimeErrorException");
   pragma Java_Constructor (New_RuntimeErrorException);
   pragma Import (Java, GetTargetError, "getTargetError");
   pragma Import (Java, GetCause, "getCause");

end Javax.Management.RuntimeErrorException;
pragma Import (Java, Javax.Management.RuntimeErrorException, "javax.management.RuntimeErrorException");
pragma Extensions_Allowed (Off);
