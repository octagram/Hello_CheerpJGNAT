pragma Extensions_Allowed (On);
limited with Java.Lang.RuntimeException;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.JMRuntimeException;

package Javax.Management.RuntimeOperationsException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.JMRuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RuntimeOperationsException (P1_RuntimeException : access Standard.Java.Lang.RuntimeException.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_RuntimeOperationsException (P1_RuntimeException : access Standard.Java.Lang.RuntimeException.Typ'Class;
                                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTargetException (This : access Typ)
                                return access Java.Lang.RuntimeException.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.management.RuntimeOperationsException");
   pragma Java_Constructor (New_RuntimeOperationsException);
   pragma Import (Java, GetTargetException, "getTargetException");
   pragma Import (Java, GetCause, "getCause");

end Javax.Management.RuntimeOperationsException;
pragma Import (Java, Javax.Management.RuntimeOperationsException, "javax.management.RuntimeOperationsException");
pragma Extensions_Allowed (Off);
