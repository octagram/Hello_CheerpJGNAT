pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.Notification;
limited with Javax.Management.NotificationFilter;
limited with Javax.Management.NotificationListener;
with Java.Lang.Object;
with Javax.Management.DynamicMBean;
with Javax.Management.MBeanRegistration;
with Javax.Management.NotificationEmitter;
with Javax.Management.StandardMBean;

package Javax.Management.StandardEmitterMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DynamicMBean_I : Javax.Management.DynamicMBean.Ref;
            MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref)
    is new Javax.Management.StandardMBean.Typ(DynamicMBean_I,
                                              MBeanRegistration_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StandardEmitterMBean (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                      P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                      P3_NotificationEmitter : access Standard.Javax.Management.NotificationEmitter.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_StandardEmitterMBean (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                      P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                                      P3_Boolean : Java.Boolean;
                                      P4_NotificationEmitter : access Standard.Javax.Management.NotificationEmitter.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   --  protected
   function New_StandardEmitterMBean (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                      P2_NotificationEmitter : access Standard.Javax.Management.NotificationEmitter.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   --  protected
   function New_StandardEmitterMBean (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                      P2_Boolean : Java.Boolean;
                                      P3_NotificationEmitter : access Standard.Javax.Management.NotificationEmitter.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure RemoveNotificationListener (This : access Typ;
                                         P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                         P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                         P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.ListenerNotFoundException.Except

   procedure AddNotificationListener (This : access Typ;
                                      P1_NotificationListener : access Standard.Javax.Management.NotificationListener.Typ'Class;
                                      P2_NotificationFilter : access Standard.Javax.Management.NotificationFilter.Typ'Class;
                                      P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   procedure SendNotification (This : access Typ;
                               P1_Notification : access Standard.Javax.Management.Notification.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StandardEmitterMBean);
   pragma Import (Java, RemoveNotificationListener, "removeNotificationListener");
   pragma Import (Java, AddNotificationListener, "addNotificationListener");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");
   pragma Import (Java, SendNotification, "sendNotification");

end Javax.Management.StandardEmitterMBean;
pragma Import (Java, Javax.Management.StandardEmitterMBean, "javax.management.StandardEmitterMBean");
pragma Extensions_Allowed (Off);
