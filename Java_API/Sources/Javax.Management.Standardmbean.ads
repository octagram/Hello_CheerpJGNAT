pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Management.Attribute;
limited with Javax.Management.AttributeList;
limited with Javax.Management.MBeanAttributeInfo;
limited with Javax.Management.MBeanConstructorInfo;
limited with Javax.Management.MBeanFeatureInfo;
limited with Javax.Management.MBeanInfo;
limited with Javax.Management.MBeanOperationInfo;
limited with Javax.Management.MBeanParameterInfo;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.DynamicMBean;
with Javax.Management.MBeanRegistration;

package Javax.Management.StandardMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DynamicMBean_I : Javax.Management.DynamicMBean.Ref;
            MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StandardMBean (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Javax.Management.NotCompliantMBeanException.Except

   --  protected
   function New_StandardMBean (P1_Class : access Standard.Java.Lang.Class.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Javax.Management.NotCompliantMBeanException.Except

   function New_StandardMBean (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                               P3_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   --  protected
   function New_StandardMBean (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                               P2_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetImplementation (This : access Typ;
                                P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Management.NotCompliantMBeanException.Except

   function GetImplementation (This : access Typ)
                               return access Java.Lang.Object.Typ'Class;

   --  final
   function GetMBeanInterface (This : access Typ)
                               return access Java.Lang.Class.Typ'Class;

   function GetImplementationClass (This : access Typ)
                                    return access Java.Lang.Class.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   procedure SetAttribute (This : access Typ;
                           P1_Attribute : access Standard.Javax.Management.Attribute.Typ'Class);
   --  can raise Javax.Management.AttributeNotFoundException.Except,
   --  Javax.Management.InvalidAttributeValueException.Except,
   --  Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetAttributes (This : access Typ;
                           P1_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Management.AttributeList.Typ'Class;

   function SetAttributes (This : access Typ;
                           P1_AttributeList : access Standard.Javax.Management.AttributeList.Typ'Class)
                           return access Javax.Management.AttributeList.Typ'Class;

   function Invoke (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P3_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Management.MBeanException.Except and
   --  Javax.Management.ReflectionException.Except

   function GetMBeanInfo (This : access Typ)
                          return access Javax.Management.MBeanInfo.Typ'Class;

   --  protected
   function GetClassName (This : access Typ;
                          P1_MBeanInfo : access Standard.Javax.Management.MBeanInfo.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function GetDescription (This : access Typ;
                            P1_MBeanInfo : access Standard.Javax.Management.MBeanInfo.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function GetDescription (This : access Typ;
                            P1_MBeanFeatureInfo : access Standard.Javax.Management.MBeanFeatureInfo.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function GetDescription (This : access Typ;
                            P1_MBeanAttributeInfo : access Standard.Javax.Management.MBeanAttributeInfo.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function GetDescription (This : access Typ;
                            P1_MBeanConstructorInfo : access Standard.Javax.Management.MBeanConstructorInfo.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function GetDescription (This : access Typ;
                            P1_MBeanConstructorInfo : access Standard.Javax.Management.MBeanConstructorInfo.Typ'Class;
                            P2_MBeanParameterInfo : access Standard.Javax.Management.MBeanParameterInfo.Typ'Class;
                            P3_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function GetParameterName (This : access Typ;
                              P1_MBeanConstructorInfo : access Standard.Javax.Management.MBeanConstructorInfo.Typ'Class;
                              P2_MBeanParameterInfo : access Standard.Javax.Management.MBeanParameterInfo.Typ'Class;
                              P3_Int : Java.Int)
                              return access Java.Lang.String.Typ'Class;

   --  protected
   function GetDescription (This : access Typ;
                            P1_MBeanOperationInfo : access Standard.Javax.Management.MBeanOperationInfo.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function GetImpact (This : access Typ;
                       P1_MBeanOperationInfo : access Standard.Javax.Management.MBeanOperationInfo.Typ'Class)
                       return Java.Int;

   --  protected
   function GetParameterName (This : access Typ;
                              P1_MBeanOperationInfo : access Standard.Javax.Management.MBeanOperationInfo.Typ'Class;
                              P2_MBeanParameterInfo : access Standard.Javax.Management.MBeanParameterInfo.Typ'Class;
                              P3_Int : Java.Int)
                              return access Java.Lang.String.Typ'Class;

   --  protected
   function GetDescription (This : access Typ;
                            P1_MBeanOperationInfo : access Standard.Javax.Management.MBeanOperationInfo.Typ'Class;
                            P2_MBeanParameterInfo : access Standard.Javax.Management.MBeanParameterInfo.Typ'Class;
                            P3_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function GetConstructors (This : access Typ;
                             P1_MBeanConstructorInfo_Arr : access Javax.Management.MBeanConstructorInfo.Arr_Obj;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetCachedMBeanInfo (This : access Typ)
                                return access Javax.Management.MBeanInfo.Typ'Class;

   --  protected
   procedure CacheMBeanInfo (This : access Typ;
                             P1_MBeanInfo : access Standard.Javax.Management.MBeanInfo.Typ'Class);

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StandardMBean);
   pragma Import (Java, SetImplementation, "setImplementation");
   pragma Import (Java, GetImplementation, "getImplementation");
   pragma Import (Java, GetMBeanInterface, "getMBeanInterface");
   pragma Import (Java, GetImplementationClass, "getImplementationClass");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, SetAttribute, "setAttribute");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, SetAttributes, "setAttributes");
   pragma Import (Java, Invoke, "invoke");
   pragma Import (Java, GetMBeanInfo, "getMBeanInfo");
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetParameterName, "getParameterName");
   pragma Import (Java, GetImpact, "getImpact");
   pragma Import (Java, GetConstructors, "getConstructors");
   pragma Import (Java, GetCachedMBeanInfo, "getCachedMBeanInfo");
   pragma Import (Java, CacheMBeanInfo, "cacheMBeanInfo");
   pragma Import (Java, PreRegister, "preRegister");
   pragma Import (Java, PostRegister, "postRegister");
   pragma Import (Java, PreDeregister, "preDeregister");
   pragma Import (Java, PostDeregister, "postDeregister");

end Javax.Management.StandardMBean;
pragma Import (Java, Javax.Management.StandardMBean, "javax.management.StandardMBean");
pragma Extensions_Allowed (Off);
