pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.ValueExp;

package Javax.Management.StringValueExp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ValueExp_I : Javax.Management.ValueExp.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringValueExp (This : Ref := null)
                                return Ref;

   function New_StringValueExp (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetMBeanServer (This : access Typ;
                             P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class);

   function Apply (This : access Typ;
                   P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                   return access Javax.Management.ValueExp.Typ'Class;
   --  can raise Javax.Management.BadStringOperationException.Except,
   --  Javax.Management.BadBinaryOpValueExpException.Except,
   --  Javax.Management.BadAttributeValueExpException.Except and
   --  Javax.Management.InvalidApplicationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringValueExp);
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SetMBeanServer, "setMBeanServer");
   pragma Import (Java, Apply, "apply");

end Javax.Management.StringValueExp;
pragma Import (Java, Javax.Management.StringValueExp, "javax.management.StringValueExp");
pragma Extensions_Allowed (Off);
