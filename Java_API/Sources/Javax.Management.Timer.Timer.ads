pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Integer;
limited with Java.Lang.Long;
limited with Java.Lang.String;
limited with Java.Util.Date;
limited with Java.Util.Vector;
limited with Javax.Management.MBeanNotificationInfo;
limited with Javax.Management.MBeanServer;
limited with Javax.Management.ObjectName;
with Java.Lang.Object;
with Javax.Management.MBeanRegistration;
with Javax.Management.NotificationBroadcasterSupport;
with Javax.Management.NotificationEmitter;
with Javax.Management.Timer.TimerMBean;

package Javax.Management.Timer.Timer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MBeanRegistration_I : Javax.Management.MBeanRegistration.Ref;
            NotificationEmitter_I : Javax.Management.NotificationEmitter.Ref;
            TimerMBean_I : Javax.Management.Timer.TimerMBean.Ref)
    is new Javax.Management.NotificationBroadcasterSupport.Typ(NotificationEmitter_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Timer (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function PreRegister (This : access Typ;
                         P1_MBeanServer : access Standard.Javax.Management.MBeanServer.Typ'Class;
                         P2_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                         return access Javax.Management.ObjectName.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   procedure PostRegister (This : access Typ;
                           P1_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure PreDeregister (This : access Typ);
   --  can raise Java.Lang.Exception_K.Except

   procedure PostDeregister (This : access Typ);

   --  synchronized
   function GetNotificationInfo (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   --  synchronized
   procedure Start (This : access Typ);

   --  synchronized
   procedure Stop (This : access Typ);

   --  synchronized
   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class;
                             P5_Long : Java.Long;
                             P6_Long : Java.Long;
                             P7_Boolean : Java.Boolean)
                             return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class;
                             P5_Long : Java.Long;
                             P6_Long : Java.Long)
                             return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class;
                             P5_Long : Java.Long)
                             return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class)
                             return access Java.Lang.Integer.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   --  synchronized
   procedure RemoveNotification (This : access Typ;
                                 P1_Integer : access Standard.Java.Lang.Integer.Typ'Class);
   --  can raise Javax.Management.InstanceNotFoundException.Except

   --  synchronized
   procedure RemoveNotifications (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Management.InstanceNotFoundException.Except

   --  synchronized
   procedure RemoveAllNotifications (This : access Typ);

   function GetNbNotifications (This : access Typ)
                                return Java.Int;

   --  synchronized
   function GetAllNotificationIDs (This : access Typ)
                                   return access Java.Util.Vector.Typ'Class;

   --  synchronized
   function GetNotificationIDs (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Util.Vector.Typ'Class;

   function GetNotificationType (This : access Typ;
                                 P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                                 return access Java.Lang.String.Typ'Class;

   function GetNotificationMessage (This : access Typ;
                                    P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                                    return access Java.Lang.String.Typ'Class;

   function GetNotificationUserData (This : access Typ;
                                     P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                                     return access Java.Lang.Object.Typ'Class;

   function GetDate (This : access Typ;
                     P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                     return access Java.Util.Date.Typ'Class;

   function GetPeriod (This : access Typ;
                       P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                       return access Java.Lang.Long.Typ'Class;

   function GetNbOccurences (This : access Typ;
                             P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                             return access Java.Lang.Long.Typ'Class;

   function GetFixedRate (This : access Typ;
                          P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                          return access Java.Lang.Boolean.Typ'Class;

   function GetSendPastNotifications (This : access Typ)
                                      return Java.Boolean;

   procedure SetSendPastNotifications (This : access Typ;
                                       P1_Boolean : Java.Boolean);

   function IsActive (This : access Typ)
                      return Java.Boolean;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ONE_SECOND : constant Java.Long;

   --  final
   ONE_MINUTE : constant Java.Long;

   --  final
   ONE_HOUR : constant Java.Long;

   --  final
   ONE_DAY : constant Java.Long;

   --  final
   ONE_WEEK : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Timer);
   pragma Import (Java, PreRegister, "preRegister");
   pragma Import (Java, PostRegister, "postRegister");
   pragma Import (Java, PreDeregister, "preDeregister");
   pragma Import (Java, PostDeregister, "postDeregister");
   pragma Import (Java, GetNotificationInfo, "getNotificationInfo");
   pragma Import (Java, Start, "start");
   pragma Import (Java, Stop, "stop");
   pragma Import (Java, AddNotification, "addNotification");
   pragma Import (Java, RemoveNotification, "removeNotification");
   pragma Import (Java, RemoveNotifications, "removeNotifications");
   pragma Import (Java, RemoveAllNotifications, "removeAllNotifications");
   pragma Import (Java, GetNbNotifications, "getNbNotifications");
   pragma Import (Java, GetAllNotificationIDs, "getAllNotificationIDs");
   pragma Import (Java, GetNotificationIDs, "getNotificationIDs");
   pragma Import (Java, GetNotificationType, "getNotificationType");
   pragma Import (Java, GetNotificationMessage, "getNotificationMessage");
   pragma Import (Java, GetNotificationUserData, "getNotificationUserData");
   pragma Import (Java, GetDate, "getDate");
   pragma Import (Java, GetPeriod, "getPeriod");
   pragma Import (Java, GetNbOccurences, "getNbOccurences");
   pragma Import (Java, GetFixedRate, "getFixedRate");
   pragma Import (Java, GetSendPastNotifications, "getSendPastNotifications");
   pragma Import (Java, SetSendPastNotifications, "setSendPastNotifications");
   pragma Import (Java, IsActive, "isActive");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, ONE_SECOND, "ONE_SECOND");
   pragma Import (Java, ONE_MINUTE, "ONE_MINUTE");
   pragma Import (Java, ONE_HOUR, "ONE_HOUR");
   pragma Import (Java, ONE_DAY, "ONE_DAY");
   pragma Import (Java, ONE_WEEK, "ONE_WEEK");

end Javax.Management.Timer.Timer;
pragma Import (Java, Javax.Management.Timer.Timer, "javax.management.timer.Timer");
pragma Extensions_Allowed (Off);
