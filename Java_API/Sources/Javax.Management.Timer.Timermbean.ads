pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Integer;
limited with Java.Lang.Long;
limited with Java.Lang.String;
limited with Java.Util.Date;
limited with Java.Util.Vector;
with Java.Lang.Object;

package Javax.Management.Timer.TimerMBean is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Start (This : access Typ) is abstract;

   procedure Stop (This : access Typ) is abstract;

   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class;
                             P5_Long : Java.Long;
                             P6_Long : Java.Long;
                             P7_Boolean : Java.Boolean)
                             return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class;
                             P5_Long : Java.Long;
                             P6_Long : Java.Long)
                             return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class;
                             P5_Long : Java.Long)
                             return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function AddNotification (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P4_Date : access Standard.Java.Util.Date.Typ'Class)
                             return access Java.Lang.Integer.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure RemoveNotification (This : access Typ;
                                 P1_Integer : access Standard.Java.Lang.Integer.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except

   procedure RemoveNotifications (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Management.InstanceNotFoundException.Except

   procedure RemoveAllNotifications (This : access Typ) is abstract;

   function GetNbNotifications (This : access Typ)
                                return Java.Int is abstract;

   function GetAllNotificationIDs (This : access Typ)
                                   return access Java.Util.Vector.Typ'Class is abstract;

   function GetNotificationIDs (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Util.Vector.Typ'Class is abstract;

   function GetNotificationType (This : access Typ;
                                 P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                                 return access Java.Lang.String.Typ'Class is abstract;

   function GetNotificationMessage (This : access Typ;
                                    P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                                    return access Java.Lang.String.Typ'Class is abstract;

   function GetNotificationUserData (This : access Typ;
                                     P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                                     return access Java.Lang.Object.Typ'Class is abstract;

   function GetDate (This : access Typ;
                     P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                     return access Java.Util.Date.Typ'Class is abstract;

   function GetPeriod (This : access Typ;
                       P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                       return access Java.Lang.Long.Typ'Class is abstract;

   function GetNbOccurences (This : access Typ;
                             P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                             return access Java.Lang.Long.Typ'Class is abstract;

   function GetFixedRate (This : access Typ;
                          P1_Integer : access Standard.Java.Lang.Integer.Typ'Class)
                          return access Java.Lang.Boolean.Typ'Class is abstract;

   function GetSendPastNotifications (This : access Typ)
                                      return Java.Boolean is abstract;

   procedure SetSendPastNotifications (This : access Typ;
                                       P1_Boolean : Java.Boolean) is abstract;

   function IsActive (This : access Typ)
                      return Java.Boolean is abstract;

   function IsEmpty (This : access Typ)
                     return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Start, "start");
   pragma Export (Java, Stop, "stop");
   pragma Export (Java, AddNotification, "addNotification");
   pragma Export (Java, RemoveNotification, "removeNotification");
   pragma Export (Java, RemoveNotifications, "removeNotifications");
   pragma Export (Java, RemoveAllNotifications, "removeAllNotifications");
   pragma Export (Java, GetNbNotifications, "getNbNotifications");
   pragma Export (Java, GetAllNotificationIDs, "getAllNotificationIDs");
   pragma Export (Java, GetNotificationIDs, "getNotificationIDs");
   pragma Export (Java, GetNotificationType, "getNotificationType");
   pragma Export (Java, GetNotificationMessage, "getNotificationMessage");
   pragma Export (Java, GetNotificationUserData, "getNotificationUserData");
   pragma Export (Java, GetDate, "getDate");
   pragma Export (Java, GetPeriod, "getPeriod");
   pragma Export (Java, GetNbOccurences, "getNbOccurences");
   pragma Export (Java, GetFixedRate, "getFixedRate");
   pragma Export (Java, GetSendPastNotifications, "getSendPastNotifications");
   pragma Export (Java, SetSendPastNotifications, "setSendPastNotifications");
   pragma Export (Java, IsActive, "isActive");
   pragma Export (Java, IsEmpty, "isEmpty");

end Javax.Management.Timer.TimerMBean;
pragma Import (Java, Javax.Management.Timer.TimerMBean, "javax.management.timer.TimerMBean");
pragma Extensions_Allowed (Off);
