pragma Extensions_Allowed (On);
limited with Java.Lang.Integer;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Notification;

package Javax.Management.Timer.TimerNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Management.Notification.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TimerNotification (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                   P3_Long : Java.Long;
                                   P4_Long : Java.Long;
                                   P5_String : access Standard.Java.Lang.String.Typ'Class;
                                   P6_Integer : access Standard.Java.Lang.Integer.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNotificationID (This : access Typ)
                               return access Java.Lang.Integer.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TimerNotification);
   pragma Import (Java, GetNotificationID, "getNotificationID");

end Javax.Management.Timer.TimerNotification;
pragma Import (Java, Javax.Management.Timer.TimerNotification, "javax.management.timer.TimerNotification");
pragma Extensions_Allowed (Off);
