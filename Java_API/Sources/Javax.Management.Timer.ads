pragma Extensions_Allowed (On);
package Javax.Management.Timer is
   pragma Preelaborate;
end Javax.Management.Timer;
pragma Import (Java, Javax.Management.Timer, "javax.management.timer");
pragma Extensions_Allowed (Off);
