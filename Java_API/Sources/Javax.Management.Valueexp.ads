pragma Extensions_Allowed (On);
limited with Javax.Management.ObjectName;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Management.ValueExp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Apply (This : access Typ;
                   P1_ObjectName : access Standard.Javax.Management.ObjectName.Typ'Class)
                   return access Javax.Management.ValueExp.Typ'Class is abstract;
   --  can raise Javax.Management.BadStringOperationException.Except,
   --  Javax.Management.BadBinaryOpValueExpException.Except,
   --  Javax.Management.BadAttributeValueExpException.Except and
   --  Javax.Management.InvalidApplicationException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Apply, "apply");

end Javax.Management.ValueExp;
pragma Import (Java, Javax.Management.ValueExp, "javax.management.ValueExp");
pragma Extensions_Allowed (Off);
