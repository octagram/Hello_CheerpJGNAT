pragma Extensions_Allowed (On);
package Javax.Management is
   pragma Preelaborate;
end Javax.Management;
pragma Import (Java, Javax.Management, "javax.management");
pragma Extensions_Allowed (Off);
