pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.RefAddr;

package Javax.Naming.BinaryRefAddr is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.RefAddr.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BinaryRefAddr (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Byte_Arr : Java.Byte_Arr; 
                               This : Ref := null)
                               return Ref;

   function New_BinaryRefAddr (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Byte_Arr : Java.Byte_Arr;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContent (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BinaryRefAddr);
   pragma Import (Java, GetContent, "getContent");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.BinaryRefAddr;
pragma Import (Java, Javax.Naming.BinaryRefAddr, "javax.naming.BinaryRefAddr");
pragma Extensions_Allowed (Off);
