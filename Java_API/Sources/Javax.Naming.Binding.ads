pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.NameClassPair;

package Javax.Naming.Binding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.NameClassPair.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Binding (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_Binding (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P3_Boolean : Java.Boolean; 
                         This : Ref := null)
                         return Ref;

   function New_Binding (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_Object : access Standard.Java.Lang.Object.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_Binding (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P4_Boolean : Java.Boolean; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetObject (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   procedure SetObject (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Binding);
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetObject, "getObject");
   pragma Import (Java, SetObject, "setObject");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.Binding;
pragma Import (Java, Javax.Naming.Binding, "javax.naming.Binding");
pragma Extensions_Allowed (Off);
