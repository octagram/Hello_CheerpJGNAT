pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Naming.Context;
limited with Javax.Naming.Name;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.NamingException;

package Javax.Naming.CannotProceedException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.NamingException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      RemainingNewName : access Javax.Naming.Name.Typ'Class;
      pragma Import (Java, RemainingNewName, "remainingNewName");

      --  protected
      Environment : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, Environment, "environment");

      --  protected
      AltName : access Javax.Naming.Name.Typ'Class;
      pragma Import (Java, AltName, "altName");

      --  protected
      AltNameCtx : access Javax.Naming.Context.Typ'Class;
      pragma Import (Java, AltNameCtx, "altNameCtx");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CannotProceedException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   function New_CannotProceedException (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEnvironment (This : access Typ)
                            return access Java.Util.Hashtable.Typ'Class;

   procedure SetEnvironment (This : access Typ;
                             P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class);

   function GetRemainingNewName (This : access Typ)
                                 return access Javax.Naming.Name.Typ'Class;

   procedure SetRemainingNewName (This : access Typ;
                                  P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   function GetAltName (This : access Typ)
                        return access Javax.Naming.Name.Typ'Class;

   procedure SetAltName (This : access Typ;
                         P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   function GetAltNameCtx (This : access Typ)
                           return access Javax.Naming.Context.Typ'Class;

   procedure SetAltNameCtx (This : access Typ;
                            P1_Context : access Standard.Javax.Naming.Context.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.naming.CannotProceedException");
   pragma Java_Constructor (New_CannotProceedException);
   pragma Import (Java, GetEnvironment, "getEnvironment");
   pragma Import (Java, SetEnvironment, "setEnvironment");
   pragma Import (Java, GetRemainingNewName, "getRemainingNewName");
   pragma Import (Java, SetRemainingNewName, "setRemainingNewName");
   pragma Import (Java, GetAltName, "getAltName");
   pragma Import (Java, SetAltName, "setAltName");
   pragma Import (Java, GetAltNameCtx, "getAltNameCtx");
   pragma Import (Java, SetAltNameCtx, "setAltNameCtx");

end Javax.Naming.CannotProceedException;
pragma Import (Java, Javax.Naming.CannotProceedException, "javax.naming.CannotProceedException");
pragma Extensions_Allowed (Off);
