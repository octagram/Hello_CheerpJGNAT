pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Lang.Object;
with Javax.Naming.Name;

package Javax.Naming.CompositeName is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Name_I : Javax.Naming.Name.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_CompositeName (P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_CompositeName (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Javax.Naming.InvalidNameException.Except

   function New_CompositeName (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function GetAll (This : access Typ)
                    return access Java.Util.Enumeration.Typ'Class;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.String.Typ'Class;

   function GetPrefix (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Naming.Name.Typ'Class;

   function GetSuffix (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Naming.Name.Typ'Class;

   function StartsWith (This : access Typ;
                        P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                        return Java.Boolean;

   function EndsWith (This : access Typ;
                      P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                      return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                    return access Javax.Naming.Name.Typ'Class;
   --  can raise Javax.Naming.InvalidNameException.Except

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Name : access Standard.Javax.Naming.Name.Typ'Class)
                    return access Javax.Naming.Name.Typ'Class;
   --  can raise Javax.Naming.InvalidNameException.Except

   function Add (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Naming.Name.Typ'Class;
   --  can raise Javax.Naming.InvalidNameException.Except

   function Add (This : access Typ;
                 P1_Int : Java.Int;
                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Naming.Name.Typ'Class;
   --  can raise Javax.Naming.InvalidNameException.Except

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.InvalidNameException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompositeName);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, GetAll, "getAll");
   pragma Import (Java, Get, "get");
   pragma Import (Java, GetPrefix, "getPrefix");
   pragma Import (Java, GetSuffix, "getSuffix");
   pragma Import (Java, StartsWith, "startsWith");
   pragma Import (Java, EndsWith, "endsWith");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");

end Javax.Naming.CompositeName;
pragma Import (Java, Javax.Naming.CompositeName, "javax.naming.CompositeName");
pragma Extensions_Allowed (Off);
