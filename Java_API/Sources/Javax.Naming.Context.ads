pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Naming.Name;
limited with Javax.Naming.NameParser;
limited with Javax.Naming.NamingEnumeration;
with Java.Lang.Object;

package Javax.Naming.Context is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Lookup (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Lookup (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Unbind (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Unbind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Rename (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                     P2_Name : access Standard.Javax.Naming.Name.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Rename (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function List (This : access Typ;
                  P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                  return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function List (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function ListBindings (This : access Typ;
                          P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                          return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function ListBindings (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure DestroySubcontext (This : access Typ;
                                P1_Name : access Standard.Javax.Naming.Name.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure DestroySubcontext (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                              return access Javax.Naming.Context.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Javax.Naming.Context.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function LookupLink (This : access Typ;
                        P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function LookupLink (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetNameParser (This : access Typ;
                           P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                           return access Javax.Naming.NameParser.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetNameParser (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Naming.NameParser.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function ComposeName (This : access Typ;
                         P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                         P2_Name : access Standard.Javax.Naming.Name.Typ'Class)
                         return access Javax.Naming.Name.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function ComposeName (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function AddToEnvironment (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                              return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function RemoveFromEnvironment (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetEnvironment (This : access Typ)
                            return access Java.Util.Hashtable.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetNameInNamespace (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INITIAL_CONTEXT_FACTORY : constant access Java.Lang.String.Typ'Class;

   --  final
   OBJECT_FACTORIES : constant access Java.Lang.String.Typ'Class;

   --  final
   STATE_FACTORIES : constant access Java.Lang.String.Typ'Class;

   --  final
   URL_PKG_PREFIXES : constant access Java.Lang.String.Typ'Class;

   --  final
   PROVIDER_URL : constant access Java.Lang.String.Typ'Class;

   --  final
   DNS_URL : constant access Java.Lang.String.Typ'Class;

   --  final
   AUTHORITATIVE : constant access Java.Lang.String.Typ'Class;

   --  final
   BATCHSIZE : constant access Java.Lang.String.Typ'Class;

   --  final
   REFERRAL : constant access Java.Lang.String.Typ'Class;

   --  final
   SECURITY_PROTOCOL : constant access Java.Lang.String.Typ'Class;

   --  final
   SECURITY_AUTHENTICATION : constant access Java.Lang.String.Typ'Class;

   --  final
   SECURITY_PRINCIPAL : constant access Java.Lang.String.Typ'Class;

   --  final
   SECURITY_CREDENTIALS : constant access Java.Lang.String.Typ'Class;

   --  final
   LANGUAGE : constant access Java.Lang.String.Typ'Class;

   --  final
   APPLET : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Lookup, "lookup");
   pragma Export (Java, Bind, "bind");
   pragma Export (Java, Rebind, "rebind");
   pragma Export (Java, Unbind, "unbind");
   pragma Export (Java, Rename, "rename");
   pragma Export (Java, List, "list");
   pragma Export (Java, ListBindings, "listBindings");
   pragma Export (Java, DestroySubcontext, "destroySubcontext");
   pragma Export (Java, CreateSubcontext, "createSubcontext");
   pragma Export (Java, LookupLink, "lookupLink");
   pragma Export (Java, GetNameParser, "getNameParser");
   pragma Export (Java, ComposeName, "composeName");
   pragma Export (Java, AddToEnvironment, "addToEnvironment");
   pragma Export (Java, RemoveFromEnvironment, "removeFromEnvironment");
   pragma Export (Java, GetEnvironment, "getEnvironment");
   pragma Export (Java, Close, "close");
   pragma Export (Java, GetNameInNamespace, "getNameInNamespace");
   pragma Import (Java, INITIAL_CONTEXT_FACTORY, "INITIAL_CONTEXT_FACTORY");
   pragma Import (Java, OBJECT_FACTORIES, "OBJECT_FACTORIES");
   pragma Import (Java, STATE_FACTORIES, "STATE_FACTORIES");
   pragma Import (Java, URL_PKG_PREFIXES, "URL_PKG_PREFIXES");
   pragma Import (Java, PROVIDER_URL, "PROVIDER_URL");
   pragma Import (Java, DNS_URL, "DNS_URL");
   pragma Import (Java, AUTHORITATIVE, "AUTHORITATIVE");
   pragma Import (Java, BATCHSIZE, "BATCHSIZE");
   pragma Import (Java, REFERRAL, "REFERRAL");
   pragma Import (Java, SECURITY_PROTOCOL, "SECURITY_PROTOCOL");
   pragma Import (Java, SECURITY_AUTHENTICATION, "SECURITY_AUTHENTICATION");
   pragma Import (Java, SECURITY_PRINCIPAL, "SECURITY_PRINCIPAL");
   pragma Import (Java, SECURITY_CREDENTIALS, "SECURITY_CREDENTIALS");
   pragma Import (Java, LANGUAGE, "LANGUAGE");
   pragma Import (Java, APPLET, "APPLET");

end Javax.Naming.Context;
pragma Import (Java, Javax.Naming.Context, "javax.naming.Context");
pragma Extensions_Allowed (Off);
