pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.DirContext;
limited with Javax.Naming.NamingEnumeration;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Naming.Directory.Attribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAll (This : access Typ)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean is abstract;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean is abstract;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   procedure Clear (This : access Typ) is abstract;

   function GetAttributeSyntaxDefinition (This : access Typ)
                                          return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributeDefinition (This : access Typ)
                                    return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function IsOrdered (This : access Typ)
                       return Java.Boolean is abstract;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class is abstract;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAll, "getAll");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Size, "size");
   pragma Export (Java, GetID, "getID");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, GetAttributeSyntaxDefinition, "getAttributeSyntaxDefinition");
   pragma Export (Java, GetAttributeDefinition, "getAttributeDefinition");
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, IsOrdered, "isOrdered");
   pragma Export (Java, Set, "set");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Javax.Naming.Directory.Attribute;
pragma Import (Java, Javax.Naming.Directory.Attribute, "javax.naming.directory.Attribute");
pragma Extensions_Allowed (Off);
