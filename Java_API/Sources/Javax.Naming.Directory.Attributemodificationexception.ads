pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.ModificationItem;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.NamingException;

package Javax.Naming.Directory.AttributeModificationException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.NamingException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributeModificationException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                                This : Ref := null)
                                                return Ref;

   function New_AttributeModificationException (This : Ref := null)
                                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetUnexecutedModifications (This : access Typ;
                                         P1_ModificationItem_Arr : access Javax.Naming.Directory.ModificationItem.Arr_Obj);

   function GetUnexecutedModifications (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.naming.directory.AttributeModificationException");
   pragma Java_Constructor (New_AttributeModificationException);
   pragma Import (Java, SetUnexecutedModifications, "setUnexecutedModifications");
   pragma Import (Java, GetUnexecutedModifications, "getUnexecutedModifications");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.Directory.AttributeModificationException;
pragma Import (Java, Javax.Naming.Directory.AttributeModificationException, "javax.naming.directory.AttributeModificationException");
pragma Extensions_Allowed (Off);
