pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.Attribute;
limited with Javax.Naming.NamingEnumeration;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Naming.Directory.Attributes is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsCaseIgnored (This : access Typ)
                           return Java.Boolean is abstract;

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Naming.Directory.Attribute.Typ'Class is abstract;

   function GetAll (This : access Typ)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;

   function GetIDs (This : access Typ)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;

   function Put (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Javax.Naming.Directory.Attribute.Typ'Class is abstract;

   function Put (This : access Typ;
                 P1_Attribute : access Standard.Javax.Naming.Directory.Attribute.Typ'Class)
                 return access Javax.Naming.Directory.Attribute.Typ'Class is abstract;

   function Remove (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Javax.Naming.Directory.Attribute.Typ'Class is abstract;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsCaseIgnored, "isCaseIgnored");
   pragma Export (Java, Size, "size");
   pragma Export (Java, Get, "get");
   pragma Export (Java, GetAll, "getAll");
   pragma Export (Java, GetIDs, "getIDs");
   pragma Export (Java, Put, "put");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Clone, "clone");

end Javax.Naming.Directory.Attributes;
pragma Import (Java, Javax.Naming.Directory.Attributes, "javax.naming.directory.Attributes");
pragma Extensions_Allowed (Off);
