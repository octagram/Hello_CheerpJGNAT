pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Naming.Directory.DirContext;
limited with Javax.Naming.NamingEnumeration;
with Java.Lang.Object;
with Javax.Naming.Directory.Attribute;

package Javax.Naming.Directory.BasicAttribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Attribute_I : Javax.Naming.Directory.Attribute.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      AttrID : access Java.Lang.String.Typ'Class;
      pragma Import (Java, AttrID, "attrID");

      --  protected
      Values : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Values, "values");

      --  protected
      Ordered : Java.Boolean;
      pragma Import (Java, Ordered, "ordered");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicAttribute (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_BasicAttribute (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_BasicAttribute (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   function New_BasicAttribute (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P3_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   function GetAll (This : access Typ)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Size (This : access Typ)
                  return Java.Int;

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   procedure Clear (This : access Typ);

   function IsOrdered (This : access Typ)
                       return Java.Boolean;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function GetAttributeSyntaxDefinition (This : access Typ)
                                          return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributeDefinition (This : access Typ)
                                    return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Java_Constructor (New_BasicAttribute);
   pragma Import (Java, GetAll, "getAll");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Size, "size");
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, IsOrdered, "isOrdered");
   pragma Import (Java, Set, "set");
   pragma Import (Java, GetAttributeSyntaxDefinition, "getAttributeSyntaxDefinition");
   pragma Import (Java, GetAttributeDefinition, "getAttributeDefinition");

end Javax.Naming.Directory.BasicAttribute;
pragma Import (Java, Javax.Naming.Directory.BasicAttribute, "javax.naming.directory.BasicAttribute");
pragma Extensions_Allowed (Off);
