pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.Attribute;
limited with Javax.Naming.NamingEnumeration;
with Java.Lang.Object;
with Javax.Naming.Directory.Attributes;

package Javax.Naming.Directory.BasicAttributes is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Attributes_I : Javax.Naming.Directory.Attributes.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicAttributes (This : Ref := null)
                                 return Ref;

   function New_BasicAttributes (P1_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   function New_BasicAttributes (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_BasicAttributes (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function IsCaseIgnored (This : access Typ)
                           return Java.Boolean;

   function Size (This : access Typ)
                  return Java.Int;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Naming.Directory.Attribute.Typ'Class;

   function GetAll (This : access Typ)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;

   function GetIDs (This : access Typ)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;

   function Put (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Javax.Naming.Directory.Attribute.Typ'Class;

   function Put (This : access Typ;
                 P1_Attribute : access Standard.Javax.Naming.Directory.Attribute.Typ'Class)
                 return access Javax.Naming.Directory.Attribute.Typ'Class;

   function Remove (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Javax.Naming.Directory.Attribute.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicAttributes);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, IsCaseIgnored, "isCaseIgnored");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Get, "get");
   pragma Import (Java, GetAll, "getAll");
   pragma Import (Java, GetIDs, "getIDs");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Naming.Directory.BasicAttributes;
pragma Import (Java, Javax.Naming.Directory.BasicAttributes, "javax.naming.directory.BasicAttributes");
pragma Extensions_Allowed (Off);
