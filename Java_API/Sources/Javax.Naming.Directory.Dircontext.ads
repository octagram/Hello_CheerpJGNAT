pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.Attributes;
limited with Javax.Naming.Directory.ModificationItem;
limited with Javax.Naming.Directory.SearchControls;
limited with Javax.Naming.Name;
limited with Javax.Naming.NamingEnumeration;
with Java.Lang.Object;
with Javax.Naming.Context;

package Javax.Naming.Directory.DirContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Context_I : Javax.Naming.Context.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributes (This : access Typ;
                           P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                           return access Javax.Naming.Directory.Attributes.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Naming.Directory.Attributes.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributes (This : access Typ;
                           P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Naming.Directory.Attributes.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Naming.Directory.Attributes.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                               P2_ModificationItem_Arr : access Javax.Naming.Directory.ModificationItem.Arr_Obj) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_ModificationItem_Arr : access Javax.Naming.Directory.ModificationItem.Arr_Obj) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                              P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                              return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                              return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchema (This : access Typ;
                       P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                       return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchema (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchemaClassDefinition (This : access Typ;
                                      P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                                      return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchemaClassDefinition (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Javax.Naming.Directory.DirContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class;
                    P3_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class;
                    P3_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P4_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P4_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ADD_ATTRIBUTE : constant Java.Int;

   --  final
   REPLACE_ATTRIBUTE : constant Java.Int;

   --  final
   REMOVE_ATTRIBUTE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, ModifyAttributes, "modifyAttributes");
   pragma Export (Java, Bind, "bind");
   pragma Export (Java, Rebind, "rebind");
   pragma Export (Java, CreateSubcontext, "createSubcontext");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, GetSchemaClassDefinition, "getSchemaClassDefinition");
   pragma Export (Java, Search, "search");
   pragma Import (Java, ADD_ATTRIBUTE, "ADD_ATTRIBUTE");
   pragma Import (Java, REPLACE_ATTRIBUTE, "REPLACE_ATTRIBUTE");
   pragma Import (Java, REMOVE_ATTRIBUTE, "REMOVE_ATTRIBUTE");

end Javax.Naming.Directory.DirContext;
pragma Import (Java, Javax.Naming.Directory.DirContext, "javax.naming.directory.DirContext");
pragma Extensions_Allowed (Off);
