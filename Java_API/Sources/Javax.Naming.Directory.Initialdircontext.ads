pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Naming.Directory.Attributes;
limited with Javax.Naming.Directory.ModificationItem;
limited with Javax.Naming.Directory.SearchControls;
limited with Javax.Naming.Name;
limited with Javax.Naming.NamingEnumeration;
with Java.Lang.Object;
with Javax.Naming.Context;
with Javax.Naming.Directory.DirContext;
with Javax.Naming.InitialContext;

package Javax.Naming.Directory.InitialDirContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Context_I : Javax.Naming.Context.Ref;
            DirContext_I : Javax.Naming.Directory.DirContext.Ref)
    is new Javax.Naming.InitialContext.Typ(Context_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_InitialDirContext (P1_Boolean : Java.Boolean; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Javax.Naming.NamingException.Except

   function New_InitialDirContext (This : Ref := null)
                                   return Ref;
   --  can raise Javax.Naming.NamingException.Except

   function New_InitialDirContext (P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
   --  can raise Javax.Naming.NamingException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Naming.Directory.Attributes.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributes (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Naming.Directory.Attributes.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributes (This : access Typ;
                           P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                           return access Javax.Naming.Directory.Attributes.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetAttributes (This : access Typ;
                           P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                           P2_String_Arr : access Java.Lang.String.Arr_Obj)
                           return access Javax.Naming.Directory.Attributes.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_ModificationItem_Arr : access Javax.Naming.Directory.ModificationItem.Arr_Obj);
   --  can raise Javax.Naming.NamingException.Except

   procedure ModifyAttributes (This : access Typ;
                               P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                               P2_ModificationItem_Arr : access Javax.Naming.Directory.ModificationItem.Arr_Obj);
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                   P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                              return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                              P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                              return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchema (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchema (This : access Typ;
                       P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                       return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchemaClassDefinition (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetSchemaClassDefinition (This : access Typ;
                                      P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                                      return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class;
                    P3_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class;
                    P3_String_Arr : access Java.Lang.String.Arr_Obj)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P4_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Search (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                    P4_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class)
                    return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InitialDirContext);
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, ModifyAttributes, "modifyAttributes");
   pragma Import (Java, Bind, "bind");
   pragma Import (Java, Rebind, "rebind");
   pragma Import (Java, CreateSubcontext, "createSubcontext");
   pragma Import (Java, GetSchema, "getSchema");
   pragma Import (Java, GetSchemaClassDefinition, "getSchemaClassDefinition");
   pragma Import (Java, Search, "search");

end Javax.Naming.Directory.InitialDirContext;
pragma Import (Java, Javax.Naming.Directory.InitialDirContext, "javax.naming.directory.InitialDirContext");
pragma Extensions_Allowed (Off);
