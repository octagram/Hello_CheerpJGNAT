pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.Attribute;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Naming.Directory.ModificationItem is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ModificationItem (P1_Int : Java.Int;
                                  P2_Attribute : access Standard.Javax.Naming.Directory.Attribute.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetModificationOp (This : access Typ)
                               return Java.Int;

   function GetAttribute (This : access Typ)
                          return access Javax.Naming.Directory.Attribute.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ModificationItem);
   pragma Import (Java, GetModificationOp, "getModificationOp");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.Directory.ModificationItem;
pragma Import (Java, Javax.Naming.Directory.ModificationItem, "javax.naming.directory.ModificationItem");
pragma Extensions_Allowed (Off);
