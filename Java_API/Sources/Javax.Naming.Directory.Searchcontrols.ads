pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Naming.Directory.SearchControls is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SearchControls (This : Ref := null)
                                return Ref;

   function New_SearchControls (P1_Int : Java.Int;
                                P2_Long : Java.Long;
                                P3_Int : Java.Int;
                                P4_String_Arr : access Java.Lang.String.Arr_Obj;
                                P5_Boolean : Java.Boolean;
                                P6_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSearchScope (This : access Typ)
                            return Java.Int;

   function GetTimeLimit (This : access Typ)
                          return Java.Int;

   function GetDerefLinkFlag (This : access Typ)
                              return Java.Boolean;

   function GetReturningObjFlag (This : access Typ)
                                 return Java.Boolean;

   function GetCountLimit (This : access Typ)
                           return Java.Long;

   function GetReturningAttributes (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   procedure SetSearchScope (This : access Typ;
                             P1_Int : Java.Int);

   procedure SetTimeLimit (This : access Typ;
                           P1_Int : Java.Int);

   procedure SetDerefLinkFlag (This : access Typ;
                               P1_Boolean : Java.Boolean);

   procedure SetReturningObjFlag (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   procedure SetCountLimit (This : access Typ;
                            P1_Long : Java.Long);

   procedure SetReturningAttributes (This : access Typ;
                                     P1_String_Arr : access Java.Lang.String.Arr_Obj);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OBJECT_SCOPE : constant Java.Int;

   --  final
   ONELEVEL_SCOPE : constant Java.Int;

   --  final
   SUBTREE_SCOPE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SearchControls);
   pragma Import (Java, GetSearchScope, "getSearchScope");
   pragma Import (Java, GetTimeLimit, "getTimeLimit");
   pragma Import (Java, GetDerefLinkFlag, "getDerefLinkFlag");
   pragma Import (Java, GetReturningObjFlag, "getReturningObjFlag");
   pragma Import (Java, GetCountLimit, "getCountLimit");
   pragma Import (Java, GetReturningAttributes, "getReturningAttributes");
   pragma Import (Java, SetSearchScope, "setSearchScope");
   pragma Import (Java, SetTimeLimit, "setTimeLimit");
   pragma Import (Java, SetDerefLinkFlag, "setDerefLinkFlag");
   pragma Import (Java, SetReturningObjFlag, "setReturningObjFlag");
   pragma Import (Java, SetCountLimit, "setCountLimit");
   pragma Import (Java, SetReturningAttributes, "setReturningAttributes");
   pragma Import (Java, OBJECT_SCOPE, "OBJECT_SCOPE");
   pragma Import (Java, ONELEVEL_SCOPE, "ONELEVEL_SCOPE");
   pragma Import (Java, SUBTREE_SCOPE, "SUBTREE_SCOPE");

end Javax.Naming.Directory.SearchControls;
pragma Import (Java, Javax.Naming.Directory.SearchControls, "javax.naming.directory.SearchControls");
pragma Extensions_Allowed (Off);
