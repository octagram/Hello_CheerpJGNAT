pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.Attributes;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.Binding;

package Javax.Naming.Directory.SearchResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.Binding.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SearchResult (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SearchResult (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class;
                              P4_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   function New_SearchResult (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P4_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SearchResult (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P4_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class;
                              P5_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributes (This : access Typ)
                           return access Javax.Naming.Directory.Attributes.Typ'Class;

   procedure SetAttributes (This : access Typ;
                            P1_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SearchResult);
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, SetAttributes, "setAttributes");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.Directory.SearchResult;
pragma Import (Java, Javax.Naming.Directory.SearchResult, "javax.naming.directory.SearchResult");
pragma Extensions_Allowed (Off);
