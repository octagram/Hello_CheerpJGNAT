pragma Extensions_Allowed (On);
package Javax.Naming.Directory is
   pragma Preelaborate;
end Javax.Naming.Directory;
pragma Import (Java, Javax.Naming.Directory, "javax.naming.directory");
pragma Extensions_Allowed (Off);
