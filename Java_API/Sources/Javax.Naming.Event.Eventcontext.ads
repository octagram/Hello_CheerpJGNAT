pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Event.NamingListener;
limited with Javax.Naming.Name;
with Java.Lang.Object;
with Javax.Naming.Context;

package Javax.Naming.Event.EventContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Context_I : Javax.Naming.Context.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNamingListener (This : access Typ;
                                P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                                P2_Int : Java.Int;
                                P3_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure AddNamingListener (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Int : Java.Int;
                                P3_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure RemoveNamingListener (This : access Typ;
                                   P1_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function TargetMustExist (This : access Typ)
                             return Java.Boolean is abstract;
   --  can raise Javax.Naming.NamingException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OBJECT_SCOPE : constant Java.Int;

   --  final
   ONELEVEL_SCOPE : constant Java.Int;

   --  final
   SUBTREE_SCOPE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddNamingListener, "addNamingListener");
   pragma Export (Java, RemoveNamingListener, "removeNamingListener");
   pragma Export (Java, TargetMustExist, "targetMustExist");
   pragma Import (Java, OBJECT_SCOPE, "OBJECT_SCOPE");
   pragma Import (Java, ONELEVEL_SCOPE, "ONELEVEL_SCOPE");
   pragma Import (Java, SUBTREE_SCOPE, "SUBTREE_SCOPE");

end Javax.Naming.Event.EventContext;
pragma Import (Java, Javax.Naming.Event.EventContext, "javax.naming.event.EventContext");
pragma Extensions_Allowed (Off);
