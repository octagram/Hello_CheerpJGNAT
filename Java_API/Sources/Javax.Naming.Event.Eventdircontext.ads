pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.SearchControls;
limited with Javax.Naming.Event.NamingListener;
limited with Javax.Naming.Name;
with Java.Lang.Object;
with Javax.Naming.Directory.DirContext;
with Javax.Naming.Event.EventContext;

package Javax.Naming.Event.EventDirContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DirContext_I : Javax.Naming.Directory.DirContext.Ref;
            EventContext_I : Javax.Naming.Event.EventContext.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNamingListener (This : access Typ;
                                P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class;
                                P4_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure AddNamingListener (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class;
                                P4_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure AddNamingListener (This : access Typ;
                                P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                P4_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class;
                                P5_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure AddNamingListener (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                P4_SearchControls : access Standard.Javax.Naming.Directory.SearchControls.Typ'Class;
                                P5_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class) is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddNamingListener, "addNamingListener");

end Javax.Naming.Event.EventDirContext;
pragma Import (Java, Javax.Naming.Event.EventDirContext, "javax.naming.event.EventDirContext");
pragma Extensions_Allowed (Off);
