pragma Extensions_Allowed (On);
limited with Javax.Naming.Binding;
limited with Javax.Naming.Event.EventContext;
limited with Javax.Naming.Event.NamingListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Naming.Event.NamingEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ChangeInfo : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, ChangeInfo, "changeInfo");

      --  protected
      type_K : Java.Int;
      pragma Import (Java, type_K, "type");

      --  protected
      OldBinding : access Javax.Naming.Binding.Typ'Class;
      pragma Import (Java, OldBinding, "oldBinding");

      --  protected
      NewBinding : access Javax.Naming.Binding.Typ'Class;
      pragma Import (Java, NewBinding, "newBinding");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NamingEvent (P1_EventContext : access Standard.Javax.Naming.Event.EventContext.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Binding : access Standard.Javax.Naming.Binding.Typ'Class;
                             P4_Binding : access Standard.Javax.Naming.Binding.Typ'Class;
                             P5_Object : access Standard.Java.Lang.Object.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return Java.Int;

   function GetEventContext (This : access Typ)
                             return access Javax.Naming.Event.EventContext.Typ'Class;

   function GetOldBinding (This : access Typ)
                           return access Javax.Naming.Binding.Typ'Class;

   function GetNewBinding (This : access Typ)
                           return access Javax.Naming.Binding.Typ'Class;

   function GetChangeInfo (This : access Typ)
                           return access Java.Lang.Object.Typ'Class;

   procedure Dispatch (This : access Typ;
                       P1_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OBJECT_ADDED : constant Java.Int;

   --  final
   OBJECT_REMOVED : constant Java.Int;

   --  final
   OBJECT_RENAMED : constant Java.Int;

   --  final
   OBJECT_CHANGED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NamingEvent);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetEventContext, "getEventContext");
   pragma Import (Java, GetOldBinding, "getOldBinding");
   pragma Import (Java, GetNewBinding, "getNewBinding");
   pragma Import (Java, GetChangeInfo, "getChangeInfo");
   pragma Import (Java, Dispatch, "dispatch");
   pragma Import (Java, OBJECT_ADDED, "OBJECT_ADDED");
   pragma Import (Java, OBJECT_REMOVED, "OBJECT_REMOVED");
   pragma Import (Java, OBJECT_RENAMED, "OBJECT_RENAMED");
   pragma Import (Java, OBJECT_CHANGED, "OBJECT_CHANGED");

end Javax.Naming.Event.NamingEvent;
pragma Import (Java, Javax.Naming.Event.NamingEvent, "javax.naming.event.NamingEvent");
pragma Extensions_Allowed (Off);
