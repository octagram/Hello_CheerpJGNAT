pragma Extensions_Allowed (On);
limited with Javax.Naming.Event.EventContext;
limited with Javax.Naming.Event.NamingListener;
limited with Javax.Naming.NamingException;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Naming.Event.NamingExceptionEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NamingExceptionEvent (P1_EventContext : access Standard.Javax.Naming.Event.EventContext.Typ'Class;
                                      P2_NamingException : access Standard.Javax.Naming.NamingException.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetException (This : access Typ)
                          return access Javax.Naming.NamingException.Typ'Class;

   function GetEventContext (This : access Typ)
                             return access Javax.Naming.Event.EventContext.Typ'Class;

   procedure Dispatch (This : access Typ;
                       P1_NamingListener : access Standard.Javax.Naming.Event.NamingListener.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NamingExceptionEvent);
   pragma Import (Java, GetException, "getException");
   pragma Import (Java, GetEventContext, "getEventContext");
   pragma Import (Java, Dispatch, "dispatch");

end Javax.Naming.Event.NamingExceptionEvent;
pragma Import (Java, Javax.Naming.Event.NamingExceptionEvent, "javax.naming.event.NamingExceptionEvent");
pragma Extensions_Allowed (Off);
