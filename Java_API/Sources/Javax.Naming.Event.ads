pragma Extensions_Allowed (On);
package Javax.Naming.Event is
   pragma Preelaborate;
end Javax.Naming.Event;
pragma Import (Java, Javax.Naming.Event, "javax.naming.event");
pragma Extensions_Allowed (Off);
