pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Naming.Name;
limited with Javax.Naming.NameParser;
limited with Javax.Naming.NamingEnumeration;
with Java.Lang.Object;
with Javax.Naming.Context;

package Javax.Naming.InitialContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Context_I : Javax.Naming.Context.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      MyProps : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, MyProps, "myProps");

      --  protected
      DefaultInitCtx : access Javax.Naming.Context.Typ'Class;
      pragma Import (Java, DefaultInitCtx, "defaultInitCtx");

      --  protected
      GotDefault : Java.Boolean;
      pragma Import (Java, GotDefault, "gotDefault");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_InitialContext (P1_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;
   --  can raise Javax.Naming.NamingException.Except

   function New_InitialContext (This : Ref := null)
                                return Ref;
   --  can raise Javax.Naming.NamingException.Except

   function New_InitialContext (P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Javax.Naming.NamingException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Init (This : access Typ;
                   P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   function DoLookup (P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function DoLookup (P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   --  protected
   function GetDefaultInitCtx (This : access Typ)
                               return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   --  protected
   function GetURLOrDefaultInitCtx (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   --  protected
   function GetURLOrDefaultInitCtx (This : access Typ;
                                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                                    return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Lookup (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function Lookup (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Bind (This : access Typ;
                   P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Rebind (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Unbind (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Unbind (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Rename (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure Rename (This : access Typ;
                     P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                     P2_Name : access Standard.Javax.Naming.Name.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   function List (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function List (This : access Typ;
                  P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                  return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function ListBindings (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function ListBindings (This : access Typ;
                          P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                          return access Javax.Naming.NamingEnumeration.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   procedure DestroySubcontext (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   procedure DestroySubcontext (This : access Typ;
                                P1_Name : access Standard.Javax.Naming.Name.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function CreateSubcontext (This : access Typ;
                              P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                              return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function LookupLink (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function LookupLink (This : access Typ;
                        P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetNameParser (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Naming.NameParser.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetNameParser (This : access Typ;
                           P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                           return access Javax.Naming.NameParser.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function ComposeName (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function ComposeName (This : access Typ;
                         P1_Name : access Standard.Javax.Naming.Name.Typ'Class;
                         P2_Name : access Standard.Javax.Naming.Name.Typ'Class)
                         return access Javax.Naming.Name.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function AddToEnvironment (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                              return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function RemoveFromEnvironment (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetEnvironment (This : access Typ)
                            return access Java.Util.Hashtable.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   procedure Close (This : access Typ);
   --  can raise Javax.Naming.NamingException.Except

   function GetNameInNamespace (This : access Typ)
                                return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InitialContext);
   pragma Import (Java, Init, "init");
   pragma Import (Java, DoLookup, "doLookup");
   pragma Import (Java, GetDefaultInitCtx, "getDefaultInitCtx");
   pragma Import (Java, GetURLOrDefaultInitCtx, "getURLOrDefaultInitCtx");
   pragma Import (Java, Lookup, "lookup");
   pragma Import (Java, Bind, "bind");
   pragma Import (Java, Rebind, "rebind");
   pragma Import (Java, Unbind, "unbind");
   pragma Import (Java, Rename, "rename");
   pragma Import (Java, List, "list");
   pragma Import (Java, ListBindings, "listBindings");
   pragma Import (Java, DestroySubcontext, "destroySubcontext");
   pragma Import (Java, CreateSubcontext, "createSubcontext");
   pragma Import (Java, LookupLink, "lookupLink");
   pragma Import (Java, GetNameParser, "getNameParser");
   pragma Import (Java, ComposeName, "composeName");
   pragma Import (Java, AddToEnvironment, "addToEnvironment");
   pragma Import (Java, RemoveFromEnvironment, "removeFromEnvironment");
   pragma Import (Java, GetEnvironment, "getEnvironment");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GetNameInNamespace, "getNameInNamespace");

end Javax.Naming.InitialContext;
pragma Import (Java, Javax.Naming.InitialContext, "javax.naming.InitialContext");
pragma Extensions_Allowed (Off);
