pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Naming.Ldap.Control;

package Javax.Naming.Ldap.BasicControl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Control_I : Javax.Naming.Ldap.Control.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Id : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Id, "id");

      --  protected
      Criticality : Java.Boolean;
      pragma Import (Java, Criticality, "criticality");

      --  protected
      Value : Java.Byte_Arr;
      pragma Import (Java, Value, "value");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicControl (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_BasicControl (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Boolean : Java.Boolean;
                              P3_Byte_Arr : Java.Byte_Arr; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function IsCritical (This : access Typ)
                        return Java.Boolean;

   function GetEncodedValue (This : access Typ)
                             return Java.Byte_Arr;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicControl);
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, IsCritical, "isCritical");
   pragma Import (Java, GetEncodedValue, "getEncodedValue");

end Javax.Naming.Ldap.BasicControl;
pragma Import (Java, Javax.Naming.Ldap.BasicControl, "javax.naming.ldap.BasicControl");
pragma Extensions_Allowed (Off);
