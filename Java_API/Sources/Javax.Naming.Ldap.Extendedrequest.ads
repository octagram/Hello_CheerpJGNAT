pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Ldap.ExtendedResponse;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Naming.Ldap.ExtendedRequest is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function GetEncodedValue (This : access Typ)
                             return Java.Byte_Arr is abstract;

   function CreateExtendedResponse (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Byte_Arr : Java.Byte_Arr;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int)
                                    return access Javax.Naming.Ldap.ExtendedResponse.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetID, "getID");
   pragma Export (Java, GetEncodedValue, "getEncodedValue");
   pragma Export (Java, CreateExtendedResponse, "createExtendedResponse");

end Javax.Naming.Ldap.ExtendedRequest;
pragma Import (Java, Javax.Naming.Ldap.ExtendedRequest, "javax.naming.ldap.ExtendedRequest");
pragma Extensions_Allowed (Off);
