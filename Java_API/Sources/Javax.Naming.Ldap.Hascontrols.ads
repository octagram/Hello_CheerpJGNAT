pragma Extensions_Allowed (On);
limited with Javax.Naming.Ldap.Control;
with Java.Lang.Object;

package Javax.Naming.Ldap.HasControls is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetControls (This : access Typ)
                         return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetControls, "getControls");

end Javax.Naming.Ldap.HasControls;
pragma Import (Java, Javax.Naming.Ldap.HasControls, "javax.naming.ldap.HasControls");
pragma Extensions_Allowed (Off);
