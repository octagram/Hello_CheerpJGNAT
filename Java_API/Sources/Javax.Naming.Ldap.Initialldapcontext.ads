pragma Extensions_Allowed (On);
limited with Java.Util.Hashtable;
limited with Javax.Naming.Ldap.Control;
limited with Javax.Naming.Ldap.ExtendedRequest;
limited with Javax.Naming.Ldap.ExtendedResponse;
with Java.Lang.Object;
with Javax.Naming.Context;
with Javax.Naming.Directory.DirContext;
with Javax.Naming.Directory.InitialDirContext;
with Javax.Naming.Ldap.LdapContext;

package Javax.Naming.Ldap.InitialLdapContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Context_I : Javax.Naming.Context.Ref;
            DirContext_I : Javax.Naming.Directory.DirContext.Ref;
            LdapContext_I : Javax.Naming.Ldap.LdapContext.Ref)
    is new Javax.Naming.Directory.InitialDirContext.Typ(Context_I,
                                                        DirContext_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InitialLdapContext (This : Ref := null)
                                    return Ref;
   --  can raise Javax.Naming.NamingException.Except

   function New_InitialLdapContext (P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class;
                                    P2_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Javax.Naming.NamingException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function ExtendedOperation (This : access Typ;
                               P1_ExtendedRequest : access Standard.Javax.Naming.Ldap.ExtendedRequest.Typ'Class)
                               return access Javax.Naming.Ldap.ExtendedResponse.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function NewInstance (This : access Typ;
                         P1_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj)
                         return access Javax.Naming.Ldap.LdapContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   procedure Reconnect (This : access Typ;
                        P1_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj);
   --  can raise Javax.Naming.NamingException.Except

   function GetConnectControls (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;
   --  can raise Javax.Naming.NamingException.Except

   procedure SetRequestControls (This : access Typ;
                                 P1_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj);
   --  can raise Javax.Naming.NamingException.Except

   function GetRequestControls (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;
   --  can raise Javax.Naming.NamingException.Except

   function GetResponseControls (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InitialLdapContext);
   pragma Import (Java, ExtendedOperation, "extendedOperation");
   pragma Import (Java, NewInstance, "newInstance");
   pragma Import (Java, Reconnect, "reconnect");
   pragma Import (Java, GetConnectControls, "getConnectControls");
   pragma Import (Java, SetRequestControls, "setRequestControls");
   pragma Import (Java, GetRequestControls, "getRequestControls");
   pragma Import (Java, GetResponseControls, "getResponseControls");

end Javax.Naming.Ldap.InitialLdapContext;
pragma Import (Java, Javax.Naming.Ldap.InitialLdapContext, "javax.naming.ldap.InitialLdapContext");
pragma Extensions_Allowed (Off);
