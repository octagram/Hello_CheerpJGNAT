pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Ldap.Control;
limited with Javax.Naming.Ldap.ExtendedRequest;
limited with Javax.Naming.Ldap.ExtendedResponse;
with Java.Lang.Object;
with Javax.Naming.Directory.DirContext;

package Javax.Naming.Ldap.LdapContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DirContext_I : Javax.Naming.Directory.DirContext.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ExtendedOperation (This : access Typ;
                               P1_ExtendedRequest : access Standard.Javax.Naming.Ldap.ExtendedRequest.Typ'Class)
                               return access Javax.Naming.Ldap.ExtendedResponse.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function NewInstance (This : access Typ;
                         P1_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj)
                         return access Javax.Naming.Ldap.LdapContext.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Reconnect (This : access Typ;
                        P1_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetConnectControls (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure SetRequestControls (This : access Typ;
                                 P1_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj) is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetRequestControls (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetResponseControls (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Javax.Naming.NamingException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CONTROL_FACTORIES : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ExtendedOperation, "extendedOperation");
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, Reconnect, "reconnect");
   pragma Export (Java, GetConnectControls, "getConnectControls");
   pragma Export (Java, SetRequestControls, "setRequestControls");
   pragma Export (Java, GetRequestControls, "getRequestControls");
   pragma Export (Java, GetResponseControls, "getResponseControls");
   pragma Import (Java, CONTROL_FACTORIES, "CONTROL_FACTORIES");

end Javax.Naming.Ldap.LdapContext;
pragma Import (Java, Javax.Naming.Ldap.LdapContext, "javax.naming.ldap.LdapContext");
pragma Extensions_Allowed (Off);
