pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Naming.Context;
limited with Javax.Naming.Ldap.Control;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.ReferralException;

package Javax.Naming.Ldap.LdapReferralException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Javax.Naming.ReferralException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   --  protected
   function New_LdapReferralException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   --  protected
   function New_LdapReferralException (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReferralContext (This : access Typ)
                                return access Javax.Naming.Context.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetReferralContext (This : access Typ;
                                P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                                return access Javax.Naming.Context.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetReferralContext (This : access Typ;
                                P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class;
                                P2_Control_Arr : access Javax.Naming.Ldap.Control.Arr_Obj)
                                return access Javax.Naming.Context.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Except, "javax.naming.ldap.LdapReferralException");
   pragma Java_Constructor (New_LdapReferralException);
   pragma Export (Java, GetReferralContext, "getReferralContext");

end Javax.Naming.Ldap.LdapReferralException;
pragma Import (Java, Javax.Naming.Ldap.LdapReferralException, "javax.naming.ldap.LdapReferralException");
pragma Extensions_Allowed (Off);
