pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Naming.Ldap.BasicControl;
with Javax.Naming.Ldap.Control;

package Javax.Naming.Ldap.PagedResultsControl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Control_I : Javax.Naming.Ldap.Control.Ref)
    is new Javax.Naming.Ldap.BasicControl.Typ(Control_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PagedResultsControl (P1_Int : Java.Int;
                                     P2_Boolean : Java.Boolean; 
                                     This : Ref := null)
                                     return Ref;
   --  can raise Java.Io.IOException.Except

   function New_PagedResultsControl (P1_Int : Java.Int;
                                     P2_Byte_Arr : Java.Byte_Arr;
                                     P3_Boolean : Java.Boolean; 
                                     This : Ref := null)
                                     return Ref;
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PagedResultsControl);
   pragma Import (Java, OID, "OID");

end Javax.Naming.Ldap.PagedResultsControl;
pragma Import (Java, Javax.Naming.Ldap.PagedResultsControl, "javax.naming.ldap.PagedResultsControl");
pragma Extensions_Allowed (Off);
