pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Directory.Attributes;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Javax.Naming.Ldap.Rdn is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Rdn (P1_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Javax.Naming.InvalidNameException.Except

   function New_Rdn (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Javax.Naming.InvalidNameException.Except

   function New_Rdn (P1_Rdn : access Standard.Javax.Naming.Ldap.Rdn.Typ'Class; 
                     This : Ref := null)
                     return Ref;

   function New_Rdn (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Javax.Naming.InvalidNameException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToAttributes (This : access Typ)
                          return access Javax.Naming.Directory.Attributes.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   function EscapeValue (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function UnescapeValue (P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Rdn);
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, CompareTo, "compareTo");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToAttributes, "toAttributes");
   pragma Import (Java, Size, "size");
   pragma Import (Java, EscapeValue, "escapeValue");
   pragma Import (Java, UnescapeValue, "unescapeValue");

end Javax.Naming.Ldap.Rdn;
pragma Import (Java, Javax.Naming.Ldap.Rdn, "javax.naming.ldap.Rdn");
pragma Extensions_Allowed (Off);
