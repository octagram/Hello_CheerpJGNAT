pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Naming.Ldap.SortKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SortKey (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_SortKey (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean;
                         P3_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributeID (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function IsAscending (This : access Typ)
                         return Java.Boolean;

   function GetMatchingRuleID (This : access Typ)
                               return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SortKey);
   pragma Import (Java, GetAttributeID, "getAttributeID");
   pragma Import (Java, IsAscending, "isAscending");
   pragma Import (Java, GetMatchingRuleID, "getMatchingRuleID");

end Javax.Naming.Ldap.SortKey;
pragma Import (Java, Javax.Naming.Ldap.SortKey, "javax.naming.ldap.SortKey");
pragma Extensions_Allowed (Off);
