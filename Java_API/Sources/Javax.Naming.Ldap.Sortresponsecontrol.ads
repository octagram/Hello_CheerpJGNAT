pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.NamingException;
with Java.Lang.Object;
with Javax.Naming.Ldap.BasicControl;
with Javax.Naming.Ldap.Control;

package Javax.Naming.Ldap.SortResponseControl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Control_I : Javax.Naming.Ldap.Control.Ref)
    is new Javax.Naming.Ldap.BasicControl.Typ(Control_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SortResponseControl (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_Boolean : Java.Boolean;
                                     P3_Byte_Arr : Java.Byte_Arr; 
                                     This : Ref := null)
                                     return Ref;
   --  can raise Java.Io.IOException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSorted (This : access Typ)
                      return Java.Boolean;

   function GetResultCode (This : access Typ)
                           return Java.Int;

   function GetAttributeID (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetException (This : access Typ)
                          return access Javax.Naming.NamingException.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SortResponseControl);
   pragma Import (Java, IsSorted, "isSorted");
   pragma Import (Java, GetResultCode, "getResultCode");
   pragma Import (Java, GetAttributeID, "getAttributeID");
   pragma Import (Java, GetException, "getException");
   pragma Import (Java, OID, "OID");

end Javax.Naming.Ldap.SortResponseControl;
pragma Import (Java, Javax.Naming.Ldap.SortResponseControl, "javax.naming.ldap.SortResponseControl");
pragma Extensions_Allowed (Off);
