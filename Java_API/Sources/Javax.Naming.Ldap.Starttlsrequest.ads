pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Ldap.ExtendedResponse;
with Java.Lang.Object;
with Javax.Naming.Ldap.ExtendedRequest;

package Javax.Naming.Ldap.StartTlsRequest is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ExtendedRequest_I : Javax.Naming.Ldap.ExtendedRequest.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StartTlsRequest (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetEncodedValue (This : access Typ)
                             return Java.Byte_Arr;

   function CreateExtendedResponse (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Byte_Arr : Java.Byte_Arr;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int)
                                    return access Javax.Naming.Ldap.ExtendedResponse.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StartTlsRequest);
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, GetEncodedValue, "getEncodedValue");
   pragma Import (Java, CreateExtendedResponse, "createExtendedResponse");
   pragma Import (Java, OID, "OID");

end Javax.Naming.Ldap.StartTlsRequest;
pragma Import (Java, Javax.Naming.Ldap.StartTlsRequest, "javax.naming.ldap.StartTlsRequest");
pragma Extensions_Allowed (Off);
