pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Net.Ssl.HostnameVerifier;
limited with Javax.Net.Ssl.SSLSession;
limited with Javax.Net.Ssl.SSLSocketFactory;
with Java.Lang.Object;
with Javax.Naming.Ldap.ExtendedResponse;

package Javax.Naming.Ldap.StartTlsResponse is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ExtendedResponse_I : Javax.Naming.Ldap.ExtendedResponse.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_StartTlsResponse (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetEncodedValue (This : access Typ)
                             return Java.Byte_Arr;

   procedure SetEnabledCipherSuites (This : access Typ;
                                     P1_String_Arr : access Java.Lang.String.Arr_Obj) is abstract;

   procedure SetHostnameVerifier (This : access Typ;
                                  P1_HostnameVerifier : access Standard.Javax.Net.Ssl.HostnameVerifier.Typ'Class) is abstract;

   function Negotiate (This : access Typ)
                       return access Javax.Net.Ssl.SSLSession.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function Negotiate (This : access Typ;
                       P1_SSLSocketFactory : access Standard.Javax.Net.Ssl.SSLSocketFactory.Typ'Class)
                       return access Javax.Net.Ssl.SSLSession.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StartTlsResponse);
   pragma Export (Java, GetID, "getID");
   pragma Export (Java, GetEncodedValue, "getEncodedValue");
   pragma Export (Java, SetEnabledCipherSuites, "setEnabledCipherSuites");
   pragma Export (Java, SetHostnameVerifier, "setHostnameVerifier");
   pragma Export (Java, Negotiate, "negotiate");
   pragma Export (Java, Close, "close");
   pragma Import (Java, OID, "OID");

end Javax.Naming.Ldap.StartTlsResponse;
pragma Import (Java, Javax.Naming.Ldap.StartTlsResponse, "javax.naming.ldap.StartTlsResponse");
pragma Extensions_Allowed (Off);
