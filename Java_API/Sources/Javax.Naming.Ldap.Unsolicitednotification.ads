pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.NamingException;
with Java.Lang.Object;
with Javax.Naming.Ldap.ExtendedResponse;
with Javax.Naming.Ldap.HasControls;

package Javax.Naming.Ldap.UnsolicitedNotification is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ExtendedResponse_I : Javax.Naming.Ldap.ExtendedResponse.Ref;
            HasControls_I : Javax.Naming.Ldap.HasControls.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReferrals (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   function GetException (This : access Typ)
                          return access Javax.Naming.NamingException.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetReferrals, "getReferrals");
   pragma Export (Java, GetException, "getException");

end Javax.Naming.Ldap.UnsolicitedNotification;
pragma Import (Java, Javax.Naming.Ldap.UnsolicitedNotification, "javax.naming.ldap.UnsolicitedNotification");
pragma Extensions_Allowed (Off);
