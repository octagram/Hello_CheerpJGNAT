pragma Extensions_Allowed (On);
limited with Javax.Naming.Ldap.UnsolicitedNotification;
limited with Javax.Naming.Ldap.UnsolicitedNotificationListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Naming.Ldap.UnsolicitedNotificationEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnsolicitedNotificationEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                              P2_UnsolicitedNotification : access Standard.Javax.Naming.Ldap.UnsolicitedNotification.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNotification (This : access Typ)
                             return access Javax.Naming.Ldap.UnsolicitedNotification.Typ'Class;

   procedure Dispatch (This : access Typ;
                       P1_UnsolicitedNotificationListener : access Standard.Javax.Naming.Ldap.UnsolicitedNotificationListener.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UnsolicitedNotificationEvent);
   pragma Import (Java, GetNotification, "getNotification");
   pragma Import (Java, Dispatch, "dispatch");

end Javax.Naming.Ldap.UnsolicitedNotificationEvent;
pragma Import (Java, Javax.Naming.Ldap.UnsolicitedNotificationEvent, "javax.naming.ldap.UnsolicitedNotificationEvent");
pragma Extensions_Allowed (Off);
