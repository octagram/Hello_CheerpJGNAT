pragma Extensions_Allowed (On);
limited with Javax.Naming.Ldap.UnsolicitedNotificationEvent;
with Java.Lang.Object;
with Javax.Naming.Event.NamingListener;

package Javax.Naming.Ldap.UnsolicitedNotificationListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            NamingListener_I : Javax.Naming.Event.NamingListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure NotificationReceived (This : access Typ;
                                   P1_UnsolicitedNotificationEvent : access Standard.Javax.Naming.Ldap.UnsolicitedNotificationEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NotificationReceived, "notificationReceived");

end Javax.Naming.Ldap.UnsolicitedNotificationListener;
pragma Import (Java, Javax.Naming.Ldap.UnsolicitedNotificationListener, "javax.naming.ldap.UnsolicitedNotificationListener");
pragma Extensions_Allowed (Off);
