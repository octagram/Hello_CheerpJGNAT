pragma Extensions_Allowed (On);
package Javax.Naming.Ldap is
   pragma Preelaborate;
end Javax.Naming.Ldap;
pragma Import (Java, Javax.Naming.Ldap, "javax.naming.ldap");
pragma Extensions_Allowed (Off);
