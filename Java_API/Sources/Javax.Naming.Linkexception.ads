pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Name;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.NamingException;

package Javax.Naming.LinkException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.NamingException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      LinkResolvedName : access Javax.Naming.Name.Typ'Class;
      pragma Import (Java, LinkResolvedName, "linkResolvedName");

      --  protected
      LinkResolvedObj : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, LinkResolvedObj, "linkResolvedObj");

      --  protected
      LinkRemainingName : access Javax.Naming.Name.Typ'Class;
      pragma Import (Java, LinkRemainingName, "linkRemainingName");

      --  protected
      LinkExplanation : access Java.Lang.String.Typ'Class;
      pragma Import (Java, LinkExplanation, "linkExplanation");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinkException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_LinkException (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLinkResolvedName (This : access Typ)
                                 return access Javax.Naming.Name.Typ'Class;

   function GetLinkRemainingName (This : access Typ)
                                  return access Javax.Naming.Name.Typ'Class;

   function GetLinkResolvedObj (This : access Typ)
                                return access Java.Lang.Object.Typ'Class;

   function GetLinkExplanation (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure SetLinkExplanation (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetLinkResolvedName (This : access Typ;
                                  P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   procedure SetLinkRemainingName (This : access Typ;
                                   P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   procedure SetLinkResolvedObj (This : access Typ;
                                 P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ;
                      P1_Boolean : Java.Boolean)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.naming.LinkException");
   pragma Java_Constructor (New_LinkException);
   pragma Import (Java, GetLinkResolvedName, "getLinkResolvedName");
   pragma Import (Java, GetLinkRemainingName, "getLinkRemainingName");
   pragma Import (Java, GetLinkResolvedObj, "getLinkResolvedObj");
   pragma Import (Java, GetLinkExplanation, "getLinkExplanation");
   pragma Import (Java, SetLinkExplanation, "setLinkExplanation");
   pragma Import (Java, SetLinkResolvedName, "setLinkResolvedName");
   pragma Import (Java, SetLinkRemainingName, "setLinkRemainingName");
   pragma Import (Java, SetLinkResolvedObj, "setLinkResolvedObj");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.LinkException;
pragma Import (Java, Javax.Naming.LinkException, "javax.naming.LinkException");
pragma Extensions_Allowed (Off);
