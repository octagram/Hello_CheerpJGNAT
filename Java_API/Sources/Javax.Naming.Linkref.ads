pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Name;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Naming.Reference;

package Javax.Naming.LinkRef is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Naming.Reference.Typ(Serializable_I,
                                      Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinkRef (P1_Name : access Standard.Javax.Naming.Name.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_LinkRef (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLinkName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LinkRef);
   pragma Import (Java, GetLinkName, "getLinkName");

end Javax.Naming.LinkRef;
pragma Import (Java, Javax.Naming.LinkRef, "javax.naming.LinkRef");
pragma Extensions_Allowed (Off);
