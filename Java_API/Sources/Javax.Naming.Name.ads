pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Comparable;
with Java.Lang.Object;

package Javax.Naming.Name is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function CompareTo (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Int is abstract;

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function IsEmpty (This : access Typ)
                     return Java.Boolean is abstract;

   function GetAll (This : access Typ)
                    return access Java.Util.Enumeration.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.String.Typ'Class is abstract;

   function GetPrefix (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Naming.Name.Typ'Class is abstract;

   function GetSuffix (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Naming.Name.Typ'Class is abstract;

   function StartsWith (This : access Typ;
                        P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                        return Java.Boolean is abstract;

   function EndsWith (This : access Typ;
                      P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                      return Java.Boolean is abstract;

   function AddAll (This : access Typ;
                    P1_Name : access Standard.Javax.Naming.Name.Typ'Class)
                    return access Javax.Naming.Name.Typ'Class is abstract;
   --  can raise Javax.Naming.InvalidNameException.Except

   function AddAll (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Name : access Standard.Javax.Naming.Name.Typ'Class)
                    return access Javax.Naming.Name.Typ'Class is abstract;
   --  can raise Javax.Naming.InvalidNameException.Except

   function Add (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Naming.Name.Typ'Class is abstract;
   --  can raise Javax.Naming.InvalidNameException.Except

   function Add (This : access Typ;
                 P1_Int : Java.Int;
                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Naming.Name.Typ'Class is abstract;
   --  can raise Javax.Naming.InvalidNameException.Except

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.InvalidNameException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SerialVersionUID : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, CompareTo, "compareTo");
   pragma Export (Java, Size, "size");
   pragma Export (Java, IsEmpty, "isEmpty");
   pragma Export (Java, GetAll, "getAll");
   pragma Export (Java, Get, "get");
   pragma Export (Java, GetPrefix, "getPrefix");
   pragma Export (Java, GetSuffix, "getSuffix");
   pragma Export (Java, StartsWith, "startsWith");
   pragma Export (Java, EndsWith, "endsWith");
   pragma Export (Java, AddAll, "addAll");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Remove, "remove");
   pragma Import (Java, SerialVersionUID, "serialVersionUID");

end Javax.Naming.Name;
pragma Import (Java, Javax.Naming.Name, "javax.naming.Name");
pragma Extensions_Allowed (Off);
