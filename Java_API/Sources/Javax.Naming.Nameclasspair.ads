pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Naming.NameClassPair is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NameClassPair (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_NameClassPair (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Boolean : Java.Boolean; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetClassName (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   function IsRelative (This : access Typ)
                        return Java.Boolean;

   procedure SetRelative (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function GetNameInNamespace (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure SetNameInNamespace (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NameClassPair);
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetName, "setName");
   pragma Import (Java, SetClassName, "setClassName");
   pragma Import (Java, IsRelative, "isRelative");
   pragma Import (Java, SetRelative, "setRelative");
   pragma Import (Java, GetNameInNamespace, "getNameInNamespace");
   pragma Import (Java, SetNameInNamespace, "setNameInNamespace");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.NameClassPair;
pragma Import (Java, Javax.Naming.NameClassPair, "javax.naming.NameClassPair");
pragma Extensions_Allowed (Off);
