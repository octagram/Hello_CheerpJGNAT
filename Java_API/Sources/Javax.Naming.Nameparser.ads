pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Name;
with Java.Lang.Object;

package Javax.Naming.NameParser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Javax.Naming.Name.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Parse, "parse");

end Javax.Naming.NameParser;
pragma Import (Java, Javax.Naming.NameParser, "javax.naming.NameParser");
pragma Extensions_Allowed (Off);
