pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Java.Util.Enumeration;

package Javax.Naming.NamingEnumeration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Enumeration_I : Java.Util.Enumeration.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Next (This : access Typ)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function HasMore (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Javax.Naming.NamingException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Next, "next");
   pragma Export (Java, HasMore, "hasMore");
   pragma Export (Java, Close, "close");

end Javax.Naming.NamingEnumeration;
pragma Import (Java, Javax.Naming.NamingEnumeration, "javax.naming.NamingEnumeration");
pragma Extensions_Allowed (Off);
