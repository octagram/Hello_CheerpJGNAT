pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Naming.Name;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Naming.NamingException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ResolvedName : access Javax.Naming.Name.Typ'Class;
      pragma Import (Java, ResolvedName, "resolvedName");

      --  protected
      ResolvedObj : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, ResolvedObj, "resolvedObj");

      --  protected
      RemainingName : access Javax.Naming.Name.Typ'Class;
      pragma Import (Java, RemainingName, "remainingName");

      --  protected
      RootException : access Java.Lang.Throwable.Typ'Class;
      pragma Import (Java, RootException, "rootException");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NamingException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_NamingException (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetResolvedName (This : access Typ)
                             return access Javax.Naming.Name.Typ'Class;

   function GetRemainingName (This : access Typ)
                              return access Javax.Naming.Name.Typ'Class;

   function GetResolvedObj (This : access Typ)
                            return access Java.Lang.Object.Typ'Class;

   function GetExplanation (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetResolvedName (This : access Typ;
                              P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   procedure SetRemainingName (This : access Typ;
                               P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   procedure SetResolvedObj (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AppendRemainingComponent (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AppendRemainingName (This : access Typ;
                                  P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   function GetRootCause (This : access Typ)
                          return access Java.Lang.Throwable.Typ'Class;

   procedure SetRootCause (This : access Typ;
                           P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;

   function InitCause (This : access Typ;
                       P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class)
                       return access Java.Lang.Throwable.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ;
                      P1_Boolean : Java.Boolean)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.naming.NamingException");
   pragma Java_Constructor (New_NamingException);
   pragma Import (Java, GetResolvedName, "getResolvedName");
   pragma Import (Java, GetRemainingName, "getRemainingName");
   pragma Import (Java, GetResolvedObj, "getResolvedObj");
   pragma Import (Java, GetExplanation, "getExplanation");
   pragma Import (Java, SetResolvedName, "setResolvedName");
   pragma Import (Java, SetRemainingName, "setRemainingName");
   pragma Import (Java, SetResolvedObj, "setResolvedObj");
   pragma Import (Java, AppendRemainingComponent, "appendRemainingComponent");
   pragma Import (Java, AppendRemainingName, "appendRemainingName");
   pragma Import (Java, GetRootCause, "getRootCause");
   pragma Import (Java, SetRootCause, "setRootCause");
   pragma Import (Java, GetCause, "getCause");
   pragma Import (Java, InitCause, "initCause");
   pragma Import (Java, ToString, "toString");

end Javax.Naming.NamingException;
pragma Import (Java, Javax.Naming.NamingException, "javax.naming.NamingException");
pragma Extensions_Allowed (Off);
