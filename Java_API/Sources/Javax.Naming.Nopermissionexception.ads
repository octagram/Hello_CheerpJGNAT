pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.NamingSecurityException;

package Javax.Naming.NoPermissionException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.NamingSecurityException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NoPermissionException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_NoPermissionException (This : Ref := null)
                                       return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.naming.NoPermissionException");
   pragma Java_Constructor (New_NoPermissionException);

end Javax.Naming.NoPermissionException;
pragma Import (Java, Javax.Naming.NoPermissionException, "javax.naming.NoPermissionException");
pragma Extensions_Allowed (Off);
