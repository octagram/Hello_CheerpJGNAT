pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.NamingException;

package Javax.Naming.PartialResultException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Naming.NamingException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PartialResultException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   function New_PartialResultException (This : Ref := null)
                                        return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.naming.PartialResultException");
   pragma Java_Constructor (New_PartialResultException);

end Javax.Naming.PartialResultException;
pragma Import (Java, Javax.Naming.PartialResultException, "javax.naming.PartialResultException");
pragma Extensions_Allowed (Off);
