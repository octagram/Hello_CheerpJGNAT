pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Vector;
limited with Javax.Naming.RefAddr;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Naming.Reference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ClassName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ClassName, "className");

      --  protected
      Addrs : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Addrs, "addrs");

      --  protected
      ClassFactory : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ClassFactory, "classFactory");

      --  protected
      ClassFactoryLocation : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ClassFactoryLocation, "classFactoryLocation");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Reference (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Reference (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_RefAddr : access Standard.Javax.Naming.RefAddr.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Reference (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_Reference (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_RefAddr : access Standard.Javax.Naming.RefAddr.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetFactoryClassName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetFactoryClassLocation (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Naming.RefAddr.Typ'Class;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Javax.Naming.RefAddr.Typ'Class;

   function GetAll (This : access Typ)
                    return access Java.Util.Enumeration.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   procedure Add (This : access Typ;
                  P1_RefAddr : access Standard.Javax.Naming.RefAddr.Typ'Class);

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_RefAddr : access Standard.Javax.Naming.RefAddr.Typ'Class);

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Reference);
   pragma Import (Java, GetClassName, "getClassName");
   pragma Import (Java, GetFactoryClassName, "getFactoryClassName");
   pragma Import (Java, GetFactoryClassLocation, "getFactoryClassLocation");
   pragma Import (Java, Get, "get");
   pragma Import (Java, GetAll, "getAll");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clone, "clone");

end Javax.Naming.Reference;
pragma Import (Java, Javax.Naming.Reference, "javax.naming.Reference");
pragma Extensions_Allowed (Off);
