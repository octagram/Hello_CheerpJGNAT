pragma Extensions_Allowed (On);
limited with Javax.Naming.Reference;
with Java.Lang.Object;

package Javax.Naming.Referenceable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReference (This : access Typ)
                          return access Javax.Naming.Reference.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetReference, "getReference");

end Javax.Naming.Referenceable;
pragma Import (Java, Javax.Naming.Referenceable, "javax.naming.Referenceable");
pragma Extensions_Allowed (Off);
