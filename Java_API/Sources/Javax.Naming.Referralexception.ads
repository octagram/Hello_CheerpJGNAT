pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Naming.Context;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Naming.NamingException;

package Javax.Naming.ReferralException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Javax.Naming.NamingException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   --  protected
   function New_ReferralException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   --  protected
   function New_ReferralException (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReferralInfo (This : access Typ)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function GetReferralContext (This : access Typ)
                                return access Javax.Naming.Context.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function GetReferralContext (This : access Typ;
                                P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                                return access Javax.Naming.Context.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except

   function SkipReferral (This : access Typ)
                          return Java.Boolean is abstract;

   procedure RetryReferral (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Except, "javax.naming.ReferralException");
   pragma Java_Constructor (New_ReferralException);
   pragma Export (Java, GetReferralInfo, "getReferralInfo");
   pragma Export (Java, GetReferralContext, "getReferralContext");
   pragma Export (Java, SkipReferral, "skipReferral");
   pragma Export (Java, RetryReferral, "retryReferral");

end Javax.Naming.ReferralException;
pragma Import (Java, Javax.Naming.ReferralException, "javax.naming.ReferralException");
pragma Extensions_Allowed (Off);
