pragma Extensions_Allowed (On);
limited with Java.Util.Hashtable;
limited with Javax.Naming.CannotProceedException;
limited with Javax.Naming.Context;
limited with Javax.Naming.Directory.Attributes;
limited with Javax.Naming.Directory.DirContext;
limited with Javax.Naming.Name;
limited with Javax.Naming.Spi.DirStateFactory.Result;
with Java.Lang.Object;
with Javax.Naming.Spi.NamingManager;

package Javax.Naming.Spi.DirectoryManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Naming.Spi.NamingManager.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContinuationDirContext (P1_CannotProceedException : access Standard.Javax.Naming.CannotProceedException.Typ'Class)
                                       return access Javax.Naming.Directory.DirContext.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetObjectInstance (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Name : access Standard.Javax.Naming.Name.Typ'Class;
                               P3_Context : access Standard.Javax.Naming.Context.Typ'Class;
                               P4_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class;
                               P5_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                               return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   function GetStateToBind (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P2_Name : access Standard.Javax.Naming.Name.Typ'Class;
                            P3_Context : access Standard.Javax.Naming.Context.Typ'Class;
                            P4_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class;
                            P5_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                            return access Javax.Naming.Spi.DirStateFactory.Result.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetContinuationDirContext, "getContinuationDirContext");
   pragma Import (Java, GetObjectInstance, "getObjectInstance");
   pragma Import (Java, GetStateToBind, "getStateToBind");

end Javax.Naming.Spi.DirectoryManager;
pragma Import (Java, Javax.Naming.Spi.DirectoryManager, "javax.naming.spi.DirectoryManager");
pragma Extensions_Allowed (Off);
