pragma Extensions_Allowed (On);
limited with Java.Util.Hashtable;
limited with Javax.Naming.Context;
limited with Javax.Naming.Directory.Attributes;
limited with Javax.Naming.Name;
with Java.Lang.Object;
with Javax.Naming.Spi.ObjectFactory;

package Javax.Naming.Spi.DirObjectFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ObjectFactory_I : Javax.Naming.Spi.ObjectFactory.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetObjectInstance (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Name : access Standard.Javax.Naming.Name.Typ'Class;
                               P3_Context : access Standard.Javax.Naming.Context.Typ'Class;
                               P4_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class;
                               P5_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class)
                               return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.Exception_K.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetObjectInstance, "getObjectInstance");

end Javax.Naming.Spi.DirObjectFactory;
pragma Import (Java, Javax.Naming.Spi.DirObjectFactory, "javax.naming.spi.DirObjectFactory");
pragma Extensions_Allowed (Off);
