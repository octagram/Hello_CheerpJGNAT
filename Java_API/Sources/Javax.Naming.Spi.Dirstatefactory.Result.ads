pragma Extensions_Allowed (On);
limited with Javax.Naming.Directory.Attributes;
with Java.Lang.Object;

package Javax.Naming.Spi.DirStateFactory.Result is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Result (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Attributes : access Standard.Javax.Naming.Directory.Attributes.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetObject (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Naming.Directory.Attributes.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Result);
   pragma Import (Java, GetObject, "getObject");
   pragma Import (Java, GetAttributes, "getAttributes");

end Javax.Naming.Spi.DirStateFactory.Result;
pragma Import (Java, Javax.Naming.Spi.DirStateFactory.Result, "javax.naming.spi.DirStateFactory$Result");
pragma Extensions_Allowed (Off);
