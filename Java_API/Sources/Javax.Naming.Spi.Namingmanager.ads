pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Naming.CannotProceedException;
limited with Javax.Naming.Context;
limited with Javax.Naming.Name;
limited with Javax.Naming.Spi.InitialContextFactoryBuilder;
limited with Javax.Naming.Spi.ObjectFactoryBuilder;
with Java.Lang.Object;

package Javax.Naming.Spi.NamingManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure SetObjectFactoryBuilder (P1_ObjectFactoryBuilder : access Standard.Javax.Naming.Spi.ObjectFactoryBuilder.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   function GetObjectInstance (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Name : access Standard.Javax.Naming.Name.Typ'Class;
                               P3_Context : access Standard.Javax.Naming.Context.Typ'Class;
                               P4_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                               return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   function GetURLContext (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                           return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetInitialContext (P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                               return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   --  synchronized
   procedure SetInitialContextFactoryBuilder (P1_InitialContextFactoryBuilder : access Standard.Javax.Naming.Spi.InitialContextFactoryBuilder.Typ'Class);
   --  can raise Javax.Naming.NamingException.Except

   function HasInitialContextFactoryBuilder return Java.Boolean;

   function GetContinuationContext (P1_CannotProceedException : access Standard.Javax.Naming.CannotProceedException.Typ'Class)
                                    return access Javax.Naming.Context.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   function GetStateToBind (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P2_Name : access Standard.Javax.Naming.Name.Typ'Class;
                            P3_Context : access Standard.Javax.Naming.Context.Typ'Class;
                            P4_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                            return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Naming.NamingException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CPE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetObjectFactoryBuilder, "setObjectFactoryBuilder");
   pragma Import (Java, GetObjectInstance, "getObjectInstance");
   pragma Import (Java, GetURLContext, "getURLContext");
   pragma Import (Java, GetInitialContext, "getInitialContext");
   pragma Import (Java, SetInitialContextFactoryBuilder, "setInitialContextFactoryBuilder");
   pragma Import (Java, HasInitialContextFactoryBuilder, "hasInitialContextFactoryBuilder");
   pragma Import (Java, GetContinuationContext, "getContinuationContext");
   pragma Import (Java, GetStateToBind, "getStateToBind");
   pragma Import (Java, CPE, "CPE");

end Javax.Naming.Spi.NamingManager;
pragma Import (Java, Javax.Naming.Spi.NamingManager, "javax.naming.spi.NamingManager");
pragma Extensions_Allowed (Off);
