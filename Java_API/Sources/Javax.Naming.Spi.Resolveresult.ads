pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Naming.Name;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Naming.Spi.ResolveResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ResolvedObj : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, ResolvedObj, "resolvedObj");

      --  protected
      RemainingName : access Javax.Naming.Name.Typ'Class;
      pragma Import (Java, RemainingName, "remainingName");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ResolveResult (This : Ref := null)
                               return Ref;

   function New_ResolveResult (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_ResolveResult (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Name : access Standard.Javax.Naming.Name.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRemainingName (This : access Typ)
                              return access Javax.Naming.Name.Typ'Class;

   function GetResolvedObj (This : access Typ)
                            return access Java.Lang.Object.Typ'Class;

   procedure SetRemainingName (This : access Typ;
                               P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   procedure AppendRemainingName (This : access Typ;
                                  P1_Name : access Standard.Javax.Naming.Name.Typ'Class);

   procedure AppendRemainingComponent (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetResolvedObj (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ResolveResult);
   pragma Import (Java, GetRemainingName, "getRemainingName");
   pragma Import (Java, GetResolvedObj, "getResolvedObj");
   pragma Import (Java, SetRemainingName, "setRemainingName");
   pragma Import (Java, AppendRemainingName, "appendRemainingName");
   pragma Import (Java, AppendRemainingComponent, "appendRemainingComponent");
   pragma Import (Java, SetResolvedObj, "setResolvedObj");

end Javax.Naming.Spi.ResolveResult;
pragma Import (Java, Javax.Naming.Spi.ResolveResult, "javax.naming.spi.ResolveResult");
pragma Extensions_Allowed (Off);
