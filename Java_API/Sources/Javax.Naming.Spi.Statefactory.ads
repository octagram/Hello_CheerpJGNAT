pragma Extensions_Allowed (On);
limited with Java.Util.Hashtable;
limited with Javax.Naming.Context;
limited with Javax.Naming.Name;
with Java.Lang.Object;

package Javax.Naming.Spi.StateFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStateToBind (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                            P2_Name : access Standard.Javax.Naming.Name.Typ'Class;
                            P3_Context : access Standard.Javax.Naming.Context.Typ'Class;
                            P4_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class)
                            return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Naming.NamingException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetStateToBind, "getStateToBind");

end Javax.Naming.Spi.StateFactory;
pragma Import (Java, Javax.Naming.Spi.StateFactory, "javax.naming.spi.StateFactory");
pragma Extensions_Allowed (Off);
