pragma Extensions_Allowed (On);
package Javax.Naming.Spi is
   pragma Preelaborate;
end Javax.Naming.Spi;
pragma Import (Java, Javax.Naming.Spi, "javax.naming.spi");
pragma Extensions_Allowed (Off);
