pragma Extensions_Allowed (On);
package Javax.Naming is
   pragma Preelaborate;
end Javax.Naming;
pragma Import (Java, Javax.Naming, "javax.naming");
pragma Extensions_Allowed (Off);
