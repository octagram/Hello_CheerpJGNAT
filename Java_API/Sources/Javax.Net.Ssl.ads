pragma Extensions_Allowed (On);
package Javax.Net.Ssl is
   pragma Preelaborate;
end Javax.Net.Ssl;
pragma Import (Java, Javax.Net.Ssl, "javax.net.ssl");
pragma Extensions_Allowed (Off);
