pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Javax.Print.Attribute.Attribute;
with Java.Lang.Object;

package Javax.Print.Attribute.AttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get (This : access Typ;
                 P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                 return access Javax.Print.Attribute.Attribute.Typ'Class is abstract;

   function Add (This : access Typ;
                 P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class)
                 return Java.Boolean is abstract;

   function Remove (This : access Typ;
                    P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return Java.Boolean is abstract;

   function Remove (This : access Typ;
                    P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class)
                    return Java.Boolean is abstract;

   function ContainsKey (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return Java.Boolean is abstract;

   function ContainsValue (This : access Typ;
                           P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class)
                           return Java.Boolean is abstract;

   function AddAll (This : access Typ;
                    P1_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                    return Java.Boolean is abstract;

   function Size (This : access Typ)
                  return Java.Int is abstract;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref is abstract;

   procedure Clear (This : access Typ) is abstract;

   function IsEmpty (This : access Typ)
                     return Java.Boolean is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get, "get");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, ContainsKey, "containsKey");
   pragma Export (Java, ContainsValue, "containsValue");
   pragma Export (Java, AddAll, "addAll");
   pragma Export (Java, Size, "size");
   pragma Export (Java, ToArray, "toArray");
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, IsEmpty, "isEmpty");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");

end Javax.Print.Attribute.AttributeSet;
pragma Import (Java, Javax.Print.Attribute.AttributeSet, "javax.print.attribute.AttributeSet");
pragma Extensions_Allowed (Off);
