pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Javax.Print.Attribute.Attribute;
limited with Javax.Print.Attribute.AttributeSet;
limited with Javax.Print.Attribute.DocAttributeSet;
limited with Javax.Print.Attribute.PrintJobAttributeSet;
limited with Javax.Print.Attribute.PrintRequestAttributeSet;
limited with Javax.Print.Attribute.PrintServiceAttributeSet;
with Java.Lang.Object;

package Javax.Print.Attribute.AttributeSetUtilities is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function UnmodifiableView (P1_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.AttributeSet.Typ'Class;

   function UnmodifiableView (P1_DocAttributeSet : access Standard.Javax.Print.Attribute.DocAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.DocAttributeSet.Typ'Class;

   function UnmodifiableView (P1_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class;

   function UnmodifiableView (P1_PrintJobAttributeSet : access Standard.Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class;

   function UnmodifiableView (P1_PrintServiceAttributeSet : access Standard.Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class;

   function SynchronizedView (P1_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.AttributeSet.Typ'Class;

   function SynchronizedView (P1_DocAttributeSet : access Standard.Javax.Print.Attribute.DocAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.DocAttributeSet.Typ'Class;

   function SynchronizedView (P1_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class;

   function SynchronizedView (P1_PrintJobAttributeSet : access Standard.Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class;

   function SynchronizedView (P1_PrintServiceAttributeSet : access Standard.Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class)
                              return access Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class;

   function VerifyAttributeCategory (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                                     return access Java.Lang.Class.Typ'Class;

   function VerifyAttributeValue (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                  P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                                  return access Javax.Print.Attribute.Attribute.Typ'Class;

   procedure VerifyCategoryForValue (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                     P2_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, UnmodifiableView, "unmodifiableView");
   pragma Import (Java, SynchronizedView, "synchronizedView");
   pragma Import (Java, VerifyAttributeCategory, "verifyAttributeCategory");
   pragma Import (Java, VerifyAttributeValue, "verifyAttributeValue");
   pragma Import (Java, VerifyCategoryForValue, "verifyCategoryForValue");

end Javax.Print.Attribute.AttributeSetUtilities;
pragma Import (Java, Javax.Print.Attribute.AttributeSetUtilities, "javax.print.attribute.AttributeSetUtilities");
pragma Extensions_Allowed (Off);
