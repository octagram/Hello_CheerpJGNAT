pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Javax.Print.Attribute.Attribute;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Attribute.AttributeSet;

package Javax.Print.Attribute.HashAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AttributeSet_I : Javax.Print.Attribute.AttributeSet.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HashAttributeSet (This : Ref := null)
                                  return Ref;

   function New_HashAttributeSet (P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_HashAttributeSet (P1_Attribute_Arr : access Javax.Print.Attribute.Attribute.Arr_Obj; 
                                  This : Ref := null)
                                  return Ref;

   function New_HashAttributeSet (P1_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   --  protected
   function New_HashAttributeSet (P1_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   --  protected
   function New_HashAttributeSet (P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class;
                                  P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   --  protected
   function New_HashAttributeSet (P1_Attribute_Arr : access Javax.Print.Attribute.Attribute.Arr_Obj;
                                  P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   --  protected
   function New_HashAttributeSet (P1_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class;
                                  P2_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get (This : access Typ;
                 P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                 return access Javax.Print.Attribute.Attribute.Typ'Class;

   function Add (This : access Typ;
                 P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return Java.Boolean;

   function Remove (This : access Typ;
                    P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class)
                    return Java.Boolean;

   function ContainsKey (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class)
                           return Java.Boolean;

   function AddAll (This : access Typ;
                    P1_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                    return Java.Boolean;

   function Size (This : access Typ)
                  return Java.Int;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   procedure Clear (This : access Typ);

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HashAttributeSet);
   pragma Import (Java, Get, "get");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, AddAll, "addAll");
   pragma Import (Java, Size, "size");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Print.Attribute.HashAttributeSet;
pragma Import (Java, Javax.Print.Attribute.HashAttributeSet, "javax.print.attribute.HashAttributeSet");
pragma Extensions_Allowed (Off);
