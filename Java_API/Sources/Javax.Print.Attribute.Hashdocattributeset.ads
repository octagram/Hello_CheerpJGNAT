pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.DocAttribute;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Attribute.AttributeSet;
with Javax.Print.Attribute.DocAttributeSet;
with Javax.Print.Attribute.HashAttributeSet;

package Javax.Print.Attribute.HashDocAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AttributeSet_I : Javax.Print.Attribute.AttributeSet.Ref;
            DocAttributeSet_I : Javax.Print.Attribute.DocAttributeSet.Ref)
    is new Javax.Print.Attribute.HashAttributeSet.Typ(Serializable_I,
                                                      AttributeSet_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HashDocAttributeSet (This : Ref := null)
                                     return Ref;

   function New_HashDocAttributeSet (P1_DocAttribute : access Standard.Javax.Print.Attribute.DocAttribute.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_HashDocAttributeSet (P1_DocAttribute_Arr : access Javax.Print.Attribute.DocAttribute.Arr_Obj; 
                                     This : Ref := null)
                                     return Ref;

   function New_HashDocAttributeSet (P1_DocAttributeSet : access Standard.Javax.Print.Attribute.DocAttributeSet.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HashDocAttributeSet);

end Javax.Print.Attribute.HashDocAttributeSet;
pragma Import (Java, Javax.Print.Attribute.HashDocAttributeSet, "javax.print.attribute.HashDocAttributeSet");
pragma Extensions_Allowed (Off);
