pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.PrintJobAttribute;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Attribute.AttributeSet;
with Javax.Print.Attribute.HashAttributeSet;
with Javax.Print.Attribute.PrintJobAttributeSet;

package Javax.Print.Attribute.HashPrintJobAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AttributeSet_I : Javax.Print.Attribute.AttributeSet.Ref;
            PrintJobAttributeSet_I : Javax.Print.Attribute.PrintJobAttributeSet.Ref)
    is new Javax.Print.Attribute.HashAttributeSet.Typ(Serializable_I,
                                                      AttributeSet_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HashPrintJobAttributeSet (This : Ref := null)
                                          return Ref;

   function New_HashPrintJobAttributeSet (P1_PrintJobAttribute : access Standard.Javax.Print.Attribute.PrintJobAttribute.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   function New_HashPrintJobAttributeSet (P1_PrintJobAttribute_Arr : access Javax.Print.Attribute.PrintJobAttribute.Arr_Obj; 
                                          This : Ref := null)
                                          return Ref;

   function New_HashPrintJobAttributeSet (P1_PrintJobAttributeSet : access Standard.Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HashPrintJobAttributeSet);

end Javax.Print.Attribute.HashPrintJobAttributeSet;
pragma Import (Java, Javax.Print.Attribute.HashPrintJobAttributeSet, "javax.print.attribute.HashPrintJobAttributeSet");
pragma Extensions_Allowed (Off);
