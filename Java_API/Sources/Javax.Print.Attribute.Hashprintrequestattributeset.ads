pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.PrintRequestAttribute;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Attribute.AttributeSet;
with Javax.Print.Attribute.HashAttributeSet;
with Javax.Print.Attribute.PrintRequestAttributeSet;

package Javax.Print.Attribute.HashPrintRequestAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AttributeSet_I : Javax.Print.Attribute.AttributeSet.Ref;
            PrintRequestAttributeSet_I : Javax.Print.Attribute.PrintRequestAttributeSet.Ref)
    is new Javax.Print.Attribute.HashAttributeSet.Typ(Serializable_I,
                                                      AttributeSet_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HashPrintRequestAttributeSet (This : Ref := null)
                                              return Ref;

   function New_HashPrintRequestAttributeSet (P1_PrintRequestAttribute : access Standard.Javax.Print.Attribute.PrintRequestAttribute.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   function New_HashPrintRequestAttributeSet (P1_PrintRequestAttribute_Arr : access Javax.Print.Attribute.PrintRequestAttribute.Arr_Obj; 
                                              This : Ref := null)
                                              return Ref;

   function New_HashPrintRequestAttributeSet (P1_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HashPrintRequestAttributeSet);

end Javax.Print.Attribute.HashPrintRequestAttributeSet;
pragma Import (Java, Javax.Print.Attribute.HashPrintRequestAttributeSet, "javax.print.attribute.HashPrintRequestAttributeSet");
pragma Extensions_Allowed (Off);
