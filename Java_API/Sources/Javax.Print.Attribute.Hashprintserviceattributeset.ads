pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.PrintServiceAttribute;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Attribute.AttributeSet;
with Javax.Print.Attribute.HashAttributeSet;
with Javax.Print.Attribute.PrintServiceAttributeSet;

package Javax.Print.Attribute.HashPrintServiceAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AttributeSet_I : Javax.Print.Attribute.AttributeSet.Ref;
            PrintServiceAttributeSet_I : Javax.Print.Attribute.PrintServiceAttributeSet.Ref)
    is new Javax.Print.Attribute.HashAttributeSet.Typ(Serializable_I,
                                                      AttributeSet_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HashPrintServiceAttributeSet (This : Ref := null)
                                              return Ref;

   function New_HashPrintServiceAttributeSet (P1_PrintServiceAttribute : access Standard.Javax.Print.Attribute.PrintServiceAttribute.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   function New_HashPrintServiceAttributeSet (P1_PrintServiceAttribute_Arr : access Javax.Print.Attribute.PrintServiceAttribute.Arr_Obj; 
                                              This : Ref := null)
                                              return Ref;

   function New_HashPrintServiceAttributeSet (P1_PrintServiceAttributeSet : access Standard.Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HashPrintServiceAttributeSet);

end Javax.Print.Attribute.HashPrintServiceAttributeSet;
pragma Import (Java, Javax.Print.Attribute.HashPrintServiceAttributeSet, "javax.print.attribute.HashPrintServiceAttributeSet");
pragma Extensions_Allowed (Off);
