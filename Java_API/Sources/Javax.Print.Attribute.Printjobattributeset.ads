pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.Attribute;
with Java.Lang.Object;
with Javax.Print.Attribute.AttributeSet;

package Javax.Print.Attribute.PrintJobAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AttributeSet_I : Javax.Print.Attribute.AttributeSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class)
                 return Java.Boolean is abstract;

   function AddAll (This : access Typ;
                    P1_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                    return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Add, "add");
   pragma Export (Java, AddAll, "addAll");

end Javax.Print.Attribute.PrintJobAttributeSet;
pragma Import (Java, Javax.Print.Attribute.PrintJobAttributeSet, "javax.print.attribute.PrintJobAttributeSet");
pragma Extensions_Allowed (Off);
