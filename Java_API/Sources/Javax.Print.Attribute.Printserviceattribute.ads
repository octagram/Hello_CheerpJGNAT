pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Print.Attribute.Attribute;

package Javax.Print.Attribute.PrintServiceAttribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Attribute_I : Javax.Print.Attribute.Attribute.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Javax.Print.Attribute.PrintServiceAttribute;
pragma Import (Java, Javax.Print.Attribute.PrintServiceAttribute, "javax.print.attribute.PrintServiceAttribute");
pragma Extensions_Allowed (Off);
