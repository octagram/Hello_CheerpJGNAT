pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Print.Attribute.ResolutionSyntax is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ResolutionSyntax (P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetResolution (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int_Arr;

   function GetCrossFeedResolution (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Java.Int;

   function GetFeedResolution (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   function ToString (This : access Typ;
                      P1_Int : Java.Int;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function LessThanOrEquals (This : access Typ;
                              P1_ResolutionSyntax : access Standard.Javax.Print.Attribute.ResolutionSyntax.Typ'Class)
                              return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   function GetCrossFeedResolutionDphi (This : access Typ)
                                        return Java.Int;

   --  protected
   function GetFeedResolutionDphi (This : access Typ)
                                   return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DPI : constant Java.Int;

   --  final
   DPCM : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ResolutionSyntax);
   pragma Import (Java, GetResolution, "getResolution");
   pragma Import (Java, GetCrossFeedResolution, "getCrossFeedResolution");
   pragma Import (Java, GetFeedResolution, "getFeedResolution");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, LessThanOrEquals, "lessThanOrEquals");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, GetCrossFeedResolutionDphi, "getCrossFeedResolutionDphi");
   pragma Import (Java, GetFeedResolutionDphi, "getFeedResolutionDphi");
   pragma Import (Java, DPI, "DPI");
   pragma Import (Java, DPCM, "DPCM");

end Javax.Print.Attribute.ResolutionSyntax;
pragma Import (Java, Javax.Print.Attribute.ResolutionSyntax, "javax.print.attribute.ResolutionSyntax");
pragma Extensions_Allowed (Off);
