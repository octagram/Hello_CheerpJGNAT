pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Print.Attribute.IntegerSyntax;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Print.Attribute.SetOfIntegerSyntax is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SetOfIntegerSyntax (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   --  protected
   function New_SetOfIntegerSyntax (P1_Int_Arr_2 : Java.Int_Arr_2; 
                                    This : Ref := null)
                                    return Ref;

   --  protected
   function New_SetOfIntegerSyntax (P1_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   --  protected
   function New_SetOfIntegerSyntax (P1_Int : Java.Int;
                                    P2_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMembers (This : access Typ)
                        return Java.Int_Arr_2;

   function Contains (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Boolean;

   function Contains (This : access Typ;
                      P1_IntegerSyntax : access Standard.Javax.Print.Attribute.IntegerSyntax.Typ'Class)
                      return Java.Boolean;

   function Next (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SetOfIntegerSyntax);
   pragma Import (Java, GetMembers, "getMembers");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Next, "next");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Print.Attribute.SetOfIntegerSyntax;
pragma Import (Java, Javax.Print.Attribute.SetOfIntegerSyntax, "javax.print.attribute.SetOfIntegerSyntax");
pragma Extensions_Allowed (Off);
