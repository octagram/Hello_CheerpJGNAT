pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Print.Attribute.Size2DSyntax is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Size2DSyntax (P1_Float : Java.Float;
                              P2_Float : Java.Float;
                              P3_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   --  protected
   function New_Size2DSyntax (P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSize (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Float_Arr;

   function GetX (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Float;

   function GetY (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Float;

   function ToString (This : access Typ;
                      P1_Int : Java.Int;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   function GetXMicrometers (This : access Typ)
                             return Java.Int;

   --  protected
   function GetYMicrometers (This : access Typ)
                             return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INCH : constant Java.Int;

   --  final
   MM : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Size2DSyntax);
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, GetXMicrometers, "getXMicrometers");
   pragma Import (Java, GetYMicrometers, "getYMicrometers");
   pragma Import (Java, INCH, "INCH");
   pragma Import (Java, MM, "MM");

end Javax.Print.Attribute.Size2DSyntax;
pragma Import (Java, Javax.Print.Attribute.Size2DSyntax, "javax.print.attribute.Size2DSyntax");
pragma Extensions_Allowed (Off);
