pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.DocAttribute;
with Javax.Print.Attribute.EnumSyntax;

package Javax.Print.Attribute.standard_C.Compression is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DocAttribute_I : Javax.Print.Attribute.DocAttribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Compression (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NONE : access Javax.Print.Attribute.standard_C.Compression.Typ'Class;

   --  final
   DEFLATE : access Javax.Print.Attribute.standard_C.Compression.Typ'Class;

   --  final
   GZIP : access Javax.Print.Attribute.standard_C.Compression.Typ'Class;

   --  final
   COMPRESS : access Javax.Print.Attribute.standard_C.Compression.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Compression);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, NONE, "NONE");
   pragma Import (Java, DEFLATE, "DEFLATE");
   pragma Import (Java, GZIP, "GZIP");
   pragma Import (Java, COMPRESS, "COMPRESS");

end Javax.Print.Attribute.standard_C.Compression;
pragma Import (Java, Javax.Print.Attribute.standard_C.Compression, "javax.print.attribute.standard.Compression");
pragma Extensions_Allowed (Off);
