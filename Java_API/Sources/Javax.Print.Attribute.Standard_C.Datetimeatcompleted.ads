pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Date;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.DateTimeSyntax;
with Javax.Print.Attribute.PrintJobAttribute;

package Javax.Print.Attribute.standard_C.DateTimeAtCompleted is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref)
    is new Javax.Print.Attribute.DateTimeSyntax.Typ(Serializable_I,
                                                    Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DateTimeAtCompleted (P1_Date : access Standard.Java.Util.Date.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DateTimeAtCompleted);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");

end Javax.Print.Attribute.standard_C.DateTimeAtCompleted;
pragma Import (Java, Javax.Print.Attribute.standard_C.DateTimeAtCompleted, "javax.print.attribute.standard.DateTimeAtCompleted");
pragma Extensions_Allowed (Off);
