pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.DocAttribute;
with Javax.Print.Attribute.EnumSyntax;
with Javax.Print.Attribute.PrintJobAttribute;
with Javax.Print.Attribute.PrintRequestAttribute;

package Javax.Print.Attribute.standard_C.Finishings is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DocAttribute_I : Javax.Print.Attribute.DocAttribute.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref;
            PrintRequestAttribute_I : Javax.Print.Attribute.PrintRequestAttribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Finishings (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetOffset (This : access Typ)
                       return Java.Int;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NONE : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   COVER : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   BIND : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   SADDLE_STITCH : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   EDGE_STITCH : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_TOP_LEFT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_BOTTOM_LEFT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_TOP_RIGHT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_BOTTOM_RIGHT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   EDGE_STITCH_LEFT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   EDGE_STITCH_TOP : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   EDGE_STITCH_RIGHT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   EDGE_STITCH_BOTTOM : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_DUAL_LEFT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_DUAL_TOP : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_DUAL_RIGHT : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;

   --  final
   STAPLE_DUAL_BOTTOM : access Javax.Print.Attribute.standard_C.Finishings.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Finishings);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, NONE, "NONE");
   pragma Import (Java, STAPLE, "STAPLE");
   pragma Import (Java, COVER, "COVER");
   pragma Import (Java, BIND, "BIND");
   pragma Import (Java, SADDLE_STITCH, "SADDLE_STITCH");
   pragma Import (Java, EDGE_STITCH, "EDGE_STITCH");
   pragma Import (Java, STAPLE_TOP_LEFT, "STAPLE_TOP_LEFT");
   pragma Import (Java, STAPLE_BOTTOM_LEFT, "STAPLE_BOTTOM_LEFT");
   pragma Import (Java, STAPLE_TOP_RIGHT, "STAPLE_TOP_RIGHT");
   pragma Import (Java, STAPLE_BOTTOM_RIGHT, "STAPLE_BOTTOM_RIGHT");
   pragma Import (Java, EDGE_STITCH_LEFT, "EDGE_STITCH_LEFT");
   pragma Import (Java, EDGE_STITCH_TOP, "EDGE_STITCH_TOP");
   pragma Import (Java, EDGE_STITCH_RIGHT, "EDGE_STITCH_RIGHT");
   pragma Import (Java, EDGE_STITCH_BOTTOM, "EDGE_STITCH_BOTTOM");
   pragma Import (Java, STAPLE_DUAL_LEFT, "STAPLE_DUAL_LEFT");
   pragma Import (Java, STAPLE_DUAL_TOP, "STAPLE_DUAL_TOP");
   pragma Import (Java, STAPLE_DUAL_RIGHT, "STAPLE_DUAL_RIGHT");
   pragma Import (Java, STAPLE_DUAL_BOTTOM, "STAPLE_DUAL_BOTTOM");

end Javax.Print.Attribute.standard_C.Finishings;
pragma Import (Java, Javax.Print.Attribute.standard_C.Finishings, "javax.print.attribute.standard.Finishings");
pragma Extensions_Allowed (Off);
