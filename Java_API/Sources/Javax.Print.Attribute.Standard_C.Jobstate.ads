pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.EnumSyntax;
with Javax.Print.Attribute.PrintJobAttribute;

package Javax.Print.Attribute.standard_C.JobState is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_JobState (P1_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNKNOWN : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;

   --  final
   PENDING : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;

   --  final
   PENDING_HELD : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;

   --  final
   PROCESSING : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;

   --  final
   PROCESSING_STOPPED : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;

   --  final
   CANCELED : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;

   --  final
   ABORTED : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;

   --  final
   COMPLETED : access Javax.Print.Attribute.standard_C.JobState.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JobState);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, UNKNOWN, "UNKNOWN");
   pragma Import (Java, PENDING, "PENDING");
   pragma Import (Java, PENDING_HELD, "PENDING_HELD");
   pragma Import (Java, PROCESSING, "PROCESSING");
   pragma Import (Java, PROCESSING_STOPPED, "PROCESSING_STOPPED");
   pragma Import (Java, CANCELED, "CANCELED");
   pragma Import (Java, ABORTED, "ABORTED");
   pragma Import (Java, COMPLETED, "COMPLETED");

end Javax.Print.Attribute.standard_C.JobState;
pragma Import (Java, Javax.Print.Attribute.standard_C.JobState, "javax.print.attribute.standard.JobState");
pragma Extensions_Allowed (Off);
