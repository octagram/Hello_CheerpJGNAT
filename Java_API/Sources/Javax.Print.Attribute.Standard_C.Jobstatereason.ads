pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.Attribute;
with Javax.Print.Attribute.EnumSyntax;

package Javax.Print.Attribute.standard_C.JobStateReason is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Attribute_I : Javax.Print.Attribute.Attribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_JobStateReason (P1_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JOB_INCOMING : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_DATA_INSUFFICIENT : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   DOCUMENT_ACCESS_ERROR : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   SUBMISSION_INTERRUPTED : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_OUTGOING : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_HOLD_UNTIL_SPECIFIED : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   RESOURCES_ARE_NOT_READY : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   PRINTER_STOPPED_PARTLY : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   PRINTER_STOPPED : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_INTERPRETING : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_QUEUED : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_TRANSFORMING : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_QUEUED_FOR_MARKER : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_PRINTING : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_CANCELED_BY_USER : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_CANCELED_BY_OPERATOR : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_CANCELED_AT_DEVICE : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   ABORTED_BY_SYSTEM : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   UNSUPPORTED_COMPRESSION : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   COMPRESSION_ERROR : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   UNSUPPORTED_DOCUMENT_FORMAT : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   DOCUMENT_FORMAT_ERROR : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   PROCESSING_TO_STOP_POINT : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   SERVICE_OFF_LINE : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_COMPLETED_SUCCESSFULLY : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_COMPLETED_WITH_WARNINGS : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_COMPLETED_WITH_ERRORS : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   JOB_RESTARTABLE : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;

   --  final
   QUEUED_IN_DEVICE : access Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JobStateReason);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, JOB_INCOMING, "JOB_INCOMING");
   pragma Import (Java, JOB_DATA_INSUFFICIENT, "JOB_DATA_INSUFFICIENT");
   pragma Import (Java, DOCUMENT_ACCESS_ERROR, "DOCUMENT_ACCESS_ERROR");
   pragma Import (Java, SUBMISSION_INTERRUPTED, "SUBMISSION_INTERRUPTED");
   pragma Import (Java, JOB_OUTGOING, "JOB_OUTGOING");
   pragma Import (Java, JOB_HOLD_UNTIL_SPECIFIED, "JOB_HOLD_UNTIL_SPECIFIED");
   pragma Import (Java, RESOURCES_ARE_NOT_READY, "RESOURCES_ARE_NOT_READY");
   pragma Import (Java, PRINTER_STOPPED_PARTLY, "PRINTER_STOPPED_PARTLY");
   pragma Import (Java, PRINTER_STOPPED, "PRINTER_STOPPED");
   pragma Import (Java, JOB_INTERPRETING, "JOB_INTERPRETING");
   pragma Import (Java, JOB_QUEUED, "JOB_QUEUED");
   pragma Import (Java, JOB_TRANSFORMING, "JOB_TRANSFORMING");
   pragma Import (Java, JOB_QUEUED_FOR_MARKER, "JOB_QUEUED_FOR_MARKER");
   pragma Import (Java, JOB_PRINTING, "JOB_PRINTING");
   pragma Import (Java, JOB_CANCELED_BY_USER, "JOB_CANCELED_BY_USER");
   pragma Import (Java, JOB_CANCELED_BY_OPERATOR, "JOB_CANCELED_BY_OPERATOR");
   pragma Import (Java, JOB_CANCELED_AT_DEVICE, "JOB_CANCELED_AT_DEVICE");
   pragma Import (Java, ABORTED_BY_SYSTEM, "ABORTED_BY_SYSTEM");
   pragma Import (Java, UNSUPPORTED_COMPRESSION, "UNSUPPORTED_COMPRESSION");
   pragma Import (Java, COMPRESSION_ERROR, "COMPRESSION_ERROR");
   pragma Import (Java, UNSUPPORTED_DOCUMENT_FORMAT, "UNSUPPORTED_DOCUMENT_FORMAT");
   pragma Import (Java, DOCUMENT_FORMAT_ERROR, "DOCUMENT_FORMAT_ERROR");
   pragma Import (Java, PROCESSING_TO_STOP_POINT, "PROCESSING_TO_STOP_POINT");
   pragma Import (Java, SERVICE_OFF_LINE, "SERVICE_OFF_LINE");
   pragma Import (Java, JOB_COMPLETED_SUCCESSFULLY, "JOB_COMPLETED_SUCCESSFULLY");
   pragma Import (Java, JOB_COMPLETED_WITH_WARNINGS, "JOB_COMPLETED_WITH_WARNINGS");
   pragma Import (Java, JOB_COMPLETED_WITH_ERRORS, "JOB_COMPLETED_WITH_ERRORS");
   pragma Import (Java, JOB_RESTARTABLE, "JOB_RESTARTABLE");
   pragma Import (Java, QUEUED_IN_DEVICE, "QUEUED_IN_DEVICE");

end Javax.Print.Attribute.standard_C.JobStateReason;
pragma Import (Java, Javax.Print.Attribute.standard_C.JobStateReason, "javax.print.attribute.standard.JobStateReason");
pragma Extensions_Allowed (Off);
