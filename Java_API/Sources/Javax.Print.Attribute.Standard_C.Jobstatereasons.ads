pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Print.Attribute.standard_C.JobStateReason;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Collection;
with Java.Util.HashSet;
with Java.Util.Set;
with Javax.Print.Attribute.PrintJobAttribute;

package Javax.Print.Attribute.standard_C.JobStateReasons is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Collection_I : Java.Util.Collection.Ref;
            Set_I : Java.Util.Set.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref)
    is new Java.Util.HashSet.Typ(Serializable_I,
                                 Cloneable_I,
                                 Collection_I,
                                 Set_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JobStateReasons (This : Ref := null)
                                 return Ref;

   function New_JobStateReasons (P1_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_JobStateReasons (P1_Int : Java.Int;
                                 P2_Float : Java.Float; 
                                 This : Ref := null)
                                 return Ref;

   function New_JobStateReasons (P1_Collection : access Standard.Java.Util.Collection.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_JobStateReason : access Standard.Javax.Print.Attribute.standard_C.JobStateReason.Typ'Class)
                 return Java.Boolean;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function Add (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JobStateReasons);
   pragma Import (Java, Add, "add");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");

end Javax.Print.Attribute.standard_C.JobStateReasons;
pragma Import (Java, Javax.Print.Attribute.standard_C.JobStateReasons, "javax.print.attribute.standard.JobStateReasons");
pragma Extensions_Allowed (Off);
