pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Print.Attribute.EnumSyntax;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.Attribute;
with Javax.Print.Attribute.DocAttribute;
with Javax.Print.Attribute.PrintJobAttribute;
with Javax.Print.Attribute.PrintRequestAttribute;
with Javax.Print.Attribute.standard_C.Media;

package Javax.Print.Attribute.standard_C.MediaName is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Attribute_I : Javax.Print.Attribute.Attribute.Ref;
            DocAttribute_I : Javax.Print.Attribute.DocAttribute.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref;
            PrintRequestAttribute_I : Javax.Print.Attribute.PrintRequestAttribute.Ref)
    is new Javax.Print.Attribute.standard_C.Media.Typ(Serializable_I,
                                                      Cloneable_I,
                                                      DocAttribute_I,
                                                      PrintJobAttribute_I,
                                                      PrintRequestAttribute_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_MediaName (P1_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NA_LETTER_WHITE : access Javax.Print.Attribute.standard_C.MediaName.Typ'Class;

   --  final
   NA_LETTER_TRANSPARENT : access Javax.Print.Attribute.standard_C.MediaName.Typ'Class;

   --  final
   ISO_A4_WHITE : access Javax.Print.Attribute.standard_C.MediaName.Typ'Class;

   --  final
   ISO_A4_TRANSPARENT : access Javax.Print.Attribute.standard_C.MediaName.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MediaName);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, NA_LETTER_WHITE, "NA_LETTER_WHITE");
   pragma Import (Java, NA_LETTER_TRANSPARENT, "NA_LETTER_TRANSPARENT");
   pragma Import (Java, ISO_A4_WHITE, "ISO_A4_WHITE");
   pragma Import (Java, ISO_A4_TRANSPARENT, "ISO_A4_TRANSPARENT");

end Javax.Print.Attribute.standard_C.MediaName;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaName, "javax.print.attribute.standard.MediaName");
pragma Extensions_Allowed (Off);
