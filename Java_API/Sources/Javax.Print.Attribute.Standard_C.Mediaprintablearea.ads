pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Print.Attribute.DocAttribute;
with Javax.Print.Attribute.PrintJobAttribute;
with Javax.Print.Attribute.PrintRequestAttribute;

package Javax.Print.Attribute.standard_C.MediaPrintableArea is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DocAttribute_I : Javax.Print.Attribute.DocAttribute.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref;
            PrintRequestAttribute_I : Javax.Print.Attribute.PrintRequestAttribute.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MediaPrintableArea (P1_Float : Java.Float;
                                    P2_Float : Java.Float;
                                    P3_Float : Java.Float;
                                    P4_Float : Java.Float;
                                    P5_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   function New_MediaPrintableArea (P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrintableArea (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float_Arr;

   function GetX (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Float;

   function GetY (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Float;

   function GetWidth (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Float;

   function GetHeight (This : access Typ;
                       P1_Int : Java.Int)
                       return Java.Float;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ;
                      P1_Int : Java.Int;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INCH : constant Java.Int;

   --  final
   MM : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MediaPrintableArea);
   pragma Import (Java, GetPrintableArea, "getPrintableArea");
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, INCH, "INCH");
   pragma Import (Java, MM, "MM");

end Javax.Print.Attribute.standard_C.MediaPrintableArea;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaPrintableArea, "javax.print.attribute.standard.MediaPrintableArea");
pragma Extensions_Allowed (Off);
