pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Print.Attribute.standard_C.MediaSize.Engineering is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   A : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   C : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   D : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   E : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, A, "A");
   pragma Import (Java, B, "B");
   pragma Import (Java, C, "C");
   pragma Import (Java, D, "D");
   pragma Import (Java, E, "E");

end Javax.Print.Attribute.standard_C.MediaSize.Engineering;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaSize.Engineering, "javax.print.attribute.standard.MediaSize$Engineering");
pragma Extensions_Allowed (Off);
