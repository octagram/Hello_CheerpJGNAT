pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Print.Attribute.standard_C.MediaSize.ISO is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   A0 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A1 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A2 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A3 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A5 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A6 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A7 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A8 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A9 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   A10 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B0 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B1 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B2 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B3 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B5 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B6 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B7 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B8 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B9 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B10 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   C3 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   C4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   C5 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   C6 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   DESIGNATED_LONG : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, A0, "A0");
   pragma Import (Java, A1, "A1");
   pragma Import (Java, A2, "A2");
   pragma Import (Java, A3, "A3");
   pragma Import (Java, A4, "A4");
   pragma Import (Java, A5, "A5");
   pragma Import (Java, A6, "A6");
   pragma Import (Java, A7, "A7");
   pragma Import (Java, A8, "A8");
   pragma Import (Java, A9, "A9");
   pragma Import (Java, A10, "A10");
   pragma Import (Java, B0, "B0");
   pragma Import (Java, B1, "B1");
   pragma Import (Java, B2, "B2");
   pragma Import (Java, B3, "B3");
   pragma Import (Java, B4, "B4");
   pragma Import (Java, B5, "B5");
   pragma Import (Java, B6, "B6");
   pragma Import (Java, B7, "B7");
   pragma Import (Java, B8, "B8");
   pragma Import (Java, B9, "B9");
   pragma Import (Java, B10, "B10");
   pragma Import (Java, C3, "C3");
   pragma Import (Java, C4, "C4");
   pragma Import (Java, C5, "C5");
   pragma Import (Java, C6, "C6");
   pragma Import (Java, DESIGNATED_LONG, "DESIGNATED_LONG");

end Javax.Print.Attribute.standard_C.MediaSize.ISO;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaSize.ISO, "javax.print.attribute.standard.MediaSize$ISO");
pragma Extensions_Allowed (Off);
