pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Print.Attribute.standard_C.MediaSize.JIS is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   B0 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B1 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B2 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B3 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B5 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B6 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B7 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B8 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B9 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   B10 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   CHOU_1 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   CHOU_2 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   CHOU_3 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   CHOU_4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   CHOU_30 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   CHOU_40 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_0 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_1 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_2 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_3 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_5 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_6 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_7 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_8 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_20 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   KAKU_A4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   YOU_1 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   YOU_2 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   YOU_3 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   YOU_4 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   YOU_5 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   YOU_6 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   YOU_7 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, B0, "B0");
   pragma Import (Java, B1, "B1");
   pragma Import (Java, B2, "B2");
   pragma Import (Java, B3, "B3");
   pragma Import (Java, B4, "B4");
   pragma Import (Java, B5, "B5");
   pragma Import (Java, B6, "B6");
   pragma Import (Java, B7, "B7");
   pragma Import (Java, B8, "B8");
   pragma Import (Java, B9, "B9");
   pragma Import (Java, B10, "B10");
   pragma Import (Java, CHOU_1, "CHOU_1");
   pragma Import (Java, CHOU_2, "CHOU_2");
   pragma Import (Java, CHOU_3, "CHOU_3");
   pragma Import (Java, CHOU_4, "CHOU_4");
   pragma Import (Java, CHOU_30, "CHOU_30");
   pragma Import (Java, CHOU_40, "CHOU_40");
   pragma Import (Java, KAKU_0, "KAKU_0");
   pragma Import (Java, KAKU_1, "KAKU_1");
   pragma Import (Java, KAKU_2, "KAKU_2");
   pragma Import (Java, KAKU_3, "KAKU_3");
   pragma Import (Java, KAKU_4, "KAKU_4");
   pragma Import (Java, KAKU_5, "KAKU_5");
   pragma Import (Java, KAKU_6, "KAKU_6");
   pragma Import (Java, KAKU_7, "KAKU_7");
   pragma Import (Java, KAKU_8, "KAKU_8");
   pragma Import (Java, KAKU_20, "KAKU_20");
   pragma Import (Java, KAKU_A4, "KAKU_A4");
   pragma Import (Java, YOU_1, "YOU_1");
   pragma Import (Java, YOU_2, "YOU_2");
   pragma Import (Java, YOU_3, "YOU_3");
   pragma Import (Java, YOU_4, "YOU_4");
   pragma Import (Java, YOU_5, "YOU_5");
   pragma Import (Java, YOU_6, "YOU_6");
   pragma Import (Java, YOU_7, "YOU_7");

end Javax.Print.Attribute.standard_C.MediaSize.JIS;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaSize.JIS, "javax.print.attribute.standard.MediaSize$JIS");
pragma Extensions_Allowed (Off);
