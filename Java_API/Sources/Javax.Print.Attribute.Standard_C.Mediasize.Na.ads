pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Print.Attribute.standard_C.MediaSize.NA is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LETTER : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   LEGAL : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_5X7 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_8X10 : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_NUMBER_9_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_NUMBER_10_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_NUMBER_11_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_NUMBER_12_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_NUMBER_14_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_6X9_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_7X9_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_9x11_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_9x12_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_10x13_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_10x14_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   NA_10X15_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, LETTER, "LETTER");
   pragma Import (Java, LEGAL, "LEGAL");
   pragma Import (Java, NA_5X7, "NA_5X7");
   pragma Import (Java, NA_8X10, "NA_8X10");
   pragma Import (Java, NA_NUMBER_9_ENVELOPE, "NA_NUMBER_9_ENVELOPE");
   pragma Import (Java, NA_NUMBER_10_ENVELOPE, "NA_NUMBER_10_ENVELOPE");
   pragma Import (Java, NA_NUMBER_11_ENVELOPE, "NA_NUMBER_11_ENVELOPE");
   pragma Import (Java, NA_NUMBER_12_ENVELOPE, "NA_NUMBER_12_ENVELOPE");
   pragma Import (Java, NA_NUMBER_14_ENVELOPE, "NA_NUMBER_14_ENVELOPE");
   pragma Import (Java, NA_6X9_ENVELOPE, "NA_6X9_ENVELOPE");
   pragma Import (Java, NA_7X9_ENVELOPE, "NA_7X9_ENVELOPE");
   pragma Import (Java, NA_9x11_ENVELOPE, "NA_9x11_ENVELOPE");
   pragma Import (Java, NA_9x12_ENVELOPE, "NA_9x12_ENVELOPE");
   pragma Import (Java, NA_10x13_ENVELOPE, "NA_10x13_ENVELOPE");
   pragma Import (Java, NA_10x14_ENVELOPE, "NA_10x14_ENVELOPE");
   pragma Import (Java, NA_10X15_ENVELOPE, "NA_10X15_ENVELOPE");

end Javax.Print.Attribute.standard_C.MediaSize.NA;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaSize.NA, "javax.print.attribute.standard.MediaSize$NA");
pragma Extensions_Allowed (Off);
