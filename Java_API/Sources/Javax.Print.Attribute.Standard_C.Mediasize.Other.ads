pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Print.Attribute.standard_C.MediaSize.Other is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   EXECUTIVE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   LEDGER : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   TABLOID : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   INVOICE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   FOLIO : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   QUARTO : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   ITALY_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   MONARCH_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   PERSONAL_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   JAPANESE_POSTCARD : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   --  final
   JAPANESE_DOUBLE_POSTCARD : access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, EXECUTIVE, "EXECUTIVE");
   pragma Import (Java, LEDGER, "LEDGER");
   pragma Import (Java, TABLOID, "TABLOID");
   pragma Import (Java, INVOICE, "INVOICE");
   pragma Import (Java, FOLIO, "FOLIO");
   pragma Import (Java, QUARTO, "QUARTO");
   pragma Import (Java, ITALY_ENVELOPE, "ITALY_ENVELOPE");
   pragma Import (Java, MONARCH_ENVELOPE, "MONARCH_ENVELOPE");
   pragma Import (Java, PERSONAL_ENVELOPE, "PERSONAL_ENVELOPE");
   pragma Import (Java, JAPANESE_POSTCARD, "JAPANESE_POSTCARD");
   pragma Import (Java, JAPANESE_DOUBLE_POSTCARD, "JAPANESE_DOUBLE_POSTCARD");

end Javax.Print.Attribute.standard_C.MediaSize.Other;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaSize.Other, "javax.print.attribute.standard.MediaSize$Other");
pragma Extensions_Allowed (Off);
