pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Print.Attribute.standard_C.MediaSizeName;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.Attribute;
with Javax.Print.Attribute.Size2DSyntax;

package Javax.Print.Attribute.standard_C.MediaSize is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Attribute_I : Javax.Print.Attribute.Attribute.Ref)
    is new Javax.Print.Attribute.Size2DSyntax.Typ(Serializable_I,
                                                  Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MediaSize (P1_Float : Java.Float;
                           P2_Float : Java.Float;
                           P3_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_MediaSize (P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_MediaSize (P1_Float : Java.Float;
                           P2_Float : Java.Float;
                           P3_Int : Java.Int;
                           P4_MediaSizeName : access Standard.Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_MediaSize (P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int;
                           P4_MediaSizeName : access Standard.Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMediaSizeName (This : access Typ)
                              return access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   function GetMediaSizeForName (P1_MediaSizeName : access Standard.Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class)
                                 return access Javax.Print.Attribute.standard_C.MediaSize.Typ'Class;

   function FindMedia (P1_Float : Java.Float;
                       P2_Float : Java.Float;
                       P3_Int : Java.Int)
                       return access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MediaSize);
   pragma Import (Java, GetMediaSizeName, "getMediaSizeName");
   pragma Import (Java, GetMediaSizeForName, "getMediaSizeForName");
   pragma Import (Java, FindMedia, "findMedia");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");

end Javax.Print.Attribute.standard_C.MediaSize;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaSize, "javax.print.attribute.standard.MediaSize");
pragma Extensions_Allowed (Off);
