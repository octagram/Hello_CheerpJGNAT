pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Print.Attribute.EnumSyntax;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.DocAttribute;
with Javax.Print.Attribute.PrintJobAttribute;
with Javax.Print.Attribute.PrintRequestAttribute;
with Javax.Print.Attribute.standard_C.Media;

package Javax.Print.Attribute.standard_C.MediaSizeName is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            DocAttribute_I : Javax.Print.Attribute.DocAttribute.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref;
            PrintRequestAttribute_I : Javax.Print.Attribute.PrintRequestAttribute.Ref)
    is new Javax.Print.Attribute.standard_C.Media.Typ(Serializable_I,
                                                      Cloneable_I,
                                                      DocAttribute_I,
                                                      PrintJobAttribute_I,
                                                      PrintRequestAttribute_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_MediaSizeName (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ISO_A0 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A1 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A2 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A3 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A4 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A5 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A6 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A7 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A8 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A9 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_A10 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B0 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B1 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B2 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B3 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B4 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B5 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B6 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B7 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B8 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B9 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_B10 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B0 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B1 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B2 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B3 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B4 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B5 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B6 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B7 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B8 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B9 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JIS_B10 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_C0 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_C1 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_C2 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_C3 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_C4 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_C5 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_C6 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_LETTER : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_LEGAL : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   EXECUTIVE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   LEDGER : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   TABLOID : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   INVOICE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   FOLIO : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   QUARTO : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JAPANESE_POSTCARD : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   JAPANESE_DOUBLE_POSTCARD : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   A : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   B : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   C : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   D : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   E : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ISO_DESIGNATED_LONG : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   ITALY_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   MONARCH_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   PERSONAL_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_NUMBER_9_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_NUMBER_10_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_NUMBER_11_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_NUMBER_12_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_NUMBER_14_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_6X9_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_7X9_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_9X11_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_9X12_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_10X13_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_10X14_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_10X15_ENVELOPE : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_5X7 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;

   --  final
   NA_8X10 : access Javax.Print.Attribute.standard_C.MediaSizeName.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MediaSizeName);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, ISO_A0, "ISO_A0");
   pragma Import (Java, ISO_A1, "ISO_A1");
   pragma Import (Java, ISO_A2, "ISO_A2");
   pragma Import (Java, ISO_A3, "ISO_A3");
   pragma Import (Java, ISO_A4, "ISO_A4");
   pragma Import (Java, ISO_A5, "ISO_A5");
   pragma Import (Java, ISO_A6, "ISO_A6");
   pragma Import (Java, ISO_A7, "ISO_A7");
   pragma Import (Java, ISO_A8, "ISO_A8");
   pragma Import (Java, ISO_A9, "ISO_A9");
   pragma Import (Java, ISO_A10, "ISO_A10");
   pragma Import (Java, ISO_B0, "ISO_B0");
   pragma Import (Java, ISO_B1, "ISO_B1");
   pragma Import (Java, ISO_B2, "ISO_B2");
   pragma Import (Java, ISO_B3, "ISO_B3");
   pragma Import (Java, ISO_B4, "ISO_B4");
   pragma Import (Java, ISO_B5, "ISO_B5");
   pragma Import (Java, ISO_B6, "ISO_B6");
   pragma Import (Java, ISO_B7, "ISO_B7");
   pragma Import (Java, ISO_B8, "ISO_B8");
   pragma Import (Java, ISO_B9, "ISO_B9");
   pragma Import (Java, ISO_B10, "ISO_B10");
   pragma Import (Java, JIS_B0, "JIS_B0");
   pragma Import (Java, JIS_B1, "JIS_B1");
   pragma Import (Java, JIS_B2, "JIS_B2");
   pragma Import (Java, JIS_B3, "JIS_B3");
   pragma Import (Java, JIS_B4, "JIS_B4");
   pragma Import (Java, JIS_B5, "JIS_B5");
   pragma Import (Java, JIS_B6, "JIS_B6");
   pragma Import (Java, JIS_B7, "JIS_B7");
   pragma Import (Java, JIS_B8, "JIS_B8");
   pragma Import (Java, JIS_B9, "JIS_B9");
   pragma Import (Java, JIS_B10, "JIS_B10");
   pragma Import (Java, ISO_C0, "ISO_C0");
   pragma Import (Java, ISO_C1, "ISO_C1");
   pragma Import (Java, ISO_C2, "ISO_C2");
   pragma Import (Java, ISO_C3, "ISO_C3");
   pragma Import (Java, ISO_C4, "ISO_C4");
   pragma Import (Java, ISO_C5, "ISO_C5");
   pragma Import (Java, ISO_C6, "ISO_C6");
   pragma Import (Java, NA_LETTER, "NA_LETTER");
   pragma Import (Java, NA_LEGAL, "NA_LEGAL");
   pragma Import (Java, EXECUTIVE, "EXECUTIVE");
   pragma Import (Java, LEDGER, "LEDGER");
   pragma Import (Java, TABLOID, "TABLOID");
   pragma Import (Java, INVOICE, "INVOICE");
   pragma Import (Java, FOLIO, "FOLIO");
   pragma Import (Java, QUARTO, "QUARTO");
   pragma Import (Java, JAPANESE_POSTCARD, "JAPANESE_POSTCARD");
   pragma Import (Java, JAPANESE_DOUBLE_POSTCARD, "JAPANESE_DOUBLE_POSTCARD");
   pragma Import (Java, A, "A");
   pragma Import (Java, B, "B");
   pragma Import (Java, C, "C");
   pragma Import (Java, D, "D");
   pragma Import (Java, E, "E");
   pragma Import (Java, ISO_DESIGNATED_LONG, "ISO_DESIGNATED_LONG");
   pragma Import (Java, ITALY_ENVELOPE, "ITALY_ENVELOPE");
   pragma Import (Java, MONARCH_ENVELOPE, "MONARCH_ENVELOPE");
   pragma Import (Java, PERSONAL_ENVELOPE, "PERSONAL_ENVELOPE");
   pragma Import (Java, NA_NUMBER_9_ENVELOPE, "NA_NUMBER_9_ENVELOPE");
   pragma Import (Java, NA_NUMBER_10_ENVELOPE, "NA_NUMBER_10_ENVELOPE");
   pragma Import (Java, NA_NUMBER_11_ENVELOPE, "NA_NUMBER_11_ENVELOPE");
   pragma Import (Java, NA_NUMBER_12_ENVELOPE, "NA_NUMBER_12_ENVELOPE");
   pragma Import (Java, NA_NUMBER_14_ENVELOPE, "NA_NUMBER_14_ENVELOPE");
   pragma Import (Java, NA_6X9_ENVELOPE, "NA_6X9_ENVELOPE");
   pragma Import (Java, NA_7X9_ENVELOPE, "NA_7X9_ENVELOPE");
   pragma Import (Java, NA_9X11_ENVELOPE, "NA_9X11_ENVELOPE");
   pragma Import (Java, NA_9X12_ENVELOPE, "NA_9X12_ENVELOPE");
   pragma Import (Java, NA_10X13_ENVELOPE, "NA_10X13_ENVELOPE");
   pragma Import (Java, NA_10X14_ENVELOPE, "NA_10X14_ENVELOPE");
   pragma Import (Java, NA_10X15_ENVELOPE, "NA_10X15_ENVELOPE");
   pragma Import (Java, NA_5X7, "NA_5X7");
   pragma Import (Java, NA_8X10, "NA_8X10");

end Javax.Print.Attribute.standard_C.MediaSizeName;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaSizeName, "javax.print.attribute.standard.MediaSizeName");
pragma Extensions_Allowed (Off);
