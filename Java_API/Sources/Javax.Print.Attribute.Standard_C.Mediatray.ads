pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Print.Attribute.EnumSyntax;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.Attribute;
with Javax.Print.Attribute.DocAttribute;
with Javax.Print.Attribute.PrintJobAttribute;
with Javax.Print.Attribute.PrintRequestAttribute;
with Javax.Print.Attribute.standard_C.Media;

package Javax.Print.Attribute.standard_C.MediaTray is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Attribute_I : Javax.Print.Attribute.Attribute.Ref;
            DocAttribute_I : Javax.Print.Attribute.DocAttribute.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref;
            PrintRequestAttribute_I : Javax.Print.Attribute.PrintRequestAttribute.Ref)
    is new Javax.Print.Attribute.standard_C.Media.Typ(Serializable_I,
                                                      Cloneable_I,
                                                      DocAttribute_I,
                                                      PrintJobAttribute_I,
                                                      PrintRequestAttribute_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_MediaTray (P1_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TOP : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;

   --  final
   MIDDLE : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;

   --  final
   BOTTOM : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;

   --  final
   ENVELOPE : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;

   --  final
   MANUAL : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;

   --  final
   LARGE_CAPACITY : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;

   --  final
   MAIN : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;

   --  final
   SIDE : access Javax.Print.Attribute.standard_C.MediaTray.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MediaTray);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, TOP, "TOP");
   pragma Import (Java, MIDDLE, "MIDDLE");
   pragma Import (Java, BOTTOM, "BOTTOM");
   pragma Import (Java, ENVELOPE, "ENVELOPE");
   pragma Import (Java, MANUAL, "MANUAL");
   pragma Import (Java, LARGE_CAPACITY, "LARGE_CAPACITY");
   pragma Import (Java, MAIN, "MAIN");
   pragma Import (Java, SIDE, "SIDE");

end Javax.Print.Attribute.standard_C.MediaTray;
pragma Import (Java, Javax.Print.Attribute.standard_C.MediaTray, "javax.print.attribute.standard.MediaTray");
pragma Extensions_Allowed (Off);
