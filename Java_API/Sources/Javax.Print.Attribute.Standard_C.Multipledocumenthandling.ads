pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.EnumSyntax;
with Javax.Print.Attribute.PrintJobAttribute;
with Javax.Print.Attribute.PrintRequestAttribute;

package Javax.Print.Attribute.standard_C.MultipleDocumentHandling is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref;
            PrintRequestAttribute_I : Javax.Print.Attribute.PrintRequestAttribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_MultipleDocumentHandling (P1_Int : Java.Int; 
                                          This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SINGLE_DOCUMENT : access Javax.Print.Attribute.standard_C.MultipleDocumentHandling.Typ'Class;

   --  final
   SEPARATE_DOCUMENTS_UNCOLLATED_COPIES : access Javax.Print.Attribute.standard_C.MultipleDocumentHandling.Typ'Class;

   --  final
   SEPARATE_DOCUMENTS_COLLATED_COPIES : access Javax.Print.Attribute.standard_C.MultipleDocumentHandling.Typ'Class;

   --  final
   SINGLE_DOCUMENT_NEW_SHEET : access Javax.Print.Attribute.standard_C.MultipleDocumentHandling.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MultipleDocumentHandling);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SINGLE_DOCUMENT, "SINGLE_DOCUMENT");
   pragma Import (Java, SEPARATE_DOCUMENTS_UNCOLLATED_COPIES, "SEPARATE_DOCUMENTS_UNCOLLATED_COPIES");
   pragma Import (Java, SEPARATE_DOCUMENTS_COLLATED_COPIES, "SEPARATE_DOCUMENTS_COLLATED_COPIES");
   pragma Import (Java, SINGLE_DOCUMENT_NEW_SHEET, "SINGLE_DOCUMENT_NEW_SHEET");

end Javax.Print.Attribute.standard_C.MultipleDocumentHandling;
pragma Import (Java, Javax.Print.Attribute.standard_C.MultipleDocumentHandling, "javax.print.attribute.standard.MultipleDocumentHandling");
pragma Extensions_Allowed (Off);
