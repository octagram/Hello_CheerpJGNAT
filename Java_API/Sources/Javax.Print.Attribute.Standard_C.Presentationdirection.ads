pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.EnumSyntax;
with Javax.Print.Attribute.PrintJobAttribute;
with Javax.Print.Attribute.PrintRequestAttribute;

package Javax.Print.Attribute.standard_C.PresentationDirection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            PrintJobAttribute_I : Javax.Print.Attribute.PrintJobAttribute.Ref;
            PrintRequestAttribute_I : Javax.Print.Attribute.PrintRequestAttribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TOBOTTOM_TORIGHT : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;

   --  final
   TOBOTTOM_TOLEFT : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;

   --  final
   TOTOP_TORIGHT : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;

   --  final
   TOTOP_TOLEFT : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;

   --  final
   TORIGHT_TOBOTTOM : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;

   --  final
   TORIGHT_TOTOP : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;

   --  final
   TOLEFT_TOBOTTOM : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;

   --  final
   TOLEFT_TOTOP : access Javax.Print.Attribute.standard_C.PresentationDirection.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, TOBOTTOM_TORIGHT, "TOBOTTOM_TORIGHT");
   pragma Import (Java, TOBOTTOM_TOLEFT, "TOBOTTOM_TOLEFT");
   pragma Import (Java, TOTOP_TORIGHT, "TOTOP_TORIGHT");
   pragma Import (Java, TOTOP_TOLEFT, "TOTOP_TOLEFT");
   pragma Import (Java, TORIGHT_TOBOTTOM, "TORIGHT_TOBOTTOM");
   pragma Import (Java, TORIGHT_TOTOP, "TORIGHT_TOTOP");
   pragma Import (Java, TOLEFT_TOBOTTOM, "TOLEFT_TOBOTTOM");
   pragma Import (Java, TOLEFT_TOTOP, "TOLEFT_TOTOP");

end Javax.Print.Attribute.standard_C.PresentationDirection;
pragma Import (Java, Javax.Print.Attribute.standard_C.PresentationDirection, "javax.print.attribute.standard.PresentationDirection");
pragma Extensions_Allowed (Off);
