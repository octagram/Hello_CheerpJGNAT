pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.EnumSyntax;
with Javax.Print.Attribute.PrintServiceAttribute;

package Javax.Print.Attribute.standard_C.PrinterState is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            PrintServiceAttribute_I : Javax.Print.Attribute.PrintServiceAttribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_PrinterState (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNKNOWN : access Javax.Print.Attribute.standard_C.PrinterState.Typ'Class;

   --  final
   IDLE : access Javax.Print.Attribute.standard_C.PrinterState.Typ'Class;

   --  final
   PROCESSING : access Javax.Print.Attribute.standard_C.PrinterState.Typ'Class;

   --  final
   STOPPED : access Javax.Print.Attribute.standard_C.PrinterState.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrinterState);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, UNKNOWN, "UNKNOWN");
   pragma Import (Java, IDLE, "IDLE");
   pragma Import (Java, PROCESSING, "PROCESSING");
   pragma Import (Java, STOPPED, "STOPPED");

end Javax.Print.Attribute.standard_C.PrinterState;
pragma Import (Java, Javax.Print.Attribute.standard_C.PrinterState, "javax.print.attribute.standard.PrinterState");
pragma Extensions_Allowed (Off);
