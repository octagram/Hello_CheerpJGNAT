pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.Attribute;
with Javax.Print.Attribute.EnumSyntax;

package Javax.Print.Attribute.standard_C.PrinterStateReason is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Attribute_I : Javax.Print.Attribute.Attribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_PrinterStateReason (P1_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OTHER : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MEDIA_NEEDED : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MEDIA_JAM : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MOVING_TO_PAUSED : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   PAUSED : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   SHUTDOWN : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   CONNECTING_TO_DEVICE : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   TIMED_OUT : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   STOPPING : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   STOPPED_PARTLY : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   TONER_LOW : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   TONER_EMPTY : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   SPOOL_AREA_FULL : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   COVER_OPEN : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   INTERLOCK_OPEN : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   DOOR_OPEN : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   INPUT_TRAY_MISSING : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MEDIA_LOW : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MEDIA_EMPTY : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   OUTPUT_TRAY_MISSING : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   OUTPUT_AREA_ALMOST_FULL : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   OUTPUT_AREA_FULL : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MARKER_SUPPLY_LOW : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MARKER_SUPPLY_EMPTY : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MARKER_WASTE_ALMOST_FULL : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   MARKER_WASTE_FULL : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   FUSER_OVER_TEMP : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   FUSER_UNDER_TEMP : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   OPC_NEAR_EOL : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   OPC_LIFE_OVER : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   DEVELOPER_LOW : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   DEVELOPER_EMPTY : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;

   --  final
   INTERPRETER_RESOURCE_UNAVAILABLE : access Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrinterStateReason);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, OTHER, "OTHER");
   pragma Import (Java, MEDIA_NEEDED, "MEDIA_NEEDED");
   pragma Import (Java, MEDIA_JAM, "MEDIA_JAM");
   pragma Import (Java, MOVING_TO_PAUSED, "MOVING_TO_PAUSED");
   pragma Import (Java, PAUSED, "PAUSED");
   pragma Import (Java, SHUTDOWN, "SHUTDOWN");
   pragma Import (Java, CONNECTING_TO_DEVICE, "CONNECTING_TO_DEVICE");
   pragma Import (Java, TIMED_OUT, "TIMED_OUT");
   pragma Import (Java, STOPPING, "STOPPING");
   pragma Import (Java, STOPPED_PARTLY, "STOPPED_PARTLY");
   pragma Import (Java, TONER_LOW, "TONER_LOW");
   pragma Import (Java, TONER_EMPTY, "TONER_EMPTY");
   pragma Import (Java, SPOOL_AREA_FULL, "SPOOL_AREA_FULL");
   pragma Import (Java, COVER_OPEN, "COVER_OPEN");
   pragma Import (Java, INTERLOCK_OPEN, "INTERLOCK_OPEN");
   pragma Import (Java, DOOR_OPEN, "DOOR_OPEN");
   pragma Import (Java, INPUT_TRAY_MISSING, "INPUT_TRAY_MISSING");
   pragma Import (Java, MEDIA_LOW, "MEDIA_LOW");
   pragma Import (Java, MEDIA_EMPTY, "MEDIA_EMPTY");
   pragma Import (Java, OUTPUT_TRAY_MISSING, "OUTPUT_TRAY_MISSING");
   pragma Import (Java, OUTPUT_AREA_ALMOST_FULL, "OUTPUT_AREA_ALMOST_FULL");
   pragma Import (Java, OUTPUT_AREA_FULL, "OUTPUT_AREA_FULL");
   pragma Import (Java, MARKER_SUPPLY_LOW, "MARKER_SUPPLY_LOW");
   pragma Import (Java, MARKER_SUPPLY_EMPTY, "MARKER_SUPPLY_EMPTY");
   pragma Import (Java, MARKER_WASTE_ALMOST_FULL, "MARKER_WASTE_ALMOST_FULL");
   pragma Import (Java, MARKER_WASTE_FULL, "MARKER_WASTE_FULL");
   pragma Import (Java, FUSER_OVER_TEMP, "FUSER_OVER_TEMP");
   pragma Import (Java, FUSER_UNDER_TEMP, "FUSER_UNDER_TEMP");
   pragma Import (Java, OPC_NEAR_EOL, "OPC_NEAR_EOL");
   pragma Import (Java, OPC_LIFE_OVER, "OPC_LIFE_OVER");
   pragma Import (Java, DEVELOPER_LOW, "DEVELOPER_LOW");
   pragma Import (Java, DEVELOPER_EMPTY, "DEVELOPER_EMPTY");
   pragma Import (Java, INTERPRETER_RESOURCE_UNAVAILABLE, "INTERPRETER_RESOURCE_UNAVAILABLE");

end Javax.Print.Attribute.standard_C.PrinterStateReason;
pragma Import (Java, Javax.Print.Attribute.standard_C.PrinterStateReason, "javax.print.attribute.standard.PrinterStateReason");
pragma Extensions_Allowed (Off);
