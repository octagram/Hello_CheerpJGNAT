pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Set;
limited with Javax.Print.Attribute.standard_C.PrinterStateReason;
limited with Javax.Print.Attribute.standard_C.Severity;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.HashMap;
with Java.Util.Map;
with Javax.Print.Attribute.PrintServiceAttribute;

package Javax.Print.Attribute.standard_C.PrinterStateReasons is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref;
            PrintServiceAttribute_I : Javax.Print.Attribute.PrintServiceAttribute.Ref)
    is new Java.Util.HashMap.Typ(Serializable_I,
                                 Cloneable_I,
                                 Map_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrinterStateReasons (This : Ref := null)
                                     return Ref;

   function New_PrinterStateReasons (P1_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   function New_PrinterStateReasons (P1_Int : Java.Int;
                                     P2_Float : Java.Float; 
                                     This : Ref := null)
                                     return Ref;

   function New_PrinterStateReasons (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Put (This : access Typ;
                 P1_PrinterStateReason : access Standard.Javax.Print.Attribute.standard_C.PrinterStateReason.Typ'Class;
                 P2_Severity : access Standard.Javax.Print.Attribute.standard_C.Severity.Typ'Class)
                 return access Javax.Print.Attribute.standard_C.Severity.Typ'Class;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function PrinterStateReasonSet (This : access Typ;
                                   P1_Severity : access Standard.Javax.Print.Attribute.standard_C.Severity.Typ'Class)
                                   return access Java.Util.Set.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrinterStateReasons);
   pragma Import (Java, Put, "put");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, PrinterStateReasonSet, "printerStateReasonSet");

end Javax.Print.Attribute.standard_C.PrinterStateReasons;
pragma Import (Java, Javax.Print.Attribute.standard_C.PrinterStateReasons, "javax.print.attribute.standard.PrinterStateReasons");
pragma Extensions_Allowed (Off);
