pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.Attribute.Attribute;
with Javax.Print.Attribute.EnumSyntax;

package Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Attribute_I : Javax.Print.Attribute.Attribute.Ref)
    is new Javax.Print.Attribute.EnumSyntax.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ReferenceUriSchemesSupported (P1_Int : Java.Int; 
                                              This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetStringTable (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetEnumValueTable (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  final
   function GetCategory (This : access Typ)
                         return access Java.Lang.Class.Typ'Class;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FTP : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;

   --  final
   HTTP : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;

   --  final
   HTTPS : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;

   --  final
   GOPHER : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;

   --  final
   NEWS : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;

   --  final
   NNTP : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;

   --  final
   WAIS : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;

   --  final
   FILE : access Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ReferenceUriSchemesSupported);
   pragma Import (Java, GetStringTable, "getStringTable");
   pragma Import (Java, GetEnumValueTable, "getEnumValueTable");
   pragma Import (Java, GetCategory, "getCategory");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, FTP, "FTP");
   pragma Import (Java, HTTP, "HTTP");
   pragma Import (Java, HTTPS, "HTTPS");
   pragma Import (Java, GOPHER, "GOPHER");
   pragma Import (Java, NEWS, "NEWS");
   pragma Import (Java, NNTP, "NNTP");
   pragma Import (Java, WAIS, "WAIS");
   pragma Import (Java, FILE, "FILE");

end Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported;
pragma Import (Java, Javax.Print.Attribute.standard_C.ReferenceUriSchemesSupported, "javax.print.attribute.standard.ReferenceUriSchemesSupported");
pragma Extensions_Allowed (Off);
