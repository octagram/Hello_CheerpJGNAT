pragma Extensions_Allowed (On);
package Javax.Print.Attribute is
   pragma Preelaborate;
end Javax.Print.Attribute;
pragma Import (Java, Javax.Print.Attribute, "javax.print.attribute");
pragma Extensions_Allowed (Off);
