pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Javax.Print.Attribute.Attribute;
with Java.Lang.Object;

package Javax.Print.AttributeException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUnsupportedAttributes (This : access Typ)
                                      return Standard.Java.Lang.Object.Ref is abstract;

   function GetUnsupportedValues (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetUnsupportedAttributes, "getUnsupportedAttributes");
   pragma Export (Java, GetUnsupportedValues, "getUnsupportedValues");

end Javax.Print.AttributeException;
pragma Import (Java, Javax.Print.AttributeException, "javax.print.AttributeException");
pragma Extensions_Allowed (Off);
