pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Javax.Print.Attribute.DocAttributeSet;
limited with Javax.Print.DocFlavor;
with Java.Lang.Object;

package Javax.Print.Doc is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDocFlavor (This : access Typ)
                          return access Javax.Print.DocFlavor.Typ'Class is abstract;

   function GetPrintData (This : access Typ)
                          return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetAttributes (This : access Typ)
                           return access Javax.Print.Attribute.DocAttributeSet.Typ'Class is abstract;

   function GetReaderForText (This : access Typ)
                              return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetStreamForBytes (This : access Typ)
                               return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDocFlavor, "getDocFlavor");
   pragma Export (Java, GetPrintData, "getPrintData");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetReaderForText, "getReaderForText");
   pragma Export (Java, GetStreamForBytes, "getStreamForBytes");

end Javax.Print.Doc;
pragma Import (Java, Javax.Print.Doc, "javax.print.Doc");
pragma Extensions_Allowed (Off);
