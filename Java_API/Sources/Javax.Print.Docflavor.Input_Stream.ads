pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.DocFlavor;

package Javax.Print.DocFlavor.INPUT_STREAM is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Print.DocFlavor.Typ(Serializable_I,
                                     Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_INPUT_STREAM (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TEXT_PLAIN_HOST : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_PLAIN_UTF_8 : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_PLAIN_UTF_16 : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_PLAIN_UTF_16BE : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_PLAIN_UTF_16LE : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_PLAIN_US_ASCII : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_HTML_HOST : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_HTML_UTF_8 : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_HTML_UTF_16 : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_HTML_UTF_16BE : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_HTML_UTF_16LE : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   TEXT_HTML_US_ASCII : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   PDF : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   POSTSCRIPT : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   PCL : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   GIF : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   JPEG : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   PNG : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;

   --  final
   AUTOSENSE : access Javax.Print.DocFlavor.INPUT_STREAM.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_INPUT_STREAM);
   pragma Import (Java, TEXT_PLAIN_HOST, "TEXT_PLAIN_HOST");
   pragma Import (Java, TEXT_PLAIN_UTF_8, "TEXT_PLAIN_UTF_8");
   pragma Import (Java, TEXT_PLAIN_UTF_16, "TEXT_PLAIN_UTF_16");
   pragma Import (Java, TEXT_PLAIN_UTF_16BE, "TEXT_PLAIN_UTF_16BE");
   pragma Import (Java, TEXT_PLAIN_UTF_16LE, "TEXT_PLAIN_UTF_16LE");
   pragma Import (Java, TEXT_PLAIN_US_ASCII, "TEXT_PLAIN_US_ASCII");
   pragma Import (Java, TEXT_HTML_HOST, "TEXT_HTML_HOST");
   pragma Import (Java, TEXT_HTML_UTF_8, "TEXT_HTML_UTF_8");
   pragma Import (Java, TEXT_HTML_UTF_16, "TEXT_HTML_UTF_16");
   pragma Import (Java, TEXT_HTML_UTF_16BE, "TEXT_HTML_UTF_16BE");
   pragma Import (Java, TEXT_HTML_UTF_16LE, "TEXT_HTML_UTF_16LE");
   pragma Import (Java, TEXT_HTML_US_ASCII, "TEXT_HTML_US_ASCII");
   pragma Import (Java, PDF, "PDF");
   pragma Import (Java, POSTSCRIPT, "POSTSCRIPT");
   pragma Import (Java, PCL, "PCL");
   pragma Import (Java, GIF, "GIF");
   pragma Import (Java, JPEG, "JPEG");
   pragma Import (Java, PNG, "PNG");
   pragma Import (Java, AUTOSENSE, "AUTOSENSE");

end Javax.Print.DocFlavor.INPUT_STREAM;
pragma Import (Java, Javax.Print.DocFlavor.INPUT_STREAM, "javax.print.DocFlavor$INPUT_STREAM");
pragma Extensions_Allowed (Off);
