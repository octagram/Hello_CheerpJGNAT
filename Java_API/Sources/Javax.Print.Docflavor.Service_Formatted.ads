pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Print.DocFlavor;

package Javax.Print.DocFlavor.SERVICE_FORMATTED is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Print.DocFlavor.Typ(Serializable_I,
                                     Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SERVICE_FORMATTED (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RENDERABLE_IMAGE : access Javax.Print.DocFlavor.SERVICE_FORMATTED.Typ'Class;

   --  final
   PRINTABLE : access Javax.Print.DocFlavor.SERVICE_FORMATTED.Typ'Class;

   --  final
   PAGEABLE : access Javax.Print.DocFlavor.SERVICE_FORMATTED.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SERVICE_FORMATTED);
   pragma Import (Java, RENDERABLE_IMAGE, "RENDERABLE_IMAGE");
   pragma Import (Java, PRINTABLE, "PRINTABLE");
   pragma Import (Java, PAGEABLE, "PAGEABLE");

end Javax.Print.DocFlavor.SERVICE_FORMATTED;
pragma Import (Java, Javax.Print.DocFlavor.SERVICE_FORMATTED, "javax.print.DocFlavor$SERVICE_FORMATTED");
pragma Extensions_Allowed (Off);
