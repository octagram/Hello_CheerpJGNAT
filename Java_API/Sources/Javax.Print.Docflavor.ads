pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Print.DocFlavor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DocFlavor (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMimeType (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetMediaType (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetMediaSubtype (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function GetRepresentationClassName (This : access Typ)
                                        return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   HostEncoding : access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DocFlavor);
   pragma Import (Java, GetMimeType, "getMimeType");
   pragma Import (Java, GetMediaType, "getMediaType");
   pragma Import (Java, GetMediaSubtype, "getMediaSubtype");
   pragma Import (Java, GetParameter, "getParameter");
   pragma Import (Java, GetRepresentationClassName, "getRepresentationClassName");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HostEncoding, "hostEncoding");

end Javax.Print.DocFlavor;
pragma Import (Java, Javax.Print.DocFlavor, "javax.print.DocFlavor");
pragma Extensions_Allowed (Off);
