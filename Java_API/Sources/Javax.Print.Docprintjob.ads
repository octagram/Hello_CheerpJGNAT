pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.PrintJobAttributeSet;
limited with Javax.Print.Attribute.PrintRequestAttributeSet;
limited with Javax.Print.Doc;
limited with Javax.Print.Event.PrintJobAttributeListener;
limited with Javax.Print.Event.PrintJobListener;
limited with Javax.Print.PrintService;
with Java.Lang.Object;

package Javax.Print.DocPrintJob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrintService (This : access Typ)
                             return access Javax.Print.PrintService.Typ'Class is abstract;

   function GetAttributes (This : access Typ)
                           return access Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class is abstract;

   procedure AddPrintJobListener (This : access Typ;
                                  P1_PrintJobListener : access Standard.Javax.Print.Event.PrintJobListener.Typ'Class) is abstract;

   procedure RemovePrintJobListener (This : access Typ;
                                     P1_PrintJobListener : access Standard.Javax.Print.Event.PrintJobListener.Typ'Class) is abstract;

   procedure AddPrintJobAttributeListener (This : access Typ;
                                           P1_PrintJobAttributeListener : access Standard.Javax.Print.Event.PrintJobAttributeListener.Typ'Class;
                                           P2_PrintJobAttributeSet : access Standard.Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class) is abstract;

   procedure RemovePrintJobAttributeListener (This : access Typ;
                                              P1_PrintJobAttributeListener : access Standard.Javax.Print.Event.PrintJobAttributeListener.Typ'Class) is abstract;

   procedure Print (This : access Typ;
                    P1_Doc : access Standard.Javax.Print.Doc.Typ'Class;
                    P2_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class) is abstract;
   --  can raise Javax.Print.PrintException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPrintService, "getPrintService");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, AddPrintJobListener, "addPrintJobListener");
   pragma Export (Java, RemovePrintJobListener, "removePrintJobListener");
   pragma Export (Java, AddPrintJobAttributeListener, "addPrintJobAttributeListener");
   pragma Export (Java, RemovePrintJobAttributeListener, "removePrintJobAttributeListener");
   pragma Export (Java, Print, "print");

end Javax.Print.DocPrintJob;
pragma Import (Java, Javax.Print.DocPrintJob, "javax.print.DocPrintJob");
pragma Extensions_Allowed (Off);
