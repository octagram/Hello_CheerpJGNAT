pragma Extensions_Allowed (On);
limited with Javax.Print.Event.PrintJobEvent;
with Java.Lang.Object;
with Javax.Print.Event.PrintJobListener;

package Javax.Print.Event.PrintJobAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PrintJobListener_I : Javax.Print.Event.PrintJobListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_PrintJobAdapter (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PrintDataTransferCompleted (This : access Typ;
                                         P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class);

   procedure PrintJobCompleted (This : access Typ;
                                P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class);

   procedure PrintJobFailed (This : access Typ;
                             P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class);

   procedure PrintJobCanceled (This : access Typ;
                               P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class);

   procedure PrintJobNoMoreEvents (This : access Typ;
                                   P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class);

   procedure PrintJobRequiresAttention (This : access Typ;
                                        P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintJobAdapter);
   pragma Import (Java, PrintDataTransferCompleted, "printDataTransferCompleted");
   pragma Import (Java, PrintJobCompleted, "printJobCompleted");
   pragma Import (Java, PrintJobFailed, "printJobFailed");
   pragma Import (Java, PrintJobCanceled, "printJobCanceled");
   pragma Import (Java, PrintJobNoMoreEvents, "printJobNoMoreEvents");
   pragma Import (Java, PrintJobRequiresAttention, "printJobRequiresAttention");

end Javax.Print.Event.PrintJobAdapter;
pragma Import (Java, Javax.Print.Event.PrintJobAdapter, "javax.print.event.PrintJobAdapter");
pragma Extensions_Allowed (Off);
