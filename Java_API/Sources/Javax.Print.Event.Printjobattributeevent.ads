pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.PrintJobAttributeSet;
limited with Javax.Print.DocPrintJob;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Event.PrintEvent;

package Javax.Print.Event.PrintJobAttributeEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Print.Event.PrintEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrintJobAttributeEvent (P1_DocPrintJob : access Standard.Javax.Print.DocPrintJob.Typ'Class;
                                        P2_PrintJobAttributeSet : access Standard.Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrintJob (This : access Typ)
                         return access Javax.Print.DocPrintJob.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Print.Attribute.PrintJobAttributeSet.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintJobAttributeEvent);
   pragma Import (Java, GetPrintJob, "getPrintJob");
   pragma Import (Java, GetAttributes, "getAttributes");

end Javax.Print.Event.PrintJobAttributeEvent;
pragma Import (Java, Javax.Print.Event.PrintJobAttributeEvent, "javax.print.event.PrintJobAttributeEvent");
pragma Extensions_Allowed (Off);
