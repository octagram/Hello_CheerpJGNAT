pragma Extensions_Allowed (On);
limited with Javax.Print.DocPrintJob;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Event.PrintEvent;

package Javax.Print.Event.PrintJobEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Print.Event.PrintEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrintJobEvent (P1_DocPrintJob : access Standard.Javax.Print.DocPrintJob.Typ'Class;
                               P2_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrintEventType (This : access Typ)
                               return Java.Int;

   function GetPrintJob (This : access Typ)
                         return access Javax.Print.DocPrintJob.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JOB_CANCELED : constant Java.Int;

   --  final
   JOB_COMPLETE : constant Java.Int;

   --  final
   JOB_FAILED : constant Java.Int;

   --  final
   REQUIRES_ATTENTION : constant Java.Int;

   --  final
   NO_MORE_EVENTS : constant Java.Int;

   --  final
   DATA_TRANSFER_COMPLETE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintJobEvent);
   pragma Import (Java, GetPrintEventType, "getPrintEventType");
   pragma Import (Java, GetPrintJob, "getPrintJob");
   pragma Import (Java, JOB_CANCELED, "JOB_CANCELED");
   pragma Import (Java, JOB_COMPLETE, "JOB_COMPLETE");
   pragma Import (Java, JOB_FAILED, "JOB_FAILED");
   pragma Import (Java, REQUIRES_ATTENTION, "REQUIRES_ATTENTION");
   pragma Import (Java, NO_MORE_EVENTS, "NO_MORE_EVENTS");
   pragma Import (Java, DATA_TRANSFER_COMPLETE, "DATA_TRANSFER_COMPLETE");

end Javax.Print.Event.PrintJobEvent;
pragma Import (Java, Javax.Print.Event.PrintJobEvent, "javax.print.event.PrintJobEvent");
pragma Extensions_Allowed (Off);
