pragma Extensions_Allowed (On);
limited with Javax.Print.Event.PrintJobEvent;
with Java.Lang.Object;

package Javax.Print.Event.PrintJobListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PrintDataTransferCompleted (This : access Typ;
                                         P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class) is abstract;

   procedure PrintJobCompleted (This : access Typ;
                                P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class) is abstract;

   procedure PrintJobFailed (This : access Typ;
                             P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class) is abstract;

   procedure PrintJobCanceled (This : access Typ;
                               P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class) is abstract;

   procedure PrintJobNoMoreEvents (This : access Typ;
                                   P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class) is abstract;

   procedure PrintJobRequiresAttention (This : access Typ;
                                        P1_PrintJobEvent : access Standard.Javax.Print.Event.PrintJobEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, PrintDataTransferCompleted, "printDataTransferCompleted");
   pragma Export (Java, PrintJobCompleted, "printJobCompleted");
   pragma Export (Java, PrintJobFailed, "printJobFailed");
   pragma Export (Java, PrintJobCanceled, "printJobCanceled");
   pragma Export (Java, PrintJobNoMoreEvents, "printJobNoMoreEvents");
   pragma Export (Java, PrintJobRequiresAttention, "printJobRequiresAttention");

end Javax.Print.Event.PrintJobListener;
pragma Import (Java, Javax.Print.Event.PrintJobListener, "javax.print.event.PrintJobListener");
pragma Extensions_Allowed (Off);
