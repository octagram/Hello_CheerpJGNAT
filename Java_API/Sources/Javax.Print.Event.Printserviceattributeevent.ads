pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.PrintServiceAttributeSet;
limited with Javax.Print.PrintService;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Print.Event.PrintEvent;

package Javax.Print.Event.PrintServiceAttributeEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Print.Event.PrintEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrintServiceAttributeEvent (P1_PrintService : access Standard.Javax.Print.PrintService.Typ'Class;
                                            P2_PrintServiceAttributeSet : access Standard.Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrintService (This : access Typ)
                             return access Javax.Print.PrintService.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintServiceAttributeEvent);
   pragma Import (Java, GetPrintService, "getPrintService");
   pragma Import (Java, GetAttributes, "getAttributes");

end Javax.Print.Event.PrintServiceAttributeEvent;
pragma Import (Java, Javax.Print.Event.PrintServiceAttributeEvent, "javax.print.event.PrintServiceAttributeEvent");
pragma Extensions_Allowed (Off);
