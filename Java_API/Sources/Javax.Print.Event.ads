pragma Extensions_Allowed (On);
package Javax.Print.Event is
   pragma Preelaborate;
end Javax.Print.Event;
pragma Import (Java, Javax.Print.Event, "javax.print.event");
pragma Extensions_Allowed (Off);
