pragma Extensions_Allowed (On);
limited with Javax.Print.Doc;
with Java.Lang.Object;

package Javax.Print.MultiDoc is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDoc (This : access Typ)
                    return access Javax.Print.Doc.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function Next (This : access Typ)
                  return access Javax.Print.MultiDoc.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDoc, "getDoc");
   pragma Export (Java, Next, "next");

end Javax.Print.MultiDoc;
pragma Import (Java, Javax.Print.MultiDoc, "javax.print.MultiDoc");
pragma Extensions_Allowed (Off);
