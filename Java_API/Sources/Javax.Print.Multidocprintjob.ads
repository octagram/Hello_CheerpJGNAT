pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.PrintRequestAttributeSet;
limited with Javax.Print.MultiDoc;
with Java.Lang.Object;
with Javax.Print.DocPrintJob;

package Javax.Print.MultiDocPrintJob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DocPrintJob_I : Javax.Print.DocPrintJob.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Print (This : access Typ;
                    P1_MultiDoc : access Standard.Javax.Print.MultiDoc.Typ'Class;
                    P2_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class) is abstract;
   --  can raise Javax.Print.PrintException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Print, "print");

end Javax.Print.MultiDocPrintJob;
pragma Import (Java, Javax.Print.MultiDocPrintJob, "javax.print.MultiDocPrintJob");
pragma Extensions_Allowed (Off);
