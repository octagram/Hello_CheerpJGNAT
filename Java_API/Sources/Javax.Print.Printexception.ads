pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Print.PrintException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrintException (This : Ref := null)
                                return Ref;

   function New_PrintException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_PrintException (P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_PrintException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                This : Ref := null)
                                return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.print.PrintException");
   pragma Java_Constructor (New_PrintException);

end Javax.Print.PrintException;
pragma Import (Java, Javax.Print.PrintException, "javax.print.PrintException");
pragma Extensions_Allowed (Off);
