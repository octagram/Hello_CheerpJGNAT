pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Print.Attribute.Attribute;
limited with Javax.Print.Attribute.AttributeSet;
limited with Javax.Print.Attribute.PrintServiceAttribute;
limited with Javax.Print.Attribute.PrintServiceAttributeSet;
limited with Javax.Print.DocFlavor;
limited with Javax.Print.DocPrintJob;
limited with Javax.Print.Event.PrintServiceAttributeListener;
limited with Javax.Print.ServiceUIFactory;
with Java.Lang.Object;

package Javax.Print.PrintService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function CreatePrintJob (This : access Typ)
                            return access Javax.Print.DocPrintJob.Typ'Class is abstract;

   procedure AddPrintServiceAttributeListener (This : access Typ;
                                               P1_PrintServiceAttributeListener : access Standard.Javax.Print.Event.PrintServiceAttributeListener.Typ'Class) is abstract;

   procedure RemovePrintServiceAttributeListener (This : access Typ;
                                                  P1_PrintServiceAttributeListener : access Standard.Javax.Print.Event.PrintServiceAttributeListener.Typ'Class) is abstract;

   function GetAttributes (This : access Typ)
                           return access Javax.Print.Attribute.PrintServiceAttributeSet.Typ'Class is abstract;

   function GetAttribute (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return access Javax.Print.Attribute.PrintServiceAttribute.Typ'Class is abstract;

   function GetSupportedDocFlavors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function IsDocFlavorSupported (This : access Typ;
                                  P1_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class)
                                  return Java.Boolean is abstract;

   function GetSupportedAttributeCategories (This : access Typ)
                                             return Standard.Java.Lang.Object.Ref is abstract;

   function IsAttributeCategorySupported (This : access Typ;
                                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                          return Java.Boolean is abstract;

   function GetDefaultAttributeValue (This : access Typ;
                                      P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                      return access Java.Lang.Object.Typ'Class is abstract;

   function GetSupportedAttributeValues (This : access Typ;
                                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                         P2_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                                         P3_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                                         return access Java.Lang.Object.Typ'Class is abstract;

   function IsAttributeValueSupported (This : access Typ;
                                       P1_Attribute : access Standard.Javax.Print.Attribute.Attribute.Typ'Class;
                                       P2_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                                       P3_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                                       return Java.Boolean is abstract;

   function GetUnsupportedAttributes (This : access Typ;
                                      P1_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                                      P2_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                                      return access Javax.Print.Attribute.AttributeSet.Typ'Class is abstract;

   function GetServiceUIFactory (This : access Typ)
                                 return access Javax.Print.ServiceUIFactory.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, CreatePrintJob, "createPrintJob");
   pragma Export (Java, AddPrintServiceAttributeListener, "addPrintServiceAttributeListener");
   pragma Export (Java, RemovePrintServiceAttributeListener, "removePrintServiceAttributeListener");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, GetSupportedDocFlavors, "getSupportedDocFlavors");
   pragma Export (Java, IsDocFlavorSupported, "isDocFlavorSupported");
   pragma Export (Java, GetSupportedAttributeCategories, "getSupportedAttributeCategories");
   pragma Export (Java, IsAttributeCategorySupported, "isAttributeCategorySupported");
   pragma Export (Java, GetDefaultAttributeValue, "getDefaultAttributeValue");
   pragma Export (Java, GetSupportedAttributeValues, "getSupportedAttributeValues");
   pragma Export (Java, IsAttributeValueSupported, "isAttributeValueSupported");
   pragma Export (Java, GetUnsupportedAttributes, "getUnsupportedAttributes");
   pragma Export (Java, GetServiceUIFactory, "getServiceUIFactory");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");

end Javax.Print.PrintService;
pragma Import (Java, Javax.Print.PrintService, "javax.print.PrintService");
pragma Extensions_Allowed (Off);
