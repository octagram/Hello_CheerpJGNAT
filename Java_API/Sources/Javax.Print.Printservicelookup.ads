pragma Extensions_Allowed (On);
limited with Javax.Print.Attribute.AttributeSet;
limited with Javax.Print.DocFlavor;
limited with Javax.Print.MultiDocPrintService;
limited with Javax.Print.PrintService;
with Java.Lang.Object;

package Javax.Print.PrintServiceLookup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_PrintServiceLookup (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function LookupPrintServices (P1_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                                 P2_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                                 return Standard.Java.Lang.Object.Ref;

   --  final
   function LookupMultiDocPrintServices (P1_DocFlavor_Arr : access Javax.Print.DocFlavor.Arr_Obj;
                                         P2_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                                         return Standard.Java.Lang.Object.Ref;

   --  final
   function LookupDefaultPrintService return access Javax.Print.PrintService.Typ'Class;

   function RegisterServiceProvider (P1_PrintServiceLookup : access Standard.Javax.Print.PrintServiceLookup.Typ'Class)
                                     return Java.Boolean;

   function RegisterService (P1_PrintService : access Standard.Javax.Print.PrintService.Typ'Class)
                             return Java.Boolean;

   function GetPrintServices (This : access Typ;
                              P1_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                              P2_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function GetPrintServices (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function GetMultiDocPrintServices (This : access Typ;
                                      P1_DocFlavor_Arr : access Javax.Print.DocFlavor.Arr_Obj;
                                      P2_AttributeSet : access Standard.Javax.Print.Attribute.AttributeSet.Typ'Class)
                                      return Standard.Java.Lang.Object.Ref is abstract;

   function GetDefaultPrintService (This : access Typ)
                                    return access Javax.Print.PrintService.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintServiceLookup);
   pragma Export (Java, LookupPrintServices, "lookupPrintServices");
   pragma Export (Java, LookupMultiDocPrintServices, "lookupMultiDocPrintServices");
   pragma Export (Java, LookupDefaultPrintService, "lookupDefaultPrintService");
   pragma Export (Java, RegisterServiceProvider, "registerServiceProvider");
   pragma Export (Java, RegisterService, "registerService");
   pragma Export (Java, GetPrintServices, "getPrintServices");
   pragma Export (Java, GetMultiDocPrintServices, "getMultiDocPrintServices");
   pragma Export (Java, GetDefaultPrintService, "getDefaultPrintService");

end Javax.Print.PrintServiceLookup;
pragma Import (Java, Javax.Print.PrintServiceLookup, "javax.print.PrintServiceLookup");
pragma Extensions_Allowed (Off);
