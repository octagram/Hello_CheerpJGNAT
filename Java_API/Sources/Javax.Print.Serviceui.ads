pragma Extensions_Allowed (On);
limited with Java.Awt.GraphicsConfiguration;
limited with Javax.Print.Attribute.PrintRequestAttributeSet;
limited with Javax.Print.DocFlavor;
limited with Javax.Print.PrintService;
with Java.Lang.Object;

package Javax.Print.ServiceUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ServiceUI (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function PrintDialog (P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_PrintService_Arr : access Javax.Print.PrintService.Arr_Obj;
                         P5_PrintService : access Standard.Javax.Print.PrintService.Typ'Class;
                         P6_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                         P7_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class)
                         return access Javax.Print.PrintService.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServiceUI);
   pragma Import (Java, PrintDialog, "printDialog");

end Javax.Print.ServiceUI;
pragma Import (Java, Javax.Print.ServiceUI, "javax.print.ServiceUI");
pragma Extensions_Allowed (Off);
