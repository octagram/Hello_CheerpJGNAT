pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Print.ServiceUIFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ServiceUIFactory (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ;
                   P1_Int : Java.Int;
                   P2_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Java.Lang.Object.Typ'Class is abstract;

   function GetUIClassNamesForRole (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JCOMPONENT_UI : constant access Java.Lang.String.Typ'Class;

   --  final
   PANEL_UI : constant access Java.Lang.String.Typ'Class;

   --  final
   DIALOG_UI : constant access Java.Lang.String.Typ'Class;

   --  final
   JDIALOG_UI : constant access Java.Lang.String.Typ'Class;

   --  final
   ABOUT_UIROLE : constant Java.Int;

   --  final
   ADMIN_UIROLE : constant Java.Int;

   --  final
   MAIN_UIROLE : constant Java.Int;

   --  final
   RESERVED_UIROLE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServiceUIFactory);
   pragma Export (Java, GetUI, "getUI");
   pragma Export (Java, GetUIClassNamesForRole, "getUIClassNamesForRole");
   pragma Import (Java, JCOMPONENT_UI, "JCOMPONENT_UI");
   pragma Import (Java, PANEL_UI, "PANEL_UI");
   pragma Import (Java, DIALOG_UI, "DIALOG_UI");
   pragma Import (Java, JDIALOG_UI, "JDIALOG_UI");
   pragma Import (Java, ABOUT_UIROLE, "ABOUT_UIROLE");
   pragma Import (Java, ADMIN_UIROLE, "ADMIN_UIROLE");
   pragma Import (Java, MAIN_UIROLE, "MAIN_UIROLE");
   pragma Import (Java, RESERVED_UIROLE, "RESERVED_UIROLE");

end Javax.Print.ServiceUIFactory;
pragma Import (Java, Javax.Print.ServiceUIFactory, "javax.print.ServiceUIFactory");
pragma Extensions_Allowed (Off);
