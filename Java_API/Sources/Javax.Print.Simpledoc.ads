pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Javax.Print.Attribute.DocAttributeSet;
limited with Javax.Print.DocFlavor;
with Java.Lang.Object;
with Javax.Print.Doc;

package Javax.Print.SimpleDoc is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Doc_I : Javax.Print.Doc.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleDoc (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                           P3_DocAttributeSet : access Standard.Javax.Print.Attribute.DocAttributeSet.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDocFlavor (This : access Typ)
                          return access Javax.Print.DocFlavor.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Print.Attribute.DocAttributeSet.Typ'Class;

   function GetPrintData (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetReaderForText (This : access Typ)
                              return access Java.Io.Reader.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetStreamForBytes (This : access Typ)
                               return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleDoc);
   pragma Import (Java, GetDocFlavor, "getDocFlavor");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetPrintData, "getPrintData");
   pragma Import (Java, GetReaderForText, "getReaderForText");
   pragma Import (Java, GetStreamForBytes, "getStreamForBytes");

end Javax.Print.SimpleDoc;
pragma Import (Java, Javax.Print.SimpleDoc, "javax.print.SimpleDoc");
pragma Extensions_Allowed (Off);
