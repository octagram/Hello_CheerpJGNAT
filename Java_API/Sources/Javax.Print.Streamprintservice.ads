pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Print.PrintService;

package Javax.Print.StreamPrintService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PrintService_I : Javax.Print.PrintService.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_StreamPrintService (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class;

   function GetOutputFormat (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure Dispose (This : access Typ);

   function IsDisposed (This : access Typ)
                        return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StreamPrintService);
   pragma Import (Java, GetOutputStream, "getOutputStream");
   pragma Export (Java, GetOutputFormat, "getOutputFormat");
   pragma Import (Java, Dispose, "dispose");
   pragma Import (Java, IsDisposed, "isDisposed");

end Javax.Print.StreamPrintService;
pragma Import (Java, Javax.Print.StreamPrintService, "javax.print.StreamPrintService");
pragma Extensions_Allowed (Off);
