pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Javax.Print.DocFlavor;
limited with Javax.Print.StreamPrintService;
with Java.Lang.Object;

package Javax.Print.StreamPrintServiceFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_StreamPrintServiceFactory (This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function LookupStreamPrintServiceFactories (P1_DocFlavor : access Standard.Javax.Print.DocFlavor.Typ'Class;
                                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                                               return Standard.Java.Lang.Object.Ref;

   function GetOutputFormat (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetSupportedDocFlavors (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function GetPrintService (This : access Typ;
                             P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                             return access Javax.Print.StreamPrintService.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StreamPrintServiceFactory);
   pragma Export (Java, LookupStreamPrintServiceFactories, "lookupStreamPrintServiceFactories");
   pragma Export (Java, GetOutputFormat, "getOutputFormat");
   pragma Export (Java, GetSupportedDocFlavors, "getSupportedDocFlavors");
   pragma Export (Java, GetPrintService, "getPrintService");

end Javax.Print.StreamPrintServiceFactory;
pragma Import (Java, Javax.Print.StreamPrintServiceFactory, "javax.print.StreamPrintServiceFactory");
pragma Extensions_Allowed (Off);
