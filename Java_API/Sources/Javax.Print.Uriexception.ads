pragma Extensions_Allowed (On);
limited with Java.Net.URI;
with Java.Lang.Object;

package Javax.Print.URIException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUnsupportedURI (This : access Typ)
                               return access Java.Net.URI.Typ'Class is abstract;

   function GetReason (This : access Typ)
                       return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   URIInaccessible : constant Java.Int;

   --  final
   URISchemeNotSupported : constant Java.Int;

   --  final
   URIOtherProblem : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetUnsupportedURI, "getUnsupportedURI");
   pragma Export (Java, GetReason, "getReason");
   pragma Import (Java, URIInaccessible, "URIInaccessible");
   pragma Import (Java, URISchemeNotSupported, "URISchemeNotSupported");
   pragma Import (Java, URIOtherProblem, "URIOtherProblem");

end Javax.Print.URIException;
pragma Import (Java, Javax.Print.URIException, "javax.print.URIException");
pragma Extensions_Allowed (Off);
