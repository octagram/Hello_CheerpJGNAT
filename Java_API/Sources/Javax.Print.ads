pragma Extensions_Allowed (On);
package Javax.Print is
   pragma Preelaborate;
end Javax.Print;
pragma Import (Java, Javax.Print, "javax.print");
pragma Extensions_Allowed (Off);
