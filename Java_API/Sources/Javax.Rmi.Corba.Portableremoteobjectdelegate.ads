pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Rmi.Remote;
with Java.Lang.Object;

package Javax.Rmi.CORBA.PortableRemoteObjectDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ExportObject (This : access Typ;
                           P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   function ToStub (This : access Typ;
                    P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class)
                    return access Java.Rmi.Remote.Typ'Class is abstract;
   --  can raise Java.Rmi.NoSuchObjectException.Except

   procedure UnexportObject (This : access Typ;
                             P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;
   --  can raise Java.Rmi.NoSuchObjectException.Except

   function Narrow (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.ClassCastException.Except

   procedure Connect (This : access Typ;
                      P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                      P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;
   --  can raise Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ExportObject, "exportObject");
   pragma Export (Java, ToStub, "toStub");
   pragma Export (Java, UnexportObject, "unexportObject");
   pragma Export (Java, Narrow, "narrow");
   pragma Export (Java, Connect, "connect");

end Javax.Rmi.CORBA.PortableRemoteObjectDelegate;
pragma Import (Java, Javax.Rmi.CORBA.PortableRemoteObjectDelegate, "javax.rmi.CORBA.PortableRemoteObjectDelegate");
pragma Extensions_Allowed (Off);
