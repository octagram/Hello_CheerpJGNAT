pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.ORB;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA_2_3.Portable.ObjectImpl;

package Javax.Rmi.CORBA.Stub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is abstract new Org.Omg.CORBA_2_3.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   function New_Stub (This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Connect (This : access Typ;
                      P1_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class);
   --  can raise Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Stub);
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Connect, "connect");

end Javax.Rmi.CORBA.Stub;
pragma Import (Java, Javax.Rmi.CORBA.Stub, "javax.rmi.CORBA.Stub");
pragma Extensions_Allowed (Off);
