pragma Extensions_Allowed (On);
limited with Java.Io.ObjectInputStream;
limited with Java.Io.ObjectOutputStream;
limited with Java.Lang.String;
limited with Javax.Rmi.CORBA.Stub;
limited with Org.Omg.CORBA.ORB;
with Java.Lang.Object;

package Javax.Rmi.CORBA.StubDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function HashCode (This : access Typ;
                      P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class)
                      return Java.Int is abstract;

   function Equals (This : access Typ;
                    P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function ToString (This : access Typ;
                      P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure Connect (This : access Typ;
                      P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class;
                      P2_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class) is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   procedure ReadObject (This : access Typ;
                         P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class;
                         P2_ObjectInputStream : access Standard.Java.Io.ObjectInputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Java.Lang.ClassNotFoundException.Except

   procedure WriteObject (This : access Typ;
                          P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class;
                          P2_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, ReadObject, "readObject");
   pragma Export (Java, WriteObject, "writeObject");

end Javax.Rmi.CORBA.StubDelegate;
pragma Import (Java, Javax.Rmi.CORBA.StubDelegate, "javax.rmi.CORBA.StubDelegate");
pragma Extensions_Allowed (Off);
