pragma Extensions_Allowed (On);
limited with Java.Rmi.Remote;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Object;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.InvokeHandler;

package Javax.Rmi.CORBA.Tie is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            InvokeHandler_I : Org.Omg.CORBA.Portable.InvokeHandler.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ThisObject (This : access Typ)
                        return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   procedure Deactivate (This : access Typ) is abstract;
   --  can raise Java.Rmi.NoSuchObjectException.Except

   function Orb (This : access Typ)
                 return access Org.Omg.CORBA.ORB.Typ'Class is abstract;

   procedure Orb (This : access Typ;
                  P1_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class) is abstract;

   procedure SetTarget (This : access Typ;
                        P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;

   function GetTarget (This : access Typ)
                       return access Java.Rmi.Remote.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ThisObject, "thisObject");
   pragma Export (Java, Deactivate, "deactivate");
   pragma Export (Java, Orb, "orb");
   pragma Export (Java, SetTarget, "setTarget");
   pragma Export (Java, GetTarget, "getTarget");

end Javax.Rmi.CORBA.Tie;
pragma Import (Java, Javax.Rmi.CORBA.Tie, "javax.rmi.CORBA.Tie");
pragma Extensions_Allowed (Off);
