pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Java.Rmi.Remote;
limited with Java.Rmi.RemoteException;
limited with Javax.Rmi.CORBA.Stub;
limited with Javax.Rmi.CORBA.Tie;
limited with Javax.Rmi.CORBA.ValueHandler;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.SystemException;
with Java.Lang.Object;

package Javax.Rmi.CORBA.Util is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function MapSystemException (P1_SystemException : access Standard.Org.Omg.CORBA.SystemException.Typ'Class)
                                return access Java.Rmi.RemoteException.Typ'Class;

   procedure WriteAny (P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function ReadAny (P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;

   procedure WriteRemoteObject (P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure WriteAbstractObject (P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RegisterTarget (P1_Tie : access Standard.Javax.Rmi.CORBA.Tie.Typ'Class;
                             P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class);

   procedure UnexportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class);
   --  can raise Java.Rmi.NoSuchObjectException.Except

   function GetTie (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class)
                    return access Javax.Rmi.CORBA.Tie.Typ'Class;

   function CreateValueHandler return access Javax.Rmi.CORBA.ValueHandler.Typ'Class;

   function GetCodebase (P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function LoadClass (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                       return access Java.Lang.Class.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function IsLocal (P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class)
                     return Java.Boolean;
   --  can raise Java.Rmi.RemoteException.Except

   function WrapException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class)
                           return access Java.Rmi.RemoteException.Typ'Class;

   function CopyObjects (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                         P2_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class)
                         return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Rmi.RemoteException.Except

   function CopyObject (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, MapSystemException, "mapSystemException");
   pragma Import (Java, WriteAny, "writeAny");
   pragma Import (Java, ReadAny, "readAny");
   pragma Import (Java, WriteRemoteObject, "writeRemoteObject");
   pragma Import (Java, WriteAbstractObject, "writeAbstractObject");
   pragma Import (Java, RegisterTarget, "registerTarget");
   pragma Import (Java, UnexportObject, "unexportObject");
   pragma Import (Java, GetTie, "getTie");
   pragma Import (Java, CreateValueHandler, "createValueHandler");
   pragma Import (Java, GetCodebase, "getCodebase");
   pragma Import (Java, LoadClass, "loadClass");
   pragma Import (Java, IsLocal, "isLocal");
   pragma Import (Java, WrapException, "wrapException");
   pragma Import (Java, CopyObjects, "copyObjects");
   pragma Import (Java, CopyObject, "copyObject");

end Javax.Rmi.CORBA.Util;
pragma Import (Java, Javax.Rmi.CORBA.Util, "javax.rmi.CORBA.Util");
pragma Extensions_Allowed (Off);
