pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Java.Rmi.Remote;
limited with Java.Rmi.RemoteException;
limited with Javax.Rmi.CORBA.Stub;
limited with Javax.Rmi.CORBA.Tie;
limited with Javax.Rmi.CORBA.ValueHandler;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.SystemException;
with Java.Lang.Object;

package Javax.Rmi.CORBA.UtilDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function MapSystemException (This : access Typ;
                                P1_SystemException : access Standard.Org.Omg.CORBA.SystemException.Typ'Class)
                                return access Java.Rmi.RemoteException.Typ'Class is abstract;

   procedure WriteAny (This : access Typ;
                       P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function ReadAny (This : access Typ;
                     P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class)
                     return access Java.Lang.Object.Typ'Class is abstract;

   procedure WriteRemoteObject (This : access Typ;
                                P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure WriteAbstractObject (This : access Typ;
                                  P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure RegisterTarget (This : access Typ;
                             P1_Tie : access Standard.Javax.Rmi.CORBA.Tie.Typ'Class;
                             P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;

   procedure UnexportObject (This : access Typ;
                             P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class) is abstract;
   --  can raise Java.Rmi.NoSuchObjectException.Except

   function GetTie (This : access Typ;
                    P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class)
                    return access Javax.Rmi.CORBA.Tie.Typ'Class is abstract;

   function CreateValueHandler (This : access Typ)
                                return access Javax.Rmi.CORBA.ValueHandler.Typ'Class is abstract;

   function GetCodebase (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return access Java.Lang.String.Typ'Class is abstract;

   function LoadClass (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                       return access Java.Lang.Class.Typ'Class is abstract;
   --  can raise Java.Lang.ClassNotFoundException.Except

   function IsLocal (This : access Typ;
                     P1_Stub : access Standard.Javax.Rmi.CORBA.Stub.Typ'Class)
                     return Java.Boolean is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   function WrapException (This : access Typ;
                           P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class)
                           return access Java.Rmi.RemoteException.Typ'Class is abstract;

   function CopyObject (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Rmi.RemoteException.Except

   function CopyObjects (This : access Typ;
                         P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                         P2_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class)
                         return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, MapSystemException, "mapSystemException");
   pragma Export (Java, WriteAny, "writeAny");
   pragma Export (Java, ReadAny, "readAny");
   pragma Export (Java, WriteRemoteObject, "writeRemoteObject");
   pragma Export (Java, WriteAbstractObject, "writeAbstractObject");
   pragma Export (Java, RegisterTarget, "registerTarget");
   pragma Export (Java, UnexportObject, "unexportObject");
   pragma Export (Java, GetTie, "getTie");
   pragma Export (Java, CreateValueHandler, "createValueHandler");
   pragma Export (Java, GetCodebase, "getCodebase");
   pragma Export (Java, LoadClass, "loadClass");
   pragma Export (Java, IsLocal, "isLocal");
   pragma Export (Java, WrapException, "wrapException");
   pragma Export (Java, CopyObject, "copyObject");
   pragma Export (Java, CopyObjects, "copyObjects");

end Javax.Rmi.CORBA.UtilDelegate;
pragma Import (Java, Javax.Rmi.CORBA.UtilDelegate, "javax.rmi.CORBA.UtilDelegate");
pragma Extensions_Allowed (Off);
