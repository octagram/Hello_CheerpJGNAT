pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.SendingContext.RunTime;
with Java.Lang.Object;

package Javax.Rmi.CORBA.ValueHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteValue (This : access Typ;
                         P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                         P2_Serializable : access Standard.Java.Io.Serializable.Typ'Class) is abstract;

   function ReadValue (This : access Typ;
                       P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                       P4_String : access Standard.Java.Lang.String.Typ'Class;
                       P5_RunTime : access Standard.Org.Omg.SendingContext.RunTime.Typ'Class)
                       return access Java.Io.Serializable.Typ'Class is abstract;

   function GetRMIRepositoryID (This : access Typ;
                                P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;

   function IsCustomMarshaled (This : access Typ;
                               P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                               return Java.Boolean is abstract;

   function GetRunTimeCodeBase (This : access Typ)
                                return access Org.Omg.SendingContext.RunTime.Typ'Class is abstract;

   function WriteReplace (This : access Typ;
                          P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class)
                          return access Java.Io.Serializable.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WriteValue, "writeValue");
   pragma Export (Java, ReadValue, "readValue");
   pragma Export (Java, GetRMIRepositoryID, "getRMIRepositoryID");
   pragma Export (Java, IsCustomMarshaled, "isCustomMarshaled");
   pragma Export (Java, GetRunTimeCodeBase, "getRunTimeCodeBase");
   pragma Export (Java, WriteReplace, "writeReplace");

end Javax.Rmi.CORBA.ValueHandler;
pragma Import (Java, Javax.Rmi.CORBA.ValueHandler, "javax.rmi.CORBA.ValueHandler");
pragma Extensions_Allowed (Off);
