pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Org.Omg.CORBA.Portable.OutputStream;
with Java.Lang.Object;
with Javax.Rmi.CORBA.ValueHandler;

package Javax.Rmi.CORBA.ValueHandlerMultiFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ValueHandler_I : Javax.Rmi.CORBA.ValueHandler.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMaximumStreamFormatVersion (This : access Typ)
                                           return Java.Byte is abstract;

   procedure WriteValue (This : access Typ;
                         P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                         P2_Serializable : access Standard.Java.Io.Serializable.Typ'Class;
                         P3_Byte : Java.Byte) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMaximumStreamFormatVersion, "getMaximumStreamFormatVersion");
   pragma Export (Java, WriteValue, "writeValue");

end Javax.Rmi.CORBA.ValueHandlerMultiFormat;
pragma Import (Java, Javax.Rmi.CORBA.ValueHandlerMultiFormat, "javax.rmi.CORBA.ValueHandlerMultiFormat");
pragma Extensions_Allowed (Off);
