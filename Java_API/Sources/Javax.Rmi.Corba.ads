pragma Extensions_Allowed (On);
package Javax.Rmi.CORBA is
   pragma Preelaborate;
end Javax.Rmi.CORBA;
pragma Import (Java, Javax.Rmi.CORBA, "javax.rmi.CORBA");
pragma Extensions_Allowed (Off);
