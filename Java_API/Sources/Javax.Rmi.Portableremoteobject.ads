pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Rmi.Remote;
with Java.Lang.Object;

package Javax.Rmi.PortableRemoteObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_PortableRemoteObject (This : Ref := null)
                                      return Ref;
   --  can raise Java.Rmi.RemoteException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ExportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class);
   --  can raise Java.Rmi.RemoteException.Except

   function ToStub (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class)
                    return access Java.Rmi.Remote.Typ'Class;
   --  can raise Java.Rmi.NoSuchObjectException.Except

   procedure UnexportObject (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class);
   --  can raise Java.Rmi.NoSuchObjectException.Except

   function Narrow (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.ClassCastException.Except

   procedure Connect (P1_Remote : access Standard.Java.Rmi.Remote.Typ'Class;
                      P2_Remote : access Standard.Java.Rmi.Remote.Typ'Class);
   --  can raise Java.Rmi.RemoteException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PortableRemoteObject);
   pragma Import (Java, ExportObject, "exportObject");
   pragma Import (Java, ToStub, "toStub");
   pragma Import (Java, UnexportObject, "unexportObject");
   pragma Import (Java, Narrow, "narrow");
   pragma Import (Java, Connect, "connect");

end Javax.Rmi.PortableRemoteObject;
pragma Import (Java, Javax.Rmi.PortableRemoteObject, "javax.rmi.PortableRemoteObject");
pragma Extensions_Allowed (Off);
