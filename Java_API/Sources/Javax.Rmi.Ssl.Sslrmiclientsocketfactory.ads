pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.Socket;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Server.RMIClientSocketFactory;

package Javax.Rmi.Ssl.SslRMIClientSocketFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            RMIClientSocketFactory_I : Java.Rmi.Server.RMIClientSocketFactory.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SslRMIClientSocketFactory (This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateSocket (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Net.Socket.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SslRMIClientSocketFactory);
   pragma Import (Java, CreateSocket, "createSocket");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Rmi.Ssl.SslRMIClientSocketFactory;
pragma Import (Java, Javax.Rmi.Ssl.SslRMIClientSocketFactory, "javax.rmi.ssl.SslRMIClientSocketFactory");
pragma Extensions_Allowed (Off);
