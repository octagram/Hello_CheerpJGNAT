pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.ServerSocket;
with Java.Lang.Object;
with Java.Rmi.Server.RMIServerSocketFactory;

package Javax.Rmi.Ssl.SslRMIServerSocketFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RMIServerSocketFactory_I : Java.Rmi.Server.RMIServerSocketFactory.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SslRMIServerSocketFactory (This : Ref := null)
                                           return Ref;

   function New_SslRMIServerSocketFactory (P1_String_Arr : access Java.Lang.String.Arr_Obj;
                                           P2_String_Arr : access Java.Lang.String.Arr_Obj;
                                           P3_Boolean : Java.Boolean; 
                                           This : Ref := null)
                                           return Ref;
   --  can raise Java.Lang.IllegalArgumentException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetEnabledCipherSuites (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   --  final
   function GetEnabledProtocols (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   --  final
   function GetNeedClientAuth (This : access Typ)
                               return Java.Boolean;

   function CreateServerSocket (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Net.ServerSocket.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SslRMIServerSocketFactory);
   pragma Import (Java, GetEnabledCipherSuites, "getEnabledCipherSuites");
   pragma Import (Java, GetEnabledProtocols, "getEnabledProtocols");
   pragma Import (Java, GetNeedClientAuth, "getNeedClientAuth");
   pragma Import (Java, CreateServerSocket, "createServerSocket");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Rmi.Ssl.SslRMIServerSocketFactory;
pragma Import (Java, Javax.Rmi.Ssl.SslRMIServerSocketFactory, "javax.rmi.ssl.SslRMIServerSocketFactory");
pragma Extensions_Allowed (Off);
