pragma Extensions_Allowed (On);
package Javax.Rmi.Ssl is
   pragma Preelaborate;
end Javax.Rmi.Ssl;
pragma Import (Java, Javax.Rmi.Ssl, "javax.rmi.ssl");
pragma Extensions_Allowed (Off);
