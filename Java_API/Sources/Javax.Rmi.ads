pragma Extensions_Allowed (On);
package Javax.Rmi is
   pragma Preelaborate;
end Javax.Rmi;
pragma Import (Java, Javax.Rmi, "javax.rmi");
pragma Extensions_Allowed (Off);
