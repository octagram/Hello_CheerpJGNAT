pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Javax.Script.Bindings;
limited with Javax.Script.ScriptContext;
with Java.Lang.Object;
with Javax.Script.ScriptEngine;

package Javax.Script.AbstractScriptEngine is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ScriptEngine_I : Javax.Script.ScriptEngine.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Context : access Javax.Script.ScriptContext.Typ'Class;
      pragma Import (Java, Context, "context");

   end record;

   function New_AbstractScriptEngine (This : Ref := null)
                                      return Ref;

   function New_AbstractScriptEngine (P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetContext (This : access Typ;
                         P1_ScriptContext : access Standard.Javax.Script.ScriptContext.Typ'Class);

   function GetContext (This : access Typ)
                        return access Javax.Script.ScriptContext.Typ'Class;

   function GetBindings (This : access Typ;
                         P1_Int : Java.Int)
                         return access Javax.Script.Bindings.Typ'Class;

   procedure SetBindings (This : access Typ;
                          P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class;
                          P2_Int : Java.Int);

   procedure Put (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Eval (This : access Typ;
                  P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                  P2_Bindings : access Standard.Javax.Script.Bindings.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Bindings : access Standard.Javax.Script.Bindings.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_Reader : access Standard.Java.Io.Reader.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Script.ScriptException.Except

   --  protected
   function GetScriptContext (This : access Typ;
                              P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class)
                              return access Javax.Script.ScriptContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractScriptEngine);
   pragma Import (Java, SetContext, "setContext");
   pragma Import (Java, GetContext, "getContext");
   pragma Import (Java, GetBindings, "getBindings");
   pragma Import (Java, SetBindings, "setBindings");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Eval, "eval");
   pragma Import (Java, GetScriptContext, "getScriptContext");

end Javax.Script.AbstractScriptEngine;
pragma Import (Java, Javax.Script.AbstractScriptEngine, "javax.script.AbstractScriptEngine");
pragma Extensions_Allowed (Off);
