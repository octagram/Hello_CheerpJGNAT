pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Javax.Script.CompiledScript;
with Java.Lang.Object;

package Javax.Script.Compilable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Compile (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Script.CompiledScript.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   function Compile (This : access Typ;
                     P1_Reader : access Standard.Java.Io.Reader.Typ'Class)
                     return access Javax.Script.CompiledScript.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Compile, "compile");

end Javax.Script.Compilable;
pragma Import (Java, Javax.Script.Compilable, "javax.script.Compilable");
pragma Extensions_Allowed (Off);
