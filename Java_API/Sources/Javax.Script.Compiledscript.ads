pragma Extensions_Allowed (On);
limited with Javax.Script.Bindings;
limited with Javax.Script.ScriptContext;
limited with Javax.Script.ScriptEngine;
with Java.Lang.Object;

package Javax.Script.CompiledScript is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_CompiledScript (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Eval (This : access Typ;
                  P1_ScriptContext : access Standard.Javax.Script.ScriptContext.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Script.ScriptException.Except

   function GetEngine (This : access Typ)
                       return access Javax.Script.ScriptEngine.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompiledScript);
   pragma Export (Java, Eval, "eval");
   pragma Export (Java, GetEngine, "getEngine");

end Javax.Script.CompiledScript;
pragma Import (Java, Javax.Script.CompiledScript, "javax.script.CompiledScript");
pragma Extensions_Allowed (Off);
