pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Script.Bindings;
with Java.Lang.Object;

package Javax.Script.ScriptContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetBindings (This : access Typ;
                          P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class;
                          P2_Int : Java.Int) is abstract;

   function GetBindings (This : access Typ;
                         P1_Int : Java.Int)
                         return access Javax.Script.Bindings.Typ'Class is abstract;

   procedure SetAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Int : Java.Int) is abstract;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function RemoveAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Int : Java.Int)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function GetAttributesScope (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int is abstract;

   function GetWriter (This : access Typ)
                       return access Java.Io.Writer.Typ'Class is abstract;

   function GetErrorWriter (This : access Typ)
                            return access Java.Io.Writer.Typ'Class is abstract;

   procedure SetWriter (This : access Typ;
                        P1_Writer : access Standard.Java.Io.Writer.Typ'Class) is abstract;

   procedure SetErrorWriter (This : access Typ;
                             P1_Writer : access Standard.Java.Io.Writer.Typ'Class) is abstract;

   function GetReader (This : access Typ)
                       return access Java.Io.Reader.Typ'Class is abstract;

   procedure SetReader (This : access Typ;
                        P1_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;

   function GetScopes (This : access Typ)
                       return access Java.Util.List.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ENGINE_SCOPE : constant Java.Int;

   --  final
   GLOBAL_SCOPE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetBindings, "setBindings");
   pragma Export (Java, GetBindings, "getBindings");
   pragma Export (Java, SetAttribute, "setAttribute");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, RemoveAttribute, "removeAttribute");
   pragma Export (Java, GetAttributesScope, "getAttributesScope");
   pragma Export (Java, GetWriter, "getWriter");
   pragma Export (Java, GetErrorWriter, "getErrorWriter");
   pragma Export (Java, SetWriter, "setWriter");
   pragma Export (Java, SetErrorWriter, "setErrorWriter");
   pragma Export (Java, GetReader, "getReader");
   pragma Export (Java, SetReader, "setReader");
   pragma Export (Java, GetScopes, "getScopes");
   pragma Import (Java, ENGINE_SCOPE, "ENGINE_SCOPE");
   pragma Import (Java, GLOBAL_SCOPE, "GLOBAL_SCOPE");

end Javax.Script.ScriptContext;
pragma Import (Java, Javax.Script.ScriptContext, "javax.script.ScriptContext");
pragma Extensions_Allowed (Off);
