pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Javax.Script.Bindings;
limited with Javax.Script.ScriptContext;
limited with Javax.Script.ScriptEngineFactory;
with Java.Lang.Object;

package Javax.Script.ScriptEngine is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Eval (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_ScriptContext : access Standard.Javax.Script.ScriptContext.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                  P2_ScriptContext : access Standard.Javax.Script.ScriptContext.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_Reader : access Standard.Java.Io.Reader.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Bindings : access Standard.Javax.Script.Bindings.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   function Eval (This : access Typ;
                  P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                  P2_Bindings : access Standard.Javax.Script.Bindings.Typ'Class)
                  return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Script.ScriptException.Except

   procedure Put (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function GetBindings (This : access Typ;
                         P1_Int : Java.Int)
                         return access Javax.Script.Bindings.Typ'Class is abstract;

   procedure SetBindings (This : access Typ;
                          P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class;
                          P2_Int : Java.Int) is abstract;

   function CreateBindings (This : access Typ)
                            return access Javax.Script.Bindings.Typ'Class is abstract;

   function GetContext (This : access Typ)
                        return access Javax.Script.ScriptContext.Typ'Class is abstract;

   procedure SetContext (This : access Typ;
                         P1_ScriptContext : access Standard.Javax.Script.ScriptContext.Typ'Class) is abstract;

   function GetFactory (This : access Typ)
                        return access Javax.Script.ScriptEngineFactory.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ARGV : constant access Java.Lang.String.Typ'Class;

   --  final
   FILENAME : constant access Java.Lang.String.Typ'Class;

   --  final
   ENGINE : constant access Java.Lang.String.Typ'Class;

   --  final
   ENGINE_VERSION : constant access Java.Lang.String.Typ'Class;

   --  final
   NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   LANGUAGE : constant access Java.Lang.String.Typ'Class;

   --  final
   LANGUAGE_VERSION : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Eval, "eval");
   pragma Export (Java, Put, "put");
   pragma Export (Java, Get, "get");
   pragma Export (Java, GetBindings, "getBindings");
   pragma Export (Java, SetBindings, "setBindings");
   pragma Export (Java, CreateBindings, "createBindings");
   pragma Export (Java, GetContext, "getContext");
   pragma Export (Java, SetContext, "setContext");
   pragma Export (Java, GetFactory, "getFactory");
   pragma Import (Java, ARGV, "ARGV");
   pragma Import (Java, FILENAME, "FILENAME");
   pragma Import (Java, ENGINE, "ENGINE");
   pragma Import (Java, ENGINE_VERSION, "ENGINE_VERSION");
   pragma Import (Java, NAME, "NAME");
   pragma Import (Java, LANGUAGE, "LANGUAGE");
   pragma Import (Java, LANGUAGE_VERSION, "LANGUAGE_VERSION");

end Javax.Script.ScriptEngine;
pragma Import (Java, Javax.Script.ScriptEngine, "javax.script.ScriptEngine");
pragma Extensions_Allowed (Off);
