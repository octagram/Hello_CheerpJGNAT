pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Script.ScriptEngine;
with Java.Lang.Object;

package Javax.Script.ScriptEngineFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEngineName (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetEngineVersion (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   function GetExtensions (This : access Typ)
                           return access Java.Util.List.Typ'Class is abstract;

   function GetMimeTypes (This : access Typ)
                          return access Java.Util.List.Typ'Class is abstract;

   function GetNames (This : access Typ)
                      return access Java.Util.List.Typ'Class is abstract;

   function GetLanguageName (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetLanguageVersion (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function GetMethodCallSyntax (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String_Arr : access Java.Lang.String.Arr_Obj)
                                 return access Java.Lang.String.Typ'Class is abstract;

   function GetOutputStatement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;

   function GetProgram (This : access Typ;
                        P1_String_Arr : access Java.Lang.String.Arr_Obj)
                        return access Java.Lang.String.Typ'Class is abstract;

   function GetScriptEngine (This : access Typ)
                             return access Javax.Script.ScriptEngine.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetEngineName, "getEngineName");
   pragma Export (Java, GetEngineVersion, "getEngineVersion");
   pragma Export (Java, GetExtensions, "getExtensions");
   pragma Export (Java, GetMimeTypes, "getMimeTypes");
   pragma Export (Java, GetNames, "getNames");
   pragma Export (Java, GetLanguageName, "getLanguageName");
   pragma Export (Java, GetLanguageVersion, "getLanguageVersion");
   pragma Export (Java, GetParameter, "getParameter");
   pragma Export (Java, GetMethodCallSyntax, "getMethodCallSyntax");
   pragma Export (Java, GetOutputStatement, "getOutputStatement");
   pragma Export (Java, GetProgram, "getProgram");
   pragma Export (Java, GetScriptEngine, "getScriptEngine");

end Javax.Script.ScriptEngineFactory;
pragma Import (Java, Javax.Script.ScriptEngineFactory, "javax.script.ScriptEngineFactory");
pragma Extensions_Allowed (Off);
