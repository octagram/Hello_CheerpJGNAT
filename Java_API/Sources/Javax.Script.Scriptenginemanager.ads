pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Script.Bindings;
limited with Javax.Script.ScriptEngine;
limited with Javax.Script.ScriptEngineFactory;
with Java.Lang.Object;

package Javax.Script.ScriptEngineManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ScriptEngineManager (This : Ref := null)
                                     return Ref;

   function New_ScriptEngineManager (P1_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetBindings (This : access Typ;
                          P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class);

   function GetBindings (This : access Typ)
                         return access Javax.Script.Bindings.Typ'Class;

   procedure Put (This : access Typ;
                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Get (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function GetEngineByName (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Script.ScriptEngine.Typ'Class;

   function GetEngineByExtension (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Script.ScriptEngine.Typ'Class;

   function GetEngineByMimeType (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Javax.Script.ScriptEngine.Typ'Class;

   function GetEngineFactories (This : access Typ)
                                return access Java.Util.List.Typ'Class;

   procedure RegisterEngineName (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_ScriptEngineFactory : access Standard.Javax.Script.ScriptEngineFactory.Typ'Class);

   procedure RegisterEngineMimeType (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_ScriptEngineFactory : access Standard.Javax.Script.ScriptEngineFactory.Typ'Class);

   procedure RegisterEngineExtension (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_ScriptEngineFactory : access Standard.Javax.Script.ScriptEngineFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ScriptEngineManager);
   pragma Import (Java, SetBindings, "setBindings");
   pragma Import (Java, GetBindings, "getBindings");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Get, "get");
   pragma Import (Java, GetEngineByName, "getEngineByName");
   pragma Import (Java, GetEngineByExtension, "getEngineByExtension");
   pragma Import (Java, GetEngineByMimeType, "getEngineByMimeType");
   pragma Import (Java, GetEngineFactories, "getEngineFactories");
   pragma Import (Java, RegisterEngineName, "registerEngineName");
   pragma Import (Java, RegisterEngineMimeType, "registerEngineMimeType");
   pragma Import (Java, RegisterEngineExtension, "registerEngineExtension");

end Javax.Script.ScriptEngineManager;
pragma Import (Java, Javax.Script.ScriptEngineManager, "javax.script.ScriptEngineManager");
pragma Extensions_Allowed (Off);
