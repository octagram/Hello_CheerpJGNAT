pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Script.ScriptException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ScriptException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_ScriptException (P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_ScriptException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_ScriptException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetLineNumber (This : access Typ)
                           return Java.Int;

   function GetColumnNumber (This : access Typ)
                             return Java.Int;

   function GetFileName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.script.ScriptException");
   pragma Java_Constructor (New_ScriptException);
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, GetLineNumber, "getLineNumber");
   pragma Import (Java, GetColumnNumber, "getColumnNumber");
   pragma Import (Java, GetFileName, "getFileName");

end Javax.Script.ScriptException;
pragma Import (Java, Javax.Script.ScriptException, "javax.script.ScriptException");
pragma Extensions_Allowed (Off);
