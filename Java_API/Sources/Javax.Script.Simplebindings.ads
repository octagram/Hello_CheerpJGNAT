pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Collection;
limited with Java.Util.Map;
limited with Java.Util.Set;
with Java.Lang.Object;
with Javax.Script.Bindings;

package Javax.Script.SimpleBindings is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Bindings_I : Javax.Script.Bindings.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleBindings (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_SimpleBindings (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Put (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   procedure Clear (This : access Typ);

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   function Size (This : access Typ)
                  return Java.Int;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleBindings);
   pragma Import (Java, Put, "put");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, Get, "get");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Values, "values");

end Javax.Script.SimpleBindings;
pragma Import (Java, Javax.Script.SimpleBindings, "javax.script.SimpleBindings");
pragma Extensions_Allowed (Off);
