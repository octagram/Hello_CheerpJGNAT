pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Script.Bindings;
with Java.Lang.Object;
with Javax.Script.ScriptContext;

package Javax.Script.SimpleScriptContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ScriptContext_I : Javax.Script.ScriptContext.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Writer : access Java.Io.Writer.Typ'Class;
      pragma Import (Java, Writer, "writer");

      --  protected
      ErrorWriter : access Java.Io.Writer.Typ'Class;
      pragma Import (Java, ErrorWriter, "errorWriter");

      --  protected
      Reader : access Java.Io.Reader.Typ'Class;
      pragma Import (Java, Reader, "reader");

      --  protected
      EngineScope : access Javax.Script.Bindings.Typ'Class;
      pragma Import (Java, EngineScope, "engineScope");

      --  protected
      GlobalScope : access Javax.Script.Bindings.Typ'Class;
      pragma Import (Java, GlobalScope, "globalScope");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleScriptContext (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetBindings (This : access Typ;
                          P1_Bindings : access Standard.Javax.Script.Bindings.Typ'Class;
                          P2_Int : Java.Int);

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Lang.Object.Typ'Class;

   function RemoveAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Int : Java.Int)
                             return access Java.Lang.Object.Typ'Class;

   procedure SetAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P3_Int : Java.Int);

   function GetWriter (This : access Typ)
                       return access Java.Io.Writer.Typ'Class;

   function GetReader (This : access Typ)
                       return access Java.Io.Reader.Typ'Class;

   procedure SetReader (This : access Typ;
                        P1_Reader : access Standard.Java.Io.Reader.Typ'Class);

   procedure SetWriter (This : access Typ;
                        P1_Writer : access Standard.Java.Io.Writer.Typ'Class);

   function GetErrorWriter (This : access Typ)
                            return access Java.Io.Writer.Typ'Class;

   procedure SetErrorWriter (This : access Typ;
                             P1_Writer : access Standard.Java.Io.Writer.Typ'Class);

   function GetAttributesScope (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int;

   function GetBindings (This : access Typ;
                         P1_Int : Java.Int)
                         return access Javax.Script.Bindings.Typ'Class;

   function GetScopes (This : access Typ)
                       return access Java.Util.List.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleScriptContext);
   pragma Import (Java, SetBindings, "setBindings");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, SetAttribute, "setAttribute");
   pragma Import (Java, GetWriter, "getWriter");
   pragma Import (Java, GetReader, "getReader");
   pragma Import (Java, SetReader, "setReader");
   pragma Import (Java, SetWriter, "setWriter");
   pragma Import (Java, GetErrorWriter, "getErrorWriter");
   pragma Import (Java, SetErrorWriter, "setErrorWriter");
   pragma Import (Java, GetAttributesScope, "getAttributesScope");
   pragma Import (Java, GetBindings, "getBindings");
   pragma Import (Java, GetScopes, "getScopes");

end Javax.Script.SimpleScriptContext;
pragma Import (Java, Javax.Script.SimpleScriptContext, "javax.script.SimpleScriptContext");
pragma Extensions_Allowed (Off);
