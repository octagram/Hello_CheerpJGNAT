pragma Extensions_Allowed (On);
limited with Javax.Security.Auth.Callback.Callback;
with Java.Lang.Object;

package Javax.Security.Auth.Callback.CallbackHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Handle (This : access Typ;
                     P1_Callback_Arr : access Javax.Security.Auth.Callback.Callback.Arr_Obj) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Security.Auth.Callback.UnsupportedCallbackException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Handle, "handle");

end Javax.Security.Auth.Callback.CallbackHandler;
pragma Import (Java, Javax.Security.Auth.Callback.CallbackHandler, "javax.security.auth.callback.CallbackHandler");
pragma Extensions_Allowed (Off);
