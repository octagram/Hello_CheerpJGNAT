pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;

package Javax.Security.Auth.Callback.ChoiceCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChoiceCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String_Arr : access Java.Lang.String.Arr_Obj;
                                P3_Int : Java.Int;
                                P4_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrompt (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetChoices (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function GetDefaultChoice (This : access Typ)
                              return Java.Int;

   function AllowMultipleSelections (This : access Typ)
                                     return Java.Boolean;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int);

   procedure SetSelectedIndexes (This : access Typ;
                                 P1_Int_Arr : Java.Int_Arr);

   function GetSelectedIndexes (This : access Typ)
                                return Java.Int_Arr;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ChoiceCallback);
   pragma Import (Java, GetPrompt, "getPrompt");
   pragma Import (Java, GetChoices, "getChoices");
   pragma Import (Java, GetDefaultChoice, "getDefaultChoice");
   pragma Import (Java, AllowMultipleSelections, "allowMultipleSelections");
   pragma Import (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Import (Java, SetSelectedIndexes, "setSelectedIndexes");
   pragma Import (Java, GetSelectedIndexes, "getSelectedIndexes");

end Javax.Security.Auth.Callback.ChoiceCallback;
pragma Import (Java, Javax.Security.Auth.Callback.ChoiceCallback, "javax.security.auth.callback.ChoiceCallback");
pragma Extensions_Allowed (Off);
