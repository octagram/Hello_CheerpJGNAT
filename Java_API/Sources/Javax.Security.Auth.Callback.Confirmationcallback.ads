pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;

package Javax.Security.Auth.Callback.ConfirmationCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ConfirmationCallback (P1_Int : Java.Int;
                                      P2_Int : Java.Int;
                                      P3_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   function New_ConfirmationCallback (P1_Int : Java.Int;
                                      P2_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P3_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   function New_ConfirmationCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Int : Java.Int;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   function New_ConfirmationCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Int : Java.Int;
                                      P3_String_Arr : access Java.Lang.String.Arr_Obj;
                                      P4_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrompt (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetMessageType (This : access Typ)
                            return Java.Int;

   function GetOptionType (This : access Typ)
                           return Java.Int;

   function GetOptions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function GetDefaultOption (This : access Typ)
                              return Java.Int;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int);

   function GetSelectedIndex (This : access Typ)
                              return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNSPECIFIED_OPTION : constant Java.Int;

   --  final
   YES_NO_OPTION : constant Java.Int;

   --  final
   YES_NO_CANCEL_OPTION : constant Java.Int;

   --  final
   OK_CANCEL_OPTION : constant Java.Int;

   --  final
   YES : constant Java.Int;

   --  final
   NO : constant Java.Int;

   --  final
   CANCEL : constant Java.Int;

   --  final
   OK : constant Java.Int;

   --  final
   INFORMATION : constant Java.Int;

   --  final
   WARNING : constant Java.Int;

   --  final
   ERROR : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConfirmationCallback);
   pragma Import (Java, GetPrompt, "getPrompt");
   pragma Import (Java, GetMessageType, "getMessageType");
   pragma Import (Java, GetOptionType, "getOptionType");
   pragma Import (Java, GetOptions, "getOptions");
   pragma Import (Java, GetDefaultOption, "getDefaultOption");
   pragma Import (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Import (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Import (Java, UNSPECIFIED_OPTION, "UNSPECIFIED_OPTION");
   pragma Import (Java, YES_NO_OPTION, "YES_NO_OPTION");
   pragma Import (Java, YES_NO_CANCEL_OPTION, "YES_NO_CANCEL_OPTION");
   pragma Import (Java, OK_CANCEL_OPTION, "OK_CANCEL_OPTION");
   pragma Import (Java, YES, "YES");
   pragma Import (Java, NO, "NO");
   pragma Import (Java, CANCEL, "CANCEL");
   pragma Import (Java, OK, "OK");
   pragma Import (Java, INFORMATION, "INFORMATION");
   pragma Import (Java, WARNING, "WARNING");
   pragma Import (Java, ERROR, "ERROR");

end Javax.Security.Auth.Callback.ConfirmationCallback;
pragma Import (Java, Javax.Security.Auth.Callback.ConfirmationCallback, "javax.security.auth.callback.ConfirmationCallback");
pragma Extensions_Allowed (Off);
