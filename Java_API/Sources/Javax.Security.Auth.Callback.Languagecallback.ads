pragma Extensions_Allowed (On);
limited with Java.Util.Locale;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;

package Javax.Security.Auth.Callback.LanguageCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LanguageCallback (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LanguageCallback);
   pragma Import (Java, SetLocale, "setLocale");
   pragma Import (Java, GetLocale, "getLocale");

end Javax.Security.Auth.Callback.LanguageCallback;
pragma Import (Java, Javax.Security.Auth.Callback.LanguageCallback, "javax.security.auth.callback.LanguageCallback");
pragma Extensions_Allowed (Off);
