pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;

package Javax.Security.Auth.Callback.NameCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NameCallback (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_NameCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrompt (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetDefaultName (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NameCallback);
   pragma Import (Java, GetPrompt, "getPrompt");
   pragma Import (Java, GetDefaultName, "getDefaultName");
   pragma Import (Java, SetName, "setName");
   pragma Import (Java, GetName, "getName");

end Javax.Security.Auth.Callback.NameCallback;
pragma Import (Java, Javax.Security.Auth.Callback.NameCallback, "javax.security.auth.callback.NameCallback");
pragma Extensions_Allowed (Off);
