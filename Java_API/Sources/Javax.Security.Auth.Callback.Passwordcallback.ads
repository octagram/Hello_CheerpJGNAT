pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;

package Javax.Security.Auth.Callback.PasswordCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PasswordCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Boolean : Java.Boolean; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrompt (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function IsEchoOn (This : access Typ)
                      return Java.Boolean;

   procedure SetPassword (This : access Typ;
                          P1_Char_Arr : Java.Char_Arr);

   function GetPassword (This : access Typ)
                         return Java.Char_Arr;

   procedure ClearPassword (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PasswordCallback);
   pragma Import (Java, GetPrompt, "getPrompt");
   pragma Import (Java, IsEchoOn, "isEchoOn");
   pragma Import (Java, SetPassword, "setPassword");
   pragma Import (Java, GetPassword, "getPassword");
   pragma Import (Java, ClearPassword, "clearPassword");

end Javax.Security.Auth.Callback.PasswordCallback;
pragma Import (Java, Javax.Security.Auth.Callback.PasswordCallback, "javax.security.auth.callback.PasswordCallback");
pragma Extensions_Allowed (Off);
