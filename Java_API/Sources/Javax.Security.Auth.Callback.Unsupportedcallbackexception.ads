pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Security.Auth.Callback.Callback;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Security.Auth.Callback.UnsupportedCallbackException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnsupportedCallbackException (P1_Callback : access Standard.Javax.Security.Auth.Callback.Callback.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   function New_UnsupportedCallbackException (P1_Callback : access Standard.Javax.Security.Auth.Callback.Callback.Typ'Class;
                                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                              This : Ref := null)
                                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCallback (This : access Typ)
                         return access Javax.Security.Auth.Callback.Callback.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.security.auth.callback.UnsupportedCallbackException");
   pragma Java_Constructor (New_UnsupportedCallbackException);
   pragma Import (Java, GetCallback, "getCallback");

end Javax.Security.Auth.Callback.UnsupportedCallbackException;
pragma Import (Java, Javax.Security.Auth.Callback.UnsupportedCallbackException, "javax.security.auth.callback.UnsupportedCallbackException");
pragma Extensions_Allowed (Off);
