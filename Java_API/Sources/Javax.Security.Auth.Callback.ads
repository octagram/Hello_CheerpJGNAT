pragma Extensions_Allowed (On);
package Javax.Security.Auth.Callback is
   pragma Preelaborate;
end Javax.Security.Auth.Callback;
pragma Import (Java, Javax.Security.Auth.Callback, "javax.security.auth.callback");
pragma Extensions_Allowed (Off);
