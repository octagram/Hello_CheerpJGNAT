pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Security.Auth.Destroyable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Destroy (This : access Typ) is abstract;
   --  can raise Javax.Security.Auth.DestroyFailedException.Except

   function IsDestroyed (This : access Typ)
                         return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Destroy, "destroy");
   pragma Export (Java, IsDestroyed, "isDestroyed");

end Javax.Security.Auth.Destroyable;
pragma Import (Java, Javax.Security.Auth.Destroyable, "javax.security.auth.Destroyable");
pragma Extensions_Allowed (Off);
