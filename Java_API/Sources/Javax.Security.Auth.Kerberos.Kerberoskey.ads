pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Security.Auth.Kerberos.KerberosPrincipal;
with Java.Lang.Object;
with Javax.Crypto.SecretKey;
with Javax.Security.Auth.Destroyable;

package Javax.Security.Auth.Kerberos.KerberosKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SecretKey_I : Javax.Crypto.SecretKey.Ref;
            Destroyable_I : Javax.Security.Auth.Destroyable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KerberosKey (P1_KerberosPrincipal : access Standard.Javax.Security.Auth.Kerberos.KerberosPrincipal.Typ'Class;
                             P2_Byte_Arr : Java.Byte_Arr;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_KerberosKey (P1_KerberosPrincipal : access Standard.Javax.Security.Auth.Kerberos.KerberosPrincipal.Typ'Class;
                             P2_Char_Arr : Java.Char_Arr;
                             P3_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetPrincipal (This : access Typ)
                          return access Javax.Security.Auth.Kerberos.KerberosPrincipal.Typ'Class;

   --  final
   function GetVersionNumber (This : access Typ)
                              return Java.Int;

   --  final
   function GetKeyType (This : access Typ)
                        return Java.Int;

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function GetFormat (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   --  final
   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr;

   procedure Destroy (This : access Typ);
   --  can raise Javax.Security.Auth.DestroyFailedException.Except

   function IsDestroyed (This : access Typ)
                         return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KerberosKey);
   pragma Import (Java, GetPrincipal, "getPrincipal");
   pragma Import (Java, GetVersionNumber, "getVersionNumber");
   pragma Import (Java, GetKeyType, "getKeyType");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GetFormat, "getFormat");
   pragma Import (Java, GetEncoded, "getEncoded");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, IsDestroyed, "isDestroyed");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Javax.Security.Auth.Kerberos.KerberosKey;
pragma Import (Java, Javax.Security.Auth.Kerberos.KerberosKey, "javax.security.auth.kerberos.KerberosKey");
pragma Extensions_Allowed (Off);
