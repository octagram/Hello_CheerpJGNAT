pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Principal;

package Javax.Security.Auth.Kerberos.KerberosPrincipal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Principal_I : Java.Security.Principal.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KerberosPrincipal (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_KerberosPrincipal (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRealm (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetNameType (This : access Typ)
                         return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   KRB_NT_UNKNOWN : constant Java.Int;

   --  final
   KRB_NT_PRINCIPAL : constant Java.Int;

   --  final
   KRB_NT_SRV_INST : constant Java.Int;

   --  final
   KRB_NT_SRV_HST : constant Java.Int;

   --  final
   KRB_NT_SRV_XHST : constant Java.Int;

   --  final
   KRB_NT_UID : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KerberosPrincipal);
   pragma Import (Java, GetRealm, "getRealm");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetNameType, "getNameType");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, KRB_NT_UNKNOWN, "KRB_NT_UNKNOWN");
   pragma Import (Java, KRB_NT_PRINCIPAL, "KRB_NT_PRINCIPAL");
   pragma Import (Java, KRB_NT_SRV_INST, "KRB_NT_SRV_INST");
   pragma Import (Java, KRB_NT_SRV_HST, "KRB_NT_SRV_HST");
   pragma Import (Java, KRB_NT_SRV_XHST, "KRB_NT_SRV_XHST");
   pragma Import (Java, KRB_NT_UID, "KRB_NT_UID");

end Javax.Security.Auth.Kerberos.KerberosPrincipal;
pragma Import (Java, Javax.Security.Auth.Kerberos.KerberosPrincipal, "javax.security.auth.kerberos.KerberosPrincipal");
pragma Extensions_Allowed (Off);
