pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.InetAddress;
limited with Java.Util.Date;
limited with Javax.Crypto.SecretKey;
limited with Javax.Security.Auth.Kerberos.KerberosPrincipal;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Destroyable;
with Javax.Security.Auth.Refreshable;

package Javax.Security.Auth.Kerberos.KerberosTicket is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Destroyable_I : Javax.Security.Auth.Destroyable.Ref;
            Refreshable_I : Javax.Security.Auth.Refreshable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KerberosTicket (P1_Byte_Arr : Java.Byte_Arr;
                                P2_KerberosPrincipal : access Standard.Javax.Security.Auth.Kerberos.KerberosPrincipal.Typ'Class;
                                P3_KerberosPrincipal : access Standard.Javax.Security.Auth.Kerberos.KerberosPrincipal.Typ'Class;
                                P4_Byte_Arr : Java.Byte_Arr;
                                P5_Int : Java.Int;
                                P6_Boolean_Arr : Java.Boolean_Arr;
                                P7_Date : access Standard.Java.Util.Date.Typ'Class;
                                P8_Date : access Standard.Java.Util.Date.Typ'Class;
                                P9_Date : access Standard.Java.Util.Date.Typ'Class;
                                P10_Date : access Standard.Java.Util.Date.Typ'Class;
                                P11_InetAddress_Arr : access Java.Net.InetAddress.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetClient (This : access Typ)
                       return access Javax.Security.Auth.Kerberos.KerberosPrincipal.Typ'Class;

   --  final
   function GetServer (This : access Typ)
                       return access Javax.Security.Auth.Kerberos.KerberosPrincipal.Typ'Class;

   --  final
   function GetSessionKey (This : access Typ)
                           return access Javax.Crypto.SecretKey.Typ'Class;

   --  final
   function GetSessionKeyType (This : access Typ)
                               return Java.Int;

   --  final
   function IsForwardable (This : access Typ)
                           return Java.Boolean;

   --  final
   function IsForwarded (This : access Typ)
                         return Java.Boolean;

   --  final
   function IsProxiable (This : access Typ)
                         return Java.Boolean;

   --  final
   function IsProxy (This : access Typ)
                     return Java.Boolean;

   --  final
   function IsPostdated (This : access Typ)
                         return Java.Boolean;

   --  final
   function IsRenewable (This : access Typ)
                         return Java.Boolean;

   --  final
   function IsInitial (This : access Typ)
                       return Java.Boolean;

   --  final
   function GetFlags (This : access Typ)
                      return Java.Boolean_Arr;

   --  final
   function GetAuthTime (This : access Typ)
                         return access Java.Util.Date.Typ'Class;

   --  final
   function GetStartTime (This : access Typ)
                          return access Java.Util.Date.Typ'Class;

   --  final
   function GetEndTime (This : access Typ)
                        return access Java.Util.Date.Typ'Class;

   --  final
   function GetRenewTill (This : access Typ)
                          return access Java.Util.Date.Typ'Class;

   --  final
   function GetClientAddresses (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  final
   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr;

   function IsCurrent (This : access Typ)
                       return Java.Boolean;

   procedure Refresh (This : access Typ);
   --  can raise Javax.Security.Auth.RefreshFailedException.Except

   procedure Destroy (This : access Typ);
   --  can raise Javax.Security.Auth.DestroyFailedException.Except

   function IsDestroyed (This : access Typ)
                         return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KerberosTicket);
   pragma Import (Java, GetClient, "getClient");
   pragma Import (Java, GetServer, "getServer");
   pragma Import (Java, GetSessionKey, "getSessionKey");
   pragma Import (Java, GetSessionKeyType, "getSessionKeyType");
   pragma Import (Java, IsForwardable, "isForwardable");
   pragma Import (Java, IsForwarded, "isForwarded");
   pragma Import (Java, IsProxiable, "isProxiable");
   pragma Import (Java, IsProxy, "isProxy");
   pragma Import (Java, IsPostdated, "isPostdated");
   pragma Import (Java, IsRenewable, "isRenewable");
   pragma Import (Java, IsInitial, "isInitial");
   pragma Import (Java, GetFlags, "getFlags");
   pragma Import (Java, GetAuthTime, "getAuthTime");
   pragma Import (Java, GetStartTime, "getStartTime");
   pragma Import (Java, GetEndTime, "getEndTime");
   pragma Import (Java, GetRenewTill, "getRenewTill");
   pragma Import (Java, GetClientAddresses, "getClientAddresses");
   pragma Import (Java, GetEncoded, "getEncoded");
   pragma Import (Java, IsCurrent, "isCurrent");
   pragma Import (Java, Refresh, "refresh");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, IsDestroyed, "isDestroyed");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Javax.Security.Auth.Kerberos.KerberosTicket;
pragma Import (Java, Javax.Security.Auth.Kerberos.KerberosTicket, "javax.security.auth.kerberos.KerberosTicket");
pragma Extensions_Allowed (Off);
