pragma Extensions_Allowed (On);
package Javax.Security.Auth.Kerberos is
   pragma Preelaborate;
end Javax.Security.Auth.Kerberos;
pragma Import (Java, Javax.Security.Auth.Kerberos, "javax.security.auth.kerberos");
pragma Extensions_Allowed (Off);
