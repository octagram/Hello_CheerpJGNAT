pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   REQUIRED : access Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag.Typ'Class;

   --  final
   REQUISITE : access Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag.Typ'Class;

   --  final
   SUFFICIENT : access Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag.Typ'Class;

   --  final
   OPTIONAL : access Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, REQUIRED, "REQUIRED");
   pragma Import (Java, REQUISITE, "REQUISITE");
   pragma Import (Java, SUFFICIENT, "SUFFICIENT");
   pragma Import (Java, OPTIONAL, "OPTIONAL");

end Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag;
pragma Import (Java, Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag, "javax.security.auth.login.AppConfigurationEntry$LoginModuleControlFlag");
pragma Extensions_Allowed (Off);
