pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag;
with Java.Lang.Object;

package Javax.Security.Auth.Login.AppConfigurationEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AppConfigurationEntry (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_LoginModuleControlFlag : access Standard.Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag.Typ'Class;
                                       P3_Map : access Standard.Java.Util.Map.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLoginModuleName (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function GetControlFlag (This : access Typ)
                            return access Javax.Security.Auth.Login.AppConfigurationEntry.LoginModuleControlFlag.Typ'Class;

   function GetOptions (This : access Typ)
                        return access Java.Util.Map.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AppConfigurationEntry);
   pragma Import (Java, GetLoginModuleName, "getLoginModuleName");
   pragma Import (Java, GetControlFlag, "getControlFlag");
   pragma Import (Java, GetOptions, "getOptions");

end Javax.Security.Auth.Login.AppConfigurationEntry;
pragma Import (Java, Javax.Security.Auth.Login.AppConfigurationEntry, "javax.security.auth.login.AppConfigurationEntry");
pragma Extensions_Allowed (Off);
