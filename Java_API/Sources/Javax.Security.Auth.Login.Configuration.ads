pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Javax.Security.Auth.Login.AppConfigurationEntry;
limited with Javax.Security.Auth.Login.Configuration.Parameters;
with Java.Lang.Object;

package Javax.Security.Auth.Login.Configuration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Configuration (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetConfiguration return access Javax.Security.Auth.Login.Configuration.Typ'Class;

   procedure SetConfiguration (P1_Configuration : access Standard.Javax.Security.Auth.Login.Configuration.Typ'Class);

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Parameters : access Standard.Javax.Security.Auth.Login.Configuration.Parameters.Typ'Class)
                         return access Javax.Security.Auth.Login.Configuration.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Parameters : access Standard.Javax.Security.Auth.Login.Configuration.Parameters.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Security.Auth.Login.Configuration.Typ'Class;
   --  can raise Java.Security.NoSuchProviderException.Except and
   --  Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Parameters : access Standard.Javax.Security.Auth.Login.Configuration.Parameters.Typ'Class;
                         P3_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Javax.Security.Auth.Login.Configuration.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetParameters (This : access Typ)
                           return access Javax.Security.Auth.Login.Configuration.Parameters.Typ'Class;

   function GetAppConfigurationEntry (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return Standard.Java.Lang.Object.Ref is abstract;

   procedure Refresh (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Configuration);
   pragma Import (Java, GetConfiguration, "getConfiguration");
   pragma Import (Java, SetConfiguration, "setConfiguration");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetParameters, "getParameters");
   pragma Export (Java, GetAppConfigurationEntry, "getAppConfigurationEntry");
   pragma Import (Java, Refresh, "refresh");

end Javax.Security.Auth.Login.Configuration;
pragma Import (Java, Javax.Security.Auth.Login.Configuration, "javax.security.auth.login.Configuration");
pragma Extensions_Allowed (Off);
