pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Security.Auth.Login.AppConfigurationEntry;
with Java.Lang.Object;

package Javax.Security.Auth.Login.ConfigurationSpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ConfigurationSpi (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function EngineGetAppConfigurationEntry (This : access Typ;
                                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                                            return Standard.Java.Lang.Object.Ref is abstract;

   --  protected
   procedure EngineRefresh (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConfigurationSpi);
   pragma Export (Java, EngineGetAppConfigurationEntry, "engineGetAppConfigurationEntry");
   pragma Export (Java, EngineRefresh, "engineRefresh");

end Javax.Security.Auth.Login.ConfigurationSpi;
pragma Import (Java, Javax.Security.Auth.Login.ConfigurationSpi, "javax.security.auth.login.ConfigurationSpi");
pragma Extensions_Allowed (Off);
