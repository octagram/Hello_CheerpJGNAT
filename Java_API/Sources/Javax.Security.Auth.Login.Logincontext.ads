pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Security.Auth.Callback.CallbackHandler;
limited with Javax.Security.Auth.Login.Configuration;
limited with Javax.Security.Auth.Subject;
with Java.Lang.Object;

package Javax.Security.Auth.Login.LoginContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LoginContext (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function New_LoginContext (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function New_LoginContext (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function New_LoginContext (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                              P3_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function New_LoginContext (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                              P3_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class;
                              P4_Configuration : access Standard.Javax.Security.Auth.Login.Configuration.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Login (This : access Typ);
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   procedure Logout (This : access Typ);
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function GetSubject (This : access Typ)
                        return access Javax.Security.Auth.Subject.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LoginContext);
   pragma Import (Java, Login, "login");
   pragma Import (Java, Logout, "logout");
   pragma Import (Java, GetSubject, "getSubject");

end Javax.Security.Auth.Login.LoginContext;
pragma Import (Java, Javax.Security.Auth.Login.LoginContext, "javax.security.auth.login.LoginContext");
pragma Extensions_Allowed (Off);
