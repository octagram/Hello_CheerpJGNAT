pragma Extensions_Allowed (On);
package Javax.Security.Auth.Login is
   pragma Preelaborate;
end Javax.Security.Auth.Login;
pragma Import (Java, Javax.Security.Auth.Login, "javax.security.auth.login");
pragma Extensions_Allowed (Off);
