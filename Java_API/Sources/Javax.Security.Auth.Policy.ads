pragma Extensions_Allowed (On);
limited with Java.Security.CodeSource;
limited with Java.Security.PermissionCollection;
limited with Javax.Security.Auth.Subject;
with Java.Lang.Object;

package Javax.Security.Auth.Policy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Policy (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPolicy return access Javax.Security.Auth.Policy.Typ'Class;

   procedure SetPolicy (P1_Policy : access Standard.Javax.Security.Auth.Policy.Typ'Class);

   function GetPermissions (This : access Typ;
                            P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                            P2_CodeSource : access Standard.Java.Security.CodeSource.Typ'Class)
                            return access Java.Security.PermissionCollection.Typ'Class is abstract;

   procedure Refresh (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Policy);
   pragma Export (Java, GetPolicy, "getPolicy");
   pragma Export (Java, SetPolicy, "setPolicy");
   pragma Export (Java, GetPermissions, "getPermissions");
   pragma Export (Java, Refresh, "refresh");

end Javax.Security.Auth.Policy;
pragma Import (Java, Javax.Security.Auth.Policy, "javax.security.auth.Policy");
pragma Extensions_Allowed (Off);
