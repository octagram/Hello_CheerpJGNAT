pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Security.Auth.Refreshable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsCurrent (This : access Typ)
                       return Java.Boolean is abstract;

   procedure Refresh (This : access Typ) is abstract;
   --  can raise Javax.Security.Auth.RefreshFailedException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsCurrent, "isCurrent");
   pragma Export (Java, Refresh, "refresh");

end Javax.Security.Auth.Refreshable;
pragma Import (Java, Javax.Security.Auth.Refreshable, "javax.security.auth.Refreshable");
pragma Extensions_Allowed (Off);
