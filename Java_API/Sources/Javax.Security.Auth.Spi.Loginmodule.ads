pragma Extensions_Allowed (On);
limited with Java.Util.Map;
limited with Javax.Security.Auth.Callback.CallbackHandler;
limited with Javax.Security.Auth.Subject;
with Java.Lang.Object;

package Javax.Security.Auth.Spi.LoginModule is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Initialize (This : access Typ;
                         P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                         P2_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class;
                         P3_Map : access Standard.Java.Util.Map.Typ'Class;
                         P4_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;

   function Login (This : access Typ)
                   return Java.Boolean is abstract;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function Commit (This : access Typ)
                    return Java.Boolean is abstract;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function abort_K (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Javax.Security.Auth.Login.LoginException.Except

   function Logout (This : access Typ)
                    return Java.Boolean is abstract;
   --  can raise Javax.Security.Auth.Login.LoginException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Initialize, "initialize");
   pragma Export (Java, Login, "login");
   pragma Export (Java, Commit, "commit");
   pragma Export (Java, abort_K, "abort");
   pragma Export (Java, Logout, "logout");

end Javax.Security.Auth.Spi.LoginModule;
pragma Import (Java, Javax.Security.Auth.Spi.LoginModule, "javax.security.auth.spi.LoginModule");
pragma Extensions_Allowed (Off);
