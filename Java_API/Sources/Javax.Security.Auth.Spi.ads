pragma Extensions_Allowed (On);
package Javax.Security.Auth.Spi is
   pragma Preelaborate;
end Javax.Security.Auth.Spi;
pragma Import (Java, Javax.Security.Auth.Spi, "javax.security.auth.spi");
pragma Extensions_Allowed (Off);
