pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Security.AccessControlContext;
limited with Java.Security.PrivilegedAction;
limited with Java.Security.PrivilegedExceptionAction;
limited with Java.Util.Set;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Security.Auth.Subject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Subject (This : Ref := null)
                         return Ref;

   function New_Subject (P1_Boolean : Java.Boolean;
                         P2_Set : access Standard.Java.Util.Set.Typ'Class;
                         P3_Set : access Standard.Java.Util.Set.Typ'Class;
                         P4_Set : access Standard.Java.Util.Set.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetReadOnly (This : access Typ);

   function IsReadOnly (This : access Typ)
                        return Java.Boolean;

   function GetSubject (P1_AccessControlContext : access Standard.Java.Security.AccessControlContext.Typ'Class)
                        return access Javax.Security.Auth.Subject.Typ'Class;

   function DoAs (P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                  P2_PrivilegedAction : access Standard.Java.Security.PrivilegedAction.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;

   function DoAs (P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                  P2_PrivilegedExceptionAction : access Standard.Java.Security.PrivilegedExceptionAction.Typ'Class)
                  return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Security.PrivilegedActionException.Except

   function DoAsPrivileged (P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                            P2_PrivilegedAction : access Standard.Java.Security.PrivilegedAction.Typ'Class;
                            P3_AccessControlContext : access Standard.Java.Security.AccessControlContext.Typ'Class)
                            return access Java.Lang.Object.Typ'Class;

   function DoAsPrivileged (P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class;
                            P2_PrivilegedExceptionAction : access Standard.Java.Security.PrivilegedExceptionAction.Typ'Class;
                            P3_AccessControlContext : access Standard.Java.Security.AccessControlContext.Typ'Class)
                            return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Security.PrivilegedActionException.Except

   function GetPrincipals (This : access Typ)
                           return access Java.Util.Set.Typ'Class;

   function GetPrincipals (This : access Typ;
                           P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                           return access Java.Util.Set.Typ'Class;

   function GetPublicCredentials (This : access Typ)
                                  return access Java.Util.Set.Typ'Class;

   function GetPrivateCredentials (This : access Typ)
                                   return access Java.Util.Set.Typ'Class;

   function GetPublicCredentials (This : access Typ;
                                  P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                  return access Java.Util.Set.Typ'Class;

   function GetPrivateCredentials (This : access Typ;
                                   P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                   return access Java.Util.Set.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Subject);
   pragma Import (Java, SetReadOnly, "setReadOnly");
   pragma Import (Java, IsReadOnly, "isReadOnly");
   pragma Import (Java, GetSubject, "getSubject");
   pragma Import (Java, DoAs, "doAs");
   pragma Import (Java, DoAsPrivileged, "doAsPrivileged");
   pragma Import (Java, GetPrincipals, "getPrincipals");
   pragma Import (Java, GetPublicCredentials, "getPublicCredentials");
   pragma Import (Java, GetPrivateCredentials, "getPrivateCredentials");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Security.Auth.Subject;
pragma Import (Java, Javax.Security.Auth.Subject, "javax.security.auth.Subject");
pragma Extensions_Allowed (Off);
