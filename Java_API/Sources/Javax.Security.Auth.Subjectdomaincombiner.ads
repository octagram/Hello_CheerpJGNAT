pragma Extensions_Allowed (On);
limited with Java.Security.ProtectionDomain;
limited with Javax.Security.Auth.Subject;
with Java.Lang.Object;
with Java.Security.DomainCombiner;

package Javax.Security.Auth.SubjectDomainCombiner is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DomainCombiner_I : Java.Security.DomainCombiner.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SubjectDomainCombiner (P1_Subject : access Standard.Javax.Security.Auth.Subject.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSubject (This : access Typ)
                        return access Javax.Security.Auth.Subject.Typ'Class;

   function Combine (This : access Typ;
                     P1_ProtectionDomain_Arr : access Java.Security.ProtectionDomain.Arr_Obj;
                     P2_ProtectionDomain_Arr : access Java.Security.ProtectionDomain.Arr_Obj)
                     return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SubjectDomainCombiner);
   pragma Import (Java, GetSubject, "getSubject");
   pragma Import (Java, Combine, "combine");

end Javax.Security.Auth.SubjectDomainCombiner;
pragma Import (Java, Javax.Security.Auth.SubjectDomainCombiner, "javax.security.auth.SubjectDomainCombiner");
pragma Extensions_Allowed (Off);
