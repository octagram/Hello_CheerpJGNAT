pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Map;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Security.Principal;

package Javax.Security.Auth.X500.X500Principal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Principal_I : Java.Security.Principal.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_X500Principal (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_X500Principal (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_X500Principal (P1_Byte_Arr : Java.Byte_Arr; 
                               This : Ref := null)
                               return Ref;

   function New_X500Principal (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Map : access Standard.Java.Util.Map.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetEncoded (This : access Typ)
                        return Java.Byte_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RFC1779 : constant access Java.Lang.String.Typ'Class;

   --  final
   RFC2253 : constant access Java.Lang.String.Typ'Class;

   --  final
   CANONICAL : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X500Principal);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetEncoded, "getEncoded");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, RFC1779, "RFC1779");
   pragma Import (Java, RFC2253, "RFC2253");
   pragma Import (Java, CANONICAL, "CANONICAL");

end Javax.Security.Auth.X500.X500Principal;
pragma Import (Java, Javax.Security.Auth.X500.X500Principal, "javax.security.auth.x500.X500Principal");
pragma Extensions_Allowed (Off);
