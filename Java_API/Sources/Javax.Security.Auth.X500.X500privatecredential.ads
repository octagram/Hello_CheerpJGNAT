pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Cert.X509Certificate;
limited with Java.Security.PrivateKey;
with Java.Lang.Object;
with Javax.Security.Auth.Destroyable;

package Javax.Security.Auth.X500.X500PrivateCredential is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Destroyable_I : Javax.Security.Auth.Destroyable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_X500PrivateCredential (P1_X509Certificate : access Standard.Java.Security.Cert.X509Certificate.Typ'Class;
                                       P2_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_X500PrivateCredential (P1_X509Certificate : access Standard.Java.Security.Cert.X509Certificate.Typ'Class;
                                       P2_PrivateKey : access Standard.Java.Security.PrivateKey.Typ'Class;
                                       P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCertificate (This : access Typ)
                            return access Java.Security.Cert.X509Certificate.Typ'Class;

   function GetPrivateKey (This : access Typ)
                           return access Java.Security.PrivateKey.Typ'Class;

   function GetAlias (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure Destroy (This : access Typ);

   function IsDestroyed (This : access Typ)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_X500PrivateCredential);
   pragma Import (Java, GetCertificate, "getCertificate");
   pragma Import (Java, GetPrivateKey, "getPrivateKey");
   pragma Import (Java, GetAlias, "getAlias");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, IsDestroyed, "isDestroyed");

end Javax.Security.Auth.X500.X500PrivateCredential;
pragma Import (Java, Javax.Security.Auth.X500.X500PrivateCredential, "javax.security.auth.x500.X500PrivateCredential");
pragma Extensions_Allowed (Off);
