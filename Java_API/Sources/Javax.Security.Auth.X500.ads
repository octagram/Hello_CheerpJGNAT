pragma Extensions_Allowed (On);
package Javax.Security.Auth.X500 is
   pragma Preelaborate;
end Javax.Security.Auth.X500;
pragma Import (Java, Javax.Security.Auth.X500, "javax.security.auth.x500");
pragma Extensions_Allowed (Off);
