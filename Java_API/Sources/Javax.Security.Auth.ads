pragma Extensions_Allowed (On);
package Javax.Security.Auth is
   pragma Preelaborate;
end Javax.Security.Auth;
pragma Import (Java, Javax.Security.Auth, "javax.security.auth");
pragma Extensions_Allowed (Off);
