pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Sasl.SaslException;

package Javax.Security.Sasl.AuthenticationException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Security.Sasl.SaslException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AuthenticationException (This : Ref := null)
                                         return Ref;

   function New_AuthenticationException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_AuthenticationException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.security.sasl.AuthenticationException");
   pragma Java_Constructor (New_AuthenticationException);

end Javax.Security.Sasl.AuthenticationException;
pragma Import (Java, Javax.Security.Sasl.AuthenticationException, "javax.security.sasl.AuthenticationException");
pragma Extensions_Allowed (Off);
