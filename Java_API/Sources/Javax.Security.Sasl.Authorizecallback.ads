pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;

package Javax.Security.Sasl.AuthorizeCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AuthorizeCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAuthenticationID (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetAuthorizationID (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function IsAuthorized (This : access Typ)
                          return Java.Boolean;

   procedure SetAuthorized (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function GetAuthorizedID (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   procedure SetAuthorizedID (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AuthorizeCallback);
   pragma Import (Java, GetAuthenticationID, "getAuthenticationID");
   pragma Import (Java, GetAuthorizationID, "getAuthorizationID");
   pragma Import (Java, IsAuthorized, "isAuthorized");
   pragma Import (Java, SetAuthorized, "setAuthorized");
   pragma Import (Java, GetAuthorizedID, "getAuthorizedID");
   pragma Import (Java, SetAuthorizedID, "setAuthorizedID");

end Javax.Security.Sasl.AuthorizeCallback;
pragma Import (Java, Javax.Security.Sasl.AuthorizeCallback, "javax.security.sasl.AuthorizeCallback");
pragma Extensions_Allowed (Off);
