pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;
with Javax.Security.Auth.Callback.TextInputCallback;

package Javax.Security.Sasl.RealmCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Javax.Security.Auth.Callback.TextInputCallback.Typ(Serializable_I,
                                                              Callback_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RealmCallback (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_RealmCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RealmCallback);

end Javax.Security.Sasl.RealmCallback;
pragma Import (Java, Javax.Security.Sasl.RealmCallback, "javax.security.sasl.RealmCallback");
pragma Extensions_Allowed (Off);
