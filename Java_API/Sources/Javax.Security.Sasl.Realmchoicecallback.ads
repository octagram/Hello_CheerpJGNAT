pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Security.Auth.Callback.Callback;
with Javax.Security.Auth.Callback.ChoiceCallback;

package Javax.Security.Sasl.RealmChoiceCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Callback_I : Javax.Security.Auth.Callback.Callback.Ref)
    is new Javax.Security.Auth.Callback.ChoiceCallback.Typ(Serializable_I,
                                                           Callback_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RealmChoiceCallback (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_String_Arr : access Java.Lang.String.Arr_Obj;
                                     P3_Int : Java.Int;
                                     P4_Boolean : Java.Boolean; 
                                     This : Ref := null)
                                     return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RealmChoiceCallback);

end Javax.Security.Sasl.RealmChoiceCallback;
pragma Import (Java, Javax.Security.Sasl.RealmChoiceCallback, "javax.security.sasl.RealmChoiceCallback");
pragma Extensions_Allowed (Off);
