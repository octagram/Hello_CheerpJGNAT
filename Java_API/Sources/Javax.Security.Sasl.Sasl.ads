pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Map;
limited with Javax.Security.Auth.Callback.CallbackHandler;
limited with Javax.Security.Sasl.SaslClient;
limited with Javax.Security.Sasl.SaslServer;
with Java.Lang.Object;

package Javax.Security.Sasl.Sasl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateSaslClient (P1_String_Arr : access Java.Lang.String.Arr_Obj;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_String : access Standard.Java.Lang.String.Typ'Class;
                              P5_Map : access Standard.Java.Util.Map.Typ'Class;
                              P6_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class)
                              return access Javax.Security.Sasl.SaslClient.Typ'Class;
   --  can raise Javax.Security.Sasl.SaslException.Except

   function CreateSaslServer (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_Map : access Standard.Java.Util.Map.Typ'Class;
                              P5_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class)
                              return access Javax.Security.Sasl.SaslServer.Typ'Class;
   --  can raise Javax.Security.Sasl.SaslException.Except

   function GetSaslClientFactories return access Java.Util.Enumeration.Typ'Class;

   function GetSaslServerFactories return access Java.Util.Enumeration.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   QOP : constant access Java.Lang.String.Typ'Class;

   --  final
   STRENGTH : constant access Java.Lang.String.Typ'Class;

   --  final
   SERVER_AUTH : constant access Java.Lang.String.Typ'Class;

   --  final
   MAX_BUFFER : constant access Java.Lang.String.Typ'Class;

   --  final
   RAW_SEND_SIZE : constant access Java.Lang.String.Typ'Class;

   --  final
   REUSE : constant access Java.Lang.String.Typ'Class;

   --  final
   POLICY_NOPLAINTEXT : constant access Java.Lang.String.Typ'Class;

   --  final
   POLICY_NOACTIVE : constant access Java.Lang.String.Typ'Class;

   --  final
   POLICY_NODICTIONARY : constant access Java.Lang.String.Typ'Class;

   --  final
   POLICY_NOANONYMOUS : constant access Java.Lang.String.Typ'Class;

   --  final
   POLICY_FORWARD_SECRECY : constant access Java.Lang.String.Typ'Class;

   --  final
   POLICY_PASS_CREDENTIALS : constant access Java.Lang.String.Typ'Class;

   --  final
   CREDENTIALS : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateSaslClient, "createSaslClient");
   pragma Import (Java, CreateSaslServer, "createSaslServer");
   pragma Import (Java, GetSaslClientFactories, "getSaslClientFactories");
   pragma Import (Java, GetSaslServerFactories, "getSaslServerFactories");
   pragma Import (Java, QOP, "QOP");
   pragma Import (Java, STRENGTH, "STRENGTH");
   pragma Import (Java, SERVER_AUTH, "SERVER_AUTH");
   pragma Import (Java, MAX_BUFFER, "MAX_BUFFER");
   pragma Import (Java, RAW_SEND_SIZE, "RAW_SEND_SIZE");
   pragma Import (Java, REUSE, "REUSE");
   pragma Import (Java, POLICY_NOPLAINTEXT, "POLICY_NOPLAINTEXT");
   pragma Import (Java, POLICY_NOACTIVE, "POLICY_NOACTIVE");
   pragma Import (Java, POLICY_NODICTIONARY, "POLICY_NODICTIONARY");
   pragma Import (Java, POLICY_NOANONYMOUS, "POLICY_NOANONYMOUS");
   pragma Import (Java, POLICY_FORWARD_SECRECY, "POLICY_FORWARD_SECRECY");
   pragma Import (Java, POLICY_PASS_CREDENTIALS, "POLICY_PASS_CREDENTIALS");
   pragma Import (Java, CREDENTIALS, "CREDENTIALS");

end Javax.Security.Sasl.Sasl;
pragma Import (Java, Javax.Security.Sasl.Sasl, "javax.security.sasl.Sasl");
pragma Extensions_Allowed (Off);
