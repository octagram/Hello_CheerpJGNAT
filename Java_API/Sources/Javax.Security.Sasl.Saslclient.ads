pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Security.Sasl.SaslClient is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMechanismName (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   function HasInitialResponse (This : access Typ)
                                return Java.Boolean is abstract;

   function EvaluateChallenge (This : access Typ;
                               P1_Byte_Arr : Java.Byte_Arr)
                               return Java.Byte_Arr is abstract;
   --  can raise Javax.Security.Sasl.SaslException.Except

   function IsComplete (This : access Typ)
                        return Java.Boolean is abstract;

   function Unwrap (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return Java.Byte_Arr is abstract;
   --  can raise Javax.Security.Sasl.SaslException.Except

   function Wrap (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Byte_Arr is abstract;
   --  can raise Javax.Security.Sasl.SaslException.Except

   function GetNegotiatedProperty (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class is abstract;

   procedure Dispose (This : access Typ) is abstract;
   --  can raise Javax.Security.Sasl.SaslException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMechanismName, "getMechanismName");
   pragma Export (Java, HasInitialResponse, "hasInitialResponse");
   pragma Export (Java, EvaluateChallenge, "evaluateChallenge");
   pragma Export (Java, IsComplete, "isComplete");
   pragma Export (Java, Unwrap, "unwrap");
   pragma Export (Java, Wrap, "wrap");
   pragma Export (Java, GetNegotiatedProperty, "getNegotiatedProperty");
   pragma Export (Java, Dispose, "dispose");

end Javax.Security.Sasl.SaslClient;
pragma Import (Java, Javax.Security.Sasl.SaslClient, "javax.security.sasl.SaslClient");
pragma Extensions_Allowed (Off);
