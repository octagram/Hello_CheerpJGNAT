pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.IOException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Security.Sasl.SaslException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Io.IOException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SaslException (This : Ref := null)
                               return Ref;

   function New_SaslException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_SaslException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;

   function InitCause (This : access Typ;
                       P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class)
                       return access Java.Lang.Throwable.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.security.sasl.SaslException");
   pragma Java_Constructor (New_SaslException);
   pragma Import (Java, GetCause, "getCause");
   pragma Import (Java, InitCause, "initCause");
   pragma Import (Java, ToString, "toString");

end Javax.Security.Sasl.SaslException;
pragma Import (Java, Javax.Security.Sasl.SaslException, "javax.security.sasl.SaslException");
pragma Extensions_Allowed (Off);
