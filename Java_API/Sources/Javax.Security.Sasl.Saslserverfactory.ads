pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Security.Auth.Callback.CallbackHandler;
limited with Javax.Security.Sasl.SaslServer;
with Java.Lang.Object;

package Javax.Security.Sasl.SaslServerFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateSaslServer (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_Map : access Standard.Java.Util.Map.Typ'Class;
                              P5_CallbackHandler : access Standard.Javax.Security.Auth.Callback.CallbackHandler.Typ'Class)
                              return access Javax.Security.Sasl.SaslServer.Typ'Class is abstract;
   --  can raise Javax.Security.Sasl.SaslException.Except

   function GetMechanismNames (This : access Typ;
                               P1_Map : access Standard.Java.Util.Map.Typ'Class)
                               return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateSaslServer, "createSaslServer");
   pragma Export (Java, GetMechanismNames, "getMechanismNames");

end Javax.Security.Sasl.SaslServerFactory;
pragma Import (Java, Javax.Security.Sasl.SaslServerFactory, "javax.security.sasl.SaslServerFactory");
pragma Extensions_Allowed (Off);
