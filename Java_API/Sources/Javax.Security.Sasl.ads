pragma Extensions_Allowed (On);
package Javax.Security.Sasl is
   pragma Preelaborate;
end Javax.Security.Sasl;
pragma Import (Java, Javax.Security.Sasl, "javax.security.sasl");
pragma Extensions_Allowed (Off);
