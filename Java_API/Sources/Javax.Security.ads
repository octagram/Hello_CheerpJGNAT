pragma Extensions_Allowed (On);
package Javax.Security is
   pragma Preelaborate;
end Javax.Security;
pragma Import (Java, Javax.Security, "javax.security");
pragma Extensions_Allowed (Off);
