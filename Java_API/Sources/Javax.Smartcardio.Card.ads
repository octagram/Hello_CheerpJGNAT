pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Smartcardio.ATR;
limited with Javax.Smartcardio.CardChannel;
with Java.Lang.Object;

package Javax.Smartcardio.Card is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Card (This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetATR (This : access Typ)
                    return access Javax.Smartcardio.ATR.Typ'Class is abstract;

   function GetProtocol (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetBasicChannel (This : access Typ)
                             return access Javax.Smartcardio.CardChannel.Typ'Class is abstract;

   function OpenLogicalChannel (This : access Typ)
                                return access Javax.Smartcardio.CardChannel.Typ'Class is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   procedure BeginExclusive (This : access Typ) is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   procedure EndExclusive (This : access Typ) is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   function TransmitControlCommand (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Byte_Arr : Java.Byte_Arr)
                                    return Java.Byte_Arr is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   procedure Disconnect (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;
   --  can raise Javax.Smartcardio.CardException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Card);
   pragma Export (Java, GetATR, "getATR");
   pragma Export (Java, GetProtocol, "getProtocol");
   pragma Export (Java, GetBasicChannel, "getBasicChannel");
   pragma Export (Java, OpenLogicalChannel, "openLogicalChannel");
   pragma Export (Java, BeginExclusive, "beginExclusive");
   pragma Export (Java, EndExclusive, "endExclusive");
   pragma Export (Java, TransmitControlCommand, "transmitControlCommand");
   pragma Export (Java, Disconnect, "disconnect");

end Javax.Smartcardio.Card;
pragma Import (Java, Javax.Smartcardio.Card, "javax.smartcardio.Card");
pragma Extensions_Allowed (Off);
