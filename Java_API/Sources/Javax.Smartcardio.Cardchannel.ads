pragma Extensions_Allowed (On);
limited with Java.Nio.ByteBuffer;
limited with Javax.Smartcardio.Card;
limited with Javax.Smartcardio.CommandAPDU;
limited with Javax.Smartcardio.ResponseAPDU;
with Java.Lang.Object;

package Javax.Smartcardio.CardChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_CardChannel (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCard (This : access Typ)
                     return access Javax.Smartcardio.Card.Typ'Class is abstract;

   function GetChannelNumber (This : access Typ)
                              return Java.Int is abstract;

   function Transmit (This : access Typ;
                      P1_CommandAPDU : access Standard.Javax.Smartcardio.CommandAPDU.Typ'Class)
                      return access Javax.Smartcardio.ResponseAPDU.Typ'Class is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   function Transmit (This : access Typ;
                      P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class;
                      P2_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class)
                      return Java.Int is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Smartcardio.CardException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CardChannel);
   pragma Export (Java, GetCard, "getCard");
   pragma Export (Java, GetChannelNumber, "getChannelNumber");
   pragma Export (Java, Transmit, "transmit");
   pragma Export (Java, Close, "close");

end Javax.Smartcardio.CardChannel;
pragma Import (Java, Javax.Smartcardio.CardChannel, "javax.smartcardio.CardChannel");
pragma Extensions_Allowed (Off);
