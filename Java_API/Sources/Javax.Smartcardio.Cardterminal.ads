pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Smartcardio.Card;
with Java.Lang.Object;

package Javax.Smartcardio.CardTerminal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_CardTerminal (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function Connect (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Smartcardio.Card.Typ'Class is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   function IsCardPresent (This : access Typ)
                           return Java.Boolean is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   function WaitForCardPresent (This : access Typ;
                                P1_Long : Java.Long)
                                return Java.Boolean is abstract;
   --  can raise Javax.Smartcardio.CardException.Except

   function WaitForCardAbsent (This : access Typ;
                               P1_Long : Java.Long)
                               return Java.Boolean is abstract;
   --  can raise Javax.Smartcardio.CardException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CardTerminal);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, IsCardPresent, "isCardPresent");
   pragma Export (Java, WaitForCardPresent, "waitForCardPresent");
   pragma Export (Java, WaitForCardAbsent, "waitForCardAbsent");

end Javax.Smartcardio.CardTerminal;
pragma Import (Java, Javax.Smartcardio.CardTerminal, "javax.smartcardio.CardTerminal");
pragma Extensions_Allowed (Off);
