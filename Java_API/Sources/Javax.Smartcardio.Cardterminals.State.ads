pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Smartcardio.CardTerminals.State is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Smartcardio.CardTerminals.State.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ALL_K : access Javax.Smartcardio.CardTerminals.State.Typ'Class;

   --  final
   CARD_PRESENT : access Javax.Smartcardio.CardTerminals.State.Typ'Class;

   --  final
   CARD_ABSENT : access Javax.Smartcardio.CardTerminals.State.Typ'Class;

   --  final
   CARD_INSERTION : access Javax.Smartcardio.CardTerminals.State.Typ'Class;

   --  final
   CARD_REMOVAL : access Javax.Smartcardio.CardTerminals.State.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, ALL_K, "ALL");
   pragma Import (Java, CARD_PRESENT, "CARD_PRESENT");
   pragma Import (Java, CARD_ABSENT, "CARD_ABSENT");
   pragma Import (Java, CARD_INSERTION, "CARD_INSERTION");
   pragma Import (Java, CARD_REMOVAL, "CARD_REMOVAL");

end Javax.Smartcardio.CardTerminals.State;
pragma Import (Java, Javax.Smartcardio.CardTerminals.State, "javax.smartcardio.CardTerminals$State");
pragma Extensions_Allowed (Off);
