pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Smartcardio.CardTerminal;
limited with Javax.Smartcardio.CardTerminals.State;
with Java.Lang.Object;

package Javax.Smartcardio.CardTerminals is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_CardTerminals (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function List (This : access Typ)
                  return access Java.Util.List.Typ'Class
                  with Import => "list", Convention => Java;
   --  can raise Javax.Smartcardio.CardException.Except

   function List (This : access Typ;
                  P1_State : access Standard.Javax.Smartcardio.CardTerminals.State.Typ'Class)
                  return access Java.Util.List.Typ'Class is abstract
                  with Export => "list", Convention => Java;
   --  can raise Javax.Smartcardio.CardException.Except

   function GetTerminal (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Smartcardio.CardTerminal.Typ'Class;

   procedure WaitForChange (This : access Typ)
                            with Import => "waitForChange", Convention => Java;
   --  can raise Javax.Smartcardio.CardException.Except

   function WaitForChange (This : access Typ;
                           P1_Long : Java.Long)
                           return Java.Boolean is abstract
                           with Export => "waitForChange", Convention => Java;
   --  can raise Javax.Smartcardio.CardException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CardTerminals);
   -- pragma Import (Java, List, "list");
   pragma Import (Java, GetTerminal, "getTerminal");
   -- pragma Import (Java, WaitForChange, "waitForChange");

end Javax.Smartcardio.CardTerminals;
pragma Import (Java, Javax.Smartcardio.CardTerminals, "javax.smartcardio.CardTerminals");
pragma Extensions_Allowed (Off);
