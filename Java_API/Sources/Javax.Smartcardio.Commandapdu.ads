pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Nio.ByteBuffer;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Smartcardio.CommandAPDU is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CommandAPDU (P1_Byte_Arr : Java.Byte_Arr; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_Byte_Arr : Java.Byte_Arr;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_ByteBuffer : access Standard.Java.Nio.ByteBuffer.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Byte_Arr : Java.Byte_Arr; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Byte_Arr : Java.Byte_Arr;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Byte_Arr : Java.Byte_Arr;
                             P6_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_CommandAPDU (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Byte_Arr : Java.Byte_Arr;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int;
                             P8_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCLA (This : access Typ)
                    return Java.Int;

   function GetINS (This : access Typ)
                    return Java.Int;

   function GetP1 (This : access Typ)
                   return Java.Int;

   function GetP2 (This : access Typ)
                   return Java.Int;

   function GetNc (This : access Typ)
                   return Java.Int;

   function GetData (This : access Typ)
                     return Java.Byte_Arr;

   function GetNe (This : access Typ)
                   return Java.Int;

   function GetBytes (This : access Typ)
                      return Java.Byte_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CommandAPDU);
   pragma Import (Java, GetCLA, "getCLA");
   pragma Import (Java, GetINS, "getINS");
   pragma Import (Java, GetP1, "getP1");
   pragma Import (Java, GetP2, "getP2");
   pragma Import (Java, GetNc, "getNc");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, GetNe, "getNe");
   pragma Import (Java, GetBytes, "getBytes");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Smartcardio.CommandAPDU;
pragma Import (Java, Javax.Smartcardio.CommandAPDU, "javax.smartcardio.CommandAPDU");
pragma Extensions_Allowed (Off);
