pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Smartcardio.ResponseAPDU is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ResponseAPDU (P1_Byte_Arr : Java.Byte_Arr; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNr (This : access Typ)
                   return Java.Int;

   function GetData (This : access Typ)
                     return Java.Byte_Arr;

   function GetSW1 (This : access Typ)
                    return Java.Int;

   function GetSW2 (This : access Typ)
                    return Java.Int;

   function GetSW (This : access Typ)
                   return Java.Int;

   function GetBytes (This : access Typ)
                      return Java.Byte_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ResponseAPDU);
   pragma Import (Java, GetNr, "getNr");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, GetSW1, "getSW1");
   pragma Import (Java, GetSW2, "getSW2");
   pragma Import (Java, GetSW, "getSW");
   pragma Import (Java, GetBytes, "getBytes");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Javax.Smartcardio.ResponseAPDU;
pragma Import (Java, Javax.Smartcardio.ResponseAPDU, "javax.smartcardio.ResponseAPDU");
pragma Extensions_Allowed (Off);
