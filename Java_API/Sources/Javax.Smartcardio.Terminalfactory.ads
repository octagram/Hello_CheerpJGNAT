pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Javax.Smartcardio.CardTerminals;
with Java.Lang.Object;

package Javax.Smartcardio.TerminalFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultType return access Java.Lang.String.Typ'Class;

   function GetDefault return access Javax.Smartcardio.TerminalFactory.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Javax.Smartcardio.TerminalFactory.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Smartcardio.TerminalFactory.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P3_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Javax.Smartcardio.TerminalFactory.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function Terminals (This : access Typ)
                       return access Javax.Smartcardio.CardTerminals.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDefaultType, "getDefaultType");
   pragma Import (Java, GetDefault, "getDefault");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, Terminals, "terminals");
   pragma Import (Java, ToString, "toString");

end Javax.Smartcardio.TerminalFactory;
pragma Import (Java, Javax.Smartcardio.TerminalFactory, "javax.smartcardio.TerminalFactory");
pragma Extensions_Allowed (Off);
