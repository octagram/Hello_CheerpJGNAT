pragma Extensions_Allowed (On);
limited with Javax.Smartcardio.CardTerminals;
with Java.Lang.Object;

package Javax.Smartcardio.TerminalFactorySpi is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_TerminalFactorySpi (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function EngineTerminals (This : access Typ)
                             return access Javax.Smartcardio.CardTerminals.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TerminalFactorySpi);
   pragma Export (Java, EngineTerminals, "engineTerminals");

end Javax.Smartcardio.TerminalFactorySpi;
pragma Import (Java, Javax.Smartcardio.TerminalFactorySpi, "javax.smartcardio.TerminalFactorySpi");
pragma Extensions_Allowed (Off);
