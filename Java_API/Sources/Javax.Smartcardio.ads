pragma Extensions_Allowed (On);
package Javax.Smartcardio is
   pragma Preelaborate;
end Javax.Smartcardio;
pragma Import (Java, Javax.Smartcardio, "javax.smartcardio");
pragma Extensions_Allowed (Off);
