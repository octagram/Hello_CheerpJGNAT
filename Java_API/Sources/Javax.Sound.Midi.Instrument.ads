pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Sound.Midi.Patch;
limited with Javax.Sound.Midi.Soundbank;
with Java.Lang.Object;
with Javax.Sound.Midi.SoundbankResource;

package Javax.Sound.Midi.Instrument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Sound.Midi.SoundbankResource.Typ
      with null record;

   --  protected
   function New_Instrument (P1_Soundbank : access Standard.Javax.Sound.Midi.Soundbank.Typ'Class;
                            P2_Patch : access Standard.Javax.Sound.Midi.Patch.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_Class : access Standard.Java.Lang.Class.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPatch (This : access Typ)
                      return access Javax.Sound.Midi.Patch.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Instrument);
   pragma Import (Java, GetPatch, "getPatch");

end Javax.Sound.Midi.Instrument;
pragma Import (Java, Javax.Sound.Midi.Instrument, "javax.sound.midi.Instrument");
pragma Extensions_Allowed (Off);
