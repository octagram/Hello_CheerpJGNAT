pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Sound.Midi.MidiMessage;

package Javax.Sound.Midi.MetaMessage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Sound.Midi.MidiMessage.Typ(Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetaMessage (This : Ref := null)
                             return Ref;

   --  protected
   function New_MetaMessage (P1_Byte_Arr : Java.Byte_Arr; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMessage (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Byte_Arr : Java.Byte_Arr;
                         P3_Int : Java.Int);
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   function GetType (This : access Typ)
                     return Java.Int;

   function GetData (This : access Typ)
                     return Java.Byte_Arr;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   META : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetaMessage);
   pragma Import (Java, SetMessage, "setMessage");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, META, "META");

end Javax.Sound.Midi.MetaMessage;
pragma Import (Java, Javax.Sound.Midi.MetaMessage, "javax.sound.midi.MetaMessage");
pragma Extensions_Allowed (Off);
