pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Sound.Midi.MidiChannel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure NoteOn (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int) is abstract;

   procedure NoteOff (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int) is abstract;

   procedure NoteOff (This : access Typ;
                      P1_Int : Java.Int) is abstract;

   procedure SetPolyPressure (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int) is abstract;

   function GetPolyPressure (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int is abstract;

   procedure SetChannelPressure (This : access Typ;
                                 P1_Int : Java.Int) is abstract;

   function GetChannelPressure (This : access Typ)
                                return Java.Int is abstract;

   procedure ControlChange (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int) is abstract;

   function GetController (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int is abstract;

   procedure ProgramChange (This : access Typ;
                            P1_Int : Java.Int) is abstract;

   procedure ProgramChange (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int) is abstract;

   function GetProgram (This : access Typ)
                        return Java.Int is abstract;

   procedure SetPitchBend (This : access Typ;
                           P1_Int : Java.Int) is abstract;

   function GetPitchBend (This : access Typ)
                          return Java.Int is abstract;

   procedure ResetAllControllers (This : access Typ) is abstract;

   procedure AllNotesOff (This : access Typ) is abstract;

   procedure AllSoundOff (This : access Typ) is abstract;

   function LocalControl (This : access Typ;
                          P1_Boolean : Java.Boolean)
                          return Java.Boolean is abstract;

   procedure SetMono (This : access Typ;
                      P1_Boolean : Java.Boolean) is abstract;

   function GetMono (This : access Typ)
                     return Java.Boolean is abstract;

   procedure SetOmni (This : access Typ;
                      P1_Boolean : Java.Boolean) is abstract;

   function GetOmni (This : access Typ)
                     return Java.Boolean is abstract;

   procedure SetMute (This : access Typ;
                      P1_Boolean : Java.Boolean) is abstract;

   function GetMute (This : access Typ)
                     return Java.Boolean is abstract;

   procedure SetSolo (This : access Typ;
                      P1_Boolean : Java.Boolean) is abstract;

   function GetSolo (This : access Typ)
                     return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NoteOn, "noteOn");
   pragma Export (Java, NoteOff, "noteOff");
   pragma Export (Java, SetPolyPressure, "setPolyPressure");
   pragma Export (Java, GetPolyPressure, "getPolyPressure");
   pragma Export (Java, SetChannelPressure, "setChannelPressure");
   pragma Export (Java, GetChannelPressure, "getChannelPressure");
   pragma Export (Java, ControlChange, "controlChange");
   pragma Export (Java, GetController, "getController");
   pragma Export (Java, ProgramChange, "programChange");
   pragma Export (Java, GetProgram, "getProgram");
   pragma Export (Java, SetPitchBend, "setPitchBend");
   pragma Export (Java, GetPitchBend, "getPitchBend");
   pragma Export (Java, ResetAllControllers, "resetAllControllers");
   pragma Export (Java, AllNotesOff, "allNotesOff");
   pragma Export (Java, AllSoundOff, "allSoundOff");
   pragma Export (Java, LocalControl, "localControl");
   pragma Export (Java, SetMono, "setMono");
   pragma Export (Java, GetMono, "getMono");
   pragma Export (Java, SetOmni, "setOmni");
   pragma Export (Java, GetOmni, "getOmni");
   pragma Export (Java, SetMute, "setMute");
   pragma Export (Java, GetMute, "getMute");
   pragma Export (Java, SetSolo, "setSolo");
   pragma Export (Java, GetSolo, "getSolo");

end Javax.Sound.Midi.MidiChannel;
pragma Import (Java, Javax.Sound.Midi.MidiChannel, "javax.sound.midi.MidiChannel");
pragma Extensions_Allowed (Off);
