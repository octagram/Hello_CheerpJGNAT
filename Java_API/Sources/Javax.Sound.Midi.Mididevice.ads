pragma Extensions_Allowed (On);
limited with Java.Util.List;
limited with Javax.Sound.Midi.MidiDevice.Info;
limited with Javax.Sound.Midi.Receiver;
limited with Javax.Sound.Midi.Transmitter;
with Java.Lang.Object;

package Javax.Sound.Midi.MidiDevice is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDeviceInfo (This : access Typ)
                           return access Javax.Sound.Midi.MidiDevice.Info.Typ'Class is abstract;

   procedure Open (This : access Typ) is abstract;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   procedure Close (This : access Typ) is abstract;

   function IsOpen (This : access Typ)
                    return Java.Boolean is abstract;

   function GetMicrosecondPosition (This : access Typ)
                                    return Java.Long is abstract;

   function GetMaxReceivers (This : access Typ)
                             return Java.Int is abstract;

   function GetMaxTransmitters (This : access Typ)
                                return Java.Int is abstract;

   function GetReceiver (This : access Typ)
                         return access Javax.Sound.Midi.Receiver.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetReceivers (This : access Typ)
                          return access Java.Util.List.Typ'Class is abstract;

   function GetTransmitter (This : access Typ)
                            return access Javax.Sound.Midi.Transmitter.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetTransmitters (This : access Typ)
                             return access Java.Util.List.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDeviceInfo, "getDeviceInfo");
   pragma Export (Java, Open, "open");
   pragma Export (Java, Close, "close");
   pragma Export (Java, IsOpen, "isOpen");
   pragma Export (Java, GetMicrosecondPosition, "getMicrosecondPosition");
   pragma Export (Java, GetMaxReceivers, "getMaxReceivers");
   pragma Export (Java, GetMaxTransmitters, "getMaxTransmitters");
   pragma Export (Java, GetReceiver, "getReceiver");
   pragma Export (Java, GetReceivers, "getReceivers");
   pragma Export (Java, GetTransmitter, "getTransmitter");
   pragma Export (Java, GetTransmitters, "getTransmitters");

end Javax.Sound.Midi.MidiDevice;
pragma Import (Java, Javax.Sound.Midi.MidiDevice, "javax.sound.midi.MidiDevice");
pragma Extensions_Allowed (Off);
