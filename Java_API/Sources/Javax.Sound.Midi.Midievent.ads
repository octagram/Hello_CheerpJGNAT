pragma Extensions_Allowed (On);
limited with Javax.Sound.Midi.MidiMessage;
with Java.Lang.Object;

package Javax.Sound.Midi.MidiEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MidiEvent (P1_MidiMessage : access Standard.Javax.Sound.Midi.MidiMessage.Typ'Class;
                           P2_Long : Java.Long; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Javax.Sound.Midi.MidiMessage.Typ'Class;

   procedure SetTick (This : access Typ;
                      P1_Long : Java.Long);

   function GetTick (This : access Typ)
                     return Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MidiEvent);
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, SetTick, "setTick");
   pragma Import (Java, GetTick, "getTick");

end Javax.Sound.Midi.MidiEvent;
pragma Import (Java, Javax.Sound.Midi.MidiEvent, "javax.sound.midi.MidiEvent");
pragma Extensions_Allowed (Off);
