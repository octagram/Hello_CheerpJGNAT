pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
with Java.Lang.Object;

package Javax.Sound.Midi.MidiFileFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      type_K : Java.Int;
      pragma Import (Java, type_K, "type");

      --  protected
      DivisionType : Java.Float;
      pragma Import (Java, DivisionType, "divisionType");

      --  protected
      Resolution : Java.Int;
      pragma Import (Java, Resolution, "resolution");

      --  protected
      ByteLength : Java.Int;
      pragma Import (Java, ByteLength, "byteLength");

      --  protected
      MicrosecondLength : Java.Long;
      pragma Import (Java, MicrosecondLength, "microsecondLength");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MidiFileFormat (P1_Int : Java.Int;
                                P2_Float : Java.Float;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Long : Java.Long; 
                                This : Ref := null)
                                return Ref;

   function New_MidiFileFormat (P1_Int : Java.Int;
                                P2_Float : Java.Float;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Long : Java.Long;
                                P6_Map : access Standard.Java.Util.Map.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return Java.Int;

   function GetDivisionType (This : access Typ)
                             return Java.Float;

   function GetResolution (This : access Typ)
                           return Java.Int;

   function GetByteLength (This : access Typ)
                           return Java.Int;

   function GetMicrosecondLength (This : access Typ)
                                  return Java.Long;

   function Properties (This : access Typ)
                        return access Java.Util.Map.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNKNOWN_LENGTH : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MidiFileFormat);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetDivisionType, "getDivisionType");
   pragma Import (Java, GetResolution, "getResolution");
   pragma Import (Java, GetByteLength, "getByteLength");
   pragma Import (Java, GetMicrosecondLength, "getMicrosecondLength");
   pragma Import (Java, Properties, "properties");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, UNKNOWN_LENGTH, "UNKNOWN_LENGTH");

end Javax.Sound.Midi.MidiFileFormat;
pragma Import (Java, Javax.Sound.Midi.MidiFileFormat, "javax.sound.midi.MidiFileFormat");
pragma Extensions_Allowed (Off);
