pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Sound.Midi.MidiMessage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Data : Java.Byte_Arr;
      pragma Import (Java, Data, "data");

      --  protected
      Length : Java.Int;
      pragma Import (Java, Length, "length");

   end record;

   --  protected
   function New_MidiMessage (P1_Byte_Arr : Java.Byte_Arr; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure SetMessage (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr;
                         P2_Int : Java.Int);
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   function GetMessage (This : access Typ)
                        return Java.Byte_Arr;

   function GetStatus (This : access Typ)
                       return Java.Int;

   function GetLength (This : access Typ)
                       return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MidiMessage);
   pragma Import (Java, SetMessage, "setMessage");
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, GetStatus, "getStatus");
   pragma Import (Java, GetLength, "getLength");
   pragma Export (Java, Clone, "clone");

end Javax.Sound.Midi.MidiMessage;
pragma Import (Java, Javax.Sound.Midi.MidiMessage, "javax.sound.midi.MidiMessage");
pragma Extensions_Allowed (Off);
