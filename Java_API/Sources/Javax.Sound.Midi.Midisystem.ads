pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Net.URL;
limited with Javax.Sound.Midi.MidiDevice;
limited with Javax.Sound.Midi.MidiDevice.Info;
limited with Javax.Sound.Midi.MidiFileFormat;
limited with Javax.Sound.Midi.Receiver;
limited with Javax.Sound.Midi.Sequence;
limited with Javax.Sound.Midi.Sequencer;
limited with Javax.Sound.Midi.Soundbank;
limited with Javax.Sound.Midi.Synthesizer;
limited with Javax.Sound.Midi.Transmitter;
with Java.Lang.Object;

package Javax.Sound.Midi.MidiSystem is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMidiDeviceInfo return Standard.Java.Lang.Object.Ref;

   function GetMidiDevice (P1_Info : access Standard.Javax.Sound.Midi.MidiDevice.Info.Typ'Class)
                           return access Javax.Sound.Midi.MidiDevice.Typ'Class;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetReceiver return access Javax.Sound.Midi.Receiver.Typ'Class;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetTransmitter return access Javax.Sound.Midi.Transmitter.Typ'Class;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetSynthesizer return access Javax.Sound.Midi.Synthesizer.Typ'Class;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetSequencer return access Javax.Sound.Midi.Sequencer.Typ'Class;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetSequencer (P1_Boolean : Java.Boolean)
                          return access Javax.Sound.Midi.Sequencer.Typ'Class;
   --  can raise Javax.Sound.Midi.MidiUnavailableException.Except

   function GetSoundbank (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                          return access Javax.Sound.Midi.Soundbank.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSoundbank (P1_URL : access Standard.Java.Net.URL.Typ'Class)
                          return access Javax.Sound.Midi.Soundbank.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSoundbank (P1_File : access Standard.Java.Io.File.Typ'Class)
                          return access Javax.Sound.Midi.Soundbank.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetMidiFileFormat (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                               return access Javax.Sound.Midi.MidiFileFormat.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetMidiFileFormat (P1_URL : access Standard.Java.Net.URL.Typ'Class)
                               return access Javax.Sound.Midi.MidiFileFormat.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetMidiFileFormat (P1_File : access Standard.Java.Io.File.Typ'Class)
                               return access Javax.Sound.Midi.MidiFileFormat.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSequence (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                         return access Javax.Sound.Midi.Sequence.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSequence (P1_URL : access Standard.Java.Net.URL.Typ'Class)
                         return access Javax.Sound.Midi.Sequence.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSequence (P1_File : access Standard.Java.Io.File.Typ'Class)
                         return access Javax.Sound.Midi.Sequence.Typ'Class;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetMidiFileTypes return Java.Int_Arr;

   function IsFileTypeSupported (P1_Int : Java.Int)
                                 return Java.Boolean;

   function GetMidiFileTypes (P1_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class)
                              return Java.Int_Arr;

   function IsFileTypeSupported (P1_Int : Java.Int;
                                 P2_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class)
                                 return Java.Boolean;

   function Write (P1_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class;
                   P2_Int : Java.Int;
                   P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                   return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Write (P1_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class;
                   P2_Int : Java.Int;
                   P3_File : access Standard.Java.Io.File.Typ'Class)
                   return Java.Int;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetMidiDeviceInfo, "getMidiDeviceInfo");
   pragma Import (Java, GetMidiDevice, "getMidiDevice");
   pragma Import (Java, GetReceiver, "getReceiver");
   pragma Import (Java, GetTransmitter, "getTransmitter");
   pragma Import (Java, GetSynthesizer, "getSynthesizer");
   pragma Import (Java, GetSequencer, "getSequencer");
   pragma Import (Java, GetSoundbank, "getSoundbank");
   pragma Import (Java, GetMidiFileFormat, "getMidiFileFormat");
   pragma Import (Java, GetSequence, "getSequence");
   pragma Import (Java, GetMidiFileTypes, "getMidiFileTypes");
   pragma Import (Java, IsFileTypeSupported, "isFileTypeSupported");
   pragma Import (Java, Write, "write");

end Javax.Sound.Midi.MidiSystem;
pragma Import (Java, Javax.Sound.Midi.MidiSystem, "javax.sound.midi.MidiSystem");
pragma Extensions_Allowed (Off);
