pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Sound.Midi.MidiUnavailableException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MidiUnavailableException (This : Ref := null)
                                          return Ref;

   function New_MidiUnavailableException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.sound.midi.MidiUnavailableException");
   pragma Java_Constructor (New_MidiUnavailableException);

end Javax.Sound.Midi.MidiUnavailableException;
pragma Import (Java, Javax.Sound.Midi.MidiUnavailableException, "javax.sound.midi.MidiUnavailableException");
pragma Extensions_Allowed (Off);
