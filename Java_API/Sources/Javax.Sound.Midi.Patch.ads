pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Sound.Midi.Patch is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Patch (P1_Int : Java.Int;
                       P2_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBank (This : access Typ)
                     return Java.Int;

   function GetProgram (This : access Typ)
                        return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Patch);
   pragma Import (Java, GetBank, "getBank");
   pragma Import (Java, GetProgram, "getProgram");

end Javax.Sound.Midi.Patch;
pragma Import (Java, Javax.Sound.Midi.Patch, "javax.sound.midi.Patch");
pragma Extensions_Allowed (Off);
