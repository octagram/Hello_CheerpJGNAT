pragma Extensions_Allowed (On);
limited with Java.Util.Vector;
limited with Javax.Sound.Midi.Patch;
limited with Javax.Sound.Midi.Track;
with Java.Lang.Object;

package Javax.Sound.Midi.Sequence is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DivisionType : Java.Float;
      pragma Import (Java, DivisionType, "divisionType");

      --  protected
      Resolution : Java.Int;
      pragma Import (Java, Resolution, "resolution");

      --  protected
      Tracks : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Tracks, "tracks");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Sequence (P1_Float : Java.Float;
                          P2_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   function New_Sequence (P1_Float : Java.Float;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDivisionType (This : access Typ)
                             return Java.Float;

   function GetResolution (This : access Typ)
                           return Java.Int;

   function CreateTrack (This : access Typ)
                         return access Javax.Sound.Midi.Track.Typ'Class;

   function DeleteTrack (This : access Typ;
                         P1_Track : access Standard.Javax.Sound.Midi.Track.Typ'Class)
                         return Java.Boolean;

   function GetTracks (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   function GetMicrosecondLength (This : access Typ)
                                  return Java.Long;

   function GetTickLength (This : access Typ)
                           return Java.Long;

   function GetPatchList (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PPQ : constant Java.Float;

   --  final
   SMPTE_24 : constant Java.Float;

   --  final
   SMPTE_25 : constant Java.Float;

   --  final
   SMPTE_30DROP : constant Java.Float;

   --  final
   SMPTE_30 : constant Java.Float;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Sequence);
   pragma Import (Java, GetDivisionType, "getDivisionType");
   pragma Import (Java, GetResolution, "getResolution");
   pragma Import (Java, CreateTrack, "createTrack");
   pragma Import (Java, DeleteTrack, "deleteTrack");
   pragma Import (Java, GetTracks, "getTracks");
   pragma Import (Java, GetMicrosecondLength, "getMicrosecondLength");
   pragma Import (Java, GetTickLength, "getTickLength");
   pragma Import (Java, GetPatchList, "getPatchList");
   pragma Import (Java, PPQ, "PPQ");
   pragma Import (Java, SMPTE_24, "SMPTE_24");
   pragma Import (Java, SMPTE_25, "SMPTE_25");
   pragma Import (Java, SMPTE_30DROP, "SMPTE_30DROP");
   pragma Import (Java, SMPTE_30, "SMPTE_30");

end Javax.Sound.Midi.Sequence;
pragma Import (Java, Javax.Sound.Midi.Sequence, "javax.sound.midi.Sequence");
pragma Extensions_Allowed (Off);
