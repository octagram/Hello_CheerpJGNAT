pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sound.Midi.Sequencer.SyncMode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_SyncMode (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INTERNAL_CLOCK : access Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class;

   --  final
   MIDI_SYNC : access Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class;

   --  final
   MIDI_TIME_CODE : access Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class;

   --  final
   NO_SYNC : access Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SyncMode);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, INTERNAL_CLOCK, "INTERNAL_CLOCK");
   pragma Import (Java, MIDI_SYNC, "MIDI_SYNC");
   pragma Import (Java, MIDI_TIME_CODE, "MIDI_TIME_CODE");
   pragma Import (Java, NO_SYNC, "NO_SYNC");

end Javax.Sound.Midi.Sequencer.SyncMode;
pragma Import (Java, Javax.Sound.Midi.Sequencer.SyncMode, "javax.sound.midi.Sequencer$SyncMode");
pragma Extensions_Allowed (Off);
