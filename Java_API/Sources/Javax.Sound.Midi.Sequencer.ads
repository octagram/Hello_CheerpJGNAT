pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Javax.Sound.Midi.ControllerEventListener;
limited with Javax.Sound.Midi.MetaEventListener;
limited with Javax.Sound.Midi.Sequence;
limited with Javax.Sound.Midi.Sequencer.SyncMode;
limited with Javax.Sound.Midi.Track;
with Java.Lang.Object;
with Javax.Sound.Midi.MidiDevice;

package Javax.Sound.Midi.Sequencer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MidiDevice_I : Javax.Sound.Midi.MidiDevice.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSequence (This : access Typ;
                          P1_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class) is abstract;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   procedure SetSequence (This : access Typ;
                          P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Sound.Midi.InvalidMidiDataException.Except

   function GetSequence (This : access Typ)
                         return access Javax.Sound.Midi.Sequence.Typ'Class is abstract;

   procedure Start (This : access Typ) is abstract;

   procedure Stop (This : access Typ) is abstract;

   function IsRunning (This : access Typ)
                       return Java.Boolean is abstract;

   procedure StartRecording (This : access Typ) is abstract;

   procedure StopRecording (This : access Typ) is abstract;

   function IsRecording (This : access Typ)
                         return Java.Boolean is abstract;

   procedure RecordEnable (This : access Typ;
                           P1_Track : access Standard.Javax.Sound.Midi.Track.Typ'Class;
                           P2_Int : Java.Int) is abstract;

   procedure RecordDisable (This : access Typ;
                            P1_Track : access Standard.Javax.Sound.Midi.Track.Typ'Class) is abstract;

   function GetTempoInBPM (This : access Typ)
                           return Java.Float is abstract;

   procedure SetTempoInBPM (This : access Typ;
                            P1_Float : Java.Float) is abstract;

   function GetTempoInMPQ (This : access Typ)
                           return Java.Float is abstract;

   procedure SetTempoInMPQ (This : access Typ;
                            P1_Float : Java.Float) is abstract;

   procedure SetTempoFactor (This : access Typ;
                             P1_Float : Java.Float) is abstract;

   function GetTempoFactor (This : access Typ)
                            return Java.Float is abstract;

   function GetTickLength (This : access Typ)
                           return Java.Long is abstract;

   function GetTickPosition (This : access Typ)
                             return Java.Long is abstract;

   procedure SetTickPosition (This : access Typ;
                              P1_Long : Java.Long) is abstract;

   function GetMicrosecondLength (This : access Typ)
                                  return Java.Long is abstract;

   function GetMicrosecondPosition (This : access Typ)
                                    return Java.Long is abstract;

   procedure SetMicrosecondPosition (This : access Typ;
                                     P1_Long : Java.Long) is abstract;

   procedure SetMasterSyncMode (This : access Typ;
                                P1_SyncMode : access Standard.Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class) is abstract;

   function GetMasterSyncMode (This : access Typ)
                               return access Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class is abstract;

   function GetMasterSyncModes (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;

   procedure SetSlaveSyncMode (This : access Typ;
                               P1_SyncMode : access Standard.Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class) is abstract;

   function GetSlaveSyncMode (This : access Typ)
                              return access Javax.Sound.Midi.Sequencer.SyncMode.Typ'Class is abstract;

   function GetSlaveSyncModes (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   procedure SetTrackMute (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Boolean : Java.Boolean) is abstract;

   function GetTrackMute (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Boolean is abstract;

   procedure SetTrackSolo (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Boolean : Java.Boolean) is abstract;

   function GetTrackSolo (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Boolean is abstract;

   function AddMetaEventListener (This : access Typ;
                                  P1_MetaEventListener : access Standard.Javax.Sound.Midi.MetaEventListener.Typ'Class)
                                  return Java.Boolean is abstract;

   procedure RemoveMetaEventListener (This : access Typ;
                                      P1_MetaEventListener : access Standard.Javax.Sound.Midi.MetaEventListener.Typ'Class) is abstract;

   function AddControllerEventListener (This : access Typ;
                                        P1_ControllerEventListener : access Standard.Javax.Sound.Midi.ControllerEventListener.Typ'Class;
                                        P2_Int_Arr : Java.Int_Arr)
                                        return Java.Int_Arr is abstract;

   function RemoveControllerEventListener (This : access Typ;
                                           P1_ControllerEventListener : access Standard.Javax.Sound.Midi.ControllerEventListener.Typ'Class;
                                           P2_Int_Arr : Java.Int_Arr)
                                           return Java.Int_Arr is abstract;

   procedure SetLoopStartPoint (This : access Typ;
                                P1_Long : Java.Long) is abstract;

   function GetLoopStartPoint (This : access Typ)
                               return Java.Long is abstract;

   procedure SetLoopEndPoint (This : access Typ;
                              P1_Long : Java.Long) is abstract;

   function GetLoopEndPoint (This : access Typ)
                             return Java.Long is abstract;

   procedure SetLoopCount (This : access Typ;
                           P1_Int : Java.Int) is abstract;

   function GetLoopCount (This : access Typ)
                          return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LOOP_CONTINUOUSLY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetSequence, "setSequence");
   pragma Export (Java, GetSequence, "getSequence");
   pragma Export (Java, Start, "start");
   pragma Export (Java, Stop, "stop");
   pragma Export (Java, IsRunning, "isRunning");
   pragma Export (Java, StartRecording, "startRecording");
   pragma Export (Java, StopRecording, "stopRecording");
   pragma Export (Java, IsRecording, "isRecording");
   pragma Export (Java, RecordEnable, "recordEnable");
   pragma Export (Java, RecordDisable, "recordDisable");
   pragma Export (Java, GetTempoInBPM, "getTempoInBPM");
   pragma Export (Java, SetTempoInBPM, "setTempoInBPM");
   pragma Export (Java, GetTempoInMPQ, "getTempoInMPQ");
   pragma Export (Java, SetTempoInMPQ, "setTempoInMPQ");
   pragma Export (Java, SetTempoFactor, "setTempoFactor");
   pragma Export (Java, GetTempoFactor, "getTempoFactor");
   pragma Export (Java, GetTickLength, "getTickLength");
   pragma Export (Java, GetTickPosition, "getTickPosition");
   pragma Export (Java, SetTickPosition, "setTickPosition");
   pragma Export (Java, GetMicrosecondLength, "getMicrosecondLength");
   pragma Export (Java, GetMicrosecondPosition, "getMicrosecondPosition");
   pragma Export (Java, SetMicrosecondPosition, "setMicrosecondPosition");
   pragma Export (Java, SetMasterSyncMode, "setMasterSyncMode");
   pragma Export (Java, GetMasterSyncMode, "getMasterSyncMode");
   pragma Export (Java, GetMasterSyncModes, "getMasterSyncModes");
   pragma Export (Java, SetSlaveSyncMode, "setSlaveSyncMode");
   pragma Export (Java, GetSlaveSyncMode, "getSlaveSyncMode");
   pragma Export (Java, GetSlaveSyncModes, "getSlaveSyncModes");
   pragma Export (Java, SetTrackMute, "setTrackMute");
   pragma Export (Java, GetTrackMute, "getTrackMute");
   pragma Export (Java, SetTrackSolo, "setTrackSolo");
   pragma Export (Java, GetTrackSolo, "getTrackSolo");
   pragma Export (Java, AddMetaEventListener, "addMetaEventListener");
   pragma Export (Java, RemoveMetaEventListener, "removeMetaEventListener");
   pragma Export (Java, AddControllerEventListener, "addControllerEventListener");
   pragma Export (Java, RemoveControllerEventListener, "removeControllerEventListener");
   pragma Export (Java, SetLoopStartPoint, "setLoopStartPoint");
   pragma Export (Java, GetLoopStartPoint, "getLoopStartPoint");
   pragma Export (Java, SetLoopEndPoint, "setLoopEndPoint");
   pragma Export (Java, GetLoopEndPoint, "getLoopEndPoint");
   pragma Export (Java, SetLoopCount, "setLoopCount");
   pragma Export (Java, GetLoopCount, "getLoopCount");
   pragma Import (Java, LOOP_CONTINUOUSLY, "LOOP_CONTINUOUSLY");

end Javax.Sound.Midi.Sequencer;
pragma Import (Java, Javax.Sound.Midi.Sequencer, "javax.sound.midi.Sequencer");
pragma Extensions_Allowed (Off);
