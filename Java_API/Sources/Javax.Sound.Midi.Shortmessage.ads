pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Sound.Midi.MidiMessage;

package Javax.Sound.Midi.ShortMessage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Sound.Midi.MidiMessage.Typ(Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ShortMessage (This : Ref := null)
                              return Ref;

   --  protected
   function New_ShortMessage (P1_Byte_Arr : Java.Byte_Arr; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMessage (This : access Typ;
                         P1_Int : Java.Int);
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   procedure SetMessage (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   procedure SetMessage (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int);
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   function GetChannel (This : access Typ)
                        return Java.Int;

   function GetCommand (This : access Typ)
                        return Java.Int;

   function GetData1 (This : access Typ)
                      return Java.Int;

   function GetData2 (This : access Typ)
                      return Java.Int;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   --  final  protected
   function GetDataLength (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MIDI_TIME_CODE : constant Java.Int;

   --  final
   SONG_POSITION_POINTER : constant Java.Int;

   --  final
   SONG_SELECT : constant Java.Int;

   --  final
   TUNE_REQUEST : constant Java.Int;

   --  final
   END_OF_EXCLUSIVE : constant Java.Int;

   --  final
   TIMING_CLOCK : constant Java.Int;

   --  final
   START : constant Java.Int;

   --  final
   CONTINUE : constant Java.Int;

   --  final
   STOP : constant Java.Int;

   --  final
   ACTIVE_SENSING : constant Java.Int;

   --  final
   SYSTEM_RESET : constant Java.Int;

   --  final
   NOTE_OFF : constant Java.Int;

   --  final
   NOTE_ON : constant Java.Int;

   --  final
   POLY_PRESSURE : constant Java.Int;

   --  final
   CONTROL_CHANGE : constant Java.Int;

   --  final
   PROGRAM_CHANGE : constant Java.Int;

   --  final
   CHANNEL_PRESSURE : constant Java.Int;

   --  final
   PITCH_BEND : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ShortMessage);
   pragma Import (Java, SetMessage, "setMessage");
   pragma Import (Java, GetChannel, "getChannel");
   pragma Import (Java, GetCommand, "getCommand");
   pragma Import (Java, GetData1, "getData1");
   pragma Import (Java, GetData2, "getData2");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetDataLength, "getDataLength");
   pragma Import (Java, MIDI_TIME_CODE, "MIDI_TIME_CODE");
   pragma Import (Java, SONG_POSITION_POINTER, "SONG_POSITION_POINTER");
   pragma Import (Java, SONG_SELECT, "SONG_SELECT");
   pragma Import (Java, TUNE_REQUEST, "TUNE_REQUEST");
   pragma Import (Java, END_OF_EXCLUSIVE, "END_OF_EXCLUSIVE");
   pragma Import (Java, TIMING_CLOCK, "TIMING_CLOCK");
   pragma Import (Java, START, "START");
   pragma Import (Java, CONTINUE, "CONTINUE");
   pragma Import (Java, STOP, "STOP");
   pragma Import (Java, ACTIVE_SENSING, "ACTIVE_SENSING");
   pragma Import (Java, SYSTEM_RESET, "SYSTEM_RESET");
   pragma Import (Java, NOTE_OFF, "NOTE_OFF");
   pragma Import (Java, NOTE_ON, "NOTE_ON");
   pragma Import (Java, POLY_PRESSURE, "POLY_PRESSURE");
   pragma Import (Java, CONTROL_CHANGE, "CONTROL_CHANGE");
   pragma Import (Java, PROGRAM_CHANGE, "PROGRAM_CHANGE");
   pragma Import (Java, CHANNEL_PRESSURE, "CHANNEL_PRESSURE");
   pragma Import (Java, PITCH_BEND, "PITCH_BEND");

end Javax.Sound.Midi.ShortMessage;
pragma Import (Java, Javax.Sound.Midi.ShortMessage, "javax.sound.midi.ShortMessage");
pragma Extensions_Allowed (Off);
