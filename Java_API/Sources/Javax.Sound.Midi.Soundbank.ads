pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Sound.Midi.Instrument;
limited with Javax.Sound.Midi.Patch;
limited with Javax.Sound.Midi.SoundbankResource;
with Java.Lang.Object;

package Javax.Sound.Midi.Soundbank is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function GetVendor (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetResources (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   function GetInstruments (This : access Typ)
                            return Standard.Java.Lang.Object.Ref is abstract;

   function GetInstrument (This : access Typ;
                           P1_Patch : access Standard.Javax.Sound.Midi.Patch.Typ'Class)
                           return access Javax.Sound.Midi.Instrument.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, GetVendor, "getVendor");
   pragma Export (Java, GetDescription, "getDescription");
   pragma Export (Java, GetResources, "getResources");
   pragma Export (Java, GetInstruments, "getInstruments");
   pragma Export (Java, GetInstrument, "getInstrument");

end Javax.Sound.Midi.Soundbank;
pragma Import (Java, Javax.Sound.Midi.Soundbank, "javax.sound.midi.Soundbank");
pragma Extensions_Allowed (Off);
