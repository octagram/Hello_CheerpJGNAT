pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Sound.Midi.Soundbank;
with Java.Lang.Object;

package Javax.Sound.Midi.SoundbankResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SoundbankResource (P1_Soundbank : access Standard.Javax.Sound.Midi.Soundbank.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                                   P3_Class : access Standard.Java.Lang.Class.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSoundbank (This : access Typ)
                          return access Javax.Sound.Midi.Soundbank.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetDataClass (This : access Typ)
                          return access Java.Lang.Class.Typ'Class;

   function GetData (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SoundbankResource);
   pragma Import (Java, GetSoundbank, "getSoundbank");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetDataClass, "getDataClass");
   pragma Export (Java, GetData, "getData");

end Javax.Sound.Midi.SoundbankResource;
pragma Import (Java, Javax.Sound.Midi.SoundbankResource, "javax.sound.midi.SoundbankResource");
pragma Extensions_Allowed (Off);
