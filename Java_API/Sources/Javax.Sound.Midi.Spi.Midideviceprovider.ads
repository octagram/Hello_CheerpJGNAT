pragma Extensions_Allowed (On);
limited with Javax.Sound.Midi.MidiDevice;
limited with Javax.Sound.Midi.MidiDevice.Info;
with Java.Lang.Object;

package Javax.Sound.Midi.Spi.MidiDeviceProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MidiDeviceProvider (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsDeviceSupported (This : access Typ;
                               P1_Info : access Standard.Javax.Sound.Midi.MidiDevice.Info.Typ'Class)
                               return Java.Boolean;

   function GetDeviceInfo (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;

   function GetDevice (This : access Typ;
                       P1_Info : access Standard.Javax.Sound.Midi.MidiDevice.Info.Typ'Class)
                       return access Javax.Sound.Midi.MidiDevice.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MidiDeviceProvider);
   pragma Import (Java, IsDeviceSupported, "isDeviceSupported");
   pragma Export (Java, GetDeviceInfo, "getDeviceInfo");
   pragma Export (Java, GetDevice, "getDevice");

end Javax.Sound.Midi.Spi.MidiDeviceProvider;
pragma Import (Java, Javax.Sound.Midi.Spi.MidiDeviceProvider, "javax.sound.midi.spi.MidiDeviceProvider");
pragma Extensions_Allowed (Off);
