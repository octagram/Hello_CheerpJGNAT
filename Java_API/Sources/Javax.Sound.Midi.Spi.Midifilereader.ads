pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Net.URL;
limited with Javax.Sound.Midi.MidiFileFormat;
limited with Javax.Sound.Midi.Sequence;
with Java.Lang.Object;

package Javax.Sound.Midi.Spi.MidiFileReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MidiFileReader (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMidiFileFormat (This : access Typ;
                               P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                               return access Javax.Sound.Midi.MidiFileFormat.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetMidiFileFormat (This : access Typ;
                               P1_URL : access Standard.Java.Net.URL.Typ'Class)
                               return access Javax.Sound.Midi.MidiFileFormat.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetMidiFileFormat (This : access Typ;
                               P1_File : access Standard.Java.Io.File.Typ'Class)
                               return access Javax.Sound.Midi.MidiFileFormat.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSequence (This : access Typ;
                         P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                         return access Javax.Sound.Midi.Sequence.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSequence (This : access Typ;
                         P1_URL : access Standard.Java.Net.URL.Typ'Class)
                         return access Javax.Sound.Midi.Sequence.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except

   function GetSequence (This : access Typ;
                         P1_File : access Standard.Java.Io.File.Typ'Class)
                         return access Javax.Sound.Midi.Sequence.Typ'Class is abstract;
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except and
   --  Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MidiFileReader);
   pragma Export (Java, GetMidiFileFormat, "getMidiFileFormat");
   pragma Export (Java, GetSequence, "getSequence");

end Javax.Sound.Midi.Spi.MidiFileReader;
pragma Import (Java, Javax.Sound.Midi.Spi.MidiFileReader, "javax.sound.midi.spi.MidiFileReader");
pragma Extensions_Allowed (Off);
