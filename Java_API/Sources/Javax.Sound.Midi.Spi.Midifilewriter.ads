pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.OutputStream;
limited with Javax.Sound.Midi.Sequence;
with Java.Lang.Object;

package Javax.Sound.Midi.Spi.MidiFileWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MidiFileWriter (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMidiFileTypes (This : access Typ)
                              return Java.Int_Arr is abstract;

   function GetMidiFileTypes (This : access Typ;
                              P1_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class)
                              return Java.Int_Arr is abstract;

   function IsFileTypeSupported (This : access Typ;
                                 P1_Int : Java.Int)
                                 return Java.Boolean;

   function IsFileTypeSupported (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class)
                                 return Java.Boolean;

   function Write (This : access Typ;
                   P1_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class;
                   P2_Int : Java.Int;
                   P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_Sequence : access Standard.Javax.Sound.Midi.Sequence.Typ'Class;
                   P2_Int : Java.Int;
                   P3_File : access Standard.Java.Io.File.Typ'Class)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MidiFileWriter);
   pragma Export (Java, GetMidiFileTypes, "getMidiFileTypes");
   pragma Export (Java, IsFileTypeSupported, "isFileTypeSupported");
   pragma Export (Java, Write, "write");

end Javax.Sound.Midi.Spi.MidiFileWriter;
pragma Import (Java, Javax.Sound.Midi.Spi.MidiFileWriter, "javax.sound.midi.spi.MidiFileWriter");
pragma Extensions_Allowed (Off);
