pragma Extensions_Allowed (On);
package Javax.Sound.Midi.Spi is
   pragma Preelaborate;
end Javax.Sound.Midi.Spi;
pragma Import (Java, Javax.Sound.Midi.Spi, "javax.sound.midi.spi");
pragma Extensions_Allowed (Off);
