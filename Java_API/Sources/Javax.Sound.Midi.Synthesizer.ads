pragma Extensions_Allowed (On);
limited with Javax.Sound.Midi.Instrument;
limited with Javax.Sound.Midi.MidiChannel;
limited with Javax.Sound.Midi.Patch;
limited with Javax.Sound.Midi.Soundbank;
limited with Javax.Sound.Midi.VoiceStatus;
with Java.Lang.Object;
with Javax.Sound.Midi.MidiDevice;

package Javax.Sound.Midi.Synthesizer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MidiDevice_I : Javax.Sound.Midi.MidiDevice.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMaxPolyphony (This : access Typ)
                             return Java.Int is abstract;

   function GetLatency (This : access Typ)
                        return Java.Long is abstract;

   function GetChannels (This : access Typ)
                         return Standard.Java.Lang.Object.Ref is abstract;

   function GetVoiceStatus (This : access Typ)
                            return Standard.Java.Lang.Object.Ref is abstract;

   function IsSoundbankSupported (This : access Typ;
                                  P1_Soundbank : access Standard.Javax.Sound.Midi.Soundbank.Typ'Class)
                                  return Java.Boolean is abstract;

   function LoadInstrument (This : access Typ;
                            P1_Instrument : access Standard.Javax.Sound.Midi.Instrument.Typ'Class)
                            return Java.Boolean is abstract;

   procedure UnloadInstrument (This : access Typ;
                               P1_Instrument : access Standard.Javax.Sound.Midi.Instrument.Typ'Class) is abstract;

   function RemapInstrument (This : access Typ;
                             P1_Instrument : access Standard.Javax.Sound.Midi.Instrument.Typ'Class;
                             P2_Instrument : access Standard.Javax.Sound.Midi.Instrument.Typ'Class)
                             return Java.Boolean is abstract;

   function GetDefaultSoundbank (This : access Typ)
                                 return access Javax.Sound.Midi.Soundbank.Typ'Class is abstract;

   function GetAvailableInstruments (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref is abstract;

   function GetLoadedInstruments (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref is abstract;

   function LoadAllInstruments (This : access Typ;
                                P1_Soundbank : access Standard.Javax.Sound.Midi.Soundbank.Typ'Class)
                                return Java.Boolean is abstract;

   procedure UnloadAllInstruments (This : access Typ;
                                   P1_Soundbank : access Standard.Javax.Sound.Midi.Soundbank.Typ'Class) is abstract;

   function LoadInstruments (This : access Typ;
                             P1_Soundbank : access Standard.Javax.Sound.Midi.Soundbank.Typ'Class;
                             P2_Patch_Arr : access Javax.Sound.Midi.Patch.Arr_Obj)
                             return Java.Boolean is abstract;

   procedure UnloadInstruments (This : access Typ;
                                P1_Soundbank : access Standard.Javax.Sound.Midi.Soundbank.Typ'Class;
                                P2_Patch_Arr : access Javax.Sound.Midi.Patch.Arr_Obj) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMaxPolyphony, "getMaxPolyphony");
   pragma Export (Java, GetLatency, "getLatency");
   pragma Export (Java, GetChannels, "getChannels");
   pragma Export (Java, GetVoiceStatus, "getVoiceStatus");
   pragma Export (Java, IsSoundbankSupported, "isSoundbankSupported");
   pragma Export (Java, LoadInstrument, "loadInstrument");
   pragma Export (Java, UnloadInstrument, "unloadInstrument");
   pragma Export (Java, RemapInstrument, "remapInstrument");
   pragma Export (Java, GetDefaultSoundbank, "getDefaultSoundbank");
   pragma Export (Java, GetAvailableInstruments, "getAvailableInstruments");
   pragma Export (Java, GetLoadedInstruments, "getLoadedInstruments");
   pragma Export (Java, LoadAllInstruments, "loadAllInstruments");
   pragma Export (Java, UnloadAllInstruments, "unloadAllInstruments");
   pragma Export (Java, LoadInstruments, "loadInstruments");
   pragma Export (Java, UnloadInstruments, "unloadInstruments");

end Javax.Sound.Midi.Synthesizer;
pragma Import (Java, Javax.Sound.Midi.Synthesizer, "javax.sound.midi.Synthesizer");
pragma Extensions_Allowed (Off);
