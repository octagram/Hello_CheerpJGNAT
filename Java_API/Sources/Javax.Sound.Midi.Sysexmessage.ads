pragma Extensions_Allowed (On);
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Sound.Midi.MidiMessage;

package Javax.Sound.Midi.SysexMessage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Sound.Midi.MidiMessage.Typ(Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SysexMessage (This : Ref := null)
                              return Ref;

   --  protected
   function New_SysexMessage (P1_Byte_Arr : Java.Byte_Arr; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMessage (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr;
                         P2_Int : Java.Int);
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   procedure SetMessage (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Byte_Arr : Java.Byte_Arr;
                         P3_Int : Java.Int);
   --  can raise Javax.Sound.Midi.InvalidMidiDataException.Except

   function GetData (This : access Typ)
                     return Java.Byte_Arr;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SYSTEM_EXCLUSIVE : constant Java.Int;

   --  final
   SPECIAL_SYSTEM_EXCLUSIVE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SysexMessage);
   pragma Import (Java, SetMessage, "setMessage");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, SYSTEM_EXCLUSIVE, "SYSTEM_EXCLUSIVE");
   pragma Import (Java, SPECIAL_SYSTEM_EXCLUSIVE, "SPECIAL_SYSTEM_EXCLUSIVE");

end Javax.Sound.Midi.SysexMessage;
pragma Import (Java, Javax.Sound.Midi.SysexMessage, "javax.sound.midi.SysexMessage");
pragma Extensions_Allowed (Off);
