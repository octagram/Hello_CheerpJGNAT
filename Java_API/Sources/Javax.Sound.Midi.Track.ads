pragma Extensions_Allowed (On);
limited with Javax.Sound.Midi.MidiEvent;
with Java.Lang.Object;

package Javax.Sound.Midi.Track is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Add (This : access Typ;
                 P1_MidiEvent : access Standard.Javax.Sound.Midi.MidiEvent.Typ'Class)
                 return Java.Boolean;

   function Remove (This : access Typ;
                    P1_MidiEvent : access Standard.Javax.Sound.Midi.MidiEvent.Typ'Class)
                    return Java.Boolean;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Javax.Sound.Midi.MidiEvent.Typ'Class;
   --  can raise Java.Lang.ArrayIndexOutOfBoundsException.Except

   function Size (This : access Typ)
                  return Java.Int;

   function Ticks (This : access Typ)
                   return Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Size, "size");
   pragma Import (Java, Ticks, "ticks");

end Javax.Sound.Midi.Track;
pragma Import (Java, Javax.Sound.Midi.Track, "javax.sound.midi.Track");
pragma Extensions_Allowed (Off);
