pragma Extensions_Allowed (On);
limited with Javax.Sound.Midi.Receiver;
with Java.Lang.Object;

package Javax.Sound.Midi.Transmitter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetReceiver (This : access Typ;
                          P1_Receiver : access Standard.Javax.Sound.Midi.Receiver.Typ'Class) is abstract;

   function GetReceiver (This : access Typ)
                         return access Javax.Sound.Midi.Receiver.Typ'Class is abstract;

   procedure Close (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetReceiver, "setReceiver");
   pragma Export (Java, GetReceiver, "getReceiver");
   pragma Export (Java, Close, "close");

end Javax.Sound.Midi.Transmitter;
pragma Import (Java, Javax.Sound.Midi.Transmitter, "javax.sound.midi.Transmitter");
pragma Extensions_Allowed (Off);
