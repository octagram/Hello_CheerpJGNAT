pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Sound.Midi.VoiceStatus is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Active : Java.Boolean;
      pragma Import (Java, Active, "active");

      Channel : Java.Int;
      pragma Import (Java, Channel, "channel");

      Bank : Java.Int;
      pragma Import (Java, Bank, "bank");

      Program : Java.Int;
      pragma Import (Java, Program, "program");

      Note : Java.Int;
      pragma Import (Java, Note, "note");

      Volume : Java.Int;
      pragma Import (Java, Volume, "volume");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_VoiceStatus (This : Ref := null)
                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_VoiceStatus);

end Javax.Sound.Midi.VoiceStatus;
pragma Import (Java, Javax.Sound.Midi.VoiceStatus, "javax.sound.midi.VoiceStatus");
pragma Extensions_Allowed (Off);
