pragma Extensions_Allowed (On);
package Javax.Sound.Midi is
   pragma Preelaborate;
end Javax.Sound.Midi;
pragma Import (Java, Javax.Sound.Midi, "javax.sound.midi");
pragma Extensions_Allowed (Off);
