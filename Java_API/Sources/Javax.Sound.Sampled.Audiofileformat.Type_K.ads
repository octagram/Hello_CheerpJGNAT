pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sound.Sampled.AudioFileFormat.Type_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Type_K (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetExtension (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WAVE : access Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;

   --  final
   AU : access Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;

   --  final
   AIFF : access Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;

   --  final
   AIFC : access Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;

   --  final
   SND : access Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Type_K);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetExtension, "getExtension");
   pragma Import (Java, WAVE, "WAVE");
   pragma Import (Java, AU, "AU");
   pragma Import (Java, AIFF, "AIFF");
   pragma Import (Java, AIFC, "AIFC");
   pragma Import (Java, SND, "SND");

end Javax.Sound.Sampled.AudioFileFormat.Type_K;
pragma Import (Java, Javax.Sound.Sampled.AudioFileFormat.Type_K, "javax.sound.sampled.AudioFileFormat$Type");
pragma Extensions_Allowed (Off);
