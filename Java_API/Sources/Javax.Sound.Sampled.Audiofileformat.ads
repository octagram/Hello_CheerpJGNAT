pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Sound.Sampled.AudioFileFormat.Type_K;
limited with Javax.Sound.Sampled.AudioFormat;
with Java.Lang.Object;

package Javax.Sound.Sampled.AudioFileFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_AudioFileFormat (P1_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                 P4_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_AudioFileFormat (P1_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                                 P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                 P3_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_AudioFileFormat (P1_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                                 P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Map : access Standard.Java.Util.Map.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;

   function GetByteLength (This : access Typ)
                           return Java.Int;

   function GetFormat (This : access Typ)
                       return access Javax.Sound.Sampled.AudioFormat.Typ'Class;

   function GetFrameLength (This : access Typ)
                            return Java.Int;

   function Properties (This : access Typ)
                        return access Java.Util.Map.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AudioFileFormat);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetByteLength, "getByteLength");
   pragma Import (Java, GetFormat, "getFormat");
   pragma Import (Java, GetFrameLength, "getFrameLength");
   pragma Import (Java, Properties, "properties");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.AudioFileFormat;
pragma Import (Java, Javax.Sound.Sampled.AudioFileFormat, "javax.sound.sampled.AudioFileFormat");
pragma Extensions_Allowed (Off);
