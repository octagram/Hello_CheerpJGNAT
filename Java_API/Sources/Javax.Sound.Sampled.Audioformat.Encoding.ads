pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sound.Sampled.AudioFormat.Encoding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Encoding (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PCM_SIGNED : access Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;

   --  final
   PCM_UNSIGNED : access Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;

   --  final
   ULAW : access Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;

   --  final
   ALAW : access Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Encoding);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, PCM_SIGNED, "PCM_SIGNED");
   pragma Import (Java, PCM_UNSIGNED, "PCM_UNSIGNED");
   pragma Import (Java, ULAW, "ULAW");
   pragma Import (Java, ALAW, "ALAW");

end Javax.Sound.Sampled.AudioFormat.Encoding;
pragma Import (Java, Javax.Sound.Sampled.AudioFormat.Encoding, "javax.sound.sampled.AudioFormat$Encoding");
pragma Extensions_Allowed (Off);
