pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Sound.Sampled.AudioFormat.Encoding;
with Java.Lang.Object;

package Javax.Sound.Sampled.AudioFormat is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Encoding : access Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
      pragma Import (Java, Encoding, "encoding");

      --  protected
      SampleRate : Java.Float;
      pragma Import (Java, SampleRate, "sampleRate");

      --  protected
      SampleSizeInBits : Java.Int;
      pragma Import (Java, SampleSizeInBits, "sampleSizeInBits");

      --  protected
      Channels : Java.Int;
      pragma Import (Java, Channels, "channels");

      --  protected
      FrameSize : Java.Int;
      pragma Import (Java, FrameSize, "frameSize");

      --  protected
      FrameRate : Java.Float;
      pragma Import (Java, FrameRate, "frameRate");

      --  protected
      BigEndian : Java.Boolean;
      pragma Import (Java, BigEndian, "bigEndian");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AudioFormat (P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                             P2_Float : Java.Float;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Float : Java.Float;
                             P7_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   function New_AudioFormat (P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                             P2_Float : Java.Float;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Float : Java.Float;
                             P7_Boolean : Java.Boolean;
                             P8_Map : access Standard.Java.Util.Map.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_AudioFormat (P1_Float : Java.Float;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Boolean : Java.Boolean;
                             P5_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEncoding (This : access Typ)
                         return access Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;

   function GetSampleRate (This : access Typ)
                           return Java.Float;

   function GetSampleSizeInBits (This : access Typ)
                                 return Java.Int;

   function GetChannels (This : access Typ)
                         return Java.Int;

   function GetFrameSize (This : access Typ)
                          return Java.Int;

   function GetFrameRate (This : access Typ)
                          return Java.Float;

   function IsBigEndian (This : access Typ)
                         return Java.Boolean;

   function Properties (This : access Typ)
                        return access Java.Util.Map.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function Matches (This : access Typ;
                     P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                     return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AudioFormat);
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, GetSampleRate, "getSampleRate");
   pragma Import (Java, GetSampleSizeInBits, "getSampleSizeInBits");
   pragma Import (Java, GetChannels, "getChannels");
   pragma Import (Java, GetFrameSize, "getFrameSize");
   pragma Import (Java, GetFrameRate, "getFrameRate");
   pragma Import (Java, IsBigEndian, "isBigEndian");
   pragma Import (Java, Properties, "properties");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, Matches, "matches");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.AudioFormat;
pragma Import (Java, Javax.Sound.Sampled.AudioFormat, "javax.sound.sampled.AudioFormat");
pragma Extensions_Allowed (Off);
