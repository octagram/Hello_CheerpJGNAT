pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.AudioFormat;
limited with Javax.Sound.Sampled.TargetDataLine;
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Javax.Sound.Sampled.AudioInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.InputStream.Typ(Closeable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Format : access Javax.Sound.Sampled.AudioFormat.Typ'Class;
      pragma Import (Java, Format, "format");

      --  protected
      FrameLength : Java.Long;
      pragma Import (Java, FrameLength, "frameLength");

      --  protected
      FrameSize : Java.Int;
      pragma Import (Java, FrameSize, "frameSize");

      --  protected
      FramePos : Java.Long;
      pragma Import (Java, FramePos, "framePos");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AudioInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                  P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                  P3_Long : Java.Long; 
                                  This : Ref := null)
                                  return Ref;

   function New_AudioInputStream (P1_TargetDataLine : access Standard.Javax.Sound.Sampled.TargetDataLine.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFormat (This : access Typ)
                       return access Javax.Sound.Sampled.AudioFormat.Typ'Class;

   function GetFrameLength (This : access Typ)
                            return Java.Long;

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   function Available (This : access Typ)
                       return Java.Int;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Mark (This : access Typ;
                   P1_Int : Java.Int);

   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except

   function MarkSupported (This : access Typ)
                           return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AudioInputStream);
   pragma Import (Java, GetFormat, "getFormat");
   pragma Import (Java, GetFrameLength, "getFrameLength");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Available, "available");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Mark, "mark");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, MarkSupported, "markSupported");

end Javax.Sound.Sampled.AudioInputStream;
pragma Import (Java, Javax.Sound.Sampled.AudioInputStream, "javax.sound.sampled.AudioInputStream");
pragma Extensions_Allowed (Off);
