pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Net.URL;
limited with Javax.Sound.Sampled.AudioFileFormat;
limited with Javax.Sound.Sampled.AudioFileFormat.Type_K;
limited with Javax.Sound.Sampled.AudioFormat;
limited with Javax.Sound.Sampled.AudioFormat.Encoding;
limited with Javax.Sound.Sampled.AudioInputStream;
limited with Javax.Sound.Sampled.Clip;
limited with Javax.Sound.Sampled.Line;
limited with Javax.Sound.Sampled.Line.Info;
limited with Javax.Sound.Sampled.Mixer;
limited with Javax.Sound.Sampled.Mixer.Info;
limited with Javax.Sound.Sampled.SourceDataLine;
limited with Javax.Sound.Sampled.TargetDataLine;
with Java.Lang.Object;

package Javax.Sound.Sampled.AudioSystem is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMixerInfo return Standard.Java.Lang.Object.Ref;

   function GetMixer (P1_Info : access Standard.Javax.Sound.Sampled.Mixer.Info.Typ'Class)
                      return access Javax.Sound.Sampled.Mixer.Typ'Class;

   function GetSourceLineInfo (P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                               return Standard.Java.Lang.Object.Ref;

   function GetTargetLineInfo (P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                               return Standard.Java.Lang.Object.Ref;

   function IsLineSupported (P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                             return Java.Boolean;

   function GetLine (P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                     return access Javax.Sound.Sampled.Line.Typ'Class;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetClip return access Javax.Sound.Sampled.Clip.Typ'Class;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetClip (P1_Info : access Standard.Javax.Sound.Sampled.Mixer.Info.Typ'Class)
                     return access Javax.Sound.Sampled.Clip.Typ'Class;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetSourceDataLine (P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                               return access Javax.Sound.Sampled.SourceDataLine.Typ'Class;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetSourceDataLine (P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                               P2_Info : access Standard.Javax.Sound.Sampled.Mixer.Info.Typ'Class)
                               return access Javax.Sound.Sampled.SourceDataLine.Typ'Class;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetTargetDataLine (P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                               return access Javax.Sound.Sampled.TargetDataLine.Typ'Class;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetTargetDataLine (P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                               P2_Info : access Standard.Javax.Sound.Sampled.Mixer.Info.Typ'Class)
                               return access Javax.Sound.Sampled.TargetDataLine.Typ'Class;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetTargetEncodings (P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class)
                                return Standard.Java.Lang.Object.Ref;

   function GetTargetEncodings (P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                                return Standard.Java.Lang.Object.Ref;

   function IsConversionSupported (P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                                   P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                                   return Java.Boolean;

   function GetAudioInputStream (P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                                 P2_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class;

   function GetTargetFormats (P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                              P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                              return Standard.Java.Lang.Object.Ref;

   function IsConversionSupported (P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                   P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                                   return Java.Boolean;

   function GetAudioInputStream (P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                 P2_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class;

   function GetAudioFileFormat (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                return access Javax.Sound.Sampled.AudioFileFormat.Typ'Class;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioFileFormat (P1_URL : access Standard.Java.Net.URL.Typ'Class)
                                return access Javax.Sound.Sampled.AudioFileFormat.Typ'Class;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioFileFormat (P1_File : access Standard.Java.Io.File.Typ'Class)
                                return access Javax.Sound.Sampled.AudioFileFormat.Typ'Class;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioInputStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioInputStream (P1_URL : access Standard.Java.Net.URL.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioInputStream (P1_File : access Standard.Java.Io.File.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioFileTypes return Standard.Java.Lang.Object.Ref;

   function IsFileTypeSupported (P1_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class)
                                 return Java.Boolean;

   function GetAudioFileTypes (P1_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                               return Standard.Java.Lang.Object.Ref;

   function IsFileTypeSupported (P1_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                                 P2_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                                 return Java.Boolean;

   function Write (P1_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class;
                   P2_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                   P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                   return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Write (P1_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class;
                   P2_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                   P3_File : access Standard.Java.Io.File.Typ'Class)
                   return Java.Int;
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NOT_SPECIFIED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetMixerInfo, "getMixerInfo");
   pragma Import (Java, GetMixer, "getMixer");
   pragma Import (Java, GetSourceLineInfo, "getSourceLineInfo");
   pragma Import (Java, GetTargetLineInfo, "getTargetLineInfo");
   pragma Import (Java, IsLineSupported, "isLineSupported");
   pragma Import (Java, GetLine, "getLine");
   pragma Import (Java, GetClip, "getClip");
   pragma Import (Java, GetSourceDataLine, "getSourceDataLine");
   pragma Import (Java, GetTargetDataLine, "getTargetDataLine");
   pragma Import (Java, GetTargetEncodings, "getTargetEncodings");
   pragma Import (Java, IsConversionSupported, "isConversionSupported");
   pragma Import (Java, GetAudioInputStream, "getAudioInputStream");
   pragma Import (Java, GetTargetFormats, "getTargetFormats");
   pragma Import (Java, GetAudioFileFormat, "getAudioFileFormat");
   pragma Import (Java, GetAudioFileTypes, "getAudioFileTypes");
   pragma Import (Java, IsFileTypeSupported, "isFileTypeSupported");
   pragma Import (Java, Write, "write");
   pragma Import (Java, NOT_SPECIFIED, "NOT_SPECIFIED");

end Javax.Sound.Sampled.AudioSystem;
pragma Import (Java, Javax.Sound.Sampled.AudioSystem, "javax.sound.sampled.AudioSystem");
pragma Extensions_Allowed (Off);
