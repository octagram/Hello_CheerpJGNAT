pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Sound.Sampled.Control.Type_K;

package Javax.Sound.Sampled.BooleanControl.Type_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Sound.Sampled.Control.Type_K.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Type_K (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MUTE : access Javax.Sound.Sampled.BooleanControl.Type_K.Typ'Class;

   --  final
   APPLY_REVERB : access Javax.Sound.Sampled.BooleanControl.Type_K.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Type_K);
   pragma Import (Java, MUTE, "MUTE");
   pragma Import (Java, APPLY_REVERB, "APPLY_REVERB");

end Javax.Sound.Sampled.BooleanControl.Type_K;
pragma Import (Java, Javax.Sound.Sampled.BooleanControl.Type_K, "javax.sound.sampled.BooleanControl$Type");
pragma Extensions_Allowed (Off);
