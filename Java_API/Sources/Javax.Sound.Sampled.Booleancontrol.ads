pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Sound.Sampled.BooleanControl.Type_K;
with Java.Lang.Object;
with Javax.Sound.Sampled.Control;

package Javax.Sound.Sampled.BooleanControl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Sound.Sampled.Control.Typ
      with null record;

   --  protected
   function New_BooleanControl (P1_Type_K : access Standard.Javax.Sound.Sampled.BooleanControl.Type_K.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   --  protected
   function New_BooleanControl (P1_Type_K : access Standard.Javax.Sound.Sampled.BooleanControl.Type_K.Typ'Class;
                                P2_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetValue (This : access Typ;
                       P1_Boolean : Java.Boolean);

   function GetValue (This : access Typ)
                      return Java.Boolean;

   function GetStateLabel (This : access Typ;
                           P1_Boolean : Java.Boolean)
                           return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BooleanControl);
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetStateLabel, "getStateLabel");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.BooleanControl;
pragma Import (Java, Javax.Sound.Sampled.BooleanControl, "javax.sound.sampled.BooleanControl");
pragma Extensions_Allowed (Off);
