pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.AudioFormat;
limited with Javax.Sound.Sampled.AudioInputStream;
with Java.Lang.Object;
with Javax.Sound.Sampled.DataLine;

package Javax.Sound.Sampled.Clip is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DataLine_I : Javax.Sound.Sampled.DataLine.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Open (This : access Typ;
                   P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                   P2_Byte_Arr : Java.Byte_Arr;
                   P3_Int : Java.Int;
                   P4_Int : Java.Int) is abstract;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   procedure Open (This : access Typ;
                   P1_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class) is abstract;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except and
   --  Java.Io.IOException.Except

   function GetFrameLength (This : access Typ)
                            return Java.Int is abstract;

   function GetMicrosecondLength (This : access Typ)
                                  return Java.Long is abstract;

   procedure SetFramePosition (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   procedure SetMicrosecondPosition (This : access Typ;
                                     P1_Long : Java.Long) is abstract;

   procedure SetLoopPoints (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int) is abstract;

   procedure loop_K (This : access Typ;
                     P1_Int : Java.Int) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LOOP_CONTINUOUSLY : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Open, "open");
   pragma Export (Java, GetFrameLength, "getFrameLength");
   pragma Export (Java, GetMicrosecondLength, "getMicrosecondLength");
   pragma Export (Java, SetFramePosition, "setFramePosition");
   pragma Export (Java, SetMicrosecondPosition, "setMicrosecondPosition");
   pragma Export (Java, SetLoopPoints, "setLoopPoints");
   pragma Export (Java, loop_K, "loop");
   pragma Import (Java, LOOP_CONTINUOUSLY, "LOOP_CONTINUOUSLY");

end Javax.Sound.Sampled.Clip;
pragma Import (Java, Javax.Sound.Sampled.Clip, "javax.sound.sampled.Clip");
pragma Extensions_Allowed (Off);
