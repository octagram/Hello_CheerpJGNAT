pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Sound.Sampled.Control.Type_K;

package Javax.Sound.Sampled.CompoundControl.Type_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Sound.Sampled.Control.Type_K.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Type_K (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Type_K);

end Javax.Sound.Sampled.CompoundControl.Type_K;
pragma Import (Java, Javax.Sound.Sampled.CompoundControl.Type_K, "javax.sound.sampled.CompoundControl$Type");
pragma Extensions_Allowed (Off);
