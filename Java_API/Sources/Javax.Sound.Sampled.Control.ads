pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Sound.Sampled.Control.Type_K;
with Java.Lang.Object;

package Javax.Sound.Sampled.Control is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Control (P1_Type_K : access Standard.Javax.Sound.Sampled.Control.Type_K.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Javax.Sound.Sampled.Control.Type_K.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Control);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.Control;
pragma Import (Java, Javax.Sound.Sampled.Control, "javax.sound.sampled.Control");
pragma Extensions_Allowed (Off);
