pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Sound.Sampled.AudioFormat;
with Java.Lang.Object;
with Javax.Sound.Sampled.Line.Info;

package Javax.Sound.Sampled.DataLine.Info is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Sound.Sampled.Line.Info.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Info (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                      P2_AudioFormat_Arr : access Javax.Sound.Sampled.AudioFormat.Arr_Obj;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int; 
                      This : Ref := null)
                      return Ref;

   function New_Info (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                      P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                      P3_Int : Java.Int; 
                      This : Ref := null)
                      return Ref;

   function New_Info (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                      P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFormats (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function IsFormatSupported (This : access Typ;
                               P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                               return Java.Boolean;

   function GetMinBufferSize (This : access Typ)
                              return Java.Int;

   function GetMaxBufferSize (This : access Typ)
                              return Java.Int;

   function Matches (This : access Typ;
                     P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                     return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Info);
   pragma Import (Java, GetFormats, "getFormats");
   pragma Import (Java, IsFormatSupported, "isFormatSupported");
   pragma Import (Java, GetMinBufferSize, "getMinBufferSize");
   pragma Import (Java, GetMaxBufferSize, "getMaxBufferSize");
   pragma Import (Java, Matches, "matches");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.DataLine.Info;
pragma Import (Java, Javax.Sound.Sampled.DataLine.Info, "javax.sound.sampled.DataLine$Info");
pragma Extensions_Allowed (Off);
