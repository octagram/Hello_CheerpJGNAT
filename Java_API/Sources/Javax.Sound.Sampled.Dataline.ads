pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.AudioFormat;
with Java.Lang.Object;
with Javax.Sound.Sampled.Line;

package Javax.Sound.Sampled.DataLine is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Line_I : Javax.Sound.Sampled.Line.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Drain (This : access Typ) is abstract;

   procedure Flush (This : access Typ) is abstract;

   procedure Start (This : access Typ) is abstract;

   procedure Stop (This : access Typ) is abstract;

   function IsRunning (This : access Typ)
                       return Java.Boolean is abstract;

   function IsActive (This : access Typ)
                      return Java.Boolean is abstract;

   function GetFormat (This : access Typ)
                       return access Javax.Sound.Sampled.AudioFormat.Typ'Class is abstract;

   function GetBufferSize (This : access Typ)
                           return Java.Int is abstract;

   function Available (This : access Typ)
                       return Java.Int is abstract;

   function GetFramePosition (This : access Typ)
                              return Java.Int is abstract;

   function GetLongFramePosition (This : access Typ)
                                  return Java.Long is abstract;

   function GetMicrosecondPosition (This : access Typ)
                                    return Java.Long is abstract;

   function GetLevel (This : access Typ)
                      return Java.Float is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Drain, "drain");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, Start, "start");
   pragma Export (Java, Stop, "stop");
   pragma Export (Java, IsRunning, "isRunning");
   pragma Export (Java, IsActive, "isActive");
   pragma Export (Java, GetFormat, "getFormat");
   pragma Export (Java, GetBufferSize, "getBufferSize");
   pragma Export (Java, Available, "available");
   pragma Export (Java, GetFramePosition, "getFramePosition");
   pragma Export (Java, GetLongFramePosition, "getLongFramePosition");
   pragma Export (Java, GetMicrosecondPosition, "getMicrosecondPosition");
   pragma Export (Java, GetLevel, "getLevel");

end Javax.Sound.Sampled.DataLine;
pragma Import (Java, Javax.Sound.Sampled.DataLine, "javax.sound.sampled.DataLine");
pragma Extensions_Allowed (Off);
