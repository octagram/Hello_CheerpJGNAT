pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Sound.Sampled.EnumControl.Type_K;
with Java.Lang.Object;
with Javax.Sound.Sampled.Control;

package Javax.Sound.Sampled.EnumControl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Sound.Sampled.Control.Typ
      with null record;

   --  protected
   function New_EnumControl (P1_Type_K : access Standard.Javax.Sound.Sampled.EnumControl.Type_K.Typ'Class;
                             P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function GetValues (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EnumControl);
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetValues, "getValues");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.EnumControl;
pragma Import (Java, Javax.Sound.Sampled.EnumControl, "javax.sound.sampled.EnumControl");
pragma Extensions_Allowed (Off);
