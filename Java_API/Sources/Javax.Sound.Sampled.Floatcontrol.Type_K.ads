pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Sound.Sampled.Control.Type_K;

package Javax.Sound.Sampled.FloatControl.Type_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Sound.Sampled.Control.Type_K.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Type_K (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MASTER_GAIN : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   AUX_SEND : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   AUX_RETURN : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   REVERB_SEND : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   REVERB_RETURN : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   VOLUME : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   PAN : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   BALANCE : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;

   --  final
   SAMPLE_RATE : access Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Type_K);
   pragma Import (Java, MASTER_GAIN, "MASTER_GAIN");
   pragma Import (Java, AUX_SEND, "AUX_SEND");
   pragma Import (Java, AUX_RETURN, "AUX_RETURN");
   pragma Import (Java, REVERB_SEND, "REVERB_SEND");
   pragma Import (Java, REVERB_RETURN, "REVERB_RETURN");
   pragma Import (Java, VOLUME, "VOLUME");
   pragma Import (Java, PAN, "PAN");
   pragma Import (Java, BALANCE, "BALANCE");
   pragma Import (Java, SAMPLE_RATE, "SAMPLE_RATE");

end Javax.Sound.Sampled.FloatControl.Type_K;
pragma Import (Java, Javax.Sound.Sampled.FloatControl.Type_K, "javax.sound.sampled.FloatControl$Type");
pragma Extensions_Allowed (Off);
