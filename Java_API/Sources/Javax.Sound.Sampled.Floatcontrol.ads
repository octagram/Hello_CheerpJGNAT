pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Sound.Sampled.FloatControl.Type_K;
with Java.Lang.Object;
with Javax.Sound.Sampled.Control;

package Javax.Sound.Sampled.FloatControl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Sound.Sampled.Control.Typ
      with null record;

   --  protected
   function New_FloatControl (P1_Type_K : access Standard.Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;
                              P2_Float : Java.Float;
                              P3_Float : Java.Float;
                              P4_Float : Java.Float;
                              P5_Int : Java.Int;
                              P6_Float : Java.Float;
                              P7_String : access Standard.Java.Lang.String.Typ'Class;
                              P8_String : access Standard.Java.Lang.String.Typ'Class;
                              P9_String : access Standard.Java.Lang.String.Typ'Class;
                              P10_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   --  protected
   function New_FloatControl (P1_Type_K : access Standard.Javax.Sound.Sampled.FloatControl.Type_K.Typ'Class;
                              P2_Float : Java.Float;
                              P3_Float : Java.Float;
                              P4_Float : Java.Float;
                              P5_Int : Java.Int;
                              P6_Float : Java.Float;
                              P7_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetValue (This : access Typ;
                       P1_Float : Java.Float);

   function GetValue (This : access Typ)
                      return Java.Float;

   function GetMaximum (This : access Typ)
                        return Java.Float;

   function GetMinimum (This : access Typ)
                        return Java.Float;

   function GetUnits (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetMinLabel (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetMidLabel (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetMaxLabel (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetPrecision (This : access Typ)
                          return Java.Float;

   function GetUpdatePeriod (This : access Typ)
                             return Java.Int;

   procedure Shift (This : access Typ;
                    P1_Float : Java.Float;
                    P2_Float : Java.Float;
                    P3_Int : Java.Int);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FloatControl);
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, GetUnits, "getUnits");
   pragma Import (Java, GetMinLabel, "getMinLabel");
   pragma Import (Java, GetMidLabel, "getMidLabel");
   pragma Import (Java, GetMaxLabel, "getMaxLabel");
   pragma Import (Java, GetPrecision, "getPrecision");
   pragma Import (Java, GetUpdatePeriod, "getUpdatePeriod");
   pragma Import (Java, Shift, "shift");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.FloatControl;
pragma Import (Java, Javax.Sound.Sampled.FloatControl, "javax.sound.sampled.FloatControl");
pragma Extensions_Allowed (Off);
