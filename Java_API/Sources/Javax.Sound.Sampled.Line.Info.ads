pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sound.Sampled.Line.Info is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Info (P1_Class : access Standard.Java.Lang.Class.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLineClass (This : access Typ)
                          return access Java.Lang.Class.Typ'Class;

   function Matches (This : access Typ;
                     P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                     return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Info);
   pragma Import (Java, GetLineClass, "getLineClass");
   pragma Import (Java, Matches, "matches");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.Line.Info;
pragma Import (Java, Javax.Sound.Sampled.Line.Info, "javax.sound.sampled.Line$Info");
pragma Extensions_Allowed (Off);
