pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.Control;
limited with Javax.Sound.Sampled.Control.Type_K;
limited with Javax.Sound.Sampled.Line.Info;
limited with Javax.Sound.Sampled.LineListener;
with Java.Lang.Object;

package Javax.Sound.Sampled.Line is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLineInfo (This : access Typ)
                         return access Javax.Sound.Sampled.Line.Info.Typ'Class is abstract;

   procedure Open (This : access Typ) is abstract;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   procedure Close (This : access Typ) is abstract;

   function IsOpen (This : access Typ)
                    return Java.Boolean is abstract;

   function GetControls (This : access Typ)
                         return Standard.Java.Lang.Object.Ref is abstract;

   function IsControlSupported (This : access Typ;
                                P1_Type_K : access Standard.Javax.Sound.Sampled.Control.Type_K.Typ'Class)
                                return Java.Boolean is abstract;

   function GetControl (This : access Typ;
                        P1_Type_K : access Standard.Javax.Sound.Sampled.Control.Type_K.Typ'Class)
                        return access Javax.Sound.Sampled.Control.Typ'Class is abstract;

   procedure AddLineListener (This : access Typ;
                              P1_LineListener : access Standard.Javax.Sound.Sampled.LineListener.Typ'Class) is abstract;

   procedure RemoveLineListener (This : access Typ;
                                 P1_LineListener : access Standard.Javax.Sound.Sampled.LineListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLineInfo, "getLineInfo");
   pragma Export (Java, Open, "open");
   pragma Export (Java, Close, "close");
   pragma Export (Java, IsOpen, "isOpen");
   pragma Export (Java, GetControls, "getControls");
   pragma Export (Java, IsControlSupported, "isControlSupported");
   pragma Export (Java, GetControl, "getControl");
   pragma Export (Java, AddLineListener, "addLineListener");
   pragma Export (Java, RemoveLineListener, "removeLineListener");

end Javax.Sound.Sampled.Line;
pragma Import (Java, Javax.Sound.Sampled.Line, "javax.sound.sampled.Line");
pragma Extensions_Allowed (Off);
