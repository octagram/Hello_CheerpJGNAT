pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Sound.Sampled.Line;
limited with Javax.Sound.Sampled.LineEvent.Type_K;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Sound.Sampled.LineEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LineEvent (P1_Line : access Standard.Javax.Sound.Sampled.Line.Typ'Class;
                           P2_Type_K : access Standard.Javax.Sound.Sampled.LineEvent.Type_K.Typ'Class;
                           P3_Long : Java.Long; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetLine (This : access Typ)
                     return access Javax.Sound.Sampled.Line.Typ'Class;

   --  final
   function GetType (This : access Typ)
                     return access Javax.Sound.Sampled.LineEvent.Type_K.Typ'Class;

   --  final
   function GetFramePosition (This : access Typ)
                              return Java.Long;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LineEvent);
   pragma Import (Java, GetLine, "getLine");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetFramePosition, "getFramePosition");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.LineEvent;
pragma Import (Java, Javax.Sound.Sampled.LineEvent, "javax.sound.sampled.LineEvent");
pragma Extensions_Allowed (Off);
