pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sound.Sampled.Mixer.Info is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Info (P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_String : access Standard.Java.Lang.String.Typ'Class;
                      P4_String : access Standard.Java.Lang.String.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetVendor (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   --  final
   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   --  final
   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Info);
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetVendor, "getVendor");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetVersion, "getVersion");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.Mixer.Info;
pragma Import (Java, Javax.Sound.Sampled.Mixer.Info, "javax.sound.sampled.Mixer$Info");
pragma Extensions_Allowed (Off);
