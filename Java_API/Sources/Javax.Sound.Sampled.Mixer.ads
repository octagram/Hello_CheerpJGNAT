pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.Line.Info;
limited with Javax.Sound.Sampled.Mixer.Info;
with Java.Lang.Object;
with Javax.Sound.Sampled.Line;

package Javax.Sound.Sampled.Mixer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Line_I : Javax.Sound.Sampled.Line.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMixerInfo (This : access Typ)
                          return access Javax.Sound.Sampled.Mixer.Info.Typ'Class is abstract;

   function GetSourceLineInfo (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function GetTargetLineInfo (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function GetSourceLineInfo (This : access Typ;
                               P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function GetTargetLineInfo (This : access Typ;
                               P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function IsLineSupported (This : access Typ;
                             P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                             return Java.Boolean is abstract;

   function GetLine (This : access Typ;
                     P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                     return access Javax.Sound.Sampled.Line.Typ'Class is abstract;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function GetMaxLines (This : access Typ;
                         P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                         return Java.Int is abstract;

   function GetSourceLines (This : access Typ)
                            return Standard.Java.Lang.Object.Ref is abstract;

   function GetTargetLines (This : access Typ)
                            return Standard.Java.Lang.Object.Ref is abstract;

   procedure Synchronize (This : access Typ;
                          P1_Line_Arr : access Javax.Sound.Sampled.Line.Arr_Obj;
                          P2_Boolean : Java.Boolean) is abstract;

   procedure Unsynchronize (This : access Typ;
                            P1_Line_Arr : access Javax.Sound.Sampled.Line.Arr_Obj) is abstract;

   function IsSynchronizationSupported (This : access Typ;
                                        P1_Line_Arr : access Javax.Sound.Sampled.Line.Arr_Obj;
                                        P2_Boolean : Java.Boolean)
                                        return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMixerInfo, "getMixerInfo");
   pragma Export (Java, GetSourceLineInfo, "getSourceLineInfo");
   pragma Export (Java, GetTargetLineInfo, "getTargetLineInfo");
   pragma Export (Java, IsLineSupported, "isLineSupported");
   pragma Export (Java, GetLine, "getLine");
   pragma Export (Java, GetMaxLines, "getMaxLines");
   pragma Export (Java, GetSourceLines, "getSourceLines");
   pragma Export (Java, GetTargetLines, "getTargetLines");
   pragma Export (Java, Synchronize, "synchronize");
   pragma Export (Java, Unsynchronize, "unsynchronize");
   pragma Export (Java, IsSynchronizationSupported, "isSynchronizationSupported");

end Javax.Sound.Sampled.Mixer;
pragma Import (Java, Javax.Sound.Sampled.Mixer, "javax.sound.sampled.Mixer");
pragma Extensions_Allowed (Off);
