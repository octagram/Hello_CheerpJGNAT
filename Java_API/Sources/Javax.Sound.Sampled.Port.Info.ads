pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Sound.Sampled.Line.Info;

package Javax.Sound.Sampled.Port.Info is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Sound.Sampled.Line.Info.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Info (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_Boolean : Java.Boolean; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function IsSource (This : access Typ)
                      return Java.Boolean;

   function Matches (This : access Typ;
                     P1_Info : access Standard.Javax.Sound.Sampled.Line.Info.Typ'Class)
                     return Java.Boolean;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MICROPHONE : access Javax.Sound.Sampled.Port.Info.Typ'Class;

   --  final
   LINE_IN : access Javax.Sound.Sampled.Port.Info.Typ'Class;

   --  final
   COMPACT_DISC : access Javax.Sound.Sampled.Port.Info.Typ'Class;

   --  final
   SPEAKER : access Javax.Sound.Sampled.Port.Info.Typ'Class;

   --  final
   HEADPHONE : access Javax.Sound.Sampled.Port.Info.Typ'Class;

   --  final
   LINE_OUT : access Javax.Sound.Sampled.Port.Info.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Info);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, IsSource, "isSource");
   pragma Import (Java, Matches, "matches");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, MICROPHONE, "MICROPHONE");
   pragma Import (Java, LINE_IN, "LINE_IN");
   pragma Import (Java, COMPACT_DISC, "COMPACT_DISC");
   pragma Import (Java, SPEAKER, "SPEAKER");
   pragma Import (Java, HEADPHONE, "HEADPHONE");
   pragma Import (Java, LINE_OUT, "LINE_OUT");

end Javax.Sound.Sampled.Port.Info;
pragma Import (Java, Javax.Sound.Sampled.Port.Info, "javax.sound.sampled.Port$Info");
pragma Extensions_Allowed (Off);
