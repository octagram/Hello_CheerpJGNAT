pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sound.Sampled.ReverbType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ReverbType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Float : Java.Float;
                            P4_Int : Java.Int;
                            P5_Float : Java.Float;
                            P6_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  final
   function GetEarlyReflectionDelay (This : access Typ)
                                     return Java.Int;

   --  final
   function GetEarlyReflectionIntensity (This : access Typ)
                                         return Java.Float;

   --  final
   function GetLateReflectionDelay (This : access Typ)
                                    return Java.Int;

   --  final
   function GetLateReflectionIntensity (This : access Typ)
                                        return Java.Float;

   --  final
   function GetDecayTime (This : access Typ)
                          return Java.Int;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   --  final
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ReverbType);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetEarlyReflectionDelay, "getEarlyReflectionDelay");
   pragma Import (Java, GetEarlyReflectionIntensity, "getEarlyReflectionIntensity");
   pragma Import (Java, GetLateReflectionDelay, "getLateReflectionDelay");
   pragma Import (Java, GetLateReflectionIntensity, "getLateReflectionIntensity");
   pragma Import (Java, GetDecayTime, "getDecayTime");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Sound.Sampled.ReverbType;
pragma Import (Java, Javax.Sound.Sampled.ReverbType, "javax.sound.sampled.ReverbType");
pragma Extensions_Allowed (Off);
