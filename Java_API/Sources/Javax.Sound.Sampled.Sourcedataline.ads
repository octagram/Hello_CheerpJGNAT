pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.AudioFormat;
with Java.Lang.Object;
with Javax.Sound.Sampled.DataLine;

package Javax.Sound.Sampled.SourceDataLine is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DataLine_I : Javax.Sound.Sampled.DataLine.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Open (This : access Typ;
                   P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                   P2_Int : Java.Int) is abstract;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   procedure Open (This : access Typ;
                   P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class) is abstract;
   --  can raise Javax.Sound.Sampled.LineUnavailableException.Except

   function Write (This : access Typ;
                   P1_Byte_Arr : Java.Byte_Arr;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int)
                   return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Open, "open");
   pragma Export (Java, Write, "write");

end Javax.Sound.Sampled.SourceDataLine;
pragma Import (Java, Javax.Sound.Sampled.SourceDataLine, "javax.sound.sampled.SourceDataLine");
pragma Extensions_Allowed (Off);
