pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Net.URL;
limited with Javax.Sound.Sampled.AudioFileFormat;
limited with Javax.Sound.Sampled.AudioInputStream;
with Java.Lang.Object;

package Javax.Sound.Sampled.Spi.AudioFileReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AudioFileReader (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAudioFileFormat (This : access Typ;
                                P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                return access Javax.Sound.Sampled.AudioFileFormat.Typ'Class is abstract;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioFileFormat (This : access Typ;
                                P1_URL : access Standard.Java.Net.URL.Typ'Class)
                                return access Javax.Sound.Sampled.AudioFileFormat.Typ'Class is abstract;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioFileFormat (This : access Typ;
                                P1_File : access Standard.Java.Io.File.Typ'Class)
                                return access Javax.Sound.Sampled.AudioFileFormat.Typ'Class is abstract;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioInputStream (This : access Typ;
                                 P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class is abstract;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioInputStream (This : access Typ;
                                 P1_URL : access Standard.Java.Net.URL.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class is abstract;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except

   function GetAudioInputStream (This : access Typ;
                                 P1_File : access Standard.Java.Io.File.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class is abstract;
   --  can raise Javax.Sound.Sampled.UnsupportedAudioFileException.Except and
   --  Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AudioFileReader);
   pragma Export (Java, GetAudioFileFormat, "getAudioFileFormat");
   pragma Export (Java, GetAudioInputStream, "getAudioInputStream");

end Javax.Sound.Sampled.Spi.AudioFileReader;
pragma Import (Java, Javax.Sound.Sampled.Spi.AudioFileReader, "javax.sound.sampled.spi.AudioFileReader");
pragma Extensions_Allowed (Off);
