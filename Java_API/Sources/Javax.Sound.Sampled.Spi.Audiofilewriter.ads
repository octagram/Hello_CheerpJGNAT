pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.OutputStream;
limited with Javax.Sound.Sampled.AudioFileFormat.Type_K;
limited with Javax.Sound.Sampled.AudioInputStream;
with Java.Lang.Object;

package Javax.Sound.Sampled.Spi.AudioFileWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AudioFileWriter (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAudioFileTypes (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function IsFileTypeSupported (This : access Typ;
                                 P1_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class)
                                 return Java.Boolean;

   function GetAudioFileTypes (This : access Typ;
                               P1_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function IsFileTypeSupported (This : access Typ;
                                 P1_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                                 P2_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                                 return Java.Boolean;

   function Write (This : access Typ;
                   P1_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class;
                   P2_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                   P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except

   function Write (This : access Typ;
                   P1_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class;
                   P2_Type_K : access Standard.Javax.Sound.Sampled.AudioFileFormat.Type_K.Typ'Class;
                   P3_File : access Standard.Java.Io.File.Typ'Class)
                   return Java.Int is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AudioFileWriter);
   pragma Export (Java, GetAudioFileTypes, "getAudioFileTypes");
   pragma Export (Java, IsFileTypeSupported, "isFileTypeSupported");
   pragma Export (Java, Write, "write");

end Javax.Sound.Sampled.Spi.AudioFileWriter;
pragma Import (Java, Javax.Sound.Sampled.Spi.AudioFileWriter, "javax.sound.sampled.spi.AudioFileWriter");
pragma Extensions_Allowed (Off);
