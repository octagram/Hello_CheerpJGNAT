pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.AudioFormat;
limited with Javax.Sound.Sampled.AudioFormat.Encoding;
limited with Javax.Sound.Sampled.AudioInputStream;
with Java.Lang.Object;

package Javax.Sound.Sampled.Spi.FormatConversionProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FormatConversionProvider (This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSourceEncodings (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;

   function GetTargetEncodings (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;

   function IsSourceEncodingSupported (This : access Typ;
                                       P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class)
                                       return Java.Boolean;

   function IsTargetEncodingSupported (This : access Typ;
                                       P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class)
                                       return Java.Boolean;

   function GetTargetEncodings (This : access Typ;
                                P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                                return Standard.Java.Lang.Object.Ref is abstract;

   function IsConversionSupported (This : access Typ;
                                   P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                                   P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                                   return Java.Boolean;

   function GetTargetFormats (This : access Typ;
                              P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                              P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                              return Standard.Java.Lang.Object.Ref is abstract;

   function IsConversionSupported (This : access Typ;
                                   P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                   P2_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class)
                                   return Java.Boolean;

   function GetAudioInputStream (This : access Typ;
                                 P1_Encoding : access Standard.Javax.Sound.Sampled.AudioFormat.Encoding.Typ'Class;
                                 P2_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class is abstract;

   function GetAudioInputStream (This : access Typ;
                                 P1_AudioFormat : access Standard.Javax.Sound.Sampled.AudioFormat.Typ'Class;
                                 P2_AudioInputStream : access Standard.Javax.Sound.Sampled.AudioInputStream.Typ'Class)
                                 return access Javax.Sound.Sampled.AudioInputStream.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FormatConversionProvider);
   pragma Export (Java, GetSourceEncodings, "getSourceEncodings");
   pragma Export (Java, GetTargetEncodings, "getTargetEncodings");
   pragma Export (Java, IsSourceEncodingSupported, "isSourceEncodingSupported");
   pragma Export (Java, IsTargetEncodingSupported, "isTargetEncodingSupported");
   pragma Export (Java, IsConversionSupported, "isConversionSupported");
   pragma Export (Java, GetTargetFormats, "getTargetFormats");
   pragma Export (Java, GetAudioInputStream, "getAudioInputStream");

end Javax.Sound.Sampled.Spi.FormatConversionProvider;
pragma Import (Java, Javax.Sound.Sampled.Spi.FormatConversionProvider, "javax.sound.sampled.spi.FormatConversionProvider");
pragma Extensions_Allowed (Off);
