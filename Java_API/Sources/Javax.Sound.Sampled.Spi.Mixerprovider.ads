pragma Extensions_Allowed (On);
limited with Javax.Sound.Sampled.Mixer;
limited with Javax.Sound.Sampled.Mixer.Info;
with Java.Lang.Object;

package Javax.Sound.Sampled.Spi.MixerProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MixerProvider (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsMixerSupported (This : access Typ;
                              P1_Info : access Standard.Javax.Sound.Sampled.Mixer.Info.Typ'Class)
                              return Java.Boolean;

   function GetMixerInfo (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   function GetMixer (This : access Typ;
                      P1_Info : access Standard.Javax.Sound.Sampled.Mixer.Info.Typ'Class)
                      return access Javax.Sound.Sampled.Mixer.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MixerProvider);
   pragma Export (Java, IsMixerSupported, "isMixerSupported");
   pragma Export (Java, GetMixerInfo, "getMixerInfo");
   pragma Export (Java, GetMixer, "getMixer");

end Javax.Sound.Sampled.Spi.MixerProvider;
pragma Import (Java, Javax.Sound.Sampled.Spi.MixerProvider, "javax.sound.sampled.spi.MixerProvider");
pragma Extensions_Allowed (Off);
