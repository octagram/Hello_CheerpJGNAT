pragma Extensions_Allowed (On);
package Javax.Sound.Sampled.Spi is
   pragma Preelaborate;
end Javax.Sound.Sampled.Spi;
pragma Import (Java, Javax.Sound.Sampled.Spi, "javax.sound.sampled.spi");
pragma Extensions_Allowed (Off);
