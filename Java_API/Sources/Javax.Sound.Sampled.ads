pragma Extensions_Allowed (On);
package Javax.Sound.Sampled is
   pragma Preelaborate;
end Javax.Sound.Sampled;
pragma Import (Java, Javax.Sound.Sampled, "javax.sound.sampled");
pragma Extensions_Allowed (Off);
