pragma Extensions_Allowed (On);
package Javax.Sound is
   pragma Preelaborate;
end Javax.Sound;
pragma Import (Java, Javax.Sound, "javax.sound");
pragma Extensions_Allowed (Off);
