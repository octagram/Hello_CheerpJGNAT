pragma Extensions_Allowed (On);
limited with Java.Io.PrintWriter;
with Java.Lang.Object;

package Javax.Sql.CommonDataSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLogWriter (This : access Typ)
                          return access Java.Io.PrintWriter.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetLogWriter (This : access Typ;
                           P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetLoginTimeout (This : access Typ;
                              P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetLoginTimeout (This : access Typ)
                             return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLogWriter, "getLogWriter");
   pragma Export (Java, SetLogWriter, "setLogWriter");
   pragma Export (Java, SetLoginTimeout, "setLoginTimeout");
   pragma Export (Java, GetLoginTimeout, "getLoginTimeout");

end Javax.Sql.CommonDataSource;
pragma Import (Java, Javax.Sql.CommonDataSource, "javax.sql.CommonDataSource");
pragma Extensions_Allowed (Off);
