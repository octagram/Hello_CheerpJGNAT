pragma Extensions_Allowed (On);
limited with Java.Sql.SQLException;
limited with Javax.Sql.PooledConnection;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Sql.ConnectionEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ConnectionEvent (P1_PooledConnection : access Standard.Javax.Sql.PooledConnection.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_ConnectionEvent (P1_PooledConnection : access Standard.Javax.Sql.PooledConnection.Typ'Class;
                                 P2_SQLException : access Standard.Java.Sql.SQLException.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSQLException (This : access Typ)
                             return access Java.Sql.SQLException.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ConnectionEvent);
   pragma Import (Java, GetSQLException, "getSQLException");

end Javax.Sql.ConnectionEvent;
pragma Import (Java, Javax.Sql.ConnectionEvent, "javax.sql.ConnectionEvent");
pragma Extensions_Allowed (Off);
