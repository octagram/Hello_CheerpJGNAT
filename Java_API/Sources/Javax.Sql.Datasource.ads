pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Sql.Connection;
with Java.Lang.Object;
with Java.Sql.Wrapper;
with Javax.Sql.CommonDataSource;

package Javax.Sql.DataSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Wrapper_I : Java.Sql.Wrapper.Ref;
            CommonDataSource_I : Javax.Sql.CommonDataSource.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetConnection (This : access Typ)
                           return access Java.Sql.Connection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetConnection (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Sql.Connection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetConnection, "getConnection");

end Javax.Sql.DataSource;
pragma Import (Java, Javax.Sql.DataSource, "javax.sql.DataSource");
pragma Extensions_Allowed (Off);
