pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
limited with Java.Util.Calendar;
limited with Java.Util.Map;
limited with Javax.Sql.RowSetListener;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Sql.Rowset.BaseRowSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      BinaryStream : access Java.Io.InputStream.Typ'Class;
      pragma Import (Java, BinaryStream, "binaryStream");

      --  protected
      UnicodeStream : access Java.Io.InputStream.Typ'Class;
      pragma Import (Java, UnicodeStream, "unicodeStream");

      --  protected
      AsciiStream : access Java.Io.InputStream.Typ'Class;
      pragma Import (Java, AsciiStream, "asciiStream");

      --  protected
      CharStream : access Java.Io.Reader.Typ'Class;
      pragma Import (Java, CharStream, "charStream");

   end record;

   function New_BaseRowSet (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure InitParams (This : access Typ);

   procedure AddRowSetListener (This : access Typ;
                                P1_RowSetListener : access Standard.Javax.Sql.RowSetListener.Typ'Class);

   procedure RemoveRowSetListener (This : access Typ;
                                   P1_RowSetListener : access Standard.Javax.Sql.RowSetListener.Typ'Class);

   --  protected
   procedure NotifyCursorMoved (This : access Typ);
   --  can raise Java.Sql.SQLException.Except

   --  protected
   procedure NotifyRowChanged (This : access Typ);
   --  can raise Java.Sql.SQLException.Except

   --  protected
   procedure NotifyRowSetChanged (This : access Typ);
   --  can raise Java.Sql.SQLException.Except

   function GetCommand (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetCommand (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   function GetUrl (This : access Typ)
                    return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   procedure SetUrl (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     with Import => "setUrl", Convention => Java;
   --  can raise Java.Sql.SQLException.Except

   function GetDataSourceName (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure SetDataSourceName (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   function GetUsername (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetUsername (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPassword (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetPassword (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetType (This : access Typ;
                      P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   function GetType (This : access Typ)
                     return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   procedure SetConcurrency (This : access Typ;
                             P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   function IsReadOnly (This : access Typ)
                        return Java.Boolean;

   procedure SetReadOnly (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function GetTransactionIsolation (This : access Typ)
                                     return Java.Int;

   procedure SetTransactionIsolation (This : access Typ;
                                      P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   function GetTypeMap (This : access Typ)
                        return access Java.Util.Map.Typ'Class;

   procedure SetTypeMap (This : access Typ;
                         P1_Map : access Standard.Java.Util.Map.Typ'Class);

   function GetMaxFieldSize (This : access Typ)
                             return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMaxFieldSize (This : access Typ;
                              P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   function GetMaxRows (This : access Typ)
                        return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMaxRows (This : access Typ;
                         P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetEscapeProcessing (This : access Typ;
                                  P1_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   function GetQueryTimeout (This : access Typ)
                             return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   procedure SetQueryTimeout (This : access Typ;
                              P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   function GetShowDeleted (This : access Typ)
                            return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   procedure SetShowDeleted (This : access Typ;
                             P1_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   function GetEscapeProcessing (This : access Typ)
                                 return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFetchDirection (This : access Typ;
                                P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   function GetFetchDirection (This : access Typ)
                               return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFetchSize (This : access Typ;
                           P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   function GetFetchSize (This : access Typ)
                          return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function GetConcurrency (This : access Typ)
                            return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBoolean (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure SetByte (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Byte : Java.Byte);
   --  can raise Java.Sql.SQLException.Except

   procedure SetShort (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Short : Java.Short);
   --  can raise Java.Sql.SQLException.Except

   procedure SetInt (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetLong (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetFloat (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Float : Java.Float);
   --  can raise Java.Sql.SQLException.Except

   procedure SetDouble (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Double : Java.Double);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBigDecimal (This : access Typ;
                            P1_Int : Java.Int;
                            P2_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetString (This : access Typ;
                        P1_Int : Java.Int;
                        P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBytes (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_Int : Java.Int;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                             P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_Int : Java.Int;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_Int : Java.Int;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_Int : Java.Int;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                 P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetRef (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Ref : access Standard.Java.Sql.Ref.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Blob : access Standard.Java.Sql.Blob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Clob : access Standard.Java.Sql.Clob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetArray (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Array_K : access Standard.Java.Sql.Array_K.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class;
                           P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure ClearParameters (This : access Typ);
   --  can raise Java.Sql.SQLException.Except

   function GetParams (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int;
                      P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBoolean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure SetByte (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Byte : Java.Byte);
   --  can raise Java.Sql.SQLException.Except

   procedure SetShort (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Short : Java.Short);
   --  can raise Java.Sql.SQLException.Except

   procedure SetInt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetLong (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetFloat (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Float : Java.Float);
   --  can raise Java.Sql.SQLException.Except

   procedure SetDouble (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Double : Java.Double);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBigDecimal (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetString (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBytes (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                             P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                 P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                      P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                      P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Blob : access Standard.Java.Sql.Blob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                      P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                      P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Clob : access Standard.Java.Sql.Clob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class;
                           P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetSQLXML (This : access Typ;
                        P1_Int : Java.Int;
                        P2_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetSQLXML (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetRowId (This : access Typ;
                       P1_Int : Java.Int;
                       P2_RowId : access Standard.Java.Sql.RowId.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetRowId (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_RowId : access Standard.Java.Sql.RowId.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNString (This : access Typ;
                         P1_Int : Java.Int;
                         P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                  P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                  P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_NClob : access Standard.Java.Sql.NClob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                       P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                       P3_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_Int : Java.Int;
                       P2_NClob : access Standard.Java.Sql.NClob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetURL (This : access Typ;
                     P1_Int : Java.Int;
                     P2_URL : access Standard.Java.Net.URL.Typ'Class)
                     with Import => "setURL", Convention => Java;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNICODE_STREAM_PARAM : constant Java.Int;

   --  final
   BINARY_STREAM_PARAM : constant Java.Int;

   --  final
   ASCII_STREAM_PARAM : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BaseRowSet);
   pragma Import (Java, InitParams, "initParams");
   pragma Import (Java, AddRowSetListener, "addRowSetListener");
   pragma Import (Java, RemoveRowSetListener, "removeRowSetListener");
   pragma Import (Java, NotifyCursorMoved, "notifyCursorMoved");
   pragma Import (Java, NotifyRowChanged, "notifyRowChanged");
   pragma Import (Java, NotifyRowSetChanged, "notifyRowSetChanged");
   pragma Import (Java, GetCommand, "getCommand");
   pragma Import (Java, SetCommand, "setCommand");
   pragma Import (Java, GetUrl, "getUrl");
   -- pragma Import (Java, SetUrl, "setUrl");
   pragma Import (Java, GetDataSourceName, "getDataSourceName");
   pragma Import (Java, SetDataSourceName, "setDataSourceName");
   pragma Import (Java, GetUsername, "getUsername");
   pragma Import (Java, SetUsername, "setUsername");
   pragma Import (Java, GetPassword, "getPassword");
   pragma Import (Java, SetPassword, "setPassword");
   pragma Import (Java, SetType, "setType");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, SetConcurrency, "setConcurrency");
   pragma Import (Java, IsReadOnly, "isReadOnly");
   pragma Import (Java, SetReadOnly, "setReadOnly");
   pragma Import (Java, GetTransactionIsolation, "getTransactionIsolation");
   pragma Import (Java, SetTransactionIsolation, "setTransactionIsolation");
   pragma Import (Java, GetTypeMap, "getTypeMap");
   pragma Import (Java, SetTypeMap, "setTypeMap");
   pragma Import (Java, GetMaxFieldSize, "getMaxFieldSize");
   pragma Import (Java, SetMaxFieldSize, "setMaxFieldSize");
   pragma Import (Java, GetMaxRows, "getMaxRows");
   pragma Import (Java, SetMaxRows, "setMaxRows");
   pragma Import (Java, SetEscapeProcessing, "setEscapeProcessing");
   pragma Import (Java, GetQueryTimeout, "getQueryTimeout");
   pragma Import (Java, SetQueryTimeout, "setQueryTimeout");
   pragma Import (Java, GetShowDeleted, "getShowDeleted");
   pragma Import (Java, SetShowDeleted, "setShowDeleted");
   pragma Import (Java, GetEscapeProcessing, "getEscapeProcessing");
   pragma Import (Java, SetFetchDirection, "setFetchDirection");
   pragma Import (Java, GetFetchDirection, "getFetchDirection");
   pragma Import (Java, SetFetchSize, "setFetchSize");
   pragma Import (Java, GetFetchSize, "getFetchSize");
   pragma Import (Java, GetConcurrency, "getConcurrency");
   pragma Import (Java, SetNull, "setNull");
   pragma Import (Java, SetBoolean, "setBoolean");
   pragma Import (Java, SetByte, "setByte");
   pragma Import (Java, SetShort, "setShort");
   pragma Import (Java, SetInt, "setInt");
   pragma Import (Java, SetLong, "setLong");
   pragma Import (Java, SetFloat, "setFloat");
   pragma Import (Java, SetDouble, "setDouble");
   pragma Import (Java, SetBigDecimal, "setBigDecimal");
   pragma Import (Java, SetString, "setString");
   pragma Import (Java, SetBytes, "setBytes");
   pragma Import (Java, SetDate, "setDate");
   pragma Import (Java, SetTime, "setTime");
   pragma Import (Java, SetTimestamp, "setTimestamp");
   pragma Import (Java, SetAsciiStream, "setAsciiStream");
   pragma Import (Java, SetBinaryStream, "setBinaryStream");
   pragma Import (Java, SetCharacterStream, "setCharacterStream");
   pragma Import (Java, SetObject, "setObject");
   pragma Import (Java, SetRef, "setRef");
   pragma Import (Java, SetBlob, "setBlob");
   pragma Import (Java, SetClob, "setClob");
   pragma Import (Java, SetArray, "setArray");
   pragma Import (Java, ClearParameters, "clearParameters");
   pragma Import (Java, GetParams, "getParams");
   pragma Import (Java, SetNCharacterStream, "setNCharacterStream");
   pragma Import (Java, SetSQLXML, "setSQLXML");
   pragma Import (Java, SetRowId, "setRowId");
   pragma Import (Java, SetNString, "setNString");
   pragma Import (Java, SetNClob, "setNClob");
   -- pragma Import (Java, SetURL, "setURL");
   pragma Import (Java, UNICODE_STREAM_PARAM, "UNICODE_STREAM_PARAM");
   pragma Import (Java, BINARY_STREAM_PARAM, "BINARY_STREAM_PARAM");
   pragma Import (Java, ASCII_STREAM_PARAM, "ASCII_STREAM_PARAM");

end Javax.Sql.Rowset.BaseRowSet;
pragma Import (Java, Javax.Sql.Rowset.BaseRowSet, "javax.sql.rowset.BaseRowSet");
pragma Extensions_Allowed (Off);
