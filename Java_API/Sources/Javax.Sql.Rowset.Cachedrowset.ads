pragma Extensions_Allowed (On);
with Java.Lang.String;
limited with Java.Sql.Connection;
with Java.Sql.ResultSet;
limited with Java.Sql.Savepoint;
limited with Java.Util.Collection;
limited with Javax.Sql.RowSetEvent;
limited with Javax.Sql.RowSetMetaData;
limited with Javax.Sql.Rowset.RowSetWarning;
limited with Javax.Sql.Rowset.Spi.SyncProvider;
with Java.Lang.Object;
with Javax.Sql.RowSet;
with Javax.Sql.Rowset.Joinable;

package Javax.Sql.Rowset.CachedRowSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RowSet_I : Javax.Sql.RowSet.Ref;
            Joinable_I : Javax.Sql.Rowset.Joinable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Populate (This : access Typ;
                       P1_ResultSet : access Standard.Java.Sql.ResultSet.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Execute (This : access Typ;
                      P1_Connection : access Standard.Java.Sql.Connection.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AcceptChanges (This : access Typ) is abstract;
   --  can raise Javax.Sql.Rowset.Spi.SyncProviderException.Except

   procedure AcceptChanges (This : access Typ;
                            P1_Connection : access Standard.Java.Sql.Connection.Typ'Class) is abstract;
   --  can raise Javax.Sql.Rowset.Spi.SyncProviderException.Except

   procedure RestoreOriginal (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Release (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UndoDelete (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UndoInsert (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UndoUpdate (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ColumnUpdated (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ColumnUpdated (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ToCollection (This : access Typ)
                          return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ToCollection (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ToCollection (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetSyncProvider (This : access Typ)
                             return access Javax.Sql.Rowset.Spi.SyncProvider.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetSyncProvider (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Size (This : access Typ)
                  return Java.Int is abstract;

   procedure SetMetaData (This : access Typ;
                          P1_RowSetMetaData : access Standard.Javax.Sql.RowSetMetaData.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetOriginal (This : access Typ)
                         return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetOriginalRow (This : access Typ)
                            return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetOriginalRow (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTableName (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTableName (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetKeyColumns (This : access Typ)
                           return Java.Int_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetKeyColumns (This : access Typ;
                            P1_Int_Arr : Java.Int_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateShared (This : access Typ)
                          return access Javax.Sql.RowSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateCopy (This : access Typ)
                        return access Javax.Sql.Rowset.CachedRowSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateCopySchema (This : access Typ)
                              return access Javax.Sql.Rowset.CachedRowSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function CreateCopyNoConstraints (This : access Typ)
                                     return access Javax.Sql.Rowset.CachedRowSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowSetWarnings (This : access Typ)
                               return access Javax.Sql.Rowset.RowSetWarning.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetShowDeleted (This : access Typ)
                            return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetShowDeleted (This : access Typ;
                             P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Commit (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ;
                       P1_Savepoint : access Standard.Java.Sql.Savepoint.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure RowSetPopulated (This : access Typ;
                              P1_RowSetEvent : access Standard.Javax.Sql.RowSetEvent.Typ'Class;
                              P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Populate (This : access Typ;
                       P1_ResultSet : access Standard.Java.Sql.ResultSet.Typ'Class;
                       P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetPageSize (This : access Typ;
                          P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetPageSize (This : access Typ)
                         return Java.Int is abstract;

   function NextPage (This : access Typ)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PreviousPage (This : access Typ)
                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   COMMIT_ON_ACCEPT_CHANGES : constant Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Populate, "populate");
   pragma Export (Java, Execute, "execute");
   pragma Export (Java, AcceptChanges, "acceptChanges");
   pragma Export (Java, RestoreOriginal, "restoreOriginal");
   pragma Export (Java, Release, "release");
   pragma Export (Java, UndoDelete, "undoDelete");
   pragma Export (Java, UndoInsert, "undoInsert");
   pragma Export (Java, UndoUpdate, "undoUpdate");
   pragma Export (Java, ColumnUpdated, "columnUpdated");
   pragma Export (Java, ToCollection, "toCollection");
   pragma Export (Java, GetSyncProvider, "getSyncProvider");
   pragma Export (Java, SetSyncProvider, "setSyncProvider");
   pragma Export (Java, Size, "size");
   pragma Export (Java, SetMetaData, "setMetaData");
   pragma Export (Java, GetOriginal, "getOriginal");
   pragma Export (Java, GetOriginalRow, "getOriginalRow");
   pragma Export (Java, SetOriginalRow, "setOriginalRow");
   pragma Export (Java, GetTableName, "getTableName");
   pragma Export (Java, SetTableName, "setTableName");
   pragma Export (Java, GetKeyColumns, "getKeyColumns");
   pragma Export (Java, SetKeyColumns, "setKeyColumns");
   pragma Export (Java, CreateShared, "createShared");
   pragma Export (Java, CreateCopy, "createCopy");
   pragma Export (Java, CreateCopySchema, "createCopySchema");
   pragma Export (Java, CreateCopyNoConstraints, "createCopyNoConstraints");
   pragma Export (Java, GetRowSetWarnings, "getRowSetWarnings");
   pragma Export (Java, GetShowDeleted, "getShowDeleted");
   pragma Export (Java, SetShowDeleted, "setShowDeleted");
   pragma Export (Java, Commit, "commit");
   pragma Export (Java, Rollback, "rollback");
   pragma Export (Java, RowSetPopulated, "rowSetPopulated");
   pragma Export (Java, SetPageSize, "setPageSize");
   pragma Export (Java, GetPageSize, "getPageSize");
   pragma Export (Java, NextPage, "nextPage");
   pragma Export (Java, PreviousPage, "previousPage");
   pragma Import (Java, COMMIT_ON_ACCEPT_CHANGES, "COMMIT_ON_ACCEPT_CHANGES");

end Javax.Sql.Rowset.CachedRowSet;
pragma Import (Java, Javax.Sql.Rowset.CachedRowSet, "javax.sql.rowset.CachedRowSet");
pragma Extensions_Allowed (Off);
