pragma Extensions_Allowed (On);
limited with Javax.Sql.Rowset.Predicate;
with Java.Lang.Object;
with Javax.Sql.Rowset.WebRowSet;

package Javax.Sql.Rowset.FilteredRowSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            WebRowSet_I : Javax.Sql.Rowset.WebRowSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFilter (This : access Typ;
                        P1_Predicate : access Standard.Javax.Sql.Rowset.Predicate.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetFilter (This : access Typ)
                       return access Javax.Sql.Rowset.Predicate.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetFilter, "setFilter");
   pragma Export (Java, GetFilter, "getFilter");

end Javax.Sql.Rowset.FilteredRowSet;
pragma Import (Java, Javax.Sql.Rowset.FilteredRowSet, "javax.sql.rowset.FilteredRowSet");
pragma Extensions_Allowed (Off);
