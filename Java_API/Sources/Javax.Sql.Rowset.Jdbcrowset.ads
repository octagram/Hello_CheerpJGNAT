pragma Extensions_Allowed (On);
limited with Java.Sql.Savepoint;
limited with Javax.Sql.Rowset.RowSetWarning;
with Java.Lang.Object;
with Javax.Sql.RowSet;
with Javax.Sql.Rowset.Joinable;

package Javax.Sql.Rowset.JdbcRowSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RowSet_I : Javax.Sql.RowSet.Ref;
            Joinable_I : Javax.Sql.Rowset.Joinable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetShowDeleted (This : access Typ)
                            return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetShowDeleted (This : access Typ;
                             P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowSetWarnings (This : access Typ)
                               return access Javax.Sql.Rowset.RowSetWarning.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Commit (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetAutoCommit (This : access Typ)
                           return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAutoCommit (This : access Typ;
                            P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ;
                       P1_Savepoint : access Standard.Java.Sql.Savepoint.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetShowDeleted, "getShowDeleted");
   pragma Export (Java, SetShowDeleted, "setShowDeleted");
   pragma Export (Java, GetRowSetWarnings, "getRowSetWarnings");
   pragma Export (Java, Commit, "commit");
   pragma Export (Java, GetAutoCommit, "getAutoCommit");
   pragma Export (Java, SetAutoCommit, "setAutoCommit");
   pragma Export (Java, Rollback, "rollback");

end Javax.Sql.Rowset.JdbcRowSet;
pragma Import (Java, Javax.Sql.Rowset.JdbcRowSet, "javax.sql.rowset.JdbcRowSet");
pragma Extensions_Allowed (Off);
