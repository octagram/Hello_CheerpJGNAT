pragma Extensions_Allowed (On);
with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sql.Rowset.Joinable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMatchColumn (This : access Typ;
                             P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMatchColumn (This : access Typ;
                             P1_Int_Arr : Java.Int_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMatchColumn (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMatchColumn (This : access Typ;
                             P1_String_Arr : access Java.Lang.String.Arr_Obj) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMatchColumnIndexes (This : access Typ)
                                   return Java.Int_Arr is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMatchColumnNames (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UnsetMatchColumn (This : access Typ;
                               P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UnsetMatchColumn (This : access Typ;
                               P1_Int_Arr : Java.Int_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UnsetMatchColumn (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure UnsetMatchColumn (This : access Typ;
                               P1_String_Arr : access Java.Lang.String.Arr_Obj) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetMatchColumn, "setMatchColumn");
   pragma Export (Java, GetMatchColumnIndexes, "getMatchColumnIndexes");
   pragma Export (Java, GetMatchColumnNames, "getMatchColumnNames");
   pragma Export (Java, UnsetMatchColumn, "unsetMatchColumn");

end Javax.Sql.Rowset.Joinable;
pragma Import (Java, Javax.Sql.Rowset.Joinable, "javax.sql.rowset.Joinable");
pragma Extensions_Allowed (Off);
