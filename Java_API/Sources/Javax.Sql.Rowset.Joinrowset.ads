pragma Extensions_Allowed (On);
with Java.Lang.String;
limited with Java.Util.Collection;
limited with Javax.Sql.Rowset.CachedRowSet;
limited with Javax.Sql.Rowset.Joinable;
with Java.Lang.Object;
with Javax.Sql.Rowset.WebRowSet;

package Javax.Sql.Rowset.JoinRowSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            WebRowSet_I : Javax.Sql.Rowset.WebRowSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddRowSet (This : access Typ;
                        P1_Joinable : access Standard.Javax.Sql.Rowset.Joinable.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AddRowSet (This : access Typ;
                        P1_RowSet : access Standard.Javax.Sql.RowSet.Typ'Class;
                        P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AddRowSet (This : access Typ;
                        P1_RowSet : access Standard.Javax.Sql.RowSet.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AddRowSet (This : access Typ;
                        P1_RowSet_Arr : access Javax.Sql.RowSet.Arr_Obj;
                        P2_Int_Arr : Java.Int_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AddRowSet (This : access Typ;
                        P1_RowSet_Arr : access Javax.Sql.RowSet.Arr_Obj;
                        P2_String_Arr : access Java.Lang.String.Arr_Obj) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowSets (This : access Typ)
                        return access Java.Util.Collection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetRowSetNames (This : access Typ)
                            return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Sql.SQLException.Except

   function ToCachedRowSet (This : access Typ)
                            return access Javax.Sql.Rowset.CachedRowSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function SupportsCrossJoin (This : access Typ)
                               return Java.Boolean is abstract;

   function SupportsInnerJoin (This : access Typ)
                               return Java.Boolean is abstract;

   function SupportsLeftOuterJoin (This : access Typ)
                                   return Java.Boolean is abstract;

   function SupportsRightOuterJoin (This : access Typ)
                                    return Java.Boolean is abstract;

   function SupportsFullJoin (This : access Typ)
                              return Java.Boolean is abstract;

   procedure SetJoinType (This : access Typ;
                          P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetWhereClause (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetJoinType (This : access Typ)
                         return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CROSS_JOIN : constant Java.Int;

   --  final
   INNER_JOIN : constant Java.Int;

   --  final
   LEFT_OUTER_JOIN : constant Java.Int;

   --  final
   RIGHT_OUTER_JOIN : constant Java.Int;

   --  final
   FULL_JOIN : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddRowSet, "addRowSet");
   pragma Export (Java, GetRowSets, "getRowSets");
   pragma Export (Java, GetRowSetNames, "getRowSetNames");
   pragma Export (Java, ToCachedRowSet, "toCachedRowSet");
   pragma Export (Java, SupportsCrossJoin, "supportsCrossJoin");
   pragma Export (Java, SupportsInnerJoin, "supportsInnerJoin");
   pragma Export (Java, SupportsLeftOuterJoin, "supportsLeftOuterJoin");
   pragma Export (Java, SupportsRightOuterJoin, "supportsRightOuterJoin");
   pragma Export (Java, SupportsFullJoin, "supportsFullJoin");
   pragma Export (Java, SetJoinType, "setJoinType");
   pragma Export (Java, GetWhereClause, "getWhereClause");
   pragma Export (Java, GetJoinType, "getJoinType");
   pragma Import (Java, CROSS_JOIN, "CROSS_JOIN");
   pragma Import (Java, INNER_JOIN, "INNER_JOIN");
   pragma Import (Java, LEFT_OUTER_JOIN, "LEFT_OUTER_JOIN");
   pragma Import (Java, RIGHT_OUTER_JOIN, "RIGHT_OUTER_JOIN");
   pragma Import (Java, FULL_JOIN, "FULL_JOIN");

end Javax.Sql.Rowset.JoinRowSet;
pragma Import (Java, Javax.Sql.Rowset.JoinRowSet, "javax.sql.rowset.JoinRowSet");
pragma Extensions_Allowed (Off);
