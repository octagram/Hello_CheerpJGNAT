pragma Extensions_Allowed (On);
with Java.Lang.String;
with Java.Lang.Object;

package Javax.Sql.Rowset.Predicate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Evaluate (This : access Typ;
                      P1_RowSet : access Standard.Javax.Sql.RowSet.Typ'Class)
                      return Java.Boolean is abstract;

   function Evaluate (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function Evaluate (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Evaluate, "evaluate");

end Javax.Sql.Rowset.Predicate;
pragma Import (Java, Javax.Sql.Rowset.Predicate, "javax.sql.rowset.Predicate");
pragma Extensions_Allowed (Off);
