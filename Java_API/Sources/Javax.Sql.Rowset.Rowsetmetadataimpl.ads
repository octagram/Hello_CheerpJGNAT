pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Sql.RowSetMetaData;

package Javax.Sql.Rowset.RowSetMetaDataImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            RowSetMetaData_I : Javax.Sql.RowSetMetaData.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RowSetMetaDataImpl (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetColumnCount (This : access Typ;
                             P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetAutoIncrement (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure SetCaseSensitive (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure SetSearchable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure SetCurrency (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure SetNullable (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetSigned (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnDisplaySize (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnLabel (This : access Typ;
                             P1_Int : Java.Int;
                             P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnName (This : access Typ;
                            P1_Int : Java.Int;
                            P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetSchemaName (This : access Typ;
                            P1_Int : Java.Int;
                            P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetPrecision (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetScale (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetTableName (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetCatalogName (This : access Typ;
                             P1_Int : Java.Int;
                             P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnType (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnTypeName (This : access Typ;
                                P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   function GetColumnCount (This : access Typ)
                            return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function IsAutoIncrement (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function IsCaseSensitive (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function IsSearchable (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function IsCurrency (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function IsNullable (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function IsSigned (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnDisplaySize (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnLabel (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetSchemaName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetPrecision (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function GetScale (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function GetTableName (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetCatalogName (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnType (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnTypeName (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function IsReadOnly (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function IsWritable (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function IsDefinitelyWritable (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function GetColumnClassName (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function Unwrap (This : access Typ;
                    P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function IsWrapperFor (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RowSetMetaDataImpl);
   pragma Import (Java, SetColumnCount, "setColumnCount");
   pragma Import (Java, SetAutoIncrement, "setAutoIncrement");
   pragma Import (Java, SetCaseSensitive, "setCaseSensitive");
   pragma Import (Java, SetSearchable, "setSearchable");
   pragma Import (Java, SetCurrency, "setCurrency");
   pragma Import (Java, SetNullable, "setNullable");
   pragma Import (Java, SetSigned, "setSigned");
   pragma Import (Java, SetColumnDisplaySize, "setColumnDisplaySize");
   pragma Import (Java, SetColumnLabel, "setColumnLabel");
   pragma Import (Java, SetColumnName, "setColumnName");
   pragma Import (Java, SetSchemaName, "setSchemaName");
   pragma Import (Java, SetPrecision, "setPrecision");
   pragma Import (Java, SetScale, "setScale");
   pragma Import (Java, SetTableName, "setTableName");
   pragma Import (Java, SetCatalogName, "setCatalogName");
   pragma Import (Java, SetColumnType, "setColumnType");
   pragma Import (Java, SetColumnTypeName, "setColumnTypeName");
   pragma Import (Java, GetColumnCount, "getColumnCount");
   pragma Import (Java, IsAutoIncrement, "isAutoIncrement");
   pragma Import (Java, IsCaseSensitive, "isCaseSensitive");
   pragma Import (Java, IsSearchable, "isSearchable");
   pragma Import (Java, IsCurrency, "isCurrency");
   pragma Import (Java, IsNullable, "isNullable");
   pragma Import (Java, IsSigned, "isSigned");
   pragma Import (Java, GetColumnDisplaySize, "getColumnDisplaySize");
   pragma Import (Java, GetColumnLabel, "getColumnLabel");
   pragma Import (Java, GetColumnName, "getColumnName");
   pragma Import (Java, GetSchemaName, "getSchemaName");
   pragma Import (Java, GetPrecision, "getPrecision");
   pragma Import (Java, GetScale, "getScale");
   pragma Import (Java, GetTableName, "getTableName");
   pragma Import (Java, GetCatalogName, "getCatalogName");
   pragma Import (Java, GetColumnType, "getColumnType");
   pragma Import (Java, GetColumnTypeName, "getColumnTypeName");
   pragma Import (Java, IsReadOnly, "isReadOnly");
   pragma Import (Java, IsWritable, "isWritable");
   pragma Import (Java, IsDefinitelyWritable, "isDefinitelyWritable");
   pragma Import (Java, GetColumnClassName, "getColumnClassName");
   pragma Import (Java, Unwrap, "unwrap");
   pragma Import (Java, IsWrapperFor, "isWrapperFor");

end Javax.Sql.Rowset.RowSetMetaDataImpl;
pragma Import (Java, Javax.Sql.Rowset.RowSetMetaDataImpl, "javax.sql.rowset.RowSetMetaDataImpl");
pragma Extensions_Allowed (Off);
