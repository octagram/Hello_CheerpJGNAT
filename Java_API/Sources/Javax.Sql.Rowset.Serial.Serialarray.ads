pragma Extensions_Allowed (On);
with Java.Lang.String;
with Java.Sql.ResultSet;
limited with Java.Util.Map;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Sql.Array_K;

package Javax.Sql.Rowset.Serial.SerialArray is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Array_K_I : Java.Sql.Array_K.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SerialArray (P1_Array_K : access Standard.Java.Sql.Array_K.Typ'Class;
                             P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Free (This : access Typ);
   --  can raise Java.Sql.SQLException.Except

   function New_SerialArray (P1_Array_K : access Standard.Java.Sql.Array_K.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function GetArray (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetArray (This : access Typ;
                      P1_Map : access Standard.Java.Util.Map.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetArray (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetArray (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Map : access Standard.Java.Util.Map.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetBaseType (This : access Typ)
                         return Java.Int;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetBaseTypeName (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetResultSet (This : access Typ;
                          P1_Long : Java.Long;
                          P2_Int : Java.Int)
                          return access Java.Sql.ResultSet.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetResultSet (This : access Typ;
                          P1_Map : access Standard.Java.Util.Map.Typ'Class)
                          return access Java.Sql.ResultSet.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetResultSet (This : access Typ)
                          return access Java.Sql.ResultSet.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetResultSet (This : access Typ;
                          P1_Long : Java.Long;
                          P2_Int : Java.Int;
                          P3_Map : access Standard.Java.Util.Map.Typ'Class)
                          return access Java.Sql.ResultSet.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SerialArray);
   pragma Import (Java, Free, "free");
   pragma Import (Java, GetArray, "getArray");
   pragma Import (Java, GetBaseType, "getBaseType");
   pragma Import (Java, GetBaseTypeName, "getBaseTypeName");
   pragma Import (Java, GetResultSet, "getResultSet");

end Javax.Sql.Rowset.Serial.SerialArray;
pragma Import (Java, Javax.Sql.Rowset.Serial.SerialArray, "javax.sql.rowset.serial.SerialArray");
pragma Extensions_Allowed (Off);
