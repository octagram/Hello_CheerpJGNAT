pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Sql.Blob;

package Javax.Sql.Rowset.Serial.SerialBlob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Blob_I : Java.Sql.Blob.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SerialBlob (P1_Byte_Arr : Java.Byte_Arr; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function New_SerialBlob (P1_Blob : access Standard.Java.Sql.Blob.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBytes (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int)
                      return Java.Byte_Arr;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function Length (This : access Typ)
                    return Java.Long;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetBinaryStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function Position (This : access Typ;
                      P1_Byte_Arr : Java.Byte_Arr;
                      P2_Long : Java.Long)
                      return Java.Long;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function Position (This : access Typ;
                      P1_Blob : access Standard.Java.Sql.Blob.Typ'Class;
                      P2_Long : Java.Long)
                      return Java.Long;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function SetBytes (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Byte_Arr : Java.Byte_Arr)
                      return Java.Int;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function SetBytes (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Byte_Arr : Java.Byte_Arr;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int)
                      return Java.Int;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function SetBinaryStream (This : access Typ;
                             P1_Long : Java.Long)
                             return access Java.Io.OutputStream.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   procedure Truncate (This : access Typ;
                       P1_Long : Java.Long);
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetBinaryStream (This : access Typ;
                             P1_Long : Java.Long;
                             P2_Long : Java.Long)
                             return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   procedure Free (This : access Typ);
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SerialBlob);
   pragma Import (Java, GetBytes, "getBytes");
   pragma Import (Java, Length, "length");
   pragma Import (Java, GetBinaryStream, "getBinaryStream");
   pragma Import (Java, Position, "position");
   pragma Import (Java, SetBytes, "setBytes");
   pragma Import (Java, SetBinaryStream, "setBinaryStream");
   pragma Import (Java, Truncate, "truncate");
   pragma Import (Java, Free, "free");

end Javax.Sql.Rowset.Serial.SerialBlob;
pragma Import (Java, Javax.Sql.Rowset.Serial.SerialBlob, "javax.sql.rowset.serial.SerialBlob");
pragma Extensions_Allowed (Off);
