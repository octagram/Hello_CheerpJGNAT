pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Sql.Clob;

package Javax.Sql.Rowset.Serial.SerialClob is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Clob_I : Java.Sql.Clob.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SerialClob (P1_Char_Arr : Java.Char_Arr; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function New_SerialClob (P1_Clob : access Standard.Java.Sql.Clob.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Length (This : access Typ)
                    return Java.Long;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetCharacterStream (This : access Typ)
                                return access Java.Io.Reader.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetAsciiStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function GetSubString (This : access Typ;
                          P1_Long : Java.Long;
                          P2_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function Position (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Long : Java.Long)
                      return Java.Long;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function Position (This : access Typ;
                      P1_Clob : access Standard.Java.Sql.Clob.Typ'Class;
                      P2_Long : Java.Long)
                      return Java.Long;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function SetString (This : access Typ;
                       P1_Long : Java.Long;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Int;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function SetString (This : access Typ;
                       P1_Long : Java.Long;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int)
                       return Java.Int;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function SetAsciiStream (This : access Typ;
                            P1_Long : Java.Long)
                            return access Java.Io.OutputStream.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   function SetCharacterStream (This : access Typ;
                                P1_Long : Java.Long)
                                return access Java.Io.Writer.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   procedure Truncate (This : access Typ;
                       P1_Long : Java.Long);
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetCharacterStream (This : access Typ;
                                P1_Long : Java.Long;
                                P2_Long : Java.Long)
                                return access Java.Io.Reader.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   procedure Free (This : access Typ);
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SerialClob);
   pragma Import (Java, Length, "length");
   pragma Import (Java, GetCharacterStream, "getCharacterStream");
   pragma Import (Java, GetAsciiStream, "getAsciiStream");
   pragma Import (Java, GetSubString, "getSubString");
   pragma Import (Java, Position, "position");
   pragma Import (Java, SetString, "setString");
   pragma Import (Java, SetAsciiStream, "setAsciiStream");
   pragma Import (Java, SetCharacterStream, "setCharacterStream");
   pragma Import (Java, Truncate, "truncate");
   pragma Import (Java, Free, "free");

end Javax.Sql.Rowset.Serial.SerialClob;
pragma Import (Java, Javax.Sql.Rowset.Serial.SerialClob, "javax.sql.rowset.serial.SerialClob");
pragma Extensions_Allowed (Off);
