pragma Extensions_Allowed (On);
limited with Java.Net.URL;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Sql.Rowset.Serial.SerialDatalink is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SerialDatalink (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                                This : Ref := null)
                                return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDatalink (This : access Typ)
                         return access Java.Net.URL.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SerialDatalink);
   pragma Import (Java, GetDatalink, "getDatalink");

end Javax.Sql.Rowset.Serial.SerialDatalink;
pragma Import (Java, Javax.Sql.Rowset.Serial.SerialDatalink, "javax.sql.rowset.serial.SerialDatalink");
pragma Extensions_Allowed (Off);
