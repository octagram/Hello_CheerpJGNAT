pragma Extensions_Allowed (On);
with Java.Lang.String;
limited with Java.Util.Map;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Sql.Ref;

package Javax.Sql.Rowset.Serial.SerialRef is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Ref_I : Java.Sql.Ref.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SerialRef (P1_Ref : access Standard.Java.Sql.Ref.Typ'Class; 
                           This : Ref := null)
                           return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except and
   --  Java.Sql.SQLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBaseTypeName (This : access Typ)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetObject (This : access Typ;
                       P1_Map : access Standard.Java.Util.Map.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetObject (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   procedure SetObject (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SerialRef);
   pragma Import (Java, GetBaseTypeName, "getBaseTypeName");
   pragma Import (Java, GetObject, "getObject");
   pragma Import (Java, SetObject, "setObject");

end Javax.Sql.Rowset.Serial.SerialRef;
pragma Import (Java, Javax.Sql.Rowset.Serial.SerialRef, "javax.sql.rowset.serial.SerialRef");
pragma Extensions_Allowed (Off);
