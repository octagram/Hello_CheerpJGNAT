pragma Extensions_Allowed (On);
with Java.Lang.String;
limited with Java.Sql.SQLData;
limited with Java.Util.Map;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Sql.Struct;

package Javax.Sql.Rowset.Serial.SerialStruct is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Struct_I : Java.Sql.Struct.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SerialStruct (P1_Struct : access Standard.Java.Sql.Struct.Typ'Class;
                              P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function New_SerialStruct (P1_SQLData : access Standard.Java.Sql.SQLData.Typ'Class;
                              P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSQLTypeName (This : access Typ)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetAttributes (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except

   function GetAttributes (This : access Typ;
                           P1_Map : access Standard.Java.Util.Map.Typ'Class)
                           return Standard.Java.Lang.Object.Ref;
   --  can raise Javax.Sql.Rowset.Serial.SerialException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SerialStruct);
   pragma Import (Java, GetSQLTypeName, "getSQLTypeName");
   pragma Import (Java, GetAttributes, "getAttributes");

end Javax.Sql.Rowset.Serial.SerialStruct;
pragma Import (Java, Javax.Sql.Rowset.Serial.SerialStruct, "javax.sql.rowset.serial.SerialStruct");
pragma Extensions_Allowed (Off);
