pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
limited with Java.Util.Map;
with Java.Lang.Object;
with Java.Sql.SQLInput;

package Javax.Sql.Rowset.Serial.SQLInputImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SQLInput_I : Java.Sql.SQLInput.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SQLInputImpl (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                              P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Java.Sql.SQLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function ReadString (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadBoolean (This : access Typ)
                         return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function ReadByte (This : access Typ)
                      return Java.Byte;
   --  can raise Java.Sql.SQLException.Except

   function ReadShort (This : access Typ)
                       return Java.Short;
   --  can raise Java.Sql.SQLException.Except

   function ReadInt (This : access Typ)
                     return Java.Int;
   --  can raise Java.Sql.SQLException.Except

   function ReadLong (This : access Typ)
                      return Java.Long;
   --  can raise Java.Sql.SQLException.Except

   function ReadFloat (This : access Typ)
                       return Java.Float;
   --  can raise Java.Sql.SQLException.Except

   function ReadDouble (This : access Typ)
                        return Java.Double;
   --  can raise Java.Sql.SQLException.Except

   function ReadBigDecimal (This : access Typ)
                            return access Java.Math.BigDecimal.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadBytes (This : access Typ)
                       return Java.Byte_Arr;
   --  can raise Java.Sql.SQLException.Except

   function ReadDate (This : access Typ)
                      return access Java.Sql.Date.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadTime (This : access Typ)
                      return access Java.Sql.Time.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadTimestamp (This : access Typ)
                           return access Java.Sql.Timestamp.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadCharacterStream (This : access Typ)
                                 return access Java.Io.Reader.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadAsciiStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadBinaryStream (This : access Typ)
                              return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadObject (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadRef (This : access Typ)
                     return access Java.Sql.Ref.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadBlob (This : access Typ)
                      return access Java.Sql.Blob.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadClob (This : access Typ)
                      return access Java.Sql.Clob.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadArray (This : access Typ)
                       return access Java.Sql.Array_K.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function WasNull (This : access Typ)
                     return Java.Boolean;
   --  can raise Java.Sql.SQLException.Except

   function ReadURL (This : access Typ)
                     return access Java.Net.URL.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadNClob (This : access Typ)
                       return access Java.Sql.NClob.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadNString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadSQLXML (This : access Typ)
                        return access Java.Sql.SQLXML.Typ'Class;
   --  can raise Java.Sql.SQLException.Except

   function ReadRowId (This : access Typ)
                       return access Java.Sql.RowId.Typ'Class;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SQLInputImpl);
   pragma Import (Java, ReadString, "readString");
   pragma Import (Java, ReadBoolean, "readBoolean");
   pragma Import (Java, ReadByte, "readByte");
   pragma Import (Java, ReadShort, "readShort");
   pragma Import (Java, ReadInt, "readInt");
   pragma Import (Java, ReadLong, "readLong");
   pragma Import (Java, ReadFloat, "readFloat");
   pragma Import (Java, ReadDouble, "readDouble");
   pragma Import (Java, ReadBigDecimal, "readBigDecimal");
   pragma Import (Java, ReadBytes, "readBytes");
   pragma Import (Java, ReadDate, "readDate");
   pragma Import (Java, ReadTime, "readTime");
   pragma Import (Java, ReadTimestamp, "readTimestamp");
   pragma Import (Java, ReadCharacterStream, "readCharacterStream");
   pragma Import (Java, ReadAsciiStream, "readAsciiStream");
   pragma Import (Java, ReadBinaryStream, "readBinaryStream");
   pragma Import (Java, ReadObject, "readObject");
   pragma Import (Java, ReadRef, "readRef");
   pragma Import (Java, ReadBlob, "readBlob");
   pragma Import (Java, ReadClob, "readClob");
   pragma Import (Java, ReadArray, "readArray");
   pragma Import (Java, WasNull, "wasNull");
   pragma Import (Java, ReadURL, "readURL");
   pragma Import (Java, ReadNClob, "readNClob");
   pragma Import (Java, ReadNString, "readNString");
   pragma Import (Java, ReadSQLXML, "readSQLXML");
   pragma Import (Java, ReadRowId, "readRowId");

end Javax.Sql.Rowset.Serial.SQLInputImpl;
pragma Import (Java, Javax.Sql.Rowset.Serial.SQLInputImpl, "javax.sql.rowset.serial.SQLInputImpl");
pragma Extensions_Allowed (Off);
