pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLData;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Struct;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
limited with Java.Util.Map;
limited with Java.Util.Vector;
with Java.Lang.Object;
with Java.Sql.SQLOutput;

package Javax.Sql.Rowset.Serial.SQLOutputImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SQLOutput_I : Java.Sql.SQLOutput.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SQLOutputImpl (P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                               P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Java.Sql.SQLException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteString (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBoolean (This : access Typ;
                           P1_Boolean : Java.Boolean);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteByte (This : access Typ;
                        P1_Byte : Java.Byte);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteShort (This : access Typ;
                         P1_Short : Java.Short);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteInt (This : access Typ;
                       P1_Int : Java.Int);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteLong (This : access Typ;
                        P1_Long : Java.Long);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteFloat (This : access Typ;
                         P1_Float : Java.Float);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteDouble (This : access Typ;
                          P1_Double : Java.Double);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBigDecimal (This : access Typ;
                              P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBytes (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteDate (This : access Typ;
                        P1_Date : access Standard.Java.Sql.Date.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteTime (This : access Typ;
                        P1_Time : access Standard.Java.Sql.Time.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteTimestamp (This : access Typ;
                             P1_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteCharacterStream (This : access Typ;
                                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteAsciiStream (This : access Typ;
                               P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBinaryStream (This : access Typ;
                                P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteObject (This : access Typ;
                          P1_SQLData : access Standard.Java.Sql.SQLData.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteRef (This : access Typ;
                       P1_Ref : access Standard.Java.Sql.Ref.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteBlob (This : access Typ;
                        P1_Blob : access Standard.Java.Sql.Blob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteClob (This : access Typ;
                        P1_Clob : access Standard.Java.Sql.Clob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteStruct (This : access Typ;
                          P1_Struct : access Standard.Java.Sql.Struct.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteArray (This : access Typ;
                         P1_Array_K : access Standard.Java.Sql.Array_K.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteURL (This : access Typ;
                       P1_URL : access Standard.Java.Net.URL.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteNString (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteNClob (This : access Typ;
                         P1_NClob : access Standard.Java.Sql.NClob.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteRowId (This : access Typ;
                         P1_RowId : access Standard.Java.Sql.RowId.Typ'Class);
   --  can raise Java.Sql.SQLException.Except

   procedure WriteSQLXML (This : access Typ;
                          P1_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class);
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SQLOutputImpl);
   pragma Import (Java, WriteString, "writeString");
   pragma Import (Java, WriteBoolean, "writeBoolean");
   pragma Import (Java, WriteByte, "writeByte");
   pragma Import (Java, WriteShort, "writeShort");
   pragma Import (Java, WriteInt, "writeInt");
   pragma Import (Java, WriteLong, "writeLong");
   pragma Import (Java, WriteFloat, "writeFloat");
   pragma Import (Java, WriteDouble, "writeDouble");
   pragma Import (Java, WriteBigDecimal, "writeBigDecimal");
   pragma Import (Java, WriteBytes, "writeBytes");
   pragma Import (Java, WriteDate, "writeDate");
   pragma Import (Java, WriteTime, "writeTime");
   pragma Import (Java, WriteTimestamp, "writeTimestamp");
   pragma Import (Java, WriteCharacterStream, "writeCharacterStream");
   pragma Import (Java, WriteAsciiStream, "writeAsciiStream");
   pragma Import (Java, WriteBinaryStream, "writeBinaryStream");
   pragma Import (Java, WriteObject, "writeObject");
   pragma Import (Java, WriteRef, "writeRef");
   pragma Import (Java, WriteBlob, "writeBlob");
   pragma Import (Java, WriteClob, "writeClob");
   pragma Import (Java, WriteStruct, "writeStruct");
   pragma Import (Java, WriteArray, "writeArray");
   pragma Import (Java, WriteURL, "writeURL");
   pragma Import (Java, WriteNString, "writeNString");
   pragma Import (Java, WriteNClob, "writeNClob");
   pragma Import (Java, WriteRowId, "writeRowId");
   pragma Import (Java, WriteSQLXML, "writeSQLXML");

end Javax.Sql.Rowset.Serial.SQLOutputImpl;
pragma Import (Java, Javax.Sql.Rowset.Serial.SQLOutputImpl, "javax.sql.rowset.serial.SQLOutputImpl");
pragma Extensions_Allowed (Off);
