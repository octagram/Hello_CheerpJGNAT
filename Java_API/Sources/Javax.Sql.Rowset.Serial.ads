pragma Extensions_Allowed (On);
package Javax.Sql.Rowset.Serial is
   pragma Preelaborate;
end Javax.Sql.Rowset.Serial;
pragma Import (Java, Javax.Sql.Rowset.Serial, "javax.sql.rowset.serial");
pragma Extensions_Allowed (Off);
