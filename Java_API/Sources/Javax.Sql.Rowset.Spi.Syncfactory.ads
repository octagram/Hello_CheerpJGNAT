pragma Extensions_Allowed (On);
with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Logging.Level;
limited with Java.Util.Logging.Logger;
limited with Javax.Naming.Context;
limited with Javax.Sql.Rowset.Spi.SyncProvider;
with Java.Lang.Object;

package Javax.Sql.Rowset.Spi.SyncFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure RegisterProvider (P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Sql.Rowset.Spi.SyncFactoryException.Except

   function GetSyncFactory return access Javax.Sql.Rowset.Spi.SyncFactory.Typ'Class;

   --  synchronized
   procedure UnregisterProvider (P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Sql.Rowset.Spi.SyncFactoryException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Sql.Rowset.Spi.SyncProvider.Typ'Class;
   --  can raise Javax.Sql.Rowset.Spi.SyncFactoryException.Except

   function GetRegisteredProviders return access Java.Util.Enumeration.Typ'Class;
   --  can raise Javax.Sql.Rowset.Spi.SyncFactoryException.Except

   procedure SetLogger (P1_Logger : access Standard.Java.Util.Logging.Logger.Typ'Class);

   procedure SetLogger (P1_Logger : access Standard.Java.Util.Logging.Logger.Typ'Class;
                        P2_Level : access Standard.Java.Util.Logging.Level.Typ'Class);

   function GetLogger return access Java.Util.Logging.Logger.Typ'Class;
   --  can raise Javax.Sql.Rowset.Spi.SyncFactoryException.Except

   procedure SetJNDIContext (P1_Context : access Standard.Javax.Naming.Context.Typ'Class);
   --  can raise Javax.Sql.Rowset.Spi.SyncFactoryException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   ROWSET_SYNC_PROVIDER : access Java.Lang.String.Typ'Class;

   ROWSET_SYNC_VENDOR : access Java.Lang.String.Typ'Class;

   ROWSET_SYNC_PROVIDER_VERSION : access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, RegisterProvider, "registerProvider");
   pragma Import (Java, GetSyncFactory, "getSyncFactory");
   pragma Import (Java, UnregisterProvider, "unregisterProvider");
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetRegisteredProviders, "getRegisteredProviders");
   pragma Import (Java, SetLogger, "setLogger");
   pragma Import (Java, GetLogger, "getLogger");
   pragma Import (Java, SetJNDIContext, "setJNDIContext");
   pragma Import (Java, ROWSET_SYNC_PROVIDER, "ROWSET_SYNC_PROVIDER");
   pragma Import (Java, ROWSET_SYNC_VENDOR, "ROWSET_SYNC_VENDOR");
   pragma Import (Java, ROWSET_SYNC_PROVIDER_VERSION, "ROWSET_SYNC_PROVIDER_VERSION");

end Javax.Sql.Rowset.Spi.SyncFactory;
pragma Import (Java, Javax.Sql.Rowset.Spi.SyncFactory, "javax.sql.rowset.spi.SyncFactory");
pragma Extensions_Allowed (Off);
