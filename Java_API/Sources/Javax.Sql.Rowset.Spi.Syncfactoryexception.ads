pragma Extensions_Allowed (On);
with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Iterable;
with Java.Lang.Object;
with Java.Sql.SQLException;

package Javax.Sql.Rowset.Spi.SyncFactoryException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Iterable_I : Java.Lang.Iterable.Ref)
    is new Java.Sql.SQLException.Typ(Serializable_I,
                                     Iterable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SyncFactoryException (This : Ref := null)
                                      return Ref;

   function New_SyncFactoryException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.sql.rowset.spi.SyncFactoryException");
   pragma Java_Constructor (New_SyncFactoryException);

end Javax.Sql.Rowset.Spi.SyncFactoryException;
pragma Import (Java, Javax.Sql.Rowset.Spi.SyncFactoryException, "javax.sql.rowset.spi.SyncFactoryException");
pragma Extensions_Allowed (Off);
