pragma Extensions_Allowed (On);
with Java.Lang.String;
limited with Javax.Sql.RowSetReader;
limited with Javax.Sql.RowSetWriter;
with Java.Lang.Object;

package Javax.Sql.Rowset.Spi.SyncProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SyncProvider (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetProviderID (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetRowSetReader (This : access Typ)
                             return access Javax.Sql.RowSetReader.Typ'Class is abstract;

   function GetRowSetWriter (This : access Typ)
                             return access Javax.Sql.RowSetWriter.Typ'Class is abstract;

   function GetProviderGrade (This : access Typ)
                              return Java.Int is abstract;

   procedure SetDataSourceLock (This : access Typ;
                                P1_Int : Java.Int) is abstract;
   --  can raise Javax.Sql.Rowset.Spi.SyncProviderException.Except

   function GetDataSourceLock (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Javax.Sql.Rowset.Spi.SyncProviderException.Except

   function SupportsUpdatableView (This : access Typ)
                                   return Java.Int is abstract;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function GetVendor (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   GRADE_NONE : Java.Int;

   GRADE_CHECK_MODIFIED_AT_COMMIT : Java.Int;

   GRADE_CHECK_ALL_AT_COMMIT : Java.Int;

   GRADE_LOCK_WHEN_MODIFIED : Java.Int;

   GRADE_LOCK_WHEN_LOADED : Java.Int;

   DATASOURCE_NO_LOCK : Java.Int;

   DATASOURCE_ROW_LOCK : Java.Int;

   DATASOURCE_TABLE_LOCK : Java.Int;

   DATASOURCE_DB_LOCK : Java.Int;

   UPDATABLE_VIEW_SYNC : Java.Int;

   NONUPDATABLE_VIEW_SYNC : Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SyncProvider);
   pragma Export (Java, GetProviderID, "getProviderID");
   pragma Export (Java, GetRowSetReader, "getRowSetReader");
   pragma Export (Java, GetRowSetWriter, "getRowSetWriter");
   pragma Export (Java, GetProviderGrade, "getProviderGrade");
   pragma Export (Java, SetDataSourceLock, "setDataSourceLock");
   pragma Export (Java, GetDataSourceLock, "getDataSourceLock");
   pragma Export (Java, SupportsUpdatableView, "supportsUpdatableView");
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, GetVendor, "getVendor");
   pragma Import (Java, GRADE_NONE, "GRADE_NONE");
   pragma Import (Java, GRADE_CHECK_MODIFIED_AT_COMMIT, "GRADE_CHECK_MODIFIED_AT_COMMIT");
   pragma Import (Java, GRADE_CHECK_ALL_AT_COMMIT, "GRADE_CHECK_ALL_AT_COMMIT");
   pragma Import (Java, GRADE_LOCK_WHEN_MODIFIED, "GRADE_LOCK_WHEN_MODIFIED");
   pragma Import (Java, GRADE_LOCK_WHEN_LOADED, "GRADE_LOCK_WHEN_LOADED");
   pragma Import (Java, DATASOURCE_NO_LOCK, "DATASOURCE_NO_LOCK");
   pragma Import (Java, DATASOURCE_ROW_LOCK, "DATASOURCE_ROW_LOCK");
   pragma Import (Java, DATASOURCE_TABLE_LOCK, "DATASOURCE_TABLE_LOCK");
   pragma Import (Java, DATASOURCE_DB_LOCK, "DATASOURCE_DB_LOCK");
   pragma Import (Java, UPDATABLE_VIEW_SYNC, "UPDATABLE_VIEW_SYNC");
   pragma Import (Java, NONUPDATABLE_VIEW_SYNC, "NONUPDATABLE_VIEW_SYNC");

end Javax.Sql.Rowset.Spi.SyncProvider;
pragma Import (Java, Javax.Sql.Rowset.Spi.SyncProvider, "javax.sql.rowset.spi.SyncProvider");
pragma Extensions_Allowed (Off);
