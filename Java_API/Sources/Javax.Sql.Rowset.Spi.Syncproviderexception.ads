pragma Extensions_Allowed (On);
with Java.Lang.String;
limited with Javax.Sql.Rowset.Spi.SyncResolver;
with Java.Io.Serializable;
with Java.Lang.Iterable;
with Java.Lang.Object;
with Java.Sql.SQLException;

package Javax.Sql.Rowset.Spi.SyncProviderException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Iterable_I : Java.Lang.Iterable.Ref)
    is new Java.Sql.SQLException.Typ(Serializable_I,
                                     Iterable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SyncProviderException (This : Ref := null)
                                       return Ref;

   function New_SyncProviderException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_SyncProviderException (P1_SyncResolver : access Standard.Javax.Sql.Rowset.Spi.SyncResolver.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSyncResolver (This : access Typ)
                             return access Javax.Sql.Rowset.Spi.SyncResolver.Typ'Class;

   procedure SetSyncResolver (This : access Typ;
                              P1_SyncResolver : access Standard.Javax.Sql.Rowset.Spi.SyncResolver.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.sql.rowset.spi.SyncProviderException");
   pragma Java_Constructor (New_SyncProviderException);
   pragma Import (Java, GetSyncResolver, "getSyncResolver");
   pragma Import (Java, SetSyncResolver, "setSyncResolver");

end Javax.Sql.Rowset.Spi.SyncProviderException;
pragma Import (Java, Javax.Sql.Rowset.Spi.SyncProviderException, "javax.sql.rowset.spi.SyncProviderException");
pragma Extensions_Allowed (Off);
