pragma Extensions_Allowed (On);
with Java.Lang.String;
with Java.Lang.Object;
with Javax.Sql.RowSet;

package Javax.Sql.Rowset.Spi.SyncResolver is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RowSet_I : Javax.Sql.RowSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStatus (This : access Typ)
                       return Java.Int is abstract;

   function GetConflictValue (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetConflictValue (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetResolvedValue (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetResolvedValue (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function NextConflict (This : access Typ)
                          return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   function PreviousConflict (This : access Typ)
                              return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UPDATE_ROW_CONFLICT : constant Java.Int;

   --  final
   DELETE_ROW_CONFLICT : constant Java.Int;

   --  final
   INSERT_ROW_CONFLICT : constant Java.Int;

   --  final
   NO_ROW_CONFLICT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetStatus, "getStatus");
   pragma Export (Java, GetConflictValue, "getConflictValue");
   pragma Export (Java, SetResolvedValue, "setResolvedValue");
   pragma Export (Java, NextConflict, "nextConflict");
   pragma Export (Java, PreviousConflict, "previousConflict");
   pragma Import (Java, UPDATE_ROW_CONFLICT, "UPDATE_ROW_CONFLICT");
   pragma Import (Java, DELETE_ROW_CONFLICT, "DELETE_ROW_CONFLICT");
   pragma Import (Java, INSERT_ROW_CONFLICT, "INSERT_ROW_CONFLICT");
   pragma Import (Java, NO_ROW_CONFLICT, "NO_ROW_CONFLICT");

end Javax.Sql.Rowset.Spi.SyncResolver;
pragma Import (Java, Javax.Sql.Rowset.Spi.SyncResolver, "javax.sql.rowset.spi.SyncResolver");
pragma Extensions_Allowed (Off);
