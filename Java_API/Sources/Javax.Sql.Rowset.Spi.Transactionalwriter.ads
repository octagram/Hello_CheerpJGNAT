pragma Extensions_Allowed (On);
limited with Java.Sql.Savepoint;
with Java.Lang.Object;
with Javax.Sql.RowSetWriter;

package Javax.Sql.Rowset.Spi.TransactionalWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RowSetWriter_I : Javax.Sql.RowSetWriter.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Commit (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Rollback (This : access Typ;
                       P1_Savepoint : access Standard.Java.Sql.Savepoint.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Commit, "commit");
   pragma Export (Java, Rollback, "rollback");

end Javax.Sql.Rowset.Spi.TransactionalWriter;
pragma Import (Java, Javax.Sql.Rowset.Spi.TransactionalWriter, "javax.sql.rowset.spi.TransactionalWriter");
pragma Extensions_Allowed (Off);
