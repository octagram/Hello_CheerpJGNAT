pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Javax.Sql.Rowset.WebRowSet;
with Java.Lang.Object;
with Javax.Sql.RowSetReader;

package Javax.Sql.Rowset.Spi.XmlReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RowSetReader_I : Javax.Sql.RowSetReader.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ReadXML (This : access Typ;
                      P1_WebRowSet : access Standard.Javax.Sql.Rowset.WebRowSet.Typ'Class;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ReadXML, "readXML");

end Javax.Sql.Rowset.Spi.XmlReader;
pragma Import (Java, Javax.Sql.Rowset.Spi.XmlReader, "javax.sql.rowset.spi.XmlReader");
pragma Extensions_Allowed (Off);
