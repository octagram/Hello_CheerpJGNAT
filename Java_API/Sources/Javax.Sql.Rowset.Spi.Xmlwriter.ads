pragma Extensions_Allowed (On);
limited with Java.Io.Writer;
limited with Javax.Sql.Rowset.WebRowSet;
with Java.Lang.Object;
with Javax.Sql.RowSetWriter;

package Javax.Sql.Rowset.Spi.XmlWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RowSetWriter_I : Javax.Sql.RowSetWriter.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteXML (This : access Typ;
                       P1_WebRowSet : access Standard.Javax.Sql.Rowset.WebRowSet.Typ'Class;
                       P2_Writer : access Standard.Java.Io.Writer.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WriteXML, "writeXML");

end Javax.Sql.Rowset.Spi.XmlWriter;
pragma Import (Java, Javax.Sql.Rowset.Spi.XmlWriter, "javax.sql.rowset.spi.XmlWriter");
pragma Extensions_Allowed (Off);
