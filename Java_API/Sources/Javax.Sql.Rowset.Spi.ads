pragma Extensions_Allowed (On);
package Javax.Sql.Rowset.Spi is
   pragma Preelaborate;
end Javax.Sql.Rowset.Spi;
pragma Import (Java, Javax.Sql.Rowset.Spi, "javax.sql.rowset.spi");
pragma Extensions_Allowed (Off);
