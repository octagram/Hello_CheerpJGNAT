pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
with Java.Lang.String;
with Java.Sql.ResultSet;
with Java.Lang.Object;
with Javax.Sql.Rowset.CachedRowSet;

package Javax.Sql.Rowset.WebRowSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CachedRowSet_I : Javax.Sql.Rowset.CachedRowSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ReadXml (This : access Typ;
                      P1_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ReadXml (This : access Typ;
                      P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except and
   --  Java.Io.IOException.Except

   procedure WriteXml (This : access Typ;
                       P1_ResultSet : access Standard.Java.Sql.ResultSet.Typ'Class;
                       P2_Writer : access Standard.Java.Io.Writer.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteXml (This : access Typ;
                       P1_ResultSet : access Standard.Java.Sql.ResultSet.Typ'Class;
                       P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except and
   --  Java.Io.IOException.Except

   procedure WriteXml (This : access Typ;
                       P1_Writer : access Standard.Java.Io.Writer.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure WriteXml (This : access Typ;
                       P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except and
   --  Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PUBLIC_XML_SCHEMA : constant access Java.Lang.String.Typ'Class;

   --  final
   SCHEMA_SYSTEM_ID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ReadXml, "readXml");
   pragma Export (Java, WriteXml, "writeXml");
   pragma Import (Java, PUBLIC_XML_SCHEMA, "PUBLIC_XML_SCHEMA");
   pragma Import (Java, SCHEMA_SYSTEM_ID, "SCHEMA_SYSTEM_ID");

end Javax.Sql.Rowset.WebRowSet;
pragma Import (Java, Javax.Sql.Rowset.WebRowSet, "javax.sql.rowset.WebRowSet");
pragma Extensions_Allowed (Off);
