pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Net.URL;
limited with Java.Sql.Array_K;
limited with Java.Sql.Blob;
limited with Java.Sql.Clob;
limited with Java.Sql.Date;
limited with Java.Sql.NClob;
limited with Java.Sql.Ref;
limited with Java.Sql.RowId;
limited with Java.Sql.SQLXML;
limited with Java.Sql.Time;
limited with Java.Sql.Timestamp;
limited with Java.Util.Calendar;
limited with Java.Util.Map;
limited with Javax.Sql.RowSetListener;
with Java.Lang.Object;
with Java.Sql.ResultSet;

package Javax.Sql.RowSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ResultSet_I : Java.Sql.ResultSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUrl (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetUrl (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract with Export => "setUrl", Convention => Java;
   --  can raise Java.Sql.SQLException.Except

   function GetDataSourceName (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetDataSourceName (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetUsername (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetUsername (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetPassword (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPassword (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTransactionIsolation (This : access Typ)
                                     return Java.Int is abstract;

   procedure SetTransactionIsolation (This : access Typ;
                                      P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetTypeMap (This : access Typ)
                        return access Java.Util.Map.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTypeMap (This : access Typ;
                         P1_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetCommand (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCommand (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function IsReadOnly (This : access Typ)
                        return Java.Boolean is abstract;

   procedure SetReadOnly (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxFieldSize (This : access Typ)
                             return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMaxFieldSize (This : access Typ;
                              P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetMaxRows (This : access Typ)
                        return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMaxRows (This : access Typ;
                         P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetEscapeProcessing (This : access Typ)
                                 return Java.Boolean is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetEscapeProcessing (This : access Typ;
                                  P1_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetQueryTimeout (This : access Typ)
                             return Java.Int is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetQueryTimeout (This : access Typ;
                              P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetType (This : access Typ;
                      P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetConcurrency (This : access Typ;
                             P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNull (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Int : Java.Int;
                      P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBoolean (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBoolean (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetByte (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Byte : Java.Byte) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetByte (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Byte : Java.Byte) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetShort (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Short : Java.Short) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetShort (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Short : Java.Short) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetInt (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetInt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetLong (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetLong (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFloat (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Float : Java.Float) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetFloat (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Float : Java.Float) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDouble (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Double : Java.Double) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDouble (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Double : Java.Double) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBigDecimal (This : access Typ;
                            P1_Int : Java.Int;
                            P2_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBigDecimal (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetString (This : access Typ;
                        P1_Int : Java.Int;
                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetString (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBytes (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBytes (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_Int : Java.Int;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                             P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                             P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_Int : Java.Int;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                 P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                 P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_Int : Java.Int;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAsciiStream (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_Int : Java.Int;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBinaryStream (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCharacterStream (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetObject (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetRef (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Ref : access Standard.Java.Sql.Ref.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Blob : access Standard.Java.Sql.Blob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                      P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                      P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Blob : access Standard.Java.Sql.Blob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetBlob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Clob : access Standard.Java.Sql.Clob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                      P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                      P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Clob : access Standard.Java.Sql.Clob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetClob (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetArray (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Array_K : access Standard.Java.Sql.Array_K.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetDate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Date : access Standard.Java.Sql.Date.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTime (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Time : access Standard.Java.Sql.Time.Typ'Class;
                      P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class;
                           P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTimestamp (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Timestamp : access Standard.Java.Sql.Timestamp.Typ'Class;
                           P3_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure ClearParameters (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure Execute (This : access Typ) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure AddRowSetListener (This : access Typ;
                                P1_RowSetListener : access Standard.Javax.Sql.RowSetListener.Typ'Class) is abstract;

   procedure RemoveRowSetListener (This : access Typ;
                                   P1_RowSetListener : access Standard.Javax.Sql.RowSetListener.Typ'Class) is abstract;

   procedure SetSQLXML (This : access Typ;
                        P1_Int : Java.Int;
                        P2_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetSQLXML (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_SQLXML : access Standard.Java.Sql.SQLXML.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetRowId (This : access Typ;
                       P1_Int : Java.Int;
                       P2_RowId : access Standard.Java.Sql.RowId.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetRowId (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_RowId : access Standard.Java.Sql.RowId.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNString (This : access Typ;
                         P1_Int : Java.Int;
                         P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                  P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                                  P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNCharacterStream (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_NClob : access Standard.Java.Sql.NClob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                       P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class;
                       P3_Long : Java.Long) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_Int : Java.Int;
                       P2_NClob : access Standard.Java.Sql.NClob.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNClob (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetURL (This : access Typ;
                     P1_Int : Java.Int;
                     P2_URL : access Standard.Java.Net.URL.Typ'Class) is abstract with Export => "setURL", Convention => Java;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetUrl, "getUrl");
   -- pragma Export (Java, SetUrl, "setUrl");
   pragma Export (Java, GetDataSourceName, "getDataSourceName");
   pragma Export (Java, SetDataSourceName, "setDataSourceName");
   pragma Export (Java, GetUsername, "getUsername");
   pragma Export (Java, SetUsername, "setUsername");
   pragma Export (Java, GetPassword, "getPassword");
   pragma Export (Java, SetPassword, "setPassword");
   pragma Export (Java, GetTransactionIsolation, "getTransactionIsolation");
   pragma Export (Java, SetTransactionIsolation, "setTransactionIsolation");
   pragma Export (Java, GetTypeMap, "getTypeMap");
   pragma Export (Java, SetTypeMap, "setTypeMap");
   pragma Export (Java, GetCommand, "getCommand");
   pragma Export (Java, SetCommand, "setCommand");
   pragma Export (Java, IsReadOnly, "isReadOnly");
   pragma Export (Java, SetReadOnly, "setReadOnly");
   pragma Export (Java, GetMaxFieldSize, "getMaxFieldSize");
   pragma Export (Java, SetMaxFieldSize, "setMaxFieldSize");
   pragma Export (Java, GetMaxRows, "getMaxRows");
   pragma Export (Java, SetMaxRows, "setMaxRows");
   pragma Export (Java, GetEscapeProcessing, "getEscapeProcessing");
   pragma Export (Java, SetEscapeProcessing, "setEscapeProcessing");
   pragma Export (Java, GetQueryTimeout, "getQueryTimeout");
   pragma Export (Java, SetQueryTimeout, "setQueryTimeout");
   pragma Export (Java, SetType, "setType");
   pragma Export (Java, SetConcurrency, "setConcurrency");
   pragma Export (Java, SetNull, "setNull");
   pragma Export (Java, SetBoolean, "setBoolean");
   pragma Export (Java, SetByte, "setByte");
   pragma Export (Java, SetShort, "setShort");
   pragma Export (Java, SetInt, "setInt");
   pragma Export (Java, SetLong, "setLong");
   pragma Export (Java, SetFloat, "setFloat");
   pragma Export (Java, SetDouble, "setDouble");
   pragma Export (Java, SetBigDecimal, "setBigDecimal");
   pragma Export (Java, SetString, "setString");
   pragma Export (Java, SetBytes, "setBytes");
   pragma Export (Java, SetDate, "setDate");
   pragma Export (Java, SetTime, "setTime");
   pragma Export (Java, SetTimestamp, "setTimestamp");
   pragma Export (Java, SetAsciiStream, "setAsciiStream");
   pragma Export (Java, SetBinaryStream, "setBinaryStream");
   pragma Export (Java, SetCharacterStream, "setCharacterStream");
   pragma Export (Java, SetNCharacterStream, "setNCharacterStream");
   pragma Export (Java, SetObject, "setObject");
   pragma Export (Java, SetRef, "setRef");
   pragma Export (Java, SetBlob, "setBlob");
   pragma Export (Java, SetClob, "setClob");
   pragma Export (Java, SetArray, "setArray");
   pragma Export (Java, ClearParameters, "clearParameters");
   pragma Export (Java, Execute, "execute");
   pragma Export (Java, AddRowSetListener, "addRowSetListener");
   pragma Export (Java, RemoveRowSetListener, "removeRowSetListener");
   pragma Export (Java, SetSQLXML, "setSQLXML");
   pragma Export (Java, SetRowId, "setRowId");
   pragma Export (Java, SetNString, "setNString");
   pragma Export (Java, SetNClob, "setNClob");
   -- pragma Export (Java, SetURL, "setURL");

end Javax.Sql.RowSet;
pragma Import (Java, Javax.Sql.RowSet, "javax.sql.RowSet");
pragma Extensions_Allowed (Off);
