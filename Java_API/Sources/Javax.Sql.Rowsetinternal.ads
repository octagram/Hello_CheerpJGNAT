pragma Extensions_Allowed (On);
limited with Java.Sql.Connection;
limited with Java.Sql.ResultSet;
limited with Javax.Sql.RowSetMetaData;
with Java.Lang.Object;

package Javax.Sql.RowSetInternal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParams (This : access Typ)
                       return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetConnection (This : access Typ)
                           return access Java.Sql.Connection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetMetaData (This : access Typ;
                          P1_RowSetMetaData : access Standard.Javax.Sql.RowSetMetaData.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetOriginal (This : access Typ)
                         return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetOriginalRow (This : access Typ)
                            return access Java.Sql.ResultSet.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParams, "getParams");
   pragma Export (Java, GetConnection, "getConnection");
   pragma Export (Java, SetMetaData, "setMetaData");
   pragma Export (Java, GetOriginal, "getOriginal");
   pragma Export (Java, GetOriginalRow, "getOriginalRow");

end Javax.Sql.RowSetInternal;
pragma Import (Java, Javax.Sql.RowSetInternal, "javax.sql.RowSetInternal");
pragma Extensions_Allowed (Off);
