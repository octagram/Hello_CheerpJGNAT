pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Java.Sql.ResultSetMetaData;

package Javax.Sql.RowSetMetaData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ResultSetMetaData_I : Java.Sql.ResultSetMetaData.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetColumnCount (This : access Typ;
                             P1_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetAutoIncrement (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCaseSensitive (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetSearchable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCurrency (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetNullable (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetSigned (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnDisplaySize (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnLabel (This : access Typ;
                             P1_Int : Java.Int;
                             P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnName (This : access Typ;
                            P1_Int : Java.Int;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetSchemaName (This : access Typ;
                            P1_Int : Java.Int;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetPrecision (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetScale (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetTableName (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetCatalogName (This : access Typ;
                             P1_Int : Java.Int;
                             P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnType (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int) is abstract;
   --  can raise Java.Sql.SQLException.Except

   procedure SetColumnTypeName (This : access Typ;
                                P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetColumnCount, "setColumnCount");
   pragma Export (Java, SetAutoIncrement, "setAutoIncrement");
   pragma Export (Java, SetCaseSensitive, "setCaseSensitive");
   pragma Export (Java, SetSearchable, "setSearchable");
   pragma Export (Java, SetCurrency, "setCurrency");
   pragma Export (Java, SetNullable, "setNullable");
   pragma Export (Java, SetSigned, "setSigned");
   pragma Export (Java, SetColumnDisplaySize, "setColumnDisplaySize");
   pragma Export (Java, SetColumnLabel, "setColumnLabel");
   pragma Export (Java, SetColumnName, "setColumnName");
   pragma Export (Java, SetSchemaName, "setSchemaName");
   pragma Export (Java, SetPrecision, "setPrecision");
   pragma Export (Java, SetScale, "setScale");
   pragma Export (Java, SetTableName, "setTableName");
   pragma Export (Java, SetCatalogName, "setCatalogName");
   pragma Export (Java, SetColumnType, "setColumnType");
   pragma Export (Java, SetColumnTypeName, "setColumnTypeName");

end Javax.Sql.RowSetMetaData;
pragma Import (Java, Javax.Sql.RowSetMetaData, "javax.sql.RowSetMetaData");
pragma Extensions_Allowed (Off);
