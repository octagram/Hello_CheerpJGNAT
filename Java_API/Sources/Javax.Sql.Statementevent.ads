pragma Extensions_Allowed (On);
limited with Java.Sql.PreparedStatement;
limited with Java.Sql.SQLException;
limited with Javax.Sql.PooledConnection;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Sql.StatementEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StatementEvent (P1_PooledConnection : access Standard.Javax.Sql.PooledConnection.Typ'Class;
                                P2_PreparedStatement : access Standard.Java.Sql.PreparedStatement.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_StatementEvent (P1_PooledConnection : access Standard.Javax.Sql.PooledConnection.Typ'Class;
                                P2_PreparedStatement : access Standard.Java.Sql.PreparedStatement.Typ'Class;
                                P3_SQLException : access Standard.Java.Sql.SQLException.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStatement (This : access Typ)
                          return access Java.Sql.PreparedStatement.Typ'Class;

   function GetSQLException (This : access Typ)
                             return access Java.Sql.SQLException.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StatementEvent);
   pragma Import (Java, GetStatement, "getStatement");
   pragma Import (Java, GetSQLException, "getSQLException");

end Javax.Sql.StatementEvent;
pragma Import (Java, Javax.Sql.StatementEvent, "javax.sql.StatementEvent");
pragma Extensions_Allowed (Off);
