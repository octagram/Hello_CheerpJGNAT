pragma Extensions_Allowed (On);
limited with Javax.Transaction.Xa.XAResource;
with Java.Lang.Object;
with Javax.Sql.PooledConnection;

package Javax.Sql.XAConnection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            PooledConnection_I : Javax.Sql.PooledConnection.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXAResource (This : access Typ)
                           return access Javax.Transaction.Xa.XAResource.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetXAResource, "getXAResource");

end Javax.Sql.XAConnection;
pragma Import (Java, Javax.Sql.XAConnection, "javax.sql.XAConnection");
pragma Extensions_Allowed (Off);
