pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Sql.XAConnection;
with Java.Lang.Object;
with Javax.Sql.CommonDataSource;

package Javax.Sql.XADataSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CommonDataSource_I : Javax.Sql.CommonDataSource.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXAConnection (This : access Typ)
                             return access Javax.Sql.XAConnection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except

   function GetXAConnection (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Sql.XAConnection.Typ'Class is abstract;
   --  can raise Java.Sql.SQLException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetXAConnection, "getXAConnection");

end Javax.Sql.XADataSource;
pragma Import (Java, Javax.Sql.XADataSource, "javax.sql.XADataSource");
pragma Extensions_Allowed (Off);
