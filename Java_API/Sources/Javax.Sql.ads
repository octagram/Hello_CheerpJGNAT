pragma Extensions_Allowed (On);
package Javax.Sql is
   pragma Preelaborate;
end Javax.Sql;
pragma Import (Java, Javax.Sql, "javax.sql");
pragma Extensions_Allowed (Off);
