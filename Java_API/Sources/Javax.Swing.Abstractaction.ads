pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Event.SwingPropertyChangeSupport;
limited with Javax.Swing.Icon;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Action;

package Javax.Swing.AbstractAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Action_I : Javax.Swing.Action.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Enabled : Java.Boolean;
      pragma Import (Java, Enabled, "enabled");

      --  protected
      ChangeSupport : access Javax.Swing.Event.SwingPropertyChangeSupport.Typ'Class;
      pragma Import (Java, ChangeSupport, "changeSupport");

   end record;

   function New_AbstractAction (This : Ref := null)
                                return Ref;

   function New_AbstractAction (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_AbstractAction (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   procedure PutValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function GetKeys (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractAction);
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, PutValue, "putValue");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, GetKeys, "getKeys");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Import (Java, Clone, "clone");

end Javax.Swing.AbstractAction;
pragma Import (Java, Javax.Swing.AbstractAction, "javax.swing.AbstractAction");
pragma Extensions_Allowed (Off);
