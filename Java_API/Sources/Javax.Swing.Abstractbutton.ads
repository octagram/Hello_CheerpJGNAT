pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Event.ItemEvent;
limited with Java.Awt.Event.ItemListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.LayoutManager;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.ButtonModel;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Plaf.ButtonUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.JComponent;
with Javax.Swing.SwingConstants;
with Java.Awt.Image;

package Javax.Swing.AbstractButton is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is abstract new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                               ImageObserver_I,
                                               Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Model : access Javax.Swing.ButtonModel.Typ'Class;
      pragma Import (Java, Model, "model");

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

      --  protected
      ActionListener : access Java.Awt.Event.ActionListener.Typ'Class;
      pragma Import (Java, ActionListener, "actionListener");

      --  protected
      ItemListener : access Java.Awt.Event.ItemListener.Typ'Class;
      pragma Import (Java, ItemListener, "itemListener");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

   end record;

   function New_AbstractButton (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetHideActionText (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function GetHideActionText (This : access Typ)
                               return Java.Boolean;

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   procedure SetSelected (This : access Typ;
                          P1_Boolean : Java.Boolean);

   procedure DoClick (This : access Typ);

   procedure DoClick (This : access Typ;
                      P1_Int : Java.Int);

   procedure SetMargin (This : access Typ;
                        P1_Insets : access Standard.Java.Awt.Insets.Typ'Class);

   function GetMargin (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   function GetIcon (This : access Typ)
                     return access Javax.Swing.Icon.Typ'Class;

   procedure SetIcon (This : access Typ;
                      P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetPressedIcon (This : access Typ)
                            return access Javax.Swing.Icon.Typ'Class;

   procedure SetPressedIcon (This : access Typ;
                             P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetSelectedIcon (This : access Typ)
                             return access Javax.Swing.Icon.Typ'Class;

   procedure SetSelectedIcon (This : access Typ;
                              P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetRolloverIcon (This : access Typ)
                             return access Javax.Swing.Icon.Typ'Class;

   procedure SetRolloverIcon (This : access Typ;
                              P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetRolloverSelectedIcon (This : access Typ)
                                     return access Javax.Swing.Icon.Typ'Class;

   procedure SetRolloverSelectedIcon (This : access Typ;
                                      P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetDisabledIcon (This : access Typ)
                             return access Javax.Swing.Icon.Typ'Class;

   procedure SetDisabledIcon (This : access Typ;
                              P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetDisabledSelectedIcon (This : access Typ)
                                     return access Javax.Swing.Icon.Typ'Class;

   procedure SetDisabledSelectedIcon (This : access Typ;
                                      P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetVerticalAlignment (This : access Typ)
                                  return Java.Int;

   procedure SetVerticalAlignment (This : access Typ;
                                   P1_Int : Java.Int);

   function GetHorizontalAlignment (This : access Typ)
                                    return Java.Int;

   procedure SetHorizontalAlignment (This : access Typ;
                                     P1_Int : Java.Int);

   function GetVerticalTextPosition (This : access Typ)
                                     return Java.Int;

   procedure SetVerticalTextPosition (This : access Typ;
                                      P1_Int : Java.Int);

   function GetHorizontalTextPosition (This : access Typ)
                                       return Java.Int;

   procedure SetHorizontalTextPosition (This : access Typ;
                                        P1_Int : Java.Int);

   function GetIconTextGap (This : access Typ)
                            return Java.Int;

   procedure SetIconTextGap (This : access Typ;
                             P1_Int : Java.Int);

   --  protected
   function CheckHorizontalKey (This : access Typ;
                                P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int;

   --  protected
   function CheckVerticalKey (This : access Typ;
                              P1_Int : Java.Int;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Int;

   procedure RemoveNotify (This : access Typ);

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   procedure SetAction (This : access Typ;
                        P1_Action : access Standard.Javax.Swing.Action.Typ'Class);

   function GetAction (This : access Typ)
                       return access Javax.Swing.Action.Typ'Class;

   --  protected
   procedure ConfigurePropertiesFromAction (This : access Typ;
                                            P1_Action : access Standard.Javax.Swing.Action.Typ'Class);

   --  protected
   procedure ActionPropertyChanged (This : access Typ;
                                    P1_Action : access Standard.Javax.Swing.Action.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function CreateActionPropertyChangeListener (This : access Typ;
                                                P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                                                return access Java.Beans.PropertyChangeListener.Typ'Class;

   function IsBorderPainted (This : access Typ)
                             return Java.Boolean;

   procedure SetBorderPainted (This : access Typ;
                               P1_Boolean : Java.Boolean);

   --  protected
   procedure PaintBorder (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function IsFocusPainted (This : access Typ)
                            return Java.Boolean;

   procedure SetFocusPainted (This : access Typ;
                              P1_Boolean : Java.Boolean);

   function IsContentAreaFilled (This : access Typ)
                                 return Java.Boolean;

   procedure SetContentAreaFilled (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function IsRolloverEnabled (This : access Typ)
                               return Java.Boolean;

   procedure SetRolloverEnabled (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   function GetMnemonic (This : access Typ)
                         return Java.Int;

   procedure SetMnemonic (This : access Typ;
                          P1_Int : Java.Int);

   procedure SetMnemonic (This : access Typ;
                          P1_Char : Java.Char);

   procedure SetDisplayedMnemonicIndex (This : access Typ;
                                        P1_Int : Java.Int);
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetDisplayedMnemonicIndex (This : access Typ)
                                       return Java.Int;

   procedure SetMultiClickThreshhold (This : access Typ;
                                      P1_Long : Java.Long);

   function GetMultiClickThreshhold (This : access Typ)
                                     return Java.Long;

   function GetModel (This : access Typ)
                      return access Javax.Swing.ButtonModel.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_ButtonModel : access Standard.Javax.Swing.ButtonModel.Typ'Class);

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ButtonUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_ButtonUI : access Standard.Javax.Swing.Plaf.ButtonUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   function CreateChangeListener (This : access Typ)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   --  protected
   procedure FireActionPerformed (This : access Typ;
                                  P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   --  protected
   procedure FireItemStateChanged (This : access Typ;
                                   P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);

   --  protected
   function CreateActionListener (This : access Typ)
                                  return access Java.Awt.Event.ActionListener.Typ'Class;

   --  protected
   function CreateItemListener (This : access Typ)
                                return access Java.Awt.Event.ItemListener.Typ'Class;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure AddItemListener (This : access Typ;
                              P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   procedure RemoveItemListener (This : access Typ;
                                 P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   function GetItemListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetSelectedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure Init (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function ImageUpdate (This : access Typ;
                         P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int : Java.Int)
                         return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MODEL_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   TEXT_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   MNEMONIC_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   MARGIN_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   VERTICAL_ALIGNMENT_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   HORIZONTAL_ALIGNMENT_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   VERTICAL_TEXT_POSITION_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   HORIZONTAL_TEXT_POSITION_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   BORDER_PAINTED_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FOCUS_PAINTED_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ROLLOVER_ENABLED_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CONTENT_AREA_FILLED_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ICON_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   PRESSED_ICON_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SELECTED_ICON_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ROLLOVER_ICON_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ROLLOVER_SELECTED_ICON_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DISABLED_ICON_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DISABLED_SELECTED_ICON_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractButton);
   pragma Import (Java, SetHideActionText, "setHideActionText");
   pragma Import (Java, GetHideActionText, "getHideActionText");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, DoClick, "doClick");
   pragma Import (Java, SetMargin, "setMargin");
   pragma Import (Java, GetMargin, "getMargin");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, SetIcon, "setIcon");
   pragma Import (Java, GetPressedIcon, "getPressedIcon");
   pragma Import (Java, SetPressedIcon, "setPressedIcon");
   pragma Import (Java, GetSelectedIcon, "getSelectedIcon");
   pragma Import (Java, SetSelectedIcon, "setSelectedIcon");
   pragma Import (Java, GetRolloverIcon, "getRolloverIcon");
   pragma Import (Java, SetRolloverIcon, "setRolloverIcon");
   pragma Import (Java, GetRolloverSelectedIcon, "getRolloverSelectedIcon");
   pragma Import (Java, SetRolloverSelectedIcon, "setRolloverSelectedIcon");
   pragma Import (Java, GetDisabledIcon, "getDisabledIcon");
   pragma Import (Java, SetDisabledIcon, "setDisabledIcon");
   pragma Import (Java, GetDisabledSelectedIcon, "getDisabledSelectedIcon");
   pragma Import (Java, SetDisabledSelectedIcon, "setDisabledSelectedIcon");
   pragma Import (Java, GetVerticalAlignment, "getVerticalAlignment");
   pragma Import (Java, SetVerticalAlignment, "setVerticalAlignment");
   pragma Import (Java, GetHorizontalAlignment, "getHorizontalAlignment");
   pragma Import (Java, SetHorizontalAlignment, "setHorizontalAlignment");
   pragma Import (Java, GetVerticalTextPosition, "getVerticalTextPosition");
   pragma Import (Java, SetVerticalTextPosition, "setVerticalTextPosition");
   pragma Import (Java, GetHorizontalTextPosition, "getHorizontalTextPosition");
   pragma Import (Java, SetHorizontalTextPosition, "setHorizontalTextPosition");
   pragma Import (Java, GetIconTextGap, "getIconTextGap");
   pragma Import (Java, SetIconTextGap, "setIconTextGap");
   pragma Import (Java, CheckHorizontalKey, "checkHorizontalKey");
   pragma Import (Java, CheckVerticalKey, "checkVerticalKey");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, SetAction, "setAction");
   pragma Import (Java, GetAction, "getAction");
   pragma Import (Java, ConfigurePropertiesFromAction, "configurePropertiesFromAction");
   pragma Import (Java, ActionPropertyChanged, "actionPropertyChanged");
   pragma Import (Java, CreateActionPropertyChangeListener, "createActionPropertyChangeListener");
   pragma Import (Java, IsBorderPainted, "isBorderPainted");
   pragma Import (Java, SetBorderPainted, "setBorderPainted");
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, IsFocusPainted, "isFocusPainted");
   pragma Import (Java, SetFocusPainted, "setFocusPainted");
   pragma Import (Java, IsContentAreaFilled, "isContentAreaFilled");
   pragma Import (Java, SetContentAreaFilled, "setContentAreaFilled");
   pragma Import (Java, IsRolloverEnabled, "isRolloverEnabled");
   pragma Import (Java, SetRolloverEnabled, "setRolloverEnabled");
   pragma Import (Java, GetMnemonic, "getMnemonic");
   pragma Import (Java, SetMnemonic, "setMnemonic");
   pragma Import (Java, SetDisplayedMnemonicIndex, "setDisplayedMnemonicIndex");
   pragma Import (Java, GetDisplayedMnemonicIndex, "getDisplayedMnemonicIndex");
   pragma Import (Java, SetMultiClickThreshhold, "setMultiClickThreshhold");
   pragma Import (Java, GetMultiClickThreshhold, "getMultiClickThreshhold");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, FireActionPerformed, "fireActionPerformed");
   pragma Import (Java, FireItemStateChanged, "fireItemStateChanged");
   pragma Import (Java, CreateActionListener, "createActionListener");
   pragma Import (Java, CreateItemListener, "createItemListener");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, AddItemListener, "addItemListener");
   pragma Import (Java, RemoveItemListener, "removeItemListener");
   pragma Import (Java, GetItemListeners, "getItemListeners");
   pragma Import (Java, GetSelectedObjects, "getSelectedObjects");
   pragma Import (Java, Init, "init");
   pragma Import (Java, ImageUpdate, "imageUpdate");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, MODEL_CHANGED_PROPERTY, "MODEL_CHANGED_PROPERTY");
   pragma Import (Java, TEXT_CHANGED_PROPERTY, "TEXT_CHANGED_PROPERTY");
   pragma Import (Java, MNEMONIC_CHANGED_PROPERTY, "MNEMONIC_CHANGED_PROPERTY");
   pragma Import (Java, MARGIN_CHANGED_PROPERTY, "MARGIN_CHANGED_PROPERTY");
   pragma Import (Java, VERTICAL_ALIGNMENT_CHANGED_PROPERTY, "VERTICAL_ALIGNMENT_CHANGED_PROPERTY");
   pragma Import (Java, HORIZONTAL_ALIGNMENT_CHANGED_PROPERTY, "HORIZONTAL_ALIGNMENT_CHANGED_PROPERTY");
   pragma Import (Java, VERTICAL_TEXT_POSITION_CHANGED_PROPERTY, "VERTICAL_TEXT_POSITION_CHANGED_PROPERTY");
   pragma Import (Java, HORIZONTAL_TEXT_POSITION_CHANGED_PROPERTY, "HORIZONTAL_TEXT_POSITION_CHANGED_PROPERTY");
   pragma Import (Java, BORDER_PAINTED_CHANGED_PROPERTY, "BORDER_PAINTED_CHANGED_PROPERTY");
   pragma Import (Java, FOCUS_PAINTED_CHANGED_PROPERTY, "FOCUS_PAINTED_CHANGED_PROPERTY");
   pragma Import (Java, ROLLOVER_ENABLED_CHANGED_PROPERTY, "ROLLOVER_ENABLED_CHANGED_PROPERTY");
   pragma Import (Java, CONTENT_AREA_FILLED_CHANGED_PROPERTY, "CONTENT_AREA_FILLED_CHANGED_PROPERTY");
   pragma Import (Java, ICON_CHANGED_PROPERTY, "ICON_CHANGED_PROPERTY");
   pragma Import (Java, PRESSED_ICON_CHANGED_PROPERTY, "PRESSED_ICON_CHANGED_PROPERTY");
   pragma Import (Java, SELECTED_ICON_CHANGED_PROPERTY, "SELECTED_ICON_CHANGED_PROPERTY");
   pragma Import (Java, ROLLOVER_ICON_CHANGED_PROPERTY, "ROLLOVER_ICON_CHANGED_PROPERTY");
   pragma Import (Java, ROLLOVER_SELECTED_ICON_CHANGED_PROPERTY, "ROLLOVER_SELECTED_ICON_CHANGED_PROPERTY");
   pragma Import (Java, DISABLED_ICON_CHANGED_PROPERTY, "DISABLED_ICON_CHANGED_PROPERTY");
   pragma Import (Java, DISABLED_SELECTED_ICON_CHANGED_PROPERTY, "DISABLED_SELECTED_ICON_CHANGED_PROPERTY");

end Javax.Swing.AbstractButton;
pragma Import (Java, Javax.Swing.AbstractButton, "javax.swing.AbstractButton");
pragma Extensions_Allowed (Off);
