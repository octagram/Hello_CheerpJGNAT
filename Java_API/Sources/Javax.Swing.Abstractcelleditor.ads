pragma Extensions_Allowed (On);
limited with Java.Util.EventObject;
limited with Javax.Swing.Event.CellEditorListener;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.EventListenerList;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.CellEditor;

package Javax.Swing.AbstractCellEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            CellEditor_I : Javax.Swing.CellEditor.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

   end record;

   function New_AbstractCellEditor (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsCellEditable (This : access Typ;
                            P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                            return Java.Boolean;

   function ShouldSelectCell (This : access Typ;
                              P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                              return Java.Boolean;

   function StopCellEditing (This : access Typ)
                             return Java.Boolean;

   procedure CancelCellEditing (This : access Typ);

   procedure AddCellEditorListener (This : access Typ;
                                    P1_CellEditorListener : access Standard.Javax.Swing.Event.CellEditorListener.Typ'Class);

   procedure RemoveCellEditorListener (This : access Typ;
                                       P1_CellEditorListener : access Standard.Javax.Swing.Event.CellEditorListener.Typ'Class);

   function GetCellEditorListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireEditingStopped (This : access Typ);

   --  protected
   procedure FireEditingCanceled (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractCellEditor);
   pragma Import (Java, IsCellEditable, "isCellEditable");
   pragma Import (Java, ShouldSelectCell, "shouldSelectCell");
   pragma Import (Java, StopCellEditing, "stopCellEditing");
   pragma Import (Java, CancelCellEditing, "cancelCellEditing");
   pragma Import (Java, AddCellEditorListener, "addCellEditorListener");
   pragma Import (Java, RemoveCellEditorListener, "removeCellEditorListener");
   pragma Import (Java, GetCellEditorListeners, "getCellEditorListeners");
   pragma Import (Java, FireEditingStopped, "fireEditingStopped");
   pragma Import (Java, FireEditingCanceled, "fireEditingCanceled");

end Javax.Swing.AbstractCellEditor;
pragma Import (Java, Javax.Swing.AbstractCellEditor, "javax.swing.AbstractCellEditor");
pragma Extensions_Allowed (Off);
