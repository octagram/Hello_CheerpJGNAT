pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Event.ListDataListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.ListModel;

package Javax.Swing.AbstractListModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ListModel_I : Javax.Swing.ListModel.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   function New_AbstractListModel (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddListDataListener (This : access Typ;
                                  P1_ListDataListener : access Standard.Javax.Swing.Event.ListDataListener.Typ'Class);

   procedure RemoveListDataListener (This : access Typ;
                                     P1_ListDataListener : access Standard.Javax.Swing.Event.ListDataListener.Typ'Class);

   function GetListDataListeners (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireContentsChanged (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);

   --  protected
   procedure FireIntervalAdded (This : access Typ;
                                P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int);

   --  protected
   procedure FireIntervalRemoved (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractListModel);
   pragma Import (Java, AddListDataListener, "addListDataListener");
   pragma Import (Java, RemoveListDataListener, "removeListDataListener");
   pragma Import (Java, GetListDataListeners, "getListDataListeners");
   pragma Import (Java, FireContentsChanged, "fireContentsChanged");
   pragma Import (Java, FireIntervalAdded, "fireIntervalAdded");
   pragma Import (Java, FireIntervalRemoved, "fireIntervalRemoved");
   pragma Import (Java, GetListeners, "getListeners");

end Javax.Swing.AbstractListModel;
pragma Import (Java, Javax.Swing.AbstractListModel, "javax.swing.AbstractListModel");
pragma Extensions_Allowed (Off);
