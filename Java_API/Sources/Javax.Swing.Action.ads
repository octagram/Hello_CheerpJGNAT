pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
with Java.Awt.Event.ActionListener;
with Java.Lang.Object;

package Javax.Swing.Action is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ActionListener_I : Java.Awt.Event.ActionListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;

   procedure PutValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   function IsEnabled (This : access Typ)
                       return Java.Boolean is abstract;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT : constant access Java.Lang.String.Typ'Class;

   --  final
   NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   SHORT_DESCRIPTION : constant access Java.Lang.String.Typ'Class;

   --  final
   LONG_DESCRIPTION : constant access Java.Lang.String.Typ'Class;

   --  final
   SMALL_ICON : constant access Java.Lang.String.Typ'Class;

   --  final
   ACTION_COMMAND_KEY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCELERATOR_KEY : constant access Java.Lang.String.Typ'Class;

   --  final
   MNEMONIC_KEY : constant access Java.Lang.String.Typ'Class;

   --  final
   SELECTED_KEY : constant access Java.Lang.String.Typ'Class;

   --  final
   DISPLAYED_MNEMONIC_INDEX_KEY : constant access Java.Lang.String.Typ'Class;

   --  final
   LARGE_ICON_KEY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, PutValue, "putValue");
   pragma Export (Java, SetEnabled, "setEnabled");
   pragma Export (Java, IsEnabled, "isEnabled");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, DEFAULT, "DEFAULT");
   pragma Import (Java, NAME, "NAME");
   pragma Import (Java, SHORT_DESCRIPTION, "SHORT_DESCRIPTION");
   pragma Import (Java, LONG_DESCRIPTION, "LONG_DESCRIPTION");
   pragma Import (Java, SMALL_ICON, "SMALL_ICON");
   pragma Import (Java, ACTION_COMMAND_KEY, "ACTION_COMMAND_KEY");
   pragma Import (Java, ACCELERATOR_KEY, "ACCELERATOR_KEY");
   pragma Import (Java, MNEMONIC_KEY, "MNEMONIC_KEY");
   pragma Import (Java, SELECTED_KEY, "SELECTED_KEY");
   pragma Import (Java, DISPLAYED_MNEMONIC_INDEX_KEY, "DISPLAYED_MNEMONIC_INDEX_KEY");
   pragma Import (Java, LARGE_ICON_KEY, "LARGE_ICON_KEY");

end Javax.Swing.Action;
pragma Import (Java, Javax.Swing.Action, "javax.swing.Action");
pragma Extensions_Allowed (Off);
