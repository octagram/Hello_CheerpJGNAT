pragma Extensions_Allowed (On);
limited with Javax.Swing.Action;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.ActionMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ActionMap (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_ActionMap : access Standard.Javax.Swing.ActionMap.Typ'Class);

   function GetParent (This : access Typ)
                       return access Javax.Swing.ActionMap.Typ'Class;

   procedure Put (This : access Typ;
                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P2_Action : access Standard.Javax.Swing.Action.Typ'Class);

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Javax.Swing.Action.Typ'Class;

   procedure Remove (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Clear (This : access Typ);

   function Keys (This : access Typ)
                  return Standard.Java.Lang.Object.Ref;

   function Size (This : access Typ)
                  return Java.Int;

   function AllKeys (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ActionMap);
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Keys, "keys");
   pragma Import (Java, Size, "size");
   pragma Import (Java, AllKeys, "allKeys");

end Javax.Swing.ActionMap;
pragma Import (Java, Javax.Swing.ActionMap, "javax.swing.ActionMap");
pragma Extensions_Allowed (Off);
