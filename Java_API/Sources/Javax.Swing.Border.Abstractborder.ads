pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.Rectangle;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.Border;

package Javax.Swing.Border.AbstractBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AbstractBorder (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;

   function GetInteriorRectangle (This : access Typ;
                                  P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int)
                                  return access Java.Awt.Rectangle.Typ'Class;

   function GetInteriorRectangle (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                  P2_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int)
                                  return access Java.Awt.Rectangle.Typ'Class;

   function GetBaseline (This : access Typ;
                         P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractBorder);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");
   pragma Import (Java, GetInteriorRectangle, "getInteriorRectangle");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");

end Javax.Swing.Border.AbstractBorder;
pragma Import (Java, Javax.Swing.Border.AbstractBorder, "javax.swing.border.AbstractBorder");
pragma Extensions_Allowed (Off);
