pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.AbstractBorder;
with Javax.Swing.Border.Border;

package Javax.Swing.Border.EtchedBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref)
    is new Javax.Swing.Border.AbstractBorder.Typ(Serializable_I,
                                                 Border_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      EtchType : Java.Int;
      pragma Import (Java, EtchType, "etchType");

      --  protected
      Highlight : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Highlight, "highlight");

      --  protected
      Shadow : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Shadow, "shadow");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EtchedBorder (This : Ref := null)
                              return Ref;

   function New_EtchedBorder (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_EtchedBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class;
                              P2_Color : access Standard.Java.Awt.Color.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_EtchedBorder (P1_Int : Java.Int;
                              P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                              P3_Color : access Standard.Java.Awt.Color.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;

   function GetEtchType (This : access Typ)
                         return Java.Int;

   function GetHighlightColor (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return access Java.Awt.Color.Typ'Class;

   function GetHighlightColor (This : access Typ)
                               return access Java.Awt.Color.Typ'Class;

   function GetShadowColor (This : access Typ;
                            P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return access Java.Awt.Color.Typ'Class;

   function GetShadowColor (This : access Typ)
                            return access Java.Awt.Color.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RAISED : constant Java.Int;

   --  final
   LOWERED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EtchedBorder);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");
   pragma Import (Java, GetEtchType, "getEtchType");
   pragma Import (Java, GetHighlightColor, "getHighlightColor");
   pragma Import (Java, GetShadowColor, "getShadowColor");
   pragma Import (Java, RAISED, "RAISED");
   pragma Import (Java, LOWERED, "LOWERED");

end Javax.Swing.Border.EtchedBorder;
pragma Import (Java, Javax.Swing.Border.EtchedBorder, "javax.swing.border.EtchedBorder");
pragma Extensions_Allowed (Off);
