pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.AbstractBorder;
with Javax.Swing.Border.Border;

package Javax.Swing.Border.LineBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref)
    is new Javax.Swing.Border.AbstractBorder.Typ(Serializable_I,
                                                 Border_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Thickness : Java.Int;
      pragma Import (Java, Thickness, "thickness");

      --  protected
      LineColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, LineColor, "lineColor");

      --  protected
      RoundedCorners : Java.Boolean;
      pragma Import (Java, RoundedCorners, "roundedCorners");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateBlackLineBorder return access Javax.Swing.Border.Border.Typ'Class;

   function CreateGrayLineBorder return access Javax.Swing.Border.Border.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LineBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_LineBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_LineBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetLineColor (This : access Typ)
                          return access Java.Awt.Color.Typ'Class;

   function GetThickness (This : access Typ)
                          return Java.Int;

   function GetRoundedCorners (This : access Typ)
                               return Java.Boolean;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateBlackLineBorder, "createBlackLineBorder");
   pragma Import (Java, CreateGrayLineBorder, "createGrayLineBorder");
   pragma Java_Constructor (New_LineBorder);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, GetLineColor, "getLineColor");
   pragma Import (Java, GetThickness, "getThickness");
   pragma Import (Java, GetRoundedCorners, "getRoundedCorners");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");

end Javax.Swing.Border.LineBorder;
pragma Import (Java, Javax.Swing.Border.LineBorder, "javax.swing.border.LineBorder");
pragma Extensions_Allowed (Off);
