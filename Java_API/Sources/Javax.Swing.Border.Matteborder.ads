pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Javax.Swing.Icon;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.Border;
with Javax.Swing.Border.EmptyBorder;

package Javax.Swing.Border.MatteBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref)
    is new Javax.Swing.Border.EmptyBorder.Typ(Serializable_I,
                                              Border_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Color : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Color, "color");

      --  protected
      TileIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, TileIcon, "tileIcon");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MatteBorder (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Color : access Standard.Java.Awt.Color.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_MatteBorder (P1_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                             P2_Color : access Standard.Java.Awt.Color.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_MatteBorder (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_MatteBorder (P1_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                             P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_MatteBorder (P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetBorderInsets (This : access Typ)
                             return access Java.Awt.Insets.Typ'Class;

   function GetMatteColor (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetTileIcon (This : access Typ)
                         return access Javax.Swing.Icon.Typ'Class;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MatteBorder);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, GetMatteColor, "getMatteColor");
   pragma Import (Java, GetTileIcon, "getTileIcon");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");

end Javax.Swing.Border.MatteBorder;
pragma Import (Java, Javax.Swing.Border.MatteBorder, "javax.swing.border.MatteBorder");
pragma Extensions_Allowed (Off);
