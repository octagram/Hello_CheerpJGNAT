pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.BevelBorder;
with Javax.Swing.Border.Border;

package Javax.Swing.Border.SoftBevelBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref)
    is new Javax.Swing.Border.BevelBorder.Typ(Serializable_I,
                                              Border_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SoftBevelBorder (P1_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_SoftBevelBorder (P1_Int : Java.Int;
                                 P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                                 P3_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_SoftBevelBorder (P1_Int : Java.Int;
                                 P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                                 P3_Color : access Standard.Java.Awt.Color.Typ'Class;
                                 P4_Color : access Standard.Java.Awt.Color.Typ'Class;
                                 P5_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SoftBevelBorder);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");

end Javax.Swing.Border.SoftBevelBorder;
pragma Import (Java, Javax.Swing.Border.SoftBevelBorder, "javax.swing.border.SoftBevelBorder");
pragma Extensions_Allowed (Off);
