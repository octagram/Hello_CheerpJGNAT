pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.AbstractBorder;
with Javax.Swing.Border.Border;

package Javax.Swing.Border.TitledBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref)
    is new Javax.Swing.Border.AbstractBorder.Typ(Serializable_I,
                                                 Border_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Title : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Title, "title");

      --  protected
      Border : access Javax.Swing.Border.Border.Typ'Class;
      pragma Import (Java, Border, "border");

      --  protected
      TitlePosition : Java.Int;
      pragma Import (Java, TitlePosition, "titlePosition");

      --  protected
      TitleJustification : Java.Int;
      pragma Import (Java, TitleJustification, "titleJustification");

      --  protected
      TitleFont : access Java.Awt.Font.Typ'Class;
      pragma Import (Java, TitleFont, "titleFont");

      --  protected
      TitleColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, TitleColor, "titleColor");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TitledBorder (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_TitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_TitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_TitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_TitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Font : access Standard.Java.Awt.Font.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_TitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Font : access Standard.Java.Awt.Font.Typ'Class;
                              P6_Color : access Standard.Java.Awt.Color.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;

   function GetTitle (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetBorder (This : access Typ)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetTitlePosition (This : access Typ)
                              return Java.Int;

   function GetTitleJustification (This : access Typ)
                                   return Java.Int;

   function GetTitleFont (This : access Typ)
                          return access Java.Awt.Font.Typ'Class;

   function GetTitleColor (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetTitle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetBorder (This : access Typ;
                        P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class);

   procedure SetTitlePosition (This : access Typ;
                               P1_Int : Java.Int);

   procedure SetTitleJustification (This : access Typ;
                                    P1_Int : Java.Int);

   procedure SetTitleFont (This : access Typ;
                           P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   procedure SetTitleColor (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetMinimumSize (This : access Typ;
                            P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetBaseline (This : access Typ;
                         P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  protected
   function GetFont (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_POSITION : constant Java.Int;

   --  final
   ABOVE_TOP : constant Java.Int;

   --  final
   TOP : constant Java.Int;

   --  final
   BELOW_TOP : constant Java.Int;

   --  final
   ABOVE_BOTTOM : constant Java.Int;

   --  final
   BOTTOM : constant Java.Int;

   --  final
   BELOW_BOTTOM : constant Java.Int;

   --  final
   DEFAULT_JUSTIFICATION : constant Java.Int;

   --  final
   LEFT : constant Java.Int;

   --  final
   CENTER : constant Java.Int;

   --  final
   RIGHT : constant Java.Int;

   --  final
   LEADING : constant Java.Int;

   --  final
   TRAILING : constant Java.Int;

   --  protected  final
   EDGE_SPACING : constant Java.Int;

   --  protected  final
   TEXT_SPACING : constant Java.Int;

   --  protected  final
   TEXT_INSET_H : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TitledBorder);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");
   pragma Import (Java, GetTitle, "getTitle");
   pragma Import (Java, GetBorder, "getBorder");
   pragma Import (Java, GetTitlePosition, "getTitlePosition");
   pragma Import (Java, GetTitleJustification, "getTitleJustification");
   pragma Import (Java, GetTitleFont, "getTitleFont");
   pragma Import (Java, GetTitleColor, "getTitleColor");
   pragma Import (Java, SetTitle, "setTitle");
   pragma Import (Java, SetBorder, "setBorder");
   pragma Import (Java, SetTitlePosition, "setTitlePosition");
   pragma Import (Java, SetTitleJustification, "setTitleJustification");
   pragma Import (Java, SetTitleFont, "setTitleFont");
   pragma Import (Java, SetTitleColor, "setTitleColor");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, DEFAULT_POSITION, "DEFAULT_POSITION");
   pragma Import (Java, ABOVE_TOP, "ABOVE_TOP");
   pragma Import (Java, TOP, "TOP");
   pragma Import (Java, BELOW_TOP, "BELOW_TOP");
   pragma Import (Java, ABOVE_BOTTOM, "ABOVE_BOTTOM");
   pragma Import (Java, BOTTOM, "BOTTOM");
   pragma Import (Java, BELOW_BOTTOM, "BELOW_BOTTOM");
   pragma Import (Java, DEFAULT_JUSTIFICATION, "DEFAULT_JUSTIFICATION");
   pragma Import (Java, LEFT, "LEFT");
   pragma Import (Java, CENTER, "CENTER");
   pragma Import (Java, RIGHT, "RIGHT");
   pragma Import (Java, LEADING, "LEADING");
   pragma Import (Java, TRAILING, "TRAILING");
   pragma Import (Java, EDGE_SPACING, "EDGE_SPACING");
   pragma Import (Java, TEXT_SPACING, "TEXT_SPACING");
   pragma Import (Java, TEXT_INSET_H, "TEXT_INSET_H");

end Javax.Swing.Border.TitledBorder;
pragma Import (Java, Javax.Swing.Border.TitledBorder, "javax.swing.border.TitledBorder");
pragma Extensions_Allowed (Off);
