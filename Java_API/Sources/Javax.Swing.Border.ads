pragma Extensions_Allowed (On);
package Javax.Swing.Border is
   pragma Preelaborate;
end Javax.Swing.Border;
pragma Import (Java, Javax.Swing.Border, "javax.swing.border");
pragma Extensions_Allowed (Off);
