pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Lang.String;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.Border.CompoundBorder;
limited with Javax.Swing.Border.MatteBorder;
limited with Javax.Swing.Border.TitledBorder;
limited with Javax.Swing.Icon;
with Java.Lang.Object;

package Javax.Swing.BorderFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateLineBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class)
                              return access Javax.Swing.Border.Border.Typ'Class;

   function CreateLineBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class;
                              P2_Int : Java.Int)
                              return access Javax.Swing.Border.Border.Typ'Class;

   function CreateRaisedBevelBorder return access Javax.Swing.Border.Border.Typ'Class;

   function CreateLoweredBevelBorder return access Javax.Swing.Border.Border.Typ'Class;

   function CreateBevelBorder (P1_Int : Java.Int)
                               return access Javax.Swing.Border.Border.Typ'Class;

   function CreateBevelBorder (P1_Int : Java.Int;
                               P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P3_Color : access Standard.Java.Awt.Color.Typ'Class)
                               return access Javax.Swing.Border.Border.Typ'Class;

   function CreateBevelBorder (P1_Int : Java.Int;
                               P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P3_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P4_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P5_Color : access Standard.Java.Awt.Color.Typ'Class)
                               return access Javax.Swing.Border.Border.Typ'Class;

   function CreateEtchedBorder return access Javax.Swing.Border.Border.Typ'Class;

   function CreateEtchedBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class;
                                P2_Color : access Standard.Java.Awt.Color.Typ'Class)
                                return access Javax.Swing.Border.Border.Typ'Class;

   function CreateEtchedBorder (P1_Int : Java.Int)
                                return access Javax.Swing.Border.Border.Typ'Class;

   function CreateEtchedBorder (P1_Int : Java.Int;
                                P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                                P3_Color : access Standard.Java.Awt.Color.Typ'Class)
                                return access Javax.Swing.Border.Border.Typ'Class;

   function CreateTitledBorder (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Javax.Swing.Border.TitledBorder.Typ'Class;

   function CreateTitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class)
                                return access Javax.Swing.Border.TitledBorder.Typ'Class;

   function CreateTitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Javax.Swing.Border.TitledBorder.Typ'Class;

   function CreateTitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int)
                                return access Javax.Swing.Border.TitledBorder.Typ'Class;

   function CreateTitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Font : access Standard.Java.Awt.Font.Typ'Class)
                                return access Javax.Swing.Border.TitledBorder.Typ'Class;

   function CreateTitledBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Font : access Standard.Java.Awt.Font.Typ'Class;
                                P6_Color : access Standard.Java.Awt.Color.Typ'Class)
                                return access Javax.Swing.Border.TitledBorder.Typ'Class;

   function CreateEmptyBorder return access Javax.Swing.Border.Border.Typ'Class;

   function CreateEmptyBorder (P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int)
                               return access Javax.Swing.Border.Border.Typ'Class;

   function CreateCompoundBorder return access Javax.Swing.Border.CompoundBorder.Typ'Class;

   function CreateCompoundBorder (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                                  P2_Border : access Standard.Javax.Swing.Border.Border.Typ'Class)
                                  return access Javax.Swing.Border.CompoundBorder.Typ'Class;

   function CreateMatteBorder (P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int;
                               P5_Color : access Standard.Java.Awt.Color.Typ'Class)
                               return access Javax.Swing.Border.MatteBorder.Typ'Class;

   function CreateMatteBorder (P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int;
                               P5_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                               return access Javax.Swing.Border.MatteBorder.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateLineBorder, "createLineBorder");
   pragma Import (Java, CreateRaisedBevelBorder, "createRaisedBevelBorder");
   pragma Import (Java, CreateLoweredBevelBorder, "createLoweredBevelBorder");
   pragma Import (Java, CreateBevelBorder, "createBevelBorder");
   pragma Import (Java, CreateEtchedBorder, "createEtchedBorder");
   pragma Import (Java, CreateTitledBorder, "createTitledBorder");
   pragma Import (Java, CreateEmptyBorder, "createEmptyBorder");
   pragma Import (Java, CreateCompoundBorder, "createCompoundBorder");
   pragma Import (Java, CreateMatteBorder, "createMatteBorder");

end Javax.Swing.BorderFactory;
pragma Import (Java, Javax.Swing.BorderFactory, "javax.swing.BorderFactory");
pragma Extensions_Allowed (Off);
