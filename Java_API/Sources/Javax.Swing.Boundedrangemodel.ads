pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ChangeListener;
with Java.Lang.Object;

package Javax.Swing.BoundedRangeModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMinimum (This : access Typ)
                        return Java.Int is abstract;

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   function GetMaximum (This : access Typ)
                        return Java.Int is abstract;

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   function GetValue (This : access Typ)
                      return Java.Int is abstract;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean) is abstract;

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean is abstract;

   function GetExtent (This : access Typ)
                       return Java.Int is abstract;

   procedure SetExtent (This : access Typ;
                        P1_Int : Java.Int) is abstract;

   procedure SetRangeProperties (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Boolean : Java.Boolean) is abstract;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMinimum, "getMinimum");
   pragma Export (Java, SetMinimum, "setMinimum");
   pragma Export (Java, GetMaximum, "getMaximum");
   pragma Export (Java, SetMaximum, "setMaximum");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Export (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Export (Java, GetExtent, "getExtent");
   pragma Export (Java, SetExtent, "setExtent");
   pragma Export (Java, SetRangeProperties, "setRangeProperties");
   pragma Export (Java, AddChangeListener, "addChangeListener");
   pragma Export (Java, RemoveChangeListener, "removeChangeListener");

end Javax.Swing.BoundedRangeModel;
pragma Import (Java, Javax.Swing.BoundedRangeModel, "javax.swing.BoundedRangeModel");
pragma Extensions_Allowed (Off);
