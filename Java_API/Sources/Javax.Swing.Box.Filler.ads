pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.Box.Filler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Filler (P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class;
                        P2_Dimension : access Standard.Java.Awt.Dimension.Typ'Class;
                        P3_Dimension : access Standard.Java.Awt.Dimension.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ChangeShape (This : access Typ;
                          P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class;
                          P2_Dimension : access Standard.Java.Awt.Dimension.Typ'Class;
                          P3_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   --  protected
   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Filler);
   pragma Import (Java, ChangeShape, "changeShape");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.Box.Filler;
pragma Import (Java, Javax.Swing.Box.Filler, "javax.swing.Box$Filler");
pragma Extensions_Allowed (Off);
