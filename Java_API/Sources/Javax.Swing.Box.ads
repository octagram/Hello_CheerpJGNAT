pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.LayoutManager;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.Box is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Box (P1_Int : Java.Int; 
                     This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateHorizontalBox return access Javax.Swing.Box.Typ'Class;

   function CreateVerticalBox return access Javax.Swing.Box.Typ'Class;

   function CreateRigidArea (P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class)
                             return access Java.Awt.Component.Typ'Class;

   function CreateHorizontalStrut (P1_Int : Java.Int)
                                   return access Java.Awt.Component.Typ'Class;

   function CreateVerticalStrut (P1_Int : Java.Int)
                                 return access Java.Awt.Component.Typ'Class;

   function CreateGlue return access Java.Awt.Component.Typ'Class;

   function CreateHorizontalGlue return access Java.Awt.Component.Typ'Class;

   function CreateVerticalGlue return access Java.Awt.Component.Typ'Class;

   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   --  protected
   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Box);
   pragma Import (Java, CreateHorizontalBox, "createHorizontalBox");
   pragma Import (Java, CreateVerticalBox, "createVerticalBox");
   pragma Import (Java, CreateRigidArea, "createRigidArea");
   pragma Import (Java, CreateHorizontalStrut, "createHorizontalStrut");
   pragma Import (Java, CreateVerticalStrut, "createVerticalStrut");
   pragma Import (Java, CreateGlue, "createGlue");
   pragma Import (Java, CreateHorizontalGlue, "createHorizontalGlue");
   pragma Import (Java, CreateVerticalGlue, "createVerticalGlue");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.Box;
pragma Import (Java, Javax.Swing.Box, "javax.swing.Box");
pragma Extensions_Allowed (Off);
