pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
with Java.Awt.LayoutManager2;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.BoxLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BoxLayout (P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                           P2_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetTarget (This : access Typ)
                       return access Java.Awt.Container.Typ'Class;

   --  final
   function GetAxis (This : access Typ)
                     return Java.Int;

   --  synchronized
   procedure InvalidateLayout (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function MaximumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   --  synchronized
   function GetLayoutAlignmentX (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   --  synchronized
   function GetLayoutAlignmentY (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   X_AXIS : constant Java.Int;

   --  final
   Y_AXIS : constant Java.Int;

   --  final
   LINE_AXIS : constant Java.Int;

   --  final
   PAGE_AXIS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BoxLayout);
   pragma Import (Java, GetTarget, "getTarget");
   pragma Import (Java, GetAxis, "getAxis");
   pragma Import (Java, InvalidateLayout, "invalidateLayout");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, MaximumLayoutSize, "maximumLayoutSize");
   pragma Import (Java, GetLayoutAlignmentX, "getLayoutAlignmentX");
   pragma Import (Java, GetLayoutAlignmentY, "getLayoutAlignmentY");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, X_AXIS, "X_AXIS");
   pragma Import (Java, Y_AXIS, "Y_AXIS");
   pragma Import (Java, LINE_AXIS, "LINE_AXIS");
   pragma Import (Java, PAGE_AXIS, "PAGE_AXIS");

end Javax.Swing.BoxLayout;
pragma Import (Java, Javax.Swing.BoxLayout, "javax.swing.BoxLayout");
pragma Extensions_Allowed (Off);
