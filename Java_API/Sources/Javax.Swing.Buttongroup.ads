pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
limited with Java.Util.Vector;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.ButtonModel;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.ButtonGroup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buttons : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Buttons, "buttons");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ButtonGroup (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Add (This : access Typ;
                  P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   procedure ClearSelection (This : access Typ);

   function GetElements (This : access Typ)
                         return access Java.Util.Enumeration.Typ'Class;

   function GetSelection (This : access Typ)
                          return access Javax.Swing.ButtonModel.Typ'Class;

   procedure SetSelected (This : access Typ;
                          P1_ButtonModel : access Standard.Javax.Swing.ButtonModel.Typ'Class;
                          P2_Boolean : Java.Boolean);

   function IsSelected (This : access Typ;
                        P1_ButtonModel : access Standard.Javax.Swing.ButtonModel.Typ'Class)
                        return Java.Boolean;

   function GetButtonCount (This : access Typ)
                            return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ButtonGroup);
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ClearSelection, "clearSelection");
   pragma Import (Java, GetElements, "getElements");
   pragma Import (Java, GetSelection, "getSelection");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, GetButtonCount, "getButtonCount");

end Javax.Swing.ButtonGroup;
pragma Import (Java, Javax.Swing.ButtonGroup, "javax.swing.ButtonGroup");
pragma Extensions_Allowed (Off);
