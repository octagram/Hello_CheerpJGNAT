pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Event.ItemListener;
limited with Java.Lang.String;
limited with Javax.Swing.ButtonGroup;
limited with Javax.Swing.Event.ChangeListener;
with Java.Awt.ItemSelectable;
with Java.Lang.Object;

package Javax.Swing.ButtonModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ItemSelectable_I : Java.Awt.ItemSelectable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsArmed (This : access Typ)
                     return Java.Boolean is abstract;

   function IsSelected (This : access Typ)
                        return Java.Boolean is abstract;

   function IsEnabled (This : access Typ)
                       return Java.Boolean is abstract;

   function IsPressed (This : access Typ)
                       return Java.Boolean is abstract;

   function IsRollover (This : access Typ)
                        return Java.Boolean is abstract;

   procedure SetArmed (This : access Typ;
                       P1_Boolean : Java.Boolean) is abstract;

   procedure SetSelected (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   procedure SetPressed (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   procedure SetRollover (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   procedure SetMnemonic (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   function GetMnemonic (This : access Typ)
                         return Java.Int is abstract;

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetGroup (This : access Typ;
                       P1_ButtonGroup : access Standard.Javax.Swing.ButtonGroup.Typ'Class) is abstract;

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class) is abstract;

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class) is abstract;

   procedure AddItemListener (This : access Typ;
                              P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class) is abstract;

   procedure RemoveItemListener (This : access Typ;
                                 P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class) is abstract;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsArmed, "isArmed");
   pragma Export (Java, IsSelected, "isSelected");
   pragma Export (Java, IsEnabled, "isEnabled");
   pragma Export (Java, IsPressed, "isPressed");
   pragma Export (Java, IsRollover, "isRollover");
   pragma Export (Java, SetArmed, "setArmed");
   pragma Export (Java, SetSelected, "setSelected");
   pragma Export (Java, SetEnabled, "setEnabled");
   pragma Export (Java, SetPressed, "setPressed");
   pragma Export (Java, SetRollover, "setRollover");
   pragma Export (Java, SetMnemonic, "setMnemonic");
   pragma Export (Java, GetMnemonic, "getMnemonic");
   pragma Export (Java, SetActionCommand, "setActionCommand");
   pragma Export (Java, GetActionCommand, "getActionCommand");
   pragma Export (Java, SetGroup, "setGroup");
   pragma Export (Java, AddActionListener, "addActionListener");
   pragma Export (Java, RemoveActionListener, "removeActionListener");
   pragma Export (Java, AddItemListener, "addItemListener");
   pragma Export (Java, RemoveItemListener, "removeItemListener");
   pragma Export (Java, AddChangeListener, "addChangeListener");
   pragma Export (Java, RemoveChangeListener, "removeChangeListener");

end Javax.Swing.ButtonModel;
pragma Import (Java, Javax.Swing.ButtonModel, "javax.swing.ButtonModel");
pragma Extensions_Allowed (Off);
