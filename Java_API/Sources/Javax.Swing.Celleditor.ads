pragma Extensions_Allowed (On);
limited with Java.Util.EventObject;
limited with Javax.Swing.Event.CellEditorListener;
with Java.Lang.Object;

package Javax.Swing.CellEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCellEditorValue (This : access Typ)
                                return access Java.Lang.Object.Typ'Class is abstract;

   function IsCellEditable (This : access Typ;
                            P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                            return Java.Boolean is abstract;

   function ShouldSelectCell (This : access Typ;
                              P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                              return Java.Boolean is abstract;

   function StopCellEditing (This : access Typ)
                             return Java.Boolean is abstract;

   procedure CancelCellEditing (This : access Typ) is abstract;

   procedure AddCellEditorListener (This : access Typ;
                                    P1_CellEditorListener : access Standard.Javax.Swing.Event.CellEditorListener.Typ'Class) is abstract;

   procedure RemoveCellEditorListener (This : access Typ;
                                       P1_CellEditorListener : access Standard.Javax.Swing.Event.CellEditorListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCellEditorValue, "getCellEditorValue");
   pragma Export (Java, IsCellEditable, "isCellEditable");
   pragma Export (Java, ShouldSelectCell, "shouldSelectCell");
   pragma Export (Java, StopCellEditing, "stopCellEditing");
   pragma Export (Java, CancelCellEditing, "cancelCellEditing");
   pragma Export (Java, AddCellEditorListener, "addCellEditorListener");
   pragma Export (Java, RemoveCellEditorListener, "removeCellEditorListener");

end Javax.Swing.CellEditor;
pragma Import (Java, Javax.Swing.CellEditor, "javax.swing.CellEditor");
pragma Extensions_Allowed (Off);
