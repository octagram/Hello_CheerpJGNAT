pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Container;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Javax.Swing.CellRendererPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Awt.Container.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      AccessibleContext : access Javax.Accessibility.AccessibleContext.Typ'Class;
      pragma Import (Java, AccessibleContext, "accessibleContext");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CellRendererPane (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Invalidate (This : access Typ);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P3_Container : access Standard.Java.Awt.Container.Typ'Class;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int;
                             P8_Boolean : Java.Boolean);

   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P3_Container : access Standard.Java.Awt.Container.Typ'Class;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int);

   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P3_Container : access Standard.Java.Awt.Container.Typ'Class;
                             P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CellRendererPane);
   pragma Import (Java, Invalidate, "invalidate");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Update, "update");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.CellRendererPane;
pragma Import (Java, Javax.Swing.CellRendererPane, "javax.swing.CellRendererPane");
pragma Extensions_Allowed (Off);
