pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Graphics;
limited with Java.Lang.String;
limited with Javax.Swing.Colorchooser.ColorSelectionModel;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JColorChooser;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JPanel;

package Javax.Swing.Colorchooser.AbstractColorChooserPanel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is abstract new Javax.Swing.JPanel.Typ(MenuContainer_I,
                                           ImageObserver_I,
                                           Serializable_I,
                                           Accessible_I)
      with null record;

   function New_AbstractColorChooserPanel (This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure UpdateChooser (This : access Typ) is abstract;

   --  protected
   procedure BuildChooser (This : access Typ) is abstract;

   function GetDisplayName (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetMnemonic (This : access Typ)
                         return Java.Int;

   function GetDisplayedMnemonicIndex (This : access Typ)
                                       return Java.Int;

   function GetSmallDisplayIcon (This : access Typ)
                                 return access Javax.Swing.Icon.Typ'Class is abstract;

   function GetLargeDisplayIcon (This : access Typ)
                                 return access Javax.Swing.Icon.Typ'Class is abstract;

   procedure InstallChooserPanel (This : access Typ;
                                  P1_JColorChooser : access Standard.Javax.Swing.JColorChooser.Typ'Class);

   procedure UninstallChooserPanel (This : access Typ;
                                    P1_JColorChooser : access Standard.Javax.Swing.JColorChooser.Typ'Class);

   function GetColorSelectionModel (This : access Typ)
                                    return access Javax.Swing.Colorchooser.ColorSelectionModel.Typ'Class;

   --  protected
   function GetColorFromModel (This : access Typ)
                               return access Java.Awt.Color.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractColorChooserPanel);
   pragma Export (Java, UpdateChooser, "updateChooser");
   pragma Export (Java, BuildChooser, "buildChooser");
   pragma Export (Java, GetDisplayName, "getDisplayName");
   pragma Export (Java, GetMnemonic, "getMnemonic");
   pragma Export (Java, GetDisplayedMnemonicIndex, "getDisplayedMnemonicIndex");
   pragma Export (Java, GetSmallDisplayIcon, "getSmallDisplayIcon");
   pragma Export (Java, GetLargeDisplayIcon, "getLargeDisplayIcon");
   pragma Export (Java, InstallChooserPanel, "installChooserPanel");
   pragma Export (Java, UninstallChooserPanel, "uninstallChooserPanel");
   pragma Export (Java, GetColorSelectionModel, "getColorSelectionModel");
   pragma Export (Java, GetColorFromModel, "getColorFromModel");
   pragma Export (Java, Paint, "paint");

end Javax.Swing.Colorchooser.AbstractColorChooserPanel;
pragma Import (Java, Javax.Swing.Colorchooser.AbstractColorChooserPanel, "javax.swing.colorchooser.AbstractColorChooserPanel");
pragma Extensions_Allowed (Off);
