pragma Extensions_Allowed (On);
limited with Javax.Swing.Colorchooser.AbstractColorChooserPanel;
limited with Javax.Swing.JComponent;
with Java.Lang.Object;

package Javax.Swing.Colorchooser.ColorChooserComponentFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultChooserPanels return Standard.Java.Lang.Object.Ref;

   function GetPreviewPanel return access Javax.Swing.JComponent.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDefaultChooserPanels, "getDefaultChooserPanels");
   pragma Import (Java, GetPreviewPanel, "getPreviewPanel");

end Javax.Swing.Colorchooser.ColorChooserComponentFactory;
pragma Import (Java, Javax.Swing.Colorchooser.ColorChooserComponentFactory, "javax.swing.colorchooser.ColorChooserComponentFactory");
pragma Extensions_Allowed (Off);
