pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Javax.Swing.Event.ChangeListener;
with Java.Lang.Object;

package Javax.Swing.Colorchooser.ColorSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSelectedColor (This : access Typ)
                              return access Java.Awt.Color.Typ'Class is abstract;

   procedure SetSelectedColor (This : access Typ;
                               P1_Color : access Standard.Java.Awt.Color.Typ'Class) is abstract;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSelectedColor, "getSelectedColor");
   pragma Export (Java, SetSelectedColor, "setSelectedColor");
   pragma Export (Java, AddChangeListener, "addChangeListener");
   pragma Export (Java, RemoveChangeListener, "removeChangeListener");

end Javax.Swing.Colorchooser.ColorSelectionModel;
pragma Import (Java, Javax.Swing.Colorchooser.ColorSelectionModel, "javax.swing.colorchooser.ColorSelectionModel");
pragma Extensions_Allowed (Off);
