pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.EventListenerList;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Colorchooser.ColorSelectionModel;

package Javax.Swing.Colorchooser.DefaultColorSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ColorSelectionModel_I : Javax.Swing.Colorchooser.ColorSelectionModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultColorSelectionModel (This : Ref := null)
                                            return Ref;

   function New_DefaultColorSelectionModel (P1_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSelectedColor (This : access Typ)
                              return access Java.Awt.Color.Typ'Class;

   procedure SetSelectedColor (This : access Typ;
                               P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultColorSelectionModel);
   pragma Import (Java, GetSelectedColor, "getSelectedColor");
   pragma Import (Java, SetSelectedColor, "setSelectedColor");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");

end Javax.Swing.Colorchooser.DefaultColorSelectionModel;
pragma Import (Java, Javax.Swing.Colorchooser.DefaultColorSelectionModel, "javax.swing.colorchooser.DefaultColorSelectionModel");
pragma Extensions_Allowed (Off);
