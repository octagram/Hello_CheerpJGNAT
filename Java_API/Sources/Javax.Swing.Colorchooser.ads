pragma Extensions_Allowed (On);
package Javax.Swing.Colorchooser is
   pragma Preelaborate;
end Javax.Swing.Colorchooser;
pragma Import (Java, Javax.Swing.Colorchooser, "javax.swing.colorchooser");
pragma Extensions_Allowed (Off);
