pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.ActionListener;
with Java.Lang.Object;

package Javax.Swing.ComboBoxEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEditorComponent (This : access Typ)
                                return access Java.Awt.Component.Typ'Class is abstract;

   procedure SetItem (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetItem (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   procedure SelectAll (This : access Typ) is abstract;

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class) is abstract;

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetEditorComponent, "getEditorComponent");
   pragma Export (Java, SetItem, "setItem");
   pragma Export (Java, GetItem, "getItem");
   pragma Export (Java, SelectAll, "selectAll");
   pragma Export (Java, AddActionListener, "addActionListener");
   pragma Export (Java, RemoveActionListener, "removeActionListener");

end Javax.Swing.ComboBoxEditor;
pragma Import (Java, Javax.Swing.ComboBoxEditor, "javax.swing.ComboBoxEditor");
pragma Extensions_Allowed (Off);
