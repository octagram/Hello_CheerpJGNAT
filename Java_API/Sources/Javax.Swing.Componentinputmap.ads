pragma Extensions_Allowed (On);
limited with Javax.Swing.JComponent;
limited with Javax.Swing.KeyStroke;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.InputMap;

package Javax.Swing.ComponentInputMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.InputMap.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ComponentInputMap (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_InputMap : access Standard.Javax.Swing.InputMap.Typ'Class);

   function GetComponent (This : access Typ)
                          return access Javax.Swing.JComponent.Typ'Class;

   procedure Put (This : access Typ;
                  P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class);

   procedure Clear (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComponentInputMap);
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");

end Javax.Swing.ComponentInputMap;
pragma Import (Java, Javax.Swing.ComponentInputMap, "javax.swing.ComponentInputMap");
pragma Extensions_Allowed (Off);
