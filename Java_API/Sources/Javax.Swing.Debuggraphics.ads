pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Image;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Java.Io.PrintStream;
limited with Java.Lang.String;
limited with Java.Text.AttributedCharacterIterator;
limited with Javax.Swing.JComponent;
with Java.Awt.Graphics;
with Java.Lang.Object;

package Javax.Swing.DebugGraphics is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Awt.Graphics.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DebugGraphics (This : Ref := null)
                               return Ref;

   function New_DebugGraphics (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_DebugGraphics (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create (This : access Typ)
                    return access Java.Awt.Graphics.Typ'Class;

   function Create (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int)
                    return access Java.Awt.Graphics.Typ'Class;

   procedure SetFlashColor (P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function FlashColor return access Java.Awt.Color.Typ'Class;

   procedure SetFlashTime (P1_Int : Java.Int);

   function FlashTime return Java.Int;

   procedure SetFlashCount (P1_Int : Java.Int);

   function FlashCount return Java.Int;

   procedure SetLogStream (P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   function LogStream return access Java.Io.PrintStream.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   procedure SetColor (This : access Typ;
                       P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetColor (This : access Typ)
                      return access Java.Awt.Color.Typ'Class;

   function GetFontMetrics (This : access Typ)
                            return access Java.Awt.FontMetrics.Typ'Class;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class;

   procedure Translate (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   procedure SetPaintMode (This : access Typ);

   procedure SetXORMode (This : access Typ;
                         P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetClipBounds (This : access Typ)
                           return access Java.Awt.Rectangle.Typ'Class;

   procedure ClipRect (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   procedure SetClip (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int);

   function GetClip (This : access Typ)
                     return access Java.Awt.Shape.Typ'Class;

   procedure SetClip (This : access Typ;
                      P1_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   procedure DrawRect (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   procedure FillRect (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   procedure ClearRect (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure DrawRoundRect (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int);

   procedure FillRoundRect (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int);

   procedure DrawLine (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   procedure Draw3DRect (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Boolean : Java.Boolean);

   procedure Fill3DRect (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Boolean : Java.Boolean);

   procedure DrawOval (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   procedure FillOval (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int);

   procedure DrawArc (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int;
                      P6_Int : Java.Int);

   procedure FillArc (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int;
                      P6_Int : Java.Int);

   procedure DrawPolyline (This : access Typ;
                           P1_Int_Arr : Java.Int_Arr;
                           P2_Int_Arr : Java.Int_Arr;
                           P3_Int : Java.Int);

   procedure DrawPolygon (This : access Typ;
                          P1_Int_Arr : Java.Int_Arr;
                          P2_Int_Arr : Java.Int_Arr;
                          P3_Int : Java.Int);

   procedure FillPolygon (This : access Typ;
                          P1_Int_Arr : Java.Int_Arr;
                          P2_Int_Arr : Java.Int_Arr;
                          P3_Int : Java.Int);

   procedure DrawString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);

   procedure DrawString (This : access Typ;
                         P1_AttributedCharacterIterator : access Standard.Java.Text.AttributedCharacterIterator.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);

   procedure DrawBytes (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   procedure DrawChars (This : access Typ;
                        P1_Char_Arr : Java.Char_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int);

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Color : access Standard.Java.Awt.Color.Typ'Class;
                       P5_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Color : access Standard.Java.Awt.Color.Typ'Class;
                       P7_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int;
                       P8_Int : Java.Int;
                       P9_Int : Java.Int;
                       P10_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean;

   function DrawImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int;
                       P8_Int : Java.Int;
                       P9_Int : Java.Int;
                       P10_Color : access Standard.Java.Awt.Color.Typ'Class;
                       P11_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class)
                       return Java.Boolean;

   procedure CopyArea (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int);

   procedure Dispose (This : access Typ);

   function IsDrawingBuffer (This : access Typ)
                             return Java.Boolean;

   procedure SetDebugOptions (This : access Typ;
                              P1_Int : Java.Int);

   function GetDebugOptions (This : access Typ)
                             return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LOG_OPTION : constant Java.Int;

   --  final
   FLASH_OPTION : constant Java.Int;

   --  final
   BUFFERED_OPTION : constant Java.Int;

   --  final
   NONE_OPTION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DebugGraphics);
   pragma Import (Java, Create, "create");
   pragma Import (Java, SetFlashColor, "setFlashColor");
   pragma Import (Java, FlashColor, "flashColor");
   pragma Import (Java, SetFlashTime, "setFlashTime");
   pragma Import (Java, FlashTime, "flashTime");
   pragma Import (Java, SetFlashCount, "setFlashCount");
   pragma Import (Java, FlashCount, "flashCount");
   pragma Import (Java, SetLogStream, "setLogStream");
   pragma Import (Java, LogStream, "logStream");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, SetColor, "setColor");
   pragma Import (Java, GetColor, "getColor");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, Translate, "translate");
   pragma Import (Java, SetPaintMode, "setPaintMode");
   pragma Import (Java, SetXORMode, "setXORMode");
   pragma Import (Java, GetClipBounds, "getClipBounds");
   pragma Import (Java, ClipRect, "clipRect");
   pragma Import (Java, SetClip, "setClip");
   pragma Import (Java, GetClip, "getClip");
   pragma Import (Java, DrawRect, "drawRect");
   pragma Import (Java, FillRect, "fillRect");
   pragma Import (Java, ClearRect, "clearRect");
   pragma Import (Java, DrawRoundRect, "drawRoundRect");
   pragma Import (Java, FillRoundRect, "fillRoundRect");
   pragma Import (Java, DrawLine, "drawLine");
   pragma Import (Java, Draw3DRect, "draw3DRect");
   pragma Import (Java, Fill3DRect, "fill3DRect");
   pragma Import (Java, DrawOval, "drawOval");
   pragma Import (Java, FillOval, "fillOval");
   pragma Import (Java, DrawArc, "drawArc");
   pragma Import (Java, FillArc, "fillArc");
   pragma Import (Java, DrawPolyline, "drawPolyline");
   pragma Import (Java, DrawPolygon, "drawPolygon");
   pragma Import (Java, FillPolygon, "fillPolygon");
   pragma Import (Java, DrawString, "drawString");
   pragma Import (Java, DrawBytes, "drawBytes");
   pragma Import (Java, DrawChars, "drawChars");
   pragma Import (Java, DrawImage, "drawImage");
   pragma Import (Java, CopyArea, "copyArea");
   pragma Import (Java, Dispose, "dispose");
   pragma Import (Java, IsDrawingBuffer, "isDrawingBuffer");
   pragma Import (Java, SetDebugOptions, "setDebugOptions");
   pragma Import (Java, GetDebugOptions, "getDebugOptions");
   pragma Import (Java, LOG_OPTION, "LOG_OPTION");
   pragma Import (Java, FLASH_OPTION, "FLASH_OPTION");
   pragma Import (Java, BUFFERED_OPTION, "BUFFERED_OPTION");
   pragma Import (Java, NONE_OPTION, "NONE_OPTION");

end Javax.Swing.DebugGraphics;
pragma Import (Java, Javax.Swing.DebugGraphics, "javax.swing.DebugGraphics");
pragma Extensions_Allowed (Off);
