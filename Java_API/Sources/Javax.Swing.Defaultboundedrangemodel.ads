pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.EventListenerList;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.BoundedRangeModel;

package Javax.Swing.DefaultBoundedRangeModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            BoundedRangeModel_I : Javax.Swing.BoundedRangeModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultBoundedRangeModel (This : Ref := null)
                                          return Ref;

   function New_DefaultBoundedRangeModel (P1_Int : Java.Int;
                                          P2_Int : Java.Int;
                                          P3_Int : Java.Int;
                                          P4_Int : Java.Int; 
                                          This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ)
                      return Java.Int;

   function GetExtent (This : access Typ)
                       return Java.Int;

   function GetMinimum (This : access Typ)
                        return Java.Int;

   function GetMaximum (This : access Typ)
                        return Java.Int;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int);

   procedure SetExtent (This : access Typ;
                        P1_Int : Java.Int);

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int);

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int);

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   procedure SetRangeProperties (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Boolean : Java.Boolean);

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultBoundedRangeModel);
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetExtent, "getExtent");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, SetExtent, "setExtent");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, SetRangeProperties, "setRangeProperties");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetListeners, "getListeners");

end Javax.Swing.DefaultBoundedRangeModel;
pragma Import (Java, Javax.Swing.DefaultBoundedRangeModel, "javax.swing.DefaultBoundedRangeModel");
pragma Extensions_Allowed (Off);
