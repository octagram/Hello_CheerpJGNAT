pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Event.ItemEvent;
limited with Java.Awt.Event.ItemListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Swing.ButtonGroup;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.EventListenerList;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.ButtonModel;

package Javax.Swing.DefaultButtonModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ButtonModel_I : Javax.Swing.ButtonModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      StateMask : Java.Int;
      pragma Import (Java, StateMask, "stateMask");

      --  protected
      ActionCommand : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ActionCommand, "actionCommand");

      --  protected
      Group : access Javax.Swing.ButtonGroup.Typ'Class;
      pragma Import (Java, Group, "group");

      --  protected
      Mnemonic : Java.Int;
      pragma Import (Java, Mnemonic, "mnemonic");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultButtonModel (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   function IsArmed (This : access Typ)
                     return Java.Boolean;

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   function IsPressed (This : access Typ)
                       return Java.Boolean;

   function IsRollover (This : access Typ)
                        return Java.Boolean;

   procedure SetArmed (This : access Typ;
                       P1_Boolean : Java.Boolean);

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure SetSelected (This : access Typ;
                          P1_Boolean : Java.Boolean);

   procedure SetPressed (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure SetRollover (This : access Typ;
                          P1_Boolean : Java.Boolean);

   procedure SetMnemonic (This : access Typ;
                          P1_Int : Java.Int);

   function GetMnemonic (This : access Typ)
                         return Java.Int;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireActionPerformed (This : access Typ;
                                  P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   procedure AddItemListener (This : access Typ;
                              P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   procedure RemoveItemListener (This : access Typ;
                                 P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   function GetItemListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireItemStateChanged (This : access Typ;
                                   P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetSelectedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   procedure SetGroup (This : access Typ;
                       P1_ButtonGroup : access Standard.Javax.Swing.ButtonGroup.Typ'Class);

   function GetGroup (This : access Typ)
                      return access Javax.Swing.ButtonGroup.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ARMED : constant Java.Int;

   --  final
   SELECTED : constant Java.Int;

   --  final
   PRESSED : constant Java.Int;

   --  final
   ENABLED : constant Java.Int;

   --  final
   ROLLOVER : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultButtonModel);
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, IsArmed, "isArmed");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, IsPressed, "isPressed");
   pragma Import (Java, IsRollover, "isRollover");
   pragma Import (Java, SetArmed, "setArmed");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, SetPressed, "setPressed");
   pragma Import (Java, SetRollover, "setRollover");
   pragma Import (Java, SetMnemonic, "setMnemonic");
   pragma Import (Java, GetMnemonic, "getMnemonic");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, FireActionPerformed, "fireActionPerformed");
   pragma Import (Java, AddItemListener, "addItemListener");
   pragma Import (Java, RemoveItemListener, "removeItemListener");
   pragma Import (Java, GetItemListeners, "getItemListeners");
   pragma Import (Java, FireItemStateChanged, "fireItemStateChanged");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, GetSelectedObjects, "getSelectedObjects");
   pragma Import (Java, SetGroup, "setGroup");
   pragma Import (Java, GetGroup, "getGroup");
   pragma Import (Java, ARMED, "ARMED");
   pragma Import (Java, SELECTED, "SELECTED");
   pragma Import (Java, PRESSED, "PRESSED");
   pragma Import (Java, ENABLED, "ENABLED");
   pragma Import (Java, ROLLOVER, "ROLLOVER");

end Javax.Swing.DefaultButtonModel;
pragma Import (Java, Javax.Swing.DefaultButtonModel, "javax.swing.DefaultButtonModel");
pragma Extensions_Allowed (Off);
