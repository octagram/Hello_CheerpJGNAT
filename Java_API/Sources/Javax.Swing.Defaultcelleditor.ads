pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Util.EventObject;
limited with Javax.Swing.JCheckBox;
limited with Javax.Swing.JComboBox;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JTable;
limited with Javax.Swing.JTextField;
limited with Javax.Swing.JTree;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.AbstractCellEditor;
with Javax.Swing.CellEditor;
with Javax.Swing.Table.TableCellEditor;
with Javax.Swing.Tree.TreeCellEditor;

package Javax.Swing.DefaultCellEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            CellEditor_I : Javax.Swing.CellEditor.Ref;
            TableCellEditor_I : Javax.Swing.Table.TableCellEditor.Ref;
            TreeCellEditor_I : Javax.Swing.Tree.TreeCellEditor.Ref)
    is new Javax.Swing.AbstractCellEditor.Typ(Serializable_I,
                                              CellEditor_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      EditorComponent : access Javax.Swing.JComponent.Typ'Class;
      pragma Import (Java, EditorComponent, "editorComponent");

      --  protected
      ClickCountToStart : Java.Int;
      pragma Import (Java, ClickCountToStart, "clickCountToStart");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultCellEditor (P1_JTextField : access Standard.Javax.Swing.JTextField.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_DefaultCellEditor (P1_JCheckBox : access Standard.Javax.Swing.JCheckBox.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_DefaultCellEditor (P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   procedure SetClickCountToStart (This : access Typ;
                                   P1_Int : Java.Int);

   function GetClickCountToStart (This : access Typ)
                                  return Java.Int;

   function GetCellEditorValue (This : access Typ)
                                return access Java.Lang.Object.Typ'Class;

   function IsCellEditable (This : access Typ;
                            P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                            return Java.Boolean;

   function ShouldSelectCell (This : access Typ;
                              P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                              return Java.Boolean;

   function StopCellEditing (This : access Typ)
                             return Java.Boolean;

   procedure CancelCellEditing (This : access Typ);

   function GetTreeCellEditorComponent (This : access Typ;
                                        P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P3_Boolean : Java.Boolean;
                                        P4_Boolean : Java.Boolean;
                                        P5_Boolean : Java.Boolean;
                                        P6_Int : Java.Int)
                                        return access Java.Awt.Component.Typ'Class;

   function GetTableCellEditorComponent (This : access Typ;
                                         P1_JTable : access Standard.Javax.Swing.JTable.Typ'Class;
                                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                         P3_Boolean : Java.Boolean;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int)
                                         return access Java.Awt.Component.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultCellEditor);
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, SetClickCountToStart, "setClickCountToStart");
   pragma Import (Java, GetClickCountToStart, "getClickCountToStart");
   pragma Import (Java, GetCellEditorValue, "getCellEditorValue");
   pragma Import (Java, IsCellEditable, "isCellEditable");
   pragma Import (Java, ShouldSelectCell, "shouldSelectCell");
   pragma Import (Java, StopCellEditing, "stopCellEditing");
   pragma Import (Java, CancelCellEditing, "cancelCellEditing");
   pragma Import (Java, GetTreeCellEditorComponent, "getTreeCellEditorComponent");
   pragma Import (Java, GetTableCellEditorComponent, "getTableCellEditorComponent");

end Javax.Swing.DefaultCellEditor;
pragma Import (Java, Javax.Swing.DefaultCellEditor, "javax.swing.DefaultCellEditor");
pragma Extensions_Allowed (Off);
