pragma Extensions_Allowed (On);
limited with Java.Util.Vector;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.AbstractListModel;
with Javax.Swing.ListModel;
with Javax.Swing.MutableComboBoxModel;

package Javax.Swing.DefaultComboBoxModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ListModel_I : Javax.Swing.ListModel.Ref;
            MutableComboBoxModel_I : Javax.Swing.MutableComboBoxModel.Ref)
    is new Javax.Swing.AbstractListModel.Typ(Serializable_I,
                                             ListModel_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultComboBoxModel (This : Ref := null)
                                      return Ref;

   function New_DefaultComboBoxModel (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                      This : Ref := null)
                                      return Ref;

   function New_DefaultComboBoxModel (P1_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSelectedItem (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetSelectedItem (This : access Typ)
                             return access Java.Lang.Object.Typ'Class;

   function GetSize (This : access Typ)
                     return Java.Int;

   function GetElementAt (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.Object.Typ'Class;

   function GetIndexOf (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Int;

   procedure AddElement (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure InsertElementAt (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_Int : Java.Int);

   procedure RemoveElementAt (This : access Typ;
                              P1_Int : Java.Int);

   procedure RemoveElement (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveAllElements (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultComboBoxModel);
   pragma Import (Java, SetSelectedItem, "setSelectedItem");
   pragma Import (Java, GetSelectedItem, "getSelectedItem");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, GetElementAt, "getElementAt");
   pragma Import (Java, GetIndexOf, "getIndexOf");
   pragma Import (Java, AddElement, "addElement");
   pragma Import (Java, InsertElementAt, "insertElementAt");
   pragma Import (Java, RemoveElementAt, "removeElementAt");
   pragma Import (Java, RemoveElement, "removeElement");
   pragma Import (Java, RemoveAllElements, "removeAllElements");

end Javax.Swing.DefaultComboBoxModel;
pragma Import (Java, Javax.Swing.DefaultComboBoxModel, "javax.swing.DefaultComboBoxModel");
pragma Extensions_Allowed (Off);
