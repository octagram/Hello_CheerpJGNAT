pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Java.Lang.Boolean;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JInternalFrame;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.DesktopManager;

package Javax.Swing.DefaultDesktopManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            DesktopManager_I : Javax.Swing.DesktopManager.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultDesktopManager (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure OpenFrame (This : access Typ;
                        P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure CloseFrame (This : access Typ;
                         P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure MaximizeFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure MinimizeFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure IconifyFrame (This : access Typ;
                           P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure DeiconifyFrame (This : access Typ;
                             P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure ActivateFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure DeactivateFrame (This : access Typ;
                              P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   procedure BeginDraggingFrame (This : access Typ;
                                 P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure DragFrame (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int);

   procedure EndDraggingFrame (This : access Typ;
                               P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure BeginResizingFrame (This : access Typ;
                                 P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                 P2_Int : Java.Int);

   procedure ResizeFrame (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int);

   procedure EndResizingFrame (This : access Typ;
                               P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure SetBoundsForFrame (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int);

   --  protected
   procedure RemoveIconFor (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   function GetBoundsForIconOf (This : access Typ;
                                P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                                return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   procedure SetPreviousBounds (This : access Typ;
                                P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class;
                                P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function GetPreviousBounds (This : access Typ;
                               P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                               return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   procedure SetWasIcon (This : access Typ;
                         P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class;
                         P2_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   --  protected
   function WasIcon (This : access Typ;
                     P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                     return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultDesktopManager);
   pragma Import (Java, OpenFrame, "openFrame");
   pragma Import (Java, CloseFrame, "closeFrame");
   pragma Import (Java, MaximizeFrame, "maximizeFrame");
   pragma Import (Java, MinimizeFrame, "minimizeFrame");
   pragma Import (Java, IconifyFrame, "iconifyFrame");
   pragma Import (Java, DeiconifyFrame, "deiconifyFrame");
   pragma Import (Java, ActivateFrame, "activateFrame");
   pragma Import (Java, DeactivateFrame, "deactivateFrame");
   pragma Import (Java, BeginDraggingFrame, "beginDraggingFrame");
   pragma Import (Java, DragFrame, "dragFrame");
   pragma Import (Java, EndDraggingFrame, "endDraggingFrame");
   pragma Import (Java, BeginResizingFrame, "beginResizingFrame");
   pragma Import (Java, ResizeFrame, "resizeFrame");
   pragma Import (Java, EndResizingFrame, "endResizingFrame");
   pragma Import (Java, SetBoundsForFrame, "setBoundsForFrame");
   pragma Import (Java, RemoveIconFor, "removeIconFor");
   pragma Import (Java, GetBoundsForIconOf, "getBoundsForIconOf");
   pragma Import (Java, SetPreviousBounds, "setPreviousBounds");
   pragma Import (Java, GetPreviousBounds, "getPreviousBounds");
   pragma Import (Java, SetWasIcon, "setWasIcon");
   pragma Import (Java, WasIcon, "wasIcon");

end Javax.Swing.DefaultDesktopManager;
pragma Import (Java, Javax.Swing.DefaultDesktopManager, "javax.swing.DefaultDesktopManager");
pragma Extensions_Allowed (Off);
