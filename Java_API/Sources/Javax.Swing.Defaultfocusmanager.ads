pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
with Java.Awt.KeyEventDispatcher;
with Java.Awt.KeyEventPostProcessor;
with Java.Lang.Object;
with Javax.Swing.FocusManager;

package Javax.Swing.DefaultFocusManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeyEventDispatcher_I : Java.Awt.KeyEventDispatcher.Ref;
            KeyEventPostProcessor_I : Java.Awt.KeyEventPostProcessor.Ref)
    is new Javax.Swing.FocusManager.Typ(KeyEventDispatcher_I,
                                        KeyEventPostProcessor_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultFocusManager (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponentAfter (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                               P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return access Java.Awt.Component.Typ'Class;

   function GetComponentBefore (This : access Typ;
                                P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                                P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return access Java.Awt.Component.Typ'Class;

   function GetFirstComponent (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Component.Typ'Class;

   function GetLastComponent (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                              return access Java.Awt.Component.Typ'Class;

   function CompareTabOrder (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultFocusManager);
   pragma Import (Java, GetComponentAfter, "getComponentAfter");
   pragma Import (Java, GetComponentBefore, "getComponentBefore");
   pragma Import (Java, GetFirstComponent, "getFirstComponent");
   pragma Import (Java, GetLastComponent, "getLastComponent");
   pragma Import (Java, CompareTabOrder, "compareTabOrder");

end Javax.Swing.DefaultFocusManager;
pragma Import (Java, Javax.Swing.DefaultFocusManager, "javax.swing.DefaultFocusManager");
pragma Extensions_Allowed (Off);
