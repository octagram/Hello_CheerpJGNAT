pragma Extensions_Allowed (On);
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.DefaultListCellRenderer;
with Javax.Swing.ListCellRenderer;
with Javax.Swing.Plaf.UIResource;
with Javax.Swing.SwingConstants;

package Javax.Swing.DefaultListCellRenderer.UIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ListCellRenderer_I : Javax.Swing.ListCellRenderer.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.DefaultListCellRenderer.Typ(MenuContainer_I,
                                                   ImageObserver_I,
                                                   Serializable_I,
                                                   Accessible_I,
                                                   ListCellRenderer_I,
                                                   SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UIResource (This : Ref := null)
                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UIResource);

end Javax.Swing.DefaultListCellRenderer.UIResource;
pragma Import (Java, Javax.Swing.DefaultListCellRenderer.UIResource, "javax.swing.DefaultListCellRenderer$UIResource");
pragma Extensions_Allowed (Off);
