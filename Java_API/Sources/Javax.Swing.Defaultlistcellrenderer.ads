pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.JList;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JLabel;
with Javax.Swing.ListCellRenderer;
with Javax.Swing.SwingConstants;

package Javax.Swing.DefaultListCellRenderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ListCellRenderer_I : Javax.Swing.ListCellRenderer.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JLabel.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I,
                                  Accessible_I,
                                  SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultListCellRenderer (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetListCellRendererComponent (This : access Typ;
                                          P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                          P3_Int : Java.Int;
                                          P4_Boolean : Java.Boolean;
                                          P5_Boolean : Java.Boolean)
                                          return access Java.Awt.Component.Typ'Class;

   function IsOpaque (This : access Typ)
                      return Java.Boolean;

   procedure Validate (This : access Typ);

   procedure Invalidate (This : access Typ);

   procedure Repaint (This : access Typ);

   procedure Revalidate (This : access Typ);

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int);

   procedure Repaint (This : access Typ;
                      P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Byte : Java.Byte;
                                 P3_Byte : Java.Byte);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Char : Java.Char;
                                 P3_Char : Java.Char);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Short : Java.Short;
                                 P3_Short : Java.Short);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Long : Java.Long;
                                 P3_Long : Java.Long);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Float : Java.Float;
                                 P3_Float : Java.Float);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Double : Java.Double;
                                 P3_Double : Java.Double);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Boolean : Java.Boolean;
                                 P3_Boolean : Java.Boolean);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   NoFocusBorder : access Javax.Swing.Border.Border.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultListCellRenderer);
   pragma Import (Java, GetListCellRendererComponent, "getListCellRendererComponent");
   pragma Import (Java, IsOpaque, "isOpaque");
   pragma Import (Java, Validate, "validate");
   pragma Import (Java, Invalidate, "invalidate");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, Revalidate, "revalidate");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, NoFocusBorder, "noFocusBorder");

end Javax.Swing.DefaultListCellRenderer;
pragma Import (Java, Javax.Swing.DefaultListCellRenderer, "javax.swing.DefaultListCellRenderer");
pragma Extensions_Allowed (Off);
