pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.AbstractListModel;
with Javax.Swing.ListModel;

package Javax.Swing.DefaultListModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ListModel_I : Javax.Swing.ListModel.Ref)
    is new Javax.Swing.AbstractListModel.Typ(Serializable_I,
                                             ListModel_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultListModel (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSize (This : access Typ)
                     return Java.Int;

   function GetElementAt (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.Object.Typ'Class;

   procedure CopyInto (This : access Typ;
                       P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure TrimToSize (This : access Typ);

   procedure EnsureCapacity (This : access Typ;
                             P1_Int : Java.Int);

   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int);

   function Capacity (This : access Typ)
                      return Java.Int;

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Elements (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Int : Java.Int)
                     return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Int;

   function LastIndexOf (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;

   function ElementAt (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.Object.Typ'Class;

   function FirstElement (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   function LastElement (This : access Typ)
                         return access Java.Lang.Object.Typ'Class;

   procedure SetElementAt (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int : Java.Int);

   procedure RemoveElementAt (This : access Typ;
                              P1_Int : Java.Int);

   procedure InsertElementAt (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_Int : Java.Int);

   procedure AddElement (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function RemoveElement (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   procedure RemoveAllElements (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ToArray (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function Get (This : access Typ;
                 P1_Int : Java.Int)
                 return access Java.Lang.Object.Typ'Class;

   function Set (This : access Typ;
                 P1_Int : Java.Int;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Remove (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   procedure RemoveRange (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultListModel);
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, GetElementAt, "getElementAt");
   pragma Import (Java, CopyInto, "copyInto");
   pragma Import (Java, TrimToSize, "trimToSize");
   pragma Import (Java, EnsureCapacity, "ensureCapacity");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, Capacity, "capacity");
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Elements, "elements");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, LastIndexOf, "lastIndexOf");
   pragma Import (Java, ElementAt, "elementAt");
   pragma Import (Java, FirstElement, "firstElement");
   pragma Import (Java, LastElement, "lastElement");
   pragma Import (Java, SetElementAt, "setElementAt");
   pragma Import (Java, RemoveElementAt, "removeElementAt");
   pragma Import (Java, InsertElementAt, "insertElementAt");
   pragma Import (Java, AddElement, "addElement");
   pragma Import (Java, RemoveElement, "removeElement");
   pragma Import (Java, RemoveAllElements, "removeAllElements");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ToArray, "toArray");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Set, "set");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, RemoveRange, "removeRange");

end Javax.Swing.DefaultListModel;
pragma Import (Java, Javax.Swing.DefaultListModel, "javax.swing.DefaultListModel");
pragma Extensions_Allowed (Off);
