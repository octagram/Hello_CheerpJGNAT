pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Event.ListSelectionListener;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.ListSelectionModel;

package Javax.Swing.DefaultListSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            ListSelectionModel_I : Javax.Swing.ListSelectionModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      LeadAnchorNotificationEnabled : Java.Boolean;
      pragma Import (Java, LeadAnchorNotificationEnabled, "leadAnchorNotificationEnabled");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultListSelectionModel (This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMinSelectionIndex (This : access Typ)
                                  return Java.Int;

   function GetMaxSelectionIndex (This : access Typ)
                                  return Java.Int;

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   function GetSelectionMode (This : access Typ)
                              return Java.Int;

   procedure SetSelectionMode (This : access Typ;
                               P1_Int : Java.Int);

   function IsSelectedIndex (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean;

   function IsSelectionEmpty (This : access Typ)
                              return Java.Boolean;

   procedure AddListSelectionListener (This : access Typ;
                                       P1_ListSelectionListener : access Standard.Javax.Swing.Event.ListSelectionListener.Typ'Class);

   procedure RemoveListSelectionListener (This : access Typ;
                                          P1_ListSelectionListener : access Standard.Javax.Swing.Event.ListSelectionListener.Typ'Class);

   function GetListSelectionListeners (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireValueChanged (This : access Typ;
                               P1_Boolean : Java.Boolean);

   --  protected
   procedure FireValueChanged (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int);

   --  protected
   procedure FireValueChanged (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Boolean : Java.Boolean);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   procedure SetLeadAnchorNotificationEnabled (This : access Typ;
                                               P1_Boolean : Java.Boolean);

   function IsLeadAnchorNotificationEnabled (This : access Typ)
                                             return Java.Boolean;

   procedure ClearSelection (This : access Typ);

   procedure SetSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure AddSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure RemoveSelectionInterval (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int);

   procedure InsertIndexInterval (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Boolean : Java.Boolean);

   procedure RemoveIndexInterval (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int);

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   function GetAnchorSelectionIndex (This : access Typ)
                                     return Java.Int;

   function GetLeadSelectionIndex (This : access Typ)
                                   return Java.Int;

   procedure SetAnchorSelectionIndex (This : access Typ;
                                      P1_Int : Java.Int);

   procedure MoveLeadSelectionIndex (This : access Typ;
                                     P1_Int : Java.Int);

   procedure SetLeadSelectionIndex (This : access Typ;
                                    P1_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultListSelectionModel);
   pragma Import (Java, GetMinSelectionIndex, "getMinSelectionIndex");
   pragma Import (Java, GetMaxSelectionIndex, "getMaxSelectionIndex");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, GetSelectionMode, "getSelectionMode");
   pragma Import (Java, SetSelectionMode, "setSelectionMode");
   pragma Import (Java, IsSelectedIndex, "isSelectedIndex");
   pragma Import (Java, IsSelectionEmpty, "isSelectionEmpty");
   pragma Import (Java, AddListSelectionListener, "addListSelectionListener");
   pragma Import (Java, RemoveListSelectionListener, "removeListSelectionListener");
   pragma Import (Java, GetListSelectionListeners, "getListSelectionListeners");
   pragma Import (Java, FireValueChanged, "fireValueChanged");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, SetLeadAnchorNotificationEnabled, "setLeadAnchorNotificationEnabled");
   pragma Import (Java, IsLeadAnchorNotificationEnabled, "isLeadAnchorNotificationEnabled");
   pragma Import (Java, ClearSelection, "clearSelection");
   pragma Import (Java, SetSelectionInterval, "setSelectionInterval");
   pragma Import (Java, AddSelectionInterval, "addSelectionInterval");
   pragma Import (Java, RemoveSelectionInterval, "removeSelectionInterval");
   pragma Import (Java, InsertIndexInterval, "insertIndexInterval");
   pragma Import (Java, RemoveIndexInterval, "removeIndexInterval");
   pragma Import (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetAnchorSelectionIndex, "getAnchorSelectionIndex");
   pragma Import (Java, GetLeadSelectionIndex, "getLeadSelectionIndex");
   pragma Import (Java, SetAnchorSelectionIndex, "setAnchorSelectionIndex");
   pragma Import (Java, MoveLeadSelectionIndex, "moveLeadSelectionIndex");
   pragma Import (Java, SetLeadSelectionIndex, "setLeadSelectionIndex");

end Javax.Swing.DefaultListSelectionModel;
pragma Import (Java, Javax.Swing.DefaultListSelectionModel, "javax.swing.DefaultListSelectionModel");
pragma Extensions_Allowed (Off);
