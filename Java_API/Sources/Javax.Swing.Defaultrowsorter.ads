pragma Extensions_Allowed (On);
limited with Java.Util.Comparator;
limited with Java.Util.List;
limited with Javax.Swing.RowFilter;
with Java.Lang.Object;
with Javax.Swing.RowSorter;

package Javax.Swing.DefaultRowSorter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.RowSorter.Typ
      with null record;

   function New_DefaultRowSorter (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetModel (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure SetSortable (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Boolean : Java.Boolean);

   function IsSortable (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   procedure SetSortKeys (This : access Typ;
                          P1_List : access Standard.Java.Util.List.Typ'Class);

   function GetSortKeys (This : access Typ)
                         return access Java.Util.List.Typ'Class;

   procedure SetMaxSortKeys (This : access Typ;
                             P1_Int : Java.Int);

   function GetMaxSortKeys (This : access Typ)
                            return Java.Int;

   procedure SetSortsOnUpdates (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function GetSortsOnUpdates (This : access Typ)
                               return Java.Boolean;

   procedure SetRowFilter (This : access Typ;
                           P1_RowFilter : access Standard.Javax.Swing.RowFilter.Typ'Class);

   function GetRowFilter (This : access Typ)
                          return access Javax.Swing.RowFilter.Typ'Class;

   procedure ToggleSortOrder (This : access Typ;
                              P1_Int : Java.Int);

   function ConvertRowIndexToView (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Int;

   function ConvertRowIndexToModel (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Java.Int;

   procedure Sort (This : access Typ);

   --  protected
   function UseToString (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;

   procedure SetComparator (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Comparator : access Standard.Java.Util.Comparator.Typ'Class);

   function GetComparator (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Util.Comparator.Typ'Class;

   function GetViewRowCount (This : access Typ)
                             return Java.Int;

   function GetModelRowCount (This : access Typ)
                              return Java.Int;

   procedure ModelStructureChanged (This : access Typ);

   procedure AllRowsChanged (This : access Typ);

   procedure RowsInserted (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int);

   procedure RowsDeleted (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);

   procedure RowsUpdated (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);

   procedure RowsUpdated (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultRowSorter);
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetSortable, "setSortable");
   pragma Import (Java, IsSortable, "isSortable");
   pragma Import (Java, SetSortKeys, "setSortKeys");
   pragma Import (Java, GetSortKeys, "getSortKeys");
   pragma Import (Java, SetMaxSortKeys, "setMaxSortKeys");
   pragma Import (Java, GetMaxSortKeys, "getMaxSortKeys");
   pragma Import (Java, SetSortsOnUpdates, "setSortsOnUpdates");
   pragma Import (Java, GetSortsOnUpdates, "getSortsOnUpdates");
   pragma Import (Java, SetRowFilter, "setRowFilter");
   pragma Import (Java, GetRowFilter, "getRowFilter");
   pragma Import (Java, ToggleSortOrder, "toggleSortOrder");
   pragma Import (Java, ConvertRowIndexToView, "convertRowIndexToView");
   pragma Import (Java, ConvertRowIndexToModel, "convertRowIndexToModel");
   pragma Import (Java, Sort, "sort");
   pragma Import (Java, UseToString, "useToString");
   pragma Import (Java, SetComparator, "setComparator");
   pragma Import (Java, GetComparator, "getComparator");
   pragma Import (Java, GetViewRowCount, "getViewRowCount");
   pragma Import (Java, GetModelRowCount, "getModelRowCount");
   pragma Import (Java, ModelStructureChanged, "modelStructureChanged");
   pragma Import (Java, AllRowsChanged, "allRowsChanged");
   pragma Import (Java, RowsInserted, "rowsInserted");
   pragma Import (Java, RowsDeleted, "rowsDeleted");
   pragma Import (Java, RowsUpdated, "rowsUpdated");

end Javax.Swing.DefaultRowSorter;
pragma Import (Java, Javax.Swing.DefaultRowSorter, "javax.swing.DefaultRowSorter");
pragma Extensions_Allowed (Off);
