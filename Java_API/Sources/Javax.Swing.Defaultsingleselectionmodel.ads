pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.EventListenerList;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.SingleSelectionModel;

package Javax.Swing.DefaultSingleSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            SingleSelectionModel_I : Javax.Swing.SingleSelectionModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultSingleSelectionModel (This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSelectedIndex (This : access Typ)
                              return Java.Int;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int);

   procedure ClearSelection (This : access Typ);

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultSingleSelectionModel);
   pragma Import (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Import (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Import (Java, ClearSelection, "clearSelection");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, GetListeners, "getListeners");

end Javax.Swing.DefaultSingleSelectionModel;
pragma Import (Java, Javax.Swing.DefaultSingleSelectionModel, "javax.swing.DefaultSingleSelectionModel");
pragma Extensions_Allowed (Off);
