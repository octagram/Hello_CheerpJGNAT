pragma Extensions_Allowed (On);
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JInternalFrame;
with Java.Lang.Object;

package Javax.Swing.DesktopManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure OpenFrame (This : access Typ;
                        P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure CloseFrame (This : access Typ;
                         P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure MaximizeFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure MinimizeFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure IconifyFrame (This : access Typ;
                           P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure DeiconifyFrame (This : access Typ;
                             P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure ActivateFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure DeactivateFrame (This : access Typ;
                              P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class) is abstract;

   procedure BeginDraggingFrame (This : access Typ;
                                 P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class) is abstract;

   procedure DragFrame (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int) is abstract;

   procedure EndDraggingFrame (This : access Typ;
                               P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class) is abstract;

   procedure BeginResizingFrame (This : access Typ;
                                 P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                 P2_Int : Java.Int) is abstract;

   procedure ResizeFrame (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int) is abstract;

   procedure EndResizingFrame (This : access Typ;
                               P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class) is abstract;

   procedure SetBoundsForFrame (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, OpenFrame, "openFrame");
   pragma Export (Java, CloseFrame, "closeFrame");
   pragma Export (Java, MaximizeFrame, "maximizeFrame");
   pragma Export (Java, MinimizeFrame, "minimizeFrame");
   pragma Export (Java, IconifyFrame, "iconifyFrame");
   pragma Export (Java, DeiconifyFrame, "deiconifyFrame");
   pragma Export (Java, ActivateFrame, "activateFrame");
   pragma Export (Java, DeactivateFrame, "deactivateFrame");
   pragma Export (Java, BeginDraggingFrame, "beginDraggingFrame");
   pragma Export (Java, DragFrame, "dragFrame");
   pragma Export (Java, EndDraggingFrame, "endDraggingFrame");
   pragma Export (Java, BeginResizingFrame, "beginResizingFrame");
   pragma Export (Java, ResizeFrame, "resizeFrame");
   pragma Export (Java, EndResizingFrame, "endResizingFrame");
   pragma Export (Java, SetBoundsForFrame, "setBoundsForFrame");

end Javax.Swing.DesktopManager;
pragma Import (Java, Javax.Swing.DesktopManager, "javax.swing.DesktopManager");
pragma Extensions_Allowed (Off);
