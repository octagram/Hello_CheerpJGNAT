pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Swing.DropMode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Swing.DropMode.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   USE_SELECTION : access Javax.Swing.DropMode.Typ'Class;

   --  final
   ON : access Javax.Swing.DropMode.Typ'Class;

   --  final
   INSERT : access Javax.Swing.DropMode.Typ'Class;

   --  final
   INSERT_ROWS : access Javax.Swing.DropMode.Typ'Class;

   --  final
   INSERT_COLS : access Javax.Swing.DropMode.Typ'Class;

   --  final
   ON_OR_INSERT : access Javax.Swing.DropMode.Typ'Class;

   --  final
   ON_OR_INSERT_ROWS : access Javax.Swing.DropMode.Typ'Class;

   --  final
   ON_OR_INSERT_COLS : access Javax.Swing.DropMode.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, USE_SELECTION, "USE_SELECTION");
   pragma Import (Java, ON, "ON");
   pragma Import (Java, INSERT, "INSERT");
   pragma Import (Java, INSERT_ROWS, "INSERT_ROWS");
   pragma Import (Java, INSERT_COLS, "INSERT_COLS");
   pragma Import (Java, ON_OR_INSERT, "ON_OR_INSERT");
   pragma Import (Java, ON_OR_INSERT_ROWS, "ON_OR_INSERT_ROWS");
   pragma Import (Java, ON_OR_INSERT_COLS, "ON_OR_INSERT_COLS");

end Javax.Swing.DropMode;
pragma Import (Java, Javax.Swing.DropMode, "javax.swing.DropMode");
pragma Extensions_Allowed (Off);
