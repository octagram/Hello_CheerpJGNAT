pragma Extensions_Allowed (On);
limited with Java.Awt.Container;
limited with Javax.Swing.JComponent;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Event.AncestorEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AncestorEvent (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Container : access Standard.Java.Awt.Container.Typ'Class;
                               P4_Container : access Standard.Java.Awt.Container.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAncestor (This : access Typ)
                         return access Java.Awt.Container.Typ'Class;

   function GetAncestorParent (This : access Typ)
                               return access Java.Awt.Container.Typ'Class;

   function GetComponent (This : access Typ)
                          return access Javax.Swing.JComponent.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ANCESTOR_ADDED : constant Java.Int;

   --  final
   ANCESTOR_REMOVED : constant Java.Int;

   --  final
   ANCESTOR_MOVED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AncestorEvent);
   pragma Import (Java, GetAncestor, "getAncestor");
   pragma Import (Java, GetAncestorParent, "getAncestorParent");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, ANCESTOR_ADDED, "ANCESTOR_ADDED");
   pragma Import (Java, ANCESTOR_REMOVED, "ANCESTOR_REMOVED");
   pragma Import (Java, ANCESTOR_MOVED, "ANCESTOR_MOVED");

end Javax.Swing.Event.AncestorEvent;
pragma Import (Java, Javax.Swing.Event.AncestorEvent, "javax.swing.event.AncestorEvent");
pragma Extensions_Allowed (Off);
