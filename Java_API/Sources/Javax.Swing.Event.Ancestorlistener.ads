pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.AncestorEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Swing.Event.AncestorListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AncestorAdded (This : access Typ;
                            P1_AncestorEvent : access Standard.Javax.Swing.Event.AncestorEvent.Typ'Class) is abstract;

   procedure AncestorRemoved (This : access Typ;
                              P1_AncestorEvent : access Standard.Javax.Swing.Event.AncestorEvent.Typ'Class) is abstract;

   procedure AncestorMoved (This : access Typ;
                            P1_AncestorEvent : access Standard.Javax.Swing.Event.AncestorEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AncestorAdded, "ancestorAdded");
   pragma Export (Java, AncestorRemoved, "ancestorRemoved");
   pragma Export (Java, AncestorMoved, "ancestorMoved");

end Javax.Swing.Event.AncestorListener;
pragma Import (Java, Javax.Swing.Event.AncestorListener, "javax.swing.event.AncestorListener");
pragma Extensions_Allowed (Off);
