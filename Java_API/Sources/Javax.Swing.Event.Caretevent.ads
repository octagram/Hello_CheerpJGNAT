pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.CaretEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   function New_CaretEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDot (This : access Typ)
                    return Java.Int is abstract;

   function GetMark (This : access Typ)
                     return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CaretEvent);
   pragma Export (Java, GetDot, "getDot");
   pragma Export (Java, GetMark, "getMark");

end Javax.Swing.Event.CaretEvent;
pragma Import (Java, Javax.Swing.Event.CaretEvent, "javax.swing.event.CaretEvent");
pragma Extensions_Allowed (Off);
