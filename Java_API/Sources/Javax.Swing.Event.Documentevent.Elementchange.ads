pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Element;
with Java.Lang.Object;

package Javax.Swing.Event.DocumentEvent.ElementChange is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetElement (This : access Typ)
                        return access Javax.Swing.Text.Element.Typ'Class is abstract;

   function GetIndex (This : access Typ)
                      return Java.Int is abstract;

   function GetChildrenRemoved (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;

   function GetChildrenAdded (This : access Typ)
                              return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetElement, "getElement");
   pragma Export (Java, GetIndex, "getIndex");
   pragma Export (Java, GetChildrenRemoved, "getChildrenRemoved");
   pragma Export (Java, GetChildrenAdded, "getChildrenAdded");

end Javax.Swing.Event.DocumentEvent.ElementChange;
pragma Import (Java, Javax.Swing.Event.DocumentEvent.ElementChange, "javax.swing.event.DocumentEvent$ElementChange");
pragma Extensions_Allowed (Off);
