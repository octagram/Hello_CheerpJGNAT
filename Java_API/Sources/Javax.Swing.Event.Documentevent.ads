pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.DocumentEvent.ElementChange;
limited with Javax.Swing.Event.DocumentEvent.EventType;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Element;
with Java.Lang.Object;

package Javax.Swing.Event.DocumentEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOffset (This : access Typ)
                       return Java.Int is abstract;

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class is abstract;

   function GetType (This : access Typ)
                     return access Javax.Swing.Event.DocumentEvent.EventType.Typ'Class is abstract;

   function GetChange (This : access Typ;
                       P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                       return access Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetOffset, "getOffset");
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, GetDocument, "getDocument");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetChange, "getChange");

end Javax.Swing.Event.DocumentEvent;
pragma Import (Java, Javax.Swing.Event.DocumentEvent, "javax.swing.event.DocumentEvent");
pragma Extensions_Allowed (Off);
