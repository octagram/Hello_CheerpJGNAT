pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Event.EventListenerList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EventListenerList (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetListenerList (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetListenerCount (This : access Typ)
                              return Java.Int;

   function GetListenerCount (This : access Typ;
                              P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                              return Java.Int;

   --  synchronized
   procedure Add (This : access Typ;
                  P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                  P2_EventListener : access Standard.Java.Util.EventListener.Typ'Class);

   --  synchronized
   procedure Remove (This : access Typ;
                     P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P2_EventListener : access Standard.Java.Util.EventListener.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EventListenerList);
   pragma Import (Java, GetListenerList, "getListenerList");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, GetListenerCount, "getListenerCount");
   pragma Import (Java, Add, "add");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Event.EventListenerList;
pragma Import (Java, Javax.Swing.Event.EventListenerList, "javax.swing.event.EventListenerList");
pragma Extensions_Allowed (Off);
