pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Event.HyperlinkEvent.EventType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ENTERED : access Javax.Swing.Event.HyperlinkEvent.EventType.Typ'Class;

   --  final
   EXITED : access Javax.Swing.Event.HyperlinkEvent.EventType.Typ'Class;

   --  final
   ACTIVATED : access Javax.Swing.Event.HyperlinkEvent.EventType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ENTERED, "ENTERED");
   pragma Import (Java, EXITED, "EXITED");
   pragma Import (Java, ACTIVATED, "ACTIVATED");

end Javax.Swing.Event.HyperlinkEvent.EventType;
pragma Import (Java, Javax.Swing.Event.HyperlinkEvent.EventType, "javax.swing.event.HyperlinkEvent$EventType");
pragma Extensions_Allowed (Off);
