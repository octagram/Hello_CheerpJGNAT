pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Swing.Event.HyperlinkEvent.EventType;
limited with Javax.Swing.Text.Element;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.HyperlinkEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HyperlinkEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_EventType : access Standard.Javax.Swing.Event.HyperlinkEvent.EventType.Typ'Class;
                                P3_URL : access Standard.Java.Net.URL.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_HyperlinkEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_EventType : access Standard.Javax.Swing.Event.HyperlinkEvent.EventType.Typ'Class;
                                P3_URL : access Standard.Java.Net.URL.Typ'Class;
                                P4_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_HyperlinkEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_EventType : access Standard.Javax.Swing.Event.HyperlinkEvent.EventType.Typ'Class;
                                P3_URL : access Standard.Java.Net.URL.Typ'Class;
                                P4_String : access Standard.Java.Lang.String.Typ'Class;
                                P5_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEventType (This : access Typ)
                          return access Javax.Swing.Event.HyperlinkEvent.EventType.Typ'Class;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetURL (This : access Typ)
                    return access Java.Net.URL.Typ'Class;

   function GetSourceElement (This : access Typ)
                              return access Javax.Swing.Text.Element.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HyperlinkEvent);
   pragma Import (Java, GetEventType, "getEventType");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetURL, "getURL");
   pragma Import (Java, GetSourceElement, "getSourceElement");

end Javax.Swing.Event.HyperlinkEvent;
pragma Import (Java, Javax.Swing.Event.HyperlinkEvent, "javax.swing.event.HyperlinkEvent");
pragma Extensions_Allowed (Off);
