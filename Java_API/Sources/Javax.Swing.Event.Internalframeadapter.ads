pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.InternalFrameEvent;
with Java.Lang.Object;
with Javax.Swing.Event.InternalFrameListener;

package Javax.Swing.Event.InternalFrameAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(InternalFrameListener_I : Javax.Swing.Event.InternalFrameListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_InternalFrameAdapter (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InternalFrameOpened (This : access Typ;
                                  P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class);

   procedure InternalFrameClosing (This : access Typ;
                                   P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class);

   procedure InternalFrameClosed (This : access Typ;
                                  P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class);

   procedure InternalFrameIconified (This : access Typ;
                                     P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class);

   procedure InternalFrameDeiconified (This : access Typ;
                                       P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class);

   procedure InternalFrameActivated (This : access Typ;
                                     P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class);

   procedure InternalFrameDeactivated (This : access Typ;
                                       P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InternalFrameAdapter);
   pragma Import (Java, InternalFrameOpened, "internalFrameOpened");
   pragma Import (Java, InternalFrameClosing, "internalFrameClosing");
   pragma Import (Java, InternalFrameClosed, "internalFrameClosed");
   pragma Import (Java, InternalFrameIconified, "internalFrameIconified");
   pragma Import (Java, InternalFrameDeiconified, "internalFrameDeiconified");
   pragma Import (Java, InternalFrameActivated, "internalFrameActivated");
   pragma Import (Java, InternalFrameDeactivated, "internalFrameDeactivated");

end Javax.Swing.Event.InternalFrameAdapter;
pragma Import (Java, Javax.Swing.Event.InternalFrameAdapter, "javax.swing.event.InternalFrameAdapter");
pragma Extensions_Allowed (Off);
