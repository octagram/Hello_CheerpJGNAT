pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.JInternalFrame;
with Java.Awt.AWTEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Event.InternalFrameEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InternalFrameEvent (P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class;
                                    P2_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetInternalFrame (This : access Typ)
                              return access Javax.Swing.JInternalFrame.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INTERNAL_FRAME_FIRST : constant Java.Int;

   --  final
   INTERNAL_FRAME_LAST : constant Java.Int;

   --  final
   INTERNAL_FRAME_OPENED : constant Java.Int;

   --  final
   INTERNAL_FRAME_CLOSING : constant Java.Int;

   --  final
   INTERNAL_FRAME_CLOSED : constant Java.Int;

   --  final
   INTERNAL_FRAME_ICONIFIED : constant Java.Int;

   --  final
   INTERNAL_FRAME_DEICONIFIED : constant Java.Int;

   --  final
   INTERNAL_FRAME_ACTIVATED : constant Java.Int;

   --  final
   INTERNAL_FRAME_DEACTIVATED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InternalFrameEvent);
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetInternalFrame, "getInternalFrame");
   pragma Import (Java, INTERNAL_FRAME_FIRST, "INTERNAL_FRAME_FIRST");
   pragma Import (Java, INTERNAL_FRAME_LAST, "INTERNAL_FRAME_LAST");
   pragma Import (Java, INTERNAL_FRAME_OPENED, "INTERNAL_FRAME_OPENED");
   pragma Import (Java, INTERNAL_FRAME_CLOSING, "INTERNAL_FRAME_CLOSING");
   pragma Import (Java, INTERNAL_FRAME_CLOSED, "INTERNAL_FRAME_CLOSED");
   pragma Import (Java, INTERNAL_FRAME_ICONIFIED, "INTERNAL_FRAME_ICONIFIED");
   pragma Import (Java, INTERNAL_FRAME_DEICONIFIED, "INTERNAL_FRAME_DEICONIFIED");
   pragma Import (Java, INTERNAL_FRAME_ACTIVATED, "INTERNAL_FRAME_ACTIVATED");
   pragma Import (Java, INTERNAL_FRAME_DEACTIVATED, "INTERNAL_FRAME_DEACTIVATED");

end Javax.Swing.Event.InternalFrameEvent;
pragma Import (Java, Javax.Swing.Event.InternalFrameEvent, "javax.swing.event.InternalFrameEvent");
pragma Extensions_Allowed (Off);
