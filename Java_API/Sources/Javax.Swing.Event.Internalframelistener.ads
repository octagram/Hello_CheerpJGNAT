pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.InternalFrameEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Swing.Event.InternalFrameListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InternalFrameOpened (This : access Typ;
                                  P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class) is abstract;

   procedure InternalFrameClosing (This : access Typ;
                                   P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class) is abstract;

   procedure InternalFrameClosed (This : access Typ;
                                  P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class) is abstract;

   procedure InternalFrameIconified (This : access Typ;
                                     P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class) is abstract;

   procedure InternalFrameDeiconified (This : access Typ;
                                       P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class) is abstract;

   procedure InternalFrameActivated (This : access Typ;
                                     P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class) is abstract;

   procedure InternalFrameDeactivated (This : access Typ;
                                       P1_InternalFrameEvent : access Standard.Javax.Swing.Event.InternalFrameEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, InternalFrameOpened, "internalFrameOpened");
   pragma Export (Java, InternalFrameClosing, "internalFrameClosing");
   pragma Export (Java, InternalFrameClosed, "internalFrameClosed");
   pragma Export (Java, InternalFrameIconified, "internalFrameIconified");
   pragma Export (Java, InternalFrameDeiconified, "internalFrameDeiconified");
   pragma Export (Java, InternalFrameActivated, "internalFrameActivated");
   pragma Export (Java, InternalFrameDeactivated, "internalFrameDeactivated");

end Javax.Swing.Event.InternalFrameListener;
pragma Import (Java, Javax.Swing.Event.InternalFrameListener, "javax.swing.event.InternalFrameListener");
pragma Extensions_Allowed (Off);
