pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.ListDataEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return Java.Int;

   function GetIndex0 (This : access Typ)
                       return Java.Int;

   function GetIndex1 (This : access Typ)
                       return Java.Int;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ListDataEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CONTENTS_CHANGED : constant Java.Int;

   --  final
   INTERVAL_ADDED : constant Java.Int;

   --  final
   INTERVAL_REMOVED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetIndex0, "getIndex0");
   pragma Import (Java, GetIndex1, "getIndex1");
   pragma Java_Constructor (New_ListDataEvent);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, CONTENTS_CHANGED, "CONTENTS_CHANGED");
   pragma Import (Java, INTERVAL_ADDED, "INTERVAL_ADDED");
   pragma Import (Java, INTERVAL_REMOVED, "INTERVAL_REMOVED");

end Javax.Swing.Event.ListDataEvent;
pragma Import (Java, Javax.Swing.Event.ListDataEvent, "javax.swing.event.ListDataEvent");
pragma Extensions_Allowed (Off);
