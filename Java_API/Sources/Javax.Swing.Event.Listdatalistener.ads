pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ListDataEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Swing.Event.ListDataListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure IntervalAdded (This : access Typ;
                            P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class) is abstract;

   procedure IntervalRemoved (This : access Typ;
                              P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class) is abstract;

   procedure ContentsChanged (This : access Typ;
                              P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IntervalAdded, "intervalAdded");
   pragma Export (Java, IntervalRemoved, "intervalRemoved");
   pragma Export (Java, ContentsChanged, "contentsChanged");

end Javax.Swing.Event.ListDataListener;
pragma Import (Java, Javax.Swing.Event.ListDataListener, "javax.swing.event.ListDataListener");
pragma Extensions_Allowed (Off);
