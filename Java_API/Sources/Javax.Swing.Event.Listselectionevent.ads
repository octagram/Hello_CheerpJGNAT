pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.ListSelectionEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ListSelectionEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                    P2_Int : Java.Int;
                                    P3_Int : Java.Int;
                                    P4_Boolean : Java.Boolean; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFirstIndex (This : access Typ)
                           return Java.Int;

   function GetLastIndex (This : access Typ)
                          return Java.Int;

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ListSelectionEvent);
   pragma Import (Java, GetFirstIndex, "getFirstIndex");
   pragma Import (Java, GetLastIndex, "getLastIndex");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Event.ListSelectionEvent;
pragma Import (Java, Javax.Swing.Event.ListSelectionEvent, "javax.swing.event.ListSelectionEvent");
pragma Extensions_Allowed (Off);
