pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.MenuElement;
limited with Javax.Swing.MenuSelectionManager;
with Java.Awt.Event.MouseEvent;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Event.MenuDragMouseEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.MouseEvent.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MenuDragMouseEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                    P2_Int : Java.Int;
                                    P3_Long : Java.Long;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int;
                                    P7_Int : Java.Int;
                                    P8_Boolean : Java.Boolean;
                                    P9_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                                    P10_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_MenuDragMouseEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                    P2_Int : Java.Int;
                                    P3_Long : Java.Long;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int;
                                    P7_Int : Java.Int;
                                    P8_Int : Java.Int;
                                    P9_Int : Java.Int;
                                    P10_Boolean : Java.Boolean;
                                    P11_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                                    P12_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPath (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function GetMenuSelectionManager (This : access Typ)
                                     return access Javax.Swing.MenuSelectionManager.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MenuDragMouseEvent);
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, GetMenuSelectionManager, "getMenuSelectionManager");

end Javax.Swing.Event.MenuDragMouseEvent;
pragma Import (Java, Javax.Swing.Event.MenuDragMouseEvent, "javax.swing.event.MenuDragMouseEvent");
pragma Extensions_Allowed (Off);
