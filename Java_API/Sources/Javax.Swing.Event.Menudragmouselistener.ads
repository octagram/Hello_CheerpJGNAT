pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.MenuDragMouseEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Swing.Event.MenuDragMouseListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MenuDragMouseEntered (This : access Typ;
                                   P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class) is abstract;

   procedure MenuDragMouseExited (This : access Typ;
                                  P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class) is abstract;

   procedure MenuDragMouseDragged (This : access Typ;
                                   P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class) is abstract;

   procedure MenuDragMouseReleased (This : access Typ;
                                    P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, MenuDragMouseEntered, "menuDragMouseEntered");
   pragma Export (Java, MenuDragMouseExited, "menuDragMouseExited");
   pragma Export (Java, MenuDragMouseDragged, "menuDragMouseDragged");
   pragma Export (Java, MenuDragMouseReleased, "menuDragMouseReleased");

end Javax.Swing.Event.MenuDragMouseListener;
pragma Import (Java, Javax.Swing.Event.MenuDragMouseListener, "javax.swing.event.MenuDragMouseListener");
pragma Extensions_Allowed (Off);
