pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.MenuEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Swing.Event.MenuListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MenuSelected (This : access Typ;
                           P1_MenuEvent : access Standard.Javax.Swing.Event.MenuEvent.Typ'Class) is abstract;

   procedure MenuDeselected (This : access Typ;
                             P1_MenuEvent : access Standard.Javax.Swing.Event.MenuEvent.Typ'Class) is abstract;

   procedure MenuCanceled (This : access Typ;
                           P1_MenuEvent : access Standard.Javax.Swing.Event.MenuEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, MenuSelected, "menuSelected");
   pragma Export (Java, MenuDeselected, "menuDeselected");
   pragma Export (Java, MenuCanceled, "menuCanceled");

end Javax.Swing.Event.MenuListener;
pragma Import (Java, Javax.Swing.Event.MenuListener, "javax.swing.event.MenuListener");
pragma Extensions_Allowed (Off);
