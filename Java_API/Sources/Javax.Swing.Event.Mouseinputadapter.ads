pragma Extensions_Allowed (On);
with Java.Awt.Event.MouseAdapter;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Lang.Object;
with Javax.Swing.Event.MouseInputListener;

package Javax.Swing.Event.MouseInputAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref;
            MouseInputListener_I : Javax.Swing.Event.MouseInputListener.Ref)
    is abstract new Java.Awt.Event.MouseAdapter.Typ(MouseListener_I,
                                                    MouseMotionListener_I,
                                                    MouseWheelListener_I)
      with null record;

   function New_MouseInputAdapter (This : Ref := null)
                                   return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MouseInputAdapter);

end Javax.Swing.Event.MouseInputAdapter;
pragma Import (Java, Javax.Swing.Event.MouseInputAdapter, "javax.swing.event.MouseInputAdapter");
pragma Extensions_Allowed (Off);
