pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.RowSorterEvent.Type_K;
limited with Javax.Swing.RowSorter;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.RowSorterEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RowSorterEvent (P1_RowSorter : access Standard.Javax.Swing.RowSorter.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_RowSorterEvent (P1_RowSorter : access Standard.Javax.Swing.RowSorter.Typ'Class;
                                P2_Type_K : access Standard.Javax.Swing.Event.RowSorterEvent.Type_K.Typ'Class;
                                P3_Int_Arr : Java.Int_Arr; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSource (This : access Typ)
                       return access Javax.Swing.RowSorter.Typ'Class;

   function GetType (This : access Typ)
                     return access Javax.Swing.Event.RowSorterEvent.Type_K.Typ'Class;

   function ConvertPreviousRowIndexToModel (This : access Typ;
                                            P1_Int : Java.Int)
                                            return Java.Int;

   function GetPreviousRowCount (This : access Typ)
                                 return Java.Int;

   function GetSource (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RowSorterEvent);
   pragma Import (Java, GetSource, "getSource");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, ConvertPreviousRowIndexToModel, "convertPreviousRowIndexToModel");
   pragma Import (Java, GetPreviousRowCount, "getPreviousRowCount");

end Javax.Swing.Event.RowSorterEvent;
pragma Import (Java, Javax.Swing.Event.RowSorterEvent, "javax.swing.event.RowSorterEvent");
pragma Extensions_Allowed (Off);
