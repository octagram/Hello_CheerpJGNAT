pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
with Java.Beans.PropertyChangeSupport;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Event.SwingPropertyChangeSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Beans.PropertyChangeSupport.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SwingPropertyChangeSupport (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_SwingPropertyChangeSupport (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                            P2_Boolean : Java.Boolean; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure FirePropertyChange (This : access Typ;
                                 P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  final
   function IsNotifyOnEDT (This : access Typ)
                           return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SwingPropertyChangeSupport);
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, IsNotifyOnEDT, "isNotifyOnEDT");

end Javax.Swing.Event.SwingPropertyChangeSupport;
pragma Import (Java, Javax.Swing.Event.SwingPropertyChangeSupport, "javax.swing.event.SwingPropertyChangeSupport");
pragma Extensions_Allowed (Off);
