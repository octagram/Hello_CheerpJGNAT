pragma Extensions_Allowed (On);
limited with Javax.Swing.Table.TableColumnModel;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.TableColumnModelEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      FromIndex : Java.Int;
      pragma Import (Java, FromIndex, "fromIndex");

      --  protected
      ToIndex : Java.Int;
      pragma Import (Java, ToIndex, "toIndex");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TableColumnModelEvent (P1_TableColumnModel : access Standard.Javax.Swing.Table.TableColumnModel.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFromIndex (This : access Typ)
                          return Java.Int;

   function GetToIndex (This : access Typ)
                        return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableColumnModelEvent);
   pragma Import (Java, GetFromIndex, "getFromIndex");
   pragma Import (Java, GetToIndex, "getToIndex");

end Javax.Swing.Event.TableColumnModelEvent;
pragma Import (Java, Javax.Swing.Event.TableColumnModelEvent, "javax.swing.event.TableColumnModelEvent");
pragma Extensions_Allowed (Off);
