pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ListSelectionEvent;
limited with Javax.Swing.Event.TableColumnModelEvent;
with Java.Lang.Object;
with Java.Util.EventListener;

package Javax.Swing.Event.TableColumnModelListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EventListener_I : Java.Util.EventListener.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ColumnAdded (This : access Typ;
                          P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class) is abstract;

   procedure ColumnRemoved (This : access Typ;
                            P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class) is abstract;

   procedure ColumnMoved (This : access Typ;
                          P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class) is abstract;

   procedure ColumnMarginChanged (This : access Typ;
                                  P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class) is abstract;

   procedure ColumnSelectionChanged (This : access Typ;
                                     P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ColumnAdded, "columnAdded");
   pragma Export (Java, ColumnRemoved, "columnRemoved");
   pragma Export (Java, ColumnMoved, "columnMoved");
   pragma Export (Java, ColumnMarginChanged, "columnMarginChanged");
   pragma Export (Java, ColumnSelectionChanged, "columnSelectionChanged");

end Javax.Swing.Event.TableColumnModelListener;
pragma Import (Java, Javax.Swing.Event.TableColumnModelListener, "javax.swing.event.TableColumnModelListener");
pragma Extensions_Allowed (Off);
