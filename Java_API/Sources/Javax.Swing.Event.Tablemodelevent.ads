pragma Extensions_Allowed (On);
limited with Javax.Swing.Table.TableModel;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.TableModelEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      type_K : Java.Int;
      pragma Import (Java, type_K, "type");

      --  protected
      FirstRow : Java.Int;
      pragma Import (Java, FirstRow, "firstRow");

      --  protected
      LastRow : Java.Int;
      pragma Import (Java, LastRow, "lastRow");

      --  protected
      Column : Java.Int;
      pragma Import (Java, Column, "column");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TableModelEvent (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_TableModelEvent (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class;
                                 P2_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_TableModelEvent (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_TableModelEvent (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_TableModelEvent (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFirstRow (This : access Typ)
                         return Java.Int;

   function GetLastRow (This : access Typ)
                        return Java.Int;

   function GetColumn (This : access Typ)
                       return Java.Int;

   function GetType (This : access Typ)
                     return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INSERT : constant Java.Int;

   --  final
   UPDATE : constant Java.Int;

   --  final
   DELETE : constant Java.Int;

   --  final
   HEADER_ROW : constant Java.Int;

   --  final
   ALL_COLUMNS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableModelEvent);
   pragma Import (Java, GetFirstRow, "getFirstRow");
   pragma Import (Java, GetLastRow, "getLastRow");
   pragma Import (Java, GetColumn, "getColumn");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, INSERT, "INSERT");
   pragma Import (Java, UPDATE, "UPDATE");
   pragma Import (Java, DELETE, "DELETE");
   pragma Import (Java, HEADER_ROW, "HEADER_ROW");
   pragma Import (Java, ALL_COLUMNS, "ALL_COLUMNS");

end Javax.Swing.Event.TableModelEvent;
pragma Import (Java, Javax.Swing.Event.TableModelEvent, "javax.swing.event.TableModelEvent");
pragma Extensions_Allowed (Off);
