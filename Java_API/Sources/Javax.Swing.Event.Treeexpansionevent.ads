pragma Extensions_Allowed (On);
limited with Javax.Swing.Tree.TreePath;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.TreeExpansionEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Path : access Javax.Swing.Tree.TreePath.Typ'Class;
      pragma Import (Java, Path, "path");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeExpansionEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                    P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPath (This : access Typ)
                     return access Javax.Swing.Tree.TreePath.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeExpansionEvent);
   pragma Import (Java, GetPath, "getPath");

end Javax.Swing.Event.TreeExpansionEvent;
pragma Import (Java, Javax.Swing.Event.TreeExpansionEvent, "javax.swing.event.TreeExpansionEvent");
pragma Extensions_Allowed (Off);
