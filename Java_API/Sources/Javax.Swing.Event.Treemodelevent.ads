pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Tree.TreePath;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.TreeModelEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Path : access Javax.Swing.Tree.TreePath.Typ'Class;
      pragma Import (Java, Path, "path");

      --  protected
      ChildIndices : Java.Int_Arr;
      pragma Import (Java, ChildIndices, "childIndices");

      --  protected
      Children : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Children, "children");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeModelEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                P3_Int_Arr : Java.Int_Arr;
                                P4_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   function New_TreeModelEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                P3_Int_Arr : Java.Int_Arr;
                                P4_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   function New_TreeModelEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   function New_TreeModelEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTreePath (This : access Typ)
                         return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetPath (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function GetChildren (This : access Typ)
                         return Standard.Java.Lang.Object.Ref;

   function GetChildIndices (This : access Typ)
                             return Java.Int_Arr;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeModelEvent);
   pragma Import (Java, GetTreePath, "getTreePath");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, GetChildren, "getChildren");
   pragma Import (Java, GetChildIndices, "getChildIndices");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Event.TreeModelEvent;
pragma Import (Java, Javax.Swing.Event.TreeModelEvent, "javax.swing.event.TreeModelEvent");
pragma Extensions_Allowed (Off);
