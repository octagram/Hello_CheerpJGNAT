pragma Extensions_Allowed (On);
limited with Javax.Swing.Tree.TreePath;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.EventObject;

package Javax.Swing.Event.TreeSelectionEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Util.EventObject.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Paths : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Paths, "paths");

      --  protected
      AreNew : Java.Boolean_Arr;
      pragma Import (Java, AreNew, "areNew");

      --  protected
      OldLeadSelectionPath : access Javax.Swing.Tree.TreePath.Typ'Class;
      pragma Import (Java, OldLeadSelectionPath, "oldLeadSelectionPath");

      --  protected
      NewLeadSelectionPath : access Javax.Swing.Tree.TreePath.Typ'Class;
      pragma Import (Java, NewLeadSelectionPath, "newLeadSelectionPath");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeSelectionEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                    P2_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj;
                                    P3_Boolean_Arr : Java.Boolean_Arr;
                                    P4_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                    P5_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_TreeSelectionEvent (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                    P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                    P3_Boolean : Java.Boolean;
                                    P4_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                    P5_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPaths (This : access Typ)
                      return Standard.Java.Lang.Object.Ref;

   function GetPath (This : access Typ)
                     return access Javax.Swing.Tree.TreePath.Typ'Class;

   function IsAddedPath (This : access Typ)
                         return Java.Boolean;

   function IsAddedPath (This : access Typ;
                         P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                         return Java.Boolean;

   function IsAddedPath (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;

   function GetOldLeadSelectionPath (This : access Typ)
                                     return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetNewLeadSelectionPath (This : access Typ)
                                     return access Javax.Swing.Tree.TreePath.Typ'Class;

   function CloneWithSource (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeSelectionEvent);
   pragma Import (Java, GetPaths, "getPaths");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, IsAddedPath, "isAddedPath");
   pragma Import (Java, GetOldLeadSelectionPath, "getOldLeadSelectionPath");
   pragma Import (Java, GetNewLeadSelectionPath, "getNewLeadSelectionPath");
   pragma Import (Java, CloneWithSource, "cloneWithSource");

end Javax.Swing.Event.TreeSelectionEvent;
pragma Import (Java, Javax.Swing.Event.TreeSelectionEvent, "javax.swing.event.TreeSelectionEvent");
pragma Extensions_Allowed (Off);
