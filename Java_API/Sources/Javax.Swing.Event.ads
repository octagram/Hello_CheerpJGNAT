pragma Extensions_Allowed (On);
package Javax.Swing.Event is
   pragma Preelaborate;
end Javax.Swing.Event;
pragma Import (Java, Javax.Swing.Event, "javax.swing.event");
pragma Extensions_Allowed (Off);
