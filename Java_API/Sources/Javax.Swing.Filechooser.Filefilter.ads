pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Filechooser.FileFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FileFilter (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function accept_K (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class)
                      return Java.Boolean is abstract;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileFilter);
   pragma Export (Java, accept_K, "accept");
   pragma Export (Java, GetDescription, "getDescription");

end Javax.Swing.Filechooser.FileFilter;
pragma Import (Java, Javax.Swing.Filechooser.FileFilter, "javax.swing.filechooser.FileFilter");
pragma Extensions_Allowed (Off);
