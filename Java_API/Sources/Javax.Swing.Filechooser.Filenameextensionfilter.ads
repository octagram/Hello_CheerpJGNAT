pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Swing.Filechooser.FileFilter;

package Javax.Swing.Filechooser.FileNameExtensionFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Filechooser.FileFilter.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FileNameExtensionFilter (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String_Arr : access Java.Lang.String.Arr_Obj; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function accept_K (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class)
                      return Java.Boolean;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetExtensions (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileNameExtensionFilter);
   pragma Import (Java, accept_K, "accept");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetExtensions, "getExtensions");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Filechooser.FileNameExtensionFilter;
pragma Import (Java, Javax.Swing.Filechooser.FileNameExtensionFilter, "javax.swing.filechooser.FileNameExtensionFilter");
pragma Extensions_Allowed (Off);
