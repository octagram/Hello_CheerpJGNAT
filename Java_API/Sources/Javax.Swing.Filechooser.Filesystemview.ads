pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.Boolean;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
with Java.Lang.Object;

package Javax.Swing.Filechooser.FileSystemView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FileSystemView (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFileSystemView return access Javax.Swing.Filechooser.FileSystemView.Typ'Class;

   function IsRoot (This : access Typ;
                    P1_File : access Standard.Java.Io.File.Typ'Class)
                    return Java.Boolean;

   function IsTraversable (This : access Typ;
                           P1_File : access Standard.Java.Io.File.Typ'Class)
                           return access Java.Lang.Boolean.Typ'Class;

   function GetSystemDisplayName (This : access Typ;
                                  P1_File : access Standard.Java.Io.File.Typ'Class)
                                  return access Java.Lang.String.Typ'Class;

   function GetSystemTypeDescription (This : access Typ;
                                      P1_File : access Standard.Java.Io.File.Typ'Class)
                                      return access Java.Lang.String.Typ'Class;

   function GetSystemIcon (This : access Typ;
                           P1_File : access Standard.Java.Io.File.Typ'Class)
                           return access Javax.Swing.Icon.Typ'Class;

   function IsParent (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class;
                      P2_File : access Standard.Java.Io.File.Typ'Class)
                      return Java.Boolean;

   function GetChild (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Io.File.Typ'Class;

   function IsFileSystem (This : access Typ;
                          P1_File : access Standard.Java.Io.File.Typ'Class)
                          return Java.Boolean;

   function CreateNewFolder (This : access Typ;
                             P1_File : access Standard.Java.Io.File.Typ'Class)
                             return access Java.Io.File.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function IsHiddenFile (This : access Typ;
                          P1_File : access Standard.Java.Io.File.Typ'Class)
                          return Java.Boolean;

   function IsFileSystemRoot (This : access Typ;
                              P1_File : access Standard.Java.Io.File.Typ'Class)
                              return Java.Boolean;

   function IsDrive (This : access Typ;
                     P1_File : access Standard.Java.Io.File.Typ'Class)
                     return Java.Boolean;

   function IsFloppyDrive (This : access Typ;
                           P1_File : access Standard.Java.Io.File.Typ'Class)
                           return Java.Boolean;

   function IsComputerNode (This : access Typ;
                            P1_File : access Standard.Java.Io.File.Typ'Class)
                            return Java.Boolean;

   function GetRoots (This : access Typ)
                      return Standard.Java.Lang.Object.Ref;

   function GetHomeDirectory (This : access Typ)
                              return access Java.Io.File.Typ'Class;

   function GetDefaultDirectory (This : access Typ)
                                 return access Java.Io.File.Typ'Class;

   function CreateFileObject (This : access Typ;
                              P1_File : access Standard.Java.Io.File.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Io.File.Typ'Class;

   function CreateFileObject (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Io.File.Typ'Class;

   function GetFiles (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class;
                      P2_Boolean : Java.Boolean)
                      return Standard.Java.Lang.Object.Ref;

   function GetParentDirectory (This : access Typ;
                                P1_File : access Standard.Java.Io.File.Typ'Class)
                                return access Java.Io.File.Typ'Class;

   --  protected
   function CreateFileSystemRoot (This : access Typ;
                                  P1_File : access Standard.Java.Io.File.Typ'Class)
                                  return access Java.Io.File.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileSystemView);
   pragma Export (Java, GetFileSystemView, "getFileSystemView");
   pragma Export (Java, IsRoot, "isRoot");
   pragma Export (Java, IsTraversable, "isTraversable");
   pragma Export (Java, GetSystemDisplayName, "getSystemDisplayName");
   pragma Export (Java, GetSystemTypeDescription, "getSystemTypeDescription");
   pragma Export (Java, GetSystemIcon, "getSystemIcon");
   pragma Export (Java, IsParent, "isParent");
   pragma Export (Java, GetChild, "getChild");
   pragma Export (Java, IsFileSystem, "isFileSystem");
   pragma Export (Java, CreateNewFolder, "createNewFolder");
   pragma Export (Java, IsHiddenFile, "isHiddenFile");
   pragma Export (Java, IsFileSystemRoot, "isFileSystemRoot");
   pragma Export (Java, IsDrive, "isDrive");
   pragma Export (Java, IsFloppyDrive, "isFloppyDrive");
   pragma Export (Java, IsComputerNode, "isComputerNode");
   pragma Export (Java, GetRoots, "getRoots");
   pragma Export (Java, GetHomeDirectory, "getHomeDirectory");
   pragma Export (Java, GetDefaultDirectory, "getDefaultDirectory");
   pragma Export (Java, CreateFileObject, "createFileObject");
   pragma Export (Java, GetFiles, "getFiles");
   pragma Export (Java, GetParentDirectory, "getParentDirectory");
   pragma Export (Java, CreateFileSystemRoot, "createFileSystemRoot");

end Javax.Swing.Filechooser.FileSystemView;
pragma Import (Java, Javax.Swing.Filechooser.FileSystemView, "javax.swing.filechooser.FileSystemView");
pragma Extensions_Allowed (Off);
