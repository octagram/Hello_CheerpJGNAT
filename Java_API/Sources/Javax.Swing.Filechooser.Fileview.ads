pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.Boolean;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
with Java.Lang.Object;

package Javax.Swing.Filechooser.FileView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FileView (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ;
                     P1_File : access Standard.Java.Io.File.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ;
                            P1_File : access Standard.Java.Io.File.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetTypeDescription (This : access Typ;
                                P1_File : access Standard.Java.Io.File.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function GetIcon (This : access Typ;
                     P1_File : access Standard.Java.Io.File.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   function IsTraversable (This : access Typ;
                           P1_File : access Standard.Java.Io.File.Typ'Class)
                           return access Java.Lang.Boolean.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileView);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetTypeDescription, "getTypeDescription");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, IsTraversable, "isTraversable");

end Javax.Swing.Filechooser.FileView;
pragma Import (Java, Javax.Swing.Filechooser.FileView, "javax.swing.filechooser.FileView");
pragma Extensions_Allowed (Off);
