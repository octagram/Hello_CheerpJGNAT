pragma Extensions_Allowed (On);
package Javax.Swing.Filechooser is
   pragma Preelaborate;
end Javax.Swing.Filechooser;
pragma Import (Java, Javax.Swing.Filechooser, "javax.swing.filechooser");
pragma Extensions_Allowed (Off);
