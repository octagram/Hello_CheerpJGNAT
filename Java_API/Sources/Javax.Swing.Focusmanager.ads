pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.DefaultKeyboardFocusManager;
with Java.Awt.KeyEventDispatcher;
with Java.Awt.KeyEventPostProcessor;
with Java.Lang.Object;

package Javax.Swing.FocusManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeyEventDispatcher_I : Java.Awt.KeyEventDispatcher.Ref;
            KeyEventPostProcessor_I : Java.Awt.KeyEventPostProcessor.Ref)
    is abstract new Java.Awt.DefaultKeyboardFocusManager.Typ(KeyEventDispatcher_I,
                                                             KeyEventPostProcessor_I)
      with null record;

   function New_FocusManager (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCurrentManager return access Javax.Swing.FocusManager.Typ'Class;

   procedure SetCurrentManager (P1_FocusManager : access Standard.Javax.Swing.FocusManager.Typ'Class);
   --  can raise Java.Lang.SecurityException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FOCUS_MANAGER_CLASS_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FocusManager);
   pragma Import (Java, GetCurrentManager, "getCurrentManager");
   pragma Import (Java, SetCurrentManager, "setCurrentManager");
   pragma Import (Java, FOCUS_MANAGER_CLASS_PROPERTY, "FOCUS_MANAGER_CLASS_PROPERTY");

end Javax.Swing.FocusManager;
pragma Import (Java, Javax.Swing.FocusManager, "javax.swing.FocusManager");
pragma Extensions_Allowed (Off);
