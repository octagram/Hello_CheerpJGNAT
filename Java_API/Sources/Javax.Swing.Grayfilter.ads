pragma Extensions_Allowed (On);
with Java.Awt.Image.ImageConsumer;
with Java.Awt.Image.RGBImageFilter;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Awt.Image;

package Javax.Swing.GrayFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ImageConsumer_I : Java.Awt.Image.ImageConsumer.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Awt.Image.RGBImageFilter.Typ(ImageConsumer_I,
                                             Cloneable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateDisabledImage (P1_Image : access Standard.Java.Awt.Image.Typ'Class)
                                 return access Java.Awt.Image.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GrayFilter (P1_Boolean : Java.Boolean;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function FilterRGB (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateDisabledImage, "createDisabledImage");
   pragma Java_Constructor (New_GrayFilter);
   pragma Import (Java, FilterRGB, "filterRGB");

end Javax.Swing.GrayFilter;
pragma Import (Java, Javax.Swing.GrayFilter, "javax.swing.GrayFilter");
pragma Extensions_Allowed (Off);
