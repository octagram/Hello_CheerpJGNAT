pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
with Java.Lang.Object;

package Javax.Swing.GroupLayout.Group is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddGroup (This : access Typ;
                      P1_Group : access Standard.Javax.Swing.GroupLayout.Group.Typ'Class)
                      return access Javax.Swing.GroupLayout.Group.Typ'Class;

   function AddComponent (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                          return access Javax.Swing.GroupLayout.Group.Typ'Class;

   function AddComponent (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int)
                          return access Javax.Swing.GroupLayout.Group.Typ'Class;

   function AddGap (This : access Typ;
                    P1_Int : Java.Int)
                    return access Javax.Swing.GroupLayout.Group.Typ'Class;

   function AddGap (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Javax.Swing.GroupLayout.Group.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, AddGroup, "addGroup");
   pragma Import (Java, AddComponent, "addComponent");
   pragma Import (Java, AddGap, "addGap");

end Javax.Swing.GroupLayout.Group;
pragma Import (Java, Javax.Swing.GroupLayout.Group, "javax.swing.GroupLayout$Group");
pragma Extensions_Allowed (Off);
