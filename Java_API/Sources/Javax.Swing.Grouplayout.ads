pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.Boolean;
limited with Java.Lang.String;
limited with Javax.Swing.GroupLayout.Alignment;
limited with Javax.Swing.GroupLayout.Group;
limited with Javax.Swing.GroupLayout.ParallelGroup;
limited with Javax.Swing.GroupLayout.SequentialGroup;
limited with Javax.Swing.LayoutStyle;
with Java.Awt.LayoutManager2;
with Java.Lang.Object;

package Javax.Swing.GroupLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GroupLayout (P1_Container : access Standard.Java.Awt.Container.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetHonorsVisibility (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function GetHonorsVisibility (This : access Typ)
                                 return Java.Boolean;

   procedure SetHonorsVisibility (This : access Typ;
                                  P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                  P2_Boolean : access Standard.Java.Lang.Boolean.Typ'Class);

   procedure SetAutoCreateGaps (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function GetAutoCreateGaps (This : access Typ)
                               return Java.Boolean;

   procedure SetAutoCreateContainerGaps (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   function GetAutoCreateContainerGaps (This : access Typ)
                                        return Java.Boolean;

   procedure SetHorizontalGroup (This : access Typ;
                                 P1_Group : access Standard.Javax.Swing.GroupLayout.Group.Typ'Class);

   procedure SetVerticalGroup (This : access Typ;
                               P1_Group : access Standard.Javax.Swing.GroupLayout.Group.Typ'Class);

   function CreateSequentialGroup (This : access Typ)
                                   return access Javax.Swing.GroupLayout.SequentialGroup.Typ'Class;

   function CreateParallelGroup (This : access Typ)
                                 return access Javax.Swing.GroupLayout.ParallelGroup.Typ'Class;

   function CreateParallelGroup (This : access Typ;
                                 P1_Alignment : access Standard.Javax.Swing.GroupLayout.Alignment.Typ'Class)
                                 return access Javax.Swing.GroupLayout.ParallelGroup.Typ'Class;

   function CreateParallelGroup (This : access Typ;
                                 P1_Alignment : access Standard.Javax.Swing.GroupLayout.Alignment.Typ'Class;
                                 P2_Boolean : Java.Boolean)
                                 return access Javax.Swing.GroupLayout.ParallelGroup.Typ'Class;

   function CreateBaselineGroup (This : access Typ;
                                 P1_Boolean : Java.Boolean;
                                 P2_Boolean : Java.Boolean)
                                 return access Javax.Swing.GroupLayout.ParallelGroup.Typ'Class;

   procedure LinkSize (This : access Typ;
                       P1_Component_Arr : access Java.Awt.Component.Arr_Obj);

   procedure LinkSize (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Component_Arr : access Java.Awt.Component.Arr_Obj);

   procedure Replace (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetLayoutStyle (This : access Typ;
                             P1_LayoutStyle : access Standard.Javax.Swing.LayoutStyle.Typ'Class);

   function GetLayoutStyle (This : access Typ)
                            return access Javax.Swing.LayoutStyle.Typ'Class;

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function MaximumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function GetLayoutAlignmentX (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   function GetLayoutAlignmentY (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   procedure InvalidateLayout (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_SIZE : constant Java.Int;

   --  final
   PREFERRED_SIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GroupLayout);
   pragma Import (Java, SetHonorsVisibility, "setHonorsVisibility");
   pragma Import (Java, GetHonorsVisibility, "getHonorsVisibility");
   pragma Import (Java, SetAutoCreateGaps, "setAutoCreateGaps");
   pragma Import (Java, GetAutoCreateGaps, "getAutoCreateGaps");
   pragma Import (Java, SetAutoCreateContainerGaps, "setAutoCreateContainerGaps");
   pragma Import (Java, GetAutoCreateContainerGaps, "getAutoCreateContainerGaps");
   pragma Import (Java, SetHorizontalGroup, "setHorizontalGroup");
   pragma Import (Java, SetVerticalGroup, "setVerticalGroup");
   pragma Import (Java, CreateSequentialGroup, "createSequentialGroup");
   pragma Import (Java, CreateParallelGroup, "createParallelGroup");
   pragma Import (Java, CreateBaselineGroup, "createBaselineGroup");
   pragma Import (Java, LinkSize, "linkSize");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, SetLayoutStyle, "setLayoutStyle");
   pragma Import (Java, GetLayoutStyle, "getLayoutStyle");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, MaximumLayoutSize, "maximumLayoutSize");
   pragma Import (Java, GetLayoutAlignmentX, "getLayoutAlignmentX");
   pragma Import (Java, GetLayoutAlignmentY, "getLayoutAlignmentY");
   pragma Import (Java, InvalidateLayout, "invalidateLayout");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, DEFAULT_SIZE, "DEFAULT_SIZE");
   pragma Import (Java, PREFERRED_SIZE, "PREFERRED_SIZE");

end Javax.Swing.GroupLayout;
pragma Import (Java, Javax.Swing.GroupLayout, "javax.swing.GroupLayout");
pragma Extensions_Allowed (Off);
