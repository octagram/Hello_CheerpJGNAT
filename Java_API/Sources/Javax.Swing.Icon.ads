pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
with Java.Lang.Object;

package Javax.Swing.Icon is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintIcon (This : access Typ;
                        P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int) is abstract;

   function GetIconWidth (This : access Typ)
                          return Java.Int is abstract;

   function GetIconHeight (This : access Typ)
                           return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, PaintIcon, "paintIcon");
   pragma Export (Java, GetIconWidth, "getIconWidth");
   pragma Export (Java, GetIconHeight, "getIconHeight");

end Javax.Swing.Icon;
pragma Import (Java, Javax.Swing.Icon, "javax.swing.Icon");
pragma Extensions_Allowed (Off);
