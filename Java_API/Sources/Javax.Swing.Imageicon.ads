pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Image;
limited with Java.Awt.Image.ImageObserver;
limited with Java.Awt.MediaTracker;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Accessibility.AccessibleContext;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Icon;

package Javax.Swing.ImageIcon is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Icon_I : Javax.Swing.Icon.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImageIcon (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (P1_Image : access Standard.Java.Awt.Image.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (P1_Byte_Arr : Java.Byte_Arr;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (P1_Byte_Arr : Java.Byte_Arr; 
                           This : Ref := null)
                           return Ref;

   function New_ImageIcon (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure LoadImage (This : access Typ;
                        P1_Image : access Standard.Java.Awt.Image.Typ'Class);

   function GetImageLoadStatus (This : access Typ)
                                return Java.Int;

   function GetImage (This : access Typ)
                      return access Java.Awt.Image.Typ'Class;

   procedure SetImage (This : access Typ;
                       P1_Image : access Standard.Java.Awt.Image.Typ'Class);

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetDescription (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   procedure PaintIcon (This : access Typ;
                        P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   function GetIconWidth (This : access Typ)
                          return Java.Int;

   function GetIconHeight (This : access Typ)
                           return Java.Int;

   procedure SetImageObserver (This : access Typ;
                               P1_ImageObserver : access Standard.Java.Awt.Image.ImageObserver.Typ'Class);

   function GetImageObserver (This : access Typ)
                              return access Java.Awt.Image.ImageObserver.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   Component : access Java.Awt.Component.Typ'Class;

   --  protected  final
   Tracker : access Java.Awt.MediaTracker.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageIcon);
   pragma Import (Java, LoadImage, "loadImage");
   pragma Import (Java, GetImageLoadStatus, "getImageLoadStatus");
   pragma Import (Java, GetImage, "getImage");
   pragma Import (Java, SetImage, "setImage");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, SetDescription, "setDescription");
   pragma Import (Java, PaintIcon, "paintIcon");
   pragma Import (Java, GetIconWidth, "getIconWidth");
   pragma Import (Java, GetIconHeight, "getIconHeight");
   pragma Import (Java, SetImageObserver, "setImageObserver");
   pragma Import (Java, GetImageObserver, "getImageObserver");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, Component, "component");
   pragma Import (Java, Tracker, "tracker");

end Javax.Swing.ImageIcon;
pragma Import (Java, Javax.Swing.ImageIcon, "javax.swing.ImageIcon");
pragma Extensions_Allowed (Off);
