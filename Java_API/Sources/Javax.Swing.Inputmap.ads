pragma Extensions_Allowed (On);
limited with Javax.Swing.KeyStroke;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.InputMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InputMap (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_InputMap : access Standard.Javax.Swing.InputMap.Typ'Class);

   function GetParent (This : access Typ)
                       return access Javax.Swing.InputMap.Typ'Class;

   procedure Put (This : access Typ;
                  P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function Get (This : access Typ;
                 P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure Remove (This : access Typ;
                     P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class);

   procedure Clear (This : access Typ);

   function Keys (This : access Typ)
                  return Standard.Java.Lang.Object.Ref;

   function Size (This : access Typ)
                  return Java.Int;

   function AllKeys (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputMap);
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, Put, "put");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, Keys, "keys");
   pragma Import (Java, Size, "size");
   pragma Import (Java, AllKeys, "allKeys");

end Javax.Swing.InputMap;
pragma Import (Java, Javax.Swing.InputMap, "javax.swing.InputMap");
pragma Extensions_Allowed (Off);
