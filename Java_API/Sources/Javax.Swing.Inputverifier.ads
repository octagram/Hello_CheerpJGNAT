pragma Extensions_Allowed (On);
limited with Javax.Swing.JComponent;
with Java.Lang.Object;

package Javax.Swing.InputVerifier is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_InputVerifier (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Verify (This : access Typ;
                    P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                    return Java.Boolean is abstract;

   function ShouldYieldFocus (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputVerifier);
   pragma Export (Java, Verify, "verify");
   pragma Export (Java, ShouldYieldFocus, "shouldYieldFocus");

end Javax.Swing.InputVerifier;
pragma Import (Java, Javax.Swing.InputVerifier, "javax.swing.InputVerifier");
pragma Extensions_Allowed (Off);
