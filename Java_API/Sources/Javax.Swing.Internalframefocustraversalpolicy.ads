pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.JInternalFrame;
with Java.Awt.FocusTraversalPolicy;
with Java.Lang.Object;

package Javax.Swing.InternalFrameFocusTraversalPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Awt.FocusTraversalPolicy.Typ
      with null record;

   function New_InternalFrameFocusTraversalPolicy (This : Ref := null)
                                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInitialComponent (This : access Typ;
                                 P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                                 return access Java.Awt.Component.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InternalFrameFocusTraversalPolicy);
   pragma Import (Java, GetInitialComponent, "getInitialComponent");

end Javax.Swing.InternalFrameFocusTraversalPolicy;
pragma Import (Java, Javax.Swing.InternalFrameFocusTraversalPolicy, "javax.swing.InternalFrameFocusTraversalPolicy");
pragma Extensions_Allowed (Off);
