pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Event.ActionListener;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Colorchooser.AbstractColorChooserPanel;
limited with Javax.Swing.Colorchooser.ColorSelectionModel;
limited with Javax.Swing.JDialog;
limited with Javax.Swing.Plaf.ColorChooserUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JColorChooser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with null record;
      
      ------------------------
      -- Field Declarations --
      ------------------------

      -- --  protected
      -- AccessibleContext : access Javax.Accessibility.AccessibleContext.Typ'Class;
      -- pragma Import (Java, AccessibleContext, "accessibleContext");

   -- end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ShowDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_Color : access Standard.Java.Awt.Color.Typ'Class)
                        return access Java.Awt.Color.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function CreateDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_Boolean : Java.Boolean;
                          P4_JColorChooser : access Standard.Javax.Swing.JColorChooser.Typ'Class;
                          P5_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class;
                          P6_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class)
                          return access Javax.Swing.JDialog.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JColorChooser (This : Ref := null)
                               return Ref;

   function New_JColorChooser (P1_Color : access Standard.Java.Awt.Color.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_JColorChooser (P1_ColorSelectionModel : access Standard.Javax.Swing.Colorchooser.ColorSelectionModel.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ColorChooserUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_ColorChooserUI : access Standard.Javax.Swing.Plaf.ColorChooserUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetColor (This : access Typ)
                      return access Java.Awt.Color.Typ'Class;

   procedure SetColor (This : access Typ;
                       P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure SetColor (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int);

   procedure SetColor (This : access Typ;
                       P1_Int : Java.Int);

   procedure SetDragEnabled (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetDragEnabled (This : access Typ)
                            return Java.Boolean;

   procedure SetPreviewPanel (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreviewPanel (This : access Typ)
                             return access Javax.Swing.JComponent.Typ'Class;

   procedure AddChooserPanel (This : access Typ;
                              P1_AbstractColorChooserPanel : access Standard.Javax.Swing.Colorchooser.AbstractColorChooserPanel.Typ'Class);

   function RemoveChooserPanel (This : access Typ;
                                P1_AbstractColorChooserPanel : access Standard.Javax.Swing.Colorchooser.AbstractColorChooserPanel.Typ'Class)
                                return access Javax.Swing.Colorchooser.AbstractColorChooserPanel.Typ'Class;

   procedure SetChooserPanels (This : access Typ;
                               P1_AbstractColorChooserPanel_Arr : access Javax.Swing.Colorchooser.AbstractColorChooserPanel.Arr_Obj);

   function GetChooserPanels (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.Colorchooser.ColorSelectionModel.Typ'Class;

   procedure SetSelectionModel (This : access Typ;
                                P1_ColorSelectionModel : access Standard.Javax.Swing.Colorchooser.ColorSelectionModel.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SELECTION_MODEL_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   PREVIEW_PANEL_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CHOOSER_PANELS_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ShowDialog, "showDialog");
   pragma Import (Java, CreateDialog, "createDialog");
   pragma Java_Constructor (New_JColorChooser);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetColor, "getColor");
   pragma Import (Java, SetColor, "setColor");
   pragma Import (Java, SetDragEnabled, "setDragEnabled");
   pragma Import (Java, GetDragEnabled, "getDragEnabled");
   pragma Import (Java, SetPreviewPanel, "setPreviewPanel");
   pragma Import (Java, GetPreviewPanel, "getPreviewPanel");
   pragma Import (Java, AddChooserPanel, "addChooserPanel");
   pragma Import (Java, RemoveChooserPanel, "removeChooserPanel");
   pragma Import (Java, SetChooserPanels, "setChooserPanels");
   pragma Import (Java, GetChooserPanels, "getChooserPanels");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, SELECTION_MODEL_PROPERTY, "SELECTION_MODEL_PROPERTY");
   pragma Import (Java, PREVIEW_PANEL_PROPERTY, "PREVIEW_PANEL_PROPERTY");
   pragma Import (Java, CHOOSER_PANELS_PROPERTY, "CHOOSER_PANELS_PROPERTY");

end Javax.Swing.JColorChooser;
pragma Import (Java, Javax.Swing.JColorChooser, "javax.swing.JColorChooser");
pragma Extensions_Allowed (Off);
