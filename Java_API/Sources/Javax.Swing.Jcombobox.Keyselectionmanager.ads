pragma Extensions_Allowed (On);
limited with Javax.Swing.ComboBoxModel;
with Java.Lang.Object;

package Javax.Swing.JComboBox.KeySelectionManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function SelectionForKey (This : access Typ;
                             P1_Char : Java.Char;
                             P2_ComboBoxModel : access Standard.Javax.Swing.ComboBoxModel.Typ'Class)
                             return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SelectionForKey, "selectionForKey");

end Javax.Swing.JComboBox.KeySelectionManager;
pragma Import (Java, Javax.Swing.JComboBox.KeySelectionManager, "javax.swing.JComboBox$KeySelectionManager");
pragma Extensions_Allowed (Off);
