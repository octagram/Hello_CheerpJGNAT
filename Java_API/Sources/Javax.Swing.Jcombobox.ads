pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ItemEvent;
limited with Java.Awt.Event.ItemListener;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.ComboBoxEditor;
limited with Javax.Swing.ComboBoxModel;
limited with Javax.Swing.Event.ListDataEvent;
limited with Javax.Swing.Event.PopupMenuListener;
limited with Javax.Swing.JComboBox.KeySelectionManager;
limited with Javax.Swing.ListCellRenderer;
limited with Javax.Swing.Plaf.ComboBoxUI;
with Java.Awt.Event.ActionListener;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Event.ListDataListener;
with Javax.Swing.JComponent;

package Javax.Swing.JComboBox is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ActionListener_I : Java.Awt.Event.ActionListener.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ListDataListener_I : Javax.Swing.Event.ListDataListener.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DataModel : access Javax.Swing.ComboBoxModel.Typ'Class;
      pragma Import (Java, DataModel, "dataModel");

      --  protected
      Renderer : access Javax.Swing.ListCellRenderer.Typ'Class;
      pragma Import (Java, Renderer, "renderer");

      --  protected
      Editor : access Javax.Swing.ComboBoxEditor.Typ'Class;
      pragma Import (Java, Editor, "editor");

      --  protected
      MaximumRowCount : Java.Int;
      pragma Import (Java, MaximumRowCount, "maximumRowCount");

      --  protected
      IsEditable : Java.Boolean;
      pragma Import (Java, IsEditable, "isEditable");

      --  protected
      KeySelectionManager : access Javax.Swing.JComboBox.KeySelectionManager.Typ'Class;
      pragma Import (Java, KeySelectionManager, "keySelectionManager");

      --  protected
      ActionCommand : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ActionCommand, "actionCommand");

      --  protected
      LightWeightPopupEnabled : Java.Boolean;
      pragma Import (Java, LightWeightPopupEnabled, "lightWeightPopupEnabled");

      --  protected
      SelectedItemReminder : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, SelectedItemReminder, "selectedItemReminder");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JComboBox (P1_ComboBoxModel : access Standard.Javax.Swing.ComboBoxModel.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JComboBox (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                           This : Ref := null)
                           return Ref;

   function New_JComboBox (P1_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JComboBox (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure InstallAncestorListener (This : access Typ);

   procedure SetUI (This : access Typ;
                    P1_ComboBoxUI : access Standard.Javax.Swing.Plaf.ComboBoxUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ComboBoxUI.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_ComboBoxModel : access Standard.Javax.Swing.ComboBoxModel.Typ'Class);

   function GetModel (This : access Typ)
                      return access Javax.Swing.ComboBoxModel.Typ'Class;

   procedure SetLightWeightPopupEnabled (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   function IsLightWeightPopupEnabled (This : access Typ)
                                       return Java.Boolean;

   procedure SetEditable (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function IsEditable (This : access Typ)
                        return Java.Boolean;

   procedure SetMaximumRowCount (This : access Typ;
                                 P1_Int : Java.Int);

   function GetMaximumRowCount (This : access Typ)
                                return Java.Int;

   procedure SetRenderer (This : access Typ;
                          P1_ListCellRenderer : access Standard.Javax.Swing.ListCellRenderer.Typ'Class);

   function GetRenderer (This : access Typ)
                         return access Javax.Swing.ListCellRenderer.Typ'Class;

   procedure SetEditor (This : access Typ;
                        P1_ComboBoxEditor : access Standard.Javax.Swing.ComboBoxEditor.Typ'Class);

   function GetEditor (This : access Typ)
                       return access Javax.Swing.ComboBoxEditor.Typ'Class;

   procedure SetSelectedItem (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetSelectedItem (This : access Typ)
                             return access Java.Lang.Object.Typ'Class;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int);

   function GetSelectedIndex (This : access Typ)
                              return Java.Int;

   function GetPrototypeDisplayValue (This : access Typ)
                                      return access Java.Lang.Object.Typ'Class;

   procedure SetPrototypeDisplayValue (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddItem (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure InsertItemAt (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Int : Java.Int);

   procedure RemoveItem (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveItemAt (This : access Typ;
                           P1_Int : Java.Int);

   procedure RemoveAllItems (This : access Typ);

   procedure ShowPopup (This : access Typ);

   procedure HidePopup (This : access Typ);

   procedure SetPopupVisible (This : access Typ;
                              P1_Boolean : Java.Boolean);

   function IsPopupVisible (This : access Typ)
                            return Java.Boolean;

   procedure AddItemListener (This : access Typ;
                              P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   procedure RemoveItemListener (This : access Typ;
                                 P1_ItemListener : access Standard.Java.Awt.Event.ItemListener.Typ'Class);

   function GetItemListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   procedure AddPopupMenuListener (This : access Typ;
                                   P1_PopupMenuListener : access Standard.Javax.Swing.Event.PopupMenuListener.Typ'Class);

   procedure RemovePopupMenuListener (This : access Typ;
                                      P1_PopupMenuListener : access Standard.Javax.Swing.Event.PopupMenuListener.Typ'Class);

   function GetPopupMenuListeners (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   procedure FirePopupMenuWillBecomeVisible (This : access Typ);

   procedure FirePopupMenuWillBecomeInvisible (This : access Typ);

   procedure FirePopupMenuCanceled (This : access Typ);

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   procedure SetAction (This : access Typ;
                        P1_Action : access Standard.Javax.Swing.Action.Typ'Class);

   function GetAction (This : access Typ)
                       return access Javax.Swing.Action.Typ'Class;

   --  protected
   procedure ConfigurePropertiesFromAction (This : access Typ;
                                            P1_Action : access Standard.Javax.Swing.Action.Typ'Class);

   --  protected
   function CreateActionPropertyChangeListener (This : access Typ;
                                                P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                                                return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure ActionPropertyChanged (This : access Typ;
                                    P1_Action : access Standard.Javax.Swing.Action.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure FireItemStateChanged (This : access Typ;
                                   P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);

   --  protected
   procedure FireActionEvent (This : access Typ);

   --  protected
   procedure SelectedItemChanged (This : access Typ);

   function GetSelectedObjects (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   procedure ContentsChanged (This : access Typ;
                              P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);

   procedure IntervalAdded (This : access Typ;
                            P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);

   procedure IntervalRemoved (This : access Typ;
                              P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);

   function SelectWithKeyChar (This : access Typ;
                               P1_Char : Java.Char)
                               return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure ConfigureEditor (This : access Typ;
                              P1_ComboBoxEditor : access Standard.Javax.Swing.ComboBoxEditor.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   procedure SetKeySelectionManager (This : access Typ;
                                     P1_KeySelectionManager : access Standard.Javax.Swing.JComboBox.KeySelectionManager.Typ'Class);

   function GetKeySelectionManager (This : access Typ)
                                    return access Javax.Swing.JComboBox.KeySelectionManager.Typ'Class;

   function GetItemCount (This : access Typ)
                          return Java.Int;

   function GetItemAt (This : access Typ;
                       P1_Int : Java.Int)
                       return access Java.Lang.Object.Typ'Class;

   --  protected
   function CreateDefaultKeySelectionManager (This : access Typ)
                                              return access Javax.Swing.JComboBox.KeySelectionManager.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JComboBox);
   pragma Import (Java, InstallAncestorListener, "installAncestorListener");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetLightWeightPopupEnabled, "setLightWeightPopupEnabled");
   pragma Import (Java, IsLightWeightPopupEnabled, "isLightWeightPopupEnabled");
   pragma Import (Java, SetEditable, "setEditable");
   pragma Import (Java, IsEditable, "isEditable");
   pragma Import (Java, SetMaximumRowCount, "setMaximumRowCount");
   pragma Import (Java, GetMaximumRowCount, "getMaximumRowCount");
   pragma Import (Java, SetRenderer, "setRenderer");
   pragma Import (Java, GetRenderer, "getRenderer");
   pragma Import (Java, SetEditor, "setEditor");
   pragma Import (Java, GetEditor, "getEditor");
   pragma Import (Java, SetSelectedItem, "setSelectedItem");
   pragma Import (Java, GetSelectedItem, "getSelectedItem");
   pragma Import (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Import (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Import (Java, GetPrototypeDisplayValue, "getPrototypeDisplayValue");
   pragma Import (Java, SetPrototypeDisplayValue, "setPrototypeDisplayValue");
   pragma Import (Java, AddItem, "addItem");
   pragma Import (Java, InsertItemAt, "insertItemAt");
   pragma Import (Java, RemoveItem, "removeItem");
   pragma Import (Java, RemoveItemAt, "removeItemAt");
   pragma Import (Java, RemoveAllItems, "removeAllItems");
   pragma Import (Java, ShowPopup, "showPopup");
   pragma Import (Java, HidePopup, "hidePopup");
   pragma Import (Java, SetPopupVisible, "setPopupVisible");
   pragma Import (Java, IsPopupVisible, "isPopupVisible");
   pragma Import (Java, AddItemListener, "addItemListener");
   pragma Import (Java, RemoveItemListener, "removeItemListener");
   pragma Import (Java, GetItemListeners, "getItemListeners");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, AddPopupMenuListener, "addPopupMenuListener");
   pragma Import (Java, RemovePopupMenuListener, "removePopupMenuListener");
   pragma Import (Java, GetPopupMenuListeners, "getPopupMenuListeners");
   pragma Import (Java, FirePopupMenuWillBecomeVisible, "firePopupMenuWillBecomeVisible");
   pragma Import (Java, FirePopupMenuWillBecomeInvisible, "firePopupMenuWillBecomeInvisible");
   pragma Import (Java, FirePopupMenuCanceled, "firePopupMenuCanceled");
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, SetAction, "setAction");
   pragma Import (Java, GetAction, "getAction");
   pragma Import (Java, ConfigurePropertiesFromAction, "configurePropertiesFromAction");
   pragma Import (Java, CreateActionPropertyChangeListener, "createActionPropertyChangeListener");
   pragma Import (Java, ActionPropertyChanged, "actionPropertyChanged");
   pragma Import (Java, FireItemStateChanged, "fireItemStateChanged");
   pragma Import (Java, FireActionEvent, "fireActionEvent");
   pragma Import (Java, SelectedItemChanged, "selectedItemChanged");
   pragma Import (Java, GetSelectedObjects, "getSelectedObjects");
   pragma Import (Java, ActionPerformed, "actionPerformed");
   pragma Import (Java, ContentsChanged, "contentsChanged");
   pragma Import (Java, IntervalAdded, "intervalAdded");
   pragma Import (Java, IntervalRemoved, "intervalRemoved");
   pragma Import (Java, SelectWithKeyChar, "selectWithKeyChar");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, ConfigureEditor, "configureEditor");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, SetKeySelectionManager, "setKeySelectionManager");
   pragma Import (Java, GetKeySelectionManager, "getKeySelectionManager");
   pragma Import (Java, GetItemCount, "getItemCount");
   pragma Import (Java, GetItemAt, "getItemAt");
   pragma Import (Java, CreateDefaultKeySelectionManager, "createDefaultKeySelectionManager");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JComboBox;
pragma Import (Java, Javax.Swing.JComboBox, "javax.swing.JComboBox");
pragma Extensions_Allowed (Off);
