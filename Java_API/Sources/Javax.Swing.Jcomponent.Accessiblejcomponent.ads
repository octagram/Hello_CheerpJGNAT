pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Cursor;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Accessibility.Accessible;
limited with Javax.Accessibility.AccessibleKeyBinding;
limited with Javax.Accessibility.AccessibleRole;
limited with Javax.Accessibility.AccessibleStateSet;
limited with Javax.Swing.Border.Border;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleComponent;
with Javax.Accessibility.AccessibleContext;
with Javax.Accessibility.AccessibleExtendedComponent;

package Javax.Swing.JComponent.AccessibleJComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AccessibleComponent_I : Javax.Accessibility.AccessibleComponent.Ref;
            AccessibleExtendedComponent_I : Javax.Accessibility.AccessibleExtendedComponent.Ref)
    is abstract new Javax.Accessibility.AccessibleContext.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      AccessibleFocusHandler : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, AccessibleFocusHandler, "accessibleFocusHandler");

   end record;

   --  protected
   function New_AccessibleJComponent (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  protected
   function GetBorderTitle (This : access Typ;
                            P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetAccessibleName (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   function GetAccessibleDescription (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetAccessibleRole (This : access Typ)
                               return access Javax.Accessibility.AccessibleRole.Typ'Class;

   function GetAccessibleStateSet (This : access Typ)
                                   return access Javax.Accessibility.AccessibleStateSet.Typ'Class;

   function GetAccessibleChildrenCount (This : access Typ)
                                        return Java.Int;

   function GetAccessibleChild (This : access Typ;
                                P1_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;

   function GetToolTipText (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetTitledBorderText (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetAccessibleKeyBinding (This : access Typ)
                                     return access Javax.Accessibility.AccessibleKeyBinding.Typ'Class;

   function GetAccessibleAt (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return access Javax.Accessibility.Accessible.Typ'Class;

   procedure RemoveFocusListener (This : access Typ;
                                  P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   procedure AddFocusListener (This : access Typ;
                               P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   procedure RequestFocus (This : access Typ);

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean;

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;

   procedure SetBounds (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   procedure SetLocation (This : access Typ;
                          P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   function GetLocationOnScreen (This : access Typ)
                                 return access Java.Awt.Point.Typ'Class;

   function Contains (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                      return Java.Boolean;

   function IsShowing (This : access Typ)
                       return Java.Boolean;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetCursor (This : access Typ)
                       return access Java.Awt.Cursor.Typ'Class;

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetForeground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetAccessibleComponent (This : access Typ)
                                    return access Javax.Accessibility.AccessibleComponent.Typ'Class;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;

   function GetAccessibleIndexInParent (This : access Typ)
                                        return Java.Int;

   function GetAccessibleParent (This : access Typ)
                                 return access Javax.Accessibility.Accessible.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleJComponent);
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetBorderTitle, "getBorderTitle");
   pragma Import (Java, GetAccessibleName, "getAccessibleName");
   pragma Import (Java, GetAccessibleDescription, "getAccessibleDescription");
   pragma Import (Java, GetAccessibleRole, "getAccessibleRole");
   pragma Import (Java, GetAccessibleStateSet, "getAccessibleStateSet");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, GetTitledBorderText, "getTitledBorderText");
   pragma Import (Java, GetAccessibleKeyBinding, "getAccessibleKeyBinding");
   pragma Import (Java, GetAccessibleAt, "getAccessibleAt");
   pragma Import (Java, RemoveFocusListener, "removeFocusListener");
   pragma Import (Java, AddFocusListener, "addFocusListener");
   pragma Import (Java, RequestFocus, "requestFocus");
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, SetBounds, "setBounds");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetLocationOnScreen, "getLocationOnScreen");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, IsShowing, "isShowing");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, SetCursor, "setCursor");
   pragma Import (Java, GetCursor, "getCursor");
   pragma Import (Java, SetForeground, "setForeground");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetAccessibleComponent, "getAccessibleComponent");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, GetAccessibleIndexInParent, "getAccessibleIndexInParent");
   pragma Import (Java, GetAccessibleParent, "getAccessibleParent");

end Javax.Swing.JComponent.AccessibleJComponent;
pragma Import (Java, Javax.Swing.JComponent.AccessibleJComponent, "javax.swing.JComponent$AccessibleJComponent");
pragma Extensions_Allowed (Off);
