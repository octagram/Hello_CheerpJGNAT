pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Beans.VetoableChangeListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Java.Util.Locale;
limited with Java.Util.Set;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.ActionMap;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.Event.AncestorListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.InputMap;
limited with Javax.Swing.InputVerifier;
limited with Javax.Swing.JPopupMenu;
limited with Javax.Swing.JRootPane;
limited with Javax.Swing.JToolTip;
limited with Javax.Swing.KeyStroke;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.TransferHandler;
with Java.Awt.Container;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.JComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Awt.Container.Typ(MenuContainer_I,
                                           ImageObserver_I,
                                           Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Ui : access Javax.Swing.Plaf.ComponentUI.Typ'Class;
      pragma Import (Java, Ui, "ui");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      AccessibleContext : access Javax.Accessibility.AccessibleContext.Typ'Class;
      pragma Import (Java, AccessibleContext, "accessibleContext");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetInheritsPopupMenu (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetInheritsPopupMenu (This : access Typ)
                                  return Java.Boolean;

   procedure SetComponentPopupMenu (This : access Typ;
                                    P1_JPopupMenu : access Standard.Javax.Swing.JPopupMenu.Typ'Class);

   function GetComponentPopupMenu (This : access Typ)
                                   return access Javax.Swing.JPopupMenu.Typ'Class;

   function New_JComponent (This : Ref := null)
                            return Ref;

   procedure UpdateUI (This : access Typ);

   --  protected
   procedure SetUI (This : access Typ;
                    P1_ComponentUI : access Standard.Javax.Swing.Plaf.ComponentUI.Typ'Class);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function GetComponentGraphics (This : access Typ;
                                  P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class)
                                  return access Java.Awt.Graphics.Typ'Class;

   --  protected
   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PaintChildren (This : access Typ;
                            P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PaintBorder (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PrintAll (This : access Typ;
                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Print (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PrintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PrintChildren (This : access Typ;
                            P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PrintBorder (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function IsPaintingTile (This : access Typ)
                            return Java.Boolean;

   --  final
   function IsPaintingForPrint (This : access Typ)
                                return Java.Boolean;

   procedure SetRequestFocusEnabled (This : access Typ;
                                     P1_Boolean : Java.Boolean);

   function IsRequestFocusEnabled (This : access Typ)
                                   return Java.Boolean;

   procedure RequestFocus (This : access Typ);

   function RequestFocus (This : access Typ;
                          P1_Boolean : Java.Boolean)
                          return Java.Boolean;

   function RequestFocusInWindow (This : access Typ)
                                  return Java.Boolean;

   --  protected
   function RequestFocusInWindow (This : access Typ;
                                  P1_Boolean : Java.Boolean)
                                  return Java.Boolean;

   procedure GrabFocus (This : access Typ);

   procedure SetVerifyInputWhenFocusTarget (This : access Typ;
                                            P1_Boolean : Java.Boolean);

   function GetVerifyInputWhenFocusTarget (This : access Typ)
                                           return Java.Boolean;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class;

   procedure SetPreferredSize (This : access Typ;
                               P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   procedure SetMaximumSize (This : access Typ;
                             P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetMaximumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   procedure SetMinimumSize (This : access Typ;
                             P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function Contains (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int)
                      return Java.Boolean;

   procedure SetBorder (This : access Typ;
                        P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class);

   function GetBorder (This : access Typ)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetInsets (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   function GetInsets (This : access Typ;
                       P1_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   function GetAlignmentY (This : access Typ)
                           return Java.Float;

   procedure SetAlignmentY (This : access Typ;
                            P1_Float : Java.Float);

   function GetAlignmentX (This : access Typ)
                           return Java.Float;

   procedure SetAlignmentX (This : access Typ;
                            P1_Float : Java.Float);

   procedure SetInputVerifier (This : access Typ;
                               P1_InputVerifier : access Standard.Javax.Swing.InputVerifier.Typ'Class);

   function GetInputVerifier (This : access Typ)
                              return access Javax.Swing.InputVerifier.Typ'Class;

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class;

   procedure SetDebugGraphicsOptions (This : access Typ;
                                      P1_Int : Java.Int);

   function GetDebugGraphicsOptions (This : access Typ)
                                     return Java.Int;

   procedure RegisterKeyboardAction (This : access Typ;
                                     P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                                     P4_Int : Java.Int);

   procedure RegisterKeyboardAction (This : access Typ;
                                     P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class;
                                     P2_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                                     P3_Int : Java.Int);

   procedure UnregisterKeyboardAction (This : access Typ;
                                       P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class);

   function GetRegisteredKeyStrokes (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   function GetConditionForKeyStroke (This : access Typ;
                                      P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class)
                                      return Java.Int;

   function GetActionForKeyStroke (This : access Typ;
                                   P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class)
                                   return access Java.Awt.Event.ActionListener.Typ'Class;

   procedure ResetKeyboardActions (This : access Typ);

   --  final
   procedure SetInputMap (This : access Typ;
                          P1_Int : Java.Int;
                          P2_InputMap : access Standard.Javax.Swing.InputMap.Typ'Class);

   --  final
   function GetInputMap (This : access Typ;
                         P1_Int : Java.Int)
                         return access Javax.Swing.InputMap.Typ'Class;

   --  final
   function GetInputMap (This : access Typ)
                         return access Javax.Swing.InputMap.Typ'Class;

   --  final
   procedure SetActionMap (This : access Typ;
                           P1_ActionMap : access Standard.Javax.Swing.ActionMap.Typ'Class);

   --  final
   function GetActionMap (This : access Typ)
                          return access Javax.Swing.ActionMap.Typ'Class;

   function GetBaseline (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetDefaultLocale return access Java.Util.Locale.Typ'Class;

   procedure SetDefaultLocale (P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   --  protected
   procedure ProcessComponentKeyEvent (This : access Typ;
                                       P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   --  protected
   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   --  protected
   function ProcessKeyBinding (This : access Typ;
                               P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                               P2_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Boolean : Java.Boolean)
                               return Java.Boolean;

   procedure SetToolTipText (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetToolTipText (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetToolTipText (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetToolTipLocation (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                                return access Java.Awt.Point.Typ'Class;

   function GetPopupLocation (This : access Typ;
                              P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                              return access Java.Awt.Point.Typ'Class;

   function CreateToolTip (This : access Typ)
                           return access Javax.Swing.JToolTip.Typ'Class;

   procedure ScrollRectToVisible (This : access Typ;
                                  P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure SetAutoscrolls (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetAutoscrolls (This : access Typ)
                            return Java.Boolean;

   procedure SetTransferHandler (This : access Typ;
                                 P1_TransferHandler : access Standard.Javax.Swing.TransferHandler.Typ'Class);

   function GetTransferHandler (This : access Typ)
                                return access Javax.Swing.TransferHandler.Typ'Class;

   --  protected
   procedure ProcessMouseEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   --  protected
   procedure ProcessMouseMotionEvent (This : access Typ;
                                      P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   --  final
   function GetClientProperty (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Java.Lang.Object.Typ'Class;

   --  final
   procedure PutClientProperty (This : access Typ;
                                P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure SetFocusTraversalKeys (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Set : access Standard.Java.Util.Set.Typ'Class);

   function IsLightweightComponent (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                    return Java.Boolean;

   function GetBounds (This : access Typ;
                       P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                       return access Java.Awt.Rectangle.Typ'Class;

   function GetSize (This : access Typ;
                     P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class)
                     return access Java.Awt.Dimension.Typ'Class;

   function GetLocation (This : access Typ;
                         P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                         return access Java.Awt.Point.Typ'Class;

   function GetX (This : access Typ)
                  return Java.Int;

   function GetY (This : access Typ)
                  return Java.Int;

   function GetWidth (This : access Typ)
                      return Java.Int;

   function GetHeight (This : access Typ)
                       return Java.Int;

   function IsOpaque (This : access Typ)
                      return Java.Boolean;

   procedure SetOpaque (This : access Typ;
                        P1_Boolean : Java.Boolean);

   procedure ComputeVisibleRect (This : access Typ;
                                 P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetVisibleRect (This : access Typ)
                            return access Java.Awt.Rectangle.Typ'Class;

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Boolean : Java.Boolean;
                                 P3_Boolean : Java.Boolean);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Char : Java.Char;
                                 P3_Char : Java.Char);

   --  protected
   procedure FireVetoableChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Beans.PropertyVetoException.Except

   --  synchronized
   procedure AddVetoableChangeListener (This : access Typ;
                                        P1_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   procedure RemoveVetoableChangeListener (This : access Typ;
                                           P1_VetoableChangeListener : access Standard.Java.Beans.VetoableChangeListener.Typ'Class);

   --  synchronized
   function GetVetoableChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   function GetTopLevelAncestor (This : access Typ)
                                 return access Java.Awt.Container.Typ'Class;

   procedure AddAncestorListener (This : access Typ;
                                  P1_AncestorListener : access Standard.Javax.Swing.Event.AncestorListener.Typ'Class);

   procedure RemoveAncestorListener (This : access Typ;
                                     P1_AncestorListener : access Standard.Javax.Swing.Event.AncestorListener.Typ'Class);

   function GetAncestorListeners (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref;

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int);

   procedure Repaint (This : access Typ;
                      P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure Revalidate (This : access Typ);

   function IsValidateRoot (This : access Typ)
                            return Java.Boolean;

   function IsOptimizedDrawingEnabled (This : access Typ)
                                       return Java.Boolean;

   procedure PaintImmediately (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int);

   procedure PaintImmediately (This : access Typ;
                               P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure SetDoubleBuffered (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function IsDoubleBuffered (This : access Typ)
                              return Java.Boolean;

   function GetRootPane (This : access Typ)
                         return access Javax.Swing.JRootPane.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WHEN_FOCUSED : constant Java.Int;

   --  final
   WHEN_ANCESTOR_OF_FOCUSED_COMPONENT : constant Java.Int;

   --  final
   WHEN_IN_FOCUSED_WINDOW : constant Java.Int;

   --  final
   UNDEFINED_CONDITION : constant Java.Int;

   --  final
   TOOL_TIP_TEXT_KEY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetInheritsPopupMenu, "setInheritsPopupMenu");
   pragma Import (Java, GetInheritsPopupMenu, "getInheritsPopupMenu");
   pragma Import (Java, SetComponentPopupMenu, "setComponentPopupMenu");
   pragma Import (Java, GetComponentPopupMenu, "getComponentPopupMenu");
   pragma Java_Constructor (New_JComponent);
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetComponentGraphics, "getComponentGraphics");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, PaintChildren, "paintChildren");
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, Update, "update");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PrintAll, "printAll");
   pragma Import (Java, Print, "print");
   pragma Import (Java, PrintComponent, "printComponent");
   pragma Import (Java, PrintChildren, "printChildren");
   pragma Import (Java, PrintBorder, "printBorder");
   pragma Import (Java, IsPaintingTile, "isPaintingTile");
   pragma Import (Java, IsPaintingForPrint, "isPaintingForPrint");
   pragma Import (Java, SetRequestFocusEnabled, "setRequestFocusEnabled");
   pragma Import (Java, IsRequestFocusEnabled, "isRequestFocusEnabled");
   pragma Import (Java, RequestFocus, "requestFocus");
   pragma Import (Java, RequestFocusInWindow, "requestFocusInWindow");
   pragma Import (Java, GrabFocus, "grabFocus");
   pragma Import (Java, SetVerifyInputWhenFocusTarget, "setVerifyInputWhenFocusTarget");
   pragma Import (Java, GetVerifyInputWhenFocusTarget, "getVerifyInputWhenFocusTarget");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, SetPreferredSize, "setPreferredSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, SetMaximumSize, "setMaximumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, SetMinimumSize, "setMinimumSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, SetBorder, "setBorder");
   pragma Import (Java, GetBorder, "getBorder");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, GetAlignmentY, "getAlignmentY");
   pragma Import (Java, SetAlignmentY, "setAlignmentY");
   pragma Import (Java, GetAlignmentX, "getAlignmentX");
   pragma Import (Java, SetAlignmentX, "setAlignmentX");
   pragma Import (Java, SetInputVerifier, "setInputVerifier");
   pragma Import (Java, GetInputVerifier, "getInputVerifier");
   pragma Import (Java, GetGraphics, "getGraphics");
   pragma Import (Java, SetDebugGraphicsOptions, "setDebugGraphicsOptions");
   pragma Import (Java, GetDebugGraphicsOptions, "getDebugGraphicsOptions");
   pragma Import (Java, RegisterKeyboardAction, "registerKeyboardAction");
   pragma Import (Java, UnregisterKeyboardAction, "unregisterKeyboardAction");
   pragma Import (Java, GetRegisteredKeyStrokes, "getRegisteredKeyStrokes");
   pragma Import (Java, GetConditionForKeyStroke, "getConditionForKeyStroke");
   pragma Import (Java, GetActionForKeyStroke, "getActionForKeyStroke");
   pragma Import (Java, ResetKeyboardActions, "resetKeyboardActions");
   pragma Import (Java, SetInputMap, "setInputMap");
   pragma Import (Java, GetInputMap, "getInputMap");
   pragma Import (Java, SetActionMap, "setActionMap");
   pragma Import (Java, GetActionMap, "getActionMap");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, SetForeground, "setForeground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetDefaultLocale, "getDefaultLocale");
   pragma Import (Java, SetDefaultLocale, "setDefaultLocale");
   pragma Import (Java, ProcessComponentKeyEvent, "processComponentKeyEvent");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, ProcessKeyBinding, "processKeyBinding");
   pragma Import (Java, SetToolTipText, "setToolTipText");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, GetToolTipLocation, "getToolTipLocation");
   pragma Import (Java, GetPopupLocation, "getPopupLocation");
   pragma Import (Java, CreateToolTip, "createToolTip");
   pragma Import (Java, ScrollRectToVisible, "scrollRectToVisible");
   pragma Import (Java, SetAutoscrolls, "setAutoscrolls");
   pragma Import (Java, GetAutoscrolls, "getAutoscrolls");
   pragma Import (Java, SetTransferHandler, "setTransferHandler");
   pragma Import (Java, GetTransferHandler, "getTransferHandler");
   pragma Import (Java, ProcessMouseEvent, "processMouseEvent");
   pragma Import (Java, ProcessMouseMotionEvent, "processMouseMotionEvent");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, GetClientProperty, "getClientProperty");
   pragma Import (Java, PutClientProperty, "putClientProperty");
   pragma Import (Java, SetFocusTraversalKeys, "setFocusTraversalKeys");
   pragma Import (Java, IsLightweightComponent, "isLightweightComponent");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, IsOpaque, "isOpaque");
   pragma Import (Java, SetOpaque, "setOpaque");
   pragma Import (Java, ComputeVisibleRect, "computeVisibleRect");
   pragma Import (Java, GetVisibleRect, "getVisibleRect");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, FireVetoableChange, "fireVetoableChange");
   pragma Import (Java, AddVetoableChangeListener, "addVetoableChangeListener");
   pragma Import (Java, RemoveVetoableChangeListener, "removeVetoableChangeListener");
   pragma Import (Java, GetVetoableChangeListeners, "getVetoableChangeListeners");
   pragma Import (Java, GetTopLevelAncestor, "getTopLevelAncestor");
   pragma Import (Java, AddAncestorListener, "addAncestorListener");
   pragma Import (Java, RemoveAncestorListener, "removeAncestorListener");
   pragma Import (Java, GetAncestorListeners, "getAncestorListeners");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, Revalidate, "revalidate");
   pragma Import (Java, IsValidateRoot, "isValidateRoot");
   pragma Import (Java, IsOptimizedDrawingEnabled, "isOptimizedDrawingEnabled");
   pragma Import (Java, PaintImmediately, "paintImmediately");
   pragma Import (Java, SetDoubleBuffered, "setDoubleBuffered");
   pragma Import (Java, IsDoubleBuffered, "isDoubleBuffered");
   pragma Import (Java, GetRootPane, "getRootPane");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, WHEN_FOCUSED, "WHEN_FOCUSED");
   pragma Import (Java, WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, "WHEN_ANCESTOR_OF_FOCUSED_COMPONENT");
   pragma Import (Java, WHEN_IN_FOCUSED_WINDOW, "WHEN_IN_FOCUSED_WINDOW");
   pragma Import (Java, UNDEFINED_CONDITION, "UNDEFINED_CONDITION");
   pragma Import (Java, TOOL_TIP_TEXT_KEY, "TOOL_TIP_TEXT_KEY");

end Javax.Swing.JComponent;
pragma Import (Java, Javax.Swing.JComponent, "javax.swing.JComponent");
pragma Extensions_Allowed (Off);
