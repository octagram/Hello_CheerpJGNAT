pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.DesktopManager;
limited with Javax.Swing.JInternalFrame;
limited with Javax.Swing.Plaf.DesktopPaneUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JLayeredPane;

package Javax.Swing.JDesktopPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JLayeredPane.Typ(MenuContainer_I,
                                        ImageObserver_I,
                                        Serializable_I,
                                        Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JDesktopPane (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.DesktopPaneUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_DesktopPaneUI : access Standard.Javax.Swing.Plaf.DesktopPaneUI.Typ'Class);

   procedure SetDragMode (This : access Typ;
                          P1_Int : Java.Int);

   function GetDragMode (This : access Typ)
                         return Java.Int;

   function GetDesktopManager (This : access Typ)
                               return access Javax.Swing.DesktopManager.Typ'Class;

   procedure SetDesktopManager (This : access Typ;
                                P1_DesktopManager : access Standard.Javax.Swing.DesktopManager.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetAllFrames (This : access Typ)
                          return Standard.Java.Lang.Object.Ref;

   function GetSelectedFrame (This : access Typ)
                              return access Javax.Swing.JInternalFrame.Typ'Class;

   procedure SetSelectedFrame (This : access Typ;
                               P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   function GetAllFramesInLayer (This : access Typ;
                                 P1_Int : Java.Int)
                                 return Standard.Java.Lang.Object.Ref;

   function SelectFrame (This : access Typ;
                         P1_Boolean : Java.Boolean)
                         return access Javax.Swing.JInternalFrame.Typ'Class;

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure RemoveAll (This : access Typ);

   procedure SetComponentZOrder (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Int : Java.Int);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   LIVE_DRAG_MODE : constant Java.Int;

   --  final
   OUTLINE_DRAG_MODE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JDesktopPane);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, SetDragMode, "setDragMode");
   pragma Import (Java, GetDragMode, "getDragMode");
   pragma Import (Java, GetDesktopManager, "getDesktopManager");
   pragma Import (Java, SetDesktopManager, "setDesktopManager");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetAllFrames, "getAllFrames");
   pragma Import (Java, GetSelectedFrame, "getSelectedFrame");
   pragma Import (Java, SetSelectedFrame, "setSelectedFrame");
   pragma Import (Java, GetAllFramesInLayer, "getAllFramesInLayer");
   pragma Import (Java, SelectFrame, "selectFrame");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, SetComponentZOrder, "setComponentZOrder");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, LIVE_DRAG_MODE, "LIVE_DRAG_MODE");
   pragma Import (Java, OUTLINE_DRAG_MODE, "OUTLINE_DRAG_MODE");

end Javax.Swing.JDesktopPane;
pragma Import (Java, Javax.Swing.JDesktopPane, "javax.swing.JDesktopPane");
pragma Extensions_Allowed (Off);
