pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleAction;
with Javax.Accessibility.AccessibleHyperlink;

package Javax.Swing.JEditorPane.JEditorPaneAccessibleHypertextSupport.HTMLLink is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AccessibleAction_I : Javax.Accessibility.AccessibleAction.Ref)
    is new Javax.Accessibility.AccessibleHyperlink.Typ(AccessibleAction_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsValid (This : access Typ)
                     return Java.Boolean;

   function GetAccessibleActionCount (This : access Typ)
                                      return Java.Int;

   function DoAccessibleAction (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean;

   function GetAccessibleActionDescription (This : access Typ;
                                            P1_Int : Java.Int)
                                            return access Java.Lang.String.Typ'Class;

   function GetAccessibleActionObject (This : access Typ;
                                       P1_Int : Java.Int)
                                       return access Java.Lang.Object.Typ'Class;

   function GetAccessibleActionAnchor (This : access Typ;
                                       P1_Int : Java.Int)
                                       return access Java.Lang.Object.Typ'Class;

   function GetStartIndex (This : access Typ)
                           return Java.Int;

   function GetEndIndex (This : access Typ)
                         return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, IsValid, "isValid");
   pragma Import (Java, GetAccessibleActionCount, "getAccessibleActionCount");
   pragma Import (Java, DoAccessibleAction, "doAccessibleAction");
   pragma Import (Java, GetAccessibleActionDescription, "getAccessibleActionDescription");
   pragma Import (Java, GetAccessibleActionObject, "getAccessibleActionObject");
   pragma Import (Java, GetAccessibleActionAnchor, "getAccessibleActionAnchor");
   pragma Import (Java, GetStartIndex, "getStartIndex");
   pragma Import (Java, GetEndIndex, "getEndIndex");

end Javax.Swing.JEditorPane.JEditorPaneAccessibleHypertextSupport.HTMLLink;
pragma Import (Java, Javax.Swing.JEditorPane.JEditorPaneAccessibleHypertextSupport.HTMLLink, "javax.swing.JEditorPane$JEditorPaneAccessibleHypertextSupport$HTMLLink");
pragma Extensions_Allowed (Off);
