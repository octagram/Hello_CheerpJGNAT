pragma Extensions_Allowed (On);
package Javax.Swing.JEditorPane.JEditorPaneAccessibleHypertextSupport is
   pragma Preelaborate;
end Javax.Swing.JEditorPane.JEditorPaneAccessibleHypertextSupport;
pragma Import (Java, Javax.Swing.JEditorPane.JEditorPaneAccessibleHypertextSupport, "javax.swing.JEditorPane$JEditorPaneAccessibleHypertextSupport");
pragma Extensions_Allowed (Off);
