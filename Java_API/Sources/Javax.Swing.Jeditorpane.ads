pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Io.InputStream;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Event.HyperlinkEvent;
limited with Javax.Swing.Event.HyperlinkListener;
limited with Javax.Swing.Text.EditorKit;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Scrollable;
with Javax.Swing.Text.JTextComponent;

package Javax.Swing.JEditorPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref)
    is new Javax.Swing.Text.JTextComponent.Typ(MenuContainer_I,
                                               ImageObserver_I,
                                               Serializable_I,
                                               Accessible_I,
                                               Scrollable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JEditorPane (This : Ref := null)
                             return Ref;

   function New_JEditorPane (P1_URL : access Standard.Java.Net.URL.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JEditorPane (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Io.IOException.Except

   function New_JEditorPane (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddHyperlinkListener (This : access Typ;
                                   P1_HyperlinkListener : access Standard.Javax.Swing.Event.HyperlinkListener.Typ'Class);

   --  synchronized
   procedure RemoveHyperlinkListener (This : access Typ;
                                      P1_HyperlinkListener : access Standard.Javax.Swing.Event.HyperlinkListener.Typ'Class);

   --  synchronized
   function GetHyperlinkListeners (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   procedure FireHyperlinkUpdate (This : access Typ;
                                  P1_HyperlinkEvent : access Standard.Javax.Swing.Event.HyperlinkEvent.Typ'Class);

   procedure SetPage (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Read (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetStream (This : access Typ;
                       P1_URL : access Standard.Java.Net.URL.Typ'Class)
                       return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure ScrollToReference (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPage (This : access Typ)
                     return access Java.Net.URL.Typ'Class;

   procedure SetPage (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function CreateDefaultEditorKit (This : access Typ)
                                    return access Javax.Swing.Text.EditorKit.Typ'Class;

   function GetEditorKit (This : access Typ)
                          return access Javax.Swing.Text.EditorKit.Typ'Class;

   --  final
   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   --  final
   procedure SetContentType (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetEditorKit (This : access Typ;
                           P1_EditorKit : access Standard.Javax.Swing.Text.EditorKit.Typ'Class);

   function GetEditorKitForContentType (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return access Javax.Swing.Text.EditorKit.Typ'Class;

   procedure SetEditorKitForContentType (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_EditorKit : access Standard.Javax.Swing.Text.EditorKit.Typ'Class);

   procedure ReplaceSelection (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function CreateEditorKitForContentType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                           return access Javax.Swing.Text.EditorKit.Typ'Class;

   procedure RegisterEditorKitForContentType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                              P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RegisterEditorKitForContentType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                                              P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class);

   function GetEditorKitClassNameForContentType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                                 return access Java.Lang.String.Typ'Class;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetScrollableTracksViewportWidth (This : access Typ)
                                              return Java.Boolean;

   function GetScrollableTracksViewportHeight (This : access Typ)
                                               return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   W3C_LENGTH_UNITS : constant access Java.Lang.String.Typ'Class;

   --  final
   HONOR_DISPLAY_PROPERTIES : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JEditorPane);
   pragma Import (Java, AddHyperlinkListener, "addHyperlinkListener");
   pragma Import (Java, RemoveHyperlinkListener, "removeHyperlinkListener");
   pragma Import (Java, GetHyperlinkListeners, "getHyperlinkListeners");
   pragma Import (Java, FireHyperlinkUpdate, "fireHyperlinkUpdate");
   pragma Import (Java, SetPage, "setPage");
   pragma Import (Java, Read, "read");
   pragma Import (Java, GetStream, "getStream");
   pragma Import (Java, ScrollToReference, "scrollToReference");
   pragma Import (Java, GetPage, "getPage");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, CreateDefaultEditorKit, "createDefaultEditorKit");
   pragma Import (Java, GetEditorKit, "getEditorKit");
   pragma Import (Java, GetContentType, "getContentType");
   pragma Import (Java, SetContentType, "setContentType");
   pragma Import (Java, SetEditorKit, "setEditorKit");
   pragma Import (Java, GetEditorKitForContentType, "getEditorKitForContentType");
   pragma Import (Java, SetEditorKitForContentType, "setEditorKitForContentType");
   pragma Import (Java, ReplaceSelection, "replaceSelection");
   pragma Import (Java, CreateEditorKitForContentType, "createEditorKitForContentType");
   pragma Import (Java, RegisterEditorKitForContentType, "registerEditorKitForContentType");
   pragma Import (Java, GetEditorKitClassNameForContentType, "getEditorKitClassNameForContentType");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, GetScrollableTracksViewportWidth, "getScrollableTracksViewportWidth");
   pragma Import (Java, GetScrollableTracksViewportHeight, "getScrollableTracksViewportHeight");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, W3C_LENGTH_UNITS, "W3C_LENGTH_UNITS");
   pragma Import (Java, HONOR_DISPLAY_PROPERTIES, "HONOR_DISPLAY_PROPERTIES");

end Javax.Swing.JEditorPane;
pragma Import (Java, Javax.Swing.JEditorPane, "javax.swing.JEditorPane");
pragma Extensions_Allowed (Off);
