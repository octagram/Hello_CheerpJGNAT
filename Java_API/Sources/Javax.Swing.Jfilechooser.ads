pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.ActionListener;
limited with Java.Io.File;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Filechooser.FileFilter;
limited with Javax.Swing.Filechooser.FileSystemView;
limited with Javax.Swing.Filechooser.FileView;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JDialog;
limited with Javax.Swing.Plaf.FileChooserUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JFileChooser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with null record;
      
      ------------------------
      -- Field Declarations --
      ------------------------

      -- --  protected
      -- AccessibleContext : access Javax.Accessibility.AccessibleContext.Typ'Class;
      -- pragma Import (Java, AccessibleContext, "accessibleContext");

   -- end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JFileChooser (This : Ref := null)
                              return Ref;

   function New_JFileChooser (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JFileChooser (P1_File : access Standard.Java.Io.File.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JFileChooser (P1_FileSystemView : access Standard.Javax.Swing.Filechooser.FileSystemView.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JFileChooser (P1_File : access Standard.Java.Io.File.Typ'Class;
                              P2_FileSystemView : access Standard.Javax.Swing.Filechooser.FileSystemView.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JFileChooser (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_FileSystemView : access Standard.Javax.Swing.Filechooser.FileSystemView.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Setup (This : access Typ;
                    P1_FileSystemView : access Standard.Javax.Swing.Filechooser.FileSystemView.Typ'Class);

   procedure SetDragEnabled (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetDragEnabled (This : access Typ)
                            return Java.Boolean;

   function GetSelectedFile (This : access Typ)
                             return access Java.Io.File.Typ'Class;

   procedure SetSelectedFile (This : access Typ;
                              P1_File : access Standard.Java.Io.File.Typ'Class);

   function GetSelectedFiles (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   procedure SetSelectedFiles (This : access Typ;
                               P1_File_Arr : access Java.Io.File.Arr_Obj);

   function GetCurrentDirectory (This : access Typ)
                                 return access Java.Io.File.Typ'Class;

   procedure SetCurrentDirectory (This : access Typ;
                                  P1_File : access Standard.Java.Io.File.Typ'Class);

   procedure ChangeToParentDirectory (This : access Typ);

   procedure RescanCurrentDirectory (This : access Typ);

   procedure EnsureFileIsVisible (This : access Typ;
                                  P1_File : access Standard.Java.Io.File.Typ'Class);

   function ShowOpenDialog (This : access Typ;
                            P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowSaveDialog (This : access Typ;
                            P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowDialog (This : access Typ;
                        P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   --  protected
   function CreateDialog (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                          return access Javax.Swing.JDialog.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function GetControlButtonsAreShown (This : access Typ)
                                       return Java.Boolean;

   procedure SetControlButtonsAreShown (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function GetDialogType (This : access Typ)
                           return Java.Int;

   procedure SetDialogType (This : access Typ;
                            P1_Int : Java.Int);

   procedure SetDialogTitle (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetDialogTitle (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetApproveButtonToolTipText (This : access Typ;
                                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetApproveButtonToolTipText (This : access Typ)
                                         return access Java.Lang.String.Typ'Class;

   function GetApproveButtonMnemonic (This : access Typ)
                                      return Java.Int;

   procedure SetApproveButtonMnemonic (This : access Typ;
                                       P1_Int : Java.Int);

   procedure SetApproveButtonMnemonic (This : access Typ;
                                       P1_Char : Java.Char);

   procedure SetApproveButtonText (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetApproveButtonText (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   function GetChoosableFileFilters (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   procedure AddChoosableFileFilter (This : access Typ;
                                     P1_FileFilter : access Standard.Javax.Swing.Filechooser.FileFilter.Typ'Class);

   function RemoveChoosableFileFilter (This : access Typ;
                                       P1_FileFilter : access Standard.Javax.Swing.Filechooser.FileFilter.Typ'Class)
                                       return Java.Boolean;

   procedure ResetChoosableFileFilters (This : access Typ);

   function GetAcceptAllFileFilter (This : access Typ)
                                    return access Javax.Swing.Filechooser.FileFilter.Typ'Class;

   function IsAcceptAllFileFilterUsed (This : access Typ)
                                       return Java.Boolean;

   procedure SetAcceptAllFileFilterUsed (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   function GetAccessory (This : access Typ)
                          return access Javax.Swing.JComponent.Typ'Class;

   procedure SetAccessory (This : access Typ;
                           P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure SetFileSelectionMode (This : access Typ;
                                   P1_Int : Java.Int);

   function GetFileSelectionMode (This : access Typ)
                                  return Java.Int;

   function IsFileSelectionEnabled (This : access Typ)
                                    return Java.Boolean;

   function IsDirectorySelectionEnabled (This : access Typ)
                                         return Java.Boolean;

   procedure SetMultiSelectionEnabled (This : access Typ;
                                       P1_Boolean : Java.Boolean);

   function IsMultiSelectionEnabled (This : access Typ)
                                     return Java.Boolean;

   function IsFileHidingEnabled (This : access Typ)
                                 return Java.Boolean;

   procedure SetFileHidingEnabled (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   procedure SetFileFilter (This : access Typ;
                            P1_FileFilter : access Standard.Javax.Swing.Filechooser.FileFilter.Typ'Class);

   function GetFileFilter (This : access Typ)
                           return access Javax.Swing.Filechooser.FileFilter.Typ'Class;

   procedure SetFileView (This : access Typ;
                          P1_FileView : access Standard.Javax.Swing.Filechooser.FileView.Typ'Class);

   function GetFileView (This : access Typ)
                         return access Javax.Swing.Filechooser.FileView.Typ'Class;

   function GetName (This : access Typ;
                     P1_File : access Standard.Java.Io.File.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ;
                            P1_File : access Standard.Java.Io.File.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetTypeDescription (This : access Typ;
                                P1_File : access Standard.Java.Io.File.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function GetIcon (This : access Typ;
                     P1_File : access Standard.Java.Io.File.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   function IsTraversable (This : access Typ;
                           P1_File : access Standard.Java.Io.File.Typ'Class)
                           return Java.Boolean;

   function accept_K (This : access Typ;
                      P1_File : access Standard.Java.Io.File.Typ'Class)
                      return Java.Boolean;

   procedure SetFileSystemView (This : access Typ;
                                P1_FileSystemView : access Standard.Javax.Swing.Filechooser.FileSystemView.Typ'Class);

   function GetFileSystemView (This : access Typ)
                               return access Javax.Swing.Filechooser.FileSystemView.Typ'Class;

   procedure ApproveSelection (This : access Typ);

   procedure CancelSelection (This : access Typ);

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireActionPerformed (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.FileChooserUI.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   OPEN_DIALOG : constant Java.Int;

   --  final
   SAVE_DIALOG : constant Java.Int;

   --  final
   CUSTOM_DIALOG : constant Java.Int;

   --  final
   CANCEL_OPTION : constant Java.Int;

   --  final
   APPROVE_OPTION : constant Java.Int;

   --  final
   ERROR_OPTION : constant Java.Int;

   --  final
   FILES_ONLY : constant Java.Int;

   --  final
   DIRECTORIES_ONLY : constant Java.Int;

   --  final
   FILES_AND_DIRECTORIES : constant Java.Int;

   --  final
   CANCEL_SELECTION : constant access Java.Lang.String.Typ'Class;

   --  final
   APPROVE_SELECTION : constant access Java.Lang.String.Typ'Class;

   --  final
   APPROVE_BUTTON_TEXT_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   APPROVE_BUTTON_TOOL_TIP_TEXT_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   APPROVE_BUTTON_MNEMONIC_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CONTROL_BUTTONS_ARE_SHOWN_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DIRECTORY_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SELECTED_FILE_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SELECTED_FILES_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   MULTI_SELECTION_ENABLED_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FILE_SYSTEM_VIEW_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FILE_VIEW_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FILE_HIDING_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FILE_FILTER_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FILE_SELECTION_MODE_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCESSORY_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ACCEPT_ALL_FILE_FILTER_USED_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DIALOG_TITLE_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DIALOG_TYPE_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CHOOSABLE_FILE_FILTER_CHANGED_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JFileChooser);
   pragma Import (Java, Setup, "setup");
   pragma Import (Java, SetDragEnabled, "setDragEnabled");
   pragma Import (Java, GetDragEnabled, "getDragEnabled");
   pragma Import (Java, GetSelectedFile, "getSelectedFile");
   pragma Import (Java, SetSelectedFile, "setSelectedFile");
   pragma Import (Java, GetSelectedFiles, "getSelectedFiles");
   pragma Import (Java, SetSelectedFiles, "setSelectedFiles");
   pragma Import (Java, GetCurrentDirectory, "getCurrentDirectory");
   pragma Import (Java, SetCurrentDirectory, "setCurrentDirectory");
   pragma Import (Java, ChangeToParentDirectory, "changeToParentDirectory");
   pragma Import (Java, RescanCurrentDirectory, "rescanCurrentDirectory");
   pragma Import (Java, EnsureFileIsVisible, "ensureFileIsVisible");
   pragma Import (Java, ShowOpenDialog, "showOpenDialog");
   pragma Import (Java, ShowSaveDialog, "showSaveDialog");
   pragma Import (Java, ShowDialog, "showDialog");
   pragma Import (Java, CreateDialog, "createDialog");
   pragma Import (Java, GetControlButtonsAreShown, "getControlButtonsAreShown");
   pragma Import (Java, SetControlButtonsAreShown, "setControlButtonsAreShown");
   pragma Import (Java, GetDialogType, "getDialogType");
   pragma Import (Java, SetDialogType, "setDialogType");
   pragma Import (Java, SetDialogTitle, "setDialogTitle");
   pragma Import (Java, GetDialogTitle, "getDialogTitle");
   pragma Import (Java, SetApproveButtonToolTipText, "setApproveButtonToolTipText");
   pragma Import (Java, GetApproveButtonToolTipText, "getApproveButtonToolTipText");
   pragma Import (Java, GetApproveButtonMnemonic, "getApproveButtonMnemonic");
   pragma Import (Java, SetApproveButtonMnemonic, "setApproveButtonMnemonic");
   pragma Import (Java, SetApproveButtonText, "setApproveButtonText");
   pragma Import (Java, GetApproveButtonText, "getApproveButtonText");
   pragma Import (Java, GetChoosableFileFilters, "getChoosableFileFilters");
   pragma Import (Java, AddChoosableFileFilter, "addChoosableFileFilter");
   pragma Import (Java, RemoveChoosableFileFilter, "removeChoosableFileFilter");
   pragma Import (Java, ResetChoosableFileFilters, "resetChoosableFileFilters");
   pragma Import (Java, GetAcceptAllFileFilter, "getAcceptAllFileFilter");
   pragma Import (Java, IsAcceptAllFileFilterUsed, "isAcceptAllFileFilterUsed");
   pragma Import (Java, SetAcceptAllFileFilterUsed, "setAcceptAllFileFilterUsed");
   pragma Import (Java, GetAccessory, "getAccessory");
   pragma Import (Java, SetAccessory, "setAccessory");
   pragma Import (Java, SetFileSelectionMode, "setFileSelectionMode");
   pragma Import (Java, GetFileSelectionMode, "getFileSelectionMode");
   pragma Import (Java, IsFileSelectionEnabled, "isFileSelectionEnabled");
   pragma Import (Java, IsDirectorySelectionEnabled, "isDirectorySelectionEnabled");
   pragma Import (Java, SetMultiSelectionEnabled, "setMultiSelectionEnabled");
   pragma Import (Java, IsMultiSelectionEnabled, "isMultiSelectionEnabled");
   pragma Import (Java, IsFileHidingEnabled, "isFileHidingEnabled");
   pragma Import (Java, SetFileHidingEnabled, "setFileHidingEnabled");
   pragma Import (Java, SetFileFilter, "setFileFilter");
   pragma Import (Java, GetFileFilter, "getFileFilter");
   pragma Import (Java, SetFileView, "setFileView");
   pragma Import (Java, GetFileView, "getFileView");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetTypeDescription, "getTypeDescription");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, IsTraversable, "isTraversable");
   pragma Import (Java, accept_K, "accept");
   pragma Import (Java, SetFileSystemView, "setFileSystemView");
   pragma Import (Java, GetFileSystemView, "getFileSystemView");
   pragma Import (Java, ApproveSelection, "approveSelection");
   pragma Import (Java, CancelSelection, "cancelSelection");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, FireActionPerformed, "fireActionPerformed");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, OPEN_DIALOG, "OPEN_DIALOG");
   pragma Import (Java, SAVE_DIALOG, "SAVE_DIALOG");
   pragma Import (Java, CUSTOM_DIALOG, "CUSTOM_DIALOG");
   pragma Import (Java, CANCEL_OPTION, "CANCEL_OPTION");
   pragma Import (Java, APPROVE_OPTION, "APPROVE_OPTION");
   pragma Import (Java, ERROR_OPTION, "ERROR_OPTION");
   pragma Import (Java, FILES_ONLY, "FILES_ONLY");
   pragma Import (Java, DIRECTORIES_ONLY, "DIRECTORIES_ONLY");
   pragma Import (Java, FILES_AND_DIRECTORIES, "FILES_AND_DIRECTORIES");
   pragma Import (Java, CANCEL_SELECTION, "CANCEL_SELECTION");
   pragma Import (Java, APPROVE_SELECTION, "APPROVE_SELECTION");
   pragma Import (Java, APPROVE_BUTTON_TEXT_CHANGED_PROPERTY, "APPROVE_BUTTON_TEXT_CHANGED_PROPERTY");
   pragma Import (Java, APPROVE_BUTTON_TOOL_TIP_TEXT_CHANGED_PROPERTY, "APPROVE_BUTTON_TOOL_TIP_TEXT_CHANGED_PROPERTY");
   pragma Import (Java, APPROVE_BUTTON_MNEMONIC_CHANGED_PROPERTY, "APPROVE_BUTTON_MNEMONIC_CHANGED_PROPERTY");
   pragma Import (Java, CONTROL_BUTTONS_ARE_SHOWN_CHANGED_PROPERTY, "CONTROL_BUTTONS_ARE_SHOWN_CHANGED_PROPERTY");
   pragma Import (Java, DIRECTORY_CHANGED_PROPERTY, "DIRECTORY_CHANGED_PROPERTY");
   pragma Import (Java, SELECTED_FILE_CHANGED_PROPERTY, "SELECTED_FILE_CHANGED_PROPERTY");
   pragma Import (Java, SELECTED_FILES_CHANGED_PROPERTY, "SELECTED_FILES_CHANGED_PROPERTY");
   pragma Import (Java, MULTI_SELECTION_ENABLED_CHANGED_PROPERTY, "MULTI_SELECTION_ENABLED_CHANGED_PROPERTY");
   pragma Import (Java, FILE_SYSTEM_VIEW_CHANGED_PROPERTY, "FILE_SYSTEM_VIEW_CHANGED_PROPERTY");
   pragma Import (Java, FILE_VIEW_CHANGED_PROPERTY, "FILE_VIEW_CHANGED_PROPERTY");
   pragma Import (Java, FILE_HIDING_CHANGED_PROPERTY, "FILE_HIDING_CHANGED_PROPERTY");
   pragma Import (Java, FILE_FILTER_CHANGED_PROPERTY, "FILE_FILTER_CHANGED_PROPERTY");
   pragma Import (Java, FILE_SELECTION_MODE_CHANGED_PROPERTY, "FILE_SELECTION_MODE_CHANGED_PROPERTY");
   pragma Import (Java, ACCESSORY_CHANGED_PROPERTY, "ACCESSORY_CHANGED_PROPERTY");
   pragma Import (Java, ACCEPT_ALL_FILE_FILTER_USED_CHANGED_PROPERTY, "ACCEPT_ALL_FILE_FILTER_USED_CHANGED_PROPERTY");
   pragma Import (Java, DIALOG_TITLE_CHANGED_PROPERTY, "DIALOG_TITLE_CHANGED_PROPERTY");
   pragma Import (Java, DIALOG_TYPE_CHANGED_PROPERTY, "DIALOG_TYPE_CHANGED_PROPERTY");
   pragma Import (Java, CHOOSABLE_FILE_FILTER_CHANGED_PROPERTY, "CHOOSABLE_FILE_FILTER_CHANGED_PROPERTY");

end Javax.Swing.JFileChooser;
pragma Import (Java, Javax.Swing.JFileChooser, "javax.swing.JFileChooser");
pragma Extensions_Allowed (Off);
