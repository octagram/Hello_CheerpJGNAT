pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.Text.DocumentFilter;
limited with Javax.Swing.Text.NavigationFilter;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.JFormattedTextField.AbstractFormatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AbstractFormatter (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Install (This : access Typ;
                      P1_JFormattedTextField : access Standard.Javax.Swing.JFormattedTextField.Typ'Class);

   procedure Uninstall (This : access Typ);

   function StringToValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Text.ParseException.Except

   function ValueToString (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Text.ParseException.Except

   --  protected
   function GetFormattedTextField (This : access Typ)
                                   return access Javax.Swing.JFormattedTextField.Typ'Class;

   --  protected
   procedure InvalidEdit (This : access Typ);

   --  protected
   procedure SetEditValid (This : access Typ;
                           P1_Boolean : Java.Boolean);

   --  protected
   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetDocumentFilter (This : access Typ)
                               return access Javax.Swing.Text.DocumentFilter.Typ'Class;

   --  protected
   function GetNavigationFilter (This : access Typ)
                                 return access Javax.Swing.Text.NavigationFilter.Typ'Class;

   --  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractFormatter);
   pragma Import (Java, Install, "install");
   pragma Import (Java, Uninstall, "uninstall");
   pragma Export (Java, StringToValue, "stringToValue");
   pragma Export (Java, ValueToString, "valueToString");
   pragma Import (Java, GetFormattedTextField, "getFormattedTextField");
   pragma Import (Java, InvalidEdit, "invalidEdit");
   pragma Import (Java, SetEditValid, "setEditValid");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, GetDocumentFilter, "getDocumentFilter");
   pragma Import (Java, GetNavigationFilter, "getNavigationFilter");
   pragma Import (Java, Clone, "clone");

end Javax.Swing.JFormattedTextField.AbstractFormatter;
pragma Import (Java, Javax.Swing.JFormattedTextField.AbstractFormatter, "javax.swing.JFormattedTextField$AbstractFormatter");
pragma Extensions_Allowed (Off);
