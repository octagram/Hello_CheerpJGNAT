pragma Extensions_Allowed (On);
limited with Javax.Swing.JFormattedTextField.AbstractFormatter;
with Java.Lang.Object;

package Javax.Swing.JFormattedTextField.AbstractFormatterFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AbstractFormatterFactory (This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFormatter (This : access Typ;
                          P1_JFormattedTextField : access Standard.Javax.Swing.JFormattedTextField.Typ'Class)
                          return access Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractFormatterFactory);
   pragma Export (Java, GetFormatter, "getFormatter");

end Javax.Swing.JFormattedTextField.AbstractFormatterFactory;
pragma Import (Java, Javax.Swing.JFormattedTextField.AbstractFormatterFactory, "javax.swing.JFormattedTextField$AbstractFormatterFactory");
pragma Extensions_Allowed (Off);
