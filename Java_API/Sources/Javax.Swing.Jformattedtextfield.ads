pragma Extensions_Allowed (On);
limited with Java.Awt.Event.FocusEvent;
limited with Java.Awt.Event.InputMethodEvent;
limited with Java.Lang.String;
limited with Java.Text.Format;
limited with Javax.Swing.Action;
limited with Javax.Swing.JFormattedTextField.AbstractFormatter;
limited with Javax.Swing.JFormattedTextField.AbstractFormatterFactory;
limited with Javax.Swing.Text.Document;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JTextField;
with Javax.Swing.Scrollable;
with Javax.Swing.SwingConstants;

package Javax.Swing.JFormattedTextField is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JTextField.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I,
                                      Accessible_I,
                                      Scrollable_I,
                                      SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JFormattedTextField (This : Ref := null)
                                     return Ref;

   function New_JFormattedTextField (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_JFormattedTextField (P1_Format : access Standard.Java.Text.Format.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_JFormattedTextField (P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_JFormattedTextField (P1_AbstractFormatterFactory : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatterFactory.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_JFormattedTextField (P1_AbstractFormatterFactory : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatterFactory.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFocusLostBehavior (This : access Typ;
                                   P1_Int : Java.Int);

   function GetFocusLostBehavior (This : access Typ)
                                  return Java.Int;

   procedure SetFormatterFactory (This : access Typ;
                                  P1_AbstractFormatterFactory : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatterFactory.Typ'Class);

   function GetFormatterFactory (This : access Typ)
                                 return access Javax.Swing.JFormattedTextField.AbstractFormatterFactory.Typ'Class;

   --  protected
   procedure SetFormatter (This : access Typ;
                           P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class);

   function GetFormatter (This : access Typ)
                          return access Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure CommitEdit (This : access Typ);
   --  can raise Java.Text.ParseException.Except

   function IsEditValid (This : access Typ)
                         return Java.Boolean;

   --  protected
   procedure InvalidEdit (This : access Typ);

   --  protected
   procedure ProcessInputMethodEvent (This : access Typ;
                                      P1_InputMethodEvent : access Standard.Java.Awt.Event.InputMethodEvent.Typ'Class);

   --  protected
   procedure ProcessFocusEvent (This : access Typ;
                                P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetDocument (This : access Typ;
                          P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   COMMIT : constant Java.Int;

   --  final
   COMMIT_OR_REVERT : constant Java.Int;

   --  final
   REVERT : constant Java.Int;

   --  final
   PERSIST : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JFormattedTextField);
   pragma Import (Java, SetFocusLostBehavior, "setFocusLostBehavior");
   pragma Import (Java, GetFocusLostBehavior, "getFocusLostBehavior");
   pragma Import (Java, SetFormatterFactory, "setFormatterFactory");
   pragma Import (Java, GetFormatterFactory, "getFormatterFactory");
   pragma Import (Java, SetFormatter, "setFormatter");
   pragma Import (Java, GetFormatter, "getFormatter");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, CommitEdit, "commitEdit");
   pragma Import (Java, IsEditValid, "isEditValid");
   pragma Import (Java, InvalidEdit, "invalidEdit");
   pragma Import (Java, ProcessInputMethodEvent, "processInputMethodEvent");
   pragma Import (Java, ProcessFocusEvent, "processFocusEvent");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetDocument, "setDocument");
   pragma Import (Java, COMMIT, "COMMIT");
   pragma Import (Java, COMMIT_OR_REVERT, "COMMIT_OR_REVERT");
   pragma Import (Java, REVERT, "REVERT");
   pragma Import (Java, PERSIST, "PERSIST");

end Javax.Swing.JFormattedTextField;
pragma Import (Java, Javax.Swing.JFormattedTextField, "javax.swing.JFormattedTextField");
pragma Extensions_Allowed (Off);
