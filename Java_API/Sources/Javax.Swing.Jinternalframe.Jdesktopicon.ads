pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.JDesktopPane;
limited with Javax.Swing.Plaf.DesktopIconUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JInternalFrame.JDesktopIcon is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JDesktopIcon (P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.DesktopIconUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_DesktopIconUI : access Standard.Javax.Swing.Plaf.DesktopIconUI.Typ'Class);

   function GetInternalFrame (This : access Typ)
                              return access Javax.Swing.JInternalFrame.Typ'Class;

   procedure SetInternalFrame (This : access Typ;
                               P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   function GetDesktopPane (This : access Typ)
                            return access Javax.Swing.JDesktopPane.Typ'Class;

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JDesktopIcon);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, GetInternalFrame, "getInternalFrame");
   pragma Import (Java, SetInternalFrame, "setInternalFrame");
   pragma Import (Java, GetDesktopPane, "getDesktopPane");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JInternalFrame.JDesktopIcon;
pragma Import (Java, Javax.Swing.JInternalFrame.JDesktopIcon, "javax.swing.JInternalFrame$JDesktopIcon");
pragma Extensions_Allowed (Off);
