pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Cursor;
limited with Java.Awt.Graphics;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Rectangle;
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Event.InternalFrameListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JDesktopPane;
limited with Javax.Swing.JInternalFrame.JDesktopIcon;
limited with Javax.Swing.JLayeredPane;
limited with Javax.Swing.JMenuBar;
limited with Javax.Swing.JRootPane;
limited with Javax.Swing.Plaf.InternalFrameUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.RootPaneContainer;
with Javax.Swing.WindowConstants;

package Javax.Swing.JInternalFrame is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            RootPaneContainer_I : Javax.Swing.RootPaneContainer.Ref;
            WindowConstants_I : Javax.Swing.WindowConstants.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      RootPane : access Javax.Swing.JRootPane.Typ'Class;
      pragma Import (Java, RootPane, "rootPane");

      --  protected
      RootPaneCheckingEnabled : Java.Boolean;
      pragma Import (Java, RootPaneCheckingEnabled, "rootPaneCheckingEnabled");

      --  protected
      Closable : Java.Boolean;
      pragma Import (Java, Closable, "closable");

      --  protected
      IsClosed : Java.Boolean;
      pragma Import (Java, IsClosed, "isClosed");

      --  protected
      Maximizable : Java.Boolean;
      pragma Import (Java, Maximizable, "maximizable");

      --  protected
      IsMaximum : Java.Boolean;
      pragma Import (Java, IsMaximum, "isMaximum");

      --  protected
      Iconable : Java.Boolean;
      pragma Import (Java, Iconable, "iconable");

      --  protected
      IsIcon : Java.Boolean;
      pragma Import (Java, IsIcon, "isIcon");

      --  protected
      Resizable : Java.Boolean;
      pragma Import (Java, Resizable, "resizable");

      --  protected
      IsSelected : Java.Boolean;
      pragma Import (Java, IsSelected, "isSelected");

      --  protected
      FrameIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, FrameIcon, "frameIcon");

      --  protected
      Title : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Title, "title");

      --  protected
      DesktopIcon : access Javax.Swing.JInternalFrame.JDesktopIcon.Typ'Class;
      pragma Import (Java, DesktopIcon, "desktopIcon");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JInternalFrame (This : Ref := null)
                                return Ref;

   function New_JInternalFrame (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_JInternalFrame (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   function New_JInternalFrame (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   function New_JInternalFrame (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean;
                                P4_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   function New_JInternalFrame (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean;
                                P4_Boolean : Java.Boolean;
                                P5_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateRootPane (This : access Typ)
                            return access Javax.Swing.JRootPane.Typ'Class;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.InternalFrameUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_InternalFrameUI : access Standard.Javax.Swing.Plaf.InternalFrameUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function IsRootPaneCheckingEnabled (This : access Typ)
                                       return Java.Boolean;

   --  protected
   procedure SetRootPaneCheckingEnabled (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   function GetJMenuBar (This : access Typ)
                         return access Javax.Swing.JMenuBar.Typ'Class;

   procedure SetJMenuBar (This : access Typ;
                          P1_JMenuBar : access Standard.Javax.Swing.JMenuBar.Typ'Class);

   function GetContentPane (This : access Typ)
                            return access Java.Awt.Container.Typ'Class;

   procedure SetContentPane (This : access Typ;
                             P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function GetLayeredPane (This : access Typ)
                            return access Javax.Swing.JLayeredPane.Typ'Class;

   procedure SetLayeredPane (This : access Typ;
                             P1_JLayeredPane : access Standard.Javax.Swing.JLayeredPane.Typ'Class);

   function GetGlassPane (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   procedure SetGlassPane (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetRootPane (This : access Typ)
                         return access Javax.Swing.JRootPane.Typ'Class;

   --  protected
   procedure SetRootPane (This : access Typ;
                          P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   procedure SetClosable (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function IsClosable (This : access Typ)
                        return Java.Boolean;

   function IsClosed (This : access Typ)
                      return Java.Boolean;

   procedure SetClosed (This : access Typ;
                        P1_Boolean : Java.Boolean);
   --  can raise Java.Beans.PropertyVetoException.Except

   procedure SetResizable (This : access Typ;
                           P1_Boolean : Java.Boolean);

   function IsResizable (This : access Typ)
                         return Java.Boolean;

   procedure SetIconifiable (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function IsIconifiable (This : access Typ)
                           return Java.Boolean;

   function IsIcon (This : access Typ)
                    return Java.Boolean;

   procedure SetIcon (This : access Typ;
                      P1_Boolean : Java.Boolean);
   --  can raise Java.Beans.PropertyVetoException.Except

   procedure SetMaximizable (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function IsMaximizable (This : access Typ)
                           return Java.Boolean;

   function IsMaximum (This : access Typ)
                       return Java.Boolean;

   procedure SetMaximum (This : access Typ;
                         P1_Boolean : Java.Boolean);
   --  can raise Java.Beans.PropertyVetoException.Except

   function GetTitle (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetTitle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetSelected (This : access Typ;
                          P1_Boolean : Java.Boolean);
   --  can raise Java.Beans.PropertyVetoException.Except

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   procedure SetFrameIcon (This : access Typ;
                           P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetFrameIcon (This : access Typ)
                          return access Javax.Swing.Icon.Typ'Class;

   procedure MoveToFront (This : access Typ);

   procedure MoveToBack (This : access Typ);

   function GetLastCursor (This : access Typ)
                           return access Java.Awt.Cursor.Typ'Class;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   procedure SetLayer (This : access Typ;
                       P1_Integer : access Standard.Java.Lang.Integer.Typ'Class);

   procedure SetLayer (This : access Typ;
                       P1_Int : Java.Int);

   function GetLayer (This : access Typ)
                      return Java.Int;

   function GetDesktopPane (This : access Typ)
                            return access Javax.Swing.JDesktopPane.Typ'Class;

   procedure SetDesktopIcon (This : access Typ;
                             P1_JDesktopIcon : access Standard.Javax.Swing.JInternalFrame.JDesktopIcon.Typ'Class);

   function GetDesktopIcon (This : access Typ)
                            return access Javax.Swing.JInternalFrame.JDesktopIcon.Typ'Class;

   function GetNormalBounds (This : access Typ)
                             return access Java.Awt.Rectangle.Typ'Class;

   procedure SetNormalBounds (This : access Typ;
                              P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetFocusOwner (This : access Typ)
                           return access Java.Awt.Component.Typ'Class;

   function GetMostRecentFocusOwner (This : access Typ)
                                     return access Java.Awt.Component.Typ'Class;

   procedure RestoreSubcomponentFocus (This : access Typ);

   procedure Reshape (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int);

   procedure AddInternalFrameListener (This : access Typ;
                                       P1_InternalFrameListener : access Standard.Javax.Swing.Event.InternalFrameListener.Typ'Class);

   procedure RemoveInternalFrameListener (This : access Typ;
                                          P1_InternalFrameListener : access Standard.Javax.Swing.Event.InternalFrameListener.Typ'Class);

   function GetInternalFrameListeners (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireInternalFrameEvent (This : access Typ;
                                     P1_Int : Java.Int);

   procedure DoDefaultCloseAction (This : access Typ);

   procedure SetDefaultCloseOperation (This : access Typ;
                                       P1_Int : Java.Int);

   function GetDefaultCloseOperation (This : access Typ)
                                      return Java.Int;

   procedure Pack (This : access Typ);

   procedure Show (This : access Typ);

   procedure Hide (This : access Typ);

   procedure Dispose (This : access Typ);

   procedure ToFront (This : access Typ);

   procedure ToBack (This : access Typ);

   --  final
   procedure SetFocusCycleRoot (This : access Typ;
                                P1_Boolean : Java.Boolean);

   --  final
   function IsFocusCycleRoot (This : access Typ)
                              return Java.Boolean;

   --  final
   function GetFocusCycleRootAncestor (This : access Typ)
                                       return access Java.Awt.Container.Typ'Class;

   --  final
   function GetWarningString (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  protected
   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CONTENT_PANE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   MENU_BAR_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   TITLE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   LAYERED_PANE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ROOT_PANE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   GLASS_PANE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   FRAME_ICON_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_SELECTED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_CLOSED_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_MAXIMUM_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_ICON_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JInternalFrame);
   pragma Import (Java, CreateRootPane, "createRootPane");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, IsRootPaneCheckingEnabled, "isRootPaneCheckingEnabled");
   pragma Import (Java, SetRootPaneCheckingEnabled, "setRootPaneCheckingEnabled");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, GetJMenuBar, "getJMenuBar");
   pragma Import (Java, SetJMenuBar, "setJMenuBar");
   pragma Import (Java, GetContentPane, "getContentPane");
   pragma Import (Java, SetContentPane, "setContentPane");
   pragma Import (Java, GetLayeredPane, "getLayeredPane");
   pragma Import (Java, SetLayeredPane, "setLayeredPane");
   pragma Import (Java, GetGlassPane, "getGlassPane");
   pragma Import (Java, SetGlassPane, "setGlassPane");
   pragma Import (Java, GetRootPane, "getRootPane");
   pragma Import (Java, SetRootPane, "setRootPane");
   pragma Import (Java, SetClosable, "setClosable");
   pragma Import (Java, IsClosable, "isClosable");
   pragma Import (Java, IsClosed, "isClosed");
   pragma Import (Java, SetClosed, "setClosed");
   pragma Import (Java, SetResizable, "setResizable");
   pragma Import (Java, IsResizable, "isResizable");
   pragma Import (Java, SetIconifiable, "setIconifiable");
   pragma Import (Java, IsIconifiable, "isIconifiable");
   pragma Import (Java, IsIcon, "isIcon");
   pragma Import (Java, SetIcon, "setIcon");
   pragma Import (Java, SetMaximizable, "setMaximizable");
   pragma Import (Java, IsMaximizable, "isMaximizable");
   pragma Import (Java, IsMaximum, "isMaximum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, GetTitle, "getTitle");
   pragma Import (Java, SetTitle, "setTitle");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, SetFrameIcon, "setFrameIcon");
   pragma Import (Java, GetFrameIcon, "getFrameIcon");
   pragma Import (Java, MoveToFront, "moveToFront");
   pragma Import (Java, MoveToBack, "moveToBack");
   pragma Import (Java, GetLastCursor, "getLastCursor");
   pragma Import (Java, SetCursor, "setCursor");
   pragma Import (Java, SetLayer, "setLayer");
   pragma Import (Java, GetLayer, "getLayer");
   pragma Import (Java, GetDesktopPane, "getDesktopPane");
   pragma Import (Java, SetDesktopIcon, "setDesktopIcon");
   pragma Import (Java, GetDesktopIcon, "getDesktopIcon");
   pragma Import (Java, GetNormalBounds, "getNormalBounds");
   pragma Import (Java, SetNormalBounds, "setNormalBounds");
   pragma Import (Java, GetFocusOwner, "getFocusOwner");
   pragma Import (Java, GetMostRecentFocusOwner, "getMostRecentFocusOwner");
   pragma Import (Java, RestoreSubcomponentFocus, "restoreSubcomponentFocus");
   pragma Import (Java, Reshape, "reshape");
   pragma Import (Java, AddInternalFrameListener, "addInternalFrameListener");
   pragma Import (Java, RemoveInternalFrameListener, "removeInternalFrameListener");
   pragma Import (Java, GetInternalFrameListeners, "getInternalFrameListeners");
   pragma Import (Java, FireInternalFrameEvent, "fireInternalFrameEvent");
   pragma Import (Java, DoDefaultCloseAction, "doDefaultCloseAction");
   pragma Import (Java, SetDefaultCloseOperation, "setDefaultCloseOperation");
   pragma Import (Java, GetDefaultCloseOperation, "getDefaultCloseOperation");
   pragma Import (Java, Pack, "pack");
   pragma Import (Java, Show, "show");
   pragma Import (Java, Hide, "hide");
   pragma Import (Java, Dispose, "dispose");
   pragma Import (Java, ToFront, "toFront");
   pragma Import (Java, ToBack, "toBack");
   pragma Import (Java, SetFocusCycleRoot, "setFocusCycleRoot");
   pragma Import (Java, IsFocusCycleRoot, "isFocusCycleRoot");
   pragma Import (Java, GetFocusCycleRootAncestor, "getFocusCycleRootAncestor");
   pragma Import (Java, GetWarningString, "getWarningString");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, CONTENT_PANE_PROPERTY, "CONTENT_PANE_PROPERTY");
   pragma Import (Java, MENU_BAR_PROPERTY, "MENU_BAR_PROPERTY");
   pragma Import (Java, TITLE_PROPERTY, "TITLE_PROPERTY");
   pragma Import (Java, LAYERED_PANE_PROPERTY, "LAYERED_PANE_PROPERTY");
   pragma Import (Java, ROOT_PANE_PROPERTY, "ROOT_PANE_PROPERTY");
   pragma Import (Java, GLASS_PANE_PROPERTY, "GLASS_PANE_PROPERTY");
   pragma Import (Java, FRAME_ICON_PROPERTY, "FRAME_ICON_PROPERTY");
   pragma Import (Java, IS_SELECTED_PROPERTY, "IS_SELECTED_PROPERTY");
   pragma Import (Java, IS_CLOSED_PROPERTY, "IS_CLOSED_PROPERTY");
   pragma Import (Java, IS_MAXIMUM_PROPERTY, "IS_MAXIMUM_PROPERTY");
   pragma Import (Java, IS_ICON_PROPERTY, "IS_ICON_PROPERTY");

end Javax.Swing.JInternalFrame;
pragma Import (Java, Javax.Swing.JInternalFrame, "javax.swing.JInternalFrame");
pragma Extensions_Allowed (Off);
