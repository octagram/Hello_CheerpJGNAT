pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Plaf.LabelUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.SwingConstants;
with Java.Awt.Image;

package Javax.Swing.JLabel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      LabelFor : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, LabelFor, "labelFor");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JLabel (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                        P3_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_JLabel (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_JLabel (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_JLabel (P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                        P2_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_JLabel (P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_JLabel (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.LabelUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_LabelUI : access Standard.Javax.Swing.Plaf.LabelUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetIcon (This : access Typ)
                     return access Javax.Swing.Icon.Typ'Class;

   procedure SetIcon (This : access Typ;
                      P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetDisabledIcon (This : access Typ)
                             return access Javax.Swing.Icon.Typ'Class;

   procedure SetDisabledIcon (This : access Typ;
                              P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   procedure SetDisplayedMnemonic (This : access Typ;
                                   P1_Int : Java.Int);

   procedure SetDisplayedMnemonic (This : access Typ;
                                   P1_Char : Java.Char);

   function GetDisplayedMnemonic (This : access Typ)
                                  return Java.Int;

   procedure SetDisplayedMnemonicIndex (This : access Typ;
                                        P1_Int : Java.Int);
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetDisplayedMnemonicIndex (This : access Typ)
                                       return Java.Int;

   --  protected
   function CheckHorizontalKey (This : access Typ;
                                P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int;

   --  protected
   function CheckVerticalKey (This : access Typ;
                              P1_Int : Java.Int;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Int;

   function GetIconTextGap (This : access Typ)
                            return Java.Int;

   procedure SetIconTextGap (This : access Typ;
                             P1_Int : Java.Int);

   function GetVerticalAlignment (This : access Typ)
                                  return Java.Int;

   procedure SetVerticalAlignment (This : access Typ;
                                   P1_Int : Java.Int);

   function GetHorizontalAlignment (This : access Typ)
                                    return Java.Int;

   procedure SetHorizontalAlignment (This : access Typ;
                                     P1_Int : Java.Int);

   function GetVerticalTextPosition (This : access Typ)
                                     return Java.Int;

   procedure SetVerticalTextPosition (This : access Typ;
                                      P1_Int : Java.Int);

   function GetHorizontalTextPosition (This : access Typ)
                                       return Java.Int;

   procedure SetHorizontalTextPosition (This : access Typ;
                                        P1_Int : Java.Int);

   function ImageUpdate (This : access Typ;
                         P1_Image : access Standard.Java.Awt.Image.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int : Java.Int)
                         return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetLabelFor (This : access Typ)
                         return access Java.Awt.Component.Typ'Class;

   procedure SetLabelFor (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JLabel);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, SetIcon, "setIcon");
   pragma Import (Java, GetDisabledIcon, "getDisabledIcon");
   pragma Import (Java, SetDisabledIcon, "setDisabledIcon");
   pragma Import (Java, SetDisplayedMnemonic, "setDisplayedMnemonic");
   pragma Import (Java, GetDisplayedMnemonic, "getDisplayedMnemonic");
   pragma Import (Java, SetDisplayedMnemonicIndex, "setDisplayedMnemonicIndex");
   pragma Import (Java, GetDisplayedMnemonicIndex, "getDisplayedMnemonicIndex");
   pragma Import (Java, CheckHorizontalKey, "checkHorizontalKey");
   pragma Import (Java, CheckVerticalKey, "checkVerticalKey");
   pragma Import (Java, GetIconTextGap, "getIconTextGap");
   pragma Import (Java, SetIconTextGap, "setIconTextGap");
   pragma Import (Java, GetVerticalAlignment, "getVerticalAlignment");
   pragma Import (Java, SetVerticalAlignment, "setVerticalAlignment");
   pragma Import (Java, GetHorizontalAlignment, "getHorizontalAlignment");
   pragma Import (Java, SetHorizontalAlignment, "setHorizontalAlignment");
   pragma Import (Java, GetVerticalTextPosition, "getVerticalTextPosition");
   pragma Import (Java, SetVerticalTextPosition, "setVerticalTextPosition");
   pragma Import (Java, GetHorizontalTextPosition, "getHorizontalTextPosition");
   pragma Import (Java, SetHorizontalTextPosition, "setHorizontalTextPosition");
   pragma Import (Java, ImageUpdate, "imageUpdate");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetLabelFor, "getLabelFor");
   pragma Import (Java, SetLabelFor, "setLabelFor");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JLabel;
pragma Import (Java, Javax.Swing.JLabel, "javax.swing.JLabel");
pragma Extensions_Allowed (Off);
