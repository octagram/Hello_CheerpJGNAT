pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Lang.Integer;
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Accessibility.AccessibleContext;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JLayeredPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JLayeredPane (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure RemoveAll (This : access Typ);

   function IsOptimizedDrawingEnabled (This : access Typ)
                                       return Java.Boolean;

   procedure PutLayer (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                       P2_Int : Java.Int);

   function GetLayer (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return Java.Int;

   function GetLayeredPaneAbove (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                 return access Javax.Swing.JLayeredPane.Typ'Class;

   procedure SetLayer (This : access Typ;
                       P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                       P2_Int : Java.Int);

   procedure SetLayer (This : access Typ;
                       P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int);

   function GetLayer (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                      return Java.Int;

   function GetIndexOf (This : access Typ;
                        P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                        return Java.Int;

   procedure MoveToFront (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure MoveToBack (This : access Typ;
                         P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetPosition (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Int : Java.Int);

   function GetPosition (This : access Typ;
                         P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                         return Java.Int;

   function HighestLayer (This : access Typ)
                          return Java.Int;

   function LowestLayer (This : access Typ)
                         return Java.Int;

   function GetComponentCountInLayer (This : access Typ;
                                      P1_Int : Java.Int)
                                      return Java.Int;

   function GetComponentsInLayer (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Standard.Java.Lang.Object.Ref;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   function GetComponentToLayer (This : access Typ)
                                 return access Java.Util.Hashtable.Typ'Class;

   --  protected
   function GetObjectForLayer (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Lang.Integer.Typ'Class;

   --  protected
   function InsertIndexForLayer (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int)
                                 return Java.Int;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_LAYER : access Java.Lang.Integer.Typ'Class;

   --  final
   PALETTE_LAYER : access Java.Lang.Integer.Typ'Class;

   --  final
   MODAL_LAYER : access Java.Lang.Integer.Typ'Class;

   --  final
   POPUP_LAYER : access Java.Lang.Integer.Typ'Class;

   --  final
   DRAG_LAYER : access Java.Lang.Integer.Typ'Class;

   --  final
   FRAME_CONTENT_LAYER : access Java.Lang.Integer.Typ'Class;

   --  final
   LAYER_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JLayeredPane);
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, IsOptimizedDrawingEnabled, "isOptimizedDrawingEnabled");
   pragma Import (Java, PutLayer, "putLayer");
   pragma Import (Java, GetLayer, "getLayer");
   pragma Import (Java, GetLayeredPaneAbove, "getLayeredPaneAbove");
   pragma Import (Java, SetLayer, "setLayer");
   pragma Import (Java, GetIndexOf, "getIndexOf");
   pragma Import (Java, MoveToFront, "moveToFront");
   pragma Import (Java, MoveToBack, "moveToBack");
   pragma Import (Java, SetPosition, "setPosition");
   pragma Import (Java, GetPosition, "getPosition");
   pragma Import (Java, HighestLayer, "highestLayer");
   pragma Import (Java, LowestLayer, "lowestLayer");
   pragma Import (Java, GetComponentCountInLayer, "getComponentCountInLayer");
   pragma Import (Java, GetComponentsInLayer, "getComponentsInLayer");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetComponentToLayer, "getComponentToLayer");
   pragma Import (Java, GetObjectForLayer, "getObjectForLayer");
   pragma Import (Java, InsertIndexForLayer, "insertIndexForLayer");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, DEFAULT_LAYER, "DEFAULT_LAYER");
   pragma Import (Java, PALETTE_LAYER, "PALETTE_LAYER");
   pragma Import (Java, MODAL_LAYER, "MODAL_LAYER");
   pragma Import (Java, POPUP_LAYER, "POPUP_LAYER");
   pragma Import (Java, DRAG_LAYER, "DRAG_LAYER");
   pragma Import (Java, FRAME_CONTENT_LAYER, "FRAME_CONTENT_LAYER");
   pragma Import (Java, LAYER_PROPERTY, "LAYER_PROPERTY");

end Javax.Swing.JLayeredPane;
pragma Import (Java, Javax.Swing.JLayeredPane, "javax.swing.JLayeredPane");
pragma Extensions_Allowed (Off);
