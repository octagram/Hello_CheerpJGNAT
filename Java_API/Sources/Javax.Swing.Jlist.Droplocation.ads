pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Swing.TransferHandler.DropLocation;

package Javax.Swing.JList.DropLocation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.TransferHandler.DropLocation.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIndex (This : access Typ)
                      return Java.Int;

   function IsInsert (This : access Typ)
                      return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, IsInsert, "isInsert");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.JList.DropLocation;
pragma Import (Java, Javax.Swing.JList.DropLocation, "javax.swing.JList$DropLocation");
pragma Extensions_Allowed (Off);
