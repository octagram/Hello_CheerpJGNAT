pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.DropMode;
limited with Javax.Swing.Event.ListSelectionListener;
limited with Javax.Swing.JList.DropLocation;
limited with Javax.Swing.ListCellRenderer;
limited with Javax.Swing.ListModel;
limited with Javax.Swing.ListSelectionModel;
limited with Javax.Swing.Plaf.ListUI;
limited with Javax.Swing.Text.Position.Bias;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.Scrollable;

package Javax.Swing.JList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JList (P1_ListModel : access Standard.Javax.Swing.ListModel.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_JList (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                       This : Ref := null)
                       return Ref;

   function New_JList (P1_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_JList (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ListUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_ListUI : access Standard.Javax.Swing.Plaf.ListUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetPrototypeCellValue (This : access Typ)
                                   return access Java.Lang.Object.Typ'Class;

   procedure SetPrototypeCellValue (This : access Typ;
                                    P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetFixedCellWidth (This : access Typ)
                               return Java.Int;

   procedure SetFixedCellWidth (This : access Typ;
                                P1_Int : Java.Int);

   function GetFixedCellHeight (This : access Typ)
                                return Java.Int;

   procedure SetFixedCellHeight (This : access Typ;
                                 P1_Int : Java.Int);

   function GetCellRenderer (This : access Typ)
                             return access Javax.Swing.ListCellRenderer.Typ'Class;

   procedure SetCellRenderer (This : access Typ;
                              P1_ListCellRenderer : access Standard.Javax.Swing.ListCellRenderer.Typ'Class);

   function GetSelectionForeground (This : access Typ)
                                    return access Java.Awt.Color.Typ'Class;

   procedure SetSelectionForeground (This : access Typ;
                                     P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetSelectionBackground (This : access Typ)
                                    return access Java.Awt.Color.Typ'Class;

   procedure SetSelectionBackground (This : access Typ;
                                     P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetVisibleRowCount (This : access Typ)
                                return Java.Int;

   procedure SetVisibleRowCount (This : access Typ;
                                 P1_Int : Java.Int);

   function GetLayoutOrientation (This : access Typ)
                                  return Java.Int;

   procedure SetLayoutOrientation (This : access Typ;
                                   P1_Int : Java.Int);

   function GetFirstVisibleIndex (This : access Typ)
                                  return Java.Int;

   function GetLastVisibleIndex (This : access Typ)
                                 return Java.Int;

   procedure EnsureIndexIsVisible (This : access Typ;
                                   P1_Int : Java.Int);

   procedure SetDragEnabled (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetDragEnabled (This : access Typ)
                            return Java.Boolean;

   --  final
   procedure SetDropMode (This : access Typ;
                          P1_DropMode : access Standard.Javax.Swing.DropMode.Typ'Class);

   --  final
   function GetDropMode (This : access Typ)
                         return access Javax.Swing.DropMode.Typ'Class;

   --  final
   function GetDropLocation (This : access Typ)
                             return access Javax.Swing.JList.DropLocation.Typ'Class;

   function GetNextMatch (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                          return Java.Int;

   function GetToolTipText (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function LocationToIndex (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return Java.Int;

   function IndexToLocation (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Awt.Point.Typ'Class;

   function GetCellBounds (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Awt.Rectangle.Typ'Class;

   function GetModel (This : access Typ)
                      return access Javax.Swing.ListModel.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_ListModel : access Standard.Javax.Swing.ListModel.Typ'Class);

   procedure SetListData (This : access Typ;
                          P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure SetListData (This : access Typ;
                          P1_Vector : access Standard.Java.Util.Vector.Typ'Class);

   --  protected
   function CreateSelectionModel (This : access Typ)
                                  return access Javax.Swing.ListSelectionModel.Typ'Class;

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.ListSelectionModel.Typ'Class;

   --  protected
   procedure FireSelectionValueChanged (This : access Typ;
                                        P1_Int : Java.Int;
                                        P2_Int : Java.Int;
                                        P3_Boolean : Java.Boolean);

   procedure AddListSelectionListener (This : access Typ;
                                       P1_ListSelectionListener : access Standard.Javax.Swing.Event.ListSelectionListener.Typ'Class);

   procedure RemoveListSelectionListener (This : access Typ;
                                          P1_ListSelectionListener : access Standard.Javax.Swing.Event.ListSelectionListener.Typ'Class);

   function GetListSelectionListeners (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   procedure SetSelectionModel (This : access Typ;
                                P1_ListSelectionModel : access Standard.Javax.Swing.ListSelectionModel.Typ'Class);

   procedure SetSelectionMode (This : access Typ;
                               P1_Int : Java.Int);

   function GetSelectionMode (This : access Typ)
                              return Java.Int;

   function GetAnchorSelectionIndex (This : access Typ)
                                     return Java.Int;

   function GetLeadSelectionIndex (This : access Typ)
                                   return Java.Int;

   function GetMinSelectionIndex (This : access Typ)
                                  return Java.Int;

   function GetMaxSelectionIndex (This : access Typ)
                                  return Java.Int;

   function IsSelectedIndex (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean;

   function IsSelectionEmpty (This : access Typ)
                              return Java.Boolean;

   procedure ClearSelection (This : access Typ);

   procedure SetSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure AddSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure RemoveSelectionInterval (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int);

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   function GetSelectedIndices (This : access Typ)
                                return Java.Int_Arr;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int);

   procedure SetSelectedIndices (This : access Typ;
                                 P1_Int_Arr : Java.Int_Arr);

   function GetSelectedValues (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetSelectedIndex (This : access Typ)
                              return Java.Int;

   function GetSelectedValue (This : access Typ)
                              return access Java.Lang.Object.Typ'Class;

   procedure SetSelectedValue (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Boolean : Java.Boolean);

   function GetPreferredScrollableViewportSize (This : access Typ)
                                                return access Java.Awt.Dimension.Typ'Class;

   function GetScrollableUnitIncrement (This : access Typ;
                                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int)
                                        return Java.Int;

   function GetScrollableBlockIncrement (This : access Typ;
                                         P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int)
                                         return Java.Int;

   function GetScrollableTracksViewportWidth (This : access Typ)
                                              return Java.Boolean;

   function GetScrollableTracksViewportHeight (This : access Typ)
                                               return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   VERTICAL : constant Java.Int;

   --  final
   VERTICAL_WRAP : constant Java.Int;

   --  final
   HORIZONTAL_WRAP : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JList);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetPrototypeCellValue, "getPrototypeCellValue");
   pragma Import (Java, SetPrototypeCellValue, "setPrototypeCellValue");
   pragma Import (Java, GetFixedCellWidth, "getFixedCellWidth");
   pragma Import (Java, SetFixedCellWidth, "setFixedCellWidth");
   pragma Import (Java, GetFixedCellHeight, "getFixedCellHeight");
   pragma Import (Java, SetFixedCellHeight, "setFixedCellHeight");
   pragma Import (Java, GetCellRenderer, "getCellRenderer");
   pragma Import (Java, SetCellRenderer, "setCellRenderer");
   pragma Import (Java, GetSelectionForeground, "getSelectionForeground");
   pragma Import (Java, SetSelectionForeground, "setSelectionForeground");
   pragma Import (Java, GetSelectionBackground, "getSelectionBackground");
   pragma Import (Java, SetSelectionBackground, "setSelectionBackground");
   pragma Import (Java, GetVisibleRowCount, "getVisibleRowCount");
   pragma Import (Java, SetVisibleRowCount, "setVisibleRowCount");
   pragma Import (Java, GetLayoutOrientation, "getLayoutOrientation");
   pragma Import (Java, SetLayoutOrientation, "setLayoutOrientation");
   pragma Import (Java, GetFirstVisibleIndex, "getFirstVisibleIndex");
   pragma Import (Java, GetLastVisibleIndex, "getLastVisibleIndex");
   pragma Import (Java, EnsureIndexIsVisible, "ensureIndexIsVisible");
   pragma Import (Java, SetDragEnabled, "setDragEnabled");
   pragma Import (Java, GetDragEnabled, "getDragEnabled");
   pragma Import (Java, SetDropMode, "setDropMode");
   pragma Import (Java, GetDropMode, "getDropMode");
   pragma Import (Java, GetDropLocation, "getDropLocation");
   pragma Import (Java, GetNextMatch, "getNextMatch");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, LocationToIndex, "locationToIndex");
   pragma Import (Java, IndexToLocation, "indexToLocation");
   pragma Import (Java, GetCellBounds, "getCellBounds");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, SetListData, "setListData");
   pragma Import (Java, CreateSelectionModel, "createSelectionModel");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, FireSelectionValueChanged, "fireSelectionValueChanged");
   pragma Import (Java, AddListSelectionListener, "addListSelectionListener");
   pragma Import (Java, RemoveListSelectionListener, "removeListSelectionListener");
   pragma Import (Java, GetListSelectionListeners, "getListSelectionListeners");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, SetSelectionMode, "setSelectionMode");
   pragma Import (Java, GetSelectionMode, "getSelectionMode");
   pragma Import (Java, GetAnchorSelectionIndex, "getAnchorSelectionIndex");
   pragma Import (Java, GetLeadSelectionIndex, "getLeadSelectionIndex");
   pragma Import (Java, GetMinSelectionIndex, "getMinSelectionIndex");
   pragma Import (Java, GetMaxSelectionIndex, "getMaxSelectionIndex");
   pragma Import (Java, IsSelectedIndex, "isSelectedIndex");
   pragma Import (Java, IsSelectionEmpty, "isSelectionEmpty");
   pragma Import (Java, ClearSelection, "clearSelection");
   pragma Import (Java, SetSelectionInterval, "setSelectionInterval");
   pragma Import (Java, AddSelectionInterval, "addSelectionInterval");
   pragma Import (Java, RemoveSelectionInterval, "removeSelectionInterval");
   pragma Import (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, GetSelectedIndices, "getSelectedIndices");
   pragma Import (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Import (Java, SetSelectedIndices, "setSelectedIndices");
   pragma Import (Java, GetSelectedValues, "getSelectedValues");
   pragma Import (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Import (Java, GetSelectedValue, "getSelectedValue");
   pragma Import (Java, SetSelectedValue, "setSelectedValue");
   pragma Import (Java, GetPreferredScrollableViewportSize, "getPreferredScrollableViewportSize");
   pragma Import (Java, GetScrollableUnitIncrement, "getScrollableUnitIncrement");
   pragma Import (Java, GetScrollableBlockIncrement, "getScrollableBlockIncrement");
   pragma Import (Java, GetScrollableTracksViewportWidth, "getScrollableTracksViewportWidth");
   pragma Import (Java, GetScrollableTracksViewportHeight, "getScrollableTracksViewportHeight");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, VERTICAL, "VERTICAL");
   pragma Import (Java, VERTICAL_WRAP, "VERTICAL_WRAP");
   pragma Import (Java, HORIZONTAL_WRAP, "HORIZONTAL_WRAP");

end Javax.Swing.JList;
pragma Import (Java, Javax.Swing.JList, "javax.swing.JList");
pragma Extensions_Allowed (Off);
