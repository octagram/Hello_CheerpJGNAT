pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.ComponentOrientation;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Point;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.ButtonModel;
limited with Javax.Swing.Event.MenuListener;
limited with Javax.Swing.JPopupMenu;
limited with Javax.Swing.KeyStroke;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JMenuItem;
with Javax.Swing.MenuElement;
with Javax.Swing.SwingConstants;

package Javax.Swing.JMenu is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            MenuElement_I : Javax.Swing.MenuElement.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JMenuItem.Typ(ItemSelectable_I,
                                     MenuContainer_I,
                                     ImageObserver_I,
                                     Serializable_I,
                                     Accessible_I,
                                     MenuElement_I,
                                     SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JMenu (This : Ref := null)
                       return Ref;

   function New_JMenu (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_JMenu (P1_Action : access Standard.Javax.Swing.Action.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_JMenu (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Boolean : Java.Boolean; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_ButtonModel : access Standard.Javax.Swing.ButtonModel.Typ'Class);

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   procedure SetSelected (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function IsPopupMenuVisible (This : access Typ)
                                return Java.Boolean;

   procedure SetPopupMenuVisible (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   --  protected
   function GetPopupMenuOrigin (This : access Typ)
                                return access Java.Awt.Point.Typ'Class;

   function GetDelay (This : access Typ)
                      return Java.Int;

   procedure SetDelay (This : access Typ;
                       P1_Int : Java.Int);

   procedure SetMenuLocation (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int);

   function Add (This : access Typ;
                 P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class)
                 return access Javax.Swing.JMenuItem.Typ'Class;

   function Add (This : access Typ;
                 P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                 return access Java.Awt.Component.Typ'Class;

   function Add (This : access Typ;
                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                 P2_Int : Java.Int)
                 return access Java.Awt.Component.Typ'Class;

   function Add (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Swing.JMenuItem.Typ'Class;

   function Add (This : access Typ;
                 P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                 return access Javax.Swing.JMenuItem.Typ'Class;

   --  protected
   function CreateActionComponent (This : access Typ;
                                   P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                                   return access Javax.Swing.JMenuItem.Typ'Class;

   --  protected
   function CreateActionChangeListener (This : access Typ;
                                        P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class)
                                        return access Java.Beans.PropertyChangeListener.Typ'Class;

   procedure AddSeparator (This : access Typ);

   procedure Insert (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int);

   function Insert (This : access Typ;
                    P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class;
                    P2_Int : Java.Int)
                    return access Javax.Swing.JMenuItem.Typ'Class;

   function Insert (This : access Typ;
                    P1_Action : access Standard.Javax.Swing.Action.Typ'Class;
                    P2_Int : Java.Int)
                    return access Javax.Swing.JMenuItem.Typ'Class;

   procedure InsertSeparator (This : access Typ;
                              P1_Int : Java.Int);

   function GetItem (This : access Typ;
                     P1_Int : Java.Int)
                     return access Javax.Swing.JMenuItem.Typ'Class;

   function GetItemCount (This : access Typ)
                          return Java.Int;

   function IsTearOff (This : access Typ)
                       return Java.Boolean;

   procedure Remove (This : access Typ;
                     P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveAll (This : access Typ);

   function GetMenuComponentCount (This : access Typ)
                                   return Java.Int;

   function GetMenuComponent (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Awt.Component.Typ'Class;

   function GetMenuComponents (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function IsTopLevelMenu (This : access Typ)
                            return Java.Boolean;

   function IsMenuComponent (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return Java.Boolean;

   function GetPopupMenu (This : access Typ)
                          return access Javax.Swing.JPopupMenu.Typ'Class;

   procedure AddMenuListener (This : access Typ;
                              P1_MenuListener : access Standard.Javax.Swing.Event.MenuListener.Typ'Class);

   procedure RemoveMenuListener (This : access Typ;
                                 P1_MenuListener : access Standard.Javax.Swing.Event.MenuListener.Typ'Class);

   function GetMenuListeners (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireMenuSelected (This : access Typ);

   --  protected
   procedure FireMenuDeselected (This : access Typ);

   --  protected
   procedure FireMenuCanceled (This : access Typ);

   procedure MenuSelectionChanged (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetSubElements (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   procedure ApplyComponentOrientation (This : access Typ;
                                        P1_ComponentOrientation : access Standard.Java.Awt.ComponentOrientation.Typ'Class);

   procedure SetComponentOrientation (This : access Typ;
                                      P1_ComponentOrientation : access Standard.Java.Awt.ComponentOrientation.Typ'Class);

   procedure SetAccelerator (This : access Typ;
                             P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class);

   --  protected
   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   procedure DoClick (This : access Typ;
                      P1_Int : Java.Int);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JMenu);
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, IsPopupMenuVisible, "isPopupMenuVisible");
   pragma Import (Java, SetPopupMenuVisible, "setPopupMenuVisible");
   pragma Import (Java, GetPopupMenuOrigin, "getPopupMenuOrigin");
   pragma Import (Java, GetDelay, "getDelay");
   pragma Import (Java, SetDelay, "setDelay");
   pragma Import (Java, SetMenuLocation, "setMenuLocation");
   pragma Import (Java, Add, "add");
   pragma Import (Java, CreateActionComponent, "createActionComponent");
   pragma Import (Java, CreateActionChangeListener, "createActionChangeListener");
   pragma Import (Java, AddSeparator, "addSeparator");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, InsertSeparator, "insertSeparator");
   pragma Import (Java, GetItem, "getItem");
   pragma Import (Java, GetItemCount, "getItemCount");
   pragma Import (Java, IsTearOff, "isTearOff");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, GetMenuComponentCount, "getMenuComponentCount");
   pragma Import (Java, GetMenuComponent, "getMenuComponent");
   pragma Import (Java, GetMenuComponents, "getMenuComponents");
   pragma Import (Java, IsTopLevelMenu, "isTopLevelMenu");
   pragma Import (Java, IsMenuComponent, "isMenuComponent");
   pragma Import (Java, GetPopupMenu, "getPopupMenu");
   pragma Import (Java, AddMenuListener, "addMenuListener");
   pragma Import (Java, RemoveMenuListener, "removeMenuListener");
   pragma Import (Java, GetMenuListeners, "getMenuListeners");
   pragma Import (Java, FireMenuSelected, "fireMenuSelected");
   pragma Import (Java, FireMenuDeselected, "fireMenuDeselected");
   pragma Import (Java, FireMenuCanceled, "fireMenuCanceled");
   pragma Import (Java, MenuSelectionChanged, "menuSelectionChanged");
   pragma Import (Java, GetSubElements, "getSubElements");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, ApplyComponentOrientation, "applyComponentOrientation");
   pragma Import (Java, SetComponentOrientation, "setComponentOrientation");
   pragma Import (Java, SetAccelerator, "setAccelerator");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, DoClick, "doClick");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JMenu;
pragma Import (Java, Javax.Swing.JMenu, "javax.swing.JMenu");
pragma Extensions_Allowed (Off);
