pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.JMenu;
limited with Javax.Swing.KeyStroke;
limited with Javax.Swing.MenuSelectionManager;
limited with Javax.Swing.Plaf.MenuBarUI;
limited with Javax.Swing.SingleSelectionModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.MenuElement;

package Javax.Swing.JMenuBar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            MenuElement_I : Javax.Swing.MenuElement.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JMenuBar (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.MenuBarUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_MenuBarUI : access Standard.Javax.Swing.Plaf.MenuBarUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.SingleSelectionModel.Typ'Class;

   procedure SetSelectionModel (This : access Typ;
                                P1_SingleSelectionModel : access Standard.Javax.Swing.SingleSelectionModel.Typ'Class);

   function Add (This : access Typ;
                 P1_JMenu : access Standard.Javax.Swing.JMenu.Typ'Class)
                 return access Javax.Swing.JMenu.Typ'Class;

   function GetMenu (This : access Typ;
                     P1_Int : Java.Int)
                     return access Javax.Swing.JMenu.Typ'Class;

   function GetMenuCount (This : access Typ)
                          return Java.Int;

   procedure SetHelpMenu (This : access Typ;
                          P1_JMenu : access Standard.Javax.Swing.JMenu.Typ'Class);

   function GetHelpMenu (This : access Typ)
                         return access Javax.Swing.JMenu.Typ'Class;

   function GetComponentIndex (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return Java.Int;

   procedure SetSelected (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   function IsBorderPainted (This : access Typ)
                             return Java.Boolean;

   procedure SetBorderPainted (This : access Typ;
                               P1_Boolean : Java.Boolean);

   --  protected
   procedure PaintBorder (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure SetMargin (This : access Typ;
                        P1_Insets : access Standard.Java.Awt.Insets.Typ'Class);

   function GetMargin (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   procedure ProcessMouseEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class;
                                P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                                P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);

   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                              P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                              P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);

   procedure MenuSelectionChanged (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetSubElements (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   --  protected
   function ProcessKeyBinding (This : access Typ;
                               P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                               P2_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Boolean : Java.Boolean)
                               return Java.Boolean;

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JMenuBar);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, Add, "add");
   pragma Import (Java, GetMenu, "getMenu");
   pragma Import (Java, GetMenuCount, "getMenuCount");
   pragma Import (Java, SetHelpMenu, "setHelpMenu");
   pragma Import (Java, GetHelpMenu, "getHelpMenu");
   pragma Import (Java, GetComponentIndex, "getComponentIndex");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, IsBorderPainted, "isBorderPainted");
   pragma Import (Java, SetBorderPainted, "setBorderPainted");
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, SetMargin, "setMargin");
   pragma Import (Java, GetMargin, "getMargin");
   pragma Import (Java, ProcessMouseEvent, "processMouseEvent");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, MenuSelectionChanged, "menuSelectionChanged");
   pragma Import (Java, GetSubElements, "getSubElements");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, ProcessKeyBinding, "processKeyBinding");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");

end Javax.Swing.JMenuBar;
pragma Import (Java, Javax.Swing.JMenuBar, "javax.swing.JMenuBar");
pragma Extensions_Allowed (Off);
