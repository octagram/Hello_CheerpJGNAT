pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.ButtonModel;
limited with Javax.Swing.Event.MenuDragMouseEvent;
limited with Javax.Swing.Event.MenuDragMouseListener;
limited with Javax.Swing.Event.MenuKeyEvent;
limited with Javax.Swing.Event.MenuKeyListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.KeyStroke;
limited with Javax.Swing.MenuSelectionManager;
limited with Javax.Swing.Plaf.MenuItemUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.AbstractButton;
with Javax.Swing.MenuElement;
with Javax.Swing.SwingConstants;

package Javax.Swing.JMenuItem is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            MenuElement_I : Javax.Swing.MenuElement.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.AbstractButton.Typ(ItemSelectable_I,
                                          MenuContainer_I,
                                          ImageObserver_I,
                                          Serializable_I,
                                          SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JMenuItem (This : Ref := null)
                           return Ref;

   function New_JMenuItem (P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JMenuItem (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JMenuItem (P1_Action : access Standard.Javax.Swing.Action.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JMenuItem (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JMenuItem (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetModel (This : access Typ;
                       P1_ButtonModel : access Standard.Javax.Swing.ButtonModel.Typ'Class);

   --  protected
   procedure Init (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                   P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   procedure SetUI (This : access Typ;
                    P1_MenuItemUI : access Standard.Javax.Swing.Plaf.MenuItemUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetArmed (This : access Typ;
                       P1_Boolean : Java.Boolean);

   function IsArmed (This : access Typ)
                     return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure SetAccelerator (This : access Typ;
                             P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class);

   function GetAccelerator (This : access Typ)
                            return access Javax.Swing.KeyStroke.Typ'Class;

   --  protected
   procedure ConfigurePropertiesFromAction (This : access Typ;
                                            P1_Action : access Standard.Javax.Swing.Action.Typ'Class);

   --  protected
   procedure ActionPropertyChanged (This : access Typ;
                                    P1_Action : access Standard.Javax.Swing.Action.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure ProcessMouseEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class;
                                P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                                P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);

   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                              P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                              P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);

   procedure ProcessMenuDragMouseEvent (This : access Typ;
                                        P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class);

   procedure ProcessMenuKeyEvent (This : access Typ;
                                  P1_MenuKeyEvent : access Standard.Javax.Swing.Event.MenuKeyEvent.Typ'Class);

   --  protected
   procedure FireMenuDragMouseEntered (This : access Typ;
                                       P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class);

   --  protected
   procedure FireMenuDragMouseExited (This : access Typ;
                                      P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class);

   --  protected
   procedure FireMenuDragMouseDragged (This : access Typ;
                                       P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class);

   --  protected
   procedure FireMenuDragMouseReleased (This : access Typ;
                                        P1_MenuDragMouseEvent : access Standard.Javax.Swing.Event.MenuDragMouseEvent.Typ'Class);

   --  protected
   procedure FireMenuKeyPressed (This : access Typ;
                                 P1_MenuKeyEvent : access Standard.Javax.Swing.Event.MenuKeyEvent.Typ'Class);

   --  protected
   procedure FireMenuKeyReleased (This : access Typ;
                                  P1_MenuKeyEvent : access Standard.Javax.Swing.Event.MenuKeyEvent.Typ'Class);

   --  protected
   procedure FireMenuKeyTyped (This : access Typ;
                               P1_MenuKeyEvent : access Standard.Javax.Swing.Event.MenuKeyEvent.Typ'Class);

   procedure MenuSelectionChanged (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetSubElements (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   procedure AddMenuDragMouseListener (This : access Typ;
                                       P1_MenuDragMouseListener : access Standard.Javax.Swing.Event.MenuDragMouseListener.Typ'Class);

   procedure RemoveMenuDragMouseListener (This : access Typ;
                                          P1_MenuDragMouseListener : access Standard.Javax.Swing.Event.MenuDragMouseListener.Typ'Class);

   function GetMenuDragMouseListeners (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   procedure AddMenuKeyListener (This : access Typ;
                                 P1_MenuKeyListener : access Standard.Javax.Swing.Event.MenuKeyListener.Typ'Class);

   procedure RemoveMenuKeyListener (This : access Typ;
                                    P1_MenuKeyListener : access Standard.Javax.Swing.Event.MenuKeyListener.Typ'Class);

   function GetMenuKeyListeners (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JMenuItem);
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, Init, "init");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetArmed, "setArmed");
   pragma Import (Java, IsArmed, "isArmed");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, SetAccelerator, "setAccelerator");
   pragma Import (Java, GetAccelerator, "getAccelerator");
   pragma Import (Java, ConfigurePropertiesFromAction, "configurePropertiesFromAction");
   pragma Import (Java, ActionPropertyChanged, "actionPropertyChanged");
   pragma Import (Java, ProcessMouseEvent, "processMouseEvent");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, ProcessMenuDragMouseEvent, "processMenuDragMouseEvent");
   pragma Import (Java, ProcessMenuKeyEvent, "processMenuKeyEvent");
   pragma Import (Java, FireMenuDragMouseEntered, "fireMenuDragMouseEntered");
   pragma Import (Java, FireMenuDragMouseExited, "fireMenuDragMouseExited");
   pragma Import (Java, FireMenuDragMouseDragged, "fireMenuDragMouseDragged");
   pragma Import (Java, FireMenuDragMouseReleased, "fireMenuDragMouseReleased");
   pragma Import (Java, FireMenuKeyPressed, "fireMenuKeyPressed");
   pragma Import (Java, FireMenuKeyReleased, "fireMenuKeyReleased");
   pragma Import (Java, FireMenuKeyTyped, "fireMenuKeyTyped");
   pragma Import (Java, MenuSelectionChanged, "menuSelectionChanged");
   pragma Import (Java, GetSubElements, "getSubElements");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, AddMenuDragMouseListener, "addMenuDragMouseListener");
   pragma Import (Java, RemoveMenuDragMouseListener, "removeMenuDragMouseListener");
   pragma Import (Java, GetMenuDragMouseListeners, "getMenuDragMouseListeners");
   pragma Import (Java, AddMenuKeyListener, "addMenuKeyListener");
   pragma Import (Java, RemoveMenuKeyListener, "removeMenuKeyListener");
   pragma Import (Java, GetMenuKeyListeners, "getMenuKeyListeners");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JMenuItem;
pragma Import (Java, Javax.Swing.JMenuItem, "javax.swing.JMenuItem");
pragma Extensions_Allowed (Off);
