pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Frame;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JDesktopPane;
limited with Javax.Swing.JDialog;
limited with Javax.Swing.JInternalFrame;
limited with Javax.Swing.Plaf.OptionPaneUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JOptionPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Icon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, Icon, "icon");

      --  protected
      Message : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Message, "message");

      --  protected
      Options : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Options, "options");

      --  protected
      InitialValue : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, InitialValue, "initialValue");

      --  protected
      MessageType : Java.Int;
      pragma Import (Java, MessageType, "messageType");

      --  protected
      OptionType : Java.Int;
      pragma Import (Java, OptionType, "optionType");

      --  protected
      Value : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Value, "value");

      --  protected
      SelectionValues : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, SelectionValues, "selectionValues");

      --  protected
      InputValue : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, InputValue, "inputValue");

      --  protected
      InitialSelectionValue : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, InitialSelectionValue, "initialSelectionValue");

      --  protected
      WantsInput : Java.Boolean;
      pragma Import (Java, WantsInput, "wantsInput");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ShowInputDialog (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowInputDialog (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.String.Typ'Class;

   function ShowInputDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowInputDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.String.Typ'Class;

   function ShowInputDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_Int : Java.Int)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowInputDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_Int : Java.Int;
                             P5_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                             P6_Object_Arr : access Java.Lang.Object.Arr_Obj;
                             P7_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   procedure ShowMessageDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Awt.HeadlessException.Except

   procedure ShowMessageDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_Int : Java.Int);
   --  can raise Java.Awt.HeadlessException.Except

   procedure ShowMessageDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_Int : Java.Int;
                                P5_Icon : access Standard.Javax.Swing.Icon.Typ'Class);
   --  can raise Java.Awt.HeadlessException.Except

   function ShowConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class;
                               P4_Int : Java.Int)
                               return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int)
                               return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int;
                               P6_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                               return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function ShowOptionDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                              P7_Object_Arr : access Java.Lang.Object.Arr_Obj;
                              P8_Object : access Standard.Java.Lang.Object.Typ'Class)
                              return Java.Int;
   --  can raise Java.Awt.HeadlessException.Except

   function CreateDialog (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.JDialog.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function CreateDialog (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.JDialog.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   procedure ShowInternalMessageDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                        P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure ShowInternalMessageDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                                        P4_Int : Java.Int);

   procedure ShowInternalMessageDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                                        P4_Int : Java.Int;
                                        P5_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function ShowInternalConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                       return Java.Int;

   function ShowInternalConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                       P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                       P3_String : access Standard.Java.Lang.String.Typ'Class;
                                       P4_Int : Java.Int)
                                       return Java.Int;

   function ShowInternalConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                       P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                       P3_String : access Standard.Java.Lang.String.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int)
                                       return Java.Int;

   function ShowInternalConfirmDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                       P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                       P3_String : access Standard.Java.Lang.String.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                                       return Java.Int;

   function ShowInternalOptionDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                      P3_String : access Standard.Java.Lang.String.Typ'Class;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                      P7_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                      P8_Object : access Standard.Java.Lang.Object.Typ'Class)
                                      return Java.Int;

   function ShowInternalInputDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                     return access Java.Lang.String.Typ'Class;

   function ShowInternalInputDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                                     P4_Int : Java.Int)
                                     return access Java.Lang.String.Typ'Class;

   function ShowInternalInputDialog (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                                     P4_Int : Java.Int;
                                     P5_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                     P6_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                     P7_Object : access Standard.Java.Lang.Object.Typ'Class)
                                     return access Java.Lang.Object.Typ'Class;

   function CreateInternalFrame (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Javax.Swing.JInternalFrame.Typ'Class;

   function GetFrameForComponent (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                  return access Java.Awt.Frame.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   function GetDesktopPaneForComponent (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                        return access Javax.Swing.JDesktopPane.Typ'Class;

   procedure SetRootFrame (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class);

   function GetRootFrame return access Java.Awt.Frame.Typ'Class;
   --  can raise Java.Awt.HeadlessException.Except

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JOptionPane (This : Ref := null)
                             return Ref;

   function New_JOptionPane (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_JOptionPane (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_JOptionPane (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_JOptionPane (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_JOptionPane (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                             P5_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                             This : Ref := null)
                             return Ref;

   function New_JOptionPane (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                             P5_Object_Arr : access Java.Lang.Object.Arr_Obj;
                             P6_Object : access Standard.Java.Lang.Object.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   procedure SetUI (This : access Typ;
                    P1_OptionPaneUI : access Standard.Javax.Swing.Plaf.OptionPaneUI.Typ'Class);

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.OptionPaneUI.Typ'Class;

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetMessage (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetMessage (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   procedure SetIcon (This : access Typ;
                      P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetIcon (This : access Typ)
                     return access Javax.Swing.Icon.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure SetOptions (This : access Typ;
                         P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function GetOptions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   procedure SetInitialValue (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetInitialValue (This : access Typ)
                             return access Java.Lang.Object.Typ'Class;

   procedure SetMessageType (This : access Typ;
                             P1_Int : Java.Int);

   function GetMessageType (This : access Typ)
                            return Java.Int;

   procedure SetOptionType (This : access Typ;
                            P1_Int : Java.Int);

   function GetOptionType (This : access Typ)
                           return Java.Int;

   procedure SetSelectionValues (This : access Typ;
                                 P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function GetSelectionValues (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   procedure SetInitialSelectionValue (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetInitialSelectionValue (This : access Typ)
                                      return access Java.Lang.Object.Typ'Class;

   procedure SetInputValue (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetInputValue (This : access Typ)
                           return access Java.Lang.Object.Typ'Class;

   function GetMaxCharactersPerLineCount (This : access Typ)
                                          return Java.Int;

   procedure SetWantsInput (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function GetWantsInput (This : access Typ)
                           return Java.Boolean;

   procedure SelectInitialValue (This : access Typ);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNINITIALIZED_VALUE : access Java.Lang.Object.Typ'Class;

   --  final
   DEFAULT_OPTION : constant Java.Int;

   --  final
   YES_NO_OPTION : constant Java.Int;

   --  final
   YES_NO_CANCEL_OPTION : constant Java.Int;

   --  final
   OK_CANCEL_OPTION : constant Java.Int;

   --  final
   YES_OPTION : constant Java.Int;

   --  final
   NO_OPTION : constant Java.Int;

   --  final
   CANCEL_OPTION : constant Java.Int;

   --  final
   OK_OPTION : constant Java.Int;

   --  final
   CLOSED_OPTION : constant Java.Int;

   --  final
   ERROR_MESSAGE : constant Java.Int;

   --  final
   INFORMATION_MESSAGE : constant Java.Int;

   --  final
   WARNING_MESSAGE : constant Java.Int;

   --  final
   QUESTION_MESSAGE : constant Java.Int;

   --  final
   PLAIN_MESSAGE : constant Java.Int;

   --  final
   ICON_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   MESSAGE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   VALUE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   OPTIONS_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   INITIAL_VALUE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   MESSAGE_TYPE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   OPTION_TYPE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SELECTION_VALUES_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   INITIAL_SELECTION_VALUE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   INPUT_VALUE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   WANTS_INPUT_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ShowInputDialog, "showInputDialog");
   pragma Import (Java, ShowMessageDialog, "showMessageDialog");
   pragma Import (Java, ShowConfirmDialog, "showConfirmDialog");
   pragma Import (Java, ShowOptionDialog, "showOptionDialog");
   pragma Import (Java, CreateDialog, "createDialog");
   pragma Import (Java, ShowInternalMessageDialog, "showInternalMessageDialog");
   pragma Import (Java, ShowInternalConfirmDialog, "showInternalConfirmDialog");
   pragma Import (Java, ShowInternalOptionDialog, "showInternalOptionDialog");
   pragma Import (Java, ShowInternalInputDialog, "showInternalInputDialog");
   pragma Import (Java, CreateInternalFrame, "createInternalFrame");
   pragma Import (Java, GetFrameForComponent, "getFrameForComponent");
   pragma Import (Java, GetDesktopPaneForComponent, "getDesktopPaneForComponent");
   pragma Import (Java, SetRootFrame, "setRootFrame");
   pragma Import (Java, GetRootFrame, "getRootFrame");
   pragma Java_Constructor (New_JOptionPane);
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetMessage, "setMessage");
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, SetIcon, "setIcon");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetOptions, "setOptions");
   pragma Import (Java, GetOptions, "getOptions");
   pragma Import (Java, SetInitialValue, "setInitialValue");
   pragma Import (Java, GetInitialValue, "getInitialValue");
   pragma Import (Java, SetMessageType, "setMessageType");
   pragma Import (Java, GetMessageType, "getMessageType");
   pragma Import (Java, SetOptionType, "setOptionType");
   pragma Import (Java, GetOptionType, "getOptionType");
   pragma Import (Java, SetSelectionValues, "setSelectionValues");
   pragma Import (Java, GetSelectionValues, "getSelectionValues");
   pragma Import (Java, SetInitialSelectionValue, "setInitialSelectionValue");
   pragma Import (Java, GetInitialSelectionValue, "getInitialSelectionValue");
   pragma Import (Java, SetInputValue, "setInputValue");
   pragma Import (Java, GetInputValue, "getInputValue");
   pragma Import (Java, GetMaxCharactersPerLineCount, "getMaxCharactersPerLineCount");
   pragma Import (Java, SetWantsInput, "setWantsInput");
   pragma Import (Java, GetWantsInput, "getWantsInput");
   pragma Import (Java, SelectInitialValue, "selectInitialValue");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, UNINITIALIZED_VALUE, "UNINITIALIZED_VALUE");
   pragma Import (Java, DEFAULT_OPTION, "DEFAULT_OPTION");
   pragma Import (Java, YES_NO_OPTION, "YES_NO_OPTION");
   pragma Import (Java, YES_NO_CANCEL_OPTION, "YES_NO_CANCEL_OPTION");
   pragma Import (Java, OK_CANCEL_OPTION, "OK_CANCEL_OPTION");
   pragma Import (Java, YES_OPTION, "YES_OPTION");
   pragma Import (Java, NO_OPTION, "NO_OPTION");
   pragma Import (Java, CANCEL_OPTION, "CANCEL_OPTION");
   pragma Import (Java, OK_OPTION, "OK_OPTION");
   pragma Import (Java, CLOSED_OPTION, "CLOSED_OPTION");
   pragma Import (Java, ERROR_MESSAGE, "ERROR_MESSAGE");
   pragma Import (Java, INFORMATION_MESSAGE, "INFORMATION_MESSAGE");
   pragma Import (Java, WARNING_MESSAGE, "WARNING_MESSAGE");
   pragma Import (Java, QUESTION_MESSAGE, "QUESTION_MESSAGE");
   pragma Import (Java, PLAIN_MESSAGE, "PLAIN_MESSAGE");
   pragma Import (Java, ICON_PROPERTY, "ICON_PROPERTY");
   pragma Import (Java, MESSAGE_PROPERTY, "MESSAGE_PROPERTY");
   pragma Import (Java, VALUE_PROPERTY, "VALUE_PROPERTY");
   pragma Import (Java, OPTIONS_PROPERTY, "OPTIONS_PROPERTY");
   pragma Import (Java, INITIAL_VALUE_PROPERTY, "INITIAL_VALUE_PROPERTY");
   pragma Import (Java, MESSAGE_TYPE_PROPERTY, "MESSAGE_TYPE_PROPERTY");
   pragma Import (Java, OPTION_TYPE_PROPERTY, "OPTION_TYPE_PROPERTY");
   pragma Import (Java, SELECTION_VALUES_PROPERTY, "SELECTION_VALUES_PROPERTY");
   pragma Import (Java, INITIAL_SELECTION_VALUE_PROPERTY, "INITIAL_SELECTION_VALUE_PROPERTY");
   pragma Import (Java, INPUT_VALUE_PROPERTY, "INPUT_VALUE_PROPERTY");
   pragma Import (Java, WANTS_INPUT_PROPERTY, "WANTS_INPUT_PROPERTY");

end Javax.Swing.JOptionPane;
pragma Import (Java, Javax.Swing.JOptionPane, "javax.swing.JOptionPane");
pragma Extensions_Allowed (Off);
