pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Text.Document;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JTextField;
with Javax.Swing.Scrollable;
with Javax.Swing.SwingConstants;

package Javax.Swing.JPasswordField is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JTextField.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I,
                                      Accessible_I,
                                      Scrollable_I,
                                      SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JPasswordField (This : Ref := null)
                                return Ref;

   function New_JPasswordField (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_JPasswordField (P1_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_JPasswordField (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_JPasswordField (P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure UpdateUI (This : access Typ);

   function GetEchoChar (This : access Typ)
                         return Java.Char;

   procedure SetEchoChar (This : access Typ;
                          P1_Char : Java.Char);

   function EchoCharIsSet (This : access Typ)
                           return Java.Boolean;

   procedure Cut (This : access Typ);

   procedure Copy (This : access Typ);

   function GetPassword (This : access Typ)
                         return Java.Char_Arr;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JPasswordField);
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetEchoChar, "getEchoChar");
   pragma Import (Java, SetEchoChar, "setEchoChar");
   pragma Import (Java, EchoCharIsSet, "echoCharIsSet");
   pragma Import (Java, Cut, "cut");
   pragma Import (Java, Copy, "copy");
   pragma Import (Java, GetPassword, "getPassword");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JPasswordField;
pragma Import (Java, Javax.Swing.JPasswordField, "javax.swing.JPasswordField");
pragma Extensions_Allowed (Off);
