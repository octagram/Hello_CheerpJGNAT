pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusEvent;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.Event.MenuKeyListener;
limited with Javax.Swing.Event.PopupMenuListener;
limited with Javax.Swing.JMenuItem;
limited with Javax.Swing.MenuSelectionManager;
limited with Javax.Swing.Plaf.PopupMenuUI;
limited with Javax.Swing.SingleSelectionModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.MenuElement;

package Javax.Swing.JPopupMenu is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            MenuElement_I : Javax.Swing.MenuElement.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDefaultLightWeightPopupEnabled (P1_Boolean : Java.Boolean);

   function GetDefaultLightWeightPopupEnabled return Java.Boolean;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JPopupMenu (This : Ref := null)
                            return Ref;

   function New_JPopupMenu (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.PopupMenuUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_PopupMenuUI : access Standard.Javax.Swing.Plaf.PopupMenuUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   procedure ProcessFocusEvent (This : access Typ;
                                P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   --  protected
   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.SingleSelectionModel.Typ'Class;

   procedure SetSelectionModel (This : access Typ;
                                P1_SingleSelectionModel : access Standard.Javax.Swing.SingleSelectionModel.Typ'Class);

   function Add (This : access Typ;
                 P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class)
                 return access Javax.Swing.JMenuItem.Typ'Class;

   function Add (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                 return access Javax.Swing.JMenuItem.Typ'Class;

   function Add (This : access Typ;
                 P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                 return access Javax.Swing.JMenuItem.Typ'Class;

   --  protected
   function CreateActionComponent (This : access Typ;
                                   P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                                   return access Javax.Swing.JMenuItem.Typ'Class;

   --  protected
   function CreateActionChangeListener (This : access Typ;
                                        P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class)
                                        return access Java.Beans.PropertyChangeListener.Typ'Class;

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure SetLightWeightPopupEnabled (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   function IsLightWeightPopupEnabled (This : access Typ)
                                       return Java.Boolean;

   function GetLabel (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure SetLabel (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AddSeparator (This : access Typ);

   procedure Insert (This : access Typ;
                     P1_Action : access Standard.Javax.Swing.Action.Typ'Class;
                     P2_Int : Java.Int);

   procedure Insert (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                     P2_Int : Java.Int);

   procedure AddPopupMenuListener (This : access Typ;
                                   P1_PopupMenuListener : access Standard.Javax.Swing.Event.PopupMenuListener.Typ'Class);

   procedure RemovePopupMenuListener (This : access Typ;
                                      P1_PopupMenuListener : access Standard.Javax.Swing.Event.PopupMenuListener.Typ'Class);

   function GetPopupMenuListeners (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   procedure AddMenuKeyListener (This : access Typ;
                                 P1_MenuKeyListener : access Standard.Javax.Swing.Event.MenuKeyListener.Typ'Class);

   procedure RemoveMenuKeyListener (This : access Typ;
                                    P1_MenuKeyListener : access Standard.Javax.Swing.Event.MenuKeyListener.Typ'Class);

   function GetMenuKeyListeners (This : access Typ)
                                 return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FirePopupMenuWillBecomeVisible (This : access Typ);

   --  protected
   procedure FirePopupMenuWillBecomeInvisible (This : access Typ);

   --  protected
   procedure FirePopupMenuCanceled (This : access Typ);

   procedure Pack (This : access Typ);

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   procedure SetLocation (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int);

   function GetInvoker (This : access Typ)
                        return access Java.Awt.Component.Typ'Class;

   procedure SetInvoker (This : access Typ;
                         P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure Show (This : access Typ;
                   P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                   P2_Int : Java.Int;
                   P3_Int : Java.Int);

   function GetComponentIndex (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return Java.Int;

   procedure SetPopupSize (This : access Typ;
                           P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   procedure SetPopupSize (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int);

   procedure SetSelected (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function IsBorderPainted (This : access Typ)
                             return Java.Boolean;

   procedure SetBorderPainted (This : access Typ;
                               P1_Boolean : Java.Boolean);

   --  protected
   procedure PaintBorder (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetMargin (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   procedure ProcessMouseEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class;
                                P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                                P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);

   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                              P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                              P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);

   procedure MenuSelectionChanged (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetSubElements (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   function IsPopupTrigger (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetDefaultLightWeightPopupEnabled, "setDefaultLightWeightPopupEnabled");
   pragma Import (Java, GetDefaultLightWeightPopupEnabled, "getDefaultLightWeightPopupEnabled");
   pragma Java_Constructor (New_JPopupMenu);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, ProcessFocusEvent, "processFocusEvent");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, Add, "add");
   pragma Import (Java, CreateActionComponent, "createActionComponent");
   pragma Import (Java, CreateActionChangeListener, "createActionChangeListener");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, SetLightWeightPopupEnabled, "setLightWeightPopupEnabled");
   pragma Import (Java, IsLightWeightPopupEnabled, "isLightWeightPopupEnabled");
   pragma Import (Java, GetLabel, "getLabel");
   pragma Import (Java, SetLabel, "setLabel");
   pragma Import (Java, AddSeparator, "addSeparator");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, AddPopupMenuListener, "addPopupMenuListener");
   pragma Import (Java, RemovePopupMenuListener, "removePopupMenuListener");
   pragma Import (Java, GetPopupMenuListeners, "getPopupMenuListeners");
   pragma Import (Java, AddMenuKeyListener, "addMenuKeyListener");
   pragma Import (Java, RemoveMenuKeyListener, "removeMenuKeyListener");
   pragma Import (Java, GetMenuKeyListeners, "getMenuKeyListeners");
   pragma Import (Java, FirePopupMenuWillBecomeVisible, "firePopupMenuWillBecomeVisible");
   pragma Import (Java, FirePopupMenuWillBecomeInvisible, "firePopupMenuWillBecomeInvisible");
   pragma Import (Java, FirePopupMenuCanceled, "firePopupMenuCanceled");
   pragma Import (Java, Pack, "pack");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, GetInvoker, "getInvoker");
   pragma Import (Java, SetInvoker, "setInvoker");
   pragma Import (Java, Show, "show");
   pragma Import (Java, GetComponentIndex, "getComponentIndex");
   pragma Import (Java, SetPopupSize, "setPopupSize");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, IsBorderPainted, "isBorderPainted");
   pragma Import (Java, SetBorderPainted, "setBorderPainted");
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetMargin, "getMargin");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, ProcessMouseEvent, "processMouseEvent");
   pragma Import (Java, MenuSelectionChanged, "menuSelectionChanged");
   pragma Import (Java, GetSubElements, "getSubElements");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, IsPopupTrigger, "isPopupTrigger");

end Javax.Swing.JPopupMenu;
pragma Import (Java, Javax.Swing.JPopupMenu, "javax.swing.JPopupMenu");
pragma Extensions_Allowed (Off);
