pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.BoundedRangeModel;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Plaf.ProgressBarUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.SwingConstants;

package Javax.Swing.JProgressBar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Orientation : Java.Int;
      pragma Import (Java, Orientation, "orientation");

      --  protected
      PaintBorder : Java.Boolean;
      pragma Import (Java, PaintBorder, "paintBorder");

      --  protected
      Model : access Javax.Swing.BoundedRangeModel.Typ'Class;
      pragma Import (Java, Model, "model");

      --  protected
      ProgressString : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ProgressString, "progressString");

      --  protected
      PaintString : Java.Boolean;
      pragma Import (Java, PaintString, "paintString");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JProgressBar (This : Ref := null)
                              return Ref;

   function New_JProgressBar (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_JProgressBar (P1_Int : Java.Int;
                              P2_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_JProgressBar (P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_JProgressBar (P1_BoundedRangeModel : access Standard.Javax.Swing.BoundedRangeModel.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   function IsStringPainted (This : access Typ)
                             return Java.Boolean;

   procedure SetStringPainted (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function GetString (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   procedure SetString (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPercentComplete (This : access Typ)
                                return Java.Double;

   function IsBorderPainted (This : access Typ)
                             return Java.Boolean;

   procedure SetBorderPainted (This : access Typ;
                               P1_Boolean : Java.Boolean);

   --  protected
   procedure PaintBorder (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ProgressBarUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_ProgressBarUI : access Standard.Javax.Swing.Plaf.ProgressBarUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function CreateChangeListener (This : access Typ)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function GetModel (This : access Typ)
                      return access Javax.Swing.BoundedRangeModel.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_BoundedRangeModel : access Standard.Javax.Swing.BoundedRangeModel.Typ'Class);

   function GetValue (This : access Typ)
                      return Java.Int;

   function GetMinimum (This : access Typ)
                        return Java.Int;

   function GetMaximum (This : access Typ)
                        return Java.Int;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int);

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int);

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int);

   procedure SetIndeterminate (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function IsIndeterminate (This : access Typ)
                             return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JProgressBar);
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, IsStringPainted, "isStringPainted");
   pragma Import (Java, SetStringPainted, "setStringPainted");
   pragma Import (Java, GetString, "getString");
   pragma Import (Java, SetString, "setString");
   pragma Import (Java, GetPercentComplete, "getPercentComplete");
   pragma Import (Java, IsBorderPainted, "isBorderPainted");
   pragma Import (Java, SetBorderPainted, "setBorderPainted");
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, SetIndeterminate, "setIndeterminate");
   pragma Import (Java, IsIndeterminate, "isIndeterminate");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JProgressBar;
pragma Import (Java, Javax.Swing.JProgressBar, "javax.swing.JProgressBar");
pragma Extensions_Allowed (Off);
