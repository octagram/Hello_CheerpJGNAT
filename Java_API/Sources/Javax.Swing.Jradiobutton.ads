pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.Icon;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JToggleButton;
with Javax.Swing.SwingConstants;

package Javax.Swing.JRadioButton is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JToggleButton.Typ(ItemSelectable_I,
                                         MenuContainer_I,
                                         ImageObserver_I,
                                         Serializable_I,
                                         Accessible_I,
                                         SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JRadioButton (This : Ref := null)
                              return Ref;

   function New_JRadioButton (P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JRadioButton (P1_Action : access Standard.Javax.Swing.Action.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JRadioButton (P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                              P2_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   function New_JRadioButton (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JRadioButton (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   function New_JRadioButton (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_JRadioButton (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                              P3_Boolean : Java.Boolean; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JRadioButton);
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JRadioButton;
pragma Import (Java, Javax.Swing.JRadioButton, "javax.swing.JRadioButton");
pragma Extensions_Allowed (Off);
