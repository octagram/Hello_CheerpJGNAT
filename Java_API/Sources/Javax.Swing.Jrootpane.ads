pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.LayoutManager;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JLayeredPane;
limited with Javax.Swing.JMenuBar;
limited with Javax.Swing.Plaf.RootPaneUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JRootPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      MenuBar : access Javax.Swing.JMenuBar.Typ'Class;
      pragma Import (Java, MenuBar, "menuBar");

      --  protected
      ContentPane : access Java.Awt.Container.Typ'Class;
      pragma Import (Java, ContentPane, "contentPane");

      --  protected
      LayeredPane : access Javax.Swing.JLayeredPane.Typ'Class;
      pragma Import (Java, LayeredPane, "layeredPane");

      --  protected
      GlassPane : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, GlassPane, "glassPane");

      --  protected
      DefaultButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, DefaultButton, "defaultButton");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JRootPane (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDoubleBuffered (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function GetWindowDecorationStyle (This : access Typ)
                                      return Java.Int;

   procedure SetWindowDecorationStyle (This : access Typ;
                                       P1_Int : Java.Int);

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.RootPaneUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_RootPaneUI : access Standard.Javax.Swing.Plaf.RootPaneUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function CreateLayeredPane (This : access Typ)
                               return access Javax.Swing.JLayeredPane.Typ'Class;

   --  protected
   function CreateContentPane (This : access Typ)
                               return access Java.Awt.Container.Typ'Class;

   --  protected
   function CreateGlassPane (This : access Typ)
                             return access Java.Awt.Component.Typ'Class;

   --  protected
   function CreateRootLayout (This : access Typ)
                              return access Java.Awt.LayoutManager.Typ'Class;

   procedure SetJMenuBar (This : access Typ;
                          P1_JMenuBar : access Standard.Javax.Swing.JMenuBar.Typ'Class);

   function GetJMenuBar (This : access Typ)
                         return access Javax.Swing.JMenuBar.Typ'Class;

   procedure SetContentPane (This : access Typ;
                             P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function GetContentPane (This : access Typ)
                            return access Java.Awt.Container.Typ'Class;

   procedure SetLayeredPane (This : access Typ;
                             P1_JLayeredPane : access Standard.Javax.Swing.JLayeredPane.Typ'Class);

   function GetLayeredPane (This : access Typ)
                            return access Javax.Swing.JLayeredPane.Typ'Class;

   procedure SetGlassPane (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetGlassPane (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   function IsValidateRoot (This : access Typ)
                            return Java.Boolean;

   function IsOptimizedDrawingEnabled (This : access Typ)
                                       return Java.Boolean;

   procedure AddNotify (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   procedure SetDefaultButton (This : access Typ;
                               P1_JButton : access Standard.Javax.Swing.JButton.Typ'Class);

   function GetDefaultButton (This : access Typ)
                              return access Javax.Swing.JButton.Typ'Class;

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NONE : constant Java.Int;

   --  final
   FRAME : constant Java.Int;

   --  final
   PLAIN_DIALOG : constant Java.Int;

   --  final
   INFORMATION_DIALOG : constant Java.Int;

   --  final
   ERROR_DIALOG : constant Java.Int;

   --  final
   COLOR_CHOOSER_DIALOG : constant Java.Int;

   --  final
   FILE_CHOOSER_DIALOG : constant Java.Int;

   --  final
   QUESTION_DIALOG : constant Java.Int;

   --  final
   WARNING_DIALOG : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JRootPane);
   pragma Import (Java, SetDoubleBuffered, "setDoubleBuffered");
   pragma Import (Java, GetWindowDecorationStyle, "getWindowDecorationStyle");
   pragma Import (Java, SetWindowDecorationStyle, "setWindowDecorationStyle");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, CreateLayeredPane, "createLayeredPane");
   pragma Import (Java, CreateContentPane, "createContentPane");
   pragma Import (Java, CreateGlassPane, "createGlassPane");
   pragma Import (Java, CreateRootLayout, "createRootLayout");
   pragma Import (Java, SetJMenuBar, "setJMenuBar");
   pragma Import (Java, GetJMenuBar, "getJMenuBar");
   pragma Import (Java, SetContentPane, "setContentPane");
   pragma Import (Java, GetContentPane, "getContentPane");
   pragma Import (Java, SetLayeredPane, "setLayeredPane");
   pragma Import (Java, GetLayeredPane, "getLayeredPane");
   pragma Import (Java, SetGlassPane, "setGlassPane");
   pragma Import (Java, GetGlassPane, "getGlassPane");
   pragma Import (Java, IsValidateRoot, "isValidateRoot");
   pragma Import (Java, IsOptimizedDrawingEnabled, "isOptimizedDrawingEnabled");
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, SetDefaultButton, "setDefaultButton");
   pragma Import (Java, GetDefaultButton, "getDefaultButton");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, NONE, "NONE");
   pragma Import (Java, FRAME, "FRAME");
   pragma Import (Java, PLAIN_DIALOG, "PLAIN_DIALOG");
   pragma Import (Java, INFORMATION_DIALOG, "INFORMATION_DIALOG");
   pragma Import (Java, ERROR_DIALOG, "ERROR_DIALOG");
   pragma Import (Java, COLOR_CHOOSER_DIALOG, "COLOR_CHOOSER_DIALOG");
   pragma Import (Java, FILE_CHOOSER_DIALOG, "FILE_CHOOSER_DIALOG");
   pragma Import (Java, QUESTION_DIALOG, "QUESTION_DIALOG");
   pragma Import (Java, WARNING_DIALOG, "WARNING_DIALOG");

end Javax.Swing.JRootPane;
pragma Import (Java, Javax.Swing.JRootPane, "javax.swing.JRootPane");
pragma Extensions_Allowed (Off);
