pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.AdjustmentListener;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.BoundedRangeModel;
limited with Javax.Swing.Plaf.ScrollBarUI;
with Java.Awt.Adjustable;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JScrollBar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Adjustable_I : Java.Awt.Adjustable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Model : access Javax.Swing.BoundedRangeModel.Typ'Class;
      pragma Import (Java, Model, "model");

      --  protected
      Orientation : Java.Int;
      pragma Import (Java, Orientation, "orientation");

      --  protected
      UnitIncrement : Java.Int;
      pragma Import (Java, UnitIncrement, "unitIncrement");

      --  protected
      BlockIncrement : Java.Int;
      pragma Import (Java, BlockIncrement, "blockIncrement");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JScrollBar (P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_JScrollBar (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_JScrollBar (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetUI (This : access Typ;
                    P1_ScrollBarUI : access Standard.Javax.Swing.Plaf.ScrollBarUI.Typ'Class);

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ScrollBarUI.Typ'Class;

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   function GetModel (This : access Typ)
                      return access Javax.Swing.BoundedRangeModel.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_BoundedRangeModel : access Standard.Javax.Swing.BoundedRangeModel.Typ'Class);

   function GetUnitIncrement (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   procedure SetUnitIncrement (This : access Typ;
                               P1_Int : Java.Int);

   function GetBlockIncrement (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   procedure SetBlockIncrement (This : access Typ;
                                P1_Int : Java.Int);

   function GetUnitIncrement (This : access Typ)
                              return Java.Int;

   function GetBlockIncrement (This : access Typ)
                               return Java.Int;

   function GetValue (This : access Typ)
                      return Java.Int;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int);

   function GetVisibleAmount (This : access Typ)
                              return Java.Int;

   procedure SetVisibleAmount (This : access Typ;
                               P1_Int : Java.Int);

   function GetMinimum (This : access Typ)
                        return Java.Int;

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int);

   function GetMaximum (This : access Typ)
                        return Java.Int;

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int);

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   procedure SetValues (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure AddAdjustmentListener (This : access Typ;
                                    P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class);

   procedure RemoveAdjustmentListener (This : access Typ;
                                       P1_AdjustmentListener : access Standard.Java.Awt.Event.AdjustmentListener.Typ'Class);

   function GetAdjustmentListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireAdjustmentValueChanged (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int);

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JScrollBar);
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetUnitIncrement, "getUnitIncrement");
   pragma Import (Java, SetUnitIncrement, "setUnitIncrement");
   pragma Import (Java, GetBlockIncrement, "getBlockIncrement");
   pragma Import (Java, SetBlockIncrement, "setBlockIncrement");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetVisibleAmount, "getVisibleAmount");
   pragma Import (Java, SetVisibleAmount, "setVisibleAmount");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Import (Java, SetValues, "setValues");
   pragma Import (Java, AddAdjustmentListener, "addAdjustmentListener");
   pragma Import (Java, RemoveAdjustmentListener, "removeAdjustmentListener");
   pragma Import (Java, GetAdjustmentListeners, "getAdjustmentListeners");
   pragma Import (Java, FireAdjustmentValueChanged, "fireAdjustmentValueChanged");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JScrollBar;
pragma Import (Java, Javax.Swing.JScrollBar, "javax.swing.JScrollBar");
pragma Extensions_Allowed (Off);
