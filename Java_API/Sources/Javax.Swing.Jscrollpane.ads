pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.ComponentOrientation;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.JScrollBar;
limited with Javax.Swing.JViewport;
limited with Javax.Swing.Plaf.ScrollPaneUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.ScrollPaneConstants;

package Javax.Swing.JScrollPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ScrollPaneConstants_I : Javax.Swing.ScrollPaneConstants.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      VerticalScrollBarPolicy : Java.Int;
      pragma Import (Java, VerticalScrollBarPolicy, "verticalScrollBarPolicy");

      --  protected
      HorizontalScrollBarPolicy : Java.Int;
      pragma Import (Java, HorizontalScrollBarPolicy, "horizontalScrollBarPolicy");

      --  protected
      Viewport : access Javax.Swing.JViewport.Typ'Class;
      pragma Import (Java, Viewport, "viewport");

      --  protected
      VerticalScrollBar : access Javax.Swing.JScrollBar.Typ'Class;
      pragma Import (Java, VerticalScrollBar, "verticalScrollBar");

      --  protected
      HorizontalScrollBar : access Javax.Swing.JScrollBar.Typ'Class;
      pragma Import (Java, HorizontalScrollBar, "horizontalScrollBar");

      --  protected
      RowHeader : access Javax.Swing.JViewport.Typ'Class;
      pragma Import (Java, RowHeader, "rowHeader");

      --  protected
      ColumnHeader : access Javax.Swing.JViewport.Typ'Class;
      pragma Import (Java, ColumnHeader, "columnHeader");

      --  protected
      LowerLeft : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, LowerLeft, "lowerLeft");

      --  protected
      LowerRight : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, LowerRight, "lowerRight");

      --  protected
      UpperLeft : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, UpperLeft, "upperLeft");

      --  protected
      UpperRight : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, UpperRight, "upperRight");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JScrollPane (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_JScrollPane (P1_Component : access Standard.Java.Awt.Component.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_JScrollPane (P1_Int : Java.Int;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_JScrollPane (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ScrollPaneUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_ScrollPaneUI : access Standard.Javax.Swing.Plaf.ScrollPaneUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   function IsValidateRoot (This : access Typ)
                            return Java.Boolean;

   function GetVerticalScrollBarPolicy (This : access Typ)
                                        return Java.Int;

   procedure SetVerticalScrollBarPolicy (This : access Typ;
                                         P1_Int : Java.Int);

   function GetHorizontalScrollBarPolicy (This : access Typ)
                                          return Java.Int;

   procedure SetHorizontalScrollBarPolicy (This : access Typ;
                                           P1_Int : Java.Int);

   function GetViewportBorder (This : access Typ)
                               return access Javax.Swing.Border.Border.Typ'Class;

   procedure SetViewportBorder (This : access Typ;
                                P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class);

   function GetViewportBorderBounds (This : access Typ)
                                     return access Java.Awt.Rectangle.Typ'Class;

   function CreateHorizontalScrollBar (This : access Typ)
                                       return access Javax.Swing.JScrollBar.Typ'Class;

   function GetHorizontalScrollBar (This : access Typ)
                                    return access Javax.Swing.JScrollBar.Typ'Class;

   procedure SetHorizontalScrollBar (This : access Typ;
                                     P1_JScrollBar : access Standard.Javax.Swing.JScrollBar.Typ'Class);

   function CreateVerticalScrollBar (This : access Typ)
                                     return access Javax.Swing.JScrollBar.Typ'Class;

   function GetVerticalScrollBar (This : access Typ)
                                  return access Javax.Swing.JScrollBar.Typ'Class;

   procedure SetVerticalScrollBar (This : access Typ;
                                   P1_JScrollBar : access Standard.Javax.Swing.JScrollBar.Typ'Class);

   --  protected
   function CreateViewport (This : access Typ)
                            return access Javax.Swing.JViewport.Typ'Class;

   function GetViewport (This : access Typ)
                         return access Javax.Swing.JViewport.Typ'Class;

   procedure SetViewport (This : access Typ;
                          P1_JViewport : access Standard.Javax.Swing.JViewport.Typ'Class);

   procedure SetViewportView (This : access Typ;
                              P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetRowHeader (This : access Typ)
                          return access Javax.Swing.JViewport.Typ'Class;

   procedure SetRowHeader (This : access Typ;
                           P1_JViewport : access Standard.Javax.Swing.JViewport.Typ'Class);

   procedure SetRowHeaderView (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetColumnHeader (This : access Typ)
                             return access Javax.Swing.JViewport.Typ'Class;

   procedure SetColumnHeader (This : access Typ;
                              P1_JViewport : access Standard.Javax.Swing.JViewport.Typ'Class);

   procedure SetColumnHeaderView (This : access Typ;
                                  P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetCorner (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Awt.Component.Typ'Class;

   procedure SetCorner (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetComponentOrientation (This : access Typ;
                                      P1_ComponentOrientation : access Standard.Java.Awt.ComponentOrientation.Typ'Class);

   function IsWheelScrollingEnabled (This : access Typ)
                                     return Java.Boolean;

   procedure SetWheelScrollingEnabled (This : access Typ;
                                       P1_Boolean : Java.Boolean);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JScrollPane);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, IsValidateRoot, "isValidateRoot");
   pragma Import (Java, GetVerticalScrollBarPolicy, "getVerticalScrollBarPolicy");
   pragma Import (Java, SetVerticalScrollBarPolicy, "setVerticalScrollBarPolicy");
   pragma Import (Java, GetHorizontalScrollBarPolicy, "getHorizontalScrollBarPolicy");
   pragma Import (Java, SetHorizontalScrollBarPolicy, "setHorizontalScrollBarPolicy");
   pragma Import (Java, GetViewportBorder, "getViewportBorder");
   pragma Import (Java, SetViewportBorder, "setViewportBorder");
   pragma Import (Java, GetViewportBorderBounds, "getViewportBorderBounds");
   pragma Import (Java, CreateHorizontalScrollBar, "createHorizontalScrollBar");
   pragma Import (Java, GetHorizontalScrollBar, "getHorizontalScrollBar");
   pragma Import (Java, SetHorizontalScrollBar, "setHorizontalScrollBar");
   pragma Import (Java, CreateVerticalScrollBar, "createVerticalScrollBar");
   pragma Import (Java, GetVerticalScrollBar, "getVerticalScrollBar");
   pragma Import (Java, SetVerticalScrollBar, "setVerticalScrollBar");
   pragma Import (Java, CreateViewport, "createViewport");
   pragma Import (Java, GetViewport, "getViewport");
   pragma Import (Java, SetViewport, "setViewport");
   pragma Import (Java, SetViewportView, "setViewportView");
   pragma Import (Java, GetRowHeader, "getRowHeader");
   pragma Import (Java, SetRowHeader, "setRowHeader");
   pragma Import (Java, SetRowHeaderView, "setRowHeaderView");
   pragma Import (Java, GetColumnHeader, "getColumnHeader");
   pragma Import (Java, SetColumnHeader, "setColumnHeader");
   pragma Import (Java, SetColumnHeaderView, "setColumnHeaderView");
   pragma Import (Java, GetCorner, "getCorner");
   pragma Import (Java, SetCorner, "setCorner");
   pragma Import (Java, SetComponentOrientation, "setComponentOrientation");
   pragma Import (Java, IsWheelScrollingEnabled, "isWheelScrollingEnabled");
   pragma Import (Java, SetWheelScrollingEnabled, "setWheelScrollingEnabled");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JScrollPane;
pragma Import (Java, Javax.Swing.JScrollPane, "javax.swing.JScrollPane");
pragma Extensions_Allowed (Off);
