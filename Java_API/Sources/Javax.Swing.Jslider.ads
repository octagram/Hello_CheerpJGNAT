pragma Extensions_Allowed (On);
limited with Java.Awt.Font;
limited with Java.Lang.String;
limited with Java.Util.Dictionary;
limited with Java.Util.Hashtable;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.BoundedRangeModel;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Plaf.SliderUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.SwingConstants;

package Javax.Swing.JSlider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      SliderModel : access Javax.Swing.BoundedRangeModel.Typ'Class;
      pragma Import (Java, SliderModel, "sliderModel");

      --  protected
      MajorTickSpacing : Java.Int;
      pragma Import (Java, MajorTickSpacing, "majorTickSpacing");

      --  protected
      MinorTickSpacing : Java.Int;
      pragma Import (Java, MinorTickSpacing, "minorTickSpacing");

      --  protected
      SnapToTicks : Java.Boolean;
      pragma Import (Java, SnapToTicks, "snapToTicks");

      --  protected
      Orientation : Java.Int;
      pragma Import (Java, Orientation, "orientation");

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JSlider (This : Ref := null)
                         return Ref;

   function New_JSlider (P1_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   function New_JSlider (P1_Int : Java.Int;
                         P2_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   function New_JSlider (P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   function New_JSlider (P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   function New_JSlider (P1_BoundedRangeModel : access Standard.Javax.Swing.BoundedRangeModel.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.SliderUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_SliderUI : access Standard.Javax.Swing.Plaf.SliderUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function CreateChangeListener (This : access Typ)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function GetModel (This : access Typ)
                      return access Javax.Swing.BoundedRangeModel.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_BoundedRangeModel : access Standard.Javax.Swing.BoundedRangeModel.Typ'Class);

   function GetValue (This : access Typ)
                      return Java.Int;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int);

   function GetMinimum (This : access Typ)
                        return Java.Int;

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int);

   function GetMaximum (This : access Typ)
                        return Java.Int;

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int);

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean;

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function GetExtent (This : access Typ)
                       return Java.Int;

   procedure SetExtent (This : access Typ;
                        P1_Int : Java.Int);

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetLabelTable (This : access Typ)
                           return access Java.Util.Dictionary.Typ'Class;

   procedure SetLabelTable (This : access Typ;
                            P1_Dictionary : access Standard.Java.Util.Dictionary.Typ'Class);

   --  protected
   procedure UpdateLabelUIs (This : access Typ);

   function CreateStandardLabels (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Java.Util.Hashtable.Typ'Class;

   function CreateStandardLabels (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int)
                                  return access Java.Util.Hashtable.Typ'Class;

   function GetInverted (This : access Typ)
                         return Java.Boolean;

   procedure SetInverted (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function GetMajorTickSpacing (This : access Typ)
                                 return Java.Int;

   procedure SetMajorTickSpacing (This : access Typ;
                                  P1_Int : Java.Int);

   function GetMinorTickSpacing (This : access Typ)
                                 return Java.Int;

   procedure SetMinorTickSpacing (This : access Typ;
                                  P1_Int : Java.Int);

   function GetSnapToTicks (This : access Typ)
                            return Java.Boolean;

   procedure SetSnapToTicks (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetPaintTicks (This : access Typ)
                           return Java.Boolean;

   procedure SetPaintTicks (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function GetPaintTrack (This : access Typ)
                           return Java.Boolean;

   procedure SetPaintTrack (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function GetPaintLabels (This : access Typ)
                            return Java.Boolean;

   procedure SetPaintLabels (This : access Typ;
                             P1_Boolean : Java.Boolean);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JSlider);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Import (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Import (Java, GetExtent, "getExtent");
   pragma Import (Java, SetExtent, "setExtent");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetLabelTable, "getLabelTable");
   pragma Import (Java, SetLabelTable, "setLabelTable");
   pragma Import (Java, UpdateLabelUIs, "updateLabelUIs");
   pragma Import (Java, CreateStandardLabels, "createStandardLabels");
   pragma Import (Java, GetInverted, "getInverted");
   pragma Import (Java, SetInverted, "setInverted");
   pragma Import (Java, GetMajorTickSpacing, "getMajorTickSpacing");
   pragma Import (Java, SetMajorTickSpacing, "setMajorTickSpacing");
   pragma Import (Java, GetMinorTickSpacing, "getMinorTickSpacing");
   pragma Import (Java, SetMinorTickSpacing, "setMinorTickSpacing");
   pragma Import (Java, GetSnapToTicks, "getSnapToTicks");
   pragma Import (Java, SetSnapToTicks, "setSnapToTicks");
   pragma Import (Java, GetPaintTicks, "getPaintTicks");
   pragma Import (Java, SetPaintTicks, "setPaintTicks");
   pragma Import (Java, GetPaintTrack, "getPaintTrack");
   pragma Import (Java, SetPaintTrack, "setPaintTrack");
   pragma Import (Java, GetPaintLabels, "getPaintLabels");
   pragma Import (Java, SetPaintLabels, "setPaintLabels");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JSlider;
pragma Import (Java, Javax.Swing.JSlider, "javax.swing.JSlider");
pragma Extensions_Allowed (Off);
