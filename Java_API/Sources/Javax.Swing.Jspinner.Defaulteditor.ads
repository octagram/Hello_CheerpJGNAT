pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.String;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.JFormattedTextField;
with Java.Awt.Image.ImageObserver;
with Java.Awt.LayoutManager;
with Java.Awt.MenuContainer;
with Java.Beans.PropertyChangeListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Event.ChangeListener;
with Javax.Swing.JPanel;

package Javax.Swing.JSpinner.DefaultEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ChangeListener_I : Javax.Swing.Event.ChangeListener.Ref)
    is new Javax.Swing.JPanel.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I,
                                  Accessible_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultEditor (P1_JSpinner : access Standard.Javax.Swing.JSpinner.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Dismiss (This : access Typ;
                      P1_JSpinner : access Standard.Javax.Swing.JSpinner.Typ'Class);

   function GetSpinner (This : access Typ)
                        return access Javax.Swing.JSpinner.Typ'Class;

   function GetTextField (This : access Typ)
                          return access Javax.Swing.JFormattedTextField.Typ'Class;

   procedure StateChanged (This : access Typ;
                           P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure CommitEdit (This : access Typ);
   --  can raise Java.Text.ParseException.Except

   function GetBaseline (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultEditor);
   pragma Import (Java, Dismiss, "dismiss");
   pragma Import (Java, GetSpinner, "getSpinner");
   pragma Import (Java, GetTextField, "getTextField");
   pragma Import (Java, StateChanged, "stateChanged");
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, CommitEdit, "commitEdit");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");

end Javax.Swing.JSpinner.DefaultEditor;
pragma Import (Java, Javax.Swing.JSpinner.DefaultEditor, "javax.swing.JSpinner$DefaultEditor");
pragma Extensions_Allowed (Off);
