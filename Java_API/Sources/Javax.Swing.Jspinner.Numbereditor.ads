pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Text.DecimalFormat;
limited with Javax.Swing.SpinnerNumberModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.LayoutManager;
with Java.Awt.MenuContainer;
with Java.Beans.PropertyChangeListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Event.ChangeListener;
with Javax.Swing.JSpinner.DefaultEditor;

package Javax.Swing.JSpinner.NumberEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ChangeListener_I : Javax.Swing.Event.ChangeListener.Ref)
    is new Javax.Swing.JSpinner.DefaultEditor.Typ(LayoutManager_I,
                                                  MenuContainer_I,
                                                  ImageObserver_I,
                                                  PropertyChangeListener_I,
                                                  Serializable_I,
                                                  Accessible_I,
                                                  ChangeListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NumberEditor (P1_JSpinner : access Standard.Javax.Swing.JSpinner.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_NumberEditor (P1_JSpinner : access Standard.Javax.Swing.JSpinner.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFormat (This : access Typ)
                       return access Java.Text.DecimalFormat.Typ'Class;

   function GetModel (This : access Typ)
                      return access Javax.Swing.SpinnerNumberModel.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NumberEditor);
   pragma Import (Java, GetFormat, "getFormat");
   pragma Import (Java, GetModel, "getModel");

end Javax.Swing.JSpinner.NumberEditor;
pragma Import (Java, Javax.Swing.JSpinner.NumberEditor, "javax.swing.JSpinner$NumberEditor");
pragma Extensions_Allowed (Off);
