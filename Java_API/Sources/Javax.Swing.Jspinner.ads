pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Plaf.SpinnerUI;
limited with Javax.Swing.SpinnerModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JSpinner is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JSpinner (P1_SpinnerModel : access Standard.Javax.Swing.SpinnerModel.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_JSpinner (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.SpinnerUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_SpinnerUI : access Standard.Javax.Swing.Plaf.SpinnerUI.Typ'Class);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure UpdateUI (This : access Typ);

   --  protected
   function CreateEditor (This : access Typ;
                          P1_SpinnerModel : access Standard.Javax.Swing.SpinnerModel.Typ'Class)
                          return access Javax.Swing.JComponent.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_SpinnerModel : access Standard.Javax.Swing.SpinnerModel.Typ'Class);

   function GetModel (This : access Typ)
                      return access Javax.Swing.SpinnerModel.Typ'Class;

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetNextValue (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function GetPreviousValue (This : access Typ)
                              return access Java.Lang.Object.Typ'Class;

   procedure SetEditor (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetEditor (This : access Typ)
                       return access Javax.Swing.JComponent.Typ'Class;

   procedure CommitEdit (This : access Typ);
   --  can raise Java.Text.ParseException.Except

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JSpinner);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, CreateEditor, "createEditor");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetNextValue, "getNextValue");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, GetPreviousValue, "getPreviousValue");
   pragma Import (Java, SetEditor, "setEditor");
   pragma Import (Java, GetEditor, "getEditor");
   pragma Import (Java, CommitEdit, "commitEdit");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JSpinner;
pragma Import (Java, Javax.Swing.JSpinner, "javax.swing.JSpinner");
pragma Extensions_Allowed (Off);
