pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Plaf.SplitPaneUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;

package Javax.Swing.JSplitPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Orientation : Java.Int;
      pragma Import (Java, Orientation, "orientation");

      --  protected
      ContinuousLayout : Java.Boolean;
      pragma Import (Java, ContinuousLayout, "continuousLayout");

      --  protected
      LeftComponent : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, LeftComponent, "leftComponent");

      --  protected
      RightComponent : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, RightComponent, "rightComponent");

      --  protected
      DividerSize : Java.Int;
      pragma Import (Java, DividerSize, "dividerSize");

      --  protected
      OneTouchExpandable : Java.Boolean;
      pragma Import (Java, OneTouchExpandable, "oneTouchExpandable");

      --  protected
      LastDividerLocation : Java.Int;
      pragma Import (Java, LastDividerLocation, "lastDividerLocation");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JSplitPane (This : Ref := null)
                            return Ref;

   function New_JSplitPane (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_JSplitPane (P1_Int : Java.Int;
                            P2_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;

   function New_JSplitPane (P1_Int : Java.Int;
                            P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P3_Component : access Standard.Java.Awt.Component.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_JSplitPane (P1_Int : Java.Int;
                            P2_Boolean : Java.Boolean;
                            P3_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P4_Component : access Standard.Java.Awt.Component.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetUI (This : access Typ;
                    P1_SplitPaneUI : access Standard.Javax.Swing.Plaf.SplitPaneUI.Typ'Class);

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.SplitPaneUI.Typ'Class;

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetDividerSize (This : access Typ;
                             P1_Int : Java.Int);

   function GetDividerSize (This : access Typ)
                            return Java.Int;

   procedure SetLeftComponent (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetLeftComponent (This : access Typ)
                              return access Java.Awt.Component.Typ'Class;

   procedure SetTopComponent (This : access Typ;
                              P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetTopComponent (This : access Typ)
                             return access Java.Awt.Component.Typ'Class;

   procedure SetRightComponent (This : access Typ;
                                P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetRightComponent (This : access Typ)
                               return access Java.Awt.Component.Typ'Class;

   procedure SetBottomComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetBottomComponent (This : access Typ)
                                return access Java.Awt.Component.Typ'Class;

   procedure SetOneTouchExpandable (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function IsOneTouchExpandable (This : access Typ)
                                  return Java.Boolean;

   procedure SetLastDividerLocation (This : access Typ;
                                     P1_Int : Java.Int);

   function GetLastDividerLocation (This : access Typ)
                                    return Java.Int;

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetContinuousLayout (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function IsContinuousLayout (This : access Typ)
                                return Java.Boolean;

   procedure SetResizeWeight (This : access Typ;
                              P1_Double : Java.Double);

   function GetResizeWeight (This : access Typ)
                             return Java.Double;

   procedure ResetToPreferredSizes (This : access Typ);

   procedure SetDividerLocation (This : access Typ;
                                 P1_Double : Java.Double);

   procedure SetDividerLocation (This : access Typ;
                                 P1_Int : Java.Int);

   function GetDividerLocation (This : access Typ)
                                return Java.Int;

   function GetMinimumDividerLocation (This : access Typ)
                                       return Java.Int;

   function GetMaximumDividerLocation (This : access Typ)
                                       return Java.Int;

   procedure Remove (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure RemoveAll (This : access Typ);

   function IsValidateRoot (This : access Typ)
                            return Java.Boolean;

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   --  protected
   procedure PaintChildren (This : access Typ;
                            P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   VERTICAL_SPLIT : constant Java.Int;

   --  final
   HORIZONTAL_SPLIT : constant Java.Int;

   --  final
   LEFT : constant access Java.Lang.String.Typ'Class;

   --  final
   RIGHT : constant access Java.Lang.String.Typ'Class;

   --  final
   TOP : constant access Java.Lang.String.Typ'Class;

   --  final
   BOTTOM : constant access Java.Lang.String.Typ'Class;

   --  final
   DIVIDER : constant access Java.Lang.String.Typ'Class;

   --  final
   ORIENTATION_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CONTINUOUS_LAYOUT_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DIVIDER_SIZE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ONE_TOUCH_EXPANDABLE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   LAST_DIVIDER_LOCATION_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DIVIDER_LOCATION_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   RESIZE_WEIGHT_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JSplitPane);
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetDividerSize, "setDividerSize");
   pragma Import (Java, GetDividerSize, "getDividerSize");
   pragma Import (Java, SetLeftComponent, "setLeftComponent");
   pragma Import (Java, GetLeftComponent, "getLeftComponent");
   pragma Import (Java, SetTopComponent, "setTopComponent");
   pragma Import (Java, GetTopComponent, "getTopComponent");
   pragma Import (Java, SetRightComponent, "setRightComponent");
   pragma Import (Java, GetRightComponent, "getRightComponent");
   pragma Import (Java, SetBottomComponent, "setBottomComponent");
   pragma Import (Java, GetBottomComponent, "getBottomComponent");
   pragma Import (Java, SetOneTouchExpandable, "setOneTouchExpandable");
   pragma Import (Java, IsOneTouchExpandable, "isOneTouchExpandable");
   pragma Import (Java, SetLastDividerLocation, "setLastDividerLocation");
   pragma Import (Java, GetLastDividerLocation, "getLastDividerLocation");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetContinuousLayout, "setContinuousLayout");
   pragma Import (Java, IsContinuousLayout, "isContinuousLayout");
   pragma Import (Java, SetResizeWeight, "setResizeWeight");
   pragma Import (Java, GetResizeWeight, "getResizeWeight");
   pragma Import (Java, ResetToPreferredSizes, "resetToPreferredSizes");
   pragma Import (Java, SetDividerLocation, "setDividerLocation");
   pragma Import (Java, GetDividerLocation, "getDividerLocation");
   pragma Import (Java, GetMinimumDividerLocation, "getMinimumDividerLocation");
   pragma Import (Java, GetMaximumDividerLocation, "getMaximumDividerLocation");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, IsValidateRoot, "isValidateRoot");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, PaintChildren, "paintChildren");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, VERTICAL_SPLIT, "VERTICAL_SPLIT");
   pragma Import (Java, HORIZONTAL_SPLIT, "HORIZONTAL_SPLIT");
   pragma Import (Java, LEFT, "LEFT");
   pragma Import (Java, RIGHT, "RIGHT");
   pragma Import (Java, TOP, "TOP");
   pragma Import (Java, BOTTOM, "BOTTOM");
   pragma Import (Java, DIVIDER, "DIVIDER");
   pragma Import (Java, ORIENTATION_PROPERTY, "ORIENTATION_PROPERTY");
   pragma Import (Java, CONTINUOUS_LAYOUT_PROPERTY, "CONTINUOUS_LAYOUT_PROPERTY");
   pragma Import (Java, DIVIDER_SIZE_PROPERTY, "DIVIDER_SIZE_PROPERTY");
   pragma Import (Java, ONE_TOUCH_EXPANDABLE_PROPERTY, "ONE_TOUCH_EXPANDABLE_PROPERTY");
   pragma Import (Java, LAST_DIVIDER_LOCATION_PROPERTY, "LAST_DIVIDER_LOCATION_PROPERTY");
   pragma Import (Java, DIVIDER_LOCATION_PROPERTY, "DIVIDER_LOCATION_PROPERTY");
   pragma Import (Java, RESIZE_WEIGHT_PROPERTY, "RESIZE_WEIGHT_PROPERTY");

end Javax.Swing.JSplitPane;
pragma Import (Java, Javax.Swing.JSplitPane, "javax.swing.JSplitPane");
pragma Extensions_Allowed (Off);
