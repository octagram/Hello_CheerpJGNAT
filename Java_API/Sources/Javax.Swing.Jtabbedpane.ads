pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Plaf.TabbedPaneUI;
limited with Javax.Swing.SingleSelectionModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.SwingConstants;

package Javax.Swing.JTabbedPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      TabPlacement : Java.Int;
      pragma Import (Java, TabPlacement, "tabPlacement");

      --  protected
      Model : access Javax.Swing.SingleSelectionModel.Typ'Class;
      pragma Import (Java, Model, "model");

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JTabbedPane (This : Ref := null)
                             return Ref;

   function New_JTabbedPane (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_JTabbedPane (P1_Int : Java.Int;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.TabbedPaneUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_TabbedPaneUI : access Standard.Javax.Swing.Plaf.TabbedPaneUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function CreateChangeListener (This : access Typ)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function GetModel (This : access Typ)
                      return access Javax.Swing.SingleSelectionModel.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_SingleSelectionModel : access Standard.Javax.Swing.SingleSelectionModel.Typ'Class);

   function GetTabPlacement (This : access Typ)
                             return Java.Int;

   procedure SetTabPlacement (This : access Typ;
                              P1_Int : Java.Int);

   function GetTabLayoutPolicy (This : access Typ)
                                return Java.Int;

   procedure SetTabLayoutPolicy (This : access Typ;
                                 P1_Int : Java.Int);

   function GetSelectedIndex (This : access Typ)
                              return Java.Int;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int);

   function GetSelectedComponent (This : access Typ)
                                  return access Java.Awt.Component.Typ'Class;

   procedure SetSelectedComponent (This : access Typ;
                                   P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure InsertTab (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                        P3_Component : access Standard.Java.Awt.Component.Typ'Class;
                        P4_String : access Standard.Java.Lang.String.Typ'Class;
                        P5_Int : Java.Int);

   procedure AddTab (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                     P3_Component : access Standard.Java.Awt.Component.Typ'Class;
                     P4_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AddTab (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                     P3_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure AddTab (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   function Add (This : access Typ;
                 P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                 return access Java.Awt.Component.Typ'Class;

   function Add (This : access Typ;
                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                 P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                 return access Java.Awt.Component.Typ'Class;

   function Add (This : access Typ;
                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                 P2_Int : Java.Int)
                 return access Java.Awt.Component.Typ'Class;

   procedure Add (This : access Typ;
                  P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Add (This : access Typ;
                  P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                  P3_Int : Java.Int);

   procedure RemoveTabAt (This : access Typ;
                          P1_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure RemoveAll (This : access Typ);

   function GetTabCount (This : access Typ)
                         return Java.Int;

   function GetTabRunCount (This : access Typ)
                            return Java.Int;

   function GetTitleAt (This : access Typ;
                        P1_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class;

   function GetIconAt (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Swing.Icon.Typ'Class;

   function GetDisabledIconAt (This : access Typ;
                               P1_Int : Java.Int)
                               return access Javax.Swing.Icon.Typ'Class;

   function GetToolTipTextAt (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Lang.String.Typ'Class;

   function GetBackgroundAt (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Awt.Color.Typ'Class;

   function GetForegroundAt (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Awt.Color.Typ'Class;

   function IsEnabledAt (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;

   function GetComponentAt (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Awt.Component.Typ'Class;

   function GetMnemonicAt (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;

   function GetDisplayedMnemonicIndexAt (This : access Typ;
                                         P1_Int : Java.Int)
                                         return Java.Int;

   function GetBoundsAt (This : access Typ;
                         P1_Int : Java.Int)
                         return access Java.Awt.Rectangle.Typ'Class;

   procedure SetTitleAt (This : access Typ;
                         P1_Int : Java.Int;
                         P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetIconAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   procedure SetDisabledIconAt (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   procedure SetToolTipTextAt (This : access Typ;
                               P1_Int : Java.Int;
                               P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetBackgroundAt (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure SetForegroundAt (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure SetEnabledAt (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Boolean : Java.Boolean);

   procedure SetComponentAt (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetDisplayedMnemonicIndexAt (This : access Typ;
                                          P1_Int : Java.Int;
                                          P2_Int : Java.Int);

   procedure SetMnemonicAt (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);

   function IndexOfTab (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Int;

   function IndexOfTab (This : access Typ;
                        P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                        return Java.Int;

   function IndexOfComponent (This : access Typ;
                              P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                              return Java.Int;

   function IndexAtLocation (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return Java.Int;

   function GetToolTipText (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   procedure SetTabComponentAt (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetTabComponentAt (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Awt.Component.Typ'Class;

   function IndexOfTabComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                 return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WRAP_TAB_LAYOUT : constant Java.Int;

   --  final
   SCROLL_TAB_LAYOUT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JTabbedPane);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetTabPlacement, "getTabPlacement");
   pragma Import (Java, SetTabPlacement, "setTabPlacement");
   pragma Import (Java, GetTabLayoutPolicy, "getTabLayoutPolicy");
   pragma Import (Java, SetTabLayoutPolicy, "setTabLayoutPolicy");
   pragma Import (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Import (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Import (Java, GetSelectedComponent, "getSelectedComponent");
   pragma Import (Java, SetSelectedComponent, "setSelectedComponent");
   pragma Import (Java, InsertTab, "insertTab");
   pragma Import (Java, AddTab, "addTab");
   pragma Import (Java, Add, "add");
   pragma Import (Java, RemoveTabAt, "removeTabAt");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, GetTabCount, "getTabCount");
   pragma Import (Java, GetTabRunCount, "getTabRunCount");
   pragma Import (Java, GetTitleAt, "getTitleAt");
   pragma Import (Java, GetIconAt, "getIconAt");
   pragma Import (Java, GetDisabledIconAt, "getDisabledIconAt");
   pragma Import (Java, GetToolTipTextAt, "getToolTipTextAt");
   pragma Import (Java, GetBackgroundAt, "getBackgroundAt");
   pragma Import (Java, GetForegroundAt, "getForegroundAt");
   pragma Import (Java, IsEnabledAt, "isEnabledAt");
   pragma Import (Java, GetComponentAt, "getComponentAt");
   pragma Import (Java, GetMnemonicAt, "getMnemonicAt");
   pragma Import (Java, GetDisplayedMnemonicIndexAt, "getDisplayedMnemonicIndexAt");
   pragma Import (Java, GetBoundsAt, "getBoundsAt");
   pragma Import (Java, SetTitleAt, "setTitleAt");
   pragma Import (Java, SetIconAt, "setIconAt");
   pragma Import (Java, SetDisabledIconAt, "setDisabledIconAt");
   pragma Import (Java, SetToolTipTextAt, "setToolTipTextAt");
   pragma Import (Java, SetBackgroundAt, "setBackgroundAt");
   pragma Import (Java, SetForegroundAt, "setForegroundAt");
   pragma Import (Java, SetEnabledAt, "setEnabledAt");
   pragma Import (Java, SetComponentAt, "setComponentAt");
   pragma Import (Java, SetDisplayedMnemonicIndexAt, "setDisplayedMnemonicIndexAt");
   pragma Import (Java, SetMnemonicAt, "setMnemonicAt");
   pragma Import (Java, IndexOfTab, "indexOfTab");
   pragma Import (Java, IndexOfComponent, "indexOfComponent");
   pragma Import (Java, IndexAtLocation, "indexAtLocation");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, SetTabComponentAt, "setTabComponentAt");
   pragma Import (Java, GetTabComponentAt, "getTabComponentAt");
   pragma Import (Java, IndexOfTabComponent, "indexOfTabComponent");
   pragma Import (Java, WRAP_TAB_LAYOUT, "WRAP_TAB_LAYOUT");
   pragma Import (Java, SCROLL_TAB_LAYOUT, "SCROLL_TAB_LAYOUT");

end Javax.Swing.JTabbedPane;
pragma Import (Java, Javax.Swing.JTabbedPane, "javax.swing.JTabbedPane");
pragma Extensions_Allowed (Off);
