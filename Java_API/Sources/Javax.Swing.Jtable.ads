pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Point;
limited with Java.Awt.Print.Printable;
limited with Java.Awt.Rectangle;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Text.MessageFormat;
limited with Java.Util.EventObject;
limited with Java.Util.Hashtable;
limited with Java.Util.Vector;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Print.Attribute.PrintRequestAttributeSet;
limited with Javax.Print.PrintService;
limited with Javax.Swing.DropMode;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ListSelectionEvent;
limited with Javax.Swing.Event.RowSorterEvent;
limited with Javax.Swing.Event.TableColumnModelEvent;
limited with Javax.Swing.Event.TableModelEvent;
limited with Javax.Swing.JTable.DropLocation;
limited with Javax.Swing.JTable.PrintMode;
limited with Javax.Swing.KeyStroke;
limited with Javax.Swing.ListSelectionModel;
limited with Javax.Swing.Plaf.TableUI;
limited with Javax.Swing.RowSorter;
limited with Javax.Swing.Table.JTableHeader;
limited with Javax.Swing.Table.TableCellEditor;
limited with Javax.Swing.Table.TableCellRenderer;
limited with Javax.Swing.Table.TableColumn;
limited with Javax.Swing.Table.TableColumnModel;
limited with Javax.Swing.Table.TableModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Event.CellEditorListener;
with Javax.Swing.Event.ListSelectionListener;
with Javax.Swing.Event.RowSorterListener;
with Javax.Swing.Event.TableColumnModelListener;
with Javax.Swing.Event.TableModelListener;
with Javax.Swing.JComponent;
with Javax.Swing.Scrollable;

package Javax.Swing.JTable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref;
            CellEditorListener_I : Javax.Swing.Event.CellEditorListener.Ref;
            ListSelectionListener_I : Javax.Swing.Event.ListSelectionListener.Ref;
            RowSorterListener_I : Javax.Swing.Event.RowSorterListener.Ref;
            TableColumnModelListener_I : Javax.Swing.Event.TableColumnModelListener.Ref;
            TableModelListener_I : Javax.Swing.Event.TableModelListener.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DataModel : access Javax.Swing.Table.TableModel.Typ'Class;
      pragma Import (Java, DataModel, "dataModel");

      --  protected
      ColumnModel : access Javax.Swing.Table.TableColumnModel.Typ'Class;
      pragma Import (Java, ColumnModel, "columnModel");

      --  protected
      SelectionModel : access Javax.Swing.ListSelectionModel.Typ'Class;
      pragma Import (Java, SelectionModel, "selectionModel");

      --  protected
      TableHeader : access Javax.Swing.Table.JTableHeader.Typ'Class;
      pragma Import (Java, TableHeader, "tableHeader");

      --  protected
      RowHeight : Java.Int;
      pragma Import (Java, RowHeight, "rowHeight");

      --  protected
      RowMargin : Java.Int;
      pragma Import (Java, RowMargin, "rowMargin");

      --  protected
      GridColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, GridColor, "gridColor");

      --  protected
      ShowHorizontalLines : Java.Boolean;
      pragma Import (Java, ShowHorizontalLines, "showHorizontalLines");

      --  protected
      ShowVerticalLines : Java.Boolean;
      pragma Import (Java, ShowVerticalLines, "showVerticalLines");

      --  protected
      AutoResizeMode : Java.Int;
      pragma Import (Java, AutoResizeMode, "autoResizeMode");

      --  protected
      AutoCreateColumnsFromModel : Java.Boolean;
      pragma Import (Java, AutoCreateColumnsFromModel, "autoCreateColumnsFromModel");

      --  protected
      PreferredViewportSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, PreferredViewportSize, "preferredViewportSize");

      --  protected
      RowSelectionAllowed : Java.Boolean;
      pragma Import (Java, RowSelectionAllowed, "rowSelectionAllowed");

      --  protected
      CellSelectionEnabled : Java.Boolean;
      pragma Import (Java, CellSelectionEnabled, "cellSelectionEnabled");

      --  protected
      EditorComp : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, EditorComp, "editorComp");

      --  protected
      CellEditor : access Javax.Swing.Table.TableCellEditor.Typ'Class;
      pragma Import (Java, CellEditor, "cellEditor");

      --  protected
      EditingColumn : Java.Int;
      pragma Import (Java, EditingColumn, "editingColumn");

      --  protected
      EditingRow : Java.Int;
      pragma Import (Java, EditingRow, "editingRow");

      --  protected
      DefaultRenderersByColumnClass : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, DefaultRenderersByColumnClass, "defaultRenderersByColumnClass");

      --  protected
      DefaultEditorsByColumnClass : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, DefaultEditorsByColumnClass, "defaultEditorsByColumnClass");

      --  protected
      SelectionForeground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectionForeground, "selectionForeground");

      --  protected
      SelectionBackground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectionBackground, "selectionBackground");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JTable (This : Ref := null)
                        return Ref;

   function New_JTable (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_JTable (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class;
                        P2_TableColumnModel : access Standard.Javax.Swing.Table.TableColumnModel.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_JTable (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class;
                        P2_TableColumnModel : access Standard.Javax.Swing.Table.TableColumnModel.Typ'Class;
                        P3_ListSelectionModel : access Standard.Javax.Swing.ListSelectionModel.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_JTable (P1_Int : Java.Int;
                        P2_Int : Java.Int; 
                        This : Ref := null)
                        return Ref;

   function New_JTable (P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                        P2_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   function New_JTable (P1_Object_Arr_2 : access Java.Lang.Object.Arr_2_Obj;
                        P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   --  protected
   procedure ConfigureEnclosingScrollPane (This : access Typ);

   procedure RemoveNotify (This : access Typ);

   --  protected
   procedure UnconfigureEnclosingScrollPane (This : access Typ);

   procedure SetTableHeader (This : access Typ;
                             P1_JTableHeader : access Standard.Javax.Swing.Table.JTableHeader.Typ'Class);

   function GetTableHeader (This : access Typ)
                            return access Javax.Swing.Table.JTableHeader.Typ'Class;

   procedure SetRowHeight (This : access Typ;
                           P1_Int : Java.Int);

   function GetRowHeight (This : access Typ)
                          return Java.Int;

   procedure SetRowHeight (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int);

   function GetRowHeight (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;

   procedure SetRowMargin (This : access Typ;
                           P1_Int : Java.Int);

   function GetRowMargin (This : access Typ)
                          return Java.Int;

   procedure SetIntercellSpacing (This : access Typ;
                                  P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetIntercellSpacing (This : access Typ)
                                 return access Java.Awt.Dimension.Typ'Class;

   procedure SetGridColor (This : access Typ;
                           P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetGridColor (This : access Typ)
                          return access Java.Awt.Color.Typ'Class;

   procedure SetShowGrid (This : access Typ;
                          P1_Boolean : Java.Boolean);

   procedure SetShowHorizontalLines (This : access Typ;
                                     P1_Boolean : Java.Boolean);

   procedure SetShowVerticalLines (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetShowHorizontalLines (This : access Typ)
                                    return Java.Boolean;

   function GetShowVerticalLines (This : access Typ)
                                  return Java.Boolean;

   procedure SetAutoResizeMode (This : access Typ;
                                P1_Int : Java.Int);

   function GetAutoResizeMode (This : access Typ)
                               return Java.Int;

   procedure SetAutoCreateColumnsFromModel (This : access Typ;
                                            P1_Boolean : Java.Boolean);

   function GetAutoCreateColumnsFromModel (This : access Typ)
                                           return Java.Boolean;

   procedure CreateDefaultColumnsFromModel (This : access Typ);

   procedure SetDefaultRenderer (This : access Typ;
                                 P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                 P2_TableCellRenderer : access Standard.Javax.Swing.Table.TableCellRenderer.Typ'Class);

   function GetDefaultRenderer (This : access Typ;
                                P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                return access Javax.Swing.Table.TableCellRenderer.Typ'Class;

   procedure SetDefaultEditor (This : access Typ;
                               P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                               P2_TableCellEditor : access Standard.Javax.Swing.Table.TableCellEditor.Typ'Class);

   function GetDefaultEditor (This : access Typ;
                              P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                              return access Javax.Swing.Table.TableCellEditor.Typ'Class;

   procedure SetDragEnabled (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetDragEnabled (This : access Typ)
                            return Java.Boolean;

   --  final
   procedure SetDropMode (This : access Typ;
                          P1_DropMode : access Standard.Javax.Swing.DropMode.Typ'Class);

   --  final
   function GetDropMode (This : access Typ)
                         return access Javax.Swing.DropMode.Typ'Class;

   --  final
   function GetDropLocation (This : access Typ)
                             return access Javax.Swing.JTable.DropLocation.Typ'Class;

   procedure SetAutoCreateRowSorter (This : access Typ;
                                     P1_Boolean : Java.Boolean);

   function GetAutoCreateRowSorter (This : access Typ)
                                    return Java.Boolean;

   procedure SetUpdateSelectionOnSort (This : access Typ;
                                       P1_Boolean : Java.Boolean);

   function GetUpdateSelectionOnSort (This : access Typ)
                                      return Java.Boolean;

   procedure SetRowSorter (This : access Typ;
                           P1_RowSorter : access Standard.Javax.Swing.RowSorter.Typ'Class);

   function GetRowSorter (This : access Typ)
                          return access Javax.Swing.RowSorter.Typ'Class;

   procedure SetSelectionMode (This : access Typ;
                               P1_Int : Java.Int);

   procedure SetRowSelectionAllowed (This : access Typ;
                                     P1_Boolean : Java.Boolean);

   function GetRowSelectionAllowed (This : access Typ)
                                    return Java.Boolean;

   procedure SetColumnSelectionAllowed (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function GetColumnSelectionAllowed (This : access Typ)
                                       return Java.Boolean;

   procedure SetCellSelectionEnabled (This : access Typ;
                                      P1_Boolean : Java.Boolean);

   function GetCellSelectionEnabled (This : access Typ)
                                     return Java.Boolean;

   procedure SelectAll (This : access Typ);

   procedure ClearSelection (This : access Typ);

   procedure SetRowSelectionInterval (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int);

   procedure SetColumnSelectionInterval (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int);

   procedure AddRowSelectionInterval (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int);

   procedure AddColumnSelectionInterval (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int);

   procedure RemoveRowSelectionInterval (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int);

   procedure RemoveColumnSelectionInterval (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Int : Java.Int);

   function GetSelectedRow (This : access Typ)
                            return Java.Int;

   function GetSelectedColumn (This : access Typ)
                               return Java.Int;

   function GetSelectedRows (This : access Typ)
                             return Java.Int_Arr;

   function GetSelectedColumns (This : access Typ)
                                return Java.Int_Arr;

   function GetSelectedRowCount (This : access Typ)
                                 return Java.Int;

   function GetSelectedColumnCount (This : access Typ)
                                    return Java.Int;

   function IsRowSelected (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean;

   function IsColumnSelected (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Boolean;

   function IsCellSelected (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Boolean;

   procedure ChangeSelection (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Boolean : Java.Boolean;
                              P4_Boolean : Java.Boolean);

   function GetSelectionForeground (This : access Typ)
                                    return access Java.Awt.Color.Typ'Class;

   procedure SetSelectionForeground (This : access Typ;
                                     P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetSelectionBackground (This : access Typ)
                                    return access Java.Awt.Color.Typ'Class;

   procedure SetSelectionBackground (This : access Typ;
                                     P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetColumn (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Javax.Swing.Table.TableColumn.Typ'Class;

   function ConvertColumnIndexToModel (This : access Typ;
                                       P1_Int : Java.Int)
                                       return Java.Int;

   function ConvertColumnIndexToView (This : access Typ;
                                      P1_Int : Java.Int)
                                      return Java.Int;

   function ConvertRowIndexToView (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Int;

   function ConvertRowIndexToModel (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Java.Int;

   function GetRowCount (This : access Typ)
                         return Java.Int;

   function GetColumnCount (This : access Typ)
                            return Java.Int;

   function GetColumnName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;

   function GetColumnClass (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.Class.Typ'Class;

   function GetValueAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return access Java.Lang.Object.Typ'Class;

   procedure SetValueAt (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);

   function IsCellEditable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Boolean;

   procedure AddColumn (This : access Typ;
                        P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class);

   procedure RemoveColumn (This : access Typ;
                           P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class);

   procedure MoveColumn (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int);

   function ColumnAtPoint (This : access Typ;
                           P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                           return Java.Int;

   function RowAtPoint (This : access Typ;
                        P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                        return Java.Int;

   function GetCellRect (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Boolean : Java.Boolean)
                         return access Java.Awt.Rectangle.Typ'Class;

   procedure DoLayout (This : access Typ);

   procedure SizeColumnsToFit (This : access Typ;
                               P1_Int : Java.Int);

   function GetToolTipText (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   procedure SetSurrendersFocusOnKeystroke (This : access Typ;
                                            P1_Boolean : Java.Boolean);

   function GetSurrendersFocusOnKeystroke (This : access Typ)
                                           return Java.Boolean;

   function EditCellAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return Java.Boolean;

   function EditCellAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                        return Java.Boolean;

   function IsEditing (This : access Typ)
                       return Java.Boolean;

   function GetEditorComponent (This : access Typ)
                                return access Java.Awt.Component.Typ'Class;

   function GetEditingColumn (This : access Typ)
                              return Java.Int;

   function GetEditingRow (This : access Typ)
                           return Java.Int;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.TableUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_TableUI : access Standard.Javax.Swing.Plaf.TableUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class);

   function GetModel (This : access Typ)
                      return access Javax.Swing.Table.TableModel.Typ'Class;

   procedure SetColumnModel (This : access Typ;
                             P1_TableColumnModel : access Standard.Javax.Swing.Table.TableColumnModel.Typ'Class);

   function GetColumnModel (This : access Typ)
                            return access Javax.Swing.Table.TableColumnModel.Typ'Class;

   procedure SetSelectionModel (This : access Typ;
                                P1_ListSelectionModel : access Standard.Javax.Swing.ListSelectionModel.Typ'Class);

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.ListSelectionModel.Typ'Class;

   procedure SorterChanged (This : access Typ;
                            P1_RowSorterEvent : access Standard.Javax.Swing.Event.RowSorterEvent.Typ'Class);

   procedure TableChanged (This : access Typ;
                           P1_TableModelEvent : access Standard.Javax.Swing.Event.TableModelEvent.Typ'Class);

   procedure ColumnAdded (This : access Typ;
                          P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   procedure ColumnRemoved (This : access Typ;
                            P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   procedure ColumnMoved (This : access Typ;
                          P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   procedure ColumnMarginChanged (This : access Typ;
                                  P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);

   procedure ColumnSelectionChanged (This : access Typ;
                                     P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class);

   procedure ValueChanged (This : access Typ;
                           P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class);

   procedure EditingStopped (This : access Typ;
                             P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);

   procedure EditingCanceled (This : access Typ;
                              P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);

   procedure SetPreferredScrollableViewportSize (This : access Typ;
                                                 P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetPreferredScrollableViewportSize (This : access Typ)
                                                return access Java.Awt.Dimension.Typ'Class;

   function GetScrollableUnitIncrement (This : access Typ;
                                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int)
                                        return Java.Int;

   function GetScrollableBlockIncrement (This : access Typ;
                                         P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int)
                                         return Java.Int;

   function GetScrollableTracksViewportWidth (This : access Typ)
                                              return Java.Boolean;

   function GetScrollableTracksViewportHeight (This : access Typ)
                                               return Java.Boolean;

   procedure SetFillsViewportHeight (This : access Typ;
                                     P1_Boolean : Java.Boolean);

   function GetFillsViewportHeight (This : access Typ)
                                    return Java.Boolean;

   --  protected
   function ProcessKeyBinding (This : access Typ;
                               P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                               P2_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Boolean : Java.Boolean)
                               return Java.Boolean;

   --  protected
   procedure CreateDefaultRenderers (This : access Typ);

   --  protected
   procedure CreateDefaultEditors (This : access Typ);

   --  protected
   procedure InitializeLocalVars (This : access Typ);

   --  protected
   function CreateDefaultDataModel (This : access Typ)
                                    return access Javax.Swing.Table.TableModel.Typ'Class;

   --  protected
   function CreateDefaultColumnModel (This : access Typ)
                                      return access Javax.Swing.Table.TableColumnModel.Typ'Class;

   --  protected
   function CreateDefaultSelectionModel (This : access Typ)
                                         return access Javax.Swing.ListSelectionModel.Typ'Class;

   --  protected
   function CreateDefaultTableHeader (This : access Typ)
                                      return access Javax.Swing.Table.JTableHeader.Typ'Class;

   --  protected
   procedure ResizeAndRepaint (This : access Typ);

   function GetCellEditor (This : access Typ)
                           return access Javax.Swing.Table.TableCellEditor.Typ'Class;

   procedure SetCellEditor (This : access Typ;
                            P1_TableCellEditor : access Standard.Javax.Swing.Table.TableCellEditor.Typ'Class);

   procedure SetEditingColumn (This : access Typ;
                               P1_Int : Java.Int);

   procedure SetEditingRow (This : access Typ;
                            P1_Int : Java.Int);

   function GetCellRenderer (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Javax.Swing.Table.TableCellRenderer.Typ'Class;

   function PrepareRenderer (This : access Typ;
                             P1_TableCellRenderer : access Standard.Javax.Swing.Table.TableCellRenderer.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int)
                             return access Java.Awt.Component.Typ'Class;

   function GetCellEditor (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Javax.Swing.Table.TableCellEditor.Typ'Class;

   function PrepareEditor (This : access Typ;
                           P1_TableCellEditor : access Standard.Javax.Swing.Table.TableCellEditor.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int)
                           return access Java.Awt.Component.Typ'Class;

   procedure RemoveEditor (This : access Typ);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function Print (This : access Typ)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except

   function Print (This : access Typ;
                   P1_PrintMode : access Standard.Javax.Swing.JTable.PrintMode.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except

   function Print (This : access Typ;
                   P1_PrintMode : access Standard.Javax.Swing.JTable.PrintMode.Typ'Class;
                   P2_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P3_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except

   function Print (This : access Typ;
                   P1_PrintMode : access Standard.Javax.Swing.JTable.PrintMode.Typ'Class;
                   P2_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P3_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P4_Boolean : Java.Boolean;
                   P5_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class;
                   P6_Boolean : Java.Boolean)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except and
   --  Java.Awt.HeadlessException.Except

   function Print (This : access Typ;
                   P1_PrintMode : access Standard.Javax.Swing.JTable.PrintMode.Typ'Class;
                   P2_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P3_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P4_Boolean : Java.Boolean;
                   P5_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class;
                   P6_Boolean : Java.Boolean;
                   P7_PrintService : access Standard.Javax.Print.PrintService.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except and
   --  Java.Awt.HeadlessException.Except

   function GetPrintable (This : access Typ;
                          P1_PrintMode : access Standard.Javax.Swing.JTable.PrintMode.Typ'Class;
                          P2_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                          P3_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class)
                          return access Java.Awt.Print.Printable.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   AUTO_RESIZE_OFF : constant Java.Int;

   --  final
   AUTO_RESIZE_NEXT_COLUMN : constant Java.Int;

   --  final
   AUTO_RESIZE_SUBSEQUENT_COLUMNS : constant Java.Int;

   --  final
   AUTO_RESIZE_LAST_COLUMN : constant Java.Int;

   --  final
   AUTO_RESIZE_ALL_COLUMNS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JTable);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, ConfigureEnclosingScrollPane, "configureEnclosingScrollPane");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, UnconfigureEnclosingScrollPane, "unconfigureEnclosingScrollPane");
   pragma Import (Java, SetTableHeader, "setTableHeader");
   pragma Import (Java, GetTableHeader, "getTableHeader");
   pragma Import (Java, SetRowHeight, "setRowHeight");
   pragma Import (Java, GetRowHeight, "getRowHeight");
   pragma Import (Java, SetRowMargin, "setRowMargin");
   pragma Import (Java, GetRowMargin, "getRowMargin");
   pragma Import (Java, SetIntercellSpacing, "setIntercellSpacing");
   pragma Import (Java, GetIntercellSpacing, "getIntercellSpacing");
   pragma Import (Java, SetGridColor, "setGridColor");
   pragma Import (Java, GetGridColor, "getGridColor");
   pragma Import (Java, SetShowGrid, "setShowGrid");
   pragma Import (Java, SetShowHorizontalLines, "setShowHorizontalLines");
   pragma Import (Java, SetShowVerticalLines, "setShowVerticalLines");
   pragma Import (Java, GetShowHorizontalLines, "getShowHorizontalLines");
   pragma Import (Java, GetShowVerticalLines, "getShowVerticalLines");
   pragma Import (Java, SetAutoResizeMode, "setAutoResizeMode");
   pragma Import (Java, GetAutoResizeMode, "getAutoResizeMode");
   pragma Import (Java, SetAutoCreateColumnsFromModel, "setAutoCreateColumnsFromModel");
   pragma Import (Java, GetAutoCreateColumnsFromModel, "getAutoCreateColumnsFromModel");
   pragma Import (Java, CreateDefaultColumnsFromModel, "createDefaultColumnsFromModel");
   pragma Import (Java, SetDefaultRenderer, "setDefaultRenderer");
   pragma Import (Java, GetDefaultRenderer, "getDefaultRenderer");
   pragma Import (Java, SetDefaultEditor, "setDefaultEditor");
   pragma Import (Java, GetDefaultEditor, "getDefaultEditor");
   pragma Import (Java, SetDragEnabled, "setDragEnabled");
   pragma Import (Java, GetDragEnabled, "getDragEnabled");
   pragma Import (Java, SetDropMode, "setDropMode");
   pragma Import (Java, GetDropMode, "getDropMode");
   pragma Import (Java, GetDropLocation, "getDropLocation");
   pragma Import (Java, SetAutoCreateRowSorter, "setAutoCreateRowSorter");
   pragma Import (Java, GetAutoCreateRowSorter, "getAutoCreateRowSorter");
   pragma Import (Java, SetUpdateSelectionOnSort, "setUpdateSelectionOnSort");
   pragma Import (Java, GetUpdateSelectionOnSort, "getUpdateSelectionOnSort");
   pragma Import (Java, SetRowSorter, "setRowSorter");
   pragma Import (Java, GetRowSorter, "getRowSorter");
   pragma Import (Java, SetSelectionMode, "setSelectionMode");
   pragma Import (Java, SetRowSelectionAllowed, "setRowSelectionAllowed");
   pragma Import (Java, GetRowSelectionAllowed, "getRowSelectionAllowed");
   pragma Import (Java, SetColumnSelectionAllowed, "setColumnSelectionAllowed");
   pragma Import (Java, GetColumnSelectionAllowed, "getColumnSelectionAllowed");
   pragma Import (Java, SetCellSelectionEnabled, "setCellSelectionEnabled");
   pragma Import (Java, GetCellSelectionEnabled, "getCellSelectionEnabled");
   pragma Import (Java, SelectAll, "selectAll");
   pragma Import (Java, ClearSelection, "clearSelection");
   pragma Import (Java, SetRowSelectionInterval, "setRowSelectionInterval");
   pragma Import (Java, SetColumnSelectionInterval, "setColumnSelectionInterval");
   pragma Import (Java, AddRowSelectionInterval, "addRowSelectionInterval");
   pragma Import (Java, AddColumnSelectionInterval, "addColumnSelectionInterval");
   pragma Import (Java, RemoveRowSelectionInterval, "removeRowSelectionInterval");
   pragma Import (Java, RemoveColumnSelectionInterval, "removeColumnSelectionInterval");
   pragma Import (Java, GetSelectedRow, "getSelectedRow");
   pragma Import (Java, GetSelectedColumn, "getSelectedColumn");
   pragma Import (Java, GetSelectedRows, "getSelectedRows");
   pragma Import (Java, GetSelectedColumns, "getSelectedColumns");
   pragma Import (Java, GetSelectedRowCount, "getSelectedRowCount");
   pragma Import (Java, GetSelectedColumnCount, "getSelectedColumnCount");
   pragma Import (Java, IsRowSelected, "isRowSelected");
   pragma Import (Java, IsColumnSelected, "isColumnSelected");
   pragma Import (Java, IsCellSelected, "isCellSelected");
   pragma Import (Java, ChangeSelection, "changeSelection");
   pragma Import (Java, GetSelectionForeground, "getSelectionForeground");
   pragma Import (Java, SetSelectionForeground, "setSelectionForeground");
   pragma Import (Java, GetSelectionBackground, "getSelectionBackground");
   pragma Import (Java, SetSelectionBackground, "setSelectionBackground");
   pragma Import (Java, GetColumn, "getColumn");
   pragma Import (Java, ConvertColumnIndexToModel, "convertColumnIndexToModel");
   pragma Import (Java, ConvertColumnIndexToView, "convertColumnIndexToView");
   pragma Import (Java, ConvertRowIndexToView, "convertRowIndexToView");
   pragma Import (Java, ConvertRowIndexToModel, "convertRowIndexToModel");
   pragma Import (Java, GetRowCount, "getRowCount");
   pragma Import (Java, GetColumnCount, "getColumnCount");
   pragma Import (Java, GetColumnName, "getColumnName");
   pragma Import (Java, GetColumnClass, "getColumnClass");
   pragma Import (Java, GetValueAt, "getValueAt");
   pragma Import (Java, SetValueAt, "setValueAt");
   pragma Import (Java, IsCellEditable, "isCellEditable");
   pragma Import (Java, AddColumn, "addColumn");
   pragma Import (Java, RemoveColumn, "removeColumn");
   pragma Import (Java, MoveColumn, "moveColumn");
   pragma Import (Java, ColumnAtPoint, "columnAtPoint");
   pragma Import (Java, RowAtPoint, "rowAtPoint");
   pragma Import (Java, GetCellRect, "getCellRect");
   pragma Import (Java, DoLayout, "doLayout");
   pragma Import (Java, SizeColumnsToFit, "sizeColumnsToFit");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, SetSurrendersFocusOnKeystroke, "setSurrendersFocusOnKeystroke");
   pragma Import (Java, GetSurrendersFocusOnKeystroke, "getSurrendersFocusOnKeystroke");
   pragma Import (Java, EditCellAt, "editCellAt");
   pragma Import (Java, IsEditing, "isEditing");
   pragma Import (Java, GetEditorComponent, "getEditorComponent");
   pragma Import (Java, GetEditingColumn, "getEditingColumn");
   pragma Import (Java, GetEditingRow, "getEditingRow");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetColumnModel, "setColumnModel");
   pragma Import (Java, GetColumnModel, "getColumnModel");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, SorterChanged, "sorterChanged");
   pragma Import (Java, TableChanged, "tableChanged");
   pragma Import (Java, ColumnAdded, "columnAdded");
   pragma Import (Java, ColumnRemoved, "columnRemoved");
   pragma Import (Java, ColumnMoved, "columnMoved");
   pragma Import (Java, ColumnMarginChanged, "columnMarginChanged");
   pragma Import (Java, ColumnSelectionChanged, "columnSelectionChanged");
   pragma Import (Java, ValueChanged, "valueChanged");
   pragma Import (Java, EditingStopped, "editingStopped");
   pragma Import (Java, EditingCanceled, "editingCanceled");
   pragma Import (Java, SetPreferredScrollableViewportSize, "setPreferredScrollableViewportSize");
   pragma Import (Java, GetPreferredScrollableViewportSize, "getPreferredScrollableViewportSize");
   pragma Import (Java, GetScrollableUnitIncrement, "getScrollableUnitIncrement");
   pragma Import (Java, GetScrollableBlockIncrement, "getScrollableBlockIncrement");
   pragma Import (Java, GetScrollableTracksViewportWidth, "getScrollableTracksViewportWidth");
   pragma Import (Java, GetScrollableTracksViewportHeight, "getScrollableTracksViewportHeight");
   pragma Import (Java, SetFillsViewportHeight, "setFillsViewportHeight");
   pragma Import (Java, GetFillsViewportHeight, "getFillsViewportHeight");
   pragma Import (Java, ProcessKeyBinding, "processKeyBinding");
   pragma Import (Java, CreateDefaultRenderers, "createDefaultRenderers");
   pragma Import (Java, CreateDefaultEditors, "createDefaultEditors");
   pragma Import (Java, InitializeLocalVars, "initializeLocalVars");
   pragma Import (Java, CreateDefaultDataModel, "createDefaultDataModel");
   pragma Import (Java, CreateDefaultColumnModel, "createDefaultColumnModel");
   pragma Import (Java, CreateDefaultSelectionModel, "createDefaultSelectionModel");
   pragma Import (Java, CreateDefaultTableHeader, "createDefaultTableHeader");
   pragma Import (Java, ResizeAndRepaint, "resizeAndRepaint");
   pragma Import (Java, GetCellEditor, "getCellEditor");
   pragma Import (Java, SetCellEditor, "setCellEditor");
   pragma Import (Java, SetEditingColumn, "setEditingColumn");
   pragma Import (Java, SetEditingRow, "setEditingRow");
   pragma Import (Java, GetCellRenderer, "getCellRenderer");
   pragma Import (Java, PrepareRenderer, "prepareRenderer");
   pragma Import (Java, PrepareEditor, "prepareEditor");
   pragma Import (Java, RemoveEditor, "removeEditor");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, Print, "print");
   pragma Import (Java, GetPrintable, "getPrintable");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, AUTO_RESIZE_OFF, "AUTO_RESIZE_OFF");
   pragma Import (Java, AUTO_RESIZE_NEXT_COLUMN, "AUTO_RESIZE_NEXT_COLUMN");
   pragma Import (Java, AUTO_RESIZE_SUBSEQUENT_COLUMNS, "AUTO_RESIZE_SUBSEQUENT_COLUMNS");
   pragma Import (Java, AUTO_RESIZE_LAST_COLUMN, "AUTO_RESIZE_LAST_COLUMN");
   pragma Import (Java, AUTO_RESIZE_ALL_COLUMNS, "AUTO_RESIZE_ALL_COLUMNS");

end Javax.Swing.JTable;
pragma Import (Java, Javax.Swing.JTable, "javax.swing.JTable");
pragma Extensions_Allowed (Off);
