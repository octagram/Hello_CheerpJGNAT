pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Text.Document;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Scrollable;
with Javax.Swing.Text.JTextComponent;

package Javax.Swing.JTextArea is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref)
    is new Javax.Swing.Text.JTextComponent.Typ(MenuContainer_I,
                                               ImageObserver_I,
                                               Serializable_I,
                                               Accessible_I,
                                               Scrollable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JTextArea (This : Ref := null)
                           return Ref;

   function New_JTextArea (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JTextArea (P1_Int : Java.Int;
                           P2_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_JTextArea (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   function New_JTextArea (P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_JTextArea (P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   function CreateDefaultModel (This : access Typ)
                                return access Javax.Swing.Text.Document.Typ'Class;

   procedure SetTabSize (This : access Typ;
                         P1_Int : Java.Int);

   function GetTabSize (This : access Typ)
                        return Java.Int;

   procedure SetLineWrap (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function GetLineWrap (This : access Typ)
                         return Java.Boolean;

   procedure SetWrapStyleWord (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function GetWrapStyleWord (This : access Typ)
                              return Java.Boolean;

   function GetLineOfOffset (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetLineCount (This : access Typ)
                          return Java.Int;

   function GetLineStartOffset (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetLineEndOffset (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure Insert (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int);

   procedure Append (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure ReplaceRange (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int);

   function GetRows (This : access Typ)
                     return Java.Int;

   procedure SetRows (This : access Typ;
                      P1_Int : Java.Int);

   --  protected
   function GetRowHeight (This : access Typ)
                          return Java.Int;

   function GetColumns (This : access Typ)
                        return Java.Int;

   procedure SetColumns (This : access Typ;
                         P1_Int : Java.Int);

   --  protected
   function GetColumnWidth (This : access Typ)
                            return Java.Int;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetScrollableTracksViewportWidth (This : access Typ)
                                              return Java.Boolean;

   function GetPreferredScrollableViewportSize (This : access Typ)
                                                return access Java.Awt.Dimension.Typ'Class;

   function GetScrollableUnitIncrement (This : access Typ;
                                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int)
                                        return Java.Int;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JTextArea);
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, CreateDefaultModel, "createDefaultModel");
   pragma Import (Java, SetTabSize, "setTabSize");
   pragma Import (Java, GetTabSize, "getTabSize");
   pragma Import (Java, SetLineWrap, "setLineWrap");
   pragma Import (Java, GetLineWrap, "getLineWrap");
   pragma Import (Java, SetWrapStyleWord, "setWrapStyleWord");
   pragma Import (Java, GetWrapStyleWord, "getWrapStyleWord");
   pragma Import (Java, GetLineOfOffset, "getLineOfOffset");
   pragma Import (Java, GetLineCount, "getLineCount");
   pragma Import (Java, GetLineStartOffset, "getLineStartOffset");
   pragma Import (Java, GetLineEndOffset, "getLineEndOffset");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Append, "append");
   pragma Import (Java, ReplaceRange, "replaceRange");
   pragma Import (Java, GetRows, "getRows");
   pragma Import (Java, SetRows, "setRows");
   pragma Import (Java, GetRowHeight, "getRowHeight");
   pragma Import (Java, GetColumns, "getColumns");
   pragma Import (Java, SetColumns, "setColumns");
   pragma Import (Java, GetColumnWidth, "getColumnWidth");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetScrollableTracksViewportWidth, "getScrollableTracksViewportWidth");
   pragma Import (Java, GetPreferredScrollableViewportSize, "getPreferredScrollableViewportSize");
   pragma Import (Java, GetScrollableUnitIncrement, "getScrollableUnitIncrement");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JTextArea;
pragma Import (Java, Javax.Swing.JTextArea, "javax.swing.JTextArea");
pragma Extensions_Allowed (Off);
