pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Font;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.BoundedRangeModel;
limited with Javax.Swing.Text.Document;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Scrollable;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.JTextComponent;

package Javax.Swing.JTextField is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.JTextComponent.Typ(MenuContainer_I,
                                               ImageObserver_I,
                                               Serializable_I,
                                               Accessible_I,
                                               Scrollable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JTextField (This : Ref := null)
                            return Ref;

   function New_JTextField (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_JTextField (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_JTextField (P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_JTextField (P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetDocument (This : access Typ;
                          P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class);

   function IsValidateRoot (This : access Typ)
                            return Java.Boolean;

   function GetHorizontalAlignment (This : access Typ)
                                    return Java.Int;

   procedure SetHorizontalAlignment (This : access Typ;
                                     P1_Int : Java.Int);

   --  protected
   function CreateDefaultModel (This : access Typ)
                                return access Javax.Swing.Text.Document.Typ'Class;

   function GetColumns (This : access Typ)
                        return Java.Int;

   procedure SetColumns (This : access Typ;
                         P1_Int : Java.Int);

   --  protected
   function GetColumnWidth (This : access Typ)
                            return Java.Int;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   --  synchronized
   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   --  synchronized
   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireActionPerformed (This : access Typ);

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetAction (This : access Typ;
                        P1_Action : access Standard.Javax.Swing.Action.Typ'Class);

   function GetAction (This : access Typ)
                       return access Javax.Swing.Action.Typ'Class;

   --  protected
   procedure ConfigurePropertiesFromAction (This : access Typ;
                                            P1_Action : access Standard.Javax.Swing.Action.Typ'Class);

   --  protected
   procedure ActionPropertyChanged (This : access Typ;
                                    P1_Action : access Standard.Javax.Swing.Action.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function CreateActionPropertyChangeListener (This : access Typ;
                                                P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                                                return access Java.Beans.PropertyChangeListener.Typ'Class;

   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   procedure PostActionEvent (This : access Typ);

   function GetHorizontalVisibility (This : access Typ)
                                     return access Javax.Swing.BoundedRangeModel.Typ'Class;

   function GetScrollOffset (This : access Typ)
                             return Java.Int;

   procedure SetScrollOffset (This : access Typ;
                              P1_Int : Java.Int);

   procedure ScrollRectToVisible (This : access Typ;
                                  P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NotifyAction : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JTextField);
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetDocument, "setDocument");
   pragma Import (Java, IsValidateRoot, "isValidateRoot");
   pragma Import (Java, GetHorizontalAlignment, "getHorizontalAlignment");
   pragma Import (Java, SetHorizontalAlignment, "setHorizontalAlignment");
   pragma Import (Java, CreateDefaultModel, "createDefaultModel");
   pragma Import (Java, GetColumns, "getColumns");
   pragma Import (Java, SetColumns, "setColumns");
   pragma Import (Java, GetColumnWidth, "getColumnWidth");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, FireActionPerformed, "fireActionPerformed");
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, SetAction, "setAction");
   pragma Import (Java, GetAction, "getAction");
   pragma Import (Java, ConfigurePropertiesFromAction, "configurePropertiesFromAction");
   pragma Import (Java, ActionPropertyChanged, "actionPropertyChanged");
   pragma Import (Java, CreateActionPropertyChangeListener, "createActionPropertyChangeListener");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, PostActionEvent, "postActionEvent");
   pragma Import (Java, GetHorizontalVisibility, "getHorizontalVisibility");
   pragma Import (Java, GetScrollOffset, "getScrollOffset");
   pragma Import (Java, SetScrollOffset, "setScrollOffset");
   pragma Import (Java, ScrollRectToVisible, "scrollRectToVisible");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, NotifyAction, "notifyAction");

end Javax.Swing.JTextField;
pragma Import (Java, Javax.Swing.JTextField, "javax.swing.JTextField");
pragma Extensions_Allowed (Off);
