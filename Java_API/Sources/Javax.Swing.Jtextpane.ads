pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.EditorKit;
limited with Javax.Swing.Text.MutableAttributeSet;
limited with Javax.Swing.Text.Style;
limited with Javax.Swing.Text.StyledDocument;
limited with Javax.Swing.Text.StyledEditorKit;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JEditorPane;
with Javax.Swing.Scrollable;

package Javax.Swing.JTextPane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref)
    is new Javax.Swing.JEditorPane.Typ(MenuContainer_I,
                                       ImageObserver_I,
                                       Serializable_I,
                                       Accessible_I,
                                       Scrollable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JTextPane (This : Ref := null)
                           return Ref;

   function New_JTextPane (P1_StyledDocument : access Standard.Javax.Swing.Text.StyledDocument.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetDocument (This : access Typ;
                          P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class);

   procedure SetStyledDocument (This : access Typ;
                                P1_StyledDocument : access Standard.Javax.Swing.Text.StyledDocument.Typ'Class);

   function GetStyledDocument (This : access Typ)
                               return access Javax.Swing.Text.StyledDocument.Typ'Class;

   procedure ReplaceSelection (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure InsertComponent (This : access Typ;
                              P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure InsertIcon (This : access Typ;
                         P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function AddStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Style : access Standard.Javax.Swing.Text.Style.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class;

   procedure RemoveStyle (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class;

   procedure SetLogicalStyle (This : access Typ;
                              P1_Style : access Standard.Javax.Swing.Text.Style.Typ'Class);

   function GetLogicalStyle (This : access Typ)
                             return access Javax.Swing.Text.Style.Typ'Class;

   function GetCharacterAttributes (This : access Typ)
                                    return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure SetCharacterAttributes (This : access Typ;
                                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P2_Boolean : Java.Boolean);

   function GetParagraphAttributes (This : access Typ)
                                    return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure SetParagraphAttributes (This : access Typ;
                                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P2_Boolean : Java.Boolean);

   function GetInputAttributes (This : access Typ)
                                return access Javax.Swing.Text.MutableAttributeSet.Typ'Class;

   --  final  protected
   function GetStyledEditorKit (This : access Typ)
                                return access Javax.Swing.Text.StyledEditorKit.Typ'Class;

   --  protected
   function CreateDefaultEditorKit (This : access Typ)
                                    return access Javax.Swing.Text.EditorKit.Typ'Class;

   --  final
   procedure SetEditorKit (This : access Typ;
                           P1_EditorKit : access Standard.Javax.Swing.Text.EditorKit.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JTextPane);
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetDocument, "setDocument");
   pragma Import (Java, SetStyledDocument, "setStyledDocument");
   pragma Import (Java, GetStyledDocument, "getStyledDocument");
   pragma Import (Java, ReplaceSelection, "replaceSelection");
   pragma Import (Java, InsertComponent, "insertComponent");
   pragma Import (Java, InsertIcon, "insertIcon");
   pragma Import (Java, AddStyle, "addStyle");
   pragma Import (Java, RemoveStyle, "removeStyle");
   pragma Import (Java, GetStyle, "getStyle");
   pragma Import (Java, SetLogicalStyle, "setLogicalStyle");
   pragma Import (Java, GetLogicalStyle, "getLogicalStyle");
   pragma Import (Java, GetCharacterAttributes, "getCharacterAttributes");
   pragma Import (Java, SetCharacterAttributes, "setCharacterAttributes");
   pragma Import (Java, GetParagraphAttributes, "getParagraphAttributes");
   pragma Import (Java, SetParagraphAttributes, "setParagraphAttributes");
   pragma Import (Java, GetInputAttributes, "getInputAttributes");
   pragma Import (Java, GetStyledEditorKit, "getStyledEditorKit");
   pragma Import (Java, CreateDefaultEditorKit, "createDefaultEditorKit");
   pragma Import (Java, SetEditorKit, "setEditorKit");
   pragma Import (Java, ParamString, "paramString");

end Javax.Swing.JTextPane;
pragma Import (Java, Javax.Swing.JTextPane, "javax.swing.JTextPane");
pragma Extensions_Allowed (Off);
