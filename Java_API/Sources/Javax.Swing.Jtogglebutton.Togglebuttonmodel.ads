pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.ButtonModel;
with Javax.Swing.DefaultButtonModel;

package Javax.Swing.JToggleButton.ToggleButtonModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ButtonModel_I : Javax.Swing.ButtonModel.Ref)
    is new Javax.Swing.DefaultButtonModel.Typ(Serializable_I,
                                              ButtonModel_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ToggleButtonModel (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   procedure SetSelected (This : access Typ;
                          P1_Boolean : Java.Boolean);

   procedure SetPressed (This : access Typ;
                         P1_Boolean : Java.Boolean);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ToggleButtonModel);
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, SetSelected, "setSelected");
   pragma Import (Java, SetPressed, "setPressed");

end Javax.Swing.JToggleButton.ToggleButtonModel;
pragma Import (Java, Javax.Swing.JToggleButton.ToggleButtonModel, "javax.swing.JToggleButton$ToggleButtonModel");
pragma Extensions_Allowed (Off);
