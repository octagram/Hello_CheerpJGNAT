pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JSeparator;
with Javax.Swing.SwingConstants;

package Javax.Swing.JToolBar.Separator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JSeparator.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I,
                                      Accessible_I,
                                      SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Separator (This : Ref := null)
                           return Ref;

   function New_Separator (P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetSeparatorSize (This : access Typ;
                               P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetSeparatorSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Separator);
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetSeparatorSize, "setSeparatorSize");
   pragma Import (Java, GetSeparatorSize, "getSeparatorSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");

end Javax.Swing.JToolBar.Separator;
pragma Import (Java, Javax.Swing.JToolBar.Separator, "javax.swing.JToolBar$Separator");
pragma Extensions_Allowed (Off);
