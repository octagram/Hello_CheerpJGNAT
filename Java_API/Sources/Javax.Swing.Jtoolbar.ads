pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.LayoutManager;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.JButton;
limited with Javax.Swing.Plaf.ToolBarUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.SwingConstants;

package Javax.Swing.JToolBar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JToolBar (This : Ref := null)
                          return Ref;

   function New_JToolBar (P1_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   function New_JToolBar (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   function New_JToolBar (P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ToolBarUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_ToolBarUI : access Standard.Javax.Swing.Plaf.ToolBarUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetComponentIndex (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return Java.Int;

   function GetComponentAtIndex (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Java.Awt.Component.Typ'Class;

   procedure SetMargin (This : access Typ;
                        P1_Insets : access Standard.Java.Awt.Insets.Typ'Class);

   function GetMargin (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   function IsBorderPainted (This : access Typ)
                             return Java.Boolean;

   procedure SetBorderPainted (This : access Typ;
                               P1_Boolean : Java.Boolean);

   --  protected
   procedure PaintBorder (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function IsFloatable (This : access Typ)
                         return Java.Boolean;

   procedure SetFloatable (This : access Typ;
                           P1_Boolean : Java.Boolean);

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   procedure SetRollover (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function IsRollover (This : access Typ)
                        return Java.Boolean;

   procedure AddSeparator (This : access Typ);

   procedure AddSeparator (This : access Typ;
                           P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function Add (This : access Typ;
                 P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                 return access Javax.Swing.JButton.Typ'Class;

   --  protected
   function CreateActionComponent (This : access Typ;
                                   P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                                   return access Javax.Swing.JButton.Typ'Class;

   --  protected
   function CreateActionChangeListener (This : access Typ;
                                        P1_JButton : access Standard.Javax.Swing.JButton.Typ'Class)
                                        return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JToolBar);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetComponentIndex, "getComponentIndex");
   pragma Import (Java, GetComponentAtIndex, "getComponentAtIndex");
   pragma Import (Java, SetMargin, "setMargin");
   pragma Import (Java, GetMargin, "getMargin");
   pragma Import (Java, IsBorderPainted, "isBorderPainted");
   pragma Import (Java, SetBorderPainted, "setBorderPainted");
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, IsFloatable, "isFloatable");
   pragma Import (Java, SetFloatable, "setFloatable");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, SetRollover, "setRollover");
   pragma Import (Java, IsRollover, "isRollover");
   pragma Import (Java, AddSeparator, "addSeparator");
   pragma Import (Java, Add, "add");
   pragma Import (Java, CreateActionComponent, "createActionComponent");
   pragma Import (Java, CreateActionChangeListener, "createActionChangeListener");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JToolBar;
pragma Import (Java, Javax.Swing.JToolBar, "javax.swing.JToolBar");
pragma Extensions_Allowed (Off);
