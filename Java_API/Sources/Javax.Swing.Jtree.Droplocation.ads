pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Tree.TreePath;
with Java.Lang.Object;
with Javax.Swing.TransferHandler.DropLocation;

package Javax.Swing.JTree.DropLocation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.TransferHandler.DropLocation.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetChildIndex (This : access Typ)
                           return Java.Int;

   function GetPath (This : access Typ)
                     return access Javax.Swing.Tree.TreePath.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetChildIndex, "getChildIndex");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.JTree.DropLocation;
pragma Import (Java, Javax.Swing.JTree.DropLocation, "javax.swing.JTree$DropLocation");
pragma Extensions_Allowed (Off);
