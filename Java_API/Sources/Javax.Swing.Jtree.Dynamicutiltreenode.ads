pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
limited with Javax.Swing.Tree.TreeNode;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Tree.DefaultMutableTreeNode;
with Javax.Swing.Tree.MutableTreeNode;

package Javax.Swing.JTree.DynamicUtilTreeNode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            MutableTreeNode_I : Javax.Swing.Tree.MutableTreeNode.Ref)
    is new Javax.Swing.Tree.DefaultMutableTreeNode.Typ(Serializable_I,
                                                       Cloneable_I,
                                                       MutableTreeNode_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      HasChildren : Java.Boolean;
      pragma Import (Java, HasChildren, "hasChildren");

      --  protected
      ChildValue : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, ChildValue, "childValue");

      --  protected
      LoadedChildren : Java.Boolean;
      pragma Import (Java, LoadedChildren, "loadedChildren");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure CreateChildren (P1_DefaultMutableTreeNode : access Standard.Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DynamicUtilTreeNode (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function IsLeaf (This : access Typ)
                    return Java.Boolean;

   function GetChildCount (This : access Typ)
                           return Java.Int;

   --  protected
   procedure LoadChildren (This : access Typ);

   function GetChildAt (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function Children (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateChildren, "createChildren");
   pragma Java_Constructor (New_DynamicUtilTreeNode);
   pragma Import (Java, IsLeaf, "isLeaf");
   pragma Import (Java, GetChildCount, "getChildCount");
   pragma Import (Java, LoadChildren, "loadChildren");
   pragma Import (Java, GetChildAt, "getChildAt");
   pragma Import (Java, Children, "children");

end Javax.Swing.JTree.DynamicUtilTreeNode;
pragma Import (Java, Javax.Swing.JTree.DynamicUtilTreeNode, "javax.swing.JTree$DynamicUtilTreeNode");
pragma Extensions_Allowed (Off);
