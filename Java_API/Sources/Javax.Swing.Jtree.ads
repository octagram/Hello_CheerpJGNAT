pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Hashtable;
limited with Java.Util.Vector;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.DropMode;
limited with Javax.Swing.Event.TreeExpansionListener;
limited with Javax.Swing.Event.TreeModelListener;
limited with Javax.Swing.Event.TreeSelectionEvent;
limited with Javax.Swing.Event.TreeSelectionListener;
limited with Javax.Swing.Event.TreeWillExpandListener;
limited with Javax.Swing.JTree.DropLocation;
limited with Javax.Swing.Plaf.TreeUI;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Tree.TreeCellEditor;
limited with Javax.Swing.Tree.TreeCellRenderer;
limited with Javax.Swing.Tree.TreeModel;
limited with Javax.Swing.Tree.TreeNode;
limited with Javax.Swing.Tree.TreePath;
limited with Javax.Swing.Tree.TreeSelectionModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.Scrollable;

package Javax.Swing.JTree is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      TreeModel : access Javax.Swing.Tree.TreeModel.Typ'Class;
      pragma Import (Java, TreeModel, "treeModel");

      --  protected
      SelectionModel : access Javax.Swing.Tree.TreeSelectionModel.Typ'Class;
      pragma Import (Java, SelectionModel, "selectionModel");

      --  protected
      RootVisible : Java.Boolean;
      pragma Import (Java, RootVisible, "rootVisible");

      --  protected
      CellRenderer : access Javax.Swing.Tree.TreeCellRenderer.Typ'Class;
      pragma Import (Java, CellRenderer, "cellRenderer");

      --  protected
      RowHeight : Java.Int;
      pragma Import (Java, RowHeight, "rowHeight");

      --  protected
      ShowsRootHandles : Java.Boolean;
      pragma Import (Java, ShowsRootHandles, "showsRootHandles");

      --  protected
      CellEditor : access Javax.Swing.Tree.TreeCellEditor.Typ'Class;
      pragma Import (Java, CellEditor, "cellEditor");

      --  protected
      Editable : Java.Boolean;
      pragma Import (Java, Editable, "editable");

      --  protected
      LargeModel : Java.Boolean;
      pragma Import (Java, LargeModel, "largeModel");

      --  protected
      VisibleRowCount : Java.Int;
      pragma Import (Java, VisibleRowCount, "visibleRowCount");

      --  protected
      InvokesStopCellEditing : Java.Boolean;
      pragma Import (Java, InvokesStopCellEditing, "invokesStopCellEditing");

      --  protected
      ScrollsOnExpand : Java.Boolean;
      pragma Import (Java, ScrollsOnExpand, "scrollsOnExpand");

      --  protected
      ToggleClickCount : Java.Int;
      pragma Import (Java, ToggleClickCount, "toggleClickCount");

      --  protected
      TreeModelListener : access Javax.Swing.Event.TreeModelListener.Typ'Class;
      pragma Import (Java, TreeModelListener, "treeModelListener");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetDefaultTreeModel return access Javax.Swing.Tree.TreeModel.Typ'Class;

   --  protected
   function CreateTreeModel (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Javax.Swing.Tree.TreeModel.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JTree (This : Ref := null)
                       return Ref;

   function New_JTree (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                       This : Ref := null)
                       return Ref;

   function New_JTree (P1_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_JTree (P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_JTree (P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_JTree (P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class;
                       P2_Boolean : Java.Boolean; 
                       This : Ref := null)
                       return Ref;

   function New_JTree (P1_TreeModel : access Standard.Javax.Swing.Tree.TreeModel.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.TreeUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_TreeUI : access Standard.Javax.Swing.Plaf.TreeUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetCellRenderer (This : access Typ)
                             return access Javax.Swing.Tree.TreeCellRenderer.Typ'Class;

   procedure SetCellRenderer (This : access Typ;
                              P1_TreeCellRenderer : access Standard.Javax.Swing.Tree.TreeCellRenderer.Typ'Class);

   procedure SetEditable (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function IsEditable (This : access Typ)
                        return Java.Boolean;

   procedure SetCellEditor (This : access Typ;
                            P1_TreeCellEditor : access Standard.Javax.Swing.Tree.TreeCellEditor.Typ'Class);

   function GetCellEditor (This : access Typ)
                           return access Javax.Swing.Tree.TreeCellEditor.Typ'Class;

   function GetModel (This : access Typ)
                      return access Javax.Swing.Tree.TreeModel.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_TreeModel : access Standard.Javax.Swing.Tree.TreeModel.Typ'Class);

   function IsRootVisible (This : access Typ)
                           return Java.Boolean;

   procedure SetRootVisible (This : access Typ;
                             P1_Boolean : Java.Boolean);

   procedure SetShowsRootHandles (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function GetShowsRootHandles (This : access Typ)
                                 return Java.Boolean;

   procedure SetRowHeight (This : access Typ;
                           P1_Int : Java.Int);

   function GetRowHeight (This : access Typ)
                          return Java.Int;

   function IsFixedRowHeight (This : access Typ)
                              return Java.Boolean;

   procedure SetLargeModel (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function IsLargeModel (This : access Typ)
                          return Java.Boolean;

   procedure SetInvokesStopCellEditing (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function GetInvokesStopCellEditing (This : access Typ)
                                       return Java.Boolean;

   procedure SetScrollsOnExpand (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   function GetScrollsOnExpand (This : access Typ)
                                return Java.Boolean;

   procedure SetToggleClickCount (This : access Typ;
                                  P1_Int : Java.Int);

   function GetToggleClickCount (This : access Typ)
                                 return Java.Int;

   procedure SetExpandsSelectedPaths (This : access Typ;
                                      P1_Boolean : Java.Boolean);

   function GetExpandsSelectedPaths (This : access Typ)
                                     return Java.Boolean;

   procedure SetDragEnabled (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetDragEnabled (This : access Typ)
                            return Java.Boolean;

   --  final
   procedure SetDropMode (This : access Typ;
                          P1_DropMode : access Standard.Javax.Swing.DropMode.Typ'Class);

   --  final
   function GetDropMode (This : access Typ)
                         return access Javax.Swing.DropMode.Typ'Class;

   --  final
   function GetDropLocation (This : access Typ)
                             return access Javax.Swing.JTree.DropLocation.Typ'Class;

   function IsPathEditable (This : access Typ;
                            P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                            return Java.Boolean;

   function GetToolTipText (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function ConvertValueToText (This : access Typ;
                                P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean;
                                P4_Boolean : Java.Boolean;
                                P5_Int : Java.Int;
                                P6_Boolean : Java.Boolean)
                                return access Java.Lang.String.Typ'Class;

   function GetRowCount (This : access Typ)
                         return Java.Int;

   procedure SetSelectionPath (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure SetSelectionPaths (This : access Typ;
                                P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj);

   procedure SetLeadSelectionPath (This : access Typ;
                                   P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure SetAnchorSelectionPath (This : access Typ;
                                     P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure SetSelectionRow (This : access Typ;
                              P1_Int : Java.Int);

   procedure SetSelectionRows (This : access Typ;
                               P1_Int_Arr : Java.Int_Arr);

   procedure AddSelectionPath (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure AddSelectionPaths (This : access Typ;
                                P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj);

   procedure AddSelectionRow (This : access Typ;
                              P1_Int : Java.Int);

   procedure AddSelectionRows (This : access Typ;
                               P1_Int_Arr : Java.Int_Arr);

   function GetLastSelectedPathComponent (This : access Typ)
                                          return access Java.Lang.Object.Typ'Class;

   function GetLeadSelectionPath (This : access Typ)
                                  return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetAnchorSelectionPath (This : access Typ)
                                    return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetSelectionPath (This : access Typ)
                              return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetSelectionPaths (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetSelectionRows (This : access Typ)
                              return Java.Int_Arr;

   function GetSelectionCount (This : access Typ)
                               return Java.Int;

   function GetMinSelectionRow (This : access Typ)
                                return Java.Int;

   function GetMaxSelectionRow (This : access Typ)
                                return Java.Int;

   function GetLeadSelectionRow (This : access Typ)
                                 return Java.Int;

   function IsPathSelected (This : access Typ;
                            P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                            return Java.Boolean;

   function IsRowSelected (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean;

   function GetExpandedDescendants (This : access Typ;
                                    P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                                    return access Java.Util.Enumeration.Typ'Class;

   function HasBeenExpanded (This : access Typ;
                             P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                             return Java.Boolean;

   function IsExpanded (This : access Typ;
                        P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                        return Java.Boolean;

   function IsExpanded (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   function IsCollapsed (This : access Typ;
                         P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                         return Java.Boolean;

   function IsCollapsed (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;

   procedure MakeVisible (This : access Typ;
                          P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   function IsVisible (This : access Typ;
                       P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                       return Java.Boolean;

   function GetPathBounds (This : access Typ;
                           P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return access Java.Awt.Rectangle.Typ'Class;

   function GetRowBounds (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Awt.Rectangle.Typ'Class;

   procedure ScrollPathToVisible (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure ScrollRowToVisible (This : access Typ;
                                 P1_Int : Java.Int);

   function GetPathForRow (This : access Typ;
                           P1_Int : Java.Int)
                           return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetRowForPath (This : access Typ;
                           P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return Java.Int;

   procedure ExpandPath (This : access Typ;
                         P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure ExpandRow (This : access Typ;
                        P1_Int : Java.Int);

   procedure CollapsePath (This : access Typ;
                           P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure CollapseRow (This : access Typ;
                          P1_Int : Java.Int);

   function GetPathForLocation (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetRowForLocation (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return Java.Int;

   function GetClosestPathForLocation (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_Int : Java.Int)
                                       return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetClosestRowForLocation (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int)
                                      return Java.Int;

   function IsEditing (This : access Typ)
                       return Java.Boolean;

   function StopEditing (This : access Typ)
                         return Java.Boolean;

   procedure CancelEditing (This : access Typ);

   procedure StartEditingAtPath (This : access Typ;
                                 P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   function GetEditingPath (This : access Typ)
                            return access Javax.Swing.Tree.TreePath.Typ'Class;

   procedure SetSelectionModel (This : access Typ;
                                P1_TreeSelectionModel : access Standard.Javax.Swing.Tree.TreeSelectionModel.Typ'Class);

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.Tree.TreeSelectionModel.Typ'Class;

   --  protected
   function GetPathBetweenRows (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return Standard.Java.Lang.Object.Ref;

   procedure SetSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure AddSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure RemoveSelectionInterval (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int);

   procedure RemoveSelectionPath (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure RemoveSelectionPaths (This : access Typ;
                                   P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj);

   procedure RemoveSelectionRow (This : access Typ;
                                 P1_Int : Java.Int);

   procedure RemoveSelectionRows (This : access Typ;
                                  P1_Int_Arr : Java.Int_Arr);

   procedure ClearSelection (This : access Typ);

   function IsSelectionEmpty (This : access Typ)
                              return Java.Boolean;

   procedure AddTreeExpansionListener (This : access Typ;
                                       P1_TreeExpansionListener : access Standard.Javax.Swing.Event.TreeExpansionListener.Typ'Class);

   procedure RemoveTreeExpansionListener (This : access Typ;
                                          P1_TreeExpansionListener : access Standard.Javax.Swing.Event.TreeExpansionListener.Typ'Class);

   function GetTreeExpansionListeners (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   procedure AddTreeWillExpandListener (This : access Typ;
                                        P1_TreeWillExpandListener : access Standard.Javax.Swing.Event.TreeWillExpandListener.Typ'Class);

   procedure RemoveTreeWillExpandListener (This : access Typ;
                                           P1_TreeWillExpandListener : access Standard.Javax.Swing.Event.TreeWillExpandListener.Typ'Class);

   function GetTreeWillExpandListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   procedure FireTreeExpanded (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure FireTreeCollapsed (This : access Typ;
                                P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure FireTreeWillExpand (This : access Typ;
                                 P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);
   --  can raise Javax.Swing.Tree.ExpandVetoException.Except

   procedure FireTreeWillCollapse (This : access Typ;
                                   P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);
   --  can raise Javax.Swing.Tree.ExpandVetoException.Except

   procedure AddTreeSelectionListener (This : access Typ;
                                       P1_TreeSelectionListener : access Standard.Javax.Swing.Event.TreeSelectionListener.Typ'Class);

   procedure RemoveTreeSelectionListener (This : access Typ;
                                          P1_TreeSelectionListener : access Standard.Javax.Swing.Event.TreeSelectionListener.Typ'Class);

   function GetTreeSelectionListeners (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireValueChanged (This : access Typ;
                               P1_TreeSelectionEvent : access Standard.Javax.Swing.Event.TreeSelectionEvent.Typ'Class);

   procedure TreeDidChange (This : access Typ);

   procedure SetVisibleRowCount (This : access Typ;
                                 P1_Int : Java.Int);

   function GetVisibleRowCount (This : access Typ)
                                return Java.Int;

   function GetNextMatch (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                          return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetPreferredScrollableViewportSize (This : access Typ)
                                                return access Java.Awt.Dimension.Typ'Class;

   function GetScrollableUnitIncrement (This : access Typ;
                                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int)
                                        return Java.Int;

   function GetScrollableBlockIncrement (This : access Typ;
                                         P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int)
                                         return Java.Int;

   function GetScrollableTracksViewportWidth (This : access Typ)
                                              return Java.Boolean;

   function GetScrollableTracksViewportHeight (This : access Typ)
                                               return Java.Boolean;

   --  protected
   procedure SetExpandedState (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                               P2_Boolean : Java.Boolean);

   --  protected
   function GetDescendantToggledPaths (This : access Typ;
                                       P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                                       return access Java.Util.Enumeration.Typ'Class;

   --  protected
   procedure RemoveDescendantToggledPaths (This : access Typ;
                                           P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class);

   --  protected
   procedure ClearToggledPaths (This : access Typ);

   --  protected
   function CreateTreeModelListener (This : access Typ)
                                     return access Javax.Swing.Event.TreeModelListener.Typ'Class;

   --  protected
   function RemoveDescendantSelectedPaths (This : access Typ;
                                           P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                           P2_Boolean : Java.Boolean)
                                           return Java.Boolean;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CELL_RENDERER_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   TREE_MODEL_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ROOT_VISIBLE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SHOWS_ROOT_HANDLES_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ROW_HEIGHT_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CELL_EDITOR_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   EDITABLE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   LARGE_MODEL_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SELECTION_MODEL_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   VISIBLE_ROW_COUNT_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   INVOKES_STOP_CELL_EDITING_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SCROLLS_ON_EXPAND_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   TOGGLE_CLICK_COUNT_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   LEAD_SELECTION_PATH_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ANCHOR_SELECTION_PATH_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   EXPANDS_SELECTED_PATHS_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDefaultTreeModel, "getDefaultTreeModel");
   pragma Import (Java, CreateTreeModel, "createTreeModel");
   pragma Java_Constructor (New_JTree);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, GetCellRenderer, "getCellRenderer");
   pragma Import (Java, SetCellRenderer, "setCellRenderer");
   pragma Import (Java, SetEditable, "setEditable");
   pragma Import (Java, IsEditable, "isEditable");
   pragma Import (Java, SetCellEditor, "setCellEditor");
   pragma Import (Java, GetCellEditor, "getCellEditor");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, IsRootVisible, "isRootVisible");
   pragma Import (Java, SetRootVisible, "setRootVisible");
   pragma Import (Java, SetShowsRootHandles, "setShowsRootHandles");
   pragma Import (Java, GetShowsRootHandles, "getShowsRootHandles");
   pragma Import (Java, SetRowHeight, "setRowHeight");
   pragma Import (Java, GetRowHeight, "getRowHeight");
   pragma Import (Java, IsFixedRowHeight, "isFixedRowHeight");
   pragma Import (Java, SetLargeModel, "setLargeModel");
   pragma Import (Java, IsLargeModel, "isLargeModel");
   pragma Import (Java, SetInvokesStopCellEditing, "setInvokesStopCellEditing");
   pragma Import (Java, GetInvokesStopCellEditing, "getInvokesStopCellEditing");
   pragma Import (Java, SetScrollsOnExpand, "setScrollsOnExpand");
   pragma Import (Java, GetScrollsOnExpand, "getScrollsOnExpand");
   pragma Import (Java, SetToggleClickCount, "setToggleClickCount");
   pragma Import (Java, GetToggleClickCount, "getToggleClickCount");
   pragma Import (Java, SetExpandsSelectedPaths, "setExpandsSelectedPaths");
   pragma Import (Java, GetExpandsSelectedPaths, "getExpandsSelectedPaths");
   pragma Import (Java, SetDragEnabled, "setDragEnabled");
   pragma Import (Java, GetDragEnabled, "getDragEnabled");
   pragma Import (Java, SetDropMode, "setDropMode");
   pragma Import (Java, GetDropMode, "getDropMode");
   pragma Import (Java, GetDropLocation, "getDropLocation");
   pragma Import (Java, IsPathEditable, "isPathEditable");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, ConvertValueToText, "convertValueToText");
   pragma Import (Java, GetRowCount, "getRowCount");
   pragma Import (Java, SetSelectionPath, "setSelectionPath");
   pragma Import (Java, SetSelectionPaths, "setSelectionPaths");
   pragma Import (Java, SetLeadSelectionPath, "setLeadSelectionPath");
   pragma Import (Java, SetAnchorSelectionPath, "setAnchorSelectionPath");
   pragma Import (Java, SetSelectionRow, "setSelectionRow");
   pragma Import (Java, SetSelectionRows, "setSelectionRows");
   pragma Import (Java, AddSelectionPath, "addSelectionPath");
   pragma Import (Java, AddSelectionPaths, "addSelectionPaths");
   pragma Import (Java, AddSelectionRow, "addSelectionRow");
   pragma Import (Java, AddSelectionRows, "addSelectionRows");
   pragma Import (Java, GetLastSelectedPathComponent, "getLastSelectedPathComponent");
   pragma Import (Java, GetLeadSelectionPath, "getLeadSelectionPath");
   pragma Import (Java, GetAnchorSelectionPath, "getAnchorSelectionPath");
   pragma Import (Java, GetSelectionPath, "getSelectionPath");
   pragma Import (Java, GetSelectionPaths, "getSelectionPaths");
   pragma Import (Java, GetSelectionRows, "getSelectionRows");
   pragma Import (Java, GetSelectionCount, "getSelectionCount");
   pragma Import (Java, GetMinSelectionRow, "getMinSelectionRow");
   pragma Import (Java, GetMaxSelectionRow, "getMaxSelectionRow");
   pragma Import (Java, GetLeadSelectionRow, "getLeadSelectionRow");
   pragma Import (Java, IsPathSelected, "isPathSelected");
   pragma Import (Java, IsRowSelected, "isRowSelected");
   pragma Import (Java, GetExpandedDescendants, "getExpandedDescendants");
   pragma Import (Java, HasBeenExpanded, "hasBeenExpanded");
   pragma Import (Java, IsExpanded, "isExpanded");
   pragma Import (Java, IsCollapsed, "isCollapsed");
   pragma Import (Java, MakeVisible, "makeVisible");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, GetPathBounds, "getPathBounds");
   pragma Import (Java, GetRowBounds, "getRowBounds");
   pragma Import (Java, ScrollPathToVisible, "scrollPathToVisible");
   pragma Import (Java, ScrollRowToVisible, "scrollRowToVisible");
   pragma Import (Java, GetPathForRow, "getPathForRow");
   pragma Import (Java, GetRowForPath, "getRowForPath");
   pragma Import (Java, ExpandPath, "expandPath");
   pragma Import (Java, ExpandRow, "expandRow");
   pragma Import (Java, CollapsePath, "collapsePath");
   pragma Import (Java, CollapseRow, "collapseRow");
   pragma Import (Java, GetPathForLocation, "getPathForLocation");
   pragma Import (Java, GetRowForLocation, "getRowForLocation");
   pragma Import (Java, GetClosestPathForLocation, "getClosestPathForLocation");
   pragma Import (Java, GetClosestRowForLocation, "getClosestRowForLocation");
   pragma Import (Java, IsEditing, "isEditing");
   pragma Import (Java, StopEditing, "stopEditing");
   pragma Import (Java, CancelEditing, "cancelEditing");
   pragma Import (Java, StartEditingAtPath, "startEditingAtPath");
   pragma Import (Java, GetEditingPath, "getEditingPath");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, GetPathBetweenRows, "getPathBetweenRows");
   pragma Import (Java, SetSelectionInterval, "setSelectionInterval");
   pragma Import (Java, AddSelectionInterval, "addSelectionInterval");
   pragma Import (Java, RemoveSelectionInterval, "removeSelectionInterval");
   pragma Import (Java, RemoveSelectionPath, "removeSelectionPath");
   pragma Import (Java, RemoveSelectionPaths, "removeSelectionPaths");
   pragma Import (Java, RemoveSelectionRow, "removeSelectionRow");
   pragma Import (Java, RemoveSelectionRows, "removeSelectionRows");
   pragma Import (Java, ClearSelection, "clearSelection");
   pragma Import (Java, IsSelectionEmpty, "isSelectionEmpty");
   pragma Import (Java, AddTreeExpansionListener, "addTreeExpansionListener");
   pragma Import (Java, RemoveTreeExpansionListener, "removeTreeExpansionListener");
   pragma Import (Java, GetTreeExpansionListeners, "getTreeExpansionListeners");
   pragma Import (Java, AddTreeWillExpandListener, "addTreeWillExpandListener");
   pragma Import (Java, RemoveTreeWillExpandListener, "removeTreeWillExpandListener");
   pragma Import (Java, GetTreeWillExpandListeners, "getTreeWillExpandListeners");
   pragma Import (Java, FireTreeExpanded, "fireTreeExpanded");
   pragma Import (Java, FireTreeCollapsed, "fireTreeCollapsed");
   pragma Import (Java, FireTreeWillExpand, "fireTreeWillExpand");
   pragma Import (Java, FireTreeWillCollapse, "fireTreeWillCollapse");
   pragma Import (Java, AddTreeSelectionListener, "addTreeSelectionListener");
   pragma Import (Java, RemoveTreeSelectionListener, "removeTreeSelectionListener");
   pragma Import (Java, GetTreeSelectionListeners, "getTreeSelectionListeners");
   pragma Import (Java, FireValueChanged, "fireValueChanged");
   pragma Import (Java, TreeDidChange, "treeDidChange");
   pragma Import (Java, SetVisibleRowCount, "setVisibleRowCount");
   pragma Import (Java, GetVisibleRowCount, "getVisibleRowCount");
   pragma Import (Java, GetNextMatch, "getNextMatch");
   pragma Import (Java, GetPreferredScrollableViewportSize, "getPreferredScrollableViewportSize");
   pragma Import (Java, GetScrollableUnitIncrement, "getScrollableUnitIncrement");
   pragma Import (Java, GetScrollableBlockIncrement, "getScrollableBlockIncrement");
   pragma Import (Java, GetScrollableTracksViewportWidth, "getScrollableTracksViewportWidth");
   pragma Import (Java, GetScrollableTracksViewportHeight, "getScrollableTracksViewportHeight");
   pragma Import (Java, SetExpandedState, "setExpandedState");
   pragma Import (Java, GetDescendantToggledPaths, "getDescendantToggledPaths");
   pragma Import (Java, RemoveDescendantToggledPaths, "removeDescendantToggledPaths");
   pragma Import (Java, ClearToggledPaths, "clearToggledPaths");
   pragma Import (Java, CreateTreeModelListener, "createTreeModelListener");
   pragma Import (Java, RemoveDescendantSelectedPaths, "removeDescendantSelectedPaths");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, CELL_RENDERER_PROPERTY, "CELL_RENDERER_PROPERTY");
   pragma Import (Java, TREE_MODEL_PROPERTY, "TREE_MODEL_PROPERTY");
   pragma Import (Java, ROOT_VISIBLE_PROPERTY, "ROOT_VISIBLE_PROPERTY");
   pragma Import (Java, SHOWS_ROOT_HANDLES_PROPERTY, "SHOWS_ROOT_HANDLES_PROPERTY");
   pragma Import (Java, ROW_HEIGHT_PROPERTY, "ROW_HEIGHT_PROPERTY");
   pragma Import (Java, CELL_EDITOR_PROPERTY, "CELL_EDITOR_PROPERTY");
   pragma Import (Java, EDITABLE_PROPERTY, "EDITABLE_PROPERTY");
   pragma Import (Java, LARGE_MODEL_PROPERTY, "LARGE_MODEL_PROPERTY");
   pragma Import (Java, SELECTION_MODEL_PROPERTY, "SELECTION_MODEL_PROPERTY");
   pragma Import (Java, VISIBLE_ROW_COUNT_PROPERTY, "VISIBLE_ROW_COUNT_PROPERTY");
   pragma Import (Java, INVOKES_STOP_CELL_EDITING_PROPERTY, "INVOKES_STOP_CELL_EDITING_PROPERTY");
   pragma Import (Java, SCROLLS_ON_EXPAND_PROPERTY, "SCROLLS_ON_EXPAND_PROPERTY");
   pragma Import (Java, TOGGLE_CLICK_COUNT_PROPERTY, "TOGGLE_CLICK_COUNT_PROPERTY");
   pragma Import (Java, LEAD_SELECTION_PATH_PROPERTY, "LEAD_SELECTION_PATH_PROPERTY");
   pragma Import (Java, ANCHOR_SELECTION_PATH_PROPERTY, "ANCHOR_SELECTION_PATH_PROPERTY");
   pragma Import (Java, EXPANDS_SELECTED_PATHS_PROPERTY, "EXPANDS_SELECTED_PATHS_PROPERTY");

end Javax.Swing.JTree;
pragma Import (Java, Javax.Swing.JTree, "javax.swing.JTree");
pragma Extensions_Allowed (Off);
