pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Plaf.ViewportUI;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Java.Awt.Image;

package Javax.Swing.JViewport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      IsViewSizeSet : Java.Boolean;
      pragma Import (Java, IsViewSizeSet, "isViewSizeSet");

      --  protected
      LastPaintPosition : access Java.Awt.Point.Typ'Class;
      pragma Import (Java, LastPaintPosition, "lastPaintPosition");

      --  protected
      BackingStoreImage : access Java.Awt.Image.Typ'Class;
      pragma Import (Java, BackingStoreImage, "backingStoreImage");

      --  protected
      ScrollUnderway : Java.Boolean;
      pragma Import (Java, ScrollUnderway, "scrollUnderway");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JViewport (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.ViewportUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_ViewportUI : access Standard.Javax.Swing.Plaf.ViewportUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure ScrollRectToVisible (This : access Typ;
                                  P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  final
   procedure SetBorder (This : access Typ;
                        P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class);

   --  final
   function GetInsets (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   --  final
   function GetInsets (This : access Typ;
                       P1_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   function IsOptimizedDrawingEnabled (This : access Typ)
                                       return Java.Boolean;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Reshape (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int);

   procedure SetScrollMode (This : access Typ;
                            P1_Int : Java.Int);

   function GetScrollMode (This : access Typ)
                           return Java.Int;

   function GetView (This : access Typ)
                     return access Java.Awt.Component.Typ'Class;

   procedure SetView (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetViewSize (This : access Typ)
                         return access Java.Awt.Dimension.Typ'Class;

   procedure SetViewSize (This : access Typ;
                          P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetViewPosition (This : access Typ)
                             return access Java.Awt.Point.Typ'Class;

   procedure SetViewPosition (This : access Typ;
                              P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetViewRect (This : access Typ)
                         return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function ComputeBlit (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int;
                         P3_Point : access Standard.Java.Awt.Point.Typ'Class;
                         P4_Point : access Standard.Java.Awt.Point.Typ'Class;
                         P5_Dimension : access Standard.Java.Awt.Dimension.Typ'Class;
                         P6_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                         return Java.Boolean;

   function GetExtentSize (This : access Typ)
                           return access Java.Awt.Dimension.Typ'Class;

   function ToViewCoordinates (This : access Typ;
                               P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function ToViewCoordinates (This : access Typ;
                               P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                               return access Java.Awt.Point.Typ'Class;

   procedure SetExtentSize (This : access Typ;
                            P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   --  protected
   function CreateLayoutManager (This : access Typ)
                                 return access Java.Awt.LayoutManager.Typ'Class;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BLIT_SCROLL_MODE : constant Java.Int;

   --  final
   BACKINGSTORE_SCROLL_MODE : constant Java.Int;

   --  final
   SIMPLE_SCROLL_MODE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JViewport);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, ScrollRectToVisible, "scrollRectToVisible");
   pragma Import (Java, SetBorder, "setBorder");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, IsOptimizedDrawingEnabled, "isOptimizedDrawingEnabled");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Reshape, "reshape");
   pragma Import (Java, SetScrollMode, "setScrollMode");
   pragma Import (Java, GetScrollMode, "getScrollMode");
   pragma Import (Java, GetView, "getView");
   pragma Import (Java, SetView, "setView");
   pragma Import (Java, GetViewSize, "getViewSize");
   pragma Import (Java, SetViewSize, "setViewSize");
   pragma Import (Java, GetViewPosition, "getViewPosition");
   pragma Import (Java, SetViewPosition, "setViewPosition");
   pragma Import (Java, GetViewRect, "getViewRect");
   pragma Import (Java, ComputeBlit, "computeBlit");
   pragma Import (Java, GetExtentSize, "getExtentSize");
   pragma Import (Java, ToViewCoordinates, "toViewCoordinates");
   pragma Import (Java, SetExtentSize, "setExtentSize");
   pragma Import (Java, CreateLayoutManager, "createLayoutManager");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, BLIT_SCROLL_MODE, "BLIT_SCROLL_MODE");
   pragma Import (Java, BACKINGSTORE_SCROLL_MODE, "BACKINGSTORE_SCROLL_MODE");
   pragma Import (Java, SIMPLE_SCROLL_MODE, "SIMPLE_SCROLL_MODE");

end Javax.Swing.JViewport;
pragma Import (Java, Javax.Swing.JViewport, "javax.swing.JViewport");
pragma Extensions_Allowed (Off);
