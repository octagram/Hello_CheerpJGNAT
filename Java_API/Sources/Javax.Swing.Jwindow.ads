pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Frame;
limited with Java.Awt.Graphics;
limited with Java.Awt.GraphicsConfiguration;
limited with Java.Awt.LayoutManager;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.JLayeredPane;
limited with Javax.Swing.JRootPane;
limited with Javax.Swing.TransferHandler;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Awt.Window;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.RootPaneContainer;

package Javax.Swing.JWindow is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            RootPaneContainer_I : Javax.Swing.RootPaneContainer.Ref)
    is new Java.Awt.Window.Typ(MenuContainer_I,
                               ImageObserver_I,
                               Serializable_I,
                               Accessible_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      RootPane : access Javax.Swing.JRootPane.Typ'Class;
      pragma Import (Java, RootPane, "rootPane");

      --  protected
      RootPaneCheckingEnabled : Java.Boolean;
      pragma Import (Java, RootPaneCheckingEnabled, "rootPaneCheckingEnabled");

      --  protected
      AccessibleContext : access Javax.Accessibility.AccessibleContext.Typ'Class;
      pragma Import (Java, AccessibleContext, "accessibleContext");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JWindow (This : Ref := null)
                         return Ref;

   function New_JWindow (P1_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_JWindow (P1_Frame : access Standard.Java.Awt.Frame.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_JWindow (P1_Window : access Standard.Java.Awt.Window.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   function New_JWindow (P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                         P2_GraphicsConfiguration : access Standard.Java.Awt.GraphicsConfiguration.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure WindowInit (This : access Typ);

   --  protected
   function CreateRootPane (This : access Typ)
                            return access Javax.Swing.JRootPane.Typ'Class;

   --  protected
   function IsRootPaneCheckingEnabled (This : access Typ)
                                       return Java.Boolean;

   procedure SetTransferHandler (This : access Typ;
                                 P1_TransferHandler : access Standard.Javax.Swing.TransferHandler.Typ'Class);

   function GetTransferHandler (This : access Typ)
                                return access Javax.Swing.TransferHandler.Typ'Class;

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure SetRootPaneCheckingEnabled (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   --  protected
   procedure AddImpl (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetLayout (This : access Typ;
                        P1_LayoutManager : access Standard.Java.Awt.LayoutManager.Typ'Class);

   function GetRootPane (This : access Typ)
                         return access Javax.Swing.JRootPane.Typ'Class;

   --  protected
   procedure SetRootPane (This : access Typ;
                          P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   function GetContentPane (This : access Typ)
                            return access Java.Awt.Container.Typ'Class;

   procedure SetContentPane (This : access Typ;
                             P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function GetLayeredPane (This : access Typ)
                            return access Javax.Swing.JLayeredPane.Typ'Class;

   procedure SetLayeredPane (This : access Typ;
                             P1_JLayeredPane : access Standard.Javax.Swing.JLayeredPane.Typ'Class);

   function GetGlassPane (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   procedure SetGlassPane (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class;

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JWindow);
   pragma Import (Java, WindowInit, "windowInit");
   pragma Import (Java, CreateRootPane, "createRootPane");
   pragma Import (Java, IsRootPaneCheckingEnabled, "isRootPaneCheckingEnabled");
   pragma Import (Java, SetTransferHandler, "setTransferHandler");
   pragma Import (Java, GetTransferHandler, "getTransferHandler");
   pragma Import (Java, Update, "update");
   pragma Import (Java, SetRootPaneCheckingEnabled, "setRootPaneCheckingEnabled");
   pragma Import (Java, AddImpl, "addImpl");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, SetLayout, "setLayout");
   pragma Import (Java, GetRootPane, "getRootPane");
   pragma Import (Java, SetRootPane, "setRootPane");
   pragma Import (Java, GetContentPane, "getContentPane");
   pragma Import (Java, SetContentPane, "setContentPane");
   pragma Import (Java, GetLayeredPane, "getLayeredPane");
   pragma Import (Java, SetLayeredPane, "setLayeredPane");
   pragma Import (Java, GetGlassPane, "getGlassPane");
   pragma Import (Java, SetGlassPane, "setGlassPane");
   pragma Import (Java, GetGraphics, "getGraphics");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.JWindow;
pragma Import (Java, Javax.Swing.JWindow, "javax.swing.JWindow");
pragma Extensions_Allowed (Off);
