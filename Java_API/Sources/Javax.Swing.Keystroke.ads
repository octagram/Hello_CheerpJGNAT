pragma Extensions_Allowed (On);
limited with Java.Awt.Event.KeyEvent;
limited with Java.Lang.Character;
limited with Java.Lang.String;
with Java.Awt.AWTKeyStroke;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.KeyStroke is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.AWTKeyStroke.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKeyStroke (P1_Char : Java.Char)
                          return access Javax.Swing.KeyStroke.Typ'Class;

   function GetKeyStroke (P1_Character : access Standard.Java.Lang.Character.Typ'Class;
                          P2_Int : Java.Int)
                          return access Javax.Swing.KeyStroke.Typ'Class;

   function GetKeyStroke (P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_Boolean : Java.Boolean)
                          return access Javax.Swing.KeyStroke.Typ'Class;

   function GetKeyStroke (P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return access Javax.Swing.KeyStroke.Typ'Class;

   function GetKeyStrokeForEvent (P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class)
                                  return access Javax.Swing.KeyStroke.Typ'Class;

   function GetKeyStroke (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.KeyStroke.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetKeyStroke, "getKeyStroke");
   pragma Import (Java, GetKeyStrokeForEvent, "getKeyStrokeForEvent");

end Javax.Swing.KeyStroke;
pragma Import (Java, Javax.Swing.KeyStroke, "javax.swing.KeyStroke");
pragma Extensions_Allowed (Off);
