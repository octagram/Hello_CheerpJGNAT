pragma Extensions_Allowed (On);
limited with Java.Awt.Container;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.LayoutStyle.ComponentPlacement;
with Java.Lang.Object;

package Javax.Swing.LayoutStyle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetInstance (P1_LayoutStyle : access Standard.Javax.Swing.LayoutStyle.Typ'Class);

   function GetInstance return access Javax.Swing.LayoutStyle.Typ'Class;

   function New_LayoutStyle (This : Ref := null)
                             return Ref;

   function GetPreferredGap (This : access Typ;
                             P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P3_ComponentPlacement : access Standard.Javax.Swing.LayoutStyle.ComponentPlacement.Typ'Class;
                             P4_Int : Java.Int;
                             P5_Container : access Standard.Java.Awt.Container.Typ'Class)
                             return Java.Int is abstract;

   function GetContainerGap (This : access Typ;
                             P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Container : access Standard.Java.Awt.Container.Typ'Class)
                             return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetInstance, "setInstance");
   pragma Export (Java, GetInstance, "getInstance");
   pragma Java_Constructor (New_LayoutStyle);
   pragma Export (Java, GetPreferredGap, "getPreferredGap");
   pragma Export (Java, GetContainerGap, "getContainerGap");

end Javax.Swing.LayoutStyle;
pragma Import (Java, Javax.Swing.LayoutStyle, "javax.swing.LayoutStyle");
pragma Extensions_Allowed (Off);
