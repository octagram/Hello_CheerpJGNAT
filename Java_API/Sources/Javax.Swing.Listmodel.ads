pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ListDataListener;
with Java.Lang.Object;

package Javax.Swing.ListModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSize (This : access Typ)
                     return Java.Int is abstract;

   function GetElementAt (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.Object.Typ'Class is abstract;

   procedure AddListDataListener (This : access Typ;
                                  P1_ListDataListener : access Standard.Javax.Swing.Event.ListDataListener.Typ'Class) is abstract;

   procedure RemoveListDataListener (This : access Typ;
                                     P1_ListDataListener : access Standard.Javax.Swing.Event.ListDataListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSize, "getSize");
   pragma Export (Java, GetElementAt, "getElementAt");
   pragma Export (Java, AddListDataListener, "addListDataListener");
   pragma Export (Java, RemoveListDataListener, "removeListDataListener");

end Javax.Swing.ListModel;
pragma Import (Java, Javax.Swing.ListModel, "javax.swing.ListModel");
pragma Extensions_Allowed (Off);
