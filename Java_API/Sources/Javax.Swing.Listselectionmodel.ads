pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ListSelectionListener;
with Java.Lang.Object;

package Javax.Swing.ListSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int) is abstract;

   procedure AddSelectionInterval (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int) is abstract;

   procedure RemoveSelectionInterval (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int) is abstract;

   function GetMinSelectionIndex (This : access Typ)
                                  return Java.Int is abstract;

   function GetMaxSelectionIndex (This : access Typ)
                                  return Java.Int is abstract;

   function IsSelectedIndex (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean is abstract;

   function GetAnchorSelectionIndex (This : access Typ)
                                     return Java.Int is abstract;

   procedure SetAnchorSelectionIndex (This : access Typ;
                                      P1_Int : Java.Int) is abstract;

   function GetLeadSelectionIndex (This : access Typ)
                                   return Java.Int is abstract;

   procedure SetLeadSelectionIndex (This : access Typ;
                                    P1_Int : Java.Int) is abstract;

   procedure ClearSelection (This : access Typ) is abstract;

   function IsSelectionEmpty (This : access Typ)
                              return Java.Boolean is abstract;

   procedure InsertIndexInterval (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Boolean : Java.Boolean) is abstract;

   procedure RemoveIndexInterval (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int) is abstract;

   procedure SetValueIsAdjusting (This : access Typ;
                                  P1_Boolean : Java.Boolean) is abstract;

   function GetValueIsAdjusting (This : access Typ)
                                 return Java.Boolean is abstract;

   procedure SetSelectionMode (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   function GetSelectionMode (This : access Typ)
                              return Java.Int is abstract;

   procedure AddListSelectionListener (This : access Typ;
                                       P1_ListSelectionListener : access Standard.Javax.Swing.Event.ListSelectionListener.Typ'Class) is abstract;

   procedure RemoveListSelectionListener (This : access Typ;
                                          P1_ListSelectionListener : access Standard.Javax.Swing.Event.ListSelectionListener.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SINGLE_SELECTION : constant Java.Int;

   --  final
   SINGLE_INTERVAL_SELECTION : constant Java.Int;

   --  final
   MULTIPLE_INTERVAL_SELECTION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetSelectionInterval, "setSelectionInterval");
   pragma Export (Java, AddSelectionInterval, "addSelectionInterval");
   pragma Export (Java, RemoveSelectionInterval, "removeSelectionInterval");
   pragma Export (Java, GetMinSelectionIndex, "getMinSelectionIndex");
   pragma Export (Java, GetMaxSelectionIndex, "getMaxSelectionIndex");
   pragma Export (Java, IsSelectedIndex, "isSelectedIndex");
   pragma Export (Java, GetAnchorSelectionIndex, "getAnchorSelectionIndex");
   pragma Export (Java, SetAnchorSelectionIndex, "setAnchorSelectionIndex");
   pragma Export (Java, GetLeadSelectionIndex, "getLeadSelectionIndex");
   pragma Export (Java, SetLeadSelectionIndex, "setLeadSelectionIndex");
   pragma Export (Java, ClearSelection, "clearSelection");
   pragma Export (Java, IsSelectionEmpty, "isSelectionEmpty");
   pragma Export (Java, InsertIndexInterval, "insertIndexInterval");
   pragma Export (Java, RemoveIndexInterval, "removeIndexInterval");
   pragma Export (Java, SetValueIsAdjusting, "setValueIsAdjusting");
   pragma Export (Java, GetValueIsAdjusting, "getValueIsAdjusting");
   pragma Export (Java, SetSelectionMode, "setSelectionMode");
   pragma Export (Java, GetSelectionMode, "getSelectionMode");
   pragma Export (Java, AddListSelectionListener, "addListSelectionListener");
   pragma Export (Java, RemoveListSelectionListener, "removeListSelectionListener");
   pragma Import (Java, SINGLE_SELECTION, "SINGLE_SELECTION");
   pragma Import (Java, SINGLE_INTERVAL_SELECTION, "SINGLE_INTERVAL_SELECTION");
   pragma Import (Java, MULTIPLE_INTERVAL_SELECTION, "MULTIPLE_INTERVAL_SELECTION");

end Javax.Swing.ListSelectionModel;
pragma Import (Java, Javax.Swing.ListSelectionModel, "javax.swing.ListSelectionModel");
pragma Extensions_Allowed (Off);
