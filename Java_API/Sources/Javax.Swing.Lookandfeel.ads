pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Swing.ComponentInputMap;
limited with Javax.Swing.Icon;
limited with Javax.Swing.InputMap;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.LayoutStyle;
limited with Javax.Swing.Text.JTextComponent.KeyBinding;
limited with Javax.Swing.UIDefaults;
with Java.Lang.Object;

package Javax.Swing.LookAndFeel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_LookAndFeel (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InstallColors (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class);

   procedure InstallColorsAndFont (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                                   P4_String : access Standard.Java.Lang.String.Typ'Class);

   procedure InstallBorder (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure UninstallBorder (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure InstallProperty (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   function MakeKeyBindings (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                             return Standard.Java.Lang.Object.Ref;

   function MakeInputMap (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                          return access Javax.Swing.InputMap.Typ'Class;

   function MakeComponentInputMap (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                   P2_Object_Arr : access Java.Lang.Object.Arr_Obj)
                                   return access Javax.Swing.ComponentInputMap.Typ'Class;

   procedure LoadKeyBindings (P1_InputMap : access Standard.Javax.Swing.InputMap.Typ'Class;
                              P2_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function MakeIcon (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function GetLayoutStyle (This : access Typ)
                            return access Javax.Swing.LayoutStyle.Typ'Class;

   procedure ProvideErrorFeedback (This : access Typ;
                                   P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetDesktopPropertyValue (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                     return access Java.Lang.Object.Typ'Class;

   function GetDisabledIcon (This : access Typ;
                             P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                             return access Javax.Swing.Icon.Typ'Class;

   function GetDisabledSelectedIcon (This : access Typ;
                                     P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                     P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                                     return access Javax.Swing.Icon.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetSupportsWindowDecorations (This : access Typ)
                                          return Java.Boolean;

   function IsNativeLookAndFeel (This : access Typ)
                                 return Java.Boolean is abstract;

   function IsSupportedLookAndFeel (This : access Typ)
                                    return Java.Boolean is abstract;

   procedure Initialize (This : access Typ);

   procedure Uninitialize (This : access Typ);

   function GetDefaults (This : access Typ)
                         return access Javax.Swing.UIDefaults.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LookAndFeel);
   pragma Export (Java, InstallColors, "installColors");
   pragma Export (Java, InstallColorsAndFont, "installColorsAndFont");
   pragma Export (Java, InstallBorder, "installBorder");
   pragma Export (Java, UninstallBorder, "uninstallBorder");
   pragma Export (Java, InstallProperty, "installProperty");
   pragma Export (Java, MakeKeyBindings, "makeKeyBindings");
   pragma Export (Java, MakeInputMap, "makeInputMap");
   pragma Export (Java, MakeComponentInputMap, "makeComponentInputMap");
   pragma Export (Java, LoadKeyBindings, "loadKeyBindings");
   pragma Export (Java, MakeIcon, "makeIcon");
   pragma Export (Java, GetLayoutStyle, "getLayoutStyle");
   pragma Export (Java, ProvideErrorFeedback, "provideErrorFeedback");
   pragma Export (Java, GetDesktopPropertyValue, "getDesktopPropertyValue");
   pragma Export (Java, GetDisabledIcon, "getDisabledIcon");
   pragma Export (Java, GetDisabledSelectedIcon, "getDisabledSelectedIcon");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetID, "getID");
   pragma Export (Java, GetDescription, "getDescription");
   pragma Export (Java, GetSupportsWindowDecorations, "getSupportsWindowDecorations");
   pragma Export (Java, IsNativeLookAndFeel, "isNativeLookAndFeel");
   pragma Export (Java, IsSupportedLookAndFeel, "isSupportedLookAndFeel");
   pragma Export (Java, Initialize, "initialize");
   pragma Export (Java, Uninitialize, "uninitialize");
   pragma Export (Java, GetDefaults, "getDefaults");
   pragma Export (Java, ToString, "toString");

end Javax.Swing.LookAndFeel;
pragma Import (Java, Javax.Swing.LookAndFeel, "javax.swing.LookAndFeel");
pragma Extensions_Allowed (Off);
