pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Javax.Swing.MenuSelectionManager;
with Java.Lang.Object;

package Javax.Swing.MenuElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ProcessMouseEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class;
                                P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                                P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class) is abstract;

   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                              P2_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                              P3_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class) is abstract;

   procedure MenuSelectionChanged (This : access Typ;
                                   P1_Boolean : Java.Boolean) is abstract;

   function GetSubElements (This : access Typ)
                            return Standard.Java.Lang.Object.Ref is abstract;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ProcessMouseEvent, "processMouseEvent");
   pragma Export (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Export (Java, MenuSelectionChanged, "menuSelectionChanged");
   pragma Export (Java, GetSubElements, "getSubElements");
   pragma Export (Java, GetComponent, "getComponent");

end Javax.Swing.MenuElement;
pragma Import (Java, Javax.Swing.MenuElement, "javax.swing.MenuElement");
pragma Extensions_Allowed (Off);
