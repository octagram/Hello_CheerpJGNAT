pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Point;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.MenuElement;
with Java.Lang.Object;

package Javax.Swing.MenuSelectionManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MenuSelectionManager (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function DefaultManager return access Javax.Swing.MenuSelectionManager.Typ'Class;

   procedure SetSelectedPath (This : access Typ;
                              P1_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj);

   function GetSelectedPath (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   procedure ClearSelectedPath (This : access Typ);

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   procedure ProcessMouseEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   function ComponentForPoint (This : access Typ;
                               P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                               P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                               return access Java.Awt.Component.Typ'Class;

   procedure ProcessKeyEvent (This : access Typ;
                              P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   function IsComponentPartOfCurrentMenu (This : access Typ;
                                          P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                          return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MenuSelectionManager);
   pragma Import (Java, DefaultManager, "defaultManager");
   pragma Import (Java, SetSelectedPath, "setSelectedPath");
   pragma Import (Java, GetSelectedPath, "getSelectedPath");
   pragma Import (Java, ClearSelectedPath, "clearSelectedPath");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, ProcessMouseEvent, "processMouseEvent");
   pragma Import (Java, ComponentForPoint, "componentForPoint");
   pragma Import (Java, ProcessKeyEvent, "processKeyEvent");
   pragma Import (Java, IsComponentPartOfCurrentMenu, "isComponentPartOfCurrentMenu");

end Javax.Swing.MenuSelectionManager;
pragma Import (Java, Javax.Swing.MenuSelectionManager, "javax.swing.MenuSelectionManager");
pragma Extensions_Allowed (Off);
