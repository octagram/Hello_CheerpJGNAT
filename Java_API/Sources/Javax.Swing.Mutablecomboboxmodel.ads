pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.ComboBoxModel;

package Javax.Swing.MutableComboBoxModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ComboBoxModel_I : Javax.Swing.ComboBoxModel.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddElement (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure RemoveElement (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure InsertElementAt (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_Int : Java.Int) is abstract;

   procedure RemoveElementAt (This : access Typ;
                              P1_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddElement, "addElement");
   pragma Export (Java, RemoveElement, "removeElement");
   pragma Export (Java, InsertElementAt, "insertElementAt");
   pragma Export (Java, RemoveElementAt, "removeElementAt");

end Javax.Swing.MutableComboBoxModel;
pragma Import (Java, Javax.Swing.MutableComboBoxModel, "javax.swing.MutableComboBoxModel");
pragma Extensions_Allowed (Off);
