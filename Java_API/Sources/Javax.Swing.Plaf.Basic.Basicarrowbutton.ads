pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JButton;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Basic.BasicArrowButton is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JButton.Typ(ItemSelectable_I,
                                   MenuContainer_I,
                                   ImageObserver_I,
                                   Serializable_I,
                                   Accessible_I,
                                   SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Direction : Java.Int;
      pragma Import (Java, Direction, "direction");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicArrowButton (P1_Int : Java.Int;
                                  P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                                  P3_Color : access Standard.Java.Awt.Color.Typ'Class;
                                  P4_Color : access Standard.Java.Awt.Color.Typ'Class;
                                  P5_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_BasicArrowButton (P1_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDirection (This : access Typ)
                          return Java.Int;

   procedure SetDirection (This : access Typ;
                           P1_Int : Java.Int);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean;

   procedure PaintTriangle (This : access Typ;
                            P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int;
                            P5_Int : Java.Int;
                            P6_Boolean : Java.Boolean);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicArrowButton);
   pragma Import (Java, GetDirection, "getDirection");
   pragma Import (Java, SetDirection, "setDirection");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, PaintTriangle, "paintTriangle");

end Javax.Swing.Plaf.Basic.BasicArrowButton;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicArrowButton, "javax.swing.plaf.basic.BasicArrowButton");
pragma Extensions_Allowed (Off);
