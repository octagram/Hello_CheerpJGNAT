pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.Border;
with Javax.Swing.Plaf.Basic.BasicBorders.ButtonBorder;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.Basic.BasicBorders.RolloverButtonBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.Plaf.Basic.BasicBorders.ButtonBorder.Typ(Serializable_I,
                                                                Border_I,
                                                                UIResource_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RolloverButtonBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class;
                                      P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                                      P3_Color : access Standard.Java.Awt.Color.Typ'Class;
                                      P4_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RolloverButtonBorder);
   pragma Import (Java, PaintBorder, "paintBorder");

end Javax.Swing.Plaf.Basic.BasicBorders.RolloverButtonBorder;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicBorders.RolloverButtonBorder, "javax.swing.plaf.basic.BasicBorders$RolloverButtonBorder");
pragma Extensions_Allowed (Off);
