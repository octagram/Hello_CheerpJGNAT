pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
with Java.Lang.Object;
with Javax.Swing.Border.Border;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.Basic.BasicBorders.SplitPaneBorder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Border_I : Javax.Swing.Border.Border.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Highlight : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Highlight, "highlight");

      --  protected
      Shadow : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Shadow, "shadow");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SplitPaneBorder (P1_Color : access Standard.Java.Awt.Color.Typ'Class;
                                 P2_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SplitPaneBorder);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");

end Javax.Swing.Plaf.Basic.BasicBorders.SplitPaneBorder;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicBorders.SplitPaneBorder, "javax.swing.plaf.basic.BasicBorders$SplitPaneBorder");
pragma Extensions_Allowed (Off);
