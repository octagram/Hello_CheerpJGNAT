pragma Extensions_Allowed (On);
limited with Javax.Swing.Border.Border;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicBorders is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicBorders (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetButtonBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetRadioButtonBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetToggleButtonBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetMenuBarBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetSplitPaneBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetSplitPaneDividerBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetTextFieldBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetProgressBarBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetInternalFrameBorder return access Javax.Swing.Border.Border.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicBorders);
   pragma Import (Java, GetButtonBorder, "getButtonBorder");
   pragma Import (Java, GetRadioButtonBorder, "getRadioButtonBorder");
   pragma Import (Java, GetToggleButtonBorder, "getToggleButtonBorder");
   pragma Import (Java, GetMenuBarBorder, "getMenuBarBorder");
   pragma Import (Java, GetSplitPaneBorder, "getSplitPaneBorder");
   pragma Import (Java, GetSplitPaneDividerBorder, "getSplitPaneDividerBorder");
   pragma Import (Java, GetTextFieldBorder, "getTextFieldBorder");
   pragma Import (Java, GetProgressBarBorder, "getProgressBarBorder");
   pragma Import (Java, GetInternalFrameBorder, "getInternalFrameBorder");

end Javax.Swing.Plaf.Basic.BasicBorders;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicBorders, "javax.swing.plaf.basic.BasicBorders");
pragma Extensions_Allowed (Off);
