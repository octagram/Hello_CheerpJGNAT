pragma Extensions_Allowed (On);
limited with Java.Awt.Event.FocusEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Beans.PropertyChangeEvent;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.JComponent;
with Java.Awt.Event.FocusListener;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;
with Javax.Swing.Event.ChangeListener;

package Javax.Swing.Plaf.Basic.BasicButtonListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(FocusListener_I : Java.Awt.Event.FocusListener.Ref;
            MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            ChangeListener_I : Javax.Swing.Event.ChangeListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicButtonListener (P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  protected
   procedure CheckOpacity (This : access Typ;
                           P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   procedure InstallKeyboardActions (This : access Typ;
                                     P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallKeyboardActions (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure StateChanged (This : access Typ;
                           P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);

   procedure FocusGained (This : access Typ;
                          P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure FocusLost (This : access Typ;
                        P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicButtonListener);
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, CheckOpacity, "checkOpacity");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, StateChanged, "stateChanged");
   pragma Import (Java, FocusGained, "focusGained");
   pragma Import (Java, FocusLost, "focusLost");
   pragma Import (Java, MouseMoved, "mouseMoved");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseClicked, "mouseClicked");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MouseEntered, "mouseEntered");
   pragma Import (Java, MouseExited, "mouseExited");

end Javax.Swing.Plaf.Basic.BasicButtonListener;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicButtonListener, "javax.swing.plaf.basic.BasicButtonListener");
pragma Extensions_Allowed (Off);
