pragma Extensions_Allowed (On);
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.Basic.BasicButtonListener;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.ButtonUI;

package Javax.Swing.Plaf.Basic.BasicButtonUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.ButtonUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DefaultTextIconGap : Java.Int;
      pragma Import (Java, DefaultTextIconGap, "defaultTextIconGap");

      --  protected
      DefaultTextShiftOffset : Java.Int;
      pragma Import (Java, DefaultTextShiftOffset, "defaultTextShiftOffset");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicButtonUI (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ;
                               P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure InstallKeyboardActions (This : access Typ;
                                     P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ;
                                       P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure UninstallListeners (This : access Typ;
                                 P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   function CreateButtonListener (This : access Typ;
                                  P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class)
                                  return access Javax.Swing.Plaf.Basic.BasicButtonListener.Typ'Class;

   function GetDefaultTextIconGap (This : access Typ;
                                   P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class)
                                   return Java.Int;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintIcon (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                        P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure PaintText (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                        P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P4_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure PaintText (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class;
                        P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P4_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure PaintFocus (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class;
                         P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                         P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                         P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure PaintButtonPressed (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure ClearTextShiftOffset (This : access Typ);

   --  protected
   procedure SetTextShiftOffset (This : access Typ);

   --  protected
   function GetTextShiftOffset (This : access Typ)
                                return Java.Int;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicButtonUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, CreateButtonListener, "createButtonListener");
   pragma Import (Java, GetDefaultTextIconGap, "getDefaultTextIconGap");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintIcon, "paintIcon");
   pragma Import (Java, PaintText, "paintText");
   pragma Import (Java, PaintFocus, "paintFocus");
   pragma Import (Java, PaintButtonPressed, "paintButtonPressed");
   pragma Import (Java, ClearTextShiftOffset, "clearTextShiftOffset");
   pragma Import (Java, SetTextShiftOffset, "setTextShiftOffset");
   pragma Import (Java, GetTextShiftOffset, "getTextShiftOffset");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");

end Javax.Swing.Plaf.Basic.BasicButtonUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicButtonUI, "javax.swing.plaf.basic.BasicButtonUI");
pragma Extensions_Allowed (Off);
