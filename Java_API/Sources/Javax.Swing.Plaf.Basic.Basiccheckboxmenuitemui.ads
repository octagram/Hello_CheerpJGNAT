pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JMenuItem;
limited with Javax.Swing.MenuElement;
limited with Javax.Swing.MenuSelectionManager;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicMenuItemUI;

package Javax.Swing.Plaf.Basic.BasicCheckBoxMenuItemUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicMenuItemUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicCheckBoxMenuItemUI (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure ProcessMouseEvent (This : access Typ;
                                P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class;
                                P2_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class;
                                P3_MenuElement_Arr : access Javax.Swing.MenuElement.Arr_Obj;
                                P4_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicCheckBoxMenuItemUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, ProcessMouseEvent, "processMouseEvent");

end Javax.Swing.Plaf.Basic.BasicCheckBoxMenuItemUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicCheckBoxMenuItemUI, "javax.swing.plaf.basic.BasicCheckBoxMenuItemUI");
pragma Extensions_Allowed (Off);
