pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.Colorchooser.AbstractColorChooserPanel;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.JColorChooser;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.ColorChooserUI;

package Javax.Swing.Plaf.Basic.BasicColorChooserUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.ColorChooserUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Chooser : access Javax.Swing.JColorChooser.Typ'Class;
      pragma Import (Java, Chooser, "chooser");

      --  protected
      DefaultChoosers : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, DefaultChoosers, "defaultChoosers");

      --  protected
      PreviewListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, PreviewListener, "previewListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicColorChooserUI (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function CreateDefaultChoosers (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure UninstallDefaultChoosers (This : access Typ);

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallPreviewPanel (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure UninstallListeners (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicColorChooserUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, CreateDefaultChoosers, "createDefaultChoosers");
   pragma Import (Java, UninstallDefaultChoosers, "uninstallDefaultChoosers");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallPreviewPanel, "installPreviewPanel");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, UninstallListeners, "uninstallListeners");

end Javax.Swing.Plaf.Basic.BasicColorChooserUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicColorChooserUI, "javax.swing.plaf.basic.BasicColorChooserUI");
pragma Extensions_Allowed (Off);
