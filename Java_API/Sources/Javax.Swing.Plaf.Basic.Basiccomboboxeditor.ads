pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.Event.FocusEvent;
limited with Javax.Swing.JTextField;
with Java.Awt.Event.FocusListener;
with Java.Lang.Object;
with Javax.Swing.ComboBoxEditor;

package Javax.Swing.Plaf.Basic.BasicComboBoxEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(FocusListener_I : Java.Awt.Event.FocusListener.Ref;
            ComboBoxEditor_I : Javax.Swing.ComboBoxEditor.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Editor : access Javax.Swing.JTextField.Typ'Class;
      pragma Import (Java, Editor, "editor");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicComboBoxEditor (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEditorComponent (This : access Typ)
                                return access Java.Awt.Component.Typ'Class;

   --  protected
   function CreateEditorComponent (This : access Typ)
                                   return access Javax.Swing.JTextField.Typ'Class;

   procedure SetItem (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetItem (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   procedure SelectAll (This : access Typ);

   procedure FocusGained (This : access Typ;
                          P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure FocusLost (This : access Typ;
                        P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicComboBoxEditor);
   pragma Import (Java, GetEditorComponent, "getEditorComponent");
   pragma Import (Java, CreateEditorComponent, "createEditorComponent");
   pragma Import (Java, SetItem, "setItem");
   pragma Import (Java, GetItem, "getItem");
   pragma Import (Java, SelectAll, "selectAll");
   pragma Import (Java, FocusGained, "focusGained");
   pragma Import (Java, FocusLost, "focusLost");
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");

end Javax.Swing.Plaf.Basic.BasicComboBoxEditor;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicComboBoxEditor, "javax.swing.plaf.basic.BasicComboBoxEditor");
pragma Extensions_Allowed (Off);
