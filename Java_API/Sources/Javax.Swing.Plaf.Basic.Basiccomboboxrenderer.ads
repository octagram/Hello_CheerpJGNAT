pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.JList;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JLabel;
with Javax.Swing.ListCellRenderer;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Basic.BasicComboBoxRenderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ListCellRenderer_I : Javax.Swing.ListCellRenderer.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JLabel.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I,
                                  Accessible_I,
                                  SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicComboBoxRenderer (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetListCellRendererComponent (This : access Typ;
                                          P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                          P3_Int : Java.Int;
                                          P4_Boolean : Java.Boolean;
                                          P5_Boolean : Java.Boolean)
                                          return access Java.Awt.Component.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   NoFocusBorder : access Javax.Swing.Border.Border.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicComboBoxRenderer);
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetListCellRendererComponent, "getListCellRendererComponent");
   pragma Import (Java, NoFocusBorder, "noFocusBorder");

end Javax.Swing.Plaf.Basic.BasicComboBoxRenderer;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicComboBoxRenderer, "javax.swing.plaf.basic.BasicComboBoxRenderer");
pragma Extensions_Allowed (Off);
