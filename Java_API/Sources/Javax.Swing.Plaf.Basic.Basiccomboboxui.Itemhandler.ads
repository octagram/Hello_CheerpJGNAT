pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ItemEvent;
with Java.Awt.Event.ItemListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicComboBoxUI.ItemHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemListener_I : Java.Awt.Event.ItemListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ItemHandler (P1_BasicComboBoxUI : access Standard.Javax.Swing.Plaf.Basic.BasicComboBoxUI.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ItemStateChanged (This : access Typ;
                               P1_ItemEvent : access Standard.Java.Awt.Event.ItemEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ItemHandler);
   pragma Import (Java, ItemStateChanged, "itemStateChanged");

end Javax.Swing.Plaf.Basic.BasicComboBoxUI.ItemHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicComboBoxUI.ItemHandler, "javax.swing.plaf.basic.BasicComboBoxUI$ItemHandler");
pragma Extensions_Allowed (Off);
