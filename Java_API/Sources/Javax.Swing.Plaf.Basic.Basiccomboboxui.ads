pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Event.ItemListener;
limited with Java.Awt.Event.KeyListener;
limited with Java.Awt.Event.MouseListener;
limited with Java.Awt.Event.MouseMotionListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Accessibility.Accessible;
limited with Javax.Swing.CellRendererPane;
limited with Javax.Swing.ComboBoxEditor;
limited with Javax.Swing.Event.ListDataListener;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JComboBox;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JList;
limited with Javax.Swing.ListCellRenderer;
limited with Javax.Swing.Plaf.Basic.ComboPopup;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComboBoxUI;

package Javax.Swing.Plaf.Basic.BasicComboBoxUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.ComboBoxUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ComboBox : access Javax.Swing.JComboBox.Typ'Class;
      pragma Import (Java, ComboBox, "comboBox");

      --  protected
      HasFocus : Java.Boolean;
      pragma Import (Java, HasFocus, "hasFocus");

      --  protected
      ListBox : access Javax.Swing.JList.Typ'Class;
      pragma Import (Java, ListBox, "listBox");

      --  protected
      CurrentValuePane : access Javax.Swing.CellRendererPane.Typ'Class;
      pragma Import (Java, CurrentValuePane, "currentValuePane");

      --  protected
      Popup : access Javax.Swing.Plaf.Basic.ComboPopup.Typ'Class;
      pragma Import (Java, Popup, "popup");

      --  protected
      Editor : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, Editor, "editor");

      --  protected
      ArrowButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, ArrowButton, "arrowButton");

      --  protected
      KeyListener : access Java.Awt.Event.KeyListener.Typ'Class;
      pragma Import (Java, KeyListener, "keyListener");

      --  protected
      FocusListener : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, FocusListener, "focusListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      ItemListener : access Java.Awt.Event.ItemListener.Typ'Class;
      pragma Import (Java, ItemListener, "itemListener");

      --  protected
      PopupMouseListener : access Java.Awt.Event.MouseListener.Typ'Class;
      pragma Import (Java, PopupMouseListener, "popupMouseListener");

      --  protected
      PopupMouseMotionListener : access Java.Awt.Event.MouseMotionListener.Typ'Class;
      pragma Import (Java, PopupMouseMotionListener, "popupMouseMotionListener");

      --  protected
      PopupKeyListener : access Java.Awt.Event.KeyListener.Typ'Class;
      pragma Import (Java, PopupKeyListener, "popupKeyListener");

      --  protected
      ListDataListener : access Javax.Swing.Event.ListDataListener.Typ'Class;
      pragma Import (Java, ListDataListener, "listDataListener");

      --  protected
      IsMinimumSizeDirty : Java.Boolean;
      pragma Import (Java, IsMinimumSizeDirty, "isMinimumSizeDirty");

      --  protected
      CachedMinimumSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, CachedMinimumSize, "cachedMinimumSize");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicComboBoxUI (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   function CreatePopup (This : access Typ)
                         return access Javax.Swing.Plaf.Basic.ComboPopup.Typ'Class;

   --  protected
   function CreateKeyListener (This : access Typ)
                               return access Java.Awt.Event.KeyListener.Typ'Class;

   --  protected
   function CreateFocusListener (This : access Typ)
                                 return access Java.Awt.Event.FocusListener.Typ'Class;

   --  protected
   function CreateListDataListener (This : access Typ)
                                    return access Javax.Swing.Event.ListDataListener.Typ'Class;

   --  protected
   function CreateItemListener (This : access Typ)
                                return access Java.Awt.Event.ItemListener.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateLayoutManager (This : access Typ)
                                 return access Java.Awt.LayoutManager.Typ'Class;

   --  protected
   function CreateRenderer (This : access Typ)
                            return access Javax.Swing.ListCellRenderer.Typ'Class;

   --  protected
   function CreateEditor (This : access Typ)
                          return access Javax.Swing.ComboBoxEditor.Typ'Class;

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   procedure AddEditor (This : access Typ);

   procedure RemoveEditor (This : access Typ);

   --  protected
   procedure ConfigureEditor (This : access Typ);

   --  protected
   procedure UnconfigureEditor (This : access Typ);

   procedure ConfigureArrowButton (This : access Typ);

   procedure UnconfigureArrowButton (This : access Typ);

   --  protected
   function CreateArrowButton (This : access Typ)
                               return access Javax.Swing.JButton.Typ'Class;

   function IsPopupVisible (This : access Typ;
                            P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class)
                            return Java.Boolean;

   procedure SetPopupVisible (This : access Typ;
                              P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class;
                              P2_Boolean : Java.Boolean);

   function IsFocusTraversable (This : access Typ;
                                P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class)
                                return Java.Boolean;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   function GetAccessibleChildrenCount (This : access Typ;
                                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                        return Java.Int;

   function GetAccessibleChild (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;

   --  protected
   function IsNavigationKey (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Boolean;

   --  protected
   procedure SelectNextPossibleValue (This : access Typ);

   --  protected
   procedure SelectPreviousPossibleValue (This : access Typ);

   --  protected
   procedure ToggleOpenClose (This : access Typ);

   --  protected
   function RectangleForCurrentValue (This : access Typ)
                                      return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function GetInsets (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   procedure PaintCurrentValue (This : access Typ;
                                P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                P3_Boolean : Java.Boolean);

   procedure PaintCurrentValueBackground (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                          P3_Boolean : Java.Boolean);

   --  protected
   function GetDefaultSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetDisplaySize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicComboBoxUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, CreatePopup, "createPopup");
   pragma Import (Java, CreateKeyListener, "createKeyListener");
   pragma Import (Java, CreateFocusListener, "createFocusListener");
   pragma Import (Java, CreateListDataListener, "createListDataListener");
   pragma Import (Java, CreateItemListener, "createItemListener");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreateLayoutManager, "createLayoutManager");
   pragma Import (Java, CreateRenderer, "createRenderer");
   pragma Import (Java, CreateEditor, "createEditor");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, AddEditor, "addEditor");
   pragma Import (Java, RemoveEditor, "removeEditor");
   pragma Import (Java, ConfigureEditor, "configureEditor");
   pragma Import (Java, UnconfigureEditor, "unconfigureEditor");
   pragma Import (Java, ConfigureArrowButton, "configureArrowButton");
   pragma Import (Java, UnconfigureArrowButton, "unconfigureArrowButton");
   pragma Import (Java, CreateArrowButton, "createArrowButton");
   pragma Import (Java, IsPopupVisible, "isPopupVisible");
   pragma Import (Java, SetPopupVisible, "setPopupVisible");
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");
   pragma Import (Java, IsNavigationKey, "isNavigationKey");
   pragma Import (Java, SelectNextPossibleValue, "selectNextPossibleValue");
   pragma Import (Java, SelectPreviousPossibleValue, "selectPreviousPossibleValue");
   pragma Import (Java, ToggleOpenClose, "toggleOpenClose");
   pragma Import (Java, RectangleForCurrentValue, "rectangleForCurrentValue");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, PaintCurrentValue, "paintCurrentValue");
   pragma Import (Java, PaintCurrentValueBackground, "paintCurrentValueBackground");
   pragma Import (Java, GetDefaultSize, "getDefaultSize");
   pragma Import (Java, GetDisplaySize, "getDisplaySize");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");

end Javax.Swing.Plaf.Basic.BasicComboBoxUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicComboBoxUI, "javax.swing.plaf.basic.BasicComboBoxUI");
pragma Extensions_Allowed (Off);
