pragma Extensions_Allowed (On);
limited with Java.Awt.Event.KeyEvent;
with Java.Awt.Event.KeyAdapter;
with Java.Awt.Event.KeyListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicComboPopup.InvocationKeyHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeyListener_I : Java.Awt.Event.KeyListener.Ref)
    is new Java.Awt.Event.KeyAdapter.Typ(KeyListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InvocationKeyHandler (P1_BasicComboPopup : access Standard.Javax.Swing.Plaf.Basic.BasicComboPopup.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure KeyReleased (This : access Typ;
                          P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InvocationKeyHandler);
   pragma Import (Java, KeyReleased, "keyReleased");

end Javax.Swing.Plaf.Basic.BasicComboPopup.InvocationKeyHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicComboPopup.InvocationKeyHandler, "javax.swing.plaf.basic.BasicComboPopup$InvocationKeyHandler");
pragma Extensions_Allowed (Off);
