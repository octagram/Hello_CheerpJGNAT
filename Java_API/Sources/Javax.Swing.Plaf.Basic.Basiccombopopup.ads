pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ItemListener;
limited with Java.Awt.Event.KeyListener;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Event.MouseListener;
limited with Java.Awt.Event.MouseMotionListener;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.ComboBoxModel;
limited with Javax.Swing.Event.ListDataListener;
limited with Javax.Swing.Event.ListSelectionListener;
limited with Javax.Swing.JComboBox;
limited with Javax.Swing.JList;
limited with Javax.Swing.JScrollPane;
limited with Javax.Swing.Timer;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JPopupMenu;
with Javax.Swing.MenuElement;
with Javax.Swing.Plaf.Basic.ComboPopup;

package Javax.Swing.Plaf.Basic.BasicComboPopup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            MenuElement_I : Javax.Swing.MenuElement.Ref;
            ComboPopup_I : Javax.Swing.Plaf.Basic.ComboPopup.Ref)
    is new Javax.Swing.JPopupMenu.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I,
                                      Accessible_I,
                                      MenuElement_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ComboBox : access Javax.Swing.JComboBox.Typ'Class;
      pragma Import (Java, ComboBox, "comboBox");

      --  protected
      List : access Javax.Swing.JList.Typ'Class;
      pragma Import (Java, List, "list");

      --  protected
      Scroller : access Javax.Swing.JScrollPane.Typ'Class;
      pragma Import (Java, Scroller, "scroller");

      --  protected
      ValueIsAdjusting : Java.Boolean;
      pragma Import (Java, ValueIsAdjusting, "valueIsAdjusting");

      --  protected
      MouseMotionListener : access Java.Awt.Event.MouseMotionListener.Typ'Class;
      pragma Import (Java, MouseMotionListener, "mouseMotionListener");

      --  protected
      MouseListener : access Java.Awt.Event.MouseListener.Typ'Class;
      pragma Import (Java, MouseListener, "mouseListener");

      --  protected
      KeyListener : access Java.Awt.Event.KeyListener.Typ'Class;
      pragma Import (Java, KeyListener, "keyListener");

      --  protected
      ListSelectionListener : access Javax.Swing.Event.ListSelectionListener.Typ'Class;
      pragma Import (Java, ListSelectionListener, "listSelectionListener");

      --  protected
      ListMouseListener : access Java.Awt.Event.MouseListener.Typ'Class;
      pragma Import (Java, ListMouseListener, "listMouseListener");

      --  protected
      ListMouseMotionListener : access Java.Awt.Event.MouseMotionListener.Typ'Class;
      pragma Import (Java, ListMouseMotionListener, "listMouseMotionListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      ListDataListener : access Javax.Swing.Event.ListDataListener.Typ'Class;
      pragma Import (Java, ListDataListener, "listDataListener");

      --  protected
      ItemListener : access Java.Awt.Event.ItemListener.Typ'Class;
      pragma Import (Java, ItemListener, "itemListener");

      --  protected
      AutoscrollTimer : access Javax.Swing.Timer.Typ'Class;
      pragma Import (Java, AutoscrollTimer, "autoscrollTimer");

      --  protected
      HasEntered : Java.Boolean;
      pragma Import (Java, HasEntered, "hasEntered");

      --  protected
      IsAutoScrolling : Java.Boolean;
      pragma Import (Java, IsAutoScrolling, "isAutoScrolling");

      --  protected
      ScrollDirection : Java.Int;
      pragma Import (Java, ScrollDirection, "scrollDirection");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Show (This : access Typ);

   procedure Hide (This : access Typ);

   function GetList (This : access Typ)
                     return access Javax.Swing.JList.Typ'Class;

   function GetMouseListener (This : access Typ)
                              return access Java.Awt.Event.MouseListener.Typ'Class;

   function GetMouseMotionListener (This : access Typ)
                                    return access Java.Awt.Event.MouseMotionListener.Typ'Class;

   function GetKeyListener (This : access Typ)
                            return access Java.Awt.Event.KeyListener.Typ'Class;

   procedure UninstallingUI (This : access Typ);

   --  protected
   procedure UninstallComboBoxModelListeners (This : access Typ;
                                              P1_ComboBoxModel : access Standard.Javax.Swing.ComboBoxModel.Typ'Class);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicComboPopup (P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   --  protected
   procedure FirePopupMenuWillBecomeVisible (This : access Typ);

   --  protected
   procedure FirePopupMenuWillBecomeInvisible (This : access Typ);

   --  protected
   procedure FirePopupMenuCanceled (This : access Typ);

   --  protected
   function CreateMouseListener (This : access Typ)
                                 return access Java.Awt.Event.MouseListener.Typ'Class;

   --  protected
   function CreateMouseMotionListener (This : access Typ)
                                       return access Java.Awt.Event.MouseMotionListener.Typ'Class;

   --  protected
   function CreateKeyListener (This : access Typ)
                               return access Java.Awt.Event.KeyListener.Typ'Class;

   --  protected
   function CreateListSelectionListener (This : access Typ)
                                         return access Javax.Swing.Event.ListSelectionListener.Typ'Class;

   --  protected
   function CreateListDataListener (This : access Typ)
                                    return access Javax.Swing.Event.ListDataListener.Typ'Class;

   --  protected
   function CreateListMouseListener (This : access Typ)
                                     return access Java.Awt.Event.MouseListener.Typ'Class;

   --  protected
   function CreateListMouseMotionListener (This : access Typ)
                                           return access Java.Awt.Event.MouseMotionListener.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateItemListener (This : access Typ)
                                return access Java.Awt.Event.ItemListener.Typ'Class;

   --  protected
   function CreateList (This : access Typ)
                        return access Javax.Swing.JList.Typ'Class;

   --  protected
   procedure ConfigureList (This : access Typ);

   --  protected
   procedure InstallListListeners (This : access Typ);

   --  protected
   function CreateScroller (This : access Typ)
                            return access Javax.Swing.JScrollPane.Typ'Class;

   --  protected
   procedure ConfigureScroller (This : access Typ);

   --  protected
   procedure ConfigurePopup (This : access Typ);

   --  protected
   procedure InstallComboBoxListeners (This : access Typ);

   --  protected
   procedure InstallComboBoxModelListeners (This : access Typ;
                                            P1_ComboBoxModel : access Standard.Javax.Swing.ComboBoxModel.Typ'Class);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean;

   --  protected
   procedure StartAutoScrolling (This : access Typ;
                                 P1_Int : Java.Int);

   --  protected
   procedure StopAutoScrolling (This : access Typ);

   --  protected
   procedure AutoScrollUp (This : access Typ);

   --  protected
   procedure AutoScrollDown (This : access Typ);

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   --  protected
   procedure DelegateFocus (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   --  protected
   procedure TogglePopup (This : access Typ);

   --  protected
   function ConvertMouseEvent (This : access Typ;
                               P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                               return access Java.Awt.Event.MouseEvent.Typ'Class;

   --  protected
   function GetPopupHeightForRowCount (This : access Typ;
                                       P1_Int : Java.Int)
                                       return Java.Int;

   --  protected
   function ComputePopupBounds (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int)
                                return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   procedure UpdateListBoxSelectionForEvent (This : access Typ;
                                             P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class;
                                             P2_Boolean : Java.Boolean);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   SCROLL_UP : constant Java.Int;

   --  protected  final
   SCROLL_DOWN : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Show, "show");
   pragma Import (Java, Hide, "hide");
   pragma Import (Java, GetList, "getList");
   pragma Import (Java, GetMouseListener, "getMouseListener");
   pragma Import (Java, GetMouseMotionListener, "getMouseMotionListener");
   pragma Import (Java, GetKeyListener, "getKeyListener");
   pragma Import (Java, UninstallingUI, "uninstallingUI");
   pragma Import (Java, UninstallComboBoxModelListeners, "uninstallComboBoxModelListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Java_Constructor (New_BasicComboPopup);
   pragma Import (Java, FirePopupMenuWillBecomeVisible, "firePopupMenuWillBecomeVisible");
   pragma Import (Java, FirePopupMenuWillBecomeInvisible, "firePopupMenuWillBecomeInvisible");
   pragma Import (Java, FirePopupMenuCanceled, "firePopupMenuCanceled");
   pragma Import (Java, CreateMouseListener, "createMouseListener");
   pragma Import (Java, CreateMouseMotionListener, "createMouseMotionListener");
   pragma Import (Java, CreateKeyListener, "createKeyListener");
   pragma Import (Java, CreateListSelectionListener, "createListSelectionListener");
   pragma Import (Java, CreateListDataListener, "createListDataListener");
   pragma Import (Java, CreateListMouseListener, "createListMouseListener");
   pragma Import (Java, CreateListMouseMotionListener, "createListMouseMotionListener");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreateItemListener, "createItemListener");
   pragma Import (Java, CreateList, "createList");
   pragma Import (Java, ConfigureList, "configureList");
   pragma Import (Java, InstallListListeners, "installListListeners");
   pragma Import (Java, CreateScroller, "createScroller");
   pragma Import (Java, ConfigureScroller, "configureScroller");
   pragma Import (Java, ConfigurePopup, "configurePopup");
   pragma Import (Java, InstallComboBoxListeners, "installComboBoxListeners");
   pragma Import (Java, InstallComboBoxModelListeners, "installComboBoxModelListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, StartAutoScrolling, "startAutoScrolling");
   pragma Import (Java, StopAutoScrolling, "stopAutoScrolling");
   pragma Import (Java, AutoScrollUp, "autoScrollUp");
   pragma Import (Java, AutoScrollDown, "autoScrollDown");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, DelegateFocus, "delegateFocus");
   pragma Import (Java, TogglePopup, "togglePopup");
   pragma Import (Java, ConvertMouseEvent, "convertMouseEvent");
   pragma Import (Java, GetPopupHeightForRowCount, "getPopupHeightForRowCount");
   pragma Import (Java, ComputePopupBounds, "computePopupBounds");
   pragma Import (Java, UpdateListBoxSelectionForEvent, "updateListBoxSelectionForEvent");
   pragma Import (Java, SCROLL_UP, "SCROLL_UP");
   pragma Import (Java, SCROLL_DOWN, "SCROLL_DOWN");

end Javax.Swing.Plaf.Basic.BasicComboPopup;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicComboPopup, "javax.swing.plaf.basic.BasicComboPopup");
pragma Extensions_Allowed (Off);
