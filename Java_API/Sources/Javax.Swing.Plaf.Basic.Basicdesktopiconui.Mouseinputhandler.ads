pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Javax.Swing.JComponent;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Lang.Object;
with Javax.Swing.Event.MouseInputAdapter;
with Javax.Swing.Event.MouseInputListener;

package Javax.Swing.Plaf.Basic.BasicDesktopIconUI.MouseInputHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref;
            MouseInputListener_I : Javax.Swing.Event.MouseInputListener.Ref)
    is new Javax.Swing.Event.MouseInputAdapter.Typ(MouseListener_I,
                                                   MouseMotionListener_I,
                                                   MouseWheelListener_I,
                                                   MouseInputListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MouseInputHandler (P1_BasicDesktopIconUI : access Standard.Javax.Swing.Plaf.Basic.BasicDesktopIconUI.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MoveAndRepaint (This : access Typ;
                             P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MouseInputHandler);
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseMoved, "mouseMoved");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MoveAndRepaint, "moveAndRepaint");

end Javax.Swing.Plaf.Basic.BasicDesktopIconUI.MouseInputHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicDesktopIconUI.MouseInputHandler, "javax.swing.plaf.basic.BasicDesktopIconUI$MouseInputHandler");
pragma Extensions_Allowed (Off);
