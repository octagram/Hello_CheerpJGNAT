pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Insets;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JInternalFrame;
limited with Javax.Swing.JInternalFrame.JDesktopIcon;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.DesktopIconUI;

package Javax.Swing.Plaf.Basic.BasicDesktopIconUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.DesktopIconUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DesktopIcon : access Javax.Swing.JInternalFrame.JDesktopIcon.Typ'Class;
      pragma Import (Java, DesktopIcon, "desktopIcon");

      --  protected
      Frame : access Javax.Swing.JInternalFrame.Typ'Class;
      pragma Import (Java, Frame, "frame");

      --  protected
      IconPane : access Javax.Swing.JComponent.Typ'Class;
      pragma Import (Java, IconPane, "iconPane");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicDesktopIconUI (This : Ref := null)
                                    return Ref;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   function CreateMouseInputListener (This : access Typ)
                                      return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetInsets (This : access Typ;
                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   procedure Deiconize (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_BasicDesktopIconUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, CreateMouseInputListener, "createMouseInputListener");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, Deiconize, "deiconize");

end Javax.Swing.Plaf.Basic.BasicDesktopIconUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicDesktopIconUI, "javax.swing.plaf.basic.BasicDesktopIconUI");
pragma Extensions_Allowed (Off);
