pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.DesktopManager;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JDesktopPane;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.DesktopPaneUI;

package Javax.Swing.Plaf.Basic.BasicDesktopPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.DesktopPaneUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Desktop : access Javax.Swing.JDesktopPane.Typ'Class;
      pragma Import (Java, Desktop, "desktop");

      --  protected
      DesktopManager : access Javax.Swing.DesktopManager.Typ'Class;
      pragma Import (Java, DesktopManager, "desktopManager");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicDesktopPaneUI (This : Ref := null)
                                    return Ref;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallDesktopManager (This : access Typ);

   --  protected
   procedure UninstallDesktopManager (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure RegisterKeyboardActions (This : access Typ);

   --  protected
   procedure UnregisterKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_BasicDesktopPaneUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallDesktopManager, "installDesktopManager");
   pragma Import (Java, UninstallDesktopManager, "uninstallDesktopManager");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, RegisterKeyboardActions, "registerKeyboardActions");
   pragma Import (Java, UnregisterKeyboardActions, "unregisterKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");

end Javax.Swing.Plaf.Basic.BasicDesktopPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicDesktopPaneUI, "javax.swing.plaf.basic.BasicDesktopPaneUI");
pragma Extensions_Allowed (Off);
