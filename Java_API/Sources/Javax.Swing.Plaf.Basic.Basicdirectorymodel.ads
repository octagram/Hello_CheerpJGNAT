pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Io.File;
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Swing.Event.ListDataEvent;
limited with Javax.Swing.JFileChooser;
with Java.Beans.PropertyChangeListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.AbstractListModel;
with Javax.Swing.ListModel;

package Javax.Swing.Plaf.Basic.BasicDirectoryModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            ListModel_I : Javax.Swing.ListModel.Ref)
    is new Javax.Swing.AbstractListModel.Typ(Serializable_I,
                                             ListModel_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicDirectoryModel (P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   procedure InvalidateFileCache (This : access Typ);

   function GetDirectories (This : access Typ)
                            return access Java.Util.Vector.Typ'Class;

   function GetFiles (This : access Typ)
                      return access Java.Util.Vector.Typ'Class;

   procedure ValidateFileCache (This : access Typ);

   function RenameFile (This : access Typ;
                        P1_File : access Standard.Java.Io.File.Typ'Class;
                        P2_File : access Standard.Java.Io.File.Typ'Class)
                        return Java.Boolean;

   procedure FireContentsChanged (This : access Typ);

   function GetSize (This : access Typ)
                     return Java.Int;

   function Contains (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return Java.Boolean;

   function IndexOf (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return Java.Int;

   function GetElementAt (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.Object.Typ'Class;

   procedure IntervalAdded (This : access Typ;
                            P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);

   procedure IntervalRemoved (This : access Typ;
                              P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);

   --  protected
   procedure Sort (This : access Typ;
                   P1_Vector : access Standard.Java.Util.Vector.Typ'Class);

   --  protected
   function Lt (This : access Typ;
                P1_File : access Standard.Java.Io.File.Typ'Class;
                P2_File : access Standard.Java.Io.File.Typ'Class)
                return Java.Boolean;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicDirectoryModel);
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, InvalidateFileCache, "invalidateFileCache");
   pragma Import (Java, GetDirectories, "getDirectories");
   pragma Import (Java, GetFiles, "getFiles");
   pragma Import (Java, ValidateFileCache, "validateFileCache");
   pragma Import (Java, RenameFile, "renameFile");
   pragma Import (Java, FireContentsChanged, "fireContentsChanged");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, IndexOf, "indexOf");
   pragma Import (Java, GetElementAt, "getElementAt");
   pragma Import (Java, IntervalAdded, "intervalAdded");
   pragma Import (Java, IntervalRemoved, "intervalRemoved");
   pragma Import (Java, Sort, "sort");
   pragma Import (Java, Lt, "lt");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");

end Javax.Swing.Plaf.Basic.BasicDirectoryModel;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicDirectoryModel, "javax.swing.plaf.basic.BasicDirectoryModel");
pragma Extensions_Allowed (Off);
