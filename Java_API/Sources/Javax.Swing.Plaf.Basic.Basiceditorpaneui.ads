pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Text.EditorKit;
limited with Javax.Swing.Text.JTextComponent;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicTextUI;
with Javax.Swing.Text.ViewFactory;

package Javax.Swing.Plaf.Basic.BasicEditorPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ViewFactory_I : Javax.Swing.Text.ViewFactory.Ref)
    is new Javax.Swing.Plaf.Basic.BasicTextUI.Typ(ViewFactory_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicEditorPaneUI (This : Ref := null)
                                   return Ref;

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetEditorKit (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class)
                          return access Javax.Swing.Text.EditorKit.Typ'Class;

   --  protected
   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_BasicEditorPaneUI);
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, GetEditorKit, "getEditorKit");
   pragma Import (Java, PropertyChange, "propertyChange");

end Javax.Swing.Plaf.Basic.BasicEditorPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicEditorPaneUI, "javax.swing.plaf.basic.BasicEditorPaneUI");
pragma Extensions_Allowed (Off);
