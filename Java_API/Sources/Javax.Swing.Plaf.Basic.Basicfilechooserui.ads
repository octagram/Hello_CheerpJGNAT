pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseListener;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Io.File;
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.Event.ListSelectionListener;
limited with Javax.Swing.Filechooser.FileFilter;
limited with Javax.Swing.Filechooser.FileView;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JFileChooser;
limited with Javax.Swing.JList;
limited with Javax.Swing.JPanel;
limited with Javax.Swing.Plaf.Basic.BasicDirectoryModel;
with Java.Lang.Object;
with Javax.Swing.Plaf.FileChooserUI;

package Javax.Swing.Plaf.Basic.BasicFileChooserUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.FileChooserUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DirectoryIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, DirectoryIcon, "directoryIcon");

      --  protected
      FileIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, FileIcon, "fileIcon");

      --  protected
      ComputerIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, ComputerIcon, "computerIcon");

      --  protected
      HardDriveIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, HardDriveIcon, "hardDriveIcon");

      --  protected
      FloppyDriveIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, FloppyDriveIcon, "floppyDriveIcon");

      --  protected
      NewFolderIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, NewFolderIcon, "newFolderIcon");

      --  protected
      UpFolderIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, UpFolderIcon, "upFolderIcon");

      --  protected
      HomeFolderIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, HomeFolderIcon, "homeFolderIcon");

      --  protected
      ListViewIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, ListViewIcon, "listViewIcon");

      --  protected
      DetailsViewIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, DetailsViewIcon, "detailsViewIcon");

      --  protected
      SaveButtonMnemonic : Java.Int;
      pragma Import (Java, SaveButtonMnemonic, "saveButtonMnemonic");

      --  protected
      OpenButtonMnemonic : Java.Int;
      pragma Import (Java, OpenButtonMnemonic, "openButtonMnemonic");

      --  protected
      CancelButtonMnemonic : Java.Int;
      pragma Import (Java, CancelButtonMnemonic, "cancelButtonMnemonic");

      --  protected
      UpdateButtonMnemonic : Java.Int;
      pragma Import (Java, UpdateButtonMnemonic, "updateButtonMnemonic");

      --  protected
      HelpButtonMnemonic : Java.Int;
      pragma Import (Java, HelpButtonMnemonic, "helpButtonMnemonic");

      --  protected
      DirectoryOpenButtonMnemonic : Java.Int;
      pragma Import (Java, DirectoryOpenButtonMnemonic, "directoryOpenButtonMnemonic");

      --  protected
      SaveButtonText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, SaveButtonText, "saveButtonText");

      --  protected
      OpenButtonText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, OpenButtonText, "openButtonText");

      --  protected
      CancelButtonText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, CancelButtonText, "cancelButtonText");

      --  protected
      UpdateButtonText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, UpdateButtonText, "updateButtonText");

      --  protected
      HelpButtonText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, HelpButtonText, "helpButtonText");

      --  protected
      DirectoryOpenButtonText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, DirectoryOpenButtonText, "directoryOpenButtonText");

      --  protected
      SaveButtonToolTipText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, SaveButtonToolTipText, "saveButtonToolTipText");

      --  protected
      OpenButtonToolTipText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, OpenButtonToolTipText, "openButtonToolTipText");

      --  protected
      CancelButtonToolTipText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, CancelButtonToolTipText, "cancelButtonToolTipText");

      --  protected
      UpdateButtonToolTipText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, UpdateButtonToolTipText, "updateButtonToolTipText");

      --  protected
      HelpButtonToolTipText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, HelpButtonToolTipText, "helpButtonToolTipText");

      --  protected
      DirectoryOpenButtonToolTipText : access Java.Lang.String.Typ'Class;
      pragma Import (Java, DirectoryOpenButtonToolTipText, "directoryOpenButtonToolTipText");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicFileChooserUI (P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure InstallComponents (This : access Typ;
                                P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   procedure UninstallComponents (This : access Typ;
                                  P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ;
                               P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure UninstallListeners (This : access Typ;
                                 P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure InstallIcons (This : access Typ;
                           P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure InstallStrings (This : access Typ;
                             P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure UninstallIcons (This : access Typ;
                             P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure UninstallStrings (This : access Typ;
                               P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure CreateModel (This : access Typ);

   function GetModel (This : access Typ)
                      return access Javax.Swing.Plaf.Basic.BasicDirectoryModel.Typ'Class;

   function CreatePropertyChangeListener (This : access Typ;
                                          P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   function GetFileName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetDirectoryName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   procedure SetFileName (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetDirectoryName (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RescanCurrentDirectory (This : access Typ;
                                     P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   procedure EnsureFileIsVisible (This : access Typ;
                                  P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class;
                                  P2_File : access Standard.Java.Io.File.Typ'Class);

   function GetFileChooser (This : access Typ)
                            return access Javax.Swing.JFileChooser.Typ'Class;

   function GetAccessoryPanel (This : access Typ)
                               return access Javax.Swing.JPanel.Typ'Class;

   --  protected
   function GetApproveButton (This : access Typ;
                              P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                              return access Javax.Swing.JButton.Typ'Class;

   function GetApproveButtonToolTipText (This : access Typ;
                                         P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                         return access Java.Lang.String.Typ'Class;

   procedure ClearIconCache (This : access Typ);

   --  protected
   function CreateDoubleClickListener (This : access Typ;
                                       P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class;
                                       P2_JList : access Standard.Javax.Swing.JList.Typ'Class)
                                       return access Java.Awt.Event.MouseListener.Typ'Class;

   function CreateListSelectionListener (This : access Typ;
                                         P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                         return access Javax.Swing.Event.ListSelectionListener.Typ'Class;

   --  protected
   function IsDirectorySelected (This : access Typ)
                                 return Java.Boolean;

   --  protected
   procedure SetDirectorySelected (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   --  protected
   function GetDirectory (This : access Typ)
                          return access Java.Io.File.Typ'Class;

   --  protected
   procedure SetDirectory (This : access Typ;
                           P1_File : access Standard.Java.Io.File.Typ'Class);

   function GetAcceptAllFileFilter (This : access Typ;
                                    P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                    return access Javax.Swing.Filechooser.FileFilter.Typ'Class;

   function GetFileView (This : access Typ;
                         P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                         return access Javax.Swing.Filechooser.FileView.Typ'Class;

   function GetDialogTitle (This : access Typ;
                            P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetApproveButtonMnemonic (This : access Typ;
                                      P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                      return Java.Int;

   function GetApproveButtonText (This : access Typ;
                                  P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                  return access Java.Lang.String.Typ'Class;

   function GetNewFolderAction (This : access Typ)
                                return access Javax.Swing.Action.Typ'Class;

   function GetGoHomeAction (This : access Typ)
                             return access Javax.Swing.Action.Typ'Class;

   function GetChangeToParentDirectoryAction (This : access Typ)
                                              return access Javax.Swing.Action.Typ'Class;

   function GetApproveSelectionAction (This : access Typ)
                                       return access Javax.Swing.Action.Typ'Class;

   function GetCancelSelectionAction (This : access Typ)
                                      return access Javax.Swing.Action.Typ'Class;

   function GetUpdateAction (This : access Typ)
                             return access Javax.Swing.Action.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicFileChooserUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallIcons, "installIcons");
   pragma Import (Java, InstallStrings, "installStrings");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallIcons, "uninstallIcons");
   pragma Import (Java, UninstallStrings, "uninstallStrings");
   pragma Import (Java, CreateModel, "createModel");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, GetFileName, "getFileName");
   pragma Import (Java, GetDirectoryName, "getDirectoryName");
   pragma Import (Java, SetFileName, "setFileName");
   pragma Import (Java, SetDirectoryName, "setDirectoryName");
   pragma Import (Java, RescanCurrentDirectory, "rescanCurrentDirectory");
   pragma Import (Java, EnsureFileIsVisible, "ensureFileIsVisible");
   pragma Import (Java, GetFileChooser, "getFileChooser");
   pragma Import (Java, GetAccessoryPanel, "getAccessoryPanel");
   pragma Import (Java, GetApproveButton, "getApproveButton");
   pragma Import (Java, GetApproveButtonToolTipText, "getApproveButtonToolTipText");
   pragma Import (Java, ClearIconCache, "clearIconCache");
   pragma Import (Java, CreateDoubleClickListener, "createDoubleClickListener");
   pragma Import (Java, CreateListSelectionListener, "createListSelectionListener");
   pragma Import (Java, IsDirectorySelected, "isDirectorySelected");
   pragma Import (Java, SetDirectorySelected, "setDirectorySelected");
   pragma Import (Java, GetDirectory, "getDirectory");
   pragma Import (Java, SetDirectory, "setDirectory");
   pragma Import (Java, GetAcceptAllFileFilter, "getAcceptAllFileFilter");
   pragma Import (Java, GetFileView, "getFileView");
   pragma Import (Java, GetDialogTitle, "getDialogTitle");
   pragma Import (Java, GetApproveButtonMnemonic, "getApproveButtonMnemonic");
   pragma Import (Java, GetApproveButtonText, "getApproveButtonText");
   pragma Import (Java, GetNewFolderAction, "getNewFolderAction");
   pragma Import (Java, GetGoHomeAction, "getGoHomeAction");
   pragma Import (Java, GetChangeToParentDirectoryAction, "getChangeToParentDirectoryAction");
   pragma Import (Java, GetApproveSelectionAction, "getApproveSelectionAction");
   pragma Import (Java, GetCancelSelectionAction, "getCancelSelectionAction");
   pragma Import (Java, GetUpdateAction, "getUpdateAction");

end Javax.Swing.Plaf.Basic.BasicFileChooserUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicFileChooserUI, "javax.swing.plaf.basic.BasicFileChooserUI");
pragma Extensions_Allowed (Off);
