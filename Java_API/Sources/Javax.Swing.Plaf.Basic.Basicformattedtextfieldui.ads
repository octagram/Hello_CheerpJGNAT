pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicTextFieldUI;
with Javax.Swing.Text.ViewFactory;

package Javax.Swing.Plaf.Basic.BasicFormattedTextFieldUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ViewFactory_I : Javax.Swing.Text.ViewFactory.Ref)
    is new Javax.Swing.Plaf.Basic.BasicTextFieldUI.Typ(ViewFactory_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicFormattedTextFieldUI (This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicFormattedTextFieldUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");

end Javax.Swing.Plaf.Basic.BasicFormattedTextFieldUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicFormattedTextFieldUI, "javax.swing.plaf.basic.BasicFormattedTextFieldUI");
pragma Extensions_Allowed (Off);
