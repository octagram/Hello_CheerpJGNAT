pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Lang.String;
limited with Javax.Swing.AbstractButton;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicGraphicsUtils is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicGraphicsUtils (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DrawEtchedRect (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Color : access Standard.Java.Awt.Color.Typ'Class;
                             P7_Color : access Standard.Java.Awt.Color.Typ'Class;
                             P8_Color : access Standard.Java.Awt.Color.Typ'Class;
                             P9_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetEtchedInsets return access Java.Awt.Insets.Typ'Class;

   procedure DrawGroove (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Color : access Standard.Java.Awt.Color.Typ'Class;
                         P7_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetGrooveInsets return access Java.Awt.Insets.Typ'Class;

   procedure DrawBezel (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Boolean : Java.Boolean;
                        P7_Boolean : Java.Boolean;
                        P8_Color : access Standard.Java.Awt.Color.Typ'Class;
                        P9_Color : access Standard.Java.Awt.Color.Typ'Class;
                        P10_Color : access Standard.Java.Awt.Color.Typ'Class;
                        P11_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure DrawLoweredBezel (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int;
                               P6_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P7_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P8_Color : access Standard.Java.Awt.Color.Typ'Class;
                               P9_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure DrawString (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int);

   procedure DrawStringUnderlineCharAt (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int);

   procedure DrawDashedRect (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int);

   function GetPreferredButtonSize (P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class;
                                    P2_Int : Java.Int)
                                    return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicGraphicsUtils);
   pragma Import (Java, DrawEtchedRect, "drawEtchedRect");
   pragma Import (Java, GetEtchedInsets, "getEtchedInsets");
   pragma Import (Java, DrawGroove, "drawGroove");
   pragma Import (Java, GetGrooveInsets, "getGrooveInsets");
   pragma Import (Java, DrawBezel, "drawBezel");
   pragma Import (Java, DrawLoweredBezel, "drawLoweredBezel");
   pragma Import (Java, DrawString, "drawString");
   pragma Import (Java, DrawStringUnderlineCharAt, "drawStringUnderlineCharAt");
   pragma Import (Java, DrawDashedRect, "drawDashedRect");
   pragma Import (Java, GetPreferredButtonSize, "getPreferredButtonSize");

end Javax.Swing.Plaf.Basic.BasicGraphicsUtils;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicGraphicsUtils, "javax.swing.plaf.basic.BasicGraphicsUtils");
pragma Extensions_Allowed (Off);
