pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicHTML is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicHTML (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateHTMLView (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Javax.Swing.Text.View.Typ'Class;

   function GetHTMLBaseline (P1_View : access Standard.Javax.Swing.Text.View.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int)
                             return Java.Int;

   function IsHTMLString (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean;

   procedure UpdateRenderer (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PropertyKey : constant access Java.Lang.String.Typ'Class;

   --  final
   DocumentBaseKey : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicHTML);
   pragma Import (Java, CreateHTMLView, "createHTMLView");
   pragma Import (Java, GetHTMLBaseline, "getHTMLBaseline");
   pragma Import (Java, IsHTMLString, "isHTMLString");
   pragma Import (Java, UpdateRenderer, "updateRenderer");
   pragma Import (Java, PropertyKey, "propertyKey");
   pragma Import (Java, DocumentBaseKey, "documentBaseKey");

end Javax.Swing.Plaf.Basic.BasicHTML;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicHTML, "javax.swing.plaf.basic.BasicHTML");
pragma Extensions_Allowed (Off);
