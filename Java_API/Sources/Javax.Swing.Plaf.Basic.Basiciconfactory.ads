pragma Extensions_Allowed (On);
limited with Javax.Swing.Icon;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicIconFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicIconFactory (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMenuItemCheckIcon return access Javax.Swing.Icon.Typ'Class;

   function GetMenuItemArrowIcon return access Javax.Swing.Icon.Typ'Class;

   function GetMenuArrowIcon return access Javax.Swing.Icon.Typ'Class;

   function GetCheckBoxIcon return access Javax.Swing.Icon.Typ'Class;

   function GetRadioButtonIcon return access Javax.Swing.Icon.Typ'Class;

   function GetCheckBoxMenuItemIcon return access Javax.Swing.Icon.Typ'Class;

   function GetRadioButtonMenuItemIcon return access Javax.Swing.Icon.Typ'Class;

   function CreateEmptyFrameIcon return access Javax.Swing.Icon.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicIconFactory);
   pragma Import (Java, GetMenuItemCheckIcon, "getMenuItemCheckIcon");
   pragma Import (Java, GetMenuItemArrowIcon, "getMenuItemArrowIcon");
   pragma Import (Java, GetMenuArrowIcon, "getMenuArrowIcon");
   pragma Import (Java, GetCheckBoxIcon, "getCheckBoxIcon");
   pragma Import (Java, GetRadioButtonIcon, "getRadioButtonIcon");
   pragma Import (Java, GetCheckBoxMenuItemIcon, "getCheckBoxMenuItemIcon");
   pragma Import (Java, GetRadioButtonMenuItemIcon, "getRadioButtonMenuItemIcon");
   pragma Import (Java, CreateEmptyFrameIcon, "createEmptyFrameIcon");

end Javax.Swing.Plaf.Basic.BasicIconFactory;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicIconFactory, "javax.swing.plaf.basic.BasicIconFactory");
pragma Extensions_Allowed (Off);
