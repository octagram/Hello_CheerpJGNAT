pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JMenuBar;
with Javax.Swing.MenuElement;

package Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.SystemMenuBar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            MenuElement_I : Javax.Swing.MenuElement.Ref)
    is new Javax.Swing.JMenuBar.Typ(MenuContainer_I,
                                    ImageObserver_I,
                                    Serializable_I,
                                    Accessible_I,
                                    MenuElement_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SystemMenuBar (P1_BasicInternalFrameTitlePane : access Standard.Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean;

   procedure RequestFocus (This : access Typ);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function IsOpaque (This : access Typ)
                      return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SystemMenuBar);
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, RequestFocus, "requestFocus");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, IsOpaque, "isOpaque");

end Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.SystemMenuBar;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.SystemMenuBar, "javax.swing.plaf.basic.BasicInternalFrameTitlePane$SystemMenuBar");
pragma Extensions_Allowed (Off);
