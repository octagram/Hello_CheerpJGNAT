pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
with Java.Awt.LayoutManager;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.TitlePaneLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TitlePaneLayout (P1_BasicInternalFrameTitlePane : access Standard.Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TitlePaneLayout);
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, LayoutContainer, "layoutContainer");

end Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.TitlePaneLayout;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.TitlePaneLayout, "javax.swing.plaf.basic.BasicInternalFrameTitlePane$TitlePaneLayout");
pragma Extensions_Allowed (Off);
