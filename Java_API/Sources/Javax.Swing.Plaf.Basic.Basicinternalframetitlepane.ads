pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.LayoutManager;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JInternalFrame;
limited with Javax.Swing.JMenu;
limited with Javax.Swing.JMenuBar;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.JComponent;

package Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      MenuBar : access Javax.Swing.JMenuBar.Typ'Class;
      pragma Import (Java, MenuBar, "menuBar");

      --  protected
      IconButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, IconButton, "iconButton");

      --  protected
      MaxButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, MaxButton, "maxButton");

      --  protected
      CloseButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, CloseButton, "closeButton");

      --  protected
      WindowMenu : access Javax.Swing.JMenu.Typ'Class;
      pragma Import (Java, WindowMenu, "windowMenu");

      --  protected
      Frame : access Javax.Swing.JInternalFrame.Typ'Class;
      pragma Import (Java, Frame, "frame");

      --  protected
      SelectedTitleColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectedTitleColor, "selectedTitleColor");

      --  protected
      SelectedTextColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectedTextColor, "selectedTextColor");

      --  protected
      NotSelectedTitleColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, NotSelectedTitleColor, "notSelectedTitleColor");

      --  protected
      NotSelectedTextColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, NotSelectedTextColor, "notSelectedTextColor");

      --  protected
      MaxIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, MaxIcon, "maxIcon");

      --  protected
      MinIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, MinIcon, "minIcon");

      --  protected
      IconIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, IconIcon, "iconIcon");

      --  protected
      CloseIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, CloseIcon, "closeIcon");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      CloseAction : access Javax.Swing.Action.Typ'Class;
      pragma Import (Java, CloseAction, "closeAction");

      --  protected
      MaximizeAction : access Javax.Swing.Action.Typ'Class;
      pragma Import (Java, MaximizeAction, "maximizeAction");

      --  protected
      IconifyAction : access Javax.Swing.Action.Typ'Class;
      pragma Import (Java, IconifyAction, "iconifyAction");

      --  protected
      RestoreAction : access Javax.Swing.Action.Typ'Class;
      pragma Import (Java, RestoreAction, "restoreAction");

      --  protected
      MoveAction : access Javax.Swing.Action.Typ'Class;
      pragma Import (Java, MoveAction, "moveAction");

      --  protected
      SizeAction : access Javax.Swing.Action.Typ'Class;
      pragma Import (Java, SizeAction, "sizeAction");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicInternalFrameTitlePane (P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure InstallTitlePane (This : access Typ);

   --  protected
   procedure AddSubComponents (This : access Typ);

   --  protected
   procedure CreateActions (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure CreateButtons (This : access Typ);

   --  protected
   procedure SetButtonIcons (This : access Typ);

   --  protected
   procedure AssembleSystemMenu (This : access Typ);

   --  protected
   procedure AddSystemMenuItems (This : access Typ;
                                 P1_JMenu : access Standard.Javax.Swing.JMenu.Typ'Class);

   --  protected
   function CreateSystemMenu (This : access Typ)
                              return access Javax.Swing.JMenu.Typ'Class;

   --  protected
   function CreateSystemMenuBar (This : access Typ)
                                 return access Javax.Swing.JMenuBar.Typ'Class;

   --  protected
   procedure ShowSystemMenu (This : access Typ);

   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PaintTitleBackground (This : access Typ;
                                   P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   function GetTitle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                      P3_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   procedure PostClosingEvent (This : access Typ;
                               P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   procedure EnableActions (This : access Typ);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateLayout (This : access Typ)
                          return access Java.Awt.LayoutManager.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   CLOSE_CMD : access Java.Lang.String.Typ'Class;

   --  protected  final
   ICONIFY_CMD : access Java.Lang.String.Typ'Class;

   --  protected  final
   RESTORE_CMD : access Java.Lang.String.Typ'Class;

   --  protected  final
   MAXIMIZE_CMD : access Java.Lang.String.Typ'Class;

   --  protected  final
   MOVE_CMD : access Java.Lang.String.Typ'Class;

   --  protected  final
   SIZE_CMD : access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicInternalFrameTitlePane);
   pragma Import (Java, InstallTitlePane, "installTitlePane");
   pragma Import (Java, AddSubComponents, "addSubComponents");
   pragma Import (Java, CreateActions, "createActions");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, CreateButtons, "createButtons");
   pragma Import (Java, SetButtonIcons, "setButtonIcons");
   pragma Import (Java, AssembleSystemMenu, "assembleSystemMenu");
   pragma Import (Java, AddSystemMenuItems, "addSystemMenuItems");
   pragma Import (Java, CreateSystemMenu, "createSystemMenu");
   pragma Import (Java, CreateSystemMenuBar, "createSystemMenuBar");
   pragma Import (Java, ShowSystemMenu, "showSystemMenu");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, PaintTitleBackground, "paintTitleBackground");
   pragma Import (Java, GetTitle, "getTitle");
   pragma Import (Java, PostClosingEvent, "postClosingEvent");
   pragma Import (Java, EnableActions, "enableActions");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreateLayout, "createLayout");
   pragma Import (Java, CLOSE_CMD, "CLOSE_CMD");
   pragma Import (Java, ICONIFY_CMD, "ICONIFY_CMD");
   pragma Import (Java, RESTORE_CMD, "RESTORE_CMD");
   pragma Import (Java, MAXIMIZE_CMD, "MAXIMIZE_CMD");
   pragma Import (Java, MOVE_CMD, "MOVE_CMD");
   pragma Import (Java, SIZE_CMD, "SIZE_CMD");

end Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane, "javax.swing.plaf.basic.BasicInternalFrameTitlePane");
pragma Extensions_Allowed (Off);
