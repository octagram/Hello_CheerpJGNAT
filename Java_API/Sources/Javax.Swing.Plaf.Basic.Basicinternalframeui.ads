pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ComponentListener;
limited with Java.Awt.LayoutManager;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.DesktopManager;
limited with Javax.Swing.Event.MouseInputAdapter;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JInternalFrame;
limited with Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.InternalFrameUI;

package Javax.Swing.Plaf.Basic.BasicInternalFrameUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.InternalFrameUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Frame : access Javax.Swing.JInternalFrame.Typ'Class;
      pragma Import (Java, Frame, "frame");

      --  protected
      BorderListener : access Javax.Swing.Event.MouseInputAdapter.Typ'Class;
      pragma Import (Java, BorderListener, "borderListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      InternalFrameLayout : access Java.Awt.LayoutManager.Typ'Class;
      pragma Import (Java, InternalFrameLayout, "internalFrameLayout");

      --  protected
      ComponentListener : access Java.Awt.Event.ComponentListener.Typ'Class;
      pragma Import (Java, ComponentListener, "componentListener");

      --  protected
      GlassPaneDispatcher : access Javax.Swing.Event.MouseInputListener.Typ'Class;
      pragma Import (Java, GlassPaneDispatcher, "glassPaneDispatcher");

      --  protected
      NorthPane : access Javax.Swing.JComponent.Typ'Class;
      pragma Import (Java, NorthPane, "northPane");

      --  protected
      SouthPane : access Javax.Swing.JComponent.Typ'Class;
      pragma Import (Java, SouthPane, "southPane");

      --  protected
      WestPane : access Javax.Swing.JComponent.Typ'Class;
      pragma Import (Java, WestPane, "westPane");

      --  protected
      EastPane : access Javax.Swing.JComponent.Typ'Class;
      pragma Import (Java, EastPane, "eastPane");

      --  protected
      TitlePane : access Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.Typ'Class;
      pragma Import (Java, TitlePane, "titlePane");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicInternalFrameUI (P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   function CreateLayoutManager (This : access Typ)
                                 return access Java.Awt.LayoutManager.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure ReplacePane (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                          P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure DeinstallMouseHandlers (This : access Typ;
                                     P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallMouseHandlers (This : access Typ;
                                   P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   function CreateNorthPane (This : access Typ;
                             P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                             return access Javax.Swing.JComponent.Typ'Class;

   --  protected
   function CreateSouthPane (This : access Typ;
                             P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                             return access Javax.Swing.JComponent.Typ'Class;

   --  protected
   function CreateWestPane (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                            return access Javax.Swing.JComponent.Typ'Class;

   --  protected
   function CreateEastPane (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                            return access Javax.Swing.JComponent.Typ'Class;

   --  protected
   function CreateBorderListener (This : access Typ;
                                  P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                                  return access Javax.Swing.Event.MouseInputAdapter.Typ'Class;

   --  protected
   procedure CreateInternalFrameListener (This : access Typ);

   --  final  protected
   function IsKeyBindingRegistered (This : access Typ)
                                    return Java.Boolean;

   --  final  protected
   procedure SetKeyBindingRegistered (This : access Typ;
                                      P1_Boolean : Java.Boolean);

   --  final
   function IsKeyBindingActive (This : access Typ)
                                return Java.Boolean;

   --  final  protected
   procedure SetKeyBindingActive (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   --  protected
   procedure SetupMenuOpenKey (This : access Typ);

   --  protected
   procedure SetupMenuCloseKey (This : access Typ);

   function GetNorthPane (This : access Typ)
                          return access Javax.Swing.JComponent.Typ'Class;

   procedure SetNorthPane (This : access Typ;
                           P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetSouthPane (This : access Typ)
                          return access Javax.Swing.JComponent.Typ'Class;

   procedure SetSouthPane (This : access Typ;
                           P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetWestPane (This : access Typ)
                         return access Javax.Swing.JComponent.Typ'Class;

   procedure SetWestPane (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetEastPane (This : access Typ)
                         return access Javax.Swing.JComponent.Typ'Class;

   procedure SetEastPane (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   function GetDesktopManager (This : access Typ)
                               return access Javax.Swing.DesktopManager.Typ'Class;

   --  protected
   function CreateDesktopManager (This : access Typ)
                                  return access Javax.Swing.DesktopManager.Typ'Class;

   --  protected
   procedure CloseFrame (This : access Typ;
                         P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   procedure MaximizeFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   procedure MinimizeFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   procedure IconifyFrame (This : access Typ;
                           P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   procedure DeiconifyFrame (This : access Typ;
                             P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   procedure ActivateFrame (This : access Typ;
                            P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   procedure DeactivateFrame (This : access Typ;
                              P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class);

   --  protected
   function CreateComponentListener (This : access Typ)
                                     return access Java.Awt.Event.ComponentListener.Typ'Class;

   --  protected
   function CreateGlassPaneDispatcher (This : access Typ)
                                       return access Javax.Swing.Event.MouseInputListener.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_BasicInternalFrameUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, CreateLayoutManager, "createLayoutManager");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, ReplacePane, "replacePane");
   pragma Import (Java, DeinstallMouseHandlers, "deinstallMouseHandlers");
   pragma Import (Java, InstallMouseHandlers, "installMouseHandlers");
   pragma Import (Java, CreateNorthPane, "createNorthPane");
   pragma Import (Java, CreateSouthPane, "createSouthPane");
   pragma Import (Java, CreateWestPane, "createWestPane");
   pragma Import (Java, CreateEastPane, "createEastPane");
   pragma Import (Java, CreateBorderListener, "createBorderListener");
   pragma Import (Java, CreateInternalFrameListener, "createInternalFrameListener");
   pragma Import (Java, IsKeyBindingRegistered, "isKeyBindingRegistered");
   pragma Import (Java, SetKeyBindingRegistered, "setKeyBindingRegistered");
   pragma Import (Java, IsKeyBindingActive, "isKeyBindingActive");
   pragma Import (Java, SetKeyBindingActive, "setKeyBindingActive");
   pragma Import (Java, SetupMenuOpenKey, "setupMenuOpenKey");
   pragma Import (Java, SetupMenuCloseKey, "setupMenuCloseKey");
   pragma Import (Java, GetNorthPane, "getNorthPane");
   pragma Import (Java, SetNorthPane, "setNorthPane");
   pragma Import (Java, GetSouthPane, "getSouthPane");
   pragma Import (Java, SetSouthPane, "setSouthPane");
   pragma Import (Java, GetWestPane, "getWestPane");
   pragma Import (Java, SetWestPane, "setWestPane");
   pragma Import (Java, GetEastPane, "getEastPane");
   pragma Import (Java, SetEastPane, "setEastPane");
   pragma Import (Java, GetDesktopManager, "getDesktopManager");
   pragma Import (Java, CreateDesktopManager, "createDesktopManager");
   pragma Import (Java, CloseFrame, "closeFrame");
   pragma Import (Java, MaximizeFrame, "maximizeFrame");
   pragma Import (Java, MinimizeFrame, "minimizeFrame");
   pragma Import (Java, IconifyFrame, "iconifyFrame");
   pragma Import (Java, DeiconifyFrame, "deiconifyFrame");
   pragma Import (Java, ActivateFrame, "activateFrame");
   pragma Import (Java, DeactivateFrame, "deactivateFrame");
   pragma Import (Java, CreateComponentListener, "createComponentListener");
   pragma Import (Java, CreateGlassPaneDispatcher, "createGlassPaneDispatcher");

end Javax.Swing.Plaf.Basic.BasicInternalFrameUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicInternalFrameUI, "javax.swing.plaf.basic.BasicInternalFrameUI");
pragma Extensions_Allowed (Off);
