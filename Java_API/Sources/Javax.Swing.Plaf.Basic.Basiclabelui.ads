pragma Extensions_Allowed (On);
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JLabel;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;
with Javax.Swing.Plaf.LabelUI;

package Javax.Swing.Plaf.Basic.BasicLabelUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref)
    is new Javax.Swing.Plaf.LabelUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicLabelUI (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function LayoutCL (This : access Typ;
                      P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class;
                      P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                      P3_String : access Standard.Java.Lang.String.Typ'Class;
                      P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                      P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                      P6_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                      P7_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   procedure PaintEnabledText (This : access Typ;
                               P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class;
                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int);

   --  protected
   procedure PaintDisabledText (This : access Typ;
                                P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class;
                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ;
                               P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   --  protected
   procedure InstallComponents (This : access Typ;
                                P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   --  protected
   procedure InstallKeyboardActions (This : access Typ;
                                     P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   --  protected
   procedure UninstallListeners (This : access Typ;
                                 P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   --  protected
   procedure UninstallComponents (This : access Typ;
                                  P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ;
                                       P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class);

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   LabelUI : access Javax.Swing.Plaf.Basic.BasicLabelUI.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicLabelUI);
   pragma Import (Java, LayoutCL, "layoutCL");
   pragma Import (Java, PaintEnabledText, "paintEnabledText");
   pragma Import (Java, PaintDisabledText, "paintDisabledText");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, LabelUI, "labelUI");

end Javax.Swing.Plaf.Basic.BasicLabelUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicLabelUI, "javax.swing.plaf.basic.BasicLabelUI");
pragma Extensions_Allowed (Off);
