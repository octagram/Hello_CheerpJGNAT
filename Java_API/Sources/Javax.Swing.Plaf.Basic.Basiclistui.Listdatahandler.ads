pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ListDataEvent;
with Java.Lang.Object;
with Javax.Swing.Event.ListDataListener;

package Javax.Swing.Plaf.Basic.BasicListUI.ListDataHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ListDataListener_I : Javax.Swing.Event.ListDataListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ListDataHandler (P1_BasicListUI : access Standard.Javax.Swing.Plaf.Basic.BasicListUI.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure IntervalAdded (This : access Typ;
                            P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);

   procedure IntervalRemoved (This : access Typ;
                              P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);

   procedure ContentsChanged (This : access Typ;
                              P1_ListDataEvent : access Standard.Javax.Swing.Event.ListDataEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ListDataHandler);
   pragma Import (Java, IntervalAdded, "intervalAdded");
   pragma Import (Java, IntervalRemoved, "intervalRemoved");
   pragma Import (Java, ContentsChanged, "contentsChanged");

end Javax.Swing.Plaf.Basic.BasicListUI.ListDataHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicListUI.ListDataHandler, "javax.swing.plaf.basic.BasicListUI$ListDataHandler");
pragma Extensions_Allowed (Off);
