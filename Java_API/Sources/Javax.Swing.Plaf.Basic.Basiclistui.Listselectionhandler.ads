pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ListSelectionEvent;
with Java.Lang.Object;
with Javax.Swing.Event.ListSelectionListener;

package Javax.Swing.Plaf.Basic.BasicListUI.ListSelectionHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ListSelectionListener_I : Javax.Swing.Event.ListSelectionListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ListSelectionHandler (P1_BasicListUI : access Standard.Javax.Swing.Plaf.Basic.BasicListUI.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ValueChanged (This : access Typ;
                           P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ListSelectionHandler);
   pragma Import (Java, ValueChanged, "valueChanged");

end Javax.Swing.Plaf.Basic.BasicListUI.ListSelectionHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicListUI.ListSelectionHandler, "javax.swing.plaf.basic.BasicListUI$ListSelectionHandler");
pragma Extensions_Allowed (Off);
