pragma Extensions_Allowed (On);
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.CellRendererPane;
limited with Javax.Swing.Event.ListDataListener;
limited with Javax.Swing.Event.ListSelectionListener;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JList;
limited with Javax.Swing.ListCellRenderer;
limited with Javax.Swing.ListModel;
limited with Javax.Swing.ListSelectionModel;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.ListUI;

package Javax.Swing.Plaf.Basic.BasicListUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.ListUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      List : access Javax.Swing.JList.Typ'Class;
      pragma Import (Java, List, "list");

      --  protected
      RendererPane : access Javax.Swing.CellRendererPane.Typ'Class;
      pragma Import (Java, RendererPane, "rendererPane");

      --  protected
      FocusListener : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, FocusListener, "focusListener");

      --  protected
      MouseInputListener : access Javax.Swing.Event.MouseInputListener.Typ'Class;
      pragma Import (Java, MouseInputListener, "mouseInputListener");

      --  protected
      ListSelectionListener : access Javax.Swing.Event.ListSelectionListener.Typ'Class;
      pragma Import (Java, ListSelectionListener, "listSelectionListener");

      --  protected
      ListDataListener : access Javax.Swing.Event.ListDataListener.Typ'Class;
      pragma Import (Java, ListDataListener, "listDataListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      CellHeights : Java.Int_Arr;
      pragma Import (Java, CellHeights, "cellHeights");

      --  protected
      CellHeight : Java.Int;
      pragma Import (Java, CellHeight, "cellHeight");

      --  protected
      CellWidth : Java.Int;
      pragma Import (Java, CellWidth, "cellWidth");

      --  protected
      UpdateLayoutStateNeeded : Java.Int;
      pragma Import (Java, UpdateLayoutStateNeeded, "updateLayoutStateNeeded");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicListUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure PaintCell (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P4_ListCellRenderer : access Standard.Javax.Swing.ListCellRenderer.Typ'Class;
                        P5_ListModel : access Standard.Javax.Swing.ListModel.Typ'Class;
                        P6_ListSelectionModel : access Standard.Javax.Swing.ListSelectionModel.Typ'Class;
                        P7_Int : Java.Int);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure SelectPreviousIndex (This : access Typ);

   --  protected
   procedure SelectNextIndex (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   function LocationToIndex (This : access Typ;
                             P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                             P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return Java.Int;

   function IndexToLocation (This : access Typ;
                             P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                             P2_Int : Java.Int)
                             return access Java.Awt.Point.Typ'Class;

   function GetCellBounds (This : access Typ;
                           P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int)
                           return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function GetRowHeight (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;

   --  protected
   function ConvertYToRow (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;

   --  protected
   function ConvertRowToY (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;

   --  protected
   procedure MaybeUpdateLayoutState (This : access Typ);

   --  protected
   procedure UpdateLayoutState (This : access Typ);

   --  protected
   function CreateMouseInputListener (This : access Typ)
                                      return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   --  protected
   function CreateFocusListener (This : access Typ)
                                 return access Java.Awt.Event.FocusListener.Typ'Class;

   --  protected
   function CreateListSelectionListener (This : access Typ)
                                         return access Javax.Swing.Event.ListSelectionListener.Typ'Class;

   --  protected
   function CreateListDataListener (This : access Typ)
                                    return access Javax.Swing.Event.ListDataListener.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   ModelChanged : constant Java.Int;

   --  protected  final
   SelectionModelChanged : constant Java.Int;

   --  protected  final
   FontChanged : constant Java.Int;

   --  protected  final
   FixedCellWidthChanged : constant Java.Int;

   --  protected  final
   FixedCellHeightChanged : constant Java.Int;

   --  protected  final
   PrototypeCellValueChanged : constant Java.Int;

   --  protected  final
   CellRendererChanged : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicListUI);
   pragma Import (Java, PaintCell, "paintCell");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, SelectPreviousIndex, "selectPreviousIndex");
   pragma Import (Java, SelectNextIndex, "selectNextIndex");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, LocationToIndex, "locationToIndex");
   pragma Import (Java, IndexToLocation, "indexToLocation");
   pragma Import (Java, GetCellBounds, "getCellBounds");
   pragma Import (Java, GetRowHeight, "getRowHeight");
   pragma Import (Java, ConvertYToRow, "convertYToRow");
   pragma Import (Java, ConvertRowToY, "convertRowToY");
   pragma Import (Java, MaybeUpdateLayoutState, "maybeUpdateLayoutState");
   pragma Import (Java, UpdateLayoutState, "updateLayoutState");
   pragma Import (Java, CreateMouseInputListener, "createMouseInputListener");
   pragma Import (Java, CreateFocusListener, "createFocusListener");
   pragma Import (Java, CreateListSelectionListener, "createListSelectionListener");
   pragma Import (Java, CreateListDataListener, "createListDataListener");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, ModelChanged, "modelChanged");
   pragma Import (Java, SelectionModelChanged, "selectionModelChanged");
   pragma Import (Java, FontChanged, "fontChanged");
   pragma Import (Java, FixedCellWidthChanged, "fixedCellWidthChanged");
   pragma Import (Java, FixedCellHeightChanged, "fixedCellHeightChanged");
   pragma Import (Java, PrototypeCellValueChanged, "prototypeCellValueChanged");
   pragma Import (Java, CellRendererChanged, "cellRendererChanged");

end Javax.Swing.Plaf.Basic.BasicListUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicListUI, "javax.swing.plaf.basic.BasicListUI");
pragma Extensions_Allowed (Off);
