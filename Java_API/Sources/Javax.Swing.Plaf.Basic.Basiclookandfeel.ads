pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.ActionMap;
limited with Javax.Swing.UIDefaults;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.LookAndFeel;

package Javax.Swing.Plaf.Basic.BasicLookAndFeel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Javax.Swing.LookAndFeel.Typ
      with null record;

   function New_BasicLookAndFeel (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaults (This : access Typ)
                         return access Javax.Swing.UIDefaults.Typ'Class;

   procedure Initialize (This : access Typ);

   procedure Uninitialize (This : access Typ);

   --  protected
   procedure InitClassDefaults (This : access Typ;
                                P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);

   --  protected
   procedure InitSystemColorDefaults (This : access Typ;
                                      P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);

   --  protected
   procedure LoadSystemColors (This : access Typ;
                               P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class;
                               P2_String_Arr : access Java.Lang.String.Arr_Obj;
                               P3_Boolean : Java.Boolean);

   --  protected
   procedure InitComponentDefaults (This : access Typ;
                                    P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);

   --  protected
   function GetAudioActionMap (This : access Typ)
                               return access Javax.Swing.ActionMap.Typ'Class;

   --  protected
   function CreateAudioAction (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Javax.Swing.Action.Typ'Class;

   --  protected
   procedure PlaySound (This : access Typ;
                        P1_Action : access Standard.Javax.Swing.Action.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicLookAndFeel);
   pragma Import (Java, GetDefaults, "getDefaults");
   pragma Import (Java, Initialize, "initialize");
   pragma Import (Java, Uninitialize, "uninitialize");
   pragma Import (Java, InitClassDefaults, "initClassDefaults");
   pragma Import (Java, InitSystemColorDefaults, "initSystemColorDefaults");
   pragma Import (Java, LoadSystemColors, "loadSystemColors");
   pragma Import (Java, InitComponentDefaults, "initComponentDefaults");
   pragma Import (Java, GetAudioActionMap, "getAudioActionMap");
   pragma Import (Java, CreateAudioAction, "createAudioAction");
   pragma Import (Java, PlaySound, "playSound");

end Javax.Swing.Plaf.Basic.BasicLookAndFeel;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicLookAndFeel, "javax.swing.plaf.basic.BasicLookAndFeel");
pragma Extensions_Allowed (Off);
