pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ContainerListener;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JMenuBar;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.MenuBarUI;

package Javax.Swing.Plaf.Basic.BasicMenuBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.MenuBarUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      MenuBar : access Javax.Swing.JMenuBar.Typ'Class;
      pragma Import (Java, MenuBar, "menuBar");

      --  protected
      ContainerListener : access Java.Awt.Event.ContainerListener.Typ'Class;
      pragma Import (Java, ContainerListener, "containerListener");

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicMenuBarUI (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   function CreateContainerListener (This : access Typ)
                                     return access Java.Awt.Event.ContainerListener.Typ'Class;

   --  protected
   function CreateChangeListener (This : access Typ)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicMenuBarUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, CreateContainerListener, "createContainerListener");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");

end Javax.Swing.Plaf.Basic.BasicMenuBarUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicMenuBarUI, "javax.swing.plaf.basic.BasicMenuBarUI");
pragma Extensions_Allowed (Off);
