pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Event.MenuDragMouseListener;
limited with Javax.Swing.Event.MenuKeyListener;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JMenuItem;
limited with Javax.Swing.MenuElement;
limited with Javax.Swing.MenuSelectionManager;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.MenuItemUI;

package Javax.Swing.Plaf.Basic.BasicMenuItemUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.MenuItemUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      MenuItem : access Javax.Swing.JMenuItem.Typ'Class;
      pragma Import (Java, MenuItem, "menuItem");

      --  protected
      SelectionBackground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectionBackground, "selectionBackground");

      --  protected
      SelectionForeground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectionForeground, "selectionForeground");

      --  protected
      DisabledForeground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, DisabledForeground, "disabledForeground");

      --  protected
      AcceleratorForeground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, AcceleratorForeground, "acceleratorForeground");

      --  protected
      AcceleratorSelectionForeground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, AcceleratorSelectionForeground, "acceleratorSelectionForeground");

      --  protected
      DefaultTextIconGap : Java.Int;
      pragma Import (Java, DefaultTextIconGap, "defaultTextIconGap");

      --  protected
      AcceleratorFont : access Java.Awt.Font.Typ'Class;
      pragma Import (Java, AcceleratorFont, "acceleratorFont");

      --  protected
      MouseInputListener : access Javax.Swing.Event.MouseInputListener.Typ'Class;
      pragma Import (Java, MouseInputListener, "mouseInputListener");

      --  protected
      MenuDragMouseListener : access Javax.Swing.Event.MenuDragMouseListener.Typ'Class;
      pragma Import (Java, MenuDragMouseListener, "menuDragMouseListener");

      --  protected
      MenuKeyListener : access Javax.Swing.Event.MenuKeyListener.Typ'Class;
      pragma Import (Java, MenuKeyListener, "menuKeyListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      ArrowIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, ArrowIcon, "arrowIcon");

      --  protected
      CheckIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, CheckIcon, "checkIcon");

      --  protected
      OldBorderPainted : Java.Boolean;
      pragma Import (Java, OldBorderPainted, "oldBorderPainted");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicMenuItemUI (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallComponents (This : access Typ;
                                P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class);

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ;
                                  P1_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   function CreateMouseInputListener (This : access Typ;
                                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                      return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   --  protected
   function CreateMenuDragMouseListener (This : access Typ;
                                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                         return access Javax.Swing.Event.MenuDragMouseListener.Typ'Class;

   --  protected
   function CreateMenuKeyListener (This : access Typ;
                                   P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                   return access Javax.Swing.Event.MenuKeyListener.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ;
                                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetPreferredMenuItemSize (This : access Typ;
                                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                      P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                      P3_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                      P4_Int : Java.Int)
                                      return access Java.Awt.Dimension.Typ'Class;

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintMenuItem (This : access Typ;
                            P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                            P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                            P3_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                            P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                            P5_Color : access Standard.Java.Awt.Color.Typ'Class;
                            P6_Color : access Standard.Java.Awt.Color.Typ'Class;
                            P7_Int : Java.Int);

   --  protected
   procedure PaintBackground (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                              P2_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class;
                              P3_Color : access Standard.Java.Awt.Color.Typ'Class);

   --  protected
   procedure PaintText (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_JMenuItem : access Standard.Javax.Swing.JMenuItem.Typ'Class;
                        P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P4_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPath (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure DoClick (This : access Typ;
                      P1_MenuSelectionManager : access Standard.Javax.Swing.MenuSelectionManager.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicMenuItemUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, CreateMouseInputListener, "createMouseInputListener");
   pragma Import (Java, CreateMenuDragMouseListener, "createMenuDragMouseListener");
   pragma Import (Java, CreateMenuKeyListener, "createMenuKeyListener");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetPreferredMenuItemSize, "getPreferredMenuItemSize");
   pragma Import (Java, Update, "update");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintMenuItem, "paintMenuItem");
   pragma Import (Java, PaintBackground, "paintBackground");
   pragma Import (Java, PaintText, "paintText");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, DoClick, "doClick");

end Javax.Swing.Plaf.Basic.BasicMenuItemUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicMenuItemUI, "javax.swing.plaf.basic.BasicMenuItemUI");
pragma Extensions_Allowed (Off);
