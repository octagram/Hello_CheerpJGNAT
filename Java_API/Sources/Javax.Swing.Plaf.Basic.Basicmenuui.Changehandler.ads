pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.JMenu;
with Java.Lang.Object;
with Javax.Swing.Event.ChangeListener;

package Javax.Swing.Plaf.Basic.BasicMenuUI.ChangeHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ChangeListener_I : Javax.Swing.Event.ChangeListener.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Menu : access Javax.Swing.JMenu.Typ'Class;
      pragma Import (Java, Menu, "menu");

      Ui : access Javax.Swing.Plaf.Basic.BasicMenuUI.Typ'Class;
      pragma Import (Java, Ui, "ui");

      IsSelected : Java.Boolean;
      pragma Import (Java, IsSelected, "isSelected");

      WasFocused : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, WasFocused, "wasFocused");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChangeHandler (P1_BasicMenuUI : access Standard.Javax.Swing.Plaf.Basic.BasicMenuUI.Typ'Class;
                               P2_JMenu : access Standard.Javax.Swing.JMenu.Typ'Class;
                               P3_BasicMenuUI : access Standard.Javax.Swing.Plaf.Basic.BasicMenuUI.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure StateChanged (This : access Typ;
                           P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ChangeHandler);
   pragma Import (Java, StateChanged, "stateChanged");

end Javax.Swing.Plaf.Basic.BasicMenuUI.ChangeHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicMenuUI.ChangeHandler, "javax.swing.plaf.basic.BasicMenuUI$ChangeHandler");
pragma Extensions_Allowed (Off);
