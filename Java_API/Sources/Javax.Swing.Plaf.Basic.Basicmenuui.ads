pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.MenuDragMouseListener;
limited with Javax.Swing.Event.MenuKeyListener;
limited with Javax.Swing.Event.MenuListener;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JMenu;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicMenuItemUI;

package Javax.Swing.Plaf.Basic.BasicMenuUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicMenuItemUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

      --  protected
      MenuListener : access Javax.Swing.Event.MenuListener.Typ'Class;
      pragma Import (Java, MenuListener, "menuListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicMenuUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   function CreateMouseInputListener (This : access Typ;
                                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                      return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   --  protected
   function CreateMenuListener (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                return access Javax.Swing.Event.MenuListener.Typ'Class;

   --  protected
   function CreateChangeListener (This : access Typ;
                                  P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ;
                                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   function CreateMenuDragMouseListener (This : access Typ;
                                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                         return access Javax.Swing.Event.MenuDragMouseListener.Typ'Class;

   --  protected
   function CreateMenuKeyListener (This : access Typ;
                                   P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                   return access Javax.Swing.Event.MenuKeyListener.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure SetupPostTimer (This : access Typ;
                             P1_JMenu : access Standard.Javax.Swing.JMenu.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicMenuUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, CreateMouseInputListener, "createMouseInputListener");
   pragma Import (Java, CreateMenuListener, "createMenuListener");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, CreateMenuDragMouseListener, "createMenuDragMouseListener");
   pragma Import (Java, CreateMenuKeyListener, "createMenuKeyListener");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, SetupPostTimer, "setupPostTimer");

end Javax.Swing.Plaf.Basic.BasicMenuUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicMenuUI, "javax.swing.plaf.basic.BasicMenuUI");
pragma Extensions_Allowed (Off);
