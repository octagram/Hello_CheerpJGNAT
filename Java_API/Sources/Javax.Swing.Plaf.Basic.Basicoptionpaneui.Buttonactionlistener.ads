pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
with Java.Awt.Event.ActionListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicOptionPaneUI.ButtonActionListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ActionListener_I : Java.Awt.Event.ActionListener.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ButtonIndex : Java.Int;
      pragma Import (Java, ButtonIndex, "buttonIndex");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ButtonActionListener (P1_BasicOptionPaneUI : access Standard.Javax.Swing.Plaf.Basic.BasicOptionPaneUI.Typ'Class;
                                      P2_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ButtonActionListener);
   pragma Import (Java, ActionPerformed, "actionPerformed");

end Javax.Swing.Plaf.Basic.BasicOptionPaneUI.ButtonActionListener;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicOptionPaneUI.ButtonActionListener, "javax.swing.plaf.basic.BasicOptionPaneUI$ButtonActionListener");
pragma Extensions_Allowed (Off);
