pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
with Java.Awt.LayoutManager;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicOptionPaneUI.ButtonAreaLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      SyncAllWidths : Java.Boolean;
      pragma Import (Java, SyncAllWidths, "syncAllWidths");

      --  protected
      Padding : Java.Int;
      pragma Import (Java, Padding, "padding");

      --  protected
      CentersChildren : Java.Boolean;
      pragma Import (Java, CentersChildren, "centersChildren");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ButtonAreaLayout (P1_Boolean : Java.Boolean;
                                  P2_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSyncAllWidths (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function GetSyncAllWidths (This : access Typ)
                              return Java.Boolean;

   procedure SetPadding (This : access Typ;
                         P1_Int : Java.Int);

   function GetPadding (This : access Typ)
                        return Java.Int;

   procedure SetCentersChildren (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   function GetCentersChildren (This : access Typ)
                                return Java.Boolean;

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ButtonAreaLayout);
   pragma Import (Java, SetSyncAllWidths, "setSyncAllWidths");
   pragma Import (Java, GetSyncAllWidths, "getSyncAllWidths");
   pragma Import (Java, SetPadding, "setPadding");
   pragma Import (Java, GetPadding, "getPadding");
   pragma Import (Java, SetCentersChildren, "setCentersChildren");
   pragma Import (Java, GetCentersChildren, "getCentersChildren");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");

end Javax.Swing.Plaf.Basic.BasicOptionPaneUI.ButtonAreaLayout;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicOptionPaneUI.ButtonAreaLayout, "javax.swing.plaf.basic.BasicOptionPaneUI$ButtonAreaLayout");
pragma Extensions_Allowed (Off);
