pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ActionListener;
limited with Java.Awt.GridBagConstraints;
limited with Java.Awt.LayoutManager;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JOptionPane;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.OptionPaneUI;

package Javax.Swing.Plaf.Basic.BasicOptionPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.OptionPaneUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      OptionPane : access Javax.Swing.JOptionPane.Typ'Class;
      pragma Import (Java, OptionPane, "optionPane");

      --  protected
      MinimumSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, MinimumSize, "minimumSize");

      --  protected
      InputComponent : access Javax.Swing.JComponent.Typ'Class;
      pragma Import (Java, InputComponent, "inputComponent");

      --  protected
      InitialFocusComponent : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, InitialFocusComponent, "initialFocusComponent");

      --  protected
      HasCustomComponents : Java.Boolean;
      pragma Import (Java, HasCustomComponents, "hasCustomComponents");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicOptionPaneUI (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   function CreateLayoutManager (This : access Typ)
                                 return access Java.Awt.LayoutManager.Typ'Class;

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   function GetMinimumOptionPaneSize (This : access Typ)
                                      return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function CreateMessageArea (This : access Typ)
                               return access Java.Awt.Container.Typ'Class;

   --  protected
   procedure AddMessageComponents (This : access Typ;
                                   P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                                   P2_GridBagConstraints : access Standard.Java.Awt.GridBagConstraints.Typ'Class;
                                   P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                                   P4_Int : Java.Int;
                                   P5_Boolean : Java.Boolean);

   --  protected
   function GetMessage (This : access Typ)
                        return access Java.Lang.Object.Typ'Class;

   --  protected
   procedure AddIcon (This : access Typ;
                      P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   --  protected
   function GetIcon (This : access Typ)
                     return access Javax.Swing.Icon.Typ'Class;

   --  protected
   function GetIconForType (This : access Typ;
                            P1_Int : Java.Int)
                            return access Javax.Swing.Icon.Typ'Class;

   --  protected
   function GetMaxCharactersPerLineCount (This : access Typ)
                                          return Java.Int;

   --  protected
   procedure BurstStringInto (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_Int : Java.Int);

   --  protected
   function CreateSeparator (This : access Typ)
                             return access Java.Awt.Container.Typ'Class;

   --  protected
   function CreateButtonArea (This : access Typ)
                              return access Java.Awt.Container.Typ'Class;

   --  protected
   procedure AddButtonComponents (This : access Typ;
                                  P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                                  P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                  P3_Int : Java.Int);

   --  protected
   function CreateButtonActionListener (This : access Typ;
                                        P1_Int : Java.Int)
                                        return access Java.Awt.Event.ActionListener.Typ'Class;

   --  protected
   function GetButtons (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetSizeButtonsToSameWidth (This : access Typ)
                                       return Java.Boolean;

   --  protected
   function GetInitialValueIndex (This : access Typ)
                                  return Java.Int;

   --  protected
   procedure ResetInputValue (This : access Typ);

   procedure SelectInitialValue (This : access Typ;
                                 P1_JOptionPane : access Standard.Javax.Swing.JOptionPane.Typ'Class);

   function ContainsCustomComponents (This : access Typ;
                                      P1_JOptionPane : access Standard.Javax.Swing.JOptionPane.Typ'Class)
                                      return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MinimumWidth : constant Java.Int;

   --  final
   MinimumHeight : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicOptionPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, CreateLayoutManager, "createLayoutManager");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, GetMinimumOptionPaneSize, "getMinimumOptionPaneSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, CreateMessageArea, "createMessageArea");
   pragma Import (Java, AddMessageComponents, "addMessageComponents");
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, AddIcon, "addIcon");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, GetIconForType, "getIconForType");
   pragma Import (Java, GetMaxCharactersPerLineCount, "getMaxCharactersPerLineCount");
   pragma Import (Java, BurstStringInto, "burstStringInto");
   pragma Import (Java, CreateSeparator, "createSeparator");
   pragma Import (Java, CreateButtonArea, "createButtonArea");
   pragma Import (Java, AddButtonComponents, "addButtonComponents");
   pragma Import (Java, CreateButtonActionListener, "createButtonActionListener");
   pragma Import (Java, GetButtons, "getButtons");
   pragma Import (Java, GetSizeButtonsToSameWidth, "getSizeButtonsToSameWidth");
   pragma Import (Java, GetInitialValueIndex, "getInitialValueIndex");
   pragma Import (Java, ResetInputValue, "resetInputValue");
   pragma Import (Java, SelectInitialValue, "selectInitialValue");
   pragma Import (Java, ContainsCustomComponents, "containsCustomComponents");
   pragma Import (Java, MinimumWidth, "MinimumWidth");
   pragma Import (Java, MinimumHeight, "MinimumHeight");

end Javax.Swing.Plaf.Basic.BasicOptionPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicOptionPaneUI, "javax.swing.plaf.basic.BasicOptionPaneUI");
pragma Extensions_Allowed (Off);
