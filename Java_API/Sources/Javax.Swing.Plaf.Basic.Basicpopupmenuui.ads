pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JPopupMenu;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.PopupMenuUI;

package Javax.Swing.Plaf.Basic.BasicPopupMenuUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.PopupMenuUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      PopupMenu : access Javax.Swing.JPopupMenu.Typ'Class;
      pragma Import (Java, PopupMenu, "popupMenu");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicPopupMenuUI (This : Ref := null)
                                  return Ref;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   function IsPopupTrigger (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_BasicPopupMenuUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, IsPopupTrigger, "isPopupTrigger");

end Javax.Swing.Plaf.Basic.BasicPopupMenuUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicPopupMenuUI, "javax.swing.plaf.basic.BasicPopupMenuUI");
pragma Extensions_Allowed (Off);
