pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ChangeEvent;
with Java.Lang.Object;
with Javax.Swing.Event.ChangeListener;

package Javax.Swing.Plaf.Basic.BasicProgressBarUI.ChangeHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ChangeListener_I : Javax.Swing.Event.ChangeListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChangeHandler (P1_BasicProgressBarUI : access Standard.Javax.Swing.Plaf.Basic.BasicProgressBarUI.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure StateChanged (This : access Typ;
                           P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ChangeHandler);
   pragma Import (Java, StateChanged, "stateChanged");

end Javax.Swing.Plaf.Basic.BasicProgressBarUI.ChangeHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicProgressBarUI.ChangeHandler, "javax.swing.plaf.basic.BasicProgressBarUI$ChangeHandler");
pragma Extensions_Allowed (Off);
