pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JProgressBar;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.ProgressBarUI;

package Javax.Swing.Plaf.Basic.BasicProgressBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.ProgressBarUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ProgressBar : access Javax.Swing.JProgressBar.Typ'Class;
      pragma Import (Java, ProgressBar, "progressBar");

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

      --  protected
      BoxRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, BoxRect, "boxRect");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicProgressBarUI (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure StartAnimationTimer (This : access Typ);

   --  protected
   procedure StopAnimationTimer (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  protected
   function GetPreferredInnerHorizontal (This : access Typ)
                                         return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetPreferredInnerVertical (This : access Typ)
                                       return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetSelectionForeground (This : access Typ)
                                    return access Java.Awt.Color.Typ'Class;

   --  protected
   function GetSelectionBackground (This : access Typ)
                                    return access Java.Awt.Color.Typ'Class;

   --  protected
   function GetCellLength (This : access Typ)
                           return Java.Int;

   --  protected
   procedure SetCellLength (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   function GetCellSpacing (This : access Typ)
                            return Java.Int;

   --  protected
   procedure SetCellSpacing (This : access Typ;
                             P1_Int : Java.Int);

   --  protected
   function GetAmountFull (This : access Typ;
                           P1_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int)
                           return Java.Int;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   function GetBox (This : access Typ;
                    P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                    return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function GetBoxLength (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return Java.Int;

   --  protected
   procedure PaintIndeterminate (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintDeterminate (This : access Typ;
                               P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintString (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int;
                          P7_Insets : access Standard.Java.Awt.Insets.Typ'Class);

   --  protected
   function GetStringPlacement (This : access Typ;
                                P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int)
                                return access Java.Awt.Point.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetAnimationIndex (This : access Typ)
                               return Java.Int;

   --  final  protected
   function GetFrameCount (This : access Typ)
                           return Java.Int;

   --  protected
   procedure SetAnimationIndex (This : access Typ;
                                P1_Int : Java.Int);

   --  protected
   procedure IncrementAnimationIndex (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicProgressBarUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, StartAnimationTimer, "startAnimationTimer");
   pragma Import (Java, StopAnimationTimer, "stopAnimationTimer");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, GetPreferredInnerHorizontal, "getPreferredInnerHorizontal");
   pragma Import (Java, GetPreferredInnerVertical, "getPreferredInnerVertical");
   pragma Import (Java, GetSelectionForeground, "getSelectionForeground");
   pragma Import (Java, GetSelectionBackground, "getSelectionBackground");
   pragma Import (Java, GetCellLength, "getCellLength");
   pragma Import (Java, SetCellLength, "setCellLength");
   pragma Import (Java, GetCellSpacing, "getCellSpacing");
   pragma Import (Java, SetCellSpacing, "setCellSpacing");
   pragma Import (Java, GetAmountFull, "getAmountFull");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetBox, "getBox");
   pragma Import (Java, GetBoxLength, "getBoxLength");
   pragma Import (Java, PaintIndeterminate, "paintIndeterminate");
   pragma Import (Java, PaintDeterminate, "paintDeterminate");
   pragma Import (Java, PaintString, "paintString");
   pragma Import (Java, GetStringPlacement, "getStringPlacement");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetAnimationIndex, "getAnimationIndex");
   pragma Import (Java, GetFrameCount, "getFrameCount");
   pragma Import (Java, SetAnimationIndex, "setAnimationIndex");
   pragma Import (Java, IncrementAnimationIndex, "incrementAnimationIndex");

end Javax.Swing.Plaf.Basic.BasicProgressBarUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicProgressBarUI, "javax.swing.plaf.basic.BasicProgressBarUI");
pragma Extensions_Allowed (Off);
