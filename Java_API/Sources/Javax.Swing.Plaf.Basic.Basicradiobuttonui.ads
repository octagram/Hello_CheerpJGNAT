pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicToggleButtonUI;

package Javax.Swing.Plaf.Basic.BasicRadioButtonUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicToggleButtonUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Icon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, Icon, "icon");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicRadioButtonUI (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   function GetDefaultIcon (This : access Typ)
                            return access Javax.Swing.Icon.Typ'Class;

   --  synchronized
   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintFocus (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                         P3_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicRadioButtonUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, GetDefaultIcon, "getDefaultIcon");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintFocus, "paintFocus");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");

end Javax.Swing.Plaf.Basic.BasicRadioButtonUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicRadioButtonUI, "javax.swing.plaf.basic.BasicRadioButtonUI");
pragma Extensions_Allowed (Off);
