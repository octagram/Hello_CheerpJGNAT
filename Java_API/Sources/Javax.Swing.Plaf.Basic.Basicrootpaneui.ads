pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JRootPane;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;
with Javax.Swing.Plaf.RootPaneUI;

package Javax.Swing.Plaf.Basic.BasicRootPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref)
    is new Javax.Swing.Plaf.RootPaneUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicRootPaneUI (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   --  protected
   procedure InstallComponents (This : access Typ;
                                P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ;
                               P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   --  protected
   procedure InstallKeyboardActions (This : access Typ;
                                     P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   --  protected
   procedure UninstallComponents (This : access Typ;
                                  P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   --  protected
   procedure UninstallListeners (This : access Typ;
                                 P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ;
                                       P1_JRootPane : access Standard.Javax.Swing.JRootPane.Typ'Class);

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicRootPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, PropertyChange, "propertyChange");

end Javax.Swing.Plaf.Basic.BasicRootPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicRootPaneUI, "javax.swing.plaf.basic.BasicRootPaneUI");
pragma Extensions_Allowed (Off);
