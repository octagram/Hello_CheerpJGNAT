pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JScrollBar;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Timer;
with Java.Awt.LayoutManager;
with Java.Lang.Object;
with Javax.Swing.Plaf.ScrollBarUI;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Basic.BasicScrollBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Plaf.ScrollBarUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      MinimumThumbSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, MinimumThumbSize, "minimumThumbSize");

      --  protected
      MaximumThumbSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, MaximumThumbSize, "maximumThumbSize");

      --  protected
      ThumbHighlightColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, ThumbHighlightColor, "thumbHighlightColor");

      --  protected
      ThumbLightShadowColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, ThumbLightShadowColor, "thumbLightShadowColor");

      --  protected
      ThumbDarkShadowColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, ThumbDarkShadowColor, "thumbDarkShadowColor");

      --  protected
      ThumbColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, ThumbColor, "thumbColor");

      --  protected
      TrackColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, TrackColor, "trackColor");

      --  protected
      TrackHighlightColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, TrackHighlightColor, "trackHighlightColor");

      --  protected
      Scrollbar : access Javax.Swing.JScrollBar.Typ'Class;
      pragma Import (Java, Scrollbar, "scrollbar");

      --  protected
      IncrButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, IncrButton, "incrButton");

      --  protected
      DecrButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, DecrButton, "decrButton");

      --  protected
      IsDragging : Java.Boolean;
      pragma Import (Java, IsDragging, "isDragging");

      --  protected
      ThumbRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, ThumbRect, "thumbRect");

      --  protected
      TrackRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, TrackRect, "trackRect");

      --  protected
      TrackHighlight : Java.Int;
      pragma Import (Java, TrackHighlight, "trackHighlight");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      ScrollTimer : access Javax.Swing.Timer.Typ'Class;
      pragma Import (Java, ScrollTimer, "scrollTimer");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicScrollBarUI (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   procedure ConfigureScrollBarColors (This : access Typ);

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure SetThumbRollover (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function IsThumbRollover (This : access Typ)
                             return Java.Boolean;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function CreateDecreaseButton (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Javax.Swing.JButton.Typ'Class;

   --  protected
   function CreateIncreaseButton (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Javax.Swing.JButton.Typ'Class;

   --  protected
   procedure PaintDecreaseHighlight (This : access Typ;
                                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PaintIncreaseHighlight (This : access Typ;
                                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PaintTrack (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure PaintThumb (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function GetMinimumThumbSize (This : access Typ)
                                 return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetMaximumThumbSize (This : access Typ)
                                 return access Java.Awt.Dimension.Typ'Class;

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure LayoutVScrollbar (This : access Typ;
                               P1_JScrollBar : access Standard.Javax.Swing.JScrollBar.Typ'Class);

   --  protected
   procedure LayoutHScrollbar (This : access Typ;
                               P1_JScrollBar : access Standard.Javax.Swing.JScrollBar.Typ'Class);

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   --  protected
   procedure SetThumbBounds (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int);

   --  protected
   function GetThumbBounds (This : access Typ)
                            return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function GetTrackBounds (This : access Typ)
                            return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   procedure ScrollByBlock (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   procedure ScrollByUnit (This : access Typ;
                           P1_Int : Java.Int);

   function GetSupportsAbsolutePositioning (This : access Typ)
                                            return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   NO_HIGHLIGHT : constant Java.Int;

   --  protected  final
   DECREASE_HIGHLIGHT : constant Java.Int;

   --  protected  final
   INCREASE_HIGHLIGHT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicScrollBarUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, ConfigureScrollBarColors, "configureScrollBarColors");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, SetThumbRollover, "setThumbRollover");
   pragma Import (Java, IsThumbRollover, "isThumbRollover");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, CreateDecreaseButton, "createDecreaseButton");
   pragma Import (Java, CreateIncreaseButton, "createIncreaseButton");
   pragma Import (Java, PaintDecreaseHighlight, "paintDecreaseHighlight");
   pragma Import (Java, PaintIncreaseHighlight, "paintIncreaseHighlight");
   pragma Import (Java, PaintTrack, "paintTrack");
   pragma Import (Java, PaintThumb, "paintThumb");
   pragma Import (Java, GetMinimumThumbSize, "getMinimumThumbSize");
   pragma Import (Java, GetMaximumThumbSize, "getMaximumThumbSize");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, LayoutVScrollbar, "layoutVScrollbar");
   pragma Import (Java, LayoutHScrollbar, "layoutHScrollbar");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, SetThumbBounds, "setThumbBounds");
   pragma Import (Java, GetThumbBounds, "getThumbBounds");
   pragma Import (Java, GetTrackBounds, "getTrackBounds");
   pragma Import (Java, ScrollByBlock, "scrollByBlock");
   pragma Import (Java, ScrollByUnit, "scrollByUnit");
   pragma Import (Java, GetSupportsAbsolutePositioning, "getSupportsAbsolutePositioning");
   pragma Import (Java, NO_HIGHLIGHT, "NO_HIGHLIGHT");
   pragma Import (Java, DECREASE_HIGHLIGHT, "DECREASE_HIGHLIGHT");
   pragma Import (Java, INCREASE_HIGHLIGHT, "INCREASE_HIGHLIGHT");

end Javax.Swing.Plaf.Basic.BasicScrollBarUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicScrollBarUI, "javax.swing.plaf.basic.BasicScrollBarUI");
pragma Extensions_Allowed (Off);
