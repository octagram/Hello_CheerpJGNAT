pragma Extensions_Allowed (On);
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.MouseWheelListener;
limited with Java.Awt.Graphics;
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JScrollPane;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.ScrollPaneUI;
with Javax.Swing.ScrollPaneConstants;

package Javax.Swing.Plaf.Basic.BasicScrollPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ScrollPaneConstants_I : Javax.Swing.ScrollPaneConstants.Ref)
    is new Javax.Swing.Plaf.ScrollPaneUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Scrollpane : access Javax.Swing.JScrollPane.Typ'Class;
      pragma Import (Java, Scrollpane, "scrollpane");

      --  protected
      VsbChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, VsbChangeListener, "vsbChangeListener");

      --  protected
      HsbChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, HsbChangeListener, "hsbChangeListener");

      --  protected
      ViewportChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ViewportChangeListener, "viewportChangeListener");

      --  protected
      SpPropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, SpPropertyChangeListener, "spPropertyChangeListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicScrollPaneUI (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ;
                               P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   --  protected
   procedure InstallKeyboardActions (This : access Typ;
                                     P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   --  protected
   procedure UninstallListeners (This : access Typ;
                                 P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ;
                                       P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure SyncScrollPaneWithViewport (This : access Typ);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  protected
   function CreateViewportChangeListener (This : access Typ)
                                          return access Javax.Swing.Event.ChangeListener.Typ'Class;

   --  protected
   function CreateHSBChangeListener (This : access Typ)
                                     return access Javax.Swing.Event.ChangeListener.Typ'Class;

   --  protected
   function CreateVSBChangeListener (This : access Typ)
                                     return access Javax.Swing.Event.ChangeListener.Typ'Class;

   --  protected
   function CreateMouseWheelListener (This : access Typ)
                                      return access Java.Awt.Event.MouseWheelListener.Typ'Class;

   --  protected
   procedure UpdateScrollBarDisplayPolicy (This : access Typ;
                                           P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  protected
   procedure UpdateViewport (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  protected
   procedure UpdateRowHeader (This : access Typ;
                              P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  protected
   procedure UpdateColumnHeader (This : access Typ;
                                 P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicScrollPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, SyncScrollPaneWithViewport, "syncScrollPaneWithViewport");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, CreateViewportChangeListener, "createViewportChangeListener");
   pragma Import (Java, CreateHSBChangeListener, "createHSBChangeListener");
   pragma Import (Java, CreateVSBChangeListener, "createVSBChangeListener");
   pragma Import (Java, CreateMouseWheelListener, "createMouseWheelListener");
   pragma Import (Java, UpdateScrollBarDisplayPolicy, "updateScrollBarDisplayPolicy");
   pragma Import (Java, UpdateViewport, "updateViewport");
   pragma Import (Java, UpdateRowHeader, "updateRowHeader");
   pragma Import (Java, UpdateColumnHeader, "updateColumnHeader");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");

end Javax.Swing.Plaf.Basic.BasicScrollPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicScrollPaneUI, "javax.swing.plaf.basic.BasicScrollPaneUI");
pragma Extensions_Allowed (Off);
