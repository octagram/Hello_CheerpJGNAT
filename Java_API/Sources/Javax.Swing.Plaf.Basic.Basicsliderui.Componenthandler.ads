pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ComponentEvent;
with Java.Awt.Event.ComponentAdapter;
with Java.Awt.Event.ComponentListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicSliderUI.ComponentHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ComponentListener_I : Java.Awt.Event.ComponentListener.Ref)
    is new Java.Awt.Event.ComponentAdapter.Typ(ComponentListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ComponentHandler (P1_BasicSliderUI : access Standard.Javax.Swing.Plaf.Basic.BasicSliderUI.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ComponentResized (This : access Typ;
                               P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComponentHandler);
   pragma Import (Java, ComponentResized, "componentResized");

end Javax.Swing.Plaf.Basic.BasicSliderUI.ComponentHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSliderUI.ComponentHandler, "javax.swing.plaf.basic.BasicSliderUI$ComponentHandler");
pragma Extensions_Allowed (Off);
