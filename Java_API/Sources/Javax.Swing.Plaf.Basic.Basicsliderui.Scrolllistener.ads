pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
with Java.Awt.Event.ActionListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicSliderUI.ScrollListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ActionListener_I : Java.Awt.Event.ActionListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ScrollListener (P1_BasicSliderUI : access Standard.Javax.Swing.Plaf.Basic.BasicSliderUI.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_ScrollListener (P1_BasicSliderUI : access Standard.Javax.Swing.Plaf.Basic.BasicSliderUI.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDirection (This : access Typ;
                           P1_Int : Java.Int);

   procedure SetScrollByBlock (This : access Typ;
                               P1_Boolean : Java.Boolean);

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ScrollListener);
   pragma Import (Java, SetDirection, "setDirection");
   pragma Import (Java, SetScrollByBlock, "setScrollByBlock");
   pragma Import (Java, ActionPerformed, "actionPerformed");

end Javax.Swing.Plaf.Basic.BasicSliderUI.ScrollListener;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSliderUI.ScrollListener, "javax.swing.plaf.basic.BasicSliderUI$ScrollListener");
pragma Extensions_Allowed (Off);
