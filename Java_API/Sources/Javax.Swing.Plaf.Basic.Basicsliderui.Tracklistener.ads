pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Lang.Object;
with Javax.Swing.Event.MouseInputAdapter;
with Javax.Swing.Event.MouseInputListener;

package Javax.Swing.Plaf.Basic.BasicSliderUI.TrackListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref;
            MouseInputListener_I : Javax.Swing.Event.MouseInputListener.Ref)
    is new Javax.Swing.Event.MouseInputAdapter.Typ(MouseListener_I,
                                                   MouseMotionListener_I,
                                                   MouseWheelListener_I,
                                                   MouseInputListener_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Offset : Java.Int;
      pragma Import (Java, Offset, "offset");

      --  protected
      CurrentMouseX : Java.Int;
      pragma Import (Java, CurrentMouseX, "currentMouseX");

      --  protected
      CurrentMouseY : Java.Int;
      pragma Import (Java, CurrentMouseY, "currentMouseY");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TrackListener (P1_BasicSliderUI : access Standard.Javax.Swing.Plaf.Basic.BasicSliderUI.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   function ShouldScroll (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Boolean;

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TrackListener);
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, ShouldScroll, "shouldScroll");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");

end Javax.Swing.Plaf.Basic.BasicSliderUI.TrackListener;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSliderUI.TrackListener, "javax.swing.plaf.basic.BasicSliderUI$TrackListener");
pragma Extensions_Allowed (Off);
