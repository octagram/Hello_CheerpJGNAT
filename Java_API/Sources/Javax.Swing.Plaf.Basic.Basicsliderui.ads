pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ComponentListener;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.Integer;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JSlider;
limited with Javax.Swing.Plaf.Basic.BasicSliderUI.ScrollListener;
limited with Javax.Swing.Plaf.Basic.BasicSliderUI.TrackListener;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Timer;
with Java.Lang.Object;
with Javax.Swing.Plaf.SliderUI;

package Javax.Swing.Plaf.Basic.BasicSliderUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.SliderUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ScrollTimer : access Javax.Swing.Timer.Typ'Class;
      pragma Import (Java, ScrollTimer, "scrollTimer");

      --  protected
      Slider : access Javax.Swing.JSlider.Typ'Class;
      pragma Import (Java, Slider, "slider");

      --  protected
      FocusInsets : access Java.Awt.Insets.Typ'Class;
      pragma Import (Java, FocusInsets, "focusInsets");

      --  protected
      InsetCache : access Java.Awt.Insets.Typ'Class;
      pragma Import (Java, InsetCache, "insetCache");

      --  protected
      LeftToRightCache : Java.Boolean;
      pragma Import (Java, LeftToRightCache, "leftToRightCache");

      --  protected
      FocusRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, FocusRect, "focusRect");

      --  protected
      ContentRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, ContentRect, "contentRect");

      --  protected
      LabelRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, LabelRect, "labelRect");

      --  protected
      TickRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, TickRect, "tickRect");

      --  protected
      TrackRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, TrackRect, "trackRect");

      --  protected
      ThumbRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, ThumbRect, "thumbRect");

      --  protected
      TrackBuffer : Java.Int;
      pragma Import (Java, TrackBuffer, "trackBuffer");

      --  protected
      TrackListener : access Javax.Swing.Plaf.Basic.BasicSliderUI.TrackListener.Typ'Class;
      pragma Import (Java, TrackListener, "trackListener");

      --  protected
      ChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, ChangeListener, "changeListener");

      --  protected
      ComponentListener : access Java.Awt.Event.ComponentListener.Typ'Class;
      pragma Import (Java, ComponentListener, "componentListener");

      --  protected
      FocusListener : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, FocusListener, "focusListener");

      --  protected
      ScrollListener : access Javax.Swing.Plaf.Basic.BasicSliderUI.ScrollListener.Typ'Class;
      pragma Import (Java, ScrollListener, "scrollListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetShadowColor (This : access Typ)
                            return access Java.Awt.Color.Typ'Class;

   --  protected
   function GetHighlightColor (This : access Typ)
                               return access Java.Awt.Color.Typ'Class;

   --  protected
   function GetFocusColor (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   --  protected
   function IsDragging (This : access Typ)
                        return Java.Boolean;

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicSliderUI (P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class);

   --  protected
   function CreateTrackListener (This : access Typ;
                                 P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class)
                                 return access Javax.Swing.Plaf.Basic.BasicSliderUI.TrackListener.Typ'Class;

   --  protected
   function CreateChangeListener (This : access Typ;
                                  P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   --  protected
   function CreateComponentListener (This : access Typ;
                                     P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class)
                                     return access Java.Awt.Event.ComponentListener.Typ'Class;

   --  protected
   function CreateFocusListener (This : access Typ;
                                 P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class)
                                 return access Java.Awt.Event.FocusListener.Typ'Class;

   --  protected
   function CreateScrollListener (This : access Typ;
                                  P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class)
                                  return access Javax.Swing.Plaf.Basic.BasicSliderUI.ScrollListener.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ;
                                          P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure InstallListeners (This : access Typ;
                               P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class);

   --  protected
   procedure UninstallListeners (This : access Typ;
                                 P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class);

   --  protected
   procedure InstallKeyboardActions (This : access Typ;
                                     P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ;
                                       P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  protected
   function LabelsHaveSameBaselines (This : access Typ)
                                     return Java.Boolean;

   function GetPreferredHorizontalSize (This : access Typ)
                                        return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredVerticalSize (This : access Typ)
                                      return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumHorizontalSize (This : access Typ)
                                      return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumVerticalSize (This : access Typ)
                                    return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure CalculateGeometry (This : access Typ);

   --  protected
   procedure CalculateFocusRect (This : access Typ);

   --  protected
   procedure CalculateThumbSize (This : access Typ);

   --  protected
   procedure CalculateContentRect (This : access Typ);

   --  protected
   procedure CalculateThumbLocation (This : access Typ);

   --  protected
   procedure CalculateTrackBuffer (This : access Typ);

   --  protected
   procedure CalculateTrackRect (This : access Typ);

   --  protected
   function GetTickLength (This : access Typ)
                           return Java.Int;

   --  protected
   procedure CalculateTickRect (This : access Typ);

   --  protected
   procedure CalculateLabelRect (This : access Typ);

   --  protected
   function GetThumbSize (This : access Typ)
                          return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetWidthOfWidestLabel (This : access Typ)
                                   return Java.Int;

   --  protected
   function GetHeightOfTallestLabel (This : access Typ)
                                     return Java.Int;

   --  protected
   function GetWidthOfHighValueLabel (This : access Typ)
                                      return Java.Int;

   --  protected
   function GetWidthOfLowValueLabel (This : access Typ)
                                     return Java.Int;

   --  protected
   function GetHeightOfHighValueLabel (This : access Typ)
                                       return Java.Int;

   --  protected
   function GetHeightOfLowValueLabel (This : access Typ)
                                      return Java.Int;

   --  protected
   function DrawInverted (This : access Typ)
                          return Java.Boolean;

   --  protected
   function GetHighestValue (This : access Typ)
                             return access Java.Lang.Integer.Typ'Class;

   --  protected
   function GetLowestValue (This : access Typ)
                            return access Java.Lang.Integer.Typ'Class;

   --  protected
   function GetLowestValueLabel (This : access Typ)
                                 return access Java.Awt.Component.Typ'Class;

   --  protected
   function GetHighestValueLabel (This : access Typ)
                                  return access Java.Awt.Component.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure RecalculateIfInsetsChanged (This : access Typ);

   --  protected
   procedure RecalculateIfOrientationChanged (This : access Typ);

   procedure PaintFocus (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PaintTrack (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PaintTicks (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PaintMinorTickForHorizSlider (This : access Typ;
                                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                           P3_Int : Java.Int);

   --  protected
   procedure PaintMajorTickForHorizSlider (This : access Typ;
                                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                           P3_Int : Java.Int);

   --  protected
   procedure PaintMinorTickForVertSlider (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                          P3_Int : Java.Int);

   --  protected
   procedure PaintMajorTickForVertSlider (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                          P3_Int : Java.Int);

   procedure PaintLabels (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure PaintHorizontalLabel (This : access Typ;
                                   P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P2_Int : Java.Int;
                                   P3_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   procedure PaintVerticalLabel (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure PaintThumb (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure SetThumbLocation (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int);

   procedure ScrollByBlock (This : access Typ;
                            P1_Int : Java.Int);

   procedure ScrollByUnit (This : access Typ;
                           P1_Int : Java.Int);

   --  protected
   procedure ScrollDueToClickInTrack (This : access Typ;
                                      P1_Int : Java.Int);

   --  protected
   function XPositionForValue (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   --  protected
   function YPositionForValue (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   --  protected
   function YPositionForValue (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int)
                               return Java.Int;

   function ValueForYPosition (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   function ValueForXPosition (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   POSITIVE_SCROLL : constant Java.Int;

   --  final
   NEGATIVE_SCROLL : constant Java.Int;

   --  final
   MIN_SCROLL : constant Java.Int;

   --  final
   MAX_SCROLL : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetShadowColor, "getShadowColor");
   pragma Import (Java, GetHighlightColor, "getHighlightColor");
   pragma Import (Java, GetFocusColor, "getFocusColor");
   pragma Import (Java, IsDragging, "isDragging");
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_BasicSliderUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, CreateTrackListener, "createTrackListener");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, CreateComponentListener, "createComponentListener");
   pragma Import (Java, CreateFocusListener, "createFocusListener");
   pragma Import (Java, CreateScrollListener, "createScrollListener");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, LabelsHaveSameBaselines, "labelsHaveSameBaselines");
   pragma Import (Java, GetPreferredHorizontalSize, "getPreferredHorizontalSize");
   pragma Import (Java, GetPreferredVerticalSize, "getPreferredVerticalSize");
   pragma Import (Java, GetMinimumHorizontalSize, "getMinimumHorizontalSize");
   pragma Import (Java, GetMinimumVerticalSize, "getMinimumVerticalSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, CalculateGeometry, "calculateGeometry");
   pragma Import (Java, CalculateFocusRect, "calculateFocusRect");
   pragma Import (Java, CalculateThumbSize, "calculateThumbSize");
   pragma Import (Java, CalculateContentRect, "calculateContentRect");
   pragma Import (Java, CalculateThumbLocation, "calculateThumbLocation");
   pragma Import (Java, CalculateTrackBuffer, "calculateTrackBuffer");
   pragma Import (Java, CalculateTrackRect, "calculateTrackRect");
   pragma Import (Java, GetTickLength, "getTickLength");
   pragma Import (Java, CalculateTickRect, "calculateTickRect");
   pragma Import (Java, CalculateLabelRect, "calculateLabelRect");
   pragma Import (Java, GetThumbSize, "getThumbSize");
   pragma Import (Java, GetWidthOfWidestLabel, "getWidthOfWidestLabel");
   pragma Import (Java, GetHeightOfTallestLabel, "getHeightOfTallestLabel");
   pragma Import (Java, GetWidthOfHighValueLabel, "getWidthOfHighValueLabel");
   pragma Import (Java, GetWidthOfLowValueLabel, "getWidthOfLowValueLabel");
   pragma Import (Java, GetHeightOfHighValueLabel, "getHeightOfHighValueLabel");
   pragma Import (Java, GetHeightOfLowValueLabel, "getHeightOfLowValueLabel");
   pragma Import (Java, DrawInverted, "drawInverted");
   pragma Import (Java, GetHighestValue, "getHighestValue");
   pragma Import (Java, GetLowestValue, "getLowestValue");
   pragma Import (Java, GetLowestValueLabel, "getLowestValueLabel");
   pragma Import (Java, GetHighestValueLabel, "getHighestValueLabel");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, RecalculateIfInsetsChanged, "recalculateIfInsetsChanged");
   pragma Import (Java, RecalculateIfOrientationChanged, "recalculateIfOrientationChanged");
   pragma Import (Java, PaintFocus, "paintFocus");
   pragma Import (Java, PaintTrack, "paintTrack");
   pragma Import (Java, PaintTicks, "paintTicks");
   pragma Import (Java, PaintMinorTickForHorizSlider, "paintMinorTickForHorizSlider");
   pragma Import (Java, PaintMajorTickForHorizSlider, "paintMajorTickForHorizSlider");
   pragma Import (Java, PaintMinorTickForVertSlider, "paintMinorTickForVertSlider");
   pragma Import (Java, PaintMajorTickForVertSlider, "paintMajorTickForVertSlider");
   pragma Import (Java, PaintLabels, "paintLabels");
   pragma Import (Java, PaintHorizontalLabel, "paintHorizontalLabel");
   pragma Import (Java, PaintVerticalLabel, "paintVerticalLabel");
   pragma Import (Java, PaintThumb, "paintThumb");
   pragma Import (Java, SetThumbLocation, "setThumbLocation");
   pragma Import (Java, ScrollByBlock, "scrollByBlock");
   pragma Import (Java, ScrollByUnit, "scrollByUnit");
   pragma Import (Java, ScrollDueToClickInTrack, "scrollDueToClickInTrack");
   pragma Import (Java, XPositionForValue, "xPositionForValue");
   pragma Import (Java, YPositionForValue, "yPositionForValue");
   pragma Import (Java, ValueForYPosition, "valueForYPosition");
   pragma Import (Java, ValueForXPosition, "valueForXPosition");
   pragma Import (Java, POSITIVE_SCROLL, "POSITIVE_SCROLL");
   pragma Import (Java, NEGATIVE_SCROLL, "NEGATIVE_SCROLL");
   pragma Import (Java, MIN_SCROLL, "MIN_SCROLL");
   pragma Import (Java, MAX_SCROLL, "MAX_SCROLL");

end Javax.Swing.Plaf.Basic.BasicSliderUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSliderUI, "javax.swing.plaf.basic.BasicSliderUI");
pragma Extensions_Allowed (Off);
