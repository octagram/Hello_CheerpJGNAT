pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.LayoutManager;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JSpinner;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.SpinnerUI;

package Javax.Swing.Plaf.Basic.BasicSpinnerUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.SpinnerUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Spinner : access Javax.Swing.JSpinner.Typ'Class;
      pragma Import (Java, Spinner, "spinner");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicSpinnerUI (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallNextButtonListeners (This : access Typ;
                                         P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   procedure InstallPreviousButtonListeners (This : access Typ;
                                             P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   function CreateLayout (This : access Typ)
                          return access Java.Awt.LayoutManager.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreatePreviousButton (This : access Typ)
                                  return access Java.Awt.Component.Typ'Class;

   --  protected
   function CreateNextButton (This : access Typ)
                              return access Java.Awt.Component.Typ'Class;

   --  protected
   function CreateEditor (This : access Typ)
                          return access Javax.Swing.JComponent.Typ'Class;

   --  protected
   procedure ReplaceEditor (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                            P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicSpinnerUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallNextButtonListeners, "installNextButtonListeners");
   pragma Import (Java, InstallPreviousButtonListeners, "installPreviousButtonListeners");
   pragma Import (Java, CreateLayout, "createLayout");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreatePreviousButton, "createPreviousButton");
   pragma Import (Java, CreateNextButton, "createNextButton");
   pragma Import (Java, CreateEditor, "createEditor");
   pragma Import (Java, ReplaceEditor, "replaceEditor");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");

end Javax.Swing.Plaf.Basic.BasicSpinnerUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSpinnerUI, "javax.swing.plaf.basic.BasicSpinnerUI");
pragma Extensions_Allowed (Off);
