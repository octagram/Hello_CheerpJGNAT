pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Beans.PropertyChangeEvent;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JSplitPane;
limited with Javax.Swing.Plaf.Basic.BasicSplitPaneUI;
with Java.Awt.Container;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Beans.PropertyChangeListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicSplitPaneDivider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Container.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      SplitPaneUI : access Javax.Swing.Plaf.Basic.BasicSplitPaneUI.Typ'Class;
      pragma Import (Java, SplitPaneUI, "splitPaneUI");

      --  protected
      DividerSize : Java.Int;
      pragma Import (Java, DividerSize, "dividerSize");

      --  protected
      HiddenDivider : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, HiddenDivider, "hiddenDivider");

      --  protected
      SplitPane : access Javax.Swing.JSplitPane.Typ'Class;
      pragma Import (Java, SplitPane, "splitPane");

      --  protected
      Orientation : Java.Int;
      pragma Import (Java, Orientation, "orientation");

      --  protected
      LeftButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, LeftButton, "leftButton");

      --  protected
      RightButton : access Javax.Swing.JButton.Typ'Class;
      pragma Import (Java, RightButton, "rightButton");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicSplitPaneDivider (P1_BasicSplitPaneUI : access Standard.Javax.Swing.Plaf.Basic.BasicSplitPaneUI.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetBasicSplitPaneUI (This : access Typ;
                                  P1_BasicSplitPaneUI : access Standard.Javax.Swing.Plaf.Basic.BasicSplitPaneUI.Typ'Class);

   function GetBasicSplitPaneUI (This : access Typ)
                                 return access Javax.Swing.Plaf.Basic.BasicSplitPaneUI.Typ'Class;

   procedure SetDividerSize (This : access Typ;
                             P1_Int : Java.Int);

   function GetDividerSize (This : access Typ)
                            return Java.Int;

   procedure SetBorder (This : access Typ;
                        P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class);

   function GetBorder (This : access Typ)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetInsets (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   --  protected
   procedure SetMouseOver (This : access Typ;
                           P1_Boolean : Java.Boolean);

   function IsMouseOver (This : access Typ)
                         return Java.Boolean;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   procedure OneTouchExpandableChanged (This : access Typ);

   --  protected
   function CreateLeftOneTouchButton (This : access Typ)
                                      return access Javax.Swing.JButton.Typ'Class;

   --  protected
   function CreateRightOneTouchButton (This : access Typ)
                                       return access Javax.Swing.JButton.Typ'Class;

   --  protected
   procedure PrepareForDragging (This : access Typ);

   --  protected
   procedure DragDividerTo (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   procedure FinishDraggingTo (This : access Typ;
                               P1_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   ONE_TOUCH_SIZE : constant Java.Int;

   --  protected  final
   ONE_TOUCH_OFFSET : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicSplitPaneDivider);
   pragma Import (Java, SetBasicSplitPaneUI, "setBasicSplitPaneUI");
   pragma Import (Java, GetBasicSplitPaneUI, "getBasicSplitPaneUI");
   pragma Import (Java, SetDividerSize, "setDividerSize");
   pragma Import (Java, GetDividerSize, "getDividerSize");
   pragma Import (Java, SetBorder, "setBorder");
   pragma Import (Java, GetBorder, "getBorder");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, SetMouseOver, "setMouseOver");
   pragma Import (Java, IsMouseOver, "isMouseOver");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, OneTouchExpandableChanged, "oneTouchExpandableChanged");
   pragma Import (Java, CreateLeftOneTouchButton, "createLeftOneTouchButton");
   pragma Import (Java, CreateRightOneTouchButton, "createRightOneTouchButton");
   pragma Import (Java, PrepareForDragging, "prepareForDragging");
   pragma Import (Java, DragDividerTo, "dragDividerTo");
   pragma Import (Java, FinishDraggingTo, "finishDraggingTo");
   pragma Import (Java, ONE_TOUCH_SIZE, "ONE_TOUCH_SIZE");
   pragma Import (Java, ONE_TOUCH_OFFSET, "ONE_TOUCH_OFFSET");

end Javax.Swing.Plaf.Basic.BasicSplitPaneDivider;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSplitPaneDivider, "javax.swing.plaf.basic.BasicSplitPaneDivider");
pragma Extensions_Allowed (Off);
