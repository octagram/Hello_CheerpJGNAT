pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Awt.Insets;
limited with Java.Lang.String;
with Java.Awt.LayoutManager2;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicHorizontalLayoutManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Sizes : Java.Int_Arr;
      pragma Import (Java, Sizes, "sizes");

      --  protected
      Components : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Components, "components");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure AddLayoutComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetLayoutAlignmentX (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   function GetLayoutAlignmentY (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   procedure InvalidateLayout (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   function MaximumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   procedure ResetToPreferredSizes (This : access Typ);

   --  protected
   procedure ResetSizeAt (This : access Typ;
                          P1_Int : Java.Int);

   --  protected
   procedure SetSizes (This : access Typ;
                       P1_Int_Arr : Java.Int_Arr);

   --  protected
   function GetSizes (This : access Typ)
                      return Java.Int_Arr;

   --  protected
   function GetPreferredSizeOfComponent (This : access Typ;
                                         P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                         return Java.Int;

   --  protected
   function GetSizeOfComponent (This : access Typ;
                                P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return Java.Int;

   --  protected
   function GetAvailableSize (This : access Typ;
                              P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class;
                              P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                              return Java.Int;

   --  protected
   function GetInitialLocation (This : access Typ;
                                P1_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                                return Java.Int;

   --  protected
   procedure SetComponentToSize (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                                 P5_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   --  protected
   procedure UpdateComponents (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, GetLayoutAlignmentX, "getLayoutAlignmentX");
   pragma Import (Java, GetLayoutAlignmentY, "getLayoutAlignmentY");
   pragma Import (Java, InvalidateLayout, "invalidateLayout");
   pragma Import (Java, MaximumLayoutSize, "maximumLayoutSize");
   pragma Import (Java, ResetToPreferredSizes, "resetToPreferredSizes");
   pragma Import (Java, ResetSizeAt, "resetSizeAt");
   pragma Import (Java, SetSizes, "setSizes");
   pragma Import (Java, GetSizes, "getSizes");
   pragma Import (Java, GetPreferredSizeOfComponent, "getPreferredSizeOfComponent");
   pragma Import (Java, GetSizeOfComponent, "getSizeOfComponent");
   pragma Import (Java, GetAvailableSize, "getAvailableSize");
   pragma Import (Java, GetInitialLocation, "getInitialLocation");
   pragma Import (Java, SetComponentToSize, "setComponentToSize");
   pragma Import (Java, UpdateComponents, "updateComponents");

end Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicHorizontalLayoutManager;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicHorizontalLayoutManager, "javax.swing.plaf.basic.BasicSplitPaneUI$BasicHorizontalLayoutManager");
pragma Extensions_Allowed (Off);
