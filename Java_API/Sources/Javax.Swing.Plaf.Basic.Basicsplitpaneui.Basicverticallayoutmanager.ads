pragma Extensions_Allowed (On);
with Java.Awt.LayoutManager2;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicHorizontalLayoutManager;

package Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicVerticalLayoutManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref)
    is new Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicHorizontalLayoutManager.Typ(LayoutManager2_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicVerticalLayoutManager (P1_BasicSplitPaneUI : access Standard.Javax.Swing.Plaf.Basic.BasicSplitPaneUI.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicVerticalLayoutManager);

end Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicVerticalLayoutManager;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicVerticalLayoutManager, "javax.swing.plaf.basic.BasicSplitPaneUI$BasicVerticalLayoutManager");
pragma Extensions_Allowed (Off);
