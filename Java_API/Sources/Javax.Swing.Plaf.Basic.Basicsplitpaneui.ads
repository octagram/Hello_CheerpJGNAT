pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JSplitPane;
limited with Javax.Swing.Plaf.Basic.BasicSplitPaneDivider;
limited with Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicHorizontalLayoutManager;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.SplitPaneUI;

package Javax.Swing.Plaf.Basic.BasicSplitPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.SplitPaneUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      SplitPane : access Javax.Swing.JSplitPane.Typ'Class;
      pragma Import (Java, SplitPane, "splitPane");

      --  protected
      LayoutManager : access Javax.Swing.Plaf.Basic.BasicSplitPaneUI.BasicHorizontalLayoutManager.Typ'Class;
      pragma Import (Java, LayoutManager, "layoutManager");

      --  protected
      Divider : access Javax.Swing.Plaf.Basic.BasicSplitPaneDivider.Typ'Class;
      pragma Import (Java, Divider, "divider");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      FocusListener : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, FocusListener, "focusListener");

      --  protected
      DividerSize : Java.Int;
      pragma Import (Java, DividerSize, "dividerSize");

      --  protected
      NonContinuousLayoutDivider : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, NonContinuousLayoutDivider, "nonContinuousLayoutDivider");

      --  protected
      DraggingHW : Java.Boolean;
      pragma Import (Java, DraggingHW, "draggingHW");

      --  protected
      BeginDragDividerLocation : Java.Int;
      pragma Import (Java, BeginDragDividerLocation, "beginDragDividerLocation");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicSplitPaneUI (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateFocusListener (This : access Typ)
                                 return access Java.Awt.Event.FocusListener.Typ'Class;

   function GetOrientation (This : access Typ)
                            return Java.Int;

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   function IsContinuousLayout (This : access Typ)
                                return Java.Boolean;

   procedure SetContinuousLayout (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function GetLastDragLocation (This : access Typ)
                                 return Java.Int;

   procedure SetLastDragLocation (This : access Typ;
                                  P1_Int : Java.Int);

   function GetDivider (This : access Typ)
                        return access Javax.Swing.Plaf.Basic.BasicSplitPaneDivider.Typ'Class;

   --  protected
   function CreateDefaultNonContinuousLayoutDivider (This : access Typ)
                                                     return access Java.Awt.Component.Typ'Class;

   --  protected
   procedure SetNonContinuousLayoutDivider (This : access Typ;
                                            P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   procedure SetNonContinuousLayoutDivider (This : access Typ;
                                            P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                            P2_Boolean : Java.Boolean);

   function GetNonContinuousLayoutDivider (This : access Typ)
                                           return access Java.Awt.Component.Typ'Class;

   function GetSplitPane (This : access Typ)
                          return access Javax.Swing.JSplitPane.Typ'Class;

   function CreateDefaultDivider (This : access Typ)
                                  return access Javax.Swing.Plaf.Basic.BasicSplitPaneDivider.Typ'Class;

   procedure ResetToPreferredSizes (This : access Typ;
                                    P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class);

   procedure SetDividerLocation (This : access Typ;
                                 P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class;
                                 P2_Int : Java.Int);

   function GetDividerLocation (This : access Typ;
                                P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class)
                                return Java.Int;

   function GetMinimumDividerLocation (This : access Typ;
                                       P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class)
                                       return Java.Int;

   function GetMaximumDividerLocation (This : access Typ;
                                       P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class)
                                       return Java.Int;

   procedure FinishedPaintingChildren (This : access Typ;
                                       P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetInsets (This : access Typ;
                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   --  protected
   procedure ResetLayoutManager (This : access Typ);

   --  protected
   procedure StartDragging (This : access Typ);

   --  protected
   procedure DragDividerTo (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   procedure FinishDraggingTo (This : access Typ;
                               P1_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   NON_CONTINUOUS_DIVIDER : constant access Java.Lang.String.Typ'Class;

   --  protected
   KEYBOARD_DIVIDER_MOVE_OFFSET : Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicSplitPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreateFocusListener, "createFocusListener");
   pragma Import (Java, GetOrientation, "getOrientation");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, IsContinuousLayout, "isContinuousLayout");
   pragma Import (Java, SetContinuousLayout, "setContinuousLayout");
   pragma Import (Java, GetLastDragLocation, "getLastDragLocation");
   pragma Import (Java, SetLastDragLocation, "setLastDragLocation");
   pragma Import (Java, GetDivider, "getDivider");
   pragma Import (Java, CreateDefaultNonContinuousLayoutDivider, "createDefaultNonContinuousLayoutDivider");
   pragma Import (Java, SetNonContinuousLayoutDivider, "setNonContinuousLayoutDivider");
   pragma Import (Java, GetNonContinuousLayoutDivider, "getNonContinuousLayoutDivider");
   pragma Import (Java, GetSplitPane, "getSplitPane");
   pragma Import (Java, CreateDefaultDivider, "createDefaultDivider");
   pragma Import (Java, ResetToPreferredSizes, "resetToPreferredSizes");
   pragma Import (Java, SetDividerLocation, "setDividerLocation");
   pragma Import (Java, GetDividerLocation, "getDividerLocation");
   pragma Import (Java, GetMinimumDividerLocation, "getMinimumDividerLocation");
   pragma Import (Java, GetMaximumDividerLocation, "getMaximumDividerLocation");
   pragma Import (Java, FinishedPaintingChildren, "finishedPaintingChildren");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, ResetLayoutManager, "resetLayoutManager");
   pragma Import (Java, StartDragging, "startDragging");
   pragma Import (Java, DragDividerTo, "dragDividerTo");
   pragma Import (Java, FinishDraggingTo, "finishDraggingTo");
   pragma Import (Java, NON_CONTINUOUS_DIVIDER, "NON_CONTINUOUS_DIVIDER");
   pragma Import (Java, KEYBOARD_DIVIDER_MOVE_OFFSET, "KEYBOARD_DIVIDER_MOVE_OFFSET");

end Javax.Swing.Plaf.Basic.BasicSplitPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicSplitPaneUI, "javax.swing.plaf.basic.BasicSplitPaneUI");
pragma Extensions_Allowed (Off);
