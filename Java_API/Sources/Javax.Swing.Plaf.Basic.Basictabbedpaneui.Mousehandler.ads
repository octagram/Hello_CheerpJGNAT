pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
with Java.Awt.Event.MouseAdapter;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.MouseHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref)
    is new Java.Awt.Event.MouseAdapter.Typ(MouseListener_I,
                                           MouseMotionListener_I,
                                           MouseWheelListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MouseHandler (P1_BasicTabbedPaneUI : access Standard.Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MouseHandler);
   pragma Import (Java, MousePressed, "mousePressed");

end Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.MouseHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.MouseHandler, "javax.swing.plaf.basic.BasicTabbedPaneUI$MouseHandler");
pragma Extensions_Allowed (Off);
