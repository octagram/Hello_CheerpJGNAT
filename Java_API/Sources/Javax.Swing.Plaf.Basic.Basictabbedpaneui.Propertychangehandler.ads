pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.PropertyChangeHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PropertyChangeHandler (P1_BasicTabbedPaneUI : access Standard.Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PropertyChangeHandler);
   pragma Import (Java, PropertyChange, "propertyChange");

end Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.PropertyChangeHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.PropertyChangeHandler, "javax.swing.plaf.basic.BasicTabbedPaneUI$PropertyChangeHandler");
pragma Extensions_Allowed (Off);
