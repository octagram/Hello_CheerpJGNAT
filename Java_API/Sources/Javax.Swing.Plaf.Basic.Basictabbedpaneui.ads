pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Event.MouseListener;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JTabbedPane;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Plaf.TabbedPaneUI;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Basic.BasicTabbedPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Plaf.TabbedPaneUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      TabPane : access Javax.Swing.JTabbedPane.Typ'Class;
      pragma Import (Java, TabPane, "tabPane");

      --  protected
      Highlight : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Highlight, "highlight");

      --  protected
      LightHighlight : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, LightHighlight, "lightHighlight");

      --  protected
      Shadow : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Shadow, "shadow");

      --  protected
      DarkShadow : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, DarkShadow, "darkShadow");

      --  protected
      Focus : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, Focus, "focus");

      --  protected
      TextIconGap : Java.Int;
      pragma Import (Java, TextIconGap, "textIconGap");

      --  protected
      TabRunOverlay : Java.Int;
      pragma Import (Java, TabRunOverlay, "tabRunOverlay");

      --  protected
      TabInsets : access Java.Awt.Insets.Typ'Class;
      pragma Import (Java, TabInsets, "tabInsets");

      --  protected
      SelectedTabPadInsets : access Java.Awt.Insets.Typ'Class;
      pragma Import (Java, SelectedTabPadInsets, "selectedTabPadInsets");

      --  protected
      TabAreaInsets : access Java.Awt.Insets.Typ'Class;
      pragma Import (Java, TabAreaInsets, "tabAreaInsets");

      --  protected
      ContentBorderInsets : access Java.Awt.Insets.Typ'Class;
      pragma Import (Java, ContentBorderInsets, "contentBorderInsets");

      --  protected
      TabRuns : Java.Int_Arr;
      pragma Import (Java, TabRuns, "tabRuns");

      --  protected
      RunCount : Java.Int;
      pragma Import (Java, RunCount, "runCount");

      --  protected
      SelectedRun : Java.Int;
      pragma Import (Java, SelectedRun, "selectedRun");

      --  protected
      Rects : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Rects, "rects");

      --  protected
      MaxTabHeight : Java.Int;
      pragma Import (Java, MaxTabHeight, "maxTabHeight");

      --  protected
      MaxTabWidth : Java.Int;
      pragma Import (Java, MaxTabWidth, "maxTabWidth");

      --  protected
      TabChangeListener : access Javax.Swing.Event.ChangeListener.Typ'Class;
      pragma Import (Java, TabChangeListener, "tabChangeListener");

      --  protected
      PropertyChangeListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyChangeListener, "propertyChangeListener");

      --  protected
      MouseListener : access Java.Awt.Event.MouseListener.Typ'Class;
      pragma Import (Java, MouseListener, "mouseListener");

      --  protected
      FocusListener : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, FocusListener, "focusListener");

      --  protected
      CalcRect : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, CalcRect, "calcRect");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicTabbedPaneUI (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   function CreateLayoutManager (This : access Typ)
                                 return access Java.Awt.LayoutManager.Typ'Class;

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   function CreateScrollButton (This : access Typ;
                                P1_Int : Java.Int)
                                return access Javax.Swing.JButton.Typ'Class;

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   function CreateMouseListener (This : access Typ)
                                 return access Java.Awt.Event.MouseListener.Typ'Class;

   --  protected
   function CreateFocusListener (This : access Typ)
                                 return access Java.Awt.Event.FocusListener.Typ'Class;

   --  protected
   function CreateChangeListener (This : access Typ)
                                  return access Javax.Swing.Event.ChangeListener.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   procedure SetRolloverTab (This : access Typ;
                             P1_Int : Java.Int);

   --  protected
   function GetRolloverTab (This : access Typ)
                            return Java.Int;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   --  protected
   function GetBaseline (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   --  protected
   function GetBaselineOffset (This : access Typ)
                               return Java.Int;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintTabArea (This : access Typ;
                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int);

   --  protected
   procedure PaintTab (This : access Typ;
                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Rectangle_Arr : access Java.Awt.Rectangle.Arr_Obj;
                       P4_Int : Java.Int;
                       P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                       P6_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure LayoutLabel (This : access Typ;
                          P1_Int : Java.Int;
                          P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_String : access Standard.Java.Lang.String.Typ'Class;
                          P5_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                          P6_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                          P7_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                          P8_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                          P9_Boolean : Java.Boolean);

   --  protected
   procedure PaintIcon (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                        P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P6_Boolean : Java.Boolean);

   --  protected
   procedure PaintText (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Font : access Standard.Java.Awt.Font.Typ'Class;
                        P4_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                        P5_Int : Java.Int;
                        P6_String : access Standard.Java.Lang.String.Typ'Class;
                        P7_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P8_Boolean : Java.Boolean);

   --  protected
   function GetTabLabelShiftX (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Boolean : Java.Boolean)
                               return Java.Int;

   --  protected
   function GetTabLabelShiftY (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Boolean : Java.Boolean)
                               return Java.Int;

   --  protected
   procedure PaintFocusIndicator (This : access Typ;
                                  P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Rectangle_Arr : access Java.Awt.Rectangle.Arr_Obj;
                                  P4_Int : Java.Int;
                                  P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                  P6_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                  P7_Boolean : Java.Boolean);

   --  protected
   procedure PaintTabBorder (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int;
                             P8_Boolean : Java.Boolean);

   --  protected
   procedure PaintTabBackground (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Boolean : Java.Boolean);

   --  protected
   procedure PaintContentBorder (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int);

   --  protected
   procedure PaintContentBorderTopEdge (This : access Typ;
                                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int;
                                        P7_Int : Java.Int);

   --  protected
   procedure PaintContentBorderLeftEdge (This : access Typ;
                                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int;
                                         P7_Int : Java.Int);

   --  protected
   procedure PaintContentBorderBottomEdge (This : access Typ;
                                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P2_Int : Java.Int;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int;
                                           P7_Int : Java.Int);

   --  protected
   procedure PaintContentBorderRightEdge (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Int : Java.Int;
                                          P3_Int : Java.Int;
                                          P4_Int : Java.Int;
                                          P5_Int : Java.Int;
                                          P6_Int : Java.Int;
                                          P7_Int : Java.Int);

   function GetTabBounds (This : access Typ;
                          P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Awt.Rectangle.Typ'Class;

   function GetTabRunCount (This : access Typ;
                            P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class)
                            return Java.Int;

   function TabForCoordinate (This : access Typ;
                              P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int)
                              return Java.Int;

   --  protected
   function GetTabBounds (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                          return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function GetVisibleComponent (This : access Typ)
                                 return access Java.Awt.Component.Typ'Class;

   --  protected
   procedure SetVisibleComponent (This : access Typ;
                                  P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   procedure AssureRectsCreated (This : access Typ;
                                 P1_Int : Java.Int);

   --  protected
   procedure ExpandTabRunsArray (This : access Typ);

   --  protected
   function GetRunForTab (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return Java.Int;

   --  protected
   function LastTabInRun (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return Java.Int;

   --  protected
   function GetTabRunOverlay (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   --  protected
   function GetTabRunIndent (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return Java.Int;

   --  protected
   function ShouldPadTabRun (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return Java.Boolean;

   --  protected
   function ShouldRotateTabRuns (This : access Typ;
                                 P1_Int : Java.Int)
                                 return Java.Boolean;

   --  protected
   function GetIconForTab (This : access Typ;
                           P1_Int : Java.Int)
                           return access Javax.Swing.Icon.Typ'Class;

   --  protected
   function GetTextViewForTab (This : access Typ;
                               P1_Int : Java.Int)
                               return access Javax.Swing.Text.View.Typ'Class;

   --  protected
   function CalculateTabHeight (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int)
                                return Java.Int;

   --  protected
   function CalculateMaxTabHeight (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Int;

   --  protected
   function CalculateTabWidth (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class)
                               return Java.Int;

   --  protected
   function CalculateMaxTabWidth (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Int;

   --  protected
   function CalculateTabAreaHeight (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Int : Java.Int)
                                    return Java.Int;

   --  protected
   function CalculateTabAreaWidth (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int)
                                   return Java.Int;

   --  protected
   function GetTabInsets (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return access Java.Awt.Insets.Typ'Class;

   --  protected
   function GetSelectedTabPadInsets (This : access Typ;
                                     P1_Int : Java.Int)
                                     return access Java.Awt.Insets.Typ'Class;

   --  protected
   function GetTabAreaInsets (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Awt.Insets.Typ'Class;

   --  protected
   function GetContentBorderInsets (This : access Typ;
                                    P1_Int : Java.Int)
                                    return access Java.Awt.Insets.Typ'Class;

   --  protected
   function GetFontMetrics (This : access Typ)
                            return access Java.Awt.FontMetrics.Typ'Class;

   --  protected
   procedure NavigateSelectedTab (This : access Typ;
                                  P1_Int : Java.Int);

   --  protected
   procedure SelectNextTabInRun (This : access Typ;
                                 P1_Int : Java.Int);

   --  protected
   procedure SelectPreviousTabInRun (This : access Typ;
                                     P1_Int : Java.Int);

   --  protected
   procedure SelectNextTab (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   procedure SelectPreviousTab (This : access Typ;
                                P1_Int : Java.Int);

   --  protected
   procedure SelectAdjacentRunTab (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int);

   --  protected
   function GetFocusIndex (This : access Typ)
                           return Java.Int;

   --  protected
   function GetTabRunOffset (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Boolean : Java.Boolean)
                             return Java.Int;

   --  protected
   function GetPreviousTabIndex (This : access Typ;
                                 P1_Int : Java.Int)
                                 return Java.Int;

   --  protected
   function GetNextTabIndex (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   --  protected
   function GetNextTabIndexInRun (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int)
                                  return Java.Int;

   --  protected
   function GetPreviousTabIndexInRun (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int)
                                      return Java.Int;

   --  protected
   function GetPreviousTabRun (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   --  protected
   function GetNextTabRun (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Int;

   --  protected
   procedure RotateInsets (P1_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                           P2_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                           P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicTabbedPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, CreateLayoutManager, "createLayoutManager");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, CreateScrollButton, "createScrollButton");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, CreateMouseListener, "createMouseListener");
   pragma Import (Java, CreateFocusListener, "createFocusListener");
   pragma Import (Java, CreateChangeListener, "createChangeListener");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, SetRolloverTab, "setRolloverTab");
   pragma Import (Java, GetRolloverTab, "getRolloverTab");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, GetBaselineOffset, "getBaselineOffset");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintTabArea, "paintTabArea");
   pragma Import (Java, PaintTab, "paintTab");
   pragma Import (Java, LayoutLabel, "layoutLabel");
   pragma Import (Java, PaintIcon, "paintIcon");
   pragma Import (Java, PaintText, "paintText");
   pragma Import (Java, GetTabLabelShiftX, "getTabLabelShiftX");
   pragma Import (Java, GetTabLabelShiftY, "getTabLabelShiftY");
   pragma Import (Java, PaintFocusIndicator, "paintFocusIndicator");
   pragma Import (Java, PaintTabBorder, "paintTabBorder");
   pragma Import (Java, PaintTabBackground, "paintTabBackground");
   pragma Import (Java, PaintContentBorder, "paintContentBorder");
   pragma Import (Java, PaintContentBorderTopEdge, "paintContentBorderTopEdge");
   pragma Import (Java, PaintContentBorderLeftEdge, "paintContentBorderLeftEdge");
   pragma Import (Java, PaintContentBorderBottomEdge, "paintContentBorderBottomEdge");
   pragma Import (Java, PaintContentBorderRightEdge, "paintContentBorderRightEdge");
   pragma Import (Java, GetTabBounds, "getTabBounds");
   pragma Import (Java, GetTabRunCount, "getTabRunCount");
   pragma Import (Java, TabForCoordinate, "tabForCoordinate");
   pragma Import (Java, GetVisibleComponent, "getVisibleComponent");
   pragma Import (Java, SetVisibleComponent, "setVisibleComponent");
   pragma Import (Java, AssureRectsCreated, "assureRectsCreated");
   pragma Import (Java, ExpandTabRunsArray, "expandTabRunsArray");
   pragma Import (Java, GetRunForTab, "getRunForTab");
   pragma Import (Java, LastTabInRun, "lastTabInRun");
   pragma Import (Java, GetTabRunOverlay, "getTabRunOverlay");
   pragma Import (Java, GetTabRunIndent, "getTabRunIndent");
   pragma Import (Java, ShouldPadTabRun, "shouldPadTabRun");
   pragma Import (Java, ShouldRotateTabRuns, "shouldRotateTabRuns");
   pragma Import (Java, GetIconForTab, "getIconForTab");
   pragma Import (Java, GetTextViewForTab, "getTextViewForTab");
   pragma Import (Java, CalculateTabHeight, "calculateTabHeight");
   pragma Import (Java, CalculateMaxTabHeight, "calculateMaxTabHeight");
   pragma Import (Java, CalculateTabWidth, "calculateTabWidth");
   pragma Import (Java, CalculateMaxTabWidth, "calculateMaxTabWidth");
   pragma Import (Java, CalculateTabAreaHeight, "calculateTabAreaHeight");
   pragma Import (Java, CalculateTabAreaWidth, "calculateTabAreaWidth");
   pragma Import (Java, GetTabInsets, "getTabInsets");
   pragma Import (Java, GetSelectedTabPadInsets, "getSelectedTabPadInsets");
   pragma Import (Java, GetTabAreaInsets, "getTabAreaInsets");
   pragma Import (Java, GetContentBorderInsets, "getContentBorderInsets");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, NavigateSelectedTab, "navigateSelectedTab");
   pragma Import (Java, SelectNextTabInRun, "selectNextTabInRun");
   pragma Import (Java, SelectPreviousTabInRun, "selectPreviousTabInRun");
   pragma Import (Java, SelectNextTab, "selectNextTab");
   pragma Import (Java, SelectPreviousTab, "selectPreviousTab");
   pragma Import (Java, SelectAdjacentRunTab, "selectAdjacentRunTab");
   pragma Import (Java, GetFocusIndex, "getFocusIndex");
   pragma Import (Java, GetTabRunOffset, "getTabRunOffset");
   pragma Import (Java, GetPreviousTabIndex, "getPreviousTabIndex");
   pragma Import (Java, GetNextTabIndex, "getNextTabIndex");
   pragma Import (Java, GetNextTabIndexInRun, "getNextTabIndexInRun");
   pragma Import (Java, GetPreviousTabIndexInRun, "getPreviousTabIndexInRun");
   pragma Import (Java, GetPreviousTabRun, "getPreviousTabRun");
   pragma Import (Java, GetNextTabRun, "getNextTabRun");
   pragma Import (Java, RotateInsets, "rotateInsets");

end Javax.Swing.Plaf.Basic.BasicTabbedPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTabbedPaneUI, "javax.swing.plaf.basic.BasicTabbedPaneUI");
pragma Extensions_Allowed (Off);
