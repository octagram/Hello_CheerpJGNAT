pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Javax.Swing.CellRendererPane;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Table.JTableHeader;
with Java.Lang.Object;
with Javax.Swing.Plaf.TableHeaderUI;

package Javax.Swing.Plaf.Basic.BasicTableHeaderUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.TableHeaderUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Header : access Javax.Swing.Table.JTableHeader.Typ'Class;
      pragma Import (Java, Header, "header");

      --  protected
      RendererPane : access Javax.Swing.CellRendererPane.Typ'Class;
      pragma Import (Java, RendererPane, "rendererPane");

      --  protected
      MouseInputListener : access Javax.Swing.Event.MouseInputListener.Typ'Class;
      pragma Import (Java, MouseInputListener, "mouseInputListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicTableHeaderUI (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateMouseInputListener (This : access Typ)
                                      return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   function GetRolloverColumn (This : access Typ)
                               return Java.Int;

   --  protected
   procedure RolloverColumnUpdated (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Int : Java.Int);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicTableHeaderUI);
   pragma Import (Java, CreateMouseInputListener, "createMouseInputListener");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, GetRolloverColumn, "getRolloverColumn");
   pragma Import (Java, RolloverColumnUpdated, "rolloverColumnUpdated");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");

end Javax.Swing.Plaf.Basic.BasicTableHeaderUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTableHeaderUI, "javax.swing.plaf.basic.BasicTableHeaderUI");
pragma Extensions_Allowed (Off);
