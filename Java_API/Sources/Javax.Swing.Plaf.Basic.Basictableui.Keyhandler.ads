pragma Extensions_Allowed (On);
limited with Java.Awt.Event.KeyEvent;
with Java.Awt.Event.KeyListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicTableUI.KeyHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(KeyListener_I : Java.Awt.Event.KeyListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KeyHandler (P1_BasicTableUI : access Standard.Javax.Swing.Plaf.Basic.BasicTableUI.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure KeyPressed (This : access Typ;
                         P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   procedure KeyReleased (This : access Typ;
                          P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);

   procedure KeyTyped (This : access Typ;
                       P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyHandler);
   pragma Import (Java, KeyPressed, "keyPressed");
   pragma Import (Java, KeyReleased, "keyReleased");
   pragma Import (Java, KeyTyped, "keyTyped");

end Javax.Swing.Plaf.Basic.BasicTableUI.KeyHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTableUI.KeyHandler, "javax.swing.plaf.basic.BasicTableUI$KeyHandler");
pragma Extensions_Allowed (Off);
