pragma Extensions_Allowed (On);
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Event.KeyListener;
limited with Java.Awt.Graphics;
limited with Javax.Swing.CellRendererPane;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JTable;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.TableUI;

package Javax.Swing.Plaf.Basic.BasicTableUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.TableUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Table : access Javax.Swing.JTable.Typ'Class;
      pragma Import (Java, Table, "table");

      --  protected
      RendererPane : access Javax.Swing.CellRendererPane.Typ'Class;
      pragma Import (Java, RendererPane, "rendererPane");

      --  protected
      KeyListener : access Java.Awt.Event.KeyListener.Typ'Class;
      pragma Import (Java, KeyListener, "keyListener");

      --  protected
      FocusListener : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, FocusListener, "focusListener");

      --  protected
      MouseInputListener : access Javax.Swing.Event.MouseInputListener.Typ'Class;
      pragma Import (Java, MouseInputListener, "mouseInputListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicTableUI (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateKeyListener (This : access Typ)
                               return access Java.Awt.Event.KeyListener.Typ'Class;

   --  protected
   function CreateFocusListener (This : access Typ)
                                 return access Java.Awt.Event.FocusListener.Typ'Class;

   --  protected
   function CreateMouseInputListener (This : access Typ)
                                      return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicTableUI);
   pragma Import (Java, CreateKeyListener, "createKeyListener");
   pragma Import (Java, CreateFocusListener, "createFocusListener");
   pragma Import (Java, CreateMouseInputListener, "createMouseInputListener");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, Paint, "paint");

end Javax.Swing.Plaf.Basic.BasicTableUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTableUI, "javax.swing.plaf.basic.BasicTableUI");
pragma Extensions_Allowed (Off);
