pragma Extensions_Allowed (On);
with Java.Awt.Event.FocusListener;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Plaf.UIResource;
with Javax.Swing.Text.Caret;
with Javax.Swing.Text.DefaultCaret;

package Javax.Swing.Plaf.Basic.BasicTextUI.BasicCaret is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            FocusListener_I : Java.Awt.Event.FocusListener.Ref;
            MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref;
            Caret_I : Javax.Swing.Text.Caret.Ref)
    is new Javax.Swing.Text.DefaultCaret.Typ(Shape_I,
                                             FocusListener_I,
                                             MouseListener_I,
                                             MouseMotionListener_I,
                                             Serializable_I,
                                             Cloneable_I,
                                             Caret_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicCaret (This : Ref := null)
                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicCaret);

end Javax.Swing.Plaf.Basic.BasicTextUI.BasicCaret;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTextUI.BasicCaret, "javax.swing.plaf.basic.BasicTextUI$BasicCaret");
pragma Extensions_Allowed (Off);
