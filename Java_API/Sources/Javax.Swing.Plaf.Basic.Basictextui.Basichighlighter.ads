pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.Plaf.UIResource;
with Javax.Swing.Text.DefaultHighlighter;
with Javax.Swing.Text.Highlighter;

package Javax.Swing.Plaf.Basic.BasicTextUI.BasicHighlighter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(UIResource_I : Javax.Swing.Plaf.UIResource.Ref;
            Highlighter_I : Javax.Swing.Text.Highlighter.Ref)
    is new Javax.Swing.Text.DefaultHighlighter.Typ(Highlighter_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicHighlighter (This : Ref := null)
                                  return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicHighlighter);

end Javax.Swing.Plaf.Basic.BasicTextUI.BasicHighlighter;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTextUI.BasicHighlighter, "javax.swing.plaf.basic.BasicTextUI$BasicHighlighter");
pragma Extensions_Allowed (Off);
