pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Text.Caret;
limited with Javax.Swing.Text.EditorKit;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Highlighter;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.Keymap;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Plaf.TextUI;
with Javax.Swing.Text.ViewFactory;

package Javax.Swing.Plaf.Basic.BasicTextUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ViewFactory_I : Javax.Swing.Text.ViewFactory.Ref)
    is abstract new Javax.Swing.Plaf.TextUI.Typ
      with null record;

   function New_BasicTextUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateCaret (This : access Typ)
                         return access Javax.Swing.Text.Caret.Typ'Class;

   --  protected
   function CreateHighlighter (This : access Typ)
                               return access Javax.Swing.Text.Highlighter.Typ'Class;

   --  protected
   function GetKeymapName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   --  protected
   function CreateKeymap (This : access Typ)
                          return access Javax.Swing.Text.Keymap.Typ'Class;

   --  protected
   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   procedure PaintBackground (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  final  protected
   function GetComponent (This : access Typ)
                          return access Javax.Swing.Text.JTextComponent.Typ'Class;

   --  protected
   procedure ModelChanged (This : access Typ);

   --  final  protected
   procedure SetView (This : access Typ;
                      P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   --  protected
   procedure PaintSafely (This : access Typ;
                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  final
   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function GetVisibleEditorRect (This : access Typ)
                                  return access Java.Awt.Rectangle.Typ'Class;

   function ModelToView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int)
                         return access Java.Awt.Rectangle.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ModelToView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Rectangle.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                         return Java.Int;

   function ViewToModel (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Point : access Standard.Java.Awt.Point.Typ'Class;
                         P3_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure DamageRange (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int);

   procedure DamageRange (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                          P5_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class);

   function GetEditorKit (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class)
                          return access Javax.Swing.Text.EditorKit.Typ'Class;

   function GetRootView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class)
                         return access Javax.Swing.Text.View.Typ'Class;

   function GetToolTipText (This : access Typ;
                            P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                            P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function Create (This : access Typ;
                    P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                    return access Javax.Swing.Text.View.Typ'Class;

   function Create (This : access Typ;
                    P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int)
                    return access Javax.Swing.Text.View.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicTextUI);
   pragma Import (Java, CreateCaret, "createCaret");
   pragma Import (Java, CreateHighlighter, "createHighlighter");
   pragma Import (Java, GetKeymapName, "getKeymapName");
   pragma Import (Java, CreateKeymap, "createKeymap");
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Export (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, PaintBackground, "paintBackground");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, ModelChanged, "modelChanged");
   pragma Import (Java, SetView, "setView");
   pragma Import (Java, PaintSafely, "paintSafely");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, Update, "update");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetVisibleEditorRect, "getVisibleEditorRect");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");
   pragma Import (Java, DamageRange, "damageRange");
   pragma Import (Java, GetEditorKit, "getEditorKit");
   pragma Import (Java, GetRootView, "getRootView");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, Create, "create");

end Javax.Swing.Plaf.Basic.BasicTextUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTextUI, "javax.swing.plaf.basic.BasicTextUI");
pragma Extensions_Allowed (Off);
