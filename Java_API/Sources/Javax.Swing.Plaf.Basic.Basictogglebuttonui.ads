pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicButtonUI;

package Javax.Swing.Plaf.Basic.BasicToggleButtonUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicButtonUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicToggleButtonUI (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintIcon (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class;
                        P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function GetTextShiftOffset (This : access Typ)
                                return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicToggleButtonUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintIcon, "paintIcon");
   pragma Import (Java, GetTextShiftOffset, "getTextShiftOffset");

end Javax.Swing.Plaf.Basic.BasicToggleButtonUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicToggleButtonUI, "javax.swing.plaf.basic.BasicToggleButtonUI");
pragma Extensions_Allowed (Off);
