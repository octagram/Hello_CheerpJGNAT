pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Point;
limited with Javax.Swing.JToolBar;
with Java.Lang.Object;
with Javax.Swing.Event.MouseInputListener;

package Javax.Swing.Plaf.Basic.BasicToolBarUI.DockingListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseInputListener_I : Javax.Swing.Event.MouseInputListener.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ToolBar : access Javax.Swing.JToolBar.Typ'Class;
      pragma Import (Java, ToolBar, "toolBar");

      --  protected
      IsDragging : Java.Boolean;
      pragma Import (Java, IsDragging, "isDragging");

      --  protected
      Origin : access Java.Awt.Point.Typ'Class;
      pragma Import (Java, Origin, "origin");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DockingListener (P1_BasicToolBarUI : access Standard.Javax.Swing.Plaf.Basic.BasicToolBarUI.Typ'Class;
                                 P2_JToolBar : access Standard.Javax.Swing.JToolBar.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DockingListener);
   pragma Import (Java, MouseClicked, "mouseClicked");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MouseEntered, "mouseEntered");
   pragma Import (Java, MouseExited, "mouseExited");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");

end Javax.Swing.Plaf.Basic.BasicToolBarUI.DockingListener;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicToolBarUI.DockingListener, "javax.swing.plaf.basic.BasicToolBarUI$DockingListener");
pragma Extensions_Allowed (Off);
