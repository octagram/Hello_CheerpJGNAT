pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Event.ContainerListener;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Event.WindowListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JFrame;
limited with Javax.Swing.JToolBar;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.RootPaneContainer;
with Java.Lang.Object;
with Javax.Swing.Plaf.ToolBarUI;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Basic.BasicToolBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Plaf.ToolBarUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ToolBar : access Javax.Swing.JToolBar.Typ'Class;
      pragma Import (Java, ToolBar, "toolBar");

      --  protected
      FocusedCompIndex : Java.Int;
      pragma Import (Java, FocusedCompIndex, "focusedCompIndex");

      --  protected
      DockingColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, DockingColor, "dockingColor");

      --  protected
      FloatingColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, FloatingColor, "floatingColor");

      --  protected
      DockingBorderColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, DockingBorderColor, "dockingBorderColor");

      --  protected
      FloatingBorderColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, FloatingBorderColor, "floatingBorderColor");

      --  protected
      DockingListener : access Javax.Swing.Event.MouseInputListener.Typ'Class;
      pragma Import (Java, DockingListener, "dockingListener");

      --  protected
      PropertyListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, PropertyListener, "propertyListener");

      --  protected
      ToolBarContListener : access Java.Awt.Event.ContainerListener.Typ'Class;
      pragma Import (Java, ToolBarContListener, "toolBarContListener");

      --  protected
      ToolBarFocusListener : access Java.Awt.Event.FocusListener.Typ'Class;
      pragma Import (Java, ToolBarFocusListener, "toolBarFocusListener");

      --  protected
      ConstraintBeforeFloating : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ConstraintBeforeFloating, "constraintBeforeFloating");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicToolBarUI (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   procedure NavigateFocusedComp (This : access Typ;
                                  P1_Int : Java.Int);

   --  protected
   function CreateRolloverBorder (This : access Typ)
                                  return access Javax.Swing.Border.Border.Typ'Class;

   --  protected
   function CreateNonRolloverBorder (This : access Typ)
                                     return access Javax.Swing.Border.Border.Typ'Class;

   --  protected
   function CreateFloatingFrame (This : access Typ;
                                 P1_JToolBar : access Standard.Javax.Swing.JToolBar.Typ'Class)
                                 return access Javax.Swing.JFrame.Typ'Class;

   --  protected
   function CreateFloatingWindow (This : access Typ;
                                  P1_JToolBar : access Standard.Javax.Swing.JToolBar.Typ'Class)
                                  return access Javax.Swing.RootPaneContainer.Typ'Class;

   function IsRolloverBorders (This : access Typ)
                               return Java.Boolean;

   procedure SetRolloverBorders (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   --  protected
   procedure InstallRolloverBorders (This : access Typ;
                                     P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallNonRolloverBorders (This : access Typ;
                                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallNormalBorders (This : access Typ;
                                   P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure SetBorderToRollover (This : access Typ;
                                  P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   function GetRolloverBorder (This : access Typ;
                               P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class)
                               return access Javax.Swing.Border.Border.Typ'Class;

   --  protected
   procedure SetBorderToNonRollover (This : access Typ;
                                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   function GetNonRolloverBorder (This : access Typ;
                                  P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class)
                                  return access Javax.Swing.Border.Border.Typ'Class;

   --  protected
   procedure SetBorderToNormal (This : access Typ;
                                P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetFloatingLocation (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int);

   function IsFloating (This : access Typ)
                        return Java.Boolean;

   procedure SetFloating (This : access Typ;
                          P1_Boolean : Java.Boolean;
                          P2_Point : access Standard.Java.Awt.Point.Typ'Class);

   procedure SetOrientation (This : access Typ;
                             P1_Int : Java.Int);

   function GetDockingColor (This : access Typ)
                             return access Java.Awt.Color.Typ'Class;

   procedure SetDockingColor (This : access Typ;
                              P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetFloatingColor (This : access Typ)
                              return access Java.Awt.Color.Typ'Class;

   procedure SetFloatingColor (This : access Typ;
                               P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function CanDock (This : access Typ;
                     P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                     P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                     return Java.Boolean;

   --  protected
   procedure DragTo (This : access Typ;
                     P1_Point : access Standard.Java.Awt.Point.Typ'Class;
                     P2_Point : access Standard.Java.Awt.Point.Typ'Class);

   --  protected
   procedure FloatAt (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class;
                      P2_Point : access Standard.Java.Awt.Point.Typ'Class);

   --  protected
   function CreateToolBarContListener (This : access Typ)
                                       return access Java.Awt.Event.ContainerListener.Typ'Class;

   --  protected
   function CreateToolBarFocusListener (This : access Typ)
                                        return access Java.Awt.Event.FocusListener.Typ'Class;

   --  protected
   function CreatePropertyListener (This : access Typ)
                                    return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateDockingListener (This : access Typ)
                                   return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   --  protected
   function CreateFrameListener (This : access Typ)
                                 return access Java.Awt.Event.WindowListener.Typ'Class;

   --  protected
   procedure PaintDragWindow (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BasicToolBarUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, NavigateFocusedComp, "navigateFocusedComp");
   pragma Import (Java, CreateRolloverBorder, "createRolloverBorder");
   pragma Import (Java, CreateNonRolloverBorder, "createNonRolloverBorder");
   pragma Import (Java, CreateFloatingFrame, "createFloatingFrame");
   pragma Import (Java, CreateFloatingWindow, "createFloatingWindow");
   pragma Import (Java, IsRolloverBorders, "isRolloverBorders");
   pragma Import (Java, SetRolloverBorders, "setRolloverBorders");
   pragma Import (Java, InstallRolloverBorders, "installRolloverBorders");
   pragma Import (Java, InstallNonRolloverBorders, "installNonRolloverBorders");
   pragma Import (Java, InstallNormalBorders, "installNormalBorders");
   pragma Import (Java, SetBorderToRollover, "setBorderToRollover");
   pragma Import (Java, GetRolloverBorder, "getRolloverBorder");
   pragma Import (Java, SetBorderToNonRollover, "setBorderToNonRollover");
   pragma Import (Java, GetNonRolloverBorder, "getNonRolloverBorder");
   pragma Import (Java, SetBorderToNormal, "setBorderToNormal");
   pragma Import (Java, SetFloatingLocation, "setFloatingLocation");
   pragma Import (Java, IsFloating, "isFloating");
   pragma Import (Java, SetFloating, "setFloating");
   pragma Import (Java, SetOrientation, "setOrientation");
   pragma Import (Java, GetDockingColor, "getDockingColor");
   pragma Import (Java, SetDockingColor, "setDockingColor");
   pragma Import (Java, GetFloatingColor, "getFloatingColor");
   pragma Import (Java, SetFloatingColor, "setFloatingColor");
   pragma Import (Java, CanDock, "canDock");
   pragma Import (Java, DragTo, "dragTo");
   pragma Import (Java, FloatAt, "floatAt");
   pragma Import (Java, CreateToolBarContListener, "createToolBarContListener");
   pragma Import (Java, CreateToolBarFocusListener, "createToolBarFocusListener");
   pragma Import (Java, CreatePropertyListener, "createPropertyListener");
   pragma Import (Java, CreateDockingListener, "createDockingListener");
   pragma Import (Java, CreateFrameListener, "createFrameListener");
   pragma Import (Java, PaintDragWindow, "paintDragWindow");

end Javax.Swing.Plaf.Basic.BasicToolBarUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicToolBarUI, "javax.swing.plaf.basic.BasicToolBarUI");
pragma Extensions_Allowed (Off);
