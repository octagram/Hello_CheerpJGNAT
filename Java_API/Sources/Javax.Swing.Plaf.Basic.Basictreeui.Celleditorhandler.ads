pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ChangeEvent;
with Java.Lang.Object;
with Javax.Swing.Event.CellEditorListener;

package Javax.Swing.Plaf.Basic.BasicTreeUI.CellEditorHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CellEditorListener_I : Javax.Swing.Event.CellEditorListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CellEditorHandler (P1_BasicTreeUI : access Standard.Javax.Swing.Plaf.Basic.BasicTreeUI.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure EditingStopped (This : access Typ;
                             P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);

   procedure EditingCanceled (This : access Typ;
                              P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CellEditorHandler);
   pragma Import (Java, EditingStopped, "editingStopped");
   pragma Import (Java, EditingCanceled, "editingCanceled");

end Javax.Swing.Plaf.Basic.BasicTreeUI.CellEditorHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI.CellEditorHandler, "javax.swing.plaf.basic.BasicTreeUI$CellEditorHandler");
pragma Extensions_Allowed (Off);
