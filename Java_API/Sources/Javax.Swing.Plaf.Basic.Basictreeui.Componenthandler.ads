pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ComponentEvent;
limited with Javax.Swing.JScrollBar;
limited with Javax.Swing.JScrollPane;
limited with Javax.Swing.Timer;
with Java.Awt.Event.ActionListener;
with Java.Awt.Event.ComponentAdapter;
with Java.Awt.Event.ComponentListener;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.BasicTreeUI.ComponentHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ActionListener_I : Java.Awt.Event.ActionListener.Ref;
            ComponentListener_I : Java.Awt.Event.ComponentListener.Ref)
    is new Java.Awt.Event.ComponentAdapter.Typ(ComponentListener_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Timer : access Javax.Swing.Timer.Typ'Class;
      pragma Import (Java, Timer, "timer");

      --  protected
      ScrollBar : access Javax.Swing.JScrollBar.Typ'Class;
      pragma Import (Java, ScrollBar, "scrollBar");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ComponentHandler (P1_BasicTreeUI : access Standard.Javax.Swing.Plaf.Basic.BasicTreeUI.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ComponentMoved (This : access Typ;
                             P1_ComponentEvent : access Standard.Java.Awt.Event.ComponentEvent.Typ'Class);

   --  protected
   procedure StartTimer (This : access Typ);

   --  protected
   function GetScrollPane (This : access Typ)
                           return access Javax.Swing.JScrollPane.Typ'Class;

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComponentHandler);
   pragma Import (Java, ComponentMoved, "componentMoved");
   pragma Import (Java, StartTimer, "startTimer");
   pragma Import (Java, GetScrollPane, "getScrollPane");
   pragma Import (Java, ActionPerformed, "actionPerformed");

end Javax.Swing.Plaf.Basic.BasicTreeUI.ComponentHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI.ComponentHandler, "javax.swing.plaf.basic.BasicTreeUI$ComponentHandler");
pragma Extensions_Allowed (Off);
