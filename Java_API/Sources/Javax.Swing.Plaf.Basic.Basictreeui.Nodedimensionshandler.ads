pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
with Java.Lang.Object;
with Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions;

package Javax.Swing.Plaf.Basic.BasicTreeUI.NodeDimensionsHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NodeDimensionsHandler (P1_BasicTreeUI : access Standard.Javax.Swing.Plaf.Basic.BasicTreeUI.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNodeDimensions (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Boolean : Java.Boolean;
                               P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function GetRowX (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NodeDimensionsHandler);
   pragma Import (Java, GetNodeDimensions, "getNodeDimensions");
   pragma Import (Java, GetRowX, "getRowX");

end Javax.Swing.Plaf.Basic.BasicTreeUI.NodeDimensionsHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI.NodeDimensionsHandler, "javax.swing.plaf.basic.BasicTreeUI$NodeDimensionsHandler");
pragma Extensions_Allowed (Off);
