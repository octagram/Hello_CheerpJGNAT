pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.AbstractAction;
with Javax.Swing.Action;

package Javax.Swing.Plaf.Basic.BasicTreeUI.TreeCancelEditingAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Action_I : Javax.Swing.Action.Ref)
    is new Javax.Swing.AbstractAction.Typ(Serializable_I,
                                          Cloneable_I,
                                          Action_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeCancelEditingAction (P1_BasicTreeUI : access Standard.Javax.Swing.Plaf.Basic.BasicTreeUI.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   function IsEnabled (This : access Typ)
                       return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeCancelEditingAction);
   pragma Import (Java, ActionPerformed, "actionPerformed");
   pragma Import (Java, IsEnabled, "isEnabled");

end Javax.Swing.Plaf.Basic.BasicTreeUI.TreeCancelEditingAction;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI.TreeCancelEditingAction, "javax.swing.plaf.basic.BasicTreeUI$TreeCancelEditingAction");
pragma Extensions_Allowed (Off);
