pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.TreeExpansionEvent;
with Java.Lang.Object;
with Javax.Swing.Event.TreeExpansionListener;

package Javax.Swing.Plaf.Basic.BasicTreeUI.TreeExpansionHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TreeExpansionListener_I : Javax.Swing.Event.TreeExpansionListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeExpansionHandler (P1_BasicTreeUI : access Standard.Javax.Swing.Plaf.Basic.BasicTreeUI.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure TreeExpanded (This : access Typ;
                           P1_TreeExpansionEvent : access Standard.Javax.Swing.Event.TreeExpansionEvent.Typ'Class);

   procedure TreeCollapsed (This : access Typ;
                            P1_TreeExpansionEvent : access Standard.Javax.Swing.Event.TreeExpansionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeExpansionHandler);
   pragma Import (Java, TreeExpanded, "treeExpanded");
   pragma Import (Java, TreeCollapsed, "treeCollapsed");

end Javax.Swing.Plaf.Basic.BasicTreeUI.TreeExpansionHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI.TreeExpansionHandler, "javax.swing.plaf.basic.BasicTreeUI$TreeExpansionHandler");
pragma Extensions_Allowed (Off);
