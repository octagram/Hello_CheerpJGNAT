pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.TreeModelEvent;
with Java.Lang.Object;
with Javax.Swing.Event.TreeModelListener;

package Javax.Swing.Plaf.Basic.BasicTreeUI.TreeModelHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TreeModelListener_I : Javax.Swing.Event.TreeModelListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeModelHandler (P1_BasicTreeUI : access Standard.Javax.Swing.Plaf.Basic.BasicTreeUI.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure TreeNodesChanged (This : access Typ;
                               P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);

   procedure TreeNodesInserted (This : access Typ;
                                P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);

   procedure TreeNodesRemoved (This : access Typ;
                               P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);

   procedure TreeStructureChanged (This : access Typ;
                                   P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeModelHandler);
   pragma Import (Java, TreeNodesChanged, "treeNodesChanged");
   pragma Import (Java, TreeNodesInserted, "treeNodesInserted");
   pragma Import (Java, TreeNodesRemoved, "treeNodesRemoved");
   pragma Import (Java, TreeStructureChanged, "treeStructureChanged");

end Javax.Swing.Plaf.Basic.BasicTreeUI.TreeModelHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI.TreeModelHandler, "javax.swing.plaf.basic.BasicTreeUI$TreeModelHandler");
pragma Extensions_Allowed (Off);
