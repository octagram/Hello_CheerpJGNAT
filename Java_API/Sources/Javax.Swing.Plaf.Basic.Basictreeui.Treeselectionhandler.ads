pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.TreeSelectionEvent;
with Java.Lang.Object;
with Javax.Swing.Event.TreeSelectionListener;

package Javax.Swing.Plaf.Basic.BasicTreeUI.TreeSelectionHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TreeSelectionListener_I : Javax.Swing.Event.TreeSelectionListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeSelectionHandler (P1_BasicTreeUI : access Standard.Javax.Swing.Plaf.Basic.BasicTreeUI.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ValueChanged (This : access Typ;
                           P1_TreeSelectionEvent : access Standard.Javax.Swing.Event.TreeSelectionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeSelectionHandler);
   pragma Import (Java, ValueChanged, "valueChanged");

end Javax.Swing.Plaf.Basic.BasicTreeUI.TreeSelectionHandler;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI.TreeSelectionHandler, "javax.swing.plaf.basic.BasicTreeUI$TreeSelectionHandler");
pragma Extensions_Allowed (Off);
