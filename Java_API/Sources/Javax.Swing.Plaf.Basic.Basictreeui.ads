pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Component.BaselineResizeBehavior;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.ComponentListener;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Event.KeyListener;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Event.MouseListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Util.Hashtable;
limited with Javax.Swing.CellRendererPane;
limited with Javax.Swing.Event.CellEditorListener;
limited with Javax.Swing.Event.TreeExpansionListener;
limited with Javax.Swing.Event.TreeModelListener;
limited with Javax.Swing.Event.TreeSelectionListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JTree;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Tree.AbstractLayoutCache;
limited with Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions;
limited with Javax.Swing.Tree.TreeCellEditor;
limited with Javax.Swing.Tree.TreeCellRenderer;
limited with Javax.Swing.Tree.TreeModel;
limited with Javax.Swing.Tree.TreePath;
limited with Javax.Swing.Tree.TreeSelectionModel;
with Java.Lang.Object;
with Javax.Swing.Plaf.TreeUI;

package Javax.Swing.Plaf.Basic.BasicTreeUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.TreeUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      CollapsedIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, CollapsedIcon, "collapsedIcon");

      --  protected
      ExpandedIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, ExpandedIcon, "expandedIcon");

      --  protected
      LeftChildIndent : Java.Int;
      pragma Import (Java, LeftChildIndent, "leftChildIndent");

      --  protected
      RightChildIndent : Java.Int;
      pragma Import (Java, RightChildIndent, "rightChildIndent");

      --  protected
      TotalChildIndent : Java.Int;
      pragma Import (Java, TotalChildIndent, "totalChildIndent");

      --  protected
      PreferredMinSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, PreferredMinSize, "preferredMinSize");

      --  protected
      LastSelectedRow : Java.Int;
      pragma Import (Java, LastSelectedRow, "lastSelectedRow");

      --  protected
      Tree : access Javax.Swing.JTree.Typ'Class;
      pragma Import (Java, Tree, "tree");

      --  protected
      CurrentCellRenderer : access Javax.Swing.Tree.TreeCellRenderer.Typ'Class;
      pragma Import (Java, CurrentCellRenderer, "currentCellRenderer");

      --  protected
      CreatedRenderer : Java.Boolean;
      pragma Import (Java, CreatedRenderer, "createdRenderer");

      --  protected
      CellEditor : access Javax.Swing.Tree.TreeCellEditor.Typ'Class;
      pragma Import (Java, CellEditor, "cellEditor");

      --  protected
      CreatedCellEditor : Java.Boolean;
      pragma Import (Java, CreatedCellEditor, "createdCellEditor");

      --  protected
      StopEditingInCompleteEditing : Java.Boolean;
      pragma Import (Java, StopEditingInCompleteEditing, "stopEditingInCompleteEditing");

      --  protected
      RendererPane : access Javax.Swing.CellRendererPane.Typ'Class;
      pragma Import (Java, RendererPane, "rendererPane");

      --  protected
      PreferredSize : access Java.Awt.Dimension.Typ'Class;
      pragma Import (Java, PreferredSize, "preferredSize");

      --  protected
      ValidCachedPreferredSize : Java.Boolean;
      pragma Import (Java, ValidCachedPreferredSize, "validCachedPreferredSize");

      --  protected
      TreeState : access Javax.Swing.Tree.AbstractLayoutCache.Typ'Class;
      pragma Import (Java, TreeState, "treeState");

      --  protected
      DrawingCache : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, DrawingCache, "drawingCache");

      --  protected
      LargeModel : Java.Boolean;
      pragma Import (Java, LargeModel, "largeModel");

      --  protected
      NodeDimensions : access Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions.Typ'Class;
      pragma Import (Java, NodeDimensions, "nodeDimensions");

      --  protected
      TreeModel : access Javax.Swing.Tree.TreeModel.Typ'Class;
      pragma Import (Java, TreeModel, "treeModel");

      --  protected
      TreeSelectionModel : access Javax.Swing.Tree.TreeSelectionModel.Typ'Class;
      pragma Import (Java, TreeSelectionModel, "treeSelectionModel");

      --  protected
      DepthOffset : Java.Int;
      pragma Import (Java, DepthOffset, "depthOffset");

      --  protected
      EditingComponent : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, EditingComponent, "editingComponent");

      --  protected
      EditingPath : access Javax.Swing.Tree.TreePath.Typ'Class;
      pragma Import (Java, EditingPath, "editingPath");

      --  protected
      EditingRow : Java.Int;
      pragma Import (Java, EditingRow, "editingRow");

      --  protected
      EditorHasDifferentSize : Java.Boolean;
      pragma Import (Java, EditorHasDifferentSize, "editorHasDifferentSize");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BasicTreeUI (This : Ref := null)
                             return Ref;

   --  protected
   function GetHashColor (This : access Typ)
                          return access Java.Awt.Color.Typ'Class;

   --  protected
   procedure SetHashColor (This : access Typ;
                           P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure SetLeftChildIndent (This : access Typ;
                                 P1_Int : Java.Int);

   function GetLeftChildIndent (This : access Typ)
                                return Java.Int;

   procedure SetRightChildIndent (This : access Typ;
                                  P1_Int : Java.Int);

   function GetRightChildIndent (This : access Typ)
                                 return Java.Int;

   procedure SetExpandedIcon (This : access Typ;
                              P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetExpandedIcon (This : access Typ)
                             return access Javax.Swing.Icon.Typ'Class;

   procedure SetCollapsedIcon (This : access Typ;
                               P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetCollapsedIcon (This : access Typ)
                              return access Javax.Swing.Icon.Typ'Class;

   --  protected
   procedure SetLargeModel (This : access Typ;
                            P1_Boolean : Java.Boolean);

   --  protected
   function IsLargeModel (This : access Typ)
                          return Java.Boolean;

   --  protected
   procedure SetRowHeight (This : access Typ;
                           P1_Int : Java.Int);

   --  protected
   function GetRowHeight (This : access Typ)
                          return Java.Int;

   --  protected
   procedure SetCellRenderer (This : access Typ;
                              P1_TreeCellRenderer : access Standard.Javax.Swing.Tree.TreeCellRenderer.Typ'Class);

   --  protected
   function GetCellRenderer (This : access Typ)
                             return access Javax.Swing.Tree.TreeCellRenderer.Typ'Class;

   --  protected
   procedure SetModel (This : access Typ;
                       P1_TreeModel : access Standard.Javax.Swing.Tree.TreeModel.Typ'Class);

   --  protected
   function GetModel (This : access Typ)
                      return access Javax.Swing.Tree.TreeModel.Typ'Class;

   --  protected
   procedure SetRootVisible (This : access Typ;
                             P1_Boolean : Java.Boolean);

   --  protected
   function IsRootVisible (This : access Typ)
                           return Java.Boolean;

   --  protected
   procedure SetShowsRootHandles (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   --  protected
   function GetShowsRootHandles (This : access Typ)
                                 return Java.Boolean;

   --  protected
   procedure SetCellEditor (This : access Typ;
                            P1_TreeCellEditor : access Standard.Javax.Swing.Tree.TreeCellEditor.Typ'Class);

   --  protected
   function GetCellEditor (This : access Typ)
                           return access Javax.Swing.Tree.TreeCellEditor.Typ'Class;

   --  protected
   procedure SetEditable (This : access Typ;
                          P1_Boolean : Java.Boolean);

   --  protected
   function IsEditable (This : access Typ)
                        return Java.Boolean;

   --  protected
   procedure SetSelectionModel (This : access Typ;
                                P1_TreeSelectionModel : access Standard.Javax.Swing.Tree.TreeSelectionModel.Typ'Class);

   --  protected
   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.Tree.TreeSelectionModel.Typ'Class;

   function GetPathBounds (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return access Java.Awt.Rectangle.Typ'Class;

   function GetPathForRow (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_Int : Java.Int)
                           return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetRowForPath (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return Java.Int;

   function GetRowCount (This : access Typ;
                         P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                         return Java.Int;

   function GetClosestPathForLocation (This : access Typ;
                                       P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int)
                                       return access Javax.Swing.Tree.TreePath.Typ'Class;

   function IsEditing (This : access Typ;
                       P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                       return Java.Boolean;

   function StopEditing (This : access Typ;
                         P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                         return Java.Boolean;

   procedure CancelEditing (This : access Typ;
                            P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class);

   procedure StartEditingAtPath (This : access Typ;
                                 P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                 P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   function GetEditingPath (This : access Typ;
                            P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                            return access Javax.Swing.Tree.TreePath.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PrepareForUIInstall (This : access Typ);

   --  protected
   procedure CompleteUIInstall (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   function CreateNodeDimensions (This : access Typ)
                                  return access Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions.Typ'Class;

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateMouseListener (This : access Typ)
                                 return access Java.Awt.Event.MouseListener.Typ'Class;

   --  protected
   function CreateFocusListener (This : access Typ)
                                 return access Java.Awt.Event.FocusListener.Typ'Class;

   --  protected
   function CreateKeyListener (This : access Typ)
                               return access Java.Awt.Event.KeyListener.Typ'Class;

   --  protected
   function CreateSelectionModelPropertyChangeListener (This : access Typ)
                                                        return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateTreeSelectionListener (This : access Typ)
                                         return access Javax.Swing.Event.TreeSelectionListener.Typ'Class;

   --  protected
   function CreateCellEditorListener (This : access Typ)
                                      return access Javax.Swing.Event.CellEditorListener.Typ'Class;

   --  protected
   function CreateComponentListener (This : access Typ)
                                     return access Java.Awt.Event.ComponentListener.Typ'Class;

   --  protected
   function CreateTreeExpansionListener (This : access Typ)
                                         return access Javax.Swing.Event.TreeExpansionListener.Typ'Class;

   --  protected
   function CreateLayoutCache (This : access Typ)
                               return access Javax.Swing.Tree.AbstractLayoutCache.Typ'Class;

   --  protected
   function CreateCellRendererPane (This : access Typ)
                                    return access Javax.Swing.CellRendererPane.Typ'Class;

   --  protected
   function CreateDefaultCellEditor (This : access Typ)
                                     return access Javax.Swing.Tree.TreeCellEditor.Typ'Class;

   --  protected
   function CreateDefaultCellRenderer (This : access Typ)
                                       return access Javax.Swing.Tree.TreeCellRenderer.Typ'Class;

   --  protected
   function CreateTreeModelListener (This : access Typ)
                                     return access Javax.Swing.Event.TreeModelListener.Typ'Class;

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PrepareForUIUninstall (This : access Typ);

   --  protected
   procedure CompleteUIUninstall (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   function GetBaselineResizeBehavior (This : access Typ;
                                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                       return access Java.Awt.Component.BaselineResizeBehavior.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintHorizontalPartOfLeg (This : access Typ;
                                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                       P3_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                                       P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                       P5_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                       P6_Int : Java.Int;
                                       P7_Boolean : Java.Boolean;
                                       P8_Boolean : Java.Boolean;
                                       P9_Boolean : Java.Boolean);

   --  protected
   procedure PaintVerticalPartOfLeg (This : access Typ;
                                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                     P3_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                                     P4_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   --  protected
   procedure PaintExpandControl (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P3_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                                 P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P5_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                 P6_Int : Java.Int;
                                 P7_Boolean : Java.Boolean;
                                 P8_Boolean : Java.Boolean;
                                 P9_Boolean : Java.Boolean);

   --  protected
   procedure PaintRow (This : access Typ;
                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                       P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                       P3_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                       P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                       P5_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                       P6_Int : Java.Int;
                       P7_Boolean : Java.Boolean;
                       P8_Boolean : Java.Boolean;
                       P9_Boolean : Java.Boolean);

   --  protected
   function ShouldPaintExpandControl (This : access Typ;
                                      P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                      P2_Int : Java.Int;
                                      P3_Boolean : Java.Boolean;
                                      P4_Boolean : Java.Boolean;
                                      P5_Boolean : Java.Boolean)
                                      return Java.Boolean;

   --  protected
   procedure PaintVerticalLine (This : access Typ;
                                P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int);

   --  protected
   procedure PaintHorizontalLine (This : access Typ;
                                  P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int);

   --  protected
   function GetVerticalLegBuffer (This : access Typ)
                                  return Java.Int;

   --  protected
   function GetHorizontalLegBuffer (This : access Typ)
                                    return Java.Int;

   --  protected
   procedure DrawCentered (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                           P3_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                           P4_Int : Java.Int;
                           P5_Int : Java.Int);

   --  protected
   procedure DrawDashedHorizontalLine (This : access Typ;
                                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int);

   --  protected
   procedure DrawDashedVerticalLine (This : access Typ;
                                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int);

   --  protected
   function GetRowX (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return Java.Int;

   --  protected
   procedure UpdateLayoutCacheExpandedNodes (This : access Typ);

   --  protected
   procedure UpdateExpandedDescendants (This : access Typ;
                                        P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   --  protected
   function GetLastChildPath (This : access Typ;
                              P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                              return access Javax.Swing.Tree.TreePath.Typ'Class;

   --  protected
   procedure UpdateDepthOffset (This : access Typ);

   --  protected
   procedure UpdateCellEditor (This : access Typ);

   --  protected
   procedure UpdateRenderer (This : access Typ);

   --  protected
   procedure ConfigureLayoutCache (This : access Typ);

   --  protected
   procedure UpdateSize (This : access Typ);

   --  protected
   procedure UpdateCachedPreferredSize (This : access Typ);

   --  protected
   procedure PathWasExpanded (This : access Typ;
                              P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   --  protected
   procedure PathWasCollapsed (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   --  protected
   procedure EnsureRowsAreVisible (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure SetPreferredMinSize (This : access Typ;
                                  P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetPreferredMinSize (This : access Typ)
                                 return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                              P2_Boolean : Java.Boolean)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure CompleteEditing (This : access Typ);

   --  protected
   procedure CompleteEditing (This : access Typ;
                              P1_Boolean : Java.Boolean;
                              P2_Boolean : Java.Boolean;
                              P3_Boolean : Java.Boolean);

   --  protected
   function StartEditing (This : access Typ;
                          P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                          P2_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                          return Java.Boolean;

   --  protected
   procedure CheckForClickInExpandControl (This : access Typ;
                                           P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                           P2_Int : Java.Int;
                                           P3_Int : Java.Int);

   --  protected
   function IsLocationInExpandControl (This : access Typ;
                                       P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int)
                                       return Java.Boolean;

   --  protected
   procedure HandleExpandControlClick (This : access Typ;
                                       P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int);

   --  protected
   procedure ToggleExpandState (This : access Typ;
                                P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   --  protected
   function IsToggleSelectionEvent (This : access Typ;
                                    P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                                    return Java.Boolean;

   --  protected
   function IsMultiSelectEvent (This : access Typ;
                                P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                                return Java.Boolean;

   --  protected
   function IsToggleEvent (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                           return Java.Boolean;

   --  protected
   procedure SelectPathForEvent (This : access Typ;
                                 P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                 P2_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   --  protected
   function IsLeaf (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_BasicTreeUI);
   pragma Import (Java, GetHashColor, "getHashColor");
   pragma Import (Java, SetHashColor, "setHashColor");
   pragma Import (Java, SetLeftChildIndent, "setLeftChildIndent");
   pragma Import (Java, GetLeftChildIndent, "getLeftChildIndent");
   pragma Import (Java, SetRightChildIndent, "setRightChildIndent");
   pragma Import (Java, GetRightChildIndent, "getRightChildIndent");
   pragma Import (Java, SetExpandedIcon, "setExpandedIcon");
   pragma Import (Java, GetExpandedIcon, "getExpandedIcon");
   pragma Import (Java, SetCollapsedIcon, "setCollapsedIcon");
   pragma Import (Java, GetCollapsedIcon, "getCollapsedIcon");
   pragma Import (Java, SetLargeModel, "setLargeModel");
   pragma Import (Java, IsLargeModel, "isLargeModel");
   pragma Import (Java, SetRowHeight, "setRowHeight");
   pragma Import (Java, GetRowHeight, "getRowHeight");
   pragma Import (Java, SetCellRenderer, "setCellRenderer");
   pragma Import (Java, GetCellRenderer, "getCellRenderer");
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, GetModel, "getModel");
   pragma Import (Java, SetRootVisible, "setRootVisible");
   pragma Import (Java, IsRootVisible, "isRootVisible");
   pragma Import (Java, SetShowsRootHandles, "setShowsRootHandles");
   pragma Import (Java, GetShowsRootHandles, "getShowsRootHandles");
   pragma Import (Java, SetCellEditor, "setCellEditor");
   pragma Import (Java, GetCellEditor, "getCellEditor");
   pragma Import (Java, SetEditable, "setEditable");
   pragma Import (Java, IsEditable, "isEditable");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, GetPathBounds, "getPathBounds");
   pragma Import (Java, GetPathForRow, "getPathForRow");
   pragma Import (Java, GetRowForPath, "getRowForPath");
   pragma Import (Java, GetRowCount, "getRowCount");
   pragma Import (Java, GetClosestPathForLocation, "getClosestPathForLocation");
   pragma Import (Java, IsEditing, "isEditing");
   pragma Import (Java, StopEditing, "stopEditing");
   pragma Import (Java, CancelEditing, "cancelEditing");
   pragma Import (Java, StartEditingAtPath, "startEditingAtPath");
   pragma Import (Java, GetEditingPath, "getEditingPath");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, PrepareForUIInstall, "prepareForUIInstall");
   pragma Import (Java, CompleteUIInstall, "completeUIInstall");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, CreateNodeDimensions, "createNodeDimensions");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreateMouseListener, "createMouseListener");
   pragma Import (Java, CreateFocusListener, "createFocusListener");
   pragma Import (Java, CreateKeyListener, "createKeyListener");
   pragma Import (Java, CreateSelectionModelPropertyChangeListener, "createSelectionModelPropertyChangeListener");
   pragma Import (Java, CreateTreeSelectionListener, "createTreeSelectionListener");
   pragma Import (Java, CreateCellEditorListener, "createCellEditorListener");
   pragma Import (Java, CreateComponentListener, "createComponentListener");
   pragma Import (Java, CreateTreeExpansionListener, "createTreeExpansionListener");
   pragma Import (Java, CreateLayoutCache, "createLayoutCache");
   pragma Import (Java, CreateCellRendererPane, "createCellRendererPane");
   pragma Import (Java, CreateDefaultCellEditor, "createDefaultCellEditor");
   pragma Import (Java, CreateDefaultCellRenderer, "createDefaultCellRenderer");
   pragma Import (Java, CreateTreeModelListener, "createTreeModelListener");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, PrepareForUIUninstall, "prepareForUIUninstall");
   pragma Import (Java, CompleteUIUninstall, "completeUIUninstall");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, GetBaselineResizeBehavior, "getBaselineResizeBehavior");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintHorizontalPartOfLeg, "paintHorizontalPartOfLeg");
   pragma Import (Java, PaintVerticalPartOfLeg, "paintVerticalPartOfLeg");
   pragma Import (Java, PaintExpandControl, "paintExpandControl");
   pragma Import (Java, PaintRow, "paintRow");
   pragma Import (Java, ShouldPaintExpandControl, "shouldPaintExpandControl");
   pragma Import (Java, PaintVerticalLine, "paintVerticalLine");
   pragma Import (Java, PaintHorizontalLine, "paintHorizontalLine");
   pragma Import (Java, GetVerticalLegBuffer, "getVerticalLegBuffer");
   pragma Import (Java, GetHorizontalLegBuffer, "getHorizontalLegBuffer");
   pragma Import (Java, DrawCentered, "drawCentered");
   pragma Import (Java, DrawDashedHorizontalLine, "drawDashedHorizontalLine");
   pragma Import (Java, DrawDashedVerticalLine, "drawDashedVerticalLine");
   pragma Import (Java, GetRowX, "getRowX");
   pragma Import (Java, UpdateLayoutCacheExpandedNodes, "updateLayoutCacheExpandedNodes");
   pragma Import (Java, UpdateExpandedDescendants, "updateExpandedDescendants");
   pragma Import (Java, GetLastChildPath, "getLastChildPath");
   pragma Import (Java, UpdateDepthOffset, "updateDepthOffset");
   pragma Import (Java, UpdateCellEditor, "updateCellEditor");
   pragma Import (Java, UpdateRenderer, "updateRenderer");
   pragma Import (Java, ConfigureLayoutCache, "configureLayoutCache");
   pragma Import (Java, UpdateSize, "updateSize");
   pragma Import (Java, UpdateCachedPreferredSize, "updateCachedPreferredSize");
   pragma Import (Java, PathWasExpanded, "pathWasExpanded");
   pragma Import (Java, PathWasCollapsed, "pathWasCollapsed");
   pragma Import (Java, EnsureRowsAreVisible, "ensureRowsAreVisible");
   pragma Import (Java, SetPreferredMinSize, "setPreferredMinSize");
   pragma Import (Java, GetPreferredMinSize, "getPreferredMinSize");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, CompleteEditing, "completeEditing");
   pragma Import (Java, StartEditing, "startEditing");
   pragma Import (Java, CheckForClickInExpandControl, "checkForClickInExpandControl");
   pragma Import (Java, IsLocationInExpandControl, "isLocationInExpandControl");
   pragma Import (Java, HandleExpandControlClick, "handleExpandControlClick");
   pragma Import (Java, ToggleExpandState, "toggleExpandState");
   pragma Import (Java, IsToggleSelectionEvent, "isToggleSelectionEvent");
   pragma Import (Java, IsMultiSelectEvent, "isMultiSelectEvent");
   pragma Import (Java, IsToggleEvent, "isToggleEvent");
   pragma Import (Java, SelectPathForEvent, "selectPathForEvent");
   pragma Import (Java, IsLeaf, "isLeaf");

end Javax.Swing.Plaf.Basic.BasicTreeUI;
pragma Import (Java, Javax.Swing.Plaf.Basic.BasicTreeUI, "javax.swing.plaf.basic.BasicTreeUI");
pragma Extensions_Allowed (Off);
