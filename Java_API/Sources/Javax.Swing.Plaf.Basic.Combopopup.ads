pragma Extensions_Allowed (On);
limited with Java.Awt.Event.KeyListener;
limited with Java.Awt.Event.MouseListener;
limited with Java.Awt.Event.MouseMotionListener;
limited with Javax.Swing.JList;
with Java.Lang.Object;

package Javax.Swing.Plaf.Basic.ComboPopup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Show (This : access Typ) is abstract;

   procedure Hide (This : access Typ) is abstract;

   function IsVisible (This : access Typ)
                       return Java.Boolean is abstract;

   function GetList (This : access Typ)
                     return access Javax.Swing.JList.Typ'Class is abstract;

   function GetMouseListener (This : access Typ)
                              return access Java.Awt.Event.MouseListener.Typ'Class is abstract;

   function GetMouseMotionListener (This : access Typ)
                                    return access Java.Awt.Event.MouseMotionListener.Typ'Class is abstract;

   function GetKeyListener (This : access Typ)
                            return access Java.Awt.Event.KeyListener.Typ'Class is abstract;

   procedure UninstallingUI (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Show, "show");
   pragma Export (Java, Hide, "hide");
   pragma Export (Java, IsVisible, "isVisible");
   pragma Export (Java, GetList, "getList");
   pragma Export (Java, GetMouseListener, "getMouseListener");
   pragma Export (Java, GetMouseMotionListener, "getMouseMotionListener");
   pragma Export (Java, GetKeyListener, "getKeyListener");
   pragma Export (Java, UninstallingUI, "uninstallingUI");

end Javax.Swing.Plaf.Basic.ComboPopup;
pragma Import (Java, Javax.Swing.Plaf.Basic.ComboPopup, "javax.swing.plaf.basic.ComboPopup");
pragma Extensions_Allowed (Off);
