pragma Extensions_Allowed (On);
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
with Java.Awt.LayoutManager2;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.BoxLayout;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.Basic.DefaultMenuLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.BoxLayout.Typ(LayoutManager2_I,
                                     Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultMenuLayout (P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultMenuLayout);
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");

end Javax.Swing.Plaf.Basic.DefaultMenuLayout;
pragma Import (Java, Javax.Swing.Plaf.Basic.DefaultMenuLayout, "javax.swing.plaf.basic.DefaultMenuLayout");
pragma Extensions_Allowed (Off);
