pragma Extensions_Allowed (On);
package Javax.Swing.Plaf.Basic.DragRecognitionSupport is
   pragma Preelaborate;
end Javax.Swing.Plaf.Basic.DragRecognitionSupport;
pragma Import (Java, Javax.Swing.Plaf.Basic.DragRecognitionSupport, "javax.swing.plaf.basic.DragRecognitionSupport");
pragma Extensions_Allowed (Off);
