pragma Extensions_Allowed (On);
package Javax.Swing.Plaf.Basic is
   pragma Preelaborate;
end Javax.Swing.Plaf.Basic;
pragma Import (Java, Javax.Swing.Plaf.Basic, "javax.swing.plaf.basic");
pragma Extensions_Allowed (Off);
