pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.BevelBorder;
with Javax.Swing.Border.Border;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.BorderUIResource.BevelBorderUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.Border.BevelBorder.Typ(Serializable_I,
                                              Border_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BevelBorderUIResource (P1_Int : Java.Int; 
                                       This : Ref := null)
                                       return Ref;

   function New_BevelBorderUIResource (P1_Int : Java.Int;
                                       P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                                       P3_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_BevelBorderUIResource (P1_Int : Java.Int;
                                       P2_Color : access Standard.Java.Awt.Color.Typ'Class;
                                       P3_Color : access Standard.Java.Awt.Color.Typ'Class;
                                       P4_Color : access Standard.Java.Awt.Color.Typ'Class;
                                       P5_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BevelBorderUIResource);

end Javax.Swing.Plaf.BorderUIResource.BevelBorderUIResource;
pragma Import (Java, Javax.Swing.Plaf.BorderUIResource.BevelBorderUIResource, "javax.swing.plaf.BorderUIResource$BevelBorderUIResource");
pragma Extensions_Allowed (Off);
