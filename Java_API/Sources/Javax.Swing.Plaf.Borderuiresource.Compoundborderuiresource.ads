pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.Border;
with Javax.Swing.Border.CompoundBorder;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.BorderUIResource.CompoundBorderUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.Border.CompoundBorder.Typ(Serializable_I,
                                                 Border_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CompoundBorderUIResource (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class;
                                          P2_Border : access Standard.Javax.Swing.Border.Border.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompoundBorderUIResource);

end Javax.Swing.Plaf.BorderUIResource.CompoundBorderUIResource;
pragma Import (Java, Javax.Swing.Plaf.BorderUIResource.CompoundBorderUIResource, "javax.swing.plaf.BorderUIResource$CompoundBorderUIResource");
pragma Extensions_Allowed (Off);
