pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Border.Border;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.BorderUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Border_I : Javax.Swing.Border.Border.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEtchedBorderUIResource return access Javax.Swing.Border.Border.Typ'Class;

   function GetLoweredBevelBorderUIResource return access Javax.Swing.Border.Border.Typ'Class;

   function GetRaisedBevelBorderUIResource return access Javax.Swing.Border.Border.Typ'Class;

   function GetBlackLineBorderUIResource return access Javax.Swing.Border.Border.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BorderUIResource (P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   procedure PaintBorder (This : access Typ;
                          P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Int : Java.Int;
                          P6_Int : Java.Int);

   function GetBorderInsets (This : access Typ;
                             P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                             return access Java.Awt.Insets.Typ'Class;

   function IsBorderOpaque (This : access Typ)
                            return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetEtchedBorderUIResource, "getEtchedBorderUIResource");
   pragma Import (Java, GetLoweredBevelBorderUIResource, "getLoweredBevelBorderUIResource");
   pragma Import (Java, GetRaisedBevelBorderUIResource, "getRaisedBevelBorderUIResource");
   pragma Import (Java, GetBlackLineBorderUIResource, "getBlackLineBorderUIResource");
   pragma Java_Constructor (New_BorderUIResource);
   pragma Import (Java, PaintBorder, "paintBorder");
   pragma Import (Java, GetBorderInsets, "getBorderInsets");
   pragma Import (Java, IsBorderOpaque, "isBorderOpaque");

end Javax.Swing.Plaf.BorderUIResource;
pragma Import (Java, Javax.Swing.Plaf.BorderUIResource, "javax.swing.plaf.BorderUIResource");
pragma Extensions_Allowed (Off);
