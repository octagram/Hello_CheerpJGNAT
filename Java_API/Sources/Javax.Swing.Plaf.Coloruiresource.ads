pragma Extensions_Allowed (On);
with Java.Awt.Color;
with Java.Awt.Paint;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.ColorUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Paint_I : Java.Awt.Paint.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Java.Awt.Color.Typ(Paint_I,
                              Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ColorUIResource (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_ColorUIResource (P1_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   function New_ColorUIResource (P1_Float : Java.Float;
                                 P2_Float : Java.Float;
                                 P3_Float : Java.Float; 
                                 This : Ref := null)
                                 return Ref;

   function New_ColorUIResource (P1_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ColorUIResource);

end Javax.Swing.Plaf.ColorUIResource;
pragma Import (Java, Javax.Swing.Plaf.ColorUIResource, "javax.swing.plaf.ColorUIResource");
pragma Extensions_Allowed (Off);
