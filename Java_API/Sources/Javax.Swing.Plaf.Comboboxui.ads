pragma Extensions_Allowed (On);
limited with Javax.Swing.JComboBox;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.ComboBoxUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_ComboBoxUI (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetPopupVisible (This : access Typ;
                              P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class;
                              P2_Boolean : Java.Boolean) is abstract;

   function IsPopupVisible (This : access Typ;
                            P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class)
                            return Java.Boolean is abstract;

   function IsFocusTraversable (This : access Typ;
                                P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class)
                                return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComboBoxUI);
   pragma Export (Java, SetPopupVisible, "setPopupVisible");
   pragma Export (Java, IsPopupVisible, "isPopupVisible");
   pragma Export (Java, IsFocusTraversable, "isFocusTraversable");

end Javax.Swing.Plaf.ComboBoxUI;
pragma Import (Java, Javax.Swing.Plaf.ComboBoxUI, "javax.swing.plaf.ComboBoxUI");
pragma Extensions_Allowed (Off);
