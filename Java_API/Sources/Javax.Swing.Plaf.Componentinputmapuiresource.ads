pragma Extensions_Allowed (On);
limited with Javax.Swing.JComponent;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.ComponentInputMap;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.ComponentInputMapUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.ComponentInputMap.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ComponentInputMapUIResource (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ComponentInputMapUIResource);

end Javax.Swing.Plaf.ComponentInputMapUIResource;
pragma Import (Java, Javax.Swing.Plaf.ComponentInputMapUIResource, "javax.swing.plaf.ComponentInputMapUIResource");
pragma Extensions_Allowed (Off);
