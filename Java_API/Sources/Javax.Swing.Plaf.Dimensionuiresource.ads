pragma Extensions_Allowed (On);
with Java.Awt.Dimension;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.DimensionUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Java.Awt.Dimension.Typ(Serializable_I,
                                  Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DimensionUIResource (P1_Int : Java.Int;
                                     P2_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DimensionUIResource);

end Javax.Swing.Plaf.DimensionUIResource;
pragma Import (Java, Javax.Swing.Plaf.DimensionUIResource, "javax.swing.plaf.DimensionUIResource");
pragma Extensions_Allowed (Off);
