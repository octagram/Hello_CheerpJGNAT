pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.String;
limited with Javax.Swing.Filechooser.FileFilter;
limited with Javax.Swing.Filechooser.FileView;
limited with Javax.Swing.JFileChooser;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.FileChooserUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_FileChooserUI (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAcceptAllFileFilter (This : access Typ;
                                    P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                    return access Javax.Swing.Filechooser.FileFilter.Typ'Class is abstract;

   function GetFileView (This : access Typ;
                         P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                         return access Javax.Swing.Filechooser.FileView.Typ'Class is abstract;

   function GetApproveButtonText (This : access Typ;
                                  P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                  return access Java.Lang.String.Typ'Class is abstract;

   function GetDialogTitle (This : access Typ;
                            P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure RescanCurrentDirectory (This : access Typ;
                                     P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class) is abstract;

   procedure EnsureFileIsVisible (This : access Typ;
                                  P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class;
                                  P2_File : access Standard.Java.Io.File.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FileChooserUI);
   pragma Export (Java, GetAcceptAllFileFilter, "getAcceptAllFileFilter");
   pragma Export (Java, GetFileView, "getFileView");
   pragma Export (Java, GetApproveButtonText, "getApproveButtonText");
   pragma Export (Java, GetDialogTitle, "getDialogTitle");
   pragma Export (Java, RescanCurrentDirectory, "rescanCurrentDirectory");
   pragma Export (Java, EnsureFileIsVisible, "ensureFileIsVisible");

end Javax.Swing.Plaf.FileChooserUI;
pragma Import (Java, Javax.Swing.Plaf.FileChooserUI, "javax.swing.plaf.FileChooserUI");
pragma Extensions_Allowed (Off);
