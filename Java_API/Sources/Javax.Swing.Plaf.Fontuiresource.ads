pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Awt.Font;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.FontUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Java.Awt.Font.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FontUIResource (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   function New_FontUIResource (P1_Font : access Standard.Java.Awt.Font.Typ'Class; 
                                This : Ref := null)
                                return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FontUIResource);

end Javax.Swing.Plaf.FontUIResource;
pragma Import (Java, Javax.Swing.Plaf.FontUIResource, "javax.swing.plaf.FontUIResource");
pragma Extensions_Allowed (Off);
