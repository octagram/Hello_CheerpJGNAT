pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.InputMap;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.InputMapUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.InputMap.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InputMapUIResource (This : Ref := null)
                                    return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputMapUIResource);

end Javax.Swing.Plaf.InputMapUIResource;
pragma Import (Java, Javax.Swing.Plaf.InputMapUIResource, "javax.swing.plaf.InputMapUIResource");
pragma Extensions_Allowed (Off);
