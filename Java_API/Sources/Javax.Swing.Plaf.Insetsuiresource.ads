pragma Extensions_Allowed (On);
with Java.Awt.Insets;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Plaf.UIResource;

package Javax.Swing.Plaf.InsetsUIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Java.Awt.Insets.Typ(Serializable_I,
                               Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InsetsUIResource (P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InsetsUIResource);

end Javax.Swing.Plaf.InsetsUIResource;
pragma Import (Java, Javax.Swing.Plaf.InsetsUIResource, "javax.swing.plaf.InsetsUIResource");
pragma Extensions_Allowed (Off);
