pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.InternalFrameUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_InternalFrameUI (This : Ref := null)
                                 return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InternalFrameUI);

end Javax.Swing.Plaf.InternalFrameUI;
pragma Import (Java, Javax.Swing.Plaf.InternalFrameUI, "javax.swing.plaf.InternalFrameUI");
pragma Extensions_Allowed (Off);
