pragma Extensions_Allowed (On);
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Javax.Swing.JList;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.ListUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_ListUI (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function LocationToIndex (This : access Typ;
                             P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                             P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return Java.Int is abstract;

   function IndexToLocation (This : access Typ;
                             P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                             P2_Int : Java.Int)
                             return access Java.Awt.Point.Typ'Class is abstract;

   function GetCellBounds (This : access Typ;
                           P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int)
                           return access Java.Awt.Rectangle.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ListUI);
   pragma Export (Java, LocationToIndex, "locationToIndex");
   pragma Export (Java, IndexToLocation, "indexToLocation");
   pragma Export (Java, GetCellBounds, "getCellBounds");

end Javax.Swing.Plaf.ListUI;
pragma Import (Java, Javax.Swing.Plaf.ListUI, "javax.swing.plaf.ListUI");
pragma Extensions_Allowed (Off);
