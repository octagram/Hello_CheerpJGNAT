pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.MenuBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_MenuBarUI (This : Ref := null)
                           return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MenuBarUI);

end Javax.Swing.Plaf.MenuBarUI;
pragma Import (Java, Javax.Swing.Plaf.MenuBarUI, "javax.swing.plaf.MenuBarUI");
pragma Extensions_Allowed (Off);
