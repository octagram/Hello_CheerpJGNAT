pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Plaf.ColorUIResource;
limited with Javax.Swing.Plaf.FontUIResource;
with Java.Lang.Object;
with Javax.Swing.Plaf.Metal.MetalTheme;

package Javax.Swing.Plaf.Metal.DefaultMetalTheme is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Metal.MetalTheme.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultMetalTheme (This : Ref := null)
                                   return Ref;

   --  protected
   function GetPrimary1 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetPrimary2 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetPrimary3 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetSecondary1 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetSecondary2 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetSecondary3 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlTextFont (This : access Typ)
                                return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetSystemTextFont (This : access Typ)
                               return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetUserTextFont (This : access Typ)
                             return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetMenuTextFont (This : access Typ)
                             return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetWindowTitleFont (This : access Typ)
                                return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetSubTextFont (This : access Typ)
                            return access Javax.Swing.Plaf.FontUIResource.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetName, "getName");
   pragma Java_Constructor (New_DefaultMetalTheme);
   pragma Import (Java, GetPrimary1, "getPrimary1");
   pragma Import (Java, GetPrimary2, "getPrimary2");
   pragma Import (Java, GetPrimary3, "getPrimary3");
   pragma Import (Java, GetSecondary1, "getSecondary1");
   pragma Import (Java, GetSecondary2, "getSecondary2");
   pragma Import (Java, GetSecondary3, "getSecondary3");
   pragma Import (Java, GetControlTextFont, "getControlTextFont");
   pragma Import (Java, GetSystemTextFont, "getSystemTextFont");
   pragma Import (Java, GetUserTextFont, "getUserTextFont");
   pragma Import (Java, GetMenuTextFont, "getMenuTextFont");
   pragma Import (Java, GetWindowTitleFont, "getWindowTitleFont");
   pragma Import (Java, GetSubTextFont, "getSubTextFont");

end Javax.Swing.Plaf.Metal.DefaultMetalTheme;
pragma Import (Java, Javax.Swing.Plaf.Metal.DefaultMetalTheme, "javax.swing.plaf.metal.DefaultMetalTheme");
pragma Extensions_Allowed (Off);
