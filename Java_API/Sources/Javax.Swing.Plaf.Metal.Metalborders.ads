pragma Extensions_Allowed (On);
limited with Javax.Swing.Border.Border;
with Java.Lang.Object;

package Javax.Swing.Plaf.Metal.MetalBorders is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalBorders (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetButtonBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetTextBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetTextFieldBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetToggleButtonBorder return access Javax.Swing.Border.Border.Typ'Class;

   function GetDesktopIconBorder return access Javax.Swing.Border.Border.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalBorders);
   pragma Import (Java, GetButtonBorder, "getButtonBorder");
   pragma Import (Java, GetTextBorder, "getTextBorder");
   pragma Import (Java, GetTextFieldBorder, "getTextFieldBorder");
   pragma Import (Java, GetToggleButtonBorder, "getToggleButtonBorder");
   pragma Import (Java, GetDesktopIconBorder, "getDesktopIconBorder");

end Javax.Swing.Plaf.Metal.MetalBorders;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalBorders, "javax.swing.plaf.metal.MetalBorders");
pragma Extensions_Allowed (Off);
