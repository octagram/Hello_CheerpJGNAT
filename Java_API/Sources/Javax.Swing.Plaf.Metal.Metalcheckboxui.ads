pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Metal.MetalRadioButtonUI;

package Javax.Swing.Plaf.Metal.MetalCheckBoxUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Metal.MetalRadioButtonUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalCheckBoxUI (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   function GetPropertyPrefix (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   procedure InstallDefaults (This : access Typ;
                              P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalCheckBoxUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, GetPropertyPrefix, "getPropertyPrefix");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");

end Javax.Swing.Plaf.Metal.MetalCheckBoxUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalCheckBoxUI, "javax.swing.plaf.metal.MetalCheckBoxUI");
pragma Extensions_Allowed (Off);
