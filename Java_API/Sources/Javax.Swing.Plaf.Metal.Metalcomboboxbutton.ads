pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Javax.Swing.CellRendererPane;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComboBox;
limited with Javax.Swing.JList;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JButton;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Metal.MetalComboBoxButton is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JButton.Typ(ItemSelectable_I,
                                   MenuContainer_I,
                                   ImageObserver_I,
                                   Serializable_I,
                                   Accessible_I,
                                   SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ComboBox : access Javax.Swing.JComboBox.Typ'Class;
      pragma Import (Java, ComboBox, "comboBox");

      --  protected
      ListBox : access Javax.Swing.JList.Typ'Class;
      pragma Import (Java, ListBox, "listBox");

      --  protected
      RendererPane : access Javax.Swing.CellRendererPane.Typ'Class;
      pragma Import (Java, RendererPane, "rendererPane");

      --  protected
      ComboIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, ComboIcon, "comboIcon");

      --  protected
      IconOnly : Java.Boolean;
      pragma Import (Java, IconOnly, "iconOnly");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetComboBox (This : access Typ)
                         return access Javax.Swing.JComboBox.Typ'Class;

   --  final
   procedure SetComboBox (This : access Typ;
                          P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class);

   --  final
   function GetComboIcon (This : access Typ)
                          return access Javax.Swing.Icon.Typ'Class;

   --  final
   procedure SetComboIcon (This : access Typ;
                           P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   --  final
   function IsIconOnly (This : access Typ)
                        return Java.Boolean;

   --  final
   procedure SetIconOnly (This : access Typ;
                          P1_Boolean : Java.Boolean);

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalComboBoxButton (P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class;
                                     P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                     P3_CellRendererPane : access Standard.Javax.Swing.CellRendererPane.Typ'Class;
                                     P4_JList : access Standard.Javax.Swing.JList.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_MetalComboBoxButton (P1_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class;
                                     P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                     P3_Boolean : Java.Boolean;
                                     P4_CellRendererPane : access Standard.Javax.Swing.CellRendererPane.Typ'Class;
                                     P5_JList : access Standard.Javax.Swing.JList.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetComboBox, "getComboBox");
   pragma Import (Java, SetComboBox, "setComboBox");
   pragma Import (Java, GetComboIcon, "getComboIcon");
   pragma Import (Java, SetComboIcon, "setComboIcon");
   pragma Import (Java, IsIconOnly, "isIconOnly");
   pragma Import (Java, SetIconOnly, "setIconOnly");
   pragma Java_Constructor (New_MetalComboBoxButton);
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");

end Javax.Swing.Plaf.Metal.MetalComboBoxButton;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalComboBoxButton, "javax.swing.plaf.metal.MetalComboBoxButton");
pragma Extensions_Allowed (Off);
