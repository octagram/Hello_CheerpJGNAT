pragma Extensions_Allowed (On);
limited with Java.Awt.Insets;
with Java.Awt.Event.FocusListener;
with Java.Lang.Object;
with Javax.Swing.ComboBoxEditor;
with Javax.Swing.Plaf.Basic.BasicComboBoxEditor;

package Javax.Swing.Plaf.Metal.MetalComboBoxEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(FocusListener_I : Java.Awt.Event.FocusListener.Ref;
            ComboBoxEditor_I : Javax.Swing.ComboBoxEditor.Ref)
    is new Javax.Swing.Plaf.Basic.BasicComboBoxEditor.Typ(FocusListener_I,
                                                          ComboBoxEditor_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalComboBoxEditor (This : Ref := null)
                                     return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   EditorBorderInsets : access Java.Awt.Insets.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalComboBoxEditor);
   pragma Import (Java, EditorBorderInsets, "editorBorderInsets");

end Javax.Swing.Plaf.Metal.MetalComboBoxEditor;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalComboBoxEditor, "javax.swing.plaf.metal.MetalComboBoxEditor");
pragma Extensions_Allowed (Off);
