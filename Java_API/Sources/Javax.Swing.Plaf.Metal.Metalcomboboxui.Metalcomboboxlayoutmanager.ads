pragma Extensions_Allowed (On);
limited with Java.Awt.Container;
with Java.Awt.LayoutManager;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicComboBoxUI.ComboBoxLayoutManager;

package Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboBoxLayoutManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref)
    is new Javax.Swing.Plaf.Basic.BasicComboBoxUI.ComboBoxLayoutManager.Typ(LayoutManager_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalComboBoxLayoutManager (P1_MetalComboBoxUI : access Standard.Javax.Swing.Plaf.Metal.MetalComboBoxUI.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure SuperLayout (This : access Typ;
                          P1_Container : access Standard.Java.Awt.Container.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalComboBoxLayoutManager);
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, SuperLayout, "superLayout");

end Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboBoxLayoutManager;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboBoxLayoutManager, "javax.swing.plaf.metal.MetalComboBoxUI$MetalComboBoxLayoutManager");
pragma Extensions_Allowed (Off);
