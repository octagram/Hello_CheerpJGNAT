pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Javax.Swing.JComboBox;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.MenuElement;
with Javax.Swing.Plaf.Basic.BasicComboPopup;
with Javax.Swing.Plaf.Basic.ComboPopup;

package Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboPopup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            MenuElement_I : Javax.Swing.MenuElement.Ref;
            ComboPopup_I : Javax.Swing.Plaf.Basic.ComboPopup.Ref)
    is new Javax.Swing.Plaf.Basic.BasicComboPopup.Typ(MenuContainer_I,
                                                      ImageObserver_I,
                                                      Serializable_I,
                                                      Accessible_I,
                                                      MenuElement_I,
                                                      ComboPopup_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalComboPopup (P1_MetalComboBoxUI : access Standard.Javax.Swing.Plaf.Metal.MetalComboBoxUI.Typ'Class;
                                 P2_JComboBox : access Standard.Javax.Swing.JComboBox.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DelegateFocus (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalComboPopup);
   pragma Import (Java, DelegateFocus, "delegateFocus");

end Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboPopup;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboPopup, "javax.swing.plaf.metal.MetalComboBoxUI$MetalComboPopup");
pragma Extensions_Allowed (Off);
