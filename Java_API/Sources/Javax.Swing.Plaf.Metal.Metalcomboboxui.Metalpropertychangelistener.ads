pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicComboBoxUI.PropertyChangeHandler;

package Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalPropertyChangeListener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref)
    is new Javax.Swing.Plaf.Basic.BasicComboBoxUI.PropertyChangeHandler.Typ(PropertyChangeListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalPropertyChangeListener (P1_MetalComboBoxUI : access Standard.Javax.Swing.Plaf.Metal.MetalComboBoxUI.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalPropertyChangeListener);
   pragma Import (Java, PropertyChange, "propertyChange");

end Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalPropertyChangeListener;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalPropertyChangeListener, "javax.swing.plaf.metal.MetalComboBoxUI$MetalPropertyChangeListener");
pragma Extensions_Allowed (Off);
