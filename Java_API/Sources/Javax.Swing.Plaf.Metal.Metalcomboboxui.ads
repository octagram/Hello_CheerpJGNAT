pragma Extensions_Allowed (On);
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.ComboBoxEditor;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.Basic.ComboPopup;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboBoxLayoutManager;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicComboBoxUI;

package Javax.Swing.Plaf.Metal.MetalComboBoxUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicComboBoxUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalComboBoxUI (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure PaintCurrentValue (This : access Typ;
                                P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                P3_Boolean : Java.Boolean);

   procedure PaintCurrentValueBackground (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                          P3_Boolean : Java.Boolean);

   function GetBaseline (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int)
                         return Java.Int;

   --  protected
   function CreateEditor (This : access Typ)
                          return access Javax.Swing.ComboBoxEditor.Typ'Class;

   --  protected
   function CreatePopup (This : access Typ)
                         return access Javax.Swing.Plaf.Basic.ComboPopup.Typ'Class;

   --  protected
   function CreateArrowButton (This : access Typ)
                               return access Javax.Swing.JButton.Typ'Class;

   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateLayoutManager (This : access Typ)
                                 return access Java.Awt.LayoutManager.Typ'Class;

   procedure LayoutComboBox (This : access Typ;
                             P1_Container : access Standard.Java.Awt.Container.Typ'Class;
                             P2_MetalComboBoxLayoutManager : access Standard.Javax.Swing.Plaf.Metal.MetalComboBoxUI.MetalComboBoxLayoutManager.Typ'Class);

   procedure ConfigureEditor (This : access Typ);

   procedure UnconfigureEditor (This : access Typ);

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalComboBoxUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintCurrentValue, "paintCurrentValue");
   pragma Import (Java, PaintCurrentValueBackground, "paintCurrentValueBackground");
   pragma Import (Java, GetBaseline, "getBaseline");
   pragma Import (Java, CreateEditor, "createEditor");
   pragma Import (Java, CreatePopup, "createPopup");
   pragma Import (Java, CreateArrowButton, "createArrowButton");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreateLayoutManager, "createLayoutManager");
   pragma Import (Java, LayoutComboBox, "layoutComboBox");
   pragma Import (Java, ConfigureEditor, "configureEditor");
   pragma Import (Java, UnconfigureEditor, "unconfigureEditor");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");

end Javax.Swing.Plaf.Metal.MetalComboBoxUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalComboBoxUI, "javax.swing.plaf.metal.MetalComboBoxUI");
pragma Extensions_Allowed (Off);
