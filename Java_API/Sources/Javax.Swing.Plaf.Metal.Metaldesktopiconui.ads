pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicDesktopIconUI;

package Javax.Swing.Plaf.Metal.MetalDesktopIconUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicDesktopIconUI.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalDesktopIconUI (This : Ref := null)
                                    return Ref;

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallComponents (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_MetalDesktopIconUI);
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");

end Javax.Swing.Plaf.Metal.MetalDesktopIconUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalDesktopIconUI, "javax.swing.plaf.metal.MetalDesktopIconUI");
pragma Extensions_Allowed (Off);
