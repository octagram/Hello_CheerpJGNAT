pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.JList;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.DefaultListCellRenderer;
with Javax.Swing.ListCellRenderer;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Metal.MetalFileChooserUI.FilterComboBoxRenderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            ListCellRenderer_I : Javax.Swing.ListCellRenderer.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.DefaultListCellRenderer.Typ(MenuContainer_I,
                                                   ImageObserver_I,
                                                   Serializable_I,
                                                   Accessible_I,
                                                   ListCellRenderer_I,
                                                   SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FilterComboBoxRenderer (P1_MetalFileChooserUI : access Standard.Javax.Swing.Plaf.Metal.MetalFileChooserUI.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetListCellRendererComponent (This : access Typ;
                                          P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                          P3_Int : Java.Int;
                                          P4_Boolean : Java.Boolean;
                                          P5_Boolean : Java.Boolean)
                                          return access Java.Awt.Component.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FilterComboBoxRenderer);
   pragma Import (Java, GetListCellRendererComponent, "getListCellRendererComponent");

end Javax.Swing.Plaf.Metal.MetalFileChooserUI.FilterComboBoxRenderer;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalFileChooserUI.FilterComboBoxRenderer, "javax.swing.plaf.metal.MetalFileChooserUI$FilterComboBoxRenderer");
pragma Extensions_Allowed (Off);
