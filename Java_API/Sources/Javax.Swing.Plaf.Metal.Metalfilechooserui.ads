pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Io.File;
limited with Java.Lang.String;
limited with Javax.Swing.ActionMap;
limited with Javax.Swing.Event.ListSelectionEvent;
limited with Javax.Swing.Event.ListSelectionListener;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JFileChooser;
limited with Javax.Swing.JPanel;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Plaf.Metal.MetalFileChooserUI.FilterComboBoxRenderer;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicFileChooserUI;

package Javax.Swing.Plaf.Metal.MetalFileChooserUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicFileChooserUI.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalFileChooserUI (P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallComponents (This : access Typ;
                                  P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   procedure InstallComponents (This : access Typ;
                                P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   function GetButtonPanel (This : access Typ)
                            return access Javax.Swing.JPanel.Typ'Class;

   --  protected
   function GetBottomPanel (This : access Typ)
                            return access Javax.Swing.JPanel.Typ'Class;

   --  protected
   procedure InstallStrings (This : access Typ;
                             P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ;
                               P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   --  protected
   function GetActionMap (This : access Typ)
                          return access Javax.Swing.ActionMap.Typ'Class;

   --  protected
   function CreateActionMap (This : access Typ)
                             return access Javax.Swing.ActionMap.Typ'Class;

   --  protected
   function CreateList (This : access Typ;
                        P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                        return access Javax.Swing.JPanel.Typ'Class;

   --  protected
   function CreateDetailsView (This : access Typ;
                               P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                               return access Javax.Swing.JPanel.Typ'Class;

   function CreateListSelectionListener (This : access Typ;
                                         P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                         return access Javax.Swing.Event.ListSelectionListener.Typ'Class;

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function CreatePropertyChangeListener (This : access Typ;
                                          P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure RemoveControlButtons (This : access Typ);

   --  protected
   procedure AddControlButtons (This : access Typ);

   procedure EnsureFileIsVisible (This : access Typ;
                                  P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class;
                                  P2_File : access Standard.Java.Io.File.Typ'Class);

   procedure RescanCurrentDirectory (This : access Typ;
                                     P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class);

   function GetFileName (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetFileName (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure SetDirectorySelected (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetDirectoryName (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   procedure SetDirectoryName (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function CreateFilterComboBoxRenderer (This : access Typ)
                                          return access Javax.Swing.Plaf.Metal.MetalFileChooserUI.FilterComboBoxRenderer.Typ'Class;

   procedure ValueChanged (This : access Typ;
                           P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class);

   --  protected
   function GetApproveButton (This : access Typ;
                              P1_JFileChooser : access Standard.Javax.Swing.JFileChooser.Typ'Class)
                              return access Javax.Swing.JButton.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_MetalFileChooserUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, InstallComponents, "installComponents");
   pragma Import (Java, GetButtonPanel, "getButtonPanel");
   pragma Import (Java, GetBottomPanel, "getBottomPanel");
   pragma Import (Java, InstallStrings, "installStrings");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, GetActionMap, "getActionMap");
   pragma Import (Java, CreateActionMap, "createActionMap");
   pragma Import (Java, CreateList, "createList");
   pragma Import (Java, CreateDetailsView, "createDetailsView");
   pragma Import (Java, CreateListSelectionListener, "createListSelectionListener");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, RemoveControlButtons, "removeControlButtons");
   pragma Import (Java, AddControlButtons, "addControlButtons");
   pragma Import (Java, EnsureFileIsVisible, "ensureFileIsVisible");
   pragma Import (Java, RescanCurrentDirectory, "rescanCurrentDirectory");
   pragma Import (Java, GetFileName, "getFileName");
   pragma Import (Java, SetFileName, "setFileName");
   pragma Import (Java, SetDirectorySelected, "setDirectorySelected");
   pragma Import (Java, GetDirectoryName, "getDirectoryName");
   pragma Import (Java, SetDirectoryName, "setDirectoryName");
   pragma Import (Java, CreateFilterComboBoxRenderer, "createFilterComboBoxRenderer");
   pragma Import (Java, ValueChanged, "valueChanged");
   pragma Import (Java, GetApproveButton, "getApproveButton");

end Javax.Swing.Plaf.Metal.MetalFileChooserUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalFileChooserUI, "javax.swing.plaf.metal.MetalFileChooserUI");
pragma Extensions_Allowed (Off);
