pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Graphics;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Icon;

package Javax.Swing.Plaf.Metal.MetalIconFactory.TreeControlIcon is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Icon_I : Javax.Swing.Icon.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      IsLight : Java.Boolean;
      pragma Import (Java, IsLight, "isLight");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeControlIcon (P1_Boolean : Java.Boolean; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintIcon (This : access Typ;
                        P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   procedure PaintMe (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int);

   function GetIconWidth (This : access Typ)
                          return Java.Int;

   function GetIconHeight (This : access Typ)
                           return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeControlIcon);
   pragma Import (Java, PaintIcon, "paintIcon");
   pragma Import (Java, PaintMe, "paintMe");
   pragma Import (Java, GetIconWidth, "getIconWidth");
   pragma Import (Java, GetIconHeight, "getIconHeight");

end Javax.Swing.Plaf.Metal.MetalIconFactory.TreeControlIcon;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalIconFactory.TreeControlIcon, "javax.swing.plaf.metal.MetalIconFactory$TreeControlIcon");
pragma Extensions_Allowed (Off);
