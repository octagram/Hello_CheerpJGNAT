pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Icon;
with Javax.Swing.Plaf.Metal.MetalIconFactory.FileIcon16;

package Javax.Swing.Plaf.Metal.MetalIconFactory.TreeLeafIcon is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Icon_I : Javax.Swing.Icon.Ref)
    is new Javax.Swing.Plaf.Metal.MetalIconFactory.FileIcon16.Typ(Serializable_I,
                                                                  Icon_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreeLeafIcon (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetShift (This : access Typ)
                      return Java.Int;

   function GetAdditionalHeight (This : access Typ)
                                 return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeLeafIcon);
   pragma Import (Java, GetShift, "getShift");
   pragma Import (Java, GetAdditionalHeight, "getAdditionalHeight");

end Javax.Swing.Plaf.Metal.MetalIconFactory.TreeLeafIcon;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalIconFactory.TreeLeafIcon, "javax.swing.plaf.metal.MetalIconFactory$TreeLeafIcon");
pragma Extensions_Allowed (Off);
