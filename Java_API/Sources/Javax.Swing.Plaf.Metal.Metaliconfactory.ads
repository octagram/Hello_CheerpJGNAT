pragma Extensions_Allowed (On);
limited with Javax.Swing.Icon;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Plaf.Metal.MetalIconFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalIconFactory (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFileChooserDetailViewIcon return access Javax.Swing.Icon.Typ'Class;

   function GetFileChooserHomeFolderIcon return access Javax.Swing.Icon.Typ'Class;

   function GetFileChooserListViewIcon return access Javax.Swing.Icon.Typ'Class;

   function GetFileChooserNewFolderIcon return access Javax.Swing.Icon.Typ'Class;

   function GetFileChooserUpFolderIcon return access Javax.Swing.Icon.Typ'Class;

   function GetInternalFrameAltMaximizeIcon (P1_Int : Java.Int)
                                             return access Javax.Swing.Icon.Typ'Class;

   function GetInternalFrameCloseIcon (P1_Int : Java.Int)
                                       return access Javax.Swing.Icon.Typ'Class;

   function GetInternalFrameDefaultMenuIcon return access Javax.Swing.Icon.Typ'Class;

   function GetInternalFrameMaximizeIcon (P1_Int : Java.Int)
                                          return access Javax.Swing.Icon.Typ'Class;

   function GetInternalFrameMinimizeIcon (P1_Int : Java.Int)
                                          return access Javax.Swing.Icon.Typ'Class;

   function GetRadioButtonIcon return access Javax.Swing.Icon.Typ'Class;

   function GetCheckBoxIcon return access Javax.Swing.Icon.Typ'Class;

   function GetTreeComputerIcon return access Javax.Swing.Icon.Typ'Class;

   function GetTreeFloppyDriveIcon return access Javax.Swing.Icon.Typ'Class;

   function GetTreeFolderIcon return access Javax.Swing.Icon.Typ'Class;

   function GetTreeHardDriveIcon return access Javax.Swing.Icon.Typ'Class;

   function GetTreeLeafIcon return access Javax.Swing.Icon.Typ'Class;

   function GetTreeControlIcon (P1_Boolean : Java.Boolean)
                                return access Javax.Swing.Icon.Typ'Class;

   function GetMenuArrowIcon return access Javax.Swing.Icon.Typ'Class;

   function GetMenuItemCheckIcon return access Javax.Swing.Icon.Typ'Class;

   function GetMenuItemArrowIcon return access Javax.Swing.Icon.Typ'Class;

   function GetCheckBoxMenuItemIcon return access Javax.Swing.Icon.Typ'Class;

   function GetRadioButtonMenuItemIcon return access Javax.Swing.Icon.Typ'Class;

   function GetHorizontalSliderThumbIcon return access Javax.Swing.Icon.Typ'Class;

   function GetVerticalSliderThumbIcon return access Javax.Swing.Icon.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DARK : constant Java.Boolean;

   --  final
   LIGHT : constant Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalIconFactory);
   pragma Import (Java, GetFileChooserDetailViewIcon, "getFileChooserDetailViewIcon");
   pragma Import (Java, GetFileChooserHomeFolderIcon, "getFileChooserHomeFolderIcon");
   pragma Import (Java, GetFileChooserListViewIcon, "getFileChooserListViewIcon");
   pragma Import (Java, GetFileChooserNewFolderIcon, "getFileChooserNewFolderIcon");
   pragma Import (Java, GetFileChooserUpFolderIcon, "getFileChooserUpFolderIcon");
   pragma Import (Java, GetInternalFrameAltMaximizeIcon, "getInternalFrameAltMaximizeIcon");
   pragma Import (Java, GetInternalFrameCloseIcon, "getInternalFrameCloseIcon");
   pragma Import (Java, GetInternalFrameDefaultMenuIcon, "getInternalFrameDefaultMenuIcon");
   pragma Import (Java, GetInternalFrameMaximizeIcon, "getInternalFrameMaximizeIcon");
   pragma Import (Java, GetInternalFrameMinimizeIcon, "getInternalFrameMinimizeIcon");
   pragma Import (Java, GetRadioButtonIcon, "getRadioButtonIcon");
   pragma Import (Java, GetCheckBoxIcon, "getCheckBoxIcon");
   pragma Import (Java, GetTreeComputerIcon, "getTreeComputerIcon");
   pragma Import (Java, GetTreeFloppyDriveIcon, "getTreeFloppyDriveIcon");
   pragma Import (Java, GetTreeFolderIcon, "getTreeFolderIcon");
   pragma Import (Java, GetTreeHardDriveIcon, "getTreeHardDriveIcon");
   pragma Import (Java, GetTreeLeafIcon, "getTreeLeafIcon");
   pragma Import (Java, GetTreeControlIcon, "getTreeControlIcon");
   pragma Import (Java, GetMenuArrowIcon, "getMenuArrowIcon");
   pragma Import (Java, GetMenuItemCheckIcon, "getMenuItemCheckIcon");
   pragma Import (Java, GetMenuItemArrowIcon, "getMenuItemArrowIcon");
   pragma Import (Java, GetCheckBoxMenuItemIcon, "getCheckBoxMenuItemIcon");
   pragma Import (Java, GetRadioButtonMenuItemIcon, "getRadioButtonMenuItemIcon");
   pragma Import (Java, GetHorizontalSliderThumbIcon, "getHorizontalSliderThumbIcon");
   pragma Import (Java, GetVerticalSliderThumbIcon, "getVerticalSliderThumbIcon");
   pragma Import (Java, DARK, "DARK");
   pragma Import (Java, LIGHT, "LIGHT");

end Javax.Swing.Plaf.Metal.MetalIconFactory;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalIconFactory, "javax.swing.plaf.metal.MetalIconFactory");
pragma Extensions_Allowed (Off);
