pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.LayoutManager;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JInternalFrame;
limited with Javax.Swing.JMenu;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane;

package Javax.Swing.Plaf.Metal.MetalInternalFrameTitlePane is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.Plaf.Basic.BasicInternalFrameTitlePane.Typ(MenuContainer_I,
                                                                  ImageObserver_I,
                                                                  Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      IsPalette : Java.Boolean;
      pragma Import (Java, IsPalette, "isPalette");

      --  protected
      PaletteCloseIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, PaletteCloseIcon, "paletteCloseIcon");

      --  protected
      PaletteTitleHeight : Java.Int;
      pragma Import (Java, PaletteTitleHeight, "paletteTitleHeight");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalInternalFrameTitlePane (P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class; 
                                             This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddNotify (This : access Typ);

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure UninstallDefaults (This : access Typ);

   --  protected
   procedure CreateButtons (This : access Typ);

   --  protected
   procedure AssembleSystemMenu (This : access Typ);

   --  protected
   procedure AddSystemMenuItems (This : access Typ;
                                 P1_JMenu : access Standard.Javax.Swing.JMenu.Typ'Class);

   --  protected
   procedure ShowSystemMenu (This : access Typ);

   --  protected
   procedure AddSubComponents (This : access Typ);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateLayout (This : access Typ)
                          return access Java.Awt.LayoutManager.Typ'Class;

   procedure PaintPalette (This : access Typ;
                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PaintComponent (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure SetPalette (This : access Typ;
                         P1_Boolean : Java.Boolean);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalInternalFrameTitlePane);
   pragma Import (Java, AddNotify, "addNotify");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, CreateButtons, "createButtons");
   pragma Import (Java, AssembleSystemMenu, "assembleSystemMenu");
   pragma Import (Java, AddSystemMenuItems, "addSystemMenuItems");
   pragma Import (Java, ShowSystemMenu, "showSystemMenu");
   pragma Import (Java, AddSubComponents, "addSubComponents");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, CreateLayout, "createLayout");
   pragma Import (Java, PaintPalette, "paintPalette");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, SetPalette, "setPalette");

end Javax.Swing.Plaf.Metal.MetalInternalFrameTitlePane;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalInternalFrameTitlePane, "javax.swing.plaf.metal.MetalInternalFrameTitlePane");
pragma Extensions_Allowed (Off);
