pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Event.MouseInputAdapter;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JInternalFrame;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicInternalFrameUI;

package Javax.Swing.Plaf.Metal.MetalInternalFrameUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicInternalFrameUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalInternalFrameUI (P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   procedure InstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallKeyboardActions (This : access Typ);

   --  protected
   procedure UninstallComponents (This : access Typ);

   --  protected
   function CreateNorthPane (This : access Typ;
                             P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                             return access Javax.Swing.JComponent.Typ'Class;

   procedure SetPalette (This : access Typ;
                         P1_Boolean : Java.Boolean);

   --  protected
   function CreateBorderListener (This : access Typ;
                                  P1_JInternalFrame : access Standard.Javax.Swing.JInternalFrame.Typ'Class)
                                  return access Javax.Swing.Event.MouseInputAdapter.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   IS_PALETTE : access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalInternalFrameUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, InstallKeyboardActions, "installKeyboardActions");
   pragma Import (Java, UninstallKeyboardActions, "uninstallKeyboardActions");
   pragma Import (Java, UninstallComponents, "uninstallComponents");
   pragma Import (Java, CreateNorthPane, "createNorthPane");
   pragma Import (Java, SetPalette, "setPalette");
   pragma Import (Java, CreateBorderListener, "createBorderListener");
   pragma Import (Java, IS_PALETTE, "IS_PALETTE");

end Javax.Swing.Plaf.Metal.MetalInternalFrameUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalInternalFrameUI, "javax.swing.plaf.metal.MetalInternalFrameUI");
pragma Extensions_Allowed (Off);
