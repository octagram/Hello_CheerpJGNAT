pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JLabel;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicLabelUI;

package Javax.Swing.Plaf.Metal.MetalLabelUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref)
    is new Javax.Swing.Plaf.Basic.BasicLabelUI.Typ(PropertyChangeListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalLabelUI (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   procedure PaintDisabledText (This : access Typ;
                                P1_JLabel : access Standard.Javax.Swing.JLabel.Typ'Class;
                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   MetalLabelUI : access Javax.Swing.Plaf.Metal.MetalLabelUI.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalLabelUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, PaintDisabledText, "paintDisabledText");
   pragma Import (Java, MetalLabelUI, "metalLabelUI");

end Javax.Swing.Plaf.Metal.MetalLabelUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalLabelUI, "javax.swing.plaf.metal.MetalLabelUI");
pragma Extensions_Allowed (Off);
