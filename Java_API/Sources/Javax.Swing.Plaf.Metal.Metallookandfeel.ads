pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.LayoutStyle;
limited with Javax.Swing.Plaf.ColorUIResource;
limited with Javax.Swing.Plaf.FontUIResource;
limited with Javax.Swing.Plaf.Metal.MetalTheme;
limited with Javax.Swing.UIDefaults;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicLookAndFeel;

package Javax.Swing.Plaf.Metal.MetalLookAndFeel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.Plaf.Basic.BasicLookAndFeel.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalLookAndFeel (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function IsNativeLookAndFeel (This : access Typ)
                                 return Java.Boolean;

   function IsSupportedLookAndFeel (This : access Typ)
                                    return Java.Boolean;

   function GetSupportsWindowDecorations (This : access Typ)
                                          return Java.Boolean;

   --  protected
   procedure InitClassDefaults (This : access Typ;
                                P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);

   --  protected
   procedure InitSystemColorDefaults (This : access Typ;
                                      P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);

   --  protected
   procedure InitComponentDefaults (This : access Typ;
                                    P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);

   --  protected
   procedure CreateDefaultTheme (This : access Typ);

   function GetDefaults (This : access Typ)
                         return access Javax.Swing.UIDefaults.Typ'Class;

   procedure ProvideErrorFeedback (This : access Typ;
                                   P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure SetCurrentTheme (P1_MetalTheme : access Standard.Javax.Swing.Plaf.Metal.MetalTheme.Typ'Class);

   function GetCurrentTheme return access Javax.Swing.Plaf.Metal.MetalTheme.Typ'Class;

   function GetDisabledIcon (This : access Typ;
                             P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                             return access Javax.Swing.Icon.Typ'Class;

   function GetDisabledSelectedIcon (This : access Typ;
                                     P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                     P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class)
                                     return access Javax.Swing.Icon.Typ'Class;

   function GetControlTextFont return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetSystemTextFont return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetUserTextFont return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetMenuTextFont return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetWindowTitleFont return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetSubTextFont return access Javax.Swing.Plaf.FontUIResource.Typ'Class;

   function GetDesktopColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetFocusColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWhite return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetBlack return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControl return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlShadow return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlDarkShadow return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlInfo return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlHighlight return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlDisabled return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControl return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlShadow return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlDarkShadow return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlInfo return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlHighlight return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetSystemTextColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlTextColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetInactiveControlTextColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetInactiveSystemTextColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetUserTextColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetTextHighlightColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetHighlightedTextColor return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowBackground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleBackground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleInactiveBackground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleInactiveForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuBackground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuSelectedBackground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuSelectedForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuDisabledForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetSeparatorBackground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetSeparatorForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetAcceleratorForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetAcceleratorSelectedForeground return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetLayoutStyle (This : access Typ)
                            return access Javax.Swing.LayoutStyle.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalLookAndFeel);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, IsNativeLookAndFeel, "isNativeLookAndFeel");
   pragma Import (Java, IsSupportedLookAndFeel, "isSupportedLookAndFeel");
   pragma Import (Java, GetSupportsWindowDecorations, "getSupportsWindowDecorations");
   pragma Import (Java, InitClassDefaults, "initClassDefaults");
   pragma Import (Java, InitSystemColorDefaults, "initSystemColorDefaults");
   pragma Import (Java, InitComponentDefaults, "initComponentDefaults");
   pragma Import (Java, CreateDefaultTheme, "createDefaultTheme");
   pragma Import (Java, GetDefaults, "getDefaults");
   pragma Import (Java, ProvideErrorFeedback, "provideErrorFeedback");
   pragma Import (Java, SetCurrentTheme, "setCurrentTheme");
   pragma Import (Java, GetCurrentTheme, "getCurrentTheme");
   pragma Import (Java, GetDisabledIcon, "getDisabledIcon");
   pragma Import (Java, GetDisabledSelectedIcon, "getDisabledSelectedIcon");
   pragma Import (Java, GetControlTextFont, "getControlTextFont");
   pragma Import (Java, GetSystemTextFont, "getSystemTextFont");
   pragma Import (Java, GetUserTextFont, "getUserTextFont");
   pragma Import (Java, GetMenuTextFont, "getMenuTextFont");
   pragma Import (Java, GetWindowTitleFont, "getWindowTitleFont");
   pragma Import (Java, GetSubTextFont, "getSubTextFont");
   pragma Import (Java, GetDesktopColor, "getDesktopColor");
   pragma Import (Java, GetFocusColor, "getFocusColor");
   pragma Import (Java, GetWhite, "getWhite");
   pragma Import (Java, GetBlack, "getBlack");
   pragma Import (Java, GetControl, "getControl");
   pragma Import (Java, GetControlShadow, "getControlShadow");
   pragma Import (Java, GetControlDarkShadow, "getControlDarkShadow");
   pragma Import (Java, GetControlInfo, "getControlInfo");
   pragma Import (Java, GetControlHighlight, "getControlHighlight");
   pragma Import (Java, GetControlDisabled, "getControlDisabled");
   pragma Import (Java, GetPrimaryControl, "getPrimaryControl");
   pragma Import (Java, GetPrimaryControlShadow, "getPrimaryControlShadow");
   pragma Import (Java, GetPrimaryControlDarkShadow, "getPrimaryControlDarkShadow");
   pragma Import (Java, GetPrimaryControlInfo, "getPrimaryControlInfo");
   pragma Import (Java, GetPrimaryControlHighlight, "getPrimaryControlHighlight");
   pragma Import (Java, GetSystemTextColor, "getSystemTextColor");
   pragma Import (Java, GetControlTextColor, "getControlTextColor");
   pragma Import (Java, GetInactiveControlTextColor, "getInactiveControlTextColor");
   pragma Import (Java, GetInactiveSystemTextColor, "getInactiveSystemTextColor");
   pragma Import (Java, GetUserTextColor, "getUserTextColor");
   pragma Import (Java, GetTextHighlightColor, "getTextHighlightColor");
   pragma Import (Java, GetHighlightedTextColor, "getHighlightedTextColor");
   pragma Import (Java, GetWindowBackground, "getWindowBackground");
   pragma Import (Java, GetWindowTitleBackground, "getWindowTitleBackground");
   pragma Import (Java, GetWindowTitleForeground, "getWindowTitleForeground");
   pragma Import (Java, GetWindowTitleInactiveBackground, "getWindowTitleInactiveBackground");
   pragma Import (Java, GetWindowTitleInactiveForeground, "getWindowTitleInactiveForeground");
   pragma Import (Java, GetMenuBackground, "getMenuBackground");
   pragma Import (Java, GetMenuForeground, "getMenuForeground");
   pragma Import (Java, GetMenuSelectedBackground, "getMenuSelectedBackground");
   pragma Import (Java, GetMenuSelectedForeground, "getMenuSelectedForeground");
   pragma Import (Java, GetMenuDisabledForeground, "getMenuDisabledForeground");
   pragma Import (Java, GetSeparatorBackground, "getSeparatorBackground");
   pragma Import (Java, GetSeparatorForeground, "getSeparatorForeground");
   pragma Import (Java, GetAcceleratorForeground, "getAcceleratorForeground");
   pragma Import (Java, GetAcceleratorSelectedForeground, "getAcceleratorSelectedForeground");
   pragma Import (Java, GetLayoutStyle, "getLayoutStyle");

end Javax.Swing.Plaf.Metal.MetalLookAndFeel;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalLookAndFeel, "javax.swing.plaf.metal.MetalLookAndFeel");
pragma Extensions_Allowed (Off);
