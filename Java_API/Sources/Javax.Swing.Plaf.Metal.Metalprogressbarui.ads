pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicProgressBarUI;

package Javax.Swing.Plaf.Metal.MetalProgressBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicProgressBarUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalProgressBarUI (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure PaintDeterminate (This : access Typ;
                               P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure PaintIndeterminate (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalProgressBarUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, PaintDeterminate, "paintDeterminate");
   pragma Import (Java, PaintIndeterminate, "paintIndeterminate");

end Javax.Swing.Plaf.Metal.MetalProgressBarUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalProgressBarUI, "javax.swing.plaf.metal.MetalProgressBarUI");
pragma Extensions_Allowed (Off);
