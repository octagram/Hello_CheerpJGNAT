pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Javax.Swing.AbstractButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicRadioButtonUI;

package Javax.Swing.Plaf.Metal.MetalRadioButtonUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicRadioButtonUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      FocusColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, FocusColor, "focusColor");

      --  protected
      SelectColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectColor, "selectColor");

      --  protected
      DisabledTextColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, DisabledTextColor, "disabledTextColor");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalRadioButtonUI (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallDefaults (This : access Typ;
                              P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   procedure UninstallDefaults (This : access Typ;
                                P1_AbstractButton : access Standard.Javax.Swing.AbstractButton.Typ'Class);

   --  protected
   function GetSelectColor (This : access Typ)
                            return access Java.Awt.Color.Typ'Class;

   --  protected
   function GetDisabledTextColor (This : access Typ)
                                  return access Java.Awt.Color.Typ'Class;

   --  protected
   function GetFocusColor (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   --  synchronized
   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintFocus (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                         P3_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalRadioButtonUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, UninstallDefaults, "uninstallDefaults");
   pragma Import (Java, GetSelectColor, "getSelectColor");
   pragma Import (Java, GetDisabledTextColor, "getDisabledTextColor");
   pragma Import (Java, GetFocusColor, "getFocusColor");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintFocus, "paintFocus");

end Javax.Swing.Plaf.Metal.MetalRadioButtonUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalRadioButtonUI, "javax.swing.plaf.metal.MetalRadioButtonUI");
pragma Extensions_Allowed (Off);
