pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Beans.PropertyChangeListener;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicRootPaneUI;

package Javax.Swing.Plaf.Metal.MetalRootPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref)
    is new Javax.Swing.Plaf.Basic.BasicRootPaneUI.Typ(PropertyChangeListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalRootPaneUI (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalRootPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, PropertyChange, "propertyChange");

end Javax.Swing.Plaf.Metal.MetalRootPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalRootPaneUI, "javax.swing.plaf.metal.MetalRootPaneUI");
pragma Extensions_Allowed (Off);
