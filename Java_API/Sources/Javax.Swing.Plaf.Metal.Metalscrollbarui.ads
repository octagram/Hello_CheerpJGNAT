pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.JButton;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Plaf.Metal.MetalScrollButton;
with Java.Awt.LayoutManager;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicScrollBarUI;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Metal.MetalScrollBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Plaf.Basic.BasicScrollBarUI.Typ(LayoutManager_I,
                                                       SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      IncreaseButton : access Javax.Swing.Plaf.Metal.MetalScrollButton.Typ'Class;
      pragma Import (Java, IncreaseButton, "increaseButton");

      --  protected
      DecreaseButton : access Javax.Swing.Plaf.Metal.MetalScrollButton.Typ'Class;
      pragma Import (Java, DecreaseButton, "decreaseButton");

      --  protected
      ScrollBarWidth : Java.Int;
      pragma Import (Java, ScrollBarWidth, "scrollBarWidth");

      --  protected
      IsFreeStanding : Java.Boolean;
      pragma Import (Java, IsFreeStanding, "isFreeStanding");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalScrollBarUI (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   function CreatePropertyChangeListener (This : access Typ)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   procedure ConfigureScrollBarColors (This : access Typ);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   --  protected
   function CreateDecreaseButton (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Javax.Swing.JButton.Typ'Class;

   --  protected
   function CreateIncreaseButton (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Javax.Swing.JButton.Typ'Class;

   --  protected
   procedure PaintTrack (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure PaintThumb (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function GetMinimumThumbSize (This : access Typ)
                                 return access Java.Awt.Dimension.Typ'Class;

   --  protected
   procedure SetThumbBounds (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FREE_STANDING_PROP : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalScrollBarUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, ConfigureScrollBarColors, "configureScrollBarColors");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, CreateDecreaseButton, "createDecreaseButton");
   pragma Import (Java, CreateIncreaseButton, "createIncreaseButton");
   pragma Import (Java, PaintTrack, "paintTrack");
   pragma Import (Java, PaintThumb, "paintThumb");
   pragma Import (Java, GetMinimumThumbSize, "getMinimumThumbSize");
   pragma Import (Java, SetThumbBounds, "setThumbBounds");
   pragma Import (Java, FREE_STANDING_PROP, "FREE_STANDING_PROP");

end Javax.Swing.Plaf.Metal.MetalScrollBarUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalScrollBarUI, "javax.swing.plaf.metal.MetalScrollBarUI");
pragma Extensions_Allowed (Off);
