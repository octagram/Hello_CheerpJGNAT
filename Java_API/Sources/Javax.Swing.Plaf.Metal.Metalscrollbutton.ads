pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
with Java.Awt.Image.ImageObserver;
with Java.Awt.ItemSelectable;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Plaf.Basic.BasicArrowButton;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Metal.MetalScrollButton is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ItemSelectable_I : Java.Awt.ItemSelectable.Ref;
            MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Plaf.Basic.BasicArrowButton.Typ(ItemSelectable_I,
                                                       MenuContainer_I,
                                                       ImageObserver_I,
                                                       Serializable_I,
                                                       Accessible_I,
                                                       SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalScrollButton (P1_Int : Java.Int;
                                   P2_Int : Java.Int;
                                   P3_Boolean : Java.Boolean; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFreeStanding (This : access Typ;
                              P1_Boolean : Java.Boolean);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetButtonWidth (This : access Typ)
                            return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalScrollButton);
   pragma Import (Java, SetFreeStanding, "setFreeStanding");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetButtonWidth, "getButtonWidth");

end Javax.Swing.Plaf.Metal.MetalScrollButton;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalScrollButton, "javax.swing.plaf.metal.MetalScrollButton");
pragma Extensions_Allowed (Off);
