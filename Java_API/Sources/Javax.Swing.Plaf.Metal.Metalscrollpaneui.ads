pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JScrollPane;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicScrollPaneUI;
with Javax.Swing.ScrollPaneConstants;

package Javax.Swing.Plaf.Metal.MetalScrollPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ScrollPaneConstants_I : Javax.Swing.ScrollPaneConstants.Ref)
    is new Javax.Swing.Plaf.Basic.BasicScrollPaneUI.Typ(ScrollPaneConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalScrollPaneUI (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure InstallListeners (This : access Typ;
                               P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   procedure UninstallListeners (This : access Typ;
                                 P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   --  protected
   function CreateScrollBarSwapListener (This : access Typ)
                                         return access Java.Beans.PropertyChangeListener.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalScrollPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, CreateScrollBarSwapListener, "createScrollBarSwapListener");

end Javax.Swing.Plaf.Metal.MetalScrollPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalScrollPaneUI, "javax.swing.plaf.metal.MetalScrollPaneUI");
pragma Extensions_Allowed (Off);
