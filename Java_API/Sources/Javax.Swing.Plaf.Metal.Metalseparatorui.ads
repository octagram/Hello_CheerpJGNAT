pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JSeparator;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicSeparatorUI;

package Javax.Swing.Plaf.Metal.MetalSeparatorUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicSeparatorUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalSeparatorUI (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   procedure InstallDefaults (This : access Typ;
                              P1_JSeparator : access Standard.Javax.Swing.JSeparator.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalSeparatorUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");

end Javax.Swing.Plaf.Metal.MetalSeparatorUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalSeparatorUI, "javax.swing.plaf.metal.MetalSeparatorUI");
pragma Extensions_Allowed (Off);
