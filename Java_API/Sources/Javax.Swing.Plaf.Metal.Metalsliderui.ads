pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JSlider;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicSliderUI;

package Javax.Swing.Plaf.Metal.MetalSliderUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicSliderUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      TICK_BUFFER : Java.Int;
      pragma Import (Java, TICK_BUFFER, "TICK_BUFFER");

      --  protected
      FilledSlider : Java.Boolean;
      pragma Import (Java, FilledSlider, "filledSlider");

      --  protected  final
      SLIDER_FILL : access Java.Lang.String.Typ'Class;
      pragma Import (Java, SLIDER_FILL, "SLIDER_FILL");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalSliderUI (This : Ref := null)
                               return Ref;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   function CreatePropertyChangeListener (This : access Typ;
                                          P1_JSlider : access Standard.Javax.Swing.JSlider.Typ'Class)
                                          return access Java.Beans.PropertyChangeListener.Typ'Class;

   procedure PaintThumb (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PaintTrack (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure PaintFocus (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  protected
   function GetThumbSize (This : access Typ)
                          return access Java.Awt.Dimension.Typ'Class;

   function GetTickLength (This : access Typ)
                           return Java.Int;

   --  protected
   function GetTrackWidth (This : access Typ)
                           return Java.Int;

   --  protected
   function GetTrackLength (This : access Typ)
                            return Java.Int;

   --  protected
   function GetThumbOverhang (This : access Typ)
                              return Java.Int;

   --  protected
   procedure ScrollDueToClickInTrack (This : access Typ;
                                      P1_Int : Java.Int);

   --  protected
   procedure PaintMinorTickForHorizSlider (This : access Typ;
                                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                           P3_Int : Java.Int);

   --  protected
   procedure PaintMajorTickForHorizSlider (This : access Typ;
                                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                           P3_Int : Java.Int);

   --  protected
   procedure PaintMinorTickForVertSlider (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                          P3_Int : Java.Int);

   --  protected
   procedure PaintMajorTickForVertSlider (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                          P3_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   ThumbColor : access Java.Awt.Color.Typ'Class;

   --  protected
   HighlightColor : access Java.Awt.Color.Typ'Class;

   --  protected
   DarkShadowColor : access Java.Awt.Color.Typ'Class;

   --  protected
   TrackWidth : Java.Int;

   --  protected
   TickLength : Java.Int;

   --  protected
   HorizThumbIcon : access Javax.Swing.Icon.Typ'Class;

   --  protected
   VertThumbIcon : access Javax.Swing.Icon.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_MetalSliderUI);
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, CreatePropertyChangeListener, "createPropertyChangeListener");
   pragma Import (Java, PaintThumb, "paintThumb");
   pragma Import (Java, PaintTrack, "paintTrack");
   pragma Import (Java, PaintFocus, "paintFocus");
   pragma Import (Java, GetThumbSize, "getThumbSize");
   pragma Import (Java, GetTickLength, "getTickLength");
   pragma Import (Java, GetTrackWidth, "getTrackWidth");
   pragma Import (Java, GetTrackLength, "getTrackLength");
   pragma Import (Java, GetThumbOverhang, "getThumbOverhang");
   pragma Import (Java, ScrollDueToClickInTrack, "scrollDueToClickInTrack");
   pragma Import (Java, PaintMinorTickForHorizSlider, "paintMinorTickForHorizSlider");
   pragma Import (Java, PaintMajorTickForHorizSlider, "paintMajorTickForHorizSlider");
   pragma Import (Java, PaintMinorTickForVertSlider, "paintMinorTickForVertSlider");
   pragma Import (Java, PaintMajorTickForVertSlider, "paintMajorTickForVertSlider");
   pragma Import (Java, ThumbColor, "thumbColor");
   pragma Import (Java, HighlightColor, "highlightColor");
   pragma Import (Java, DarkShadowColor, "darkShadowColor");
   pragma Import (Java, TrackWidth, "trackWidth");
   pragma Import (Java, TickLength, "tickLength");
   pragma Import (Java, HorizThumbIcon, "horizThumbIcon");
   pragma Import (Java, VertThumbIcon, "vertThumbIcon");

end Javax.Swing.Plaf.Metal.MetalSliderUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalSliderUI, "javax.swing.plaf.metal.MetalSliderUI");
pragma Extensions_Allowed (Off);
