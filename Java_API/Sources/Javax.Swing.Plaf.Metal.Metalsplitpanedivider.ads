pragma Extensions_Allowed (On);
package Javax.Swing.Plaf.Metal.MetalSplitPaneDivider is
   pragma Preelaborate;
end Javax.Swing.Plaf.Metal.MetalSplitPaneDivider;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalSplitPaneDivider, "javax.swing.plaf.metal.MetalSplitPaneDivider");
pragma Extensions_Allowed (Off);
