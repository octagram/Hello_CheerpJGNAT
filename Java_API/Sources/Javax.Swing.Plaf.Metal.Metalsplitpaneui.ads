pragma Extensions_Allowed (On);
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.Basic.BasicSplitPaneDivider;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicSplitPaneUI;

package Javax.Swing.Plaf.Metal.MetalSplitPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicSplitPaneUI.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalSplitPaneUI (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   function CreateDefaultDivider (This : access Typ)
                                  return access Javax.Swing.Plaf.Basic.BasicSplitPaneDivider.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalSplitPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, CreateDefaultDivider, "createDefaultDivider");

end Javax.Swing.Plaf.Metal.MetalSplitPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalSplitPaneUI, "javax.swing.plaf.metal.MetalSplitPaneUI");
pragma Extensions_Allowed (Off);
