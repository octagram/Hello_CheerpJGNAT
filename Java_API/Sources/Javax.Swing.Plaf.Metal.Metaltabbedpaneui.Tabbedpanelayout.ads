pragma Extensions_Allowed (On);
with Java.Awt.LayoutManager;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.TabbedPaneLayout;

package Javax.Swing.Plaf.Metal.MetalTabbedPaneUI.TabbedPaneLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref)
    is new Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.TabbedPaneLayout.Typ(LayoutManager_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TabbedPaneLayout (P1_MetalTabbedPaneUI : access Standard.Javax.Swing.Plaf.Metal.MetalTabbedPaneUI.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure NormalizeTabRuns (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int);

   --  protected
   procedure RotateTabRuns (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);

   --  protected
   procedure PadSelectedTab (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TabbedPaneLayout);
   pragma Import (Java, NormalizeTabRuns, "normalizeTabRuns");
   pragma Import (Java, RotateTabRuns, "rotateTabRuns");
   pragma Import (Java, PadSelectedTab, "padSelectedTab");

end Javax.Swing.Plaf.Metal.MetalTabbedPaneUI.TabbedPaneLayout;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalTabbedPaneUI.TabbedPaneLayout, "javax.swing.plaf.metal.MetalTabbedPaneUI$TabbedPaneLayout");
pragma Extensions_Allowed (Off);
