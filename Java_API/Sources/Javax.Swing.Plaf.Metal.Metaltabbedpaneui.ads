pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Graphics;
limited with Java.Awt.LayoutManager;
limited with Java.Awt.Rectangle;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicTabbedPaneUI;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Metal.MetalTabbedPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Plaf.Basic.BasicTabbedPaneUI.Typ(SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      MinTabWidth : Java.Int;
      pragma Import (Java, MinTabWidth, "minTabWidth");

      --  protected
      TabAreaBackground : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, TabAreaBackground, "tabAreaBackground");

      --  protected
      SelectColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectColor, "selectColor");

      --  protected
      SelectHighlight : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, SelectHighlight, "selectHighlight");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalTabbedPaneUI (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function CreateLayoutManager (This : access Typ)
                                 return access Java.Awt.LayoutManager.Typ'Class;

   --  protected
   procedure InstallDefaults (This : access Typ);

   --  protected
   procedure PaintTabBorder (This : access Typ;
                             P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int;
                             P8_Boolean : Java.Boolean);

   --  protected
   procedure PaintTopTabBorder (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int;
                                P7_Int : Java.Int;
                                P8_Int : Java.Int;
                                P9_Boolean : Java.Boolean);

   --  protected
   function ShouldFillGap (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int;
                           P4_Int : Java.Int)
                           return Java.Boolean;

   --  protected
   function GetColorForGap (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return access Java.Awt.Color.Typ'Class;

   --  protected
   procedure PaintLeftTabBorder (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Int : Java.Int;
                                 P9_Boolean : Java.Boolean);

   --  protected
   procedure PaintBottomTabBorder (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int;
                                   P7_Int : Java.Int;
                                   P8_Int : Java.Int;
                                   P9_Boolean : Java.Boolean);

   --  protected
   procedure PaintRightTabBorder (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int;
                                  P7_Int : Java.Int;
                                  P8_Int : Java.Int;
                                  P9_Boolean : Java.Boolean);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintTabBackground (This : access Typ;
                                 P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Boolean : Java.Boolean);

   --  protected
   function GetTabLabelShiftX (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Boolean : Java.Boolean)
                               return Java.Int;

   --  protected
   function GetTabLabelShiftY (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int;
                               P3_Boolean : Java.Boolean)
                               return Java.Int;

   --  protected
   function GetBaselineOffset (This : access Typ)
                               return Java.Int;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintHighlightBelowTab (This : access Typ);

   --  protected
   procedure PaintFocusIndicator (This : access Typ;
                                  P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Rectangle_Arr : access Java.Awt.Rectangle.Arr_Obj;
                                  P4_Int : Java.Int;
                                  P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                  P6_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                  P7_Boolean : Java.Boolean);

   --  protected
   procedure PaintContentBorderTopEdge (This : access Typ;
                                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int;
                                        P7_Int : Java.Int);

   --  protected
   procedure PaintContentBorderBottomEdge (This : access Typ;
                                           P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P2_Int : Java.Int;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int;
                                           P7_Int : Java.Int);

   --  protected
   procedure PaintContentBorderLeftEdge (This : access Typ;
                                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int;
                                         P7_Int : Java.Int);

   --  protected
   procedure PaintContentBorderRightEdge (This : access Typ;
                                          P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P2_Int : Java.Int;
                                          P3_Int : Java.Int;
                                          P4_Int : Java.Int;
                                          P5_Int : Java.Int;
                                          P6_Int : Java.Int;
                                          P7_Int : Java.Int);

   --  protected
   function CalculateMaxTabHeight (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Int;

   --  protected
   function GetTabRunOverlay (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   --  protected
   function ShouldRotateTabRuns (This : access Typ;
                                 P1_Int : Java.Int;
                                 P2_Int : Java.Int)
                                 return Java.Boolean;

   --  protected
   function ShouldPadTabRun (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalTabbedPaneUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, CreateLayoutManager, "createLayoutManager");
   pragma Import (Java, InstallDefaults, "installDefaults");
   pragma Import (Java, PaintTabBorder, "paintTabBorder");
   pragma Import (Java, PaintTopTabBorder, "paintTopTabBorder");
   pragma Import (Java, ShouldFillGap, "shouldFillGap");
   pragma Import (Java, GetColorForGap, "getColorForGap");
   pragma Import (Java, PaintLeftTabBorder, "paintLeftTabBorder");
   pragma Import (Java, PaintBottomTabBorder, "paintBottomTabBorder");
   pragma Import (Java, PaintRightTabBorder, "paintRightTabBorder");
   pragma Import (Java, Update, "update");
   pragma Import (Java, PaintTabBackground, "paintTabBackground");
   pragma Import (Java, GetTabLabelShiftX, "getTabLabelShiftX");
   pragma Import (Java, GetTabLabelShiftY, "getTabLabelShiftY");
   pragma Import (Java, GetBaselineOffset, "getBaselineOffset");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintHighlightBelowTab, "paintHighlightBelowTab");
   pragma Import (Java, PaintFocusIndicator, "paintFocusIndicator");
   pragma Import (Java, PaintContentBorderTopEdge, "paintContentBorderTopEdge");
   pragma Import (Java, PaintContentBorderBottomEdge, "paintContentBorderBottomEdge");
   pragma Import (Java, PaintContentBorderLeftEdge, "paintContentBorderLeftEdge");
   pragma Import (Java, PaintContentBorderRightEdge, "paintContentBorderRightEdge");
   pragma Import (Java, CalculateMaxTabHeight, "calculateMaxTabHeight");
   pragma Import (Java, GetTabRunOverlay, "getTabRunOverlay");
   pragma Import (Java, ShouldRotateTabRuns, "shouldRotateTabRuns");
   pragma Import (Java, ShouldPadTabRun, "shouldPadTabRun");

end Javax.Swing.Plaf.Metal.MetalTabbedPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalTabbedPaneUI, "javax.swing.plaf.metal.MetalTabbedPaneUI");
pragma Extensions_Allowed (Off);
