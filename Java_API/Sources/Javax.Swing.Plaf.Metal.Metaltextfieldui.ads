pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicTextFieldUI;
with Javax.Swing.Text.ViewFactory;

package Javax.Swing.Plaf.Metal.MetalTextFieldUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ViewFactory_I : Javax.Swing.Text.ViewFactory.Ref)
    is new Javax.Swing.Plaf.Basic.BasicTextFieldUI.Typ(ViewFactory_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalTextFieldUI (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalTextFieldUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, PropertyChange, "propertyChange");

end Javax.Swing.Plaf.Metal.MetalTextFieldUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalTextFieldUI, "javax.swing.plaf.metal.MetalTextFieldUI");
pragma Extensions_Allowed (Off);
