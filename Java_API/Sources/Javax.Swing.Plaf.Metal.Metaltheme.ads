pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Plaf.ColorUIResource;
limited with Javax.Swing.Plaf.FontUIResource;
limited with Javax.Swing.UIDefaults;
with Java.Lang.Object;

package Javax.Swing.Plaf.Metal.MetalTheme is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MetalTheme (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   --  protected
   function GetPrimary1 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class is abstract;

   --  protected
   function GetPrimary2 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class is abstract;

   --  protected
   function GetPrimary3 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class is abstract;

   --  protected
   function GetSecondary1 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class is abstract;

   --  protected
   function GetSecondary2 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class is abstract;

   --  protected
   function GetSecondary3 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class is abstract;

   function GetControlTextFont (This : access Typ)
                                return access Javax.Swing.Plaf.FontUIResource.Typ'Class is abstract;

   function GetSystemTextFont (This : access Typ)
                               return access Javax.Swing.Plaf.FontUIResource.Typ'Class is abstract;

   function GetUserTextFont (This : access Typ)
                             return access Javax.Swing.Plaf.FontUIResource.Typ'Class is abstract;

   function GetMenuTextFont (This : access Typ)
                             return access Javax.Swing.Plaf.FontUIResource.Typ'Class is abstract;

   function GetWindowTitleFont (This : access Typ)
                                return access Javax.Swing.Plaf.FontUIResource.Typ'Class is abstract;

   function GetSubTextFont (This : access Typ)
                            return access Javax.Swing.Plaf.FontUIResource.Typ'Class is abstract;

   --  protected
   function GetWhite (This : access Typ)
                      return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetBlack (This : access Typ)
                      return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetFocusColor (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetDesktopColor (This : access Typ)
                             return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControl (This : access Typ)
                        return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlShadow (This : access Typ)
                              return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlDarkShadow (This : access Typ)
                                  return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlInfo (This : access Typ)
                            return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlHighlight (This : access Typ)
                                 return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlDisabled (This : access Typ)
                                return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControl (This : access Typ)
                               return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlShadow (This : access Typ)
                                     return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlDarkShadow (This : access Typ)
                                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlInfo (This : access Typ)
                                   return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetPrimaryControlHighlight (This : access Typ)
                                        return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetSystemTextColor (This : access Typ)
                                return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlTextColor (This : access Typ)
                                 return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetInactiveControlTextColor (This : access Typ)
                                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetInactiveSystemTextColor (This : access Typ)
                                        return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetUserTextColor (This : access Typ)
                              return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetTextHighlightColor (This : access Typ)
                                   return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetHighlightedTextColor (This : access Typ)
                                     return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowBackground (This : access Typ)
                                 return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleBackground (This : access Typ)
                                      return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleForeground (This : access Typ)
                                      return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleInactiveBackground (This : access Typ)
                                              return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetWindowTitleInactiveForeground (This : access Typ)
                                              return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuBackground (This : access Typ)
                               return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuForeground (This : access Typ)
                               return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuSelectedBackground (This : access Typ)
                                       return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuSelectedForeground (This : access Typ)
                                       return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuDisabledForeground (This : access Typ)
                                       return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetSeparatorBackground (This : access Typ)
                                    return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetSeparatorForeground (This : access Typ)
                                    return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetAcceleratorForeground (This : access Typ)
                                      return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetAcceleratorSelectedForeground (This : access Typ)
                                              return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   procedure AddCustomEntriesToTable (This : access Typ;
                                      P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalTheme);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetPrimary1, "getPrimary1");
   pragma Export (Java, GetPrimary2, "getPrimary2");
   pragma Export (Java, GetPrimary3, "getPrimary3");
   pragma Export (Java, GetSecondary1, "getSecondary1");
   pragma Export (Java, GetSecondary2, "getSecondary2");
   pragma Export (Java, GetSecondary3, "getSecondary3");
   pragma Export (Java, GetControlTextFont, "getControlTextFont");
   pragma Export (Java, GetSystemTextFont, "getSystemTextFont");
   pragma Export (Java, GetUserTextFont, "getUserTextFont");
   pragma Export (Java, GetMenuTextFont, "getMenuTextFont");
   pragma Export (Java, GetWindowTitleFont, "getWindowTitleFont");
   pragma Export (Java, GetSubTextFont, "getSubTextFont");
   pragma Import (Java, GetWhite, "getWhite");
   pragma Import (Java, GetBlack, "getBlack");
   pragma Import (Java, GetFocusColor, "getFocusColor");
   pragma Import (Java, GetDesktopColor, "getDesktopColor");
   pragma Import (Java, GetControl, "getControl");
   pragma Import (Java, GetControlShadow, "getControlShadow");
   pragma Import (Java, GetControlDarkShadow, "getControlDarkShadow");
   pragma Import (Java, GetControlInfo, "getControlInfo");
   pragma Import (Java, GetControlHighlight, "getControlHighlight");
   pragma Import (Java, GetControlDisabled, "getControlDisabled");
   pragma Import (Java, GetPrimaryControl, "getPrimaryControl");
   pragma Import (Java, GetPrimaryControlShadow, "getPrimaryControlShadow");
   pragma Import (Java, GetPrimaryControlDarkShadow, "getPrimaryControlDarkShadow");
   pragma Import (Java, GetPrimaryControlInfo, "getPrimaryControlInfo");
   pragma Import (Java, GetPrimaryControlHighlight, "getPrimaryControlHighlight");
   pragma Import (Java, GetSystemTextColor, "getSystemTextColor");
   pragma Import (Java, GetControlTextColor, "getControlTextColor");
   pragma Import (Java, GetInactiveControlTextColor, "getInactiveControlTextColor");
   pragma Import (Java, GetInactiveSystemTextColor, "getInactiveSystemTextColor");
   pragma Import (Java, GetUserTextColor, "getUserTextColor");
   pragma Import (Java, GetTextHighlightColor, "getTextHighlightColor");
   pragma Import (Java, GetHighlightedTextColor, "getHighlightedTextColor");
   pragma Import (Java, GetWindowBackground, "getWindowBackground");
   pragma Import (Java, GetWindowTitleBackground, "getWindowTitleBackground");
   pragma Import (Java, GetWindowTitleForeground, "getWindowTitleForeground");
   pragma Import (Java, GetWindowTitleInactiveBackground, "getWindowTitleInactiveBackground");
   pragma Import (Java, GetWindowTitleInactiveForeground, "getWindowTitleInactiveForeground");
   pragma Import (Java, GetMenuBackground, "getMenuBackground");
   pragma Import (Java, GetMenuForeground, "getMenuForeground");
   pragma Import (Java, GetMenuSelectedBackground, "getMenuSelectedBackground");
   pragma Import (Java, GetMenuSelectedForeground, "getMenuSelectedForeground");
   pragma Import (Java, GetMenuDisabledForeground, "getMenuDisabledForeground");
   pragma Import (Java, GetSeparatorBackground, "getSeparatorBackground");
   pragma Import (Java, GetSeparatorForeground, "getSeparatorForeground");
   pragma Import (Java, GetAcceleratorForeground, "getAcceleratorForeground");
   pragma Import (Java, GetAcceleratorSelectedForeground, "getAcceleratorSelectedForeground");
   pragma Import (Java, AddCustomEntriesToTable, "addCustomEntriesToTable");

end Javax.Swing.Plaf.Metal.MetalTheme;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalTheme, "javax.swing.plaf.metal.MetalTheme");
pragma Extensions_Allowed (Off);
