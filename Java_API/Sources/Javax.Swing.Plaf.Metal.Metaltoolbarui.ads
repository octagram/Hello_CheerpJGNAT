pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.ContainerListener;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.Event.MouseInputListener;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicToolBarUI;
with Javax.Swing.SwingConstants;

package Javax.Swing.Plaf.Metal.MetalToolBarUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Plaf.Basic.BasicToolBarUI.Typ(SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ContListener : access Java.Awt.Event.ContainerListener.Typ'Class;
      pragma Import (Java, ContListener, "contListener");

      --  protected
      RolloverListener : access Java.Beans.PropertyChangeListener.Typ'Class;
      pragma Import (Java, RolloverListener, "rolloverListener");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalToolBarUI (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure InstallListeners (This : access Typ);

   --  protected
   procedure UninstallListeners (This : access Typ);

   --  protected
   function CreateRolloverBorder (This : access Typ)
                                  return access Javax.Swing.Border.Border.Typ'Class;

   --  protected
   function CreateNonRolloverBorder (This : access Typ)
                                     return access Javax.Swing.Border.Border.Typ'Class;

   --  protected
   procedure SetBorderToNonRollover (This : access Typ;
                                     P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   --  protected
   function CreateContainerListener (This : access Typ)
                                     return access Java.Awt.Event.ContainerListener.Typ'Class;

   --  protected
   function CreateRolloverListener (This : access Typ)
                                    return access Java.Beans.PropertyChangeListener.Typ'Class;

   --  protected
   function CreateDockingListener (This : access Typ)
                                   return access Javax.Swing.Event.MouseInputListener.Typ'Class;

   --  protected
   procedure SetDragOffset (This : access Typ;
                            P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MetalToolBarUI);
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, InstallListeners, "installListeners");
   pragma Import (Java, UninstallListeners, "uninstallListeners");
   pragma Import (Java, CreateRolloverBorder, "createRolloverBorder");
   pragma Import (Java, CreateNonRolloverBorder, "createNonRolloverBorder");
   pragma Import (Java, SetBorderToNonRollover, "setBorderToNonRollover");
   pragma Import (Java, CreateContainerListener, "createContainerListener");
   pragma Import (Java, CreateRolloverListener, "createRolloverListener");
   pragma Import (Java, CreateDockingListener, "createDockingListener");
   pragma Import (Java, SetDragOffset, "setDragOffset");
   pragma Import (Java, Update, "update");

end Javax.Swing.Plaf.Metal.MetalToolBarUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalToolBarUI, "javax.swing.plaf.metal.MetalToolBarUI");
pragma Extensions_Allowed (Off);
