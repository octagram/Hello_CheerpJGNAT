pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Insets;
limited with Java.Awt.Rectangle;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Tree.TreePath;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicTreeUI;

package Javax.Swing.Plaf.Metal.MetalTreeUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Basic.BasicTreeUI.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MetalTreeUI (This : Ref := null)
                             return Ref;

   --  protected
   function GetHorizontalLegBuffer (This : access Typ)
                                    return Java.Int;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure DecodeLineStyle (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  protected
   function IsLocationInExpandControl (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int)
                                       return Java.Boolean;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintHorizontalSeparators (This : access Typ;
                                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  protected
   procedure PaintVerticalPartOfLeg (This : access Typ;
                                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                     P3_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                                     P4_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   --  protected
   procedure PaintHorizontalPartOfLeg (This : access Typ;
                                       P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                       P3_Insets : access Standard.Java.Awt.Insets.Typ'Class;
                                       P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                       P5_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                       P6_Int : Java.Int;
                                       P7_Boolean : Java.Boolean;
                                       P8_Boolean : Java.Boolean;
                                       P9_Boolean : Java.Boolean);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_MetalTreeUI);
   pragma Import (Java, GetHorizontalLegBuffer, "getHorizontalLegBuffer");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, DecodeLineStyle, "decodeLineStyle");
   pragma Import (Java, IsLocationInExpandControl, "isLocationInExpandControl");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintHorizontalSeparators, "paintHorizontalSeparators");
   pragma Import (Java, PaintVerticalPartOfLeg, "paintVerticalPartOfLeg");
   pragma Import (Java, PaintHorizontalPartOfLeg, "paintHorizontalPartOfLeg");

end Javax.Swing.Plaf.Metal.MetalTreeUI;
pragma Import (Java, Javax.Swing.Plaf.Metal.MetalTreeUI, "javax.swing.plaf.metal.MetalTreeUI");
pragma Extensions_Allowed (Off);
