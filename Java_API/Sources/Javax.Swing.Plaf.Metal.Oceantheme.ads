pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Plaf.ColorUIResource;
limited with Javax.Swing.UIDefaults;
with Java.Lang.Object;
with Javax.Swing.Plaf.Metal.DefaultMetalTheme;

package Javax.Swing.Plaf.Metal.OceanTheme is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.Metal.DefaultMetalTheme.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_OceanTheme (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddCustomEntriesToTable (This : access Typ;
                                      P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class);

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   --  protected
   function GetPrimary1 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetPrimary2 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetPrimary3 (This : access Typ)
                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetSecondary1 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetSecondary2 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetSecondary3 (This : access Typ)
                           return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   --  protected
   function GetBlack (This : access Typ)
                      return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetDesktopColor (This : access Typ)
                             return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetInactiveControlTextColor (This : access Typ)
                                         return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetControlTextColor (This : access Typ)
                                 return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;

   function GetMenuDisabledForeground (This : access Typ)
                                       return access Javax.Swing.Plaf.ColorUIResource.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OceanTheme);
   pragma Import (Java, AddCustomEntriesToTable, "addCustomEntriesToTable");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetPrimary1, "getPrimary1");
   pragma Import (Java, GetPrimary2, "getPrimary2");
   pragma Import (Java, GetPrimary3, "getPrimary3");
   pragma Import (Java, GetSecondary1, "getSecondary1");
   pragma Import (Java, GetSecondary2, "getSecondary2");
   pragma Import (Java, GetSecondary3, "getSecondary3");
   pragma Import (Java, GetBlack, "getBlack");
   pragma Import (Java, GetDesktopColor, "getDesktopColor");
   pragma Import (Java, GetInactiveControlTextColor, "getInactiveControlTextColor");
   pragma Import (Java, GetControlTextColor, "getControlTextColor");
   pragma Import (Java, GetMenuDisabledForeground, "getMenuDisabledForeground");

end Javax.Swing.Plaf.Metal.OceanTheme;
pragma Import (Java, Javax.Swing.Plaf.Metal.OceanTheme, "javax.swing.plaf.metal.OceanTheme");
pragma Extensions_Allowed (Off);
