pragma Extensions_Allowed (On);
package Javax.Swing.Plaf.Metal is
   pragma Preelaborate;
end Javax.Swing.Plaf.Metal;
pragma Import (Java, Javax.Swing.Plaf.Metal, "javax.swing.plaf.metal");
pragma Extensions_Allowed (Off);
