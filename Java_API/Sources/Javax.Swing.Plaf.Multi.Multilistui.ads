pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Util.Vector;
limited with Javax.Accessibility.Accessible;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JList;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.ListUI;

package Javax.Swing.Plaf.Multi.MultiListUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.ListUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Uis : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Uis, "uis");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MultiListUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIs (This : access Typ)
                    return Standard.Java.Lang.Object.Ref;

   function LocationToIndex (This : access Typ;
                             P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                             P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return Java.Int;

   function IndexToLocation (This : access Typ;
                             P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                             P2_Int : Java.Int)
                             return access Java.Awt.Point.Typ'Class;

   function GetCellBounds (This : access Typ;
                           P1_JList : access Standard.Javax.Swing.JList.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Int : Java.Int)
                           return access Java.Awt.Rectangle.Typ'Class;

   function Contains (This : access Typ;
                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int)
                      return Java.Boolean;

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetAccessibleChildrenCount (This : access Typ;
                                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                        return Java.Int;

   function GetAccessibleChild (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MultiListUI);
   pragma Import (Java, GetUIs, "getUIs");
   pragma Import (Java, LocationToIndex, "locationToIndex");
   pragma Import (Java, IndexToLocation, "indexToLocation");
   pragma Import (Java, GetCellBounds, "getCellBounds");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Update, "update");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");

end Javax.Swing.Plaf.Multi.MultiListUI;
pragma Import (Java, Javax.Swing.Plaf.Multi.MultiListUI, "javax.swing.plaf.multi.MultiListUI");
pragma Extensions_Allowed (Off);
