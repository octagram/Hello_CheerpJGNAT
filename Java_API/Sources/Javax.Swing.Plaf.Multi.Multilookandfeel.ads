pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.UIDefaults;
with Java.Lang.Object;
with Javax.Swing.LookAndFeel;

package Javax.Swing.Plaf.Multi.MultiLookAndFeel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.LookAndFeel.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MultiLookAndFeel (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function IsNativeLookAndFeel (This : access Typ)
                                 return Java.Boolean;

   function IsSupportedLookAndFeel (This : access Typ)
                                    return Java.Boolean;

   function GetDefaults (This : access Typ)
                         return access Javax.Swing.UIDefaults.Typ'Class;

   function CreateUIs (P1_ComponentUI : access Standard.Javax.Swing.Plaf.ComponentUI.Typ'Class;
                       P2_Vector : access Standard.Java.Util.Vector.Typ'Class;
                       P3_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                       return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  protected
   function UisToArray (P1_Vector : access Standard.Java.Util.Vector.Typ'Class)
                        return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MultiLookAndFeel);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, IsNativeLookAndFeel, "isNativeLookAndFeel");
   pragma Import (Java, IsSupportedLookAndFeel, "isSupportedLookAndFeel");
   pragma Import (Java, GetDefaults, "getDefaults");
   pragma Import (Java, CreateUIs, "createUIs");
   pragma Import (Java, UisToArray, "uisToArray");

end Javax.Swing.Plaf.Multi.MultiLookAndFeel;
pragma Import (Java, Javax.Swing.Plaf.Multi.MultiLookAndFeel, "javax.swing.plaf.multi.MultiLookAndFeel");
pragma Extensions_Allowed (Off);
