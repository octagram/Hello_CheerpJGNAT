pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Util.Vector;
limited with Javax.Accessibility.Accessible;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JTabbedPane;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Lang.Object;
with Javax.Swing.Plaf.TabbedPaneUI;

package Javax.Swing.Plaf.Multi.MultiTabbedPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.TabbedPaneUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Uis : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Uis, "uis");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MultiTabbedPaneUI (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIs (This : access Typ)
                    return Standard.Java.Lang.Object.Ref;

   function TabForCoordinate (This : access Typ;
                              P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int)
                              return Java.Int;

   function GetTabBounds (This : access Typ;
                          P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Awt.Rectangle.Typ'Class;

   function GetTabRunCount (This : access Typ;
                            P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class)
                            return Java.Int;

   function Contains (This : access Typ;
                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int)
                      return Java.Boolean;

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetAccessibleChildrenCount (This : access Typ;
                                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                        return Java.Int;

   function GetAccessibleChild (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MultiTabbedPaneUI);
   pragma Import (Java, GetUIs, "getUIs");
   pragma Import (Java, TabForCoordinate, "tabForCoordinate");
   pragma Import (Java, GetTabBounds, "getTabBounds");
   pragma Import (Java, GetTabRunCount, "getTabRunCount");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Update, "update");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");

end Javax.Swing.Plaf.Multi.MultiTabbedPaneUI;
pragma Import (Java, Javax.Swing.Plaf.Multi.MultiTabbedPaneUI, "javax.swing.plaf.multi.MultiTabbedPaneUI");
pragma Extensions_Allowed (Off);
