pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Accessibility.Accessible;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Text.EditorKit;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Plaf.TextUI;

package Javax.Swing.Plaf.Multi.MultiTextUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.TextUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Uis : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Uis, "uis");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MultiTextUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIs (This : access Typ)
                    return Standard.Java.Lang.Object.Ref;

   function GetToolTipText (This : access Typ;
                            P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                            P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function ModelToView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int)
                         return access Java.Awt.Rectangle.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ModelToView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Rectangle.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                         return Java.Int;

   function ViewToModel (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Point : access Standard.Java.Awt.Point.Typ'Class;
                         P3_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure DamageRange (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int);

   procedure DamageRange (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                          P5_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class);

   function GetEditorKit (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class)
                          return access Javax.Swing.Text.EditorKit.Typ'Class;

   function GetRootView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class)
                         return access Javax.Swing.Text.View.Typ'Class;

   function Contains (This : access Typ;
                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int)
                      return Java.Boolean;

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetAccessibleChildrenCount (This : access Typ;
                                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                        return Java.Int;

   function GetAccessibleChild (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MultiTextUI);
   pragma Import (Java, GetUIs, "getUIs");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");
   pragma Import (Java, DamageRange, "damageRange");
   pragma Import (Java, GetEditorKit, "getEditorKit");
   pragma Import (Java, GetRootView, "getRootView");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Update, "update");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");

end Javax.Swing.Plaf.Multi.MultiTextUI;
pragma Import (Java, Javax.Swing.Plaf.Multi.MultiTextUI, "javax.swing.plaf.multi.MultiTextUI");
pragma Extensions_Allowed (Off);
