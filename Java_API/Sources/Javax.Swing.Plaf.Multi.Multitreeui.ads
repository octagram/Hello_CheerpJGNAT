pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Util.Vector;
limited with Javax.Accessibility.Accessible;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JTree;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Tree.TreePath;
with Java.Lang.Object;
with Javax.Swing.Plaf.TreeUI;

package Javax.Swing.Plaf.Multi.MultiTreeUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Plaf.TreeUI.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Uis : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Uis, "uis");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MultiTreeUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUIs (This : access Typ)
                    return Standard.Java.Lang.Object.Ref;

   function GetPathBounds (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return access Java.Awt.Rectangle.Typ'Class;

   function GetPathForRow (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_Int : Java.Int)
                           return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetRowForPath (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return Java.Int;

   function GetRowCount (This : access Typ;
                         P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                         return Java.Int;

   function GetClosestPathForLocation (This : access Typ;
                                       P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int)
                                       return access Javax.Swing.Tree.TreePath.Typ'Class;

   function IsEditing (This : access Typ;
                       P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                       return Java.Boolean;

   function StopEditing (This : access Typ;
                         P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                         return Java.Boolean;

   procedure CancelEditing (This : access Typ;
                            P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class);

   procedure StartEditingAtPath (This : access Typ;
                                 P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                 P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   function GetEditingPath (This : access Typ;
                            P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                            return access Javax.Swing.Tree.TreePath.Typ'Class;

   function Contains (This : access Typ;
                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int)
                      return Java.Boolean;

   procedure Update (This : access Typ;
                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                     P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   procedure InstallUI (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UninstallUI (This : access Typ;
                          P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function GetPreferredSize (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return access Java.Awt.Dimension.Typ'Class;

   function GetMinimumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetAccessibleChildrenCount (This : access Typ;
                                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                        return Java.Int;

   function GetAccessibleChild (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MultiTreeUI);
   pragma Import (Java, GetUIs, "getUIs");
   pragma Import (Java, GetPathBounds, "getPathBounds");
   pragma Import (Java, GetPathForRow, "getPathForRow");
   pragma Import (Java, GetRowForPath, "getRowForPath");
   pragma Import (Java, GetRowCount, "getRowCount");
   pragma Import (Java, GetClosestPathForLocation, "getClosestPathForLocation");
   pragma Import (Java, IsEditing, "isEditing");
   pragma Import (Java, StopEditing, "stopEditing");
   pragma Import (Java, CancelEditing, "cancelEditing");
   pragma Import (Java, StartEditingAtPath, "startEditingAtPath");
   pragma Import (Java, GetEditingPath, "getEditingPath");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, Update, "update");
   pragma Import (Java, CreateUI, "createUI");
   pragma Import (Java, InstallUI, "installUI");
   pragma Import (Java, UninstallUI, "uninstallUI");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");

end Javax.Swing.Plaf.Multi.MultiTreeUI;
pragma Import (Java, Javax.Swing.Plaf.Multi.MultiTreeUI, "javax.swing.plaf.multi.MultiTreeUI");
pragma Extensions_Allowed (Off);
