pragma Extensions_Allowed (On);
package Javax.Swing.Plaf.Multi is
   pragma Preelaborate;
end Javax.Swing.Plaf.Multi;
pragma Import (Java, Javax.Swing.Plaf.Multi, "javax.swing.plaf.multi");
pragma Extensions_Allowed (Off);
