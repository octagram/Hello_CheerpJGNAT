pragma Extensions_Allowed (On);
limited with Javax.Swing.JOptionPane;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.OptionPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_OptionPaneUI (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SelectInitialValue (This : access Typ;
                                 P1_JOptionPane : access Standard.Javax.Swing.JOptionPane.Typ'Class) is abstract;

   function ContainsCustomComponents (This : access Typ;
                                      P1_JOptionPane : access Standard.Javax.Swing.JOptionPane.Typ'Class)
                                      return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OptionPaneUI);
   pragma Export (Java, SelectInitialValue, "selectInitialValue");
   pragma Export (Java, ContainsCustomComponents, "containsCustomComponents");

end Javax.Swing.Plaf.OptionPaneUI;
pragma Import (Java, Javax.Swing.Plaf.OptionPaneUI, "javax.swing.plaf.OptionPaneUI");
pragma Extensions_Allowed (Off);
