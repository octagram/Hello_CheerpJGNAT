pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Javax.Swing.JPopupMenu;
limited with Javax.Swing.Popup;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.PopupMenuUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_PopupMenuUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsPopupTrigger (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return Java.Boolean;

   function GetPopup (This : access Typ;
                      P1_JPopupMenu : access Standard.Javax.Swing.JPopupMenu.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int)
                      return access Javax.Swing.Popup.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PopupMenuUI);
   pragma Import (Java, IsPopupTrigger, "isPopupTrigger");
   pragma Import (Java, GetPopup, "getPopup");

end Javax.Swing.Plaf.PopupMenuUI;
pragma Import (Java, Javax.Swing.Plaf.PopupMenuUI, "javax.swing.plaf.PopupMenuUI");
pragma Extensions_Allowed (Off);
