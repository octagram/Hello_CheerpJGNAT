pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Javax.Swing.JSplitPane;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.SplitPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_SplitPaneUI (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ResetToPreferredSizes (This : access Typ;
                                    P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class) is abstract;

   procedure SetDividerLocation (This : access Typ;
                                 P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class;
                                 P2_Int : Java.Int) is abstract;

   function GetDividerLocation (This : access Typ;
                                P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class)
                                return Java.Int is abstract;

   function GetMinimumDividerLocation (This : access Typ;
                                       P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class)
                                       return Java.Int is abstract;

   function GetMaximumDividerLocation (This : access Typ;
                                       P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class)
                                       return Java.Int is abstract;

   procedure FinishedPaintingChildren (This : access Typ;
                                       P1_JSplitPane : access Standard.Javax.Swing.JSplitPane.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SplitPaneUI);
   pragma Export (Java, ResetToPreferredSizes, "resetToPreferredSizes");
   pragma Export (Java, SetDividerLocation, "setDividerLocation");
   pragma Export (Java, GetDividerLocation, "getDividerLocation");
   pragma Export (Java, GetMinimumDividerLocation, "getMinimumDividerLocation");
   pragma Export (Java, GetMaximumDividerLocation, "getMaximumDividerLocation");
   pragma Export (Java, FinishedPaintingChildren, "finishedPaintingChildren");

end Javax.Swing.Plaf.SplitPaneUI;
pragma Import (Java, Javax.Swing.Plaf.SplitPaneUI, "javax.swing.plaf.SplitPaneUI");
pragma Extensions_Allowed (Off);
