pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.ColorType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ColorType (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetID (This : access Typ)
                   return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FOREGROUND : access Javax.Swing.Plaf.Synth.ColorType.Typ'Class;

   --  final
   BACKGROUND : access Javax.Swing.Plaf.Synth.ColorType.Typ'Class;

   --  final
   TEXT_FOREGROUND : access Javax.Swing.Plaf.Synth.ColorType.Typ'Class;

   --  final
   TEXT_BACKGROUND : access Javax.Swing.Plaf.Synth.ColorType.Typ'Class;

   --  final
   FOCUS : access Javax.Swing.Plaf.Synth.ColorType.Typ'Class;

   --  final
   MAX_COUNT : Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ColorType);
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, FOREGROUND, "FOREGROUND");
   pragma Import (Java, BACKGROUND, "BACKGROUND");
   pragma Import (Java, TEXT_FOREGROUND, "TEXT_FOREGROUND");
   pragma Import (Java, TEXT_BACKGROUND, "TEXT_BACKGROUND");
   pragma Import (Java, FOCUS, "FOCUS");
   pragma Import (Java, MAX_COUNT, "MAX_COUNT");

end Javax.Swing.Plaf.Synth.ColorType;
pragma Import (Java, Javax.Swing.Plaf.Synth.ColorType, "javax.swing.plaf.synth.ColorType");
pragma Extensions_Allowed (Off);
