pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.Region is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Region (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_Boolean : Java.Boolean; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSubregion (This : access Typ)
                         return Java.Boolean;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ARROW_BUTTON : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   BUTTON : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   CHECK_BOX : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   CHECK_BOX_MENU_ITEM : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   COLOR_CHOOSER : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   COMBO_BOX : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   DESKTOP_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   DESKTOP_ICON : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   EDITOR_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   FILE_CHOOSER : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   FORMATTED_TEXT_FIELD : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   INTERNAL_FRAME : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   INTERNAL_FRAME_TITLE_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   LABEL : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   LIST : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   MENU : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   MENU_BAR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   MENU_ITEM : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   MENU_ITEM_ACCELERATOR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   OPTION_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   PANEL : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   PASSWORD_FIELD : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   POPUP_MENU : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   POPUP_MENU_SEPARATOR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   PROGRESS_BAR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   RADIO_BUTTON : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   RADIO_BUTTON_MENU_ITEM : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   ROOT_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SCROLL_BAR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SCROLL_BAR_TRACK : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SCROLL_BAR_THUMB : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SCROLL_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SEPARATOR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SLIDER : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SLIDER_TRACK : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SLIDER_THUMB : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SPINNER : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SPLIT_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   SPLIT_PANE_DIVIDER : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TABBED_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TABBED_PANE_TAB : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TABBED_PANE_TAB_AREA : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TABBED_PANE_CONTENT : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TABLE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TABLE_HEADER : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TEXT_AREA : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TEXT_FIELD : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TEXT_PANE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TOGGLE_BUTTON : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TOOL_BAR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TOOL_BAR_CONTENT : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TOOL_BAR_DRAG_WINDOW : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TOOL_TIP : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TOOL_BAR_SEPARATOR : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TREE : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   TREE_CELL : access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   --  final
   VIEWPORT : access Javax.Swing.Plaf.Synth.Region.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Region);
   pragma Import (Java, IsSubregion, "isSubregion");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ARROW_BUTTON, "ARROW_BUTTON");
   pragma Import (Java, BUTTON, "BUTTON");
   pragma Import (Java, CHECK_BOX, "CHECK_BOX");
   pragma Import (Java, CHECK_BOX_MENU_ITEM, "CHECK_BOX_MENU_ITEM");
   pragma Import (Java, COLOR_CHOOSER, "COLOR_CHOOSER");
   pragma Import (Java, COMBO_BOX, "COMBO_BOX");
   pragma Import (Java, DESKTOP_PANE, "DESKTOP_PANE");
   pragma Import (Java, DESKTOP_ICON, "DESKTOP_ICON");
   pragma Import (Java, EDITOR_PANE, "EDITOR_PANE");
   pragma Import (Java, FILE_CHOOSER, "FILE_CHOOSER");
   pragma Import (Java, FORMATTED_TEXT_FIELD, "FORMATTED_TEXT_FIELD");
   pragma Import (Java, INTERNAL_FRAME, "INTERNAL_FRAME");
   pragma Import (Java, INTERNAL_FRAME_TITLE_PANE, "INTERNAL_FRAME_TITLE_PANE");
   pragma Import (Java, LABEL, "LABEL");
   pragma Import (Java, LIST, "LIST");
   pragma Import (Java, MENU, "MENU");
   pragma Import (Java, MENU_BAR, "MENU_BAR");
   pragma Import (Java, MENU_ITEM, "MENU_ITEM");
   pragma Import (Java, MENU_ITEM_ACCELERATOR, "MENU_ITEM_ACCELERATOR");
   pragma Import (Java, OPTION_PANE, "OPTION_PANE");
   pragma Import (Java, PANEL, "PANEL");
   pragma Import (Java, PASSWORD_FIELD, "PASSWORD_FIELD");
   pragma Import (Java, POPUP_MENU, "POPUP_MENU");
   pragma Import (Java, POPUP_MENU_SEPARATOR, "POPUP_MENU_SEPARATOR");
   pragma Import (Java, PROGRESS_BAR, "PROGRESS_BAR");
   pragma Import (Java, RADIO_BUTTON, "RADIO_BUTTON");
   pragma Import (Java, RADIO_BUTTON_MENU_ITEM, "RADIO_BUTTON_MENU_ITEM");
   pragma Import (Java, ROOT_PANE, "ROOT_PANE");
   pragma Import (Java, SCROLL_BAR, "SCROLL_BAR");
   pragma Import (Java, SCROLL_BAR_TRACK, "SCROLL_BAR_TRACK");
   pragma Import (Java, SCROLL_BAR_THUMB, "SCROLL_BAR_THUMB");
   pragma Import (Java, SCROLL_PANE, "SCROLL_PANE");
   pragma Import (Java, SEPARATOR, "SEPARATOR");
   pragma Import (Java, SLIDER, "SLIDER");
   pragma Import (Java, SLIDER_TRACK, "SLIDER_TRACK");
   pragma Import (Java, SLIDER_THUMB, "SLIDER_THUMB");
   pragma Import (Java, SPINNER, "SPINNER");
   pragma Import (Java, SPLIT_PANE, "SPLIT_PANE");
   pragma Import (Java, SPLIT_PANE_DIVIDER, "SPLIT_PANE_DIVIDER");
   pragma Import (Java, TABBED_PANE, "TABBED_PANE");
   pragma Import (Java, TABBED_PANE_TAB, "TABBED_PANE_TAB");
   pragma Import (Java, TABBED_PANE_TAB_AREA, "TABBED_PANE_TAB_AREA");
   pragma Import (Java, TABBED_PANE_CONTENT, "TABBED_PANE_CONTENT");
   pragma Import (Java, TABLE, "TABLE");
   pragma Import (Java, TABLE_HEADER, "TABLE_HEADER");
   pragma Import (Java, TEXT_AREA, "TEXT_AREA");
   pragma Import (Java, TEXT_FIELD, "TEXT_FIELD");
   pragma Import (Java, TEXT_PANE, "TEXT_PANE");
   pragma Import (Java, TOGGLE_BUTTON, "TOGGLE_BUTTON");
   pragma Import (Java, TOOL_BAR, "TOOL_BAR");
   pragma Import (Java, TOOL_BAR_CONTENT, "TOOL_BAR_CONTENT");
   pragma Import (Java, TOOL_BAR_DRAG_WINDOW, "TOOL_BAR_DRAG_WINDOW");
   pragma Import (Java, TOOL_TIP, "TOOL_TIP");
   pragma Import (Java, TOOL_BAR_SEPARATOR, "TOOL_BAR_SEPARATOR");
   pragma Import (Java, TREE, "TREE");
   pragma Import (Java, TREE_CELL, "TREE_CELL");
   pragma Import (Java, VIEWPORT, "VIEWPORT");

end Javax.Swing.Plaf.Synth.Region;
pragma Import (Java, Javax.Swing.Plaf.Synth.Region, "javax.swing.plaf.synth.Region");
pragma Extensions_Allowed (Off);
