pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.SynthConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ENABLED : constant Java.Int;

   --  final
   MOUSE_OVER : constant Java.Int;

   --  final
   PRESSED : constant Java.Int;

   --  final
   DISABLED : constant Java.Int;

   --  final
   FOCUSED : constant Java.Int;

   --  final
   SELECTED : constant Java.Int;

   --  final
   DEFAULT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ENABLED, "ENABLED");
   pragma Import (Java, MOUSE_OVER, "MOUSE_OVER");
   pragma Import (Java, PRESSED, "PRESSED");
   pragma Import (Java, DISABLED, "DISABLED");
   pragma Import (Java, FOCUSED, "FOCUSED");
   pragma Import (Java, SELECTED, "SELECTED");
   pragma Import (Java, DEFAULT, "DEFAULT");

end Javax.Swing.Plaf.Synth.SynthConstants;
pragma Import (Java, Javax.Swing.Plaf.Synth.SynthConstants, "javax.swing.plaf.synth.SynthConstants");
pragma Extensions_Allowed (Off);
