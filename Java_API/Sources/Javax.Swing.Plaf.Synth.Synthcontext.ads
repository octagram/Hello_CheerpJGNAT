pragma Extensions_Allowed (On);
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.Synth.Region;
limited with Javax.Swing.Plaf.Synth.SynthStyle;
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.SynthContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SynthContext (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                              P2_Region : access Standard.Javax.Swing.Plaf.Synth.Region.Typ'Class;
                              P3_SynthStyle : access Standard.Javax.Swing.Plaf.Synth.SynthStyle.Typ'Class;
                              P4_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetComponent (This : access Typ)
                          return access Javax.Swing.JComponent.Typ'Class;

   function GetRegion (This : access Typ)
                       return access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   function GetStyle (This : access Typ)
                      return access Javax.Swing.Plaf.Synth.SynthStyle.Typ'Class;

   function GetComponentState (This : access Typ)
                               return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SynthContext);
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, GetRegion, "getRegion");
   pragma Import (Java, GetStyle, "getStyle");
   pragma Import (Java, GetComponentState, "getComponentState");

end Javax.Swing.Plaf.Synth.SynthContext;
pragma Import (Java, Javax.Swing.Plaf.Synth.SynthContext, "javax.swing.plaf.synth.SynthContext");
pragma Extensions_Allowed (Off);
