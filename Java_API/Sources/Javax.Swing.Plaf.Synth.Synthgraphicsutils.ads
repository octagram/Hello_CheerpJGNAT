pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Plaf.Synth.SynthContext;
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.SynthGraphicsUtils is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SynthGraphicsUtils (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure DrawLine (This : access Typ;
                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P3_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int);

   procedure DrawLine (This : access Typ;
                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P3_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int;
                       P6_Int : Java.Int;
                       P7_Int : Java.Int;
                       P8_Object : access Standard.Java.Lang.Object.Typ'Class);

   function LayoutText (This : access Typ;
                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                        P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                        P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                        P5_Int : Java.Int;
                        P6_Int : Java.Int;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int;
                        P9_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P10_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P11_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P12_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class;

   function ComputeStringWidth (This : access Typ;
                                P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                P2_Font : access Standard.Java.Awt.Font.Typ'Class;
                                P3_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                                P4_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int;

   function GetMinimumSize (This : access Typ;
                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                            P2_Font : access Standard.Java.Awt.Font.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int;
                            P7_Int : Java.Int;
                            P8_Int : Java.Int;
                            P9_Int : Java.Int;
                            P10_Int : Java.Int)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumSize (This : access Typ;
                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                            P2_Font : access Standard.Java.Awt.Font.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                            P5_Int : Java.Int;
                            P6_Int : Java.Int;
                            P7_Int : Java.Int;
                            P8_Int : Java.Int;
                            P9_Int : Java.Int;
                            P10_Int : Java.Int)
                            return access Java.Awt.Dimension.Typ'Class;

   function GetMaximumCharHeight (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class)
                                  return Java.Int;

   function GetPreferredSize (This : access Typ;
                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                              P2_Font : access Standard.Java.Awt.Font.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                              P5_Int : Java.Int;
                              P6_Int : Java.Int;
                              P7_Int : Java.Int;
                              P8_Int : Java.Int;
                              P9_Int : Java.Int;
                              P10_Int : Java.Int)
                              return access Java.Awt.Dimension.Typ'Class;

   procedure PaintText (This : access Typ;
                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                        P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                        P5_Int : Java.Int);

   procedure PaintText (This : access Typ;
                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                        P4_Int : Java.Int;
                        P5_Int : Java.Int;
                        P6_Int : Java.Int);

   procedure PaintText (This : access Typ;
                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class;
                        P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                        P5_Int : Java.Int;
                        P6_Int : Java.Int;
                        P7_Int : Java.Int;
                        P8_Int : Java.Int;
                        P9_Int : Java.Int;
                        P10_Int : Java.Int;
                        P11_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SynthGraphicsUtils);
   pragma Import (Java, DrawLine, "drawLine");
   pragma Import (Java, LayoutText, "layoutText");
   pragma Import (Java, ComputeStringWidth, "computeStringWidth");
   pragma Import (Java, GetMinimumSize, "getMinimumSize");
   pragma Import (Java, GetMaximumSize, "getMaximumSize");
   pragma Import (Java, GetMaximumCharHeight, "getMaximumCharHeight");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, PaintText, "paintText");

end Javax.Swing.Plaf.Synth.SynthGraphicsUtils;
pragma Import (Java, Javax.Swing.Plaf.Synth.SynthGraphicsUtils, "javax.swing.plaf.synth.SynthGraphicsUtils");
pragma Extensions_Allowed (Off);
