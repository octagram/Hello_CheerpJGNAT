pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Io.InputStream;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.Plaf.Synth.Region;
limited with Javax.Swing.Plaf.Synth.SynthStyle;
limited with Javax.Swing.Plaf.Synth.SynthStyleFactory;
limited with Javax.Swing.UIDefaults;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicLookAndFeel;

package Javax.Swing.Plaf.Synth.SynthLookAndFeel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.Plaf.Basic.BasicLookAndFeel.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetStyleFactory (P1_SynthStyleFactory : access Standard.Javax.Swing.Plaf.Synth.SynthStyleFactory.Typ'Class);

   function GetStyleFactory return access Javax.Swing.Plaf.Synth.SynthStyleFactory.Typ'Class;

   function GetStyle (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                      P2_Region : access Standard.Javax.Swing.Plaf.Synth.Region.Typ'Class)
                      return access Javax.Swing.Plaf.Synth.SynthStyle.Typ'Class;

   procedure UpdateStyles (P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetRegion (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                       return access Javax.Swing.Plaf.Synth.Region.Typ'Class;

   function CreateUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                      return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SynthLookAndFeel (This : Ref := null)
                                  return Ref;

   procedure Load (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_Class : access Standard.Java.Lang.Class.Typ'Class);
   --  can raise Java.Text.ParseException.Except

   procedure Load (This : access Typ;
                   P1_URL : access Standard.Java.Net.URL.Typ'Class);
   --  can raise Java.Text.ParseException.Except and
   --  Java.Io.IOException.Except

   procedure Initialize (This : access Typ);

   procedure Uninitialize (This : access Typ);

   function GetDefaults (This : access Typ)
                         return access Javax.Swing.UIDefaults.Typ'Class;

   function IsSupportedLookAndFeel (This : access Typ)
                                    return Java.Boolean;

   function IsNativeLookAndFeel (This : access Typ)
                                 return Java.Boolean;

   function GetDescription (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function ShouldUpdateStyleOnAncestorChanged (This : access Typ)
                                                return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetStyleFactory, "setStyleFactory");
   pragma Import (Java, GetStyleFactory, "getStyleFactory");
   pragma Import (Java, GetStyle, "getStyle");
   pragma Import (Java, UpdateStyles, "updateStyles");
   pragma Import (Java, GetRegion, "getRegion");
   pragma Import (Java, CreateUI, "createUI");
   pragma Java_Constructor (New_SynthLookAndFeel);
   pragma Import (Java, Load, "load");
   pragma Import (Java, Initialize, "initialize");
   pragma Import (Java, Uninitialize, "uninitialize");
   pragma Import (Java, GetDefaults, "getDefaults");
   pragma Import (Java, IsSupportedLookAndFeel, "isSupportedLookAndFeel");
   pragma Import (Java, IsNativeLookAndFeel, "isNativeLookAndFeel");
   pragma Import (Java, GetDescription, "getDescription");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, ShouldUpdateStyleOnAncestorChanged, "shouldUpdateStyleOnAncestorChanged");

end Javax.Swing.Plaf.Synth.SynthLookAndFeel;
pragma Import (Java, Javax.Swing.Plaf.Synth.SynthLookAndFeel, "javax.swing.plaf.synth.SynthLookAndFeel");
pragma Extensions_Allowed (Off);
