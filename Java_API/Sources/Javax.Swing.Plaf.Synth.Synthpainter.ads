pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Javax.Swing.Plaf.Synth.SynthContext;
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.SynthPainter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SynthPainter (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure PaintArrowButtonBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintArrowButtonBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintArrowButtonForeground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int;
                                         P7_Int : Java.Int);

   procedure PaintButtonBackground (This : access Typ;
                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int);

   procedure PaintButtonBorder (This : access Typ;
                                P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int);

   procedure PaintCheckBoxMenuItemBackground (This : access Typ;
                                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                              P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                              P3_Int : Java.Int;
                                              P4_Int : Java.Int;
                                              P5_Int : Java.Int;
                                              P6_Int : Java.Int);

   procedure PaintCheckBoxMenuItemBorder (This : access Typ;
                                          P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P3_Int : Java.Int;
                                          P4_Int : Java.Int;
                                          P5_Int : Java.Int;
                                          P6_Int : Java.Int);

   procedure PaintCheckBoxBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintCheckBoxBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintColorChooserBackground (This : access Typ;
                                          P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P3_Int : Java.Int;
                                          P4_Int : Java.Int;
                                          P5_Int : Java.Int;
                                          P6_Int : Java.Int);

   procedure PaintColorChooserBorder (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintComboBoxBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintComboBoxBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintDesktopIconBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintDesktopIconBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintDesktopPaneBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintDesktopPaneBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintEditorPaneBackground (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int);

   procedure PaintEditorPaneBorder (This : access Typ;
                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int);

   procedure PaintFileChooserBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintFileChooserBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintFormattedTextFieldBackground (This : access Typ;
                                                P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                                P3_Int : Java.Int;
                                                P4_Int : Java.Int;
                                                P5_Int : Java.Int;
                                                P6_Int : Java.Int);

   procedure PaintFormattedTextFieldBorder (This : access Typ;
                                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                            P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                            P3_Int : Java.Int;
                                            P4_Int : Java.Int;
                                            P5_Int : Java.Int;
                                            P6_Int : Java.Int);

   procedure PaintInternalFrameTitlePaneBackground (This : access Typ;
                                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                                    P3_Int : Java.Int;
                                                    P4_Int : Java.Int;
                                                    P5_Int : Java.Int;
                                                    P6_Int : Java.Int);

   procedure PaintInternalFrameTitlePaneBorder (This : access Typ;
                                                P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                                P3_Int : Java.Int;
                                                P4_Int : Java.Int;
                                                P5_Int : Java.Int;
                                                P6_Int : Java.Int);

   procedure PaintInternalFrameBackground (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int);

   procedure PaintInternalFrameBorder (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int);

   procedure PaintLabelBackground (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintLabelBorder (This : access Typ;
                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int;
                               P6_Int : Java.Int);

   procedure PaintListBackground (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintListBorder (This : access Typ;
                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                              P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Int : Java.Int);

   procedure PaintMenuBarBackground (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintMenuBarBorder (This : access Typ;
                                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int);

   procedure PaintMenuItemBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintMenuItemBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintMenuBackground (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintMenuBorder (This : access Typ;
                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                              P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Int : Java.Int);

   procedure PaintOptionPaneBackground (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int);

   procedure PaintOptionPaneBorder (This : access Typ;
                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int);

   procedure PaintPanelBackground (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintPanelBorder (This : access Typ;
                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int;
                               P6_Int : Java.Int);

   procedure PaintPasswordFieldBackground (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int);

   procedure PaintPasswordFieldBorder (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int);

   procedure PaintPopupMenuBackground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int);

   procedure PaintPopupMenuBorder (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintProgressBarBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintProgressBarBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int;
                                         P7_Int : Java.Int);

   procedure PaintProgressBarBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintProgressBarBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int;
                                     P7_Int : Java.Int);

   procedure PaintProgressBarForeground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int;
                                         P7_Int : Java.Int);

   procedure PaintRadioButtonMenuItemBackground (This : access Typ;
                                                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                                 P3_Int : Java.Int;
                                                 P4_Int : Java.Int;
                                                 P5_Int : Java.Int;
                                                 P6_Int : Java.Int);

   procedure PaintRadioButtonMenuItemBorder (This : access Typ;
                                             P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                             P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                             P3_Int : Java.Int;
                                             P4_Int : Java.Int;
                                             P5_Int : Java.Int;
                                             P6_Int : Java.Int);

   procedure PaintRadioButtonBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintRadioButtonBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintRootPaneBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintRootPaneBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintScrollBarBackground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int);

   procedure PaintScrollBarBackground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int;
                                       P7_Int : Java.Int);

   procedure PaintScrollBarBorder (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintScrollBarBorder (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int;
                                   P7_Int : Java.Int);

   procedure PaintScrollBarThumbBackground (This : access Typ;
                                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                            P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                            P3_Int : Java.Int;
                                            P4_Int : Java.Int;
                                            P5_Int : Java.Int;
                                            P6_Int : Java.Int;
                                            P7_Int : Java.Int);

   procedure PaintScrollBarThumbBorder (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int;
                                        P7_Int : Java.Int);

   procedure PaintScrollBarTrackBackground (This : access Typ;
                                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                            P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                            P3_Int : Java.Int;
                                            P4_Int : Java.Int;
                                            P5_Int : Java.Int;
                                            P6_Int : Java.Int);

   procedure PaintScrollBarTrackBackground (This : access Typ;
                                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                            P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                            P3_Int : Java.Int;
                                            P4_Int : Java.Int;
                                            P5_Int : Java.Int;
                                            P6_Int : Java.Int;
                                            P7_Int : Java.Int);

   procedure PaintScrollBarTrackBorder (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int);

   procedure PaintScrollBarTrackBorder (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int;
                                        P7_Int : Java.Int);

   procedure PaintScrollPaneBackground (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int);

   procedure PaintScrollPaneBorder (This : access Typ;
                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int);

   procedure PaintSeparatorBackground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int);

   procedure PaintSeparatorBackground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int;
                                       P7_Int : Java.Int);

   procedure PaintSeparatorBorder (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintSeparatorBorder (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int;
                                   P7_Int : Java.Int);

   procedure PaintSeparatorForeground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int;
                                       P7_Int : Java.Int);

   procedure PaintSliderBackground (This : access Typ;
                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int);

   procedure PaintSliderBackground (This : access Typ;
                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int;
                                    P7_Int : Java.Int);

   procedure PaintSliderBorder (This : access Typ;
                                P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int);

   procedure PaintSliderBorder (This : access Typ;
                                P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int;
                                P6_Int : Java.Int;
                                P7_Int : Java.Int);

   procedure PaintSliderThumbBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int;
                                         P7_Int : Java.Int);

   procedure PaintSliderThumbBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int;
                                     P7_Int : Java.Int);

   procedure PaintSliderTrackBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintSliderTrackBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int;
                                         P7_Int : Java.Int);

   procedure PaintSliderTrackBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintSliderTrackBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int;
                                     P7_Int : Java.Int);

   procedure PaintSpinnerBackground (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintSpinnerBorder (This : access Typ;
                                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int);

   procedure PaintSplitPaneDividerBackground (This : access Typ;
                                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                              P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                              P3_Int : Java.Int;
                                              P4_Int : Java.Int;
                                              P5_Int : Java.Int;
                                              P6_Int : Java.Int);

   procedure PaintSplitPaneDividerBackground (This : access Typ;
                                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                              P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                              P3_Int : Java.Int;
                                              P4_Int : Java.Int;
                                              P5_Int : Java.Int;
                                              P6_Int : Java.Int;
                                              P7_Int : Java.Int);

   procedure PaintSplitPaneDividerForeground (This : access Typ;
                                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                              P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                              P3_Int : Java.Int;
                                              P4_Int : Java.Int;
                                              P5_Int : Java.Int;
                                              P6_Int : Java.Int;
                                              P7_Int : Java.Int);

   procedure PaintSplitPaneDragDivider (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int;
                                        P7_Int : Java.Int);

   procedure PaintSplitPaneBackground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int);

   procedure PaintSplitPaneBorder (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintTabbedPaneBackground (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int);

   procedure PaintTabbedPaneBorder (This : access Typ;
                                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int;
                                    P5_Int : Java.Int;
                                    P6_Int : Java.Int);

   procedure PaintTabbedPaneTabAreaBackground (This : access Typ;
                                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                               P3_Int : Java.Int;
                                               P4_Int : Java.Int;
                                               P5_Int : Java.Int;
                                               P6_Int : Java.Int);

   procedure PaintTabbedPaneTabAreaBackground (This : access Typ;
                                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                               P3_Int : Java.Int;
                                               P4_Int : Java.Int;
                                               P5_Int : Java.Int;
                                               P6_Int : Java.Int;
                                               P7_Int : Java.Int);

   procedure PaintTabbedPaneTabAreaBorder (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int);

   procedure PaintTabbedPaneTabAreaBorder (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int;
                                           P7_Int : Java.Int);

   procedure PaintTabbedPaneTabBackground (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int;
                                           P7_Int : Java.Int);

   procedure PaintTabbedPaneTabBackground (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int;
                                           P7_Int : Java.Int;
                                           P8_Int : Java.Int);

   procedure PaintTabbedPaneTabBorder (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int;
                                       P7_Int : Java.Int);

   procedure PaintTabbedPaneTabBorder (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int;
                                       P7_Int : Java.Int;
                                       P8_Int : Java.Int);

   procedure PaintTabbedPaneContentBackground (This : access Typ;
                                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                               P3_Int : Java.Int;
                                               P4_Int : Java.Int;
                                               P5_Int : Java.Int;
                                               P6_Int : Java.Int);

   procedure PaintTabbedPaneContentBorder (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int);

   procedure PaintTableHeaderBackground (This : access Typ;
                                         P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                         P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int;
                                         P6_Int : Java.Int);

   procedure PaintTableHeaderBorder (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintTableBackground (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintTableBorder (This : access Typ;
                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int;
                               P5_Int : Java.Int;
                               P6_Int : Java.Int);

   procedure PaintTextAreaBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintTextAreaBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintTextPaneBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintTextPaneBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintTextFieldBackground (This : access Typ;
                                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                       P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                       P3_Int : Java.Int;
                                       P4_Int : Java.Int;
                                       P5_Int : Java.Int;
                                       P6_Int : Java.Int);

   procedure PaintTextFieldBorder (This : access Typ;
                                   P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                   P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Int : Java.Int);

   procedure PaintToggleButtonBackground (This : access Typ;
                                          P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                          P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                          P3_Int : Java.Int;
                                          P4_Int : Java.Int;
                                          P5_Int : Java.Int;
                                          P6_Int : Java.Int);

   procedure PaintToggleButtonBorder (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintToolBarBackground (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintToolBarBackground (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int;
                                     P7_Int : Java.Int);

   procedure PaintToolBarBorder (This : access Typ;
                                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int);

   procedure PaintToolBarBorder (This : access Typ;
                                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int);

   procedure PaintToolBarContentBackground (This : access Typ;
                                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                            P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                            P3_Int : Java.Int;
                                            P4_Int : Java.Int;
                                            P5_Int : Java.Int;
                                            P6_Int : Java.Int);

   procedure PaintToolBarContentBackground (This : access Typ;
                                            P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                            P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                            P3_Int : Java.Int;
                                            P4_Int : Java.Int;
                                            P5_Int : Java.Int;
                                            P6_Int : Java.Int;
                                            P7_Int : Java.Int);

   procedure PaintToolBarContentBorder (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int);

   procedure PaintToolBarContentBorder (This : access Typ;
                                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                        P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                        P3_Int : Java.Int;
                                        P4_Int : Java.Int;
                                        P5_Int : Java.Int;
                                        P6_Int : Java.Int;
                                        P7_Int : Java.Int);

   procedure PaintToolBarDragWindowBackground (This : access Typ;
                                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                               P3_Int : Java.Int;
                                               P4_Int : Java.Int;
                                               P5_Int : Java.Int;
                                               P6_Int : Java.Int);

   procedure PaintToolBarDragWindowBackground (This : access Typ;
                                               P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                               P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                               P3_Int : Java.Int;
                                               P4_Int : Java.Int;
                                               P5_Int : Java.Int;
                                               P6_Int : Java.Int;
                                               P7_Int : Java.Int);

   procedure PaintToolBarDragWindowBorder (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int);

   procedure PaintToolBarDragWindowBorder (This : access Typ;
                                           P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                           P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                           P3_Int : Java.Int;
                                           P4_Int : Java.Int;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int;
                                           P7_Int : Java.Int);

   procedure PaintToolTipBackground (This : access Typ;
                                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                     P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int);

   procedure PaintToolTipBorder (This : access Typ;
                                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int);

   procedure PaintTreeBackground (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintTreeBorder (This : access Typ;
                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                              P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int;
                              P6_Int : Java.Int);

   procedure PaintTreeCellBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintTreeCellBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);

   procedure PaintTreeCellFocus (This : access Typ;
                                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                 P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int);

   procedure PaintViewportBackground (This : access Typ;
                                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                      P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                      P3_Int : Java.Int;
                                      P4_Int : Java.Int;
                                      P5_Int : Java.Int;
                                      P6_Int : Java.Int);

   procedure PaintViewportBorder (This : access Typ;
                                  P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                                  P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                  P3_Int : Java.Int;
                                  P4_Int : Java.Int;
                                  P5_Int : Java.Int;
                                  P6_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SynthPainter);
   pragma Import (Java, PaintArrowButtonBackground, "paintArrowButtonBackground");
   pragma Import (Java, PaintArrowButtonBorder, "paintArrowButtonBorder");
   pragma Import (Java, PaintArrowButtonForeground, "paintArrowButtonForeground");
   pragma Import (Java, PaintButtonBackground, "paintButtonBackground");
   pragma Import (Java, PaintButtonBorder, "paintButtonBorder");
   pragma Import (Java, PaintCheckBoxMenuItemBackground, "paintCheckBoxMenuItemBackground");
   pragma Import (Java, PaintCheckBoxMenuItemBorder, "paintCheckBoxMenuItemBorder");
   pragma Import (Java, PaintCheckBoxBackground, "paintCheckBoxBackground");
   pragma Import (Java, PaintCheckBoxBorder, "paintCheckBoxBorder");
   pragma Import (Java, PaintColorChooserBackground, "paintColorChooserBackground");
   pragma Import (Java, PaintColorChooserBorder, "paintColorChooserBorder");
   pragma Import (Java, PaintComboBoxBackground, "paintComboBoxBackground");
   pragma Import (Java, PaintComboBoxBorder, "paintComboBoxBorder");
   pragma Import (Java, PaintDesktopIconBackground, "paintDesktopIconBackground");
   pragma Import (Java, PaintDesktopIconBorder, "paintDesktopIconBorder");
   pragma Import (Java, PaintDesktopPaneBackground, "paintDesktopPaneBackground");
   pragma Import (Java, PaintDesktopPaneBorder, "paintDesktopPaneBorder");
   pragma Import (Java, PaintEditorPaneBackground, "paintEditorPaneBackground");
   pragma Import (Java, PaintEditorPaneBorder, "paintEditorPaneBorder");
   pragma Import (Java, PaintFileChooserBackground, "paintFileChooserBackground");
   pragma Import (Java, PaintFileChooserBorder, "paintFileChooserBorder");
   pragma Import (Java, PaintFormattedTextFieldBackground, "paintFormattedTextFieldBackground");
   pragma Import (Java, PaintFormattedTextFieldBorder, "paintFormattedTextFieldBorder");
   pragma Import (Java, PaintInternalFrameTitlePaneBackground, "paintInternalFrameTitlePaneBackground");
   pragma Import (Java, PaintInternalFrameTitlePaneBorder, "paintInternalFrameTitlePaneBorder");
   pragma Import (Java, PaintInternalFrameBackground, "paintInternalFrameBackground");
   pragma Import (Java, PaintInternalFrameBorder, "paintInternalFrameBorder");
   pragma Import (Java, PaintLabelBackground, "paintLabelBackground");
   pragma Import (Java, PaintLabelBorder, "paintLabelBorder");
   pragma Import (Java, PaintListBackground, "paintListBackground");
   pragma Import (Java, PaintListBorder, "paintListBorder");
   pragma Import (Java, PaintMenuBarBackground, "paintMenuBarBackground");
   pragma Import (Java, PaintMenuBarBorder, "paintMenuBarBorder");
   pragma Import (Java, PaintMenuItemBackground, "paintMenuItemBackground");
   pragma Import (Java, PaintMenuItemBorder, "paintMenuItemBorder");
   pragma Import (Java, PaintMenuBackground, "paintMenuBackground");
   pragma Import (Java, PaintMenuBorder, "paintMenuBorder");
   pragma Import (Java, PaintOptionPaneBackground, "paintOptionPaneBackground");
   pragma Import (Java, PaintOptionPaneBorder, "paintOptionPaneBorder");
   pragma Import (Java, PaintPanelBackground, "paintPanelBackground");
   pragma Import (Java, PaintPanelBorder, "paintPanelBorder");
   pragma Import (Java, PaintPasswordFieldBackground, "paintPasswordFieldBackground");
   pragma Import (Java, PaintPasswordFieldBorder, "paintPasswordFieldBorder");
   pragma Import (Java, PaintPopupMenuBackground, "paintPopupMenuBackground");
   pragma Import (Java, PaintPopupMenuBorder, "paintPopupMenuBorder");
   pragma Import (Java, PaintProgressBarBackground, "paintProgressBarBackground");
   pragma Import (Java, PaintProgressBarBorder, "paintProgressBarBorder");
   pragma Import (Java, PaintProgressBarForeground, "paintProgressBarForeground");
   pragma Import (Java, PaintRadioButtonMenuItemBackground, "paintRadioButtonMenuItemBackground");
   pragma Import (Java, PaintRadioButtonMenuItemBorder, "paintRadioButtonMenuItemBorder");
   pragma Import (Java, PaintRadioButtonBackground, "paintRadioButtonBackground");
   pragma Import (Java, PaintRadioButtonBorder, "paintRadioButtonBorder");
   pragma Import (Java, PaintRootPaneBackground, "paintRootPaneBackground");
   pragma Import (Java, PaintRootPaneBorder, "paintRootPaneBorder");
   pragma Import (Java, PaintScrollBarBackground, "paintScrollBarBackground");
   pragma Import (Java, PaintScrollBarBorder, "paintScrollBarBorder");
   pragma Import (Java, PaintScrollBarThumbBackground, "paintScrollBarThumbBackground");
   pragma Import (Java, PaintScrollBarThumbBorder, "paintScrollBarThumbBorder");
   pragma Import (Java, PaintScrollBarTrackBackground, "paintScrollBarTrackBackground");
   pragma Import (Java, PaintScrollBarTrackBorder, "paintScrollBarTrackBorder");
   pragma Import (Java, PaintScrollPaneBackground, "paintScrollPaneBackground");
   pragma Import (Java, PaintScrollPaneBorder, "paintScrollPaneBorder");
   pragma Import (Java, PaintSeparatorBackground, "paintSeparatorBackground");
   pragma Import (Java, PaintSeparatorBorder, "paintSeparatorBorder");
   pragma Import (Java, PaintSeparatorForeground, "paintSeparatorForeground");
   pragma Import (Java, PaintSliderBackground, "paintSliderBackground");
   pragma Import (Java, PaintSliderBorder, "paintSliderBorder");
   pragma Import (Java, PaintSliderThumbBackground, "paintSliderThumbBackground");
   pragma Import (Java, PaintSliderThumbBorder, "paintSliderThumbBorder");
   pragma Import (Java, PaintSliderTrackBackground, "paintSliderTrackBackground");
   pragma Import (Java, PaintSliderTrackBorder, "paintSliderTrackBorder");
   pragma Import (Java, PaintSpinnerBackground, "paintSpinnerBackground");
   pragma Import (Java, PaintSpinnerBorder, "paintSpinnerBorder");
   pragma Import (Java, PaintSplitPaneDividerBackground, "paintSplitPaneDividerBackground");
   pragma Import (Java, PaintSplitPaneDividerForeground, "paintSplitPaneDividerForeground");
   pragma Import (Java, PaintSplitPaneDragDivider, "paintSplitPaneDragDivider");
   pragma Import (Java, PaintSplitPaneBackground, "paintSplitPaneBackground");
   pragma Import (Java, PaintSplitPaneBorder, "paintSplitPaneBorder");
   pragma Import (Java, PaintTabbedPaneBackground, "paintTabbedPaneBackground");
   pragma Import (Java, PaintTabbedPaneBorder, "paintTabbedPaneBorder");
   pragma Import (Java, PaintTabbedPaneTabAreaBackground, "paintTabbedPaneTabAreaBackground");
   pragma Import (Java, PaintTabbedPaneTabAreaBorder, "paintTabbedPaneTabAreaBorder");
   pragma Import (Java, PaintTabbedPaneTabBackground, "paintTabbedPaneTabBackground");
   pragma Import (Java, PaintTabbedPaneTabBorder, "paintTabbedPaneTabBorder");
   pragma Import (Java, PaintTabbedPaneContentBackground, "paintTabbedPaneContentBackground");
   pragma Import (Java, PaintTabbedPaneContentBorder, "paintTabbedPaneContentBorder");
   pragma Import (Java, PaintTableHeaderBackground, "paintTableHeaderBackground");
   pragma Import (Java, PaintTableHeaderBorder, "paintTableHeaderBorder");
   pragma Import (Java, PaintTableBackground, "paintTableBackground");
   pragma Import (Java, PaintTableBorder, "paintTableBorder");
   pragma Import (Java, PaintTextAreaBackground, "paintTextAreaBackground");
   pragma Import (Java, PaintTextAreaBorder, "paintTextAreaBorder");
   pragma Import (Java, PaintTextPaneBackground, "paintTextPaneBackground");
   pragma Import (Java, PaintTextPaneBorder, "paintTextPaneBorder");
   pragma Import (Java, PaintTextFieldBackground, "paintTextFieldBackground");
   pragma Import (Java, PaintTextFieldBorder, "paintTextFieldBorder");
   pragma Import (Java, PaintToggleButtonBackground, "paintToggleButtonBackground");
   pragma Import (Java, PaintToggleButtonBorder, "paintToggleButtonBorder");
   pragma Import (Java, PaintToolBarBackground, "paintToolBarBackground");
   pragma Import (Java, PaintToolBarBorder, "paintToolBarBorder");
   pragma Import (Java, PaintToolBarContentBackground, "paintToolBarContentBackground");
   pragma Import (Java, PaintToolBarContentBorder, "paintToolBarContentBorder");
   pragma Import (Java, PaintToolBarDragWindowBackground, "paintToolBarDragWindowBackground");
   pragma Import (Java, PaintToolBarDragWindowBorder, "paintToolBarDragWindowBorder");
   pragma Import (Java, PaintToolTipBackground, "paintToolTipBackground");
   pragma Import (Java, PaintToolTipBorder, "paintToolTipBorder");
   pragma Import (Java, PaintTreeBackground, "paintTreeBackground");
   pragma Import (Java, PaintTreeBorder, "paintTreeBorder");
   pragma Import (Java, PaintTreeCellBackground, "paintTreeCellBackground");
   pragma Import (Java, PaintTreeCellBorder, "paintTreeCellBorder");
   pragma Import (Java, PaintTreeCellFocus, "paintTreeCellFocus");
   pragma Import (Java, PaintViewportBackground, "paintViewportBackground");
   pragma Import (Java, PaintViewportBorder, "paintViewportBorder");

end Javax.Swing.Plaf.Synth.SynthPainter;
pragma Import (Java, Javax.Swing.Plaf.Synth.SynthPainter, "javax.swing.plaf.synth.SynthPainter");
pragma Extensions_Allowed (Off);
