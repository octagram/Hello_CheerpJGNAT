pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Awt.Insets;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Plaf.Synth.ColorType;
limited with Javax.Swing.Plaf.Synth.SynthContext;
limited with Javax.Swing.Plaf.Synth.SynthGraphicsUtils;
limited with Javax.Swing.Plaf.Synth.SynthPainter;
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.SynthStyle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SynthStyle (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetGraphicsUtils (This : access Typ;
                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class)
                              return access Javax.Swing.Plaf.Synth.SynthGraphicsUtils.Typ'Class;

   function GetColor (This : access Typ;
                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                      P2_ColorType : access Standard.Javax.Swing.Plaf.Synth.ColorType.Typ'Class)
                      return access Java.Awt.Color.Typ'Class;

   --  protected
   function GetColorForState (This : access Typ;
                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                              P2_ColorType : access Standard.Javax.Swing.Plaf.Synth.ColorType.Typ'Class)
                              return access Java.Awt.Color.Typ'Class is abstract;

   function GetFont (This : access Typ;
                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   --  protected
   function GetFontForState (This : access Typ;
                             P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class)
                             return access Java.Awt.Font.Typ'Class is abstract;

   function GetInsets (This : access Typ;
                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                       P2_Insets : access Standard.Java.Awt.Insets.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   function GetPainter (This : access Typ;
                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class)
                        return access Javax.Swing.Plaf.Synth.SynthPainter.Typ'Class;

   function IsOpaque (This : access Typ;
                      P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class)
                      return Java.Boolean;

   function Get (This : access Typ;
                 P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure InstallDefaults (This : access Typ;
                              P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class);

   procedure UninstallDefaults (This : access Typ;
                                P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class);

   function GetInt (This : access Typ;
                    P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P3_Int : Java.Int)
                    return Java.Int;

   function GetBoolean (This : access Typ;
                        P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Boolean : Java.Boolean)
                        return Java.Boolean;

   function GetIcon (This : access Typ;
                     P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   function GetString (This : access Typ;
                       P1_SynthContext : access Standard.Javax.Swing.Plaf.Synth.SynthContext.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SynthStyle);
   pragma Export (Java, GetGraphicsUtils, "getGraphicsUtils");
   pragma Export (Java, GetColor, "getColor");
   pragma Export (Java, GetColorForState, "getColorForState");
   pragma Export (Java, GetFont, "getFont");
   pragma Export (Java, GetFontForState, "getFontForState");
   pragma Export (Java, GetInsets, "getInsets");
   pragma Export (Java, GetPainter, "getPainter");
   pragma Export (Java, IsOpaque, "isOpaque");
   pragma Export (Java, Get, "get");
   pragma Export (Java, InstallDefaults, "installDefaults");
   pragma Export (Java, UninstallDefaults, "uninstallDefaults");
   pragma Export (Java, GetInt, "getInt");
   pragma Export (Java, GetBoolean, "getBoolean");
   pragma Export (Java, GetIcon, "getIcon");
   pragma Export (Java, GetString, "getString");

end Javax.Swing.Plaf.Synth.SynthStyle;
pragma Import (Java, Javax.Swing.Plaf.Synth.SynthStyle, "javax.swing.plaf.synth.SynthStyle");
pragma Extensions_Allowed (Off);
