pragma Extensions_Allowed (On);
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.Synth.Region;
limited with Javax.Swing.Plaf.Synth.SynthStyle;
with Java.Lang.Object;

package Javax.Swing.Plaf.Synth.SynthStyleFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SynthStyleFactory (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStyle (This : access Typ;
                      P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                      P2_Region : access Standard.Javax.Swing.Plaf.Synth.Region.Typ'Class)
                      return access Javax.Swing.Plaf.Synth.SynthStyle.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SynthStyleFactory);
   pragma Export (Java, GetStyle, "getStyle");

end Javax.Swing.Plaf.Synth.SynthStyleFactory;
pragma Import (Java, Javax.Swing.Plaf.Synth.SynthStyleFactory, "javax.swing.plaf.synth.SynthStyleFactory");
pragma Extensions_Allowed (Off);
