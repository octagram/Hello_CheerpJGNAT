pragma Extensions_Allowed (On);
package Javax.Swing.Plaf.Synth is
   pragma Preelaborate;
end Javax.Swing.Plaf.Synth;
pragma Import (Java, Javax.Swing.Plaf.Synth, "javax.swing.plaf.synth");
pragma Extensions_Allowed (Off);
