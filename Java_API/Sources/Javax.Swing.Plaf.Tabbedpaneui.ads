pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Javax.Swing.JTabbedPane;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.TabbedPaneUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_TabbedPaneUI (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function TabForCoordinate (This : access Typ;
                              P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int)
                              return Java.Int is abstract;

   function GetTabBounds (This : access Typ;
                          P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class;
                          P2_Int : Java.Int)
                          return access Java.Awt.Rectangle.Typ'Class is abstract;

   function GetTabRunCount (This : access Typ;
                            P1_JTabbedPane : access Standard.Javax.Swing.JTabbedPane.Typ'Class)
                            return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TabbedPaneUI);
   pragma Export (Java, TabForCoordinate, "tabForCoordinate");
   pragma Export (Java, GetTabBounds, "getTabBounds");
   pragma Export (Java, GetTabRunCount, "getTabRunCount");

end Javax.Swing.Plaf.TabbedPaneUI;
pragma Import (Java, Javax.Swing.Plaf.TabbedPaneUI, "javax.swing.plaf.TabbedPaneUI");
pragma Extensions_Allowed (Off);
