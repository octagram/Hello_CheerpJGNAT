pragma Extensions_Allowed (On);
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.Text.EditorKit;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.TextUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_TextUI (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ModelToView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int)
                         return access Java.Awt.Rectangle.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ModelToView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Rectangle.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                         return Java.Int is abstract;

   function ViewToModel (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Point : access Standard.Java.Awt.Point.Typ'Class;
                         P3_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int is abstract;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure DamageRange (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int) is abstract;

   procedure DamageRange (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                          P5_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class) is abstract;

   function GetEditorKit (This : access Typ;
                          P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class)
                          return access Javax.Swing.Text.EditorKit.Typ'Class is abstract;

   function GetRootView (This : access Typ;
                         P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class)
                         return access Javax.Swing.Text.View.Typ'Class is abstract;

   function GetToolTipText (This : access Typ;
                            P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                            P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                            return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TextUI);
   pragma Export (Java, ModelToView, "modelToView");
   pragma Export (Java, ViewToModel, "viewToModel");
   pragma Export (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");
   pragma Export (Java, DamageRange, "damageRange");
   pragma Export (Java, GetEditorKit, "getEditorKit");
   pragma Export (Java, GetRootView, "getRootView");
   pragma Import (Java, GetToolTipText, "getToolTipText");

end Javax.Swing.Plaf.TextUI;
pragma Import (Java, Javax.Swing.Plaf.TextUI, "javax.swing.plaf.TextUI");
pragma Extensions_Allowed (Off);
