pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Javax.Swing.JTree;
limited with Javax.Swing.Tree.TreePath;
with Java.Lang.Object;
with Javax.Swing.Plaf.ComponentUI;

package Javax.Swing.Plaf.TreeUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Swing.Plaf.ComponentUI.Typ
      with null record;

   function New_TreeUI (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPathBounds (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return access Java.Awt.Rectangle.Typ'Class is abstract;

   function GetPathForRow (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_Int : Java.Int)
                           return access Javax.Swing.Tree.TreePath.Typ'Class is abstract;

   function GetRowForPath (This : access Typ;
                           P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                           P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return Java.Int is abstract;

   function GetRowCount (This : access Typ;
                         P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                         return Java.Int is abstract;

   function GetClosestPathForLocation (This : access Typ;
                                       P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Int : Java.Int)
                                       return access Javax.Swing.Tree.TreePath.Typ'Class is abstract;

   function IsEditing (This : access Typ;
                       P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                       return Java.Boolean is abstract;

   function StopEditing (This : access Typ;
                         P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                         return Java.Boolean is abstract;

   procedure CancelEditing (This : access Typ;
                            P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class) is abstract;

   procedure StartEditingAtPath (This : access Typ;
                                 P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                 P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class) is abstract;

   function GetEditingPath (This : access Typ;
                            P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class)
                            return access Javax.Swing.Tree.TreePath.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreeUI);
   pragma Export (Java, GetPathBounds, "getPathBounds");
   pragma Export (Java, GetPathForRow, "getPathForRow");
   pragma Export (Java, GetRowForPath, "getRowForPath");
   pragma Export (Java, GetRowCount, "getRowCount");
   pragma Export (Java, GetClosestPathForLocation, "getClosestPathForLocation");
   pragma Export (Java, IsEditing, "isEditing");
   pragma Export (Java, StopEditing, "stopEditing");
   pragma Export (Java, CancelEditing, "cancelEditing");
   pragma Export (Java, StartEditingAtPath, "startEditingAtPath");
   pragma Export (Java, GetEditingPath, "getEditingPath");

end Javax.Swing.Plaf.TreeUI;
pragma Import (Java, Javax.Swing.Plaf.TreeUI, "javax.swing.plaf.TreeUI");
pragma Extensions_Allowed (Off);
