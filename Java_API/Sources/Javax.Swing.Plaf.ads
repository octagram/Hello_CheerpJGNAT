pragma Extensions_Allowed (On);
package Javax.Swing.Plaf is
   pragma Preelaborate;
end Javax.Swing.Plaf;
pragma Import (Java, Javax.Swing.Plaf, "javax.swing.plaf");
pragma Extensions_Allowed (Off);
