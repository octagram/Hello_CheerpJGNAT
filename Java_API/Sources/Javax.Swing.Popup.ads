pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
with Java.Lang.Object;

package Javax.Swing.Popup is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Popup (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                       P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                       P3_Int : Java.Int;
                       P4_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   --  protected
   function New_Popup (This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Show (This : access Typ);

   procedure Hide (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Popup);
   pragma Import (Java, Show, "show");
   pragma Import (Java, Hide, "hide");

end Javax.Swing.Popup;
pragma Import (Java, Javax.Swing.Popup, "javax.swing.Popup");
pragma Extensions_Allowed (Off);
