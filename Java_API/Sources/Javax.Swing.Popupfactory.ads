pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.Popup;
with Java.Lang.Object;

package Javax.Swing.PopupFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PopupFactory (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSharedInstance (P1_PopupFactory : access Standard.Javax.Swing.PopupFactory.Typ'Class);

   function GetSharedInstance return access Javax.Swing.PopupFactory.Typ'Class;

   function GetPopup (This : access Typ;
                      P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int)
                      return access Javax.Swing.Popup.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PopupFactory);
   pragma Import (Java, SetSharedInstance, "setSharedInstance");
   pragma Import (Java, GetSharedInstance, "getSharedInstance");
   pragma Import (Java, GetPopup, "getPopup");

end Javax.Swing.PopupFactory;
pragma Import (Java, Javax.Swing.PopupFactory, "javax.swing.PopupFactory");
pragma Extensions_Allowed (Off);
