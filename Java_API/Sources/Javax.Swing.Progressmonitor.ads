pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;

package Javax.Swing.ProgressMonitor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      AccessibleContext : access Javax.Accessibility.AccessibleContext.Typ'Class;
      pragma Import (Java, AccessibleContext, "accessibleContext");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ProgressMonitor (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetProgress (This : access Typ;
                          P1_Int : Java.Int);

   procedure Close (This : access Typ);

   function GetMinimum (This : access Typ)
                        return Java.Int;

   procedure SetMinimum (This : access Typ;
                         P1_Int : Java.Int);

   function GetMaximum (This : access Typ)
                        return Java.Int;

   procedure SetMaximum (This : access Typ;
                         P1_Int : Java.Int);

   function IsCanceled (This : access Typ)
                        return Java.Boolean;

   procedure SetMillisToDecideToPopup (This : access Typ;
                                       P1_Int : Java.Int);

   function GetMillisToDecideToPopup (This : access Typ)
                                      return Java.Int;

   procedure SetMillisToPopup (This : access Typ;
                               P1_Int : Java.Int);

   function GetMillisToPopup (This : access Typ)
                              return Java.Int;

   procedure SetNote (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetNote (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ProgressMonitor);
   pragma Import (Java, SetProgress, "setProgress");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, IsCanceled, "isCanceled");
   pragma Import (Java, SetMillisToDecideToPopup, "setMillisToDecideToPopup");
   pragma Import (Java, GetMillisToDecideToPopup, "getMillisToDecideToPopup");
   pragma Import (Java, SetMillisToPopup, "setMillisToPopup");
   pragma Import (Java, GetMillisToPopup, "getMillisToPopup");
   pragma Import (Java, SetNote, "setNote");
   pragma Import (Java, GetNote, "getNote");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.ProgressMonitor;
pragma Import (Java, Javax.Swing.ProgressMonitor, "javax.swing.ProgressMonitor");
pragma Extensions_Allowed (Off);
