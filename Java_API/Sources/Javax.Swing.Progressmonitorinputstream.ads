pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Io.InputStream;
limited with Javax.Swing.ProgressMonitor;
with Java.Io.Closeable;
with Java.Io.FilterInputStream;
with Java.Lang.Object;

package Javax.Swing.ProgressMonitorInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is new Java.Io.FilterInputStream.Typ(Closeable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ProgressMonitorInputStream (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                            P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                            P3_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetProgressMonitor (This : access Typ)
                                return access Javax.Swing.ProgressMonitor.Typ'Class;

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Skip (This : access Typ;
                  P1_Long : Java.Long)
                  return Java.Long;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Reset (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ProgressMonitorInputStream);
   pragma Import (Java, GetProgressMonitor, "getProgressMonitor");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Skip, "skip");
   pragma Import (Java, Close, "close");
   pragma Import (Java, Reset, "reset");

end Javax.Swing.ProgressMonitorInputStream;
pragma Import (Java, Javax.Swing.ProgressMonitorInputStream, "javax.swing.ProgressMonitorInputStream");
pragma Extensions_Allowed (Off);
