pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
with Java.Lang.Object;

package Javax.Swing.Renderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Boolean : Java.Boolean) is abstract;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, GetComponent, "getComponent");

end Javax.Swing.Renderer;
pragma Import (Java, Javax.Swing.Renderer, "javax.swing.Renderer");
pragma Extensions_Allowed (Off);
