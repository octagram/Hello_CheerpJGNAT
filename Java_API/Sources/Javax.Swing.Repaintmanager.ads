pragma Extensions_Allowed (On);
limited with Java.Applet.Applet;
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Image;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Window;
limited with Java.Lang.String;
limited with Javax.Swing.JComponent;
with Java.Lang.Object;

package Javax.Swing.RepaintManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CurrentManager (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return access Javax.Swing.RepaintManager.Typ'Class;

   function CurrentManager (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Javax.Swing.RepaintManager.Typ'Class;

   procedure SetCurrentManager (P1_RepaintManager : access Standard.Javax.Swing.RepaintManager.Typ'Class);

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RepaintManager (This : Ref := null)
                                return Ref;

   --  synchronized
   procedure AddInvalidComponent (This : access Typ;
                                  P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   --  synchronized
   procedure RemoveInvalidComponent (This : access Typ;
                                     P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure AddDirtyRegion (This : access Typ;
                             P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int);

   procedure AddDirtyRegion (This : access Typ;
                             P1_Window : access Standard.Java.Awt.Window.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int);

   procedure AddDirtyRegion (This : access Typ;
                             P1_Applet : access Standard.Java.Applet.Applet.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int);

   function GetDirtyRegion (This : access Typ;
                            P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Java.Awt.Rectangle.Typ'Class;

   procedure MarkCompletelyDirty (This : access Typ;
                                  P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure MarkCompletelyClean (This : access Typ;
                                  P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   function IsCompletelyDirty (This : access Typ;
                               P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                               return Java.Boolean;

   procedure ValidateInvalidComponents (This : access Typ);

   procedure PaintDirtyRegions (This : access Typ);

   --  synchronized
   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetOffscreenBuffer (This : access Typ;
                                P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int)
                                return access Java.Awt.Image.Typ'Class;

   function GetVolatileOffscreenBuffer (This : access Typ;
                                        P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int)
                                        return access Java.Awt.Image.Typ'Class;

   procedure SetDoubleBufferMaximumSize (This : access Typ;
                                         P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetDoubleBufferMaximumSize (This : access Typ)
                                        return access Java.Awt.Dimension.Typ'Class;

   procedure SetDoubleBufferingEnabled (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function IsDoubleBufferingEnabled (This : access Typ)
                                      return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CurrentManager, "currentManager");
   pragma Import (Java, SetCurrentManager, "setCurrentManager");
   pragma Java_Constructor (New_RepaintManager);
   pragma Import (Java, AddInvalidComponent, "addInvalidComponent");
   pragma Import (Java, RemoveInvalidComponent, "removeInvalidComponent");
   pragma Import (Java, AddDirtyRegion, "addDirtyRegion");
   pragma Import (Java, GetDirtyRegion, "getDirtyRegion");
   pragma Import (Java, MarkCompletelyDirty, "markCompletelyDirty");
   pragma Import (Java, MarkCompletelyClean, "markCompletelyClean");
   pragma Import (Java, IsCompletelyDirty, "isCompletelyDirty");
   pragma Import (Java, ValidateInvalidComponents, "validateInvalidComponents");
   pragma Import (Java, PaintDirtyRegions, "paintDirtyRegions");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetOffscreenBuffer, "getOffscreenBuffer");
   pragma Import (Java, GetVolatileOffscreenBuffer, "getVolatileOffscreenBuffer");
   pragma Import (Java, SetDoubleBufferMaximumSize, "setDoubleBufferMaximumSize");
   pragma Import (Java, GetDoubleBufferMaximumSize, "getDoubleBufferMaximumSize");
   pragma Import (Java, SetDoubleBufferingEnabled, "setDoubleBufferingEnabled");
   pragma Import (Java, IsDoubleBufferingEnabled, "isDoubleBufferingEnabled");

end Javax.Swing.RepaintManager;
pragma Import (Java, Javax.Swing.RepaintManager, "javax.swing.RepaintManager");
pragma Extensions_Allowed (Off);
