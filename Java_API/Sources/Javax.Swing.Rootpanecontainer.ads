pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Javax.Swing.JLayeredPane;
limited with Javax.Swing.JRootPane;
with Java.Lang.Object;

package Javax.Swing.RootPaneContainer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRootPane (This : access Typ)
                         return access Javax.Swing.JRootPane.Typ'Class is abstract;

   procedure SetContentPane (This : access Typ;
                             P1_Container : access Standard.Java.Awt.Container.Typ'Class) is abstract;

   function GetContentPane (This : access Typ)
                            return access Java.Awt.Container.Typ'Class is abstract;

   procedure SetLayeredPane (This : access Typ;
                             P1_JLayeredPane : access Standard.Javax.Swing.JLayeredPane.Typ'Class) is abstract;

   function GetLayeredPane (This : access Typ)
                            return access Javax.Swing.JLayeredPane.Typ'Class is abstract;

   procedure SetGlassPane (This : access Typ;
                           P1_Component : access Standard.Java.Awt.Component.Typ'Class) is abstract;

   function GetGlassPane (This : access Typ)
                          return access Java.Awt.Component.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRootPane, "getRootPane");
   pragma Export (Java, SetContentPane, "setContentPane");
   pragma Export (Java, GetContentPane, "getContentPane");
   pragma Export (Java, SetLayeredPane, "setLayeredPane");
   pragma Export (Java, GetLayeredPane, "getLayeredPane");
   pragma Export (Java, SetGlassPane, "setGlassPane");
   pragma Export (Java, GetGlassPane, "getGlassPane");

end Javax.Swing.RootPaneContainer;
pragma Import (Java, Javax.Swing.RootPaneContainer, "javax.swing.RootPaneContainer");
pragma Extensions_Allowed (Off);
