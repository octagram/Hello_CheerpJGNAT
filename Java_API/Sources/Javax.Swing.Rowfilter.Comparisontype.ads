pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Swing.RowFilter.ComparisonType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Swing.RowFilter.ComparisonType.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BEFORE : access Javax.Swing.RowFilter.ComparisonType.Typ'Class;

   --  final
   AFTER : access Javax.Swing.RowFilter.ComparisonType.Typ'Class;

   --  final
   EQUAL : access Javax.Swing.RowFilter.ComparisonType.Typ'Class;

   --  final
   NOT_EQUAL : access Javax.Swing.RowFilter.ComparisonType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, BEFORE, "BEFORE");
   pragma Import (Java, AFTER, "AFTER");
   pragma Import (Java, EQUAL, "EQUAL");
   pragma Import (Java, NOT_EQUAL, "NOT_EQUAL");

end Javax.Swing.RowFilter.ComparisonType;
pragma Import (Java, Javax.Swing.RowFilter.ComparisonType, "javax.swing.RowFilter$ComparisonType");
pragma Extensions_Allowed (Off);
