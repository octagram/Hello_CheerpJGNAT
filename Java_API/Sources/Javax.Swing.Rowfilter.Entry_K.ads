pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.RowFilter.Entry_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Entry_K (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetModel (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function GetValueCount (This : access Typ)
                           return Java.Int is abstract;

   function GetValue (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function GetStringValue (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   function GetIdentifier (This : access Typ)
                           return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Entry_K);
   pragma Export (Java, GetModel, "getModel");
   pragma Export (Java, GetValueCount, "getValueCount");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, GetStringValue, "getStringValue");
   pragma Export (Java, GetIdentifier, "getIdentifier");

end Javax.Swing.RowFilter.Entry_K;
pragma Import (Java, Javax.Swing.RowFilter.Entry_K, "javax.swing.RowFilter$Entry");
pragma Extensions_Allowed (Off);
