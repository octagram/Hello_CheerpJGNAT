pragma Extensions_Allowed (On);
limited with Java.Lang.Iterable;
limited with Java.Lang.Number;
limited with Java.Lang.String;
limited with Java.Util.Date;
limited with Javax.Swing.RowFilter.ComparisonType;
limited with Javax.Swing.RowFilter.Entry_K;
with Java.Lang.Object;

package Javax.Swing.RowFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_RowFilter (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function RegexFilter (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int_Arr : Java.Int_Arr)
                         return access Javax.Swing.RowFilter.Typ'Class;

   function DateFilter (P1_ComparisonType : access Standard.Javax.Swing.RowFilter.ComparisonType.Typ'Class;
                        P2_Date : access Standard.Java.Util.Date.Typ'Class;
                        P3_Int_Arr : Java.Int_Arr)
                        return access Javax.Swing.RowFilter.Typ'Class;

   function NumberFilter (P1_ComparisonType : access Standard.Javax.Swing.RowFilter.ComparisonType.Typ'Class;
                          P2_Number : access Standard.Java.Lang.Number.Typ'Class;
                          P3_Int_Arr : Java.Int_Arr)
                          return access Javax.Swing.RowFilter.Typ'Class;

   function OrFilter (P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                      return access Javax.Swing.RowFilter.Typ'Class;

   function AndFilter (P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                       return access Javax.Swing.RowFilter.Typ'Class;

   function NotFilter (P1_RowFilter : access Standard.Javax.Swing.RowFilter.Typ'Class)
                       return access Javax.Swing.RowFilter.Typ'Class;

   function Include (This : access Typ;
                     P1_Entry_K : access Standard.Javax.Swing.RowFilter.Entry_K.Typ'Class)
                     return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RowFilter);
   pragma Export (Java, RegexFilter, "regexFilter");
   pragma Export (Java, DateFilter, "dateFilter");
   pragma Export (Java, NumberFilter, "numberFilter");
   pragma Export (Java, OrFilter, "orFilter");
   pragma Export (Java, AndFilter, "andFilter");
   pragma Export (Java, NotFilter, "notFilter");
   pragma Export (Java, Include, "include");

end Javax.Swing.RowFilter;
pragma Import (Java, Javax.Swing.RowFilter, "javax.swing.RowFilter");
pragma Extensions_Allowed (Off);
