pragma Extensions_Allowed (On);
limited with Javax.Swing.SortOrder;
with Java.Lang.Object;

package Javax.Swing.RowSorter.SortKey is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SortKey (P1_Int : Java.Int;
                         P2_SortOrder : access Standard.Javax.Swing.SortOrder.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetColumn (This : access Typ)
                       return Java.Int;

   --  final
   function GetSortOrder (This : access Typ)
                          return access Javax.Swing.SortOrder.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SortKey);
   pragma Import (Java, GetColumn, "getColumn");
   pragma Import (Java, GetSortOrder, "getSortOrder");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Javax.Swing.RowSorter.SortKey;
pragma Import (Java, Javax.Swing.RowSorter.SortKey, "javax.swing.RowSorter$SortKey");
pragma Extensions_Allowed (Off);
