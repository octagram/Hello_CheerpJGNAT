pragma Extensions_Allowed (On);
limited with Java.Util.List;
limited with Javax.Swing.Event.RowSorterListener;
with Java.Lang.Object;

package Javax.Swing.RowSorter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_RowSorter (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetModel (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   procedure ToggleSortOrder (This : access Typ;
                              P1_Int : Java.Int) is abstract;

   function ConvertRowIndexToModel (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Java.Int is abstract;

   function ConvertRowIndexToView (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Int is abstract;

   procedure SetSortKeys (This : access Typ;
                          P1_List : access Standard.Java.Util.List.Typ'Class) is abstract;

   function GetSortKeys (This : access Typ)
                         return access Java.Util.List.Typ'Class is abstract;

   function GetViewRowCount (This : access Typ)
                             return Java.Int is abstract;

   function GetModelRowCount (This : access Typ)
                              return Java.Int is abstract;

   procedure ModelStructureChanged (This : access Typ) is abstract;

   procedure AllRowsChanged (This : access Typ) is abstract;

   procedure RowsInserted (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int) is abstract;

   procedure RowsDeleted (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int) is abstract;

   procedure RowsUpdated (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int) is abstract;

   procedure RowsUpdated (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int) is abstract;

   procedure AddRowSorterListener (This : access Typ;
                                   P1_RowSorterListener : access Standard.Javax.Swing.Event.RowSorterListener.Typ'Class);

   procedure RemoveRowSorterListener (This : access Typ;
                                      P1_RowSorterListener : access Standard.Javax.Swing.Event.RowSorterListener.Typ'Class);

   --  protected
   procedure FireSortOrderChanged (This : access Typ);

   --  protected
   procedure FireRowSorterChanged (This : access Typ;
                                   P1_Int_Arr : Java.Int_Arr);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RowSorter);
   pragma Export (Java, GetModel, "getModel");
   pragma Export (Java, ToggleSortOrder, "toggleSortOrder");
   pragma Export (Java, ConvertRowIndexToModel, "convertRowIndexToModel");
   pragma Export (Java, ConvertRowIndexToView, "convertRowIndexToView");
   pragma Export (Java, SetSortKeys, "setSortKeys");
   pragma Export (Java, GetSortKeys, "getSortKeys");
   pragma Export (Java, GetViewRowCount, "getViewRowCount");
   pragma Export (Java, GetModelRowCount, "getModelRowCount");
   pragma Export (Java, ModelStructureChanged, "modelStructureChanged");
   pragma Export (Java, AllRowsChanged, "allRowsChanged");
   pragma Export (Java, RowsInserted, "rowsInserted");
   pragma Export (Java, RowsDeleted, "rowsDeleted");
   pragma Export (Java, RowsUpdated, "rowsUpdated");
   pragma Export (Java, AddRowSorterListener, "addRowSorterListener");
   pragma Export (Java, RemoveRowSorterListener, "removeRowSorterListener");
   pragma Export (Java, FireSortOrderChanged, "fireSortOrderChanged");
   pragma Export (Java, FireRowSorterChanged, "fireRowSorterChanged");

end Javax.Swing.RowSorter;
pragma Import (Java, Javax.Swing.RowSorter, "javax.swing.RowSorter");
pragma Extensions_Allowed (Off);
