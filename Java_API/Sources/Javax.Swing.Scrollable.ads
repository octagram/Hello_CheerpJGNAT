pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Rectangle;
with Java.Lang.Object;

package Javax.Swing.Scrollable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPreferredScrollableViewportSize (This : access Typ)
                                                return access Java.Awt.Dimension.Typ'Class is abstract;

   function GetScrollableUnitIncrement (This : access Typ;
                                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int)
                                        return Java.Int is abstract;

   function GetScrollableBlockIncrement (This : access Typ;
                                         P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int)
                                         return Java.Int is abstract;

   function GetScrollableTracksViewportWidth (This : access Typ)
                                              return Java.Boolean is abstract;

   function GetScrollableTracksViewportHeight (This : access Typ)
                                               return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPreferredScrollableViewportSize, "getPreferredScrollableViewportSize");
   pragma Export (Java, GetScrollableUnitIncrement, "getScrollableUnitIncrement");
   pragma Export (Java, GetScrollableBlockIncrement, "getScrollableBlockIncrement");
   pragma Export (Java, GetScrollableTracksViewportWidth, "getScrollableTracksViewportWidth");
   pragma Export (Java, GetScrollableTracksViewportHeight, "getScrollableTracksViewportHeight");

end Javax.Swing.Scrollable;
pragma Import (Java, Javax.Swing.Scrollable, "javax.swing.Scrollable");
pragma Extensions_Allowed (Off);
