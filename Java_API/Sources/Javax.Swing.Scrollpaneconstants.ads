pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.ScrollPaneConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   VIEWPORT : constant access Java.Lang.String.Typ'Class;

   --  final
   VERTICAL_SCROLLBAR : constant access Java.Lang.String.Typ'Class;

   --  final
   HORIZONTAL_SCROLLBAR : constant access Java.Lang.String.Typ'Class;

   --  final
   ROW_HEADER : constant access Java.Lang.String.Typ'Class;

   --  final
   COLUMN_HEADER : constant access Java.Lang.String.Typ'Class;

   --  final
   LOWER_LEFT_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   LOWER_RIGHT_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   UPPER_LEFT_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   UPPER_RIGHT_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   LOWER_LEADING_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   LOWER_TRAILING_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   UPPER_LEADING_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   UPPER_TRAILING_CORNER : constant access Java.Lang.String.Typ'Class;

   --  final
   VERTICAL_SCROLLBAR_POLICY : constant access Java.Lang.String.Typ'Class;

   --  final
   HORIZONTAL_SCROLLBAR_POLICY : constant access Java.Lang.String.Typ'Class;

   --  final
   VERTICAL_SCROLLBAR_AS_NEEDED : constant Java.Int;

   --  final
   VERTICAL_SCROLLBAR_NEVER : constant Java.Int;

   --  final
   VERTICAL_SCROLLBAR_ALWAYS : constant Java.Int;

   --  final
   HORIZONTAL_SCROLLBAR_AS_NEEDED : constant Java.Int;

   --  final
   HORIZONTAL_SCROLLBAR_NEVER : constant Java.Int;

   --  final
   HORIZONTAL_SCROLLBAR_ALWAYS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, VIEWPORT, "VIEWPORT");
   pragma Import (Java, VERTICAL_SCROLLBAR, "VERTICAL_SCROLLBAR");
   pragma Import (Java, HORIZONTAL_SCROLLBAR, "HORIZONTAL_SCROLLBAR");
   pragma Import (Java, ROW_HEADER, "ROW_HEADER");
   pragma Import (Java, COLUMN_HEADER, "COLUMN_HEADER");
   pragma Import (Java, LOWER_LEFT_CORNER, "LOWER_LEFT_CORNER");
   pragma Import (Java, LOWER_RIGHT_CORNER, "LOWER_RIGHT_CORNER");
   pragma Import (Java, UPPER_LEFT_CORNER, "UPPER_LEFT_CORNER");
   pragma Import (Java, UPPER_RIGHT_CORNER, "UPPER_RIGHT_CORNER");
   pragma Import (Java, LOWER_LEADING_CORNER, "LOWER_LEADING_CORNER");
   pragma Import (Java, LOWER_TRAILING_CORNER, "LOWER_TRAILING_CORNER");
   pragma Import (Java, UPPER_LEADING_CORNER, "UPPER_LEADING_CORNER");
   pragma Import (Java, UPPER_TRAILING_CORNER, "UPPER_TRAILING_CORNER");
   pragma Import (Java, VERTICAL_SCROLLBAR_POLICY, "VERTICAL_SCROLLBAR_POLICY");
   pragma Import (Java, HORIZONTAL_SCROLLBAR_POLICY, "HORIZONTAL_SCROLLBAR_POLICY");
   pragma Import (Java, VERTICAL_SCROLLBAR_AS_NEEDED, "VERTICAL_SCROLLBAR_AS_NEEDED");
   pragma Import (Java, VERTICAL_SCROLLBAR_NEVER, "VERTICAL_SCROLLBAR_NEVER");
   pragma Import (Java, VERTICAL_SCROLLBAR_ALWAYS, "VERTICAL_SCROLLBAR_ALWAYS");
   pragma Import (Java, HORIZONTAL_SCROLLBAR_AS_NEEDED, "HORIZONTAL_SCROLLBAR_AS_NEEDED");
   pragma Import (Java, HORIZONTAL_SCROLLBAR_NEVER, "HORIZONTAL_SCROLLBAR_NEVER");
   pragma Import (Java, HORIZONTAL_SCROLLBAR_ALWAYS, "HORIZONTAL_SCROLLBAR_ALWAYS");

end Javax.Swing.ScrollPaneConstants;
pragma Import (Java, Javax.Swing.ScrollPaneConstants, "javax.swing.ScrollPaneConstants");
pragma Extensions_Allowed (Off);
