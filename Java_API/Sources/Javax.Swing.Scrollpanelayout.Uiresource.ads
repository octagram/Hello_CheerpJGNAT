pragma Extensions_Allowed (On);
with Java.Awt.LayoutManager;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Plaf.UIResource;
with Javax.Swing.ScrollPaneConstants;
with Javax.Swing.ScrollPaneLayout;

package Javax.Swing.ScrollPaneLayout.UIResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            ScrollPaneConstants_I : Javax.Swing.ScrollPaneConstants.Ref;
            UIResource_I : Javax.Swing.Plaf.UIResource.Ref)
    is new Javax.Swing.ScrollPaneLayout.Typ(LayoutManager_I,
                                            Serializable_I,
                                            ScrollPaneConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UIResource (This : Ref := null)
                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UIResource);

end Javax.Swing.ScrollPaneLayout.UIResource;
pragma Import (Java, Javax.Swing.ScrollPaneLayout.UIResource, "javax.swing.ScrollPaneLayout$UIResource");
pragma Extensions_Allowed (Off);
