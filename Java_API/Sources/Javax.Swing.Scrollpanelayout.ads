pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
limited with Javax.Swing.JScrollBar;
limited with Javax.Swing.JScrollPane;
limited with Javax.Swing.JViewport;
with Java.Awt.LayoutManager;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.ScrollPaneConstants;

package Javax.Swing.ScrollPaneLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager_I : Java.Awt.LayoutManager.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            ScrollPaneConstants_I : Javax.Swing.ScrollPaneConstants.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Viewport : access Javax.Swing.JViewport.Typ'Class;
      pragma Import (Java, Viewport, "viewport");

      --  protected
      Vsb : access Javax.Swing.JScrollBar.Typ'Class;
      pragma Import (Java, Vsb, "vsb");

      --  protected
      Hsb : access Javax.Swing.JScrollBar.Typ'Class;
      pragma Import (Java, Hsb, "hsb");

      --  protected
      RowHead : access Javax.Swing.JViewport.Typ'Class;
      pragma Import (Java, RowHead, "rowHead");

      --  protected
      ColHead : access Javax.Swing.JViewport.Typ'Class;
      pragma Import (Java, ColHead, "colHead");

      --  protected
      LowerLeft : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, LowerLeft, "lowerLeft");

      --  protected
      LowerRight : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, LowerRight, "lowerRight");

      --  protected
      UpperLeft : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, UpperLeft, "upperLeft");

      --  protected
      UpperRight : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, UpperRight, "upperRight");

      --  protected
      VsbPolicy : Java.Int;
      pragma Import (Java, VsbPolicy, "vsbPolicy");

      --  protected
      HsbPolicy : Java.Int;
      pragma Import (Java, HsbPolicy, "hsbPolicy");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ScrollPaneLayout (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SyncWithScrollPane (This : access Typ;
                                 P1_JScrollPane : access Standard.Javax.Swing.JScrollPane.Typ'Class);

   --  protected
   function AddSingletonComponent (This : access Typ;
                                   P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                   P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                                   return access Java.Awt.Component.Typ'Class;

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetVerticalScrollBarPolicy (This : access Typ)
                                        return Java.Int;

   procedure SetVerticalScrollBarPolicy (This : access Typ;
                                         P1_Int : Java.Int);

   function GetHorizontalScrollBarPolicy (This : access Typ)
                                          return Java.Int;

   procedure SetHorizontalScrollBarPolicy (This : access Typ;
                                           P1_Int : Java.Int);

   function GetViewport (This : access Typ)
                         return access Javax.Swing.JViewport.Typ'Class;

   function GetHorizontalScrollBar (This : access Typ)
                                    return access Javax.Swing.JScrollBar.Typ'Class;

   function GetVerticalScrollBar (This : access Typ)
                                  return access Javax.Swing.JScrollBar.Typ'Class;

   function GetRowHeader (This : access Typ)
                          return access Javax.Swing.JViewport.Typ'Class;

   function GetColumnHeader (This : access Typ)
                             return access Javax.Swing.JViewport.Typ'Class;

   function GetCorner (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Awt.Component.Typ'Class;

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ScrollPaneLayout);
   pragma Import (Java, SyncWithScrollPane, "syncWithScrollPane");
   pragma Import (Java, AddSingletonComponent, "addSingletonComponent");
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, GetVerticalScrollBarPolicy, "getVerticalScrollBarPolicy");
   pragma Import (Java, SetVerticalScrollBarPolicy, "setVerticalScrollBarPolicy");
   pragma Import (Java, GetHorizontalScrollBarPolicy, "getHorizontalScrollBarPolicy");
   pragma Import (Java, SetHorizontalScrollBarPolicy, "setHorizontalScrollBarPolicy");
   pragma Import (Java, GetViewport, "getViewport");
   pragma Import (Java, GetHorizontalScrollBar, "getHorizontalScrollBar");
   pragma Import (Java, GetVerticalScrollBar, "getVerticalScrollBar");
   pragma Import (Java, GetRowHeader, "getRowHeader");
   pragma Import (Java, GetColumnHeader, "getColumnHeader");
   pragma Import (Java, GetCorner, "getCorner");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, LayoutContainer, "layoutContainer");

end Javax.Swing.ScrollPaneLayout;
pragma Import (Java, Javax.Swing.ScrollPaneLayout, "javax.swing.ScrollPaneLayout");
pragma Extensions_Allowed (Off);
