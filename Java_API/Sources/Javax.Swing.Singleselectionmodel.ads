pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ChangeListener;
with Java.Lang.Object;

package Javax.Swing.SingleSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSelectedIndex (This : access Typ)
                              return Java.Int is abstract;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   procedure ClearSelection (This : access Typ) is abstract;

   function IsSelected (This : access Typ)
                        return Java.Boolean is abstract;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Export (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Export (Java, ClearSelection, "clearSelection");
   pragma Export (Java, IsSelected, "isSelected");
   pragma Export (Java, AddChangeListener, "addChangeListener");
   pragma Export (Java, RemoveChangeListener, "removeChangeListener");

end Javax.Swing.SingleSelectionModel;
pragma Import (Java, Javax.Swing.SingleSelectionModel, "javax.swing.SingleSelectionModel");
pragma Extensions_Allowed (Off);
