pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.SizeRequirements is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Minimum : Java.Int;
      pragma Import (Java, Minimum, "minimum");

      Preferred : Java.Int;
      pragma Import (Java, Preferred, "preferred");

      Maximum : Java.Int;
      pragma Import (Java, Maximum, "maximum");

      Alignment : Java.Float;
      pragma Import (Java, Alignment, "alignment");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SizeRequirements (This : Ref := null)
                                  return Ref;

   function New_SizeRequirements (P1_Int : Java.Int;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int;
                                  P4_Float : Java.Float; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetTiledSizeRequirements (P1_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj)
                                      return access Javax.Swing.SizeRequirements.Typ'Class;

   function GetAlignedSizeRequirements (P1_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj)
                                        return access Javax.Swing.SizeRequirements.Typ'Class;

   procedure CalculateTiledPositions (P1_Int : Java.Int;
                                      P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class;
                                      P3_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj;
                                      P4_Int_Arr : Java.Int_Arr;
                                      P5_Int_Arr : Java.Int_Arr);

   procedure CalculateTiledPositions (P1_Int : Java.Int;
                                      P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class;
                                      P3_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj;
                                      P4_Int_Arr : Java.Int_Arr;
                                      P5_Int_Arr : Java.Int_Arr;
                                      P6_Boolean : Java.Boolean);

   procedure CalculateAlignedPositions (P1_Int : Java.Int;
                                        P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class;
                                        P3_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj;
                                        P4_Int_Arr : Java.Int_Arr;
                                        P5_Int_Arr : Java.Int_Arr);

   procedure CalculateAlignedPositions (P1_Int : Java.Int;
                                        P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class;
                                        P3_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj;
                                        P4_Int_Arr : Java.Int_Arr;
                                        P5_Int_Arr : Java.Int_Arr;
                                        P6_Boolean : Java.Boolean);

   function AdjustSizes (P1_Int : Java.Int;
                         P2_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj)
                         return Java.Int_Arr;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SizeRequirements);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetTiledSizeRequirements, "getTiledSizeRequirements");
   pragma Import (Java, GetAlignedSizeRequirements, "getAlignedSizeRequirements");
   pragma Import (Java, CalculateTiledPositions, "calculateTiledPositions");
   pragma Import (Java, CalculateAlignedPositions, "calculateAlignedPositions");
   pragma Import (Java, AdjustSizes, "adjustSizes");

end Javax.Swing.SizeRequirements;
pragma Import (Java, Javax.Swing.SizeRequirements, "javax.swing.SizeRequirements");
pragma Extensions_Allowed (Off);
