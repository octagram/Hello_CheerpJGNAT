pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Swing.SizeSequence is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SizeSequence (This : Ref := null)
                              return Ref;

   function New_SizeSequence (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_SizeSequence (P1_Int : Java.Int;
                              P2_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_SizeSequence (P1_Int_Arr : Java.Int_Arr; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSizes (This : access Typ;
                       P1_Int_Arr : Java.Int_Arr);

   function GetSizes (This : access Typ)
                      return Java.Int_Arr;

   function GetPosition (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function GetIndex (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Int;

   function GetSize (This : access Typ;
                     P1_Int : Java.Int)
                     return Java.Int;

   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int);

   procedure InsertEntries (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int);

   procedure RemoveEntries (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SizeSequence);
   pragma Import (Java, SetSizes, "setSizes");
   pragma Import (Java, GetSizes, "getSizes");
   pragma Import (Java, GetPosition, "getPosition");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, InsertEntries, "insertEntries");
   pragma Import (Java, RemoveEntries, "removeEntries");

end Javax.Swing.SizeSequence;
pragma Import (Java, Javax.Swing.SizeSequence, "javax.swing.SizeSequence");
pragma Extensions_Allowed (Off);
