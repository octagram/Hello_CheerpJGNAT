pragma Extensions_Allowed (On);
limited with Java.Lang.Comparable;
limited with Java.Util.Date;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.AbstractSpinnerModel;
with Javax.Swing.SpinnerModel;

package Javax.Swing.SpinnerDateModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            SpinnerModel_I : Javax.Swing.SpinnerModel.Ref)
    is new Javax.Swing.AbstractSpinnerModel.Typ(SpinnerModel_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SpinnerDateModel (P1_Date : access Standard.Java.Util.Date.Typ'Class;
                                  P2_Comparable : access Standard.Java.Lang.Comparable.Typ'Class;
                                  P3_Comparable : access Standard.Java.Lang.Comparable.Typ'Class;
                                  P4_Int : Java.Int; 
                                  This : Ref := null)
                                  return Ref;

   function New_SpinnerDateModel (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetStart (This : access Typ;
                       P1_Comparable : access Standard.Java.Lang.Comparable.Typ'Class);

   function GetStart (This : access Typ)
                      return access Java.Lang.Comparable.Typ'Class;

   procedure SetEnd (This : access Typ;
                     P1_Comparable : access Standard.Java.Lang.Comparable.Typ'Class);

   function GetEnd (This : access Typ)
                    return access Java.Lang.Comparable.Typ'Class;

   procedure SetCalendarField (This : access Typ;
                               P1_Int : Java.Int);

   function GetCalendarField (This : access Typ)
                              return Java.Int;

   function GetNextValue (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   function GetPreviousValue (This : access Typ)
                              return access Java.Lang.Object.Typ'Class;

   function GetDate (This : access Typ)
                     return access Java.Util.Date.Typ'Class;

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SpinnerDateModel);
   pragma Import (Java, SetStart, "setStart");
   pragma Import (Java, GetStart, "getStart");
   pragma Import (Java, SetEnd, "setEnd");
   pragma Import (Java, GetEnd, "getEnd");
   pragma Import (Java, SetCalendarField, "setCalendarField");
   pragma Import (Java, GetCalendarField, "getCalendarField");
   pragma Import (Java, GetNextValue, "getNextValue");
   pragma Import (Java, GetPreviousValue, "getPreviousValue");
   pragma Import (Java, GetDate, "getDate");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");

end Javax.Swing.SpinnerDateModel;
pragma Import (Java, Javax.Swing.SpinnerDateModel, "javax.swing.SpinnerDateModel");
pragma Extensions_Allowed (Off);
