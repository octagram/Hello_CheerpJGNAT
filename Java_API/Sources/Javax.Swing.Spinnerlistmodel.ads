pragma Extensions_Allowed (On);
limited with Java.Util.List;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.AbstractSpinnerModel;
with Javax.Swing.SpinnerModel;

package Javax.Swing.SpinnerListModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            SpinnerModel_I : Javax.Swing.SpinnerModel.Ref)
    is new Javax.Swing.AbstractSpinnerModel.Typ(SpinnerModel_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SpinnerListModel (P1_List : access Standard.Java.Util.List.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_SpinnerListModel (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                  This : Ref := null)
                                  return Ref;

   function New_SpinnerListModel (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetList (This : access Typ)
                     return access Java.Util.List.Typ'Class;

   procedure SetList (This : access Typ;
                      P1_List : access Standard.Java.Util.List.Typ'Class);

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetNextValue (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   function GetPreviousValue (This : access Typ)
                              return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SpinnerListModel);
   pragma Import (Java, GetList, "getList");
   pragma Import (Java, SetList, "setList");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetNextValue, "getNextValue");
   pragma Import (Java, GetPreviousValue, "getPreviousValue");

end Javax.Swing.SpinnerListModel;
pragma Import (Java, Javax.Swing.SpinnerListModel, "javax.swing.SpinnerListModel");
pragma Extensions_Allowed (Off);
