pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.ChangeListener;
with Java.Lang.Object;

package Javax.Swing.SpinnerModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class is abstract;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetNextValue (This : access Typ)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function GetPreviousValue (This : access Typ)
                              return access Java.Lang.Object.Typ'Class is abstract;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, GetNextValue, "getNextValue");
   pragma Export (Java, GetPreviousValue, "getPreviousValue");
   pragma Export (Java, AddChangeListener, "addChangeListener");
   pragma Export (Java, RemoveChangeListener, "removeChangeListener");

end Javax.Swing.SpinnerModel;
pragma Import (Java, Javax.Swing.SpinnerModel, "javax.swing.SpinnerModel");
pragma Extensions_Allowed (Off);
