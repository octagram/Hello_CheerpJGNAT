pragma Extensions_Allowed (On);
limited with Java.Lang.Comparable;
limited with Java.Lang.Number;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.AbstractSpinnerModel;
with Javax.Swing.SpinnerModel;

package Javax.Swing.SpinnerNumberModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            SpinnerModel_I : Javax.Swing.SpinnerModel.Ref)
    is new Javax.Swing.AbstractSpinnerModel.Typ(SpinnerModel_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SpinnerNumberModel (P1_Number : access Standard.Java.Lang.Number.Typ'Class;
                                    P2_Comparable : access Standard.Java.Lang.Comparable.Typ'Class;
                                    P3_Comparable : access Standard.Java.Lang.Comparable.Typ'Class;
                                    P4_Number : access Standard.Java.Lang.Number.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_SpinnerNumberModel (P1_Int : Java.Int;
                                    P2_Int : Java.Int;
                                    P3_Int : Java.Int;
                                    P4_Int : Java.Int; 
                                    This : Ref := null)
                                    return Ref;

   function New_SpinnerNumberModel (P1_Double : Java.Double;
                                    P2_Double : Java.Double;
                                    P3_Double : Java.Double;
                                    P4_Double : Java.Double; 
                                    This : Ref := null)
                                    return Ref;

   function New_SpinnerNumberModel (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMinimum (This : access Typ;
                         P1_Comparable : access Standard.Java.Lang.Comparable.Typ'Class);

   function GetMinimum (This : access Typ)
                        return access Java.Lang.Comparable.Typ'Class;

   procedure SetMaximum (This : access Typ;
                         P1_Comparable : access Standard.Java.Lang.Comparable.Typ'Class);

   function GetMaximum (This : access Typ)
                        return access Java.Lang.Comparable.Typ'Class;

   procedure SetStepSize (This : access Typ;
                          P1_Number : access Standard.Java.Lang.Number.Typ'Class);

   function GetStepSize (This : access Typ)
                         return access Java.Lang.Number.Typ'Class;

   function GetNextValue (This : access Typ)
                          return access Java.Lang.Object.Typ'Class;

   function GetPreviousValue (This : access Typ)
                              return access Java.Lang.Object.Typ'Class;

   function GetNumber (This : access Typ)
                       return access Java.Lang.Number.Typ'Class;

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SpinnerNumberModel);
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, SetStepSize, "setStepSize");
   pragma Import (Java, GetStepSize, "getStepSize");
   pragma Import (Java, GetNextValue, "getNextValue");
   pragma Import (Java, GetPreviousValue, "getPreviousValue");
   pragma Import (Java, GetNumber, "getNumber");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");

end Javax.Swing.SpinnerNumberModel;
pragma Import (Java, Javax.Swing.SpinnerNumberModel, "javax.swing.SpinnerNumberModel");
pragma Extensions_Allowed (Off);
