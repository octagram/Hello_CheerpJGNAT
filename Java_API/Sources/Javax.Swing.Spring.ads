pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
with Java.Lang.Object;

package Javax.Swing.Spring is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Spring (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMinimumValue (This : access Typ)
                             return Java.Int is abstract;

   function GetPreferredValue (This : access Typ)
                               return Java.Int is abstract;

   function GetMaximumValue (This : access Typ)
                             return Java.Int is abstract;

   function GetValue (This : access Typ)
                      return Java.Int is abstract;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   function constant_K (P1_Int : Java.Int)
                        return access Javax.Swing.Spring.Typ'Class;

   function constant_K (P1_Int : Java.Int;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int)
                        return access Javax.Swing.Spring.Typ'Class;

   function Minus (P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class)
                   return access Javax.Swing.Spring.Typ'Class;

   function Sum (P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                 P2_Spring : access Standard.Javax.Swing.Spring.Typ'Class)
                 return access Javax.Swing.Spring.Typ'Class;

   function Max (P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                 P2_Spring : access Standard.Javax.Swing.Spring.Typ'Class)
                 return access Javax.Swing.Spring.Typ'Class;

   function Scale (P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                   P2_Float : Java.Float)
                   return access Javax.Swing.Spring.Typ'Class;

   function Width (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                   return access Javax.Swing.Spring.Typ'Class;

   function Height (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                    return access Javax.Swing.Spring.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNSET : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Spring);
   pragma Export (Java, GetMinimumValue, "getMinimumValue");
   pragma Export (Java, GetPreferredValue, "getPreferredValue");
   pragma Export (Java, GetMaximumValue, "getMaximumValue");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, constant_K, "constant");
   pragma Export (Java, Minus, "minus");
   pragma Export (Java, Sum, "sum");
   pragma Export (Java, Max, "max");
   pragma Export (Java, Scale, "scale");
   pragma Export (Java, Width, "width");
   pragma Export (Java, Height, "height");
   pragma Import (Java, UNSET, "UNSET");

end Javax.Swing.Spring;
pragma Import (Java, Javax.Swing.Spring, "javax.swing.Spring");
pragma Extensions_Allowed (Off);
