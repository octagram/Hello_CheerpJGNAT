pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Swing.Spring;
with Java.Lang.Object;

package Javax.Swing.SpringLayout.Constraints is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Constraints (This : Ref := null)
                             return Ref;

   function New_Constraints (P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                             P2_Spring : access Standard.Javax.Swing.Spring.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_Constraints (P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                             P2_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                             P3_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                             P4_Spring : access Standard.Javax.Swing.Spring.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_Constraints (P1_Component : access Standard.Java.Awt.Component.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetX (This : access Typ;
                   P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class);

   function GetX (This : access Typ)
                  return access Javax.Swing.Spring.Typ'Class;

   procedure SetY (This : access Typ;
                   P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class);

   function GetY (This : access Typ)
                  return access Javax.Swing.Spring.Typ'Class;

   procedure SetWidth (This : access Typ;
                       P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class);

   function GetWidth (This : access Typ)
                      return access Javax.Swing.Spring.Typ'Class;

   procedure SetHeight (This : access Typ;
                        P1_Spring : access Standard.Javax.Swing.Spring.Typ'Class);

   function GetHeight (This : access Typ)
                       return access Javax.Swing.Spring.Typ'Class;

   procedure SetConstraint (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Spring : access Standard.Javax.Swing.Spring.Typ'Class);

   function GetConstraint (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Swing.Spring.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Constraints);
   pragma Import (Java, SetX, "setX");
   pragma Import (Java, GetX, "getX");
   pragma Import (Java, SetY, "setY");
   pragma Import (Java, GetY, "getY");
   pragma Import (Java, SetWidth, "setWidth");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, SetHeight, "setHeight");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, SetConstraint, "setConstraint");
   pragma Import (Java, GetConstraint, "getConstraint");

end Javax.Swing.SpringLayout.Constraints;
pragma Import (Java, Javax.Swing.SpringLayout.Constraints, "javax.swing.SpringLayout$Constraints");
pragma Extensions_Allowed (Off);
