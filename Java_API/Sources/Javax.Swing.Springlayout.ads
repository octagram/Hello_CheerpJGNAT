pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Dimension;
limited with Java.Lang.String;
limited with Javax.Swing.Spring;
limited with Javax.Swing.SpringLayout.Constraints;
with Java.Awt.LayoutManager2;
with Java.Lang.Object;

package Javax.Swing.SpringLayout is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LayoutManager2_I : Java.Awt.LayoutManager2.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SpringLayout (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddLayoutComponent (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure RemoveLayoutComponent (This : access Typ;
                                    P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   function MinimumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   function PreferredLayoutSize (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return access Java.Awt.Dimension.Typ'Class;

   function MaximumLayoutSize (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                               return access Java.Awt.Dimension.Typ'Class;

   procedure AddLayoutComponent (This : access Typ;
                                 P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetLayoutAlignmentX (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   function GetLayoutAlignmentY (This : access Typ;
                                 P1_Container : access Standard.Java.Awt.Container.Typ'Class)
                                 return Java.Float;

   procedure InvalidateLayout (This : access Typ;
                               P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   procedure PutConstraint (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P3_Int : Java.Int;
                            P4_String : access Standard.Java.Lang.String.Typ'Class;
                            P5_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure PutConstraint (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                            P3_Spring : access Standard.Javax.Swing.Spring.Typ'Class;
                            P4_String : access Standard.Java.Lang.String.Typ'Class;
                            P5_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetConstraints (This : access Typ;
                            P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return access Javax.Swing.SpringLayout.Constraints.Typ'Class;

   function GetConstraint (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                           return access Javax.Swing.Spring.Typ'Class;

   procedure LayoutContainer (This : access Typ;
                              P1_Container : access Standard.Java.Awt.Container.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NORTH : constant access Java.Lang.String.Typ'Class;

   --  final
   SOUTH : constant access Java.Lang.String.Typ'Class;

   --  final
   EAST : constant access Java.Lang.String.Typ'Class;

   --  final
   WEST : constant access Java.Lang.String.Typ'Class;

   --  final
   HORIZONTAL_CENTER : constant access Java.Lang.String.Typ'Class;

   --  final
   VERTICAL_CENTER : constant access Java.Lang.String.Typ'Class;

   --  final
   BASELINE : constant access Java.Lang.String.Typ'Class;

   --  final
   WIDTH : constant access Java.Lang.String.Typ'Class;

   --  final
   HEIGHT : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SpringLayout);
   pragma Import (Java, AddLayoutComponent, "addLayoutComponent");
   pragma Import (Java, RemoveLayoutComponent, "removeLayoutComponent");
   pragma Import (Java, MinimumLayoutSize, "minimumLayoutSize");
   pragma Import (Java, PreferredLayoutSize, "preferredLayoutSize");
   pragma Import (Java, MaximumLayoutSize, "maximumLayoutSize");
   pragma Import (Java, GetLayoutAlignmentX, "getLayoutAlignmentX");
   pragma Import (Java, GetLayoutAlignmentY, "getLayoutAlignmentY");
   pragma Import (Java, InvalidateLayout, "invalidateLayout");
   pragma Import (Java, PutConstraint, "putConstraint");
   pragma Import (Java, GetConstraints, "getConstraints");
   pragma Import (Java, GetConstraint, "getConstraint");
   pragma Import (Java, LayoutContainer, "layoutContainer");
   pragma Import (Java, NORTH, "NORTH");
   pragma Import (Java, SOUTH, "SOUTH");
   pragma Import (Java, EAST, "EAST");
   pragma Import (Java, WEST, "WEST");
   pragma Import (Java, HORIZONTAL_CENTER, "HORIZONTAL_CENTER");
   pragma Import (Java, VERTICAL_CENTER, "VERTICAL_CENTER");
   pragma Import (Java, BASELINE, "BASELINE");
   pragma Import (Java, WIDTH, "WIDTH");
   pragma Import (Java, HEIGHT, "HEIGHT");

end Javax.Swing.SpringLayout;
pragma Import (Java, Javax.Swing.SpringLayout, "javax.swing.SpringLayout");
pragma Extensions_Allowed (Off);
