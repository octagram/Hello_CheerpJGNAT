pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Swing.SwingConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CENTER : constant Java.Int;

   --  final
   TOP : constant Java.Int;

   --  final
   LEFT : constant Java.Int;

   --  final
   BOTTOM : constant Java.Int;

   --  final
   RIGHT : constant Java.Int;

   --  final
   NORTH : constant Java.Int;

   --  final
   NORTH_EAST : constant Java.Int;

   --  final
   EAST : constant Java.Int;

   --  final
   SOUTH_EAST : constant Java.Int;

   --  final
   SOUTH : constant Java.Int;

   --  final
   SOUTH_WEST : constant Java.Int;

   --  final
   WEST : constant Java.Int;

   --  final
   NORTH_WEST : constant Java.Int;

   --  final
   HORIZONTAL : constant Java.Int;

   --  final
   VERTICAL : constant Java.Int;

   --  final
   LEADING : constant Java.Int;

   --  final
   TRAILING : constant Java.Int;

   --  final
   NEXT : constant Java.Int;

   --  final
   PREVIOUS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CENTER, "CENTER");
   pragma Import (Java, TOP, "TOP");
   pragma Import (Java, LEFT, "LEFT");
   pragma Import (Java, BOTTOM, "BOTTOM");
   pragma Import (Java, RIGHT, "RIGHT");
   pragma Import (Java, NORTH, "NORTH");
   pragma Import (Java, NORTH_EAST, "NORTH_EAST");
   pragma Import (Java, EAST, "EAST");
   pragma Import (Java, SOUTH_EAST, "SOUTH_EAST");
   pragma Import (Java, SOUTH, "SOUTH");
   pragma Import (Java, SOUTH_WEST, "SOUTH_WEST");
   pragma Import (Java, WEST, "WEST");
   pragma Import (Java, NORTH_WEST, "NORTH_WEST");
   pragma Import (Java, HORIZONTAL, "HORIZONTAL");
   pragma Import (Java, VERTICAL, "VERTICAL");
   pragma Import (Java, LEADING, "LEADING");
   pragma Import (Java, TRAILING, "TRAILING");
   pragma Import (Java, NEXT, "NEXT");
   pragma Import (Java, PREVIOUS, "PREVIOUS");

end Javax.Swing.SwingConstants;
pragma Import (Java, Javax.Swing.SwingConstants, "javax.swing.SwingConstants");
pragma Extensions_Allowed (Off);
