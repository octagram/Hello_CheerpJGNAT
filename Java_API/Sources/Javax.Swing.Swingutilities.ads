pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Event.KeyEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Window;
limited with Java.Lang.Class;
limited with Java.Lang.Runnable;
limited with Java.Lang.String;
limited with Javax.Accessibility.Accessible;
limited with Javax.Accessibility.AccessibleStateSet;
limited with Javax.Swing.Action;
limited with Javax.Swing.ActionMap;
limited with Javax.Swing.Icon;
limited with Javax.Swing.InputMap;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.JRootPane;
limited with Javax.Swing.KeyStroke;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;

package Javax.Swing.SwingUtilities is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function IsRectangleContainingRectangle (P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                            P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                                            return Java.Boolean;

   function GetLocalBounds (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                            return access Java.Awt.Rectangle.Typ'Class;

   function GetWindowAncestor (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return access Java.Awt.Window.Typ'Class;

   function ConvertPoint (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Point : access Standard.Java.Awt.Point.Typ'Class;
                          P3_Component : access Standard.Java.Awt.Component.Typ'Class)
                          return access Java.Awt.Point.Typ'Class;

   function ConvertPoint (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Component : access Standard.Java.Awt.Component.Typ'Class)
                          return access Java.Awt.Point.Typ'Class;

   function ConvertRectangle (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                              P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                              P3_Component : access Standard.Java.Awt.Component.Typ'Class)
                              return access Java.Awt.Rectangle.Typ'Class;

   function GetAncestorOfClass (P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return access Java.Awt.Container.Typ'Class;

   function GetAncestorNamed (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                              return access Java.Awt.Container.Typ'Class;

   function GetDeepestComponentAt (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int)
                                   return access Java.Awt.Component.Typ'Class;

   function ConvertMouseEvent (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                               P2_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class;
                               P3_Component : access Standard.Java.Awt.Component.Typ'Class)
                               return access Java.Awt.Event.MouseEvent.Typ'Class;

   procedure ConvertPointToScreen (P1_Point : access Standard.Java.Awt.Point.Typ'Class;
                                   P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure ConvertPointFromScreen (P1_Point : access Standard.Java.Awt.Point.Typ'Class;
                                     P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   function WindowForComponent (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                return access Java.Awt.Window.Typ'Class;

   function IsDescendingFrom (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                              P2_Component : access Standard.Java.Awt.Component.Typ'Class)
                              return Java.Boolean;

   function ComputeIntersection (P1_Int : Java.Int;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                                 return access Java.Awt.Rectangle.Typ'Class;

   function ComputeUnion (P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_Int : Java.Int;
                          P4_Int : Java.Int;
                          P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                          return access Java.Awt.Rectangle.Typ'Class;

   function ComputeDifference (P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                               P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return Standard.Java.Lang.Object.Ref;

   function IsLeftMouseButton (P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                               return Java.Boolean;

   function IsMiddleMouseButton (P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                                 return Java.Boolean;

   function IsRightMouseButton (P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                                return Java.Boolean;

   function ComputeStringWidth (P1_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int;

   function LayoutCompoundLabel (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                 P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Int : Java.Int;
                                 P9_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P10_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P11_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P12_Int : Java.Int)
                                 return access Java.Lang.String.Typ'Class;

   function LayoutCompoundLabel (P1_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_Icon : access Standard.Javax.Swing.Icon.Typ'Class;
                                 P4_Int : Java.Int;
                                 P5_Int : Java.Int;
                                 P6_Int : Java.Int;
                                 P7_Int : Java.Int;
                                 P8_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P9_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P10_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                 P11_Int : Java.Int)
                                 return access Java.Lang.String.Typ'Class;

   procedure PaintComponent (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P3_Container : access Standard.Java.Awt.Container.Typ'Class;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int);

   procedure PaintComponent (P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                             P2_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P3_Container : access Standard.Java.Awt.Container.Typ'Class;
                             P4_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure UpdateComponentTreeUI (P1_Component : access Standard.Java.Awt.Component.Typ'Class);

   procedure InvokeLater (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);

   procedure InvokeAndWait (P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Lang.Reflect.InvocationTargetException.Except

   function IsEventDispatchThread return Java.Boolean;

   function GetAccessibleIndexInParent (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                        return Java.Int;

   function GetAccessibleAt (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                             P2_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return access Javax.Accessibility.Accessible.Typ'Class;

   function GetAccessibleStateSet (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                   return access Javax.Accessibility.AccessibleStateSet.Typ'Class;

   function GetAccessibleChildrenCount (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                                        return Java.Int;

   function GetAccessibleChild (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                P2_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;

   function GetRootPane (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                         return access Javax.Swing.JRootPane.Typ'Class;

   function GetRoot (P1_Component : access Standard.Java.Awt.Component.Typ'Class)
                     return access Java.Awt.Component.Typ'Class;

   function ProcessKeyBindings (P1_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class)
                                return Java.Boolean;

   function NotifyAction (P1_Action : access Standard.Javax.Swing.Action.Typ'Class;
                          P2_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                          P3_KeyEvent : access Standard.Java.Awt.Event.KeyEvent.Typ'Class;
                          P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P5_Int : Java.Int)
                          return Java.Boolean;

   procedure ReplaceUIInputMap (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Int : Java.Int;
                                P3_InputMap : access Standard.Javax.Swing.InputMap.Typ'Class);

   procedure ReplaceUIActionMap (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                 P2_ActionMap : access Standard.Javax.Swing.ActionMap.Typ'Class);

   function GetUIInputMap (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                           P2_Int : Java.Int)
                           return access Javax.Swing.InputMap.Typ'Class;

   function GetUIActionMap (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                            return access Javax.Swing.ActionMap.Typ'Class;

   function CalculateInnerArea (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                                return access Java.Awt.Rectangle.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, IsRectangleContainingRectangle, "isRectangleContainingRectangle");
   pragma Import (Java, GetLocalBounds, "getLocalBounds");
   pragma Import (Java, GetWindowAncestor, "getWindowAncestor");
   pragma Import (Java, ConvertPoint, "convertPoint");
   pragma Import (Java, ConvertRectangle, "convertRectangle");
   pragma Import (Java, GetAncestorOfClass, "getAncestorOfClass");
   pragma Import (Java, GetAncestorNamed, "getAncestorNamed");
   pragma Import (Java, GetDeepestComponentAt, "getDeepestComponentAt");
   pragma Import (Java, ConvertMouseEvent, "convertMouseEvent");
   pragma Import (Java, ConvertPointToScreen, "convertPointToScreen");
   pragma Import (Java, ConvertPointFromScreen, "convertPointFromScreen");
   pragma Import (Java, WindowForComponent, "windowForComponent");
   pragma Import (Java, IsDescendingFrom, "isDescendingFrom");
   pragma Import (Java, ComputeIntersection, "computeIntersection");
   pragma Import (Java, ComputeUnion, "computeUnion");
   pragma Import (Java, ComputeDifference, "computeDifference");
   pragma Import (Java, IsLeftMouseButton, "isLeftMouseButton");
   pragma Import (Java, IsMiddleMouseButton, "isMiddleMouseButton");
   pragma Import (Java, IsRightMouseButton, "isRightMouseButton");
   pragma Import (Java, ComputeStringWidth, "computeStringWidth");
   pragma Import (Java, LayoutCompoundLabel, "layoutCompoundLabel");
   pragma Import (Java, PaintComponent, "paintComponent");
   pragma Import (Java, UpdateComponentTreeUI, "updateComponentTreeUI");
   pragma Import (Java, InvokeLater, "invokeLater");
   pragma Import (Java, InvokeAndWait, "invokeAndWait");
   pragma Import (Java, IsEventDispatchThread, "isEventDispatchThread");
   pragma Import (Java, GetAccessibleIndexInParent, "getAccessibleIndexInParent");
   pragma Import (Java, GetAccessibleAt, "getAccessibleAt");
   pragma Import (Java, GetAccessibleStateSet, "getAccessibleStateSet");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");
   pragma Import (Java, GetRootPane, "getRootPane");
   pragma Import (Java, GetRoot, "getRoot");
   pragma Import (Java, ProcessKeyBindings, "processKeyBindings");
   pragma Import (Java, NotifyAction, "notifyAction");
   pragma Import (Java, ReplaceUIInputMap, "replaceUIInputMap");
   pragma Import (Java, ReplaceUIActionMap, "replaceUIActionMap");
   pragma Import (Java, GetUIInputMap, "getUIInputMap");
   pragma Import (Java, GetUIActionMap, "getUIActionMap");
   pragma Import (Java, CalculateInnerArea, "calculateInnerArea");

end Javax.Swing.SwingUtilities;
pragma Import (Java, Javax.Swing.SwingUtilities, "javax.swing.SwingUtilities");
pragma Extensions_Allowed (Off);
