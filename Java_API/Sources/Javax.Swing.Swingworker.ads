pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Beans.PropertyChangeSupport;
limited with Java.Lang.String;
limited with Java.Util.Concurrent.TimeUnit;
limited with Java.Util.List;
limited with Javax.Swing.SwingWorker.StateValue;
with Java.Lang.Object;
with Java.Util.Concurrent.RunnableFuture;

package Javax.Swing.SwingWorker is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RunnableFuture_I : Java.Util.Concurrent.RunnableFuture.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SwingWorker (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function DoInBackground (This : access Typ)
                            return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.Exception_K.Except

   --  final
   procedure Run (This : access Typ);

   --  final  protected
   procedure Publish (This : access Typ;
                      P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   --  protected
   procedure Process (This : access Typ;
                      P1_List : access Standard.Java.Util.List.Typ'Class);

   --  protected
   procedure Done (This : access Typ);

   --  final  protected
   procedure SetProgress (This : access Typ;
                          P1_Int : Java.Int);

   --  final
   function GetProgress (This : access Typ)
                         return Java.Int;

   --  final
   procedure Execute (This : access Typ);

   --  final
   function Cancel (This : access Typ;
                    P1_Boolean : Java.Boolean)
                    return Java.Boolean;

   --  final
   function IsCancelled (This : access Typ)
                         return Java.Boolean;

   --  final
   function IsDone (This : access Typ)
                    return Java.Boolean;

   --  final
   function Get (This : access Typ)
                 return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except and
   --  Java.Util.Concurrent.ExecutionException.Except

   --  final
   function Get (This : access Typ;
                 P1_Long : Java.Long;
                 P2_TimeUnit : access Standard.Java.Util.Concurrent.TimeUnit.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.InterruptedException.Except,
   --  Java.Util.Concurrent.ExecutionException.Except and
   --  Java.Util.Concurrent.TimeoutException.Except

   --  final
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  final
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  final
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  final
   function GetPropertyChangeSupport (This : access Typ)
                                      return access Java.Beans.PropertyChangeSupport.Typ'Class;

   --  final
   function GetState (This : access Typ)
                      return access Javax.Swing.SwingWorker.StateValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SwingWorker);
   pragma Export (Java, DoInBackground, "doInBackground");
   pragma Export (Java, Run, "run");
   pragma Export (Java, Publish, "publish");
   pragma Export (Java, Process, "process");
   pragma Export (Java, Done, "done");
   pragma Export (Java, SetProgress, "setProgress");
   pragma Export (Java, GetProgress, "getProgress");
   pragma Export (Java, Execute, "execute");
   pragma Export (Java, Cancel, "cancel");
   pragma Export (Java, IsCancelled, "isCancelled");
   pragma Export (Java, IsDone, "isDone");
   pragma Export (Java, Get, "get");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Export (Java, FirePropertyChange, "firePropertyChange");
   pragma Export (Java, GetPropertyChangeSupport, "getPropertyChangeSupport");
   pragma Export (Java, GetState, "getState");

end Javax.Swing.SwingWorker;
pragma Import (Java, Javax.Swing.SwingWorker, "javax.swing.SwingWorker");
pragma Extensions_Allowed (Off);
