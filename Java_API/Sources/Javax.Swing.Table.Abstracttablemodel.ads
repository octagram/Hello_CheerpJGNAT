pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Event.TableModelEvent;
limited with Javax.Swing.Event.TableModelListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Table.TableModel;

package Javax.Swing.Table.AbstractTableModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            TableModel_I : Javax.Swing.Table.TableModel.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   function New_AbstractTableModel (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetColumnName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;

   function FindColumn (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Int;

   function GetColumnClass (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.Class.Typ'Class;

   function IsCellEditable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Boolean;

   procedure SetValueAt (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);

   procedure AddTableModelListener (This : access Typ;
                                    P1_TableModelListener : access Standard.Javax.Swing.Event.TableModelListener.Typ'Class);

   procedure RemoveTableModelListener (This : access Typ;
                                       P1_TableModelListener : access Standard.Javax.Swing.Event.TableModelListener.Typ'Class);

   function GetTableModelListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   procedure FireTableDataChanged (This : access Typ);

   procedure FireTableStructureChanged (This : access Typ);

   procedure FireTableRowsInserted (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Int : Java.Int);

   procedure FireTableRowsUpdated (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure FireTableRowsDeleted (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure FireTableCellUpdated (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int);

   procedure FireTableChanged (This : access Typ;
                               P1_TableModelEvent : access Standard.Javax.Swing.Event.TableModelEvent.Typ'Class);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractTableModel);
   pragma Import (Java, GetColumnName, "getColumnName");
   pragma Import (Java, FindColumn, "findColumn");
   pragma Import (Java, GetColumnClass, "getColumnClass");
   pragma Import (Java, IsCellEditable, "isCellEditable");
   pragma Import (Java, SetValueAt, "setValueAt");
   pragma Import (Java, AddTableModelListener, "addTableModelListener");
   pragma Import (Java, RemoveTableModelListener, "removeTableModelListener");
   pragma Import (Java, GetTableModelListeners, "getTableModelListeners");
   pragma Import (Java, FireTableDataChanged, "fireTableDataChanged");
   pragma Import (Java, FireTableStructureChanged, "fireTableStructureChanged");
   pragma Import (Java, FireTableRowsInserted, "fireTableRowsInserted");
   pragma Import (Java, FireTableRowsUpdated, "fireTableRowsUpdated");
   pragma Import (Java, FireTableRowsDeleted, "fireTableRowsDeleted");
   pragma Import (Java, FireTableCellUpdated, "fireTableCellUpdated");
   pragma Import (Java, FireTableChanged, "fireTableChanged");
   pragma Import (Java, GetListeners, "getListeners");

end Javax.Swing.Table.AbstractTableModel;
pragma Import (Java, Javax.Swing.Table.AbstractTableModel, "javax.swing.table.AbstractTableModel");
pragma Extensions_Allowed (Off);
