pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.JTable;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JLabel;
with Javax.Swing.SwingConstants;
with Javax.Swing.Table.TableCellRenderer;

package Javax.Swing.Table.DefaultTableCellRenderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TableCellRenderer_I : Javax.Swing.Table.TableCellRenderer.Ref)
    is new Javax.Swing.JLabel.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I,
                                  Accessible_I,
                                  SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTableCellRenderer (This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetTableCellRendererComponent (This : access Typ;
                                           P1_JTable : access Standard.Javax.Swing.JTable.Typ'Class;
                                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                           P3_Boolean : Java.Boolean;
                                           P4_Boolean : Java.Boolean;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int)
                                           return access Java.Awt.Component.Typ'Class;

   function IsOpaque (This : access Typ)
                      return Java.Boolean;

   procedure Invalidate (This : access Typ);

   procedure Validate (This : access Typ);

   procedure Revalidate (This : access Typ);

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int);

   procedure Repaint (This : access Typ;
                      P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure Repaint (This : access Typ);

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Boolean : Java.Boolean;
                                 P3_Boolean : Java.Boolean);

   --  protected
   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected
   NoFocusBorder : access Javax.Swing.Border.Border.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTableCellRenderer);
   pragma Import (Java, SetForeground, "setForeground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetTableCellRendererComponent, "getTableCellRendererComponent");
   pragma Import (Java, IsOpaque, "isOpaque");
   pragma Import (Java, Invalidate, "invalidate");
   pragma Import (Java, Validate, "validate");
   pragma Import (Java, Revalidate, "revalidate");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, NoFocusBorder, "noFocusBorder");

end Javax.Swing.Table.DefaultTableCellRenderer;
pragma Import (Java, Javax.Swing.Table.DefaultTableCellRenderer, "javax.swing.table.DefaultTableCellRenderer");
pragma Extensions_Allowed (Off);
