pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeEvent;
limited with Java.Lang.Class;
limited with Java.Util.Enumeration;
limited with Java.Util.EventListener;
limited with Java.Util.Vector;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Event.ListSelectionEvent;
limited with Javax.Swing.Event.TableColumnModelEvent;
limited with Javax.Swing.Event.TableColumnModelListener;
limited with Javax.Swing.ListSelectionModel;
limited with Javax.Swing.Table.TableColumn;
with Java.Beans.PropertyChangeListener;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Event.ListSelectionListener;
with Javax.Swing.Table.TableColumnModel;

package Javax.Swing.Table.DefaultTableColumnModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PropertyChangeListener_I : Java.Beans.PropertyChangeListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            ListSelectionListener_I : Javax.Swing.Event.ListSelectionListener.Ref;
            TableColumnModel_I : Javax.Swing.Table.TableColumnModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      TableColumns : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, TableColumns, "tableColumns");

      --  protected
      SelectionModel : access Javax.Swing.ListSelectionModel.Typ'Class;
      pragma Import (Java, SelectionModel, "selectionModel");

      --  protected
      ColumnMargin : Java.Int;
      pragma Import (Java, ColumnMargin, "columnMargin");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

      --  protected
      ColumnSelectionAllowed : Java.Boolean;
      pragma Import (Java, ColumnSelectionAllowed, "columnSelectionAllowed");

      --  protected
      TotalColumnWidth : Java.Int;
      pragma Import (Java, TotalColumnWidth, "totalColumnWidth");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTableColumnModel (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddColumn (This : access Typ;
                        P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class);

   procedure RemoveColumn (This : access Typ;
                           P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class);

   procedure MoveColumn (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int);

   procedure SetColumnMargin (This : access Typ;
                              P1_Int : Java.Int);

   function GetColumnCount (This : access Typ)
                            return Java.Int;

   function GetColumns (This : access Typ)
                        return access Java.Util.Enumeration.Typ'Class;

   function GetColumnIndex (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return Java.Int;

   function GetColumn (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Swing.Table.TableColumn.Typ'Class;

   function GetColumnMargin (This : access Typ)
                             return Java.Int;

   function GetColumnIndexAtX (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int;

   function GetTotalColumnWidth (This : access Typ)
                                 return Java.Int;

   procedure SetSelectionModel (This : access Typ;
                                P1_ListSelectionModel : access Standard.Javax.Swing.ListSelectionModel.Typ'Class);

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.ListSelectionModel.Typ'Class;

   procedure SetColumnSelectionAllowed (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function GetColumnSelectionAllowed (This : access Typ)
                                       return Java.Boolean;

   function GetSelectedColumns (This : access Typ)
                                return Java.Int_Arr;

   function GetSelectedColumnCount (This : access Typ)
                                    return Java.Int;

   procedure AddColumnModelListener (This : access Typ;
                                     P1_TableColumnModelListener : access Standard.Javax.Swing.Event.TableColumnModelListener.Typ'Class);

   procedure RemoveColumnModelListener (This : access Typ;
                                        P1_TableColumnModelListener : access Standard.Javax.Swing.Event.TableColumnModelListener.Typ'Class);

   function GetColumnModelListeners (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireColumnAdded (This : access Typ;
                              P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   --  protected
   procedure FireColumnRemoved (This : access Typ;
                                P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   --  protected
   procedure FireColumnMoved (This : access Typ;
                              P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   --  protected
   procedure FireColumnSelectionChanged (This : access Typ;
                                         P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class);

   --  protected
   procedure FireColumnMarginChanged (This : access Typ);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   procedure PropertyChange (This : access Typ;
                             P1_PropertyChangeEvent : access Standard.Java.Beans.PropertyChangeEvent.Typ'Class);

   procedure ValueChanged (This : access Typ;
                           P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class);

   --  protected
   function CreateSelectionModel (This : access Typ)
                                  return access Javax.Swing.ListSelectionModel.Typ'Class;

   --  protected
   procedure RecalcWidthCache (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTableColumnModel);
   pragma Import (Java, AddColumn, "addColumn");
   pragma Import (Java, RemoveColumn, "removeColumn");
   pragma Import (Java, MoveColumn, "moveColumn");
   pragma Import (Java, SetColumnMargin, "setColumnMargin");
   pragma Import (Java, GetColumnCount, "getColumnCount");
   pragma Import (Java, GetColumns, "getColumns");
   pragma Import (Java, GetColumnIndex, "getColumnIndex");
   pragma Import (Java, GetColumn, "getColumn");
   pragma Import (Java, GetColumnMargin, "getColumnMargin");
   pragma Import (Java, GetColumnIndexAtX, "getColumnIndexAtX");
   pragma Import (Java, GetTotalColumnWidth, "getTotalColumnWidth");
   pragma Import (Java, SetSelectionModel, "setSelectionModel");
   pragma Import (Java, GetSelectionModel, "getSelectionModel");
   pragma Import (Java, SetColumnSelectionAllowed, "setColumnSelectionAllowed");
   pragma Import (Java, GetColumnSelectionAllowed, "getColumnSelectionAllowed");
   pragma Import (Java, GetSelectedColumns, "getSelectedColumns");
   pragma Import (Java, GetSelectedColumnCount, "getSelectedColumnCount");
   pragma Import (Java, AddColumnModelListener, "addColumnModelListener");
   pragma Import (Java, RemoveColumnModelListener, "removeColumnModelListener");
   pragma Import (Java, GetColumnModelListeners, "getColumnModelListeners");
   pragma Import (Java, FireColumnAdded, "fireColumnAdded");
   pragma Import (Java, FireColumnRemoved, "fireColumnRemoved");
   pragma Import (Java, FireColumnMoved, "fireColumnMoved");
   pragma Import (Java, FireColumnSelectionChanged, "fireColumnSelectionChanged");
   pragma Import (Java, FireColumnMarginChanged, "fireColumnMarginChanged");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, PropertyChange, "propertyChange");
   pragma Import (Java, ValueChanged, "valueChanged");
   pragma Import (Java, CreateSelectionModel, "createSelectionModel");
   pragma Import (Java, RecalcWidthCache, "recalcWidthCache");

end Javax.Swing.Table.DefaultTableColumnModel;
pragma Import (Java, Javax.Swing.Table.DefaultTableColumnModel, "javax.swing.table.DefaultTableColumnModel");
pragma Extensions_Allowed (Off);
