pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Swing.Event.TableModelEvent;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Table.AbstractTableModel;
with Javax.Swing.Table.TableModel;

package Javax.Swing.Table.DefaultTableModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            TableModel_I : Javax.Swing.Table.TableModel.Ref)
    is new Javax.Swing.Table.AbstractTableModel.Typ(Serializable_I,
                                                    TableModel_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      DataVector : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, DataVector, "dataVector");

      --  protected
      ColumnIdentifiers : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, ColumnIdentifiers, "columnIdentifiers");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTableModel (This : Ref := null)
                                   return Ref;

   function New_DefaultTableModel (P1_Int : Java.Int;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_DefaultTableModel (P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_DefaultTableModel (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                   P2_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_DefaultTableModel (P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                                   P2_Vector : access Standard.Java.Util.Vector.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_DefaultTableModel (P1_Object_Arr_2 : access Java.Lang.Object.Arr_2_Obj;
                                   P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDataVector (This : access Typ)
                           return access Java.Util.Vector.Typ'Class;

   procedure SetDataVector (This : access Typ;
                            P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                            P2_Vector : access Standard.Java.Util.Vector.Typ'Class);

   procedure SetDataVector (This : access Typ;
                            P1_Object_Arr_2 : access Java.Lang.Object.Arr_2_Obj;
                            P2_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure NewDataAvailable (This : access Typ;
                               P1_TableModelEvent : access Standard.Javax.Swing.Event.TableModelEvent.Typ'Class);

   procedure NewRowsAdded (This : access Typ;
                           P1_TableModelEvent : access Standard.Javax.Swing.Event.TableModelEvent.Typ'Class);

   procedure RowsRemoved (This : access Typ;
                          P1_TableModelEvent : access Standard.Javax.Swing.Event.TableModelEvent.Typ'Class);

   procedure SetNumRows (This : access Typ;
                         P1_Int : Java.Int);

   procedure SetRowCount (This : access Typ;
                          P1_Int : Java.Int);

   procedure AddRow (This : access Typ;
                     P1_Vector : access Standard.Java.Util.Vector.Typ'Class);

   procedure AddRow (This : access Typ;
                     P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure InsertRow (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Vector : access Standard.Java.Util.Vector.Typ'Class);

   procedure InsertRow (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure MoveRow (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int);

   procedure RemoveRow (This : access Typ;
                        P1_Int : Java.Int);

   procedure SetColumnIdentifiers (This : access Typ;
                                   P1_Vector : access Standard.Java.Util.Vector.Typ'Class);

   procedure SetColumnIdentifiers (This : access Typ;
                                   P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure SetColumnCount (This : access Typ;
                             P1_Int : Java.Int);

   procedure AddColumn (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddColumn (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Vector : access Standard.Java.Util.Vector.Typ'Class);

   procedure AddColumn (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function GetRowCount (This : access Typ)
                         return Java.Int;

   function GetColumnCount (This : access Typ)
                            return Java.Int;

   function GetColumnName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;

   function IsCellEditable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Boolean;

   function GetValueAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return access Java.Lang.Object.Typ'Class;

   procedure SetValueAt (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);

   --  protected
   function ConvertToVector (P1_Object_Arr : access Java.Lang.Object.Arr_Obj)
                             return access Java.Util.Vector.Typ'Class;

   --  protected
   function ConvertToVector (P1_Object_Arr_2 : access Java.Lang.Object.Arr_2_Obj)
                             return access Java.Util.Vector.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTableModel);
   pragma Import (Java, GetDataVector, "getDataVector");
   pragma Import (Java, SetDataVector, "setDataVector");
   pragma Import (Java, NewDataAvailable, "newDataAvailable");
   pragma Import (Java, NewRowsAdded, "newRowsAdded");
   pragma Import (Java, RowsRemoved, "rowsRemoved");
   pragma Import (Java, SetNumRows, "setNumRows");
   pragma Import (Java, SetRowCount, "setRowCount");
   pragma Import (Java, AddRow, "addRow");
   pragma Import (Java, InsertRow, "insertRow");
   pragma Import (Java, MoveRow, "moveRow");
   pragma Import (Java, RemoveRow, "removeRow");
   pragma Import (Java, SetColumnIdentifiers, "setColumnIdentifiers");
   pragma Import (Java, SetColumnCount, "setColumnCount");
   pragma Import (Java, AddColumn, "addColumn");
   pragma Import (Java, GetRowCount, "getRowCount");
   pragma Import (Java, GetColumnCount, "getColumnCount");
   pragma Import (Java, GetColumnName, "getColumnName");
   pragma Import (Java, IsCellEditable, "isCellEditable");
   pragma Import (Java, GetValueAt, "getValueAt");
   pragma Import (Java, SetValueAt, "setValueAt");
   pragma Import (Java, ConvertToVector, "convertToVector");

end Javax.Swing.Table.DefaultTableModel;
pragma Import (Java, Javax.Swing.Table.DefaultTableModel, "javax.swing.table.DefaultTableModel");
pragma Extensions_Allowed (Off);
