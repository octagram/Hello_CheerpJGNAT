pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ListSelectionEvent;
limited with Javax.Swing.Event.TableColumnModelEvent;
limited with Javax.Swing.JTable;
limited with Javax.Swing.Plaf.TableHeaderUI;
limited with Javax.Swing.Table.TableCellRenderer;
limited with Javax.Swing.Table.TableColumn;
limited with Javax.Swing.Table.TableColumnModel;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Event.TableColumnModelListener;
with Javax.Swing.JComponent;

package Javax.Swing.Table.JTableHeader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            TableColumnModelListener_I : Javax.Swing.Event.TableColumnModelListener.Ref)
    is new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Table : access Javax.Swing.JTable.Typ'Class;
      pragma Import (Java, Table, "table");

      --  protected
      ColumnModel : access Javax.Swing.Table.TableColumnModel.Typ'Class;
      pragma Import (Java, ColumnModel, "columnModel");

      --  protected
      ReorderingAllowed : Java.Boolean;
      pragma Import (Java, ReorderingAllowed, "reorderingAllowed");

      --  protected
      ResizingAllowed : Java.Boolean;
      pragma Import (Java, ResizingAllowed, "resizingAllowed");

      --  protected
      UpdateTableInRealTime : Java.Boolean;
      pragma Import (Java, UpdateTableInRealTime, "updateTableInRealTime");

      --  protected
      ResizingColumn : access Javax.Swing.Table.TableColumn.Typ'Class;
      pragma Import (Java, ResizingColumn, "resizingColumn");

      --  protected
      DraggedColumn : access Javax.Swing.Table.TableColumn.Typ'Class;
      pragma Import (Java, DraggedColumn, "draggedColumn");

      --  protected
      DraggedDistance : Java.Int;
      pragma Import (Java, DraggedDistance, "draggedDistance");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JTableHeader (This : Ref := null)
                              return Ref;

   function New_JTableHeader (P1_TableColumnModel : access Standard.Javax.Swing.Table.TableColumnModel.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetTable (This : access Typ;
                       P1_JTable : access Standard.Javax.Swing.JTable.Typ'Class);

   function GetTable (This : access Typ)
                      return access Javax.Swing.JTable.Typ'Class;

   procedure SetReorderingAllowed (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function GetReorderingAllowed (This : access Typ)
                                  return Java.Boolean;

   procedure SetResizingAllowed (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   function GetResizingAllowed (This : access Typ)
                                return Java.Boolean;

   function GetDraggedColumn (This : access Typ)
                              return access Javax.Swing.Table.TableColumn.Typ'Class;

   function GetDraggedDistance (This : access Typ)
                                return Java.Int;

   function GetResizingColumn (This : access Typ)
                               return access Javax.Swing.Table.TableColumn.Typ'Class;

   procedure SetUpdateTableInRealTime (This : access Typ;
                                       P1_Boolean : Java.Boolean);

   function GetUpdateTableInRealTime (This : access Typ)
                                      return Java.Boolean;

   procedure SetDefaultRenderer (This : access Typ;
                                 P1_TableCellRenderer : access Standard.Javax.Swing.Table.TableCellRenderer.Typ'Class);

   function GetDefaultRenderer (This : access Typ)
                                return access Javax.Swing.Table.TableCellRenderer.Typ'Class;

   function ColumnAtPoint (This : access Typ;
                           P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                           return Java.Int;

   function GetHeaderRect (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Awt.Rectangle.Typ'Class;

   function GetToolTipText (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.TableHeaderUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_TableHeaderUI : access Standard.Javax.Swing.Plaf.TableHeaderUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   function GetUIClassID (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure SetColumnModel (This : access Typ;
                             P1_TableColumnModel : access Standard.Javax.Swing.Table.TableColumnModel.Typ'Class);

   function GetColumnModel (This : access Typ)
                            return access Javax.Swing.Table.TableColumnModel.Typ'Class;

   procedure ColumnAdded (This : access Typ;
                          P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   procedure ColumnRemoved (This : access Typ;
                            P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   procedure ColumnMoved (This : access Typ;
                          P1_TableColumnModelEvent : access Standard.Javax.Swing.Event.TableColumnModelEvent.Typ'Class);

   procedure ColumnMarginChanged (This : access Typ;
                                  P1_ChangeEvent : access Standard.Javax.Swing.Event.ChangeEvent.Typ'Class);

   procedure ColumnSelectionChanged (This : access Typ;
                                     P1_ListSelectionEvent : access Standard.Javax.Swing.Event.ListSelectionEvent.Typ'Class);

   --  protected
   function CreateDefaultColumnModel (This : access Typ)
                                      return access Javax.Swing.Table.TableColumnModel.Typ'Class;

   --  protected
   function CreateDefaultRenderer (This : access Typ)
                                   return access Javax.Swing.Table.TableCellRenderer.Typ'Class;

   --  protected
   procedure InitializeLocalVars (This : access Typ);

   procedure ResizeAndRepaint (This : access Typ);

   procedure SetDraggedColumn (This : access Typ;
                               P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class);

   procedure SetDraggedDistance (This : access Typ;
                                 P1_Int : Java.Int);

   procedure SetResizingColumn (This : access Typ;
                                P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class);

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JTableHeader);
   pragma Import (Java, SetTable, "setTable");
   pragma Import (Java, GetTable, "getTable");
   pragma Import (Java, SetReorderingAllowed, "setReorderingAllowed");
   pragma Import (Java, GetReorderingAllowed, "getReorderingAllowed");
   pragma Import (Java, SetResizingAllowed, "setResizingAllowed");
   pragma Import (Java, GetResizingAllowed, "getResizingAllowed");
   pragma Import (Java, GetDraggedColumn, "getDraggedColumn");
   pragma Import (Java, GetDraggedDistance, "getDraggedDistance");
   pragma Import (Java, GetResizingColumn, "getResizingColumn");
   pragma Import (Java, SetUpdateTableInRealTime, "setUpdateTableInRealTime");
   pragma Import (Java, GetUpdateTableInRealTime, "getUpdateTableInRealTime");
   pragma Import (Java, SetDefaultRenderer, "setDefaultRenderer");
   pragma Import (Java, GetDefaultRenderer, "getDefaultRenderer");
   pragma Import (Java, ColumnAtPoint, "columnAtPoint");
   pragma Import (Java, GetHeaderRect, "getHeaderRect");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, GetUIClassID, "getUIClassID");
   pragma Import (Java, SetColumnModel, "setColumnModel");
   pragma Import (Java, GetColumnModel, "getColumnModel");
   pragma Import (Java, ColumnAdded, "columnAdded");
   pragma Import (Java, ColumnRemoved, "columnRemoved");
   pragma Import (Java, ColumnMoved, "columnMoved");
   pragma Import (Java, ColumnMarginChanged, "columnMarginChanged");
   pragma Import (Java, ColumnSelectionChanged, "columnSelectionChanged");
   pragma Import (Java, CreateDefaultColumnModel, "createDefaultColumnModel");
   pragma Import (Java, CreateDefaultRenderer, "createDefaultRenderer");
   pragma Import (Java, InitializeLocalVars, "initializeLocalVars");
   pragma Import (Java, ResizeAndRepaint, "resizeAndRepaint");
   pragma Import (Java, SetDraggedColumn, "setDraggedColumn");
   pragma Import (Java, SetDraggedDistance, "setDraggedDistance");
   pragma Import (Java, SetResizingColumn, "setResizingColumn");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.Table.JTableHeader;
pragma Import (Java, Javax.Swing.Table.JTableHeader, "javax.swing.table.JTableHeader");
pragma Extensions_Allowed (Off);
