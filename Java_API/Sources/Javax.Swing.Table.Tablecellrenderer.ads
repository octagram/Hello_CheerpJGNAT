pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.JTable;
with Java.Lang.Object;

package Javax.Swing.Table.TableCellRenderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTableCellRendererComponent (This : access Typ;
                                           P1_JTable : access Standard.Javax.Swing.JTable.Typ'Class;
                                           P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                           P3_Boolean : Java.Boolean;
                                           P4_Boolean : Java.Boolean;
                                           P5_Int : Java.Int;
                                           P6_Int : Java.Int)
                                           return access Java.Awt.Component.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTableCellRendererComponent, "getTableCellRendererComponent");

end Javax.Swing.Table.TableCellRenderer;
pragma Import (Java, Javax.Swing.Table.TableCellRenderer, "javax.swing.table.TableCellRenderer");
pragma Extensions_Allowed (Off);
