pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Javax.Swing.Table.TableCellEditor;
limited with Javax.Swing.Table.TableCellRenderer;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Table.TableColumn is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ModelIndex : Java.Int;
      pragma Import (Java, ModelIndex, "modelIndex");

      --  protected
      Identifier : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Identifier, "identifier");

      --  protected
      Width : Java.Int;
      pragma Import (Java, Width, "width");

      --  protected
      MinWidth : Java.Int;
      pragma Import (Java, MinWidth, "minWidth");

      --  protected
      MaxWidth : Java.Int;
      pragma Import (Java, MaxWidth, "maxWidth");

      --  protected
      HeaderRenderer : access Javax.Swing.Table.TableCellRenderer.Typ'Class;
      pragma Import (Java, HeaderRenderer, "headerRenderer");

      --  protected
      HeaderValue : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, HeaderValue, "headerValue");

      --  protected
      CellRenderer : access Javax.Swing.Table.TableCellRenderer.Typ'Class;
      pragma Import (Java, CellRenderer, "cellRenderer");

      --  protected
      CellEditor : access Javax.Swing.Table.TableCellEditor.Typ'Class;
      pragma Import (Java, CellEditor, "cellEditor");

      --  protected
      IsResizable : Java.Boolean;
      pragma Import (Java, IsResizable, "isResizable");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TableColumn (This : Ref := null)
                             return Ref;

   function New_TableColumn (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_TableColumn (P1_Int : Java.Int;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_TableColumn (P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_TableCellRenderer : access Standard.Javax.Swing.Table.TableCellRenderer.Typ'Class;
                             P4_TableCellEditor : access Standard.Javax.Swing.Table.TableCellEditor.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetModelIndex (This : access Typ;
                            P1_Int : Java.Int);

   function GetModelIndex (This : access Typ)
                           return Java.Int;

   procedure SetIdentifier (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetIdentifier (This : access Typ)
                           return access Java.Lang.Object.Typ'Class;

   procedure SetHeaderValue (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetHeaderValue (This : access Typ)
                            return access Java.Lang.Object.Typ'Class;

   procedure SetHeaderRenderer (This : access Typ;
                                P1_TableCellRenderer : access Standard.Javax.Swing.Table.TableCellRenderer.Typ'Class);

   function GetHeaderRenderer (This : access Typ)
                               return access Javax.Swing.Table.TableCellRenderer.Typ'Class;

   procedure SetCellRenderer (This : access Typ;
                              P1_TableCellRenderer : access Standard.Javax.Swing.Table.TableCellRenderer.Typ'Class);

   function GetCellRenderer (This : access Typ)
                             return access Javax.Swing.Table.TableCellRenderer.Typ'Class;

   procedure SetCellEditor (This : access Typ;
                            P1_TableCellEditor : access Standard.Javax.Swing.Table.TableCellEditor.Typ'Class);

   function GetCellEditor (This : access Typ)
                           return access Javax.Swing.Table.TableCellEditor.Typ'Class;

   procedure SetWidth (This : access Typ;
                       P1_Int : Java.Int);

   function GetWidth (This : access Typ)
                      return Java.Int;

   procedure SetPreferredWidth (This : access Typ;
                                P1_Int : Java.Int);

   function GetPreferredWidth (This : access Typ)
                               return Java.Int;

   procedure SetMinWidth (This : access Typ;
                          P1_Int : Java.Int);

   function GetMinWidth (This : access Typ)
                         return Java.Int;

   procedure SetMaxWidth (This : access Typ;
                          P1_Int : Java.Int);

   function GetMaxWidth (This : access Typ)
                         return Java.Int;

   procedure SetResizable (This : access Typ;
                           P1_Boolean : Java.Boolean);

   function GetResizable (This : access Typ)
                          return Java.Boolean;

   procedure SizeWidthToFit (This : access Typ);

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   function CreateDefaultHeaderRenderer (This : access Typ)
                                         return access Javax.Swing.Table.TableCellRenderer.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   COLUMN_WIDTH_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   HEADER_VALUE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   HEADER_RENDERER_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   CELL_RENDERER_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableColumn);
   pragma Import (Java, SetModelIndex, "setModelIndex");
   pragma Import (Java, GetModelIndex, "getModelIndex");
   pragma Import (Java, SetIdentifier, "setIdentifier");
   pragma Import (Java, GetIdentifier, "getIdentifier");
   pragma Import (Java, SetHeaderValue, "setHeaderValue");
   pragma Import (Java, GetHeaderValue, "getHeaderValue");
   pragma Import (Java, SetHeaderRenderer, "setHeaderRenderer");
   pragma Import (Java, GetHeaderRenderer, "getHeaderRenderer");
   pragma Import (Java, SetCellRenderer, "setCellRenderer");
   pragma Import (Java, GetCellRenderer, "getCellRenderer");
   pragma Import (Java, SetCellEditor, "setCellEditor");
   pragma Import (Java, GetCellEditor, "getCellEditor");
   pragma Import (Java, SetWidth, "setWidth");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, SetPreferredWidth, "setPreferredWidth");
   pragma Import (Java, GetPreferredWidth, "getPreferredWidth");
   pragma Import (Java, SetMinWidth, "setMinWidth");
   pragma Import (Java, GetMinWidth, "getMinWidth");
   pragma Import (Java, SetMaxWidth, "setMaxWidth");
   pragma Import (Java, GetMaxWidth, "getMaxWidth");
   pragma Import (Java, SetResizable, "setResizable");
   pragma Import (Java, GetResizable, "getResizable");
   pragma Import (Java, SizeWidthToFit, "sizeWidthToFit");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Import (Java, CreateDefaultHeaderRenderer, "createDefaultHeaderRenderer");
   pragma Import (Java, COLUMN_WIDTH_PROPERTY, "COLUMN_WIDTH_PROPERTY");
   pragma Import (Java, HEADER_VALUE_PROPERTY, "HEADER_VALUE_PROPERTY");
   pragma Import (Java, HEADER_RENDERER_PROPERTY, "HEADER_RENDERER_PROPERTY");
   pragma Import (Java, CELL_RENDERER_PROPERTY, "CELL_RENDERER_PROPERTY");

end Javax.Swing.Table.TableColumn;
pragma Import (Java, Javax.Swing.Table.TableColumn, "javax.swing.table.TableColumn");
pragma Extensions_Allowed (Off);
