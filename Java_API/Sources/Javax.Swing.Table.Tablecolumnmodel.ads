pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
limited with Javax.Swing.Event.TableColumnModelListener;
limited with Javax.Swing.ListSelectionModel;
limited with Javax.Swing.Table.TableColumn;
with Java.Lang.Object;

package Javax.Swing.Table.TableColumnModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddColumn (This : access Typ;
                        P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class) is abstract;

   procedure RemoveColumn (This : access Typ;
                           P1_TableColumn : access Standard.Javax.Swing.Table.TableColumn.Typ'Class) is abstract;

   procedure MoveColumn (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int) is abstract;

   procedure SetColumnMargin (This : access Typ;
                              P1_Int : Java.Int) is abstract;

   function GetColumnCount (This : access Typ)
                            return Java.Int is abstract;

   function GetColumns (This : access Typ)
                        return access Java.Util.Enumeration.Typ'Class is abstract;

   function GetColumnIndex (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return Java.Int is abstract;

   function GetColumn (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Swing.Table.TableColumn.Typ'Class is abstract;

   function GetColumnMargin (This : access Typ)
                             return Java.Int is abstract;

   function GetColumnIndexAtX (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Int is abstract;

   function GetTotalColumnWidth (This : access Typ)
                                 return Java.Int is abstract;

   procedure SetColumnSelectionAllowed (This : access Typ;
                                        P1_Boolean : Java.Boolean) is abstract;

   function GetColumnSelectionAllowed (This : access Typ)
                                       return Java.Boolean is abstract;

   function GetSelectedColumns (This : access Typ)
                                return Java.Int_Arr is abstract;

   function GetSelectedColumnCount (This : access Typ)
                                    return Java.Int is abstract;

   procedure SetSelectionModel (This : access Typ;
                                P1_ListSelectionModel : access Standard.Javax.Swing.ListSelectionModel.Typ'Class) is abstract;

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.ListSelectionModel.Typ'Class is abstract;

   procedure AddColumnModelListener (This : access Typ;
                                     P1_TableColumnModelListener : access Standard.Javax.Swing.Event.TableColumnModelListener.Typ'Class) is abstract;

   procedure RemoveColumnModelListener (This : access Typ;
                                        P1_TableColumnModelListener : access Standard.Javax.Swing.Event.TableColumnModelListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddColumn, "addColumn");
   pragma Export (Java, RemoveColumn, "removeColumn");
   pragma Export (Java, MoveColumn, "moveColumn");
   pragma Export (Java, SetColumnMargin, "setColumnMargin");
   pragma Export (Java, GetColumnCount, "getColumnCount");
   pragma Export (Java, GetColumns, "getColumns");
   pragma Export (Java, GetColumnIndex, "getColumnIndex");
   pragma Export (Java, GetColumn, "getColumn");
   pragma Export (Java, GetColumnMargin, "getColumnMargin");
   pragma Export (Java, GetColumnIndexAtX, "getColumnIndexAtX");
   pragma Export (Java, GetTotalColumnWidth, "getTotalColumnWidth");
   pragma Export (Java, SetColumnSelectionAllowed, "setColumnSelectionAllowed");
   pragma Export (Java, GetColumnSelectionAllowed, "getColumnSelectionAllowed");
   pragma Export (Java, GetSelectedColumns, "getSelectedColumns");
   pragma Export (Java, GetSelectedColumnCount, "getSelectedColumnCount");
   pragma Export (Java, SetSelectionModel, "setSelectionModel");
   pragma Export (Java, GetSelectionModel, "getSelectionModel");
   pragma Export (Java, AddColumnModelListener, "addColumnModelListener");
   pragma Export (Java, RemoveColumnModelListener, "removeColumnModelListener");

end Javax.Swing.Table.TableColumnModel;
pragma Import (Java, Javax.Swing.Table.TableColumnModel, "javax.swing.table.TableColumnModel");
pragma Extensions_Allowed (Off);
