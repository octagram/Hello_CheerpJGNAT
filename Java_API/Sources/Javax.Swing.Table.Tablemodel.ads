pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Swing.Event.TableModelListener;
with Java.Lang.Object;

package Javax.Swing.Table.TableModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRowCount (This : access Typ)
                         return Java.Int is abstract;

   function GetColumnCount (This : access Typ)
                            return Java.Int is abstract;

   function GetColumnName (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetColumnClass (This : access Typ;
                            P1_Int : Java.Int)
                            return access Java.Lang.Class.Typ'Class is abstract;

   function IsCellEditable (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Boolean is abstract;

   function GetValueAt (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return access Java.Lang.Object.Typ'Class is abstract;

   procedure SetValueAt (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;

   procedure AddTableModelListener (This : access Typ;
                                    P1_TableModelListener : access Standard.Javax.Swing.Event.TableModelListener.Typ'Class) is abstract;

   procedure RemoveTableModelListener (This : access Typ;
                                       P1_TableModelListener : access Standard.Javax.Swing.Event.TableModelListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRowCount, "getRowCount");
   pragma Export (Java, GetColumnCount, "getColumnCount");
   pragma Export (Java, GetColumnName, "getColumnName");
   pragma Export (Java, GetColumnClass, "getColumnClass");
   pragma Export (Java, IsCellEditable, "isCellEditable");
   pragma Export (Java, GetValueAt, "getValueAt");
   pragma Export (Java, SetValueAt, "setValueAt");
   pragma Export (Java, AddTableModelListener, "addTableModelListener");
   pragma Export (Java, RemoveTableModelListener, "removeTableModelListener");

end Javax.Swing.Table.TableModel;
pragma Import (Java, Javax.Swing.Table.TableModel, "javax.swing.table.TableModel");
pragma Extensions_Allowed (Off);
