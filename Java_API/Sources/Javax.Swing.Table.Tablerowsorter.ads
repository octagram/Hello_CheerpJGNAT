pragma Extensions_Allowed (On);
limited with Java.Util.Comparator;
limited with Javax.Swing.Table.TableModel;
limited with Javax.Swing.Table.TableStringConverter;
with Java.Lang.Object;
with Javax.Swing.DefaultRowSorter;

package Javax.Swing.Table.TableRowSorter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.DefaultRowSorter.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TableRowSorter (This : Ref := null)
                                return Ref;

   function New_TableRowSorter (P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetModel (This : access Typ;
                       P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class);

   procedure SetStringConverter (This : access Typ;
                                 P1_TableStringConverter : access Standard.Javax.Swing.Table.TableStringConverter.Typ'Class);

   function GetStringConverter (This : access Typ)
                                return access Javax.Swing.Table.TableStringConverter.Typ'Class;

   function GetComparator (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Util.Comparator.Typ'Class;

   --  protected
   function UseToString (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableRowSorter);
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, SetStringConverter, "setStringConverter");
   pragma Import (Java, GetStringConverter, "getStringConverter");
   pragma Import (Java, GetComparator, "getComparator");
   pragma Import (Java, UseToString, "useToString");

end Javax.Swing.Table.TableRowSorter;
pragma Import (Java, Javax.Swing.Table.TableRowSorter, "javax.swing.table.TableRowSorter");
pragma Extensions_Allowed (Off);
