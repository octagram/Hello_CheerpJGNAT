pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Table.TableModel;
with Java.Lang.Object;

package Javax.Swing.Table.TableStringConverter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_TableStringConverter (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ;
                      P1_TableModel : access Standard.Javax.Swing.Table.TableModel.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableStringConverter);
   pragma Export (Java, ToString, "toString");

end Javax.Swing.Table.TableStringConverter;
pragma Import (Java, Javax.Swing.Table.TableStringConverter, "javax.swing.table.TableStringConverter");
pragma Extensions_Allowed (Off);
