pragma Extensions_Allowed (On);
limited with Java.Io.PrintStream;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Text.AttributeSet;
with Javax.Swing.Text.Document;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Element;
with Javax.Swing.Text.MutableAttributeSet;
with Javax.Swing.Tree.TreeNode;

package Javax.Swing.Text.AbstractDocument.AbstractElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Element_I : Javax.Swing.Text.Element.Ref;
            MutableAttributeSet_I : Javax.Swing.Text.MutableAttributeSet.Ref;
            TreeNode_I : Javax.Swing.Tree.TreeNode.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AbstractElement (P1_AbstractDocument : access Standard.Javax.Swing.Text.AbstractDocument.Typ'Class;
                                 P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                 P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Dump (This : access Typ;
                   P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class;
                   P2_Int : Java.Int);

   function GetAttributeCount (This : access Typ)
                               return Java.Int;

   function IsDefined (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   function IsEqual (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return Java.Boolean;

   function CopyAttributes (This : access Typ)
                            return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function GetAttributeNames (This : access Typ)
                               return access Java.Util.Enumeration.Typ'Class;

   function ContainsAttribute (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean;

   function ContainsAttributes (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                return Java.Boolean;

   function GetResolveParent (This : access Typ)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure AddAttribute (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddAttributes (This : access Typ;
                            P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   procedure RemoveAttribute (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveAttributes (This : access Typ;
                               P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class);

   procedure RemoveAttributes (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   procedure SetResolveParent (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class;

   function GetParentElement (This : access Typ)
                              return access Javax.Swing.Text.Element.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetStartOffset (This : access Typ)
                            return Java.Int is abstract;

   function GetEndOffset (This : access Typ)
                          return Java.Int is abstract;

   function GetElement (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Text.Element.Typ'Class is abstract;

   function GetElementCount (This : access Typ)
                             return Java.Int is abstract;

   function GetElementIndex (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int is abstract;

   function IsLeaf (This : access Typ)
                    return Java.Boolean is abstract;

   function GetChildAt (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function GetChildCount (This : access Typ)
                           return Java.Int;

   function GetParent (This : access Typ)
                       return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function GetIndex (This : access Typ;
                      P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                      return Java.Int;

   function GetAllowsChildren (This : access Typ)
                               return Java.Boolean is abstract;

   function Children (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractElement);
   pragma Import (Java, Dump, "dump");
   pragma Import (Java, GetAttributeCount, "getAttributeCount");
   pragma Import (Java, IsDefined, "isDefined");
   pragma Import (Java, IsEqual, "isEqual");
   pragma Import (Java, CopyAttributes, "copyAttributes");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetAttributeNames, "getAttributeNames");
   pragma Import (Java, ContainsAttribute, "containsAttribute");
   pragma Import (Java, ContainsAttributes, "containsAttributes");
   pragma Import (Java, GetResolveParent, "getResolveParent");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, AddAttributes, "addAttributes");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, RemoveAttributes, "removeAttributes");
   pragma Import (Java, SetResolveParent, "setResolveParent");
   pragma Import (Java, GetDocument, "getDocument");
   pragma Import (Java, GetParentElement, "getParentElement");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetName, "getName");
   pragma Export (Java, GetStartOffset, "getStartOffset");
   pragma Export (Java, GetEndOffset, "getEndOffset");
   pragma Export (Java, GetElement, "getElement");
   pragma Export (Java, GetElementCount, "getElementCount");
   pragma Export (Java, GetElementIndex, "getElementIndex");
   pragma Export (Java, IsLeaf, "isLeaf");
   pragma Import (Java, GetChildAt, "getChildAt");
   pragma Import (Java, GetChildCount, "getChildCount");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Export (Java, GetAllowsChildren, "getAllowsChildren");
   pragma Export (Java, Children, "children");

end Javax.Swing.Text.AbstractDocument.AbstractElement;
pragma Import (Java, Javax.Swing.Text.AbstractDocument.AbstractElement, "javax.swing.text.AbstractDocument$AbstractElement");
pragma Extensions_Allowed (Off);
