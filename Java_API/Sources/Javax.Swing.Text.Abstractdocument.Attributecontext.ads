pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
limited with Javax.Swing.Text.AttributeSet;
with Java.Lang.Object;

package Javax.Swing.Text.AbstractDocument.AttributeContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddAttribute (This : access Typ;
                          P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function AddAttributes (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                           P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function RemoveAttribute (This : access Typ;
                             P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function RemoveAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                              P2_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function RemoveAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                              P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function GetEmptySet (This : access Typ)
                         return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   procedure Reclaim (This : access Typ;
                      P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddAttribute, "addAttribute");
   pragma Export (Java, AddAttributes, "addAttributes");
   pragma Export (Java, RemoveAttribute, "removeAttribute");
   pragma Export (Java, RemoveAttributes, "removeAttributes");
   pragma Export (Java, GetEmptySet, "getEmptySet");
   pragma Export (Java, Reclaim, "reclaim");

end Javax.Swing.Text.AbstractDocument.AttributeContext;
pragma Import (Java, Javax.Swing.Text.AbstractDocument.AttributeContext, "javax.swing.text.AbstractDocument$AttributeContext");
pragma Extensions_Allowed (Off);
