pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.Position;
limited with Javax.Swing.Text.Segment;
limited with Javax.Swing.Undo.UndoableEdit;
with Java.Lang.Object;

package Javax.Swing.Text.AbstractDocument.Content is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreatePosition (This : access Typ;
                            P1_Int : Java.Int)
                            return access Javax.Swing.Text.Position.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function Length (This : access Typ)
                    return Java.Int is abstract;

   function InsertString (This : access Typ;
                          P1_Int : Java.Int;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.Undo.UndoableEdit.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function Remove (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Javax.Swing.Undo.UndoableEdit.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetString (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure GetChars (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreatePosition, "createPosition");
   pragma Export (Java, Length, "length");
   pragma Export (Java, InsertString, "insertString");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, GetString, "getString");
   pragma Export (Java, GetChars, "getChars");

end Javax.Swing.Text.AbstractDocument.Content;
pragma Import (Java, Javax.Swing.Text.AbstractDocument.Content, "javax.swing.text.AbstractDocument$Content");
pragma Extensions_Allowed (Off);
