pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Event.DocumentEvent.ElementChange;
limited with Javax.Swing.Event.DocumentEvent.EventType;
with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Element;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Event.DocumentEvent;
with Javax.Swing.Undo.CompoundEdit;
with Javax.Swing.Undo.UndoableEdit;

package Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            DocumentEvent_I : Javax.Swing.Event.DocumentEvent.Ref;
            UndoableEdit_I : Javax.Swing.Undo.UndoableEdit.Ref)
    is new Javax.Swing.Undo.CompoundEdit.Typ(Serializable_I,
                                             UndoableEdit_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultDocumentEvent (P1_AbstractDocument : access Standard.Javax.Swing.Text.AbstractDocument.Typ'Class;
                                      P2_Int : Java.Int;
                                      P3_Int : Java.Int;
                                      P4_EventType : access Standard.Javax.Swing.Event.DocumentEvent.EventType.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function AddEdit (This : access Typ;
                     P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class)
                     return Java.Boolean;

   procedure Redo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   procedure Undo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotUndoException.Except

   function IsSignificant (This : access Typ)
                           return Java.Boolean;

   function GetPresentationName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetUndoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetRedoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ)
                     return access Javax.Swing.Event.DocumentEvent.EventType.Typ'Class;

   function GetOffset (This : access Typ)
                       return Java.Int;

   function GetLength (This : access Typ)
                       return Java.Int;

   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class;

   function GetChange (This : access Typ;
                       P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                       return access Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultDocumentEvent);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, AddEdit, "addEdit");
   pragma Import (Java, Redo, "redo");
   pragma Import (Java, Undo, "undo");
   pragma Import (Java, IsSignificant, "isSignificant");
   pragma Import (Java, GetPresentationName, "getPresentationName");
   pragma Import (Java, GetUndoPresentationName, "getUndoPresentationName");
   pragma Import (Java, GetRedoPresentationName, "getRedoPresentationName");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, GetDocument, "getDocument");
   pragma Import (Java, GetChange, "getChange");

end Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent;
pragma Import (Java, Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent, "javax.swing.text.AbstractDocument$DefaultDocumentEvent");
pragma Extensions_Allowed (Off);
