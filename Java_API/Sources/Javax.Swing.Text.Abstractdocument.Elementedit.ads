pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Element;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Event.DocumentEvent.ElementChange;
with Javax.Swing.Undo.AbstractUndoableEdit;
with Javax.Swing.Undo.UndoableEdit;

package Javax.Swing.Text.AbstractDocument.ElementEdit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            ElementChange_I : Javax.Swing.Event.DocumentEvent.ElementChange.Ref;
            UndoableEdit_I : Javax.Swing.Undo.UndoableEdit.Ref)
    is new Javax.Swing.Undo.AbstractUndoableEdit.Typ(Serializable_I,
                                                     UndoableEdit_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ElementEdit (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Element_Arr : access Javax.Swing.Text.Element.Arr_Obj;
                             P4_Element_Arr : access Javax.Swing.Text.Element.Arr_Obj; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetElement (This : access Typ)
                        return access Javax.Swing.Text.Element.Typ'Class;

   function GetIndex (This : access Typ)
                      return Java.Int;

   function GetChildrenRemoved (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetChildrenAdded (This : access Typ)
                              return Standard.Java.Lang.Object.Ref;

   procedure Redo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   procedure Undo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotUndoException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ElementEdit);
   pragma Import (Java, GetElement, "getElement");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, GetChildrenRemoved, "getChildrenRemoved");
   pragma Import (Java, GetChildrenAdded, "getChildrenAdded");
   pragma Import (Java, Redo, "redo");
   pragma Import (Java, Undo, "undo");

end Javax.Swing.Text.AbstractDocument.ElementEdit;
pragma Import (Java, Javax.Swing.Text.AbstractDocument.ElementEdit, "javax.swing.text.AbstractDocument$ElementEdit");
pragma Extensions_Allowed (Off);
