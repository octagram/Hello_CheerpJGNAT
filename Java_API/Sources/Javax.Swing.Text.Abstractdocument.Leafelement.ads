pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Text.AttributeSet;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractDocument.AbstractElement;
with Javax.Swing.Text.Element;
with Javax.Swing.Text.MutableAttributeSet;
with Javax.Swing.Tree.TreeNode;

package Javax.Swing.Text.AbstractDocument.LeafElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Element_I : Javax.Swing.Text.Element.Ref;
            MutableAttributeSet_I : Javax.Swing.Text.MutableAttributeSet.Ref;
            TreeNode_I : Javax.Swing.Tree.TreeNode.Ref)
    is new Javax.Swing.Text.AbstractDocument.AbstractElement.Typ(Serializable_I,
                                                                 Element_I,
                                                                 MutableAttributeSet_I,
                                                                 TreeNode_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LeafElement (P1_AbstractDocument : access Standard.Javax.Swing.Text.AbstractDocument.Typ'Class;
                             P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                             P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetStartOffset (This : access Typ)
                            return Java.Int;

   function GetEndOffset (This : access Typ)
                          return Java.Int;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetElementIndex (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   function GetElement (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Text.Element.Typ'Class;

   function GetElementCount (This : access Typ)
                             return Java.Int;

   function IsLeaf (This : access Typ)
                    return Java.Boolean;

   function GetAllowsChildren (This : access Typ)
                               return Java.Boolean;

   function Children (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LeafElement);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetStartOffset, "getStartOffset");
   pragma Import (Java, GetEndOffset, "getEndOffset");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetElementIndex, "getElementIndex");
   pragma Import (Java, GetElement, "getElement");
   pragma Import (Java, GetElementCount, "getElementCount");
   pragma Import (Java, IsLeaf, "isLeaf");
   pragma Import (Java, GetAllowsChildren, "getAllowsChildren");
   pragma Import (Java, Children, "children");

end Javax.Swing.Text.AbstractDocument.LeafElement;
pragma Import (Java, Javax.Swing.Text.AbstractDocument.LeafElement, "javax.swing.text.AbstractDocument$LeafElement");
pragma Extensions_Allowed (Off);
