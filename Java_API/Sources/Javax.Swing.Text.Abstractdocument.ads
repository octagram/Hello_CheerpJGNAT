pragma Extensions_Allowed (On);
limited with Java.Io.PrintStream;
limited with Java.Lang.Class;
limited with Java.Lang.Runnable;
limited with Java.Lang.String;
limited with Java.Lang.Thread;
limited with Java.Util.Dictionary;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Event.DocumentListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Event.UndoableEditEvent;
limited with Javax.Swing.Event.UndoableEditListener;
limited with Javax.Swing.Text.AbstractDocument.AttributeContext;
limited with Javax.Swing.Text.AbstractDocument.Content;
limited with Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.DocumentFilter;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position;
limited with Javax.Swing.Text.Segment;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Document;

package Javax.Swing.Text.AbstractDocument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Document_I : Javax.Swing.Text.Document.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   --  protected
   function New_AbstractDocument (P1_Content : access Standard.Javax.Swing.Text.AbstractDocument.Content.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   --  protected
   function New_AbstractDocument (P1_Content : access Standard.Javax.Swing.Text.AbstractDocument.Content.Typ'Class;
                                  P2_AttributeContext : access Standard.Javax.Swing.Text.AbstractDocument.AttributeContext.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDocumentProperties (This : access Typ)
                                   return access Java.Util.Dictionary.Typ'Class;

   procedure SetDocumentProperties (This : access Typ;
                                    P1_Dictionary : access Standard.Java.Util.Dictionary.Typ'Class);

   --  protected
   procedure FireInsertUpdate (This : access Typ;
                               P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class);

   --  protected
   procedure FireChangedUpdate (This : access Typ;
                                P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class);

   --  protected
   procedure FireRemoveUpdate (This : access Typ;
                               P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class);

   --  protected
   procedure FireUndoableEditUpdate (This : access Typ;
                                     P1_UndoableEditEvent : access Standard.Javax.Swing.Event.UndoableEditEvent.Typ'Class);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetAsynchronousLoadPriority (This : access Typ)
                                         return Java.Int;

   procedure SetAsynchronousLoadPriority (This : access Typ;
                                          P1_Int : Java.Int);

   procedure SetDocumentFilter (This : access Typ;
                                P1_DocumentFilter : access Standard.Javax.Swing.Text.DocumentFilter.Typ'Class);

   function GetDocumentFilter (This : access Typ)
                               return access Javax.Swing.Text.DocumentFilter.Typ'Class;

   procedure Render (This : access Typ;
                     P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);

   function GetLength (This : access Typ)
                       return Java.Int;

   procedure AddDocumentListener (This : access Typ;
                                  P1_DocumentListener : access Standard.Javax.Swing.Event.DocumentListener.Typ'Class);

   procedure RemoveDocumentListener (This : access Typ;
                                     P1_DocumentListener : access Standard.Javax.Swing.Event.DocumentListener.Typ'Class);

   function GetDocumentListeners (This : access Typ)
                                  return Standard.Java.Lang.Object.Ref;

   procedure AddUndoableEditListener (This : access Typ;
                                      P1_UndoableEditListener : access Standard.Javax.Swing.Event.UndoableEditListener.Typ'Class);

   procedure RemoveUndoableEditListener (This : access Typ;
                                         P1_UndoableEditListener : access Standard.Javax.Swing.Event.UndoableEditListener.Typ'Class);

   function GetUndoableEditListeners (This : access Typ)
                                      return Standard.Java.Lang.Object.Ref;

   --  final
   function GetProperty (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   --  final
   procedure PutProperty (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_String : access Standard.Java.Lang.String.Typ'Class;
                      P4_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure InsertString (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetText (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure GetText (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  synchronized
   function CreatePosition (This : access Typ;
                            P1_Int : Java.Int)
                            return access Javax.Swing.Text.Position.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetStartPosition (This : access Typ)
                              return access Javax.Swing.Text.Position.Typ'Class;

   --  final
   function GetEndPosition (This : access Typ)
                            return access Javax.Swing.Text.Position.Typ'Class;

   function GetRootElements (This : access Typ)
                             return Standard.Java.Lang.Object.Ref;

   function GetDefaultRootElement (This : access Typ)
                                   return access Javax.Swing.Text.Element.Typ'Class is abstract;

   function GetBidiRootElement (This : access Typ)
                                return access Javax.Swing.Text.Element.Typ'Class;

   function GetParagraphElement (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Javax.Swing.Text.Element.Typ'Class is abstract;

   --  final  protected
   function GetAttributeContext (This : access Typ)
                                 return access Javax.Swing.Text.AbstractDocument.AttributeContext.Typ'Class;

   --  protected
   procedure InsertUpdate (This : access Typ;
                           P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class;
                           P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   --  protected
   procedure RemoveUpdate (This : access Typ;
                           P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class);

   --  protected
   procedure PostRemoveUpdate (This : access Typ;
                               P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class);

   procedure Dump (This : access Typ;
                   P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   --  final  protected
   function GetContent (This : access Typ)
                        return access Javax.Swing.Text.AbstractDocument.Content.Typ'Class;

   --  protected
   function CreateLeafElement (This : access Typ;
                               P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                               P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int)
                               return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   function CreateBranchElement (This : access Typ;
                                 P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                 P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                 return access Javax.Swing.Text.Element.Typ'Class;

   --  final  protected  synchronized
   function GetCurrentWriter (This : access Typ)
                              return access Java.Lang.Thread.Typ'Class;

   --  final  protected  synchronized
   procedure WriteLock (This : access Typ);

   --  final  protected  synchronized
   procedure WriteUnlock (This : access Typ);

   --  final  synchronized
   procedure ReadLock (This : access Typ);

   --  final  synchronized
   procedure ReadUnlock (This : access Typ);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   BAD_LOCATION : constant access Java.Lang.String.Typ'Class;

   --  final
   ParagraphElementName : constant access Java.Lang.String.Typ'Class;

   --  final
   ContentElementName : constant access Java.Lang.String.Typ'Class;

   --  final
   SectionElementName : constant access Java.Lang.String.Typ'Class;

   --  final
   BidiElementName : constant access Java.Lang.String.Typ'Class;

   --  final
   ElementNameAttribute : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractDocument);
   pragma Import (Java, GetDocumentProperties, "getDocumentProperties");
   pragma Import (Java, SetDocumentProperties, "setDocumentProperties");
   pragma Import (Java, FireInsertUpdate, "fireInsertUpdate");
   pragma Import (Java, FireChangedUpdate, "fireChangedUpdate");
   pragma Import (Java, FireRemoveUpdate, "fireRemoveUpdate");
   pragma Import (Java, FireUndoableEditUpdate, "fireUndoableEditUpdate");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, GetAsynchronousLoadPriority, "getAsynchronousLoadPriority");
   pragma Import (Java, SetAsynchronousLoadPriority, "setAsynchronousLoadPriority");
   pragma Import (Java, SetDocumentFilter, "setDocumentFilter");
   pragma Import (Java, GetDocumentFilter, "getDocumentFilter");
   pragma Import (Java, Render, "render");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, AddDocumentListener, "addDocumentListener");
   pragma Import (Java, RemoveDocumentListener, "removeDocumentListener");
   pragma Import (Java, GetDocumentListeners, "getDocumentListeners");
   pragma Import (Java, AddUndoableEditListener, "addUndoableEditListener");
   pragma Import (Java, RemoveUndoableEditListener, "removeUndoableEditListener");
   pragma Import (Java, GetUndoableEditListeners, "getUndoableEditListeners");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, PutProperty, "putProperty");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, InsertString, "insertString");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, CreatePosition, "createPosition");
   pragma Import (Java, GetStartPosition, "getStartPosition");
   pragma Import (Java, GetEndPosition, "getEndPosition");
   pragma Import (Java, GetRootElements, "getRootElements");
   pragma Export (Java, GetDefaultRootElement, "getDefaultRootElement");
   pragma Import (Java, GetBidiRootElement, "getBidiRootElement");
   pragma Export (Java, GetParagraphElement, "getParagraphElement");
   pragma Import (Java, GetAttributeContext, "getAttributeContext");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, PostRemoveUpdate, "postRemoveUpdate");
   pragma Import (Java, Dump, "dump");
   pragma Import (Java, GetContent, "getContent");
   pragma Import (Java, CreateLeafElement, "createLeafElement");
   pragma Import (Java, CreateBranchElement, "createBranchElement");
   pragma Import (Java, GetCurrentWriter, "getCurrentWriter");
   pragma Import (Java, WriteLock, "writeLock");
   pragma Import (Java, WriteUnlock, "writeUnlock");
   pragma Import (Java, ReadLock, "readLock");
   pragma Import (Java, ReadUnlock, "readUnlock");
   pragma Import (Java, BAD_LOCATION, "BAD_LOCATION");
   pragma Import (Java, ParagraphElementName, "ParagraphElementName");
   pragma Import (Java, ContentElementName, "ContentElementName");
   pragma Import (Java, SectionElementName, "SectionElementName");
   pragma Import (Java, BidiElementName, "BidiElementName");
   pragma Import (Java, ElementNameAttribute, "ElementNameAttribute");

end Javax.Swing.Text.AbstractDocument;
pragma Import (Java, Javax.Swing.Text.AbstractDocument, "javax.swing.text.AbstractDocument");
pragma Extensions_Allowed (Off);
