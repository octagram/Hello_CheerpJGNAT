pragma Extensions_Allowed (On);
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.ElementIterator;
with Java.Lang.Object;

package Javax.Swing.Text.AbstractWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_AbstractWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                                P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   --  protected
   function New_AbstractWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                                P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   --  protected
   function New_AbstractWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                                P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   --  protected
   function New_AbstractWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                                P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStartOffset (This : access Typ)
                            return Java.Int;

   function GetEndOffset (This : access Typ)
                          return Java.Int;

   --  protected
   function GetElementIterator (This : access Typ)
                                return access Javax.Swing.Text.ElementIterator.Typ'Class;

   --  protected
   function GetWriter (This : access Typ)
                       return access Java.Io.Writer.Typ'Class;

   --  protected
   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class;

   --  protected
   function InRange (This : access Typ;
                     P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                     return Java.Boolean;

   --  protected
   procedure Write (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   --  protected
   function GetText (This : access Typ;
                     P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure Text (This : access Typ;
                   P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   --  protected
   procedure SetLineLength (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   function GetLineLength (This : access Typ)
                           return Java.Int;

   --  protected
   procedure SetCurrentLineLength (This : access Typ;
                                   P1_Int : Java.Int);

   --  protected
   function GetCurrentLineLength (This : access Typ)
                                  return Java.Int;

   --  protected
   function IsLineEmpty (This : access Typ)
                         return Java.Boolean;

   --  protected
   procedure SetCanWrapLines (This : access Typ;
                              P1_Boolean : Java.Boolean);

   --  protected
   function GetCanWrapLines (This : access Typ)
                             return Java.Boolean;

   --  protected
   procedure SetIndentSpace (This : access Typ;
                             P1_Int : Java.Int);

   --  protected
   function GetIndentSpace (This : access Typ)
                            return Java.Int;

   procedure SetLineSeparator (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetLineSeparator (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  protected
   procedure IncrIndent (This : access Typ);

   --  protected
   procedure DecrIndent (This : access Typ);

   --  protected
   function GetIndentLevel (This : access Typ)
                            return Java.Int;

   --  protected
   procedure Indent (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Write (This : access Typ;
                    P1_Char : Java.Char);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteLineSeparator (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Write (This : access Typ;
                    P1_Char_Arr : Java.Char_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Output (This : access Typ;
                     P1_Char_Arr : Java.Char_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   NEWLINE : constant Java.Char;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractWriter);
   pragma Export (Java, GetStartOffset, "getStartOffset");
   pragma Export (Java, GetEndOffset, "getEndOffset");
   pragma Export (Java, GetElementIterator, "getElementIterator");
   pragma Export (Java, GetWriter, "getWriter");
   pragma Export (Java, GetDocument, "getDocument");
   pragma Export (Java, InRange, "inRange");
   pragma Export (Java, Write, "write");
   pragma Export (Java, GetText, "getText");
   pragma Export (Java, Text, "text");
   pragma Export (Java, SetLineLength, "setLineLength");
   pragma Export (Java, GetLineLength, "getLineLength");
   pragma Export (Java, SetCurrentLineLength, "setCurrentLineLength");
   pragma Export (Java, GetCurrentLineLength, "getCurrentLineLength");
   pragma Export (Java, IsLineEmpty, "isLineEmpty");
   pragma Export (Java, SetCanWrapLines, "setCanWrapLines");
   pragma Export (Java, GetCanWrapLines, "getCanWrapLines");
   pragma Export (Java, SetIndentSpace, "setIndentSpace");
   pragma Export (Java, GetIndentSpace, "getIndentSpace");
   pragma Export (Java, SetLineSeparator, "setLineSeparator");
   pragma Export (Java, GetLineSeparator, "getLineSeparator");
   pragma Export (Java, IncrIndent, "incrIndent");
   pragma Export (Java, DecrIndent, "decrIndent");
   pragma Export (Java, GetIndentLevel, "getIndentLevel");
   pragma Export (Java, Indent, "indent");
   pragma Export (Java, WriteLineSeparator, "writeLineSeparator");
   pragma Export (Java, WriteAttributes, "writeAttributes");
   pragma Export (Java, Output, "output");
   pragma Import (Java, NEWLINE, "NEWLINE");

end Javax.Swing.Text.AbstractWriter;
pragma Import (Java, Javax.Swing.Text.AbstractWriter, "javax.swing.text.AbstractWriter");
pragma Extensions_Allowed (Off);
