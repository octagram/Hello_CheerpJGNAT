pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.AsyncBoxView.ChildState;
with Java.Lang.Object;

package Javax.Swing.Text.AsyncBoxView.ChildLocator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      LastValidOffset : access Javax.Swing.Text.AsyncBoxView.ChildState.Typ'Class;
      pragma Import (Java, LastValidOffset, "lastValidOffset");

      --  protected
      LastAlloc : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, LastAlloc, "lastAlloc");

      --  protected
      ChildAlloc : access Java.Awt.Rectangle.Typ'Class;
      pragma Import (Java, ChildAlloc, "childAlloc");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChildLocator (P1_AsyncBoxView : access Standard.Javax.Swing.Text.AsyncBoxView.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure ChildChanged (This : access Typ;
                           P1_ChildState : access Standard.Javax.Swing.Text.AsyncBoxView.ChildState.Typ'Class);

   --  synchronized
   procedure PaintChildren (This : access Typ;
                            P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   --  synchronized
   function GetChildAllocation (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                return access Java.Awt.Shape.Typ'Class;

   function GetViewIndexAtPoint (This : access Typ;
                                 P1_Float : Java.Float;
                                 P2_Float : Java.Float;
                                 P3_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                 return Java.Int;

   --  protected
   function GetChildAllocation (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Awt.Shape.Typ'Class;

   --  protected
   procedure SetAllocation (This : access Typ;
                            P1_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   --  protected
   function GetViewIndexAtVisualOffset (This : access Typ;
                                        P1_Float : Java.Float)
                                        return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ChildLocator);
   pragma Import (Java, ChildChanged, "childChanged");
   pragma Import (Java, PaintChildren, "paintChildren");
   pragma Import (Java, GetChildAllocation, "getChildAllocation");
   pragma Import (Java, GetViewIndexAtPoint, "getViewIndexAtPoint");
   pragma Import (Java, SetAllocation, "setAllocation");
   pragma Import (Java, GetViewIndexAtVisualOffset, "getViewIndexAtVisualOffset");

end Javax.Swing.Text.AsyncBoxView.ChildLocator;
pragma Import (Java, Javax.Swing.Text.AsyncBoxView.ChildLocator, "javax.swing.text.AsyncBoxView$ChildLocator");
pragma Extensions_Allowed (Off);
