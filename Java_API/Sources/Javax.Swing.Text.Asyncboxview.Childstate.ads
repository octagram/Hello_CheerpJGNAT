pragma Extensions_Allowed (On);
with Javax.Swing.Text.View;
with Java.Lang.Object;
with Java.Lang.Runnable;

package Javax.Swing.Text.AsyncBoxView.ChildState is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Runnable_I : Java.Lang.Runnable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChildState (P1_AsyncBoxView : access Standard.Javax.Swing.Text.AsyncBoxView.Typ'Class;
                            P2_View : access Standard.Javax.Swing.Text.View.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetChildView (This : access Typ)
                          return access Javax.Swing.Text.View.Typ'Class;

   procedure Run (This : access Typ);

   function GetMinorSpan (This : access Typ)
                          return Java.Float;

   function GetMinorOffset (This : access Typ)
                            return Java.Float;

   function GetMajorSpan (This : access Typ)
                          return Java.Float;

   function GetMajorOffset (This : access Typ)
                            return Java.Float;

   procedure SetMajorOffset (This : access Typ;
                             P1_Float : Java.Float);

   procedure PreferenceChanged (This : access Typ;
                                P1_Boolean : Java.Boolean;
                                P2_Boolean : Java.Boolean);

   function IsLayoutValid (This : access Typ)
                           return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ChildState);
   pragma Import (Java, GetChildView, "getChildView");
   pragma Import (Java, Run, "run");
   pragma Import (Java, GetMinorSpan, "getMinorSpan");
   pragma Import (Java, GetMinorOffset, "getMinorOffset");
   pragma Import (Java, GetMajorSpan, "getMajorSpan");
   pragma Import (Java, GetMajorOffset, "getMajorOffset");
   pragma Import (Java, SetMajorOffset, "setMajorOffset");
   pragma Import (Java, PreferenceChanged, "preferenceChanged");
   pragma Import (Java, IsLayoutValid, "isLayoutValid");

end Javax.Swing.Text.AsyncBoxView.ChildState;
pragma Import (Java, Javax.Swing.Text.AsyncBoxView.ChildState, "javax.swing.text.AsyncBoxView$ChildState");
pragma Extensions_Allowed (Off);
