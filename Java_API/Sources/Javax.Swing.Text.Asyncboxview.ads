pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Event.DocumentEvent.ElementChange;
limited with Javax.Swing.Text.AsyncBoxView.ChildLocator;
limited with Javax.Swing.Text.AsyncBoxView.ChildState;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.LayoutQueue;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.View;

package Javax.Swing.Text.AsyncBoxView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.View.Typ(SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Locator : access Javax.Swing.Text.AsyncBoxView.ChildLocator.Typ'Class;
      pragma Import (Java, Locator, "locator");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AsyncBoxView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                              P2_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMajorAxis (This : access Typ)
                          return Java.Int;

   function GetMinorAxis (This : access Typ)
                          return Java.Int;

   function GetTopInset (This : access Typ)
                         return Java.Float;

   procedure SetTopInset (This : access Typ;
                          P1_Float : Java.Float);

   function GetBottomInset (This : access Typ)
                            return Java.Float;

   procedure SetBottomInset (This : access Typ;
                             P1_Float : Java.Float);

   function GetLeftInset (This : access Typ)
                          return Java.Float;

   procedure SetLeftInset (This : access Typ;
                           P1_Float : Java.Float);

   function GetRightInset (This : access Typ)
                           return Java.Float;

   procedure SetRightInset (This : access Typ;
                            P1_Float : Java.Float);

   --  protected
   function GetInsetSpan (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   --  protected
   procedure SetEstimatedMajorSpan (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   --  protected
   function GetEstimatedMajorSpan (This : access Typ)
                                   return Java.Boolean;

   --  protected
   function GetChildState (This : access Typ;
                           P1_Int : Java.Int)
                           return access Javax.Swing.Text.AsyncBoxView.ChildState.Typ'Class;

   --  protected
   function GetLayoutQueue (This : access Typ)
                            return access Javax.Swing.Text.LayoutQueue.Typ'Class;

   --  protected
   function CreateChildState (This : access Typ;
                              P1_View : access Standard.Javax.Swing.Text.View.Typ'Class)
                              return access Javax.Swing.Text.AsyncBoxView.ChildState.Typ'Class;

   --  protected  synchronized
   procedure MajorRequirementChange (This : access Typ;
                                     P1_ChildState : access Standard.Javax.Swing.Text.AsyncBoxView.ChildState.Typ'Class;
                                     P2_Float : Java.Float);

   --  protected  synchronized
   procedure MinorRequirementChange (This : access Typ;
                                     P1_ChildState : access Standard.Javax.Swing.Text.AsyncBoxView.ChildState.Typ'Class);

   --  protected
   procedure FlushRequirementChanges (This : access Typ);

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_View_Arr : access Javax.Swing.Text.View.Arr_Obj);

   --  protected
   procedure LoadChildren (This : access Typ;
                           P1_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   --  protected  synchronized
   function GetViewIndexAtPosition (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                                    return Java.Int;

   --  protected
   procedure UpdateLayout (This : access Typ;
                           P1_ElementChange : access Standard.Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
                           P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P3_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   procedure SetParent (This : access Typ;
                        P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   --  synchronized
   procedure PreferenceChanged (This : access Typ;
                                P1_View : access Standard.Javax.Swing.Text.View.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean);

   procedure SetSize (This : access Typ;
                      P1_Float : Java.Float;
                      P2_Float : Java.Float);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   function GetMinimumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetMaximumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetViewCount (This : access Typ)
                          return Java.Int;

   function GetView (This : access Typ;
                     P1_Int : Java.Int)
                     return access Javax.Swing.Text.View.Typ'Class;

   function GetChildAllocation (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                return access Java.Awt.Shape.Typ'Class;

   function GetViewIndex (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                          return Java.Int;

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AsyncBoxView);
   pragma Import (Java, GetMajorAxis, "getMajorAxis");
   pragma Import (Java, GetMinorAxis, "getMinorAxis");
   pragma Import (Java, GetTopInset, "getTopInset");
   pragma Import (Java, SetTopInset, "setTopInset");
   pragma Import (Java, GetBottomInset, "getBottomInset");
   pragma Import (Java, SetBottomInset, "setBottomInset");
   pragma Import (Java, GetLeftInset, "getLeftInset");
   pragma Import (Java, SetLeftInset, "setLeftInset");
   pragma Import (Java, GetRightInset, "getRightInset");
   pragma Import (Java, SetRightInset, "setRightInset");
   pragma Import (Java, GetInsetSpan, "getInsetSpan");
   pragma Import (Java, SetEstimatedMajorSpan, "setEstimatedMajorSpan");
   pragma Import (Java, GetEstimatedMajorSpan, "getEstimatedMajorSpan");
   pragma Import (Java, GetChildState, "getChildState");
   pragma Import (Java, GetLayoutQueue, "getLayoutQueue");
   pragma Import (Java, CreateChildState, "createChildState");
   pragma Import (Java, MajorRequirementChange, "majorRequirementChange");
   pragma Import (Java, MinorRequirementChange, "minorRequirementChange");
   pragma Import (Java, FlushRequirementChanges, "flushRequirementChanges");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, LoadChildren, "loadChildren");
   pragma Import (Java, GetViewIndexAtPosition, "getViewIndexAtPosition");
   pragma Import (Java, UpdateLayout, "updateLayout");
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, PreferenceChanged, "preferenceChanged");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetMinimumSpan, "getMinimumSpan");
   pragma Import (Java, GetMaximumSpan, "getMaximumSpan");
   pragma Import (Java, GetViewCount, "getViewCount");
   pragma Import (Java, GetView, "getView");
   pragma Import (Java, GetChildAllocation, "getChildAllocation");
   pragma Import (Java, GetViewIndex, "getViewIndex");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");

end Javax.Swing.Text.AsyncBoxView;
pragma Import (Java, Javax.Swing.Text.AsyncBoxView, "javax.swing.text.AsyncBoxView");
pragma Extensions_Allowed (Off);
