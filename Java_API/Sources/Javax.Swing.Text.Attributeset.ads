pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Javax.Swing.Text.AttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributeCount (This : access Typ)
                               return Java.Int is abstract;

   function IsDefined (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean is abstract;

   function IsEqual (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return Java.Boolean is abstract;

   function CopyAttributes (This : access Typ)
                            return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function GetAttribute (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   function GetAttributeNames (This : access Typ)
                               return access Java.Util.Enumeration.Typ'Class is abstract;

   function ContainsAttribute (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean is abstract;

   function ContainsAttributes (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                return Java.Boolean is abstract;

   function GetResolveParent (This : access Typ)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NameAttribute : access Java.Lang.Object.Typ'Class;

   --  final
   ResolveAttribute : access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAttributeCount, "getAttributeCount");
   pragma Export (Java, IsDefined, "isDefined");
   pragma Export (Java, IsEqual, "isEqual");
   pragma Export (Java, CopyAttributes, "copyAttributes");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, GetAttributeNames, "getAttributeNames");
   pragma Export (Java, ContainsAttribute, "containsAttribute");
   pragma Export (Java, ContainsAttributes, "containsAttributes");
   pragma Export (Java, GetResolveParent, "getResolveParent");
   pragma Import (Java, NameAttribute, "NameAttribute");
   pragma Import (Java, ResolveAttribute, "ResolveAttribute");

end Javax.Swing.Text.AttributeSet;
pragma Import (Java, Javax.Swing.Text.AttributeSet, "javax.swing.text.AttributeSet");
pragma Extensions_Allowed (Off);
