pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Event.DocumentEvent.ElementChange;
limited with Javax.Swing.SizeRequirements;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.View;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.CompositeView;

package Javax.Swing.Text.BoxView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.CompositeView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BoxView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                         P2_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAxis (This : access Typ)
                     return Java.Int;

   procedure SetAxis (This : access Typ;
                      P1_Int : Java.Int);

   procedure LayoutChanged (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   function IsLayoutValid (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean;

   --  protected
   procedure PaintChild (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                         P3_Int : Java.Int);

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_View_Arr : access Javax.Swing.Text.View.Arr_Obj);

   --  protected
   procedure ForwardUpdate (This : access Typ;
                            P1_ElementChange : access Standard.Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
                            P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P4_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure PreferenceChanged (This : access Typ;
                                P1_View : access Standard.Javax.Swing.Text.View.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean);

   function GetResizeWeight (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   procedure SetSize (This : access Typ;
                      P1_Float : Java.Float;
                      P2_Float : Java.Float);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetChildAllocation (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                return access Java.Awt.Shape.Typ'Class;

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   function GetAlignment (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   function GetMinimumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetMaximumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   --  protected
   function IsAllocationValid (This : access Typ)
                               return Java.Boolean;

   --  protected
   function IsBefore (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                      return Java.Boolean;

   --  protected
   function IsAfter (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                     return Java.Boolean;

   --  protected
   function GetViewAtPoint (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                            return access Javax.Swing.Text.View.Typ'Class;

   --  protected
   procedure ChildAllocation (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure Layout (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int);

   function GetWidth (This : access Typ)
                      return Java.Int;

   function GetHeight (This : access Typ)
                       return Java.Int;

   --  protected
   procedure LayoutMajorAxis (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int_Arr : Java.Int_Arr;
                              P4_Int_Arr : Java.Int_Arr);

   --  protected
   procedure LayoutMinorAxis (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int_Arr : Java.Int_Arr;
                              P4_Int_Arr : Java.Int_Arr);

   --  protected
   function CalculateMajorAxisRequirements (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                            return access Javax.Swing.SizeRequirements.Typ'Class;

   --  protected
   function CalculateMinorAxisRequirements (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                            return access Javax.Swing.SizeRequirements.Typ'Class;

   --  protected
   procedure BaselineLayout (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int;
                             P3_Int_Arr : Java.Int_Arr;
                             P4_Int_Arr : Java.Int_Arr);

   --  protected
   function BaselineRequirements (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                  return access Javax.Swing.SizeRequirements.Typ'Class;

   --  protected
   function GetOffset (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return Java.Int;

   --  protected
   function GetSpan (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return Java.Int;

   --  protected
   function FlipEastAndWestAtEnds (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                                   return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BoxView);
   pragma Import (Java, GetAxis, "getAxis");
   pragma Import (Java, SetAxis, "setAxis");
   pragma Import (Java, LayoutChanged, "layoutChanged");
   pragma Import (Java, IsLayoutValid, "isLayoutValid");
   pragma Import (Java, PaintChild, "paintChild");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, ForwardUpdate, "forwardUpdate");
   pragma Import (Java, PreferenceChanged, "preferenceChanged");
   pragma Import (Java, GetResizeWeight, "getResizeWeight");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetChildAllocation, "getChildAllocation");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetMinimumSpan, "getMinimumSpan");
   pragma Import (Java, GetMaximumSpan, "getMaximumSpan");
   pragma Import (Java, IsAllocationValid, "isAllocationValid");
   pragma Import (Java, IsBefore, "isBefore");
   pragma Import (Java, IsAfter, "isAfter");
   pragma Import (Java, GetViewAtPoint, "getViewAtPoint");
   pragma Import (Java, ChildAllocation, "childAllocation");
   pragma Import (Java, Layout, "layout");
   pragma Import (Java, GetWidth, "getWidth");
   pragma Import (Java, GetHeight, "getHeight");
   pragma Import (Java, LayoutMajorAxis, "layoutMajorAxis");
   pragma Import (Java, LayoutMinorAxis, "layoutMinorAxis");
   pragma Import (Java, CalculateMajorAxisRequirements, "calculateMajorAxisRequirements");
   pragma Import (Java, CalculateMinorAxisRequirements, "calculateMinorAxisRequirements");
   pragma Import (Java, BaselineLayout, "baselineLayout");
   pragma Import (Java, BaselineRequirements, "baselineRequirements");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetSpan, "getSpan");
   pragma Import (Java, FlipEastAndWestAtEnds, "flipEastAndWestAtEnds");

end Javax.Swing.Text.BoxView;
pragma Import (Java, Javax.Swing.Text.BoxView, "javax.swing.text.BoxView");
pragma Extensions_Allowed (Off);
