pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Text.JTextComponent;
with Java.Lang.Object;

package Javax.Swing.Text.Caret is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Install (This : access Typ;
                      P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class) is abstract;

   procedure Deinstall (This : access Typ;
                        P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class) is abstract;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class) is abstract;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   function IsVisible (This : access Typ)
                       return Java.Boolean is abstract;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   function IsSelectionVisible (This : access Typ)
                                return Java.Boolean is abstract;

   procedure SetSelectionVisible (This : access Typ;
                                  P1_Boolean : Java.Boolean) is abstract;

   procedure SetMagicCaretPosition (This : access Typ;
                                    P1_Point : access Standard.Java.Awt.Point.Typ'Class) is abstract;

   function GetMagicCaretPosition (This : access Typ)
                                   return access Java.Awt.Point.Typ'Class is abstract;

   procedure SetBlinkRate (This : access Typ;
                           P1_Int : Java.Int) is abstract;

   function GetBlinkRate (This : access Typ)
                          return Java.Int is abstract;

   function GetDot (This : access Typ)
                    return Java.Int is abstract;

   function GetMark (This : access Typ)
                     return Java.Int is abstract;

   procedure SetDot (This : access Typ;
                     P1_Int : Java.Int) is abstract;

   procedure MoveDot (This : access Typ;
                      P1_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Install, "install");
   pragma Export (Java, Deinstall, "deinstall");
   pragma Export (Java, Paint, "paint");
   pragma Export (Java, AddChangeListener, "addChangeListener");
   pragma Export (Java, RemoveChangeListener, "removeChangeListener");
   pragma Export (Java, IsVisible, "isVisible");
   pragma Export (Java, SetVisible, "setVisible");
   pragma Export (Java, IsSelectionVisible, "isSelectionVisible");
   pragma Export (Java, SetSelectionVisible, "setSelectionVisible");
   pragma Export (Java, SetMagicCaretPosition, "setMagicCaretPosition");
   pragma Export (Java, GetMagicCaretPosition, "getMagicCaretPosition");
   pragma Export (Java, SetBlinkRate, "setBlinkRate");
   pragma Export (Java, GetBlinkRate, "getBlinkRate");
   pragma Export (Java, GetDot, "getDot");
   pragma Export (Java, GetMark, "getMark");
   pragma Export (Java, SetDot, "setDot");
   pragma Export (Java, MoveDot, "moveDot");

end Javax.Swing.Text.Caret;
pragma Import (Java, Javax.Swing.Text.Caret, "javax.swing.text.Caret");
pragma Extensions_Allowed (Off);
