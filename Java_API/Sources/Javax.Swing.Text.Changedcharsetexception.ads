pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.IOException;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.ChangedCharSetException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Io.IOException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChangedCharSetException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Boolean : Java.Boolean; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCharSetSpec (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function KeyEqualsCharSet (This : access Typ)
                              return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.swing.text.ChangedCharSetException");
   pragma Java_Constructor (New_ChangedCharSetException);
   pragma Import (Java, GetCharSetSpec, "getCharSetSpec");
   pragma Import (Java, KeyEqualsCharSet, "keyEqualsCharSet");

end Javax.Swing.Text.ChangedCharSetException;
pragma Import (Java, Javax.Swing.Text.ChangedCharSetException, "javax.swing.text.ChangedCharSetException");
pragma Extensions_Allowed (Off);
