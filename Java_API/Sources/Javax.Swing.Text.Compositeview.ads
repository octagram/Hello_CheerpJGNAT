pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.View;

package Javax.Swing.Text.CompositeView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is abstract new Javax.Swing.Text.View.Typ(SwingConstants_I)
      with null record;

   function New_CompositeView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure LoadChildren (This : access Typ;
                           P1_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure SetParent (This : access Typ;
                        P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   function GetViewCount (This : access Typ)
                          return Java.Int;

   function GetView (This : access Typ;
                     P1_Int : Java.Int)
                     return access Javax.Swing.Text.View.Typ'Class;

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_View_Arr : access Javax.Swing.Text.View.Arr_Obj);

   function GetChildAllocation (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                return access Java.Awt.Shape.Typ'Class;

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                         P3_Int : Java.Int;
                         P4_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                         P5_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetViewIndex (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                          return Java.Int;

   --  protected
   function IsBefore (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                      return Java.Boolean is abstract;

   --  protected
   function IsAfter (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                     return Java.Boolean is abstract;

   --  protected
   function GetViewAtPoint (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                            return access Javax.Swing.Text.View.Typ'Class is abstract;

   --  protected
   procedure ChildAllocation (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class) is abstract;

   --  protected
   function GetViewAtPosition (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return access Javax.Swing.Text.View.Typ'Class;

   --  protected
   function GetViewIndexAtPosition (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Java.Int;

   --  protected
   function GetInsideAllocation (This : access Typ;
                                 P1_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                 return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   procedure SetParagraphInsets (This : access Typ;
                                 P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   --  protected
   procedure SetInsets (This : access Typ;
                        P1_Short : Java.Short;
                        P2_Short : Java.Short;
                        P3_Short : Java.Short;
                        P4_Short : Java.Short);

   --  protected
   function GetLeftInset (This : access Typ)
                          return Java.Short;

   --  protected
   function GetRightInset (This : access Typ)
                           return Java.Short;

   --  protected
   function GetTopInset (This : access Typ)
                         return Java.Short;

   --  protected
   function GetBottomInset (This : access Typ)
                            return Java.Short;

   --  protected
   function GetNextNorthSouthVisualPositionFrom (This : access Typ;
                                                 P1_Int : Java.Int;
                                                 P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                                 P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                                 P4_Int : Java.Int;
                                                 P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                                 return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   function GetNextEastWestVisualPositionFrom (This : access Typ;
                                               P1_Int : Java.Int;
                                               P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                               P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                               P4_Int : Java.Int;
                                               P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                               return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   function FlipEastAndWestAtEnds (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                                   return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompositeView);
   pragma Import (Java, LoadChildren, "loadChildren");
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetViewCount, "getViewCount");
   pragma Import (Java, GetView, "getView");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, GetChildAllocation, "getChildAllocation");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");
   pragma Import (Java, GetViewIndex, "getViewIndex");
   pragma Export (Java, IsBefore, "isBefore");
   pragma Export (Java, IsAfter, "isAfter");
   pragma Export (Java, GetViewAtPoint, "getViewAtPoint");
   pragma Export (Java, ChildAllocation, "childAllocation");
   pragma Import (Java, GetViewAtPosition, "getViewAtPosition");
   pragma Import (Java, GetViewIndexAtPosition, "getViewIndexAtPosition");
   pragma Import (Java, GetInsideAllocation, "getInsideAllocation");
   pragma Import (Java, SetParagraphInsets, "setParagraphInsets");
   pragma Import (Java, SetInsets, "setInsets");
   pragma Import (Java, GetLeftInset, "getLeftInset");
   pragma Import (Java, GetRightInset, "getRightInset");
   pragma Import (Java, GetTopInset, "getTopInset");
   pragma Import (Java, GetBottomInset, "getBottomInset");
   pragma Import (Java, GetNextNorthSouthVisualPositionFrom, "getNextNorthSouthVisualPositionFrom");
   pragma Import (Java, GetNextEastWestVisualPositionFrom, "getNextEastWestVisualPositionFrom");
   pragma Import (Java, FlipEastAndWestAtEnds, "flipEastAndWestAtEnds");

end Javax.Swing.Text.CompositeView;
pragma Import (Java, Javax.Swing.Text.CompositeView, "javax.swing.text.CompositeView");
pragma Extensions_Allowed (Off);
