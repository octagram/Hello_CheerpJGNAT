pragma Extensions_Allowed (On);
limited with Java.Awt.Event.FocusEvent;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Graphics;
limited with Java.Awt.Point;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Text.Highlighter.HighlightPainter;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.Position.Bias;
with Java.Awt.Event.FocusListener;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Rectangle;
with Java.Awt.Shape;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.Caret;

package Javax.Swing.Text.DefaultCaret is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Shape_I : Java.Awt.Shape.Ref;
            FocusListener_I : Java.Awt.Event.FocusListener.Ref;
            MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Caret_I : Javax.Swing.Text.Caret.Ref)
    is new Java.Awt.Rectangle.Typ(Shape_I,
                                  Serializable_I,
                                  Cloneable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultCaret (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetUpdatePolicy (This : access Typ;
                              P1_Int : Java.Int);

   function GetUpdatePolicy (This : access Typ)
                             return Java.Int;

   --  final  protected
   function GetComponent (This : access Typ)
                          return access Javax.Swing.Text.JTextComponent.Typ'Class;

   --  final  protected  synchronized
   procedure Repaint (This : access Typ);

   --  protected  synchronized
   procedure Damage (This : access Typ;
                     P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   procedure AdjustVisibility (This : access Typ;
                               P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function GetSelectionPainter (This : access Typ)
                                 return access Javax.Swing.Text.Highlighter.HighlightPainter.Typ'Class;

   --  protected
   procedure PositionCaret (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   --  protected
   procedure MoveCaret (This : access Typ;
                        P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure FocusGained (This : access Typ;
                          P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure FocusLost (This : access Typ;
                        P1_FocusEvent : access Standard.Java.Awt.Event.FocusEvent.Typ'Class);

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseReleased (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Install (This : access Typ;
                      P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class);

   procedure Deinstall (This : access Typ;
                        P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class);

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   procedure SetSelectionVisible (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   function IsSelectionVisible (This : access Typ)
                                return Java.Boolean;

   function IsActive (This : access Typ)
                      return Java.Boolean;

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   procedure SetBlinkRate (This : access Typ;
                           P1_Int : Java.Int);

   function GetBlinkRate (This : access Typ)
                          return Java.Int;

   function GetDot (This : access Typ)
                    return Java.Int;

   function GetMark (This : access Typ)
                     return Java.Int;

   procedure SetDot (This : access Typ;
                     P1_Int : Java.Int);

   procedure MoveDot (This : access Typ;
                      P1_Int : Java.Int);

   procedure MoveDot (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class);

   procedure SetDot (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class);

   function GetDotBias (This : access Typ)
                        return access Javax.Swing.Text.Position.Bias.Typ'Class;

   function GetMarkBias (This : access Typ)
                         return access Javax.Swing.Text.Position.Bias.Typ'Class;

   procedure SetMagicCaretPosition (This : access Typ;
                                    P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetMagicCaretPosition (This : access Typ)
                                   return access Java.Awt.Point.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UPDATE_WHEN_ON_EDT : constant Java.Int;

   --  final
   NEVER_UPDATE : constant Java.Int;

   --  final
   ALWAYS_UPDATE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultCaret);
   pragma Import (Java, SetUpdatePolicy, "setUpdatePolicy");
   pragma Import (Java, GetUpdatePolicy, "getUpdatePolicy");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, Damage, "damage");
   pragma Import (Java, AdjustVisibility, "adjustVisibility");
   pragma Import (Java, GetSelectionPainter, "getSelectionPainter");
   pragma Import (Java, PositionCaret, "positionCaret");
   pragma Import (Java, MoveCaret, "moveCaret");
   pragma Import (Java, FocusGained, "focusGained");
   pragma Import (Java, FocusLost, "focusLost");
   pragma Import (Java, MouseClicked, "mouseClicked");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseReleased, "mouseReleased");
   pragma Import (Java, MouseEntered, "mouseEntered");
   pragma Import (Java, MouseExited, "mouseExited");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Install, "install");
   pragma Import (Java, Deinstall, "deinstall");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, SetSelectionVisible, "setSelectionVisible");
   pragma Import (Java, IsSelectionVisible, "isSelectionVisible");
   pragma Import (Java, IsActive, "isActive");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, SetBlinkRate, "setBlinkRate");
   pragma Import (Java, GetBlinkRate, "getBlinkRate");
   pragma Import (Java, GetDot, "getDot");
   pragma Import (Java, GetMark, "getMark");
   pragma Import (Java, SetDot, "setDot");
   pragma Import (Java, MoveDot, "moveDot");
   pragma Import (Java, GetDotBias, "getDotBias");
   pragma Import (Java, GetMarkBias, "getMarkBias");
   pragma Import (Java, SetMagicCaretPosition, "setMagicCaretPosition");
   pragma Import (Java, GetMagicCaretPosition, "getMagicCaretPosition");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, UPDATE_WHEN_ON_EDT, "UPDATE_WHEN_ON_EDT");
   pragma Import (Java, NEVER_UPDATE, "NEVER_UPDATE");
   pragma Import (Java, ALWAYS_UPDATE, "ALWAYS_UPDATE");

end Javax.Swing.Text.DefaultCaret;
pragma Import (Java, Javax.Swing.Text.DefaultCaret, "javax.swing.text.DefaultCaret");
pragma Extensions_Allowed (Off);
