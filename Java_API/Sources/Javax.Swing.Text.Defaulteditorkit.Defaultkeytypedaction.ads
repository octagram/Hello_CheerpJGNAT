pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Action;
with Javax.Swing.Text.TextAction;

package Javax.Swing.Text.DefaultEditorKit.DefaultKeyTypedAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Action_I : Javax.Swing.Action.Ref)
    is new Javax.Swing.Text.TextAction.Typ(Serializable_I,
                                           Cloneable_I,
                                           Action_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultKeyTypedAction (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultKeyTypedAction);
   pragma Import (Java, ActionPerformed, "actionPerformed");

end Javax.Swing.Text.DefaultEditorKit.DefaultKeyTypedAction;
pragma Import (Java, Javax.Swing.Text.DefaultEditorKit.DefaultKeyTypedAction, "javax.swing.text.DefaultEditorKit$DefaultKeyTypedAction");
pragma Extensions_Allowed (Off);
