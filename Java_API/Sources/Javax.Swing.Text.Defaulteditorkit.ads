pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.Text.Caret;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.ViewFactory;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.EditorKit;

package Javax.Swing.Text.DefaultEditorKit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Swing.Text.EditorKit.Typ(Serializable_I,
                                          Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultEditorKit (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetViewFactory (This : access Typ)
                            return access Javax.Swing.Text.ViewFactory.Typ'Class;

   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function CreateCaret (This : access Typ)
                         return access Javax.Swing.Text.Caret.Typ'Class;

   function CreateDefaultDocument (This : access Typ)
                                   return access Javax.Swing.Text.Document.Typ'Class;

   procedure Read (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                   P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Write (This : access Typ;
                    P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                    P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Read (This : access Typ;
                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                   P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                   P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Write (This : access Typ;
                    P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                    P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   EndOfLineStringProperty : constant access Java.Lang.String.Typ'Class;

   --  final
   InsertContentAction_K : constant access Java.Lang.String.Typ'Class;

   --  final
   InsertBreakAction_K : constant access Java.Lang.String.Typ'Class;

   --  final
   InsertTabAction_K : constant access Java.Lang.String.Typ'Class;

   --  final
   DeletePrevCharAction : constant access Java.Lang.String.Typ'Class;

   --  final
   DeleteNextCharAction : constant access Java.Lang.String.Typ'Class;

   --  final
   DeleteNextWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   DeletePrevWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   ReadOnlyAction : constant access Java.Lang.String.Typ'Class;

   --  final
   WritableAction : constant access Java.Lang.String.Typ'Class;

   --  final
   CutAction_K : constant access Java.Lang.String.Typ'Class;

   --  final
   CopyAction_K : constant access Java.Lang.String.Typ'Class;

   --  final
   PasteAction_K : constant access Java.Lang.String.Typ'Class;

   --  final
   BeepAction_K : constant access Java.Lang.String.Typ'Class;

   --  final
   PageUpAction : constant access Java.Lang.String.Typ'Class;

   --  final
   PageDownAction : constant access Java.Lang.String.Typ'Class;

   --  final
   ForwardAction : constant access Java.Lang.String.Typ'Class;

   --  final
   BackwardAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionForwardAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionBackwardAction : constant access Java.Lang.String.Typ'Class;

   --  final
   UpAction : constant access Java.Lang.String.Typ'Class;

   --  final
   DownAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionUpAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionDownAction : constant access Java.Lang.String.Typ'Class;

   --  final
   BeginWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   EndWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionBeginWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionEndWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   PreviousWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   NextWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionPreviousWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionNextWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   BeginLineAction : constant access Java.Lang.String.Typ'Class;

   --  final
   EndLineAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionBeginLineAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionEndLineAction : constant access Java.Lang.String.Typ'Class;

   --  final
   BeginParagraphAction : constant access Java.Lang.String.Typ'Class;

   --  final
   EndParagraphAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionBeginParagraphAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionEndParagraphAction : constant access Java.Lang.String.Typ'Class;

   --  final
   BeginAction : constant access Java.Lang.String.Typ'Class;

   --  final
   EndAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionBeginAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectionEndAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectWordAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectLineAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectParagraphAction : constant access Java.Lang.String.Typ'Class;

   --  final
   SelectAllAction : constant access Java.Lang.String.Typ'Class;

   --  final
   DefaultKeyTypedAction_K : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultEditorKit);
   pragma Import (Java, GetContentType, "getContentType");
   pragma Import (Java, GetViewFactory, "getViewFactory");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, CreateCaret, "createCaret");
   pragma Import (Java, CreateDefaultDocument, "createDefaultDocument");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Write, "write");
   pragma Import (Java, EndOfLineStringProperty, "EndOfLineStringProperty");
   pragma Import (Java, InsertContentAction_K, "insertContentAction");
   pragma Import (Java, InsertBreakAction_K, "insertBreakAction");
   pragma Import (Java, InsertTabAction_K, "insertTabAction");
   pragma Import (Java, DeletePrevCharAction, "deletePrevCharAction");
   pragma Import (Java, DeleteNextCharAction, "deleteNextCharAction");
   pragma Import (Java, DeleteNextWordAction, "deleteNextWordAction");
   pragma Import (Java, DeletePrevWordAction, "deletePrevWordAction");
   pragma Import (Java, ReadOnlyAction, "readOnlyAction");
   pragma Import (Java, WritableAction, "writableAction");
   pragma Import (Java, CutAction_K, "cutAction");
   pragma Import (Java, CopyAction_K, "copyAction");
   pragma Import (Java, PasteAction_K, "pasteAction");
   pragma Import (Java, BeepAction_K, "beepAction");
   pragma Import (Java, PageUpAction, "pageUpAction");
   pragma Import (Java, PageDownAction, "pageDownAction");
   pragma Import (Java, ForwardAction, "forwardAction");
   pragma Import (Java, BackwardAction, "backwardAction");
   pragma Import (Java, SelectionForwardAction, "selectionForwardAction");
   pragma Import (Java, SelectionBackwardAction, "selectionBackwardAction");
   pragma Import (Java, UpAction, "upAction");
   pragma Import (Java, DownAction, "downAction");
   pragma Import (Java, SelectionUpAction, "selectionUpAction");
   pragma Import (Java, SelectionDownAction, "selectionDownAction");
   pragma Import (Java, BeginWordAction, "beginWordAction");
   pragma Import (Java, EndWordAction, "endWordAction");
   pragma Import (Java, SelectionBeginWordAction, "selectionBeginWordAction");
   pragma Import (Java, SelectionEndWordAction, "selectionEndWordAction");
   pragma Import (Java, PreviousWordAction, "previousWordAction");
   pragma Import (Java, NextWordAction, "nextWordAction");
   pragma Import (Java, SelectionPreviousWordAction, "selectionPreviousWordAction");
   pragma Import (Java, SelectionNextWordAction, "selectionNextWordAction");
   pragma Import (Java, BeginLineAction, "beginLineAction");
   pragma Import (Java, EndLineAction, "endLineAction");
   pragma Import (Java, SelectionBeginLineAction, "selectionBeginLineAction");
   pragma Import (Java, SelectionEndLineAction, "selectionEndLineAction");
   pragma Import (Java, BeginParagraphAction, "beginParagraphAction");
   pragma Import (Java, EndParagraphAction, "endParagraphAction");
   pragma Import (Java, SelectionBeginParagraphAction, "selectionBeginParagraphAction");
   pragma Import (Java, SelectionEndParagraphAction, "selectionEndParagraphAction");
   pragma Import (Java, BeginAction, "beginAction");
   pragma Import (Java, EndAction, "endAction");
   pragma Import (Java, SelectionBeginAction, "selectionBeginAction");
   pragma Import (Java, SelectionEndAction, "selectionEndAction");
   pragma Import (Java, SelectWordAction, "selectWordAction");
   pragma Import (Java, SelectLineAction, "selectLineAction");
   pragma Import (Java, SelectParagraphAction, "selectParagraphAction");
   pragma Import (Java, SelectAllAction, "selectAllAction");
   pragma Import (Java, DefaultKeyTypedAction_K, "defaultKeyTypedAction");

end Javax.Swing.Text.DefaultEditorKit;
pragma Import (Java, Javax.Swing.Text.DefaultEditorKit, "javax.swing.text.DefaultEditorKit");
pragma Extensions_Allowed (Off);
