pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Swing.Text.DocumentFilter;
limited with Javax.Swing.Text.NavigationFilter;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.JFormattedTextField.AbstractFormatter;
with Javax.Swing.JFormattedTextField;

package Javax.Swing.Text.DefaultFormatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Swing.JFormattedTextField.AbstractFormatter.Typ(Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultFormatter (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Install (This : access Typ;
                      P1_JFormattedTextField : access Standard.Javax.Swing.JFormattedTextField.Typ'Class);

   procedure SetCommitsOnValidEdit (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function GetCommitsOnValidEdit (This : access Typ)
                                   return Java.Boolean;

   procedure SetOverwriteMode (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function GetOverwriteMode (This : access Typ)
                              return Java.Boolean;

   procedure SetAllowsInvalid (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function GetAllowsInvalid (This : access Typ)
                              return Java.Boolean;

   procedure SetValueClass (This : access Typ;
                            P1_Class : access Standard.Java.Lang.Class.Typ'Class);

   function GetValueClass (This : access Typ)
                           return access Java.Lang.Class.Typ'Class;

   function StringToValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   function ValueToString (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.String.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   --  protected
   function GetDocumentFilter (This : access Typ)
                               return access Javax.Swing.Text.DocumentFilter.Typ'Class;

   --  protected
   function GetNavigationFilter (This : access Typ)
                                 return access Javax.Swing.Text.NavigationFilter.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultFormatter);
   pragma Import (Java, Install, "install");
   pragma Import (Java, SetCommitsOnValidEdit, "setCommitsOnValidEdit");
   pragma Import (Java, GetCommitsOnValidEdit, "getCommitsOnValidEdit");
   pragma Import (Java, SetOverwriteMode, "setOverwriteMode");
   pragma Import (Java, GetOverwriteMode, "getOverwriteMode");
   pragma Import (Java, SetAllowsInvalid, "setAllowsInvalid");
   pragma Import (Java, GetAllowsInvalid, "getAllowsInvalid");
   pragma Import (Java, SetValueClass, "setValueClass");
   pragma Import (Java, GetValueClass, "getValueClass");
   pragma Import (Java, StringToValue, "stringToValue");
   pragma Import (Java, ValueToString, "valueToString");
   pragma Import (Java, GetDocumentFilter, "getDocumentFilter");
   pragma Import (Java, GetNavigationFilter, "getNavigationFilter");
   pragma Import (Java, Clone, "clone");

end Javax.Swing.Text.DefaultFormatter;
pragma Import (Java, Javax.Swing.Text.DefaultFormatter, "javax.swing.text.DefaultFormatter");
pragma Extensions_Allowed (Off);
