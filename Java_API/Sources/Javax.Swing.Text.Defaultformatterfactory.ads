pragma Extensions_Allowed (On);
limited with Javax.Swing.JFormattedTextField.AbstractFormatter;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.JFormattedTextField.AbstractFormatterFactory;
with Javax.Swing.JFormattedTextField;

package Javax.Swing.Text.DefaultFormatterFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.JFormattedTextField.AbstractFormatterFactory.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultFormatterFactory (This : Ref := null)
                                         return Ref;

   function New_DefaultFormatterFactory (P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_DefaultFormatterFactory (P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;
                                         P2_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_DefaultFormatterFactory (P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;
                                         P2_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;
                                         P3_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_DefaultFormatterFactory (P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;
                                         P2_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;
                                         P3_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;
                                         P4_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDefaultFormatter (This : access Typ;
                                  P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class);

   function GetDefaultFormatter (This : access Typ)
                                 return access Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;

   procedure SetDisplayFormatter (This : access Typ;
                                  P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class);

   function GetDisplayFormatter (This : access Typ)
                                 return access Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;

   procedure SetEditFormatter (This : access Typ;
                               P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class);

   function GetEditFormatter (This : access Typ)
                              return access Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;

   procedure SetNullFormatter (This : access Typ;
                               P1_AbstractFormatter : access Standard.Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class);

   function GetNullFormatter (This : access Typ)
                              return access Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;

   function GetFormatter (This : access Typ;
                          P1_JFormattedTextField : access Standard.Javax.Swing.JFormattedTextField.Typ'Class)
                          return access Javax.Swing.JFormattedTextField.AbstractFormatter.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultFormatterFactory);
   pragma Import (Java, SetDefaultFormatter, "setDefaultFormatter");
   pragma Import (Java, GetDefaultFormatter, "getDefaultFormatter");
   pragma Import (Java, SetDisplayFormatter, "setDisplayFormatter");
   pragma Import (Java, GetDisplayFormatter, "getDisplayFormatter");
   pragma Import (Java, SetEditFormatter, "setEditFormatter");
   pragma Import (Java, GetEditFormatter, "getEditFormatter");
   pragma Import (Java, SetNullFormatter, "setNullFormatter");
   pragma Import (Java, GetNullFormatter, "getNullFormatter");
   pragma Import (Java, GetFormatter, "getFormatter");

end Javax.Swing.Text.DefaultFormatterFactory;
pragma Import (Java, Javax.Swing.Text.DefaultFormatterFactory, "javax.swing.text.DefaultFormatterFactory");
pragma Extensions_Allowed (Off);
