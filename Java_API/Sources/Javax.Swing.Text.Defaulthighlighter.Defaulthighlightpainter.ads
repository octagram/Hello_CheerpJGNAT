pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Text.Highlighter.HighlightPainter;
with Javax.Swing.Text.LayeredHighlighter.LayerPainter;

package Javax.Swing.Text.DefaultHighlighter.DefaultHighlightPainter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(HighlightPainter_I : Javax.Swing.Text.Highlighter.HighlightPainter.Ref)
    is new Javax.Swing.Text.LayeredHighlighter.LayerPainter.Typ(HighlightPainter_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultHighlightPainter (P1_Color : access Standard.Java.Awt.Color.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetColor (This : access Typ)
                      return access Java.Awt.Color.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                    P5_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class);

   function PaintLayer (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                        P5_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                        P6_View : access Standard.Javax.Swing.Text.View.Typ'Class)
                        return access Java.Awt.Shape.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultHighlightPainter);
   pragma Import (Java, GetColor, "getColor");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintLayer, "paintLayer");

end Javax.Swing.Text.DefaultHighlighter.DefaultHighlightPainter;
pragma Import (Java, Javax.Swing.Text.DefaultHighlighter.DefaultHighlightPainter, "javax.swing.text.DefaultHighlighter$DefaultHighlightPainter");
pragma Extensions_Allowed (Off);
