pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.Highlighter.Highlight;
limited with Javax.Swing.Text.Highlighter.HighlightPainter;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.LayeredHighlighter.LayerPainter;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Text.Highlighter;
with Javax.Swing.Text.LayeredHighlighter;

package Javax.Swing.Text.DefaultHighlighter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Highlighter_I : Javax.Swing.Text.Highlighter.Ref)
    is new Javax.Swing.Text.LayeredHighlighter.Typ(Highlighter_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultHighlighter (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure Install (This : access Typ;
                      P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class);

   procedure Deinstall (This : access Typ;
                        P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class);

   function AddHighlight (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_HighlightPainter : access Standard.Javax.Swing.Text.Highlighter.HighlightPainter.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure RemoveHighlight (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveAllHighlights (This : access Typ);

   procedure ChangeHighlight (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetHighlights (This : access Typ)
                           return Standard.Java.Lang.Object.Ref;

   procedure PaintLayeredHighlights (This : access Typ;
                                     P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                     P5_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                                     P6_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   procedure SetDrawsLayeredHighlights (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   function GetDrawsLayeredHighlights (This : access Typ)
                                       return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DefaultPainter : access Javax.Swing.Text.LayeredHighlighter.LayerPainter.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultHighlighter);
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Install, "install");
   pragma Import (Java, Deinstall, "deinstall");
   pragma Import (Java, AddHighlight, "addHighlight");
   pragma Import (Java, RemoveHighlight, "removeHighlight");
   pragma Import (Java, RemoveAllHighlights, "removeAllHighlights");
   pragma Import (Java, ChangeHighlight, "changeHighlight");
   pragma Import (Java, GetHighlights, "getHighlights");
   pragma Import (Java, PaintLayeredHighlights, "paintLayeredHighlights");
   pragma Import (Java, SetDrawsLayeredHighlights, "setDrawsLayeredHighlights");
   pragma Import (Java, GetDrawsLayeredHighlights, "getDrawsLayeredHighlights");
   pragma Import (Java, DefaultPainter, "DefaultPainter");

end Javax.Swing.Text.DefaultHighlighter;
pragma Import (Java, Javax.Swing.Text.DefaultHighlighter, "javax.swing.text.DefaultHighlighter");
pragma Extensions_Allowed (Off);
