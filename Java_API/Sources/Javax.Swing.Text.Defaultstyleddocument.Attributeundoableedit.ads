pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Undo.AbstractUndoableEdit;
with Javax.Swing.Undo.UndoableEdit;

package Javax.Swing.Text.DefaultStyledDocument.AttributeUndoableEdit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UndoableEdit_I : Javax.Swing.Undo.UndoableEdit.Ref)
    is new Javax.Swing.Undo.AbstractUndoableEdit.Typ(Serializable_I,
                                                     UndoableEdit_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      NewAttributes : access Javax.Swing.Text.AttributeSet.Typ'Class;
      pragma Import (Java, NewAttributes, "newAttributes");

      --  protected
      Copy : access Javax.Swing.Text.AttributeSet.Typ'Class;
      pragma Import (Java, Copy, "copy");

      --  protected
      IsReplacing : Java.Boolean;
      pragma Import (Java, IsReplacing, "isReplacing");

      --  protected
      Element : access Javax.Swing.Text.Element.Typ'Class;
      pragma Import (Java, Element, "element");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributeUndoableEdit (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                       P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                       P3_Boolean : Java.Boolean; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Redo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   procedure Undo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotUndoException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributeUndoableEdit);
   pragma Import (Java, Redo, "redo");
   pragma Import (Java, Undo, "undo");

end Javax.Swing.Text.DefaultStyledDocument.AttributeUndoableEdit;
pragma Import (Java, Javax.Swing.Text.DefaultStyledDocument.AttributeUndoableEdit, "javax.swing.text.DefaultStyledDocument$AttributeUndoableEdit");
pragma Extensions_Allowed (Off);
