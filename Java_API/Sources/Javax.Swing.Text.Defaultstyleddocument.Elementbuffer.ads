pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent;
limited with Javax.Swing.Text.DefaultStyledDocument.ElementSpec;
limited with Javax.Swing.Text.Element;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.DefaultStyledDocument.ElementBuffer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ElementBuffer (P1_DefaultStyledDocument : access Standard.Javax.Swing.Text.DefaultStyledDocument.Typ'Class;
                               P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRootElement (This : access Typ)
                            return access Javax.Swing.Text.Element.Typ'Class;

   procedure Insert (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_ElementSpec_Arr : access Javax.Swing.Text.DefaultStyledDocument.ElementSpec.Arr_Obj;
                     P4_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class);

   procedure Change (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int;
                     P3_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class);

   --  protected
   procedure InsertUpdate (This : access Typ;
                           P1_ElementSpec_Arr : access Javax.Swing.Text.DefaultStyledDocument.ElementSpec.Arr_Obj);

   --  protected
   procedure RemoveUpdate (This : access Typ);

   --  protected
   procedure ChangeUpdate (This : access Typ);

   function Clone (This : access Typ;
                   P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                   P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                   return access Javax.Swing.Text.Element.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ElementBuffer);
   pragma Import (Java, GetRootElement, "getRootElement");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Change, "change");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, ChangeUpdate, "changeUpdate");
   pragma Import (Java, Clone, "clone");

end Javax.Swing.Text.DefaultStyledDocument.ElementBuffer;
pragma Import (Java, Javax.Swing.Text.DefaultStyledDocument.ElementBuffer, "javax.swing.text.DefaultStyledDocument$ElementBuffer");
pragma Extensions_Allowed (Off);
