pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
with Java.Lang.Object;

package Javax.Swing.Text.DefaultStyledDocument.ElementSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ElementSpec (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                             P2_Short : Java.Short; 
                             This : Ref := null)
                             return Ref;

   function New_ElementSpec (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                             P2_Short : Java.Short;
                             P3_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_ElementSpec (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                             P2_Short : Java.Short;
                             P3_Char_Arr : Java.Char_Arr;
                             P4_Int : Java.Int;
                             P5_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetType (This : access Typ;
                      P1_Short : Java.Short);

   function GetType (This : access Typ)
                     return Java.Short;

   procedure SetDirection (This : access Typ;
                           P1_Short : Java.Short);

   function GetDirection (This : access Typ)
                          return Java.Short;

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetArray (This : access Typ)
                      return Java.Char_Arr;

   function GetOffset (This : access Typ)
                       return Java.Int;

   function GetLength (This : access Typ)
                       return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   StartTagType : constant Java.Short;

   --  final
   EndTagType : constant Java.Short;

   --  final
   ContentType : constant Java.Short;

   --  final
   JoinPreviousDirection : constant Java.Short;

   --  final
   JoinNextDirection : constant Java.Short;

   --  final
   OriginateDirection : constant Java.Short;

   --  final
   JoinFractureDirection : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ElementSpec);
   pragma Import (Java, SetType, "setType");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, SetDirection, "setDirection");
   pragma Import (Java, GetDirection, "getDirection");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetArray, "getArray");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, StartTagType, "StartTagType");
   pragma Import (Java, EndTagType, "EndTagType");
   pragma Import (Java, ContentType, "ContentType");
   pragma Import (Java, JoinPreviousDirection, "JoinPreviousDirection");
   pragma Import (Java, JoinNextDirection, "JoinNextDirection");
   pragma Import (Java, OriginateDirection, "OriginateDirection");
   pragma Import (Java, JoinFractureDirection, "JoinFractureDirection");

end Javax.Swing.Text.DefaultStyledDocument.ElementSpec;
pragma Import (Java, Javax.Swing.Text.DefaultStyledDocument.ElementSpec, "javax.swing.text.DefaultStyledDocument$ElementSpec");
pragma Extensions_Allowed (Off);
