pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Event.DocumentListener;
limited with Javax.Swing.Text.AbstractDocument.AbstractElement;
limited with Javax.Swing.Text.AbstractDocument.Content;
limited with Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.DefaultStyledDocument.ElementBuffer;
limited with Javax.Swing.Text.DefaultStyledDocument.ElementSpec;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Style;
limited with Javax.Swing.Text.StyleContext;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractDocument;
with Javax.Swing.Text.Document;
with Javax.Swing.Text.StyledDocument;

package Javax.Swing.Text.DefaultStyledDocument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Document_I : Javax.Swing.Text.Document.Ref;
            StyledDocument_I : Javax.Swing.Text.StyledDocument.Ref)
    is new Javax.Swing.Text.AbstractDocument.Typ(Serializable_I,
                                                 Document_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Buffer : access Javax.Swing.Text.DefaultStyledDocument.ElementBuffer.Typ'Class;
      pragma Import (Java, Buffer, "buffer");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultStyledDocument (P1_Content : access Standard.Javax.Swing.Text.AbstractDocument.Content.Typ'Class;
                                       P2_StyleContext : access Standard.Javax.Swing.Text.StyleContext.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_DefaultStyledDocument (P1_StyleContext : access Standard.Javax.Swing.Text.StyleContext.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_DefaultStyledDocument (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultRootElement (This : access Typ)
                                   return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   procedure Create (This : access Typ;
                     P1_ElementSpec_Arr : access Javax.Swing.Text.DefaultStyledDocument.ElementSpec.Arr_Obj);

   --  protected
   procedure Insert (This : access Typ;
                     P1_Int : Java.Int;
                     P2_ElementSpec_Arr : access Javax.Swing.Text.DefaultStyledDocument.ElementSpec.Arr_Obj);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function AddStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Style : access Standard.Javax.Swing.Text.Style.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class;

   procedure RemoveStyle (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class;

   function GetStyleNames (This : access Typ)
                           return access Java.Util.Enumeration.Typ'Class;

   procedure SetLogicalStyle (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Style : access Standard.Javax.Swing.Text.Style.Typ'Class);

   function GetLogicalStyle (This : access Typ;
                             P1_Int : Java.Int)
                             return access Javax.Swing.Text.Style.Typ'Class;

   procedure SetCharacterAttributes (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P4_Boolean : Java.Boolean);

   procedure SetParagraphAttributes (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P4_Boolean : Java.Boolean);

   function GetParagraphElement (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Javax.Swing.Text.Element.Typ'Class;

   function GetCharacterElement (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   procedure InsertUpdate (This : access Typ;
                           P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class;
                           P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   --  protected
   procedure RemoveUpdate (This : access Typ;
                           P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class);

   --  protected
   function CreateDefaultRoot (This : access Typ)
                               return access Javax.Swing.Text.AbstractDocument.AbstractElement.Typ'Class;

   function GetForeground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   function GetBackground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   function GetFont (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   --  protected
   procedure StyleChanged (This : access Typ;
                           P1_Style : access Standard.Javax.Swing.Text.Style.Typ'Class);

   procedure AddDocumentListener (This : access Typ;
                                  P1_DocumentListener : access Standard.Javax.Swing.Event.DocumentListener.Typ'Class);

   procedure RemoveDocumentListener (This : access Typ;
                                     P1_DocumentListener : access Standard.Javax.Swing.Event.DocumentListener.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BUFFER_SIZE_DEFAULT : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultStyledDocument);
   pragma Import (Java, GetDefaultRootElement, "getDefaultRootElement");
   pragma Import (Java, Create, "create");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, AddStyle, "addStyle");
   pragma Import (Java, RemoveStyle, "removeStyle");
   pragma Import (Java, GetStyle, "getStyle");
   pragma Import (Java, GetStyleNames, "getStyleNames");
   pragma Import (Java, SetLogicalStyle, "setLogicalStyle");
   pragma Import (Java, GetLogicalStyle, "getLogicalStyle");
   pragma Import (Java, SetCharacterAttributes, "setCharacterAttributes");
   pragma Import (Java, SetParagraphAttributes, "setParagraphAttributes");
   pragma Import (Java, GetParagraphElement, "getParagraphElement");
   pragma Import (Java, GetCharacterElement, "getCharacterElement");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, CreateDefaultRoot, "createDefaultRoot");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, StyleChanged, "styleChanged");
   pragma Import (Java, AddDocumentListener, "addDocumentListener");
   pragma Import (Java, RemoveDocumentListener, "removeDocumentListener");
   pragma Import (Java, BUFFER_SIZE_DEFAULT, "BUFFER_SIZE_DEFAULT");

end Javax.Swing.Text.DefaultStyledDocument;
pragma Import (Java, Javax.Swing.Text.DefaultStyledDocument, "javax.swing.text.DefaultStyledDocument");
pragma Extensions_Allowed (Off);
