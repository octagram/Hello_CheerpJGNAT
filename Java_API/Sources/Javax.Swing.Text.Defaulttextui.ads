pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.Plaf.Basic.BasicTextUI;
with Javax.Swing.Text.ViewFactory;

package Javax.Swing.Text.DefaultTextUI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ViewFactory_I : Javax.Swing.Text.ViewFactory.Ref)
    is abstract new Javax.Swing.Plaf.Basic.BasicTextUI.Typ(ViewFactory_I)
      with null record;

   function New_DefaultTextUI (This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTextUI);

end Javax.Swing.Text.DefaultTextUI;
pragma Import (Java, Javax.Swing.Text.DefaultTextUI, "javax.swing.text.DefaultTextUI");
pragma Extensions_Allowed (Off);
