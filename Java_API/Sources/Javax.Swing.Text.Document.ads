pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
limited with Java.Lang.String;
limited with Javax.Swing.Event.DocumentListener;
limited with Javax.Swing.Event.UndoableEditListener;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position;
limited with Javax.Swing.Text.Segment;
with Java.Lang.Object;

package Javax.Swing.Text.Document is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   procedure AddDocumentListener (This : access Typ;
                                  P1_DocumentListener : access Standard.Javax.Swing.Event.DocumentListener.Typ'Class) is abstract;

   procedure RemoveDocumentListener (This : access Typ;
                                     P1_DocumentListener : access Standard.Javax.Swing.Event.DocumentListener.Typ'Class) is abstract;

   procedure AddUndoableEditListener (This : access Typ;
                                      P1_UndoableEditListener : access Standard.Javax.Swing.Event.UndoableEditListener.Typ'Class) is abstract;

   procedure RemoveUndoableEditListener (This : access Typ;
                                         P1_UndoableEditListener : access Standard.Javax.Swing.Event.UndoableEditListener.Typ'Class) is abstract;

   function GetProperty (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   procedure PutProperty (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure InsertString (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetText (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure GetText (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetStartPosition (This : access Typ)
                              return access Javax.Swing.Text.Position.Typ'Class is abstract;

   function GetEndPosition (This : access Typ)
                            return access Javax.Swing.Text.Position.Typ'Class is abstract;

   function CreatePosition (This : access Typ;
                            P1_Int : Java.Int)
                            return access Javax.Swing.Text.Position.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetRootElements (This : access Typ)
                             return Standard.Java.Lang.Object.Ref is abstract;

   function GetDefaultRootElement (This : access Typ)
                                   return access Javax.Swing.Text.Element.Typ'Class is abstract;

   procedure Render (This : access Typ;
                     P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   StreamDescriptionProperty : constant access Java.Lang.String.Typ'Class;

   --  final
   TitleProperty : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, AddDocumentListener, "addDocumentListener");
   pragma Export (Java, RemoveDocumentListener, "removeDocumentListener");
   pragma Export (Java, AddUndoableEditListener, "addUndoableEditListener");
   pragma Export (Java, RemoveUndoableEditListener, "removeUndoableEditListener");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, PutProperty, "putProperty");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, InsertString, "insertString");
   pragma Export (Java, GetText, "getText");
   pragma Export (Java, GetStartPosition, "getStartPosition");
   pragma Export (Java, GetEndPosition, "getEndPosition");
   pragma Export (Java, CreatePosition, "createPosition");
   pragma Export (Java, GetRootElements, "getRootElements");
   pragma Export (Java, GetDefaultRootElement, "getDefaultRootElement");
   pragma Export (Java, Render, "render");
   pragma Import (Java, StreamDescriptionProperty, "StreamDescriptionProperty");
   pragma Import (Java, TitleProperty, "TitleProperty");

end Javax.Swing.Text.Document;
pragma Import (Java, Javax.Swing.Text.Document, "javax.swing.text.Document");
pragma Extensions_Allowed (Off);
