pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Document;
with Java.Lang.Object;

package Javax.Swing.Text.DocumentFilter.FilterBypass is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FilterBypass (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class is abstract;

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure InsertString (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_String : access Standard.Java.Lang.String.Typ'Class;
                      P4_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FilterBypass);
   pragma Export (Java, GetDocument, "getDocument");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, InsertString, "insertString");
   pragma Export (Java, Replace, "replace");

end Javax.Swing.Text.DocumentFilter.FilterBypass;
pragma Import (Java, Javax.Swing.Text.DocumentFilter.FilterBypass, "javax.swing.text.DocumentFilter$FilterBypass");
pragma Extensions_Allowed (Off);
