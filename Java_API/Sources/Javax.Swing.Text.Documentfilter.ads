pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.DocumentFilter.FilterBypass;
with Java.Lang.Object;

package Javax.Swing.Text.DocumentFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DocumentFilter (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Remove (This : access Typ;
                     P1_FilterBypass : access Standard.Javax.Swing.Text.DocumentFilter.FilterBypass.Typ'Class;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure InsertString (This : access Typ;
                           P1_FilterBypass : access Standard.Javax.Swing.Text.DocumentFilter.FilterBypass.Typ'Class;
                           P2_Int : Java.Int;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure Replace (This : access Typ;
                      P1_FilterBypass : access Standard.Javax.Swing.Text.DocumentFilter.FilterBypass.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_String : access Standard.Java.Lang.String.Typ'Class;
                      P5_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DocumentFilter);
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, InsertString, "insertString");
   pragma Import (Java, Replace, "replace");

end Javax.Swing.Text.DocumentFilter;
pragma Import (Java, Javax.Swing.Text.DocumentFilter, "javax.swing.text.DocumentFilter");
pragma Extensions_Allowed (Off);
