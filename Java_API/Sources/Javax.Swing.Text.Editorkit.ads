pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.JEditorPane;
limited with Javax.Swing.Text.Caret;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.ViewFactory;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Swing.Text.EditorKit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_EditorKit (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   procedure Install (This : access Typ;
                      P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class);

   procedure Deinstall (This : access Typ;
                        P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class);

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetViewFactory (This : access Typ)
                            return access Javax.Swing.Text.ViewFactory.Typ'Class is abstract;

   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref is abstract;

   function CreateCaret (This : access Typ)
                         return access Javax.Swing.Text.Caret.Typ'Class is abstract;

   function CreateDefaultDocument (This : access Typ)
                                   return access Javax.Swing.Text.Document.Typ'Class is abstract;

   procedure Read (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                   P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Write (This : access Typ;
                    P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                    P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Read (This : access Typ;
                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                   P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                   P3_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Write (This : access Typ;
                    P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                    P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EditorKit);
   pragma Export (Java, Clone, "clone");
   pragma Export (Java, Install, "install");
   pragma Export (Java, Deinstall, "deinstall");
   pragma Export (Java, GetContentType, "getContentType");
   pragma Export (Java, GetViewFactory, "getViewFactory");
   pragma Export (Java, GetActions, "getActions");
   pragma Export (Java, CreateCaret, "createCaret");
   pragma Export (Java, CreateDefaultDocument, "createDefaultDocument");
   pragma Export (Java, Read, "read");
   pragma Export (Java, Write, "write");

end Javax.Swing.Text.EditorKit;
pragma Import (Java, Javax.Swing.Text.EditorKit, "javax.swing.text.EditorKit");
pragma Extensions_Allowed (Off);
