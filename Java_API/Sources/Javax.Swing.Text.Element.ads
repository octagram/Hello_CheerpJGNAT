pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Document;
with Java.Lang.Object;

package Javax.Swing.Text.Element is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class is abstract;

   function GetParentElement (This : access Typ)
                              return access Javax.Swing.Text.Element.Typ'Class is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function GetStartOffset (This : access Typ)
                            return Java.Int is abstract;

   function GetEndOffset (This : access Typ)
                          return Java.Int is abstract;

   function GetElementIndex (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int is abstract;

   function GetElementCount (This : access Typ)
                             return Java.Int is abstract;

   function GetElement (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Text.Element.Typ'Class is abstract;

   function IsLeaf (This : access Typ)
                    return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDocument, "getDocument");
   pragma Export (Java, GetParentElement, "getParentElement");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetStartOffset, "getStartOffset");
   pragma Export (Java, GetEndOffset, "getEndOffset");
   pragma Export (Java, GetElementIndex, "getElementIndex");
   pragma Export (Java, GetElementCount, "getElementCount");
   pragma Export (Java, GetElement, "getElement");
   pragma Export (Java, IsLeaf, "isLeaf");

end Javax.Swing.Text.Element;
pragma Import (Java, Javax.Swing.Text.Element, "javax.swing.text.Element");
pragma Extensions_Allowed (Off);
