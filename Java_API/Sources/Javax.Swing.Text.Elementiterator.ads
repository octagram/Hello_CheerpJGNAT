pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Element;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Swing.Text.ElementIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ElementIterator (P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_ElementIterator (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function First (This : access Typ)
                   return access Javax.Swing.Text.Element.Typ'Class;

   function Depth (This : access Typ)
                   return Java.Int;

   function Current (This : access Typ)
                     return access Javax.Swing.Text.Element.Typ'Class;

   function Next (This : access Typ)
                  return access Javax.Swing.Text.Element.Typ'Class;

   function Previous (This : access Typ)
                      return access Javax.Swing.Text.Element.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ElementIterator);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, First, "first");
   pragma Import (Java, Depth, "depth");
   pragma Import (Java, Current, "current");
   pragma Import (Java, Next, "next");
   pragma Import (Java, Previous, "previous");

end Javax.Swing.Text.ElementIterator;
pragma Import (Java, Javax.Swing.Text.ElementIterator, "javax.swing.text.ElementIterator");
pragma Extensions_Allowed (Off);
