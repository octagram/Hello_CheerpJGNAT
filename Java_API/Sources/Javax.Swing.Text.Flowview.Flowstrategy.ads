pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;

package Javax.Swing.Text.FlowView.FlowStrategy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FlowStrategy (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InsertUpdate (This : access Typ;
                           P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class;
                           P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class;
                           P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class;
                            P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P3_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   --  protected
   function GetLogicalView (This : access Typ;
                            P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class)
                            return access Javax.Swing.Text.View.Typ'Class;

   procedure Layout (This : access Typ;
                     P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class);

   --  protected
   function LayoutRow (This : access Typ;
                       P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int)
                       return Java.Int;

   --  protected
   procedure AdjustRow (This : access Typ;
                        P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int);

   --  protected
   function CreateView (This : access Typ;
                        P1_FlowView : access Standard.Javax.Swing.Text.FlowView.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Int : Java.Int)
                        return access Javax.Swing.Text.View.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FlowStrategy);
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, ChangedUpdate, "changedUpdate");
   pragma Import (Java, GetLogicalView, "getLogicalView");
   pragma Import (Java, Layout, "layout");
   pragma Import (Java, LayoutRow, "layoutRow");
   pragma Import (Java, AdjustRow, "adjustRow");
   pragma Import (Java, CreateView, "createView");

end Javax.Swing.Text.FlowView.FlowStrategy;
pragma Import (Java, Javax.Swing.Text.FlowView.FlowStrategy, "javax.swing.text.FlowView$FlowStrategy");
pragma Extensions_Allowed (Off);
