pragma Extensions_Allowed (On);
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.SizeRequirements;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.FlowView.FlowStrategy;
limited with Javax.Swing.Text.View;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.BoxView;

package Javax.Swing.Text.FlowView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is abstract new Javax.Swing.Text.BoxView.Typ(SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      LayoutSpan : Java.Int;
      pragma Import (Java, LayoutSpan, "layoutSpan");

      --  protected
      LayoutPool : access Javax.Swing.Text.View.Typ'Class;
      pragma Import (Java, LayoutPool, "layoutPool");

      --  protected
      Strategy : access Javax.Swing.Text.FlowView.FlowStrategy.Typ'Class;
      pragma Import (Java, Strategy, "strategy");

   end record;

   function New_FlowView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                          P2_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFlowAxis (This : access Typ)
                         return Java.Int;

   function GetFlowSpan (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function GetFlowStart (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;

   --  protected
   function CreateRow (This : access Typ)
                       return access Javax.Swing.Text.View.Typ'Class is abstract;

   --  protected
   procedure LoadChildren (This : access Typ;
                           P1_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   --  protected
   function GetViewIndexAtPosition (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Java.Int;

   --  protected
   procedure Layout (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int);

   --  protected
   function CalculateMinorAxisRequirements (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                            return access Javax.Swing.SizeRequirements.Typ'Class;

   procedure InsertUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure SetParent (This : access Typ;
                        P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FlowView);
   pragma Export (Java, GetFlowAxis, "getFlowAxis");
   pragma Export (Java, GetFlowSpan, "getFlowSpan");
   pragma Export (Java, GetFlowStart, "getFlowStart");
   pragma Export (Java, CreateRow, "createRow");
   pragma Export (Java, LoadChildren, "loadChildren");
   pragma Export (Java, GetViewIndexAtPosition, "getViewIndexAtPosition");
   pragma Export (Java, Layout, "layout");
   pragma Export (Java, CalculateMinorAxisRequirements, "calculateMinorAxisRequirements");
   pragma Export (Java, InsertUpdate, "insertUpdate");
   pragma Export (Java, RemoveUpdate, "removeUpdate");
   pragma Export (Java, ChangedUpdate, "changedUpdate");
   pragma Export (Java, SetParent, "setParent");

end Javax.Swing.Text.FlowView;
pragma Import (Java, Javax.Swing.Text.FlowView, "javax.swing.text.FlowView");
pragma Extensions_Allowed (Off);
