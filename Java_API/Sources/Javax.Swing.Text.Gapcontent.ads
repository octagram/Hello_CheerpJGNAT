pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Swing.Text.Position;
limited with Javax.Swing.Text.Segment;
limited with Javax.Swing.Undo.UndoableEdit;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractDocument.Content;

package Javax.Swing.Text.GapContent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Content_I : Javax.Swing.Text.AbstractDocument.Content.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GapContent (This : Ref := null)
                            return Ref;

   function New_GapContent (P1_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function AllocateArray (This : access Typ;
                           P1_Int : Java.Int)
                           return access Java.Lang.Object.Typ'Class;

   --  protected
   function GetArrayLength (This : access Typ)
                            return Java.Int;

   function Length (This : access Typ)
                    return Java.Int;

   function InsertString (This : access Typ;
                          P1_Int : Java.Int;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.Undo.UndoableEdit.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function Remove (This : access Typ;
                    P1_Int : Java.Int;
                    P2_Int : Java.Int)
                    return access Javax.Swing.Undo.UndoableEdit.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetString (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure GetChars (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function CreatePosition (This : access Typ;
                            P1_Int : Java.Int)
                            return access Javax.Swing.Text.Position.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure ShiftEnd (This : access Typ;
                       P1_Int : Java.Int);

   --  protected
   procedure ShiftGap (This : access Typ;
                       P1_Int : Java.Int);

   --  protected
   procedure ResetMarksAtZero (This : access Typ);

   --  protected
   procedure ShiftGapStartDown (This : access Typ;
                                P1_Int : Java.Int);

   --  protected
   procedure ShiftGapEndUp (This : access Typ;
                            P1_Int : Java.Int);

   --  protected
   function GetPositionsInRange (This : access Typ;
                                 P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int)
                                 return access Java.Util.Vector.Typ'Class;

   --  protected
   procedure UpdateUndoPositions (This : access Typ;
                                  P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GapContent);
   pragma Import (Java, AllocateArray, "allocateArray");
   pragma Import (Java, GetArrayLength, "getArrayLength");
   pragma Import (Java, Length, "length");
   pragma Import (Java, InsertString, "insertString");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, GetString, "getString");
   pragma Import (Java, GetChars, "getChars");
   pragma Import (Java, CreatePosition, "createPosition");
   pragma Import (Java, ShiftEnd, "shiftEnd");
   pragma Import (Java, ShiftGap, "shiftGap");
   pragma Import (Java, ResetMarksAtZero, "resetMarksAtZero");
   pragma Import (Java, ShiftGapStartDown, "shiftGapStartDown");
   pragma Import (Java, ShiftGapEndUp, "shiftGapEndUp");
   pragma Import (Java, GetPositionsInRange, "getPositionsInRange");
   pragma Import (Java, UpdateUndoPositions, "updateUndoPositions");

end Javax.Swing.Text.GapContent;
pragma Import (Java, Javax.Swing.Text.GapContent, "javax.swing.text.GapContent");
pragma Extensions_Allowed (Off);
