pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.TabExpander;
with Java.Lang.Object;

package Javax.Swing.Text.GlyphView.GlyphPainter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_GlyphPainter (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSpan (This : access Typ;
                     P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int;
                     P4_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class;
                     P5_Float : Java.Float)
                     return Java.Float is abstract;

   function GetHeight (This : access Typ;
                       P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class)
                       return Java.Float is abstract;

   function GetAscent (This : access Typ;
                       P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class)
                       return Java.Float is abstract;

   function GetDescent (This : access Typ;
                        P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class)
                        return Java.Float is abstract;

   procedure Paint (This : access Typ;
                    P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class;
                    P2_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                    P4_Int : Java.Int;
                    P5_Int : Java.Int) is abstract;

   function ModelToView (This : access Typ;
                         P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class;
                         P2_Int : Java.Int;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                         P4_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class;
                         P2_Float : Java.Float;
                         P3_Float : Java.Float;
                         P4_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int is abstract;

   function GetBoundedPosition (This : access Typ;
                                P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Float : Java.Float;
                                P4_Float : Java.Float)
                                return Java.Int is abstract;

   function GetPainter (This : access Typ;
                        P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int)
                        return access Javax.Swing.Text.GlyphView.GlyphPainter.Typ'Class;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_GlyphView : access Standard.Javax.Swing.Text.GlyphView.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P4_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                       P5_Int : Java.Int;
                                       P6_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GlyphPainter);
   pragma Export (Java, GetSpan, "getSpan");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, GetAscent, "getAscent");
   pragma Export (Java, GetDescent, "getDescent");
   pragma Export (Java, Paint, "paint");
   pragma Export (Java, ModelToView, "modelToView");
   pragma Export (Java, ViewToModel, "viewToModel");
   pragma Export (Java, GetBoundedPosition, "getBoundedPosition");
   pragma Export (Java, GetPainter, "getPainter");
   pragma Export (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");

end Javax.Swing.Text.GlyphView.GlyphPainter;
pragma Import (Java, Javax.Swing.Text.GlyphView.GlyphPainter, "javax.swing.text.GlyphView$GlyphPainter");
pragma Extensions_Allowed (Off);
