pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.GlyphView.GlyphPainter;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.Segment;
limited with Javax.Swing.Text.TabExpander;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.TabableView;
with Javax.Swing.Text.View;

package Javax.Swing.Text.GlyphView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TabableView_I : Javax.Swing.Text.TabableView.Ref)
    is new Javax.Swing.Text.View.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GlyphView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetGlyphPainter (This : access Typ)
                             return access Javax.Swing.Text.GlyphView.GlyphPainter.Typ'Class;

   procedure SetGlyphPainter (This : access Typ;
                              P1_GlyphPainter : access Standard.Javax.Swing.Text.GlyphView.GlyphPainter.Typ'Class);

   function GetText (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Javax.Swing.Text.Segment.Typ'Class;

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetForeground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   function IsUnderline (This : access Typ)
                         return Java.Boolean;

   function IsStrikeThrough (This : access Typ)
                             return Java.Boolean;

   function IsSubscript (This : access Typ)
                         return Java.Boolean;

   function IsSuperscript (This : access Typ)
                           return Java.Boolean;

   function GetTabExpander (This : access Typ)
                            return access Javax.Swing.Text.TabExpander.Typ'Class;

   --  protected
   procedure CheckPainter (This : access Typ);

   function GetTabbedSpan (This : access Typ;
                           P1_Float : Java.Float;
                           P2_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class)
                           return Java.Float;

   function GetPartialSpan (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Float;

   function GetStartOffset (This : access Typ)
                            return Java.Int;

   function GetEndOffset (This : access Typ)
                          return Java.Int;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   function GetAlignment (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   function GetBreakWeight (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Float : Java.Float;
                            P3_Float : Java.Float)
                            return Java.Int;

   function BreakView (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Float : Java.Float;
                       P4_Float : Java.Float)
                       return access Javax.Swing.Text.View.Typ'Class;

   function CreateFragment (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Javax.Swing.Text.View.Typ'Class;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure InsertUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GlyphView);
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetGlyphPainter, "getGlyphPainter");
   pragma Import (Java, SetGlyphPainter, "setGlyphPainter");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, IsUnderline, "isUnderline");
   pragma Import (Java, IsStrikeThrough, "isStrikeThrough");
   pragma Import (Java, IsSubscript, "isSubscript");
   pragma Import (Java, IsSuperscript, "isSuperscript");
   pragma Import (Java, GetTabExpander, "getTabExpander");
   pragma Import (Java, CheckPainter, "checkPainter");
   pragma Import (Java, GetTabbedSpan, "getTabbedSpan");
   pragma Import (Java, GetPartialSpan, "getPartialSpan");
   pragma Import (Java, GetStartOffset, "getStartOffset");
   pragma Import (Java, GetEndOffset, "getEndOffset");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, GetBreakWeight, "getBreakWeight");
   pragma Import (Java, BreakView, "breakView");
   pragma Import (Java, CreateFragment, "createFragment");
   pragma Import (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, ChangedUpdate, "changedUpdate");

end Javax.Swing.Text.GlyphView;
pragma Import (Java, Javax.Swing.Text.GlyphView, "javax.swing.text.GlyphView");
pragma Extensions_Allowed (Off);
