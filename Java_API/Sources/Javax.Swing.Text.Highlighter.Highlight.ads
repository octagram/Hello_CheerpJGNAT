pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Highlighter.HighlightPainter;
with Java.Lang.Object;

package Javax.Swing.Text.Highlighter.Highlight is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStartOffset (This : access Typ)
                            return Java.Int is abstract;

   function GetEndOffset (This : access Typ)
                          return Java.Int is abstract;

   function GetPainter (This : access Typ)
                        return access Javax.Swing.Text.Highlighter.HighlightPainter.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetStartOffset, "getStartOffset");
   pragma Export (Java, GetEndOffset, "getEndOffset");
   pragma Export (Java, GetPainter, "getPainter");

end Javax.Swing.Text.Highlighter.Highlight;
pragma Import (Java, Javax.Swing.Text.Highlighter.Highlight, "javax.swing.text.Highlighter$Highlight");
pragma Extensions_Allowed (Off);
