pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.JTextComponent;
with Java.Lang.Object;

package Javax.Swing.Text.Highlighter.HighlightPainter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                    P5_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Paint, "paint");

end Javax.Swing.Text.Highlighter.HighlightPainter;
pragma Import (Java, Javax.Swing.Text.Highlighter.HighlightPainter, "javax.swing.text.Highlighter$HighlightPainter");
pragma Extensions_Allowed (Off);
