pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Javax.Swing.Text.Highlighter.Highlight;
limited with Javax.Swing.Text.Highlighter.HighlightPainter;
limited with Javax.Swing.Text.JTextComponent;
with Java.Lang.Object;

package Javax.Swing.Text.Highlighter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Install (This : access Typ;
                      P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class) is abstract;

   procedure Deinstall (This : access Typ;
                        P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class) is abstract;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class) is abstract;

   function AddHighlight (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_HighlightPainter : access Standard.Javax.Swing.Text.Highlighter.HighlightPainter.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure RemoveHighlight (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure RemoveAllHighlights (This : access Typ) is abstract;

   procedure ChangeHighlight (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int) is abstract;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetHighlights (This : access Typ)
                           return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Install, "install");
   pragma Export (Java, Deinstall, "deinstall");
   pragma Export (Java, Paint, "paint");
   pragma Export (Java, AddHighlight, "addHighlight");
   pragma Export (Java, RemoveHighlight, "removeHighlight");
   pragma Export (Java, RemoveAllHighlights, "removeAllHighlights");
   pragma Export (Java, ChangeHighlight, "changeHighlight");
   pragma Export (Java, GetHighlights, "getHighlights");

end Javax.Swing.Text.Highlighter;
pragma Import (Java, Javax.Swing.Text.Highlighter, "javax.swing.text.Highlighter");
pragma Extensions_Allowed (Off);
