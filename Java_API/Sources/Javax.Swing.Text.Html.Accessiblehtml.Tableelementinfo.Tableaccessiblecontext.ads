pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Cursor;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Accessibility.AccessibleRole;
limited with Javax.Accessibility.AccessibleStateSet;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Accessibility.AccessibleComponent;
with Javax.Accessibility.AccessibleContext;
with Javax.Accessibility.AccessibleTable;

package Javax.Swing.Text.Html.AccessibleHTML.TableElementInfo.TableAccessibleContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Accessible_I : Javax.Accessibility.Accessible.Ref;
            AccessibleComponent_I : Javax.Accessibility.AccessibleComponent.Ref;
            AccessibleTable_I : Javax.Accessibility.AccessibleTable.Ref)
    is new Javax.Accessibility.AccessibleContext.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessibleName (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   function GetAccessibleDescription (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetAccessibleRole (This : access Typ)
                               return access Javax.Accessibility.AccessibleRole.Typ'Class;

   function GetAccessibleIndexInParent (This : access Typ)
                                        return Java.Int;

   function GetAccessibleChildrenCount (This : access Typ)
                                        return Java.Int;

   function GetAccessibleChild (This : access Typ;
                                P1_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;

   function GetAccessibleTable (This : access Typ)
                                return access Javax.Accessibility.AccessibleTable.Typ'Class;

   function GetAccessibleCaption (This : access Typ)
                                  return access Javax.Accessibility.Accessible.Typ'Class;

   procedure SetAccessibleCaption (This : access Typ;
                                   P1_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class);

   function GetAccessibleSummary (This : access Typ)
                                  return access Javax.Accessibility.Accessible.Typ'Class;

   procedure SetAccessibleSummary (This : access Typ;
                                   P1_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class);

   function GetAccessibleRowCount (This : access Typ)
                                   return Java.Int;

   function GetAccessibleColumnCount (This : access Typ)
                                      return Java.Int;

   function GetAccessibleAt (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Int : Java.Int)
                             return access Javax.Accessibility.Accessible.Typ'Class;

   function GetAccessibleRowExtentAt (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_Int : Java.Int)
                                      return Java.Int;

   function GetAccessibleColumnExtentAt (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int)
                                         return Java.Int;

   function GetAccessibleRowHeader (This : access Typ)
                                    return access Javax.Accessibility.AccessibleTable.Typ'Class;

   procedure SetAccessibleRowHeader (This : access Typ;
                                     P1_AccessibleTable : access Standard.Javax.Accessibility.AccessibleTable.Typ'Class);

   function GetAccessibleColumnHeader (This : access Typ)
                                       return access Javax.Accessibility.AccessibleTable.Typ'Class;

   procedure SetAccessibleColumnHeader (This : access Typ;
                                        P1_AccessibleTable : access Standard.Javax.Accessibility.AccessibleTable.Typ'Class);

   function GetAccessibleRowDescription (This : access Typ;
                                         P1_Int : Java.Int)
                                         return access Javax.Accessibility.Accessible.Typ'Class;

   procedure SetAccessibleRowDescription (This : access Typ;
                                          P1_Int : Java.Int;
                                          P2_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class);

   function GetAccessibleColumnDescription (This : access Typ;
                                            P1_Int : Java.Int)
                                            return access Javax.Accessibility.Accessible.Typ'Class;

   procedure SetAccessibleColumnDescription (This : access Typ;
                                             P1_Int : Java.Int;
                                             P2_Accessible : access Standard.Javax.Accessibility.Accessible.Typ'Class);

   function IsAccessibleSelected (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int)
                                  return Java.Boolean;

   function IsAccessibleRowSelected (This : access Typ;
                                     P1_Int : Java.Int)
                                     return Java.Boolean;

   function IsAccessibleColumnSelected (This : access Typ;
                                        P1_Int : Java.Int)
                                        return Java.Boolean;

   function GetSelectedAccessibleRows (This : access Typ)
                                       return Java.Int_Arr;

   function GetSelectedAccessibleColumns (This : access Typ)
                                          return Java.Int_Arr;

   function GetAccessibleRow (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Int;

   function GetAccessibleColumn (This : access Typ;
                                 P1_Int : Java.Int)
                                 return Java.Int;

   function GetAccessibleIndex (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Int : Java.Int)
                                return Java.Int;

   function GetAccessibleRowHeader (This : access Typ;
                                    P1_Int : Java.Int)
                                    return access Java.Lang.String.Typ'Class;

   function GetAccessibleColumnHeader (This : access Typ;
                                       P1_Int : Java.Int)
                                       return access Java.Lang.String.Typ'Class;

   procedure RemoveFocusListener (This : access Typ;
                                  P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   procedure AddFocusListener (This : access Typ;
                               P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   procedure RequestFocus (This : access Typ);

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean;

   function GetAccessibleAt (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return access Javax.Accessibility.Accessible.Typ'Class;

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;

   procedure SetBounds (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   procedure SetLocation (This : access Typ;
                          P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   function GetLocationOnScreen (This : access Typ)
                                 return access Java.Awt.Point.Typ'Class;

   function Contains (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                      return Java.Boolean;

   function IsShowing (This : access Typ)
                       return Java.Boolean;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetCursor (This : access Typ)
                       return access Java.Awt.Cursor.Typ'Class;

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetForeground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetAccessibleComponent (This : access Typ)
                                    return access Javax.Accessibility.AccessibleComponent.Typ'Class;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;
   --  can raise Java.Awt.IllegalComponentStateException.Except

   function GetAccessibleStateSet (This : access Typ)
                                   return access Javax.Accessibility.AccessibleStateSet.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetAccessibleName, "getAccessibleName");
   pragma Import (Java, GetAccessibleDescription, "getAccessibleDescription");
   pragma Import (Java, GetAccessibleRole, "getAccessibleRole");
   pragma Import (Java, GetAccessibleIndexInParent, "getAccessibleIndexInParent");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");
   pragma Import (Java, GetAccessibleTable, "getAccessibleTable");
   pragma Import (Java, GetAccessibleCaption, "getAccessibleCaption");
   pragma Import (Java, SetAccessibleCaption, "setAccessibleCaption");
   pragma Import (Java, GetAccessibleSummary, "getAccessibleSummary");
   pragma Import (Java, SetAccessibleSummary, "setAccessibleSummary");
   pragma Import (Java, GetAccessibleRowCount, "getAccessibleRowCount");
   pragma Import (Java, GetAccessibleColumnCount, "getAccessibleColumnCount");
   pragma Import (Java, GetAccessibleAt, "getAccessibleAt");
   pragma Import (Java, GetAccessibleRowExtentAt, "getAccessibleRowExtentAt");
   pragma Import (Java, GetAccessibleColumnExtentAt, "getAccessibleColumnExtentAt");
   pragma Import (Java, GetAccessibleRowHeader, "getAccessibleRowHeader");
   pragma Import (Java, SetAccessibleRowHeader, "setAccessibleRowHeader");
   pragma Import (Java, GetAccessibleColumnHeader, "getAccessibleColumnHeader");
   pragma Import (Java, SetAccessibleColumnHeader, "setAccessibleColumnHeader");
   pragma Import (Java, GetAccessibleRowDescription, "getAccessibleRowDescription");
   pragma Import (Java, SetAccessibleRowDescription, "setAccessibleRowDescription");
   pragma Import (Java, GetAccessibleColumnDescription, "getAccessibleColumnDescription");
   pragma Import (Java, SetAccessibleColumnDescription, "setAccessibleColumnDescription");
   pragma Import (Java, IsAccessibleSelected, "isAccessibleSelected");
   pragma Import (Java, IsAccessibleRowSelected, "isAccessibleRowSelected");
   pragma Import (Java, IsAccessibleColumnSelected, "isAccessibleColumnSelected");
   pragma Import (Java, GetSelectedAccessibleRows, "getSelectedAccessibleRows");
   pragma Import (Java, GetSelectedAccessibleColumns, "getSelectedAccessibleColumns");
   pragma Import (Java, GetAccessibleRow, "getAccessibleRow");
   pragma Import (Java, GetAccessibleColumn, "getAccessibleColumn");
   pragma Import (Java, GetAccessibleIndex, "getAccessibleIndex");
   pragma Import (Java, RemoveFocusListener, "removeFocusListener");
   pragma Import (Java, AddFocusListener, "addFocusListener");
   pragma Import (Java, RequestFocus, "requestFocus");
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, SetBounds, "setBounds");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetLocationOnScreen, "getLocationOnScreen");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, IsShowing, "isShowing");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, SetCursor, "setCursor");
   pragma Import (Java, GetCursor, "getCursor");
   pragma Import (Java, SetForeground, "setForeground");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetAccessibleComponent, "getAccessibleComponent");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, GetAccessibleStateSet, "getAccessibleStateSet");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.Text.Html.AccessibleHTML.TableElementInfo.TableAccessibleContext;
pragma Import (Java, Javax.Swing.Text.Html.AccessibleHTML.TableElementInfo.TableAccessibleContext, "javax.swing.text.html.AccessibleHTML$TableElementInfo$TableAccessibleContext");
pragma Extensions_Allowed (Off);
