pragma Extensions_Allowed (On);
package Javax.Swing.Text.Html.AccessibleHTML.TableElementInfo is
   pragma Preelaborate;
end Javax.Swing.Text.Html.AccessibleHTML.TableElementInfo;
pragma Import (Java, Javax.Swing.Text.Html.AccessibleHTML.TableElementInfo, "javax.swing.text.html.AccessibleHTML$TableElementInfo");
pragma Extensions_Allowed (Off);
