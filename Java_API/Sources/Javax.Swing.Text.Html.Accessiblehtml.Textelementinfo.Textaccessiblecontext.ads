pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Cursor;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.FocusListener;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Accessibility.AccessibleRole;
limited with Javax.Accessibility.AccessibleStateSet;
limited with Javax.Swing.Text.AttributeSet;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Accessibility.AccessibleComponent;
with Javax.Accessibility.AccessibleContext;
with Javax.Accessibility.AccessibleText;

package Javax.Swing.Text.Html.AccessibleHTML.TextElementInfo.TextAccessibleContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Accessible_I : Javax.Accessibility.Accessible.Ref;
            AccessibleComponent_I : Javax.Accessibility.AccessibleComponent.Ref;
            AccessibleText_I : Javax.Accessibility.AccessibleText.Ref)
    is new Javax.Accessibility.AccessibleContext.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessibleText (This : access Typ)
                               return access Javax.Accessibility.AccessibleText.Typ'Class;

   function GetAccessibleName (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   function GetAccessibleDescription (This : access Typ)
                                      return access Java.Lang.String.Typ'Class;

   function GetAccessibleRole (This : access Typ)
                               return access Javax.Accessibility.AccessibleRole.Typ'Class;

   function GetIndexAtPoint (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return Java.Int;

   function GetCharacterBounds (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Awt.Rectangle.Typ'Class;

   function GetCharCount (This : access Typ)
                          return Java.Int;

   function GetCaretPosition (This : access Typ)
                              return Java.Int;

   function GetAtIndex (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class;

   function GetAfterIndex (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;

   function GetBeforeIndex (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   function GetCharacterAttribute (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetSelectionStart (This : access Typ)
                               return Java.Int;

   function GetSelectionEnd (This : access Typ)
                             return Java.Int;

   function GetSelectedText (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   procedure RemoveFocusListener (This : access Typ;
                                  P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   procedure AddFocusListener (This : access Typ;
                               P1_FocusListener : access Standard.Java.Awt.Event.FocusListener.Typ'Class);

   procedure RequestFocus (This : access Typ);

   function IsFocusTraversable (This : access Typ)
                                return Java.Boolean;

   function GetAccessibleAt (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return access Javax.Accessibility.Accessible.Typ'Class;

   procedure SetSize (This : access Typ;
                      P1_Dimension : access Standard.Java.Awt.Dimension.Typ'Class);

   function GetSize (This : access Typ)
                     return access Java.Awt.Dimension.Typ'Class;

   procedure SetBounds (This : access Typ;
                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   function GetBounds (This : access Typ)
                       return access Java.Awt.Rectangle.Typ'Class;

   procedure SetLocation (This : access Typ;
                          P1_Point : access Standard.Java.Awt.Point.Typ'Class);

   function GetLocation (This : access Typ)
                         return access Java.Awt.Point.Typ'Class;

   function GetLocationOnScreen (This : access Typ)
                                 return access Java.Awt.Point.Typ'Class;

   function Contains (This : access Typ;
                      P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                      return Java.Boolean;

   function IsShowing (This : access Typ)
                       return Java.Boolean;

   procedure SetVisible (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   procedure SetCursor (This : access Typ;
                        P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetCursor (This : access Typ)
                       return access Java.Awt.Cursor.Typ'Class;

   procedure SetForeground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetForeground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetAccessibleComponent (This : access Typ)
                                    return access Javax.Accessibility.AccessibleComponent.Typ'Class;

   function GetLocale (This : access Typ)
                       return access Java.Util.Locale.Typ'Class;
   --  can raise Java.Awt.IllegalComponentStateException.Except

   function GetAccessibleChild (This : access Typ;
                                P1_Int : Java.Int)
                                return access Javax.Accessibility.Accessible.Typ'Class;

   function GetAccessibleChildrenCount (This : access Typ)
                                        return Java.Int;

   function GetAccessibleIndexInParent (This : access Typ)
                                        return Java.Int;

   function GetAccessibleStateSet (This : access Typ)
                                   return access Javax.Accessibility.AccessibleStateSet.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetAccessibleText, "getAccessibleText");
   pragma Import (Java, GetAccessibleName, "getAccessibleName");
   pragma Import (Java, GetAccessibleDescription, "getAccessibleDescription");
   pragma Import (Java, GetAccessibleRole, "getAccessibleRole");
   pragma Import (Java, GetIndexAtPoint, "getIndexAtPoint");
   pragma Import (Java, GetCharacterBounds, "getCharacterBounds");
   pragma Import (Java, GetCharCount, "getCharCount");
   pragma Import (Java, GetCaretPosition, "getCaretPosition");
   pragma Import (Java, GetAtIndex, "getAtIndex");
   pragma Import (Java, GetAfterIndex, "getAfterIndex");
   pragma Import (Java, GetBeforeIndex, "getBeforeIndex");
   pragma Import (Java, GetCharacterAttribute, "getCharacterAttribute");
   pragma Import (Java, GetSelectionStart, "getSelectionStart");
   pragma Import (Java, GetSelectionEnd, "getSelectionEnd");
   pragma Import (Java, GetSelectedText, "getSelectedText");
   pragma Import (Java, RemoveFocusListener, "removeFocusListener");
   pragma Import (Java, AddFocusListener, "addFocusListener");
   pragma Import (Java, RequestFocus, "requestFocus");
   pragma Import (Java, IsFocusTraversable, "isFocusTraversable");
   pragma Import (Java, GetAccessibleAt, "getAccessibleAt");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetSize, "getSize");
   pragma Import (Java, SetBounds, "setBounds");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, SetLocation, "setLocation");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetLocationOnScreen, "getLocationOnScreen");
   pragma Import (Java, Contains, "contains");
   pragma Import (Java, IsShowing, "isShowing");
   pragma Import (Java, SetVisible, "setVisible");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, SetCursor, "setCursor");
   pragma Import (Java, GetCursor, "getCursor");
   pragma Import (Java, SetForeground, "setForeground");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetAccessibleComponent, "getAccessibleComponent");
   pragma Import (Java, GetLocale, "getLocale");
   pragma Import (Java, GetAccessibleChild, "getAccessibleChild");
   pragma Import (Java, GetAccessibleChildrenCount, "getAccessibleChildrenCount");
   pragma Import (Java, GetAccessibleIndexInParent, "getAccessibleIndexInParent");
   pragma Import (Java, GetAccessibleStateSet, "getAccessibleStateSet");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");

end Javax.Swing.Text.Html.AccessibleHTML.TextElementInfo.TextAccessibleContext;
pragma Import (Java, Javax.Swing.Text.Html.AccessibleHTML.TextElementInfo.TextAccessibleContext, "javax.swing.text.html.AccessibleHTML$TextElementInfo$TextAccessibleContext");
pragma Extensions_Allowed (Off);
