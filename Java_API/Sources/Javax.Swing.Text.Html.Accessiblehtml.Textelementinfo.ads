pragma Extensions_Allowed (On);
package Javax.Swing.Text.Html.AccessibleHTML.TextElementInfo is
   pragma Preelaborate;
end Javax.Swing.Text.Html.AccessibleHTML.TextElementInfo;
pragma Import (Java, Javax.Swing.Text.Html.AccessibleHTML.TextElementInfo, "javax.swing.text.html.AccessibleHTML$TextElementInfo");
pragma Extensions_Allowed (Off);
