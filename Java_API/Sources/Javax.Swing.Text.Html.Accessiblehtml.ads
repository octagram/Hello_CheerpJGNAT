pragma Extensions_Allowed (On);
package Javax.Swing.Text.Html.AccessibleHTML is
   pragma Preelaborate;
end Javax.Swing.Text.Html.AccessibleHTML;
pragma Import (Java, Javax.Swing.Text.Html.AccessibleHTML, "javax.swing.text.html.AccessibleHTML");
pragma Extensions_Allowed (Off);
