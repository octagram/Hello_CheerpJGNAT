pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Text.Html.CSS.Attribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetDefaultValue (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function IsInherited (This : access Typ)
                         return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BACKGROUND : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BACKGROUND_ATTACHMENT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BACKGROUND_COLOR : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BACKGROUND_IMAGE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BACKGROUND_POSITION : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BACKGROUND_REPEAT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_BOTTOM : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_BOTTOM_WIDTH : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_COLOR : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_LEFT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_LEFT_WIDTH : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_RIGHT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_RIGHT_WIDTH : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_STYLE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_TOP : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_TOP_WIDTH : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   BORDER_WIDTH : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   CLEAR : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   COLOR : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   DISPLAY : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   FLOAT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   FONT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   FONT_FAMILY : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   FONT_SIZE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   FONT_STYLE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   FONT_VARIANT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   FONT_WEIGHT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   HEIGHT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   LETTER_SPACING : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   LINE_HEIGHT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   LIST_STYLE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   LIST_STYLE_IMAGE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   LIST_STYLE_POSITION : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   LIST_STYLE_TYPE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   MARGIN : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   MARGIN_BOTTOM : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   MARGIN_LEFT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   MARGIN_RIGHT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   MARGIN_TOP : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   PADDING : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   PADDING_BOTTOM : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   PADDING_LEFT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   PADDING_RIGHT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   PADDING_TOP : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   TEXT_ALIGN : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   TEXT_DECORATION : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   TEXT_INDENT : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   TEXT_TRANSFORM : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   VERTICAL_ALIGN : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   WORD_SPACING : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   WHITE_SPACE : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;

   --  final
   WIDTH : access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetDefaultValue, "getDefaultValue");
   pragma Import (Java, IsInherited, "isInherited");
   pragma Import (Java, BACKGROUND, "BACKGROUND");
   pragma Import (Java, BACKGROUND_ATTACHMENT, "BACKGROUND_ATTACHMENT");
   pragma Import (Java, BACKGROUND_COLOR, "BACKGROUND_COLOR");
   pragma Import (Java, BACKGROUND_IMAGE, "BACKGROUND_IMAGE");
   pragma Import (Java, BACKGROUND_POSITION, "BACKGROUND_POSITION");
   pragma Import (Java, BACKGROUND_REPEAT, "BACKGROUND_REPEAT");
   pragma Import (Java, BORDER, "BORDER");
   pragma Import (Java, BORDER_BOTTOM, "BORDER_BOTTOM");
   pragma Import (Java, BORDER_BOTTOM_WIDTH, "BORDER_BOTTOM_WIDTH");
   pragma Import (Java, BORDER_COLOR, "BORDER_COLOR");
   pragma Import (Java, BORDER_LEFT, "BORDER_LEFT");
   pragma Import (Java, BORDER_LEFT_WIDTH, "BORDER_LEFT_WIDTH");
   pragma Import (Java, BORDER_RIGHT, "BORDER_RIGHT");
   pragma Import (Java, BORDER_RIGHT_WIDTH, "BORDER_RIGHT_WIDTH");
   pragma Import (Java, BORDER_STYLE, "BORDER_STYLE");
   pragma Import (Java, BORDER_TOP, "BORDER_TOP");
   pragma Import (Java, BORDER_TOP_WIDTH, "BORDER_TOP_WIDTH");
   pragma Import (Java, BORDER_WIDTH, "BORDER_WIDTH");
   pragma Import (Java, CLEAR, "CLEAR");
   pragma Import (Java, COLOR, "COLOR");
   pragma Import (Java, DISPLAY, "DISPLAY");
   pragma Import (Java, FLOAT, "FLOAT");
   pragma Import (Java, FONT, "FONT");
   pragma Import (Java, FONT_FAMILY, "FONT_FAMILY");
   pragma Import (Java, FONT_SIZE, "FONT_SIZE");
   pragma Import (Java, FONT_STYLE, "FONT_STYLE");
   pragma Import (Java, FONT_VARIANT, "FONT_VARIANT");
   pragma Import (Java, FONT_WEIGHT, "FONT_WEIGHT");
   pragma Import (Java, HEIGHT, "HEIGHT");
   pragma Import (Java, LETTER_SPACING, "LETTER_SPACING");
   pragma Import (Java, LINE_HEIGHT, "LINE_HEIGHT");
   pragma Import (Java, LIST_STYLE, "LIST_STYLE");
   pragma Import (Java, LIST_STYLE_IMAGE, "LIST_STYLE_IMAGE");
   pragma Import (Java, LIST_STYLE_POSITION, "LIST_STYLE_POSITION");
   pragma Import (Java, LIST_STYLE_TYPE, "LIST_STYLE_TYPE");
   pragma Import (Java, MARGIN, "MARGIN");
   pragma Import (Java, MARGIN_BOTTOM, "MARGIN_BOTTOM");
   pragma Import (Java, MARGIN_LEFT, "MARGIN_LEFT");
   pragma Import (Java, MARGIN_RIGHT, "MARGIN_RIGHT");
   pragma Import (Java, MARGIN_TOP, "MARGIN_TOP");
   pragma Import (Java, PADDING, "PADDING");
   pragma Import (Java, PADDING_BOTTOM, "PADDING_BOTTOM");
   pragma Import (Java, PADDING_LEFT, "PADDING_LEFT");
   pragma Import (Java, PADDING_RIGHT, "PADDING_RIGHT");
   pragma Import (Java, PADDING_TOP, "PADDING_TOP");
   pragma Import (Java, TEXT_ALIGN, "TEXT_ALIGN");
   pragma Import (Java, TEXT_DECORATION, "TEXT_DECORATION");
   pragma Import (Java, TEXT_INDENT, "TEXT_INDENT");
   pragma Import (Java, TEXT_TRANSFORM, "TEXT_TRANSFORM");
   pragma Import (Java, VERTICAL_ALIGN, "VERTICAL_ALIGN");
   pragma Import (Java, WORD_SPACING, "WORD_SPACING");
   pragma Import (Java, WHITE_SPACE, "WHITE_SPACE");
   pragma Import (Java, WIDTH, "WIDTH");

end Javax.Swing.Text.Html.CSS.Attribute;
pragma Import (Java, Javax.Swing.Text.Html.CSS.Attribute, "javax.swing.text.html.CSS$Attribute");
pragma Extensions_Allowed (Off);
