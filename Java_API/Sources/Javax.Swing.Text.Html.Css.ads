pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.Html.CSS.Attribute;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.Html.CSS is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CSS (This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAllAttributeKeys return Standard.Java.Lang.Object.Ref;

   --  final
   function GetAttribute (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CSS);
   pragma Import (Java, GetAllAttributeKeys, "getAllAttributeKeys");
   pragma Import (Java, GetAttribute, "getAttribute");

end Javax.Swing.Text.Html.CSS;
pragma Import (Java, Javax.Swing.Text.Html.CSS, "javax.swing.text.html.CSS");
pragma Extensions_Allowed (Off);
