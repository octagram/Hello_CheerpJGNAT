pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.Html.FormSubmitEvent.MethodType;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Html.HTMLFrameHyperlinkEvent;

package Javax.Swing.Text.Html.FormSubmitEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.Text.Html.HTMLFrameHyperlinkEvent.Typ(Serializable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMethod (This : access Typ)
                       return access Javax.Swing.Text.Html.FormSubmitEvent.MethodType.Typ'Class;

   function GetData (This : access Typ)
                     return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetMethod, "getMethod");
   pragma Import (Java, GetData, "getData");

end Javax.Swing.Text.Html.FormSubmitEvent;
pragma Import (Java, Javax.Swing.Text.Html.FormSubmitEvent, "javax.swing.text.html.FormSubmitEvent");
pragma Extensions_Allowed (Off);
