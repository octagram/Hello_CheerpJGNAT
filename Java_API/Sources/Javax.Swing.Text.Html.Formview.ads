pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Event.ActionEvent;
limited with Java.Lang.String;
limited with Javax.Swing.Text.Element;
with Java.Awt.Event.ActionListener;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.ComponentView;

package Javax.Swing.Text.Html.FormView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ActionListener_I : Java.Awt.Event.ActionListener.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.ComponentView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FormView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateComponent (This : access Typ)
                             return access Java.Awt.Component.Typ'Class;

   function GetMaximumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   --  protected
   procedure SubmitData (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure ImageSubmit (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FormView);
   pragma Import (Java, CreateComponent, "createComponent");
   pragma Import (Java, GetMaximumSpan, "getMaximumSpan");
   pragma Import (Java, ActionPerformed, "actionPerformed");
   pragma Import (Java, SubmitData, "submitData");
   pragma Import (Java, ImageSubmit, "imageSubmit");

end Javax.Swing.Text.Html.FormView;
pragma Import (Java, Javax.Swing.Text.Html.FormView, "javax.swing.text.html.FormView");
pragma Extensions_Allowed (Off);
