pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Text.Html.HTML.Attribute is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SIZE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   COLOR : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CLEAR : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   BACKGROUND : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   BGCOLOR : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   TEXT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   LINK : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   VLINK : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ALINK : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   WIDTH : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   HEIGHT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ALIGN : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   NAME : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   HREF : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   REL : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   REV : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   TITLE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   TARGET : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   SHAPE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   COORDS : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ISMAP : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   NOHREF : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ALT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ID : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   SRC : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   HSPACE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   VSPACE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   USEMAP : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   LOWSRC : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CODEBASE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CODE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ARCHIVE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   VALUE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   VALUETYPE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   TYPE_K : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CLASS : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   STYLE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   LANG : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   FACE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   DIR : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   DECLARE_K : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CLASSID : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   DATA : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CODETYPE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   STANDBY : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   BORDER : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   SHAPES : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   NOSHADE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   COMPACT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   START : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ACTION : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   METHOD : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ENCTYPE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CHECKED : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   MAXLENGTH : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   MULTIPLE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   SELECTED : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ROWS : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   COLS : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   DUMMY : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CELLSPACING : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CELLPADDING : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   VALIGN : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   HALIGN : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   NOWRAP : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ROWSPAN : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   COLSPAN : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   PROMPT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   HTTPEQUIV : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   CONTENT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   LANGUAGE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   VERSION : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   N : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   FRAMEBORDER : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   MARGINWIDTH : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   MARGINHEIGHT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   SCROLLING : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   NORESIZE : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   ENDTAG : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   --  final
   COMMENT : access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SIZE, "SIZE");
   pragma Import (Java, COLOR, "COLOR");
   pragma Import (Java, CLEAR, "CLEAR");
   pragma Import (Java, BACKGROUND, "BACKGROUND");
   pragma Import (Java, BGCOLOR, "BGCOLOR");
   pragma Import (Java, TEXT, "TEXT");
   pragma Import (Java, LINK, "LINK");
   pragma Import (Java, VLINK, "VLINK");
   pragma Import (Java, ALINK, "ALINK");
   pragma Import (Java, WIDTH, "WIDTH");
   pragma Import (Java, HEIGHT, "HEIGHT");
   pragma Import (Java, ALIGN, "ALIGN");
   pragma Import (Java, NAME, "NAME");
   pragma Import (Java, HREF, "HREF");
   pragma Import (Java, REL, "REL");
   pragma Import (Java, REV, "REV");
   pragma Import (Java, TITLE, "TITLE");
   pragma Import (Java, TARGET, "TARGET");
   pragma Import (Java, SHAPE, "SHAPE");
   pragma Import (Java, COORDS, "COORDS");
   pragma Import (Java, ISMAP, "ISMAP");
   pragma Import (Java, NOHREF, "NOHREF");
   pragma Import (Java, ALT, "ALT");
   pragma Import (Java, ID, "ID");
   pragma Import (Java, SRC, "SRC");
   pragma Import (Java, HSPACE, "HSPACE");
   pragma Import (Java, VSPACE, "VSPACE");
   pragma Import (Java, USEMAP, "USEMAP");
   pragma Import (Java, LOWSRC, "LOWSRC");
   pragma Import (Java, CODEBASE, "CODEBASE");
   pragma Import (Java, CODE, "CODE");
   pragma Import (Java, ARCHIVE, "ARCHIVE");
   pragma Import (Java, VALUE, "VALUE");
   pragma Import (Java, VALUETYPE, "VALUETYPE");
   pragma Import (Java, TYPE_K, "TYPE");
   pragma Import (Java, CLASS, "CLASS");
   pragma Import (Java, STYLE, "STYLE");
   pragma Import (Java, LANG, "LANG");
   pragma Import (Java, FACE, "FACE");
   pragma Import (Java, DIR, "DIR");
   pragma Import (Java, DECLARE_K, "DECLARE");
   pragma Import (Java, CLASSID, "CLASSID");
   pragma Import (Java, DATA, "DATA");
   pragma Import (Java, CODETYPE, "CODETYPE");
   pragma Import (Java, STANDBY, "STANDBY");
   pragma Import (Java, BORDER, "BORDER");
   pragma Import (Java, SHAPES, "SHAPES");
   pragma Import (Java, NOSHADE, "NOSHADE");
   pragma Import (Java, COMPACT, "COMPACT");
   pragma Import (Java, START, "START");
   pragma Import (Java, ACTION, "ACTION");
   pragma Import (Java, METHOD, "METHOD");
   pragma Import (Java, ENCTYPE, "ENCTYPE");
   pragma Import (Java, CHECKED, "CHECKED");
   pragma Import (Java, MAXLENGTH, "MAXLENGTH");
   pragma Import (Java, MULTIPLE, "MULTIPLE");
   pragma Import (Java, SELECTED, "SELECTED");
   pragma Import (Java, ROWS, "ROWS");
   pragma Import (Java, COLS, "COLS");
   pragma Import (Java, DUMMY, "DUMMY");
   pragma Import (Java, CELLSPACING, "CELLSPACING");
   pragma Import (Java, CELLPADDING, "CELLPADDING");
   pragma Import (Java, VALIGN, "VALIGN");
   pragma Import (Java, HALIGN, "HALIGN");
   pragma Import (Java, NOWRAP, "NOWRAP");
   pragma Import (Java, ROWSPAN, "ROWSPAN");
   pragma Import (Java, COLSPAN, "COLSPAN");
   pragma Import (Java, PROMPT, "PROMPT");
   pragma Import (Java, HTTPEQUIV, "HTTPEQUIV");
   pragma Import (Java, CONTENT, "CONTENT");
   pragma Import (Java, LANGUAGE, "LANGUAGE");
   pragma Import (Java, VERSION, "VERSION");
   pragma Import (Java, N, "N");
   pragma Import (Java, FRAMEBORDER, "FRAMEBORDER");
   pragma Import (Java, MARGINWIDTH, "MARGINWIDTH");
   pragma Import (Java, MARGINHEIGHT, "MARGINHEIGHT");
   pragma Import (Java, SCROLLING, "SCROLLING");
   pragma Import (Java, NORESIZE, "NORESIZE");
   pragma Import (Java, ENDTAG, "ENDTAG");
   pragma Import (Java, COMMENT, "COMMENT");

end Javax.Swing.Text.Html.HTML.Attribute;
pragma Import (Java, Javax.Swing.Text.Html.HTML.Attribute, "javax.swing.text.html.HTML$Attribute");
pragma Extensions_Allowed (Off);
