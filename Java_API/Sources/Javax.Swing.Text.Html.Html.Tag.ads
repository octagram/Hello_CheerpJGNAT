pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Text.Html.HTML.Tag is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Tag (This : Ref := null)
                     return Ref;

   --  protected
   function New_Tag (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;

   --  protected
   function New_Tag (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Boolean : Java.Boolean;
                     P3_Boolean : Java.Boolean; 
                     This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsBlock (This : access Typ)
                     return Java.Boolean;

   function BreaksFlow (This : access Typ)
                        return Java.Boolean;

   function IsPreformatted (This : access Typ)
                            return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   A : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   ADDRESS : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   APPLET : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   AREA : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   B : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   BASE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   BASEFONT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   BIG : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   BLOCKQUOTE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   BODY_K : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   BR : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   CAPTION : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   CENTER : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   CITE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   CODE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   DD : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   DFN : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   DIR : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   DIV : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   DL : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   DT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   EM : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   FONT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   FORM : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   FRAME : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   FRAMESET : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   H1 : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   H2 : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   H3 : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   H4 : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   H5 : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   H6 : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   HEAD : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   HR : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   HTML : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   I : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   IMG : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   INPUT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   ISINDEX : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   KBD : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   LI : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   LINK : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   MAP : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   MENU : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   META : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   NOFRAMES : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   OBJECT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   OL : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   OPTION : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   P : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   PARAM : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   PRE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   SAMP : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   SCRIPT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   SELECT_K : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   SMALL : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   SPAN : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   STRIKE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   S : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   STRONG : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   STYLE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   SUB : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   SUP : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   TABLE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   TD : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   TEXTAREA : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   TH : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   TITLE : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   TR : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   TT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   U : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   UL : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   VAR : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   IMPLIED : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   CONTENT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   --  final
   COMMENT : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Tag);
   pragma Import (Java, IsBlock, "isBlock");
   pragma Import (Java, BreaksFlow, "breaksFlow");
   pragma Import (Java, IsPreformatted, "isPreformatted");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, A, "A");
   pragma Import (Java, ADDRESS, "ADDRESS");
   pragma Import (Java, APPLET, "APPLET");
   pragma Import (Java, AREA, "AREA");
   pragma Import (Java, B, "B");
   pragma Import (Java, BASE, "BASE");
   pragma Import (Java, BASEFONT, "BASEFONT");
   pragma Import (Java, BIG, "BIG");
   pragma Import (Java, BLOCKQUOTE, "BLOCKQUOTE");
   pragma Import (Java, BODY_K, "BODY");
   pragma Import (Java, BR, "BR");
   pragma Import (Java, CAPTION, "CAPTION");
   pragma Import (Java, CENTER, "CENTER");
   pragma Import (Java, CITE, "CITE");
   pragma Import (Java, CODE, "CODE");
   pragma Import (Java, DD, "DD");
   pragma Import (Java, DFN, "DFN");
   pragma Import (Java, DIR, "DIR");
   pragma Import (Java, DIV, "DIV");
   pragma Import (Java, DL, "DL");
   pragma Import (Java, DT, "DT");
   pragma Import (Java, EM, "EM");
   pragma Import (Java, FONT, "FONT");
   pragma Import (Java, FORM, "FORM");
   pragma Import (Java, FRAME, "FRAME");
   pragma Import (Java, FRAMESET, "FRAMESET");
   pragma Import (Java, H1, "H1");
   pragma Import (Java, H2, "H2");
   pragma Import (Java, H3, "H3");
   pragma Import (Java, H4, "H4");
   pragma Import (Java, H5, "H5");
   pragma Import (Java, H6, "H6");
   pragma Import (Java, HEAD, "HEAD");
   pragma Import (Java, HR, "HR");
   pragma Import (Java, HTML, "HTML");
   pragma Import (Java, I, "I");
   pragma Import (Java, IMG, "IMG");
   pragma Import (Java, INPUT, "INPUT");
   pragma Import (Java, ISINDEX, "ISINDEX");
   pragma Import (Java, KBD, "KBD");
   pragma Import (Java, LI, "LI");
   pragma Import (Java, LINK, "LINK");
   pragma Import (Java, MAP, "MAP");
   pragma Import (Java, MENU, "MENU");
   pragma Import (Java, META, "META");
   pragma Import (Java, NOFRAMES, "NOFRAMES");
   pragma Import (Java, OBJECT, "OBJECT");
   pragma Import (Java, OL, "OL");
   pragma Import (Java, OPTION, "OPTION");
   pragma Import (Java, P, "P");
   pragma Import (Java, PARAM, "PARAM");
   pragma Import (Java, PRE, "PRE");
   pragma Import (Java, SAMP, "SAMP");
   pragma Import (Java, SCRIPT, "SCRIPT");
   pragma Import (Java, SELECT_K, "SELECT");
   pragma Import (Java, SMALL, "SMALL");
   pragma Import (Java, SPAN, "SPAN");
   pragma Import (Java, STRIKE, "STRIKE");
   pragma Import (Java, S, "S");
   pragma Import (Java, STRONG, "STRONG");
   pragma Import (Java, STYLE, "STYLE");
   pragma Import (Java, SUB, "SUB");
   pragma Import (Java, SUP, "SUP");
   pragma Import (Java, TABLE, "TABLE");
   pragma Import (Java, TD, "TD");
   pragma Import (Java, TEXTAREA, "TEXTAREA");
   pragma Import (Java, TH, "TH");
   pragma Import (Java, TITLE, "TITLE");
   pragma Import (Java, TR, "TR");
   pragma Import (Java, TT, "TT");
   pragma Import (Java, U, "U");
   pragma Import (Java, UL, "UL");
   pragma Import (Java, VAR, "VAR");
   pragma Import (Java, IMPLIED, "IMPLIED");
   pragma Import (Java, CONTENT, "CONTENT");
   pragma Import (Java, COMMENT, "COMMENT");

end Javax.Swing.Text.Html.HTML.Tag;
pragma Import (Java, Javax.Swing.Text.Html.HTML.Tag, "javax.swing.text.html.HTML$Tag");
pragma Extensions_Allowed (Off);
