pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Html.HTML.Tag;

package Javax.Swing.Text.Html.HTML.UnknownTag is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.Text.Html.HTML.Tag.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnknownTag (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UnknownTag);
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Javax.Swing.Text.Html.HTML.UnknownTag;
pragma Import (Java, Javax.Swing.Text.Html.HTML.UnknownTag, "javax.swing.text.html.HTML$UnknownTag");
pragma Extensions_Allowed (Off);
