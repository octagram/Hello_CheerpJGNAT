pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Html.HTML.Attribute;
limited with Javax.Swing.Text.Html.HTML.Tag;
with Java.Lang.Object;

package Javax.Swing.Text.Html.HTML is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HTML (This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAllTags return Standard.Java.Lang.Object.Ref;

   function GetTag (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   function GetIntegerAttributeValue (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                      P2_Attribute : access Standard.Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;
                                      P3_Int : Java.Int)
                                      return Java.Int;

   function GetAllAttributeKeys return Standard.Java.Lang.Object.Ref;

   function GetAttributeKey (P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Swing.Text.Html.HTML.Attribute.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NULL_ATTRIBUTE_VALUE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HTML);
   pragma Import (Java, GetAllTags, "getAllTags");
   pragma Import (Java, GetTag, "getTag");
   pragma Import (Java, GetIntegerAttributeValue, "getIntegerAttributeValue");
   pragma Import (Java, GetAllAttributeKeys, "getAllAttributeKeys");
   pragma Import (Java, GetAttributeKey, "getAttributeKey");
   pragma Import (Java, NULL_ATTRIBUTE_VALUE, "NULL_ATTRIBUTE_VALUE");

end Javax.Swing.Text.Html.HTML;
pragma Import (Java, Javax.Swing.Text.Html.HTML, "javax.swing.text.html.HTML");
pragma Extensions_Allowed (Off);
