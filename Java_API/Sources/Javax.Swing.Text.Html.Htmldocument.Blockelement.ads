pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractDocument.BranchElement;
with Javax.Swing.Text.Element;
with Javax.Swing.Text.MutableAttributeSet;
with Javax.Swing.Tree.TreeNode;

package Javax.Swing.Text.Html.HTMLDocument.BlockElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Element_I : Javax.Swing.Text.Element.Ref;
            MutableAttributeSet_I : Javax.Swing.Text.MutableAttributeSet.Ref;
            TreeNode_I : Javax.Swing.Tree.TreeNode.Ref)
    is new Javax.Swing.Text.AbstractDocument.BranchElement.Typ(Serializable_I,
                                                               Element_I,
                                                               MutableAttributeSet_I,
                                                               TreeNode_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_BlockElement (P1_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                              P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                              P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetResolveParent (This : access Typ)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_BlockElement);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetResolveParent, "getResolveParent");

end Javax.Swing.Text.Html.HTMLDocument.BlockElement;
pragma Import (Java, Javax.Swing.Text.Html.HTMLDocument.BlockElement, "javax.swing.text.html.HTMLDocument$BlockElement");
pragma Extensions_Allowed (Off);
