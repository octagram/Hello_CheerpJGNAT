pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.MutableAttributeSet;
with Java.Lang.Object;
with Javax.Swing.Text.Html.HTMLDocument.HTMLReader.TagAction;

package Javax.Swing.Text.Html.HTMLDocument.HTMLReader.IsindexAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Text.Html.HTMLDocument.HTMLReader.TagAction.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IsindexAction (P1_HTMLReader : access Standard.Javax.Swing.Text.Html.HTMLDocument.HTMLReader.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Start (This : access Typ;
                    P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                    P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IsindexAction);
   pragma Import (Java, Start, "start");

end Javax.Swing.Text.Html.HTMLDocument.HTMLReader.IsindexAction;
pragma Import (Java, Javax.Swing.Text.Html.HTMLDocument.HTMLReader.IsindexAction, "javax.swing.text.html.HTMLDocument$HTMLReader$IsindexAction");
pragma Extensions_Allowed (Off);
