pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.HTMLDocument.HTMLReader.TagAction;
limited with Javax.Swing.Text.MutableAttributeSet;
with Java.Lang.Object;
with Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback;

package Javax.Swing.Text.Html.HTMLDocument.HTMLReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ParseBuffer : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, ParseBuffer, "parseBuffer");

      --  protected
      CharAttr : access Javax.Swing.Text.MutableAttributeSet.Typ'Class;
      pragma Import (Java, CharAttr, "charAttr");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HTMLReader (P1_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                            P2_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   function New_HTMLReader (P1_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int;
                            P5_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Flush (This : access Typ);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure HandleText (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int);

   procedure HandleStartTag (This : access Typ;
                             P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                             P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                             P3_Int : Java.Int);

   procedure HandleComment (This : access Typ;
                            P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int);

   procedure HandleEndTag (This : access Typ;
                           P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                           P2_Int : Java.Int);

   procedure HandleSimpleTag (This : access Typ;
                              P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                              P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                              P3_Int : Java.Int);

   procedure HandleEndOfLineString (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure RegisterTag (This : access Typ;
                          P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                          P2_TagAction : access Standard.Javax.Swing.Text.Html.HTMLDocument.HTMLReader.TagAction.Typ'Class);

   --  protected
   procedure PushCharacterStyle (This : access Typ);

   --  protected
   procedure PopCharacterStyle (This : access Typ);

   --  protected
   procedure TextAreaContent (This : access Typ;
                              P1_Char_Arr : Java.Char_Arr);

   --  protected
   procedure PreContent (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr);

   --  protected
   procedure BlockOpen (This : access Typ;
                        P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                        P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class);

   --  protected
   procedure BlockClose (This : access Typ;
                         P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class);

   --  protected
   procedure AddContent (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);

   --  protected
   procedure AddContent (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Boolean : Java.Boolean);

   --  protected
   procedure AddSpecialElement (This : access Typ;
                                P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                                P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HTMLReader);
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, HandleText, "handleText");
   pragma Import (Java, HandleStartTag, "handleStartTag");
   pragma Import (Java, HandleComment, "handleComment");
   pragma Import (Java, HandleEndTag, "handleEndTag");
   pragma Import (Java, HandleSimpleTag, "handleSimpleTag");
   pragma Import (Java, HandleEndOfLineString, "handleEndOfLineString");
   pragma Import (Java, RegisterTag, "registerTag");
   pragma Import (Java, PushCharacterStyle, "pushCharacterStyle");
   pragma Import (Java, PopCharacterStyle, "popCharacterStyle");
   pragma Import (Java, TextAreaContent, "textAreaContent");
   pragma Import (Java, PreContent, "preContent");
   pragma Import (Java, BlockOpen, "blockOpen");
   pragma Import (Java, BlockClose, "blockClose");
   pragma Import (Java, AddContent, "addContent");
   pragma Import (Java, AddSpecialElement, "addSpecialElement");

end Javax.Swing.Text.Html.HTMLDocument.HTMLReader;
pragma Import (Java, Javax.Swing.Text.Html.HTMLDocument.HTMLReader, "javax.swing.text.html.HTMLDocument$HTMLReader");
pragma Extensions_Allowed (Off);
