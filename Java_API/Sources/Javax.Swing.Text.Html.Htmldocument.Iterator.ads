pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Html.HTML.Tag;
with Java.Lang.Object;

package Javax.Swing.Text.Html.HTMLDocument.Iterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Iterator (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class is abstract;

   function GetStartOffset (This : access Typ)
                            return Java.Int is abstract;

   function GetEndOffset (This : access Typ)
                          return Java.Int is abstract;

   procedure Next (This : access Typ) is abstract;

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;

   function GetTag (This : access Typ)
                    return access Javax.Swing.Text.Html.HTML.Tag.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Iterator);
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetStartOffset, "getStartOffset");
   pragma Export (Java, GetEndOffset, "getEndOffset");
   pragma Export (Java, Next, "next");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, GetTag, "getTag");

end Javax.Swing.Text.Html.HTMLDocument.Iterator;
pragma Import (Java, Javax.Swing.Text.Html.HTMLDocument.Iterator, "javax.swing.text.html.HTMLDocument$Iterator");
pragma Extensions_Allowed (Off);
