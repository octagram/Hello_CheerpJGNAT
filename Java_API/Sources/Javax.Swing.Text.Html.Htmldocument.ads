pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Event.UndoableEditEvent;
limited with Javax.Swing.Text.AbstractDocument.AbstractElement;
limited with Javax.Swing.Text.AbstractDocument.Content;
limited with Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.DefaultStyledDocument.ElementSpec;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.HTMLDocument.Iterator;
limited with Javax.Swing.Text.Html.HTMLEditorKit.Parser;
limited with Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback;
limited with Javax.Swing.Text.Html.HTMLFrameHyperlinkEvent;
limited with Javax.Swing.Text.Html.StyleSheet;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.DefaultStyledDocument;
with Javax.Swing.Text.Document;
with Javax.Swing.Text.StyledDocument;

package Javax.Swing.Text.Html.HTMLDocument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Document_I : Javax.Swing.Text.Document.Ref;
            StyledDocument_I : Javax.Swing.Text.StyledDocument.Ref)
    is new Javax.Swing.Text.DefaultStyledDocument.Typ(Serializable_I,
                                                      Document_I,
                                                      StyledDocument_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HTMLDocument (This : Ref := null)
                              return Ref;

   function New_HTMLDocument (P1_StyleSheet : access Standard.Javax.Swing.Text.Html.StyleSheet.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_HTMLDocument (P1_Content : access Standard.Javax.Swing.Text.AbstractDocument.Content.Typ'Class;
                              P2_StyleSheet : access Standard.Javax.Swing.Text.Html.StyleSheet.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetReader (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback.Typ'Class;

   function GetReader (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int;
                       P4_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class)
                       return access Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback.Typ'Class;

   function GetBase (This : access Typ)
                     return access Java.Net.URL.Typ'Class;

   procedure SetBase (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class);

   --  protected
   procedure Insert (This : access Typ;
                     P1_Int : Java.Int;
                     P2_ElementSpec_Arr : access Javax.Swing.Text.DefaultStyledDocument.ElementSpec.Arr_Obj);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure InsertUpdate (This : access Typ;
                           P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class;
                           P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   --  protected
   procedure Create (This : access Typ;
                     P1_ElementSpec_Arr : access Javax.Swing.Text.DefaultStyledDocument.ElementSpec.Arr_Obj);

   procedure SetParagraphAttributes (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P4_Boolean : Java.Boolean);

   function GetStyleSheet (This : access Typ)
                           return access Javax.Swing.Text.Html.StyleSheet.Typ'Class;

   function GetIterator (This : access Typ;
                         P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class)
                         return access Javax.Swing.Text.Html.HTMLDocument.Iterator.Typ'Class;

   --  protected
   function CreateLeafElement (This : access Typ;
                               P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                               P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int)
                               return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   function CreateBranchElement (This : access Typ;
                                 P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                 P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                 return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   function CreateDefaultRoot (This : access Typ)
                               return access Javax.Swing.Text.AbstractDocument.AbstractElement.Typ'Class;

   procedure SetTokenThreshold (This : access Typ;
                                P1_Int : Java.Int);

   function GetTokenThreshold (This : access Typ)
                               return Java.Int;

   procedure SetPreservesUnknownTags (This : access Typ;
                                      P1_Boolean : Java.Boolean);

   function GetPreservesUnknownTags (This : access Typ)
                                     return Java.Boolean;

   procedure ProcessHTMLFrameHyperlinkEvent (This : access Typ;
                                             P1_HTMLFrameHyperlinkEvent : access Standard.Javax.Swing.Text.Html.HTMLFrameHyperlinkEvent.Typ'Class);

   procedure SetParser (This : access Typ;
                        P1_Parser : access Standard.Javax.Swing.Text.Html.HTMLEditorKit.Parser.Typ'Class);

   function GetParser (This : access Typ)
                       return access Javax.Swing.Text.Html.HTMLEditorKit.Parser.Typ'Class;

   procedure SetInnerHTML (This : access Typ;
                           P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   procedure SetOuterHTML (This : access Typ;
                           P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   procedure InsertAfterStart (This : access Typ;
                               P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   procedure InsertBeforeEnd (This : access Typ;
                              P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   procedure InsertBeforeStart (This : access Typ;
                                P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   procedure InsertAfterEnd (This : access Typ;
                             P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   function GetElement (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Swing.Text.Element.Typ'Class;

   function GetElement (This : access Typ;
                        P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   procedure FireChangedUpdate (This : access Typ;
                                P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class);

   --  protected
   procedure FireUndoableEditUpdate (This : access Typ;
                                     P1_UndoableEditEvent : access Standard.Javax.Swing.Event.UndoableEditEvent.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   AdditionalComments : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HTMLDocument);
   pragma Import (Java, GetReader, "getReader");
   pragma Import (Java, GetBase, "getBase");
   pragma Import (Java, SetBase, "setBase");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, Create, "create");
   pragma Import (Java, SetParagraphAttributes, "setParagraphAttributes");
   pragma Import (Java, GetStyleSheet, "getStyleSheet");
   pragma Import (Java, GetIterator, "getIterator");
   pragma Import (Java, CreateLeafElement, "createLeafElement");
   pragma Import (Java, CreateBranchElement, "createBranchElement");
   pragma Import (Java, CreateDefaultRoot, "createDefaultRoot");
   pragma Import (Java, SetTokenThreshold, "setTokenThreshold");
   pragma Import (Java, GetTokenThreshold, "getTokenThreshold");
   pragma Import (Java, SetPreservesUnknownTags, "setPreservesUnknownTags");
   pragma Import (Java, GetPreservesUnknownTags, "getPreservesUnknownTags");
   pragma Import (Java, ProcessHTMLFrameHyperlinkEvent, "processHTMLFrameHyperlinkEvent");
   pragma Import (Java, SetParser, "setParser");
   pragma Import (Java, GetParser, "getParser");
   pragma Import (Java, SetInnerHTML, "setInnerHTML");
   pragma Import (Java, SetOuterHTML, "setOuterHTML");
   pragma Import (Java, InsertAfterStart, "insertAfterStart");
   pragma Import (Java, InsertBeforeEnd, "insertBeforeEnd");
   pragma Import (Java, InsertBeforeStart, "insertBeforeStart");
   pragma Import (Java, InsertAfterEnd, "insertAfterEnd");
   pragma Import (Java, GetElement, "getElement");
   pragma Import (Java, FireChangedUpdate, "fireChangedUpdate");
   pragma Import (Java, FireUndoableEditUpdate, "fireUndoableEditUpdate");
   pragma Import (Java, AdditionalComments, "AdditionalComments");

end Javax.Swing.Text.Html.HTMLDocument;
pragma Import (Java, Javax.Swing.Text.Html.HTMLDocument, "javax.swing.text.html.HTMLDocument");
pragma Extensions_Allowed (Off);
