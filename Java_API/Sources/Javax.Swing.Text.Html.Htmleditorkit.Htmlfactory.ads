pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Text.ViewFactory;

package Javax.Swing.Text.Html.HTMLEditorKit.HTMLFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ViewFactory_I : Javax.Swing.Text.ViewFactory.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HTMLFactory (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create (This : access Typ;
                    P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                    return access Javax.Swing.Text.View.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HTMLFactory);
   pragma Import (Java, Create, "create");

end Javax.Swing.Text.Html.HTMLEditorKit.HTMLFactory;
pragma Import (Java, Javax.Swing.Text.Html.HTMLEditorKit.HTMLFactory, "javax.swing.text.html.HTMLEditorKit$HTMLFactory");
pragma Extensions_Allowed (Off);
