pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.JEditorPane;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.HTMLDocument;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Action;
with Javax.Swing.Text.StyledEditorKit.StyledTextAction;

package Javax.Swing.Text.Html.HTMLEditorKit.HTMLTextAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Action_I : Javax.Swing.Action.Ref)
    is abstract new Javax.Swing.Text.StyledEditorKit.StyledTextAction.Typ(Serializable_I,
                                                                          Cloneable_I,
                                                                          Action_I)
      with null record;

   function New_HTMLTextAction (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetHTMLDocument (This : access Typ;
                             P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class)
                             return access Javax.Swing.Text.Html.HTMLDocument.Typ'Class;

   --  protected
   function GetHTMLEditorKit (This : access Typ;
                              P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class)
                              return access Javax.Swing.Text.Html.HTMLEditorKit.Typ'Class;

   --  protected
   function GetElementsAt (This : access Typ;
                           P1_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                           P2_Int : Java.Int)
                           return Standard.Java.Lang.Object.Ref;

   --  protected
   function ElementCountToTag (This : access Typ;
                               P1_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class)
                               return Java.Int;

   --  protected
   function FindElementMatchingTag (This : access Typ;
                                    P1_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                                    P2_Int : Java.Int;
                                    P3_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class)
                                    return access Javax.Swing.Text.Element.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HTMLTextAction);
   pragma Import (Java, GetHTMLDocument, "getHTMLDocument");
   pragma Import (Java, GetHTMLEditorKit, "getHTMLEditorKit");
   pragma Import (Java, GetElementsAt, "getElementsAt");
   pragma Import (Java, ElementCountToTag, "elementCountToTag");
   pragma Import (Java, FindElementMatchingTag, "findElementMatchingTag");

end Javax.Swing.Text.Html.HTMLEditorKit.HTMLTextAction;
pragma Import (Java, Javax.Swing.Text.Html.HTMLEditorKit.HTMLTextAction, "javax.swing.text.html.HTMLEditorKit$HTMLTextAction");
pragma Extensions_Allowed (Off);
