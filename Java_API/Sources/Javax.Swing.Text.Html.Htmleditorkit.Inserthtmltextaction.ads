pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Lang.String;
limited with Javax.Swing.JEditorPane;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.HTMLDocument;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Action;
with Javax.Swing.Text.Html.HTMLEditorKit.HTMLTextAction;

package Javax.Swing.Text.Html.HTMLEditorKit.InsertHTMLTextAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Action_I : Javax.Swing.Action.Ref)
    is new Javax.Swing.Text.Html.HTMLEditorKit.HTMLTextAction.Typ(Serializable_I,
                                                                  Cloneable_I,
                                                                  Action_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Html : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Html, "html");

      --  protected
      ParentTag : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
      pragma Import (Java, ParentTag, "parentTag");

      --  protected
      AddTag : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
      pragma Import (Java, AddTag, "addTag");

      --  protected
      AlternateParentTag : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
      pragma Import (Java, AlternateParentTag, "alternateParentTag");

      --  protected
      AlternateAddTag : access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
      pragma Import (Java, AlternateAddTag, "alternateAddTag");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InsertHTMLTextAction (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                                      P3_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                                      P4_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_InsertHTMLTextAction (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                                      P3_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                                      P4_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                                      P5_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                                      P6_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure InsertHTML (This : access Typ;
                         P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class;
                         P2_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                         P3_Int : Java.Int;
                         P4_String : access Standard.Java.Lang.String.Typ'Class;
                         P5_Int : Java.Int;
                         P6_Int : Java.Int;
                         P7_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class);

   --  protected
   procedure InsertAtBoundary (This : access Typ;
                               P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class;
                               P2_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                               P3_Int : Java.Int;
                               P4_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                               P5_String : access Standard.Java.Lang.String.Typ'Class;
                               P6_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                               P7_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class);

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InsertHTMLTextAction);
   pragma Import (Java, InsertHTML, "insertHTML");
   pragma Import (Java, InsertAtBoundary, "insertAtBoundary");
   pragma Import (Java, ActionPerformed, "actionPerformed");

end Javax.Swing.Text.Html.HTMLEditorKit.InsertHTMLTextAction;
pragma Import (Java, Javax.Swing.Text.Html.HTMLEditorKit.InsertHTMLTextAction, "javax.swing.text.html.HTMLEditorKit$InsertHTMLTextAction");
pragma Extensions_Allowed (Off);
