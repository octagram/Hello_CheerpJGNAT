pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Javax.Swing.JEditorPane;
with Java.Awt.Event.MouseAdapter;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.Html.HTMLEditorKit.LinkController is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Event.MouseAdapter.Typ(MouseListener_I,
                                           MouseMotionListener_I,
                                           MouseWheelListener_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LinkController (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure MouseClicked (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   --  protected
   procedure ActivateLink (This : access Typ;
                           P1_Int : Java.Int;
                           P2_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LinkController);
   pragma Import (Java, MouseClicked, "mouseClicked");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");
   pragma Import (Java, ActivateLink, "activateLink");

end Javax.Swing.Text.Html.HTMLEditorKit.LinkController;
pragma Import (Java, Javax.Swing.Text.Html.HTMLEditorKit.LinkController, "javax.swing.text.html.HTMLEditorKit$LinkController");
pragma Extensions_Allowed (Off);
