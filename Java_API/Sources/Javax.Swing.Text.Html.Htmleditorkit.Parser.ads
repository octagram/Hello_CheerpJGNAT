pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback;
with Java.Lang.Object;

package Javax.Swing.Text.Html.HTMLEditorKit.Parser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Parser (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Parse (This : access Typ;
                    P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                    P2_ParserCallback : access Standard.Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback.Typ'Class;
                    P3_Boolean : Java.Boolean) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Parser);
   pragma Export (Java, Parse, "parse");

end Javax.Swing.Text.Html.HTMLEditorKit.Parser;
pragma Import (Java, Javax.Swing.Text.Html.HTMLEditorKit.Parser, "javax.swing.text.html.HTMLEditorKit$Parser");
pragma Extensions_Allowed (Off);
