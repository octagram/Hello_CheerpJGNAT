pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.MutableAttributeSet;
with Java.Lang.Object;

package Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ParserCallback (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Flush (This : access Typ);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   procedure HandleText (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int);

   procedure HandleComment (This : access Typ;
                            P1_Char_Arr : Java.Char_Arr;
                            P2_Int : Java.Int);

   procedure HandleStartTag (This : access Typ;
                             P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                             P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                             P3_Int : Java.Int);

   procedure HandleEndTag (This : access Typ;
                           P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                           P2_Int : Java.Int);

   procedure HandleSimpleTag (This : access Typ;
                              P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                              P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                              P3_Int : Java.Int);

   procedure HandleError (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int);

   procedure HandleEndOfLineString (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   IMPLIED : access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ParserCallback);
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, HandleText, "handleText");
   pragma Import (Java, HandleComment, "handleComment");
   pragma Import (Java, HandleStartTag, "handleStartTag");
   pragma Import (Java, HandleEndTag, "handleEndTag");
   pragma Import (Java, HandleSimpleTag, "handleSimpleTag");
   pragma Import (Java, HandleError, "handleError");
   pragma Import (Java, HandleEndOfLineString, "handleEndOfLineString");
   pragma Import (Java, IMPLIED, "IMPLIED");

end Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback;
pragma Import (Java, Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback, "javax.swing.text.html.HTMLEditorKit$ParserCallback");
pragma Extensions_Allowed (Off);
