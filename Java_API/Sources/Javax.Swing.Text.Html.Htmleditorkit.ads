pragma Extensions_Allowed (On);
limited with Java.Awt.Cursor;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Swing.Action;
limited with Javax.Swing.JEditorPane;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.HTMLDocument;
limited with Javax.Swing.Text.Html.HTMLEditorKit.Parser;
limited with Javax.Swing.Text.Html.StyleSheet;
limited with Javax.Swing.Text.MutableAttributeSet;
limited with Javax.Swing.Text.ViewFactory;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.Text.StyledEditorKit;

package Javax.Swing.Text.Html.HTMLEditorKit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref)
    is new Javax.Swing.Text.StyledEditorKit.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HTMLEditorKit (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetViewFactory (This : access Typ)
                            return access Javax.Swing.Text.ViewFactory.Typ'Class;

   function CreateDefaultDocument (This : access Typ)
                                   return access Javax.Swing.Text.Document.Typ'Class;

   procedure Read (This : access Typ;
                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                   P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                   P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure InsertHTML (This : access Typ;
                         P1_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                         P2_Int : Java.Int;
                         P3_String : access Standard.Java.Lang.String.Typ'Class;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                    P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Install (This : access Typ;
                      P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class);

   procedure Deinstall (This : access Typ;
                        P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class);

   procedure SetStyleSheet (This : access Typ;
                            P1_StyleSheet : access Standard.Javax.Swing.Text.Html.StyleSheet.Typ'Class);

   function GetStyleSheet (This : access Typ)
                           return access Javax.Swing.Text.Html.StyleSheet.Typ'Class;

   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure CreateInputAttributes (This : access Typ;
                                    P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                    P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class);

   function GetInputAttributes (This : access Typ)
                                return access Javax.Swing.Text.MutableAttributeSet.Typ'Class;

   procedure SetDefaultCursor (This : access Typ;
                               P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetDefaultCursor (This : access Typ)
                              return access Java.Awt.Cursor.Typ'Class;

   procedure SetLinkCursor (This : access Typ;
                            P1_Cursor : access Standard.Java.Awt.Cursor.Typ'Class);

   function GetLinkCursor (This : access Typ)
                           return access Java.Awt.Cursor.Typ'Class;

   function IsAutoFormSubmission (This : access Typ)
                                  return Java.Boolean;

   procedure SetAutoFormSubmission (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   --  protected
   function GetParser (This : access Typ)
                       return access Javax.Swing.Text.Html.HTMLEditorKit.Parser.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_CSS : constant access Java.Lang.String.Typ'Class;

   --  final
   BOLD_ACTION : constant access Java.Lang.String.Typ'Class;

   --  final
   ITALIC_ACTION : constant access Java.Lang.String.Typ'Class;

   --  final
   PARA_INDENT_LEFT : constant access Java.Lang.String.Typ'Class;

   --  final
   PARA_INDENT_RIGHT : constant access Java.Lang.String.Typ'Class;

   --  final
   FONT_CHANGE_BIGGER : constant access Java.Lang.String.Typ'Class;

   --  final
   FONT_CHANGE_SMALLER : constant access Java.Lang.String.Typ'Class;

   --  final
   COLOR_ACTION : constant access Java.Lang.String.Typ'Class;

   --  final
   LOGICAL_STYLE_ACTION : constant access Java.Lang.String.Typ'Class;

   --  final
   IMG_ALIGN_TOP : constant access Java.Lang.String.Typ'Class;

   --  final
   IMG_ALIGN_MIDDLE : constant access Java.Lang.String.Typ'Class;

   --  final
   IMG_ALIGN_BOTTOM : constant access Java.Lang.String.Typ'Class;

   --  final
   IMG_BORDER : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HTMLEditorKit);
   pragma Import (Java, GetContentType, "getContentType");
   pragma Import (Java, GetViewFactory, "getViewFactory");
   pragma Import (Java, CreateDefaultDocument, "createDefaultDocument");
   pragma Import (Java, Read, "read");
   pragma Import (Java, InsertHTML, "insertHTML");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Install, "install");
   pragma Import (Java, Deinstall, "deinstall");
   pragma Import (Java, SetStyleSheet, "setStyleSheet");
   pragma Import (Java, GetStyleSheet, "getStyleSheet");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, CreateInputAttributes, "createInputAttributes");
   pragma Import (Java, GetInputAttributes, "getInputAttributes");
   pragma Import (Java, SetDefaultCursor, "setDefaultCursor");
   pragma Import (Java, GetDefaultCursor, "getDefaultCursor");
   pragma Import (Java, SetLinkCursor, "setLinkCursor");
   pragma Import (Java, GetLinkCursor, "getLinkCursor");
   pragma Import (Java, IsAutoFormSubmission, "isAutoFormSubmission");
   pragma Import (Java, SetAutoFormSubmission, "setAutoFormSubmission");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetParser, "getParser");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, DEFAULT_CSS, "DEFAULT_CSS");
   pragma Import (Java, BOLD_ACTION, "BOLD_ACTION");
   pragma Import (Java, ITALIC_ACTION, "ITALIC_ACTION");
   pragma Import (Java, PARA_INDENT_LEFT, "PARA_INDENT_LEFT");
   pragma Import (Java, PARA_INDENT_RIGHT, "PARA_INDENT_RIGHT");
   pragma Import (Java, FONT_CHANGE_BIGGER, "FONT_CHANGE_BIGGER");
   pragma Import (Java, FONT_CHANGE_SMALLER, "FONT_CHANGE_SMALLER");
   pragma Import (Java, COLOR_ACTION, "COLOR_ACTION");
   pragma Import (Java, LOGICAL_STYLE_ACTION, "LOGICAL_STYLE_ACTION");
   pragma Import (Java, IMG_ALIGN_TOP, "IMG_ALIGN_TOP");
   pragma Import (Java, IMG_ALIGN_MIDDLE, "IMG_ALIGN_MIDDLE");
   pragma Import (Java, IMG_ALIGN_BOTTOM, "IMG_ALIGN_BOTTOM");
   pragma Import (Java, IMG_BORDER, "IMG_BORDER");

end Javax.Swing.Text.Html.HTMLEditorKit;
pragma Import (Java, Javax.Swing.Text.Html.HTMLEditorKit, "javax.swing.text.html.HTMLEditorKit");
pragma Extensions_Allowed (Off);
