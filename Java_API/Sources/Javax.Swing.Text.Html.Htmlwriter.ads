pragma Extensions_Allowed (On);
limited with Java.Io.Writer;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.HTMLDocument;
limited with Javax.Swing.Text.Html.Option;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractWriter;

package Javax.Swing.Text.Html.HTMLWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Text.AbstractWriter.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HTMLWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                            P2_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_HTMLWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                            P2_HTMLDocument : access Standard.Javax.Swing.Text.Html.HTMLDocument.Typ'Class;
                            P3_Int : Java.Int;
                            P4_Int : Java.Int; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure WriteAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure EmptyTag (This : access Typ;
                       P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   --  protected
   function IsBlockTag (This : access Typ;
                        P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                        return Java.Boolean;

   --  protected
   procedure StartTag (This : access Typ;
                       P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure TextAreaContent (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   --  protected
   procedure Text (This : access Typ;
                   P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   --  protected
   procedure SelectContent (This : access Typ;
                            P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteOption (This : access Typ;
                          P1_Option : access Standard.Javax.Swing.Text.Html.Option.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure EndTag (This : access Typ;
                     P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Comment (This : access Typ;
                      P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except and
   --  Java.Io.IOException.Except

   --  protected
   function SynthesizedElement (This : access Typ;
                                P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                                return Java.Boolean;

   --  protected
   function MatchNameAttribute (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                P2_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class)
                                return Java.Boolean;

   --  protected
   procedure WriteEmbeddedTags (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure CloseOutUnwantedEmbeddedTags (This : access Typ;
                                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteLineSeparator (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Output (This : access Typ;
                     P1_Char_Arr : Java.Char_Arr;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HTMLWriter);
   pragma Import (Java, Write, "write");
   pragma Import (Java, WriteAttributes, "writeAttributes");
   pragma Import (Java, EmptyTag, "emptyTag");
   pragma Import (Java, IsBlockTag, "isBlockTag");
   pragma Import (Java, StartTag, "startTag");
   pragma Import (Java, TextAreaContent, "textAreaContent");
   pragma Import (Java, Text, "text");
   pragma Import (Java, SelectContent, "selectContent");
   pragma Import (Java, WriteOption, "writeOption");
   pragma Import (Java, EndTag, "endTag");
   pragma Import (Java, Comment, "comment");
   pragma Import (Java, SynthesizedElement, "synthesizedElement");
   pragma Import (Java, MatchNameAttribute, "matchNameAttribute");
   pragma Import (Java, WriteEmbeddedTags, "writeEmbeddedTags");
   pragma Import (Java, CloseOutUnwantedEmbeddedTags, "closeOutUnwantedEmbeddedTags");
   pragma Import (Java, WriteLineSeparator, "writeLineSeparator");
   pragma Import (Java, Output, "output");

end Javax.Swing.Text.Html.HTMLWriter;
pragma Import (Java, Javax.Swing.Text.Html.HTMLWriter, "javax.swing.text.html.HTMLWriter");
pragma Extensions_Allowed (Off);
