pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Image;
limited with Java.Awt.Shape;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.StyleSheet;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.View;

package Javax.Swing.Text.Html.ImageView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.View.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ImageView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAltText (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetImageURL (This : access Typ)
                         return access Java.Net.URL.Typ'Class;

   function GetNoImageIcon (This : access Typ)
                            return access Javax.Swing.Icon.Typ'Class;

   function GetLoadingImageIcon (This : access Typ)
                                 return access Javax.Swing.Icon.Typ'Class;

   function GetImage (This : access Typ)
                      return access Java.Awt.Image.Typ'Class;

   procedure SetLoadsSynchronously (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function GetLoadsSynchronously (This : access Typ)
                                   return Java.Boolean;

   --  protected
   function GetStyleSheet (This : access Typ)
                           return access Javax.Swing.Text.Html.StyleSheet.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetToolTipText (This : access Typ;
                            P1_Float : Java.Float;
                            P2_Float : Java.Float;
                            P3_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   --  protected
   procedure SetPropertiesFromAttributes (This : access Typ);

   procedure SetParent (This : access Typ;
                        P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   function GetAlignment (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   procedure SetSize (This : access Typ;
                      P1_Float : Java.Float;
                      P2_Float : Java.Float);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ImageView);
   pragma Import (Java, GetAltText, "getAltText");
   pragma Import (Java, GetImageURL, "getImageURL");
   pragma Import (Java, GetNoImageIcon, "getNoImageIcon");
   pragma Import (Java, GetLoadingImageIcon, "getLoadingImageIcon");
   pragma Import (Java, GetImage, "getImage");
   pragma Import (Java, SetLoadsSynchronously, "setLoadsSynchronously");
   pragma Import (Java, GetLoadsSynchronously, "getLoadsSynchronously");
   pragma Import (Java, GetStyleSheet, "getStyleSheet");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, SetPropertiesFromAttributes, "setPropertiesFromAttributes");
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, ChangedUpdate, "changedUpdate");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, SetSize, "setSize");

end Javax.Swing.Text.Html.ImageView;
pragma Import (Java, Javax.Swing.Text.Html.ImageView, "javax.swing.text.html.ImageView");
pragma Extensions_Allowed (Off);
