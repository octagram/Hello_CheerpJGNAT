pragma Extensions_Allowed (On);
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.StyleSheet;
limited with Javax.Swing.Text.View;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.LabelView;
with Javax.Swing.Text.TabableView;

package Javax.Swing.Text.Html.InlineView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TabableView_I : Javax.Swing.Text.TabableView.Ref)
    is new Javax.Swing.Text.LabelView.Typ(Cloneable_I,
                                          SwingConstants_I,
                                          TabableView_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InlineView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InsertUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetBreakWeight (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Float : Java.Float;
                            P3_Float : Java.Float)
                            return Java.Int;

   function BreakView (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Float : Java.Float;
                       P4_Float : Java.Float)
                       return access Javax.Swing.Text.View.Typ'Class;

   --  protected
   procedure SetPropertiesFromAttributes (This : access Typ);

   --  protected
   function GetStyleSheet (This : access Typ)
                           return access Javax.Swing.Text.Html.StyleSheet.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InlineView);
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, ChangedUpdate, "changedUpdate");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetBreakWeight, "getBreakWeight");
   pragma Import (Java, BreakView, "breakView");
   pragma Import (Java, SetPropertiesFromAttributes, "setPropertiesFromAttributes");
   pragma Import (Java, GetStyleSheet, "getStyleSheet");

end Javax.Swing.Text.Html.InlineView;
pragma Import (Java, Javax.Swing.Text.Html.InlineView, "javax.swing.text.html.InlineView");
pragma Extensions_Allowed (Off);
