pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.Element;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.Html.BlockView;

package Javax.Swing.Text.Html.ListView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.Html.BlockView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ListView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAlignment (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   --  protected
   procedure PaintChild (This : access Typ;
                         P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                         P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                         P3_Int : Java.Int);

   --  protected
   procedure SetPropertiesFromAttributes (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ListView);
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, PaintChild, "paintChild");
   pragma Import (Java, SetPropertiesFromAttributes, "setPropertiesFromAttributes");

end Javax.Swing.Text.Html.ListView;
pragma Import (Java, Javax.Swing.Text.Html.ListView, "javax.swing.text.html.ListView");
pragma Extensions_Allowed (Off);
