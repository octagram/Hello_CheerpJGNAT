pragma Extensions_Allowed (On);
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.StyledDocument;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractWriter;

package Javax.Swing.Text.Html.MinimalHTMLWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.Text.AbstractWriter.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MinimalHTMLWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                                   P2_StyledDocument : access Standard.Javax.Swing.Text.StyledDocument.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_MinimalHTMLWriter (P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                                   P2_StyledDocument : access Standard.Javax.Swing.Text.StyledDocument.Typ'Class;
                                   P3_Int : Java.Int;
                                   P4_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure WriteAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure Text (This : access Typ;
                   P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure WriteStartTag (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteEndTag (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteHeader (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteStyles (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteBody (This : access Typ);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure WriteEndParagraph (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteStartParagraph (This : access Typ;
                                  P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteLeaf (This : access Typ;
                        P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteImage (This : access Typ;
                         P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteComponent (This : access Typ;
                             P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   function IsText (This : access Typ;
                    P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                    return Java.Boolean;

   --  protected
   procedure WriteContent (This : access Typ;
                           P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                           P2_Boolean : Java.Boolean);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   --  protected
   procedure WriteHTMLTags (This : access Typ;
                            P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure WriteNonHTMLAttributes (This : access Typ;
                                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   function InFontTag (This : access Typ)
                       return Java.Boolean;

   --  protected
   procedure EndFontTag (This : access Typ);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure StartFontTag (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MinimalHTMLWriter);
   pragma Import (Java, Write, "write");
   pragma Import (Java, WriteAttributes, "writeAttributes");
   pragma Import (Java, Text, "text");
   pragma Import (Java, WriteStartTag, "writeStartTag");
   pragma Import (Java, WriteEndTag, "writeEndTag");
   pragma Import (Java, WriteHeader, "writeHeader");
   pragma Import (Java, WriteStyles, "writeStyles");
   pragma Import (Java, WriteBody, "writeBody");
   pragma Import (Java, WriteEndParagraph, "writeEndParagraph");
   pragma Import (Java, WriteStartParagraph, "writeStartParagraph");
   pragma Import (Java, WriteLeaf, "writeLeaf");
   pragma Import (Java, WriteImage, "writeImage");
   pragma Import (Java, WriteComponent, "writeComponent");
   pragma Import (Java, IsText, "isText");
   pragma Import (Java, WriteContent, "writeContent");
   pragma Import (Java, WriteHTMLTags, "writeHTMLTags");
   pragma Import (Java, WriteNonHTMLAttributes, "writeNonHTMLAttributes");
   pragma Import (Java, InFontTag, "inFontTag");
   pragma Import (Java, EndFontTag, "endFontTag");
   pragma Import (Java, StartFontTag, "startFontTag");

end Javax.Swing.Text.Html.MinimalHTMLWriter;
pragma Import (Java, Javax.Swing.Text.Html.MinimalHTMLWriter, "javax.swing.text.html.MinimalHTMLWriter");
pragma Extensions_Allowed (Off);
