pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.Text.Element;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.ComponentView;

package Javax.Swing.Text.Html.ObjectView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.ComponentView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjectView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateComponent (This : access Typ)
                             return access Java.Awt.Component.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjectView);
   pragma Import (Java, CreateComponent, "createComponent");

end Javax.Swing.Text.Html.ObjectView;
pragma Import (Java, Javax.Swing.Text.Html.ObjectView, "javax.swing.text.html.ObjectView");
pragma Extensions_Allowed (Off);
