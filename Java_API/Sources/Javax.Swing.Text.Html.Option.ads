pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.Html.Option is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Option (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetLabel (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetLabel (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   --  protected
   procedure SetSelection (This : access Typ;
                           P1_Boolean : Java.Boolean);

   function IsSelected (This : access Typ)
                        return Java.Boolean;

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Option);
   pragma Import (Java, SetLabel, "setLabel");
   pragma Import (Java, GetLabel, "getLabel");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, SetSelection, "setSelection");
   pragma Import (Java, IsSelected, "isSelected");
   pragma Import (Java, GetValue, "getValue");

end Javax.Swing.Text.Html.Option;
pragma Import (Java, Javax.Swing.Text.Html.Option, "javax.swing.text.html.Option");
pragma Extensions_Allowed (Off);
