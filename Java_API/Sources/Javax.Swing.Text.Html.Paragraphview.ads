pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.SizeRequirements;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.StyleSheet;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.ParagraphView;
with Javax.Swing.Text.TabExpander;

package Javax.Swing.Text.Html.ParagraphView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TabExpander_I : Javax.Swing.Text.TabExpander.Ref)
    is new Javax.Swing.Text.ParagraphView.Typ(SwingConstants_I,
                                              TabExpander_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ParagraphView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   --  protected
   procedure SetPropertiesFromAttributes (This : access Typ);

   --  protected
   function GetStyleSheet (This : access Typ)
                           return access Javax.Swing.Text.Html.StyleSheet.Typ'Class;

   --  protected
   function CalculateMinorAxisRequirements (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                            return access Javax.Swing.SizeRequirements.Typ'Class;

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   function GetMinimumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetMaximumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ParagraphView);
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, SetPropertiesFromAttributes, "setPropertiesFromAttributes");
   pragma Import (Java, GetStyleSheet, "getStyleSheet");
   pragma Import (Java, CalculateMinorAxisRequirements, "calculateMinorAxisRequirements");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetMinimumSpan, "getMinimumSpan");
   pragma Import (Java, GetMaximumSpan, "getMaximumSpan");

end Javax.Swing.Text.Html.ParagraphView;
pragma Import (Java, Javax.Swing.Text.Html.ParagraphView, "javax.swing.text.html.ParagraphView");
pragma Extensions_Allowed (Off);
