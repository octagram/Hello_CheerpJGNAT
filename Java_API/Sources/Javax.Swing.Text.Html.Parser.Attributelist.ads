pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Vector;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Html.Parser.DTDConstants;

package Javax.Swing.Text.Html.Parser.AttributeList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            DTDConstants_I : Javax.Swing.Text.Html.Parser.DTDConstants.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      type_K : Java.Int;
      pragma Import (Java, type_K, "type");

      Values : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Values, "values");

      Modifier : Java.Int;
      pragma Import (Java, Modifier, "modifier");

      Value : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Value, "value");

      Next : access Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class;
      pragma Import (Java, Next, "next");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributeList (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_AttributeList (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_String : access Standard.Java.Lang.String.Typ'Class;
                               P5_Vector : access Standard.Java.Util.Vector.Typ'Class;
                               P6_AttributeList : access Standard.Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ)
                     return Java.Int;

   function GetModifier (This : access Typ)
                         return Java.Int;

   function GetValues (This : access Typ)
                       return access Java.Util.Enumeration.Typ'Class;

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetNext (This : access Typ)
                     return access Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Name2type (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Int;

   function Type2name (P1_Int : Java.Int)
                       return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributeList);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetModifier, "getModifier");
   pragma Import (Java, GetValues, "getValues");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetNext, "getNext");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Name2type, "name2type");
   pragma Import (Java, Type2name, "type2name");

end Javax.Swing.Text.Html.Parser.AttributeList;
pragma Import (Java, Javax.Swing.Text.Html.Parser.AttributeList, "javax.swing.text.html.parser.AttributeList");
pragma Extensions_Allowed (Off);
