pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Swing.Text.Html.Parser.Element;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.Html.Parser.ContentModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      type_K : Java.Int;
      pragma Import (Java, type_K, "type");

      Content : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Content, "content");

      Next : access Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class;
      pragma Import (Java, Next, "next");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ContentModel (This : Ref := null)
                              return Ref;

   function New_ContentModel (P1_Element : access Standard.Javax.Swing.Text.Html.Parser.Element.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_ContentModel (P1_Int : Java.Int;
                              P2_ContentModel : access Standard.Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_ContentModel (P1_Int : Java.Int;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_ContentModel : access Standard.Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Empty (This : access Typ)
                   return Java.Boolean;

   procedure GetElements (This : access Typ;
                          P1_Vector : access Standard.Java.Util.Vector.Typ'Class);

   function First (This : access Typ;
                   P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                   return Java.Boolean;

   function First (This : access Typ)
                   return access Javax.Swing.Text.Html.Parser.Element.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ContentModel);
   pragma Import (Java, Empty, "empty");
   pragma Import (Java, GetElements, "getElements");
   pragma Import (Java, First, "first");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Text.Html.Parser.ContentModel;
pragma Import (Java, Javax.Swing.Text.Html.Parser.ContentModel, "javax.swing.text.html.parser.ContentModel");
pragma Extensions_Allowed (Off);
