pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback;
limited with Javax.Swing.Text.Html.Parser.DTD;
limited with Javax.Swing.Text.Html.Parser.TagElement;
with Java.Lang.Object;
with Javax.Swing.Text.Html.Parser.DTDConstants;
with Javax.Swing.Text.Html.Parser.Parser;

package Javax.Swing.Text.Html.Parser.DocumentParser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DTDConstants_I : Javax.Swing.Text.Html.Parser.DTDConstants.Ref)
    is new Javax.Swing.Text.Html.Parser.Parser.Typ(DTDConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DocumentParser (P1_DTD : access Standard.Javax.Swing.Text.Html.Parser.DTD.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Parse (This : access Typ;
                    P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                    P2_ParserCallback : access Standard.Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback.Typ'Class;
                    P3_Boolean : Java.Boolean);
   --  can raise Java.Io.IOException.Except

   --  protected
   procedure HandleStartTag (This : access Typ;
                             P1_TagElement : access Standard.Javax.Swing.Text.Html.Parser.TagElement.Typ'Class);

   --  protected
   procedure HandleComment (This : access Typ;
                            P1_Char_Arr : Java.Char_Arr);

   --  protected
   procedure HandleEmptyTag (This : access Typ;
                             P1_TagElement : access Standard.Javax.Swing.Text.Html.Parser.TagElement.Typ'Class);
   --  can raise Javax.Swing.Text.ChangedCharSetException.Except

   --  protected
   procedure HandleEndTag (This : access Typ;
                           P1_TagElement : access Standard.Javax.Swing.Text.Html.Parser.TagElement.Typ'Class);

   --  protected
   procedure HandleText (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr);

   --  protected
   procedure HandleError (This : access Typ;
                          P1_Int : Java.Int;
                          P2_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DocumentParser);
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, HandleStartTag, "handleStartTag");
   pragma Import (Java, HandleComment, "handleComment");
   pragma Import (Java, HandleEmptyTag, "handleEmptyTag");
   pragma Import (Java, HandleEndTag, "handleEndTag");
   pragma Import (Java, HandleText, "handleText");
   pragma Import (Java, HandleError, "handleError");

end Javax.Swing.Text.Html.Parser.DocumentParser;
pragma Import (Java, Javax.Swing.Text.Html.Parser.DocumentParser, "javax.swing.text.html.parser.DocumentParser");
pragma Extensions_Allowed (Off);
