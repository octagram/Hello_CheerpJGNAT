pragma Extensions_Allowed (On);
limited with Java.Io.DataInputStream;
limited with Java.Lang.String;
limited with Java.Util.BitSet;
limited with Java.Util.Hashtable;
limited with Java.Util.Vector;
limited with Javax.Swing.Text.Html.Parser.AttributeList;
limited with Javax.Swing.Text.Html.Parser.ContentModel;
limited with Javax.Swing.Text.Html.Parser.Element;
limited with Javax.Swing.Text.Html.Parser.Entity;
with Java.Lang.Object;
with Javax.Swing.Text.Html.Parser.DTDConstants;

package Javax.Swing.Text.Html.Parser.DTD is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DTDConstants_I : Javax.Swing.Text.Html.Parser.DTDConstants.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      Elements : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Elements, "elements");

      ElementHash : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, ElementHash, "elementHash");

      EntityHash : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, EntityHash, "entityHash");

      --  final
      Pcdata : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Pcdata, "pcdata");

      --  final
      Html : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Html, "html");

      --  final
      Meta : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Meta, "meta");

      --  final
      Base : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Base, "base");

      --  final
      Isindex : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Isindex, "isindex");

      --  final
      Head : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Head, "head");

      --  final
      body_K : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, body_K, "body");

      --  final
      Applet : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Applet, "applet");

      --  final
      Param : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Param, "param");

      --  final
      P : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, P, "p");

      --  final
      Title : access Javax.Swing.Text.Html.Parser.Element.Typ'Class;
      pragma Import (Java, Title, "title");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_DTD (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetEntity (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Javax.Swing.Text.Html.Parser.Entity.Typ'Class;

   function GetEntity (This : access Typ;
                       P1_Int : Java.Int)
                       return access Javax.Swing.Text.Html.Parser.Entity.Typ'Class;

   function GetElement (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Swing.Text.Html.Parser.Element.Typ'Class;

   function GetElement (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Text.Html.Parser.Element.Typ'Class;

   function DefineEntity (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Int : Java.Int;
                          P3_Char_Arr : Java.Char_Arr)
                          return access Javax.Swing.Text.Html.Parser.Entity.Typ'Class;

   function DefineElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Int : Java.Int;
                           P3_Boolean : Java.Boolean;
                           P4_Boolean : Java.Boolean;
                           P5_ContentModel : access Standard.Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class;
                           P6_BitSet : access Standard.Java.Util.BitSet.Typ'Class;
                           P7_BitSet : access Standard.Java.Util.BitSet.Typ'Class;
                           P8_AttributeList : access Standard.Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class)
                           return access Javax.Swing.Text.Html.Parser.Element.Typ'Class;

   procedure DefineAttributes (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_AttributeList : access Standard.Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class);

   function DefEntity (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int;
                       P3_Int : Java.Int)
                       return access Javax.Swing.Text.Html.Parser.Entity.Typ'Class;

   --  protected
   function DefEntity (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Int : Java.Int;
                       P3_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Javax.Swing.Text.Html.Parser.Entity.Typ'Class;

   --  protected
   function DefElement (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Boolean : Java.Boolean;
                        P4_Boolean : Java.Boolean;
                        P5_ContentModel : access Standard.Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class;
                        P6_String_Arr : access Java.Lang.String.Arr_Obj;
                        P7_String_Arr : access Java.Lang.String.Arr_Obj;
                        P8_AttributeList : access Standard.Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class)
                        return access Javax.Swing.Text.Html.Parser.Element.Typ'Class;

   --  protected
   function DefAttributeList (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_String : access Standard.Java.Lang.String.Typ'Class;
                              P5_String : access Standard.Java.Lang.String.Typ'Class;
                              P6_AttributeList : access Standard.Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class)
                              return access Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class;

   --  protected
   function DefContentModel (This : access Typ;
                             P1_Int : Java.Int;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P3_ContentModel : access Standard.Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class)
                             return access Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure PutDTDHash (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_DTD : access Standard.Javax.Swing.Text.Html.Parser.DTD.Typ'Class);

   function GetDTD (P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Javax.Swing.Text.Html.Parser.DTD.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure Read (This : access Typ;
                   P1_DataInputStream : access Standard.Java.Io.DataInputStream.Typ'Class);
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FILE_VERSION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DTD);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetEntity, "getEntity");
   pragma Import (Java, GetElement, "getElement");
   pragma Import (Java, DefineEntity, "defineEntity");
   pragma Import (Java, DefineElement, "defineElement");
   pragma Import (Java, DefineAttributes, "defineAttributes");
   pragma Import (Java, DefEntity, "defEntity");
   pragma Import (Java, DefElement, "defElement");
   pragma Import (Java, DefAttributeList, "defAttributeList");
   pragma Import (Java, DefContentModel, "defContentModel");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, PutDTDHash, "putDTDHash");
   pragma Import (Java, GetDTD, "getDTD");
   pragma Import (Java, Read, "read");
   pragma Import (Java, FILE_VERSION, "FILE_VERSION");

end Javax.Swing.Text.Html.Parser.DTD;
pragma Import (Java, Javax.Swing.Text.Html.Parser.DTD, "javax.swing.text.html.parser.DTD");
pragma Extensions_Allowed (Off);
