pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Swing.Text.Html.Parser.DTDConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CDATA : constant Java.Int;

   --  final
   ENTITY : constant Java.Int;

   --  final
   ENTITIES : constant Java.Int;

   --  final
   ID : constant Java.Int;

   --  final
   IDREF : constant Java.Int;

   --  final
   IDREFS : constant Java.Int;

   --  final
   NAME : constant Java.Int;

   --  final
   NAMES : constant Java.Int;

   --  final
   NMTOKEN : constant Java.Int;

   --  final
   NMTOKENS : constant Java.Int;

   --  final
   NOTATION : constant Java.Int;

   --  final
   NUMBER : constant Java.Int;

   --  final
   NUMBERS : constant Java.Int;

   --  final
   NUTOKEN : constant Java.Int;

   --  final
   NUTOKENS : constant Java.Int;

   --  final
   RCDATA : constant Java.Int;

   --  final
   EMPTY : constant Java.Int;

   --  final
   MODEL : constant Java.Int;

   --  final
   ANY : constant Java.Int;

   --  final
   FIXED : constant Java.Int;

   --  final
   REQUIRED : constant Java.Int;

   --  final
   CURRENT : constant Java.Int;

   --  final
   CONREF : constant Java.Int;

   --  final
   IMPLIED : constant Java.Int;

   --  final
   PUBLIC : constant Java.Int;

   --  final
   SDATA : constant Java.Int;

   --  final
   PI : constant Java.Int;

   --  final
   STARTTAG : constant Java.Int;

   --  final
   ENDTAG : constant Java.Int;

   --  final
   MS : constant Java.Int;

   --  final
   MD : constant Java.Int;

   --  final
   SYSTEM : constant Java.Int;

   --  final
   GENERAL : constant Java.Int;

   --  final
   DEFAULT : constant Java.Int;

   --  final
   PARAMETER : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CDATA, "CDATA");
   pragma Import (Java, ENTITY, "ENTITY");
   pragma Import (Java, ENTITIES, "ENTITIES");
   pragma Import (Java, ID, "ID");
   pragma Import (Java, IDREF, "IDREF");
   pragma Import (Java, IDREFS, "IDREFS");
   pragma Import (Java, NAME, "NAME");
   pragma Import (Java, NAMES, "NAMES");
   pragma Import (Java, NMTOKEN, "NMTOKEN");
   pragma Import (Java, NMTOKENS, "NMTOKENS");
   pragma Import (Java, NOTATION, "NOTATION");
   pragma Import (Java, NUMBER, "NUMBER");
   pragma Import (Java, NUMBERS, "NUMBERS");
   pragma Import (Java, NUTOKEN, "NUTOKEN");
   pragma Import (Java, NUTOKENS, "NUTOKENS");
   pragma Import (Java, RCDATA, "RCDATA");
   pragma Import (Java, EMPTY, "EMPTY");
   pragma Import (Java, MODEL, "MODEL");
   pragma Import (Java, ANY, "ANY");
   pragma Import (Java, FIXED, "FIXED");
   pragma Import (Java, REQUIRED, "REQUIRED");
   pragma Import (Java, CURRENT, "CURRENT");
   pragma Import (Java, CONREF, "CONREF");
   pragma Import (Java, IMPLIED, "IMPLIED");
   pragma Import (Java, PUBLIC, "PUBLIC");
   pragma Import (Java, SDATA, "SDATA");
   pragma Import (Java, PI, "PI");
   pragma Import (Java, STARTTAG, "STARTTAG");
   pragma Import (Java, ENDTAG, "ENDTAG");
   pragma Import (Java, MS, "MS");
   pragma Import (Java, MD, "MD");
   pragma Import (Java, SYSTEM, "SYSTEM");
   pragma Import (Java, GENERAL, "GENERAL");
   pragma Import (Java, DEFAULT, "DEFAULT");
   pragma Import (Java, PARAMETER, "PARAMETER");

end Javax.Swing.Text.Html.Parser.DTDConstants;
pragma Import (Java, Javax.Swing.Text.Html.Parser.DTDConstants, "javax.swing.text.html.parser.DTDConstants");
pragma Extensions_Allowed (Off);
