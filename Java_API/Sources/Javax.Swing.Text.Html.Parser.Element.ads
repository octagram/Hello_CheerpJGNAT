pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.BitSet;
limited with Javax.Swing.Text.Html.Parser.AttributeList;
limited with Javax.Swing.Text.Html.Parser.ContentModel;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Html.Parser.DTDConstants;

package Javax.Swing.Text.Html.Parser.Element is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            DTDConstants_I : Javax.Swing.Text.Html.Parser.DTDConstants.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Index : Java.Int;
      pragma Import (Java, Index, "index");

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      OStart : Java.Boolean;
      pragma Import (Java, OStart, "oStart");

      OEnd : Java.Boolean;
      pragma Import (Java, OEnd, "oEnd");

      Inclusions : access Java.Util.BitSet.Typ'Class;
      pragma Import (Java, Inclusions, "inclusions");

      Exclusions : access Java.Util.BitSet.Typ'Class;
      pragma Import (Java, Exclusions, "exclusions");

      type_K : Java.Int;
      pragma Import (Java, type_K, "type");

      Content : access Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class;
      pragma Import (Java, Content, "content");

      Atts : access Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class;
      pragma Import (Java, Atts, "atts");

      Data : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Data, "data");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function OmitStart (This : access Typ)
                       return Java.Boolean;

   function OmitEnd (This : access Typ)
                     return Java.Boolean;

   function GetType (This : access Typ)
                     return Java.Int;

   function GetContent (This : access Typ)
                        return access Javax.Swing.Text.Html.Parser.ContentModel.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class;

   function GetIndex (This : access Typ)
                      return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class;

   function GetAttributeByValue (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Javax.Swing.Text.Html.Parser.AttributeList.Typ'Class;

   function Name2type (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, OmitStart, "omitStart");
   pragma Import (Java, OmitEnd, "omitEnd");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetContent, "getContent");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetAttributeByValue, "getAttributeByValue");
   pragma Import (Java, Name2type, "name2type");

end Javax.Swing.Text.Html.Parser.Element;
pragma Import (Java, Javax.Swing.Text.Html.Parser.Element, "javax.swing.text.html.parser.Element");
pragma Extensions_Allowed (Off);
