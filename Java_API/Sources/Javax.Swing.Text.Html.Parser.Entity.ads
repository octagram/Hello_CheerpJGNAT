pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Swing.Text.Html.Parser.DTDConstants;

package Javax.Swing.Text.Html.Parser.Entity is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DTDConstants_I : Javax.Swing.Text.Html.Parser.DTDConstants.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      type_K : Java.Int;
      pragma Import (Java, type_K, "type");

      Data : Java.Char_Arr;
      pragma Import (Java, Data, "data");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Entity (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Char_Arr : Java.Char_Arr; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ)
                     return Java.Int;

   function IsParameter (This : access Typ)
                         return Java.Boolean;

   function IsGeneral (This : access Typ)
                       return Java.Boolean;

   function GetData (This : access Typ)
                     return Java.Char_Arr;

   function GetString (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function Name2type (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Entity);
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, IsParameter, "isParameter");
   pragma Import (Java, IsGeneral, "isGeneral");
   pragma Import (Java, GetData, "getData");
   pragma Import (Java, GetString, "getString");
   pragma Import (Java, Name2type, "name2type");

end Javax.Swing.Text.Html.Parser.Entity;
pragma Import (Java, Javax.Swing.Text.Html.Parser.Entity, "javax.swing.text.html.parser.Entity");
pragma Extensions_Allowed (Off);
