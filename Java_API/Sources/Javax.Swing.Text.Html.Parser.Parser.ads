pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Java.Lang.StringBuffer;
limited with Javax.Swing.Text.Html.Parser.DTD;
limited with Javax.Swing.Text.Html.Parser.Element;
limited with Javax.Swing.Text.Html.Parser.TagElement;
limited with Javax.Swing.Text.SimpleAttributeSet;
with Java.Lang.Object;
with Javax.Swing.Text.Html.Parser.DTDConstants;

package Javax.Swing.Text.Html.Parser.Parser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DTDConstants_I : Javax.Swing.Text.Html.Parser.DTDConstants.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Dtd : access Javax.Swing.Text.Html.Parser.DTD.Typ'Class;
      pragma Import (Java, Dtd, "dtd");

      --  protected
      Strict : Java.Boolean;
      pragma Import (Java, Strict, "strict");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Parser (P1_DTD : access Standard.Javax.Swing.Text.Html.Parser.DTD.Typ'Class; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetCurrentLine (This : access Typ)
                            return Java.Int;

   --  protected
   function MakeTag (This : access Typ;
                     P1_Element : access Standard.Javax.Swing.Text.Html.Parser.Element.Typ'Class;
                     P2_Boolean : Java.Boolean)
                     return access Javax.Swing.Text.Html.Parser.TagElement.Typ'Class;

   --  protected
   function MakeTag (This : access Typ;
                     P1_Element : access Standard.Javax.Swing.Text.Html.Parser.Element.Typ'Class)
                     return access Javax.Swing.Text.Html.Parser.TagElement.Typ'Class;

   --  protected
   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.SimpleAttributeSet.Typ'Class;

   --  protected
   procedure FlushAttributes (This : access Typ);

   --  protected
   procedure HandleText (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr);

   --  protected
   procedure HandleTitle (This : access Typ;
                          P1_Char_Arr : Java.Char_Arr);

   --  protected
   procedure HandleComment (This : access Typ;
                            P1_Char_Arr : Java.Char_Arr);

   --  protected
   procedure HandleEOFInComment (This : access Typ);

   --  protected
   procedure HandleEmptyTag (This : access Typ;
                             P1_TagElement : access Standard.Javax.Swing.Text.Html.Parser.TagElement.Typ'Class);
   --  can raise Javax.Swing.Text.ChangedCharSetException.Except

   --  protected
   procedure HandleStartTag (This : access Typ;
                             P1_TagElement : access Standard.Javax.Swing.Text.Html.Parser.TagElement.Typ'Class);

   --  protected
   procedure HandleEndTag (This : access Typ;
                           P1_TagElement : access Standard.Javax.Swing.Text.Html.Parser.TagElement.Typ'Class);

   --  protected
   procedure HandleError (This : access Typ;
                          P1_Int : Java.Int;
                          P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure Error (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class;
                    P4_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure Error (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure Error (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure Error (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   procedure StartTag (This : access Typ;
                       P1_TagElement : access Standard.Javax.Swing.Text.Html.Parser.TagElement.Typ'Class);
   --  can raise Javax.Swing.Text.ChangedCharSetException.Except

   --  protected
   procedure EndTag (This : access Typ;
                     P1_Boolean : Java.Boolean);

   --  protected
   procedure MarkFirstTime (This : access Typ;
                            P1_Element : access Standard.Javax.Swing.Text.Html.Parser.Element.Typ'Class);

   function ParseDTDMarkup (This : access Typ)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.IOException.Except

   --  protected
   function ParseMarkupDeclarations (This : access Typ;
                                     P1_StringBuffer : access Standard.Java.Lang.StringBuffer.Typ'Class)
                                     return Java.Boolean;
   --  can raise Java.Io.IOException.Except

   --  synchronized
   procedure Parse (This : access Typ;
                    P1_Reader : access Standard.Java.Io.Reader.Typ'Class);
   --  can raise Java.Io.IOException.Except

   --  protected
   function GetCurrentPos (This : access Typ)
                           return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Parser);
   pragma Import (Java, GetCurrentLine, "getCurrentLine");
   pragma Import (Java, MakeTag, "makeTag");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, FlushAttributes, "flushAttributes");
   pragma Import (Java, HandleText, "handleText");
   pragma Import (Java, HandleTitle, "handleTitle");
   pragma Import (Java, HandleComment, "handleComment");
   pragma Import (Java, HandleEOFInComment, "handleEOFInComment");
   pragma Import (Java, HandleEmptyTag, "handleEmptyTag");
   pragma Import (Java, HandleStartTag, "handleStartTag");
   pragma Import (Java, HandleEndTag, "handleEndTag");
   pragma Import (Java, HandleError, "handleError");
   pragma Import (Java, Error, "error");
   pragma Import (Java, StartTag, "startTag");
   pragma Import (Java, EndTag, "endTag");
   pragma Import (Java, MarkFirstTime, "markFirstTime");
   pragma Import (Java, ParseDTDMarkup, "parseDTDMarkup");
   pragma Import (Java, ParseMarkupDeclarations, "parseMarkupDeclarations");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, GetCurrentPos, "getCurrentPos");

end Javax.Swing.Text.Html.Parser.Parser;
pragma Import (Java, Javax.Swing.Text.Html.Parser.Parser, "javax.swing.text.html.parser.Parser");
pragma Extensions_Allowed (Off);
