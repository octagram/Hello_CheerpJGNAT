pragma Extensions_Allowed (On);
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback;
limited with Javax.Swing.Text.Html.Parser.DTD;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Html.HTMLEditorKit.Parser;

package Javax.Swing.Text.Html.Parser.ParserDelegator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Swing.Text.Html.HTMLEditorKit.Parser.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure SetDefaultDTD ;

   --  protected
   function CreateDTD (P1_DTD : access Standard.Javax.Swing.Text.Html.Parser.DTD.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Javax.Swing.Text.Html.Parser.DTD.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ParserDelegator (This : Ref := null)
                                 return Ref;

   procedure Parse (This : access Typ;
                    P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                    P2_ParserCallback : access Standard.Javax.Swing.Text.Html.HTMLEditorKit.ParserCallback.Typ'Class;
                    P3_Boolean : Java.Boolean);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetDefaultDTD, "setDefaultDTD");
   pragma Import (Java, CreateDTD, "createDTD");
   pragma Java_Constructor (New_ParserDelegator);
   pragma Import (Java, Parse, "parse");

end Javax.Swing.Text.Html.Parser.ParserDelegator;
pragma Import (Java, Javax.Swing.Text.Html.Parser.ParserDelegator, "javax.swing.text.html.parser.ParserDelegator");
pragma Extensions_Allowed (Off);
