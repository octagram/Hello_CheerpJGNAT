pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.Parser.Element;
with Java.Lang.Object;

package Javax.Swing.Text.Html.Parser.TagElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TagElement (P1_Element : access Standard.Javax.Swing.Text.Html.Parser.Element.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_TagElement (P1_Element : access Standard.Javax.Swing.Text.Html.Parser.Element.Typ'Class;
                            P2_Boolean : Java.Boolean; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function BreaksFlow (This : access Typ)
                        return Java.Boolean;

   function IsPreformatted (This : access Typ)
                            return Java.Boolean;

   function GetElement (This : access Typ)
                        return access Javax.Swing.Text.Html.Parser.Element.Typ'Class;

   function GetHTMLTag (This : access Typ)
                        return access Javax.Swing.Text.Html.HTML.Tag.Typ'Class;

   function Fictional (This : access Typ)
                       return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TagElement);
   pragma Import (Java, BreaksFlow, "breaksFlow");
   pragma Import (Java, IsPreformatted, "isPreformatted");
   pragma Import (Java, GetElement, "getElement");
   pragma Import (Java, GetHTMLTag, "getHTMLTag");
   pragma Import (Java, Fictional, "fictional");

end Javax.Swing.Text.Html.Parser.TagElement;
pragma Import (Java, Javax.Swing.Text.Html.Parser.TagElement, "javax.swing.text.html.parser.TagElement");
pragma Extensions_Allowed (Off);
