pragma Extensions_Allowed (On);
package Javax.Swing.Text.Html.Parser is
   pragma Preelaborate;
end Javax.Swing.Text.Html.Parser;
pragma Import (Java, Javax.Swing.Text.Html.Parser, "javax.swing.text.html.parser");
pragma Extensions_Allowed (Off);
