pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Javax.Swing.Text.View;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.Html.StyleSheet.ListPainter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Float : Java.Float;
                    P3_Float : Java.Float;
                    P4_Float : Java.Float;
                    P5_Float : Java.Float;
                    P6_View : access Standard.Javax.Swing.Text.View.Typ'Class;
                    P7_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Paint, "paint");

end Javax.Swing.Text.Html.StyleSheet.ListPainter;
pragma Import (Java, Javax.Swing.Text.Html.StyleSheet.ListPainter, "javax.swing.text.html.StyleSheet$ListPainter");
pragma Extensions_Allowed (Off);
