pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Io.Reader;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Html.CSS.Attribute;
limited with Javax.Swing.Text.Html.HTML.Tag;
limited with Javax.Swing.Text.Html.StyleSheet.BoxPainter;
limited with Javax.Swing.Text.Html.StyleSheet.ListPainter;
limited with Javax.Swing.Text.MutableAttributeSet;
limited with Javax.Swing.Text.Style;
limited with Javax.Swing.Text.StyleContext.SmallAttributeSet;
limited with Javax.Swing.Text.View;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractDocument.AttributeContext;
with Javax.Swing.Text.StyleContext;

package Javax.Swing.Text.Html.StyleSheet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AttributeContext_I : Javax.Swing.Text.AbstractDocument.AttributeContext.Ref)
    is new Javax.Swing.Text.StyleContext.Typ(Serializable_I,
                                             AttributeContext_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StyleSheet (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRule (This : access Typ;
                     P1_Tag : access Standard.Javax.Swing.Text.Html.HTML.Tag.Typ'Class;
                     P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                     return access Javax.Swing.Text.Style.Typ'Class;

   function GetRule (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Swing.Text.Style.Typ'Class;

   procedure AddRule (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetDeclaration (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure LoadRules (This : access Typ;
                        P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                        P2_URL : access Standard.Java.Net.URL.Typ'Class);
   --  can raise Java.Io.IOException.Except

   function GetViewAttributes (This : access Typ;
                               P1_View : access Standard.Javax.Swing.Text.View.Typ'Class)
                               return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure RemoveStyle (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AddStyleSheet (This : access Typ;
                            P1_StyleSheet : access Standard.Javax.Swing.Text.Html.StyleSheet.Typ'Class);

   procedure RemoveStyleSheet (This : access Typ;
                               P1_StyleSheet : access Standard.Javax.Swing.Text.Html.StyleSheet.Typ'Class);

   function GetStyleSheets (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   procedure ImportStyleSheet (This : access Typ;
                               P1_URL : access Standard.Java.Net.URL.Typ'Class);

   procedure SetBase (This : access Typ;
                      P1_URL : access Standard.Java.Net.URL.Typ'Class);

   function GetBase (This : access Typ)
                     return access Java.Net.URL.Typ'Class;

   procedure AddCSSAttribute (This : access Typ;
                              P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                              P2_Attribute : access Standard.Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class);

   function AddCSSAttributeFromHTML (This : access Typ;
                                     P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                                     P2_Attribute : access Standard.Javax.Swing.Text.Html.CSS.Attribute.Typ'Class;
                                     P3_String : access Standard.Java.Lang.String.Typ'Class)
                                     return Java.Boolean;

   function TranslateHTMLToCSS (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function AddAttribute (This : access Typ;
                          P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function AddAttributes (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                           P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function RemoveAttribute (This : access Typ;
                             P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function RemoveAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                              P2_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function RemoveAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                              P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;

   --  protected
   function CreateSmallAttributeSet (This : access Typ;
                                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                     return access Javax.Swing.Text.StyleContext.SmallAttributeSet.Typ'Class;

   --  protected
   function CreateLargeAttributeSet (This : access Typ;
                                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                     return access Javax.Swing.Text.MutableAttributeSet.Typ'Class;

   function GetFont (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function GetForeground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   function GetBackground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   function GetBoxPainter (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Javax.Swing.Text.Html.StyleSheet.BoxPainter.Typ'Class;

   function GetListPainter (This : access Typ;
                            P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                            return access Javax.Swing.Text.Html.StyleSheet.ListPainter.Typ'Class;

   procedure SetBaseFontSize (This : access Typ;
                              P1_Int : Java.Int);

   procedure SetBaseFontSize (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetIndexOfSize (P1_Float : Java.Float)
                            return Java.Int;

   function GetPointSize (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   function GetPointSize (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Float;

   function StringToColor (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StyleSheet);
   pragma Import (Java, GetRule, "getRule");
   pragma Import (Java, AddRule, "addRule");
   pragma Import (Java, GetDeclaration, "getDeclaration");
   pragma Import (Java, LoadRules, "loadRules");
   pragma Import (Java, GetViewAttributes, "getViewAttributes");
   pragma Import (Java, RemoveStyle, "removeStyle");
   pragma Import (Java, AddStyleSheet, "addStyleSheet");
   pragma Import (Java, RemoveStyleSheet, "removeStyleSheet");
   pragma Import (Java, GetStyleSheets, "getStyleSheets");
   pragma Import (Java, ImportStyleSheet, "importStyleSheet");
   pragma Import (Java, SetBase, "setBase");
   pragma Import (Java, GetBase, "getBase");
   pragma Import (Java, AddCSSAttribute, "addCSSAttribute");
   pragma Import (Java, AddCSSAttributeFromHTML, "addCSSAttributeFromHTML");
   pragma Import (Java, TranslateHTMLToCSS, "translateHTMLToCSS");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, AddAttributes, "addAttributes");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, RemoveAttributes, "removeAttributes");
   pragma Import (Java, CreateSmallAttributeSet, "createSmallAttributeSet");
   pragma Import (Java, CreateLargeAttributeSet, "createLargeAttributeSet");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetBoxPainter, "getBoxPainter");
   pragma Import (Java, GetListPainter, "getListPainter");
   pragma Import (Java, SetBaseFontSize, "setBaseFontSize");
   pragma Import (Java, GetIndexOfSize, "getIndexOfSize");
   pragma Import (Java, GetPointSize, "getPointSize");
   pragma Import (Java, StringToColor, "stringToColor");

end Javax.Swing.Text.Html.StyleSheet;
pragma Import (Java, Javax.Swing.Text.Html.StyleSheet, "javax.swing.text.html.StyleSheet");
pragma Extensions_Allowed (Off);
