pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.SizeRequirements;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Html.StyleSheet;
limited with Javax.Swing.Text.View;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.BoxView;

package Javax.Swing.Text.Html.TableView.RowView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.BoxView.Typ(SwingConstants_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   --  protected
   function GetStyleSheet (This : access Typ)
                           return access Javax.Swing.Text.Html.StyleSheet.Typ'Class;

   procedure PreferenceChanged (This : access Typ;
                                P1_View : access Standard.Javax.Swing.Text.View.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean);

   --  protected
   function CalculateMajorAxisRequirements (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                            return access Javax.Swing.SizeRequirements.Typ'Class;

   function GetMinimumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetMaximumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_View_Arr : access Javax.Swing.Text.View.Arr_Obj);

   --  protected
   function CalculateMinorAxisRequirements (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                            return access Javax.Swing.SizeRequirements.Typ'Class;

   --  protected
   procedure LayoutMajorAxis (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int_Arr : Java.Int_Arr;
                              P4_Int_Arr : Java.Int_Arr);

   --  protected
   procedure LayoutMinorAxis (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int_Arr : Java.Int_Arr;
                              P4_Int_Arr : Java.Int_Arr);

   function GetResizeWeight (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   --  protected
   function GetViewAtPosition (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return access Javax.Swing.Text.View.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, GetStyleSheet, "getStyleSheet");
   pragma Import (Java, PreferenceChanged, "preferenceChanged");
   pragma Import (Java, CalculateMajorAxisRequirements, "calculateMajorAxisRequirements");
   pragma Import (Java, GetMinimumSpan, "getMinimumSpan");
   pragma Import (Java, GetMaximumSpan, "getMaximumSpan");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, ChangedUpdate, "changedUpdate");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, CalculateMinorAxisRequirements, "calculateMinorAxisRequirements");
   pragma Import (Java, LayoutMajorAxis, "layoutMajorAxis");
   pragma Import (Java, LayoutMinorAxis, "layoutMinorAxis");
   pragma Import (Java, GetResizeWeight, "getResizeWeight");
   pragma Import (Java, GetViewAtPosition, "getViewAtPosition");

end Javax.Swing.Text.Html.TableView.RowView;
pragma Import (Java, Javax.Swing.Text.Html.TableView.RowView, "javax.swing.text.html.TableView$RowView");
pragma Extensions_Allowed (Off);
