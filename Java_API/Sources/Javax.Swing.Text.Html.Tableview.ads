pragma Extensions_Allowed (On);
package Javax.Swing.Text.Html.TableView is
   pragma Preelaborate;
end Javax.Swing.Text.Html.TableView;
pragma Import (Java, Javax.Swing.Text.Html.TableView, "javax.swing.text.html.TableView");
pragma Extensions_Allowed (Off);
