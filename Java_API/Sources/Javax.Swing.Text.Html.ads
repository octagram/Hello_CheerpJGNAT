pragma Extensions_Allowed (On);
package Javax.Swing.Text.Html is
   pragma Preelaborate;
end Javax.Swing.Text.Html;
pragma Import (Java, Javax.Swing.Text.Html, "javax.swing.text.html");
pragma Extensions_Allowed (Off);
