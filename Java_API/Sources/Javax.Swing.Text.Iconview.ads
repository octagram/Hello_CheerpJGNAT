pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position.Bias;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.View;

package Javax.Swing.Text.IconView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.View.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IconView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   function GetAlignment (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_IconView);
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");

end Javax.Swing.Text.IconView;
pragma Import (Java, Javax.Swing.Text.IconView, "javax.swing.text.IconView");
pragma Extensions_Allowed (Off);
