pragma Extensions_Allowed (On);
limited with Java.Lang.Comparable;
limited with Java.Lang.String;
limited with Java.Text.Format;
limited with Java.Text.Format.Field;
limited with Javax.Swing.Action;
limited with Javax.Swing.JFormattedTextField;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.DefaultFormatter;

package Javax.Swing.Text.InternationalFormatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Swing.Text.DefaultFormatter.Typ(Serializable_I,
                                                 Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InternationalFormatter (This : Ref := null)
                                        return Ref;

   function New_InternationalFormatter (P1_Format : access Standard.Java.Text.Format.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFormat (This : access Typ;
                        P1_Format : access Standard.Java.Text.Format.Typ'Class);

   function GetFormat (This : access Typ)
                       return access Java.Text.Format.Typ'Class;

   procedure SetMinimum (This : access Typ;
                         P1_Comparable : access Standard.Java.Lang.Comparable.Typ'Class);

   function GetMinimum (This : access Typ)
                        return access Java.Lang.Comparable.Typ'Class;

   procedure SetMaximum (This : access Typ;
                         P1_Comparable : access Standard.Java.Lang.Comparable.Typ'Class);

   function GetMaximum (This : access Typ)
                        return access Java.Lang.Comparable.Typ'Class;

   procedure Install (This : access Typ;
                      P1_JFormattedTextField : access Standard.Javax.Swing.JFormattedTextField.Typ'Class);

   function ValueToString (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.String.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   function StringToValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   function GetFields (This : access Typ;
                       P1_Int : Java.Int)
                       return Standard.Java.Lang.Object.Ref;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   --  protected
   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InternationalFormatter);
   pragma Import (Java, SetFormat, "setFormat");
   pragma Import (Java, GetFormat, "getFormat");
   pragma Import (Java, SetMinimum, "setMinimum");
   pragma Import (Java, GetMinimum, "getMinimum");
   pragma Import (Java, SetMaximum, "setMaximum");
   pragma Import (Java, GetMaximum, "getMaximum");
   pragma Import (Java, Install, "install");
   pragma Import (Java, ValueToString, "valueToString");
   pragma Import (Java, StringToValue, "stringToValue");
   pragma Import (Java, GetFields, "getFields");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetActions, "getActions");

end Javax.Swing.Text.InternationalFormatter;
pragma Import (Java, Javax.Swing.Text.InternationalFormatter, "javax.swing.text.InternationalFormatter");
pragma Extensions_Allowed (Off);
