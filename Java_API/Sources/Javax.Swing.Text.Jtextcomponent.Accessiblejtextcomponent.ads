pragma Extensions_Allowed (On);
limited with Java.Awt.Point;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Accessibility.AccessibleRole;
limited with Javax.Accessibility.AccessibleStateSet;
limited with Javax.Accessibility.AccessibleTextSequence;
limited with Javax.Swing.Event.CaretEvent;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Text.AttributeSet;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.AccessibleAction;
with Javax.Accessibility.AccessibleComponent;
with Javax.Accessibility.AccessibleEditableText;
with Javax.Accessibility.AccessibleExtendedComponent;
with Javax.Accessibility.AccessibleExtendedText;
with Javax.Accessibility.AccessibleText;
with Javax.Swing.Event.CaretListener;
with Javax.Swing.Event.DocumentListener;
with Javax.Swing.JComponent.AccessibleJComponent;

package Javax.Swing.Text.JTextComponent.AccessibleJTextComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AccessibleAction_I : Javax.Accessibility.AccessibleAction.Ref;
            AccessibleComponent_I : Javax.Accessibility.AccessibleComponent.Ref;
            AccessibleEditableText_I : Javax.Accessibility.AccessibleEditableText.Ref;
            AccessibleExtendedComponent_I : Javax.Accessibility.AccessibleExtendedComponent.Ref;
            AccessibleExtendedText_I : Javax.Accessibility.AccessibleExtendedText.Ref;
            AccessibleText_I : Javax.Accessibility.AccessibleText.Ref;
            CaretListener_I : Javax.Swing.Event.CaretListener.Ref;
            DocumentListener_I : Javax.Swing.Event.DocumentListener.Ref)
    is new Javax.Swing.JComponent.AccessibleJComponent.Typ(Serializable_I,
                                                           AccessibleComponent_I,
                                                           AccessibleExtendedComponent_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AccessibleJTextComponent (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure CaretUpdate (This : access Typ;
                          P1_CaretEvent : access Standard.Javax.Swing.Event.CaretEvent.Typ'Class);

   procedure InsertUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class);

   function GetAccessibleStateSet (This : access Typ)
                                   return access Javax.Accessibility.AccessibleStateSet.Typ'Class;

   function GetAccessibleRole (This : access Typ)
                               return access Javax.Accessibility.AccessibleRole.Typ'Class;

   function GetAccessibleText (This : access Typ)
                               return access Javax.Accessibility.AccessibleText.Typ'Class;

   function GetIndexAtPoint (This : access Typ;
                             P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                             return Java.Int;

   function GetCharacterBounds (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Awt.Rectangle.Typ'Class;

   function GetCharCount (This : access Typ)
                          return Java.Int;

   function GetCaretPosition (This : access Typ)
                              return Java.Int;

   function GetCharacterAttribute (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetSelectionStart (This : access Typ)
                               return Java.Int;

   function GetSelectionEnd (This : access Typ)
                             return Java.Int;

   function GetSelectedText (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetAtIndex (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return access Java.Lang.String.Typ'Class;

   function GetAfterIndex (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Lang.String.Typ'Class;

   function GetBeforeIndex (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Java.Lang.String.Typ'Class;

   function GetAccessibleEditableText (This : access Typ)
                                       return access Javax.Accessibility.AccessibleEditableText.Typ'Class;

   procedure SetTextContents (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure InsertTextAtIndex (This : access Typ;
                                P1_Int : Java.Int;
                                P2_String : access Standard.Java.Lang.String.Typ'Class);

   function GetTextRange (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class;

   procedure Delete (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int);

   procedure Cut (This : access Typ;
                  P1_Int : Java.Int;
                  P2_Int : Java.Int);

   procedure Paste (This : access Typ;
                    P1_Int : Java.Int);

   procedure ReplaceText (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Int : Java.Int;
                          P3_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SelectText (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int);

   procedure SetAttributes (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int;
                            P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   function GetTextSequenceAt (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Int : Java.Int)
                               return access Javax.Accessibility.AccessibleTextSequence.Typ'Class;

   function GetTextSequenceAfter (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Int : Java.Int)
                                  return access Javax.Accessibility.AccessibleTextSequence.Typ'Class;

   function GetTextSequenceBefore (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Int : Java.Int)
                                   return access Javax.Accessibility.AccessibleTextSequence.Typ'Class;

   function GetTextBounds (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Int : Java.Int)
                           return access Java.Awt.Rectangle.Typ'Class;

   function GetAccessibleAction (This : access Typ)
                                 return access Javax.Accessibility.AccessibleAction.Typ'Class;

   function GetAccessibleActionCount (This : access Typ)
                                      return Java.Int;

   function GetAccessibleActionDescription (This : access Typ;
                                            P1_Int : Java.Int)
                                            return access Java.Lang.String.Typ'Class;

   function DoAccessibleAction (This : access Typ;
                                P1_Int : Java.Int)
                                return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AccessibleJTextComponent);
   pragma Import (Java, CaretUpdate, "caretUpdate");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, ChangedUpdate, "changedUpdate");
   pragma Import (Java, GetAccessibleStateSet, "getAccessibleStateSet");
   pragma Import (Java, GetAccessibleRole, "getAccessibleRole");
   pragma Import (Java, GetAccessibleText, "getAccessibleText");
   pragma Import (Java, GetIndexAtPoint, "getIndexAtPoint");
   pragma Import (Java, GetCharacterBounds, "getCharacterBounds");
   pragma Import (Java, GetCharCount, "getCharCount");
   pragma Import (Java, GetCaretPosition, "getCaretPosition");
   pragma Import (Java, GetCharacterAttribute, "getCharacterAttribute");
   pragma Import (Java, GetSelectionStart, "getSelectionStart");
   pragma Import (Java, GetSelectionEnd, "getSelectionEnd");
   pragma Import (Java, GetSelectedText, "getSelectedText");
   pragma Import (Java, GetAtIndex, "getAtIndex");
   pragma Import (Java, GetAfterIndex, "getAfterIndex");
   pragma Import (Java, GetBeforeIndex, "getBeforeIndex");
   pragma Import (Java, GetAccessibleEditableText, "getAccessibleEditableText");
   pragma Import (Java, SetTextContents, "setTextContents");
   pragma Import (Java, InsertTextAtIndex, "insertTextAtIndex");
   pragma Import (Java, GetTextRange, "getTextRange");
   pragma Import (Java, Delete, "delete");
   pragma Import (Java, Cut, "cut");
   pragma Import (Java, Paste, "paste");
   pragma Import (Java, ReplaceText, "replaceText");
   pragma Import (Java, SelectText, "selectText");
   pragma Import (Java, SetAttributes, "setAttributes");
   pragma Import (Java, GetTextSequenceAt, "getTextSequenceAt");
   pragma Import (Java, GetTextSequenceAfter, "getTextSequenceAfter");
   pragma Import (Java, GetTextSequenceBefore, "getTextSequenceBefore");
   pragma Import (Java, GetTextBounds, "getTextBounds");
   pragma Import (Java, GetAccessibleAction, "getAccessibleAction");
   pragma Import (Java, GetAccessibleActionCount, "getAccessibleActionCount");
   pragma Import (Java, GetAccessibleActionDescription, "getAccessibleActionDescription");
   pragma Import (Java, DoAccessibleAction, "doAccessibleAction");

end Javax.Swing.Text.JTextComponent.AccessibleJTextComponent;
pragma Import (Java, Javax.Swing.Text.JTextComponent.AccessibleJTextComponent, "javax.swing.text.JTextComponent$AccessibleJTextComponent");
pragma Extensions_Allowed (Off);
