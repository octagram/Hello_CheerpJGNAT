pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.Position.Bias;
with Java.Lang.Object;
with Javax.Swing.TransferHandler.DropLocation;

package Javax.Swing.Text.JTextComponent.DropLocation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Swing.TransferHandler.DropLocation.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIndex (This : access Typ)
                      return Java.Int;

   function GetBias (This : access Typ)
                     return access Javax.Swing.Text.Position.Bias.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, GetBias, "getBias");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Text.JTextComponent.DropLocation;
pragma Import (Java, Javax.Swing.Text.JTextComponent.DropLocation, "javax.swing.text.JTextComponent$DropLocation");
pragma Extensions_Allowed (Off);
