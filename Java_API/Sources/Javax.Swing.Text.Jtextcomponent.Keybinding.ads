pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.KeyStroke;
with Java.Lang.Object;

package Javax.Swing.Text.JTextComponent.KeyBinding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Key : access Javax.Swing.KeyStroke.Typ'Class;
      pragma Import (Java, Key, "key");

      ActionName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, ActionName, "actionName");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_KeyBinding (P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyBinding);

end Javax.Swing.Text.JTextComponent.KeyBinding;
pragma Import (Java, Javax.Swing.Text.JTextComponent.KeyBinding, "javax.swing.text.JTextComponent$KeyBinding");
pragma Extensions_Allowed (Off);
