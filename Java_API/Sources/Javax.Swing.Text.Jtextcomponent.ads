pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.ComponentOrientation;
limited with Java.Awt.Dimension;
limited with Java.Awt.Event.InputMethodEvent;
limited with Java.Awt.Event.InputMethodListener;
limited with Java.Awt.Event.MouseEvent;
limited with Java.Awt.Im.InputMethodRequests;
limited with Java.Awt.Insets;
limited with Java.Awt.Point;
limited with Java.Awt.Print.Printable;
limited with Java.Awt.Rectangle;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Java.Text.MessageFormat;
limited with Javax.Accessibility.AccessibleContext;
limited with Javax.Print.Attribute.PrintRequestAttributeSet;
limited with Javax.Print.PrintService;
limited with Javax.Swing.Action;
limited with Javax.Swing.DropMode;
limited with Javax.Swing.Event.CaretEvent;
limited with Javax.Swing.Event.CaretListener;
limited with Javax.Swing.Plaf.TextUI;
limited with Javax.Swing.Text.Caret;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Highlighter;
limited with Javax.Swing.Text.JTextComponent.DropLocation;
limited with Javax.Swing.Text.JTextComponent.KeyBinding;
limited with Javax.Swing.Text.Keymap;
limited with Javax.Swing.Text.NavigationFilter;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JComponent;
with Javax.Swing.Scrollable;

package Javax.Swing.Text.JTextComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref)
    is abstract new Javax.Swing.JComponent.Typ(MenuContainer_I,
                                               ImageObserver_I,
                                               Serializable_I)
      with null record;

   function New_JTextComponent (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetUI (This : access Typ)
                   return access Javax.Swing.Plaf.TextUI.Typ'Class;

   procedure SetUI (This : access Typ;
                    P1_TextUI : access Standard.Javax.Swing.Plaf.TextUI.Typ'Class);

   procedure UpdateUI (This : access Typ);

   procedure AddCaretListener (This : access Typ;
                               P1_CaretListener : access Standard.Javax.Swing.Event.CaretListener.Typ'Class);

   procedure RemoveCaretListener (This : access Typ;
                                  P1_CaretListener : access Standard.Javax.Swing.Event.CaretListener.Typ'Class);

   function GetCaretListeners (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireCaretUpdate (This : access Typ;
                              P1_CaretEvent : access Standard.Javax.Swing.Event.CaretEvent.Typ'Class);

   procedure SetDocument (This : access Typ;
                          P1_Document : access Standard.Javax.Swing.Text.Document.Typ'Class);

   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class;

   procedure SetComponentOrientation (This : access Typ;
                                      P1_ComponentOrientation : access Standard.Java.Awt.ComponentOrientation.Typ'Class);

   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   procedure SetMargin (This : access Typ;
                        P1_Insets : access Standard.Java.Awt.Insets.Typ'Class);

   function GetMargin (This : access Typ)
                       return access Java.Awt.Insets.Typ'Class;

   procedure SetNavigationFilter (This : access Typ;
                                  P1_NavigationFilter : access Standard.Javax.Swing.Text.NavigationFilter.Typ'Class);

   function GetNavigationFilter (This : access Typ)
                                 return access Javax.Swing.Text.NavigationFilter.Typ'Class;

   function GetCaret (This : access Typ)
                      return access Javax.Swing.Text.Caret.Typ'Class;

   procedure SetCaret (This : access Typ;
                       P1_Caret : access Standard.Javax.Swing.Text.Caret.Typ'Class);

   function GetHighlighter (This : access Typ)
                            return access Javax.Swing.Text.Highlighter.Typ'Class;

   procedure SetHighlighter (This : access Typ;
                             P1_Highlighter : access Standard.Javax.Swing.Text.Highlighter.Typ'Class);

   procedure SetKeymap (This : access Typ;
                        P1_Keymap : access Standard.Javax.Swing.Text.Keymap.Typ'Class);

   procedure SetDragEnabled (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function GetDragEnabled (This : access Typ)
                            return Java.Boolean;

   --  final
   procedure SetDropMode (This : access Typ;
                          P1_DropMode : access Standard.Javax.Swing.DropMode.Typ'Class);

   --  final
   function GetDropMode (This : access Typ)
                         return access Javax.Swing.DropMode.Typ'Class;

   --  final
   function GetDropLocation (This : access Typ)
                             return access Javax.Swing.Text.JTextComponent.DropLocation.Typ'Class;

   function GetKeymap (This : access Typ)
                       return access Javax.Swing.Text.Keymap.Typ'Class;

   function AddKeymap (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Keymap : access Standard.Javax.Swing.Text.Keymap.Typ'Class)
                       return access Javax.Swing.Text.Keymap.Typ'Class;

   function RemoveKeymap (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Swing.Text.Keymap.Typ'Class;

   function GetKeymap (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Javax.Swing.Text.Keymap.Typ'Class;

   procedure LoadKeymap (P1_Keymap : access Standard.Javax.Swing.Text.Keymap.Typ'Class;
                         P2_KeyBinding_Arr : access Javax.Swing.Text.JTextComponent.KeyBinding.Arr_Obj;
                         P3_Action_Arr : access Javax.Swing.Action.Arr_Obj);

   function GetCaretColor (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetCaretColor (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetSelectionColor (This : access Typ)
                               return access Java.Awt.Color.Typ'Class;

   procedure SetSelectionColor (This : access Typ;
                                P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetSelectedTextColor (This : access Typ)
                                  return access Java.Awt.Color.Typ'Class;

   procedure SetSelectedTextColor (This : access Typ;
                                   P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetDisabledTextColor (This : access Typ)
                                  return access Java.Awt.Color.Typ'Class;

   procedure SetDisabledTextColor (This : access Typ;
                                   P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   procedure ReplaceSelection (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetText (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int)
                         return access Java.Awt.Rectangle.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Point : access Standard.Java.Awt.Point.Typ'Class)
                         return Java.Int;

   procedure Cut (This : access Typ);

   procedure Copy (This : access Typ);

   procedure Paste (This : access Typ);

   procedure MoveCaretPosition (This : access Typ;
                                P1_Int : Java.Int);

   procedure SetFocusAccelerator (This : access Typ;
                                  P1_Char : Java.Char);

   function GetFocusAccelerator (This : access Typ)
                                 return Java.Char;

   procedure Read (This : access Typ;
                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                   P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure Write (This : access Typ;
                    P1_Writer : access Standard.Java.Io.Writer.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure RemoveNotify (This : access Typ);

   procedure SetCaretPosition (This : access Typ;
                               P1_Int : Java.Int);

   function GetCaretPosition (This : access Typ)
                              return Java.Int;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetSelectedText (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function IsEditable (This : access Typ)
                        return Java.Boolean;

   procedure SetEditable (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function GetSelectionStart (This : access Typ)
                               return Java.Int;

   procedure SetSelectionStart (This : access Typ;
                                P1_Int : Java.Int);

   function GetSelectionEnd (This : access Typ)
                             return Java.Int;

   procedure SetSelectionEnd (This : access Typ;
                              P1_Int : Java.Int);

   procedure select_K (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int);

   procedure SelectAll (This : access Typ);

   function GetToolTipText (This : access Typ;
                            P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetPreferredScrollableViewportSize (This : access Typ)
                                                return access Java.Awt.Dimension.Typ'Class;

   function GetScrollableUnitIncrement (This : access Typ;
                                        P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                        P2_Int : Java.Int;
                                        P3_Int : Java.Int)
                                        return Java.Int;

   function GetScrollableBlockIncrement (This : access Typ;
                                         P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int)
                                         return Java.Int;

   function GetScrollableTracksViewportWidth (This : access Typ)
                                              return Java.Boolean;

   function GetScrollableTracksViewportHeight (This : access Typ)
                                               return Java.Boolean;

   function Print (This : access Typ)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except

   function Print (This : access Typ;
                   P1_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P2_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except

   function Print (This : access Typ;
                   P1_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P2_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                   P3_Boolean : Java.Boolean;
                   P4_PrintService : access Standard.Javax.Print.PrintService.Typ'Class;
                   P5_PrintRequestAttributeSet : access Standard.Javax.Print.Attribute.PrintRequestAttributeSet.Typ'Class;
                   P6_Boolean : Java.Boolean)
                   return Java.Boolean;
   --  can raise Java.Awt.Print.PrinterException.Except

   function GetPrintable (This : access Typ;
                          P1_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class;
                          P2_MessageFormat : access Standard.Java.Text.MessageFormat.Typ'Class)
                          return access Java.Awt.Print.Printable.Typ'Class;

   function GetAccessibleContext (This : access Typ)
                                  return access Javax.Accessibility.AccessibleContext.Typ'Class;

   --  protected
   function ParamString (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  protected
   procedure ProcessInputMethodEvent (This : access Typ;
                                      P1_InputMethodEvent : access Standard.Java.Awt.Event.InputMethodEvent.Typ'Class);

   function GetInputMethodRequests (This : access Typ)
                                    return access Java.Awt.Im.InputMethodRequests.Typ'Class;

   procedure AddInputMethodListener (This : access Typ;
                                     P1_InputMethodListener : access Standard.Java.Awt.Event.InputMethodListener.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FOCUS_ACCELERATOR_KEY : constant access Java.Lang.String.Typ'Class;

   --  final
   DEFAULT_KEYMAP : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JTextComponent);
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, SetUI, "setUI");
   pragma Import (Java, UpdateUI, "updateUI");
   pragma Import (Java, AddCaretListener, "addCaretListener");
   pragma Import (Java, RemoveCaretListener, "removeCaretListener");
   pragma Import (Java, GetCaretListeners, "getCaretListeners");
   pragma Import (Java, FireCaretUpdate, "fireCaretUpdate");
   pragma Import (Java, SetDocument, "setDocument");
   pragma Import (Java, GetDocument, "getDocument");
   pragma Import (Java, SetComponentOrientation, "setComponentOrientation");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, SetMargin, "setMargin");
   pragma Import (Java, GetMargin, "getMargin");
   pragma Import (Java, SetNavigationFilter, "setNavigationFilter");
   pragma Import (Java, GetNavigationFilter, "getNavigationFilter");
   pragma Import (Java, GetCaret, "getCaret");
   pragma Import (Java, SetCaret, "setCaret");
   pragma Import (Java, GetHighlighter, "getHighlighter");
   pragma Import (Java, SetHighlighter, "setHighlighter");
   pragma Import (Java, SetKeymap, "setKeymap");
   pragma Import (Java, SetDragEnabled, "setDragEnabled");
   pragma Import (Java, GetDragEnabled, "getDragEnabled");
   pragma Import (Java, SetDropMode, "setDropMode");
   pragma Import (Java, GetDropMode, "getDropMode");
   pragma Import (Java, GetDropLocation, "getDropLocation");
   pragma Import (Java, GetKeymap, "getKeymap");
   pragma Import (Java, AddKeymap, "addKeymap");
   pragma Import (Java, RemoveKeymap, "removeKeymap");
   pragma Import (Java, LoadKeymap, "loadKeymap");
   pragma Import (Java, GetCaretColor, "getCaretColor");
   pragma Import (Java, SetCaretColor, "setCaretColor");
   pragma Import (Java, GetSelectionColor, "getSelectionColor");
   pragma Import (Java, SetSelectionColor, "setSelectionColor");
   pragma Import (Java, GetSelectedTextColor, "getSelectedTextColor");
   pragma Import (Java, SetSelectedTextColor, "setSelectedTextColor");
   pragma Import (Java, GetDisabledTextColor, "getDisabledTextColor");
   pragma Import (Java, SetDisabledTextColor, "setDisabledTextColor");
   pragma Import (Java, ReplaceSelection, "replaceSelection");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, Cut, "cut");
   pragma Import (Java, Copy, "copy");
   pragma Import (Java, Paste, "paste");
   pragma Import (Java, MoveCaretPosition, "moveCaretPosition");
   pragma Import (Java, SetFocusAccelerator, "setFocusAccelerator");
   pragma Import (Java, GetFocusAccelerator, "getFocusAccelerator");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Write, "write");
   pragma Import (Java, RemoveNotify, "removeNotify");
   pragma Import (Java, SetCaretPosition, "setCaretPosition");
   pragma Import (Java, GetCaretPosition, "getCaretPosition");
   pragma Import (Java, SetText, "setText");
   pragma Import (Java, GetSelectedText, "getSelectedText");
   pragma Import (Java, IsEditable, "isEditable");
   pragma Import (Java, SetEditable, "setEditable");
   pragma Import (Java, GetSelectionStart, "getSelectionStart");
   pragma Import (Java, SetSelectionStart, "setSelectionStart");
   pragma Import (Java, GetSelectionEnd, "getSelectionEnd");
   pragma Import (Java, SetSelectionEnd, "setSelectionEnd");
   pragma Import (Java, select_K, "select");
   pragma Import (Java, SelectAll, "selectAll");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, GetPreferredScrollableViewportSize, "getPreferredScrollableViewportSize");
   pragma Import (Java, GetScrollableUnitIncrement, "getScrollableUnitIncrement");
   pragma Import (Java, GetScrollableBlockIncrement, "getScrollableBlockIncrement");
   pragma Import (Java, GetScrollableTracksViewportWidth, "getScrollableTracksViewportWidth");
   pragma Import (Java, GetScrollableTracksViewportHeight, "getScrollableTracksViewportHeight");
   pragma Import (Java, Print, "print");
   pragma Import (Java, GetPrintable, "getPrintable");
   pragma Import (Java, GetAccessibleContext, "getAccessibleContext");
   pragma Import (Java, ParamString, "paramString");
   pragma Import (Java, ProcessInputMethodEvent, "processInputMethodEvent");
   pragma Import (Java, GetInputMethodRequests, "getInputMethodRequests");
   pragma Import (Java, AddInputMethodListener, "addInputMethodListener");
   pragma Import (Java, FOCUS_ACCELERATOR_KEY, "FOCUS_ACCELERATOR_KEY");
   pragma Import (Java, DEFAULT_KEYMAP, "DEFAULT_KEYMAP");

end Javax.Swing.Text.JTextComponent;
pragma Import (Java, Javax.Swing.Text.JTextComponent, "javax.swing.text.JTextComponent");
pragma Extensions_Allowed (Off);
