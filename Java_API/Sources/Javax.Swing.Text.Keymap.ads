pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.KeyStroke;
with Java.Lang.Object;

package Javax.Swing.Text.Keymap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetDefaultAction (This : access Typ)
                              return access Javax.Swing.Action.Typ'Class is abstract;

   procedure SetDefaultAction (This : access Typ;
                               P1_Action : access Standard.Javax.Swing.Action.Typ'Class) is abstract;

   function GetAction (This : access Typ;
                       P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class)
                       return access Javax.Swing.Action.Typ'Class is abstract;

   function GetBoundKeyStrokes (This : access Typ)
                                return Standard.Java.Lang.Object.Ref is abstract;

   function GetBoundActions (This : access Typ)
                             return Standard.Java.Lang.Object.Ref is abstract;

   function GetKeyStrokesForAction (This : access Typ;
                                    P1_Action : access Standard.Javax.Swing.Action.Typ'Class)
                                    return Standard.Java.Lang.Object.Ref is abstract;

   function IsLocallyDefined (This : access Typ;
                              P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class)
                              return Java.Boolean is abstract;

   procedure AddActionForKeyStroke (This : access Typ;
                                    P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class;
                                    P2_Action : access Standard.Javax.Swing.Action.Typ'Class) is abstract;

   procedure RemoveKeyStrokeBinding (This : access Typ;
                                     P1_KeyStroke : access Standard.Javax.Swing.KeyStroke.Typ'Class) is abstract;

   procedure RemoveBindings (This : access Typ) is abstract;

   function GetResolveParent (This : access Typ)
                              return access Javax.Swing.Text.Keymap.Typ'Class is abstract;

   procedure SetResolveParent (This : access Typ;
                               P1_Keymap : access Standard.Javax.Swing.Text.Keymap.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetDefaultAction, "getDefaultAction");
   pragma Export (Java, SetDefaultAction, "setDefaultAction");
   pragma Export (Java, GetAction, "getAction");
   pragma Export (Java, GetBoundKeyStrokes, "getBoundKeyStrokes");
   pragma Export (Java, GetBoundActions, "getBoundActions");
   pragma Export (Java, GetKeyStrokesForAction, "getKeyStrokesForAction");
   pragma Export (Java, IsLocallyDefined, "isLocallyDefined");
   pragma Export (Java, AddActionForKeyStroke, "addActionForKeyStroke");
   pragma Export (Java, RemoveKeyStrokeBinding, "removeKeyStrokeBinding");
   pragma Export (Java, RemoveBindings, "removeBindings");
   pragma Export (Java, GetResolveParent, "getResolveParent");
   pragma Export (Java, SetResolveParent, "setResolveParent");

end Javax.Swing.Text.Keymap;
pragma Import (Java, Javax.Swing.Text.Keymap, "javax.swing.text.Keymap");
pragma Extensions_Allowed (Off);
