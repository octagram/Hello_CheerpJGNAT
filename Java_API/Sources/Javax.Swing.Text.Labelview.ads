pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.GlyphView;
with Javax.Swing.Text.TabableView;

package Javax.Swing.Text.LabelView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TabableView_I : Javax.Swing.Text.TabableView.Ref)
    is new Javax.Swing.Text.GlyphView.Typ(Cloneable_I,
                                          SwingConstants_I,
                                          TabableView_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LabelView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure SetUnderline (This : access Typ;
                           P1_Boolean : Java.Boolean);

   --  protected
   procedure SetStrikeThrough (This : access Typ;
                               P1_Boolean : Java.Boolean);

   --  protected
   procedure SetSuperscript (This : access Typ;
                             P1_Boolean : Java.Boolean);

   --  protected
   procedure SetSubscript (This : access Typ;
                           P1_Boolean : Java.Boolean);

   --  protected
   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   --  protected
   procedure SetPropertiesFromAttributes (This : access Typ);

   function GetBackground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetForeground (This : access Typ)
                           return access Java.Awt.Color.Typ'Class;

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   function IsUnderline (This : access Typ)
                         return Java.Boolean;

   function IsStrikeThrough (This : access Typ)
                             return Java.Boolean;

   function IsSubscript (This : access Typ)
                         return Java.Boolean;

   function IsSuperscript (This : access Typ)
                           return Java.Boolean;

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LabelView);
   pragma Import (Java, SetUnderline, "setUnderline");
   pragma Import (Java, SetStrikeThrough, "setStrikeThrough");
   pragma Import (Java, SetSuperscript, "setSuperscript");
   pragma Import (Java, SetSubscript, "setSubscript");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, SetPropertiesFromAttributes, "setPropertiesFromAttributes");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, IsUnderline, "isUnderline");
   pragma Import (Java, IsStrikeThrough, "isStrikeThrough");
   pragma Import (Java, IsSubscript, "isSubscript");
   pragma Import (Java, IsSuperscript, "isSuperscript");
   pragma Import (Java, ChangedUpdate, "changedUpdate");

end Javax.Swing.Text.LabelView;
pragma Import (Java, Javax.Swing.Text.LabelView, "javax.swing.text.LabelView");
pragma Extensions_Allowed (Off);
