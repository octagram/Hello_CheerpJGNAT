pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.Text.Highlighter.HighlightPainter;

package Javax.Swing.Text.LayeredHighlighter.LayerPainter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(HighlightPainter_I : Javax.Swing.Text.Highlighter.HighlightPainter.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_LayerPainter (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function PaintLayer (This : access Typ;
                        P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                        P5_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                        P6_View : access Standard.Javax.Swing.Text.View.Typ'Class)
                        return access Java.Awt.Shape.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LayerPainter);
   pragma Export (Java, PaintLayer, "paintLayer");

end Javax.Swing.Text.LayeredHighlighter.LayerPainter;
pragma Import (Java, Javax.Swing.Text.LayeredHighlighter.LayerPainter, "javax.swing.text.LayeredHighlighter$LayerPainter");
pragma Extensions_Allowed (Off);
