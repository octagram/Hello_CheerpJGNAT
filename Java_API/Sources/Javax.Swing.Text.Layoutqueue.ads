pragma Extensions_Allowed (On);
limited with Java.Lang.Runnable;
with Java.Lang.Object;

package Javax.Swing.Text.LayoutQueue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LayoutQueue (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultQueue return access Javax.Swing.Text.LayoutQueue.Typ'Class;

   procedure SetDefaultQueue (P1_LayoutQueue : access Standard.Javax.Swing.Text.LayoutQueue.Typ'Class);

   --  synchronized
   procedure AddTask (This : access Typ;
                      P1_Runnable : access Standard.Java.Lang.Runnable.Typ'Class);

   --  protected  synchronized
   function WaitForWork (This : access Typ)
                         return access Java.Lang.Runnable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LayoutQueue);
   pragma Import (Java, GetDefaultQueue, "getDefaultQueue");
   pragma Import (Java, SetDefaultQueue, "setDefaultQueue");
   pragma Import (Java, AddTask, "addTask");
   pragma Import (Java, WaitForWork, "waitForWork");

end Javax.Swing.Text.LayoutQueue;
pragma Import (Java, Javax.Swing.Text.LayoutQueue, "javax.swing.text.LayoutQueue");
pragma Extensions_Allowed (Off);
