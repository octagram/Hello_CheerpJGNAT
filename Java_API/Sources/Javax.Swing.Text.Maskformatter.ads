pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.JFormattedTextField;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.DefaultFormatter;

package Javax.Swing.Text.MaskFormatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Swing.Text.DefaultFormatter.Typ(Serializable_I,
                                                 Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MaskFormatter (This : Ref := null)
                               return Ref;

   function New_MaskFormatter (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Java.Text.ParseException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetMask (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Text.ParseException.Except

   function GetMask (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetValidCharacters (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetValidCharacters (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure SetInvalidCharacters (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetInvalidCharacters (This : access Typ)
                                  return access Java.Lang.String.Typ'Class;

   procedure SetPlaceholder (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPlaceholder (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetPlaceholderCharacter (This : access Typ;
                                      P1_Char : Java.Char);

   function GetPlaceholderCharacter (This : access Typ)
                                     return Java.Char;

   procedure SetValueContainsLiteralCharacters (This : access Typ;
                                                P1_Boolean : Java.Boolean);

   function GetValueContainsLiteralCharacters (This : access Typ)
                                               return Java.Boolean;

   function StringToValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   function ValueToString (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return access Java.Lang.String.Typ'Class;
   --  can raise Java.Text.ParseException.Except

   procedure Install (This : access Typ;
                      P1_JFormattedTextField : access Standard.Javax.Swing.JFormattedTextField.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MaskFormatter);
   pragma Import (Java, SetMask, "setMask");
   pragma Import (Java, GetMask, "getMask");
   pragma Import (Java, SetValidCharacters, "setValidCharacters");
   pragma Import (Java, GetValidCharacters, "getValidCharacters");
   pragma Import (Java, SetInvalidCharacters, "setInvalidCharacters");
   pragma Import (Java, GetInvalidCharacters, "getInvalidCharacters");
   pragma Import (Java, SetPlaceholder, "setPlaceholder");
   pragma Import (Java, GetPlaceholder, "getPlaceholder");
   pragma Import (Java, SetPlaceholderCharacter, "setPlaceholderCharacter");
   pragma Import (Java, GetPlaceholderCharacter, "getPlaceholderCharacter");
   pragma Import (Java, SetValueContainsLiteralCharacters, "setValueContainsLiteralCharacters");
   pragma Import (Java, GetValueContainsLiteralCharacters, "getValueContainsLiteralCharacters");
   pragma Import (Java, StringToValue, "stringToValue");
   pragma Import (Java, ValueToString, "valueToString");
   pragma Import (Java, Install, "install");

end Javax.Swing.Text.MaskFormatter;
pragma Import (Java, Javax.Swing.Text.MaskFormatter, "javax.swing.text.MaskFormatter");
pragma Extensions_Allowed (Off);
