pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
with Java.Lang.Object;
with Javax.Swing.Text.AttributeSet;

package Javax.Swing.Text.MutableAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AttributeSet_I : Javax.Swing.Text.AttributeSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddAttribute (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure AddAttributes (This : access Typ;
                            P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;

   procedure RemoveAttribute (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure RemoveAttributes (This : access Typ;
                               P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class) is abstract;

   procedure RemoveAttributes (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;

   procedure SetResolveParent (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddAttribute, "addAttribute");
   pragma Export (Java, AddAttributes, "addAttributes");
   pragma Export (Java, RemoveAttribute, "removeAttribute");
   pragma Export (Java, RemoveAttributes, "removeAttributes");
   pragma Export (Java, SetResolveParent, "setResolveParent");

end Javax.Swing.Text.MutableAttributeSet;
pragma Import (Java, Javax.Swing.Text.MutableAttributeSet, "javax.swing.text.MutableAttributeSet");
pragma Extensions_Allowed (Off);
