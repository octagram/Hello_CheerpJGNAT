pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Caret;
limited with Javax.Swing.Text.Position.Bias;
with Java.Lang.Object;

package Javax.Swing.Text.NavigationFilter.FilterBypass is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_FilterBypass (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCaret (This : access Typ)
                      return access Javax.Swing.Text.Caret.Typ'Class is abstract;

   procedure SetDot (This : access Typ;
                     P1_Int : Java.Int;
                     P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class) is abstract;

   procedure MoveDot (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FilterBypass);
   pragma Export (Java, GetCaret, "getCaret");
   pragma Export (Java, SetDot, "setDot");
   pragma Export (Java, MoveDot, "moveDot");

end Javax.Swing.Text.NavigationFilter.FilterBypass;
pragma Import (Java, Javax.Swing.Text.NavigationFilter.FilterBypass, "javax.swing.text.NavigationFilter$FilterBypass");
pragma Extensions_Allowed (Off);
