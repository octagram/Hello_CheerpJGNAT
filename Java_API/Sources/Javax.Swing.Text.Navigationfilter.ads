pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.NavigationFilter.FilterBypass;
limited with Javax.Swing.Text.Position.Bias;
with Java.Lang.Object;

package Javax.Swing.Text.NavigationFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NavigationFilter (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDot (This : access Typ;
                     P1_FilterBypass : access Standard.Javax.Swing.Text.NavigationFilter.FilterBypass.Typ'Class;
                     P2_Int : Java.Int;
                     P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class);

   procedure MoveDot (This : access Typ;
                      P1_FilterBypass : access Standard.Javax.Swing.Text.NavigationFilter.FilterBypass.Typ'Class;
                      P2_Int : Java.Int;
                      P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class);

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                                       P2_Int : Java.Int;
                                       P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NavigationFilter);
   pragma Import (Java, SetDot, "setDot");
   pragma Import (Java, MoveDot, "moveDot");
   pragma Import (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");

end Javax.Swing.Text.NavigationFilter;
pragma Import (Java, Javax.Swing.Text.NavigationFilter, "javax.swing.text.NavigationFilter");
pragma Extensions_Allowed (Off);
