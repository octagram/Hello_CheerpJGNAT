pragma Extensions_Allowed (On);
limited with Java.Text.Format;
limited with Java.Text.NumberFormat;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.InternationalFormatter;

package Javax.Swing.Text.NumberFormatter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Swing.Text.InternationalFormatter.Typ(Serializable_I,
                                                       Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NumberFormatter (This : Ref := null)
                                 return Ref;

   function New_NumberFormatter (P1_NumberFormat : access Standard.Java.Text.NumberFormat.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFormat (This : access Typ;
                        P1_Format : access Standard.Java.Text.Format.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NumberFormatter);
   pragma Import (Java, SetFormat, "setFormat");

end Javax.Swing.Text.NumberFormatter;
pragma Import (Java, Javax.Swing.Text.NumberFormatter, "javax.swing.text.NumberFormatter");
pragma Extensions_Allowed (Off);
