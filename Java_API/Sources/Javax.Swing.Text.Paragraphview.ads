pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.TabSet;
limited with Javax.Swing.Text.View;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.FlowView;
with Javax.Swing.Text.TabExpander;

package Javax.Swing.Text.ParagraphView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TabExpander_I : Javax.Swing.Text.TabExpander.Ref)
    is new Javax.Swing.Text.FlowView.Typ(SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      FirstLineIndent : Java.Int;
      pragma Import (Java, FirstLineIndent, "firstLineIndent");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ParagraphView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure SetJustification (This : access Typ;
                               P1_Int : Java.Int);

   --  protected
   procedure SetLineSpacing (This : access Typ;
                             P1_Float : Java.Float);

   --  protected
   procedure SetFirstLineIndent (This : access Typ;
                                 P1_Float : Java.Float);

   --  protected
   procedure SetPropertiesFromAttributes (This : access Typ);

   --  protected
   function GetLayoutViewCount (This : access Typ)
                                return Java.Int;

   --  protected
   function GetLayoutView (This : access Typ;
                           P1_Int : Java.Int)
                           return access Javax.Swing.Text.View.Typ'Class;

   --  protected
   function GetNextNorthSouthVisualPositionFrom (This : access Typ;
                                                 P1_Int : Java.Int;
                                                 P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                                 P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                                 P4_Int : Java.Int;
                                                 P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                                 return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   function GetClosestPositionTo (This : access Typ;
                                  P1_Int : Java.Int;
                                  P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                  P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                  P4_Int : Java.Int;
                                  P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj;
                                  P6_Int : Java.Int;
                                  P7_Int : Java.Int)
                                  return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   function FlipEastAndWestAtEnds (This : access Typ;
                                   P1_Int : Java.Int;
                                   P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                                   return Java.Boolean;

   function GetFlowSpan (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Int;

   function GetFlowStart (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Int;

   --  protected
   function CreateRow (This : access Typ)
                       return access Javax.Swing.Text.View.Typ'Class;

   function NextTabStop (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Int : Java.Int)
                         return Java.Float;

   --  protected
   function GetTabSet (This : access Typ)
                       return access Javax.Swing.Text.TabSet.Typ'Class;

   --  protected
   function GetPartialSize (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Float;

   --  protected
   function FindOffsetToCharactersInString (This : access Typ;
                                            P1_Char_Arr : Java.Char_Arr;
                                            P2_Int : Java.Int)
                                            return Java.Int;

   --  protected
   function GetTabBase (This : access Typ)
                        return Java.Float;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   function GetAlignment (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   function BreakView (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Float : Java.Float;
                       P3_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                       return access Javax.Swing.Text.View.Typ'Class;

   function GetBreakWeight (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Float : Java.Float)
                            return Java.Int;

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ParagraphView);
   pragma Import (Java, SetJustification, "setJustification");
   pragma Import (Java, SetLineSpacing, "setLineSpacing");
   pragma Import (Java, SetFirstLineIndent, "setFirstLineIndent");
   pragma Import (Java, SetPropertiesFromAttributes, "setPropertiesFromAttributes");
   pragma Import (Java, GetLayoutViewCount, "getLayoutViewCount");
   pragma Import (Java, GetLayoutView, "getLayoutView");
   pragma Import (Java, GetNextNorthSouthVisualPositionFrom, "getNextNorthSouthVisualPositionFrom");
   pragma Import (Java, GetClosestPositionTo, "getClosestPositionTo");
   pragma Import (Java, FlipEastAndWestAtEnds, "flipEastAndWestAtEnds");
   pragma Import (Java, GetFlowSpan, "getFlowSpan");
   pragma Import (Java, GetFlowStart, "getFlowStart");
   pragma Import (Java, CreateRow, "createRow");
   pragma Import (Java, NextTabStop, "nextTabStop");
   pragma Import (Java, GetTabSet, "getTabSet");
   pragma Import (Java, GetPartialSize, "getPartialSize");
   pragma Import (Java, FindOffsetToCharactersInString, "findOffsetToCharactersInString");
   pragma Import (Java, GetTabBase, "getTabBase");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, BreakView, "breakView");
   pragma Import (Java, GetBreakWeight, "getBreakWeight");
   pragma Import (Java, ChangedUpdate, "changedUpdate");

end Javax.Swing.Text.ParagraphView;
pragma Import (Java, Javax.Swing.Text.ParagraphView, "javax.swing.text.ParagraphView");
pragma Extensions_Allowed (Off);
