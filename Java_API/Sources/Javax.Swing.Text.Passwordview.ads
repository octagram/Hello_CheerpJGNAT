pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position.Bias;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.FieldView;
with Javax.Swing.Text.TabExpander;

package Javax.Swing.Text.PasswordView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TabExpander_I : Javax.Swing.Text.TabExpander.Ref)
    is new Javax.Swing.Text.FieldView.Typ(SwingConstants_I,
                                          TabExpander_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PasswordView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function DrawUnselectedText (This : access Typ;
                                P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int)
                                return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   function DrawSelectedText (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int)
                              return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   function DrawEchoCharacter (This : access Typ;
                               P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Char : Java.Char)
                               return Java.Int;

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int;

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PasswordView);
   pragma Import (Java, DrawUnselectedText, "drawUnselectedText");
   pragma Import (Java, DrawSelectedText, "drawSelectedText");
   pragma Import (Java, DrawEchoCharacter, "drawEchoCharacter");
   pragma Import (Java, ModelToView, "modelToView");
   pragma Import (Java, ViewToModel, "viewToModel");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");

end Javax.Swing.Text.PasswordView;
pragma Import (Java, Javax.Swing.Text.PasswordView, "javax.swing.text.PasswordView");
pragma Extensions_Allowed (Off);
