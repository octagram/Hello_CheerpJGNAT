pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.AbstractDocument.AbstractElement;
limited with Javax.Swing.Text.AbstractDocument.Content;
limited with Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractDocument;
with Javax.Swing.Text.Document;

package Javax.Swing.Text.PlainDocument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Document_I : Javax.Swing.Text.Document.Ref)
    is new Javax.Swing.Text.AbstractDocument.Typ(Serializable_I,
                                                 Document_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PlainDocument (This : Ref := null)
                               return Ref;

   function New_PlainDocument (P1_Content : access Standard.Javax.Swing.Text.AbstractDocument.Content.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure InsertString (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function GetDefaultRootElement (This : access Typ)
                                   return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   function CreateDefaultRoot (This : access Typ)
                               return access Javax.Swing.Text.AbstractDocument.AbstractElement.Typ'Class;

   function GetParagraphElement (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Javax.Swing.Text.Element.Typ'Class;

   --  protected
   procedure InsertUpdate (This : access Typ;
                           P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class;
                           P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   --  protected
   procedure RemoveUpdate (This : access Typ;
                           P1_DefaultDocumentEvent : access Standard.Javax.Swing.Text.AbstractDocument.DefaultDocumentEvent.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TabSizeAttribute : constant access Java.Lang.String.Typ'Class;

   --  final
   LineLimitAttribute : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PlainDocument);
   pragma Import (Java, InsertString, "insertString");
   pragma Import (Java, GetDefaultRootElement, "getDefaultRootElement");
   pragma Import (Java, CreateDefaultRoot, "createDefaultRoot");
   pragma Import (Java, GetParagraphElement, "getParagraphElement");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, TabSizeAttribute, "tabSizeAttribute");
   pragma Import (Java, LineLimitAttribute, "lineLimitAttribute");

end Javax.Swing.Text.PlainDocument;
pragma Import (Java, Javax.Swing.Text.PlainDocument, "javax.swing.text.PlainDocument");
pragma Extensions_Allowed (Off);
