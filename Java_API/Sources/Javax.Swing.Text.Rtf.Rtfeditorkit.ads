pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.String;
limited with Javax.Swing.Text.Document;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.StyledEditorKit;

package Javax.Swing.Text.Rtf.RTFEditorKit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Swing.Text.StyledEditorKit.Typ(Serializable_I,
                                                Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_RTFEditorKit (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure Read (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                   P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Write (This : access Typ;
                    P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                    P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Read (This : access Typ;
                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                   P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                   P3_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except

   procedure Write (This : access Typ;
                    P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                    P2_Document : access Standard.Javax.Swing.Text.Document.Typ'Class;
                    P3_Int : Java.Int;
                    P4_Int : Java.Int);
   --  can raise Java.Io.IOException.Except and
   --  Javax.Swing.Text.BadLocationException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_RTFEditorKit);
   pragma Import (Java, GetContentType, "getContentType");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Write, "write");

end Javax.Swing.Text.Rtf.RTFEditorKit;
pragma Import (Java, Javax.Swing.Text.Rtf.RTFEditorKit, "javax.swing.text.rtf.RTFEditorKit");
pragma Extensions_Allowed (Off);
