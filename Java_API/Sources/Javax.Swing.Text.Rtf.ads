pragma Extensions_Allowed (On);
package Javax.Swing.Text.Rtf is
   pragma Preelaborate;
end Javax.Swing.Text.Rtf;
pragma Import (Java, Javax.Swing.Text.Rtf, "javax.swing.text.rtf");
pragma Extensions_Allowed (Off);
