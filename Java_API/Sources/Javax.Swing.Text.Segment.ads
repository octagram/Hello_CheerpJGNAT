pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.CharSequence;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Text.CharacterIterator;

package Javax.Swing.Text.Segment is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CharSequence_I : Java.Lang.CharSequence.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            CharacterIterator_I : Java.Text.CharacterIterator.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      array_K : Java.Char_Arr;
      pragma Import (Java, array_K, "array");

      Offset : Java.Int;
      pragma Import (Java, Offset, "offset");

      Count : Java.Int;
      pragma Import (Java, Count, "count");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Segment (This : Ref := null)
                         return Ref;

   function New_Segment (P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetPartialReturn (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function IsPartialReturn (This : access Typ)
                             return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function First (This : access Typ)
                   return Java.Char;

   function Last (This : access Typ)
                  return Java.Char;

   function Current (This : access Typ)
                     return Java.Char;

   function Next (This : access Typ)
                  return Java.Char;

   function Previous (This : access Typ)
                      return Java.Char;

   function SetIndex (This : access Typ;
                      P1_Int : Java.Int)
                      return Java.Char;

   function GetBeginIndex (This : access Typ)
                           return Java.Int;

   function GetEndIndex (This : access Typ)
                         return Java.Int;

   function GetIndex (This : access Typ)
                      return Java.Int;

   function CharAt (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Char;

   function Length (This : access Typ)
                    return Java.Int;

   function SubSequence (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return access Java.Lang.CharSequence.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Segment);
   pragma Import (Java, SetPartialReturn, "setPartialReturn");
   pragma Import (Java, IsPartialReturn, "isPartialReturn");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, First, "first");
   pragma Import (Java, Last, "last");
   pragma Import (Java, Current, "current");
   pragma Import (Java, Next, "next");
   pragma Import (Java, Previous, "previous");
   pragma Import (Java, SetIndex, "setIndex");
   pragma Import (Java, GetBeginIndex, "getBeginIndex");
   pragma Import (Java, GetEndIndex, "getEndIndex");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, CharAt, "charAt");
   pragma Import (Java, Length, "length");
   pragma Import (Java, SubSequence, "subSequence");
   pragma Import (Java, Clone, "clone");

end Javax.Swing.Text.Segment;
pragma Import (Java, Javax.Swing.Text.Segment, "javax.swing.text.Segment");
pragma Extensions_Allowed (Off);
