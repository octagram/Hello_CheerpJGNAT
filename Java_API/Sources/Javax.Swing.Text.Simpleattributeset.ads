pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Text.AttributeSet;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.MutableAttributeSet;

package Javax.Swing.Text.SimpleAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            MutableAttributeSet_I : Javax.Swing.Text.MutableAttributeSet.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleAttributeSet (This : Ref := null)
                                    return Ref;

   function New_SimpleAttributeSet (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function GetAttributeCount (This : access Typ)
                               return Java.Int;

   function IsDefined (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   function IsEqual (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return Java.Boolean;

   function CopyAttributes (This : access Typ)
                            return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetAttributeNames (This : access Typ)
                               return access Java.Util.Enumeration.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function ContainsAttribute (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean;

   function ContainsAttributes (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                return Java.Boolean;

   procedure AddAttribute (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddAttributes (This : access Typ;
                            P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   procedure RemoveAttribute (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveAttributes (This : access Typ;
                               P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class);

   procedure RemoveAttributes (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   function GetResolveParent (This : access Typ)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure SetResolveParent (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   EMPTY : access Javax.Swing.Text.AttributeSet.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleAttributeSet);
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, GetAttributeCount, "getAttributeCount");
   pragma Import (Java, IsDefined, "isDefined");
   pragma Import (Java, IsEqual, "isEqual");
   pragma Import (Java, CopyAttributes, "copyAttributes");
   pragma Import (Java, GetAttributeNames, "getAttributeNames");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, ContainsAttribute, "containsAttribute");
   pragma Import (Java, ContainsAttributes, "containsAttributes");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, AddAttributes, "addAttributes");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, RemoveAttributes, "removeAttributes");
   pragma Import (Java, GetResolveParent, "getResolveParent");
   pragma Import (Java, SetResolveParent, "setResolveParent");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, EMPTY, "EMPTY");

end Javax.Swing.Text.SimpleAttributeSet;
pragma Import (Java, Javax.Swing.Text.SimpleAttributeSet, "javax.swing.text.SimpleAttributeSet");
pragma Extensions_Allowed (Off);
