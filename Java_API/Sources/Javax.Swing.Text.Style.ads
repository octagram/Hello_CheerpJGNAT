pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Event.ChangeListener;
with Java.Lang.Object;
with Javax.Swing.Text.MutableAttributeSet;

package Javax.Swing.Text.Style is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MutableAttributeSet_I : Javax.Swing.Text.MutableAttributeSet.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, AddChangeListener, "addChangeListener");
   pragma Export (Java, RemoveChangeListener, "removeChangeListener");

end Javax.Swing.Text.Style;
pragma Import (Java, Javax.Swing.Text.Style, "javax.swing.text.Style");
pragma Extensions_Allowed (Off);
