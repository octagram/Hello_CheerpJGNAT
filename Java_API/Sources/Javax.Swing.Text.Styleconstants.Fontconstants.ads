pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.Text.AttributeSet.CharacterAttribute;
with Javax.Swing.Text.AttributeSet.FontAttribute;
with Javax.Swing.Text.StyleConstants;

package Javax.Swing.Text.StyleConstants.FontConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(CharacterAttribute_I : Javax.Swing.Text.AttributeSet.CharacterAttribute.Ref;
            FontAttribute_I : Javax.Swing.Text.AttributeSet.FontAttribute.Ref)
    is new Javax.Swing.Text.StyleConstants.Typ
      with null record;
private
   pragma Convention (Java, Typ);

end Javax.Swing.Text.StyleConstants.FontConstants;
pragma Import (Java, Javax.Swing.Text.StyleConstants.FontConstants, "javax.swing.text.StyleConstants$FontConstants");
pragma Extensions_Allowed (Off);
