pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.Text.AttributeSet.ParagraphAttribute;
with Javax.Swing.Text.StyleConstants;

package Javax.Swing.Text.StyleConstants.ParagraphConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ParagraphAttribute_I : Javax.Swing.Text.AttributeSet.ParagraphAttribute.Ref)
    is new Javax.Swing.Text.StyleConstants.Typ
      with null record;
private
   pragma Convention (Java, Typ);

end Javax.Swing.Text.StyleConstants.ParagraphConstants;
pragma Import (Java, Javax.Swing.Text.StyleConstants.ParagraphConstants, "javax.swing.text.StyleConstants$ParagraphConstants");
pragma Extensions_Allowed (Off);
