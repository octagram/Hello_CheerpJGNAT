pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.MutableAttributeSet;
limited with Javax.Swing.Text.TabSet;
with Java.Lang.Object;

package Javax.Swing.Text.StyleConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetBidiLevel (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                          return Java.Int;

   procedure SetBidiLevel (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                           P2_Int : Java.Int);

   function GetComponent (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                          return access Java.Awt.Component.Typ'Class;

   procedure SetComponent (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                           P2_Component : access Standard.Java.Awt.Component.Typ'Class);

   function GetIcon (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   procedure SetIcon (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                      P2_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetFontFamily (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Lang.String.Typ'Class;

   procedure SetFontFamily (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class);

   function GetFontSize (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                         return Java.Int;

   procedure SetFontSize (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                          P2_Int : Java.Int);

   function IsBold (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                    return Java.Boolean;

   procedure SetBold (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                      P2_Boolean : Java.Boolean);

   function IsItalic (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                      return Java.Boolean;

   procedure SetItalic (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                        P2_Boolean : Java.Boolean);

   function IsUnderline (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                         return Java.Boolean;

   function IsStrikeThrough (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                             return Java.Boolean;

   function IsSuperscript (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return Java.Boolean;

   function IsSubscript (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                         return Java.Boolean;

   procedure SetUnderline (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                           P2_Boolean : Java.Boolean);

   procedure SetStrikeThrough (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                               P2_Boolean : Java.Boolean);

   procedure SetSuperscript (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                             P2_Boolean : Java.Boolean);

   procedure SetSubscript (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                           P2_Boolean : Java.Boolean);

   function GetForeground (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetForeground (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                            P2_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBackground (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   procedure SetBackground (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                            P2_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetFirstLineIndent (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                return Java.Float;

   procedure SetFirstLineIndent (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                                 P2_Float : Java.Float);

   function GetRightIndent (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                            return Java.Float;

   procedure SetRightIndent (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                             P2_Float : Java.Float);

   function GetLeftIndent (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return Java.Float;

   procedure SetLeftIndent (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                            P2_Float : Java.Float);

   function GetLineSpacing (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                            return Java.Float;

   procedure SetLineSpacing (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                             P2_Float : Java.Float);

   function GetSpaceAbove (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return Java.Float;

   procedure SetSpaceAbove (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                            P2_Float : Java.Float);

   function GetSpaceBelow (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return Java.Float;

   procedure SetSpaceBelow (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                            P2_Float : Java.Float);

   function GetAlignment (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                          return Java.Int;

   procedure SetAlignment (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                           P2_Int : Java.Int);

   function GetTabSet (P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                       return access Javax.Swing.Text.TabSet.Typ'Class;

   procedure SetTabSet (P1_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class;
                        P2_TabSet : access Standard.Javax.Swing.Text.TabSet.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ComponentElementName : constant access Java.Lang.String.Typ'Class;

   --  final
   IconElementName : constant access Java.Lang.String.Typ'Class;

   --  final
   NameAttribute : access Java.Lang.Object.Typ'Class;

   --  final
   ResolveAttribute : access Java.Lang.Object.Typ'Class;

   --  final
   ModelAttribute : access Java.Lang.Object.Typ'Class;

   --  final
   BidiLevel : access Java.Lang.Object.Typ'Class;

   --  final
   FontFamily : access Java.Lang.Object.Typ'Class;

   --  final
   Family : access Java.Lang.Object.Typ'Class;

   --  final
   FontSize : access Java.Lang.Object.Typ'Class;

   --  final
   Size : access Java.Lang.Object.Typ'Class;

   --  final
   Bold : access Java.Lang.Object.Typ'Class;

   --  final
   Italic : access Java.Lang.Object.Typ'Class;

   --  final
   Underline : access Java.Lang.Object.Typ'Class;

   --  final
   StrikeThrough : access Java.Lang.Object.Typ'Class;

   --  final
   Superscript : access Java.Lang.Object.Typ'Class;

   --  final
   Subscript : access Java.Lang.Object.Typ'Class;

   --  final
   Foreground : access Java.Lang.Object.Typ'Class;

   --  final
   Background : access Java.Lang.Object.Typ'Class;

   --  final
   ComponentAttribute : access Java.Lang.Object.Typ'Class;

   --  final
   IconAttribute : access Java.Lang.Object.Typ'Class;

   --  final
   ComposedTextAttribute : access Java.Lang.Object.Typ'Class;

   --  final
   FirstLineIndent : access Java.Lang.Object.Typ'Class;

   --  final
   LeftIndent : access Java.Lang.Object.Typ'Class;

   --  final
   RightIndent : access Java.Lang.Object.Typ'Class;

   --  final
   LineSpacing : access Java.Lang.Object.Typ'Class;

   --  final
   SpaceAbove : access Java.Lang.Object.Typ'Class;

   --  final
   SpaceBelow : access Java.Lang.Object.Typ'Class;

   --  final
   Alignment : access Java.Lang.Object.Typ'Class;

   --  final
   TabSet : access Java.Lang.Object.Typ'Class;

   --  final
   Orientation : access Java.Lang.Object.Typ'Class;

   --  final
   ALIGN_LEFT : constant Java.Int;

   --  final
   ALIGN_CENTER : constant Java.Int;

   --  final
   ALIGN_RIGHT : constant Java.Int;

   --  final
   ALIGN_JUSTIFIED : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetBidiLevel, "getBidiLevel");
   pragma Import (Java, SetBidiLevel, "setBidiLevel");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, SetComponent, "setComponent");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, SetIcon, "setIcon");
   pragma Import (Java, GetFontFamily, "getFontFamily");
   pragma Import (Java, SetFontFamily, "setFontFamily");
   pragma Import (Java, GetFontSize, "getFontSize");
   pragma Import (Java, SetFontSize, "setFontSize");
   pragma Import (Java, IsBold, "isBold");
   pragma Import (Java, SetBold, "setBold");
   pragma Import (Java, IsItalic, "isItalic");
   pragma Import (Java, SetItalic, "setItalic");
   pragma Import (Java, IsUnderline, "isUnderline");
   pragma Import (Java, IsStrikeThrough, "isStrikeThrough");
   pragma Import (Java, IsSuperscript, "isSuperscript");
   pragma Import (Java, IsSubscript, "isSubscript");
   pragma Import (Java, SetUnderline, "setUnderline");
   pragma Import (Java, SetStrikeThrough, "setStrikeThrough");
   pragma Import (Java, SetSuperscript, "setSuperscript");
   pragma Import (Java, SetSubscript, "setSubscript");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, SetForeground, "setForeground");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, GetFirstLineIndent, "getFirstLineIndent");
   pragma Import (Java, SetFirstLineIndent, "setFirstLineIndent");
   pragma Import (Java, GetRightIndent, "getRightIndent");
   pragma Import (Java, SetRightIndent, "setRightIndent");
   pragma Import (Java, GetLeftIndent, "getLeftIndent");
   pragma Import (Java, SetLeftIndent, "setLeftIndent");
   pragma Import (Java, GetLineSpacing, "getLineSpacing");
   pragma Import (Java, SetLineSpacing, "setLineSpacing");
   pragma Import (Java, GetSpaceAbove, "getSpaceAbove");
   pragma Import (Java, SetSpaceAbove, "setSpaceAbove");
   pragma Import (Java, GetSpaceBelow, "getSpaceBelow");
   pragma Import (Java, SetSpaceBelow, "setSpaceBelow");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, SetAlignment, "setAlignment");
   pragma Import (Java, GetTabSet, "getTabSet");
   pragma Import (Java, SetTabSet, "setTabSet");
   pragma Import (Java, ComponentElementName, "ComponentElementName");
   pragma Import (Java, IconElementName, "IconElementName");
   pragma Import (Java, NameAttribute, "NameAttribute");
   pragma Import (Java, ResolveAttribute, "ResolveAttribute");
   pragma Import (Java, ModelAttribute, "ModelAttribute");
   pragma Import (Java, BidiLevel, "BidiLevel");
   pragma Import (Java, FontFamily, "FontFamily");
   pragma Import (Java, Family, "Family");
   pragma Import (Java, FontSize, "FontSize");
   pragma Import (Java, Size, "Size");
   pragma Import (Java, Bold, "Bold");
   pragma Import (Java, Italic, "Italic");
   pragma Import (Java, Underline, "Underline");
   pragma Import (Java, StrikeThrough, "StrikeThrough");
   pragma Import (Java, Superscript, "Superscript");
   pragma Import (Java, Subscript, "Subscript");
   pragma Import (Java, Foreground, "Foreground");
   pragma Import (Java, Background, "Background");
   pragma Import (Java, ComponentAttribute, "ComponentAttribute");
   pragma Import (Java, IconAttribute, "IconAttribute");
   pragma Import (Java, ComposedTextAttribute, "ComposedTextAttribute");
   pragma Import (Java, FirstLineIndent, "FirstLineIndent");
   pragma Import (Java, LeftIndent, "LeftIndent");
   pragma Import (Java, RightIndent, "RightIndent");
   pragma Import (Java, LineSpacing, "LineSpacing");
   pragma Import (Java, SpaceAbove, "SpaceAbove");
   pragma Import (Java, SpaceBelow, "SpaceBelow");
   pragma Import (Java, Alignment, "Alignment");
   pragma Import (Java, TabSet, "TabSet");
   pragma Import (Java, Orientation, "Orientation");
   pragma Import (Java, ALIGN_LEFT, "ALIGN_LEFT");
   pragma Import (Java, ALIGN_CENTER, "ALIGN_CENTER");
   pragma Import (Java, ALIGN_RIGHT, "ALIGN_RIGHT");
   pragma Import (Java, ALIGN_JUSTIFIED, "ALIGN_JUSTIFIED");

end Javax.Swing.Text.StyleConstants;
pragma Import (Java, Javax.Swing.Text.StyleConstants, "javax.swing.text.StyleConstants");
pragma Extensions_Allowed (Off);
