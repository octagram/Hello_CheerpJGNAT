pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.ChangeEvent;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Text.AttributeSet;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.Style;

package Javax.Swing.Text.StyleContext.NamedStyle is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Style_I : Javax.Swing.Text.Style.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      ChangeEvent : access Javax.Swing.Event.ChangeEvent.Typ'Class;
      pragma Import (Java, ChangeEvent, "changeEvent");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NamedStyle (P1_StyleContext : access Standard.Javax.Swing.Text.StyleContext.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_Style : access Standard.Javax.Swing.Text.Style.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_NamedStyle (P1_StyleContext : access Standard.Javax.Swing.Text.StyleContext.Typ'Class;
                            P2_Style : access Standard.Javax.Swing.Text.Style.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_NamedStyle (P1_StyleContext : access Standard.Javax.Swing.Text.StyleContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireStateChanged (This : access Typ);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetAttributeCount (This : access Typ)
                               return Java.Int;

   function IsDefined (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   function IsEqual (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return Java.Boolean;

   function CopyAttributes (This : access Typ)
                            return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function GetAttributeNames (This : access Typ)
                               return access Java.Util.Enumeration.Typ'Class;

   function ContainsAttribute (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean;

   function ContainsAttributes (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                return Java.Boolean;

   function GetResolveParent (This : access Typ)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure AddAttribute (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AddAttributes (This : access Typ;
                            P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   procedure RemoveAttribute (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure RemoveAttributes (This : access Typ;
                               P1_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class);

   procedure RemoveAttributes (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   procedure SetResolveParent (This : access Typ;
                               P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NamedStyle);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetName, "setName");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, FireStateChanged, "fireStateChanged");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, GetAttributeCount, "getAttributeCount");
   pragma Import (Java, IsDefined, "isDefined");
   pragma Import (Java, IsEqual, "isEqual");
   pragma Import (Java, CopyAttributes, "copyAttributes");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetAttributeNames, "getAttributeNames");
   pragma Import (Java, ContainsAttribute, "containsAttribute");
   pragma Import (Java, ContainsAttributes, "containsAttributes");
   pragma Import (Java, GetResolveParent, "getResolveParent");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, AddAttributes, "addAttributes");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, RemoveAttributes, "removeAttributes");
   pragma Import (Java, SetResolveParent, "setResolveParent");

end Javax.Swing.Text.StyleContext.NamedStyle;
pragma Import (Java, Javax.Swing.Text.StyleContext.NamedStyle, "javax.swing.text.StyleContext$NamedStyle");
pragma Extensions_Allowed (Off);
