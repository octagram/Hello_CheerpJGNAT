pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Lang.Object;
with Javax.Swing.Text.AttributeSet;

package Javax.Swing.Text.StyleContext.SmallAttributeSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AttributeSet_I : Javax.Swing.Text.AttributeSet.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SmallAttributeSet (P1_StyleContext : access Standard.Javax.Swing.Text.StyleContext.Typ'Class;
                                   P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                   This : Ref := null)
                                   return Ref;

   function New_SmallAttributeSet (P1_StyleContext : access Standard.Javax.Swing.Text.StyleContext.Typ'Class;
                                   P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   function GetAttributeCount (This : access Typ)
                               return Java.Int;

   function IsDefined (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean;

   function IsEqual (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return Java.Boolean;

   function CopyAttributes (This : access Typ)
                            return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetAttribute (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Lang.Object.Typ'Class;

   function GetAttributeNames (This : access Typ)
                               return access Java.Util.Enumeration.Typ'Class;

   function ContainsAttribute (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return Java.Boolean;

   function ContainsAttributes (This : access Typ;
                                P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                return Java.Boolean;

   function GetResolveParent (This : access Typ)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SmallAttributeSet);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, GetAttributeCount, "getAttributeCount");
   pragma Import (Java, IsDefined, "isDefined");
   pragma Import (Java, IsEqual, "isEqual");
   pragma Import (Java, CopyAttributes, "copyAttributes");
   pragma Import (Java, GetAttribute, "getAttribute");
   pragma Import (Java, GetAttributeNames, "getAttributeNames");
   pragma Import (Java, ContainsAttribute, "containsAttribute");
   pragma Import (Java, ContainsAttributes, "containsAttributes");
   pragma Import (Java, GetResolveParent, "getResolveParent");

end Javax.Swing.Text.StyleContext.SmallAttributeSet;
pragma Import (Java, Javax.Swing.Text.StyleContext.SmallAttributeSet, "javax.swing.text.StyleContext$SmallAttributeSet");
pragma Extensions_Allowed (Off);
