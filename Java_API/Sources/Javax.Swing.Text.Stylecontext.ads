pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Awt.FontMetrics;
limited with Java.Io.ObjectInputStream;
limited with Java.Io.ObjectOutputStream;
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Event.ChangeListener;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.MutableAttributeSet;
limited with Javax.Swing.Text.Style;
limited with Javax.Swing.Text.StyleContext.SmallAttributeSet;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Text.AbstractDocument.AttributeContext;

package Javax.Swing.Text.StyleContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            AttributeContext_I : Javax.Swing.Text.AbstractDocument.AttributeContext.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetDefaultStyleContext return access Javax.Swing.Text.StyleContext.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StyleContext (This : Ref := null)
                              return Ref;

   function AddStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Style : access Standard.Javax.Swing.Text.Style.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class;

   procedure RemoveStyle (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class;

   function GetStyleNames (This : access Typ)
                           return access Java.Util.Enumeration.Typ'Class;

   procedure AddChangeListener (This : access Typ;
                                P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   procedure RemoveChangeListener (This : access Typ;
                                   P1_ChangeListener : access Standard.Javax.Swing.Event.ChangeListener.Typ'Class);

   function GetChangeListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   function GetFont (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function GetForeground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   function GetBackground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class;

   function GetFont (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Int : Java.Int;
                     P3_Int : Java.Int)
                     return access Java.Awt.Font.Typ'Class;

   function GetFontMetrics (This : access Typ;
                            P1_Font : access Standard.Java.Awt.Font.Typ'Class)
                            return access Java.Awt.FontMetrics.Typ'Class;

   --  synchronized
   function AddAttribute (This : access Typ;
                          P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Javax.Swing.Text.AttributeSet.Typ'Class;

   --  synchronized
   function AddAttributes (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                           P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   --  synchronized
   function RemoveAttribute (This : access Typ;
                             P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return access Javax.Swing.Text.AttributeSet.Typ'Class;

   --  synchronized
   function RemoveAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                              P2_Enumeration : access Standard.Java.Util.Enumeration.Typ'Class)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;

   --  synchronized
   function RemoveAttributes (This : access Typ;
                              P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                              P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                              return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function GetEmptySet (This : access Typ)
                         return access Javax.Swing.Text.AttributeSet.Typ'Class;

   procedure Reclaim (This : access Typ;
                      P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);

   --  protected
   function GetCompressionThreshold (This : access Typ)
                                     return Java.Int;

   --  protected
   function CreateSmallAttributeSet (This : access Typ;
                                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                     return access Javax.Swing.Text.StyleContext.SmallAttributeSet.Typ'Class;

   --  protected
   function CreateLargeAttributeSet (This : access Typ;
                                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                                     return access Javax.Swing.Text.MutableAttributeSet.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure WriteAttributes (This : access Typ;
                              P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class;
                              P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure ReadAttributes (This : access Typ;
                             P1_ObjectInputStream : access Standard.Java.Io.ObjectInputStream.Typ'Class;
                             P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class);
   --  can raise Java.Lang.ClassNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure WriteAttributeSet (P1_ObjectOutputStream : access Standard.Java.Io.ObjectOutputStream.Typ'Class;
                                P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class);
   --  can raise Java.Io.IOException.Except

   procedure ReadAttributeSet (P1_ObjectInputStream : access Standard.Java.Io.ObjectInputStream.Typ'Class;
                               P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class);
   --  can raise Java.Lang.ClassNotFoundException.Except and
   --  Java.Io.IOException.Except

   procedure RegisterStaticAttributeKey (P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetStaticAttribute (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                return access Java.Lang.Object.Typ'Class;

   function GetStaticAttributeKey (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                                   return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_STYLE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetDefaultStyleContext, "getDefaultStyleContext");
   pragma Java_Constructor (New_StyleContext);
   pragma Import (Java, AddStyle, "addStyle");
   pragma Import (Java, RemoveStyle, "removeStyle");
   pragma Import (Java, GetStyle, "getStyle");
   pragma Import (Java, GetStyleNames, "getStyleNames");
   pragma Import (Java, AddChangeListener, "addChangeListener");
   pragma Import (Java, RemoveChangeListener, "removeChangeListener");
   pragma Import (Java, GetChangeListeners, "getChangeListeners");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, GetForeground, "getForeground");
   pragma Import (Java, GetBackground, "getBackground");
   pragma Import (Java, GetFontMetrics, "getFontMetrics");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, AddAttributes, "addAttributes");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, RemoveAttributes, "removeAttributes");
   pragma Import (Java, GetEmptySet, "getEmptySet");
   pragma Import (Java, Reclaim, "reclaim");
   pragma Import (Java, GetCompressionThreshold, "getCompressionThreshold");
   pragma Import (Java, CreateSmallAttributeSet, "createSmallAttributeSet");
   pragma Import (Java, CreateLargeAttributeSet, "createLargeAttributeSet");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, WriteAttributes, "writeAttributes");
   pragma Import (Java, ReadAttributes, "readAttributes");
   pragma Import (Java, WriteAttributeSet, "writeAttributeSet");
   pragma Import (Java, ReadAttributeSet, "readAttributeSet");
   pragma Import (Java, RegisterStaticAttributeKey, "registerStaticAttributeKey");
   pragma Import (Java, GetStaticAttribute, "getStaticAttribute");
   pragma Import (Java, GetStaticAttributeKey, "getStaticAttributeKey");
   pragma Import (Java, DEFAULT_STYLE, "DEFAULT_STYLE");

end Javax.Swing.Text.StyleContext;
pragma Import (Java, Javax.Swing.Text.StyleContext, "javax.swing.text.StyleContext");
pragma Extensions_Allowed (Off);
