pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Font;
limited with Java.Lang.String;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Style;
with Java.Lang.Object;
with Javax.Swing.Text.Document;

package Javax.Swing.Text.StyledDocument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Document_I : Javax.Swing.Text.Document.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Style : access Standard.Javax.Swing.Text.Style.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class is abstract;

   procedure RemoveStyle (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetStyle (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Javax.Swing.Text.Style.Typ'Class is abstract;

   procedure SetCharacterAttributes (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P4_Boolean : Java.Boolean) is abstract;

   procedure SetParagraphAttributes (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P4_Boolean : Java.Boolean) is abstract;

   procedure SetLogicalStyle (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Style : access Standard.Javax.Swing.Text.Style.Typ'Class) is abstract;

   function GetLogicalStyle (This : access Typ;
                             P1_Int : Java.Int)
                             return access Javax.Swing.Text.Style.Typ'Class is abstract;

   function GetParagraphElement (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Javax.Swing.Text.Element.Typ'Class is abstract;

   function GetCharacterElement (This : access Typ;
                                 P1_Int : Java.Int)
                                 return access Javax.Swing.Text.Element.Typ'Class is abstract;

   function GetForeground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class is abstract;

   function GetBackground (This : access Typ;
                           P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                           return access Java.Awt.Color.Typ'Class is abstract;

   function GetFont (This : access Typ;
                     P1_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class)
                     return access Java.Awt.Font.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddStyle, "addStyle");
   pragma Export (Java, RemoveStyle, "removeStyle");
   pragma Export (Java, GetStyle, "getStyle");
   pragma Export (Java, SetCharacterAttributes, "setCharacterAttributes");
   pragma Export (Java, SetParagraphAttributes, "setParagraphAttributes");
   pragma Export (Java, SetLogicalStyle, "setLogicalStyle");
   pragma Export (Java, GetLogicalStyle, "getLogicalStyle");
   pragma Export (Java, GetParagraphElement, "getParagraphElement");
   pragma Export (Java, GetCharacterElement, "getCharacterElement");
   pragma Export (Java, GetForeground, "getForeground");
   pragma Export (Java, GetBackground, "getBackground");
   pragma Export (Java, GetFont, "getFont");

end Javax.Swing.Text.StyledDocument;
pragma Import (Java, Javax.Swing.Text.StyledDocument, "javax.swing.text.StyledDocument");
pragma Extensions_Allowed (Off);
