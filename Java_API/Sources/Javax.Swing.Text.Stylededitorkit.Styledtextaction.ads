pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Lang.String;
limited with Javax.Swing.JEditorPane;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.StyledDocument;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Action;
with Javax.Swing.Text.TextAction;

package Javax.Swing.Text.StyledEditorKit.StyledTextAction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Action_I : Javax.Swing.Action.Ref)
    is abstract new Javax.Swing.Text.TextAction.Typ(Serializable_I,
                                                    Cloneable_I,
                                                    Action_I)
      with null record;

   function New_StyledTextAction (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final  protected
   function GetEditor (This : access Typ;
                       P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class)
                       return access Javax.Swing.JEditorPane.Typ'Class;

   --  final  protected
   function GetStyledDocument (This : access Typ;
                               P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class)
                               return access Javax.Swing.Text.StyledDocument.Typ'Class;

   --  final  protected
   function GetStyledEditorKit (This : access Typ;
                                P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class)
                                return access Javax.Swing.Text.StyledEditorKit.Typ'Class;

   --  final  protected
   procedure SetCharacterAttributes (This : access Typ;
                                     P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class;
                                     P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P3_Boolean : Java.Boolean);

   --  final  protected
   procedure SetParagraphAttributes (This : access Typ;
                                     P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class;
                                     P2_AttributeSet : access Standard.Javax.Swing.Text.AttributeSet.Typ'Class;
                                     P3_Boolean : Java.Boolean);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StyledTextAction);
   pragma Import (Java, GetEditor, "getEditor");
   pragma Import (Java, GetStyledDocument, "getStyledDocument");
   pragma Import (Java, GetStyledEditorKit, "getStyledEditorKit");
   pragma Import (Java, SetCharacterAttributes, "setCharacterAttributes");
   pragma Import (Java, SetParagraphAttributes, "setParagraphAttributes");

end Javax.Swing.Text.StyledEditorKit.StyledTextAction;
pragma Import (Java, Javax.Swing.Text.StyledEditorKit.StyledTextAction, "javax.swing.text.StyledEditorKit$StyledTextAction");
pragma Extensions_Allowed (Off);
