pragma Extensions_Allowed (On);
limited with Javax.Swing.Action;
limited with Javax.Swing.JEditorPane;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.MutableAttributeSet;
limited with Javax.Swing.Text.ViewFactory;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Text.DefaultEditorKit;

package Javax.Swing.Text.StyledEditorKit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is new Javax.Swing.Text.DefaultEditorKit.Typ(Serializable_I,
                                                 Cloneable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StyledEditorKit (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInputAttributes (This : access Typ)
                                return access Javax.Swing.Text.MutableAttributeSet.Typ'Class;

   function GetCharacterAttributeRun (This : access Typ)
                                      return access Javax.Swing.Text.Element.Typ'Class;

   function GetActions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref;

   function CreateDefaultDocument (This : access Typ)
                                   return access Javax.Swing.Text.Document.Typ'Class;

   procedure Install (This : access Typ;
                      P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class);

   procedure Deinstall (This : access Typ;
                        P1_JEditorPane : access Standard.Javax.Swing.JEditorPane.Typ'Class);

   function GetViewFactory (This : access Typ)
                            return access Javax.Swing.Text.ViewFactory.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   --  protected
   procedure CreateInputAttributes (This : access Typ;
                                    P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                    P2_MutableAttributeSet : access Standard.Javax.Swing.Text.MutableAttributeSet.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StyledEditorKit);
   pragma Import (Java, GetInputAttributes, "getInputAttributes");
   pragma Import (Java, GetCharacterAttributeRun, "getCharacterAttributeRun");
   pragma Import (Java, GetActions, "getActions");
   pragma Import (Java, CreateDefaultDocument, "createDefaultDocument");
   pragma Import (Java, Install, "install");
   pragma Import (Java, Deinstall, "deinstall");
   pragma Import (Java, GetViewFactory, "getViewFactory");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, CreateInputAttributes, "createInputAttributes");

end Javax.Swing.Text.StyledEditorKit;
pragma Import (Java, Javax.Swing.Text.StyledEditorKit, "javax.swing.text.StyledEditorKit");
pragma Extensions_Allowed (Off);
