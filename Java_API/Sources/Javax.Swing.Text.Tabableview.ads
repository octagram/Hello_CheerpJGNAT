pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.TabExpander;
with Java.Lang.Object;

package Javax.Swing.Text.TabableView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTabbedSpan (This : access Typ;
                           P1_Float : Java.Float;
                           P2_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class)
                           return Java.Float is abstract;

   function GetPartialSpan (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return Java.Float is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTabbedSpan, "getTabbedSpan");
   pragma Export (Java, GetPartialSpan, "getPartialSpan");

end Javax.Swing.Text.TabableView;
pragma Import (Java, Javax.Swing.Text.TabableView, "javax.swing.text.TabableView");
pragma Extensions_Allowed (Off);
