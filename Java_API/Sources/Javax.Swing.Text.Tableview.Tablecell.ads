pragma Extensions_Allowed (On);
limited with Javax.Swing.Text.Element;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.BoxView;

package Javax.Swing.Text.TableView.TableCell is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.BoxView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TableCell (P1_TableView : access Standard.Javax.Swing.Text.TableView.Typ'Class;
                           P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetColumnCount (This : access Typ)
                            return Java.Int;

   function GetRowCount (This : access Typ)
                         return Java.Int;

   procedure SetGridLocation (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int);

   function GetGridRow (This : access Typ)
                        return Java.Int;

   function GetGridColumn (This : access Typ)
                           return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableCell);
   pragma Import (Java, GetColumnCount, "getColumnCount");
   pragma Import (Java, GetRowCount, "getRowCount");
   pragma Import (Java, SetGridLocation, "setGridLocation");
   pragma Import (Java, GetGridRow, "getGridRow");
   pragma Import (Java, GetGridColumn, "getGridColumn");

end Javax.Swing.Text.TableView.TableCell;
pragma Import (Java, Javax.Swing.Text.TableView.TableCell, "javax.swing.text.TableView$TableCell");
pragma Extensions_Allowed (Off);
