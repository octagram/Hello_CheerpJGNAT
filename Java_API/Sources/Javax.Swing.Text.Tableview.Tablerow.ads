pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.View;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.BoxView;

package Javax.Swing.Text.TableView.TableRow is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.BoxView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TableRow (P1_TableView : access Standard.Javax.Swing.Text.TableView.Typ'Class;
                          P2_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_View_Arr : access Javax.Swing.Text.View.Arr_Obj);

   --  protected
   procedure LayoutMajorAxis (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int_Arr : Java.Int_Arr;
                              P4_Int_Arr : Java.Int_Arr);

   --  protected
   procedure LayoutMinorAxis (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int_Arr : Java.Int_Arr;
                              P4_Int_Arr : Java.Int_Arr);

   function GetResizeWeight (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   --  protected
   function GetViewAtPosition (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return access Javax.Swing.Text.View.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableRow);
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, LayoutMajorAxis, "layoutMajorAxis");
   pragma Import (Java, LayoutMinorAxis, "layoutMinorAxis");
   pragma Import (Java, GetResizeWeight, "getResizeWeight");
   pragma Import (Java, GetViewAtPosition, "getViewAtPosition");

end Javax.Swing.Text.TableView.TableRow;
pragma Import (Java, Javax.Swing.Text.TableView.TableRow, "javax.swing.text.TableView$TableRow");
pragma Extensions_Allowed (Off);
