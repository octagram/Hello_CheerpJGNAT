pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Event.DocumentEvent.ElementChange;
limited with Javax.Swing.SizeRequirements;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.TableView.TableRow;
limited with Javax.Swing.Text.View;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.BoxView;

package Javax.Swing.Text.TableView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is abstract new Javax.Swing.Text.BoxView.Typ(SwingConstants_I)
      with null record;

   function New_TableView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function CreateTableRow (This : access Typ;
                            P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class)
                            return access Javax.Swing.Text.TableView.TableRow.Typ'Class;

   --  protected
   procedure ForwardUpdate (This : access Typ;
                            P1_ElementChange : access Standard.Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
                            P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P4_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_View_Arr : access Javax.Swing.Text.View.Arr_Obj);

   --  protected
   procedure LayoutColumns (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int_Arr : Java.Int_Arr;
                            P3_Int_Arr : Java.Int_Arr;
                            P4_SizeRequirements_Arr : access Javax.Swing.SizeRequirements.Arr_Obj);

   --  protected
   procedure LayoutMinorAxis (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_Int_Arr : Java.Int_Arr;
                              P4_Int_Arr : Java.Int_Arr);

   --  protected
   function CalculateMinorAxisRequirements (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_SizeRequirements : access Standard.Javax.Swing.SizeRequirements.Typ'Class)
                                            return access Javax.Swing.SizeRequirements.Typ'Class;

   --  protected
   function GetViewAtPosition (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return access Javax.Swing.Text.View.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TableView);
   pragma Import (Java, CreateTableRow, "createTableRow");
   pragma Import (Java, ForwardUpdate, "forwardUpdate");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, LayoutColumns, "layoutColumns");
   pragma Import (Java, LayoutMinorAxis, "layoutMinorAxis");
   pragma Import (Java, CalculateMinorAxisRequirements, "calculateMinorAxisRequirements");
   pragma Import (Java, GetViewAtPosition, "getViewAtPosition");

end Javax.Swing.Text.TableView;
pragma Import (Java, Javax.Swing.Text.TableView, "javax.swing.text.TableView");
pragma Extensions_Allowed (Off);
