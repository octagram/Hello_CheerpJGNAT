pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Text.TabStop;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.TabSet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TabSet (P1_TabStop_Arr : access Javax.Swing.Text.TabStop.Arr_Obj; 
                        This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTabCount (This : access Typ)
                         return Java.Int;

   function GetTab (This : access Typ;
                    P1_Int : Java.Int)
                    return access Javax.Swing.Text.TabStop.Typ'Class;

   function GetTabAfter (This : access Typ;
                         P1_Float : Java.Float)
                         return access Javax.Swing.Text.TabStop.Typ'Class;

   function GetTabIndex (This : access Typ;
                         P1_TabStop : access Standard.Javax.Swing.Text.TabStop.Typ'Class)
                         return Java.Int;

   function GetTabIndexAfter (This : access Typ;
                              P1_Float : Java.Float)
                              return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TabSet);
   pragma Import (Java, GetTabCount, "getTabCount");
   pragma Import (Java, GetTab, "getTab");
   pragma Import (Java, GetTabAfter, "getTabAfter");
   pragma Import (Java, GetTabIndex, "getTabIndex");
   pragma Import (Java, GetTabIndexAfter, "getTabIndexAfter");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Text.TabSet;
pragma Import (Java, Javax.Swing.Text.TabSet, "javax.swing.text.TabSet");
pragma Extensions_Allowed (Off);
