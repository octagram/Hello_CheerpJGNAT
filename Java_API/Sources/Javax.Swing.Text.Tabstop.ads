pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Text.TabStop is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TabStop (P1_Float : Java.Float; 
                         This : Ref := null)
                         return Ref;

   function New_TabStop (P1_Float : Java.Float;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPosition (This : access Typ)
                         return Java.Float;

   function GetAlignment (This : access Typ)
                          return Java.Int;

   function GetLeader (This : access Typ)
                       return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ALIGN_LEFT : constant Java.Int;

   --  final
   ALIGN_RIGHT : constant Java.Int;

   --  final
   ALIGN_CENTER : constant Java.Int;

   --  final
   ALIGN_DECIMAL : constant Java.Int;

   --  final
   ALIGN_BAR : constant Java.Int;

   --  final
   LEAD_NONE : constant Java.Int;

   --  final
   LEAD_DOTS : constant Java.Int;

   --  final
   LEAD_HYPHENS : constant Java.Int;

   --  final
   LEAD_UNDERLINE : constant Java.Int;

   --  final
   LEAD_THICKLINE : constant Java.Int;

   --  final
   LEAD_EQUALS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TabStop);
   pragma Import (Java, GetPosition, "getPosition");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Import (Java, GetLeader, "getLeader");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ALIGN_LEFT, "ALIGN_LEFT");
   pragma Import (Java, ALIGN_RIGHT, "ALIGN_RIGHT");
   pragma Import (Java, ALIGN_CENTER, "ALIGN_CENTER");
   pragma Import (Java, ALIGN_DECIMAL, "ALIGN_DECIMAL");
   pragma Import (Java, ALIGN_BAR, "ALIGN_BAR");
   pragma Import (Java, LEAD_NONE, "LEAD_NONE");
   pragma Import (Java, LEAD_DOTS, "LEAD_DOTS");
   pragma Import (Java, LEAD_HYPHENS, "LEAD_HYPHENS");
   pragma Import (Java, LEAD_UNDERLINE, "LEAD_UNDERLINE");
   pragma Import (Java, LEAD_THICKLINE, "LEAD_THICKLINE");
   pragma Import (Java, LEAD_EQUALS, "LEAD_EQUALS");

end Javax.Swing.Text.TabStop;
pragma Import (Java, Javax.Swing.Text.TabStop, "javax.swing.text.TabStop");
pragma Extensions_Allowed (Off);
