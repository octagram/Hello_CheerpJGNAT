pragma Extensions_Allowed (On);
limited with Java.Awt.FontMetrics;
limited with Java.Awt.Graphics;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.JTextComponent;
limited with Javax.Swing.Text.Segment;
limited with Javax.Swing.Text.TabExpander;
with Java.Lang.Object;

package Javax.Swing.Text.Utilities is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Utilities (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function DrawTabbedText (P1_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int;
                            P4_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                            P5_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class;
                            P6_Int : Java.Int)
                            return Java.Int;

   --  final
   function GetTabbedTextWidth (P1_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class;
                                P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                                P3_Int : Java.Int;
                                P4_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class;
                                P5_Int : Java.Int)
                                return Java.Int;

   --  final
   function GetTabbedTextOffset (P1_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class;
                                 P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class;
                                 P6_Int : Java.Int)
                                 return Java.Int;

   --  final
   function GetTabbedTextOffset (P1_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class;
                                 P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                                 P3_Int : Java.Int;
                                 P4_Int : Java.Int;
                                 P5_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class;
                                 P6_Int : Java.Int;
                                 P7_Boolean : Java.Boolean)
                                 return Java.Int;

   --  final
   function GetBreakLocation (P1_Segment : access Standard.Javax.Swing.Text.Segment.Typ'Class;
                              P2_FontMetrics : access Standard.Java.Awt.FontMetrics.Typ'Class;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_TabExpander : access Standard.Javax.Swing.Text.TabExpander.Typ'Class;
                              P6_Int : Java.Int)
                              return Java.Int;

   --  final
   function GetRowStart (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetRowEnd (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                       P2_Int : Java.Int)
                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetPositionAbove (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int)
                              return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetPositionBelow (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int)
                              return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetWordStart (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                          P2_Int : Java.Int)
                          return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetWordEnd (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                        P2_Int : Java.Int)
                        return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetNextWord (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetPreviousWord (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                             P2_Int : Java.Int)
                             return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final
   function GetParagraphElement (P1_JTextComponent : access Standard.Javax.Swing.Text.JTextComponent.Typ'Class;
                                 P2_Int : Java.Int)
                                 return access Javax.Swing.Text.Element.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Utilities);
   pragma Import (Java, DrawTabbedText, "drawTabbedText");
   pragma Import (Java, GetTabbedTextWidth, "getTabbedTextWidth");
   pragma Import (Java, GetTabbedTextOffset, "getTabbedTextOffset");
   pragma Import (Java, GetBreakLocation, "getBreakLocation");
   pragma Import (Java, GetRowStart, "getRowStart");
   pragma Import (Java, GetRowEnd, "getRowEnd");
   pragma Import (Java, GetPositionAbove, "getPositionAbove");
   pragma Import (Java, GetPositionBelow, "getPositionBelow");
   pragma Import (Java, GetWordStart, "getWordStart");
   pragma Import (Java, GetWordEnd, "getWordEnd");
   pragma Import (Java, GetNextWord, "getNextWord");
   pragma Import (Java, GetPreviousWord, "getPreviousWord");
   pragma Import (Java, GetParagraphElement, "getParagraphElement");

end Javax.Swing.Text.Utilities;
pragma Import (Java, Javax.Swing.Text.Utilities, "javax.swing.text.Utilities");
pragma Extensions_Allowed (Off);
