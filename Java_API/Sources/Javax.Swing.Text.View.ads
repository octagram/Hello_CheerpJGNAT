pragma Extensions_Allowed (On);
limited with Java.Awt.Container;
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Java.Lang.String;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Event.DocumentEvent.ElementChange;
limited with Javax.Swing.Text.AttributeSet;
limited with Javax.Swing.Text.Document;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Position.Bias;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;

package Javax.Swing.Text.View is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_View (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                      This : Ref := null)
                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParent (This : access Typ)
                       return access Javax.Swing.Text.View.Typ'Class;

   function IsVisible (This : access Typ)
                       return Java.Boolean;

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float is abstract;

   function GetMinimumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetMaximumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   procedure PreferenceChanged (This : access Typ;
                                P1_View : access Standard.Javax.Swing.Text.View.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean);

   function GetAlignment (This : access Typ;
                          P1_Int : Java.Int)
                          return Java.Float;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class) is abstract;

   procedure SetParent (This : access Typ;
                        P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   function GetViewCount (This : access Typ)
                          return Java.Int;

   function GetView (This : access Typ;
                     P1_Int : Java.Int)
                     return access Javax.Swing.Text.View.Typ'Class;

   procedure RemoveAll (This : access Typ);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure Insert (This : access Typ;
                     P1_Int : Java.Int;
                     P2_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   procedure Append (This : access Typ;
                     P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   procedure Replace (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_View_Arr : access Javax.Swing.Text.View.Arr_Obj);

   function GetViewIndex (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                          return Java.Int;

   function GetChildAllocation (This : access Typ;
                                P1_Int : Java.Int;
                                P2_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                                return access Java.Awt.Shape.Typ'Class;

   function GetNextVisualPositionFrom (This : access Typ;
                                       P1_Int : Java.Int;
                                       P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                                       P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                       P4_Int : Java.Int;
                                       P5_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                                       return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P3_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class is abstract
                         with Export => "modelToView", Convention => Java;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ModelToView (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                         P3_Int : Java.Int;
                         P4_Bias : access Standard.Javax.Swing.Text.Position.Bias.Typ'Class;
                         P5_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                         return access Java.Awt.Shape.Typ'Class
                         with Import => "modelToView", Convention => Java;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   function ViewToModel (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Float : Java.Float;
                         P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                         P4_Bias_Arr : access Javax.Swing.Text.Position.Bias.Arr_Obj)
                         return Java.Int is abstract;

   procedure InsertUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   function GetDocument (This : access Typ)
                         return access Javax.Swing.Text.Document.Typ'Class;

   function GetStartOffset (This : access Typ)
                            return Java.Int;

   function GetEndOffset (This : access Typ)
                          return Java.Int;

   function GetElement (This : access Typ)
                        return access Javax.Swing.Text.Element.Typ'Class;

   function GetGraphics (This : access Typ)
                         return access Java.Awt.Graphics.Typ'Class;

   function GetAttributes (This : access Typ)
                           return access Javax.Swing.Text.AttributeSet.Typ'Class;

   function BreakView (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Float : Java.Float;
                       P4_Float : Java.Float)
                       return access Javax.Swing.Text.View.Typ'Class;

   function CreateFragment (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Int : Java.Int)
                            return access Javax.Swing.Text.View.Typ'Class;

   function GetBreakWeight (This : access Typ;
                            P1_Int : Java.Int;
                            P2_Float : Java.Float;
                            P3_Float : Java.Float)
                            return Java.Int;

   function GetResizeWeight (This : access Typ;
                             P1_Int : Java.Int)
                             return Java.Int;

   procedure SetSize (This : access Typ;
                      P1_Float : Java.Float;
                      P2_Float : Java.Float);

   function GetContainer (This : access Typ)
                          return access Java.Awt.Container.Typ'Class;

   function GetViewFactory (This : access Typ)
                            return access Javax.Swing.Text.ViewFactory.Typ'Class;

   function GetToolTipText (This : access Typ;
                            P1_Float : Java.Float;
                            P2_Float : Java.Float;
                            P3_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                            return access Java.Lang.String.Typ'Class;

   function GetViewIndex (This : access Typ;
                          P1_Float : Java.Float;
                          P2_Float : Java.Float;
                          P3_Shape : access Standard.Java.Awt.Shape.Typ'Class)
                          return Java.Int;

   --  protected
   function UpdateChildren (This : access Typ;
                            P1_ElementChange : access Standard.Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
                            P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class)
                            return Java.Boolean;

   --  protected
   procedure ForwardUpdate (This : access Typ;
                            P1_ElementChange : access Standard.Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
                            P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P4_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   --  protected
   procedure ForwardUpdateToView (This : access Typ;
                                  P1_View : access Standard.Javax.Swing.Text.View.Typ'Class;
                                  P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                                  P3_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                                  P4_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   --  protected
   procedure UpdateLayout (This : access Typ;
                           P1_ElementChange : access Standard.Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
                           P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P3_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BadBreakWeight : constant Java.Int;

   --  final
   GoodBreakWeight : constant Java.Int;

   --  final
   ExcellentBreakWeight : constant Java.Int;

   --  final
   ForcedBreakWeight : constant Java.Int;

   --  final
   X_AXIS : constant Java.Int;

   --  final
   Y_AXIS : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_View);
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, IsVisible, "isVisible");
   pragma Export (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetMinimumSpan, "getMinimumSpan");
   pragma Import (Java, GetMaximumSpan, "getMaximumSpan");
   pragma Import (Java, PreferenceChanged, "preferenceChanged");
   pragma Import (Java, GetAlignment, "getAlignment");
   pragma Export (Java, Paint, "paint");
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetViewCount, "getViewCount");
   pragma Import (Java, GetView, "getView");
   pragma Import (Java, RemoveAll, "removeAll");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Append, "append");
   pragma Import (Java, Replace, "replace");
   pragma Import (Java, GetViewIndex, "getViewIndex");
   pragma Import (Java, GetChildAllocation, "getChildAllocation");
   pragma Import (Java, GetNextVisualPositionFrom, "getNextVisualPositionFrom");
   -- pragma Import (Java, ModelToView, "modelToView");
   pragma Export (Java, ViewToModel, "viewToModel");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, ChangedUpdate, "changedUpdate");
   pragma Import (Java, GetDocument, "getDocument");
   pragma Import (Java, GetStartOffset, "getStartOffset");
   pragma Import (Java, GetEndOffset, "getEndOffset");
   pragma Import (Java, GetElement, "getElement");
   pragma Import (Java, GetGraphics, "getGraphics");
   pragma Import (Java, GetAttributes, "getAttributes");
   pragma Import (Java, BreakView, "breakView");
   pragma Import (Java, CreateFragment, "createFragment");
   pragma Import (Java, GetBreakWeight, "getBreakWeight");
   pragma Import (Java, GetResizeWeight, "getResizeWeight");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetContainer, "getContainer");
   pragma Import (Java, GetViewFactory, "getViewFactory");
   pragma Import (Java, GetToolTipText, "getToolTipText");
   pragma Import (Java, UpdateChildren, "updateChildren");
   pragma Import (Java, ForwardUpdate, "forwardUpdate");
   pragma Import (Java, ForwardUpdateToView, "forwardUpdateToView");
   pragma Import (Java, UpdateLayout, "updateLayout");
   pragma Import (Java, BadBreakWeight, "BadBreakWeight");
   pragma Import (Java, GoodBreakWeight, "GoodBreakWeight");
   pragma Import (Java, ExcellentBreakWeight, "ExcellentBreakWeight");
   pragma Import (Java, ForcedBreakWeight, "ForcedBreakWeight");
   pragma Import (Java, X_AXIS, "X_AXIS");
   pragma Import (Java, Y_AXIS, "Y_AXIS");

end Javax.Swing.Text.View;
pragma Import (Java, Javax.Swing.Text.View, "javax.swing.text.View");
pragma Extensions_Allowed (Off);
