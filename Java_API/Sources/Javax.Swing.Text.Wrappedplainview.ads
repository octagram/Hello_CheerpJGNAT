pragma Extensions_Allowed (On);
limited with Java.Awt.Graphics;
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.Segment;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.BoxView;
with Javax.Swing.Text.TabExpander;

package Javax.Swing.Text.WrappedPlainView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TabExpander_I : Javax.Swing.Text.TabExpander.Ref)
    is new Javax.Swing.Text.BoxView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_WrappedPlainView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_WrappedPlainView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                                  P2_Boolean : Java.Boolean; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetTabSize (This : access Typ)
                        return Java.Int;

   --  protected
   procedure DrawLine (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Int : Java.Int;
                       P3_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                       P4_Int : Java.Int;
                       P5_Int : Java.Int);

   --  protected
   function DrawUnselectedText (This : access Typ;
                                P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int)
                                return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  protected
   function DrawSelectedText (This : access Typ;
                              P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int;
                              P4_Int : Java.Int;
                              P5_Int : Java.Int)
                              return Java.Int;
   --  can raise Javax.Swing.Text.BadLocationException.Except

   --  final  protected
   function GetLineBuffer (This : access Typ)
                           return access Javax.Swing.Text.Segment.Typ'Class;

   --  protected
   function CalculateBreakPosition (This : access Typ;
                                    P1_Int : Java.Int;
                                    P2_Int : Java.Int)
                                    return Java.Int;

   --  protected
   procedure LoadChildren (This : access Typ;
                           P1_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   function NextTabStop (This : access Typ;
                         P1_Float : Java.Float;
                         P2_Int : Java.Int)
                         return Java.Float;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class;
                    P2_Shape : access Standard.Java.Awt.Shape.Typ'Class);

   procedure SetSize (This : access Typ;
                      P1_Float : Java.Float;
                      P2_Float : Java.Float);

   function GetPreferredSpan (This : access Typ;
                              P1_Int : Java.Int)
                              return Java.Float;

   function GetMinimumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   function GetMaximumSpan (This : access Typ;
                            P1_Int : Java.Int)
                            return Java.Float;

   procedure InsertUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure ChangedUpdate (This : access Typ;
                            P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_WrappedPlainView);
   pragma Import (Java, GetTabSize, "getTabSize");
   pragma Import (Java, DrawLine, "drawLine");
   pragma Import (Java, DrawUnselectedText, "drawUnselectedText");
   pragma Import (Java, DrawSelectedText, "drawSelectedText");
   pragma Import (Java, GetLineBuffer, "getLineBuffer");
   pragma Import (Java, CalculateBreakPosition, "calculateBreakPosition");
   pragma Import (Java, LoadChildren, "loadChildren");
   pragma Import (Java, NextTabStop, "nextTabStop");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, SetSize, "setSize");
   pragma Import (Java, GetPreferredSpan, "getPreferredSpan");
   pragma Import (Java, GetMinimumSpan, "getMinimumSpan");
   pragma Import (Java, GetMaximumSpan, "getMaximumSpan");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");
   pragma Import (Java, ChangedUpdate, "changedUpdate");

end Javax.Swing.Text.WrappedPlainView;
pragma Import (Java, Javax.Swing.Text.WrappedPlainView, "javax.swing.text.WrappedPlainView");
pragma Extensions_Allowed (Off);
