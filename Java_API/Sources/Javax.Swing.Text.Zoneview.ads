pragma Extensions_Allowed (On);
limited with Java.Awt.Shape;
limited with Javax.Swing.Event.DocumentEvent;
limited with Javax.Swing.Event.DocumentEvent.ElementChange;
limited with Javax.Swing.Text.Element;
limited with Javax.Swing.Text.View;
limited with Javax.Swing.Text.ViewFactory;
with Java.Lang.Object;
with Javax.Swing.SwingConstants;
with Javax.Swing.Text.BoxView;

package Javax.Swing.Text.ZoneView is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.Text.BoxView.Typ(SwingConstants_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ZoneView (P1_Element : access Standard.Javax.Swing.Text.Element.Typ'Class;
                          P2_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMaximumZoneSize (This : access Typ)
                                return Java.Int;

   procedure SetMaximumZoneSize (This : access Typ;
                                 P1_Int : Java.Int);

   function GetMaxZonesLoaded (This : access Typ)
                               return Java.Int;

   procedure SetMaxZonesLoaded (This : access Typ;
                                P1_Int : Java.Int);

   --  protected
   procedure ZoneWasLoaded (This : access Typ;
                            P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   --  protected
   procedure UnloadZone (This : access Typ;
                         P1_View : access Standard.Javax.Swing.Text.View.Typ'Class);

   --  protected
   function IsZoneLoaded (This : access Typ;
                          P1_View : access Standard.Javax.Swing.Text.View.Typ'Class)
                          return Java.Boolean;

   --  protected
   function CreateZone (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int)
                        return access Javax.Swing.Text.View.Typ'Class;

   --  protected
   procedure LoadChildren (This : access Typ;
                           P1_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   --  protected
   function GetViewIndexAtPosition (This : access Typ;
                                    P1_Int : Java.Int)
                                    return Java.Int;

   --  protected
   function UpdateChildren (This : access Typ;
                            P1_ElementChange : access Standard.Javax.Swing.Event.DocumentEvent.ElementChange.Typ'Class;
                            P2_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                            P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class)
                            return Java.Boolean;

   procedure InsertUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);

   procedure RemoveUpdate (This : access Typ;
                           P1_DocumentEvent : access Standard.Javax.Swing.Event.DocumentEvent.Typ'Class;
                           P2_Shape : access Standard.Java.Awt.Shape.Typ'Class;
                           P3_ViewFactory : access Standard.Javax.Swing.Text.ViewFactory.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ZoneView);
   pragma Import (Java, GetMaximumZoneSize, "getMaximumZoneSize");
   pragma Import (Java, SetMaximumZoneSize, "setMaximumZoneSize");
   pragma Import (Java, GetMaxZonesLoaded, "getMaxZonesLoaded");
   pragma Import (Java, SetMaxZonesLoaded, "setMaxZonesLoaded");
   pragma Import (Java, ZoneWasLoaded, "zoneWasLoaded");
   pragma Import (Java, UnloadZone, "unloadZone");
   pragma Import (Java, IsZoneLoaded, "isZoneLoaded");
   pragma Import (Java, CreateZone, "createZone");
   pragma Import (Java, LoadChildren, "loadChildren");
   pragma Import (Java, GetViewIndexAtPosition, "getViewIndexAtPosition");
   pragma Import (Java, UpdateChildren, "updateChildren");
   pragma Import (Java, InsertUpdate, "insertUpdate");
   pragma Import (Java, RemoveUpdate, "removeUpdate");

end Javax.Swing.Text.ZoneView;
pragma Import (Java, Javax.Swing.Text.ZoneView, "javax.swing.text.ZoneView");
pragma Extensions_Allowed (Off);
