pragma Extensions_Allowed (On);
package Javax.Swing.Text is
   pragma Preelaborate;
end Javax.Swing.Text;
pragma Import (Java, Javax.Swing.Text, "javax.swing.text");
pragma Extensions_Allowed (Off);
