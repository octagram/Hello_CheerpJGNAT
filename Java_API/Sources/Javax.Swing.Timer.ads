pragma Extensions_Allowed (On);
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Event.ActionListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.EventListenerList;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Timer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Timer (P1_Int : Java.Int;
                       P2_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddActionListener (This : access Typ;
                                P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   procedure RemoveActionListener (This : access Typ;
                                   P1_ActionListener : access Standard.Java.Awt.Event.ActionListener.Typ'Class);

   function GetActionListeners (This : access Typ)
                                return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireActionPerformed (This : access Typ;
                                  P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   procedure SetLogTimers (P1_Boolean : Java.Boolean);

   function GetLogTimers return Java.Boolean;

   procedure SetDelay (This : access Typ;
                       P1_Int : Java.Int);

   function GetDelay (This : access Typ)
                      return Java.Int;

   procedure SetInitialDelay (This : access Typ;
                              P1_Int : Java.Int);

   function GetInitialDelay (This : access Typ)
                             return Java.Int;

   procedure SetRepeats (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsRepeats (This : access Typ)
                       return Java.Boolean;

   procedure SetCoalesce (This : access Typ;
                          P1_Boolean : Java.Boolean);

   function IsCoalesce (This : access Typ)
                        return Java.Boolean;

   procedure SetActionCommand (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetActionCommand (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   procedure Start (This : access Typ);

   function IsRunning (This : access Typ)
                       return Java.Boolean;

   procedure Stop (This : access Typ);

   procedure Restart (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Timer);
   pragma Import (Java, AddActionListener, "addActionListener");
   pragma Import (Java, RemoveActionListener, "removeActionListener");
   pragma Import (Java, GetActionListeners, "getActionListeners");
   pragma Import (Java, FireActionPerformed, "fireActionPerformed");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, SetLogTimers, "setLogTimers");
   pragma Import (Java, GetLogTimers, "getLogTimers");
   pragma Import (Java, SetDelay, "setDelay");
   pragma Import (Java, GetDelay, "getDelay");
   pragma Import (Java, SetInitialDelay, "setInitialDelay");
   pragma Import (Java, GetInitialDelay, "getInitialDelay");
   pragma Import (Java, SetRepeats, "setRepeats");
   pragma Import (Java, IsRepeats, "isRepeats");
   pragma Import (Java, SetCoalesce, "setCoalesce");
   pragma Import (Java, IsCoalesce, "isCoalesce");
   pragma Import (Java, SetActionCommand, "setActionCommand");
   pragma Import (Java, GetActionCommand, "getActionCommand");
   pragma Import (Java, Start, "start");
   pragma Import (Java, IsRunning, "isRunning");
   pragma Import (Java, Stop, "stop");
   pragma Import (Java, Restart, "restart");

end Javax.Swing.Timer;
pragma Import (Java, Javax.Swing.Timer, "javax.swing.Timer");
pragma Extensions_Allowed (Off);
