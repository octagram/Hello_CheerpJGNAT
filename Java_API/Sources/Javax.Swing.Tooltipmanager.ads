pragma Extensions_Allowed (On);
limited with Java.Awt.Event.MouseEvent;
limited with Javax.Swing.JComponent;
with Java.Awt.Event.MouseAdapter;
with Java.Awt.Event.MouseListener;
with Java.Awt.Event.MouseMotionListener;
with Java.Awt.Event.MouseWheelListener;
with Java.Lang.Object;

package Javax.Swing.ToolTipManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MouseListener_I : Java.Awt.Event.MouseListener.Ref;
            MouseMotionListener_I : Java.Awt.Event.MouseMotionListener.Ref;
            MouseWheelListener_I : Java.Awt.Event.MouseWheelListener.Ref)
    is new Java.Awt.Event.MouseAdapter.Typ(MouseListener_I,
                                           MouseMotionListener_I,
                                           MouseWheelListener_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      LightWeightPopupEnabled : Java.Boolean;
      pragma Import (Java, LightWeightPopupEnabled, "lightWeightPopupEnabled");

      --  protected
      HeavyWeightPopupEnabled : Java.Boolean;
      pragma Import (Java, HeavyWeightPopupEnabled, "heavyWeightPopupEnabled");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetEnabled (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsEnabled (This : access Typ)
                       return Java.Boolean;

   procedure SetLightWeightPopupEnabled (This : access Typ;
                                         P1_Boolean : Java.Boolean);

   function IsLightWeightPopupEnabled (This : access Typ)
                                       return Java.Boolean;

   procedure SetInitialDelay (This : access Typ;
                              P1_Int : Java.Int);

   function GetInitialDelay (This : access Typ)
                             return Java.Int;

   procedure SetDismissDelay (This : access Typ;
                              P1_Int : Java.Int);

   function GetDismissDelay (This : access Typ)
                             return Java.Int;

   procedure SetReshowDelay (This : access Typ;
                             P1_Int : Java.Int);

   function GetReshowDelay (This : access Typ)
                            return Java.Int;

   function SharedInstance return access Javax.Swing.ToolTipManager.Typ'Class;

   procedure RegisterComponent (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure UnregisterComponent (This : access Typ;
                                  P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class);

   procedure MouseEntered (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseExited (This : access Typ;
                          P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MousePressed (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseDragged (This : access Typ;
                           P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);

   procedure MouseMoved (This : access Typ;
                         P1_MouseEvent : access Standard.Java.Awt.Event.MouseEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetEnabled, "setEnabled");
   pragma Import (Java, IsEnabled, "isEnabled");
   pragma Import (Java, SetLightWeightPopupEnabled, "setLightWeightPopupEnabled");
   pragma Import (Java, IsLightWeightPopupEnabled, "isLightWeightPopupEnabled");
   pragma Import (Java, SetInitialDelay, "setInitialDelay");
   pragma Import (Java, GetInitialDelay, "getInitialDelay");
   pragma Import (Java, SetDismissDelay, "setDismissDelay");
   pragma Import (Java, GetDismissDelay, "getDismissDelay");
   pragma Import (Java, SetReshowDelay, "setReshowDelay");
   pragma Import (Java, GetReshowDelay, "getReshowDelay");
   pragma Import (Java, SharedInstance, "sharedInstance");
   pragma Import (Java, RegisterComponent, "registerComponent");
   pragma Import (Java, UnregisterComponent, "unregisterComponent");
   pragma Import (Java, MouseEntered, "mouseEntered");
   pragma Import (Java, MouseExited, "mouseExited");
   pragma Import (Java, MousePressed, "mousePressed");
   pragma Import (Java, MouseDragged, "mouseDragged");
   pragma Import (Java, MouseMoved, "mouseMoved");

end Javax.Swing.ToolTipManager;
pragma Import (Java, Javax.Swing.ToolTipManager, "javax.swing.ToolTipManager");
pragma Extensions_Allowed (Off);
