pragma Extensions_Allowed (On);
limited with Java.Awt.Point;
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.TransferHandler.DropLocation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_DropLocation (P1_Point : access Standard.Java.Awt.Point.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetDropPoint (This : access Typ)
                          return access Java.Awt.Point.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DropLocation);
   pragma Import (Java, GetDropPoint, "getDropPoint");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.TransferHandler.DropLocation;
pragma Import (Java, Javax.Swing.TransferHandler.DropLocation, "javax.swing.TransferHandler$DropLocation");
pragma Extensions_Allowed (Off);
