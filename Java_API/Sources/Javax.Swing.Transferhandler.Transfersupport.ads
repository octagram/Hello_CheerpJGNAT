pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Awt.Datatransfer.Transferable;
limited with Javax.Swing.TransferHandler.DropLocation;
with Java.Lang.Object;

package Javax.Swing.TransferHandler.TransferSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TransferSupport (P1_Component : access Standard.Java.Awt.Component.Typ'Class;
                                 P2_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsDrop (This : access Typ)
                    return Java.Boolean;

   function GetComponent (This : access Typ)
                          return access Java.Awt.Component.Typ'Class;

   function GetDropLocation (This : access Typ)
                             return access Javax.Swing.TransferHandler.DropLocation.Typ'Class;

   procedure SetShowDropLocation (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   procedure SetDropAction (This : access Typ;
                            P1_Int : Java.Int);

   function GetDropAction (This : access Typ)
                           return Java.Int;

   function GetUserDropAction (This : access Typ)
                               return Java.Int;

   function GetSourceDropActions (This : access Typ)
                                  return Java.Int;

   function GetDataFlavors (This : access Typ)
                            return Standard.Java.Lang.Object.Ref;

   function IsDataFlavorSupported (This : access Typ;
                                   P1_DataFlavor : access Standard.Java.Awt.Datatransfer.DataFlavor.Typ'Class)
                                   return Java.Boolean;

   function GetTransferable (This : access Typ)
                             return access Java.Awt.Datatransfer.Transferable.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TransferSupport);
   pragma Import (Java, IsDrop, "isDrop");
   pragma Import (Java, GetComponent, "getComponent");
   pragma Import (Java, GetDropLocation, "getDropLocation");
   pragma Import (Java, SetShowDropLocation, "setShowDropLocation");
   pragma Import (Java, SetDropAction, "setDropAction");
   pragma Import (Java, GetDropAction, "getDropAction");
   pragma Import (Java, GetUserDropAction, "getUserDropAction");
   pragma Import (Java, GetSourceDropActions, "getSourceDropActions");
   pragma Import (Java, GetDataFlavors, "getDataFlavors");
   pragma Import (Java, IsDataFlavorSupported, "isDataFlavorSupported");
   pragma Import (Java, GetTransferable, "getTransferable");

end Javax.Swing.TransferHandler.TransferSupport;
pragma Import (Java, Javax.Swing.TransferHandler.TransferSupport, "javax.swing.TransferHandler$TransferSupport");
pragma Extensions_Allowed (Off);
