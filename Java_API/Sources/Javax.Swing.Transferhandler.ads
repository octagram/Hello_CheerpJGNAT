pragma Extensions_Allowed (On);
limited with Java.Awt.Datatransfer.Clipboard;
limited with Java.Awt.Datatransfer.DataFlavor;
limited with Java.Awt.Datatransfer.Transferable;
limited with Java.Awt.Event.InputEvent;
limited with Java.Lang.String;
limited with Javax.Swing.Action;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.TransferHandler.TransferSupport;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.TransferHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCutAction return access Javax.Swing.Action.Typ'Class;

   function GetCopyAction return access Javax.Swing.Action.Typ'Class;

   function GetPasteAction return access Javax.Swing.Action.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TransferHandler (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   --  protected
   function New_TransferHandler (This : Ref := null)
                                 return Ref;

   procedure ExportAsDrag (This : access Typ;
                           P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                           P2_InputEvent : access Standard.Java.Awt.Event.InputEvent.Typ'Class;
                           P3_Int : Java.Int);

   procedure ExportToClipboard (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                                P2_Clipboard : access Standard.Java.Awt.Datatransfer.Clipboard.Typ'Class;
                                P3_Int : Java.Int);
   --  can raise Java.Lang.IllegalStateException.Except

   function ImportData (This : access Typ;
                        P1_TransferSupport : access Standard.Javax.Swing.TransferHandler.TransferSupport.Typ'Class)
                        return Java.Boolean;

   function ImportData (This : access Typ;
                        P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                        P2_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class)
                        return Java.Boolean;

   function CanImport (This : access Typ;
                       P1_TransferSupport : access Standard.Javax.Swing.TransferHandler.TransferSupport.Typ'Class)
                       return Java.Boolean;

   function CanImport (This : access Typ;
                       P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                       P2_DataFlavor_Arr : access Java.Awt.Datatransfer.DataFlavor.Arr_Obj)
                       return Java.Boolean;

   function GetSourceActions (This : access Typ;
                              P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                              return Java.Int;

   function GetVisualRepresentation (This : access Typ;
                                     P1_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class)
                                     return access Javax.Swing.Icon.Typ'Class;

   --  protected
   function CreateTransferable (This : access Typ;
                                P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                                return access Java.Awt.Datatransfer.Transferable.Typ'Class;

   --  protected
   procedure ExportDone (This : access Typ;
                         P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class;
                         P2_Transferable : access Standard.Java.Awt.Datatransfer.Transferable.Typ'Class;
                         P3_Int : Java.Int);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NONE : constant Java.Int;

   --  final
   COPY : constant Java.Int;

   --  final
   MOVE : constant Java.Int;

   --  final
   COPY_OR_MOVE : constant Java.Int;

   --  final
   LINK : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetCutAction, "getCutAction");
   pragma Import (Java, GetCopyAction, "getCopyAction");
   pragma Import (Java, GetPasteAction, "getPasteAction");
   pragma Java_Constructor (New_TransferHandler);
   pragma Import (Java, ExportAsDrag, "exportAsDrag");
   pragma Import (Java, ExportToClipboard, "exportToClipboard");
   pragma Import (Java, ImportData, "importData");
   pragma Import (Java, CanImport, "canImport");
   pragma Import (Java, GetSourceActions, "getSourceActions");
   pragma Import (Java, GetVisualRepresentation, "getVisualRepresentation");
   pragma Import (Java, CreateTransferable, "createTransferable");
   pragma Import (Java, ExportDone, "exportDone");
   pragma Import (Java, NONE, "NONE");
   pragma Import (Java, COPY, "COPY");
   pragma Import (Java, MOVE, "MOVE");
   pragma Import (Java, COPY_OR_MOVE, "COPY_OR_MOVE");
   pragma Import (Java, LINK, "LINK");

end Javax.Swing.TransferHandler;
pragma Import (Java, Javax.Swing.TransferHandler, "javax.swing.TransferHandler");
pragma Extensions_Allowed (Off);
