pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
with Java.Lang.Object;

package Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_NodeDimensions (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNodeDimensions (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Boolean : Java.Boolean;
                               P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return access Java.Awt.Rectangle.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NodeDimensions);
   pragma Export (Java, GetNodeDimensions, "getNodeDimensions");

end Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions;
pragma Import (Java, Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions, "javax.swing.tree.AbstractLayoutCache$NodeDimensions");
pragma Extensions_Allowed (Off);
