pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Event.TreeModelEvent;
limited with Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions;
limited with Javax.Swing.Tree.TreeModel;
limited with Javax.Swing.Tree.TreePath;
limited with Javax.Swing.Tree.TreeSelectionModel;
with Java.Lang.Object;
with Javax.Swing.Tree.RowMapper;

package Javax.Swing.Tree.AbstractLayoutCache is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RowMapper_I : Javax.Swing.Tree.RowMapper.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      NodeDimensions : access Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions.Typ'Class;
      pragma Import (Java, NodeDimensions, "nodeDimensions");

      --  protected
      TreeModel : access Javax.Swing.Tree.TreeModel.Typ'Class;
      pragma Import (Java, TreeModel, "treeModel");

      --  protected
      TreeSelectionModel : access Javax.Swing.Tree.TreeSelectionModel.Typ'Class;
      pragma Import (Java, TreeSelectionModel, "treeSelectionModel");

      --  protected
      RootVisible : Java.Boolean;
      pragma Import (Java, RootVisible, "rootVisible");

      --  protected
      RowHeight : Java.Int;
      pragma Import (Java, RowHeight, "rowHeight");

   end record;

   function New_AbstractLayoutCache (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetNodeDimensions (This : access Typ;
                                P1_NodeDimensions : access Standard.Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions.Typ'Class);

   function GetNodeDimensions (This : access Typ)
                               return access Javax.Swing.Tree.AbstractLayoutCache.NodeDimensions.Typ'Class;

   procedure SetModel (This : access Typ;
                       P1_TreeModel : access Standard.Javax.Swing.Tree.TreeModel.Typ'Class);

   function GetModel (This : access Typ)
                      return access Javax.Swing.Tree.TreeModel.Typ'Class;

   procedure SetRootVisible (This : access Typ;
                             P1_Boolean : Java.Boolean);

   function IsRootVisible (This : access Typ)
                           return Java.Boolean;

   procedure SetRowHeight (This : access Typ;
                           P1_Int : Java.Int);

   function GetRowHeight (This : access Typ)
                          return Java.Int;

   procedure SetSelectionModel (This : access Typ;
                                P1_TreeSelectionModel : access Standard.Javax.Swing.Tree.TreeSelectionModel.Typ'Class);

   function GetSelectionModel (This : access Typ)
                               return access Javax.Swing.Tree.TreeSelectionModel.Typ'Class;

   function GetPreferredHeight (This : access Typ)
                                return Java.Int;

   function GetPreferredWidth (This : access Typ;
                               P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return Java.Int;

   function IsExpanded (This : access Typ;
                        P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                        return Java.Boolean is abstract;

   function GetBounds (This : access Typ;
                       P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                       P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                       return access Java.Awt.Rectangle.Typ'Class is abstract;

   function GetPathForRow (This : access Typ;
                           P1_Int : Java.Int)
                           return access Javax.Swing.Tree.TreePath.Typ'Class is abstract;

   function GetRowForPath (This : access Typ;
                           P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return Java.Int is abstract;

   function GetPathClosestTo (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int)
                              return access Javax.Swing.Tree.TreePath.Typ'Class is abstract;

   function GetVisiblePathsFrom (This : access Typ;
                                 P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                                 return access Java.Util.Enumeration.Typ'Class is abstract;

   function GetVisibleChildCount (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                                  return Java.Int is abstract;

   procedure SetExpandedState (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                               P2_Boolean : Java.Boolean) is abstract;

   function GetExpandedState (This : access Typ;
                              P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                              return Java.Boolean is abstract;

   function GetRowCount (This : access Typ)
                         return Java.Int is abstract;

   procedure InvalidateSizes (This : access Typ) is abstract;

   procedure InvalidatePathBounds (This : access Typ;
                                   P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class) is abstract;

   procedure TreeNodesChanged (This : access Typ;
                               P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class) is abstract;

   procedure TreeNodesInserted (This : access Typ;
                                P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class) is abstract;

   procedure TreeNodesRemoved (This : access Typ;
                               P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class) is abstract;

   procedure TreeStructureChanged (This : access Typ;
                                   P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class) is abstract;

   function GetRowsForPaths (This : access Typ;
                             P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj)
                             return Java.Int_Arr;

   --  protected
   function GetNodeDimensions (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_Boolean : Java.Boolean;
                               P5_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                               return access Java.Awt.Rectangle.Typ'Class;

   --  protected
   function IsFixedRowHeight (This : access Typ)
                              return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractLayoutCache);
   pragma Export (Java, SetNodeDimensions, "setNodeDimensions");
   pragma Export (Java, GetNodeDimensions, "getNodeDimensions");
   pragma Export (Java, SetModel, "setModel");
   pragma Export (Java, GetModel, "getModel");
   pragma Export (Java, SetRootVisible, "setRootVisible");
   pragma Export (Java, IsRootVisible, "isRootVisible");
   pragma Export (Java, SetRowHeight, "setRowHeight");
   pragma Export (Java, GetRowHeight, "getRowHeight");
   pragma Export (Java, SetSelectionModel, "setSelectionModel");
   pragma Export (Java, GetSelectionModel, "getSelectionModel");
   pragma Export (Java, GetPreferredHeight, "getPreferredHeight");
   pragma Export (Java, GetPreferredWidth, "getPreferredWidth");
   pragma Export (Java, IsExpanded, "isExpanded");
   pragma Export (Java, GetBounds, "getBounds");
   pragma Export (Java, GetPathForRow, "getPathForRow");
   pragma Export (Java, GetRowForPath, "getRowForPath");
   pragma Export (Java, GetPathClosestTo, "getPathClosestTo");
   pragma Export (Java, GetVisiblePathsFrom, "getVisiblePathsFrom");
   pragma Export (Java, GetVisibleChildCount, "getVisibleChildCount");
   pragma Export (Java, SetExpandedState, "setExpandedState");
   pragma Export (Java, GetExpandedState, "getExpandedState");
   pragma Export (Java, GetRowCount, "getRowCount");
   pragma Export (Java, InvalidateSizes, "invalidateSizes");
   pragma Export (Java, InvalidatePathBounds, "invalidatePathBounds");
   pragma Export (Java, TreeNodesChanged, "treeNodesChanged");
   pragma Export (Java, TreeNodesInserted, "treeNodesInserted");
   pragma Export (Java, TreeNodesRemoved, "treeNodesRemoved");
   pragma Export (Java, TreeStructureChanged, "treeStructureChanged");
   pragma Export (Java, GetRowsForPaths, "getRowsForPaths");
   pragma Export (Java, IsFixedRowHeight, "isFixedRowHeight");

end Javax.Swing.Tree.AbstractLayoutCache;
pragma Import (Java, Javax.Swing.Tree.AbstractLayoutCache, "javax.swing.tree.AbstractLayoutCache");
pragma Extensions_Allowed (Off);
