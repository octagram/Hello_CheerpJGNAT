pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
limited with Java.Util.Vector;
limited with Javax.Swing.Tree.TreeNode;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Tree.MutableTreeNode;

package Javax.Swing.Tree.DefaultMutableTreeNode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            MutableTreeNode_I : Javax.Swing.Tree.MutableTreeNode.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Parent : access Javax.Swing.Tree.MutableTreeNode.Typ'Class;
      pragma Import (Java, Parent, "parent");

      --  protected
      Children : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Children, "children");

      --  protected
      UserObject : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, UserObject, "userObject");

      --  protected
      AllowsChildren : Java.Boolean;
      pragma Import (Java, AllowsChildren, "allowsChildren");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultMutableTreeNode (This : Ref := null)
                                        return Ref;

   function New_DefaultMutableTreeNode (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                        This : Ref := null)
                                        return Ref;

   function New_DefaultMutableTreeNode (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P2_Boolean : Java.Boolean; 
                                        This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Insert (This : access Typ;
                     P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class;
                     P2_Int : Java.Int);

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int);

   procedure SetParent (This : access Typ;
                        P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class);

   function GetParent (This : access Typ)
                       return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function GetChildAt (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function GetChildCount (This : access Typ)
                           return Java.Int;

   function GetIndex (This : access Typ;
                      P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                      return Java.Int;

   function Children (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class;

   procedure SetAllowsChildren (This : access Typ;
                                P1_Boolean : Java.Boolean);

   function GetAllowsChildren (This : access Typ)
                               return Java.Boolean;

   procedure SetUserObject (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetUserObject (This : access Typ)
                           return access Java.Lang.Object.Typ'Class;

   procedure RemoveFromParent (This : access Typ);

   procedure Remove (This : access Typ;
                     P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class);

   procedure RemoveAllChildren (This : access Typ);

   procedure Add (This : access Typ;
                  P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class);

   function IsNodeAncestor (This : access Typ;
                            P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                            return Java.Boolean;

   function IsNodeDescendant (This : access Typ;
                              P1_DefaultMutableTreeNode : access Standard.Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class)
                              return Java.Boolean;

   function GetSharedAncestor (This : access Typ;
                               P1_DefaultMutableTreeNode : access Standard.Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class)
                               return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function IsNodeRelated (This : access Typ;
                           P1_DefaultMutableTreeNode : access Standard.Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class)
                           return Java.Boolean;

   function GetDepth (This : access Typ)
                      return Java.Int;

   function GetLevel (This : access Typ)
                      return Java.Int;

   function GetPath (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetPathToRoot (This : access Typ;
                           P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class;
                           P2_Int : Java.Int)
                           return Standard.Java.Lang.Object.Ref;

   function GetUserObjectPath (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetRoot (This : access Typ)
                     return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function IsRoot (This : access Typ)
                    return Java.Boolean;

   function GetNextNode (This : access Typ)
                         return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function GetPreviousNode (This : access Typ)
                             return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function PreorderEnumeration (This : access Typ)
                                 return access Java.Util.Enumeration.Typ'Class;

   function PostorderEnumeration (This : access Typ)
                                  return access Java.Util.Enumeration.Typ'Class;

   function BreadthFirstEnumeration (This : access Typ)
                                     return access Java.Util.Enumeration.Typ'Class;

   function DepthFirstEnumeration (This : access Typ)
                                   return access Java.Util.Enumeration.Typ'Class;

   function PathFromAncestorEnumeration (This : access Typ;
                                         P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                                         return access Java.Util.Enumeration.Typ'Class;

   function IsNodeChild (This : access Typ;
                         P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                         return Java.Boolean;

   function GetFirstChild (This : access Typ)
                           return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function GetLastChild (This : access Typ)
                          return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function GetChildAfter (This : access Typ;
                           P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                           return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function GetChildBefore (This : access Typ;
                            P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                            return access Javax.Swing.Tree.TreeNode.Typ'Class;

   function IsNodeSibling (This : access Typ;
                           P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                           return Java.Boolean;

   function GetSiblingCount (This : access Typ)
                             return Java.Int;

   function GetNextSibling (This : access Typ)
                            return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function GetPreviousSibling (This : access Typ)
                                return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function IsLeaf (This : access Typ)
                    return Java.Boolean;

   function GetFirstLeaf (This : access Typ)
                          return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function GetLastLeaf (This : access Typ)
                         return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function GetNextLeaf (This : access Typ)
                         return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function GetPreviousLeaf (This : access Typ)
                             return access Javax.Swing.Tree.DefaultMutableTreeNode.Typ'Class;

   function GetLeafCount (This : access Typ)
                          return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   EMPTY_ENUMERATION : access Java.Util.Enumeration.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultMutableTreeNode);
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, GetChildAt, "getChildAt");
   pragma Import (Java, GetChildCount, "getChildCount");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, Children, "children");
   pragma Import (Java, SetAllowsChildren, "setAllowsChildren");
   pragma Import (Java, GetAllowsChildren, "getAllowsChildren");
   pragma Import (Java, SetUserObject, "setUserObject");
   pragma Import (Java, GetUserObject, "getUserObject");
   pragma Import (Java, RemoveFromParent, "removeFromParent");
   pragma Import (Java, RemoveAllChildren, "removeAllChildren");
   pragma Import (Java, Add, "add");
   pragma Import (Java, IsNodeAncestor, "isNodeAncestor");
   pragma Import (Java, IsNodeDescendant, "isNodeDescendant");
   pragma Import (Java, GetSharedAncestor, "getSharedAncestor");
   pragma Import (Java, IsNodeRelated, "isNodeRelated");
   pragma Import (Java, GetDepth, "getDepth");
   pragma Import (Java, GetLevel, "getLevel");
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, GetPathToRoot, "getPathToRoot");
   pragma Import (Java, GetUserObjectPath, "getUserObjectPath");
   pragma Import (Java, GetRoot, "getRoot");
   pragma Import (Java, IsRoot, "isRoot");
   pragma Import (Java, GetNextNode, "getNextNode");
   pragma Import (Java, GetPreviousNode, "getPreviousNode");
   pragma Import (Java, PreorderEnumeration, "preorderEnumeration");
   pragma Import (Java, PostorderEnumeration, "postorderEnumeration");
   pragma Import (Java, BreadthFirstEnumeration, "breadthFirstEnumeration");
   pragma Import (Java, DepthFirstEnumeration, "depthFirstEnumeration");
   pragma Import (Java, PathFromAncestorEnumeration, "pathFromAncestorEnumeration");
   pragma Import (Java, IsNodeChild, "isNodeChild");
   pragma Import (Java, GetFirstChild, "getFirstChild");
   pragma Import (Java, GetLastChild, "getLastChild");
   pragma Import (Java, GetChildAfter, "getChildAfter");
   pragma Import (Java, GetChildBefore, "getChildBefore");
   pragma Import (Java, IsNodeSibling, "isNodeSibling");
   pragma Import (Java, GetSiblingCount, "getSiblingCount");
   pragma Import (Java, GetNextSibling, "getNextSibling");
   pragma Import (Java, GetPreviousSibling, "getPreviousSibling");
   pragma Import (Java, IsLeaf, "isLeaf");
   pragma Import (Java, GetFirstLeaf, "getFirstLeaf");
   pragma Import (Java, GetLastLeaf, "getLastLeaf");
   pragma Import (Java, GetNextLeaf, "getNextLeaf");
   pragma Import (Java, GetPreviousLeaf, "getPreviousLeaf");
   pragma Import (Java, GetLeafCount, "getLeafCount");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, EMPTY_ENUMERATION, "EMPTY_ENUMERATION");

end Javax.Swing.Tree.DefaultMutableTreeNode;
pragma Import (Java, Javax.Swing.Tree.DefaultMutableTreeNode, "javax.swing.tree.DefaultMutableTreeNode");
pragma Extensions_Allowed (Off);
