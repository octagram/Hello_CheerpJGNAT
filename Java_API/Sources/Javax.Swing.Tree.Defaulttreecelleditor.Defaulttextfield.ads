pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Javax.Swing.Border.Border;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JTextField;
with Javax.Swing.Scrollable;
with Javax.Swing.SwingConstants;

package Javax.Swing.Tree.DefaultTreeCellEditor.DefaultTextField is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            Scrollable_I : Javax.Swing.Scrollable.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref)
    is new Javax.Swing.JTextField.Typ(MenuContainer_I,
                                      ImageObserver_I,
                                      Serializable_I,
                                      Accessible_I,
                                      Scrollable_I,
                                      SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Border : access Javax.Swing.Border.Border.Typ'Class;
      pragma Import (Java, Border, "border");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTextField (P1_DefaultTreeCellEditor : access Standard.Javax.Swing.Tree.DefaultTreeCellEditor.Typ'Class;
                                  P2_Border : access Standard.Javax.Swing.Border.Border.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetBorder (This : access Typ;
                        P1_Border : access Standard.Javax.Swing.Border.Border.Typ'Class);

   function GetBorder (This : access Typ)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTextField);
   pragma Import (Java, SetBorder, "setBorder");
   pragma Import (Java, GetBorder, "getBorder");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");

end Javax.Swing.Tree.DefaultTreeCellEditor.DefaultTextField;
pragma Import (Java, Javax.Swing.Tree.DefaultTreeCellEditor.DefaultTextField, "javax.swing.tree.DefaultTreeCellEditor$DefaultTextField");
pragma Extensions_Allowed (Off);
