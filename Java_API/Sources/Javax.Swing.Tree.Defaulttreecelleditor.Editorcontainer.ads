pragma Extensions_Allowed (On);
limited with Java.Awt.Dimension;
limited with Java.Awt.Graphics;
with Java.Awt.Container;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Tree.DefaultTreeCellEditor.EditorContainer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Awt.Container.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EditorContainer (P1_DefaultTreeCellEditor : access Standard.Javax.Swing.Tree.DefaultTreeCellEditor.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure EditorContainer (This : access Typ);

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   procedure DoLayout (This : access Typ);

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EditorContainer);
   pragma Import (Java, EditorContainer, "EditorContainer");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, DoLayout, "doLayout");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");

end Javax.Swing.Tree.DefaultTreeCellEditor.EditorContainer;
pragma Import (Java, Javax.Swing.Tree.DefaultTreeCellEditor.EditorContainer, "javax.swing.tree.DefaultTreeCellEditor$EditorContainer");
pragma Extensions_Allowed (Off);
