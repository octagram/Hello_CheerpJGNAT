pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Container;
limited with Java.Awt.Event.ActionEvent;
limited with Java.Awt.Font;
limited with Java.Util.EventObject;
limited with Javax.Swing.Event.CellEditorListener;
limited with Javax.Swing.Event.TreeSelectionEvent;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JTree;
limited with Javax.Swing.Timer;
limited with Javax.Swing.Tree.DefaultTreeCellRenderer;
limited with Javax.Swing.Tree.TreePath;
with Java.Awt.Event.ActionListener;
with Java.Lang.Object;
with Javax.Swing.Event.TreeSelectionListener;
with Javax.Swing.Tree.TreeCellEditor;

package Javax.Swing.Tree.DefaultTreeCellEditor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ActionListener_I : Java.Awt.Event.ActionListener.Ref;
            TreeSelectionListener_I : Javax.Swing.Event.TreeSelectionListener.Ref;
            TreeCellEditor_I : Javax.Swing.Tree.TreeCellEditor.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      RealEditor : access Javax.Swing.Tree.TreeCellEditor.Typ'Class;
      pragma Import (Java, RealEditor, "realEditor");

      --  protected
      Renderer : access Javax.Swing.Tree.DefaultTreeCellRenderer.Typ'Class;
      pragma Import (Java, Renderer, "renderer");

      --  protected
      EditingContainer : access Java.Awt.Container.Typ'Class;
      pragma Import (Java, EditingContainer, "editingContainer");

      --  protected
      EditingComponent : access Java.Awt.Component.Typ'Class;
      pragma Import (Java, EditingComponent, "editingComponent");

      --  protected
      CanEdit : Java.Boolean;
      pragma Import (Java, CanEdit, "canEdit");

      --  protected
      Offset : Java.Int;
      pragma Import (Java, Offset, "offset");

      --  protected
      Tree : access Javax.Swing.JTree.Typ'Class;
      pragma Import (Java, Tree, "tree");

      --  protected
      LastPath : access Javax.Swing.Tree.TreePath.Typ'Class;
      pragma Import (Java, LastPath, "lastPath");

      --  protected
      Timer : access Javax.Swing.Timer.Typ'Class;
      pragma Import (Java, Timer, "timer");

      --  protected
      LastRow : Java.Int;
      pragma Import (Java, LastRow, "lastRow");

      --  protected
      BorderSelectionColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, BorderSelectionColor, "borderSelectionColor");

      --  protected
      EditingIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, EditingIcon, "editingIcon");

      --  protected
      Font : access Java.Awt.Font.Typ'Class;
      pragma Import (Java, Font, "font");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTreeCellEditor (P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                       P2_DefaultTreeCellRenderer : access Standard.Javax.Swing.Tree.DefaultTreeCellRenderer.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   function New_DefaultTreeCellEditor (P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                       P2_DefaultTreeCellRenderer : access Standard.Javax.Swing.Tree.DefaultTreeCellRenderer.Typ'Class;
                                       P3_TreeCellEditor : access Standard.Javax.Swing.Tree.TreeCellEditor.Typ'Class; 
                                       This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetBorderSelectionColor (This : access Typ;
                                      P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBorderSelectionColor (This : access Typ)
                                     return access Java.Awt.Color.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   function GetTreeCellEditorComponent (This : access Typ;
                                        P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                        P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                        P3_Boolean : Java.Boolean;
                                        P4_Boolean : Java.Boolean;
                                        P5_Boolean : Java.Boolean;
                                        P6_Int : Java.Int)
                                        return access Java.Awt.Component.Typ'Class;

   function GetCellEditorValue (This : access Typ)
                                return access Java.Lang.Object.Typ'Class;

   function IsCellEditable (This : access Typ;
                            P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                            return Java.Boolean;

   function ShouldSelectCell (This : access Typ;
                              P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                              return Java.Boolean;

   function StopCellEditing (This : access Typ)
                             return Java.Boolean;

   procedure CancelCellEditing (This : access Typ);

   procedure AddCellEditorListener (This : access Typ;
                                    P1_CellEditorListener : access Standard.Javax.Swing.Event.CellEditorListener.Typ'Class);

   procedure RemoveCellEditorListener (This : access Typ;
                                       P1_CellEditorListener : access Standard.Javax.Swing.Event.CellEditorListener.Typ'Class);

   function GetCellEditorListeners (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;

   procedure ValueChanged (This : access Typ;
                           P1_TreeSelectionEvent : access Standard.Javax.Swing.Event.TreeSelectionEvent.Typ'Class);

   procedure ActionPerformed (This : access Typ;
                              P1_ActionEvent : access Standard.Java.Awt.Event.ActionEvent.Typ'Class);

   --  protected
   procedure SetTree (This : access Typ;
                      P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class);

   --  protected
   function ShouldStartEditingTimer (This : access Typ;
                                     P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                                     return Java.Boolean;

   --  protected
   procedure StartEditingTimer (This : access Typ);

   --  protected
   function CanEditImmediately (This : access Typ;
                                P1_EventObject : access Standard.Java.Util.EventObject.Typ'Class)
                                return Java.Boolean;

   --  protected
   function InHitRegion (This : access Typ;
                         P1_Int : Java.Int;
                         P2_Int : Java.Int)
                         return Java.Boolean;

   --  protected
   procedure DetermineOffset (This : access Typ;
                              P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P3_Boolean : Java.Boolean;
                              P4_Boolean : Java.Boolean;
                              P5_Boolean : Java.Boolean;
                              P6_Int : Java.Int);

   --  protected
   procedure PrepareForEditing (This : access Typ);

   --  protected
   function CreateContainer (This : access Typ)
                             return access Java.Awt.Container.Typ'Class;

   --  protected
   function CreateTreeCellEditor (This : access Typ)
                                  return access Javax.Swing.Tree.TreeCellEditor.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTreeCellEditor);
   pragma Import (Java, SetBorderSelectionColor, "setBorderSelectionColor");
   pragma Import (Java, GetBorderSelectionColor, "getBorderSelectionColor");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, GetTreeCellEditorComponent, "getTreeCellEditorComponent");
   pragma Import (Java, GetCellEditorValue, "getCellEditorValue");
   pragma Import (Java, IsCellEditable, "isCellEditable");
   pragma Import (Java, ShouldSelectCell, "shouldSelectCell");
   pragma Import (Java, StopCellEditing, "stopCellEditing");
   pragma Import (Java, CancelCellEditing, "cancelCellEditing");
   pragma Import (Java, AddCellEditorListener, "addCellEditorListener");
   pragma Import (Java, RemoveCellEditorListener, "removeCellEditorListener");
   pragma Import (Java, GetCellEditorListeners, "getCellEditorListeners");
   pragma Import (Java, ValueChanged, "valueChanged");
   pragma Import (Java, ActionPerformed, "actionPerformed");
   pragma Import (Java, SetTree, "setTree");
   pragma Import (Java, ShouldStartEditingTimer, "shouldStartEditingTimer");
   pragma Import (Java, StartEditingTimer, "startEditingTimer");
   pragma Import (Java, CanEditImmediately, "canEditImmediately");
   pragma Import (Java, InHitRegion, "inHitRegion");
   pragma Import (Java, DetermineOffset, "determineOffset");
   pragma Import (Java, PrepareForEditing, "prepareForEditing");
   pragma Import (Java, CreateContainer, "createContainer");
   pragma Import (Java, CreateTreeCellEditor, "createTreeCellEditor");

end Javax.Swing.Tree.DefaultTreeCellEditor;
pragma Import (Java, Javax.Swing.Tree.DefaultTreeCellEditor, "javax.swing.tree.DefaultTreeCellEditor");
pragma Extensions_Allowed (Off);
