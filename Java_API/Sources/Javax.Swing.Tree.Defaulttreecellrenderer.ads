pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Component;
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Java.Awt.Graphics;
limited with Java.Awt.Rectangle;
limited with Java.Lang.String;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JTree;
with Java.Awt.Image.ImageObserver;
with Java.Awt.MenuContainer;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Accessibility.Accessible;
with Javax.Swing.JLabel;
with Javax.Swing.SwingConstants;
with Javax.Swing.Tree.TreeCellRenderer;

package Javax.Swing.Tree.DefaultTreeCellRenderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(MenuContainer_I : Java.Awt.MenuContainer.Ref;
            ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
            Serializable_I : Java.Io.Serializable.Ref;
            Accessible_I : Javax.Accessibility.Accessible.Ref;
            SwingConstants_I : Javax.Swing.SwingConstants.Ref;
            TreeCellRenderer_I : Javax.Swing.Tree.TreeCellRenderer.Ref)
    is new Javax.Swing.JLabel.Typ(MenuContainer_I,
                                  ImageObserver_I,
                                  Serializable_I,
                                  Accessible_I,
                                  SwingConstants_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Selected : Java.Boolean;
      pragma Import (Java, Selected, "selected");

      --  protected
      HasFocus : Java.Boolean;
      pragma Import (Java, HasFocus, "hasFocus");

      --  protected
      ClosedIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, ClosedIcon, "closedIcon");

      --  protected
      LeafIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, LeafIcon, "leafIcon");

      --  protected
      OpenIcon : access Javax.Swing.Icon.Typ'Class;
      pragma Import (Java, OpenIcon, "openIcon");

      --  protected
      TextSelectionColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, TextSelectionColor, "textSelectionColor");

      --  protected
      TextNonSelectionColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, TextNonSelectionColor, "textNonSelectionColor");

      --  protected
      BackgroundSelectionColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, BackgroundSelectionColor, "backgroundSelectionColor");

      --  protected
      BackgroundNonSelectionColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, BackgroundNonSelectionColor, "backgroundNonSelectionColor");

      --  protected
      BorderSelectionColor : access Java.Awt.Color.Typ'Class;
      pragma Import (Java, BorderSelectionColor, "borderSelectionColor");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTreeCellRenderer (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultOpenIcon (This : access Typ)
                                return access Javax.Swing.Icon.Typ'Class;

   function GetDefaultClosedIcon (This : access Typ)
                                  return access Javax.Swing.Icon.Typ'Class;

   function GetDefaultLeafIcon (This : access Typ)
                                return access Javax.Swing.Icon.Typ'Class;

   procedure SetOpenIcon (This : access Typ;
                          P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetOpenIcon (This : access Typ)
                         return access Javax.Swing.Icon.Typ'Class;

   procedure SetClosedIcon (This : access Typ;
                            P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetClosedIcon (This : access Typ)
                           return access Javax.Swing.Icon.Typ'Class;

   procedure SetLeafIcon (This : access Typ;
                          P1_Icon : access Standard.Javax.Swing.Icon.Typ'Class);

   function GetLeafIcon (This : access Typ)
                         return access Javax.Swing.Icon.Typ'Class;

   procedure SetTextSelectionColor (This : access Typ;
                                    P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetTextSelectionColor (This : access Typ)
                                   return access Java.Awt.Color.Typ'Class;

   procedure SetTextNonSelectionColor (This : access Typ;
                                       P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetTextNonSelectionColor (This : access Typ)
                                      return access Java.Awt.Color.Typ'Class;

   procedure SetBackgroundSelectionColor (This : access Typ;
                                          P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBackgroundSelectionColor (This : access Typ)
                                         return access Java.Awt.Color.Typ'Class;

   procedure SetBackgroundNonSelectionColor (This : access Typ;
                                             P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBackgroundNonSelectionColor (This : access Typ)
                                            return access Java.Awt.Color.Typ'Class;

   procedure SetBorderSelectionColor (This : access Typ;
                                      P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetBorderSelectionColor (This : access Typ)
                                     return access Java.Awt.Color.Typ'Class;

   procedure SetFont (This : access Typ;
                      P1_Font : access Standard.Java.Awt.Font.Typ'Class);

   function GetFont (This : access Typ)
                     return access Java.Awt.Font.Typ'Class;

   procedure SetBackground (This : access Typ;
                            P1_Color : access Standard.Java.Awt.Color.Typ'Class);

   function GetTreeCellRendererComponent (This : access Typ;
                                          P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                          P3_Boolean : Java.Boolean;
                                          P4_Boolean : Java.Boolean;
                                          P5_Boolean : Java.Boolean;
                                          P6_Int : Java.Int;
                                          P7_Boolean : Java.Boolean)
                                          return access Java.Awt.Component.Typ'Class;

   procedure Paint (This : access Typ;
                    P1_Graphics : access Standard.Java.Awt.Graphics.Typ'Class);

   function GetPreferredSize (This : access Typ)
                              return access Java.Awt.Dimension.Typ'Class;

   procedure Validate (This : access Typ);

   procedure Invalidate (This : access Typ);

   procedure Revalidate (This : access Typ);

   procedure Repaint (This : access Typ;
                      P1_Long : Java.Long;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int;
                      P5_Int : Java.Int);

   procedure Repaint (This : access Typ;
                      P1_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class);

   procedure Repaint (This : access Typ);

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Byte : Java.Byte;
                                 P3_Byte : Java.Byte);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Char : Java.Char;
                                 P3_Char : Java.Char);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Short : Java.Short;
                                 P3_Short : Java.Short);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Long : Java.Long;
                                 P3_Long : Java.Long);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Float : Java.Float;
                                 P3_Float : Java.Float);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Double : Java.Double;
                                 P3_Double : Java.Double);

   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Boolean : Java.Boolean;
                                 P3_Boolean : Java.Boolean);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTreeCellRenderer);
   pragma Import (Java, GetDefaultOpenIcon, "getDefaultOpenIcon");
   pragma Import (Java, GetDefaultClosedIcon, "getDefaultClosedIcon");
   pragma Import (Java, GetDefaultLeafIcon, "getDefaultLeafIcon");
   pragma Import (Java, SetOpenIcon, "setOpenIcon");
   pragma Import (Java, GetOpenIcon, "getOpenIcon");
   pragma Import (Java, SetClosedIcon, "setClosedIcon");
   pragma Import (Java, GetClosedIcon, "getClosedIcon");
   pragma Import (Java, SetLeafIcon, "setLeafIcon");
   pragma Import (Java, GetLeafIcon, "getLeafIcon");
   pragma Import (Java, SetTextSelectionColor, "setTextSelectionColor");
   pragma Import (Java, GetTextSelectionColor, "getTextSelectionColor");
   pragma Import (Java, SetTextNonSelectionColor, "setTextNonSelectionColor");
   pragma Import (Java, GetTextNonSelectionColor, "getTextNonSelectionColor");
   pragma Import (Java, SetBackgroundSelectionColor, "setBackgroundSelectionColor");
   pragma Import (Java, GetBackgroundSelectionColor, "getBackgroundSelectionColor");
   pragma Import (Java, SetBackgroundNonSelectionColor, "setBackgroundNonSelectionColor");
   pragma Import (Java, GetBackgroundNonSelectionColor, "getBackgroundNonSelectionColor");
   pragma Import (Java, SetBorderSelectionColor, "setBorderSelectionColor");
   pragma Import (Java, GetBorderSelectionColor, "getBorderSelectionColor");
   pragma Import (Java, SetFont, "setFont");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, SetBackground, "setBackground");
   pragma Import (Java, GetTreeCellRendererComponent, "getTreeCellRendererComponent");
   pragma Import (Java, Paint, "paint");
   pragma Import (Java, GetPreferredSize, "getPreferredSize");
   pragma Import (Java, Validate, "validate");
   pragma Import (Java, Invalidate, "invalidate");
   pragma Import (Java, Revalidate, "revalidate");
   pragma Import (Java, Repaint, "repaint");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");

end Javax.Swing.Tree.DefaultTreeCellRenderer;
pragma Import (Java, Javax.Swing.Tree.DefaultTreeCellRenderer, "javax.swing.tree.DefaultTreeCellRenderer");
pragma Extensions_Allowed (Off);
