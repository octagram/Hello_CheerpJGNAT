pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Util.EventListener;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Event.TreeModelListener;
limited with Javax.Swing.Tree.MutableTreeNode;
limited with Javax.Swing.Tree.TreeNode;
limited with Javax.Swing.Tree.TreePath;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Tree.TreeModel;

package Javax.Swing.Tree.DefaultTreeModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            TreeModel_I : Javax.Swing.Tree.TreeModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Root : access Javax.Swing.Tree.TreeNode.Typ'Class;
      pragma Import (Java, Root, "root");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      AsksAllowsChildren : Java.Boolean;
      pragma Import (Java, AsksAllowsChildren, "asksAllowsChildren");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTreeModel (P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   function New_DefaultTreeModel (P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class;
                                  P2_Boolean : Java.Boolean; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetAsksAllowsChildren (This : access Typ;
                                    P1_Boolean : Java.Boolean);

   function AsksAllowsChildren (This : access Typ)
                                return Java.Boolean;

   procedure SetRoot (This : access Typ;
                      P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class);

   function GetRoot (This : access Typ)
                     return access Java.Lang.Object.Typ'Class;

   function GetIndexOfChild (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Int;

   function GetChild (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int)
                      return access Java.Lang.Object.Typ'Class;

   function GetChildCount (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Int;

   function IsLeaf (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   procedure Reload (This : access Typ);

   procedure ValueForPathChanged (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure InsertNodeInto (This : access Typ;
                             P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class;
                             P2_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class;
                             P3_Int : Java.Int);

   procedure RemoveNodeFromParent (This : access Typ;
                                   P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class);

   procedure NodeChanged (This : access Typ;
                          P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class);

   procedure Reload (This : access Typ;
                     P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class);

   procedure NodesWereInserted (This : access Typ;
                                P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class;
                                P2_Int_Arr : Java.Int_Arr);

   procedure NodesWereRemoved (This : access Typ;
                               P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class;
                               P2_Int_Arr : Java.Int_Arr;
                               P3_Object_Arr : access Java.Lang.Object.Arr_Obj);

   procedure NodesChanged (This : access Typ;
                           P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class;
                           P2_Int_Arr : Java.Int_Arr);

   procedure NodeStructureChanged (This : access Typ;
                                   P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class);

   function GetPathToRoot (This : access Typ;
                           P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                           return Standard.Java.Lang.Object.Ref;

   --  protected
   function GetPathToRoot (This : access Typ;
                           P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class;
                           P2_Int : Java.Int)
                           return Standard.Java.Lang.Object.Ref;

   procedure AddTreeModelListener (This : access Typ;
                                   P1_TreeModelListener : access Standard.Javax.Swing.Event.TreeModelListener.Typ'Class);

   procedure RemoveTreeModelListener (This : access Typ;
                                      P1_TreeModelListener : access Standard.Javax.Swing.Event.TreeModelListener.Typ'Class);

   function GetTreeModelListeners (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireTreeNodesChanged (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                   P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                   P3_Int_Arr : Java.Int_Arr;
                                   P4_Object_Arr : access Java.Lang.Object.Arr_Obj);

   --  protected
   procedure FireTreeNodesInserted (This : access Typ;
                                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                    P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                    P3_Int_Arr : Java.Int_Arr;
                                    P4_Object_Arr : access Java.Lang.Object.Arr_Obj);

   --  protected
   procedure FireTreeNodesRemoved (This : access Typ;
                                   P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                   P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                   P3_Int_Arr : Java.Int_Arr;
                                   P4_Object_Arr : access Java.Lang.Object.Arr_Obj);

   --  protected
   procedure FireTreeStructureChanged (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                       P2_Object_Arr : access Java.Lang.Object.Arr_Obj;
                                       P3_Int_Arr : Java.Int_Arr;
                                       P4_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTreeModel);
   pragma Import (Java, SetAsksAllowsChildren, "setAsksAllowsChildren");
   pragma Import (Java, AsksAllowsChildren, "asksAllowsChildren");
   pragma Import (Java, SetRoot, "setRoot");
   pragma Import (Java, GetRoot, "getRoot");
   pragma Import (Java, GetIndexOfChild, "getIndexOfChild");
   pragma Import (Java, GetChild, "getChild");
   pragma Import (Java, GetChildCount, "getChildCount");
   pragma Import (Java, IsLeaf, "isLeaf");
   pragma Import (Java, Reload, "reload");
   pragma Import (Java, ValueForPathChanged, "valueForPathChanged");
   pragma Import (Java, InsertNodeInto, "insertNodeInto");
   pragma Import (Java, RemoveNodeFromParent, "removeNodeFromParent");
   pragma Import (Java, NodeChanged, "nodeChanged");
   pragma Import (Java, NodesWereInserted, "nodesWereInserted");
   pragma Import (Java, NodesWereRemoved, "nodesWereRemoved");
   pragma Import (Java, NodesChanged, "nodesChanged");
   pragma Import (Java, NodeStructureChanged, "nodeStructureChanged");
   pragma Import (Java, GetPathToRoot, "getPathToRoot");
   pragma Import (Java, AddTreeModelListener, "addTreeModelListener");
   pragma Import (Java, RemoveTreeModelListener, "removeTreeModelListener");
   pragma Import (Java, GetTreeModelListeners, "getTreeModelListeners");
   pragma Import (Java, FireTreeNodesChanged, "fireTreeNodesChanged");
   pragma Import (Java, FireTreeNodesInserted, "fireTreeNodesInserted");
   pragma Import (Java, FireTreeNodesRemoved, "fireTreeNodesRemoved");
   pragma Import (Java, FireTreeStructureChanged, "fireTreeStructureChanged");
   pragma Import (Java, GetListeners, "getListeners");

end Javax.Swing.Tree.DefaultTreeModel;
pragma Import (Java, Javax.Swing.Tree.DefaultTreeModel, "javax.swing.tree.DefaultTreeModel");
pragma Extensions_Allowed (Off);
