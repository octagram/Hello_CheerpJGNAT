pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.EventListener;
limited with Java.Util.Vector;
limited with Javax.Swing.DefaultListSelectionModel;
limited with Javax.Swing.Event.EventListenerList;
limited with Javax.Swing.Event.SwingPropertyChangeSupport;
limited with Javax.Swing.Event.TreeSelectionEvent;
limited with Javax.Swing.Event.TreeSelectionListener;
limited with Javax.Swing.Tree.RowMapper;
limited with Javax.Swing.Tree.TreePath;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Javax.Swing.Tree.TreeSelectionModel;

package Javax.Swing.Tree.DefaultTreeSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            TreeSelectionModel_I : Javax.Swing.Tree.TreeSelectionModel.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      ChangeSupport : access Javax.Swing.Event.SwingPropertyChangeSupport.Typ'Class;
      pragma Import (Java, ChangeSupport, "changeSupport");

      --  protected
      Selection : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Selection, "selection");

      --  protected
      ListenerList : access Javax.Swing.Event.EventListenerList.Typ'Class;
      pragma Import (Java, ListenerList, "listenerList");

      --  protected
      RowMapper : access Javax.Swing.Tree.RowMapper.Typ'Class;
      pragma Import (Java, RowMapper, "rowMapper");

      --  protected
      ListSelectionModel : access Javax.Swing.DefaultListSelectionModel.Typ'Class;
      pragma Import (Java, ListSelectionModel, "listSelectionModel");

      --  protected
      SelectionMode : Java.Int;
      pragma Import (Java, SelectionMode, "selectionMode");

      --  protected
      LeadPath : access Javax.Swing.Tree.TreePath.Typ'Class;
      pragma Import (Java, LeadPath, "leadPath");

      --  protected
      LeadIndex : Java.Int;
      pragma Import (Java, LeadIndex, "leadIndex");

      --  protected
      LeadRow : Java.Int;
      pragma Import (Java, LeadRow, "leadRow");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultTreeSelectionModel (This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetRowMapper (This : access Typ;
                           P1_RowMapper : access Standard.Javax.Swing.Tree.RowMapper.Typ'Class);

   function GetRowMapper (This : access Typ)
                          return access Javax.Swing.Tree.RowMapper.Typ'Class;

   procedure SetSelectionMode (This : access Typ;
                               P1_Int : Java.Int);

   function GetSelectionMode (This : access Typ)
                              return Java.Int;

   procedure SetSelectionPath (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure SetSelectionPaths (This : access Typ;
                                P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj);

   procedure AddSelectionPath (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure AddSelectionPaths (This : access Typ;
                                P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj);

   procedure RemoveSelectionPath (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure RemoveSelectionPaths (This : access Typ;
                                   P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj);

   function GetSelectionPath (This : access Typ)
                              return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetSelectionPaths (This : access Typ)
                               return Standard.Java.Lang.Object.Ref;

   function GetSelectionCount (This : access Typ)
                               return Java.Int;

   function IsPathSelected (This : access Typ;
                            P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                            return Java.Boolean;

   function IsSelectionEmpty (This : access Typ)
                              return Java.Boolean;

   procedure ClearSelection (This : access Typ);

   procedure AddTreeSelectionListener (This : access Typ;
                                       P1_TreeSelectionListener : access Standard.Javax.Swing.Event.TreeSelectionListener.Typ'Class);

   procedure RemoveTreeSelectionListener (This : access Typ;
                                          P1_TreeSelectionListener : access Standard.Javax.Swing.Event.TreeSelectionListener.Typ'Class);

   function GetTreeSelectionListeners (This : access Typ)
                                       return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FireValueChanged (This : access Typ;
                               P1_TreeSelectionEvent : access Standard.Javax.Swing.Event.TreeSelectionEvent.Typ'Class);

   function GetListeners (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return Standard.Java.Lang.Object.Ref;

   function GetSelectionRows (This : access Typ)
                              return Java.Int_Arr;

   function GetMinSelectionRow (This : access Typ)
                                return Java.Int;

   function GetMaxSelectionRow (This : access Typ)
                                return Java.Int;

   function IsRowSelected (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean;

   procedure ResetRowSelection (This : access Typ);

   function GetLeadSelectionRow (This : access Typ)
                                 return Java.Int;

   function GetLeadSelectionPath (This : access Typ)
                                  return access Javax.Swing.Tree.TreePath.Typ'Class;

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure InsureRowContinuity (This : access Typ);

   --  protected
   function ArePathsContiguous (This : access Typ;
                                P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj)
                                return Java.Boolean;

   --  protected
   function CanPathsBeAdded (This : access Typ;
                             P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj)
                             return Java.Boolean;

   --  protected
   function CanPathsBeRemoved (This : access Typ;
                               P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj)
                               return Java.Boolean;

   --  protected
   procedure NotifyPathChange (This : access Typ;
                               P1_Vector : access Standard.Java.Util.Vector.Typ'Class;
                               P2_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   --  protected
   procedure UpdateLeadIndex (This : access Typ);

   --  protected
   procedure InsureUniqueness (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.CloneNotSupportedException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SELECTION_MODE_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultTreeSelectionModel);
   pragma Import (Java, SetRowMapper, "setRowMapper");
   pragma Import (Java, GetRowMapper, "getRowMapper");
   pragma Import (Java, SetSelectionMode, "setSelectionMode");
   pragma Import (Java, GetSelectionMode, "getSelectionMode");
   pragma Import (Java, SetSelectionPath, "setSelectionPath");
   pragma Import (Java, SetSelectionPaths, "setSelectionPaths");
   pragma Import (Java, AddSelectionPath, "addSelectionPath");
   pragma Import (Java, AddSelectionPaths, "addSelectionPaths");
   pragma Import (Java, RemoveSelectionPath, "removeSelectionPath");
   pragma Import (Java, RemoveSelectionPaths, "removeSelectionPaths");
   pragma Import (Java, GetSelectionPath, "getSelectionPath");
   pragma Import (Java, GetSelectionPaths, "getSelectionPaths");
   pragma Import (Java, GetSelectionCount, "getSelectionCount");
   pragma Import (Java, IsPathSelected, "isPathSelected");
   pragma Import (Java, IsSelectionEmpty, "isSelectionEmpty");
   pragma Import (Java, ClearSelection, "clearSelection");
   pragma Import (Java, AddTreeSelectionListener, "addTreeSelectionListener");
   pragma Import (Java, RemoveTreeSelectionListener, "removeTreeSelectionListener");
   pragma Import (Java, GetTreeSelectionListeners, "getTreeSelectionListeners");
   pragma Import (Java, FireValueChanged, "fireValueChanged");
   pragma Import (Java, GetListeners, "getListeners");
   pragma Import (Java, GetSelectionRows, "getSelectionRows");
   pragma Import (Java, GetMinSelectionRow, "getMinSelectionRow");
   pragma Import (Java, GetMaxSelectionRow, "getMaxSelectionRow");
   pragma Import (Java, IsRowSelected, "isRowSelected");
   pragma Import (Java, ResetRowSelection, "resetRowSelection");
   pragma Import (Java, GetLeadSelectionRow, "getLeadSelectionRow");
   pragma Import (Java, GetLeadSelectionPath, "getLeadSelectionPath");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Import (Java, InsureRowContinuity, "insureRowContinuity");
   pragma Import (Java, ArePathsContiguous, "arePathsContiguous");
   pragma Import (Java, CanPathsBeAdded, "canPathsBeAdded");
   pragma Import (Java, CanPathsBeRemoved, "canPathsBeRemoved");
   pragma Import (Java, NotifyPathChange, "notifyPathChange");
   pragma Import (Java, UpdateLeadIndex, "updateLeadIndex");
   pragma Import (Java, InsureUniqueness, "insureUniqueness");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Clone, "clone");
   pragma Import (Java, SELECTION_MODE_PROPERTY, "SELECTION_MODE_PROPERTY");

end Javax.Swing.Tree.DefaultTreeSelectionModel;
pragma Import (Java, Javax.Swing.Tree.DefaultTreeSelectionModel, "javax.swing.tree.DefaultTreeSelectionModel");
pragma Extensions_Allowed (Off);
