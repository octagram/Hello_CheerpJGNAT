pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Event.TreeExpansionEvent;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Swing.Tree.ExpandVetoException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Event : access Javax.Swing.Event.TreeExpansionEvent.Typ'Class;
      pragma Import (Java, Event, "event");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ExpandVetoException (P1_TreeExpansionEvent : access Standard.Javax.Swing.Event.TreeExpansionEvent.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_ExpandVetoException (P1_TreeExpansionEvent : access Standard.Javax.Swing.Event.TreeExpansionEvent.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.swing.tree.ExpandVetoException");
   pragma Java_Constructor (New_ExpandVetoException);

end Javax.Swing.Tree.ExpandVetoException;
pragma Import (Java, Javax.Swing.Tree.ExpandVetoException, "javax.swing.tree.ExpandVetoException");
pragma Extensions_Allowed (Off);
