pragma Extensions_Allowed (On);
limited with Java.Awt.Rectangle;
limited with Java.Util.Enumeration;
limited with Javax.Swing.Event.TreeModelEvent;
limited with Javax.Swing.Tree.TreeModel;
limited with Javax.Swing.Tree.TreePath;
with Java.Lang.Object;
with Javax.Swing.Tree.AbstractLayoutCache;
with Javax.Swing.Tree.RowMapper;

package Javax.Swing.Tree.FixedHeightLayoutCache is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(RowMapper_I : Javax.Swing.Tree.RowMapper.Ref)
    is new Javax.Swing.Tree.AbstractLayoutCache.Typ(RowMapper_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FixedHeightLayoutCache (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetModel (This : access Typ;
                       P1_TreeModel : access Standard.Javax.Swing.Tree.TreeModel.Typ'Class);

   procedure SetRootVisible (This : access Typ;
                             P1_Boolean : Java.Boolean);

   procedure SetRowHeight (This : access Typ;
                           P1_Int : Java.Int);

   function GetRowCount (This : access Typ)
                         return Java.Int;

   procedure InvalidatePathBounds (This : access Typ;
                                   P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class);

   procedure InvalidateSizes (This : access Typ);

   function IsExpanded (This : access Typ;
                        P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                        return Java.Boolean;

   function GetBounds (This : access Typ;
                       P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                       P2_Rectangle : access Standard.Java.Awt.Rectangle.Typ'Class)
                       return access Java.Awt.Rectangle.Typ'Class;

   function GetPathForRow (This : access Typ;
                           P1_Int : Java.Int)
                           return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetRowForPath (This : access Typ;
                           P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                           return Java.Int;

   function GetPathClosestTo (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Int : Java.Int)
                              return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetVisibleChildCount (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                                  return Java.Int;

   function GetVisiblePathsFrom (This : access Typ;
                                 P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                                 return access Java.Util.Enumeration.Typ'Class;

   procedure SetExpandedState (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                               P2_Boolean : Java.Boolean);

   function GetExpandedState (This : access Typ;
                              P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                              return Java.Boolean;

   procedure TreeNodesChanged (This : access Typ;
                               P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);

   procedure TreeNodesInserted (This : access Typ;
                                P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);

   procedure TreeNodesRemoved (This : access Typ;
                               P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);

   procedure TreeStructureChanged (This : access Typ;
                                   P1_TreeModelEvent : access Standard.Javax.Swing.Event.TreeModelEvent.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_FixedHeightLayoutCache);
   pragma Import (Java, SetModel, "setModel");
   pragma Import (Java, SetRootVisible, "setRootVisible");
   pragma Import (Java, SetRowHeight, "setRowHeight");
   pragma Import (Java, GetRowCount, "getRowCount");
   pragma Import (Java, InvalidatePathBounds, "invalidatePathBounds");
   pragma Import (Java, InvalidateSizes, "invalidateSizes");
   pragma Import (Java, IsExpanded, "isExpanded");
   pragma Import (Java, GetBounds, "getBounds");
   pragma Import (Java, GetPathForRow, "getPathForRow");
   pragma Import (Java, GetRowForPath, "getRowForPath");
   pragma Import (Java, GetPathClosestTo, "getPathClosestTo");
   pragma Import (Java, GetVisibleChildCount, "getVisibleChildCount");
   pragma Import (Java, GetVisiblePathsFrom, "getVisiblePathsFrom");
   pragma Import (Java, SetExpandedState, "setExpandedState");
   pragma Import (Java, GetExpandedState, "getExpandedState");
   pragma Import (Java, TreeNodesChanged, "treeNodesChanged");
   pragma Import (Java, TreeNodesInserted, "treeNodesInserted");
   pragma Import (Java, TreeNodesRemoved, "treeNodesRemoved");
   pragma Import (Java, TreeStructureChanged, "treeStructureChanged");

end Javax.Swing.Tree.FixedHeightLayoutCache;
pragma Import (Java, Javax.Swing.Tree.FixedHeightLayoutCache, "javax.swing.tree.FixedHeightLayoutCache");
pragma Extensions_Allowed (Off);
