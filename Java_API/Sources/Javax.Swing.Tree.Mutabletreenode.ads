pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.Tree.TreeNode;

package Javax.Swing.Tree.MutableTreeNode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            TreeNode_I : Javax.Swing.Tree.TreeNode.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Insert (This : access Typ;
                     P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class;
                     P2_Int : Java.Int) is abstract;

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int) is abstract;

   procedure Remove (This : access Typ;
                     P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class) is abstract;

   procedure SetUserObject (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure RemoveFromParent (This : access Typ) is abstract;

   procedure SetParent (This : access Typ;
                        P1_MutableTreeNode : access Standard.Javax.Swing.Tree.MutableTreeNode.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Insert, "insert");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, SetUserObject, "setUserObject");
   pragma Export (Java, RemoveFromParent, "removeFromParent");
   pragma Export (Java, SetParent, "setParent");

end Javax.Swing.Tree.MutableTreeNode;
pragma Import (Java, Javax.Swing.Tree.MutableTreeNode, "javax.swing.tree.MutableTreeNode");
pragma Extensions_Allowed (Off);
