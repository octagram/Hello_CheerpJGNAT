pragma Extensions_Allowed (On);
limited with Java.Awt.Component;
limited with Javax.Swing.JTree;
with Java.Lang.Object;

package Javax.Swing.Tree.TreeCellRenderer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTreeCellRendererComponent (This : access Typ;
                                          P1_JTree : access Standard.Javax.Swing.JTree.Typ'Class;
                                          P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                          P3_Boolean : Java.Boolean;
                                          P4_Boolean : Java.Boolean;
                                          P5_Boolean : Java.Boolean;
                                          P6_Int : Java.Int;
                                          P7_Boolean : Java.Boolean)
                                          return access Java.Awt.Component.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTreeCellRendererComponent, "getTreeCellRendererComponent");

end Javax.Swing.Tree.TreeCellRenderer;
pragma Import (Java, Javax.Swing.Tree.TreeCellRenderer, "javax.swing.tree.TreeCellRenderer");
pragma Extensions_Allowed (Off);
