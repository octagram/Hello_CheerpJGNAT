pragma Extensions_Allowed (On);
limited with Javax.Swing.Event.TreeModelListener;
limited with Javax.Swing.Tree.TreePath;
with Java.Lang.Object;

package Javax.Swing.Tree.TreeModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRoot (This : access Typ)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function GetChild (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Int : Java.Int)
                      return access Java.Lang.Object.Typ'Class is abstract;

   function GetChildCount (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Int is abstract;

   function IsLeaf (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   procedure ValueForPathChanged (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                                  P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetIndexOfChild (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Int is abstract;

   procedure AddTreeModelListener (This : access Typ;
                                   P1_TreeModelListener : access Standard.Javax.Swing.Event.TreeModelListener.Typ'Class) is abstract;

   procedure RemoveTreeModelListener (This : access Typ;
                                      P1_TreeModelListener : access Standard.Javax.Swing.Event.TreeModelListener.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRoot, "getRoot");
   pragma Export (Java, GetChild, "getChild");
   pragma Export (Java, GetChildCount, "getChildCount");
   pragma Export (Java, IsLeaf, "isLeaf");
   pragma Export (Java, ValueForPathChanged, "valueForPathChanged");
   pragma Export (Java, GetIndexOfChild, "getIndexOfChild");
   pragma Export (Java, AddTreeModelListener, "addTreeModelListener");
   pragma Export (Java, RemoveTreeModelListener, "removeTreeModelListener");

end Javax.Swing.Tree.TreeModel;
pragma Import (Java, Javax.Swing.Tree.TreeModel, "javax.swing.tree.TreeModel");
pragma Extensions_Allowed (Off);
