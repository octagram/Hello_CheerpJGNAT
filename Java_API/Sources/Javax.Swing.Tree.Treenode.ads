pragma Extensions_Allowed (On);
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Javax.Swing.Tree.TreeNode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetChildAt (This : access Typ;
                        P1_Int : Java.Int)
                        return access Javax.Swing.Tree.TreeNode.Typ'Class is abstract;

   function GetChildCount (This : access Typ)
                           return Java.Int is abstract;

   function GetParent (This : access Typ)
                       return access Javax.Swing.Tree.TreeNode.Typ'Class is abstract;

   function GetIndex (This : access Typ;
                      P1_TreeNode : access Standard.Javax.Swing.Tree.TreeNode.Typ'Class)
                      return Java.Int is abstract;

   function GetAllowsChildren (This : access Typ)
                               return Java.Boolean is abstract;

   function IsLeaf (This : access Typ)
                    return Java.Boolean is abstract;

   function Children (This : access Typ)
                      return access Java.Util.Enumeration.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetChildAt, "getChildAt");
   pragma Export (Java, GetChildCount, "getChildCount");
   pragma Export (Java, GetParent, "getParent");
   pragma Export (Java, GetIndex, "getIndex");
   pragma Export (Java, GetAllowsChildren, "getAllowsChildren");
   pragma Export (Java, IsLeaf, "isLeaf");
   pragma Export (Java, Children, "children");

end Javax.Swing.Tree.TreeNode;
pragma Import (Java, Javax.Swing.Tree.TreeNode, "javax.swing.tree.TreeNode");
pragma Extensions_Allowed (Off);
