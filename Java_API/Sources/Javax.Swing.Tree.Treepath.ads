pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.Tree.TreePath is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TreePath (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                          This : Ref := null)
                          return Ref;

   function New_TreePath (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   --  protected
   function New_TreePath (P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                          This : Ref := null)
                          return Ref;

   --  protected
   function New_TreePath (P1_Object_Arr : access Java.Lang.Object.Arr_Obj;
                          P2_Int : Java.Int; 
                          This : Ref := null)
                          return Ref;

   --  protected
   function New_TreePath (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPath (This : access Typ)
                     return Standard.Java.Lang.Object.Ref;

   function GetLastPathComponent (This : access Typ)
                                  return access Java.Lang.Object.Typ'Class;

   function GetPathCount (This : access Typ)
                          return Java.Int;

   function GetPathComponent (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function IsDescendant (This : access Typ;
                          P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                          return Java.Boolean;

   function PathByAddingChild (This : access Typ;
                               P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                               return access Javax.Swing.Tree.TreePath.Typ'Class;

   function GetParentPath (This : access Typ)
                           return access Javax.Swing.Tree.TreePath.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TreePath);
   pragma Import (Java, GetPath, "getPath");
   pragma Import (Java, GetLastPathComponent, "getLastPathComponent");
   pragma Import (Java, GetPathCount, "getPathCount");
   pragma Import (Java, GetPathComponent, "getPathComponent");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, IsDescendant, "isDescendant");
   pragma Import (Java, PathByAddingChild, "pathByAddingChild");
   pragma Import (Java, GetParentPath, "getParentPath");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Tree.TreePath;
pragma Import (Java, Javax.Swing.Tree.TreePath, "javax.swing.tree.TreePath");
pragma Extensions_Allowed (Off);
