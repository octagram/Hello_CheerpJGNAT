pragma Extensions_Allowed (On);
limited with Java.Beans.PropertyChangeListener;
limited with Javax.Swing.Event.TreeSelectionListener;
limited with Javax.Swing.Tree.RowMapper;
limited with Javax.Swing.Tree.TreePath;
with Java.Lang.Object;

package Javax.Swing.Tree.TreeSelectionModel is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSelectionMode (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   function GetSelectionMode (This : access Typ)
                              return Java.Int is abstract;

   procedure SetSelectionPath (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class) is abstract;

   procedure SetSelectionPaths (This : access Typ;
                                P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj) is abstract;

   procedure AddSelectionPath (This : access Typ;
                               P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class) is abstract;

   procedure AddSelectionPaths (This : access Typ;
                                P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj) is abstract;

   procedure RemoveSelectionPath (This : access Typ;
                                  P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class) is abstract;

   procedure RemoveSelectionPaths (This : access Typ;
                                   P1_TreePath_Arr : access Javax.Swing.Tree.TreePath.Arr_Obj) is abstract;

   function GetSelectionPath (This : access Typ)
                              return access Javax.Swing.Tree.TreePath.Typ'Class is abstract;

   function GetSelectionPaths (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function GetSelectionCount (This : access Typ)
                               return Java.Int is abstract;

   function IsPathSelected (This : access Typ;
                            P1_TreePath : access Standard.Javax.Swing.Tree.TreePath.Typ'Class)
                            return Java.Boolean is abstract;

   function IsSelectionEmpty (This : access Typ)
                              return Java.Boolean is abstract;

   procedure ClearSelection (This : access Typ) is abstract;

   procedure SetRowMapper (This : access Typ;
                           P1_RowMapper : access Standard.Javax.Swing.Tree.RowMapper.Typ'Class) is abstract;

   function GetRowMapper (This : access Typ)
                          return access Javax.Swing.Tree.RowMapper.Typ'Class is abstract;

   function GetSelectionRows (This : access Typ)
                              return Java.Int_Arr is abstract;

   function GetMinSelectionRow (This : access Typ)
                                return Java.Int is abstract;

   function GetMaxSelectionRow (This : access Typ)
                                return Java.Int is abstract;

   function IsRowSelected (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean is abstract;

   procedure ResetRowSelection (This : access Typ) is abstract;

   function GetLeadSelectionRow (This : access Typ)
                                 return Java.Int is abstract;

   function GetLeadSelectionPath (This : access Typ)
                                  return access Javax.Swing.Tree.TreePath.Typ'Class is abstract;

   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class) is abstract;

   procedure AddTreeSelectionListener (This : access Typ;
                                       P1_TreeSelectionListener : access Standard.Javax.Swing.Event.TreeSelectionListener.Typ'Class) is abstract;

   procedure RemoveTreeSelectionListener (This : access Typ;
                                          P1_TreeSelectionListener : access Standard.Javax.Swing.Event.TreeSelectionListener.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SINGLE_TREE_SELECTION : constant Java.Int;

   --  final
   CONTIGUOUS_TREE_SELECTION : constant Java.Int;

   --  final
   DISCONTIGUOUS_TREE_SELECTION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetSelectionMode, "setSelectionMode");
   pragma Export (Java, GetSelectionMode, "getSelectionMode");
   pragma Export (Java, SetSelectionPath, "setSelectionPath");
   pragma Export (Java, SetSelectionPaths, "setSelectionPaths");
   pragma Export (Java, AddSelectionPath, "addSelectionPath");
   pragma Export (Java, AddSelectionPaths, "addSelectionPaths");
   pragma Export (Java, RemoveSelectionPath, "removeSelectionPath");
   pragma Export (Java, RemoveSelectionPaths, "removeSelectionPaths");
   pragma Export (Java, GetSelectionPath, "getSelectionPath");
   pragma Export (Java, GetSelectionPaths, "getSelectionPaths");
   pragma Export (Java, GetSelectionCount, "getSelectionCount");
   pragma Export (Java, IsPathSelected, "isPathSelected");
   pragma Export (Java, IsSelectionEmpty, "isSelectionEmpty");
   pragma Export (Java, ClearSelection, "clearSelection");
   pragma Export (Java, SetRowMapper, "setRowMapper");
   pragma Export (Java, GetRowMapper, "getRowMapper");
   pragma Export (Java, GetSelectionRows, "getSelectionRows");
   pragma Export (Java, GetMinSelectionRow, "getMinSelectionRow");
   pragma Export (Java, GetMaxSelectionRow, "getMaxSelectionRow");
   pragma Export (Java, IsRowSelected, "isRowSelected");
   pragma Export (Java, ResetRowSelection, "resetRowSelection");
   pragma Export (Java, GetLeadSelectionRow, "getLeadSelectionRow");
   pragma Export (Java, GetLeadSelectionPath, "getLeadSelectionPath");
   pragma Export (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Export (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Export (Java, AddTreeSelectionListener, "addTreeSelectionListener");
   pragma Export (Java, RemoveTreeSelectionListener, "removeTreeSelectionListener");
   pragma Import (Java, SINGLE_TREE_SELECTION, "SINGLE_TREE_SELECTION");
   pragma Import (Java, CONTIGUOUS_TREE_SELECTION, "CONTIGUOUS_TREE_SELECTION");
   pragma Import (Java, DISCONTIGUOUS_TREE_SELECTION, "DISCONTIGUOUS_TREE_SELECTION");

end Javax.Swing.Tree.TreeSelectionModel;
pragma Import (Java, Javax.Swing.Tree.TreeSelectionModel, "javax.swing.tree.TreeSelectionModel");
pragma Extensions_Allowed (Off);
