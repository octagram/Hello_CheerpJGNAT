pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Swing.UIDefaults.LazyValue;

package Javax.Swing.UIDefaults.LazyInputMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LazyValue_I : Javax.Swing.UIDefaults.LazyValue.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LazyInputMap (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateValue (This : access Typ;
                         P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LazyInputMap);
   pragma Import (Java, CreateValue, "createValue");

end Javax.Swing.UIDefaults.LazyInputMap;
pragma Import (Java, Javax.Swing.UIDefaults.LazyInputMap, "javax.swing.UIDefaults$LazyInputMap");
pragma Extensions_Allowed (Off);
