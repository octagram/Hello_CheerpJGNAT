pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Swing.UIDefaults.LazyValue;

package Javax.Swing.UIDefaults.ProxyLazyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(LazyValue_I : Javax.Swing.UIDefaults.LazyValue.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ProxyLazyValue (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_ProxyLazyValue (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_ProxyLazyValue (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   function New_ProxyLazyValue (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateValue (This : access Typ;
                         P1_UIDefaults : access Standard.Javax.Swing.UIDefaults.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ProxyLazyValue);
   pragma Import (Java, CreateValue, "createValue");

end Javax.Swing.UIDefaults.ProxyLazyValue;
pragma Import (Java, Javax.Swing.UIDefaults.ProxyLazyValue, "javax.swing.UIDefaults$ProxyLazyValue");
pragma Extensions_Allowed (Off);
