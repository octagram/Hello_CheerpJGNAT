pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Java.Awt.Insets;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.Plaf.ComponentUI;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Hashtable;
with Java.Util.Map;

package Javax.Swing.UIDefaults is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Java.Util.Hashtable.Typ(Serializable_I,
                                   Cloneable_I,
                                   Map_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UIDefaults (This : Ref := null)
                            return Ref;

   function New_UIDefaults (P1_Int : Java.Int;
                            P2_Float : Java.Float; 
                            This : Ref := null)
                            return Ref;

   function New_UIDefaults (P1_Object_Arr : access Java.Lang.Object.Arr_Obj; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure PutDefaults (This : access Typ;
                          P1_Object_Arr : access Java.Lang.Object.Arr_Obj);

   function GetFont (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function GetFont (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function GetColor (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Awt.Color.Typ'Class;

   function GetColor (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                      return access Java.Awt.Color.Typ'Class;

   function GetIcon (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   function GetIcon (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   function GetBorder (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetBorder (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetString (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function GetString (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function GetInt (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetInt (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                    return Java.Int;

   function GetBoolean (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean;

   function GetBoolean (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                        return Java.Boolean;

   function GetInsets (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   function GetInsets (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   function GetDimension (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Awt.Dimension.Typ'Class;

   function GetDimension (This : access Typ;
                          P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                          return access Java.Awt.Dimension.Typ'Class;

   function GetUIClass (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                        return access Java.Lang.Class.Typ'Class;

   function GetUIClass (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Class.Typ'Class;

   --  protected
   procedure GetUIError (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetUI (This : access Typ;
                   P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                   return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   --  synchronized
   procedure AddPropertyChangeListener (This : access Typ;
                                        P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   procedure RemovePropertyChangeListener (This : access Typ;
                                           P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   --  synchronized
   function GetPropertyChangeListeners (This : access Typ)
                                        return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure FirePropertyChange (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                                 P3_Object : access Standard.Java.Lang.Object.Typ'Class);

   --  synchronized
   procedure AddResourceBundle (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  synchronized
   procedure RemoveResourceBundle (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetDefaultLocale (This : access Typ;
                               P1_Locale : access Standard.Java.Util.Locale.Typ'Class);

   function GetDefaultLocale (This : access Typ)
                              return access Java.Util.Locale.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UIDefaults);
   pragma Import (Java, Get, "get");
   pragma Import (Java, Put, "put");
   pragma Import (Java, PutDefaults, "putDefaults");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, GetColor, "getColor");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, GetBorder, "getBorder");
   pragma Import (Java, GetString, "getString");
   pragma Import (Java, GetInt, "getInt");
   pragma Import (Java, GetBoolean, "getBoolean");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, GetDimension, "getDimension");
   pragma Import (Java, GetUIClass, "getUIClass");
   pragma Import (Java, GetUIError, "getUIError");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");
   pragma Import (Java, FirePropertyChange, "firePropertyChange");
   pragma Import (Java, AddResourceBundle, "addResourceBundle");
   pragma Import (Java, RemoveResourceBundle, "removeResourceBundle");
   pragma Import (Java, SetDefaultLocale, "setDefaultLocale");
   pragma Import (Java, GetDefaultLocale, "getDefaultLocale");

end Javax.Swing.UIDefaults;
pragma Import (Java, Javax.Swing.UIDefaults, "javax.swing.UIDefaults");
pragma Extensions_Allowed (Off);
