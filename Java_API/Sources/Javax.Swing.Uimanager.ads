pragma Extensions_Allowed (On);
limited with Java.Awt.Color;
limited with Java.Awt.Dimension;
limited with Java.Awt.Font;
limited with Java.Awt.Insets;
limited with Java.Beans.PropertyChangeListener;
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Swing.Border.Border;
limited with Javax.Swing.Icon;
limited with Javax.Swing.JComponent;
limited with Javax.Swing.LookAndFeel;
limited with Javax.Swing.Plaf.ComponentUI;
limited with Javax.Swing.UIDefaults;
limited with Javax.Swing.UIManager.LookAndFeelInfo;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Swing.UIManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UIManager (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstalledLookAndFeels return Standard.Java.Lang.Object.Ref;

   procedure SetInstalledLookAndFeels (P1_LookAndFeelInfo_Arr : access Javax.Swing.UIManager.LookAndFeelInfo.Arr_Obj);
   --  can raise Java.Lang.SecurityException.Except

   procedure InstallLookAndFeel (P1_LookAndFeelInfo : access Standard.Javax.Swing.UIManager.LookAndFeelInfo.Typ'Class);

   procedure InstallLookAndFeel (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class);

   function GetLookAndFeel return access Javax.Swing.LookAndFeel.Typ'Class;

   procedure SetLookAndFeel (P1_LookAndFeel : access Standard.Javax.Swing.LookAndFeel.Typ'Class);
   --  can raise Javax.Swing.UnsupportedLookAndFeelException.Except

   procedure SetLookAndFeel (P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Lang.ClassNotFoundException.Except,
   --  Java.Lang.InstantiationException.Except,
   --  Java.Lang.IllegalAccessException.Except and
   --  Javax.Swing.UnsupportedLookAndFeelException.Except

   function GetSystemLookAndFeelClassName return access Java.Lang.String.Typ'Class;

   function GetCrossPlatformLookAndFeelClassName return access Java.Lang.String.Typ'Class;

   function GetDefaults return access Javax.Swing.UIDefaults.Typ'Class;

   function GetFont (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function GetFont (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                     return access Java.Awt.Font.Typ'Class;

   function GetColor (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Awt.Color.Typ'Class;

   function GetColor (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                      return access Java.Awt.Color.Typ'Class;

   function GetIcon (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   function GetIcon (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                     return access Javax.Swing.Icon.Typ'Class;

   function GetBorder (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetBorder (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Javax.Swing.Border.Border.Typ'Class;

   function GetString (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function GetString (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function GetInt (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Int;

   function GetInt (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                    P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                    return Java.Int;

   function GetBoolean (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return Java.Boolean;

   function GetBoolean (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                        P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                        return Java.Boolean;

   function GetInsets (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   function GetInsets (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                       return access Java.Awt.Insets.Typ'Class;

   function GetDimension (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                          return access Java.Awt.Dimension.Typ'Class;

   function GetDimension (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                          P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                          return access Java.Awt.Dimension.Typ'Class;

   function Get (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Get (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Locale : access Standard.Java.Util.Locale.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Put (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function GetUI (P1_JComponent : access Standard.Javax.Swing.JComponent.Typ'Class)
                   return access Javax.Swing.Plaf.ComponentUI.Typ'Class;

   function GetLookAndFeelDefaults return access Javax.Swing.UIDefaults.Typ'Class;

   procedure AddAuxiliaryLookAndFeel (P1_LookAndFeel : access Standard.Javax.Swing.LookAndFeel.Typ'Class);

   function RemoveAuxiliaryLookAndFeel (P1_LookAndFeel : access Standard.Javax.Swing.LookAndFeel.Typ'Class)
                                        return Java.Boolean;

   function GetAuxiliaryLookAndFeels return Standard.Java.Lang.Object.Ref;

   procedure AddPropertyChangeListener (P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   procedure RemovePropertyChangeListener (P1_PropertyChangeListener : access Standard.Java.Beans.PropertyChangeListener.Typ'Class);

   function GetPropertyChangeListeners return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UIManager);
   pragma Import (Java, GetInstalledLookAndFeels, "getInstalledLookAndFeels");
   pragma Import (Java, SetInstalledLookAndFeels, "setInstalledLookAndFeels");
   pragma Import (Java, InstallLookAndFeel, "installLookAndFeel");
   pragma Import (Java, GetLookAndFeel, "getLookAndFeel");
   pragma Import (Java, SetLookAndFeel, "setLookAndFeel");
   pragma Import (Java, GetSystemLookAndFeelClassName, "getSystemLookAndFeelClassName");
   pragma Import (Java, GetCrossPlatformLookAndFeelClassName, "getCrossPlatformLookAndFeelClassName");
   pragma Import (Java, GetDefaults, "getDefaults");
   pragma Import (Java, GetFont, "getFont");
   pragma Import (Java, GetColor, "getColor");
   pragma Import (Java, GetIcon, "getIcon");
   pragma Import (Java, GetBorder, "getBorder");
   pragma Import (Java, GetString, "getString");
   pragma Import (Java, GetInt, "getInt");
   pragma Import (Java, GetBoolean, "getBoolean");
   pragma Import (Java, GetInsets, "getInsets");
   pragma Import (Java, GetDimension, "getDimension");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Put, "put");
   pragma Import (Java, GetUI, "getUI");
   pragma Import (Java, GetLookAndFeelDefaults, "getLookAndFeelDefaults");
   pragma Import (Java, AddAuxiliaryLookAndFeel, "addAuxiliaryLookAndFeel");
   pragma Import (Java, RemoveAuxiliaryLookAndFeel, "removeAuxiliaryLookAndFeel");
   pragma Import (Java, GetAuxiliaryLookAndFeels, "getAuxiliaryLookAndFeels");
   pragma Import (Java, AddPropertyChangeListener, "addPropertyChangeListener");
   pragma Import (Java, RemovePropertyChangeListener, "removePropertyChangeListener");
   pragma Import (Java, GetPropertyChangeListeners, "getPropertyChangeListeners");

end Javax.Swing.UIManager;
pragma Import (Java, Javax.Swing.UIManager, "javax.swing.UIManager");
pragma Extensions_Allowed (Off);
