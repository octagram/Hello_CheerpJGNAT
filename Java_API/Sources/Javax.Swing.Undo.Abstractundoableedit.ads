pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Undo.UndoableEdit;

package Javax.Swing.Undo.AbstractUndoableEdit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UndoableEdit_I : Javax.Swing.Undo.UndoableEdit.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AbstractUndoableEdit (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Die (This : access Typ);

   procedure Undo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotUndoException.Except

   function CanUndo (This : access Typ)
                     return Java.Boolean;

   procedure Redo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   function CanRedo (This : access Typ)
                     return Java.Boolean;

   function AddEdit (This : access Typ;
                     P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class)
                     return Java.Boolean;

   function ReplaceEdit (This : access Typ;
                         P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class)
                         return Java.Boolean;

   function IsSignificant (This : access Typ)
                           return Java.Boolean;

   function GetPresentationName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetUndoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetRedoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   UndoName : constant access Java.Lang.String.Typ'Class;

   --  protected  final
   RedoName : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractUndoableEdit);
   pragma Import (Java, Die, "die");
   pragma Import (Java, Undo, "undo");
   pragma Import (Java, CanUndo, "canUndo");
   pragma Import (Java, Redo, "redo");
   pragma Import (Java, CanRedo, "canRedo");
   pragma Import (Java, AddEdit, "addEdit");
   pragma Import (Java, ReplaceEdit, "replaceEdit");
   pragma Import (Java, IsSignificant, "isSignificant");
   pragma Import (Java, GetPresentationName, "getPresentationName");
   pragma Import (Java, GetUndoPresentationName, "getUndoPresentationName");
   pragma Import (Java, GetRedoPresentationName, "getRedoPresentationName");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, UndoName, "UndoName");
   pragma Import (Java, RedoName, "RedoName");

end Javax.Swing.Undo.AbstractUndoableEdit;
pragma Import (Java, Javax.Swing.Undo.AbstractUndoableEdit, "javax.swing.undo.AbstractUndoableEdit");
pragma Extensions_Allowed (Off);
