pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Undo.AbstractUndoableEdit;
with Javax.Swing.Undo.UndoableEdit;

package Javax.Swing.Undo.CompoundEdit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UndoableEdit_I : Javax.Swing.Undo.UndoableEdit.Ref)
    is new Javax.Swing.Undo.AbstractUndoableEdit.Typ(Serializable_I,
                                                     UndoableEdit_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Edits : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Edits, "edits");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CompoundEdit (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Undo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotUndoException.Except

   procedure Redo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   --  protected
   function LastEdit (This : access Typ)
                      return access Javax.Swing.Undo.UndoableEdit.Typ'Class;

   procedure Die (This : access Typ);

   function AddEdit (This : access Typ;
                     P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class)
                     return Java.Boolean;

   procedure end_K (This : access Typ);

   function CanUndo (This : access Typ)
                     return Java.Boolean;

   function CanRedo (This : access Typ)
                     return Java.Boolean;

   function IsInProgress (This : access Typ)
                          return Java.Boolean;

   function IsSignificant (This : access Typ)
                           return Java.Boolean;

   function GetPresentationName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   function GetUndoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function GetRedoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_CompoundEdit);
   pragma Import (Java, Undo, "undo");
   pragma Import (Java, Redo, "redo");
   pragma Import (Java, LastEdit, "lastEdit");
   pragma Import (Java, Die, "die");
   pragma Import (Java, AddEdit, "addEdit");
   pragma Import (Java, end_K, "end");
   pragma Import (Java, CanUndo, "canUndo");
   pragma Import (Java, CanRedo, "canRedo");
   pragma Import (Java, IsInProgress, "isInProgress");
   pragma Import (Java, IsSignificant, "isSignificant");
   pragma Import (Java, GetPresentationName, "getPresentationName");
   pragma Import (Java, GetUndoPresentationName, "getUndoPresentationName");
   pragma Import (Java, GetRedoPresentationName, "getRedoPresentationName");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Undo.CompoundEdit;
pragma Import (Java, Javax.Swing.Undo.CompoundEdit, "javax.swing.undo.CompoundEdit");
pragma Extensions_Allowed (Off);
