pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
limited with Javax.Swing.Undo.StateEditable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Undo.AbstractUndoableEdit;
with Javax.Swing.Undo.UndoableEdit;

package Javax.Swing.Undo.StateEdit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UndoableEdit_I : Javax.Swing.Undo.UndoableEdit.Ref)
    is new Javax.Swing.Undo.AbstractUndoableEdit.Typ(Serializable_I,
                                                     UndoableEdit_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Object : access Javax.Swing.Undo.StateEditable.Typ'Class;
      pragma Import (Java, Object, "object");

      --  protected
      PreState : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, PreState, "preState");

      --  protected
      PostState : access Java.Util.Hashtable.Typ'Class;
      pragma Import (Java, PostState, "postState");

      --  protected
      UndoRedoName : access Java.Lang.String.Typ'Class;
      pragma Import (Java, UndoRedoName, "undoRedoName");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StateEdit (P1_StateEditable : access Standard.Javax.Swing.Undo.StateEditable.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_StateEdit (P1_StateEditable : access Standard.Javax.Swing.Undo.StateEditable.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   procedure Init (This : access Typ;
                   P1_StateEditable : access Standard.Javax.Swing.Undo.StateEditable.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure end_K (This : access Typ);

   procedure Undo (This : access Typ);

   procedure Redo (This : access Typ);

   function GetPresentationName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   --  protected
   procedure RemoveRedundantState (This : access Typ);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   RCSID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StateEdit);
   pragma Import (Java, Init, "init");
   pragma Import (Java, end_K, "end");
   pragma Import (Java, Undo, "undo");
   pragma Import (Java, Redo, "redo");
   pragma Import (Java, GetPresentationName, "getPresentationName");
   pragma Import (Java, RemoveRedundantState, "removeRedundantState");
   pragma Import (Java, RCSID, "RCSID");

end Javax.Swing.Undo.StateEdit;
pragma Import (Java, Javax.Swing.Undo.StateEdit, "javax.swing.undo.StateEdit");
pragma Extensions_Allowed (Off);
