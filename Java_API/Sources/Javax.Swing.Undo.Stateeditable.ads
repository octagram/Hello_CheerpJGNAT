pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Hashtable;
with Java.Lang.Object;

package Javax.Swing.Undo.StateEditable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure StoreState (This : access Typ;
                         P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class) is abstract;

   procedure RestoreState (This : access Typ;
                           P1_Hashtable : access Standard.Java.Util.Hashtable.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   RCSID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, StoreState, "storeState");
   pragma Export (Java, RestoreState, "restoreState");
   pragma Import (Java, RCSID, "RCSID");

end Javax.Swing.Undo.StateEditable;
pragma Import (Java, Javax.Swing.Undo.StateEditable, "javax.swing.undo.StateEditable");
pragma Extensions_Allowed (Off);
