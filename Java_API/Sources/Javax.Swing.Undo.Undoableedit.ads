pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Swing.Undo.UndoableEdit is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Undo (This : access Typ) is abstract;
   --  can raise Javax.Swing.Undo.CannotUndoException.Except

   function CanUndo (This : access Typ)
                     return Java.Boolean is abstract;

   procedure Redo (This : access Typ) is abstract;
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   function CanRedo (This : access Typ)
                     return Java.Boolean is abstract;

   procedure Die (This : access Typ) is abstract;

   function AddEdit (This : access Typ;
                     P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class)
                     return Java.Boolean is abstract;

   function ReplaceEdit (This : access Typ;
                         P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class)
                         return Java.Boolean is abstract;

   function IsSignificant (This : access Typ)
                           return Java.Boolean is abstract;

   function GetPresentationName (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   function GetUndoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class is abstract;

   function GetRedoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Undo, "undo");
   pragma Export (Java, CanUndo, "canUndo");
   pragma Export (Java, Redo, "redo");
   pragma Export (Java, CanRedo, "canRedo");
   pragma Export (Java, Die, "die");
   pragma Export (Java, AddEdit, "addEdit");
   pragma Export (Java, ReplaceEdit, "replaceEdit");
   pragma Export (Java, IsSignificant, "isSignificant");
   pragma Export (Java, GetPresentationName, "getPresentationName");
   pragma Export (Java, GetUndoPresentationName, "getUndoPresentationName");
   pragma Export (Java, GetRedoPresentationName, "getRedoPresentationName");

end Javax.Swing.Undo.UndoableEdit;
pragma Import (Java, Javax.Swing.Undo.UndoableEdit, "javax.swing.undo.UndoableEdit");
pragma Extensions_Allowed (Off);
