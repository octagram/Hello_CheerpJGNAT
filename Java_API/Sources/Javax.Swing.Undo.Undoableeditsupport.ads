pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Vector;
limited with Javax.Swing.Event.UndoableEditListener;
limited with Javax.Swing.Undo.CompoundEdit;
limited with Javax.Swing.Undo.UndoableEdit;
with Java.Lang.Object;

package Javax.Swing.Undo.UndoableEditSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      UpdateLevel : Java.Int;
      pragma Import (Java, UpdateLevel, "updateLevel");

      --  protected
      CompoundEdit : access Javax.Swing.Undo.CompoundEdit.Typ'Class;
      pragma Import (Java, CompoundEdit, "compoundEdit");

      --  protected
      Listeners : access Java.Util.Vector.Typ'Class;
      pragma Import (Java, Listeners, "listeners");

      --  protected
      RealSource : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, RealSource, "realSource");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UndoableEditSupport (This : Ref := null)
                                     return Ref;

   function New_UndoableEditSupport (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   procedure AddUndoableEditListener (This : access Typ;
                                      P1_UndoableEditListener : access Standard.Javax.Swing.Event.UndoableEditListener.Typ'Class);

   --  synchronized
   procedure RemoveUndoableEditListener (This : access Typ;
                                         P1_UndoableEditListener : access Standard.Javax.Swing.Event.UndoableEditListener.Typ'Class);

   --  synchronized
   function GetUndoableEditListeners (This : access Typ)
                                      return Standard.Java.Lang.Object.Ref;

   --  protected
   procedure U_PostEdit (This : access Typ;
                         P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class);

   --  synchronized
   procedure PostEdit (This : access Typ;
                       P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class);

   function GetUpdateLevel (This : access Typ)
                            return Java.Int;

   --  synchronized
   procedure BeginUpdate (This : access Typ);

   --  protected
   function CreateCompoundEdit (This : access Typ)
                                return access Javax.Swing.Undo.CompoundEdit.Typ'Class;

   --  synchronized
   procedure EndUpdate (This : access Typ);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UndoableEditSupport);
   pragma Import (Java, AddUndoableEditListener, "addUndoableEditListener");
   pragma Import (Java, RemoveUndoableEditListener, "removeUndoableEditListener");
   pragma Import (Java, GetUndoableEditListeners, "getUndoableEditListeners");
   pragma Import (Java, U_PostEdit, "_postEdit");
   pragma Import (Java, PostEdit, "postEdit");
   pragma Import (Java, GetUpdateLevel, "getUpdateLevel");
   pragma Import (Java, BeginUpdate, "beginUpdate");
   pragma Import (Java, CreateCompoundEdit, "createCompoundEdit");
   pragma Import (Java, EndUpdate, "endUpdate");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Undo.UndoableEditSupport;
pragma Import (Java, Javax.Swing.Undo.UndoableEditSupport, "javax.swing.undo.UndoableEditSupport");
pragma Extensions_Allowed (Off);
