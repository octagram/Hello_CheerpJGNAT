pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Swing.Event.UndoableEditEvent;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Swing.Event.UndoableEditListener;
with Javax.Swing.Undo.CompoundEdit;
with Javax.Swing.Undo.UndoableEdit;

package Javax.Swing.Undo.UndoManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            UndoableEditListener_I : Javax.Swing.Event.UndoableEditListener.Ref;
            UndoableEdit_I : Javax.Swing.Undo.UndoableEdit.Ref)
    is new Javax.Swing.Undo.CompoundEdit.Typ(Serializable_I,
                                             UndoableEdit_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UndoManager (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function GetLimit (This : access Typ)
                      return Java.Int;

   --  synchronized
   procedure DiscardAllEdits (This : access Typ);

   --  protected
   procedure TrimForLimit (This : access Typ);

   --  protected
   procedure TrimEdits (This : access Typ;
                        P1_Int : Java.Int;
                        P2_Int : Java.Int);

   --  synchronized
   procedure SetLimit (This : access Typ;
                       P1_Int : Java.Int);

   --  protected
   function EditToBeUndone (This : access Typ)
                            return access Javax.Swing.Undo.UndoableEdit.Typ'Class;

   --  protected
   function EditToBeRedone (This : access Typ)
                            return access Javax.Swing.Undo.UndoableEdit.Typ'Class;

   --  protected
   procedure UndoTo (This : access Typ;
                     P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class);
   --  can raise Javax.Swing.Undo.CannotUndoException.Except

   --  protected
   procedure RedoTo (This : access Typ;
                     P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   --  synchronized
   procedure UndoOrRedo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except and
   --  Javax.Swing.Undo.CannotUndoException.Except

   --  synchronized
   function CanUndoOrRedo (This : access Typ)
                           return Java.Boolean;

   --  synchronized
   procedure Undo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotUndoException.Except

   --  synchronized
   function CanUndo (This : access Typ)
                     return Java.Boolean;

   --  synchronized
   procedure Redo (This : access Typ);
   --  can raise Javax.Swing.Undo.CannotRedoException.Except

   --  synchronized
   function CanRedo (This : access Typ)
                     return Java.Boolean;

   --  synchronized
   function AddEdit (This : access Typ;
                     P1_UndoableEdit : access Standard.Javax.Swing.Undo.UndoableEdit.Typ'Class)
                     return Java.Boolean;

   --  synchronized
   procedure end_K (This : access Typ);

   --  synchronized
   function GetUndoOrRedoPresentationName (This : access Typ)
                                           return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetUndoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   --  synchronized
   function GetRedoPresentationName (This : access Typ)
                                     return access Java.Lang.String.Typ'Class;

   procedure UndoableEditHappened (This : access Typ;
                                   P1_UndoableEditEvent : access Standard.Javax.Swing.Event.UndoableEditEvent.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UndoManager);
   pragma Import (Java, GetLimit, "getLimit");
   pragma Import (Java, DiscardAllEdits, "discardAllEdits");
   pragma Import (Java, TrimForLimit, "trimForLimit");
   pragma Import (Java, TrimEdits, "trimEdits");
   pragma Import (Java, SetLimit, "setLimit");
   pragma Import (Java, EditToBeUndone, "editToBeUndone");
   pragma Import (Java, EditToBeRedone, "editToBeRedone");
   pragma Import (Java, UndoTo, "undoTo");
   pragma Import (Java, RedoTo, "redoTo");
   pragma Import (Java, UndoOrRedo, "undoOrRedo");
   pragma Import (Java, CanUndoOrRedo, "canUndoOrRedo");
   pragma Import (Java, Undo, "undo");
   pragma Import (Java, CanUndo, "canUndo");
   pragma Import (Java, Redo, "redo");
   pragma Import (Java, CanRedo, "canRedo");
   pragma Import (Java, AddEdit, "addEdit");
   pragma Import (Java, end_K, "end");
   pragma Import (Java, GetUndoOrRedoPresentationName, "getUndoOrRedoPresentationName");
   pragma Import (Java, GetUndoPresentationName, "getUndoPresentationName");
   pragma Import (Java, GetRedoPresentationName, "getRedoPresentationName");
   pragma Import (Java, UndoableEditHappened, "undoableEditHappened");
   pragma Import (Java, ToString, "toString");

end Javax.Swing.Undo.UndoManager;
pragma Import (Java, Javax.Swing.Undo.UndoManager, "javax.swing.undo.UndoManager");
pragma Extensions_Allowed (Off);
