pragma Extensions_Allowed (On);
package Javax.Swing.Undo is
   pragma Preelaborate;
end Javax.Swing.Undo;
pragma Import (Java, Javax.Swing.Undo, "javax.swing.undo");
pragma Extensions_Allowed (Off);
