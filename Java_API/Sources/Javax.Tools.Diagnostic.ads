pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Tools.Diagnostic.Kind;
with Java.Lang.Object;

package Javax.Tools.Diagnostic is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKind (This : access Typ)
                     return access Javax.Tools.Diagnostic.Kind.Typ'Class is abstract;

   function GetSource (This : access Typ)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function GetPosition (This : access Typ)
                         return Java.Long is abstract;

   function GetStartPosition (This : access Typ)
                              return Java.Long is abstract;

   function GetEndPosition (This : access Typ)
                            return Java.Long is abstract;

   function GetLineNumber (This : access Typ)
                           return Java.Long is abstract;

   function GetColumnNumber (This : access Typ)
                             return Java.Long is abstract;

   function GetCode (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetMessage (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                        return access Java.Lang.String.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NOPOS : constant Java.Long;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetKind, "getKind");
   pragma Export (Java, GetSource, "getSource");
   pragma Export (Java, GetPosition, "getPosition");
   pragma Export (Java, GetStartPosition, "getStartPosition");
   pragma Export (Java, GetEndPosition, "getEndPosition");
   pragma Export (Java, GetLineNumber, "getLineNumber");
   pragma Export (Java, GetColumnNumber, "getColumnNumber");
   pragma Export (Java, GetCode, "getCode");
   pragma Export (Java, GetMessage, "getMessage");
   pragma Import (Java, NOPOS, "NOPOS");

end Javax.Tools.Diagnostic;
pragma Import (Java, Javax.Tools.Diagnostic, "javax.tools.Diagnostic");
pragma Extensions_Allowed (Off);
