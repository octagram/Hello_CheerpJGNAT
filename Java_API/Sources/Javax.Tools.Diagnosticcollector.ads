pragma Extensions_Allowed (On);
limited with Java.Util.List;
limited with Javax.Tools.Diagnostic;
with Java.Lang.Object;
with Javax.Tools.DiagnosticListener;

package Javax.Tools.DiagnosticCollector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DiagnosticListener_I : Javax.Tools.DiagnosticListener.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DiagnosticCollector (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Report (This : access Typ;
                     P1_Diagnostic : access Standard.Javax.Tools.Diagnostic.Typ'Class);

   function GetDiagnostics (This : access Typ)
                            return access Java.Util.List.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DiagnosticCollector);
   pragma Import (Java, Report, "report");
   pragma Import (Java, GetDiagnostics, "getDiagnostics");

end Javax.Tools.DiagnosticCollector;
pragma Import (Java, Javax.Tools.DiagnosticCollector, "javax.tools.DiagnosticCollector");
pragma Extensions_Allowed (Off);
