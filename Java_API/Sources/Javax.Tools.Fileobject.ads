pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Net.URI;
with Java.Lang.Object;

package Javax.Tools.FileObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToUri (This : access Typ)
                   return access Java.Net.URI.Typ'Class is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function OpenInputStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function OpenOutputStream (This : access Typ)
                              return access Java.Io.OutputStream.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function OpenReader (This : access Typ;
                        P1_Boolean : Java.Boolean)
                        return access Java.Io.Reader.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetCharContent (This : access Typ;
                            P1_Boolean : Java.Boolean)
                            return access Java.Lang.CharSequence.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function OpenWriter (This : access Typ)
                        return access Java.Io.Writer.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetLastModified (This : access Typ)
                             return Java.Long is abstract;

   function Delete (This : access Typ)
                    return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ToUri, "toUri");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, OpenInputStream, "openInputStream");
   pragma Export (Java, OpenOutputStream, "openOutputStream");
   pragma Export (Java, OpenReader, "openReader");
   pragma Export (Java, GetCharContent, "getCharContent");
   pragma Export (Java, OpenWriter, "openWriter");
   pragma Export (Java, GetLastModified, "getLastModified");
   pragma Export (Java, Delete, "delete");

end Javax.Tools.FileObject;
pragma Import (Java, Javax.Tools.FileObject, "javax.tools.FileObject");
pragma Extensions_Allowed (Off);
