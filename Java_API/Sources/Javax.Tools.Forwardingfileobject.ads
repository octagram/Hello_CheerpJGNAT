pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Net.URI;
with Java.Lang.Object;
with Javax.Tools.FileObject;

package Javax.Tools.ForwardingFileObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(FileObject_I : Javax.Tools.FileObject.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      FileObject : access Javax.Tools.FileObject.Typ'Class;
      pragma Import (Java, FileObject, "fileObject");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ForwardingFileObject (P1_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToUri (This : access Typ)
                   return access Java.Net.URI.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function OpenInputStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenOutputStream (This : access Typ)
                              return access Java.Io.OutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenReader (This : access Typ;
                        P1_Boolean : Java.Boolean)
                        return access Java.Io.Reader.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetCharContent (This : access Typ;
                            P1_Boolean : Java.Boolean)
                            return access Java.Lang.CharSequence.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenWriter (This : access Typ)
                        return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetLastModified (This : access Typ)
                             return Java.Long;

   function Delete (This : access Typ)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ForwardingFileObject);
   pragma Import (Java, ToUri, "toUri");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, OpenInputStream, "openInputStream");
   pragma Import (Java, OpenOutputStream, "openOutputStream");
   pragma Import (Java, OpenReader, "openReader");
   pragma Import (Java, GetCharContent, "getCharContent");
   pragma Import (Java, OpenWriter, "openWriter");
   pragma Import (Java, GetLastModified, "getLastModified");
   pragma Import (Java, Delete, "delete");

end Javax.Tools.ForwardingFileObject;
pragma Import (Java, Javax.Tools.ForwardingFileObject, "javax.tools.ForwardingFileObject");
pragma Extensions_Allowed (Off);
