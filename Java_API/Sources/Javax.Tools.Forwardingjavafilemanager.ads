pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.Iterable;
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Java.Util.Set;
limited with Javax.Tools.FileObject;
limited with Javax.Tools.JavaFileManager.Location;
limited with Javax.Tools.JavaFileObject;
limited with Javax.Tools.JavaFileObject.Kind;
with Java.Lang.Object;
with Javax.Tools.JavaFileManager;

package Javax.Tools.ForwardingJavaFileManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(JavaFileManager_I : Javax.Tools.JavaFileManager.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      FileManager : access Javax.Tools.JavaFileManager.Typ'Class;
      pragma Import (Java, FileManager, "fileManager");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ForwardingJavaFileManager (P1_JavaFileManager : access Standard.Javax.Tools.JavaFileManager.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassLoader (This : access Typ;
                            P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class)
                            return access Java.Lang.ClassLoader.Typ'Class;

   function List (This : access Typ;
                  P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                  P3_Set : access Standard.Java.Util.Set.Typ'Class;
                  P4_Boolean : Java.Boolean)
                  return access Java.Lang.Iterable.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function InferBinaryName (This : access Typ;
                             P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                             P2_JavaFileObject : access Standard.Javax.Tools.JavaFileObject.Typ'Class)
                             return access Java.Lang.String.Typ'Class;

   function IsSameFile (This : access Typ;
                        P1_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class;
                        P2_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class)
                        return Java.Boolean;

   function HandleOption (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                          return Java.Boolean;

   function HasLocation (This : access Typ;
                         P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class)
                         return Java.Boolean;

   function IsSupportedOption (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return Java.Int;

   function GetJavaFileForInput (This : access Typ;
                                 P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class)
                                 return access Javax.Tools.JavaFileObject.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetJavaFileForOutput (This : access Typ;
                                  P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class;
                                  P4_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class)
                                  return access Javax.Tools.JavaFileObject.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetFileForInput (This : access Typ;
                             P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Tools.FileObject.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetFileForOutput (This : access Typ;
                              P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class)
                              return access Javax.Tools.FileObject.Typ'Class;
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ);
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ);
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ForwardingJavaFileManager);
   pragma Import (Java, GetClassLoader, "getClassLoader");
   pragma Import (Java, List, "list");
   pragma Import (Java, InferBinaryName, "inferBinaryName");
   pragma Import (Java, IsSameFile, "isSameFile");
   pragma Import (Java, HandleOption, "handleOption");
   pragma Import (Java, HasLocation, "hasLocation");
   pragma Import (Java, IsSupportedOption, "isSupportedOption");
   pragma Import (Java, GetJavaFileForInput, "getJavaFileForInput");
   pragma Import (Java, GetJavaFileForOutput, "getJavaFileForOutput");
   pragma Import (Java, GetFileForInput, "getFileForInput");
   pragma Import (Java, GetFileForOutput, "getFileForOutput");
   pragma Import (Java, Flush, "flush");
   pragma Import (Java, Close, "close");

end Javax.Tools.ForwardingJavaFileManager;
pragma Import (Java, Javax.Tools.ForwardingJavaFileManager, "javax.tools.ForwardingJavaFileManager");
pragma Extensions_Allowed (Off);
