pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Lang.Model.Element.Modifier;
limited with Javax.Lang.Model.Element.NestingKind;
limited with Javax.Tools.JavaFileObject.Kind;
with Java.Lang.Object;
with Javax.Tools.FileObject;
with Javax.Tools.ForwardingFileObject;
with Javax.Tools.JavaFileObject;

package Javax.Tools.ForwardingJavaFileObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(FileObject_I : Javax.Tools.FileObject.Ref;
            JavaFileObject_I : Javax.Tools.JavaFileObject.Ref)
    is new Javax.Tools.ForwardingFileObject.Typ(FileObject_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ForwardingJavaFileObject (P1_JavaFileObject : access Standard.Javax.Tools.JavaFileObject.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKind (This : access Typ)
                     return access Javax.Tools.JavaFileObject.Kind.Typ'Class;

   function IsNameCompatible (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class)
                              return Java.Boolean;

   function GetNestingKind (This : access Typ)
                            return access Javax.Lang.Model.Element.NestingKind.Typ'Class;

   function GetAccessLevel (This : access Typ)
                            return access Javax.Lang.Model.Element.Modifier.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ForwardingJavaFileObject);
   pragma Import (Java, GetKind, "getKind");
   pragma Import (Java, IsNameCompatible, "isNameCompatible");
   pragma Import (Java, GetNestingKind, "getNestingKind");
   pragma Import (Java, GetAccessLevel, "getAccessLevel");

end Javax.Tools.ForwardingJavaFileObject;
pragma Import (Java, Javax.Tools.ForwardingJavaFileObject, "javax.tools.ForwardingJavaFileObject");
pragma Extensions_Allowed (Off);
