pragma Extensions_Allowed (On);
limited with Java.Lang.Boolean;
limited with Java.Lang.Iterable;
limited with Java.Util.Locale;
with Java.Lang.Object;
with Java.Util.Concurrent.Callable;

package Javax.Tools.JavaCompiler.CompilationTask is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Callable_I : Java.Util.Concurrent.Callable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetProcessors (This : access Typ;
                            P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class) is abstract;

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class) is abstract;

   function Call (This : access Typ)
                  return access Java.Lang.Boolean.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetProcessors, "setProcessors");
   pragma Export (Java, SetLocale, "setLocale");
   pragma Export (Java, Call, "call");

end Javax.Tools.JavaCompiler.CompilationTask;
pragma Import (Java, Javax.Tools.JavaCompiler.CompilationTask, "javax.tools.JavaCompiler$CompilationTask");
pragma Extensions_Allowed (Off);
