pragma Extensions_Allowed (On);
limited with Java.Io.Writer;
limited with Java.Lang.Iterable;
limited with Java.Nio.Charset.Charset;
limited with Java.Util.Locale;
limited with Javax.Tools.DiagnosticListener;
limited with Javax.Tools.JavaCompiler.CompilationTask;
limited with Javax.Tools.JavaFileManager;
limited with Javax.Tools.StandardJavaFileManager;
with Java.Lang.Object;
with Javax.Tools.OptionChecker;
with Javax.Tools.Tool;

package Javax.Tools.JavaCompiler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            OptionChecker_I : Javax.Tools.OptionChecker.Ref;
            Tool_I : Javax.Tools.Tool.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTask (This : access Typ;
                     P1_Writer : access Standard.Java.Io.Writer.Typ'Class;
                     P2_JavaFileManager : access Standard.Javax.Tools.JavaFileManager.Typ'Class;
                     P3_DiagnosticListener : access Standard.Javax.Tools.DiagnosticListener.Typ'Class;
                     P4_Iterable : access Standard.Java.Lang.Iterable.Typ'Class;
                     P5_Iterable : access Standard.Java.Lang.Iterable.Typ'Class;
                     P6_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                     return access Javax.Tools.JavaCompiler.CompilationTask.Typ'Class is abstract;

   function GetStandardFileManager (This : access Typ;
                                    P1_DiagnosticListener : access Standard.Javax.Tools.DiagnosticListener.Typ'Class;
                                    P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                                    P3_Charset : access Standard.Java.Nio.Charset.Charset.Typ'Class)
                                    return access Javax.Tools.StandardJavaFileManager.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTask, "getTask");
   pragma Export (Java, GetStandardFileManager, "getStandardFileManager");

end Javax.Tools.JavaCompiler;
pragma Import (Java, Javax.Tools.JavaCompiler, "javax.tools.JavaCompiler");
pragma Extensions_Allowed (Off);
