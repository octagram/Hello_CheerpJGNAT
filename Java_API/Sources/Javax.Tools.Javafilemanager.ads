pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.Iterable;
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Java.Util.Set;
limited with Javax.Tools.FileObject;
limited with Javax.Tools.JavaFileManager.Location;
limited with Javax.Tools.JavaFileObject;
limited with Javax.Tools.JavaFileObject.Kind;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Object;
with Javax.Tools.OptionChecker;

package Javax.Tools.JavaFileManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref;
            OptionChecker_I : Javax.Tools.OptionChecker.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetClassLoader (This : access Typ;
                            P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class)
                            return access Java.Lang.ClassLoader.Typ'Class is abstract;

   function List (This : access Typ;
                  P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                  P3_Set : access Standard.Java.Util.Set.Typ'Class;
                  P4_Boolean : Java.Boolean)
                  return access Java.Lang.Iterable.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function InferBinaryName (This : access Typ;
                             P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                             P2_JavaFileObject : access Standard.Javax.Tools.JavaFileObject.Typ'Class)
                             return access Java.Lang.String.Typ'Class is abstract;

   function IsSameFile (This : access Typ;
                        P1_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class;
                        P2_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class)
                        return Java.Boolean is abstract;

   function HandleOption (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                          return Java.Boolean is abstract;

   function HasLocation (This : access Typ;
                         P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class)
                         return Java.Boolean is abstract;

   function GetJavaFileForInput (This : access Typ;
                                 P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class)
                                 return access Javax.Tools.JavaFileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetJavaFileForOutput (This : access Typ;
                                  P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class;
                                  P4_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class)
                                  return access Javax.Tools.JavaFileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetFileForInput (This : access Typ;
                             P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Tools.FileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   function GetFileForOutput (This : access Typ;
                              P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class)
                              return access Javax.Tools.FileObject.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Flush (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetClassLoader, "getClassLoader");
   pragma Export (Java, List, "list");
   pragma Export (Java, InferBinaryName, "inferBinaryName");
   pragma Export (Java, IsSameFile, "isSameFile");
   pragma Export (Java, HandleOption, "handleOption");
   pragma Export (Java, HasLocation, "hasLocation");
   pragma Export (Java, GetJavaFileForInput, "getJavaFileForInput");
   pragma Export (Java, GetJavaFileForOutput, "getJavaFileForOutput");
   pragma Export (Java, GetFileForInput, "getFileForInput");
   pragma Export (Java, GetFileForOutput, "getFileForOutput");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, Close, "close");

end Javax.Tools.JavaFileManager;
pragma Import (Java, Javax.Tools.JavaFileManager, "javax.tools.JavaFileManager");
pragma Extensions_Allowed (Off);
