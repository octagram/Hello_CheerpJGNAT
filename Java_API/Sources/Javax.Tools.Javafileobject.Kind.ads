pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;

package Javax.Tools.JavaFileObject.Kind is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  final
      Extension : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Extension, "extension");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Tools.JavaFileObject.Kind.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SOURCE : access Javax.Tools.JavaFileObject.Kind.Typ'Class;

   --  final
   CLASS : access Javax.Tools.JavaFileObject.Kind.Typ'Class;

   --  final
   HTML : access Javax.Tools.JavaFileObject.Kind.Typ'Class;

   --  final
   OTHER : access Javax.Tools.JavaFileObject.Kind.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, SOURCE, "SOURCE");
   pragma Import (Java, CLASS, "CLASS");
   pragma Import (Java, HTML, "HTML");
   pragma Import (Java, OTHER, "OTHER");

end Javax.Tools.JavaFileObject.Kind;
pragma Import (Java, Javax.Tools.JavaFileObject.Kind, "javax.tools.JavaFileObject$Kind");
pragma Extensions_Allowed (Off);
