pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Lang.Model.Element.Modifier;
limited with Javax.Lang.Model.Element.NestingKind;
limited with Javax.Tools.JavaFileObject.Kind;
with Java.Lang.Object;
with Javax.Tools.FileObject;

package Javax.Tools.JavaFileObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            FileObject_I : Javax.Tools.FileObject.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKind (This : access Typ)
                     return access Javax.Tools.JavaFileObject.Kind.Typ'Class is abstract;

   function IsNameCompatible (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class)
                              return Java.Boolean is abstract;

   function GetNestingKind (This : access Typ)
                            return access Javax.Lang.Model.Element.NestingKind.Typ'Class is abstract;

   function GetAccessLevel (This : access Typ)
                            return access Javax.Lang.Model.Element.Modifier.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetKind, "getKind");
   pragma Export (Java, IsNameCompatible, "isNameCompatible");
   pragma Export (Java, GetNestingKind, "getNestingKind");
   pragma Export (Java, GetAccessLevel, "getAccessLevel");

end Javax.Tools.JavaFileObject;
pragma Import (Java, Javax.Tools.JavaFileObject, "javax.tools.JavaFileObject");
pragma Extensions_Allowed (Off);
