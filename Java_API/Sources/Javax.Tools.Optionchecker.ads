pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Tools.OptionChecker is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSupportedOption (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsSupportedOption, "isSupportedOption");

end Javax.Tools.OptionChecker;
pragma Import (Java, Javax.Tools.OptionChecker, "javax.tools.OptionChecker");
pragma Extensions_Allowed (Off);
