pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.CharSequence;
limited with Java.Lang.String;
limited with Java.Net.URI;
limited with Javax.Lang.Model.Element.Modifier;
limited with Javax.Lang.Model.Element.NestingKind;
limited with Javax.Tools.JavaFileObject.Kind;
with Java.Lang.Object;
with Javax.Tools.JavaFileObject;

package Javax.Tools.SimpleJavaFileObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(JavaFileObject_I : Javax.Tools.JavaFileObject.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      Uri : access Java.Net.URI.Typ'Class;
      pragma Import (Java, Uri, "uri");

      --  protected  final
      Kind : access Javax.Tools.JavaFileObject.Kind.Typ'Class;
      pragma Import (Java, Kind, "kind");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_SimpleJavaFileObject (P1_URI : access Standard.Java.Net.URI.Typ'Class;
                                      P2_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToUri (This : access Typ)
                   return access Java.Net.URI.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function OpenInputStream (This : access Typ)
                             return access Java.Io.InputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenOutputStream (This : access Typ)
                              return access Java.Io.OutputStream.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenReader (This : access Typ;
                        P1_Boolean : Java.Boolean)
                        return access Java.Io.Reader.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetCharContent (This : access Typ;
                            P1_Boolean : Java.Boolean)
                            return access Java.Lang.CharSequence.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function OpenWriter (This : access Typ)
                        return access Java.Io.Writer.Typ'Class;
   --  can raise Java.Io.IOException.Except

   function GetLastModified (This : access Typ)
                             return Java.Long;

   function Delete (This : access Typ)
                    return Java.Boolean;

   function GetKind (This : access Typ)
                     return access Javax.Tools.JavaFileObject.Kind.Typ'Class;

   function IsNameCompatible (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Kind : access Standard.Javax.Tools.JavaFileObject.Kind.Typ'Class)
                              return Java.Boolean;

   function GetNestingKind (This : access Typ)
                            return access Javax.Lang.Model.Element.NestingKind.Typ'Class;

   function GetAccessLevel (This : access Typ)
                            return access Javax.Lang.Model.Element.Modifier.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleJavaFileObject);
   pragma Import (Java, ToUri, "toUri");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, OpenInputStream, "openInputStream");
   pragma Import (Java, OpenOutputStream, "openOutputStream");
   pragma Import (Java, OpenReader, "openReader");
   pragma Import (Java, GetCharContent, "getCharContent");
   pragma Import (Java, OpenWriter, "openWriter");
   pragma Import (Java, GetLastModified, "getLastModified");
   pragma Import (Java, Delete, "delete");
   pragma Import (Java, GetKind, "getKind");
   pragma Import (Java, IsNameCompatible, "isNameCompatible");
   pragma Import (Java, GetNestingKind, "getNestingKind");
   pragma Import (Java, GetAccessLevel, "getAccessLevel");
   pragma Import (Java, ToString, "toString");

end Javax.Tools.SimpleJavaFileObject;
pragma Import (Java, Javax.Tools.SimpleJavaFileObject, "javax.tools.SimpleJavaFileObject");
pragma Extensions_Allowed (Off);
