pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.Iterable;
limited with Java.Lang.String;
limited with Javax.Tools.FileObject;
limited with Javax.Tools.JavaFileManager.Location;
with Java.Lang.Object;
with Javax.Tools.JavaFileManager;

package Javax.Tools.StandardJavaFileManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            JavaFileManager_I : Javax.Tools.JavaFileManager.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsSameFile (This : access Typ;
                        P1_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class;
                        P2_FileObject : access Standard.Javax.Tools.FileObject.Typ'Class)
                        return Java.Boolean is abstract;

   function GetJavaFileObjectsFromFiles (This : access Typ;
                                         P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                                         return access Java.Lang.Iterable.Typ'Class is abstract;

   function GetJavaFileObjects (This : access Typ;
                                P1_File_Arr : access Java.Io.File.Arr_Obj)
                                return access Java.Lang.Iterable.Typ'Class is abstract;

   function GetJavaFileObjectsFromStrings (This : access Typ;
                                           P1_Iterable : access Standard.Java.Lang.Iterable.Typ'Class)
                                           return access Java.Lang.Iterable.Typ'Class is abstract;

   function GetJavaFileObjects (This : access Typ;
                                P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                return access Java.Lang.Iterable.Typ'Class is abstract;

   procedure SetLocation (This : access Typ;
                          P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class;
                          P2_Iterable : access Standard.Java.Lang.Iterable.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except

   function GetLocation (This : access Typ;
                         P1_Location : access Standard.Javax.Tools.JavaFileManager.Location.Typ'Class)
                         return access Java.Lang.Iterable.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsSameFile, "isSameFile");
   pragma Export (Java, GetJavaFileObjectsFromFiles, "getJavaFileObjectsFromFiles");
   pragma Export (Java, GetJavaFileObjects, "getJavaFileObjects");
   pragma Export (Java, GetJavaFileObjectsFromStrings, "getJavaFileObjectsFromStrings");
   pragma Export (Java, SetLocation, "setLocation");
   pragma Export (Java, GetLocation, "getLocation");

end Javax.Tools.StandardJavaFileManager;
pragma Import (Java, Javax.Tools.StandardJavaFileManager, "javax.tools.StandardJavaFileManager");
pragma Extensions_Allowed (Off);
