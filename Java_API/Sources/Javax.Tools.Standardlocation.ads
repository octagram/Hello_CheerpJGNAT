pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Comparable;
with Java.Lang.Enum;
with Java.Lang.Object;
with Javax.Tools.JavaFileManager.Location;

package Javax.Tools.StandardLocation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Comparable_I : Java.Lang.Comparable.Ref;
            Location_I : Javax.Tools.JavaFileManager.Location.Ref)
    is new Java.Lang.Enum.Typ(Serializable_I,
                              Comparable_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Values return Standard.Java.Lang.Object.Ref;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Tools.StandardLocation.Typ'Class;

   function LocationFor (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Tools.JavaFileManager.Location.Typ'Class;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function IsOutputLocation (This : access Typ)
                              return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CLASS_OUTPUT : access Javax.Tools.StandardLocation.Typ'Class;

   --  final
   SOURCE_OUTPUT : access Javax.Tools.StandardLocation.Typ'Class;

   --  final
   CLASS_PATH : access Javax.Tools.StandardLocation.Typ'Class;

   --  final
   SOURCE_PATH : access Javax.Tools.StandardLocation.Typ'Class;

   --  final
   ANNOTATION_PROCESSOR_PATH : access Javax.Tools.StandardLocation.Typ'Class;

   --  final
   PLATFORM_CLASS_PATH : access Javax.Tools.StandardLocation.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Values, "values");
   pragma Import (Java, ValueOf, "valueOf");
   pragma Import (Java, LocationFor, "locationFor");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, IsOutputLocation, "isOutputLocation");
   pragma Import (Java, CLASS_OUTPUT, "CLASS_OUTPUT");
   pragma Import (Java, SOURCE_OUTPUT, "SOURCE_OUTPUT");
   pragma Import (Java, CLASS_PATH, "CLASS_PATH");
   pragma Import (Java, SOURCE_PATH, "SOURCE_PATH");
   pragma Import (Java, ANNOTATION_PROCESSOR_PATH, "ANNOTATION_PROCESSOR_PATH");
   pragma Import (Java, PLATFORM_CLASS_PATH, "PLATFORM_CLASS_PATH");

end Javax.Tools.StandardLocation;
pragma Import (Java, Javax.Tools.StandardLocation, "javax.tools.StandardLocation");
pragma Extensions_Allowed (Off);
