pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Util.Set;
with Java.Lang.Object;

package Javax.Tools.Tool is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Run (This : access Typ;
                 P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                 P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                 P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                 P4_String_Arr : access Java.Lang.String.Arr_Obj)
                 return Java.Int is abstract;

   function GetSourceVersions (This : access Typ)
                               return access Java.Util.Set.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Run, "run");
   pragma Export (Java, GetSourceVersions, "getSourceVersions");

end Javax.Tools.Tool;
pragma Import (Java, Javax.Tools.Tool, "javax.tools.Tool");
pragma Extensions_Allowed (Off);
