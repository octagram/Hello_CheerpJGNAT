pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Javax.Tools.JavaCompiler;
with Java.Lang.Object;

package Javax.Tools.ToolProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSystemJavaCompiler return access Javax.Tools.JavaCompiler.Typ'Class;

   function GetSystemToolClassLoader return access Java.Lang.ClassLoader.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetSystemJavaCompiler, "getSystemJavaCompiler");
   pragma Import (Java, GetSystemToolClassLoader, "getSystemToolClassLoader");

end Javax.Tools.ToolProvider;
pragma Import (Java, Javax.Tools.ToolProvider, "javax.tools.ToolProvider");
pragma Extensions_Allowed (Off);
