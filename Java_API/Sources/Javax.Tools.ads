pragma Extensions_Allowed (On);
package Javax.Tools is
   pragma Preelaborate;
end Javax.Tools;
pragma Import (Java, Javax.Tools, "javax.tools");
pragma Extensions_Allowed (Off);
