pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Transaction.Xa.XAException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      ErrorCode : Java.Int;
      pragma Import (Java, ErrorCode, "errorCode");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XAException (This : Ref := null)
                             return Ref;

   function New_XAException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_XAException (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   XA_RBBASE : constant Java.Int;

   --  final
   XA_RBROLLBACK : constant Java.Int;

   --  final
   XA_RBCOMMFAIL : constant Java.Int;

   --  final
   XA_RBDEADLOCK : constant Java.Int;

   --  final
   XA_RBINTEGRITY : constant Java.Int;

   --  final
   XA_RBOTHER : constant Java.Int;

   --  final
   XA_RBPROTO : constant Java.Int;

   --  final
   XA_RBTIMEOUT : constant Java.Int;

   --  final
   XA_RBTRANSIENT : constant Java.Int;

   --  final
   XA_RBEND : constant Java.Int;

   --  final
   XA_NOMIGRATE : constant Java.Int;

   --  final
   XA_HEURHAZ : constant Java.Int;

   --  final
   XA_HEURCOM : constant Java.Int;

   --  final
   XA_HEURRB : constant Java.Int;

   --  final
   XA_HEURMIX : constant Java.Int;

   --  final
   XA_RETRY : constant Java.Int;

   --  final
   XA_RDONLY : constant Java.Int;

   --  final
   XAER_ASYNC : constant Java.Int;

   --  final
   XAER_RMERR : constant Java.Int;

   --  final
   XAER_NOTA : constant Java.Int;

   --  final
   XAER_INVAL : constant Java.Int;

   --  final
   XAER_PROTO : constant Java.Int;

   --  final
   XAER_RMFAIL : constant Java.Int;

   --  final
   XAER_DUPID : constant Java.Int;

   --  final
   XAER_OUTSIDE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.transaction.xa.XAException");
   pragma Java_Constructor (New_XAException);
   pragma Import (Java, XA_RBBASE, "XA_RBBASE");
   pragma Import (Java, XA_RBROLLBACK, "XA_RBROLLBACK");
   pragma Import (Java, XA_RBCOMMFAIL, "XA_RBCOMMFAIL");
   pragma Import (Java, XA_RBDEADLOCK, "XA_RBDEADLOCK");
   pragma Import (Java, XA_RBINTEGRITY, "XA_RBINTEGRITY");
   pragma Import (Java, XA_RBOTHER, "XA_RBOTHER");
   pragma Import (Java, XA_RBPROTO, "XA_RBPROTO");
   pragma Import (Java, XA_RBTIMEOUT, "XA_RBTIMEOUT");
   pragma Import (Java, XA_RBTRANSIENT, "XA_RBTRANSIENT");
   pragma Import (Java, XA_RBEND, "XA_RBEND");
   pragma Import (Java, XA_NOMIGRATE, "XA_NOMIGRATE");
   pragma Import (Java, XA_HEURHAZ, "XA_HEURHAZ");
   pragma Import (Java, XA_HEURCOM, "XA_HEURCOM");
   pragma Import (Java, XA_HEURRB, "XA_HEURRB");
   pragma Import (Java, XA_HEURMIX, "XA_HEURMIX");
   pragma Import (Java, XA_RETRY, "XA_RETRY");
   pragma Import (Java, XA_RDONLY, "XA_RDONLY");
   pragma Import (Java, XAER_ASYNC, "XAER_ASYNC");
   pragma Import (Java, XAER_RMERR, "XAER_RMERR");
   pragma Import (Java, XAER_NOTA, "XAER_NOTA");
   pragma Import (Java, XAER_INVAL, "XAER_INVAL");
   pragma Import (Java, XAER_PROTO, "XAER_PROTO");
   pragma Import (Java, XAER_RMFAIL, "XAER_RMFAIL");
   pragma Import (Java, XAER_DUPID, "XAER_DUPID");
   pragma Import (Java, XAER_OUTSIDE, "XAER_OUTSIDE");

end Javax.Transaction.Xa.XAException;
pragma Import (Java, Javax.Transaction.Xa.XAException, "javax.transaction.xa.XAException");
pragma Extensions_Allowed (Off);
