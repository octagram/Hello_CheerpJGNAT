pragma Extensions_Allowed (On);
limited with Javax.Transaction.Xa.Xid;
with Java.Lang.Object;

package Javax.Transaction.Xa.XAResource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Commit (This : access Typ;
                     P1_Xid : access Standard.Javax.Transaction.Xa.Xid.Typ'Class;
                     P2_Boolean : Java.Boolean) is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   procedure end_K (This : access Typ;
                    P1_Xid : access Standard.Javax.Transaction.Xa.Xid.Typ'Class;
                    P2_Int : Java.Int) is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   procedure Forget (This : access Typ;
                     P1_Xid : access Standard.Javax.Transaction.Xa.Xid.Typ'Class) is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   function GetTransactionTimeout (This : access Typ)
                                   return Java.Int is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   function IsSameRM (This : access Typ;
                      P1_XAResource : access Standard.Javax.Transaction.Xa.XAResource.Typ'Class)
                      return Java.Boolean is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   function Prepare (This : access Typ;
                     P1_Xid : access Standard.Javax.Transaction.Xa.Xid.Typ'Class)
                     return Java.Int is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   function Recover (This : access Typ;
                     P1_Int : Java.Int)
                     return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   procedure Rollback (This : access Typ;
                       P1_Xid : access Standard.Javax.Transaction.Xa.Xid.Typ'Class) is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   function SetTransactionTimeout (This : access Typ;
                                   P1_Int : Java.Int)
                                   return Java.Boolean is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   procedure Start (This : access Typ;
                    P1_Xid : access Standard.Javax.Transaction.Xa.Xid.Typ'Class;
                    P2_Int : Java.Int) is abstract;
   --  can raise Javax.Transaction.Xa.XAException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TMENDRSCAN : constant Java.Int;

   --  final
   TMFAIL : constant Java.Int;

   --  final
   TMJOIN : constant Java.Int;

   --  final
   TMNOFLAGS : constant Java.Int;

   --  final
   TMONEPHASE : constant Java.Int;

   --  final
   TMRESUME : constant Java.Int;

   --  final
   TMSTARTRSCAN : constant Java.Int;

   --  final
   TMSUCCESS : constant Java.Int;

   --  final
   TMSUSPEND : constant Java.Int;

   --  final
   XA_RDONLY : constant Java.Int;

   --  final
   XA_OK : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Commit, "commit");
   pragma Export (Java, end_K, "end");
   pragma Export (Java, Forget, "forget");
   pragma Export (Java, GetTransactionTimeout, "getTransactionTimeout");
   pragma Export (Java, IsSameRM, "isSameRM");
   pragma Export (Java, Prepare, "prepare");
   pragma Export (Java, Recover, "recover");
   pragma Export (Java, Rollback, "rollback");
   pragma Export (Java, SetTransactionTimeout, "setTransactionTimeout");
   pragma Export (Java, Start, "start");
   pragma Import (Java, TMENDRSCAN, "TMENDRSCAN");
   pragma Import (Java, TMFAIL, "TMFAIL");
   pragma Import (Java, TMJOIN, "TMJOIN");
   pragma Import (Java, TMNOFLAGS, "TMNOFLAGS");
   pragma Import (Java, TMONEPHASE, "TMONEPHASE");
   pragma Import (Java, TMRESUME, "TMRESUME");
   pragma Import (Java, TMSTARTRSCAN, "TMSTARTRSCAN");
   pragma Import (Java, TMSUCCESS, "TMSUCCESS");
   pragma Import (Java, TMSUSPEND, "TMSUSPEND");
   pragma Import (Java, XA_RDONLY, "XA_RDONLY");
   pragma Import (Java, XA_OK, "XA_OK");

end Javax.Transaction.Xa.XAResource;
pragma Import (Java, Javax.Transaction.Xa.XAResource, "javax.transaction.xa.XAResource");
pragma Extensions_Allowed (Off);
