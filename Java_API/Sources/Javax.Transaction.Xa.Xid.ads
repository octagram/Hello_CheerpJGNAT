pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Transaction.Xa.Xid is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFormatId (This : access Typ)
                         return Java.Int is abstract;

   function GetGlobalTransactionId (This : access Typ)
                                    return Java.Byte_Arr is abstract;

   function GetBranchQualifier (This : access Typ)
                                return Java.Byte_Arr is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MAXGTRIDSIZE : constant Java.Int;

   --  final
   MAXBQUALSIZE : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetFormatId, "getFormatId");
   pragma Export (Java, GetGlobalTransactionId, "getGlobalTransactionId");
   pragma Export (Java, GetBranchQualifier, "getBranchQualifier");
   pragma Import (Java, MAXGTRIDSIZE, "MAXGTRIDSIZE");
   pragma Import (Java, MAXBQUALSIZE, "MAXBQUALSIZE");

end Javax.Transaction.Xa.Xid;
pragma Import (Java, Javax.Transaction.Xa.Xid, "javax.transaction.xa.Xid");
pragma Extensions_Allowed (Off);
