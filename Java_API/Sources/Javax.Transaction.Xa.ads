pragma Extensions_Allowed (On);
package Javax.Transaction.Xa is
   pragma Preelaborate;
end Javax.Transaction.Xa;
pragma Import (Java, Javax.Transaction.Xa, "javax.transaction.xa");
pragma Extensions_Allowed (Off);
