pragma Extensions_Allowed (On);
package Javax.Transaction is
   pragma Preelaborate;
end Javax.Transaction;
pragma Import (Java, Javax.Transaction, "javax.transaction");
pragma Extensions_Allowed (Off);
