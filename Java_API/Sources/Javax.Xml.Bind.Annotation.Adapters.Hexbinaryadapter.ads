pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Bind.Annotation.Adapters.XmlAdapter;

package Javax.Xml.Bind.Annotation.Adapters.HexBinaryAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HexBinaryAdapter (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Unmarshal (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Byte_Arr;

   function Marshal (This : access Typ;
                     P1_Byte_Arr : Java.Byte_Arr)
                     return access Java.Lang.String.Typ'Class;

   function Marshal (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except

   function Unmarshal (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.Exception_K.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HexBinaryAdapter);
   pragma Import (Java, Unmarshal, "unmarshal");
   pragma Import (Java, Marshal, "marshal");

end Javax.Xml.Bind.Annotation.Adapters.HexBinaryAdapter;
pragma Import (Java, Javax.Xml.Bind.Annotation.Adapters.HexBinaryAdapter, "javax.xml.bind.annotation.adapters.HexBinaryAdapter");
pragma Extensions_Allowed (Off);
