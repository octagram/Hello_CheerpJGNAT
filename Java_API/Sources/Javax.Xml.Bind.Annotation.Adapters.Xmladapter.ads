pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Xml.Bind.Annotation.Adapters.XmlAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_XmlAdapter (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Unmarshal (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.Exception_K.Except

   function Marshal (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.Exception_K.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XmlAdapter);
   pragma Export (Java, Unmarshal, "unmarshal");
   pragma Export (Java, Marshal, "marshal");

end Javax.Xml.Bind.Annotation.Adapters.XmlAdapter;
pragma Import (Java, Javax.Xml.Bind.Annotation.Adapters.XmlAdapter, "javax.xml.bind.annotation.adapters.XmlAdapter");
pragma Extensions_Allowed (Off);
