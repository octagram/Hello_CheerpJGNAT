pragma Extensions_Allowed (On);
package Javax.Xml.Bind.Annotation.Adapters is
   pragma Preelaborate;
end Javax.Xml.Bind.Annotation.Adapters;
pragma Import (Java, Javax.Xml.Bind.Annotation.Adapters, "javax.xml.bind.annotation.adapters");
pragma Extensions_Allowed (Off);
