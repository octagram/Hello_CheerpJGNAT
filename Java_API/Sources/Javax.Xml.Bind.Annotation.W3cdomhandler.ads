pragma Extensions_Allowed (On);
limited with Javax.Xml.Bind.ValidationEventHandler;
limited with Javax.Xml.Parsers.DocumentBuilder;
limited with Javax.Xml.Transform.Dom.DOMResult;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Source;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;
with Javax.Xml.Bind.Annotation.DomHandler;

package Javax.Xml.Bind.Annotation.W3CDomHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DomHandler_I : Javax.Xml.Bind.Annotation.DomHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_W3CDomHandler (This : Ref := null)
                               return Ref;

   function New_W3CDomHandler (P1_DocumentBuilder : access Standard.Javax.Xml.Parsers.DocumentBuilder.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBuilder (This : access Typ)
                        return access Javax.Xml.Parsers.DocumentBuilder.Typ'Class;

   procedure SetBuilder (This : access Typ;
                         P1_DocumentBuilder : access Standard.Javax.Xml.Parsers.DocumentBuilder.Typ'Class);

   function CreateUnmarshaller (This : access Typ;
                                P1_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class)
                                return access Javax.Xml.Transform.Dom.DOMResult.Typ'Class;

   function GetElement (This : access Typ;
                        P1_DOMResult : access Standard.Javax.Xml.Transform.Dom.DOMResult.Typ'Class)
                        return access Org.W3c.Dom.Element.Typ'Class;

   function Marshal (This : access Typ;
                     P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                     P2_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class)
                     return access Javax.Xml.Transform.Source.Typ'Class;

   function Marshal (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P2_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class)
                     return access Javax.Xml.Transform.Source.Typ'Class;

   function GetElement (This : access Typ;
                        P1_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class)
                        return access Java.Lang.Object.Typ'Class;

   function CreateUnmarshaller (This : access Typ;
                                P1_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class)
                                return access Javax.Xml.Transform.Result.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_W3CDomHandler);
   pragma Import (Java, GetBuilder, "getBuilder");
   pragma Import (Java, SetBuilder, "setBuilder");
   pragma Import (Java, CreateUnmarshaller, "createUnmarshaller");
   pragma Import (Java, GetElement, "getElement");
   pragma Import (Java, Marshal, "marshal");

end Javax.Xml.Bind.Annotation.W3CDomHandler;
pragma Import (Java, Javax.Xml.Bind.Annotation.W3CDomHandler, "javax.xml.bind.annotation.W3CDomHandler");
pragma Extensions_Allowed (Off);
