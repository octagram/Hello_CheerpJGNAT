pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Bind.Annotation.XmlNs;
limited with Javax.Xml.Bind.Annotation.XmlNsForm;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Xml.Bind.Annotation.XmlSchema is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Xmlns (This : access Typ)
                   return Standard.Java.Lang.Object.Ref is abstract;

   function Namespace (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function ElementFormDefault (This : access Typ)
                                return access Javax.Xml.Bind.Annotation.XmlNsForm.Typ'Class is abstract;

   function AttributeFormDefault (This : access Typ)
                                  return access Javax.Xml.Bind.Annotation.XmlNsForm.Typ'Class is abstract;

   function Location (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NO_LOCATION : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Xmlns, "xmlns");
   pragma Export (Java, Namespace, "namespace");
   pragma Export (Java, ElementFormDefault, "elementFormDefault");
   pragma Export (Java, AttributeFormDefault, "attributeFormDefault");
   pragma Export (Java, Location, "location");
   pragma Import (Java, NO_LOCATION, "NO_LOCATION");

end Javax.Xml.Bind.Annotation.XmlSchema;
pragma Import (Java, Javax.Xml.Bind.Annotation.XmlSchema, "javax.xml.bind.annotation.XmlSchema");
pragma Extensions_Allowed (Off);
