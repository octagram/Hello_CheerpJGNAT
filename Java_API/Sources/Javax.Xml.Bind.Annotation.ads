pragma Extensions_Allowed (On);
package Javax.Xml.Bind.Annotation is
   pragma Preelaborate;
end Javax.Xml.Bind.Annotation;
pragma Import (Java, Javax.Xml.Bind.Annotation, "javax.xml.bind.annotation");
pragma Extensions_Allowed (Off);
