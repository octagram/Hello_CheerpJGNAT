pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Activation.DataHandler;
with Java.Lang.Object;

package Javax.Xml.Bind.Attachment.AttachmentMarshaller is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AttachmentMarshaller (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddMtomAttachment (This : access Typ;
                               P1_DataHandler : access Standard.Javax.Activation.DataHandler.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.String.Typ'Class is abstract;

   function AddMtomAttachment (This : access Typ;
                               P1_Byte_Arr : Java.Byte_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int;
                               P4_String : access Standard.Java.Lang.String.Typ'Class;
                               P5_String : access Standard.Java.Lang.String.Typ'Class;
                               P6_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.String.Typ'Class is abstract;

   function IsXOPPackage (This : access Typ)
                          return Java.Boolean;

   function AddSwaRefAttachment (This : access Typ;
                                 P1_DataHandler : access Standard.Javax.Activation.DataHandler.Typ'Class)
                                 return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttachmentMarshaller);
   pragma Export (Java, AddMtomAttachment, "addMtomAttachment");
   pragma Export (Java, IsXOPPackage, "isXOPPackage");
   pragma Export (Java, AddSwaRefAttachment, "addSwaRefAttachment");

end Javax.Xml.Bind.Attachment.AttachmentMarshaller;
pragma Import (Java, Javax.Xml.Bind.Attachment.AttachmentMarshaller, "javax.xml.bind.attachment.AttachmentMarshaller");
pragma Extensions_Allowed (Off);
