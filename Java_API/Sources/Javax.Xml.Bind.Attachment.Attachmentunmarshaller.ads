pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Activation.DataHandler;
with Java.Lang.Object;

package Javax.Xml.Bind.Attachment.AttachmentUnmarshaller is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AttachmentUnmarshaller (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAttachmentAsDataHandler (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return access Javax.Activation.DataHandler.Typ'Class is abstract;

   function GetAttachmentAsByteArray (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return Java.Byte_Arr is abstract;

   function IsXOPPackage (This : access Typ)
                          return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttachmentUnmarshaller);
   pragma Export (Java, GetAttachmentAsDataHandler, "getAttachmentAsDataHandler");
   pragma Export (Java, GetAttachmentAsByteArray, "getAttachmentAsByteArray");
   pragma Import (Java, IsXOPPackage, "isXOPPackage");

end Javax.Xml.Bind.Attachment.AttachmentUnmarshaller;
pragma Import (Java, Javax.Xml.Bind.Attachment.AttachmentUnmarshaller, "javax.xml.bind.attachment.AttachmentUnmarshaller");
pragma Extensions_Allowed (Off);
