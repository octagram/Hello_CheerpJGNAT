pragma Extensions_Allowed (On);
package Javax.Xml.Bind.Attachment is
   pragma Preelaborate;
end Javax.Xml.Bind.Attachment;
pragma Import (Java, Javax.Xml.Bind.Attachment, "javax.xml.bind.attachment");
pragma Extensions_Allowed (Off);
