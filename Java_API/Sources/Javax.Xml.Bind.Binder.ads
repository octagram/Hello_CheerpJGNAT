pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Xml.Bind.JAXBElement;
limited with Javax.Xml.Bind.ValidationEventHandler;
limited with Javax.Xml.Validation.Schema;
with Java.Lang.Object;

package Javax.Xml.Bind.Binder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Binder (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Unmarshal (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetXMLNode (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function GetJAXBNode (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function UpdateXML (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function UpdateXML (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                       P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function UpdateJAXB (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetSchema (This : access Typ;
                        P1_Schema : access Standard.Javax.Xml.Validation.Schema.Typ'Class) is abstract;

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class is abstract;

   procedure SetEventHandler (This : access Typ;
                              P1_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetEventHandler (This : access Typ)
                             return access Javax.Xml.Bind.ValidationEventHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.PropertyException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.PropertyException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Binder);
   pragma Export (Java, Unmarshal, "unmarshal");
   pragma Export (Java, Marshal, "marshal");
   pragma Export (Java, GetXMLNode, "getXMLNode");
   pragma Export (Java, GetJAXBNode, "getJAXBNode");
   pragma Export (Java, UpdateXML, "updateXML");
   pragma Export (Java, UpdateJAXB, "updateJAXB");
   pragma Export (Java, SetSchema, "setSchema");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, SetEventHandler, "setEventHandler");
   pragma Export (Java, GetEventHandler, "getEventHandler");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");

end Javax.Xml.Bind.Binder;
pragma Import (Java, Javax.Xml.Bind.Binder, "javax.xml.bind.Binder");
pragma Extensions_Allowed (Off);
