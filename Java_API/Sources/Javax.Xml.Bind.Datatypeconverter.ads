pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Math.BigInteger;
limited with Java.Util.Calendar;
limited with Javax.Xml.Bind.DatatypeConverterInterface;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Bind.DatatypeConverter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDatatypeConverter (P1_DatatypeConverterInterface : access Standard.Javax.Xml.Bind.DatatypeConverterInterface.Typ'Class);

   function ParseString (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function ParseInteger (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Math.BigInteger.Typ'Class;

   function ParseInt (P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Int;

   function ParseLong (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Long;

   function ParseShort (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Short;

   function ParseDecimal (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Math.BigDecimal.Typ'Class;

   function ParseFloat (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Float;

   function ParseDouble (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Double;

   function ParseBoolean (P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean;

   function ParseByte (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Byte;

   function ParseQName (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class)
                        return access Javax.Xml.Namespace.QName.Typ'Class;

   function ParseDateTime (P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Util.Calendar.Typ'Class;

   function ParseBase64Binary (P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return Java.Byte_Arr;

   function ParseHexBinary (P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Byte_Arr;

   function ParseUnsignedInt (P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Long;

   function ParseUnsignedShort (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int;

   function ParseTime (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.Calendar.Typ'Class;

   function ParseDate (P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.Calendar.Typ'Class;

   function ParseAnySimpleType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function PrintString (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function PrintInteger (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function PrintInt (P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function PrintLong (P1_Long : Java.Long)
                       return access Java.Lang.String.Typ'Class;

   function PrintShort (P1_Short : Java.Short)
                        return access Java.Lang.String.Typ'Class;

   function PrintDecimal (P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                          return access Java.Lang.String.Typ'Class;

   function PrintFloat (P1_Float : Java.Float)
                        return access Java.Lang.String.Typ'Class;

   function PrintDouble (P1_Double : Java.Double)
                         return access Java.Lang.String.Typ'Class;

   function PrintBoolean (P1_Boolean : Java.Boolean)
                          return access Java.Lang.String.Typ'Class;

   function PrintByte (P1_Byte : Java.Byte)
                       return access Java.Lang.String.Typ'Class;

   function PrintQName (P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                        P2_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class)
                        return access Java.Lang.String.Typ'Class;

   function PrintDateTime (P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                           return access Java.Lang.String.Typ'Class;

   function PrintBase64Binary (P1_Byte_Arr : Java.Byte_Arr)
                               return access Java.Lang.String.Typ'Class;

   function PrintHexBinary (P1_Byte_Arr : Java.Byte_Arr)
                            return access Java.Lang.String.Typ'Class;

   function PrintUnsignedInt (P1_Long : Java.Long)
                              return access Java.Lang.String.Typ'Class;

   function PrintUnsignedShort (P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class;

   function PrintTime (P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function PrintDate (P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function PrintAnySimpleType (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, SetDatatypeConverter, "setDatatypeConverter");
   pragma Import (Java, ParseString, "parseString");
   pragma Import (Java, ParseInteger, "parseInteger");
   pragma Import (Java, ParseInt, "parseInt");
   pragma Import (Java, ParseLong, "parseLong");
   pragma Import (Java, ParseShort, "parseShort");
   pragma Import (Java, ParseDecimal, "parseDecimal");
   pragma Import (Java, ParseFloat, "parseFloat");
   pragma Import (Java, ParseDouble, "parseDouble");
   pragma Import (Java, ParseBoolean, "parseBoolean");
   pragma Import (Java, ParseByte, "parseByte");
   pragma Import (Java, ParseQName, "parseQName");
   pragma Import (Java, ParseDateTime, "parseDateTime");
   pragma Import (Java, ParseBase64Binary, "parseBase64Binary");
   pragma Import (Java, ParseHexBinary, "parseHexBinary");
   pragma Import (Java, ParseUnsignedInt, "parseUnsignedInt");
   pragma Import (Java, ParseUnsignedShort, "parseUnsignedShort");
   pragma Import (Java, ParseTime, "parseTime");
   pragma Import (Java, ParseDate, "parseDate");
   pragma Import (Java, ParseAnySimpleType, "parseAnySimpleType");
   pragma Import (Java, PrintString, "printString");
   pragma Import (Java, PrintInteger, "printInteger");
   pragma Import (Java, PrintInt, "printInt");
   pragma Import (Java, PrintLong, "printLong");
   pragma Import (Java, PrintShort, "printShort");
   pragma Import (Java, PrintDecimal, "printDecimal");
   pragma Import (Java, PrintFloat, "printFloat");
   pragma Import (Java, PrintDouble, "printDouble");
   pragma Import (Java, PrintBoolean, "printBoolean");
   pragma Import (Java, PrintByte, "printByte");
   pragma Import (Java, PrintQName, "printQName");
   pragma Import (Java, PrintDateTime, "printDateTime");
   pragma Import (Java, PrintBase64Binary, "printBase64Binary");
   pragma Import (Java, PrintHexBinary, "printHexBinary");
   pragma Import (Java, PrintUnsignedInt, "printUnsignedInt");
   pragma Import (Java, PrintUnsignedShort, "printUnsignedShort");
   pragma Import (Java, PrintTime, "printTime");
   pragma Import (Java, PrintDate, "printDate");
   pragma Import (Java, PrintAnySimpleType, "printAnySimpleType");

end Javax.Xml.Bind.DatatypeConverter;
pragma Import (Java, Javax.Xml.Bind.DatatypeConverter, "javax.xml.bind.DatatypeConverter");
pragma Extensions_Allowed (Off);
