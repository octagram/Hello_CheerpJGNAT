pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Math.BigInteger;
limited with Java.Util.Calendar;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Bind.DatatypeConverterInterface is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ParseString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class is abstract;

   function ParseInteger (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Math.BigInteger.Typ'Class is abstract;

   function ParseInt (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Int is abstract;

   function ParseLong (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Long is abstract;

   function ParseShort (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Short is abstract;

   function ParseDecimal (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Math.BigDecimal.Typ'Class is abstract;

   function ParseFloat (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Float is abstract;

   function ParseDouble (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Double is abstract;

   function ParseBoolean (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean is abstract;

   function ParseByte (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Byte is abstract;

   function ParseQName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class)
                        return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function ParseDateTime (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Java.Util.Calendar.Typ'Class is abstract;

   function ParseBase64Binary (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return Java.Byte_Arr is abstract;

   function ParseHexBinary (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Byte_Arr is abstract;

   function ParseUnsignedInt (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return Java.Long is abstract;

   function ParseUnsignedShort (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Int is abstract;

   function ParseTime (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.Calendar.Typ'Class is abstract;

   function ParseDate (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Util.Calendar.Typ'Class is abstract;

   function ParseAnySimpleType (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;

   function PrintString (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class is abstract;

   function PrintInteger (This : access Typ;
                          P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                          return access Java.Lang.String.Typ'Class is abstract;

   function PrintInt (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class is abstract;

   function PrintLong (This : access Typ;
                       P1_Long : Java.Long)
                       return access Java.Lang.String.Typ'Class is abstract;

   function PrintShort (This : access Typ;
                        P1_Short : Java.Short)
                        return access Java.Lang.String.Typ'Class is abstract;

   function PrintDecimal (This : access Typ;
                          P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                          return access Java.Lang.String.Typ'Class is abstract;

   function PrintFloat (This : access Typ;
                        P1_Float : Java.Float)
                        return access Java.Lang.String.Typ'Class is abstract;

   function PrintDouble (This : access Typ;
                         P1_Double : Java.Double)
                         return access Java.Lang.String.Typ'Class is abstract;

   function PrintBoolean (This : access Typ;
                          P1_Boolean : Java.Boolean)
                          return access Java.Lang.String.Typ'Class is abstract;

   function PrintByte (This : access Typ;
                       P1_Byte : Java.Byte)
                       return access Java.Lang.String.Typ'Class is abstract;

   function PrintQName (This : access Typ;
                        P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                        P2_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class)
                        return access Java.Lang.String.Typ'Class is abstract;

   function PrintDateTime (This : access Typ;
                           P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                           return access Java.Lang.String.Typ'Class is abstract;

   function PrintBase64Binary (This : access Typ;
                               P1_Byte_Arr : Java.Byte_Arr)
                               return access Java.Lang.String.Typ'Class is abstract;

   function PrintHexBinary (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr)
                            return access Java.Lang.String.Typ'Class is abstract;

   function PrintUnsignedInt (This : access Typ;
                              P1_Long : Java.Long)
                              return access Java.Lang.String.Typ'Class is abstract;

   function PrintUnsignedShort (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class is abstract;

   function PrintTime (This : access Typ;
                       P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                       return access Java.Lang.String.Typ'Class is abstract;

   function PrintDate (This : access Typ;
                       P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                       return access Java.Lang.String.Typ'Class is abstract;

   function PrintAnySimpleType (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ParseString, "parseString");
   pragma Export (Java, ParseInteger, "parseInteger");
   pragma Export (Java, ParseInt, "parseInt");
   pragma Export (Java, ParseLong, "parseLong");
   pragma Export (Java, ParseShort, "parseShort");
   pragma Export (Java, ParseDecimal, "parseDecimal");
   pragma Export (Java, ParseFloat, "parseFloat");
   pragma Export (Java, ParseDouble, "parseDouble");
   pragma Export (Java, ParseBoolean, "parseBoolean");
   pragma Export (Java, ParseByte, "parseByte");
   pragma Export (Java, ParseQName, "parseQName");
   pragma Export (Java, ParseDateTime, "parseDateTime");
   pragma Export (Java, ParseBase64Binary, "parseBase64Binary");
   pragma Export (Java, ParseHexBinary, "parseHexBinary");
   pragma Export (Java, ParseUnsignedInt, "parseUnsignedInt");
   pragma Export (Java, ParseUnsignedShort, "parseUnsignedShort");
   pragma Export (Java, ParseTime, "parseTime");
   pragma Export (Java, ParseDate, "parseDate");
   pragma Export (Java, ParseAnySimpleType, "parseAnySimpleType");
   pragma Export (Java, PrintString, "printString");
   pragma Export (Java, PrintInteger, "printInteger");
   pragma Export (Java, PrintInt, "printInt");
   pragma Export (Java, PrintLong, "printLong");
   pragma Export (Java, PrintShort, "printShort");
   pragma Export (Java, PrintDecimal, "printDecimal");
   pragma Export (Java, PrintFloat, "printFloat");
   pragma Export (Java, PrintDouble, "printDouble");
   pragma Export (Java, PrintBoolean, "printBoolean");
   pragma Export (Java, PrintByte, "printByte");
   pragma Export (Java, PrintQName, "printQName");
   pragma Export (Java, PrintDateTime, "printDateTime");
   pragma Export (Java, PrintBase64Binary, "printBase64Binary");
   pragma Export (Java, PrintHexBinary, "printHexBinary");
   pragma Export (Java, PrintUnsignedInt, "printUnsignedInt");
   pragma Export (Java, PrintUnsignedShort, "printUnsignedShort");
   pragma Export (Java, PrintTime, "printTime");
   pragma Export (Java, PrintDate, "printDate");
   pragma Export (Java, PrintAnySimpleType, "printAnySimpleType");

end Javax.Xml.Bind.DatatypeConverterInterface;
pragma Import (Java, Javax.Xml.Bind.DatatypeConverterInterface, "javax.xml.bind.DatatypeConverterInterface");
pragma Extensions_Allowed (Off);
