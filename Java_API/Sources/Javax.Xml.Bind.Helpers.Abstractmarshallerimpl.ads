pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.OutputStream;
limited with Java.Io.Writer;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Xml.Bind.Annotation.Adapters.XmlAdapter;
limited with Javax.Xml.Bind.Attachment.AttachmentMarshaller;
limited with Javax.Xml.Bind.Marshaller.Listener;
limited with Javax.Xml.Bind.ValidationEventHandler;
limited with Javax.Xml.Stream.XMLEventWriter;
limited with Javax.Xml.Stream.XMLStreamWriter;
limited with Javax.Xml.Validation.Schema;
limited with Org.W3c.Dom.Node;
limited with Org.Xml.Sax.ContentHandler;
with Java.Lang.Object;
with Javax.Xml.Bind.Marshaller;

package Javax.Xml.Bind.Helpers.AbstractMarshallerImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Marshaller_I : Javax.Xml.Bind.Marshaller.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AbstractMarshallerImpl (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_File : access Standard.Java.Io.File.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Writer : access Standard.Java.Io.Writer.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetNode (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Org.W3c.Dom.Node.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  protected
   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   --  protected
   procedure SetEncoding (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function GetSchemaLocation (This : access Typ)
                               return access Java.Lang.String.Typ'Class;

   --  protected
   procedure SetSchemaLocation (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function GetNoNSSchemaLocation (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   --  protected
   procedure SetNoNSSchemaLocation (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class);

   --  protected
   function IsFormattedOutput (This : access Typ)
                               return Java.Boolean;

   --  protected
   procedure SetFormattedOutput (This : access Typ;
                                 P1_Boolean : Java.Boolean);

   --  protected
   function IsFragment (This : access Typ)
                        return Java.Boolean;

   --  protected
   procedure SetFragment (This : access Typ;
                          P1_Boolean : Java.Boolean);

   --  protected
   function GetJavaEncoding (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.String.Typ'Class;
   --  can raise Java.Io.UnsupportedEncodingException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Xml.Bind.PropertyException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Xml.Bind.PropertyException.Except

   function GetEventHandler (This : access Typ)
                             return access Javax.Xml.Bind.ValidationEventHandler.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetEventHandler (This : access Typ;
                              P1_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_XMLEventWriter : access Standard.Javax.Xml.Stream.XMLEventWriter.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_XMLStreamWriter : access Standard.Javax.Xml.Stream.XMLStreamWriter.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetSchema (This : access Typ;
                        P1_Schema : access Standard.Javax.Xml.Validation.Schema.Typ'Class);

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class;

   procedure SetAdapter (This : access Typ;
                         P1_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class);

   procedure SetAdapter (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class);

   function GetAdapter (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class;

   procedure SetAttachmentMarshaller (This : access Typ;
                                      P1_AttachmentMarshaller : access Standard.Javax.Xml.Bind.Attachment.AttachmentMarshaller.Typ'Class);

   function GetAttachmentMarshaller (This : access Typ)
                                     return access Javax.Xml.Bind.Attachment.AttachmentMarshaller.Typ'Class;

   procedure SetListener (This : access Typ;
                          P1_Listener : access Standard.Javax.Xml.Bind.Marshaller.Listener.Typ'Class);

   function GetListener (This : access Typ)
                         return access Javax.Xml.Bind.Marshaller.Listener.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractMarshallerImpl);
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, GetNode, "getNode");
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, SetEncoding, "setEncoding");
   pragma Import (Java, GetSchemaLocation, "getSchemaLocation");
   pragma Import (Java, SetSchemaLocation, "setSchemaLocation");
   pragma Import (Java, GetNoNSSchemaLocation, "getNoNSSchemaLocation");
   pragma Import (Java, SetNoNSSchemaLocation, "setNoNSSchemaLocation");
   pragma Import (Java, IsFormattedOutput, "isFormattedOutput");
   pragma Import (Java, SetFormattedOutput, "setFormattedOutput");
   pragma Import (Java, IsFragment, "isFragment");
   pragma Import (Java, SetFragment, "setFragment");
   pragma Import (Java, GetJavaEncoding, "getJavaEncoding");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, GetEventHandler, "getEventHandler");
   pragma Import (Java, SetEventHandler, "setEventHandler");
   pragma Import (Java, SetSchema, "setSchema");
   pragma Import (Java, GetSchema, "getSchema");
   pragma Import (Java, SetAdapter, "setAdapter");
   pragma Import (Java, GetAdapter, "getAdapter");
   pragma Import (Java, SetAttachmentMarshaller, "setAttachmentMarshaller");
   pragma Import (Java, GetAttachmentMarshaller, "getAttachmentMarshaller");
   pragma Import (Java, SetListener, "setListener");
   pragma Import (Java, GetListener, "getListener");

end Javax.Xml.Bind.Helpers.AbstractMarshallerImpl;
pragma Import (Java, Javax.Xml.Bind.Helpers.AbstractMarshallerImpl, "javax.xml.bind.helpers.AbstractMarshallerImpl");
pragma Extensions_Allowed (Off);
