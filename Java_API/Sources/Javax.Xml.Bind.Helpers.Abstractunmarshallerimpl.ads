pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Xml.Bind.Annotation.Adapters.XmlAdapter;
limited with Javax.Xml.Bind.Attachment.AttachmentUnmarshaller;
limited with Javax.Xml.Bind.JAXBElement;
limited with Javax.Xml.Bind.UnmarshalException;
limited with Javax.Xml.Bind.Unmarshaller.Listener;
limited with Javax.Xml.Bind.ValidationEventHandler;
limited with Javax.Xml.Stream.XMLEventReader;
limited with Javax.Xml.Stream.XMLStreamReader;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Validation.Schema;
limited with Org.W3c.Dom.Node;
limited with Org.Xml.Sax.InputSource;
limited with Org.Xml.Sax.SAXException;
limited with Org.Xml.Sax.XMLReader;
with Java.Lang.Object;
with Javax.Xml.Bind.Unmarshaller;

package Javax.Xml.Bind.Helpers.AbstractUnmarshallerImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Unmarshaller_I : Javax.Xml.Bind.Unmarshaller.Ref)
    is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Validating : Java.Boolean;
      pragma Import (Java, Validating, "validating");

   end record;

   function New_AbstractUnmarshallerImpl (This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function GetXMLReader (This : access Typ)
                          return access Org.Xml.Sax.XMLReader.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  protected
   function Unmarshal (This : access Typ;
                       P1_XMLReader : access Standard.Org.Xml.Sax.XMLReader.Typ'Class;
                       P2_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract with Export => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   function Unmarshal (This : access Typ;
                       P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   function Unmarshal (This : access Typ;
                       P1_URL : access Standard.Java.Net.URL.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   function Unmarshal (This : access Typ;
                       P1_File : access Standard.Java.Io.File.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   function Unmarshal (This : access Typ;
                       P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  final
   function Unmarshal (This : access Typ;
                       P1_Reader : access Standard.Java.Io.Reader.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function IsValidating (This : access Typ)
                          return Java.Boolean;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetEventHandler (This : access Typ;
                              P1_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetValidating (This : access Typ;
                            P1_Boolean : Java.Boolean);
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetEventHandler (This : access Typ)
                             return access Javax.Xml.Bind.ValidationEventHandler.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   --  protected
   function CreateUnmarshalException (This : access Typ;
                                      P1_SAXException : access Standard.Org.Xml.Sax.SAXException.Typ'Class)
                                      return access Javax.Xml.Bind.UnmarshalException.Typ'Class;

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Xml.Bind.PropertyException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Xml.Bind.PropertyException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class)
                       return access Java.Lang.Object.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class with Import => "unmarshal", Convention => Java;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetSchema (This : access Typ;
                        P1_Schema : access Standard.Javax.Xml.Validation.Schema.Typ'Class);

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class;

   procedure SetAdapter (This : access Typ;
                         P1_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class);

   procedure SetAdapter (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class);

   function GetAdapter (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class;

   procedure SetAttachmentUnmarshaller (This : access Typ;
                                        P1_AttachmentUnmarshaller : access Standard.Javax.Xml.Bind.Attachment.AttachmentUnmarshaller.Typ'Class);

   function GetAttachmentUnmarshaller (This : access Typ)
                                       return access Javax.Xml.Bind.Attachment.AttachmentUnmarshaller.Typ'Class;

   procedure SetListener (This : access Typ;
                          P1_Listener : access Standard.Javax.Xml.Bind.Unmarshaller.Listener.Typ'Class);

   function GetListener (This : access Typ)
                         return access Javax.Xml.Bind.Unmarshaller.Listener.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AbstractUnmarshallerImpl);
   pragma Import (Java, GetXMLReader, "getXMLReader");
   -- pragma Import (Java, Unmarshal, "unmarshal");
   pragma Import (Java, IsValidating, "isValidating");
   pragma Import (Java, SetEventHandler, "setEventHandler");
   pragma Import (Java, SetValidating, "setValidating");
   pragma Import (Java, GetEventHandler, "getEventHandler");
   pragma Import (Java, CreateUnmarshalException, "createUnmarshalException");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, SetSchema, "setSchema");
   pragma Import (Java, GetSchema, "getSchema");
   pragma Import (Java, SetAdapter, "setAdapter");
   pragma Import (Java, GetAdapter, "getAdapter");
   pragma Import (Java, SetAttachmentUnmarshaller, "setAttachmentUnmarshaller");
   pragma Import (Java, GetAttachmentUnmarshaller, "getAttachmentUnmarshaller");
   pragma Import (Java, SetListener, "setListener");
   pragma Import (Java, GetListener, "getListener");

end Javax.Xml.Bind.Helpers.AbstractUnmarshallerImpl;
pragma Import (Java, Javax.Xml.Bind.Helpers.AbstractUnmarshallerImpl, "javax.xml.bind.helpers.AbstractUnmarshallerImpl");
pragma Extensions_Allowed (Off);
