pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Xml.Bind.ValidationEventLocator;
with Java.Lang.Object;
with Javax.Xml.Bind.Helpers.ValidationEventImpl;
with Javax.Xml.Bind.NotIdentifiableEvent;
with Javax.Xml.Bind.ValidationEvent;

package Javax.Xml.Bind.Helpers.NotIdentifiableEventImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(NotIdentifiableEvent_I : Javax.Xml.Bind.NotIdentifiableEvent.Ref;
            ValidationEvent_I : Javax.Xml.Bind.ValidationEvent.Ref)
    is new Javax.Xml.Bind.Helpers.ValidationEventImpl.Typ(ValidationEvent_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NotIdentifiableEventImpl (P1_Int : Java.Int;
                                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                                          P3_ValidationEventLocator : access Standard.Javax.Xml.Bind.ValidationEventLocator.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   function New_NotIdentifiableEventImpl (P1_Int : Java.Int;
                                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                                          P3_ValidationEventLocator : access Standard.Javax.Xml.Bind.ValidationEventLocator.Typ'Class;
                                          P4_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NotIdentifiableEventImpl);

end Javax.Xml.Bind.Helpers.NotIdentifiableEventImpl;
pragma Import (Java, Javax.Xml.Bind.Helpers.NotIdentifiableEventImpl, "javax.xml.bind.helpers.NotIdentifiableEventImpl");
pragma Extensions_Allowed (Off);
