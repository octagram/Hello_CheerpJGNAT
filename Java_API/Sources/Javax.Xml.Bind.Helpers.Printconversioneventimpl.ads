pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Xml.Bind.ValidationEventLocator;
with Java.Lang.Object;
with Javax.Xml.Bind.Helpers.ValidationEventImpl;
with Javax.Xml.Bind.PrintConversionEvent;
with Javax.Xml.Bind.ValidationEvent;

package Javax.Xml.Bind.Helpers.PrintConversionEventImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(PrintConversionEvent_I : Javax.Xml.Bind.PrintConversionEvent.Ref;
            ValidationEvent_I : Javax.Xml.Bind.ValidationEvent.Ref)
    is new Javax.Xml.Bind.Helpers.ValidationEventImpl.Typ(ValidationEvent_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PrintConversionEventImpl (P1_Int : Java.Int;
                                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                                          P3_ValidationEventLocator : access Standard.Javax.Xml.Bind.ValidationEventLocator.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   function New_PrintConversionEventImpl (P1_Int : Java.Int;
                                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                                          P3_ValidationEventLocator : access Standard.Javax.Xml.Bind.ValidationEventLocator.Typ'Class;
                                          P4_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_PrintConversionEventImpl);

end Javax.Xml.Bind.Helpers.PrintConversionEventImpl;
pragma Import (Java, Javax.Xml.Bind.Helpers.PrintConversionEventImpl, "javax.xml.bind.helpers.PrintConversionEventImpl");
pragma Extensions_Allowed (Off);
