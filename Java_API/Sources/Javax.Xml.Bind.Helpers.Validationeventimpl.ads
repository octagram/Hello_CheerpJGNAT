pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Xml.Bind.ValidationEventLocator;
with Java.Lang.Object;
with Javax.Xml.Bind.ValidationEvent;

package Javax.Xml.Bind.Helpers.ValidationEventImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ValidationEvent_I : Javax.Xml.Bind.ValidationEvent.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ValidationEventImpl (P1_Int : Java.Int;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_ValidationEventLocator : access Standard.Javax.Xml.Bind.ValidationEventLocator.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_ValidationEventImpl (P1_Int : Java.Int;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_ValidationEventLocator : access Standard.Javax.Xml.Bind.ValidationEventLocator.Typ'Class;
                                     P4_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSeverity (This : access Typ)
                         return Java.Int;

   procedure SetSeverity (This : access Typ;
                          P1_Int : Java.Int);

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetMessage (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetLinkedException (This : access Typ)
                                return access Java.Lang.Throwable.Typ'Class;

   procedure SetLinkedException (This : access Typ;
                                 P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   function GetLocator (This : access Typ)
                        return access Javax.Xml.Bind.ValidationEventLocator.Typ'Class;

   procedure SetLocator (This : access Typ;
                         P1_ValidationEventLocator : access Standard.Javax.Xml.Bind.ValidationEventLocator.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ValidationEventImpl);
   pragma Import (Java, GetSeverity, "getSeverity");
   pragma Import (Java, SetSeverity, "setSeverity");
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, SetMessage, "setMessage");
   pragma Import (Java, GetLinkedException, "getLinkedException");
   pragma Import (Java, SetLinkedException, "setLinkedException");
   pragma Import (Java, GetLocator, "getLocator");
   pragma Import (Java, SetLocator, "setLocator");
   pragma Import (Java, ToString, "toString");

end Javax.Xml.Bind.Helpers.ValidationEventImpl;
pragma Import (Java, Javax.Xml.Bind.Helpers.ValidationEventImpl, "javax.xml.bind.helpers.ValidationEventImpl");
pragma Extensions_Allowed (Off);
