pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Org.W3c.Dom.Node;
limited with Org.Xml.Sax.Locator;
limited with Org.Xml.Sax.SAXParseException;
with Java.Lang.Object;
with Javax.Xml.Bind.ValidationEventLocator;

package Javax.Xml.Bind.Helpers.ValidationEventLocatorImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ValidationEventLocator_I : Javax.Xml.Bind.ValidationEventLocator.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ValidationEventLocatorImpl (This : Ref := null)
                                            return Ref;

   function New_ValidationEventLocatorImpl (P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_ValidationEventLocatorImpl (P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_ValidationEventLocatorImpl (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   function New_ValidationEventLocatorImpl (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                            This : Ref := null)
                                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetURL (This : access Typ)
                    return access Java.Net.URL.Typ'Class;

   procedure SetURL (This : access Typ;
                     P1_URL : access Standard.Java.Net.URL.Typ'Class);

   function GetOffset (This : access Typ)
                       return Java.Int;

   procedure SetOffset (This : access Typ;
                        P1_Int : Java.Int);

   function GetLineNumber (This : access Typ)
                           return Java.Int;

   procedure SetLineNumber (This : access Typ;
                            P1_Int : Java.Int);

   function GetColumnNumber (This : access Typ)
                             return Java.Int;

   procedure SetColumnNumber (This : access Typ;
                              P1_Int : Java.Int);

   function GetObject (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;

   procedure SetObject (This : access Typ;
                        P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetNode (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class;

   procedure SetNode (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ValidationEventLocatorImpl);
   pragma Import (Java, GetURL, "getURL");
   pragma Import (Java, SetURL, "setURL");
   pragma Import (Java, GetOffset, "getOffset");
   pragma Import (Java, SetOffset, "setOffset");
   pragma Import (Java, GetLineNumber, "getLineNumber");
   pragma Import (Java, SetLineNumber, "setLineNumber");
   pragma Import (Java, GetColumnNumber, "getColumnNumber");
   pragma Import (Java, SetColumnNumber, "setColumnNumber");
   pragma Import (Java, GetObject, "getObject");
   pragma Import (Java, SetObject, "setObject");
   pragma Import (Java, GetNode, "getNode");
   pragma Import (Java, SetNode, "setNode");
   pragma Import (Java, ToString, "toString");

end Javax.Xml.Bind.Helpers.ValidationEventLocatorImpl;
pragma Import (Java, Javax.Xml.Bind.Helpers.ValidationEventLocatorImpl, "javax.xml.bind.helpers.ValidationEventLocatorImpl");
pragma Extensions_Allowed (Off);
