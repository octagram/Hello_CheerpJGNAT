pragma Extensions_Allowed (On);
package Javax.Xml.Bind.Helpers is
   pragma Preelaborate;
end Javax.Xml.Bind.Helpers;
pragma Import (Java, Javax.Xml.Bind.Helpers, "javax.xml.bind.helpers");
pragma Extensions_Allowed (Off);
