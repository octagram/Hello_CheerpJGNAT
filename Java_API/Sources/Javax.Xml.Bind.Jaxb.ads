pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Java.Io.Reader;
limited with Java.Io.Writer;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URI;
limited with Java.Net.URL;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Source;
with Java.Lang.Object;

package Javax.Xml.Bind.JAXB is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Unmarshal (P1_File : access Standard.Java.Io.File.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function Unmarshal (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function Unmarshal (P1_URI : access Standard.Java.Net.URI.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function Unmarshal (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function Unmarshal (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function Unmarshal (P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   function Unmarshal (P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Java.Lang.Object.Typ'Class;

   procedure Marshal (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_File : access Standard.Java.Io.File.Typ'Class);

   procedure Marshal (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_URL : access Standard.Java.Net.URL.Typ'Class);

   procedure Marshal (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_URI : access Standard.Java.Net.URI.Typ'Class);

   procedure Marshal (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Marshal (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);

   procedure Marshal (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Writer : access Standard.Java.Io.Writer.Typ'Class);

   procedure Marshal (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Unmarshal, "unmarshal");
   pragma Import (Java, Marshal, "marshal");

end Javax.Xml.Bind.JAXB;
pragma Import (Java, Javax.Xml.Bind.JAXB, "javax.xml.bind.JAXB");
pragma Extensions_Allowed (Off);
