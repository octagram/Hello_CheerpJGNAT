pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Xml.Bind.Binder;
limited with Javax.Xml.Bind.JAXBIntrospector;
limited with Javax.Xml.Bind.Marshaller;
limited with Javax.Xml.Bind.SchemaOutputResolver;
limited with Javax.Xml.Bind.Unmarshaller;
with Java.Lang.Object;

package Javax.Xml.Bind.JAXBContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_JAXBContext (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Bind.JAXBContext.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Javax.Xml.Bind.JAXBContext.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class;
                         P3_Map : access Standard.Java.Util.Map.Typ'Class)
                         return access Javax.Xml.Bind.JAXBContext.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function NewInstance (P1_Class_Arr : access Java.Lang.Class.Arr_Obj)
                         return access Javax.Xml.Bind.JAXBContext.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function NewInstance (P1_Class_Arr : access Java.Lang.Class.Arr_Obj;
                         P2_Map : access Standard.Java.Util.Map.Typ'Class)
                         return access Javax.Xml.Bind.JAXBContext.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function CreateUnmarshaller (This : access Typ)
                                return access Javax.Xml.Bind.Unmarshaller.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function CreateMarshaller (This : access Typ)
                              return access Javax.Xml.Bind.Marshaller.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function CreateBinder (This : access Typ;
                          P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                          return access Javax.Xml.Bind.Binder.Typ'Class;

   function CreateBinder (This : access Typ)
                          return access Javax.Xml.Bind.Binder.Typ'Class;

   function CreateJAXBIntrospector (This : access Typ)
                                    return access Javax.Xml.Bind.JAXBIntrospector.Typ'Class;

   procedure GenerateSchema (This : access Typ;
                             P1_SchemaOutputResolver : access Standard.Javax.Xml.Bind.SchemaOutputResolver.Typ'Class);
   --  can raise Java.Io.IOException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JAXB_CONTEXT_FACTORY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JAXBContext);
   pragma Import (Java, NewInstance, "newInstance");
   pragma Export (Java, CreateUnmarshaller, "createUnmarshaller");
   pragma Export (Java, CreateMarshaller, "createMarshaller");
   pragma Import (Java, CreateBinder, "createBinder");
   pragma Import (Java, CreateJAXBIntrospector, "createJAXBIntrospector");
   pragma Import (Java, GenerateSchema, "generateSchema");
   pragma Import (Java, JAXB_CONTEXT_FACTORY, "JAXB_CONTEXT_FACTORY");

end Javax.Xml.Bind.JAXBContext;
pragma Import (Java, Javax.Xml.Bind.JAXBContext, "javax.xml.bind.JAXBContext");
pragma Extensions_Allowed (Off);
