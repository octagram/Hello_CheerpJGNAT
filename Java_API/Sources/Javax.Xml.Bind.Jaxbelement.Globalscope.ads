pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Xml.Bind.JAXBElement.GlobalScope is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GlobalScope (This : Ref := null)
                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GlobalScope);

end Javax.Xml.Bind.JAXBElement.GlobalScope;
pragma Import (Java, Javax.Xml.Bind.JAXBElement.GlobalScope, "javax.xml.bind.JAXBElement$GlobalScope");
pragma Extensions_Allowed (Off);
