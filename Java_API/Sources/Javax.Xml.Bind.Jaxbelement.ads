pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Javax.Xml.Namespace.QName;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Xml.Bind.JAXBElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected  final
      Name : access Javax.Xml.Namespace.QName.Typ'Class;
      pragma Import (Java, Name, "name");

      --  protected  final
      DeclaredType : access Java.Lang.Class.Typ'Class;
      pragma Import (Java, DeclaredType, "declaredType");

      --  protected  final
      Scope : access Java.Lang.Class.Typ'Class;
      pragma Import (Java, Scope, "scope");

      --  protected
      Value : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Value, "value");

      --  protected
      Nil : Java.Boolean;
      pragma Import (Java, Nil, "nil");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JAXBElement (P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P3_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P4_Object : access Standard.Java.Lang.Object.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_JAXBElement (P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                             P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                             P3_Object : access Standard.Java.Lang.Object.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDeclaredType (This : access Typ)
                             return access Java.Lang.Class.Typ'Class;

   function GetName (This : access Typ)
                     return access Javax.Xml.Namespace.QName.Typ'Class;

   procedure SetValue (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function GetScope (This : access Typ)
                      return access Java.Lang.Class.Typ'Class;

   function IsNil (This : access Typ)
                   return Java.Boolean;

   procedure SetNil (This : access Typ;
                     P1_Boolean : Java.Boolean);

   function IsGlobalScope (This : access Typ)
                           return Java.Boolean;

   function IsTypeSubstituted (This : access Typ)
                               return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JAXBElement);
   pragma Import (Java, GetDeclaredType, "getDeclaredType");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetScope, "getScope");
   pragma Import (Java, IsNil, "isNil");
   pragma Import (Java, SetNil, "setNil");
   pragma Import (Java, IsGlobalScope, "isGlobalScope");
   pragma Import (Java, IsTypeSubstituted, "isTypeSubstituted");

end Javax.Xml.Bind.JAXBElement;
pragma Import (Java, Javax.Xml.Bind.JAXBElement, "javax.xml.bind.JAXBElement");
pragma Extensions_Allowed (Off);
