pragma Extensions_Allowed (On);
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Bind.JAXBIntrospector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_JAXBIntrospector (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsElement (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return Java.Boolean is abstract;

   function GetElementName (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetValue (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JAXBIntrospector);
   pragma Export (Java, IsElement, "isElement");
   pragma Export (Java, GetElementName, "getElementName");
   pragma Export (Java, GetValue, "getValue");

end Javax.Xml.Bind.JAXBIntrospector;
pragma Import (Java, Javax.Xml.Bind.JAXBIntrospector, "javax.xml.bind.JAXBIntrospector");
pragma Extensions_Allowed (Off);
