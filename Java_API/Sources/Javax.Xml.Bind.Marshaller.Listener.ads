pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Xml.Bind.Marshaller.Listener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Listener (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure BeforeMarshal (This : access Typ;
                            P1_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AfterMarshal (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Listener);
   pragma Import (Java, BeforeMarshal, "beforeMarshal");
   pragma Import (Java, AfterMarshal, "afterMarshal");

end Javax.Xml.Bind.Marshaller.Listener;
pragma Import (Java, Javax.Xml.Bind.Marshaller.Listener, "javax.xml.bind.Marshaller$Listener");
pragma Extensions_Allowed (Off);
