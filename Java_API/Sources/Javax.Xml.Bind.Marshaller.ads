pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.OutputStream;
limited with Java.Io.Writer;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Xml.Bind.Annotation.Adapters.XmlAdapter;
limited with Javax.Xml.Bind.Attachment.AttachmentMarshaller;
limited with Javax.Xml.Bind.Marshaller.Listener;
limited with Javax.Xml.Bind.ValidationEventHandler;
limited with Javax.Xml.Stream.XMLEventWriter;
limited with Javax.Xml.Stream.XMLStreamWriter;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Validation.Schema;
limited with Org.W3c.Dom.Node;
limited with Org.Xml.Sax.ContentHandler;
with Java.Lang.Object;

package Javax.Xml.Bind.Marshaller is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_File : access Standard.Java.Io.File.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Writer : access Standard.Java.Io.Writer.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_XMLStreamWriter : access Standard.Javax.Xml.Stream.XMLStreamWriter.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure Marshal (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_XMLEventWriter : access Standard.Javax.Xml.Stream.XMLEventWriter.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetNode (This : access Typ;
                     P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.PropertyException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.PropertyException.Except

   procedure SetEventHandler (This : access Typ;
                              P1_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetEventHandler (This : access Typ)
                             return access Javax.Xml.Bind.ValidationEventHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetAdapter (This : access Typ;
                         P1_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class) is abstract;

   procedure SetAdapter (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class) is abstract;

   function GetAdapter (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class is abstract;

   procedure SetAttachmentMarshaller (This : access Typ;
                                      P1_AttachmentMarshaller : access Standard.Javax.Xml.Bind.Attachment.AttachmentMarshaller.Typ'Class) is abstract;

   function GetAttachmentMarshaller (This : access Typ)
                                     return access Javax.Xml.Bind.Attachment.AttachmentMarshaller.Typ'Class is abstract;

   procedure SetSchema (This : access Typ;
                        P1_Schema : access Standard.Javax.Xml.Validation.Schema.Typ'Class) is abstract;

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class is abstract;

   procedure SetListener (This : access Typ;
                          P1_Listener : access Standard.Javax.Xml.Bind.Marshaller.Listener.Typ'Class) is abstract;

   function GetListener (This : access Typ)
                         return access Javax.Xml.Bind.Marshaller.Listener.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JAXB_ENCODING : constant access Java.Lang.String.Typ'Class;

   --  final
   JAXB_FORMATTED_OUTPUT : constant access Java.Lang.String.Typ'Class;

   --  final
   JAXB_SCHEMA_LOCATION : constant access Java.Lang.String.Typ'Class;

   --  final
   JAXB_NO_NAMESPACE_SCHEMA_LOCATION : constant access Java.Lang.String.Typ'Class;

   --  final
   JAXB_FRAGMENT : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Marshal, "marshal");
   pragma Export (Java, GetNode, "getNode");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, SetEventHandler, "setEventHandler");
   pragma Export (Java, GetEventHandler, "getEventHandler");
   pragma Export (Java, SetAdapter, "setAdapter");
   pragma Export (Java, GetAdapter, "getAdapter");
   pragma Export (Java, SetAttachmentMarshaller, "setAttachmentMarshaller");
   pragma Export (Java, GetAttachmentMarshaller, "getAttachmentMarshaller");
   pragma Export (Java, SetSchema, "setSchema");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, SetListener, "setListener");
   pragma Export (Java, GetListener, "getListener");
   pragma Import (Java, JAXB_ENCODING, "JAXB_ENCODING");
   pragma Import (Java, JAXB_FORMATTED_OUTPUT, "JAXB_FORMATTED_OUTPUT");
   pragma Import (Java, JAXB_SCHEMA_LOCATION, "JAXB_SCHEMA_LOCATION");
   pragma Import (Java, JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "JAXB_NO_NAMESPACE_SCHEMA_LOCATION");
   pragma Import (Java, JAXB_FRAGMENT, "JAXB_FRAGMENT");

end Javax.Xml.Bind.Marshaller;
pragma Import (Java, Javax.Xml.Bind.Marshaller, "javax.xml.bind.Marshaller");
pragma Extensions_Allowed (Off);
