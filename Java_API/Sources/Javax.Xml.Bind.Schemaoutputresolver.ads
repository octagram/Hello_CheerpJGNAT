pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Result;
with Java.Lang.Object;

package Javax.Xml.Bind.SchemaOutputResolver is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SchemaOutputResolver (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateOutput (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Xml.Transform.Result.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SchemaOutputResolver);
   pragma Export (Java, CreateOutput, "createOutput");

end Javax.Xml.Bind.SchemaOutputResolver;
pragma Import (Java, Javax.Xml.Bind.SchemaOutputResolver, "javax.xml.bind.SchemaOutputResolver");
pragma Extensions_Allowed (Off);
