pragma Extensions_Allowed (On);
limited with Java.Io.PrintStream;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Javax.Xml.Bind.TypeConstraintException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TypeConstraintException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_TypeConstraintException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_TypeConstraintException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_TypeConstraintException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   function New_TypeConstraintException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                                         P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                         This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetErrorCode (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetLinkedException (This : access Typ)
                                return access Java.Lang.Throwable.Typ'Class;

   --  synchronized
   procedure SetLinkedException (This : access Typ;
                                 P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   procedure PrintStackTrace (This : access Typ;
                              P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   procedure PrintStackTrace (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.bind.TypeConstraintException");
   pragma Java_Constructor (New_TypeConstraintException);
   pragma Import (Java, GetErrorCode, "getErrorCode");
   pragma Import (Java, GetLinkedException, "getLinkedException");
   pragma Import (Java, SetLinkedException, "setLinkedException");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, PrintStackTrace, "printStackTrace");

end Javax.Xml.Bind.TypeConstraintException;
pragma Import (Java, Javax.Xml.Bind.TypeConstraintException, "javax.xml.bind.TypeConstraintException");
pragma Extensions_Allowed (Off);
