pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Xml.Bind.Unmarshaller.Listener is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Listener (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure BeforeUnmarshal (This : access Typ;
                              P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                              P2_Object : access Standard.Java.Lang.Object.Typ'Class);

   procedure AfterUnmarshal (This : access Typ;
                             P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Listener);
   pragma Import (Java, BeforeUnmarshal, "beforeUnmarshal");
   pragma Import (Java, AfterUnmarshal, "afterUnmarshal");

end Javax.Xml.Bind.Unmarshaller.Listener;
pragma Import (Java, Javax.Xml.Bind.Unmarshaller.Listener, "javax.xml.bind.Unmarshaller$Listener");
pragma Extensions_Allowed (Off);
