pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Xml.Bind.Annotation.Adapters.XmlAdapter;
limited with Javax.Xml.Bind.Attachment.AttachmentUnmarshaller;
limited with Javax.Xml.Bind.JAXBElement;
limited with Javax.Xml.Bind.Unmarshaller.Listener;
limited with Javax.Xml.Bind.UnmarshallerHandler;
limited with Javax.Xml.Bind.ValidationEventHandler;
limited with Javax.Xml.Stream.XMLEventReader;
limited with Javax.Xml.Stream.XMLStreamReader;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Validation.Schema;
limited with Org.W3c.Dom.Node;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;

package Javax.Xml.Bind.Unmarshaller is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Unmarshal (This : access Typ;
                       P1_File : access Standard.Java.Io.File.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Reader : access Standard.Java.Io.Reader.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_URL : access Standard.Java.Net.URL.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function Unmarshal (This : access Typ;
                       P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class;
                       P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                       return access Javax.Xml.Bind.JAXBElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetUnmarshallerHandler (This : access Typ)
                                    return access Javax.Xml.Bind.UnmarshallerHandler.Typ'Class is abstract;

   procedure SetEventHandler (This : access Typ;
                              P1_ValidationEventHandler : access Standard.Javax.Xml.Bind.ValidationEventHandler.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function GetEventHandler (This : access Typ)
                             return access Javax.Xml.Bind.ValidationEventHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Javax.Xml.Bind.PropertyException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.PropertyException.Except

   procedure SetSchema (This : access Typ;
                        P1_Schema : access Standard.Javax.Xml.Validation.Schema.Typ'Class) is abstract;

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class is abstract;

   procedure SetAdapter (This : access Typ;
                         P1_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class) is abstract;

   procedure SetAdapter (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                         P2_XmlAdapter : access Standard.Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class) is abstract;

   function GetAdapter (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Javax.Xml.Bind.Annotation.Adapters.XmlAdapter.Typ'Class is abstract;

   procedure SetAttachmentUnmarshaller (This : access Typ;
                                        P1_AttachmentUnmarshaller : access Standard.Javax.Xml.Bind.Attachment.AttachmentUnmarshaller.Typ'Class) is abstract;

   function GetAttachmentUnmarshaller (This : access Typ)
                                       return access Javax.Xml.Bind.Attachment.AttachmentUnmarshaller.Typ'Class is abstract;

   procedure SetListener (This : access Typ;
                          P1_Listener : access Standard.Javax.Xml.Bind.Unmarshaller.Listener.Typ'Class) is abstract;

   function GetListener (This : access Typ)
                         return access Javax.Xml.Bind.Unmarshaller.Listener.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Unmarshal, "unmarshal");
   pragma Export (Java, GetUnmarshallerHandler, "getUnmarshallerHandler");
   pragma Export (Java, SetEventHandler, "setEventHandler");
   pragma Export (Java, GetEventHandler, "getEventHandler");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, SetSchema, "setSchema");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, SetAdapter, "setAdapter");
   pragma Export (Java, GetAdapter, "getAdapter");
   pragma Export (Java, SetAttachmentUnmarshaller, "setAttachmentUnmarshaller");
   pragma Export (Java, GetAttachmentUnmarshaller, "getAttachmentUnmarshaller");
   pragma Export (Java, SetListener, "setListener");
   pragma Export (Java, GetListener, "getListener");

end Javax.Xml.Bind.Unmarshaller;
pragma Import (Java, Javax.Xml.Bind.Unmarshaller, "javax.xml.bind.Unmarshaller");
pragma Extensions_Allowed (Off);
