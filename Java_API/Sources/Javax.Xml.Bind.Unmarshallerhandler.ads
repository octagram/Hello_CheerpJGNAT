pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Xml.Sax.ContentHandler;

package Javax.Xml.Bind.UnmarshallerHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ContentHandler_I : Org.Xml.Sax.ContentHandler.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetResult (This : access Typ)
                       return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Bind.JAXBException.Except and
   --  Java.Lang.IllegalStateException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetResult, "getResult");

end Javax.Xml.Bind.UnmarshallerHandler;
pragma Import (Java, Javax.Xml.Bind.UnmarshallerHandler, "javax.xml.bind.UnmarshallerHandler");
pragma Extensions_Allowed (Off);
