pragma Extensions_Allowed (On);
limited with Javax.Xml.Bind.JAXBContext;
limited with Javax.Xml.Bind.Unmarshaller;
with Java.Lang.Object;
with Javax.Xml.Transform.Result;
with Javax.Xml.Transform.Sax.SAXResult;

package Javax.Xml.Bind.Util.JAXBResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Result_I : Javax.Xml.Transform.Result.Ref)
    is new Javax.Xml.Transform.Sax.SAXResult.Typ(Result_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JAXBResult (P1_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function New_JAXBResult (P1_Unmarshaller : access Standard.Javax.Xml.Bind.Unmarshaller.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetResult (This : access Typ)
                       return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Xml.Bind.JAXBException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JAXBResult);
   pragma Import (Java, GetResult, "getResult");

end Javax.Xml.Bind.Util.JAXBResult;
pragma Import (Java, Javax.Xml.Bind.Util.JAXBResult, "javax.xml.bind.util.JAXBResult");
pragma Extensions_Allowed (Off);
