pragma Extensions_Allowed (On);
limited with Javax.Xml.Bind.JAXBContext;
limited with Javax.Xml.Bind.Marshaller;
with Java.Lang.Object;
with Javax.Xml.Transform.Sax.SAXSource;
with Javax.Xml.Transform.Source;

package Javax.Xml.Bind.Util.JAXBSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Source_I : Javax.Xml.Transform.Source.Ref)
    is new Javax.Xml.Transform.Sax.SAXSource.Typ(Source_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_JAXBSource (P1_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Bind.JAXBException.Except

   function New_JAXBSource (P1_Marshaller : access Standard.Javax.Xml.Bind.Marshaller.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Bind.JAXBException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_JAXBSource);

end Javax.Xml.Bind.Util.JAXBSource;
pragma Import (Java, Javax.Xml.Bind.Util.JAXBSource, "javax.xml.bind.util.JAXBSource");
pragma Extensions_Allowed (Off);
