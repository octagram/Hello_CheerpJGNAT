pragma Extensions_Allowed (On);
limited with Javax.Xml.Bind.ValidationEvent;
with Java.Lang.Object;
with Javax.Xml.Bind.ValidationEventHandler;

package Javax.Xml.Bind.Util.ValidationEventCollector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ValidationEventHandler_I : Javax.Xml.Bind.ValidationEventHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ValidationEventCollector (This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEvents (This : access Typ)
                       return Standard.Java.Lang.Object.Ref;

   procedure Reset (This : access Typ);

   function HasEvents (This : access Typ)
                       return Java.Boolean;

   function HandleEvent (This : access Typ;
                         P1_ValidationEvent : access Standard.Javax.Xml.Bind.ValidationEvent.Typ'Class)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ValidationEventCollector);
   pragma Import (Java, GetEvents, "getEvents");
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, HasEvents, "hasEvents");
   pragma Import (Java, HandleEvent, "handleEvent");

end Javax.Xml.Bind.Util.ValidationEventCollector;
pragma Import (Java, Javax.Xml.Bind.Util.ValidationEventCollector, "javax.xml.bind.util.ValidationEventCollector");
pragma Extensions_Allowed (Off);
