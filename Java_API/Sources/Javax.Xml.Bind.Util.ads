pragma Extensions_Allowed (On);
package Javax.Xml.Bind.Util is
   pragma Preelaborate;
end Javax.Xml.Bind.Util;
pragma Import (Java, Javax.Xml.Bind.Util, "javax.xml.bind.util");
pragma Extensions_Allowed (Off);
