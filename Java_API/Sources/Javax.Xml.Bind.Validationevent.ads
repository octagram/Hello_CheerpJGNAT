pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Xml.Bind.ValidationEventLocator;
with Java.Lang.Object;

package Javax.Xml.Bind.ValidationEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSeverity (This : access Typ)
                         return Java.Int is abstract;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function GetLinkedException (This : access Typ)
                                return access Java.Lang.Throwable.Typ'Class is abstract;

   function GetLocator (This : access Typ)
                        return access Javax.Xml.Bind.ValidationEventLocator.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WARNING : constant Java.Int;

   --  final
   ERROR : constant Java.Int;

   --  final
   FATAL_ERROR : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSeverity, "getSeverity");
   pragma Export (Java, GetMessage, "getMessage");
   pragma Export (Java, GetLinkedException, "getLinkedException");
   pragma Export (Java, GetLocator, "getLocator");
   pragma Import (Java, WARNING, "WARNING");
   pragma Import (Java, ERROR, "ERROR");
   pragma Import (Java, FATAL_ERROR, "FATAL_ERROR");

end Javax.Xml.Bind.ValidationEvent;
pragma Import (Java, Javax.Xml.Bind.ValidationEvent, "javax.xml.bind.ValidationEvent");
pragma Extensions_Allowed (Off);
