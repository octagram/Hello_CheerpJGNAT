pragma Extensions_Allowed (On);
limited with Javax.Xml.Bind.ValidationEvent;
with Java.Lang.Object;

package Javax.Xml.Bind.ValidationEventHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function HandleEvent (This : access Typ;
                         P1_ValidationEvent : access Standard.Javax.Xml.Bind.ValidationEvent.Typ'Class)
                         return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, HandleEvent, "handleEvent");

end Javax.Xml.Bind.ValidationEventHandler;
pragma Import (Java, Javax.Xml.Bind.ValidationEventHandler, "javax.xml.bind.ValidationEventHandler");
pragma Extensions_Allowed (Off);
