pragma Extensions_Allowed (On);
limited with Java.Net.URL;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Javax.Xml.Bind.ValidationEventLocator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetURL (This : access Typ)
                    return access Java.Net.URL.Typ'Class is abstract;

   function GetOffset (This : access Typ)
                       return Java.Int is abstract;

   function GetLineNumber (This : access Typ)
                           return Java.Int is abstract;

   function GetColumnNumber (This : access Typ)
                             return Java.Int is abstract;

   function GetObject (This : access Typ)
                       return access Java.Lang.Object.Typ'Class is abstract;

   function GetNode (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetURL, "getURL");
   pragma Export (Java, GetOffset, "getOffset");
   pragma Export (Java, GetLineNumber, "getLineNumber");
   pragma Export (Java, GetColumnNumber, "getColumnNumber");
   pragma Export (Java, GetObject, "getObject");
   pragma Export (Java, GetNode, "getNode");

end Javax.Xml.Bind.ValidationEventLocator;
pragma Import (Java, Javax.Xml.Bind.ValidationEventLocator, "javax.xml.bind.ValidationEventLocator");
pragma Extensions_Allowed (Off);
