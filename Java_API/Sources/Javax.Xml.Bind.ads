pragma Extensions_Allowed (On);
package Javax.Xml.Bind is
   pragma Preelaborate;
end Javax.Xml.Bind;
pragma Import (Java, Javax.Xml.Bind, "javax.xml.bind");
pragma Extensions_Allowed (Off);
