pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;

package Javax.Xml.Crypto.AlgorithmMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAlgorithm, "getAlgorithm");
   pragma Export (Java, GetParameterSpec, "getParameterSpec");

end Javax.Xml.Crypto.AlgorithmMethod;
pragma Import (Java, Javax.Xml.Crypto.AlgorithmMethod, "javax.xml.crypto.AlgorithmMethod");
pragma Extensions_Allowed (Off);
