pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Xml.Crypto.KeySelector;
limited with Javax.Xml.Crypto.URIDereferencer;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLCryptoContext;

package Javax.Xml.Crypto.Dom.DOMCryptoContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLCryptoContext_I : Javax.Xml.Crypto.XMLCryptoContext.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_DOMCryptoContext (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNamespacePrefix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function PutNamespacePrefix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   function GetDefaultNamespacePrefix (This : access Typ)
                                       return access Java.Lang.String.Typ'Class;

   procedure SetDefaultNamespacePrefix (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetBaseURI (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   procedure SetBaseURI (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetURIDereferencer (This : access Typ)
                                return access Javax.Xml.Crypto.URIDereferencer.Typ'Class;

   procedure SetURIDereferencer (This : access Typ;
                                 P1_URIDereferencer : access Standard.Javax.Xml.Crypto.URIDereferencer.Typ'Class);

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function SetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;

   function GetKeySelector (This : access Typ)
                            return access Javax.Xml.Crypto.KeySelector.Typ'Class;

   procedure SetKeySelector (This : access Typ;
                             P1_KeySelector : access Standard.Javax.Xml.Crypto.KeySelector.Typ'Class);

   function GetElementById (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Org.W3c.Dom.Element.Typ'Class;

   procedure SetIdAttributeNS (This : access Typ;
                               P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_String : access Standard.Java.Lang.String.Typ'Class);

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMCryptoContext);
   pragma Import (Java, GetNamespacePrefix, "getNamespacePrefix");
   pragma Import (Java, PutNamespacePrefix, "putNamespacePrefix");
   pragma Import (Java, GetDefaultNamespacePrefix, "getDefaultNamespacePrefix");
   pragma Import (Java, SetDefaultNamespacePrefix, "setDefaultNamespacePrefix");
   pragma Import (Java, GetBaseURI, "getBaseURI");
   pragma Import (Java, SetBaseURI, "setBaseURI");
   pragma Import (Java, GetURIDereferencer, "getURIDereferencer");
   pragma Import (Java, SetURIDereferencer, "setURIDereferencer");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetKeySelector, "getKeySelector");
   pragma Import (Java, SetKeySelector, "setKeySelector");
   pragma Import (Java, GetElementById, "getElementById");
   pragma Import (Java, SetIdAttributeNS, "setIdAttributeNS");
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, Get, "get");
   pragma Import (Java, Put, "put");

end Javax.Xml.Crypto.Dom.DOMCryptoContext;
pragma Import (Java, Javax.Xml.Crypto.Dom.DOMCryptoContext, "javax.xml.crypto.dom.DOMCryptoContext");
pragma Extensions_Allowed (Off);
