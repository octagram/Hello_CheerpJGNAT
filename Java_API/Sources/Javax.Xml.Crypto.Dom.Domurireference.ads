pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.URIReference;

package Javax.Xml.Crypto.Dom.DOMURIReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            URIReference_I : Javax.Xml.Crypto.URIReference.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHere (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetHere, "getHere");

end Javax.Xml.Crypto.Dom.DOMURIReference;
pragma Import (Java, Javax.Xml.Crypto.Dom.DOMURIReference, "javax.xml.crypto.dom.DOMURIReference");
pragma Extensions_Allowed (Off);
