pragma Extensions_Allowed (On);
package Javax.Xml.Crypto.Dom is
   pragma Preelaborate;
end Javax.Xml.Crypto.Dom;
pragma Import (Java, Javax.Xml.Crypto.Dom, "javax.xml.crypto.dom");
pragma Extensions_Allowed (Off);
