pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Transform;

package Javax.Xml.Crypto.Dsig.CanonicalizationMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Transform_I : Javax.Xml.Crypto.Dsig.Transform.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INCLUSIVE : constant access Java.Lang.String.Typ'Class;

   --  final
   INCLUSIVE_WITH_COMMENTS : constant access Java.Lang.String.Typ'Class;

   --  final
   EXCLUSIVE : constant access Java.Lang.String.Typ'Class;

   --  final
   EXCLUSIVE_WITH_COMMENTS : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParameterSpec, "getParameterSpec");
   pragma Import (Java, INCLUSIVE, "INCLUSIVE");
   pragma Import (Java, INCLUSIVE_WITH_COMMENTS, "INCLUSIVE_WITH_COMMENTS");
   pragma Import (Java, EXCLUSIVE, "EXCLUSIVE");
   pragma Import (Java, EXCLUSIVE_WITH_COMMENTS, "EXCLUSIVE_WITH_COMMENTS");

end Javax.Xml.Crypto.Dsig.CanonicalizationMethod;
pragma Import (Java, Javax.Xml.Crypto.Dsig.CanonicalizationMethod, "javax.xml.crypto.dsig.CanonicalizationMethod");
pragma Extensions_Allowed (Off);
