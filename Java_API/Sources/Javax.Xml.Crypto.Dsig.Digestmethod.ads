pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;
with Javax.Xml.Crypto.AlgorithmMethod;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.DigestMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AlgorithmMethod_I : Javax.Xml.Crypto.AlgorithmMethod.Ref;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SHA1 : constant access Java.Lang.String.Typ'Class;

   --  final
   SHA256 : constant access Java.Lang.String.Typ'Class;

   --  final
   SHA512 : constant access Java.Lang.String.Typ'Class;

   --  final
   RIPEMD160 : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParameterSpec, "getParameterSpec");
   pragma Import (Java, SHA1, "SHA1");
   pragma Import (Java, SHA256, "SHA256");
   pragma Import (Java, SHA512, "SHA512");
   pragma Import (Java, RIPEMD160, "RIPEMD160");

end Javax.Xml.Crypto.Dsig.DigestMethod;
pragma Import (Java, Javax.Xml.Crypto.Dsig.DigestMethod, "javax.xml.crypto.dsig.DigestMethod");
pragma Extensions_Allowed (Off);
