pragma Extensions_Allowed (On);
limited with Java.Security.Key;
limited with Javax.Xml.Crypto.KeySelector;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dom.DOMCryptoContext;
with Javax.Xml.Crypto.Dsig.XMLValidateContext;
with Javax.Xml.Crypto.XMLCryptoContext;

package Javax.Xml.Crypto.Dsig.Dom.DOMValidateContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLCryptoContext_I : Javax.Xml.Crypto.XMLCryptoContext.Ref;
            XMLValidateContext_I : Javax.Xml.Crypto.Dsig.XMLValidateContext.Ref)
    is new Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ(XMLCryptoContext_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMValidateContext (P1_KeySelector : access Standard.Javax.Xml.Crypto.KeySelector.Typ'Class;
                                    P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_DOMValidateContext (P1_Key : access Standard.Java.Security.Key.Typ'Class;
                                    P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetNode (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);

   function GetNode (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMValidateContext);
   pragma Import (Java, SetNode, "setNode");
   pragma Import (Java, GetNode, "getNode");

end Javax.Xml.Crypto.Dsig.Dom.DOMValidateContext;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Dom.DOMValidateContext, "javax.xml.crypto.dsig.dom.DOMValidateContext");
pragma Extensions_Allowed (Off);
