pragma Extensions_Allowed (On);
package Javax.Xml.Crypto.Dsig.Dom is
   pragma Preelaborate;
end Javax.Xml.Crypto.Dsig.Dom;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Dom, "javax.xml.crypto.dsig.dom");
pragma Extensions_Allowed (Off);
