pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.XMLCryptoContext;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContent (This : access Typ)
                        return access Java.Util.List.Typ'Class is abstract;

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   procedure Marshal (This : access Typ;
                      P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                      P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class) is abstract;
   --  can raise Javax.Xml.Crypto.MarshalException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetContent, "getContent");
   pragma Export (Java, GetId, "getId");
   pragma Export (Java, Marshal, "marshal");

end Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo, "javax.xml.crypto.dsig.keyinfo.KeyInfo");
pragma Extensions_Allowed (Off);
