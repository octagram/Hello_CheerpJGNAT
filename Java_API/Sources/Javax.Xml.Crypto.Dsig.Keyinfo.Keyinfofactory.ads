pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Security.Provider;
limited with Java.Security.PublicKey;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyName;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.PGPData;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.X509Data;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial;
limited with Javax.Xml.Crypto.URIDereferencer;
limited with Javax.Xml.Crypto.XMLStructure;
with Java.Lang.Object;

package Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_KeyInfoFactory (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory.Typ'Class;
   --  can raise Java.Security.NoSuchProviderException.Except

   function GetInstance return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory.Typ'Class;

   --  final
   function GetMechanismType (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   function NewKeyInfo (This : access Typ;
                        P1_List : access Standard.Java.Util.List.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class is abstract;

   function NewKeyInfo (This : access Typ;
                        P1_List : access Standard.Java.Util.List.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class is abstract;

   function NewKeyName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyName.Typ'Class is abstract;

   function NewKeyValue (This : access Typ;
                         P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue.Typ'Class is abstract;
   --  can raise Java.Security.KeyException.Except

   function NewPGPData (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.PGPData.Typ'Class is abstract;

   function NewPGPData (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Byte_Arr : Java.Byte_Arr;
                        P3_List : access Standard.Java.Util.List.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.PGPData.Typ'Class is abstract;

   function NewPGPData (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_List : access Standard.Java.Util.List.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.PGPData.Typ'Class is abstract;

   function NewRetrievalMethod (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod.Typ'Class is abstract;

   function NewRetrievalMethod (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_List : access Standard.Java.Util.List.Typ'Class)
                                return access Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod.Typ'Class is abstract;

   function NewX509Data (This : access Typ;
                         P1_List : access Standard.Java.Util.List.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Keyinfo.X509Data.Typ'Class is abstract;

   function NewX509IssuerSerial (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                                 return access Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial.Typ'Class is abstract;

   function IsFeatureSupported (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean is abstract;

   function GetURIDereferencer (This : access Typ)
                                return access Javax.Xml.Crypto.URIDereferencer.Typ'Class is abstract;

   function UnmarshalKeyInfo (This : access Typ;
                              P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class)
                              return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class is abstract;
   --  can raise Javax.Xml.Crypto.MarshalException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeyInfoFactory);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetMechanismType, "getMechanismType");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Export (Java, NewKeyInfo, "newKeyInfo");
   pragma Export (Java, NewKeyName, "newKeyName");
   pragma Export (Java, NewKeyValue, "newKeyValue");
   pragma Export (Java, NewPGPData, "newPGPData");
   pragma Export (Java, NewRetrievalMethod, "newRetrievalMethod");
   pragma Export (Java, NewX509Data, "newX509Data");
   pragma Export (Java, NewX509IssuerSerial, "newX509IssuerSerial");
   pragma Export (Java, IsFeatureSupported, "isFeatureSupported");
   pragma Export (Java, GetURIDereferencer, "getURIDereferencer");
   pragma Export (Java, UnmarshalKeyInfo, "unmarshalKeyInfo");

end Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory, "javax.xml.crypto.dsig.keyinfo.KeyInfoFactory");
pragma Extensions_Allowed (Off);
