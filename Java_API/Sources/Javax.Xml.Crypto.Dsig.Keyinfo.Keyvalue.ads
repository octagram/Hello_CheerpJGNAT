pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.PublicKey;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublicKey (This : access Typ)
                          return access Java.Security.PublicKey.Typ'Class is abstract;
   --  can raise Java.Security.KeyException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DSA_TYPE : constant access Java.Lang.String.Typ'Class;

   --  final
   RSA_TYPE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPublicKey, "getPublicKey");
   pragma Import (Java, DSA_TYPE, "DSA_TYPE");
   pragma Import (Java, RSA_TYPE, "RSA_TYPE");

end Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue, "javax.xml.crypto.dsig.keyinfo.KeyValue");
pragma Extensions_Allowed (Off);
