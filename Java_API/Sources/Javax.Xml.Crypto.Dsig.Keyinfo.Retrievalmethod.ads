pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.XMLCryptoContext;
with Java.Lang.Object;
with Javax.Xml.Crypto.URIReference;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            URIReference_I : Javax.Xml.Crypto.URIReference.Ref;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransforms (This : access Typ)
                           return access Java.Util.List.Typ'Class is abstract;

   function GetURI (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   function Dereference (This : access Typ;
                         P1_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                         return access Javax.Xml.Crypto.Data.Typ'Class is abstract;
   --  can raise Javax.Xml.Crypto.URIReferenceException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTransforms, "getTransforms");
   pragma Export (Java, GetURI, "getURI");
   pragma Export (Java, Dereference, "dereference");

end Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod, "javax.xml.crypto.dsig.keyinfo.RetrievalMethod");
pragma Extensions_Allowed (Off);
