pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.Keyinfo.X509Data is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContent (This : access Typ)
                        return access Java.Util.List.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   TYPE_K : constant access Java.Lang.String.Typ'Class;

   --  final
   RAW_X509_CERTIFICATE_TYPE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetContent, "getContent");
   pragma Import (Java, TYPE_K, "TYPE");
   pragma Import (Java, RAW_X509_CERTIFICATE_TYPE, "RAW_X509_CERTIFICATE_TYPE");

end Javax.Xml.Crypto.Dsig.Keyinfo.X509Data;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Keyinfo.X509Data, "javax.xml.crypto.dsig.keyinfo.X509Data");
pragma Extensions_Allowed (Off);
