pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIssuerName (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetSerialNumber (This : access Typ)
                             return access Java.Math.BigInteger.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetIssuerName, "getIssuerName");
   pragma Export (Java, GetSerialNumber, "getSerialNumber");

end Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial, "javax.xml.crypto.dsig.keyinfo.X509IssuerSerial");
pragma Extensions_Allowed (Off);
