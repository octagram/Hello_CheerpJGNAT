pragma Extensions_Allowed (On);
package Javax.Xml.Crypto.Dsig.Keyinfo is
   pragma Preelaborate;
end Javax.Xml.Crypto.Dsig.Keyinfo;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Keyinfo, "javax.xml.crypto.dsig.keyinfo");
pragma Extensions_Allowed (Off);
