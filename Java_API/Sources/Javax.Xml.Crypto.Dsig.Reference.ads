pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dsig.DigestMethod;
limited with Javax.Xml.Crypto.Dsig.XMLValidateContext;
with Java.Lang.Object;
with Javax.Xml.Crypto.URIReference;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.Reference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            URIReference_I : Javax.Xml.Crypto.URIReference.Ref;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTransforms (This : access Typ)
                           return access Java.Util.List.Typ'Class is abstract;

   function GetDigestMethod (This : access Typ)
                             return access Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class is abstract;

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function GetDigestValue (This : access Typ)
                            return Java.Byte_Arr is abstract;

   function GetCalculatedDigestValue (This : access Typ)
                                      return Java.Byte_Arr is abstract;

   function Validate (This : access Typ;
                      P1_XMLValidateContext : access Standard.Javax.Xml.Crypto.Dsig.XMLValidateContext.Typ'Class)
                      return Java.Boolean is abstract;
   --  can raise Javax.Xml.Crypto.Dsig.XMLSignatureException.Except

   function GetDereferencedData (This : access Typ)
                                 return access Javax.Xml.Crypto.Data.Typ'Class is abstract;

   function GetDigestInputStream (This : access Typ)
                                  return access Java.Io.InputStream.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTransforms, "getTransforms");
   pragma Export (Java, GetDigestMethod, "getDigestMethod");
   pragma Export (Java, GetId, "getId");
   pragma Export (Java, GetDigestValue, "getDigestValue");
   pragma Export (Java, GetCalculatedDigestValue, "getCalculatedDigestValue");
   pragma Export (Java, Validate, "validate");
   pragma Export (Java, GetDereferencedData, "getDereferencedData");
   pragma Export (Java, GetDigestInputStream, "getDigestInputStream");

end Javax.Xml.Crypto.Dsig.Reference;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Reference, "javax.xml.crypto.dsig.Reference");
pragma Extensions_Allowed (Off);
