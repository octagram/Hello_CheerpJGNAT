pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
with Java.Lang.Object;
with Javax.Xml.Crypto.AlgorithmMethod;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.SignatureMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AlgorithmMethod_I : Javax.Xml.Crypto.AlgorithmMethod.Ref;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DSA_SHA1 : constant access Java.Lang.String.Typ'Class;

   --  final
   RSA_SHA1 : constant access Java.Lang.String.Typ'Class;

   --  final
   HMAC_SHA1 : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParameterSpec, "getParameterSpec");
   pragma Import (Java, DSA_SHA1, "DSA_SHA1");
   pragma Import (Java, RSA_SHA1, "RSA_SHA1");
   pragma Import (Java, HMAC_SHA1, "HMAC_SHA1");

end Javax.Xml.Crypto.Dsig.SignatureMethod;
pragma Import (Java, Javax.Xml.Crypto.Dsig.SignatureMethod, "javax.xml.crypto.dsig.SignatureMethod");
pragma Extensions_Allowed (Off);
