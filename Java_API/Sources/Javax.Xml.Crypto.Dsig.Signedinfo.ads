pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dsig.CanonicalizationMethod;
limited with Javax.Xml.Crypto.Dsig.SignatureMethod;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.SignedInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCanonicalizationMethod (This : access Typ)
                                       return access Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class is abstract;

   function GetSignatureMethod (This : access Typ)
                                return access Javax.Xml.Crypto.Dsig.SignatureMethod.Typ'Class is abstract;

   function GetReferences (This : access Typ)
                           return access Java.Util.List.Typ'Class is abstract;

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function GetCanonicalizedData (This : access Typ)
                                  return access Java.Io.InputStream.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCanonicalizationMethod, "getCanonicalizationMethod");
   pragma Export (Java, GetSignatureMethod, "getSignatureMethod");
   pragma Export (Java, GetReferences, "getReferences");
   pragma Export (Java, GetId, "getId");
   pragma Export (Java, GetCanonicalizedData, "getCanonicalizedData");

end Javax.Xml.Crypto.Dsig.SignedInfo;
pragma Import (Java, Javax.Xml.Crypto.Dsig.SignedInfo, "javax.xml.crypto.dsig.SignedInfo");
pragma Extensions_Allowed (Off);
