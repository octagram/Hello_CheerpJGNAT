pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Spec.C14NMethodParameterSpec;

package Javax.Xml.Crypto.Dsig.Spec.ExcC14NParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(C14NMethodParameterSpec_I : Javax.Xml.Crypto.Dsig.Spec.C14NMethodParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ExcC14NParameterSpec (This : Ref := null)
                                      return Ref;

   function New_ExcC14NParameterSpec (P1_List : access Standard.Java.Util.List.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrefixList (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ExcC14NParameterSpec);
   pragma Import (Java, GetPrefixList, "getPrefixList");
   pragma Import (Java, DEFAULT, "DEFAULT");

end Javax.Xml.Crypto.Dsig.Spec.ExcC14NParameterSpec;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Spec.ExcC14NParameterSpec, "javax.xml.crypto.dsig.spec.ExcC14NParameterSpec");
pragma Extensions_Allowed (Off);
