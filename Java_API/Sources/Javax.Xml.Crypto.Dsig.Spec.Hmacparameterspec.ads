pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Spec.SignatureMethodParameterSpec;

package Javax.Xml.Crypto.Dsig.Spec.HMACParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(SignatureMethodParameterSpec_I : Javax.Xml.Crypto.Dsig.Spec.SignatureMethodParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HMACParameterSpec (P1_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOutputLength (This : access Typ)
                             return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HMACParameterSpec);
   pragma Import (Java, GetOutputLength, "getOutputLength");

end Javax.Xml.Crypto.Dsig.Spec.HMACParameterSpec;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Spec.HMACParameterSpec, "javax.xml.crypto.dsig.spec.HMACParameterSpec");
pragma Extensions_Allowed (Off);
