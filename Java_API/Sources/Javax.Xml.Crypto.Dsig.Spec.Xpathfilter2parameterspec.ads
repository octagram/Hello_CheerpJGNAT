pragma Extensions_Allowed (On);
limited with Java.Util.List;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec;

package Javax.Xml.Crypto.Dsig.Spec.XPathFilter2ParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TransformParameterSpec_I : Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XPathFilter2ParameterSpec (P1_List : access Standard.Java.Util.List.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXPathList (This : access Typ)
                          return access Java.Util.List.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XPathFilter2ParameterSpec);
   pragma Import (Java, GetXPathList, "getXPathList");

end Javax.Xml.Crypto.Dsig.Spec.XPathFilter2ParameterSpec;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Spec.XPathFilter2ParameterSpec, "javax.xml.crypto.dsig.spec.XPathFilter2ParameterSpec");
pragma Extensions_Allowed (Off);
