pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec;

package Javax.Xml.Crypto.Dsig.Spec.XPathFilterParameterSpec is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(TransformParameterSpec_I : Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XPathFilterParameterSpec (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   function New_XPathFilterParameterSpec (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                          P2_Map : access Standard.Java.Util.Map.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXPath (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetNamespaceMap (This : access Typ)
                             return access Java.Util.Map.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XPathFilterParameterSpec);
   pragma Import (Java, GetXPath, "getXPath");
   pragma Import (Java, GetNamespaceMap, "getNamespaceMap");

end Javax.Xml.Crypto.Dsig.Spec.XPathFilterParameterSpec;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Spec.XPathFilterParameterSpec, "javax.xml.crypto.dsig.spec.XPathFilterParameterSpec");
pragma Extensions_Allowed (Off);
