pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INTERSECT : access Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter.Typ'Class;

   --  final
   SUBTRACT : access Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter.Typ'Class;

   --  final
   UNION : access Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, INTERSECT, "INTERSECT");
   pragma Import (Java, SUBTRACT, "SUBTRACT");
   pragma Import (Java, UNION, "UNION");

end Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter, "javax.xml.crypto.dsig.spec.XPathType$Filter");
pragma Extensions_Allowed (Off);
