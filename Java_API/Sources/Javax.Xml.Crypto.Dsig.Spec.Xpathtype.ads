pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter;
with Java.Lang.Object;

package Javax.Xml.Crypto.Dsig.Spec.XPathType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XPathType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Filter : access Standard.Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_XPathType (P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Filter : access Standard.Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter.Typ'Class;
                           P3_Map : access Standard.Java.Util.Map.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetExpression (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetFilter (This : access Typ)
                       return access Javax.Xml.Crypto.Dsig.Spec.XPathType.Filter.Typ'Class;

   function GetNamespaceMap (This : access Typ)
                             return access Java.Util.Map.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XPathType);
   pragma Import (Java, GetExpression, "getExpression");
   pragma Import (Java, GetFilter, "getFilter");
   pragma Import (Java, GetNamespaceMap, "getNamespaceMap");

end Javax.Xml.Crypto.Dsig.Spec.XPathType;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Spec.XPathType, "javax.xml.crypto.dsig.spec.XPathType");
pragma Extensions_Allowed (Off);
