pragma Extensions_Allowed (On);
package Javax.Xml.Crypto.Dsig.Spec is
   pragma Preelaborate;
end Javax.Xml.Crypto.Dsig.Spec;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Spec, "javax.xml.crypto.dsig.spec");
pragma Extensions_Allowed (Off);
