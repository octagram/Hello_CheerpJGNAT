pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.XMLCryptoContext;
with Java.Lang.Object;
with Javax.Xml.Crypto.AlgorithmMethod;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.Transform is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            AlgorithmMethod_I : Javax.Xml.Crypto.AlgorithmMethod.Ref;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class is abstract;

   function Transform (This : access Typ;
                       P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                       P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                       return access Javax.Xml.Crypto.Data.Typ'Class is abstract;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   function Transform (This : access Typ;
                       P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                       P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                       P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                       return access Javax.Xml.Crypto.Data.Typ'Class is abstract;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BASE64 : constant access Java.Lang.String.Typ'Class;

   --  final
   ENVELOPED : constant access Java.Lang.String.Typ'Class;

   --  final
   XPATH : constant access Java.Lang.String.Typ'Class;

   --  final
   XPATH2 : constant access Java.Lang.String.Typ'Class;

   --  final
   XSLT : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetParameterSpec, "getParameterSpec");
   pragma Export (Java, Transform, "transform");
   pragma Import (Java, BASE64, "BASE64");
   pragma Import (Java, ENVELOPED, "ENVELOPED");
   pragma Import (Java, XPATH, "XPATH");
   pragma Import (Java, XPATH2, "XPATH2");
   pragma Import (Java, XSLT, "XSLT");

end Javax.Xml.Crypto.Dsig.Transform;
pragma Import (Java, Javax.Xml.Crypto.Dsig.Transform, "javax.xml.crypto.dsig.Transform");
pragma Extensions_Allowed (Off);
