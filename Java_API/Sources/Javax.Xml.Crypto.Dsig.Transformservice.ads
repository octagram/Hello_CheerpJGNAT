pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Javax.Xml.Crypto.XMLStructure;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Transform;

package Javax.Xml.Crypto.Dsig.TransformService is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transform_I : Javax.Xml.Crypto.Dsig.Transform.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_TransformService (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.TransformService.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.TransformService.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.TransformService.Typ'Class;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.NoSuchProviderException.Except

   --  final
   function GetMechanismType (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   procedure Init (This : access Typ;
                   P1_TransformParameterSpec : access Standard.Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec.Typ'Class) is abstract;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   procedure MarshalParams (This : access Typ;
                            P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                            P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class) is abstract;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   procedure Init (This : access Typ;
                   P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                   P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class) is abstract;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TransformService);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Import (Java, GetMechanismType, "getMechanismType");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, GetProvider, "getProvider");
   pragma Export (Java, Init, "init");
   pragma Export (Java, MarshalParams, "marshalParams");

end Javax.Xml.Crypto.Dsig.TransformService;
pragma Import (Java, Javax.Xml.Crypto.Dsig.TransformService, "javax.xml.crypto.dsig.TransformService");
pragma Extensions_Allowed (Off);
