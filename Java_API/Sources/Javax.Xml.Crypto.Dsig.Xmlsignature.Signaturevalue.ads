pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Crypto.Dsig.XMLValidateContext;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.XMLSignature.SignatureValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function GetValue (This : access Typ)
                      return Java.Byte_Arr is abstract;

   function Validate (This : access Typ;
                      P1_XMLValidateContext : access Standard.Javax.Xml.Crypto.Dsig.XMLValidateContext.Typ'Class)
                      return Java.Boolean is abstract;
   --  can raise Javax.Xml.Crypto.Dsig.XMLSignatureException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetId, "getId");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, Validate, "validate");

end Javax.Xml.Crypto.Dsig.XMLSignature.SignatureValue;
pragma Import (Java, Javax.Xml.Crypto.Dsig.XMLSignature.SignatureValue, "javax.xml.crypto.dsig.XMLSignature$SignatureValue");
pragma Extensions_Allowed (Off);
