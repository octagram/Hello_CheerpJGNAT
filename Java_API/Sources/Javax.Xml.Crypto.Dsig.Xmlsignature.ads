pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo;
limited with Javax.Xml.Crypto.Dsig.SignedInfo;
limited with Javax.Xml.Crypto.Dsig.XMLSignContext;
limited with Javax.Xml.Crypto.Dsig.XMLSignature.SignatureValue;
limited with Javax.Xml.Crypto.Dsig.XMLValidateContext;
limited with Javax.Xml.Crypto.KeySelectorResult;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Javax.Xml.Crypto.Dsig.XMLSignature is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Validate (This : access Typ;
                      P1_XMLValidateContext : access Standard.Javax.Xml.Crypto.Dsig.XMLValidateContext.Typ'Class)
                      return Java.Boolean is abstract;
   --  can raise Javax.Xml.Crypto.Dsig.XMLSignatureException.Except

   function GetKeyInfo (This : access Typ)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class is abstract;

   function GetSignedInfo (This : access Typ)
                           return access Javax.Xml.Crypto.Dsig.SignedInfo.Typ'Class is abstract;

   function GetObjects (This : access Typ)
                        return access Java.Util.List.Typ'Class is abstract;

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function GetSignatureValue (This : access Typ)
                               return access Javax.Xml.Crypto.Dsig.XMLSignature.SignatureValue.Typ'Class is abstract;

   procedure Sign (This : access Typ;
                   P1_XMLSignContext : access Standard.Javax.Xml.Crypto.Dsig.XMLSignContext.Typ'Class) is abstract;
   --  can raise Javax.Xml.Crypto.MarshalException.Except and
   --  Javax.Xml.Crypto.Dsig.XMLSignatureException.Except

   function GetKeySelectorResult (This : access Typ)
                                  return access Javax.Xml.Crypto.KeySelectorResult.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   XMLNS : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Validate, "validate");
   pragma Export (Java, GetKeyInfo, "getKeyInfo");
   pragma Export (Java, GetSignedInfo, "getSignedInfo");
   pragma Export (Java, GetObjects, "getObjects");
   pragma Export (Java, GetId, "getId");
   pragma Export (Java, GetSignatureValue, "getSignatureValue");
   pragma Export (Java, Sign, "sign");
   pragma Export (Java, GetKeySelectorResult, "getKeySelectorResult");
   pragma Import (Java, XMLNS, "XMLNS");

end Javax.Xml.Crypto.Dsig.XMLSignature;
pragma Import (Java, Javax.Xml.Crypto.Dsig.XMLSignature, "javax.xml.crypto.dsig.XMLSignature");
pragma Extensions_Allowed (Off);
