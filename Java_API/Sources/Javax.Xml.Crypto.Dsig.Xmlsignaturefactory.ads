pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dsig.CanonicalizationMethod;
limited with Javax.Xml.Crypto.Dsig.DigestMethod;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory;
limited with Javax.Xml.Crypto.Dsig.Manifest;
limited with Javax.Xml.Crypto.Dsig.Reference;
limited with Javax.Xml.Crypto.Dsig.SignatureMethod;
limited with Javax.Xml.Crypto.Dsig.SignatureProperties;
limited with Javax.Xml.Crypto.Dsig.SignatureProperty;
limited with Javax.Xml.Crypto.Dsig.SignedInfo;
limited with Javax.Xml.Crypto.Dsig.Spec.C14NMethodParameterSpec;
limited with Javax.Xml.Crypto.Dsig.Spec.DigestMethodParameterSpec;
limited with Javax.Xml.Crypto.Dsig.Spec.SignatureMethodParameterSpec;
limited with Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec;
limited with Javax.Xml.Crypto.Dsig.Transform;
limited with Javax.Xml.Crypto.Dsig.XMLObject;
limited with Javax.Xml.Crypto.Dsig.XMLSignature;
limited with Javax.Xml.Crypto.Dsig.XMLValidateContext;
limited with Javax.Xml.Crypto.URIDereferencer;
limited with Javax.Xml.Crypto.XMLStructure;
with Java.Lang.Object;

package Javax.Xml.Crypto.Dsig.XMLSignatureFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_XMLSignatureFactory (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.XMLSignatureFactory.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Provider : access Standard.Java.Security.Provider.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.XMLSignatureFactory.Typ'Class;

   function GetInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.XMLSignatureFactory.Typ'Class;
   --  can raise Java.Security.NoSuchProviderException.Except

   function GetInstance return access Javax.Xml.Crypto.Dsig.XMLSignatureFactory.Typ'Class;

   --  final
   function GetMechanismType (This : access Typ)
                              return access Java.Lang.String.Typ'Class;

   --  final
   function GetProvider (This : access Typ)
                         return access Java.Security.Provider.Typ'Class;

   function NewXMLSignature (This : access Typ;
                             P1_SignedInfo : access Standard.Javax.Xml.Crypto.Dsig.SignedInfo.Typ'Class;
                             P2_KeyInfo : access Standard.Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class)
                             return access Javax.Xml.Crypto.Dsig.XMLSignature.Typ'Class is abstract;

   function NewXMLSignature (This : access Typ;
                             P1_SignedInfo : access Standard.Javax.Xml.Crypto.Dsig.SignedInfo.Typ'Class;
                             P2_KeyInfo : access Standard.Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class;
                             P3_List : access Standard.Java.Util.List.Typ'Class;
                             P4_String : access Standard.Java.Lang.String.Typ'Class;
                             P5_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Crypto.Dsig.XMLSignature.Typ'Class is abstract;

   function NewReference (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_DigestMethod : access Standard.Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class)
                          return access Javax.Xml.Crypto.Dsig.Reference.Typ'Class is abstract;

   function NewReference (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_DigestMethod : access Standard.Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class;
                          P3_List : access Standard.Java.Util.List.Typ'Class;
                          P4_String : access Standard.Java.Lang.String.Typ'Class;
                          P5_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Xml.Crypto.Dsig.Reference.Typ'Class is abstract;

   function NewReference (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_DigestMethod : access Standard.Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class;
                          P3_List : access Standard.Java.Util.List.Typ'Class;
                          P4_String : access Standard.Java.Lang.String.Typ'Class;
                          P5_String : access Standard.Java.Lang.String.Typ'Class;
                          P6_Byte_Arr : Java.Byte_Arr)
                          return access Javax.Xml.Crypto.Dsig.Reference.Typ'Class is abstract;

   function NewReference (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_DigestMethod : access Standard.Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class;
                          P3_List : access Standard.Java.Util.List.Typ'Class;
                          P4_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                          P5_List : access Standard.Java.Util.List.Typ'Class;
                          P6_String : access Standard.Java.Lang.String.Typ'Class;
                          P7_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Xml.Crypto.Dsig.Reference.Typ'Class is abstract;

   function NewSignedInfo (This : access Typ;
                           P1_CanonicalizationMethod : access Standard.Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class;
                           P2_SignatureMethod : access Standard.Javax.Xml.Crypto.Dsig.SignatureMethod.Typ'Class;
                           P3_List : access Standard.Java.Util.List.Typ'Class)
                           return access Javax.Xml.Crypto.Dsig.SignedInfo.Typ'Class is abstract;

   function NewSignedInfo (This : access Typ;
                           P1_CanonicalizationMethod : access Standard.Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class;
                           P2_SignatureMethod : access Standard.Javax.Xml.Crypto.Dsig.SignatureMethod.Typ'Class;
                           P3_List : access Standard.Java.Util.List.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Xml.Crypto.Dsig.SignedInfo.Typ'Class is abstract;

   function NewXMLObject (This : access Typ;
                          P1_List : access Standard.Java.Util.List.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_String : access Standard.Java.Lang.String.Typ'Class;
                          P4_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Xml.Crypto.Dsig.XMLObject.Typ'Class is abstract;

   function NewManifest (This : access Typ;
                         P1_List : access Standard.Java.Util.List.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Manifest.Typ'Class is abstract;

   function NewManifest (This : access Typ;
                         P1_List : access Standard.Java.Util.List.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Manifest.Typ'Class is abstract;

   function NewSignatureProperty (This : access Typ;
                                  P1_List : access Standard.Java.Util.List.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class;
                                  P3_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Xml.Crypto.Dsig.SignatureProperty.Typ'Class is abstract;

   function NewSignatureProperties (This : access Typ;
                                    P1_List : access Standard.Java.Util.List.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Javax.Xml.Crypto.Dsig.SignatureProperties.Typ'Class is abstract;

   function NewDigestMethod (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_DigestMethodParameterSpec : access Standard.Javax.Xml.Crypto.Dsig.Spec.DigestMethodParameterSpec.Typ'Class)
                             return access Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class is abstract;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   function NewSignatureMethod (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_SignatureMethodParameterSpec : access Standard.Javax.Xml.Crypto.Dsig.Spec.SignatureMethodParameterSpec.Typ'Class)
                                return access Javax.Xml.Crypto.Dsig.SignatureMethod.Typ'Class is abstract;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   function NewTransform (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_TransformParameterSpec : access Standard.Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec.Typ'Class)
                          return access Javax.Xml.Crypto.Dsig.Transform.Typ'Class is abstract;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   function NewTransform (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class)
                          return access Javax.Xml.Crypto.Dsig.Transform.Typ'Class is abstract;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   function NewCanonicalizationMethod (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_C14NMethodParameterSpec : access Standard.Javax.Xml.Crypto.Dsig.Spec.C14NMethodParameterSpec.Typ'Class)
                                       return access Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class is abstract;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   function NewCanonicalizationMethod (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                                       P2_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class)
                                       return access Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class is abstract;
   --  can raise Java.Security.NoSuchAlgorithmException.Except and
   --  Java.Security.InvalidAlgorithmParameterException.Except

   --  final
   function GetKeyInfoFactory (This : access Typ)
                               return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory.Typ'Class;

   function UnmarshalXMLSignature (This : access Typ;
                                   P1_XMLValidateContext : access Standard.Javax.Xml.Crypto.Dsig.XMLValidateContext.Typ'Class)
                                   return access Javax.Xml.Crypto.Dsig.XMLSignature.Typ'Class is abstract;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function UnmarshalXMLSignature (This : access Typ;
                                   P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class)
                                   return access Javax.Xml.Crypto.Dsig.XMLSignature.Typ'Class is abstract;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function IsFeatureSupported (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean is abstract;

   function GetURIDereferencer (This : access Typ)
                                return access Javax.Xml.Crypto.URIDereferencer.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLSignatureFactory);
   pragma Export (Java, GetInstance, "getInstance");
   pragma Export (Java, GetMechanismType, "getMechanismType");
   pragma Export (Java, GetProvider, "getProvider");
   pragma Export (Java, NewXMLSignature, "newXMLSignature");
   pragma Export (Java, NewReference, "newReference");
   pragma Export (Java, NewSignedInfo, "newSignedInfo");
   pragma Export (Java, NewXMLObject, "newXMLObject");
   pragma Export (Java, NewManifest, "newManifest");
   pragma Export (Java, NewSignatureProperty, "newSignatureProperty");
   pragma Export (Java, NewSignatureProperties, "newSignatureProperties");
   pragma Export (Java, NewDigestMethod, "newDigestMethod");
   pragma Export (Java, NewSignatureMethod, "newSignatureMethod");
   pragma Export (Java, NewTransform, "newTransform");
   pragma Export (Java, NewCanonicalizationMethod, "newCanonicalizationMethod");
   pragma Export (Java, GetKeyInfoFactory, "getKeyInfoFactory");
   pragma Export (Java, UnmarshalXMLSignature, "unmarshalXMLSignature");
   pragma Export (Java, IsFeatureSupported, "isFeatureSupported");
   pragma Export (Java, GetURIDereferencer, "getURIDereferencer");

end Javax.Xml.Crypto.Dsig.XMLSignatureFactory;
pragma Import (Java, Javax.Xml.Crypto.Dsig.XMLSignatureFactory, "javax.xml.crypto.dsig.XMLSignatureFactory");
pragma Extensions_Allowed (Off);
