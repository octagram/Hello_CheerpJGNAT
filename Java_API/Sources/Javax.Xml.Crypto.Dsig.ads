pragma Extensions_Allowed (On);
package Javax.Xml.Crypto.Dsig is
   pragma Preelaborate;
end Javax.Xml.Crypto.Dsig;
pragma Import (Java, Javax.Xml.Crypto.Dsig, "javax.xml.crypto.dsig");
pragma Extensions_Allowed (Off);
