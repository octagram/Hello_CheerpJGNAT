pragma Extensions_Allowed (On);
limited with Java.Security.Key;
limited with Javax.Xml.Crypto.AlgorithmMethod;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo;
limited with Javax.Xml.Crypto.KeySelector.Purpose;
limited with Javax.Xml.Crypto.KeySelectorResult;
limited with Javax.Xml.Crypto.XMLCryptoContext;
with Java.Lang.Object;

package Javax.Xml.Crypto.KeySelector is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_KeySelector (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function select_K (This : access Typ;
                      P1_KeyInfo : access Standard.Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class;
                      P2_Purpose : access Standard.Javax.Xml.Crypto.KeySelector.Purpose.Typ'Class;
                      P3_AlgorithmMethod : access Standard.Javax.Xml.Crypto.AlgorithmMethod.Typ'Class;
                      P4_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                      return access Javax.Xml.Crypto.KeySelectorResult.Typ'Class is abstract;
   --  can raise Javax.Xml.Crypto.KeySelectorException.Except

   function SingletonKeySelector (P1_Key : access Standard.Java.Security.Key.Typ'Class)
                                  return access Javax.Xml.Crypto.KeySelector.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_KeySelector);
   pragma Export (Java, select_K, "select");
   pragma Import (Java, SingletonKeySelector, "singletonKeySelector");

end Javax.Xml.Crypto.KeySelector;
pragma Import (Java, Javax.Xml.Crypto.KeySelector, "javax.xml.crypto.KeySelector");
pragma Extensions_Allowed (Off);
