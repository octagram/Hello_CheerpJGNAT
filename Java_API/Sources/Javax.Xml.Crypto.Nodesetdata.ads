pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Lang.Object;
with Javax.Xml.Crypto.Data;

package Javax.Xml.Crypto.NodeSetData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Data_I : Javax.Xml.Crypto.Data.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Iterator, "iterator");

end Javax.Xml.Crypto.NodeSetData;
pragma Import (Java, Javax.Xml.Crypto.NodeSetData, "javax.xml.crypto.NodeSetData");
pragma Extensions_Allowed (Off);
