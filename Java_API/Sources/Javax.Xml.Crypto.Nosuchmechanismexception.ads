pragma Extensions_Allowed (On);
limited with Java.Io.PrintStream;
limited with Java.Io.PrintWriter;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Javax.Xml.Crypto.NoSuchMechanismException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NoSuchMechanismException (This : Ref := null)
                                          return Ref;

   function New_NoSuchMechanismException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   function New_NoSuchMechanismException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                          P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   function New_NoSuchMechanismException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                          This : Ref := null)
                                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;

   procedure PrintStackTrace (This : access Typ);

   procedure PrintStackTrace (This : access Typ;
                              P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   procedure PrintStackTrace (This : access Typ;
                              P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.crypto.NoSuchMechanismException");
   pragma Java_Constructor (New_NoSuchMechanismException);
   pragma Import (Java, GetCause, "getCause");
   pragma Import (Java, PrintStackTrace, "printStackTrace");

end Javax.Xml.Crypto.NoSuchMechanismException;
pragma Import (Java, Javax.Xml.Crypto.NoSuchMechanismException, "javax.xml.crypto.NoSuchMechanismException");
pragma Extensions_Allowed (Off);
