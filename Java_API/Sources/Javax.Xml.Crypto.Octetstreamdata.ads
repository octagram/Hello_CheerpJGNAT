pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Crypto.Data;

package Javax.Xml.Crypto.OctetStreamData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Data_I : Javax.Xml.Crypto.Data.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_OctetStreamData (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_OctetStreamData (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOctetStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;

   function GetURI (This : access Typ)
                    return access Java.Lang.String.Typ'Class;

   function GetMimeType (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OctetStreamData);
   pragma Import (Java, GetOctetStream, "getOctetStream");
   pragma Import (Java, GetURI, "getURI");
   pragma Import (Java, GetMimeType, "getMimeType");

end Javax.Xml.Crypto.OctetStreamData;
pragma Import (Java, Javax.Xml.Crypto.OctetStreamData, "javax.xml.crypto.OctetStreamData");
pragma Extensions_Allowed (Off);
