pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Crypto.KeySelector;
limited with Javax.Xml.Crypto.URIDereferencer;
with Java.Lang.Object;

package Javax.Xml.Crypto.XMLCryptoContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBaseURI (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBaseURI (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetKeySelector (This : access Typ)
                            return access Javax.Xml.Crypto.KeySelector.Typ'Class is abstract;

   procedure SetKeySelector (This : access Typ;
                             P1_KeySelector : access Standard.Javax.Xml.Crypto.KeySelector.Typ'Class) is abstract;

   function GetURIDereferencer (This : access Typ)
                                return access Javax.Xml.Crypto.URIDereferencer.Typ'Class is abstract;

   procedure SetURIDereferencer (This : access Typ;
                                 P1_URIDereferencer : access Standard.Javax.Xml.Crypto.URIDereferencer.Typ'Class) is abstract;

   function GetNamespacePrefix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;

   function PutNamespacePrefix (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;

   function GetDefaultNamespacePrefix (This : access Typ)
                                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetDefaultNamespacePrefix (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function SetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetBaseURI, "getBaseURI");
   pragma Export (Java, SetBaseURI, "setBaseURI");
   pragma Export (Java, GetKeySelector, "getKeySelector");
   pragma Export (Java, SetKeySelector, "setKeySelector");
   pragma Export (Java, GetURIDereferencer, "getURIDereferencer");
   pragma Export (Java, SetURIDereferencer, "setURIDereferencer");
   pragma Export (Java, GetNamespacePrefix, "getNamespacePrefix");
   pragma Export (Java, PutNamespacePrefix, "putNamespacePrefix");
   pragma Export (Java, GetDefaultNamespacePrefix, "getDefaultNamespacePrefix");
   pragma Export (Java, SetDefaultNamespacePrefix, "setDefaultNamespacePrefix");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Put, "put");

end Javax.Xml.Crypto.XMLCryptoContext;
pragma Import (Java, Javax.Xml.Crypto.XMLCryptoContext, "javax.xml.crypto.XMLCryptoContext");
pragma Extensions_Allowed (Off);
