pragma Extensions_Allowed (On);
package Javax.Xml.Crypto is
   pragma Preelaborate;
end Javax.Xml.Crypto;
pragma Import (Java, Javax.Xml.Crypto, "javax.xml.crypto");
pragma Extensions_Allowed (Off);
