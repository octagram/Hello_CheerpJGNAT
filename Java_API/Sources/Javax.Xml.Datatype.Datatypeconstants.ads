pragma Extensions_Allowed (On);
limited with Javax.Xml.Datatype.DatatypeConstants.Field;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Datatype.DatatypeConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JANUARY : constant Java.Int;

   --  final
   FEBRUARY : constant Java.Int;

   --  final
   MARCH : constant Java.Int;

   --  final
   APRIL : constant Java.Int;

   --  final
   MAY : constant Java.Int;

   --  final
   JUNE : constant Java.Int;

   --  final
   JULY : constant Java.Int;

   --  final
   AUGUST : constant Java.Int;

   --  final
   SEPTEMBER : constant Java.Int;

   --  final
   OCTOBER : constant Java.Int;

   --  final
   NOVEMBER : constant Java.Int;

   --  final
   DECEMBER : constant Java.Int;

   --  final
   LESSER : constant Java.Int;

   --  final
   EQUAL : constant Java.Int;

   --  final
   GREATER : constant Java.Int;

   --  final
   INDETERMINATE : constant Java.Int;

   --  final
   FIELD_UNDEFINED : constant Java.Int;

   --  final
   YEARS : access Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class;

   --  final
   MONTHS : access Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class;

   --  final
   DAYS : access Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class;

   --  final
   HOURS : access Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class;

   --  final
   MINUTES : access Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class;

   --  final
   SECONDS : access Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class;

   --  final
   DATETIME : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   TIME : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   DATE : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   GYEARMONTH : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   GMONTHDAY : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   GYEAR : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   GMONTH : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   GDAY : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   DURATION : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   DURATION_DAYTIME : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   DURATION_YEARMONTH : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   MAX_TIMEZONE_OFFSET : constant Java.Int;

   --  final
   MIN_TIMEZONE_OFFSET : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, JANUARY, "JANUARY");
   pragma Import (Java, FEBRUARY, "FEBRUARY");
   pragma Import (Java, MARCH, "MARCH");
   pragma Import (Java, APRIL, "APRIL");
   pragma Import (Java, MAY, "MAY");
   pragma Import (Java, JUNE, "JUNE");
   pragma Import (Java, JULY, "JULY");
   pragma Import (Java, AUGUST, "AUGUST");
   pragma Import (Java, SEPTEMBER, "SEPTEMBER");
   pragma Import (Java, OCTOBER, "OCTOBER");
   pragma Import (Java, NOVEMBER, "NOVEMBER");
   pragma Import (Java, DECEMBER, "DECEMBER");
   pragma Import (Java, LESSER, "LESSER");
   pragma Import (Java, EQUAL, "EQUAL");
   pragma Import (Java, GREATER, "GREATER");
   pragma Import (Java, INDETERMINATE, "INDETERMINATE");
   pragma Import (Java, FIELD_UNDEFINED, "FIELD_UNDEFINED");
   pragma Import (Java, YEARS, "YEARS");
   pragma Import (Java, MONTHS, "MONTHS");
   pragma Import (Java, DAYS, "DAYS");
   pragma Import (Java, HOURS, "HOURS");
   pragma Import (Java, MINUTES, "MINUTES");
   pragma Import (Java, SECONDS, "SECONDS");
   pragma Import (Java, DATETIME, "DATETIME");
   pragma Import (Java, TIME, "TIME");
   pragma Import (Java, DATE, "DATE");
   pragma Import (Java, GYEARMONTH, "GYEARMONTH");
   pragma Import (Java, GMONTHDAY, "GMONTHDAY");
   pragma Import (Java, GYEAR, "GYEAR");
   pragma Import (Java, GMONTH, "GMONTH");
   pragma Import (Java, GDAY, "GDAY");
   pragma Import (Java, DURATION, "DURATION");
   pragma Import (Java, DURATION_DAYTIME, "DURATION_DAYTIME");
   pragma Import (Java, DURATION_YEARMONTH, "DURATION_YEARMONTH");
   pragma Import (Java, MAX_TIMEZONE_OFFSET, "MAX_TIMEZONE_OFFSET");
   pragma Import (Java, MIN_TIMEZONE_OFFSET, "MIN_TIMEZONE_OFFSET");

end Javax.Xml.Datatype.DatatypeConstants;
pragma Import (Java, Javax.Xml.Datatype.DatatypeConstants, "javax.xml.datatype.DatatypeConstants");
pragma Extensions_Allowed (Off);
