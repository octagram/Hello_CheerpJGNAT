pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Math.BigInteger;
limited with Java.Util.GregorianCalendar;
limited with Javax.Xml.Datatype.Duration;
limited with Javax.Xml.Datatype.XMLGregorianCalendar;
with Java.Lang.Object;

package Javax.Xml.Datatype.DatatypeFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_DatatypeFactory (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Datatype.DatatypeFactory.Typ'Class;
   --  can raise Javax.Xml.Datatype.DatatypeConfigurationException.Except

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Javax.Xml.Datatype.DatatypeFactory.Typ'Class;
   --  can raise Javax.Xml.Datatype.DatatypeConfigurationException.Except

   function NewDuration (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Datatype.Duration.Typ'Class is abstract;

   function NewDuration (This : access Typ;
                         P1_Long : Java.Long)
                         return access Javax.Xml.Datatype.Duration.Typ'Class is abstract;

   function NewDuration (This : access Typ;
                         P1_Boolean : Java.Boolean;
                         P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                         P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                         P4_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                         P5_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                         P6_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                         P7_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                         return access Javax.Xml.Datatype.Duration.Typ'Class is abstract;

   function NewDuration (This : access Typ;
                         P1_Boolean : Java.Boolean;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int;
                         P4_Int : Java.Int;
                         P5_Int : Java.Int;
                         P6_Int : Java.Int;
                         P7_Int : Java.Int)
                         return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationDayTime (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationDayTime (This : access Typ;
                                P1_Long : Java.Long)
                                return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationDayTime (This : access Typ;
                                P1_Boolean : Java.Boolean;
                                P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                P4_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                P5_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                                return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationDayTime (This : access Typ;
                                P1_Boolean : Java.Boolean;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int;
                                P4_Int : Java.Int;
                                P5_Int : Java.Int)
                                return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationYearMonth (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationYearMonth (This : access Typ;
                                  P1_Long : Java.Long)
                                  return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationYearMonth (This : access Typ;
                                  P1_Boolean : Java.Boolean;
                                  P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                  P3_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                                  return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewDurationYearMonth (This : access Typ;
                                  P1_Boolean : Java.Boolean;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int)
                                  return access Javax.Xml.Datatype.Duration.Typ'Class;

   function NewXMLGregorianCalendar (This : access Typ)
                                     return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class is abstract;

   function NewXMLGregorianCalendar (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class is abstract;

   function NewXMLGregorianCalendar (This : access Typ;
                                     P1_GregorianCalendar : access Standard.Java.Util.GregorianCalendar.Typ'Class)
                                     return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class is abstract;

   function NewXMLGregorianCalendar (This : access Typ;
                                     P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int;
                                     P7_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                                     P8_Int : Java.Int)
                                     return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class is abstract;

   function NewXMLGregorianCalendar (This : access Typ;
                                     P1_Int : Java.Int;
                                     P2_Int : Java.Int;
                                     P3_Int : Java.Int;
                                     P4_Int : Java.Int;
                                     P5_Int : Java.Int;
                                     P6_Int : Java.Int;
                                     P7_Int : Java.Int;
                                     P8_Int : Java.Int)
                                     return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class;

   function NewXMLGregorianCalendarDate (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int)
                                         return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class;

   function NewXMLGregorianCalendarTime (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int)
                                         return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class;

   function NewXMLGregorianCalendarTime (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int;
                                         P4_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                                         P5_Int : Java.Int)
                                         return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class;

   function NewXMLGregorianCalendarTime (This : access Typ;
                                         P1_Int : Java.Int;
                                         P2_Int : Java.Int;
                                         P3_Int : Java.Int;
                                         P4_Int : Java.Int;
                                         P5_Int : Java.Int)
                                         return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DATATYPEFACTORY_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   DATATYPEFACTORY_IMPLEMENTATION_CLASS : access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DatatypeFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, NewDuration, "newDuration");
   pragma Export (Java, NewDurationDayTime, "newDurationDayTime");
   pragma Export (Java, NewDurationYearMonth, "newDurationYearMonth");
   pragma Export (Java, NewXMLGregorianCalendar, "newXMLGregorianCalendar");
   pragma Export (Java, NewXMLGregorianCalendarDate, "newXMLGregorianCalendarDate");
   pragma Export (Java, NewXMLGregorianCalendarTime, "newXMLGregorianCalendarTime");
   pragma Import (Java, DATATYPEFACTORY_PROPERTY, "DATATYPEFACTORY_PROPERTY");
   pragma Import (Java, DATATYPEFACTORY_IMPLEMENTATION_CLASS, "DATATYPEFACTORY_IMPLEMENTATION_CLASS");

end Javax.Xml.Datatype.DatatypeFactory;
pragma Import (Java, Javax.Xml.Datatype.DatatypeFactory, "javax.xml.datatype.DatatypeFactory");
pragma Extensions_Allowed (Off);
