pragma Extensions_Allowed (On);
limited with Java.Lang.Number;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Util.Calendar;
limited with Java.Util.Date;
limited with Javax.Xml.Datatype.DatatypeConstants.Field;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Datatype.Duration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Duration (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXMLSchemaType (This : access Typ)
                              return access Javax.Xml.Namespace.QName.Typ'Class;

   function GetSign (This : access Typ)
                     return Java.Int is abstract;

   function GetYears (This : access Typ)
                      return Java.Int;

   function GetMonths (This : access Typ)
                       return Java.Int;

   function GetDays (This : access Typ)
                     return Java.Int;

   function GetHours (This : access Typ)
                      return Java.Int;

   function GetMinutes (This : access Typ)
                        return Java.Int;

   function GetSeconds (This : access Typ)
                        return Java.Int;

   function GetTimeInMillis (This : access Typ;
                             P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                             return Java.Long;

   function GetTimeInMillis (This : access Typ;
                             P1_Date : access Standard.Java.Util.Date.Typ'Class)
                             return Java.Long;

   function GetField (This : access Typ;
                      P1_Field : access Standard.Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class)
                      return access Java.Lang.Number.Typ'Class is abstract;

   function IsSet (This : access Typ;
                   P1_Field : access Standard.Javax.Xml.Datatype.DatatypeConstants.Field.Typ'Class)
                   return Java.Boolean is abstract;

   function Add (This : access Typ;
                 P1_Duration : access Standard.Javax.Xml.Datatype.Duration.Typ'Class)
                 return access Javax.Xml.Datatype.Duration.Typ'Class is abstract;

   procedure AddTo (This : access Typ;
                    P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class) is abstract;

   procedure AddTo (This : access Typ;
                    P1_Date : access Standard.Java.Util.Date.Typ'Class);

   function Subtract (This : access Typ;
                      P1_Duration : access Standard.Javax.Xml.Datatype.Duration.Typ'Class)
                      return access Javax.Xml.Datatype.Duration.Typ'Class;

   function Multiply (This : access Typ;
                      P1_Int : Java.Int)
                      return access Javax.Xml.Datatype.Duration.Typ'Class;

   function Multiply (This : access Typ;
                      P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class)
                      return access Javax.Xml.Datatype.Duration.Typ'Class is abstract;

   function Negate (This : access Typ)
                    return access Javax.Xml.Datatype.Duration.Typ'Class is abstract;

   function NormalizeWith (This : access Typ;
                           P1_Calendar : access Standard.Java.Util.Calendar.Typ'Class)
                           return access Javax.Xml.Datatype.Duration.Typ'Class is abstract;

   function Compare (This : access Typ;
                     P1_Duration : access Standard.Javax.Xml.Datatype.Duration.Typ'Class)
                     return Java.Int is abstract;

   function IsLongerThan (This : access Typ;
                          P1_Duration : access Standard.Javax.Xml.Datatype.Duration.Typ'Class)
                          return Java.Boolean;

   function IsShorterThan (This : access Typ;
                           P1_Duration : access Standard.Javax.Xml.Datatype.Duration.Typ'Class)
                           return Java.Boolean;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Duration);
   pragma Export (Java, GetXMLSchemaType, "getXMLSchemaType");
   pragma Export (Java, GetSign, "getSign");
   pragma Export (Java, GetYears, "getYears");
   pragma Export (Java, GetMonths, "getMonths");
   pragma Export (Java, GetDays, "getDays");
   pragma Export (Java, GetHours, "getHours");
   pragma Export (Java, GetMinutes, "getMinutes");
   pragma Export (Java, GetSeconds, "getSeconds");
   pragma Export (Java, GetTimeInMillis, "getTimeInMillis");
   pragma Export (Java, GetField, "getField");
   pragma Export (Java, IsSet, "isSet");
   pragma Export (Java, Add, "add");
   pragma Export (Java, AddTo, "addTo");
   pragma Export (Java, Subtract, "subtract");
   pragma Export (Java, Multiply, "multiply");
   pragma Export (Java, Negate, "negate");
   pragma Export (Java, NormalizeWith, "normalizeWith");
   pragma Export (Java, Compare, "compare");
   pragma Export (Java, IsLongerThan, "isLongerThan");
   pragma Export (Java, IsShorterThan, "isShorterThan");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToString, "toString");

end Javax.Xml.Datatype.Duration;
pragma Import (Java, Javax.Xml.Datatype.Duration, "javax.xml.datatype.Duration");
pragma Extensions_Allowed (Off);
