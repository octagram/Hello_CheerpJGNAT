pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Java.Math.BigInteger;
limited with Java.Util.GregorianCalendar;
limited with Java.Util.Locale;
limited with Java.Util.TimeZone;
limited with Javax.Xml.Datatype.Duration;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Javax.Xml.Datatype.XMLGregorianCalendar is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_XMLGregorianCalendar (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Clear (This : access Typ) is abstract;

   procedure Reset (This : access Typ) is abstract;

   procedure SetYear (This : access Typ;
                      P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class) is abstract;

   procedure SetYear (This : access Typ;
                      P1_Int : Java.Int) is abstract;

   procedure SetMonth (This : access Typ;
                       P1_Int : Java.Int) is abstract;

   procedure SetDay (This : access Typ;
                     P1_Int : Java.Int) is abstract;

   procedure SetTimezone (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   procedure SetTime (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int);

   procedure SetHour (This : access Typ;
                      P1_Int : Java.Int) is abstract;

   procedure SetMinute (This : access Typ;
                        P1_Int : Java.Int) is abstract;

   procedure SetSecond (This : access Typ;
                        P1_Int : Java.Int) is abstract;

   procedure SetMillisecond (This : access Typ;
                             P1_Int : Java.Int) is abstract;

   procedure SetFractionalSecond (This : access Typ;
                                  P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class) is abstract;

   procedure SetTime (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class);

   procedure SetTime (This : access Typ;
                      P1_Int : Java.Int;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int;
                      P4_Int : Java.Int);

   function GetEon (This : access Typ)
                    return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetYear (This : access Typ)
                     return Java.Int is abstract;

   function GetEonAndYear (This : access Typ)
                           return access Java.Math.BigInteger.Typ'Class is abstract;

   function GetMonth (This : access Typ)
                      return Java.Int is abstract;

   function GetDay (This : access Typ)
                    return Java.Int is abstract;

   function GetTimezone (This : access Typ)
                         return Java.Int is abstract
                         with Export => "getTimezone", Convention => Java;

   function GetHour (This : access Typ)
                     return Java.Int is abstract;

   function GetMinute (This : access Typ)
                       return Java.Int is abstract;

   function GetSecond (This : access Typ)
                       return Java.Int is abstract;

   function GetMillisecond (This : access Typ)
                            return Java.Int;

   function GetFractionalSecond (This : access Typ)
                                 return access Java.Math.BigDecimal.Typ'Class is abstract;

   function Compare (This : access Typ;
                     P1_XMLGregorianCalendar : access Standard.Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class)
                     return Java.Int is abstract;

   function Normalize (This : access Typ)
                       return access Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class is abstract;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToXMLFormat (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetXMLSchemaType (This : access Typ)
                              return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function IsValid (This : access Typ)
                     return Java.Boolean is abstract;

   procedure Add (This : access Typ;
                  P1_Duration : access Standard.Javax.Xml.Datatype.Duration.Typ'Class) is abstract;

   function ToGregorianCalendar (This : access Typ)
                                 return access Java.Util.GregorianCalendar.Typ'Class is abstract;

   function ToGregorianCalendar (This : access Typ;
                                 P1_TimeZone : access Standard.Java.Util.TimeZone.Typ'Class;
                                 P2_Locale : access Standard.Java.Util.Locale.Typ'Class;
                                 P3_XMLGregorianCalendar : access Standard.Javax.Xml.Datatype.XMLGregorianCalendar.Typ'Class)
                                 return access Java.Util.GregorianCalendar.Typ'Class is abstract;

   function GetTimeZone (This : access Typ;
                         P1_Int : Java.Int)
                         return access Java.Util.TimeZone.Typ'Class is abstract
                         with Export => "getTimeZone", Convention => Java;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLGregorianCalendar);
   pragma Export (Java, Clear, "clear");
   pragma Export (Java, Reset, "reset");
   pragma Export (Java, SetYear, "setYear");
   pragma Export (Java, SetMonth, "setMonth");
   pragma Export (Java, SetDay, "setDay");
   pragma Export (Java, SetTimezone, "setTimezone");
   pragma Export (Java, SetTime, "setTime");
   pragma Export (Java, SetHour, "setHour");
   pragma Export (Java, SetMinute, "setMinute");
   pragma Export (Java, SetSecond, "setSecond");
   pragma Export (Java, SetMillisecond, "setMillisecond");
   pragma Export (Java, SetFractionalSecond, "setFractionalSecond");
   pragma Export (Java, GetEon, "getEon");
   pragma Export (Java, GetYear, "getYear");
   pragma Export (Java, GetEonAndYear, "getEonAndYear");
   pragma Export (Java, GetMonth, "getMonth");
   pragma Export (Java, GetDay, "getDay");
   -- pragma Import (Java, GetTimezone, "getTimezone");
   pragma Export (Java, GetHour, "getHour");
   pragma Export (Java, GetMinute, "getMinute");
   pragma Export (Java, GetSecond, "getSecond");
   pragma Export (Java, GetMillisecond, "getMillisecond");
   pragma Export (Java, GetFractionalSecond, "getFractionalSecond");
   pragma Export (Java, Compare, "compare");
   pragma Export (Java, Normalize, "normalize");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, ToXMLFormat, "toXMLFormat");
   pragma Export (Java, GetXMLSchemaType, "getXMLSchemaType");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, IsValid, "isValid");
   pragma Export (Java, Add, "add");
   pragma Export (Java, ToGregorianCalendar, "toGregorianCalendar");
   -- pragma Import (Java, GetTimeZone, "getTimeZone");
   pragma Export (Java, Clone, "clone");

end Javax.Xml.Datatype.XMLGregorianCalendar;
pragma Import (Java, Javax.Xml.Datatype.XMLGregorianCalendar, "javax.xml.datatype.XMLGregorianCalendar");
pragma Extensions_Allowed (Off);
