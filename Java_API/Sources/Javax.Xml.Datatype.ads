pragma Extensions_Allowed (On);
package Javax.Xml.Datatype is
   pragma Preelaborate;
end Javax.Xml.Datatype;
pragma Import (Java, Javax.Xml.Datatype, "javax.xml.datatype");
pragma Extensions_Allowed (Off);
