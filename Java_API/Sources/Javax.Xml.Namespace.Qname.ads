pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;

package Javax.Xml.Namespace.QName is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_QName (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_QName (P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   function New_QName (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                       This : Ref := null)
                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNamespaceURI (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetLocalPart (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetPrefix (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   --  final
   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   --  final
   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function ValueOf (P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Xml.Namespace.QName.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_QName);
   pragma Import (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Import (Java, GetLocalPart, "getLocalPart");
   pragma Import (Java, GetPrefix, "getPrefix");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, ValueOf, "valueOf");

end Javax.Xml.Namespace.QName;
pragma Import (Java, Javax.Xml.Namespace.QName, "javax.xml.namespace.QName");
pragma Extensions_Allowed (Off);
