pragma Extensions_Allowed (On);
package Javax.Xml.Namespace is
   pragma Preelaborate;
end Javax.Xml.Namespace;
pragma Import (Java, Javax.Xml.Namespace, "javax.xml.namespace");
pragma Extensions_Allowed (Off);
