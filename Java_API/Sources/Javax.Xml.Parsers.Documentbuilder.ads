pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Javax.Xml.Validation.Schema;
limited with Org.W3c.Dom.DOMImplementation;
limited with Org.W3c.Dom.Document;
limited with Org.Xml.Sax.EntityResolver;
limited with Org.Xml.Sax.ErrorHandler;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;

package Javax.Xml.Parsers.DocumentBuilder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_DocumentBuilder (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ);

   function Parse (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                   return access Org.W3c.Dom.Document.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function Parse (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Org.W3c.Dom.Document.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function Parse (This : access Typ;
                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                   return access Org.W3c.Dom.Document.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function Parse (This : access Typ;
                   P1_File : access Standard.Java.Io.File.Typ'Class)
                   return access Org.W3c.Dom.Document.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function Parse (This : access Typ;
                   P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class)
                   return access Org.W3c.Dom.Document.Typ'Class is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function IsNamespaceAware (This : access Typ)
                              return Java.Boolean is abstract;

   function IsValidating (This : access Typ)
                          return Java.Boolean is abstract;

   procedure SetEntityResolver (This : access Typ;
                                P1_EntityResolver : access Standard.Org.Xml.Sax.EntityResolver.Typ'Class) is abstract;

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class) is abstract;

   function NewDocument (This : access Typ)
                         return access Org.W3c.Dom.Document.Typ'Class is abstract;

   function GetDOMImplementation (This : access Typ)
                                  return access Org.W3c.Dom.DOMImplementation.Typ'Class is abstract;

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class;

   function IsXIncludeAware (This : access Typ)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DocumentBuilder);
   pragma Export (Java, Reset, "reset");
   pragma Export (Java, Parse, "parse");
   pragma Export (Java, IsNamespaceAware, "isNamespaceAware");
   pragma Export (Java, IsValidating, "isValidating");
   pragma Export (Java, SetEntityResolver, "setEntityResolver");
   pragma Export (Java, SetErrorHandler, "setErrorHandler");
   pragma Export (Java, NewDocument, "newDocument");
   pragma Export (Java, GetDOMImplementation, "getDOMImplementation");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, IsXIncludeAware, "isXIncludeAware");

end Javax.Xml.Parsers.DocumentBuilder;
pragma Import (Java, Javax.Xml.Parsers.DocumentBuilder, "javax.xml.parsers.DocumentBuilder");
pragma Extensions_Allowed (Off);
