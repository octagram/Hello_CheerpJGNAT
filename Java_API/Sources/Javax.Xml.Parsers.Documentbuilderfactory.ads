pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Javax.Xml.Parsers.DocumentBuilder;
limited with Javax.Xml.Validation.Schema;
with Java.Lang.Object;

package Javax.Xml.Parsers.DocumentBuilderFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_DocumentBuilderFactory (This : Ref := null)
                                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Parsers.DocumentBuilderFactory.Typ'Class;

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Javax.Xml.Parsers.DocumentBuilderFactory.Typ'Class;

   function NewDocumentBuilder (This : access Typ)
                                return access Javax.Xml.Parsers.DocumentBuilder.Typ'Class is abstract;
   --  can raise Javax.Xml.Parsers.ParserConfigurationException.Except

   procedure SetNamespaceAware (This : access Typ;
                                P1_Boolean : Java.Boolean);

   procedure SetValidating (This : access Typ;
                            P1_Boolean : Java.Boolean);

   procedure SetIgnoringElementContentWhitespace (This : access Typ;
                                                  P1_Boolean : Java.Boolean);

   procedure SetExpandEntityReferences (This : access Typ;
                                        P1_Boolean : Java.Boolean);

   procedure SetIgnoringComments (This : access Typ;
                                  P1_Boolean : Java.Boolean);

   procedure SetCoalescing (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function IsNamespaceAware (This : access Typ)
                              return Java.Boolean;

   function IsValidating (This : access Typ)
                          return Java.Boolean;

   function IsIgnoringElementContentWhitespace (This : access Typ)
                                                return Java.Boolean;

   function IsExpandEntityReferences (This : access Typ)
                                      return Java.Boolean;

   function IsIgnoringComments (This : access Typ)
                                return Java.Boolean;

   function IsCoalescing (This : access Typ)
                          return Java.Boolean;

   procedure SetAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Javax.Xml.Parsers.ParserConfigurationException.Except

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Javax.Xml.Parsers.ParserConfigurationException.Except

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class;

   procedure SetSchema (This : access Typ;
                        P1_Schema : access Standard.Javax.Xml.Validation.Schema.Typ'Class);

   procedure SetXIncludeAware (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function IsXIncludeAware (This : access Typ)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DocumentBuilderFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, NewDocumentBuilder, "newDocumentBuilder");
   pragma Export (Java, SetNamespaceAware, "setNamespaceAware");
   pragma Export (Java, SetValidating, "setValidating");
   pragma Export (Java, SetIgnoringElementContentWhitespace, "setIgnoringElementContentWhitespace");
   pragma Export (Java, SetExpandEntityReferences, "setExpandEntityReferences");
   pragma Export (Java, SetIgnoringComments, "setIgnoringComments");
   pragma Export (Java, SetCoalescing, "setCoalescing");
   pragma Export (Java, IsNamespaceAware, "isNamespaceAware");
   pragma Export (Java, IsValidating, "isValidating");
   pragma Export (Java, IsIgnoringElementContentWhitespace, "isIgnoringElementContentWhitespace");
   pragma Export (Java, IsExpandEntityReferences, "isExpandEntityReferences");
   pragma Export (Java, IsIgnoringComments, "isIgnoringComments");
   pragma Export (Java, IsCoalescing, "isCoalescing");
   pragma Export (Java, SetAttribute, "setAttribute");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, SetFeature, "setFeature");
   pragma Export (Java, GetFeature, "getFeature");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, SetSchema, "setSchema");
   pragma Export (Java, SetXIncludeAware, "setXIncludeAware");
   pragma Export (Java, IsXIncludeAware, "isXIncludeAware");

end Javax.Xml.Parsers.DocumentBuilderFactory;
pragma Import (Java, Javax.Xml.Parsers.DocumentBuilderFactory, "javax.xml.parsers.DocumentBuilderFactory");
pragma Extensions_Allowed (Off);
