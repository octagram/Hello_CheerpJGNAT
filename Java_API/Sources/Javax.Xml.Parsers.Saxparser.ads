pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Javax.Xml.Validation.Schema;
limited with Org.Xml.Sax.HandlerBase;
limited with Org.Xml.Sax.Helpers.DefaultHandler;
limited with Org.Xml.Sax.InputSource;
limited with Org.Xml.Sax.Parser;
limited with Org.Xml.Sax.XMLReader;
with Java.Lang.Object;

package Javax.Xml.Parsers.SAXParser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SAXParser (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ);

   procedure Parse (This : access Typ;
                    P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                    P2_HandlerBase : access Standard.Org.Xml.Sax.HandlerBase.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                    P2_HandlerBase : access Standard.Org.Xml.Sax.HandlerBase.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                    P2_DefaultHandler : access Standard.Org.Xml.Sax.Helpers.DefaultHandler.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                    P2_DefaultHandler : access Standard.Org.Xml.Sax.Helpers.DefaultHandler.Typ'Class;
                    P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_HandlerBase : access Standard.Org.Xml.Sax.HandlerBase.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_DefaultHandler : access Standard.Org.Xml.Sax.Helpers.DefaultHandler.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_File : access Standard.Java.Io.File.Typ'Class;
                    P2_HandlerBase : access Standard.Org.Xml.Sax.HandlerBase.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_File : access Standard.Java.Io.File.Typ'Class;
                    P2_DefaultHandler : access Standard.Org.Xml.Sax.Helpers.DefaultHandler.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class;
                    P2_HandlerBase : access Standard.Org.Xml.Sax.HandlerBase.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class;
                    P2_DefaultHandler : access Standard.Org.Xml.Sax.Helpers.DefaultHandler.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function GetParser (This : access Typ)
                       return access Org.Xml.Sax.Parser.Typ'Class is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   function GetXMLReader (This : access Typ)
                          return access Org.Xml.Sax.XMLReader.Typ'Class is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   function IsNamespaceAware (This : access Typ)
                              return Java.Boolean is abstract;

   function IsValidating (This : access Typ)
                          return Java.Boolean is abstract;

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class;

   function IsXIncludeAware (This : access Typ)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SAXParser);
   pragma Export (Java, Reset, "reset");
   pragma Export (Java, Parse, "parse");
   pragma Export (Java, GetParser, "getParser");
   pragma Export (Java, GetXMLReader, "getXMLReader");
   pragma Export (Java, IsNamespaceAware, "isNamespaceAware");
   pragma Export (Java, IsValidating, "isValidating");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, IsXIncludeAware, "isXIncludeAware");

end Javax.Xml.Parsers.SAXParser;
pragma Import (Java, Javax.Xml.Parsers.SAXParser, "javax.xml.parsers.SAXParser");
pragma Extensions_Allowed (Off);
