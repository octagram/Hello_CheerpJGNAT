pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Javax.Xml.Parsers.SAXParser;
limited with Javax.Xml.Validation.Schema;
with Java.Lang.Object;

package Javax.Xml.Parsers.SAXParserFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SAXParserFactory (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Parsers.SAXParserFactory.Typ'Class;

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Javax.Xml.Parsers.SAXParserFactory.Typ'Class;

   function NewSAXParser (This : access Typ)
                          return access Javax.Xml.Parsers.SAXParser.Typ'Class is abstract;
   --  can raise Javax.Xml.Parsers.ParserConfigurationException.Except and
   --  Org.Xml.Sax.SAXException.Except

   procedure SetNamespaceAware (This : access Typ;
                                P1_Boolean : Java.Boolean);

   procedure SetValidating (This : access Typ;
                            P1_Boolean : Java.Boolean);

   function IsNamespaceAware (This : access Typ)
                              return Java.Boolean;

   function IsValidating (This : access Typ)
                          return Java.Boolean;

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Javax.Xml.Parsers.ParserConfigurationException.Except,
   --  Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Javax.Xml.Parsers.ParserConfigurationException.Except,
   --  Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class;

   procedure SetSchema (This : access Typ;
                        P1_Schema : access Standard.Javax.Xml.Validation.Schema.Typ'Class);

   procedure SetXIncludeAware (This : access Typ;
                               P1_Boolean : Java.Boolean);

   function IsXIncludeAware (This : access Typ)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SAXParserFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, NewSAXParser, "newSAXParser");
   pragma Export (Java, SetNamespaceAware, "setNamespaceAware");
   pragma Export (Java, SetValidating, "setValidating");
   pragma Export (Java, IsNamespaceAware, "isNamespaceAware");
   pragma Export (Java, IsValidating, "isValidating");
   pragma Export (Java, SetFeature, "setFeature");
   pragma Export (Java, GetFeature, "getFeature");
   pragma Export (Java, GetSchema, "getSchema");
   pragma Export (Java, SetSchema, "setSchema");
   pragma Export (Java, SetXIncludeAware, "setXIncludeAware");
   pragma Export (Java, IsXIncludeAware, "isXIncludeAware");

end Javax.Xml.Parsers.SAXParserFactory;
pragma Import (Java, Javax.Xml.Parsers.SAXParserFactory, "javax.xml.parsers.SAXParserFactory");
pragma Extensions_Allowed (Off);
