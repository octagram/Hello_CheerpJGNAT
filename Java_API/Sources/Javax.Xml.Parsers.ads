pragma Extensions_Allowed (On);
package Javax.Xml.Parsers is
   pragma Preelaborate;
end Javax.Xml.Parsers;
pragma Import (Java, Javax.Xml.Parsers, "javax.xml.parsers");
pragma Extensions_Allowed (Off);
