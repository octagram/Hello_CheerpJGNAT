pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Activation.DataHandler;
with Java.Lang.Object;

package Javax.Xml.Soap.AttachmentPart is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_AttachmentPart (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSize (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure ClearContent (This : access Typ) is abstract;

   function GetContent (This : access Typ)
                        return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetRawContent (This : access Typ)
                           return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetRawContentBytes (This : access Typ)
                                return Java.Byte_Arr is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetBase64Content (This : access Typ)
                              return access Java.Io.InputStream.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure SetContent (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetRawContent (This : access Typ;
                            P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure SetRawContentBytes (This : access Typ;
                                 P1_Byte_Arr : Java.Byte_Arr;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int;
                                 P4_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure SetBase64Content (This : access Typ;
                               P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetDataHandler (This : access Typ)
                            return access Javax.Activation.DataHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure SetDataHandler (This : access Typ;
                             P1_DataHandler : access Standard.Javax.Activation.DataHandler.Typ'Class) is abstract;

   function GetContentId (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetContentLocation (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   function GetContentType (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetContentId (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetContentLocation (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetContentType (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveMimeHeader (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure RemoveAllMimeHeaders (This : access Typ) is abstract;

   function GetMimeHeader (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return Standard.Java.Lang.Object.Ref is abstract;

   procedure SetMimeHeader (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure AddMimeHeader (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAllMimeHeaders (This : access Typ)
                               return access Java.Util.Iterator.Typ'Class is abstract;

   function GetMatchingMimeHeaders (This : access Typ;
                                    P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                    return access Java.Util.Iterator.Typ'Class is abstract;

   function GetNonMatchingMimeHeaders (This : access Typ;
                                       P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                       return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttachmentPart);
   pragma Export (Java, GetSize, "getSize");
   pragma Export (Java, ClearContent, "clearContent");
   pragma Export (Java, GetContent, "getContent");
   pragma Export (Java, GetRawContent, "getRawContent");
   pragma Export (Java, GetRawContentBytes, "getRawContentBytes");
   pragma Export (Java, GetBase64Content, "getBase64Content");
   pragma Export (Java, SetContent, "setContent");
   pragma Export (Java, SetRawContent, "setRawContent");
   pragma Export (Java, SetRawContentBytes, "setRawContentBytes");
   pragma Export (Java, SetBase64Content, "setBase64Content");
   pragma Export (Java, GetDataHandler, "getDataHandler");
   pragma Export (Java, SetDataHandler, "setDataHandler");
   pragma Export (Java, GetContentId, "getContentId");
   pragma Export (Java, GetContentLocation, "getContentLocation");
   pragma Export (Java, GetContentType, "getContentType");
   pragma Export (Java, SetContentId, "setContentId");
   pragma Export (Java, SetContentLocation, "setContentLocation");
   pragma Export (Java, SetContentType, "setContentType");
   pragma Export (Java, RemoveMimeHeader, "removeMimeHeader");
   pragma Export (Java, RemoveAllMimeHeaders, "removeAllMimeHeaders");
   pragma Export (Java, GetMimeHeader, "getMimeHeader");
   pragma Export (Java, SetMimeHeader, "setMimeHeader");
   pragma Export (Java, AddMimeHeader, "addMimeHeader");
   pragma Export (Java, GetAllMimeHeaders, "getAllMimeHeaders");
   pragma Export (Java, GetMatchingMimeHeaders, "getMatchingMimeHeaders");
   pragma Export (Java, GetNonMatchingMimeHeaders, "getNonMatchingMimeHeaders");

end Javax.Xml.Soap.AttachmentPart;
pragma Import (Java, Javax.Xml.Soap.AttachmentPart, "javax.xml.soap.AttachmentPart");
pragma Extensions_Allowed (Off);
