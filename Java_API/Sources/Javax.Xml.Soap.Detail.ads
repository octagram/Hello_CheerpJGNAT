pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Soap.DetailEntry;
limited with Javax.Xml.Soap.Name;
with Java.Lang.Object;
with Javax.Xml.Soap.SOAPFaultElement;

package Javax.Xml.Soap.Detail is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SOAPFaultElement_I : Javax.Xml.Soap.SOAPFaultElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddDetailEntry (This : access Typ;
                            P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                            return access Javax.Xml.Soap.DetailEntry.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddDetailEntry (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                            return access Javax.Xml.Soap.DetailEntry.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetDetailEntries (This : access Typ)
                              return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddDetailEntry, "addDetailEntry");
   pragma Export (Java, GetDetailEntries, "getDetailEntries");

end Javax.Xml.Soap.Detail;
pragma Import (Java, Javax.Xml.Soap.Detail, "javax.xml.soap.Detail");
pragma Extensions_Allowed (Off);
