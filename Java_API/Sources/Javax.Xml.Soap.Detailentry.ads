pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Xml.Soap.SOAPElement;

package Javax.Xml.Soap.DetailEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SOAPElement_I : Javax.Xml.Soap.SOAPElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Javax.Xml.Soap.DetailEntry;
pragma Import (Java, Javax.Xml.Soap.DetailEntry, "javax.xml.soap.DetailEntry");
pragma Extensions_Allowed (Off);
