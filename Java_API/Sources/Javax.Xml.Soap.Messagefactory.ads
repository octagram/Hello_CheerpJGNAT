pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Javax.Xml.Soap.MimeHeaders;
limited with Javax.Xml.Soap.SOAPMessage;
with Java.Lang.Object;

package Javax.Xml.Soap.MessageFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_MessageFactory (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Soap.MessageFactory.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Soap.MessageFactory.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateMessage (This : access Typ)
                           return access Javax.Xml.Soap.SOAPMessage.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateMessage (This : access Typ;
                           P1_MimeHeaders : access Standard.Javax.Xml.Soap.MimeHeaders.Typ'Class;
                           P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                           return access Javax.Xml.Soap.SOAPMessage.Typ'Class is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MessageFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, CreateMessage, "createMessage");

end Javax.Xml.Soap.MessageFactory;
pragma Import (Java, Javax.Xml.Soap.MessageFactory, "javax.xml.soap.MessageFactory");
pragma Extensions_Allowed (Off);
