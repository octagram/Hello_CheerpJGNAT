pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
with Java.Lang.Object;

package Javax.Xml.Soap.MimeHeaders is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MimeHeaders (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHeader (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Standard.Java.Lang.Object.Ref;

   procedure SetHeader (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure AddHeader (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveHeader (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveAllHeaders (This : access Typ);

   function GetAllHeaders (This : access Typ)
                           return access Java.Util.Iterator.Typ'Class;

   function GetMatchingHeaders (This : access Typ;
                                P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                return access Java.Util.Iterator.Typ'Class;

   function GetNonMatchingHeaders (This : access Typ;
                                   P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                   return access Java.Util.Iterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MimeHeaders);
   pragma Import (Java, GetHeader, "getHeader");
   pragma Import (Java, SetHeader, "setHeader");
   pragma Import (Java, AddHeader, "addHeader");
   pragma Import (Java, RemoveHeader, "removeHeader");
   pragma Import (Java, RemoveAllHeaders, "removeAllHeaders");
   pragma Import (Java, GetAllHeaders, "getAllHeaders");
   pragma Import (Java, GetMatchingHeaders, "getMatchingHeaders");
   pragma Import (Java, GetNonMatchingHeaders, "getNonMatchingHeaders");

end Javax.Xml.Soap.MimeHeaders;
pragma Import (Java, Javax.Xml.Soap.MimeHeaders, "javax.xml.soap.MimeHeaders");
pragma Extensions_Allowed (Off);
