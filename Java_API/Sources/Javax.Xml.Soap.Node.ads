pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Soap.SOAPElement;
with Java.Lang.Object;
with Org.W3c.Dom.Node;

package Javax.Xml.Soap.Node is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Org.W3c.Dom.Node.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetParentElement (This : access Typ;
                               P1_SOAPElement : access Standard.Javax.Xml.Soap.SOAPElement.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetParentElement (This : access Typ)
                              return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;

   procedure DetachNode (This : access Typ) is abstract;

   procedure RecycleNode (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, SetParentElement, "setParentElement");
   pragma Export (Java, GetParentElement, "getParentElement");
   pragma Export (Java, DetachNode, "detachNode");
   pragma Export (Java, RecycleNode, "recycleNode");

end Javax.Xml.Soap.Node;
pragma Import (Java, Javax.Xml.Soap.Node, "javax.xml.soap.Node");
pragma Extensions_Allowed (Off);
