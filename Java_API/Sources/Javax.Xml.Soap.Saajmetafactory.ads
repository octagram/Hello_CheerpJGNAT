pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Soap.MessageFactory;
limited with Javax.Xml.Soap.SOAPFactory;
with Java.Lang.Object;

package Javax.Xml.Soap.SAAJMetaFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SAAJMetaFactory (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  protected
   function NewMessageFactory (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Javax.Xml.Soap.MessageFactory.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   --  protected
   function NewSOAPFactory (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Javax.Xml.Soap.SOAPFactory.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SAAJMetaFactory);
   pragma Export (Java, NewMessageFactory, "newMessageFactory");
   pragma Export (Java, NewSOAPFactory, "newSOAPFactory");

end Javax.Xml.Soap.SAAJMetaFactory;
pragma Import (Java, Javax.Xml.Soap.SAAJMetaFactory, "javax.xml.soap.SAAJMetaFactory");
pragma Extensions_Allowed (Off);
