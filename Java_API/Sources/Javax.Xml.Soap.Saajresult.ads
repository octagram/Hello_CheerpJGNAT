pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Soap.Node;
limited with Javax.Xml.Soap.SOAPElement;
limited with Javax.Xml.Soap.SOAPMessage;
with Java.Lang.Object;
with Javax.Xml.Transform.Dom.DOMResult;
with Javax.Xml.Transform.Result;

package Javax.Xml.Soap.SAAJResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Result_I : Javax.Xml.Transform.Result.Ref)
    is new Javax.Xml.Transform.Dom.DOMResult.Typ(Result_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SAAJResult (This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function New_SAAJResult (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function New_SAAJResult (P1_SOAPMessage : access Standard.Javax.Xml.Soap.SOAPMessage.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_SAAJResult (P1_SOAPElement : access Standard.Javax.Xml.Soap.SOAPElement.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetResult (This : access Typ)
                       return access Javax.Xml.Soap.Node.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SAAJResult);
   pragma Import (Java, GetResult, "getResult");

end Javax.Xml.Soap.SAAJResult;
pragma Import (Java, Javax.Xml.Soap.SAAJResult, "javax.xml.soap.SAAJResult");
pragma Extensions_Allowed (Off);
