pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Soap.Name;
limited with Javax.Xml.Soap.SOAPBodyElement;
limited with Javax.Xml.Soap.SOAPFault;
limited with Org.W3c.Dom.Document;
with Java.Lang.Object;
with Javax.Xml.Soap.SOAPElement;

package Javax.Xml.Soap.SOAPBody is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SOAPElement_I : Javax.Xml.Soap.SOAPElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddFault (This : access Typ)
                      return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddFault (This : access Typ;
                      P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                      return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddFault (This : access Typ;
                      P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_Locale : access Standard.Java.Util.Locale.Typ'Class)
                      return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddFault (This : access Typ;
                      P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddFault (This : access Typ;
                      P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function HasFault (This : access Typ)
                      return Java.Boolean is abstract;

   function GetFault (This : access Typ)
                      return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;

   function AddBodyElement (This : access Typ;
                            P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                            return access Javax.Xml.Soap.SOAPBodyElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddBodyElement (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                            return access Javax.Xml.Soap.SOAPBodyElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddDocument (This : access Typ;
                         P1_Document : access Standard.Org.W3c.Dom.Document.Typ'Class)
                         return access Javax.Xml.Soap.SOAPBodyElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function ExtractContentAsDocument (This : access Typ)
                                      return access Org.W3c.Dom.Document.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddFault, "addFault");
   pragma Export (Java, HasFault, "hasFault");
   pragma Export (Java, GetFault, "getFault");
   pragma Export (Java, AddBodyElement, "addBodyElement");
   pragma Export (Java, AddDocument, "addDocument");
   pragma Export (Java, ExtractContentAsDocument, "extractContentAsDocument");

end Javax.Xml.Soap.SOAPBody;
pragma Import (Java, Javax.Xml.Soap.SOAPBody, "javax.xml.soap.SOAPBody");
pragma Extensions_Allowed (Off);
