pragma Extensions_Allowed (On);
limited with Javax.Xml.Soap.SOAPMessage;
with Java.Lang.Object;

package Javax.Xml.Soap.SOAPConnection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SOAPConnection (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Call (This : access Typ;
                  P1_SOAPMessage : access Standard.Javax.Xml.Soap.SOAPMessage.Typ'Class;
                  P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                  return access Javax.Xml.Soap.SOAPMessage.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Javax.Xml.Soap.SOAPMessage.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SOAPConnection);
   pragma Export (Java, Call, "call");
   pragma Export (Java, Get, "get");
   pragma Export (Java, Close, "close");

end Javax.Xml.Soap.SOAPConnection;
pragma Import (Java, Javax.Xml.Soap.SOAPConnection, "javax.xml.soap.SOAPConnection");
pragma Extensions_Allowed (Off);
