pragma Extensions_Allowed (On);
limited with Javax.Xml.Soap.SOAPConnection;
with Java.Lang.Object;

package Javax.Xml.Soap.SOAPConnectionFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SOAPConnectionFactory (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Soap.SOAPConnectionFactory.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except and
   --  Java.Lang.UnsupportedOperationException.Except

   function CreateConnection (This : access Typ)
                              return access Javax.Xml.Soap.SOAPConnection.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SOAPConnectionFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, CreateConnection, "createConnection");

end Javax.Xml.Soap.SOAPConnectionFactory;
pragma Import (Java, Javax.Xml.Soap.SOAPConnectionFactory, "javax.xml.soap.SOAPConnectionFactory");
pragma Extensions_Allowed (Off);
