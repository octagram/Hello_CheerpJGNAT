pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Soap.SOAPConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DYNAMIC_SOAP_PROTOCOL : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP_1_1_PROTOCOL : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP_1_2_PROTOCOL : constant access Java.Lang.String.Typ'Class;

   --  final
   DEFAULT_SOAP_PROTOCOL : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_NS_SOAP_1_1_ENVELOPE : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_NS_SOAP_1_2_ENVELOPE : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_NS_SOAP_ENVELOPE : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_NS_SOAP_ENCODING : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_NS_SOAP_1_2_ENCODING : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP_1_1_CONTENT_TYPE : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP_1_2_CONTENT_TYPE : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_SOAP_ACTOR_NEXT : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_SOAP_1_2_ROLE_NEXT : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_SOAP_1_2_ROLE_NONE : constant access Java.Lang.String.Typ'Class;

   --  final
   URI_SOAP_1_2_ROLE_ULTIMATE_RECEIVER : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP_ENV_PREFIX : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP_VERSIONMISMATCH_FAULT : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   SOAP_MUSTUNDERSTAND_FAULT : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   SOAP_DATAENCODINGUNKNOWN_FAULT : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   SOAP_SENDER_FAULT : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   SOAP_RECEIVER_FAULT : access Javax.Xml.Namespace.QName.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, DYNAMIC_SOAP_PROTOCOL, "DYNAMIC_SOAP_PROTOCOL");
   pragma Import (Java, SOAP_1_1_PROTOCOL, "SOAP_1_1_PROTOCOL");
   pragma Import (Java, SOAP_1_2_PROTOCOL, "SOAP_1_2_PROTOCOL");
   pragma Import (Java, DEFAULT_SOAP_PROTOCOL, "DEFAULT_SOAP_PROTOCOL");
   pragma Import (Java, URI_NS_SOAP_1_1_ENVELOPE, "URI_NS_SOAP_1_1_ENVELOPE");
   pragma Import (Java, URI_NS_SOAP_1_2_ENVELOPE, "URI_NS_SOAP_1_2_ENVELOPE");
   pragma Import (Java, URI_NS_SOAP_ENVELOPE, "URI_NS_SOAP_ENVELOPE");
   pragma Import (Java, URI_NS_SOAP_ENCODING, "URI_NS_SOAP_ENCODING");
   pragma Import (Java, URI_NS_SOAP_1_2_ENCODING, "URI_NS_SOAP_1_2_ENCODING");
   pragma Import (Java, SOAP_1_1_CONTENT_TYPE, "SOAP_1_1_CONTENT_TYPE");
   pragma Import (Java, SOAP_1_2_CONTENT_TYPE, "SOAP_1_2_CONTENT_TYPE");
   pragma Import (Java, URI_SOAP_ACTOR_NEXT, "URI_SOAP_ACTOR_NEXT");
   pragma Import (Java, URI_SOAP_1_2_ROLE_NEXT, "URI_SOAP_1_2_ROLE_NEXT");
   pragma Import (Java, URI_SOAP_1_2_ROLE_NONE, "URI_SOAP_1_2_ROLE_NONE");
   pragma Import (Java, URI_SOAP_1_2_ROLE_ULTIMATE_RECEIVER, "URI_SOAP_1_2_ROLE_ULTIMATE_RECEIVER");
   pragma Import (Java, SOAP_ENV_PREFIX, "SOAP_ENV_PREFIX");
   pragma Import (Java, SOAP_VERSIONMISMATCH_FAULT, "SOAP_VERSIONMISMATCH_FAULT");
   pragma Import (Java, SOAP_MUSTUNDERSTAND_FAULT, "SOAP_MUSTUNDERSTAND_FAULT");
   pragma Import (Java, SOAP_DATAENCODINGUNKNOWN_FAULT, "SOAP_DATAENCODINGUNKNOWN_FAULT");
   pragma Import (Java, SOAP_SENDER_FAULT, "SOAP_SENDER_FAULT");
   pragma Import (Java, SOAP_RECEIVER_FAULT, "SOAP_RECEIVER_FAULT");

end Javax.Xml.Soap.SOAPConstants;
pragma Import (Java, Javax.Xml.Soap.SOAPConstants, "javax.xml.soap.SOAPConstants");
pragma Extensions_Allowed (Off);
