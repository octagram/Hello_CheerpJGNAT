pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Soap.Name;
with Java.Lang.Object;
with Javax.Xml.Soap.Node;
with Org.W3c.Dom.Element;

package Javax.Xml.Soap.SOAPElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Javax.Xml.Soap.Node.Ref;
            Element_I : Org.W3c.Dom.Element.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddChildElement (This : access Typ;
                             P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                             return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddChildElement (This : access Typ;
                             P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                             return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddChildElement (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddChildElement (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddChildElement (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddChildElement (This : access Typ;
                             P1_SOAPElement : access Standard.Javax.Xml.Soap.SOAPElement.Typ'Class)
                             return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure RemoveContents (This : access Typ) is abstract;

   function AddTextNode (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddAttribute (This : access Typ;
                          P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddAttribute (This : access Typ;
                          P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddNamespaceDeclaration (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                                     return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetAttributeValue (This : access Typ;
                               P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                               return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeValue (This : access Typ;
                               P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                               return access Java.Lang.String.Typ'Class is abstract;

   function GetAllAttributes (This : access Typ)
                              return access Java.Util.Iterator.Typ'Class is abstract;

   function GetAllAttributesAsQNames (This : access Typ)
                                      return access Java.Util.Iterator.Typ'Class is abstract;

   function GetNamespaceURI (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetNamespacePrefixes (This : access Typ)
                                  return access Java.Util.Iterator.Typ'Class is abstract;

   function GetVisibleNamespacePrefixes (This : access Typ)
                                         return access Java.Util.Iterator.Typ'Class is abstract;

   function CreateQName (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Namespace.QName.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetElementName (This : access Typ)
                            return access Javax.Xml.Soap.Name.Typ'Class is abstract;

   function GetElementQName (This : access Typ)
                             return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function SetElementQName (This : access Typ;
                             P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                             return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function RemoveAttribute (This : access Typ;
                             P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                             return Java.Boolean is abstract;

   function RemoveAttribute (This : access Typ;
                             P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                             return Java.Boolean is abstract;

   function RemoveNamespaceDeclaration (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Java.Boolean is abstract;

   function GetChildElements (This : access Typ)
                              return access Java.Util.Iterator.Typ'Class is abstract;

   function GetChildElements (This : access Typ;
                              P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                              return access Java.Util.Iterator.Typ'Class is abstract;

   function GetChildElements (This : access Typ;
                              P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                              return access Java.Util.Iterator.Typ'Class is abstract;

   procedure SetEncodingStyle (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetEncodingStyle (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddChildElement, "addChildElement");
   pragma Export (Java, RemoveContents, "removeContents");
   pragma Export (Java, AddTextNode, "addTextNode");
   pragma Export (Java, AddAttribute, "addAttribute");
   pragma Export (Java, AddNamespaceDeclaration, "addNamespaceDeclaration");
   pragma Export (Java, GetAttributeValue, "getAttributeValue");
   pragma Export (Java, GetAllAttributes, "getAllAttributes");
   pragma Export (Java, GetAllAttributesAsQNames, "getAllAttributesAsQNames");
   pragma Export (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Export (Java, GetNamespacePrefixes, "getNamespacePrefixes");
   pragma Export (Java, GetVisibleNamespacePrefixes, "getVisibleNamespacePrefixes");
   pragma Export (Java, CreateQName, "createQName");
   pragma Export (Java, GetElementName, "getElementName");
   pragma Export (Java, GetElementQName, "getElementQName");
   pragma Export (Java, SetElementQName, "setElementQName");
   pragma Export (Java, RemoveAttribute, "removeAttribute");
   pragma Export (Java, RemoveNamespaceDeclaration, "removeNamespaceDeclaration");
   pragma Export (Java, GetChildElements, "getChildElements");
   pragma Export (Java, SetEncodingStyle, "setEncodingStyle");
   pragma Export (Java, GetEncodingStyle, "getEncodingStyle");

end Javax.Xml.Soap.SOAPElement;
pragma Import (Java, Javax.Xml.Soap.SOAPElement, "javax.xml.soap.SOAPElement");
pragma Extensions_Allowed (Off);
