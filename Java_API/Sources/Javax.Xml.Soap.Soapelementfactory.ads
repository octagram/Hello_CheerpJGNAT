pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Xml.Soap.SOAPElementFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Soap.SOAPElementFactory.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewInstance, "newInstance");

end Javax.Xml.Soap.SOAPElementFactory;
pragma Import (Java, Javax.Xml.Soap.SOAPElementFactory, "javax.xml.soap.SOAPElementFactory");
pragma Extensions_Allowed (Off);
