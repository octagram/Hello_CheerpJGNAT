pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Soap.Name;
limited with Javax.Xml.Soap.SOAPBody;
limited with Javax.Xml.Soap.SOAPHeader;
with Java.Lang.Object;
with Javax.Xml.Soap.SOAPElement;

package Javax.Xml.Soap.SOAPEnvelope is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SOAPElement_I : Javax.Xml.Soap.SOAPElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Soap.Name.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Soap.Name.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetHeader (This : access Typ)
                       return access Javax.Xml.Soap.SOAPHeader.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetBody (This : access Typ)
                     return access Javax.Xml.Soap.SOAPBody.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddHeader (This : access Typ)
                       return access Javax.Xml.Soap.SOAPHeader.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddBody (This : access Typ)
                     return access Javax.Xml.Soap.SOAPBody.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateName, "createName");
   pragma Export (Java, GetHeader, "getHeader");
   pragma Export (Java, GetBody, "getBody");
   pragma Export (Java, AddHeader, "addHeader");
   pragma Export (Java, AddBody, "addBody");

end Javax.Xml.Soap.SOAPEnvelope;
pragma Import (Java, Javax.Xml.Soap.SOAPEnvelope, "javax.xml.soap.SOAPEnvelope");
pragma Extensions_Allowed (Off);
