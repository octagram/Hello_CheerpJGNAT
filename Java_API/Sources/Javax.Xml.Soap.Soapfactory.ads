pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Soap.Detail;
limited with Javax.Xml.Soap.Name;
limited with Javax.Xml.Soap.SOAPElement;
limited with Javax.Xml.Soap.SOAPFault;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;

package Javax.Xml.Soap.SOAPFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SOAPFactory (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateElement (This : access Typ;
                           P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class)
                           return access Javax.Xml.Soap.SOAPElement.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateElement (This : access Typ;
                           P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                           return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateElement (This : access Typ;
                           P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                           return access Javax.Xml.Soap.SOAPElement.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Xml.Soap.SOAPElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateDetail (This : access Typ)
                          return access Javax.Xml.Soap.Detail.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateFault (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                         return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateFault (This : access Typ)
                         return access Javax.Xml.Soap.SOAPFault.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Soap.Name.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function CreateName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Soap.Name.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function NewInstance return access Javax.Xml.Soap.SOAPFactory.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Soap.SOAPFactory.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SOAPFactory);
   pragma Export (Java, CreateElement, "createElement");
   pragma Export (Java, CreateDetail, "createDetail");
   pragma Export (Java, CreateFault, "createFault");
   pragma Export (Java, CreateName, "createName");
   pragma Export (Java, NewInstance, "newInstance");

end Javax.Xml.Soap.SOAPFactory;
pragma Import (Java, Javax.Xml.Soap.SOAPFactory, "javax.xml.soap.SOAPFactory");
pragma Extensions_Allowed (Off);
