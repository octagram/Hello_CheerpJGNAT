pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Java.Util.Locale;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Soap.Detail;
limited with Javax.Xml.Soap.Name;
with Java.Lang.Object;
with Javax.Xml.Soap.SOAPBodyElement;

package Javax.Xml.Soap.SOAPFault is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SOAPBodyElement_I : Javax.Xml.Soap.SOAPBodyElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFaultCode (This : access Typ;
                           P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure SetFaultCode (This : access Typ;
                           P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure SetFaultCode (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultCodeAsName (This : access Typ)
                                return access Javax.Xml.Soap.Name.Typ'Class is abstract;

   function GetFaultCodeAsQName (This : access Typ)
                                 return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetFaultSubcodes (This : access Typ)
                              return access Java.Util.Iterator.Typ'Class is abstract;

   procedure RemoveAllFaultSubcodes (This : access Typ) is abstract;

   procedure AppendFaultSubcode (This : access Typ;
                                 P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultCode (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFaultActor (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultActor (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFaultString (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure SetFaultString (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Locale : access Standard.Java.Util.Locale.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultString (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetFaultStringLocale (This : access Typ)
                                  return access Java.Util.Locale.Typ'Class is abstract;

   function HasDetail (This : access Typ)
                       return Java.Boolean is abstract;

   function GetDetail (This : access Typ)
                       return access Javax.Xml.Soap.Detail.Typ'Class is abstract;

   function AddDetail (This : access Typ)
                       return access Javax.Xml.Soap.Detail.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultReasonLocales (This : access Typ)
                                   return access Java.Util.Iterator.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultReasonTexts (This : access Typ)
                                 return access Java.Util.Iterator.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultReasonText (This : access Typ;
                                P1_Locale : access Standard.Java.Util.Locale.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure AddFaultReasonText (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Locale : access Standard.Java.Util.Locale.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultNode (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFaultNode (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetFaultRole (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFaultRole (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetFaultCode, "setFaultCode");
   pragma Export (Java, GetFaultCodeAsName, "getFaultCodeAsName");
   pragma Export (Java, GetFaultCodeAsQName, "getFaultCodeAsQName");
   pragma Export (Java, GetFaultSubcodes, "getFaultSubcodes");
   pragma Export (Java, RemoveAllFaultSubcodes, "removeAllFaultSubcodes");
   pragma Export (Java, AppendFaultSubcode, "appendFaultSubcode");
   pragma Export (Java, GetFaultCode, "getFaultCode");
   pragma Export (Java, SetFaultActor, "setFaultActor");
   pragma Export (Java, GetFaultActor, "getFaultActor");
   pragma Export (Java, SetFaultString, "setFaultString");
   pragma Export (Java, GetFaultString, "getFaultString");
   pragma Export (Java, GetFaultStringLocale, "getFaultStringLocale");
   pragma Export (Java, HasDetail, "hasDetail");
   pragma Export (Java, GetDetail, "getDetail");
   pragma Export (Java, AddDetail, "addDetail");
   pragma Export (Java, GetFaultReasonLocales, "getFaultReasonLocales");
   pragma Export (Java, GetFaultReasonTexts, "getFaultReasonTexts");
   pragma Export (Java, GetFaultReasonText, "getFaultReasonText");
   pragma Export (Java, AddFaultReasonText, "addFaultReasonText");
   pragma Export (Java, GetFaultNode, "getFaultNode");
   pragma Export (Java, SetFaultNode, "setFaultNode");
   pragma Export (Java, GetFaultRole, "getFaultRole");
   pragma Export (Java, SetFaultRole, "setFaultRole");

end Javax.Xml.Soap.SOAPFault;
pragma Import (Java, Javax.Xml.Soap.SOAPFault, "javax.xml.soap.SOAPFault");
pragma Extensions_Allowed (Off);
