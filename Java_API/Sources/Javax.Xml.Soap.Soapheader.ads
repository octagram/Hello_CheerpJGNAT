pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Soap.Name;
limited with Javax.Xml.Soap.SOAPHeaderElement;
with Java.Lang.Object;
with Javax.Xml.Soap.SOAPElement;

package Javax.Xml.Soap.SOAPHeader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SOAPElement_I : Javax.Xml.Soap.SOAPElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AddHeaderElement (This : access Typ;
                              P1_Name : access Standard.Javax.Xml.Soap.Name.Typ'Class)
                              return access Javax.Xml.Soap.SOAPHeaderElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddHeaderElement (This : access Typ;
                              P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                              return access Javax.Xml.Soap.SOAPHeaderElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function ExamineMustUnderstandHeaderElements (This : access Typ;
                                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                                 return access Java.Util.Iterator.Typ'Class is abstract;

   function ExamineHeaderElements (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Util.Iterator.Typ'Class is abstract;

   function ExtractHeaderElements (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Java.Util.Iterator.Typ'Class is abstract;

   function AddNotUnderstoodHeaderElement (This : access Typ;
                                           P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                                           return access Javax.Xml.Soap.SOAPHeaderElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddUpgradeHeaderElement (This : access Typ;
                                     P1_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                                     return access Javax.Xml.Soap.SOAPHeaderElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddUpgradeHeaderElement (This : access Typ;
                                     P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                     return access Javax.Xml.Soap.SOAPHeaderElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function AddUpgradeHeaderElement (This : access Typ;
                                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                                     return access Javax.Xml.Soap.SOAPHeaderElement.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function ExamineAllHeaderElements (This : access Typ)
                                      return access Java.Util.Iterator.Typ'Class is abstract;

   function ExtractAllHeaderElements (This : access Typ)
                                      return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddHeaderElement, "addHeaderElement");
   pragma Export (Java, ExamineMustUnderstandHeaderElements, "examineMustUnderstandHeaderElements");
   pragma Export (Java, ExamineHeaderElements, "examineHeaderElements");
   pragma Export (Java, ExtractHeaderElements, "extractHeaderElements");
   pragma Export (Java, AddNotUnderstoodHeaderElement, "addNotUnderstoodHeaderElement");
   pragma Export (Java, AddUpgradeHeaderElement, "addUpgradeHeaderElement");
   pragma Export (Java, ExamineAllHeaderElements, "examineAllHeaderElements");
   pragma Export (Java, ExtractAllHeaderElements, "extractAllHeaderElements");

end Javax.Xml.Soap.SOAPHeader;
pragma Import (Java, Javax.Xml.Soap.SOAPHeader, "javax.xml.soap.SOAPHeader");
pragma Extensions_Allowed (Off);
