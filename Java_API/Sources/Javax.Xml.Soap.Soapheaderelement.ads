pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Soap.SOAPElement;

package Javax.Xml.Soap.SOAPHeaderElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            SOAPElement_I : Javax.Xml.Soap.SOAPElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetActor (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure SetRole (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetActor (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function GetRole (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMustUnderstand (This : access Typ;
                                P1_Boolean : Java.Boolean) is abstract;

   function GetMustUnderstand (This : access Typ)
                               return Java.Boolean is abstract;

   procedure SetRelay (This : access Typ;
                       P1_Boolean : Java.Boolean) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetRelay (This : access Typ)
                      return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetActor, "setActor");
   pragma Export (Java, SetRole, "setRole");
   pragma Export (Java, GetActor, "getActor");
   pragma Export (Java, GetRole, "getRole");
   pragma Export (Java, SetMustUnderstand, "setMustUnderstand");
   pragma Export (Java, GetMustUnderstand, "getMustUnderstand");
   pragma Export (Java, SetRelay, "setRelay");
   pragma Export (Java, GetRelay, "getRelay");

end Javax.Xml.Soap.SOAPHeaderElement;
pragma Import (Java, Javax.Xml.Soap.SOAPHeaderElement, "javax.xml.soap.SOAPHeaderElement");
pragma Extensions_Allowed (Off);
