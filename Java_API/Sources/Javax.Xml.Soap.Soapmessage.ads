pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Activation.DataHandler;
limited with Javax.Xml.Soap.AttachmentPart;
limited with Javax.Xml.Soap.MimeHeaders;
limited with Javax.Xml.Soap.SOAPBody;
limited with Javax.Xml.Soap.SOAPElement;
limited with Javax.Xml.Soap.SOAPHeader;
limited with Javax.Xml.Soap.SOAPPart;
with Java.Lang.Object;

package Javax.Xml.Soap.SOAPMessage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SOAPMessage (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetContentDescription (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetContentDescription (This : access Typ)
                                   return access Java.Lang.String.Typ'Class is abstract;

   function GetSOAPPart (This : access Typ)
                         return access Javax.Xml.Soap.SOAPPart.Typ'Class is abstract;

   function GetSOAPBody (This : access Typ)
                         return access Javax.Xml.Soap.SOAPBody.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetSOAPHeader (This : access Typ)
                           return access Javax.Xml.Soap.SOAPHeader.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure RemoveAllAttachments (This : access Typ) is abstract;

   function CountAttachments (This : access Typ)
                              return Java.Int is abstract;

   function GetAttachments (This : access Typ)
                            return access Java.Util.Iterator.Typ'Class is abstract;

   function GetAttachments (This : access Typ;
                            P1_MimeHeaders : access Standard.Javax.Xml.Soap.MimeHeaders.Typ'Class)
                            return access Java.Util.Iterator.Typ'Class is abstract;

   procedure RemoveAttachments (This : access Typ;
                                P1_MimeHeaders : access Standard.Javax.Xml.Soap.MimeHeaders.Typ'Class) is abstract;

   function GetAttachment (This : access Typ;
                           P1_SOAPElement : access Standard.Javax.Xml.Soap.SOAPElement.Typ'Class)
                           return access Javax.Xml.Soap.AttachmentPart.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   procedure AddAttachmentPart (This : access Typ;
                                P1_AttachmentPart : access Standard.Javax.Xml.Soap.AttachmentPart.Typ'Class) is abstract;

   function CreateAttachmentPart (This : access Typ)
                                  return access Javax.Xml.Soap.AttachmentPart.Typ'Class is abstract;

   function CreateAttachmentPart (This : access Typ;
                                  P1_DataHandler : access Standard.Javax.Activation.DataHandler.Typ'Class)
                                  return access Javax.Xml.Soap.AttachmentPart.Typ'Class;

   function GetMimeHeaders (This : access Typ)
                            return access Javax.Xml.Soap.MimeHeaders.Typ'Class is abstract;

   function CreateAttachmentPart (This : access Typ;
                                  P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Xml.Soap.AttachmentPart.Typ'Class;

   procedure SaveChanges (This : access Typ) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function SaveRequired (This : access Typ)
                          return Java.Boolean is abstract;

   procedure WriteTo (This : access Typ;
                      P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except and
   --  Java.Io.IOException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CHARACTER_SET_ENCODING : constant access Java.Lang.String.Typ'Class;

   --  final
   WRITE_XML_DECLARATION : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SOAPMessage);
   pragma Export (Java, SetContentDescription, "setContentDescription");
   pragma Export (Java, GetContentDescription, "getContentDescription");
   pragma Export (Java, GetSOAPPart, "getSOAPPart");
   pragma Export (Java, GetSOAPBody, "getSOAPBody");
   pragma Export (Java, GetSOAPHeader, "getSOAPHeader");
   pragma Export (Java, RemoveAllAttachments, "removeAllAttachments");
   pragma Export (Java, CountAttachments, "countAttachments");
   pragma Export (Java, GetAttachments, "getAttachments");
   pragma Export (Java, RemoveAttachments, "removeAttachments");
   pragma Export (Java, GetAttachment, "getAttachment");
   pragma Export (Java, AddAttachmentPart, "addAttachmentPart");
   pragma Export (Java, CreateAttachmentPart, "createAttachmentPart");
   pragma Export (Java, GetMimeHeaders, "getMimeHeaders");
   pragma Export (Java, SaveChanges, "saveChanges");
   pragma Export (Java, SaveRequired, "saveRequired");
   pragma Export (Java, WriteTo, "writeTo");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Import (Java, CHARACTER_SET_ENCODING, "CHARACTER_SET_ENCODING");
   pragma Import (Java, WRITE_XML_DECLARATION, "WRITE_XML_DECLARATION");

end Javax.Xml.Soap.SOAPMessage;
pragma Import (Java, Javax.Xml.Soap.SOAPMessage, "javax.xml.soap.SOAPMessage");
pragma Extensions_Allowed (Off);
