pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Xml.Soap.SOAPEnvelope;
limited with Javax.Xml.Transform.Source;
with Java.Lang.Object;
with Javax.Xml.Soap.Node;
with Org.W3c.Dom.Document;

package Javax.Xml.Soap.SOAPPart is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Node_I : Javax.Xml.Soap.Node.Ref;
            Document_I : Org.W3c.Dom.Document.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_SOAPPart (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEnvelope (This : access Typ)
                         return access Javax.Xml.Soap.SOAPEnvelope.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetContentId (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function GetContentLocation (This : access Typ)
                                return access Java.Lang.String.Typ'Class;

   procedure SetContentId (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetContentLocation (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveMimeHeader (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure RemoveAllMimeHeaders (This : access Typ) is abstract;

   function GetMimeHeader (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return Standard.Java.Lang.Object.Ref is abstract;

   procedure SetMimeHeader (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure AddMimeHeader (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAllMimeHeaders (This : access Typ)
                               return access Java.Util.Iterator.Typ'Class is abstract;

   function GetMatchingMimeHeaders (This : access Typ;
                                    P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                    return access Java.Util.Iterator.Typ'Class is abstract;

   function GetNonMatchingMimeHeaders (This : access Typ;
                                       P1_String_Arr : access Java.Lang.String.Arr_Obj)
                                       return access Java.Util.Iterator.Typ'Class is abstract;

   procedure SetContent (This : access Typ;
                         P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class) is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except

   function GetContent (This : access Typ)
                        return access Javax.Xml.Transform.Source.Typ'Class is abstract;
   --  can raise Javax.Xml.Soap.SOAPException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SOAPPart);
   pragma Export (Java, GetEnvelope, "getEnvelope");
   pragma Export (Java, GetContentId, "getContentId");
   pragma Export (Java, GetContentLocation, "getContentLocation");
   pragma Export (Java, SetContentId, "setContentId");
   pragma Export (Java, SetContentLocation, "setContentLocation");
   pragma Export (Java, RemoveMimeHeader, "removeMimeHeader");
   pragma Export (Java, RemoveAllMimeHeaders, "removeAllMimeHeaders");
   pragma Export (Java, GetMimeHeader, "getMimeHeader");
   pragma Export (Java, SetMimeHeader, "setMimeHeader");
   pragma Export (Java, AddMimeHeader, "addMimeHeader");
   pragma Export (Java, GetAllMimeHeaders, "getAllMimeHeaders");
   pragma Export (Java, GetMatchingMimeHeaders, "getMatchingMimeHeaders");
   pragma Export (Java, GetNonMatchingMimeHeaders, "getNonMatchingMimeHeaders");
   pragma Export (Java, SetContent, "setContent");
   pragma Export (Java, GetContent, "getContent");

end Javax.Xml.Soap.SOAPPart;
pragma Import (Java, Javax.Xml.Soap.SOAPPart, "javax.xml.soap.SOAPPart");
pragma Extensions_Allowed (Off);
