pragma Extensions_Allowed (On);
package Javax.Xml.Soap is
   pragma Preelaborate;
end Javax.Xml.Soap;
pragma Import (Java, Javax.Xml.Soap, "javax.xml.soap");
pragma Extensions_Allowed (Off);
