pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Stream.Events.XMLEvent;

package Javax.Xml.Stream.Events.Characters is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLEvent_I : Javax.Xml.Stream.Events.XMLEvent.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetData (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function IsWhiteSpace (This : access Typ)
                          return Java.Boolean is abstract;

   function IsCData (This : access Typ)
                     return Java.Boolean is abstract;

   function IsIgnorableWhiteSpace (This : access Typ)
                                   return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetData, "getData");
   pragma Export (Java, IsWhiteSpace, "isWhiteSpace");
   pragma Export (Java, IsCData, "isCData");
   pragma Export (Java, IsIgnorableWhiteSpace, "isIgnorableWhiteSpace");

end Javax.Xml.Stream.Events.Characters;
pragma Import (Java, Javax.Xml.Stream.Events.Characters, "javax.xml.stream.events.Characters");
pragma Extensions_Allowed (Off);
