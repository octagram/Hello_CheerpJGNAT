pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
with Java.Lang.Object;
with Javax.Xml.Stream.Events.XMLEvent;

package Javax.Xml.Stream.Events.DTD is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLEvent_I : Javax.Xml.Stream.Events.XMLEvent.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDocumentTypeDeclaration (This : access Typ)
                                        return access Java.Lang.String.Typ'Class is abstract;

   function GetProcessedDTD (This : access Typ)
                             return access Java.Lang.Object.Typ'Class is abstract;

   function GetNotations (This : access Typ)
                          return access Java.Util.List.Typ'Class is abstract;

   function GetEntities (This : access Typ)
                         return access Java.Util.List.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDocumentTypeDeclaration, "getDocumentTypeDeclaration");
   pragma Export (Java, GetProcessedDTD, "getProcessedDTD");
   pragma Export (Java, GetNotations, "getNotations");
   pragma Export (Java, GetEntities, "getEntities");

end Javax.Xml.Stream.Events.DTD;
pragma Import (Java, Javax.Xml.Stream.Events.DTD, "javax.xml.stream.events.DTD");
pragma Extensions_Allowed (Off);
