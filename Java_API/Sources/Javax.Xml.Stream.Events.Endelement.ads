pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;
with Javax.Xml.Stream.Events.XMLEvent;

package Javax.Xml.Stream.Events.EndElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLEvent_I : Javax.Xml.Stream.Events.XMLEvent.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetNamespaces (This : access Typ)
                           return access Java.Util.Iterator.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetNamespaces, "getNamespaces");

end Javax.Xml.Stream.Events.EndElement;
pragma Import (Java, Javax.Xml.Stream.Events.EndElement, "javax.xml.stream.events.EndElement");
pragma Extensions_Allowed (Off);
