pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Stream.Events.Attribute;

package Javax.Xml.Stream.Events.Namespace is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Attribute_I : Javax.Xml.Stream.Events.Attribute.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrefix (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function GetNamespaceURI (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   function IsDefaultNamespaceDeclaration (This : access Typ)
                                           return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPrefix, "getPrefix");
   pragma Export (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Export (Java, IsDefaultNamespaceDeclaration, "isDefaultNamespaceDeclaration");

end Javax.Xml.Stream.Events.Namespace;
pragma Import (Java, Javax.Xml.Stream.Events.Namespace, "javax.xml.stream.events.Namespace");
pragma Extensions_Allowed (Off);
