pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Stream.Events.XMLEvent;

package Javax.Xml.Stream.Events.StartDocument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLEvent_I : Javax.Xml.Stream.Events.XMLEvent.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetCharacterEncodingScheme (This : access Typ)
                                        return access Java.Lang.String.Typ'Class is abstract;

   function EncodingSet (This : access Typ)
                         return Java.Boolean is abstract;

   function IsStandalone (This : access Typ)
                          return Java.Boolean is abstract;

   function StandaloneSet (This : access Typ)
                           return Java.Boolean is abstract;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSystemId, "getSystemId");
   pragma Export (Java, GetCharacterEncodingScheme, "getCharacterEncodingScheme");
   pragma Export (Java, EncodingSet, "encodingSet");
   pragma Export (Java, IsStandalone, "isStandalone");
   pragma Export (Java, StandaloneSet, "standaloneSet");
   pragma Export (Java, GetVersion, "getVersion");

end Javax.Xml.Stream.Events.StartDocument;
pragma Import (Java, Javax.Xml.Stream.Events.StartDocument, "javax.xml.stream.events.StartDocument");
pragma Extensions_Allowed (Off);
