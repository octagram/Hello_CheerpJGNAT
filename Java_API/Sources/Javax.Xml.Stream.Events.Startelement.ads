pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Stream.Events.Attribute;
with Java.Lang.Object;
with Javax.Xml.Stream.Events.XMLEvent;

package Javax.Xml.Stream.Events.StartElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLEvent_I : Javax.Xml.Stream.Events.XMLEvent.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetAttributes (This : access Typ)
                           return access Java.Util.Iterator.Typ'Class is abstract;

   function GetNamespaces (This : access Typ)
                           return access Java.Util.Iterator.Typ'Class is abstract;

   function GetAttributeByName (This : access Typ;
                                P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                                return access Javax.Xml.Stream.Events.Attribute.Typ'Class is abstract;

   function GetNamespaceContext (This : access Typ)
                                 return access Javax.Xml.Namespace.NamespaceContext.Typ'Class is abstract;

   function GetNamespaceURI (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetNamespaces, "getNamespaces");
   pragma Export (Java, GetAttributeByName, "getAttributeByName");
   pragma Export (Java, GetNamespaceContext, "getNamespaceContext");
   pragma Export (Java, GetNamespaceURI, "getNamespaceURI");

end Javax.Xml.Stream.Events.StartElement;
pragma Import (Java, Javax.Xml.Stream.Events.StartElement, "javax.xml.stream.events.StartElement");
pragma Extensions_Allowed (Off);
