pragma Extensions_Allowed (On);
limited with Java.Io.Writer;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Stream.Events.Characters;
limited with Javax.Xml.Stream.Events.EndElement;
limited with Javax.Xml.Stream.Events.StartElement;
limited with Javax.Xml.Stream.Location;
with Java.Lang.Object;
with Javax.Xml.Stream.XMLStreamConstants;

package Javax.Xml.Stream.Events.XMLEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStreamConstants_I : Javax.Xml.Stream.XMLStreamConstants.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetEventType (This : access Typ)
                          return Java.Int is abstract;

   function GetLocation (This : access Typ)
                         return access Javax.Xml.Stream.Location.Typ'Class is abstract;

   function IsStartElement (This : access Typ)
                            return Java.Boolean is abstract;

   function IsAttribute (This : access Typ)
                         return Java.Boolean is abstract;

   function IsNamespace (This : access Typ)
                         return Java.Boolean is abstract;

   function IsEndElement (This : access Typ)
                          return Java.Boolean is abstract;

   function IsEntityReference (This : access Typ)
                               return Java.Boolean is abstract;

   function IsProcessingInstruction (This : access Typ)
                                     return Java.Boolean is abstract;

   function IsCharacters (This : access Typ)
                          return Java.Boolean is abstract;

   function IsStartDocument (This : access Typ)
                             return Java.Boolean is abstract;

   function IsEndDocument (This : access Typ)
                           return Java.Boolean is abstract;

   function AsStartElement (This : access Typ)
                            return access Javax.Xml.Stream.Events.StartElement.Typ'Class is abstract;

   function AsEndElement (This : access Typ)
                          return access Javax.Xml.Stream.Events.EndElement.Typ'Class is abstract;

   function AsCharacters (This : access Typ)
                          return access Javax.Xml.Stream.Events.Characters.Typ'Class is abstract;

   function GetSchemaType (This : access Typ)
                           return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   procedure WriteAsEncodedUnicode (This : access Typ;
                                    P1_Writer : access Standard.Java.Io.Writer.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetEventType, "getEventType");
   pragma Export (Java, GetLocation, "getLocation");
   pragma Export (Java, IsStartElement, "isStartElement");
   pragma Export (Java, IsAttribute, "isAttribute");
   pragma Export (Java, IsNamespace, "isNamespace");
   pragma Export (Java, IsEndElement, "isEndElement");
   pragma Export (Java, IsEntityReference, "isEntityReference");
   pragma Export (Java, IsProcessingInstruction, "isProcessingInstruction");
   pragma Export (Java, IsCharacters, "isCharacters");
   pragma Export (Java, IsStartDocument, "isStartDocument");
   pragma Export (Java, IsEndDocument, "isEndDocument");
   pragma Export (Java, AsStartElement, "asStartElement");
   pragma Export (Java, AsEndElement, "asEndElement");
   pragma Export (Java, AsCharacters, "asCharacters");
   pragma Export (Java, GetSchemaType, "getSchemaType");
   pragma Export (Java, WriteAsEncodedUnicode, "writeAsEncodedUnicode");

end Javax.Xml.Stream.Events.XMLEvent;
pragma Import (Java, Javax.Xml.Stream.Events.XMLEvent, "javax.xml.stream.events.XMLEvent");
pragma Extensions_Allowed (Off);
