pragma Extensions_Allowed (On);
package Javax.Xml.Stream.Events is
   pragma Preelaborate;
end Javax.Xml.Stream.Events;
pragma Import (Java, Javax.Xml.Stream.Events, "javax.xml.stream.events");
pragma Extensions_Allowed (Off);
