pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Error;
with Java.Lang.Object;

package Javax.Xml.Stream.FactoryConfigurationError is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Error.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_FactoryConfigurationError (This : Ref := null)
                                           return Ref;

   function New_FactoryConfigurationError (P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   function New_FactoryConfigurationError (P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class;
                                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   function New_FactoryConfigurationError (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                           P2_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   function New_FactoryConfigurationError (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetException (This : access Typ)
                          return access Java.Lang.Exception_K.Typ'Class;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.stream.FactoryConfigurationError");
   pragma Java_Constructor (New_FactoryConfigurationError);
   pragma Import (Java, GetException, "getException");
   pragma Import (Java, GetMessage, "getMessage");

end Javax.Xml.Stream.FactoryConfigurationError;
pragma Import (Java, Javax.Xml.Stream.FactoryConfigurationError, "javax.xml.stream.FactoryConfigurationError");
pragma Extensions_Allowed (Off);
