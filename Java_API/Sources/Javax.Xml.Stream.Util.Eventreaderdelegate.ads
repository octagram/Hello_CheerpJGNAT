pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Stream.Events.XMLEvent;
with Java.Lang.Object;
with Javax.Xml.Stream.XMLEventReader;

package Javax.Xml.Stream.Util.EventReaderDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLEventReader_I : Javax.Xml.Stream.XMLEventReader.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EventReaderDelegate (This : Ref := null)
                                     return Ref;

   function New_EventReaderDelegate (P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class);

   function GetParent (This : access Typ)
                       return access Javax.Xml.Stream.XMLEventReader.Typ'Class;

   function NextEvent (This : access Typ)
                       return access Javax.Xml.Stream.Events.XMLEvent.Typ'Class;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function Next (This : access Typ)
                  return access Java.Lang.Object.Typ'Class;

   function HasNext (This : access Typ)
                     return Java.Boolean;

   function Peek (This : access Typ)
                  return access Javax.Xml.Stream.Events.XMLEvent.Typ'Class;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Close (This : access Typ);
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetElementText (This : access Typ)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function NextTag (This : access Typ)
                     return access Javax.Xml.Stream.Events.XMLEvent.Typ'Class;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure Remove (This : access Typ);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EventReaderDelegate);
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, NextEvent, "nextEvent");
   pragma Import (Java, Next, "next");
   pragma Import (Java, HasNext, "hasNext");
   pragma Import (Java, Peek, "peek");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GetElementText, "getElementText");
   pragma Import (Java, NextTag, "nextTag");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, Remove, "remove");

end Javax.Xml.Stream.Util.EventReaderDelegate;
pragma Import (Java, Javax.Xml.Stream.Util.EventReaderDelegate, "javax.xml.stream.util.EventReaderDelegate");
pragma Extensions_Allowed (Off);
