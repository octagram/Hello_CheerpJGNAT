pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Stream.Location;
with Java.Lang.Object;
with Javax.Xml.Stream.XMLStreamReader;

package Javax.Xml.Stream.Util.StreamReaderDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStreamReader_I : Javax.Xml.Stream.XMLStreamReader.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StreamReaderDelegate (This : Ref := null)
                                      return Ref;

   function New_StreamReaderDelegate (P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class);

   function GetParent (This : access Typ)
                       return access Javax.Xml.Stream.XMLStreamReader.Typ'Class;

   function Next (This : access Typ)
                  return Java.Int;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function NextTag (This : access Typ)
                     return Java.Int;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetElementText (This : access Typ)
                            return access Java.Lang.String.Typ'Class;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Require (This : access Typ;
                      P1_Int : Java.Int;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function HasNext (This : access Typ)
                     return Java.Boolean;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Close (This : access Typ);
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetNamespaceURI (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.String.Typ'Class;

   function GetNamespaceContext (This : access Typ)
                                 return access Javax.Xml.Namespace.NamespaceContext.Typ'Class;

   function IsStartElement (This : access Typ)
                            return Java.Boolean;

   function IsEndElement (This : access Typ)
                          return Java.Boolean;

   function IsCharacters (This : access Typ)
                          return Java.Boolean;

   function IsWhiteSpace (This : access Typ)
                          return Java.Boolean;

   function GetAttributeValue (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.String.Typ'Class;

   function GetAttributeCount (This : access Typ)
                               return Java.Int;

   function GetAttributeName (This : access Typ;
                              P1_Int : Java.Int)
                              return access Javax.Xml.Namespace.QName.Typ'Class;

   function GetAttributePrefix (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class;

   function GetAttributeNamespace (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Lang.String.Typ'Class;

   function GetAttributeLocalName (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Lang.String.Typ'Class;

   function GetAttributeType (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Lang.String.Typ'Class;

   function GetAttributeValue (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class;

   function IsAttributeSpecified (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Boolean;

   function GetNamespaceCount (This : access Typ)
                               return Java.Int;

   function GetNamespacePrefix (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class;

   function GetNamespaceURI (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Lang.String.Typ'Class;

   function GetEventType (This : access Typ)
                          return Java.Int;

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetTextCharacters (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Char_Arr : Java.Char_Arr;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int)
                               return Java.Int;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetTextCharacters (This : access Typ)
                               return Java.Char_Arr;

   function GetTextStart (This : access Typ)
                          return Java.Int;

   function GetTextLength (This : access Typ)
                           return Java.Int;

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function HasText (This : access Typ)
                     return Java.Boolean;

   function GetLocation (This : access Typ)
                         return access Javax.Xml.Stream.Location.Typ'Class;

   function GetName (This : access Typ)
                     return access Javax.Xml.Namespace.QName.Typ'Class;

   function GetLocalName (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   function HasName (This : access Typ)
                     return Java.Boolean;

   function GetNamespaceURI (This : access Typ)
                             return access Java.Lang.String.Typ'Class;

   function GetPrefix (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function IsStandalone (This : access Typ)
                          return Java.Boolean;

   function StandaloneSet (This : access Typ)
                           return Java.Boolean;

   function GetCharacterEncodingScheme (This : access Typ)
                                        return access Java.Lang.String.Typ'Class;

   function GetPITarget (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetPIData (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StreamReaderDelegate);
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, Next, "next");
   pragma Import (Java, NextTag, "nextTag");
   pragma Import (Java, GetElementText, "getElementText");
   pragma Import (Java, Require, "require");
   pragma Import (Java, HasNext, "hasNext");
   pragma Import (Java, Close, "close");
   pragma Import (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Import (Java, GetNamespaceContext, "getNamespaceContext");
   pragma Import (Java, IsStartElement, "isStartElement");
   pragma Import (Java, IsEndElement, "isEndElement");
   pragma Import (Java, IsCharacters, "isCharacters");
   pragma Import (Java, IsWhiteSpace, "isWhiteSpace");
   pragma Import (Java, GetAttributeValue, "getAttributeValue");
   pragma Import (Java, GetAttributeCount, "getAttributeCount");
   pragma Import (Java, GetAttributeName, "getAttributeName");
   pragma Import (Java, GetAttributePrefix, "getAttributePrefix");
   pragma Import (Java, GetAttributeNamespace, "getAttributeNamespace");
   pragma Import (Java, GetAttributeLocalName, "getAttributeLocalName");
   pragma Import (Java, GetAttributeType, "getAttributeType");
   pragma Import (Java, IsAttributeSpecified, "isAttributeSpecified");
   pragma Import (Java, GetNamespaceCount, "getNamespaceCount");
   pragma Import (Java, GetNamespacePrefix, "getNamespacePrefix");
   pragma Import (Java, GetEventType, "getEventType");
   pragma Import (Java, GetText, "getText");
   pragma Import (Java, GetTextCharacters, "getTextCharacters");
   pragma Import (Java, GetTextStart, "getTextStart");
   pragma Import (Java, GetTextLength, "getTextLength");
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, HasText, "hasText");
   pragma Import (Java, GetLocation, "getLocation");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetLocalName, "getLocalName");
   pragma Import (Java, HasName, "hasName");
   pragma Import (Java, GetPrefix, "getPrefix");
   pragma Import (Java, GetVersion, "getVersion");
   pragma Import (Java, IsStandalone, "isStandalone");
   pragma Import (Java, StandaloneSet, "standaloneSet");
   pragma Import (Java, GetCharacterEncodingScheme, "getCharacterEncodingScheme");
   pragma Import (Java, GetPITarget, "getPITarget");
   pragma Import (Java, GetPIData, "getPIData");
   pragma Import (Java, GetProperty, "getProperty");

end Javax.Xml.Stream.Util.StreamReaderDelegate;
pragma Import (Java, Javax.Xml.Stream.Util.StreamReaderDelegate, "javax.xml.stream.util.StreamReaderDelegate");
pragma Extensions_Allowed (Off);
