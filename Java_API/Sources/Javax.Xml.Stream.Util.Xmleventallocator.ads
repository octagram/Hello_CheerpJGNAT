pragma Extensions_Allowed (On);
limited with Javax.Xml.Stream.Events.XMLEvent;
limited with Javax.Xml.Stream.Util.XMLEventConsumer;
limited with Javax.Xml.Stream.XMLStreamReader;
with Java.Lang.Object;

package Javax.Xml.Stream.Util.XMLEventAllocator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance (This : access Typ)
                         return access Javax.Xml.Stream.Util.XMLEventAllocator.Typ'Class is abstract;

   function Allocate (This : access Typ;
                      P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class)
                      return access Javax.Xml.Stream.Events.XMLEvent.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Allocate (This : access Typ;
                       P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class;
                       P2_XMLEventConsumer : access Standard.Javax.Xml.Stream.Util.XMLEventConsumer.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, Allocate, "allocate");

end Javax.Xml.Stream.Util.XMLEventAllocator;
pragma Import (Java, Javax.Xml.Stream.Util.XMLEventAllocator, "javax.xml.stream.util.XMLEventAllocator");
pragma Extensions_Allowed (Off);
