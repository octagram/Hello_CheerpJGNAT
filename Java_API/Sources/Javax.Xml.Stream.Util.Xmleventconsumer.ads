pragma Extensions_Allowed (On);
limited with Javax.Xml.Stream.Events.XMLEvent;
with Java.Lang.Object;

package Javax.Xml.Stream.Util.XMLEventConsumer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Add (This : access Typ;
                  P1_XMLEvent : access Standard.Javax.Xml.Stream.Events.XMLEvent.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Add, "add");

end Javax.Xml.Stream.Util.XMLEventConsumer;
pragma Import (Java, Javax.Xml.Stream.Util.XMLEventConsumer, "javax.xml.stream.util.XMLEventConsumer");
pragma Extensions_Allowed (Off);
