pragma Extensions_Allowed (On);
package Javax.Xml.Stream.Util is
   pragma Preelaborate;
end Javax.Xml.Stream.Util;
pragma Import (Java, Javax.Xml.Stream.Util, "javax.xml.stream.util");
pragma Extensions_Allowed (Off);
