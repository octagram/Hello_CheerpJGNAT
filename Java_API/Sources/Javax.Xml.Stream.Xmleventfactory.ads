pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Util.Iterator;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Stream.Events.Attribute;
limited with Javax.Xml.Stream.Events.Characters;
limited with Javax.Xml.Stream.Events.Comment;
limited with Javax.Xml.Stream.Events.DTD;
limited with Javax.Xml.Stream.Events.EndDocument;
limited with Javax.Xml.Stream.Events.EndElement;
limited with Javax.Xml.Stream.Events.EntityDeclaration;
limited with Javax.Xml.Stream.Events.EntityReference;
limited with Javax.Xml.Stream.Events.Namespace;
limited with Javax.Xml.Stream.Events.ProcessingInstruction;
limited with Javax.Xml.Stream.Events.StartDocument;
limited with Javax.Xml.Stream.Events.StartElement;
limited with Javax.Xml.Stream.Location;
with Java.Lang.Object;

package Javax.Xml.Stream.XMLEventFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_XMLEventFactory (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Stream.XMLEventFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function NewFactory return access Javax.Xml.Stream.XMLEventFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function NewFactory (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                        return access Javax.Xml.Stream.XMLEventFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   procedure SetLocation (This : access Typ;
                          P1_Location : access Standard.Javax.Xml.Stream.Location.Typ'Class) is abstract;

   function CreateAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Stream.Events.Attribute.Typ'Class is abstract;

   function CreateAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Stream.Events.Attribute.Typ'Class is abstract;

   function CreateAttribute (This : access Typ;
                             P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Stream.Events.Attribute.Typ'Class is abstract;

   function CreateNamespace (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Stream.Events.Namespace.Typ'Class is abstract;

   function CreateNamespace (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Javax.Xml.Stream.Events.Namespace.Typ'Class is abstract;

   function CreateStartElement (This : access Typ;
                                P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                                P2_Iterator : access Standard.Java.Util.Iterator.Typ'Class;
                                P3_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                                return access Javax.Xml.Stream.Events.StartElement.Typ'Class is abstract;

   function CreateStartElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Javax.Xml.Stream.Events.StartElement.Typ'Class is abstract;

   function CreateStartElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_Iterator : access Standard.Java.Util.Iterator.Typ'Class;
                                P5_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                                return access Javax.Xml.Stream.Events.StartElement.Typ'Class is abstract;

   function CreateStartElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class;
                                P4_Iterator : access Standard.Java.Util.Iterator.Typ'Class;
                                P5_Iterator : access Standard.Java.Util.Iterator.Typ'Class;
                                P6_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class)
                                return access Javax.Xml.Stream.Events.StartElement.Typ'Class is abstract;

   function CreateEndElement (This : access Typ;
                              P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                              P2_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                              return access Javax.Xml.Stream.Events.EndElement.Typ'Class is abstract;

   function CreateEndElement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Javax.Xml.Stream.Events.EndElement.Typ'Class is abstract;

   function CreateEndElement (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_Iterator : access Standard.Java.Util.Iterator.Typ'Class)
                              return access Javax.Xml.Stream.Events.EndElement.Typ'Class is abstract;

   function CreateCharacters (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Javax.Xml.Stream.Events.Characters.Typ'Class is abstract;

   function CreateCData (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Stream.Events.Characters.Typ'Class is abstract;

   function CreateSpace (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Stream.Events.Characters.Typ'Class is abstract;

   function CreateIgnorableSpace (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Xml.Stream.Events.Characters.Typ'Class is abstract;

   function CreateStartDocument (This : access Typ)
                                 return access Javax.Xml.Stream.Events.StartDocument.Typ'Class is abstract;

   function CreateStartDocument (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_Boolean : Java.Boolean)
                                 return access Javax.Xml.Stream.Events.StartDocument.Typ'Class is abstract;

   function CreateStartDocument (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Javax.Xml.Stream.Events.StartDocument.Typ'Class is abstract;

   function CreateStartDocument (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Javax.Xml.Stream.Events.StartDocument.Typ'Class is abstract;

   function CreateEndDocument (This : access Typ)
                               return access Javax.Xml.Stream.Events.EndDocument.Typ'Class is abstract;

   function CreateEntityReference (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_EntityDeclaration : access Standard.Javax.Xml.Stream.Events.EntityDeclaration.Typ'Class)
                                   return access Javax.Xml.Stream.Events.EntityReference.Typ'Class is abstract;

   function CreateComment (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Javax.Xml.Stream.Events.Comment.Typ'Class is abstract;

   function CreateProcessingInstruction (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                                         return access Javax.Xml.Stream.Events.ProcessingInstruction.Typ'Class is abstract;

   function CreateDTD (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Javax.Xml.Stream.Events.DTD.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLEventFactory);
   pragma Import (Java, NewInstance, "newInstance");
   pragma Import (Java, NewFactory, "newFactory");
   pragma Export (Java, SetLocation, "setLocation");
   pragma Export (Java, CreateAttribute, "createAttribute");
   pragma Export (Java, CreateNamespace, "createNamespace");
   pragma Export (Java, CreateStartElement, "createStartElement");
   pragma Export (Java, CreateEndElement, "createEndElement");
   pragma Export (Java, CreateCharacters, "createCharacters");
   pragma Export (Java, CreateCData, "createCData");
   pragma Export (Java, CreateSpace, "createSpace");
   pragma Export (Java, CreateIgnorableSpace, "createIgnorableSpace");
   pragma Export (Java, CreateStartDocument, "createStartDocument");
   pragma Export (Java, CreateEndDocument, "createEndDocument");
   pragma Export (Java, CreateEntityReference, "createEntityReference");
   pragma Export (Java, CreateComment, "createComment");
   pragma Export (Java, CreateProcessingInstruction, "createProcessingInstruction");
   pragma Export (Java, CreateDTD, "createDTD");

end Javax.Xml.Stream.XMLEventFactory;
pragma Import (Java, Javax.Xml.Stream.XMLEventFactory, "javax.xml.stream.XMLEventFactory");
pragma Extensions_Allowed (Off);
