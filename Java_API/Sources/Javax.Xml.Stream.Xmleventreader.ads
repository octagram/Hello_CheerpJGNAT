pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Stream.Events.XMLEvent;
with Java.Lang.Object;
with Java.Util.Iterator;

package Javax.Xml.Stream.XMLEventReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Iterator_I : Java.Util.Iterator.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function NextEvent (This : access Typ)
                       return access Javax.Xml.Stream.Events.XMLEvent.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function HasNext (This : access Typ)
                     return Java.Boolean is abstract;

   function Peek (This : access Typ)
                  return access Javax.Xml.Stream.Events.XMLEvent.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetElementText (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function NextTag (This : access Typ)
                     return access Javax.Xml.Stream.Events.XMLEvent.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NextEvent, "nextEvent");
   pragma Export (Java, HasNext, "hasNext");
   pragma Export (Java, Peek, "peek");
   pragma Export (Java, GetElementText, "getElementText");
   pragma Export (Java, NextTag, "nextTag");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, Close, "close");

end Javax.Xml.Stream.XMLEventReader;
pragma Import (Java, Javax.Xml.Stream.XMLEventReader, "javax.xml.stream.XMLEventReader");
pragma Extensions_Allowed (Off);
