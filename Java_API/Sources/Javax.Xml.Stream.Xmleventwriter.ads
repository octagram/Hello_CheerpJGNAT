pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Stream.Events.XMLEvent;
limited with Javax.Xml.Stream.XMLEventReader;
with Java.Lang.Object;
with Javax.Xml.Stream.Util.XMLEventConsumer;

package Javax.Xml.Stream.XMLEventWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLEventConsumer_I : Javax.Xml.Stream.Util.XMLEventConsumer.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Flush (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Add (This : access Typ;
                  P1_XMLEvent : access Standard.Javax.Xml.Stream.Events.XMLEvent.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Add (This : access Typ;
                  P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetPrefix (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure SetPrefix (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure SetDefaultNamespace (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure SetNamespaceContext (This : access Typ;
                                  P1_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetNamespaceContext (This : access Typ)
                                 return access Javax.Xml.Namespace.NamespaceContext.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, Close, "close");
   pragma Export (Java, Add, "add");
   pragma Export (Java, GetPrefix, "getPrefix");
   pragma Export (Java, SetPrefix, "setPrefix");
   pragma Export (Java, SetDefaultNamespace, "setDefaultNamespace");
   pragma Export (Java, SetNamespaceContext, "setNamespaceContext");
   pragma Export (Java, GetNamespaceContext, "getNamespaceContext");

end Javax.Xml.Stream.XMLEventWriter;
pragma Import (Java, Javax.Xml.Stream.XMLEventWriter, "javax.xml.stream.XMLEventWriter");
pragma Extensions_Allowed (Off);
