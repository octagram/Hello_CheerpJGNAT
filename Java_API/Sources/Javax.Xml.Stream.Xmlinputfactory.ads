pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Javax.Xml.Stream.EventFilter;
limited with Javax.Xml.Stream.StreamFilter;
limited with Javax.Xml.Stream.Util.XMLEventAllocator;
limited with Javax.Xml.Stream.XMLEventReader;
limited with Javax.Xml.Stream.XMLReporter;
limited with Javax.Xml.Stream.XMLResolver;
limited with Javax.Xml.Stream.XMLStreamReader;
limited with Javax.Xml.Transform.Source;
with Java.Lang.Object;

package Javax.Xml.Stream.XMLInputFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_XMLInputFactory (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Stream.XMLInputFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function NewFactory return access Javax.Xml.Stream.XMLInputFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function NewFactory (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                        return access Javax.Xml.Stream.XMLInputFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function CreateXMLStreamReader (This : access Typ;
                                   P1_Reader : access Standard.Java.Io.Reader.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamReader (This : access Typ;
                                   P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamReader (This : access Typ;
                                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamReader (This : access Typ;
                                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamReader (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamReader (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Reader : access Standard.Java.Io.Reader.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventReader (This : access Typ;
                                  P1_Reader : access Standard.Java.Io.Reader.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventReader (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_Reader : access Standard.Java.Io.Reader.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventReader (This : access Typ;
                                  P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventReader (This : access Typ;
                                  P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventReader (This : access Typ;
                                  P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventReader (This : access Typ;
                                  P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventReader (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateFilteredReader (This : access Typ;
                                  P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class;
                                  P2_StreamFilter : access Standard.Javax.Xml.Stream.StreamFilter.Typ'Class)
                                  return access Javax.Xml.Stream.XMLStreamReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateFilteredReader (This : access Typ;
                                  P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class;
                                  P2_EventFilter : access Standard.Javax.Xml.Stream.EventFilter.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventReader.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetXMLResolver (This : access Typ)
                            return access Javax.Xml.Stream.XMLResolver.Typ'Class is abstract;

   procedure SetXMLResolver (This : access Typ;
                             P1_XMLResolver : access Standard.Javax.Xml.Stream.XMLResolver.Typ'Class) is abstract;

   function GetXMLReporter (This : access Typ)
                            return access Javax.Xml.Stream.XMLReporter.Typ'Class is abstract;

   procedure SetXMLReporter (This : access Typ;
                             P1_XMLReporter : access Standard.Javax.Xml.Stream.XMLReporter.Typ'Class) is abstract;

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function IsPropertySupported (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return Java.Boolean is abstract;

   procedure SetEventAllocator (This : access Typ;
                                P1_XMLEventAllocator : access Standard.Javax.Xml.Stream.Util.XMLEventAllocator.Typ'Class) is abstract;

   function GetEventAllocator (This : access Typ)
                               return access Javax.Xml.Stream.Util.XMLEventAllocator.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   IS_NAMESPACE_AWARE : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_VALIDATING : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_COALESCING : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_REPLACING_ENTITY_REFERENCES : constant access Java.Lang.String.Typ'Class;

   --  final
   IS_SUPPORTING_EXTERNAL_ENTITIES : constant access Java.Lang.String.Typ'Class;

   --  final
   SUPPORT_DTD : constant access Java.Lang.String.Typ'Class;

   --  final
   REPORTER : constant access Java.Lang.String.Typ'Class;

   --  final
   RESOLVER : constant access Java.Lang.String.Typ'Class;

   --  final
   ALLOCATOR : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLInputFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, NewFactory, "newFactory");
   pragma Export (Java, CreateXMLStreamReader, "createXMLStreamReader");
   pragma Export (Java, CreateXMLEventReader, "createXMLEventReader");
   pragma Export (Java, CreateFilteredReader, "createFilteredReader");
   pragma Export (Java, GetXMLResolver, "getXMLResolver");
   pragma Export (Java, SetXMLResolver, "setXMLResolver");
   pragma Export (Java, GetXMLReporter, "getXMLReporter");
   pragma Export (Java, SetXMLReporter, "setXMLReporter");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, IsPropertySupported, "isPropertySupported");
   pragma Export (Java, SetEventAllocator, "setEventAllocator");
   pragma Export (Java, GetEventAllocator, "getEventAllocator");
   pragma Import (Java, IS_NAMESPACE_AWARE, "IS_NAMESPACE_AWARE");
   pragma Import (Java, IS_VALIDATING, "IS_VALIDATING");
   pragma Import (Java, IS_COALESCING, "IS_COALESCING");
   pragma Import (Java, IS_REPLACING_ENTITY_REFERENCES, "IS_REPLACING_ENTITY_REFERENCES");
   pragma Import (Java, IS_SUPPORTING_EXTERNAL_ENTITIES, "IS_SUPPORTING_EXTERNAL_ENTITIES");
   pragma Import (Java, SUPPORT_DTD, "SUPPORT_DTD");
   pragma Import (Java, REPORTER, "REPORTER");
   pragma Import (Java, RESOLVER, "RESOLVER");
   pragma Import (Java, ALLOCATOR, "ALLOCATOR");

end Javax.Xml.Stream.XMLInputFactory;
pragma Import (Java, Javax.Xml.Stream.XMLInputFactory, "javax.xml.stream.XMLInputFactory");
pragma Extensions_Allowed (Off);
