pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Io.Writer;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Javax.Xml.Stream.XMLEventWriter;
limited with Javax.Xml.Stream.XMLStreamWriter;
limited with Javax.Xml.Transform.Result;
with Java.Lang.Object;

package Javax.Xml.Stream.XMLOutputFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_XMLOutputFactory (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Stream.XMLOutputFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function NewFactory return access Javax.Xml.Stream.XMLOutputFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function NewFactory (P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                        return access Javax.Xml.Stream.XMLOutputFactory.Typ'Class;
   --  can raise Javax.Xml.Stream.FactoryConfigurationError.Except

   function CreateXMLStreamWriter (This : access Typ;
                                   P1_Writer : access Standard.Java.Io.Writer.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamWriter (This : access Typ;
                                   P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamWriter (This : access Typ;
                                   P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLStreamWriter (This : access Typ;
                                   P1_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class)
                                   return access Javax.Xml.Stream.XMLStreamWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventWriter (This : access Typ;
                                  P1_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventWriter (This : access Typ;
                                  P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventWriter (This : access Typ;
                                  P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function CreateXMLEventWriter (This : access Typ;
                                  P1_Writer : access Standard.Java.Io.Writer.Typ'Class)
                                  return access Javax.Xml.Stream.XMLEventWriter.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function IsPropertySupported (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   IS_REPAIRING_NAMESPACES : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLOutputFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, NewFactory, "newFactory");
   pragma Export (Java, CreateXMLStreamWriter, "createXMLStreamWriter");
   pragma Export (Java, CreateXMLEventWriter, "createXMLEventWriter");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, IsPropertySupported, "isPropertySupported");
   pragma Import (Java, IS_REPAIRING_NAMESPACES, "IS_REPAIRING_NAMESPACES");

end Javax.Xml.Stream.XMLOutputFactory;
pragma Import (Java, Javax.Xml.Stream.XMLOutputFactory, "javax.xml.stream.XMLOutputFactory");
pragma Extensions_Allowed (Off);
