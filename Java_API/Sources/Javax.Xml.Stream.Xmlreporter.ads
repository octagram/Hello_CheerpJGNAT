pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Stream.Location;
with Java.Lang.Object;

package Javax.Xml.Stream.XMLReporter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Report (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P4_Location : access Standard.Javax.Xml.Stream.Location.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Report, "report");

end Javax.Xml.Stream.XMLReporter;
pragma Import (Java, Javax.Xml.Stream.XMLReporter, "javax.xml.stream.XMLReporter");
pragma Extensions_Allowed (Off);
