pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Xml.Stream.XMLStreamConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   START_ELEMENT : constant Java.Int;

   --  final
   END_ELEMENT : constant Java.Int;

   --  final
   PROCESSING_INSTRUCTION : constant Java.Int;

   --  final
   CHARACTERS : constant Java.Int;

   --  final
   COMMENT : constant Java.Int;

   --  final
   SPACE : constant Java.Int;

   --  final
   START_DOCUMENT : constant Java.Int;

   --  final
   END_DOCUMENT : constant Java.Int;

   --  final
   ENTITY_REFERENCE : constant Java.Int;

   --  final
   ATTRIBUTE : constant Java.Int;

   --  final
   DTD : constant Java.Int;

   --  final
   CDATA : constant Java.Int;

   --  final
   NAMESPACE : constant Java.Int;

   --  final
   NOTATION_DECLARATION : constant Java.Int;

   --  final
   ENTITY_DECLARATION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, START_ELEMENT, "START_ELEMENT");
   pragma Import (Java, END_ELEMENT, "END_ELEMENT");
   pragma Import (Java, PROCESSING_INSTRUCTION, "PROCESSING_INSTRUCTION");
   pragma Import (Java, CHARACTERS, "CHARACTERS");
   pragma Import (Java, COMMENT, "COMMENT");
   pragma Import (Java, SPACE, "SPACE");
   pragma Import (Java, START_DOCUMENT, "START_DOCUMENT");
   pragma Import (Java, END_DOCUMENT, "END_DOCUMENT");
   pragma Import (Java, ENTITY_REFERENCE, "ENTITY_REFERENCE");
   pragma Import (Java, ATTRIBUTE, "ATTRIBUTE");
   pragma Import (Java, DTD, "DTD");
   pragma Import (Java, CDATA, "CDATA");
   pragma Import (Java, NAMESPACE, "NAMESPACE");
   pragma Import (Java, NOTATION_DECLARATION, "NOTATION_DECLARATION");
   pragma Import (Java, ENTITY_DECLARATION, "ENTITY_DECLARATION");

end Javax.Xml.Stream.XMLStreamConstants;
pragma Import (Java, Javax.Xml.Stream.XMLStreamConstants, "javax.xml.stream.XMLStreamConstants");
pragma Extensions_Allowed (Off);
