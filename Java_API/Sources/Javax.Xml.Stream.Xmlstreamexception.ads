pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Xml.Stream.Location;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Xml.Stream.XMLStreamException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Nested : access Java.Lang.Throwable.Typ'Class;
      pragma Import (Java, Nested, "nested");

      --  protected
      Location : access Javax.Xml.Stream.Location.Typ'Class;
      pragma Import (Java, Location, "location");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XMLStreamException (This : Ref := null)
                                    return Ref;

   function New_XMLStreamException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_XMLStreamException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_XMLStreamException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_XMLStreamException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Location : access Standard.Javax.Xml.Stream.Location.Typ'Class;
                                    P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_XMLStreamException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_Location : access Standard.Javax.Xml.Stream.Location.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNestedException (This : access Typ)
                                return access Java.Lang.Throwable.Typ'Class;

   function GetLocation (This : access Typ)
                         return access Javax.Xml.Stream.Location.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.stream.XMLStreamException");
   pragma Java_Constructor (New_XMLStreamException);
   pragma Import (Java, GetNestedException, "getNestedException");
   pragma Import (Java, GetLocation, "getLocation");

end Javax.Xml.Stream.XMLStreamException;
pragma Import (Java, Javax.Xml.Stream.XMLStreamException, "javax.xml.stream.XMLStreamException");
pragma Extensions_Allowed (Off);
