pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Stream.Location;
with Java.Lang.Object;
with Javax.Xml.Stream.XMLStreamConstants;

package Javax.Xml.Stream.XMLStreamReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLStreamConstants_I : Javax.Xml.Stream.XMLStreamConstants.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function Next (This : access Typ)
                  return Java.Int is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Require (This : access Typ;
                      P1_Int : Java.Int;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetElementText (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function NextTag (This : access Typ)
                     return Java.Int is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function HasNext (This : access Typ)
                     return Java.Boolean is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetNamespaceURI (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Java.Lang.String.Typ'Class is abstract;

   function IsStartElement (This : access Typ)
                            return Java.Boolean is abstract;

   function IsEndElement (This : access Typ)
                          return Java.Boolean is abstract;

   function IsCharacters (This : access Typ)
                          return Java.Boolean is abstract;

   function IsWhiteSpace (This : access Typ)
                          return Java.Boolean is abstract;

   function GetAttributeValue (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeCount (This : access Typ)
                               return Java.Int is abstract;

   function GetAttributeName (This : access Typ;
                              P1_Int : Java.Int)
                              return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetAttributeNamespace (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeLocalName (This : access Typ;
                                   P1_Int : Java.Int)
                                   return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributePrefix (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeType (This : access Typ;
                              P1_Int : Java.Int)
                              return access Java.Lang.String.Typ'Class is abstract;

   function GetAttributeValue (This : access Typ;
                               P1_Int : Java.Int)
                               return access Java.Lang.String.Typ'Class is abstract;

   function IsAttributeSpecified (This : access Typ;
                                  P1_Int : Java.Int)
                                  return Java.Boolean is abstract;

   function GetNamespaceCount (This : access Typ)
                               return Java.Int is abstract;

   function GetNamespacePrefix (This : access Typ;
                                P1_Int : Java.Int)
                                return access Java.Lang.String.Typ'Class is abstract;

   function GetNamespaceURI (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetNamespaceContext (This : access Typ)
                                 return access Javax.Xml.Namespace.NamespaceContext.Typ'Class is abstract;

   function GetEventType (This : access Typ)
                          return Java.Int is abstract;

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetTextCharacters (This : access Typ)
                               return Java.Char_Arr is abstract;

   function GetTextCharacters (This : access Typ;
                               P1_Int : Java.Int;
                               P2_Char_Arr : Java.Char_Arr;
                               P3_Int : Java.Int;
                               P4_Int : Java.Int)
                               return Java.Int is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetTextStart (This : access Typ)
                          return Java.Int is abstract;

   function GetTextLength (This : access Typ)
                           return Java.Int is abstract;

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function HasText (This : access Typ)
                     return Java.Boolean is abstract;

   function GetLocation (This : access Typ)
                         return access Javax.Xml.Stream.Location.Typ'Class is abstract;

   function GetName (This : access Typ)
                     return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetLocalName (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function HasName (This : access Typ)
                     return Java.Boolean is abstract;

   function GetNamespaceURI (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetPrefix (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function IsStandalone (This : access Typ)
                          return Java.Boolean is abstract;

   function StandaloneSet (This : access Typ)
                           return Java.Boolean is abstract;

   function GetCharacterEncodingScheme (This : access Typ)
                                        return access Java.Lang.String.Typ'Class is abstract;

   function GetPITarget (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetPIData (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, Next, "next");
   pragma Export (Java, Require, "require");
   pragma Export (Java, GetElementText, "getElementText");
   pragma Export (Java, NextTag, "nextTag");
   pragma Export (Java, HasNext, "hasNext");
   pragma Export (Java, Close, "close");
   pragma Export (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Export (Java, IsStartElement, "isStartElement");
   pragma Export (Java, IsEndElement, "isEndElement");
   pragma Export (Java, IsCharacters, "isCharacters");
   pragma Export (Java, IsWhiteSpace, "isWhiteSpace");
   pragma Export (Java, GetAttributeValue, "getAttributeValue");
   pragma Export (Java, GetAttributeCount, "getAttributeCount");
   pragma Export (Java, GetAttributeName, "getAttributeName");
   pragma Export (Java, GetAttributeNamespace, "getAttributeNamespace");
   pragma Export (Java, GetAttributeLocalName, "getAttributeLocalName");
   pragma Export (Java, GetAttributePrefix, "getAttributePrefix");
   pragma Export (Java, GetAttributeType, "getAttributeType");
   pragma Export (Java, IsAttributeSpecified, "isAttributeSpecified");
   pragma Export (Java, GetNamespaceCount, "getNamespaceCount");
   pragma Export (Java, GetNamespacePrefix, "getNamespacePrefix");
   pragma Export (Java, GetNamespaceContext, "getNamespaceContext");
   pragma Export (Java, GetEventType, "getEventType");
   pragma Export (Java, GetText, "getText");
   pragma Export (Java, GetTextCharacters, "getTextCharacters");
   pragma Export (Java, GetTextStart, "getTextStart");
   pragma Export (Java, GetTextLength, "getTextLength");
   pragma Export (Java, GetEncoding, "getEncoding");
   pragma Export (Java, HasText, "hasText");
   pragma Export (Java, GetLocation, "getLocation");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetLocalName, "getLocalName");
   pragma Export (Java, HasName, "hasName");
   pragma Export (Java, GetPrefix, "getPrefix");
   pragma Export (Java, GetVersion, "getVersion");
   pragma Export (Java, IsStandalone, "isStandalone");
   pragma Export (Java, StandaloneSet, "standaloneSet");
   pragma Export (Java, GetCharacterEncodingScheme, "getCharacterEncodingScheme");
   pragma Export (Java, GetPITarget, "getPITarget");
   pragma Export (Java, GetPIData, "getPIData");

end Javax.Xml.Stream.XMLStreamReader;
pragma Import (Java, Javax.Xml.Stream.XMLStreamReader, "javax.xml.stream.XMLStreamReader");
pragma Extensions_Allowed (Off);
