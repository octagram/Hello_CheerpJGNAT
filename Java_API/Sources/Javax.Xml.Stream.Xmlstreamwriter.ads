pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.NamespaceContext;
with Java.Lang.Object;

package Javax.Xml.Stream.XMLStreamWriter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteStartElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteStartElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteStartElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteEmptyElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteEmptyElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteEmptyElement (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteEndElement (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteEndDocument (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Close (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure Flush (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteNamespace (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteDefaultNamespace (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteComment (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteProcessingInstruction (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteProcessingInstruction (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteCData (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteDTD (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteEntityRef (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteStartDocument (This : access Typ) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteStartDocument (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteStartDocument (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteCharacters (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure WriteCharacters (This : access Typ;
                              P1_Char_Arr : Java.Char_Arr;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetPrefix (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure SetPrefix (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure SetDefaultNamespace (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   procedure SetNamespaceContext (This : access Typ;
                                  P1_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class) is abstract;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function GetNamespaceContext (This : access Typ)
                                 return access Javax.Xml.Namespace.NamespaceContext.Typ'Class is abstract;

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, WriteStartElement, "writeStartElement");
   pragma Export (Java, WriteEmptyElement, "writeEmptyElement");
   pragma Export (Java, WriteEndElement, "writeEndElement");
   pragma Export (Java, WriteEndDocument, "writeEndDocument");
   pragma Export (Java, Close, "close");
   pragma Export (Java, Flush, "flush");
   pragma Export (Java, WriteAttribute, "writeAttribute");
   pragma Export (Java, WriteNamespace, "writeNamespace");
   pragma Export (Java, WriteDefaultNamespace, "writeDefaultNamespace");
   pragma Export (Java, WriteComment, "writeComment");
   pragma Export (Java, WriteProcessingInstruction, "writeProcessingInstruction");
   pragma Export (Java, WriteCData, "writeCData");
   pragma Export (Java, WriteDTD, "writeDTD");
   pragma Export (Java, WriteEntityRef, "writeEntityRef");
   pragma Export (Java, WriteStartDocument, "writeStartDocument");
   pragma Export (Java, WriteCharacters, "writeCharacters");
   pragma Export (Java, GetPrefix, "getPrefix");
   pragma Export (Java, SetPrefix, "setPrefix");
   pragma Export (Java, SetDefaultNamespace, "setDefaultNamespace");
   pragma Export (Java, SetNamespaceContext, "setNamespaceContext");
   pragma Export (Java, GetNamespaceContext, "getNamespaceContext");
   pragma Export (Java, GetProperty, "getProperty");

end Javax.Xml.Stream.XMLStreamWriter;
pragma Import (Java, Javax.Xml.Stream.XMLStreamWriter, "javax.xml.stream.XMLStreamWriter");
pragma Extensions_Allowed (Off);
