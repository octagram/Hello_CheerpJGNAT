pragma Extensions_Allowed (On);
package Javax.Xml.Stream is
   pragma Preelaborate;
end Javax.Xml.Stream;
pragma Import (Java, Javax.Xml.Stream, "javax.xml.stream");
pragma Extensions_Allowed (Off);
