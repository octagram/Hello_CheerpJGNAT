pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Transform.Result;

package Javax.Xml.Transform.Dom.DOMResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Result_I : Javax.Xml.Transform.Result.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMResult (This : Ref := null)
                           return Ref;

   function New_DOMResult (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_DOMResult (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_DOMResult (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                           P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_DOMResult (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                           P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetNode (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);

   function GetNode (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class;

   procedure SetNextSibling (This : access Typ;
                             P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);

   function GetNextSibling (This : access Typ)
                            return access Org.W3c.Dom.Node.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMResult);
   pragma Import (Java, SetNode, "setNode");
   pragma Import (Java, GetNode, "getNode");
   pragma Import (Java, SetNextSibling, "setNextSibling");
   pragma Import (Java, GetNextSibling, "getNextSibling");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, FEATURE, "FEATURE");

end Javax.Xml.Transform.Dom.DOMResult;
pragma Import (Java, Javax.Xml.Transform.Dom.DOMResult, "javax.xml.transform.dom.DOMResult");
pragma Extensions_Allowed (Off);
