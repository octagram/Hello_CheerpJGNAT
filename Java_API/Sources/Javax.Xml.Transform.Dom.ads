pragma Extensions_Allowed (On);
package Javax.Xml.Transform.Dom is
   pragma Preelaborate;
end Javax.Xml.Transform.Dom;
pragma Import (Java, Javax.Xml.Transform.Dom, "javax.xml.transform.dom");
pragma Extensions_Allowed (Off);
