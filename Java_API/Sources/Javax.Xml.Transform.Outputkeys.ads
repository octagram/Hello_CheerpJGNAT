pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Xml.Transform.OutputKeys is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   METHOD : constant access Java.Lang.String.Typ'Class;

   --  final
   VERSION : constant access Java.Lang.String.Typ'Class;

   --  final
   ENCODING : constant access Java.Lang.String.Typ'Class;

   --  final
   OMIT_XML_DECLARATION : constant access Java.Lang.String.Typ'Class;

   --  final
   STANDALONE : constant access Java.Lang.String.Typ'Class;

   --  final
   DOCTYPE_PUBLIC : constant access Java.Lang.String.Typ'Class;

   --  final
   DOCTYPE_SYSTEM : constant access Java.Lang.String.Typ'Class;

   --  final
   CDATA_SECTION_ELEMENTS : constant access Java.Lang.String.Typ'Class;

   --  final
   INDENT : constant access Java.Lang.String.Typ'Class;

   --  final
   MEDIA_TYPE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, METHOD, "METHOD");
   pragma Import (Java, VERSION, "VERSION");
   pragma Import (Java, ENCODING, "ENCODING");
   pragma Import (Java, OMIT_XML_DECLARATION, "OMIT_XML_DECLARATION");
   pragma Import (Java, STANDALONE, "STANDALONE");
   pragma Import (Java, DOCTYPE_PUBLIC, "DOCTYPE_PUBLIC");
   pragma Import (Java, DOCTYPE_SYSTEM, "DOCTYPE_SYSTEM");
   pragma Import (Java, CDATA_SECTION_ELEMENTS, "CDATA_SECTION_ELEMENTS");
   pragma Import (Java, INDENT, "INDENT");
   pragma Import (Java, MEDIA_TYPE, "MEDIA_TYPE");

end Javax.Xml.Transform.OutputKeys;
pragma Import (Java, Javax.Xml.Transform.OutputKeys, "javax.xml.transform.OutputKeys");
pragma Extensions_Allowed (Off);
