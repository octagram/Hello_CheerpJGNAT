pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Xml.Transform.Result is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PI_DISABLE_OUTPUT_ESCAPING : constant access Java.Lang.String.Typ'Class;

   --  final
   PI_ENABLE_OUTPUT_ESCAPING : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetSystemId, "setSystemId");
   pragma Export (Java, GetSystemId, "getSystemId");
   pragma Import (Java, PI_DISABLE_OUTPUT_ESCAPING, "PI_DISABLE_OUTPUT_ESCAPING");
   pragma Import (Java, PI_ENABLE_OUTPUT_ESCAPING, "PI_ENABLE_OUTPUT_ESCAPING");

end Javax.Xml.Transform.Result;
pragma Import (Java, Javax.Xml.Transform.Result, "javax.xml.transform.Result");
pragma Extensions_Allowed (Off);
