pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.ContentHandler;
limited with Org.Xml.Sax.Ext.LexicalHandler;
with Java.Lang.Object;
with Javax.Xml.Transform.Result;

package Javax.Xml.Transform.Sax.SAXResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Result_I : Javax.Xml.Transform.Result.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SAXResult (This : Ref := null)
                           return Ref;

   function New_SAXResult (P1_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetHandler (This : access Typ;
                         P1_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class);

   function GetHandler (This : access Typ)
                        return access Org.Xml.Sax.ContentHandler.Typ'Class;

   procedure SetLexicalHandler (This : access Typ;
                                P1_LexicalHandler : access Standard.Org.Xml.Sax.Ext.LexicalHandler.Typ'Class);

   function GetLexicalHandler (This : access Typ)
                               return access Org.Xml.Sax.Ext.LexicalHandler.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SAXResult);
   pragma Import (Java, SetHandler, "setHandler");
   pragma Import (Java, GetHandler, "getHandler");
   pragma Import (Java, SetLexicalHandler, "setLexicalHandler");
   pragma Import (Java, GetLexicalHandler, "getLexicalHandler");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, FEATURE, "FEATURE");

end Javax.Xml.Transform.Sax.SAXResult;
pragma Import (Java, Javax.Xml.Transform.Sax.SAXResult, "javax.xml.transform.sax.SAXResult");
pragma Extensions_Allowed (Off);
