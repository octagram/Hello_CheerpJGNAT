pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.InputSource;
limited with Org.Xml.Sax.XMLReader;
with Java.Lang.Object;
with Javax.Xml.Transform.Source;

package Javax.Xml.Transform.Sax.SAXSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Source_I : Javax.Xml.Transform.Source.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SAXSource (This : Ref := null)
                           return Ref;

   function New_SAXSource (P1_XMLReader : access Standard.Org.Xml.Sax.XMLReader.Typ'Class;
                           P2_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   function New_SAXSource (P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class; 
                           This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetXMLReader (This : access Typ;
                           P1_XMLReader : access Standard.Org.Xml.Sax.XMLReader.Typ'Class);

   function GetXMLReader (This : access Typ)
                          return access Org.Xml.Sax.XMLReader.Typ'Class;

   procedure SetInputSource (This : access Typ;
                             P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class);

   function GetInputSource (This : access Typ)
                            return access Org.Xml.Sax.InputSource.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function SourceToInputSource (P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                                 return access Org.Xml.Sax.InputSource.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SAXSource);
   pragma Import (Java, SetXMLReader, "setXMLReader");
   pragma Import (Java, GetXMLReader, "getXMLReader");
   pragma Import (Java, SetInputSource, "setInputSource");
   pragma Import (Java, GetInputSource, "getInputSource");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, SourceToInputSource, "sourceToInputSource");
   pragma Import (Java, FEATURE, "FEATURE");

end Javax.Xml.Transform.Sax.SAXSource;
pragma Import (Java, Javax.Xml.Transform.Sax.SAXSource, "javax.xml.transform.sax.SAXSource");
pragma Extensions_Allowed (Off);
