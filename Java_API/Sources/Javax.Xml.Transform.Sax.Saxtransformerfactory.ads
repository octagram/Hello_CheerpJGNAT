pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Sax.TemplatesHandler;
limited with Javax.Xml.Transform.Sax.TransformerHandler;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Transform.Templates;
limited with Org.Xml.Sax.XMLFilter;
with Java.Lang.Object;
with Javax.Xml.Transform.TransformerFactory;

package Javax.Xml.Transform.Sax.SAXTransformerFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Javax.Xml.Transform.TransformerFactory.Typ
      with null record;

   --  protected
   function New_SAXTransformerFactory (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewTransformerHandler (This : access Typ;
                                   P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                                   return access Javax.Xml.Transform.Sax.TransformerHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function NewTransformerHandler (This : access Typ;
                                   P1_Templates : access Standard.Javax.Xml.Transform.Templates.Typ'Class)
                                   return access Javax.Xml.Transform.Sax.TransformerHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function NewTransformerHandler (This : access Typ)
                                   return access Javax.Xml.Transform.Sax.TransformerHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function NewTemplatesHandler (This : access Typ)
                                 return access Javax.Xml.Transform.Sax.TemplatesHandler.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function NewXMLFilter (This : access Typ;
                          P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                          return access Org.Xml.Sax.XMLFilter.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function NewXMLFilter (This : access Typ;
                          P1_Templates : access Standard.Javax.Xml.Transform.Templates.Typ'Class)
                          return access Org.Xml.Sax.XMLFilter.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;

   --  final
   FEATURE_XMLFILTER : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SAXTransformerFactory);
   pragma Export (Java, NewTransformerHandler, "newTransformerHandler");
   pragma Export (Java, NewTemplatesHandler, "newTemplatesHandler");
   pragma Export (Java, NewXMLFilter, "newXMLFilter");
   pragma Import (Java, FEATURE, "FEATURE");
   pragma Import (Java, FEATURE_XMLFILTER, "FEATURE_XMLFILTER");

end Javax.Xml.Transform.Sax.SAXTransformerFactory;
pragma Import (Java, Javax.Xml.Transform.Sax.SAXTransformerFactory, "javax.xml.transform.sax.SAXTransformerFactory");
pragma Extensions_Allowed (Off);
