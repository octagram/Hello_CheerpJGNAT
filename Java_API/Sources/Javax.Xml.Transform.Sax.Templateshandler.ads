pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Templates;
with Java.Lang.Object;
with Org.Xml.Sax.ContentHandler;

package Javax.Xml.Transform.Sax.TemplatesHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ContentHandler_I : Org.Xml.Sax.ContentHandler.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTemplates (This : access Typ)
                          return access Javax.Xml.Transform.Templates.Typ'Class is abstract;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTemplates, "getTemplates");
   pragma Export (Java, SetSystemId, "setSystemId");
   pragma Export (Java, GetSystemId, "getSystemId");

end Javax.Xml.Transform.Sax.TemplatesHandler;
pragma Import (Java, Javax.Xml.Transform.Sax.TemplatesHandler, "javax.xml.transform.sax.TemplatesHandler");
pragma Extensions_Allowed (Off);
