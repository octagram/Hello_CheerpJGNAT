pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Transformer;
with Java.Lang.Object;
with Org.Xml.Sax.ContentHandler;
with Org.Xml.Sax.DTDHandler;
with Org.Xml.Sax.Ext.LexicalHandler;

package Javax.Xml.Transform.Sax.TransformerHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ContentHandler_I : Org.Xml.Sax.ContentHandler.Ref;
            DTDHandler_I : Org.Xml.Sax.DTDHandler.Ref;
            LexicalHandler_I : Org.Xml.Sax.Ext.LexicalHandler.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetResult (This : access Typ;
                        P1_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetTransformer (This : access Typ)
                            return access Javax.Xml.Transform.Transformer.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetResult, "setResult");
   pragma Export (Java, SetSystemId, "setSystemId");
   pragma Export (Java, GetSystemId, "getSystemId");
   pragma Export (Java, GetTransformer, "getTransformer");

end Javax.Xml.Transform.Sax.TransformerHandler;
pragma Import (Java, Javax.Xml.Transform.Sax.TransformerHandler, "javax.xml.transform.sax.TransformerHandler");
pragma Extensions_Allowed (Off);
