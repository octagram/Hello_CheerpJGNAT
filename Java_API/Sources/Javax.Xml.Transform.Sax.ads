pragma Extensions_Allowed (On);
package Javax.Xml.Transform.Sax is
   pragma Preelaborate;
end Javax.Xml.Transform.Sax;
pragma Import (Java, Javax.Xml.Transform.Sax, "javax.xml.transform.sax");
pragma Extensions_Allowed (Off);
