pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Xml.Transform.SourceLocator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublicId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetLineNumber (This : access Typ)
                           return Java.Int is abstract;

   function GetColumnNumber (This : access Typ)
                             return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPublicId, "getPublicId");
   pragma Export (Java, GetSystemId, "getSystemId");
   pragma Export (Java, GetLineNumber, "getLineNumber");
   pragma Export (Java, GetColumnNumber, "getColumnNumber");

end Javax.Xml.Transform.SourceLocator;
pragma Import (Java, Javax.Xml.Transform.SourceLocator, "javax.xml.transform.SourceLocator");
pragma Extensions_Allowed (Off);
