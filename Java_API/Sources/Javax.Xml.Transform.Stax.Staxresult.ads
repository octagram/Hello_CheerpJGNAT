pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Stream.XMLEventWriter;
limited with Javax.Xml.Stream.XMLStreamWriter;
with Java.Lang.Object;
with Javax.Xml.Transform.Result;

package Javax.Xml.Transform.Stax.StAXResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Result_I : Javax.Xml.Transform.Result.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StAXResult (P1_XMLEventWriter : access Standard.Javax.Xml.Stream.XMLEventWriter.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_StAXResult (P1_XMLStreamWriter : access Standard.Javax.Xml.Stream.XMLStreamWriter.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXMLEventWriter (This : access Typ)
                               return access Javax.Xml.Stream.XMLEventWriter.Typ'Class;

   function GetXMLStreamWriter (This : access Typ)
                                return access Javax.Xml.Stream.XMLStreamWriter.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StAXResult);
   pragma Import (Java, GetXMLEventWriter, "getXMLEventWriter");
   pragma Import (Java, GetXMLStreamWriter, "getXMLStreamWriter");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, FEATURE, "FEATURE");

end Javax.Xml.Transform.Stax.StAXResult;
pragma Import (Java, Javax.Xml.Transform.Stax.StAXResult, "javax.xml.transform.stax.StAXResult");
pragma Extensions_Allowed (Off);
