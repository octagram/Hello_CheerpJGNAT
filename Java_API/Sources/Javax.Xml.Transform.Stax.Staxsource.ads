pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Stream.XMLEventReader;
limited with Javax.Xml.Stream.XMLStreamReader;
with Java.Lang.Object;
with Javax.Xml.Transform.Source;

package Javax.Xml.Transform.Stax.StAXSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Source_I : Javax.Xml.Transform.Source.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StAXSource (P1_XMLEventReader : access Standard.Javax.Xml.Stream.XMLEventReader.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Stream.XMLStreamException.Except

   function New_StAXSource (P1_XMLStreamReader : access Standard.Javax.Xml.Stream.XMLStreamReader.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXMLEventReader (This : access Typ)
                               return access Javax.Xml.Stream.XMLEventReader.Typ'Class;

   function GetXMLStreamReader (This : access Typ)
                                return access Javax.Xml.Stream.XMLStreamReader.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StAXSource);
   pragma Import (Java, GetXMLEventReader, "getXMLEventReader");
   pragma Import (Java, GetXMLStreamReader, "getXMLStreamReader");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, FEATURE, "FEATURE");

end Javax.Xml.Transform.Stax.StAXSource;
pragma Import (Java, Javax.Xml.Transform.Stax.StAXSource, "javax.xml.transform.stax.StAXSource");
pragma Extensions_Allowed (Off);
