pragma Extensions_Allowed (On);
package Javax.Xml.Transform.Stax is
   pragma Preelaborate;
end Javax.Xml.Transform.Stax;
pragma Import (Java, Javax.Xml.Transform.Stax, "javax.xml.transform.stax");
pragma Extensions_Allowed (Off);
