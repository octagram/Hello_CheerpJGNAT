pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.OutputStream;
limited with Java.Io.Writer;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Transform.Result;

package Javax.Xml.Transform.Stream.StreamResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Result_I : Javax.Xml.Transform.Result.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StreamResult (This : Ref := null)
                              return Ref;

   function New_StreamResult (P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamResult (P1_Writer : access Standard.Java.Io.Writer.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamResult (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamResult (P1_File : access Standard.Java.Io.File.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetOutputStream (This : access Typ;
                              P1_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class);

   function GetOutputStream (This : access Typ)
                             return access Java.Io.OutputStream.Typ'Class;

   procedure SetWriter (This : access Typ;
                        P1_Writer : access Standard.Java.Io.Writer.Typ'Class);

   function GetWriter (This : access Typ)
                       return access Java.Io.Writer.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetSystemId (This : access Typ;
                          P1_File : access Standard.Java.Io.File.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StreamResult);
   pragma Import (Java, SetOutputStream, "setOutputStream");
   pragma Import (Java, GetOutputStream, "getOutputStream");
   pragma Import (Java, SetWriter, "setWriter");
   pragma Import (Java, GetWriter, "getWriter");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, FEATURE, "FEATURE");

end Javax.Xml.Transform.Stream.StreamResult;
pragma Import (Java, Javax.Xml.Transform.Stream.StreamResult, "javax.xml.transform.stream.StreamResult");
pragma Extensions_Allowed (Off);
