pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Transform.Source;

package Javax.Xml.Transform.Stream.StreamSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Source_I : Javax.Xml.Transform.Source.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StreamSource (This : Ref := null)
                              return Ref;

   function New_StreamSource (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamSource (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamSource (P1_Reader : access Standard.Java.Io.Reader.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamSource (P1_Reader : access Standard.Java.Io.Reader.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamSource (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_StreamSource (P1_File : access Standard.Java.Io.File.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetInputStream (This : access Typ;
                             P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;

   procedure SetReader (This : access Typ;
                        P1_Reader : access Standard.Java.Io.Reader.Typ'Class);

   function GetReader (This : access Typ)
                       return access Java.Io.Reader.Typ'Class;

   procedure SetPublicId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPublicId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_File : access Standard.Java.Io.File.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FEATURE : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StreamSource);
   pragma Import (Java, SetInputStream, "setInputStream");
   pragma Import (Java, GetInputStream, "getInputStream");
   pragma Import (Java, SetReader, "setReader");
   pragma Import (Java, GetReader, "getReader");
   pragma Import (Java, SetPublicId, "setPublicId");
   pragma Import (Java, GetPublicId, "getPublicId");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, FEATURE, "FEATURE");

end Javax.Xml.Transform.Stream.StreamSource;
pragma Import (Java, Javax.Xml.Transform.Stream.StreamSource, "javax.xml.transform.stream.StreamSource");
pragma Extensions_Allowed (Off);
