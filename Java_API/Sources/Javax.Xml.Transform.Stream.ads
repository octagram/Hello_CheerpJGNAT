pragma Extensions_Allowed (On);
package Javax.Xml.Transform.Stream is
   pragma Preelaborate;
end Javax.Xml.Transform.Stream;
pragma Import (Java, Javax.Xml.Transform.Stream, "javax.xml.transform.stream");
pragma Extensions_Allowed (Off);
