pragma Extensions_Allowed (On);
limited with Java.Util.Properties;
limited with Javax.Xml.Transform.Transformer;
with Java.Lang.Object;

package Javax.Xml.Transform.Templates is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewTransformer (This : access Typ)
                            return access Javax.Xml.Transform.Transformer.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function GetOutputProperties (This : access Typ)
                                 return access Java.Util.Properties.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NewTransformer, "newTransformer");
   pragma Export (Java, GetOutputProperties, "getOutputProperties");

end Javax.Xml.Transform.Templates;
pragma Import (Java, Javax.Xml.Transform.Templates, "javax.xml.transform.Templates");
pragma Extensions_Allowed (Off);
