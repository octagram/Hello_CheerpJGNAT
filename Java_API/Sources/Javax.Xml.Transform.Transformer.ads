pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Properties;
limited with Javax.Xml.Transform.ErrorListener;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Transform.URIResolver;
with Java.Lang.Object;

package Javax.Xml.Transform.Transformer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Transformer (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ);

   procedure Transform (This : access Typ;
                        P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class;
                        P2_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class) is abstract;
   --  can raise Javax.Xml.Transform.TransformerException.Except

   procedure SetParameter (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   procedure ClearParameters (This : access Typ) is abstract;

   procedure SetURIResolver (This : access Typ;
                             P1_URIResolver : access Standard.Javax.Xml.Transform.URIResolver.Typ'Class) is abstract;

   function GetURIResolver (This : access Typ)
                            return access Javax.Xml.Transform.URIResolver.Typ'Class is abstract;

   procedure SetOutputProperties (This : access Typ;
                                  P1_Properties : access Standard.Java.Util.Properties.Typ'Class) is abstract;

   function GetOutputProperties (This : access Typ)
                                 return access Java.Util.Properties.Typ'Class is abstract;

   procedure SetOutputProperty (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetOutputProperty (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   procedure SetErrorListener (This : access Typ;
                               P1_ErrorListener : access Standard.Javax.Xml.Transform.ErrorListener.Typ'Class) is abstract;
   --  can raise Java.Lang.IllegalArgumentException.Except

   function GetErrorListener (This : access Typ)
                              return access Javax.Xml.Transform.ErrorListener.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Transformer);
   pragma Import (Java, Reset, "reset");
   pragma Export (Java, Transform, "transform");
   pragma Export (Java, SetParameter, "setParameter");
   pragma Export (Java, GetParameter, "getParameter");
   pragma Export (Java, ClearParameters, "clearParameters");
   pragma Export (Java, SetURIResolver, "setURIResolver");
   pragma Export (Java, GetURIResolver, "getURIResolver");
   pragma Export (Java, SetOutputProperties, "setOutputProperties");
   pragma Export (Java, GetOutputProperties, "getOutputProperties");
   pragma Export (Java, SetOutputProperty, "setOutputProperty");
   pragma Export (Java, GetOutputProperty, "getOutputProperty");
   pragma Export (Java, SetErrorListener, "setErrorListener");
   pragma Export (Java, GetErrorListener, "getErrorListener");

end Javax.Xml.Transform.Transformer;
pragma Import (Java, Javax.Xml.Transform.Transformer, "javax.xml.transform.Transformer");
pragma Extensions_Allowed (Off);
