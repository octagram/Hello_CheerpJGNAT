pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Xml.Transform.SourceLocator;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Xml.Transform.TransformerException;

package Javax.Xml.Transform.TransformerConfigurationException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Xml.Transform.TransformerException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TransformerConfigurationException (This : Ref := null)
                                                   return Ref;

   function New_TransformerConfigurationException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                                   This : Ref := null)
                                                   return Ref;

   function New_TransformerConfigurationException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                                   This : Ref := null)
                                                   return Ref;

   function New_TransformerConfigurationException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                                   P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                                   This : Ref := null)
                                                   return Ref;

   function New_TransformerConfigurationException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                                   P2_SourceLocator : access Standard.Javax.Xml.Transform.SourceLocator.Typ'Class; 
                                                   This : Ref := null)
                                                   return Ref;

   function New_TransformerConfigurationException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                                   P2_SourceLocator : access Standard.Javax.Xml.Transform.SourceLocator.Typ'Class;
                                                   P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                                   This : Ref := null)
                                                   return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.transform.TransformerConfigurationException");
   pragma Java_Constructor (New_TransformerConfigurationException);

end Javax.Xml.Transform.TransformerConfigurationException;
pragma Import (Java, Javax.Xml.Transform.TransformerConfigurationException, "javax.xml.transform.TransformerConfigurationException");
pragma Extensions_Allowed (Off);
