pragma Extensions_Allowed (On);
limited with Java.Io.PrintStream;
limited with Java.Io.PrintWriter;
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
limited with Javax.Xml.Transform.SourceLocator;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Javax.Xml.Transform.TransformerException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLocator (This : access Typ)
                        return access Javax.Xml.Transform.SourceLocator.Typ'Class;

   procedure SetLocator (This : access Typ;
                         P1_SourceLocator : access Standard.Javax.Xml.Transform.SourceLocator.Typ'Class);

   function GetException (This : access Typ)
                          return access Java.Lang.Throwable.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;

   --  synchronized
   function InitCause (This : access Typ;
                       P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class)
                       return access Java.Lang.Throwable.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TransformerException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_TransformerException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_TransformerException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_TransformerException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_SourceLocator : access Standard.Javax.Xml.Transform.SourceLocator.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_TransformerException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_SourceLocator : access Standard.Javax.Xml.Transform.SourceLocator.Typ'Class;
                                      P3_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function GetMessageAndLocation (This : access Typ)
                                   return access Java.Lang.String.Typ'Class;

   function GetLocationAsString (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;

   procedure PrintStackTrace (This : access Typ);

   procedure PrintStackTrace (This : access Typ;
                              P1_PrintStream : access Standard.Java.Io.PrintStream.Typ'Class);

   procedure PrintStackTrace (This : access Typ;
                              P1_PrintWriter : access Standard.Java.Io.PrintWriter.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.transform.TransformerException");
   pragma Import (Java, GetLocator, "getLocator");
   pragma Import (Java, SetLocator, "setLocator");
   pragma Import (Java, GetException, "getException");
   pragma Import (Java, GetCause, "getCause");
   pragma Import (Java, InitCause, "initCause");
   pragma Java_Constructor (New_TransformerException);
   pragma Import (Java, GetMessageAndLocation, "getMessageAndLocation");
   pragma Import (Java, GetLocationAsString, "getLocationAsString");
   pragma Import (Java, PrintStackTrace, "printStackTrace");

end Javax.Xml.Transform.TransformerException;
pragma Import (Java, Javax.Xml.Transform.TransformerException, "javax.xml.transform.TransformerException");
pragma Extensions_Allowed (Off);
