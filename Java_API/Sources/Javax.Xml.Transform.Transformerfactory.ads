pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Javax.Xml.Transform.ErrorListener;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Transform.Templates;
limited with Javax.Xml.Transform.Transformer;
limited with Javax.Xml.Transform.URIResolver;
with Java.Lang.Object;

package Javax.Xml.Transform.TransformerFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_TransformerFactory (This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Javax.Xml.Transform.TransformerFactory.Typ'Class;
   --  can raise Javax.Xml.Transform.TransformerFactoryConfigurationError.Except

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Javax.Xml.Transform.TransformerFactory.Typ'Class;
   --  can raise Javax.Xml.Transform.TransformerFactoryConfigurationError.Except

   function NewTransformer (This : access Typ;
                            P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                            return access Javax.Xml.Transform.Transformer.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function NewTransformer (This : access Typ)
                            return access Javax.Xml.Transform.Transformer.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function NewTemplates (This : access Typ;
                          P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                          return access Javax.Xml.Transform.Templates.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function GetAssociatedStylesheet (This : access Typ;
                                     P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class;
                                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                                     P3_String : access Standard.Java.Lang.String.Typ'Class;
                                     P4_String : access Standard.Java.Lang.String.Typ'Class)
                                     return access Javax.Xml.Transform.Source.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   procedure SetURIResolver (This : access Typ;
                             P1_URIResolver : access Standard.Javax.Xml.Transform.URIResolver.Typ'Class) is abstract;

   function GetURIResolver (This : access Typ)
                            return access Javax.Xml.Transform.URIResolver.Typ'Class is abstract;

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Javax.Xml.Transform.TransformerConfigurationException.Except

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;

   procedure SetAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;

   procedure SetErrorListener (This : access Typ;
                               P1_ErrorListener : access Standard.Javax.Xml.Transform.ErrorListener.Typ'Class) is abstract;

   function GetErrorListener (This : access Typ)
                              return access Javax.Xml.Transform.ErrorListener.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TransformerFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, NewTransformer, "newTransformer");
   pragma Export (Java, NewTemplates, "newTemplates");
   pragma Export (Java, GetAssociatedStylesheet, "getAssociatedStylesheet");
   pragma Export (Java, SetURIResolver, "setURIResolver");
   pragma Export (Java, GetURIResolver, "getURIResolver");
   pragma Export (Java, SetFeature, "setFeature");
   pragma Export (Java, GetFeature, "getFeature");
   pragma Export (Java, SetAttribute, "setAttribute");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, SetErrorListener, "setErrorListener");
   pragma Export (Java, GetErrorListener, "getErrorListener");

end Javax.Xml.Transform.TransformerFactory;
pragma Import (Java, Javax.Xml.Transform.TransformerFactory, "javax.xml.transform.TransformerFactory");
pragma Extensions_Allowed (Off);
