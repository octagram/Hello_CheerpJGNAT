pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Source;
with Java.Lang.Object;

package Javax.Xml.Transform.URIResolver is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Resolve (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Xml.Transform.Source.Typ'Class is abstract;
   --  can raise Javax.Xml.Transform.TransformerException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Resolve, "resolve");

end Javax.Xml.Transform.URIResolver;
pragma Import (Java, Javax.Xml.Transform.URIResolver, "javax.xml.transform.URIResolver");
pragma Extensions_Allowed (Off);
