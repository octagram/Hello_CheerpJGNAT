pragma Extensions_Allowed (On);
package Javax.Xml.Transform is
   pragma Preelaborate;
end Javax.Xml.Transform;
pragma Import (Java, Javax.Xml.Transform, "javax.xml.transform");
pragma Extensions_Allowed (Off);
