pragma Extensions_Allowed (On);
limited with Javax.Xml.Validation.Validator;
limited with Javax.Xml.Validation.ValidatorHandler;
with Java.Lang.Object;

package Javax.Xml.Validation.Schema is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Schema (This : Ref := null)
                        return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewValidator (This : access Typ)
                          return access Javax.Xml.Validation.Validator.Typ'Class is abstract;

   function NewValidatorHandler (This : access Typ)
                                 return access Javax.Xml.Validation.ValidatorHandler.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Schema);
   pragma Export (Java, NewValidator, "newValidator");
   pragma Export (Java, NewValidatorHandler, "newValidatorHandler");

end Javax.Xml.Validation.Schema;
pragma Import (Java, Javax.Xml.Validation.Schema, "javax.xml.validation.Schema");
pragma Extensions_Allowed (Off);
