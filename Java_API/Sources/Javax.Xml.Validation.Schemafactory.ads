pragma Extensions_Allowed (On);
limited with Java.Io.File;
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Validation.Schema;
limited with Org.W3c.Dom.Ls.LSResourceResolver;
limited with Org.Xml.Sax.ErrorHandler;
with Java.Lang.Object;

package Javax.Xml.Validation.SchemaFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SchemaFactory (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Validation.SchemaFactory.Typ'Class;

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Javax.Xml.Validation.SchemaFactory.Typ'Class;

   function IsSchemaLanguageSupported (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                                       return Java.Boolean is abstract;

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class) is abstract;

   function GetErrorHandler (This : access Typ)
                             return access Org.Xml.Sax.ErrorHandler.Typ'Class is abstract;

   procedure SetResourceResolver (This : access Typ;
                                  P1_LSResourceResolver : access Standard.Org.W3c.Dom.Ls.LSResourceResolver.Typ'Class) is abstract;

   function GetResourceResolver (This : access Typ)
                                 return access Org.W3c.Dom.Ls.LSResourceResolver.Typ'Class is abstract;

   function NewSchema (This : access Typ;
                       P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                       return access Javax.Xml.Validation.Schema.Typ'Class
                       with Import => "newSchema", Convention => Java;
   --  can raise Org.Xml.Sax.SAXException.Except

   function NewSchema (This : access Typ;
                       P1_File : access Standard.Java.Io.File.Typ'Class)
                       return access Javax.Xml.Validation.Schema.Typ'Class
                       with Import => "newSchema", Convention => Java;
   --  can raise Org.Xml.Sax.SAXException.Except

   function NewSchema (This : access Typ;
                       P1_URL : access Standard.Java.Net.URL.Typ'Class)
                       return access Javax.Xml.Validation.Schema.Typ'Class
                       with Import => "newSchema", Convention => Java;
   --  can raise Org.Xml.Sax.SAXException.Except

   function NewSchema (This : access Typ;
                       P1_Source_Arr : access Javax.Xml.Transform.Source.Arr_Obj)
                       return access Javax.Xml.Validation.Schema.Typ'Class is abstract
                       with Export => "newSchema", Convention => Java;
   --  can raise Org.Xml.Sax.SAXException.Except

   function NewSchema (This : access Typ)
                       return access Javax.Xml.Validation.Schema.Typ'Class is abstract
                       with Export => "newSchema", Convention => Java;
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SchemaFactory);
   pragma Import (Java, NewInstance, "newInstance");
   pragma Export (Java, IsSchemaLanguageSupported, "isSchemaLanguageSupported");
   pragma Import (Java, GetFeature, "getFeature");
   pragma Import (Java, SetFeature, "setFeature");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Export (Java, SetErrorHandler, "setErrorHandler");
   pragma Export (Java, GetErrorHandler, "getErrorHandler");
   pragma Export (Java, SetResourceResolver, "setResourceResolver");
   pragma Export (Java, GetResourceResolver, "getResourceResolver");
   -- pragma Import (Java, NewSchema, "newSchema");

end Javax.Xml.Validation.SchemaFactory;
pragma Import (Java, Javax.Xml.Validation.SchemaFactory, "javax.xml.validation.SchemaFactory");
pragma Extensions_Allowed (Off);
