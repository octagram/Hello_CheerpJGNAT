pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Validation.SchemaFactory;
with Java.Lang.Object;

package Javax.Xml.Validation.SchemaFactoryLoader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_SchemaFactoryLoader (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewFactory (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Validation.SchemaFactory.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SchemaFactoryLoader);
   pragma Export (Java, NewFactory, "newFactory");

end Javax.Xml.Validation.SchemaFactoryLoader;
pragma Import (Java, Javax.Xml.Validation.SchemaFactoryLoader, "javax.xml.validation.SchemaFactoryLoader");
pragma Extensions_Allowed (Off);
