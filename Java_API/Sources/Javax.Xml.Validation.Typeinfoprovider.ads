pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.TypeInfo;
with Java.Lang.Object;

package Javax.Xml.Validation.TypeInfoProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_TypeInfoProvider (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetElementTypeInfo (This : access Typ)
                                return access Org.W3c.Dom.TypeInfo.Typ'Class is abstract;

   function GetAttributeTypeInfo (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Org.W3c.Dom.TypeInfo.Typ'Class is abstract;

   function IsIdAttribute (This : access Typ;
                           P1_Int : Java.Int)
                           return Java.Boolean is abstract;

   function IsSpecified (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TypeInfoProvider);
   pragma Export (Java, GetElementTypeInfo, "getElementTypeInfo");
   pragma Export (Java, GetAttributeTypeInfo, "getAttributeTypeInfo");
   pragma Export (Java, IsIdAttribute, "isIdAttribute");
   pragma Export (Java, IsSpecified, "isSpecified");

end Javax.Xml.Validation.TypeInfoProvider;
pragma Import (Java, Javax.Xml.Validation.TypeInfoProvider, "javax.xml.validation.TypeInfoProvider");
pragma Extensions_Allowed (Off);
