pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Source;
limited with Org.W3c.Dom.Ls.LSResourceResolver;
limited with Org.Xml.Sax.ErrorHandler;
with Java.Lang.Object;

package Javax.Xml.Validation.Validator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Validator (This : Ref := null)
                           return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ) is abstract;

   procedure Validate (This : access Typ;
                       P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class) with Import => "validate", Convention => Java;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Validate (This : access Typ;
                       P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class;
                       P2_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class) is abstract with Export => "validate", Convention => Java;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class) is abstract;

   function GetErrorHandler (This : access Typ)
                             return access Org.Xml.Sax.ErrorHandler.Typ'Class is abstract;

   procedure SetResourceResolver (This : access Typ;
                                  P1_LSResourceResolver : access Standard.Org.W3c.Dom.Ls.LSResourceResolver.Typ'Class) is abstract;

   function GetResourceResolver (This : access Typ)
                                 return access Org.W3c.Dom.Ls.LSResourceResolver.Typ'Class is abstract;

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Validator);
   pragma Export (Java, Reset, "reset");
   -- pragma Import (Java, Validate, "validate");
   pragma Export (Java, SetErrorHandler, "setErrorHandler");
   pragma Export (Java, GetErrorHandler, "getErrorHandler");
   pragma Export (Java, SetResourceResolver, "setResourceResolver");
   pragma Export (Java, GetResourceResolver, "getResourceResolver");
   pragma Import (Java, GetFeature, "getFeature");
   pragma Import (Java, SetFeature, "setFeature");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetProperty, "getProperty");

end Javax.Xml.Validation.Validator;
pragma Import (Java, Javax.Xml.Validation.Validator, "javax.xml.validation.Validator");
pragma Extensions_Allowed (Off);
