pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Validation.TypeInfoProvider;
limited with Org.W3c.Dom.Ls.LSResourceResolver;
limited with Org.Xml.Sax.ErrorHandler;
with Java.Lang.Object;
with Org.Xml.Sax.ContentHandler;

package Javax.Xml.Validation.ValidatorHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ContentHandler_I : Org.Xml.Sax.ContentHandler.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_ValidatorHandler (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetContentHandler (This : access Typ;
                                P1_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class) is abstract;

   function GetContentHandler (This : access Typ)
                               return access Org.Xml.Sax.ContentHandler.Typ'Class is abstract;

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class) is abstract;

   function GetErrorHandler (This : access Typ)
                             return access Org.Xml.Sax.ErrorHandler.Typ'Class is abstract;

   procedure SetResourceResolver (This : access Typ;
                                  P1_LSResourceResolver : access Standard.Org.W3c.Dom.Ls.LSResourceResolver.Typ'Class) is abstract;

   function GetResourceResolver (This : access Typ)
                                 return access Org.W3c.Dom.Ls.LSResourceResolver.Typ'Class is abstract;

   function GetTypeInfoProvider (This : access Typ)
                                 return access Javax.Xml.Validation.TypeInfoProvider.Typ'Class is abstract;

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ValidatorHandler);
   pragma Export (Java, SetContentHandler, "setContentHandler");
   pragma Export (Java, GetContentHandler, "getContentHandler");
   pragma Export (Java, SetErrorHandler, "setErrorHandler");
   pragma Export (Java, GetErrorHandler, "getErrorHandler");
   pragma Export (Java, SetResourceResolver, "setResourceResolver");
   pragma Export (Java, GetResourceResolver, "getResourceResolver");
   pragma Export (Java, GetTypeInfoProvider, "getTypeInfoProvider");
   pragma Export (Java, GetFeature, "getFeature");
   pragma Export (Java, SetFeature, "setFeature");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetProperty, "getProperty");

end Javax.Xml.Validation.ValidatorHandler;
pragma Import (Java, Javax.Xml.Validation.ValidatorHandler, "javax.xml.validation.ValidatorHandler");
pragma Extensions_Allowed (Off);
