pragma Extensions_Allowed (On);
package Javax.Xml.Validation is
   pragma Preelaborate;
end Javax.Xml.Validation;
pragma Import (Java, Javax.Xml.Validation, "javax.xml.validation");
pragma Extensions_Allowed (Off);
