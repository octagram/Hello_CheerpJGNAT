pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Ws.FaultAction;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Xml.Ws.Action is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Input (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   function Output (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   function Fault (This : access Typ)
                   return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Input, "input");
   pragma Export (Java, Output, "output");
   pragma Export (Java, Fault, "fault");

end Javax.Xml.Ws.Action;
pragma Import (Java, Javax.Xml.Ws.Action, "javax.xml.ws.Action");
pragma Extensions_Allowed (Off);
