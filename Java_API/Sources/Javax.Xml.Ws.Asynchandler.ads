pragma Extensions_Allowed (On);
limited with Javax.Xml.Ws.Response;
with Java.Lang.Object;

package Javax.Xml.Ws.AsyncHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure HandleResponse (This : access Typ;
                             P1_Response : access Standard.Javax.Xml.Ws.Response.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, HandleResponse, "handleResponse");

end Javax.Xml.Ws.AsyncHandler;
pragma Import (Java, Javax.Xml.Ws.AsyncHandler, "javax.xml.ws.AsyncHandler");
pragma Extensions_Allowed (Off);
