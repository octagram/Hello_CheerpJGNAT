pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Map;
limited with Javax.Xml.Ws.Binding;
limited with Javax.Xml.Ws.EndpointReference;
with Java.Lang.Object;

package Javax.Xml.Ws.BindingProvider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRequestContext (This : access Typ)
                               return access Java.Util.Map.Typ'Class is abstract;

   function GetResponseContext (This : access Typ)
                                return access Java.Util.Map.Typ'Class is abstract;

   function GetBinding (This : access Typ)
                        return access Javax.Xml.Ws.Binding.Typ'Class is abstract;

   function GetEndpointReference (This : access Typ)
                                  return access Javax.Xml.Ws.EndpointReference.Typ'Class is abstract;

   function GetEndpointReference (This : access Typ;
                                  P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                  return access Javax.Xml.Ws.EndpointReference.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   USERNAME_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   PASSWORD_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   ENDPOINT_ADDRESS_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SESSION_MAINTAIN_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAPACTION_USE_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAPACTION_URI_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRequestContext, "getRequestContext");
   pragma Export (Java, GetResponseContext, "getResponseContext");
   pragma Export (Java, GetBinding, "getBinding");
   pragma Export (Java, GetEndpointReference, "getEndpointReference");
   pragma Import (Java, USERNAME_PROPERTY, "USERNAME_PROPERTY");
   pragma Import (Java, PASSWORD_PROPERTY, "PASSWORD_PROPERTY");
   pragma Import (Java, ENDPOINT_ADDRESS_PROPERTY, "ENDPOINT_ADDRESS_PROPERTY");
   pragma Import (Java, SESSION_MAINTAIN_PROPERTY, "SESSION_MAINTAIN_PROPERTY");
   pragma Import (Java, SOAPACTION_USE_PROPERTY, "SOAPACTION_USE_PROPERTY");
   pragma Import (Java, SOAPACTION_URI_PROPERTY, "SOAPACTION_URI_PROPERTY");

end Javax.Xml.Ws.BindingProvider;
pragma Import (Java, Javax.Xml.Ws.BindingProvider, "javax.xml.ws.BindingProvider");
pragma Extensions_Allowed (Off);
