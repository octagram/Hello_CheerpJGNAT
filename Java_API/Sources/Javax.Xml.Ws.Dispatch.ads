pragma Extensions_Allowed (On);
limited with Java.Util.Concurrent.Future;
limited with Javax.Xml.Ws.AsyncHandler;
limited with Javax.Xml.Ws.Response;
with Java.Lang.Object;
with Javax.Xml.Ws.BindingProvider;

package Javax.Xml.Ws.Dispatch is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            BindingProvider_I : Javax.Xml.Ws.BindingProvider.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Invoke (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class is abstract;

   function InvokeAsync (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return access Javax.Xml.Ws.Response.Typ'Class is abstract;

   function InvokeAsync (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_AsyncHandler : access Standard.Javax.Xml.Ws.AsyncHandler.Typ'Class)
                         return access Java.Util.Concurrent.Future.Typ'Class is abstract;

   procedure InvokeOneWay (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Invoke, "invoke");
   pragma Export (Java, InvokeAsync, "invokeAsync");
   pragma Export (Java, InvokeOneWay, "invokeOneWay");

end Javax.Xml.Ws.Dispatch;
pragma Import (Java, Javax.Xml.Ws.Dispatch, "javax.xml.ws.Dispatch");
pragma Extensions_Allowed (Off);
