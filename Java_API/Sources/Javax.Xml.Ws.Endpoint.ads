pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Util.Concurrent.Executor;
limited with Java.Util.List;
limited with Java.Util.Map;
limited with Javax.Xml.Ws.Binding;
limited with Javax.Xml.Ws.EndpointReference;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;

package Javax.Xml.Ws.Endpoint is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Endpoint (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create (P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Javax.Xml.Ws.Endpoint.Typ'Class;

   function Create (P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Javax.Xml.Ws.Endpoint.Typ'Class;

   function GetBinding (This : access Typ)
                        return access Javax.Xml.Ws.Binding.Typ'Class is abstract;

   function GetImplementor (This : access Typ)
                            return access Java.Lang.Object.Typ'Class is abstract;

   procedure Publish (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function Publish (P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                     return access Javax.Xml.Ws.Endpoint.Typ'Class;

   procedure Publish (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;

   procedure Stop (This : access Typ) is abstract;

   function IsPublished (This : access Typ)
                         return Java.Boolean is abstract;

   function GetMetadata (This : access Typ)
                         return access Java.Util.List.Typ'Class is abstract;

   procedure SetMetadata (This : access Typ;
                          P1_List : access Standard.Java.Util.List.Typ'Class) is abstract;

   function GetExecutor (This : access Typ)
                         return access Java.Util.Concurrent.Executor.Typ'Class is abstract;

   procedure SetExecutor (This : access Typ;
                          P1_Executor : access Standard.Java.Util.Concurrent.Executor.Typ'Class) is abstract;

   function GetProperties (This : access Typ)
                           return access Java.Util.Map.Typ'Class is abstract;

   procedure SetProperties (This : access Typ;
                            P1_Map : access Standard.Java.Util.Map.Typ'Class) is abstract;

   function GetEndpointReference (This : access Typ;
                                  P1_Element_Arr : access Org.W3c.Dom.Element.Arr_Obj)
                                  return access Javax.Xml.Ws.EndpointReference.Typ'Class is abstract;

   function GetEndpointReference (This : access Typ;
                                  P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                  P2_Element_Arr : access Org.W3c.Dom.Element.Arr_Obj)
                                  return access Javax.Xml.Ws.EndpointReference.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   WSDL_SERVICE : constant access Java.Lang.String.Typ'Class;

   --  final
   WSDL_PORT : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Endpoint);
   pragma Export (Java, Create, "create");
   pragma Export (Java, GetBinding, "getBinding");
   pragma Export (Java, GetImplementor, "getImplementor");
   pragma Export (Java, Publish, "publish");
   pragma Export (Java, Stop, "stop");
   pragma Export (Java, IsPublished, "isPublished");
   pragma Export (Java, GetMetadata, "getMetadata");
   pragma Export (Java, SetMetadata, "setMetadata");
   pragma Export (Java, GetExecutor, "getExecutor");
   pragma Export (Java, SetExecutor, "setExecutor");
   pragma Export (Java, GetProperties, "getProperties");
   pragma Export (Java, SetProperties, "setProperties");
   pragma Export (Java, GetEndpointReference, "getEndpointReference");
   pragma Import (Java, WSDL_SERVICE, "WSDL_SERVICE");
   pragma Import (Java, WSDL_PORT, "WSDL_PORT");

end Javax.Xml.Ws.Endpoint;
pragma Import (Java, Javax.Xml.Ws.Endpoint, "javax.xml.ws.Endpoint");
pragma Extensions_Allowed (Off);
