pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Ws.WebServiceFeature;
with Java.Lang.Object;

package Javax.Xml.Ws.EndpointReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_EndpointReference (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ReadFrom (P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                      return access Javax.Xml.Ws.EndpointReference.Typ'Class;

   procedure WriteTo (This : access Typ;
                      P1_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class) is abstract;

   function GetPort (This : access Typ;
                     P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P2_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_EndpointReference);
   pragma Export (Java, ReadFrom, "readFrom");
   pragma Export (Java, WriteTo, "writeTo");
   pragma Export (Java, GetPort, "getPort");
   pragma Export (Java, ToString, "toString");

end Javax.Xml.Ws.EndpointReference;
pragma Import (Java, Javax.Xml.Ws.EndpointReference, "javax.xml.ws.EndpointReference");
pragma Extensions_Allowed (Off);
