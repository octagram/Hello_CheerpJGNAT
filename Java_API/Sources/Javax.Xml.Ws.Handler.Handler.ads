pragma Extensions_Allowed (On);
limited with Javax.Xml.Ws.Handler.MessageContext;
with Java.Lang.Object;

package Javax.Xml.Ws.Handler.Handler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function HandleMessage (This : access Typ;
                           P1_MessageContext : access Standard.Javax.Xml.Ws.Handler.MessageContext.Typ'Class)
                           return Java.Boolean is abstract;

   function HandleFault (This : access Typ;
                         P1_MessageContext : access Standard.Javax.Xml.Ws.Handler.MessageContext.Typ'Class)
                         return Java.Boolean is abstract;

   procedure Close (This : access Typ;
                    P1_MessageContext : access Standard.Javax.Xml.Ws.Handler.MessageContext.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, HandleMessage, "handleMessage");
   pragma Export (Java, HandleFault, "handleFault");
   pragma Export (Java, Close, "close");

end Javax.Xml.Ws.Handler.Handler;
pragma Import (Java, Javax.Xml.Ws.Handler.Handler, "javax.xml.ws.handler.Handler");
pragma Extensions_Allowed (Off);
