pragma Extensions_Allowed (On);
limited with Javax.Xml.Ws.LogicalMessage;
with Java.Lang.Object;
with Javax.Xml.Ws.Handler.MessageContext;

package Javax.Xml.Ws.Handler.LogicalMessageContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MessageContext_I : Javax.Xml.Ws.Handler.MessageContext.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Javax.Xml.Ws.LogicalMessage.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMessage, "getMessage");

end Javax.Xml.Ws.Handler.LogicalMessageContext;
pragma Import (Java, Javax.Xml.Ws.Handler.LogicalMessageContext, "javax.xml.ws.handler.LogicalMessageContext");
pragma Extensions_Allowed (Off);
