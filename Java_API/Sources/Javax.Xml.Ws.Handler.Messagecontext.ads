pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Ws.Handler.MessageContext.Scope;
with Java.Lang.Object;
with Java.Util.Map;

package Javax.Xml.Ws.Handler.MessageContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Map_I : Java.Util.Map.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetScope (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Scope : access Standard.Javax.Xml.Ws.Handler.MessageContext.Scope.Typ'Class) is abstract;

   function GetScope (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Javax.Xml.Ws.Handler.MessageContext.Scope.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MESSAGE_OUTBOUND_PROPERTY : constant access Java.Lang.String.Typ'Class;

   --  final
   INBOUND_MESSAGE_ATTACHMENTS : constant access Java.Lang.String.Typ'Class;

   --  final
   OUTBOUND_MESSAGE_ATTACHMENTS : constant access Java.Lang.String.Typ'Class;

   --  final
   WSDL_DESCRIPTION : constant access Java.Lang.String.Typ'Class;

   --  final
   WSDL_SERVICE : constant access Java.Lang.String.Typ'Class;

   --  final
   WSDL_PORT : constant access Java.Lang.String.Typ'Class;

   --  final
   WSDL_INTERFACE : constant access Java.Lang.String.Typ'Class;

   --  final
   WSDL_OPERATION : constant access Java.Lang.String.Typ'Class;

   --  final
   HTTP_RESPONSE_CODE : constant access Java.Lang.String.Typ'Class;

   --  final
   HTTP_REQUEST_HEADERS : constant access Java.Lang.String.Typ'Class;

   --  final
   HTTP_RESPONSE_HEADERS : constant access Java.Lang.String.Typ'Class;

   --  final
   HTTP_REQUEST_METHOD : constant access Java.Lang.String.Typ'Class;

   --  final
   SERVLET_REQUEST : constant access Java.Lang.String.Typ'Class;

   --  final
   SERVLET_RESPONSE : constant access Java.Lang.String.Typ'Class;

   --  final
   SERVLET_CONTEXT : constant access Java.Lang.String.Typ'Class;

   --  final
   QUERY_STRING : constant access Java.Lang.String.Typ'Class;

   --  final
   PATH_INFO : constant access Java.Lang.String.Typ'Class;

   --  final
   REFERENCE_PARAMETERS : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetScope, "setScope");
   pragma Export (Java, GetScope, "getScope");
   pragma Import (Java, MESSAGE_OUTBOUND_PROPERTY, "MESSAGE_OUTBOUND_PROPERTY");
   pragma Import (Java, INBOUND_MESSAGE_ATTACHMENTS, "INBOUND_MESSAGE_ATTACHMENTS");
   pragma Import (Java, OUTBOUND_MESSAGE_ATTACHMENTS, "OUTBOUND_MESSAGE_ATTACHMENTS");
   pragma Import (Java, WSDL_DESCRIPTION, "WSDL_DESCRIPTION");
   pragma Import (Java, WSDL_SERVICE, "WSDL_SERVICE");
   pragma Import (Java, WSDL_PORT, "WSDL_PORT");
   pragma Import (Java, WSDL_INTERFACE, "WSDL_INTERFACE");
   pragma Import (Java, WSDL_OPERATION, "WSDL_OPERATION");
   pragma Import (Java, HTTP_RESPONSE_CODE, "HTTP_RESPONSE_CODE");
   pragma Import (Java, HTTP_REQUEST_HEADERS, "HTTP_REQUEST_HEADERS");
   pragma Import (Java, HTTP_RESPONSE_HEADERS, "HTTP_RESPONSE_HEADERS");
   pragma Import (Java, HTTP_REQUEST_METHOD, "HTTP_REQUEST_METHOD");
   pragma Import (Java, SERVLET_REQUEST, "SERVLET_REQUEST");
   pragma Import (Java, SERVLET_RESPONSE, "SERVLET_RESPONSE");
   pragma Import (Java, SERVLET_CONTEXT, "SERVLET_CONTEXT");
   pragma Import (Java, QUERY_STRING, "QUERY_STRING");
   pragma Import (Java, PATH_INFO, "PATH_INFO");
   pragma Import (Java, REFERENCE_PARAMETERS, "REFERENCE_PARAMETERS");

end Javax.Xml.Ws.Handler.MessageContext;
pragma Import (Java, Javax.Xml.Ws.Handler.MessageContext, "javax.xml.ws.handler.MessageContext");
pragma Extensions_Allowed (Off);
