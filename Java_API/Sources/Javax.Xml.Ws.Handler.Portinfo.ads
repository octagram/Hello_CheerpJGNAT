pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Ws.Handler.PortInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetServiceName (This : access Typ)
                            return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetPortName (This : access Typ)
                         return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetBindingID (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetServiceName, "getServiceName");
   pragma Export (Java, GetPortName, "getPortName");
   pragma Export (Java, GetBindingID, "getBindingID");

end Javax.Xml.Ws.Handler.PortInfo;
pragma Import (Java, Javax.Xml.Ws.Handler.PortInfo, "javax.xml.ws.handler.PortInfo");
pragma Extensions_Allowed (Off);
