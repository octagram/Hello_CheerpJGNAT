pragma Extensions_Allowed (On);
limited with Java.Util.Set;
with Java.Lang.Object;
with Javax.Xml.Ws.Handler.Handler;

package Javax.Xml.Ws.Handler.Soap.SOAPHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Handler_I : Javax.Xml.Ws.Handler.Handler.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHeaders (This : access Typ)
                        return access Java.Util.Set.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetHeaders, "getHeaders");

end Javax.Xml.Ws.Handler.Soap.SOAPHandler;
pragma Import (Java, Javax.Xml.Ws.Handler.Soap.SOAPHandler, "javax.xml.ws.handler.soap.SOAPHandler");
pragma Extensions_Allowed (Off);
