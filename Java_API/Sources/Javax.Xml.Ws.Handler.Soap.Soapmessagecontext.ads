pragma Extensions_Allowed (On);
limited with Java.Util.Set;
limited with Javax.Xml.Bind.JAXBContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Soap.SOAPMessage;
with Java.Lang.Object;
with Javax.Xml.Ws.Handler.MessageContext;

package Javax.Xml.Ws.Handler.Soap.SOAPMessageContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            MessageContext_I : Javax.Xml.Ws.Handler.MessageContext.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Javax.Xml.Soap.SOAPMessage.Typ'Class is abstract;

   procedure SetMessage (This : access Typ;
                         P1_SOAPMessage : access Standard.Javax.Xml.Soap.SOAPMessage.Typ'Class) is abstract;

   function GetHeaders (This : access Typ;
                        P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                        P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                        P3_Boolean : Java.Boolean)
                        return Standard.Java.Lang.Object.Ref is abstract;

   function GetRoles (This : access Typ)
                      return access Java.Util.Set.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMessage, "getMessage");
   pragma Export (Java, SetMessage, "setMessage");
   pragma Export (Java, GetHeaders, "getHeaders");
   pragma Export (Java, GetRoles, "getRoles");

end Javax.Xml.Ws.Handler.Soap.SOAPMessageContext;
pragma Import (Java, Javax.Xml.Ws.Handler.Soap.SOAPMessageContext, "javax.xml.ws.handler.soap.SOAPMessageContext");
pragma Extensions_Allowed (Off);
