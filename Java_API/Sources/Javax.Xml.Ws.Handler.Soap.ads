pragma Extensions_Allowed (On);
package Javax.Xml.Ws.Handler.Soap is
   pragma Preelaborate;
end Javax.Xml.Ws.Handler.Soap;
pragma Import (Java, Javax.Xml.Ws.Handler.Soap, "javax.xml.ws.handler.soap");
pragma Extensions_Allowed (Off);
