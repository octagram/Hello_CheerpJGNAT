pragma Extensions_Allowed (On);
package Javax.Xml.Ws.Handler is
   pragma Preelaborate;
end Javax.Xml.Ws.Handler;
pragma Import (Java, Javax.Xml.Ws.Handler, "javax.xml.ws.handler");
pragma Extensions_Allowed (Off);
