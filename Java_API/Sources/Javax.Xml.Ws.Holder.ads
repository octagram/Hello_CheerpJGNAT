pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Javax.Xml.Ws.Holder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Value : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Value, "value");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Holder (This : Ref := null)
                        return Ref;

   function New_Holder (P1_Object : access Standard.Java.Lang.Object.Typ'Class; 
                        This : Ref := null)
                        return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Holder);

end Javax.Xml.Ws.Holder;
pragma Import (Java, Javax.Xml.Ws.Holder, "javax.xml.ws.Holder");
pragma Extensions_Allowed (Off);
