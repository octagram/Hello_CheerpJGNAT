pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Ws.Binding;

package Javax.Xml.Ws.Http.HTTPBinding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Binding_I : Javax.Xml.Ws.Binding.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   HTTP_BINDING : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, HTTP_BINDING, "HTTP_BINDING");

end Javax.Xml.Ws.Http.HTTPBinding;
pragma Import (Java, Javax.Xml.Ws.Http.HTTPBinding, "javax.xml.ws.http.HTTPBinding");
pragma Extensions_Allowed (Off);
