pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Xml.Ws.ProtocolException;

package Javax.Xml.Ws.Http.HTTPException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Xml.Ws.ProtocolException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HTTPException (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStatusCode (This : access Typ)
                           return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.ws.http.HTTPException");
   pragma Java_Constructor (New_HTTPException);
   pragma Import (Java, GetStatusCode, "getStatusCode");

end Javax.Xml.Ws.Http.HTTPException;
pragma Import (Java, Javax.Xml.Ws.Http.HTTPException, "javax.xml.ws.http.HTTPException");
pragma Extensions_Allowed (Off);
