pragma Extensions_Allowed (On);
package Javax.Xml.Ws.Http is
   pragma Preelaborate;
end Javax.Xml.Ws.Http;
pragma Import (Java, Javax.Xml.Ws.Http, "javax.xml.ws.http");
pragma Extensions_Allowed (Off);
