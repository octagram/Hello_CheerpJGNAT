pragma Extensions_Allowed (On);
limited with Javax.Xml.Bind.JAXBContext;
limited with Javax.Xml.Transform.Source;
with Java.Lang.Object;

package Javax.Xml.Ws.LogicalMessage is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPayload (This : access Typ)
                        return access Javax.Xml.Transform.Source.Typ'Class is abstract;

   procedure SetPayload (This : access Typ;
                         P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class) is abstract;

   function GetPayload (This : access Typ;
                        P1_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   procedure SetPayload (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPayload, "getPayload");
   pragma Export (Java, SetPayload, "setPayload");

end Javax.Xml.Ws.LogicalMessage;
pragma Import (Java, Javax.Xml.Ws.LogicalMessage, "javax.xml.ws.LogicalMessage");
pragma Extensions_Allowed (Off);
