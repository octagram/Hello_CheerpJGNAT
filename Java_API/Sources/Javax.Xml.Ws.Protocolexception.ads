pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Xml.Ws.WebServiceException;

package Javax.Xml.Ws.ProtocolException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Xml.Ws.WebServiceException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ProtocolException (This : Ref := null)
                                   return Ref;

   function New_ProtocolException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_ProtocolException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_ProtocolException (P1_Throwable : access Standard.Java.Lang.Throwable.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.ws.ProtocolException");
   pragma Java_Constructor (New_ProtocolException);

end Javax.Xml.Ws.ProtocolException;
pragma Import (Java, Javax.Xml.Ws.ProtocolException, "javax.xml.ws.ProtocolException");
pragma Extensions_Allowed (Off);
