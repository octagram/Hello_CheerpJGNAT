pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Concurrent.Executor;
limited with Java.Util.Iterator;
limited with Javax.Xml.Bind.JAXBContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Ws.Dispatch;
limited with Javax.Xml.Ws.EndpointReference;
limited with Javax.Xml.Ws.Handler.HandlerResolver;
limited with Javax.Xml.Ws.Service.Mode;
limited with Javax.Xml.Ws.WebServiceFeature;
with Java.Lang.Object;

package Javax.Xml.Ws.Service is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_Service (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                         P2_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPort (This : access Typ;
                     P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                     P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;

   function GetPort (This : access Typ;
                     P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                     P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P3_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class;

   function GetPort (This : access Typ;
                     P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                     return access Java.Lang.Object.Typ'Class;

   function GetPort (This : access Typ;
                     P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P2_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class;

   function GetPort (This : access Typ;
                     P1_EndpointReference : access Standard.Javax.Xml.Ws.EndpointReference.Typ'Class;
                     P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P3_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class;

   procedure AddPort (This : access Typ;
                      P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_String : access Standard.Java.Lang.String.Typ'Class);

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class;

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class;

   function CreateDispatch (This : access Typ;
                            P1_EndpointReference : access Standard.Javax.Xml.Ws.EndpointReference.Typ'Class;
                            P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class;

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class;

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class;

   function CreateDispatch (This : access Typ;
                            P1_EndpointReference : access Standard.Javax.Xml.Ws.EndpointReference.Typ'Class;
                            P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class;

   function GetServiceName (This : access Typ)
                            return access Javax.Xml.Namespace.QName.Typ'Class;

   function GetPorts (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function GetWSDLDocumentLocation (This : access Typ)
                                     return access Java.Net.URL.Typ'Class;

   function GetHandlerResolver (This : access Typ)
                                return access Javax.Xml.Ws.Handler.HandlerResolver.Typ'Class;

   procedure SetHandlerResolver (This : access Typ;
                                 P1_HandlerResolver : access Standard.Javax.Xml.Ws.Handler.HandlerResolver.Typ'Class);

   function GetExecutor (This : access Typ)
                         return access Java.Util.Concurrent.Executor.Typ'Class;

   procedure SetExecutor (This : access Typ;
                          P1_Executor : access Standard.Java.Util.Concurrent.Executor.Typ'Class);

   function Create (P1_URL : access Standard.Java.Net.URL.Typ'Class;
                    P2_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                    return access Javax.Xml.Ws.Service.Typ'Class;

   function Create (P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                    return access Javax.Xml.Ws.Service.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Service);
   pragma Import (Java, GetPort, "getPort");
   pragma Import (Java, AddPort, "addPort");
   pragma Import (Java, CreateDispatch, "createDispatch");
   pragma Import (Java, GetServiceName, "getServiceName");
   pragma Import (Java, GetPorts, "getPorts");
   pragma Import (Java, GetWSDLDocumentLocation, "getWSDLDocumentLocation");
   pragma Import (Java, GetHandlerResolver, "getHandlerResolver");
   pragma Import (Java, SetHandlerResolver, "setHandlerResolver");
   pragma Import (Java, GetExecutor, "getExecutor");
   pragma Import (Java, SetExecutor, "setExecutor");
   pragma Import (Java, Create, "create");

end Javax.Xml.Ws.Service;
pragma Import (Java, Javax.Xml.Ws.Service, "javax.xml.ws.Service");
pragma Extensions_Allowed (Off);
