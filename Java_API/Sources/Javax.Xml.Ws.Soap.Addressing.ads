pragma Extensions_Allowed (On);
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Xml.Ws.Soap.Addressing is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Enabled (This : access Typ)
                     return Java.Boolean is abstract;

   function Required (This : access Typ)
                      return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Enabled, "enabled");
   pragma Export (Java, Required, "required");

end Javax.Xml.Ws.Soap.Addressing;
pragma Import (Java, Javax.Xml.Ws.Soap.Addressing, "javax.xml.ws.soap.Addressing");
pragma Extensions_Allowed (Off);
