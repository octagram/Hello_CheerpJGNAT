pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Ws.WebServiceFeature;

package Javax.Xml.Ws.Soap.AddressingFeature is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Xml.Ws.WebServiceFeature.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Required : Java.Boolean;
      pragma Import (Java, Required, "required");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AddressingFeature (This : Ref := null)
                                   return Ref;

   function New_AddressingFeature (P1_Boolean : Java.Boolean; 
                                   This : Ref := null)
                                   return Ref;

   function New_AddressingFeature (P1_Boolean : Java.Boolean;
                                   P2_Boolean : Java.Boolean; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function IsRequired (This : access Typ)
                        return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AddressingFeature);
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, IsRequired, "isRequired");
   pragma Import (Java, ID, "ID");

end Javax.Xml.Ws.Soap.AddressingFeature;
pragma Import (Java, Javax.Xml.Ws.Soap.AddressingFeature, "javax.xml.ws.soap.AddressingFeature");
pragma Extensions_Allowed (Off);
