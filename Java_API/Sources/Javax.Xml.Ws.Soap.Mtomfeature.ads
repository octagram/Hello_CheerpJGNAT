pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Javax.Xml.Ws.WebServiceFeature;

package Javax.Xml.Ws.Soap.MTOMFeature is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Xml.Ws.WebServiceFeature.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Threshold : Java.Int;
      pragma Import (Java, Threshold, "threshold");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MTOMFeature (This : Ref := null)
                             return Ref;

   function New_MTOMFeature (P1_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   function New_MTOMFeature (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   function New_MTOMFeature (P1_Boolean : Java.Boolean;
                             P2_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetThreshold (This : access Typ)
                          return Java.Int;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ID : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MTOMFeature);
   pragma Import (Java, GetID, "getID");
   pragma Import (Java, GetThreshold, "getThreshold");
   pragma Import (Java, ID, "ID");

end Javax.Xml.Ws.Soap.MTOMFeature;
pragma Import (Java, Javax.Xml.Ws.Soap.MTOMFeature, "javax.xml.ws.soap.MTOMFeature");
pragma Extensions_Allowed (Off);
