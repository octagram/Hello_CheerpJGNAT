pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Set;
limited with Javax.Xml.Soap.MessageFactory;
limited with Javax.Xml.Soap.SOAPFactory;
with Java.Lang.Object;
with Javax.Xml.Ws.Binding;

package Javax.Xml.Ws.Soap.SOAPBinding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Binding_I : Javax.Xml.Ws.Binding.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRoles (This : access Typ)
                      return access Java.Util.Set.Typ'Class is abstract;

   procedure SetRoles (This : access Typ;
                       P1_Set : access Standard.Java.Util.Set.Typ'Class) is abstract;

   function IsMTOMEnabled (This : access Typ)
                           return Java.Boolean is abstract;

   procedure SetMTOMEnabled (This : access Typ;
                             P1_Boolean : Java.Boolean) is abstract;

   function GetSOAPFactory (This : access Typ)
                            return access Javax.Xml.Soap.SOAPFactory.Typ'Class is abstract;

   function GetMessageFactory (This : access Typ)
                               return access Javax.Xml.Soap.MessageFactory.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SOAP11HTTP_BINDING : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP12HTTP_BINDING : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP11HTTP_MTOM_BINDING : constant access Java.Lang.String.Typ'Class;

   --  final
   SOAP12HTTP_MTOM_BINDING : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRoles, "getRoles");
   pragma Export (Java, SetRoles, "setRoles");
   pragma Export (Java, IsMTOMEnabled, "isMTOMEnabled");
   pragma Export (Java, SetMTOMEnabled, "setMTOMEnabled");
   pragma Export (Java, GetSOAPFactory, "getSOAPFactory");
   pragma Export (Java, GetMessageFactory, "getMessageFactory");
   pragma Import (Java, SOAP11HTTP_BINDING, "SOAP11HTTP_BINDING");
   pragma Import (Java, SOAP12HTTP_BINDING, "SOAP12HTTP_BINDING");
   pragma Import (Java, SOAP11HTTP_MTOM_BINDING, "SOAP11HTTP_MTOM_BINDING");
   pragma Import (Java, SOAP12HTTP_MTOM_BINDING, "SOAP12HTTP_MTOM_BINDING");

end Javax.Xml.Ws.Soap.SOAPBinding;
pragma Import (Java, Javax.Xml.Ws.Soap.SOAPBinding, "javax.xml.ws.soap.SOAPBinding");
pragma Extensions_Allowed (Off);
