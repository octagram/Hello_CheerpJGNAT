pragma Extensions_Allowed (On);
limited with Javax.Xml.Soap.SOAPFault;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Xml.Ws.ProtocolException;

package Javax.Xml.Ws.Soap.SOAPFaultException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Javax.Xml.Ws.ProtocolException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SOAPFaultException (P1_SOAPFault : access Standard.Javax.Xml.Soap.SOAPFault.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFault (This : access Typ)
                      return access Javax.Xml.Soap.SOAPFault.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "javax.xml.ws.soap.SOAPFaultException");
   pragma Java_Constructor (New_SOAPFaultException);
   pragma Import (Java, GetFault, "getFault");

end Javax.Xml.Ws.Soap.SOAPFaultException;
pragma Import (Java, Javax.Xml.Ws.Soap.SOAPFaultException, "javax.xml.ws.soap.SOAPFaultException");
pragma Extensions_Allowed (Off);
