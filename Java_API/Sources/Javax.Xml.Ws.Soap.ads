pragma Extensions_Allowed (On);
package Javax.Xml.Ws.Soap is
   pragma Preelaborate;
end Javax.Xml.Ws.Soap;
pragma Import (Java, Javax.Xml.Ws.Soap, "javax.xml.ws.soap");
pragma Extensions_Allowed (Off);
