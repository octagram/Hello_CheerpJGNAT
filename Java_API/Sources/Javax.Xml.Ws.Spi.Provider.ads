pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.List;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Transform.Source;
limited with Javax.Xml.Ws.Endpoint;
limited with Javax.Xml.Ws.EndpointReference;
limited with Javax.Xml.Ws.Spi.ServiceDelegate;
limited with Javax.Xml.Ws.WebServiceFeature;
limited with Javax.Xml.Ws.Wsaddressing.W3CEndpointReference;
with Java.Lang.Object;

package Javax.Xml.Ws.Spi.Provider is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_Provider (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Provider return access Javax.Xml.Ws.Spi.Provider.Typ'Class;

   function CreateServiceDelegate (This : access Typ;
                                   P1_URL : access Standard.Java.Net.URL.Typ'Class;
                                   P2_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                                   P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                                   return access Javax.Xml.Ws.Spi.ServiceDelegate.Typ'Class is abstract;

   function CreateEndpoint (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                            return access Javax.Xml.Ws.Endpoint.Typ'Class is abstract;

   function CreateAndPublishEndpoint (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                                      return access Javax.Xml.Ws.Endpoint.Typ'Class is abstract;

   function ReadEndpointReference (This : access Typ;
                                   P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class)
                                   return access Javax.Xml.Ws.EndpointReference.Typ'Class is abstract;

   function GetPort (This : access Typ;
                     P1_EndpointReference : access Standard.Javax.Xml.Ws.EndpointReference.Typ'Class;
                     P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P3_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function CreateW3CEndpointReference (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                                        P2_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                                        P3_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                                        P4_List : access Standard.Java.Util.List.Typ'Class;
                                        P5_String : access Standard.Java.Lang.String.Typ'Class;
                                        P6_List : access Standard.Java.Util.List.Typ'Class)
                                        return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReference.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   JAXWSPROVIDER_PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Provider);
   pragma Import (Java, Provider, "provider");
   pragma Export (Java, CreateServiceDelegate, "createServiceDelegate");
   pragma Export (Java, CreateEndpoint, "createEndpoint");
   pragma Export (Java, CreateAndPublishEndpoint, "createAndPublishEndpoint");
   pragma Export (Java, ReadEndpointReference, "readEndpointReference");
   pragma Export (Java, GetPort, "getPort");
   pragma Export (Java, CreateW3CEndpointReference, "createW3CEndpointReference");
   pragma Import (Java, JAXWSPROVIDER_PROPERTY, "JAXWSPROVIDER_PROPERTY");

end Javax.Xml.Ws.Spi.Provider;
pragma Import (Java, Javax.Xml.Ws.Spi.Provider, "javax.xml.ws.spi.Provider");
pragma Extensions_Allowed (Off);
