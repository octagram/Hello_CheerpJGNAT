pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Net.URL;
limited with Java.Util.Concurrent.Executor;
limited with Java.Util.Iterator;
limited with Javax.Xml.Bind.JAXBContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Ws.Dispatch;
limited with Javax.Xml.Ws.EndpointReference;
limited with Javax.Xml.Ws.Handler.HandlerResolver;
limited with Javax.Xml.Ws.Service.Mode;
limited with Javax.Xml.Ws.WebServiceFeature;
with Java.Lang.Object;

package Javax.Xml.Ws.Spi.ServiceDelegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_ServiceDelegate (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPort (This : access Typ;
                     P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                     P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function GetPort (This : access Typ;
                     P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                     P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P3_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function GetPort (This : access Typ;
                     P1_EndpointReference : access Standard.Javax.Xml.Ws.EndpointReference.Typ'Class;
                     P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P3_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function GetPort (This : access Typ;
                     P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                     return access Java.Lang.Object.Typ'Class is abstract;

   function GetPort (This : access Typ;
                     P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                     P2_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                     return access Java.Lang.Object.Typ'Class is abstract;

   procedure AddPort (This : access Typ;
                      P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class is abstract;

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class is abstract;

   function CreateDispatch (This : access Typ;
                            P1_EndpointReference : access Standard.Javax.Xml.Ws.EndpointReference.Typ'Class;
                            P2_Class : access Standard.Java.Lang.Class.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class is abstract;

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class is abstract;

   function CreateDispatch (This : access Typ;
                            P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class;
                            P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class is abstract;

   function CreateDispatch (This : access Typ;
                            P1_EndpointReference : access Standard.Javax.Xml.Ws.EndpointReference.Typ'Class;
                            P2_JAXBContext : access Standard.Javax.Xml.Bind.JAXBContext.Typ'Class;
                            P3_Mode : access Standard.Javax.Xml.Ws.Service.Mode.Typ'Class;
                            P4_WebServiceFeature_Arr : access Javax.Xml.Ws.WebServiceFeature.Arr_Obj)
                            return access Javax.Xml.Ws.Dispatch.Typ'Class is abstract;

   function GetServiceName (This : access Typ)
                            return access Javax.Xml.Namespace.QName.Typ'Class is abstract;

   function GetPorts (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class is abstract;

   function GetWSDLDocumentLocation (This : access Typ)
                                     return access Java.Net.URL.Typ'Class is abstract;

   function GetHandlerResolver (This : access Typ)
                                return access Javax.Xml.Ws.Handler.HandlerResolver.Typ'Class is abstract;

   procedure SetHandlerResolver (This : access Typ;
                                 P1_HandlerResolver : access Standard.Javax.Xml.Ws.Handler.HandlerResolver.Typ'Class) is abstract;

   function GetExecutor (This : access Typ)
                         return access Java.Util.Concurrent.Executor.Typ'Class is abstract;

   procedure SetExecutor (This : access Typ;
                          P1_Executor : access Standard.Java.Util.Concurrent.Executor.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServiceDelegate);
   pragma Export (Java, GetPort, "getPort");
   pragma Export (Java, AddPort, "addPort");
   pragma Export (Java, CreateDispatch, "createDispatch");
   pragma Export (Java, GetServiceName, "getServiceName");
   pragma Export (Java, GetPorts, "getPorts");
   pragma Export (Java, GetWSDLDocumentLocation, "getWSDLDocumentLocation");
   pragma Export (Java, GetHandlerResolver, "getHandlerResolver");
   pragma Export (Java, SetHandlerResolver, "setHandlerResolver");
   pragma Export (Java, GetExecutor, "getExecutor");
   pragma Export (Java, SetExecutor, "setExecutor");

end Javax.Xml.Ws.Spi.ServiceDelegate;
pragma Import (Java, Javax.Xml.Ws.Spi.ServiceDelegate, "javax.xml.ws.spi.ServiceDelegate");
pragma Extensions_Allowed (Off);
