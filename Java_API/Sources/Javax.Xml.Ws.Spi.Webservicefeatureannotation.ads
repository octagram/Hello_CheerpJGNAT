pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
with Java.Lang.Annotation.Annotation;
with Java.Lang.Object;

package Javax.Xml.Ws.Spi.WebServiceFeatureAnnotation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Annotation_I : Java.Lang.Annotation.Annotation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Id (This : access Typ)
                return access Java.Lang.String.Typ'Class is abstract;

   function Bean (This : access Typ)
                  return access Java.Lang.Class.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Id, "id");
   pragma Export (Java, Bean, "bean");

end Javax.Xml.Ws.Spi.WebServiceFeatureAnnotation;
pragma Import (Java, Javax.Xml.Ws.Spi.WebServiceFeatureAnnotation, "javax.xml.ws.spi.WebServiceFeatureAnnotation");
pragma Extensions_Allowed (Off);
