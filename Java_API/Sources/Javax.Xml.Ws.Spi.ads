pragma Extensions_Allowed (On);
package Javax.Xml.Ws.Spi is
   pragma Preelaborate;
end Javax.Xml.Ws.Spi;
pragma Import (Java, Javax.Xml.Ws.Spi, "javax.xml.ws.spi");
pragma Extensions_Allowed (Off);
