pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Security.Principal;
limited with Javax.Xml.Ws.EndpointReference;
limited with Javax.Xml.Ws.Handler.MessageContext;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;

package Javax.Xml.Ws.WebServiceContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessageContext (This : access Typ)
                               return access Javax.Xml.Ws.Handler.MessageContext.Typ'Class is abstract;

   function GetUserPrincipal (This : access Typ)
                              return access Java.Security.Principal.Typ'Class is abstract;

   function IsUserInRole (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean is abstract;

   function GetEndpointReference (This : access Typ;
                                  P1_Element_Arr : access Org.W3c.Dom.Element.Arr_Obj)
                                  return access Javax.Xml.Ws.EndpointReference.Typ'Class is abstract;

   function GetEndpointReference (This : access Typ;
                                  P1_Class : access Standard.Java.Lang.Class.Typ'Class;
                                  P2_Element_Arr : access Org.W3c.Dom.Element.Arr_Obj)
                                  return access Javax.Xml.Ws.EndpointReference.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMessageContext, "getMessageContext");
   pragma Export (Java, GetUserPrincipal, "getUserPrincipal");
   pragma Export (Java, IsUserInRole, "isUserInRole");
   pragma Export (Java, GetEndpointReference, "getEndpointReference");

end Javax.Xml.Ws.WebServiceContext;
pragma Import (Java, Javax.Xml.Ws.WebServiceContext, "javax.xml.ws.WebServiceContext");
pragma Extensions_Allowed (Off);
