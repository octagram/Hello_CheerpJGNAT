pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Xml.Ws.WebServiceFeature is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Enabled : Java.Boolean;
      pragma Import (Java, Enabled, "enabled");

   end record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetID (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   --  protected
   function New_WebServiceFeature (This : Ref := null)
                                   return Ref;

   function IsEnabled (This : access Typ)
                       return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetID, "getID");
   pragma Java_Constructor (New_WebServiceFeature);
   pragma Import (Java, IsEnabled, "isEnabled");

end Javax.Xml.Ws.WebServiceFeature;
pragma Import (Java, Javax.Xml.Ws.WebServiceFeature, "javax.xml.ws.WebServiceFeature");
pragma Extensions_Allowed (Off);
