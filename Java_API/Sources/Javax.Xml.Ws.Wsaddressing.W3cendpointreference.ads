pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Transform.Result;
limited with Javax.Xml.Transform.Source;
with Java.Lang.Object;
with Javax.Xml.Ws.EndpointReference;

package Javax.Xml.Ws.Wsaddressing.W3CEndpointReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Xml.Ws.EndpointReference.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_W3CEndpointReference (This : Ref := null)
                                      return Ref;

   function New_W3CEndpointReference (P1_Source : access Standard.Javax.Xml.Transform.Source.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure WriteTo (This : access Typ;
                      P1_Result : access Standard.Javax.Xml.Transform.Result.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  protected  final
   NS : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_W3CEndpointReference);
   pragma Import (Java, WriteTo, "writeTo");
   pragma Import (Java, NS, "NS");

end Javax.Xml.Ws.Wsaddressing.W3CEndpointReference;
pragma Import (Java, Javax.Xml.Ws.Wsaddressing.W3CEndpointReference, "javax.xml.ws.wsaddressing.W3CEndpointReference");
pragma Extensions_Allowed (Off);
