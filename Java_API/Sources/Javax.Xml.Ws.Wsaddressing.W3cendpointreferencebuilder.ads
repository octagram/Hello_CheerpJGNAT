pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Ws.Wsaddressing.W3CEndpointReference;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;

package Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_W3CEndpointReferenceBuilder (This : Ref := null)
                                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Address (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder.Typ'Class;

   function ServiceName (This : access Typ;
                         P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                         return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder.Typ'Class;

   function EndpointName (This : access Typ;
                          P1_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                          return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder.Typ'Class;

   function WsdlDocumentLocation (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder.Typ'Class;

   function ReferenceParameter (This : access Typ;
                                P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class)
                                return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder.Typ'Class;

   function Metadata (This : access Typ;
                      P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class)
                      return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder.Typ'Class;

   function Build (This : access Typ)
                   return access Javax.Xml.Ws.Wsaddressing.W3CEndpointReference.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_W3CEndpointReferenceBuilder);
   pragma Import (Java, Address, "address");
   pragma Import (Java, ServiceName, "serviceName");
   pragma Import (Java, EndpointName, "endpointName");
   pragma Import (Java, WsdlDocumentLocation, "wsdlDocumentLocation");
   pragma Import (Java, ReferenceParameter, "referenceParameter");
   pragma Import (Java, Metadata, "metadata");
   pragma Import (Java, Build, "build");

end Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder;
pragma Import (Java, Javax.Xml.Ws.Wsaddressing.W3CEndpointReferenceBuilder, "javax.xml.ws.wsaddressing.W3CEndpointReferenceBuilder");
pragma Extensions_Allowed (Off);
