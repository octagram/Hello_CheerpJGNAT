pragma Extensions_Allowed (On);
package Javax.Xml.Ws is
   pragma Preelaborate;
end Javax.Xml.Ws;
pragma Import (Java, Javax.Xml.Ws, "javax.xml.ws");
pragma Extensions_Allowed (Off);
