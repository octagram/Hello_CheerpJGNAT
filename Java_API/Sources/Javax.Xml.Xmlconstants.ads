pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Javax.Xml.XMLConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NULL_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   DEFAULT_NS_PREFIX : constant access Java.Lang.String.Typ'Class;

   --  final
   XML_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   XML_NS_PREFIX : constant access Java.Lang.String.Typ'Class;

   --  final
   XMLNS_ATTRIBUTE_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   XMLNS_ATTRIBUTE : constant access Java.Lang.String.Typ'Class;

   --  final
   W3C_XML_SCHEMA_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   W3C_XML_SCHEMA_INSTANCE_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   W3C_XPATH_DATATYPE_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   XML_DTD_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   RELAXNG_NS_URI : constant access Java.Lang.String.Typ'Class;

   --  final
   FEATURE_SECURE_PROCESSING : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NULL_NS_URI, "NULL_NS_URI");
   pragma Import (Java, DEFAULT_NS_PREFIX, "DEFAULT_NS_PREFIX");
   pragma Import (Java, XML_NS_URI, "XML_NS_URI");
   pragma Import (Java, XML_NS_PREFIX, "XML_NS_PREFIX");
   pragma Import (Java, XMLNS_ATTRIBUTE_NS_URI, "XMLNS_ATTRIBUTE_NS_URI");
   pragma Import (Java, XMLNS_ATTRIBUTE, "XMLNS_ATTRIBUTE");
   pragma Import (Java, W3C_XML_SCHEMA_NS_URI, "W3C_XML_SCHEMA_NS_URI");
   pragma Import (Java, W3C_XML_SCHEMA_INSTANCE_NS_URI, "W3C_XML_SCHEMA_INSTANCE_NS_URI");
   pragma Import (Java, W3C_XPATH_DATATYPE_NS_URI, "W3C_XPATH_DATATYPE_NS_URI");
   pragma Import (Java, XML_DTD_NS_URI, "XML_DTD_NS_URI");
   pragma Import (Java, RELAXNG_NS_URI, "RELAXNG_NS_URI");
   pragma Import (Java, FEATURE_SECURE_PROCESSING, "FEATURE_SECURE_PROCESSING");

end Javax.Xml.XMLConstants;
pragma Import (Java, Javax.Xml.XMLConstants, "javax.xml.XMLConstants");
pragma Extensions_Allowed (Off);
