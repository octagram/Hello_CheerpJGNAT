pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.NamespaceContext;
limited with Javax.Xml.Namespace.QName;
limited with Javax.Xml.Xpath.XPathExpression;
limited with Javax.Xml.Xpath.XPathFunctionResolver;
limited with Javax.Xml.Xpath.XPathVariableResolver;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;

package Javax.Xml.Xpath.XPath is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ) is abstract;

   procedure SetXPathVariableResolver (This : access Typ;
                                       P1_XPathVariableResolver : access Standard.Javax.Xml.Xpath.XPathVariableResolver.Typ'Class) is abstract;

   function GetXPathVariableResolver (This : access Typ)
                                      return access Javax.Xml.Xpath.XPathVariableResolver.Typ'Class is abstract;

   procedure SetXPathFunctionResolver (This : access Typ;
                                       P1_XPathFunctionResolver : access Standard.Javax.Xml.Xpath.XPathFunctionResolver.Typ'Class) is abstract;

   function GetXPathFunctionResolver (This : access Typ)
                                      return access Javax.Xml.Xpath.XPathFunctionResolver.Typ'Class is abstract;

   procedure SetNamespaceContext (This : access Typ;
                                  P1_NamespaceContext : access Standard.Javax.Xml.Namespace.NamespaceContext.Typ'Class) is abstract;

   function GetNamespaceContext (This : access Typ)
                                 return access Javax.Xml.Namespace.NamespaceContext.Typ'Class is abstract;

   function Compile (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Javax.Xml.Xpath.XPathExpression.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except

   function Evaluate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P3_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except

   function Evaluate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except

   function Evaluate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class;
                      P3_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except

   function Evaluate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class)
                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Reset, "reset");
   pragma Export (Java, SetXPathVariableResolver, "setXPathVariableResolver");
   pragma Export (Java, GetXPathVariableResolver, "getXPathVariableResolver");
   pragma Export (Java, SetXPathFunctionResolver, "setXPathFunctionResolver");
   pragma Export (Java, GetXPathFunctionResolver, "getXPathFunctionResolver");
   pragma Export (Java, SetNamespaceContext, "setNamespaceContext");
   pragma Export (Java, GetNamespaceContext, "getNamespaceContext");
   pragma Export (Java, Compile, "compile");
   pragma Export (Java, Evaluate, "evaluate");

end Javax.Xml.Xpath.XPath;
pragma Import (Java, Javax.Xml.Xpath.XPath, "javax.xml.xpath.XPath");
pragma Extensions_Allowed (Off);
