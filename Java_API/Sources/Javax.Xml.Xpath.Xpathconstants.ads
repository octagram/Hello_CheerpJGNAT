pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.QName;
with Java.Lang.Object;

package Javax.Xml.Xpath.XPathConstants is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NUMBER : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   STRING : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   BOOLEAN : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   NODESET : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   NODE : access Javax.Xml.Namespace.QName.Typ'Class;

   --  final
   DOM_OBJECT_MODEL : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NUMBER, "NUMBER");
   pragma Import (Java, STRING, "STRING");
   pragma Import (Java, BOOLEAN, "BOOLEAN");
   pragma Import (Java, NODESET, "NODESET");
   pragma Import (Java, NODE, "NODE");
   pragma Import (Java, DOM_OBJECT_MODEL, "DOM_OBJECT_MODEL");

end Javax.Xml.Xpath.XPathConstants;
pragma Import (Java, Javax.Xml.Xpath.XPathConstants, "javax.xml.xpath.XPathConstants");
pragma Extensions_Allowed (Off);
