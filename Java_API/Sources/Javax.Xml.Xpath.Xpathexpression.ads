pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Namespace.QName;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;

package Javax.Xml.Xpath.XPathExpression is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Evaluate (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                      P2_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except

   function Evaluate (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except

   function Evaluate (This : access Typ;
                      P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class;
                      P2_QName : access Standard.Javax.Xml.Namespace.QName.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except

   function Evaluate (This : access Typ;
                      P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class)
                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathExpressionException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Evaluate, "evaluate");

end Javax.Xml.Xpath.XPathExpression;
pragma Import (Java, Javax.Xml.Xpath.XPathExpression, "javax.xml.xpath.XPathExpression");
pragma Extensions_Allowed (Off);
