pragma Extensions_Allowed (On);
limited with Java.Lang.ClassLoader;
limited with Java.Lang.String;
limited with Javax.Xml.Xpath.XPath;
limited with Javax.Xml.Xpath.XPathFunctionResolver;
limited with Javax.Xml.Xpath.XPathVariableResolver;
with Java.Lang.Object;

package Javax.Xml.Xpath.XPathFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   --  protected
   function New_XPathFactory (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function NewInstance return access Javax.Xml.Xpath.XPathFactory.Typ'Class;

   --  final
   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Javax.Xml.Xpath.XPathFactory.Typ'Class;
   --  can raise Javax.Xml.Xpath.XPathFactoryConfigurationException.Except

   function NewInstance (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_ClassLoader : access Standard.Java.Lang.ClassLoader.Typ'Class)
                         return access Javax.Xml.Xpath.XPathFactory.Typ'Class;
   --  can raise Javax.Xml.Xpath.XPathFactoryConfigurationException.Except

   function IsObjectModelSupported (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                                    return Java.Boolean is abstract;

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Javax.Xml.Xpath.XPathFactoryConfigurationException.Except

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Javax.Xml.Xpath.XPathFactoryConfigurationException.Except

   procedure SetXPathVariableResolver (This : access Typ;
                                       P1_XPathVariableResolver : access Standard.Javax.Xml.Xpath.XPathVariableResolver.Typ'Class) is abstract;

   procedure SetXPathFunctionResolver (This : access Typ;
                                       P1_XPathFunctionResolver : access Standard.Javax.Xml.Xpath.XPathFunctionResolver.Typ'Class) is abstract;

   function NewXPath (This : access Typ)
                      return access Javax.Xml.Xpath.XPath.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_PROPERTY_NAME : constant access Java.Lang.String.Typ'Class;

   --  final
   DEFAULT_OBJECT_MODEL_URI : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XPathFactory);
   pragma Export (Java, NewInstance, "newInstance");
   pragma Export (Java, IsObjectModelSupported, "isObjectModelSupported");
   pragma Export (Java, SetFeature, "setFeature");
   pragma Export (Java, GetFeature, "getFeature");
   pragma Export (Java, SetXPathVariableResolver, "setXPathVariableResolver");
   pragma Export (Java, SetXPathFunctionResolver, "setXPathFunctionResolver");
   pragma Export (Java, NewXPath, "newXPath");
   pragma Import (Java, DEFAULT_PROPERTY_NAME, "DEFAULT_PROPERTY_NAME");
   pragma Import (Java, DEFAULT_OBJECT_MODEL_URI, "DEFAULT_OBJECT_MODEL_URI");

end Javax.Xml.Xpath.XPathFactory;
pragma Import (Java, Javax.Xml.Xpath.XPathFactory, "javax.xml.xpath.XPathFactory");
pragma Extensions_Allowed (Off);
