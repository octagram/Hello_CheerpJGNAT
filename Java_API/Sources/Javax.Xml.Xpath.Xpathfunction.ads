pragma Extensions_Allowed (On);
limited with Java.Util.List;
with Java.Lang.Object;

package Javax.Xml.Xpath.XPathFunction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Evaluate (This : access Typ;
                      P1_List : access Standard.Java.Util.List.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Javax.Xml.Xpath.XPathFunctionException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Evaluate, "evaluate");

end Javax.Xml.Xpath.XPathFunction;
pragma Import (Java, Javax.Xml.Xpath.XPathFunction, "javax.xml.xpath.XPathFunction");
pragma Extensions_Allowed (Off);
