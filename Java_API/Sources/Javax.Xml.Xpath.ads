pragma Extensions_Allowed (On);
package Javax.Xml.Xpath is
   pragma Preelaborate;
end Javax.Xml.Xpath;
pragma Import (Java, Javax.Xml.Xpath, "javax.xml.xpath");
pragma Extensions_Allowed (Off);
