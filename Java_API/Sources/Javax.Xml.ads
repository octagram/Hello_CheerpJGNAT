pragma Extensions_Allowed (On);
package Javax.Xml is
   pragma Preelaborate;
end Javax.Xml;
pragma Import (Java, Javax.Xml, "javax.xml");
pragma Extensions_Allowed (Off);
