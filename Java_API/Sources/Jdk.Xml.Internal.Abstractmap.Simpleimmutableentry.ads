pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Util.Map.Entry_K;

package Jdk.Xml.Internal.AbstractMap.SimpleImmutableEntry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Entry_K_I : Java.Util.Map.Entry_K.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SimpleImmutableEntry (P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                                      P2_Object : access Standard.Java.Lang.Object.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_SimpleImmutableEntry (P1_Entry_K : access Standard.Java.Util.Map.Entry_K.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKey (This : access Typ)
                    return access Java.Lang.Object.Typ'Class;

   function GetValue (This : access Typ)
                      return access Java.Lang.Object.Typ'Class;

   function SetValue (This : access Typ;
                      P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SimpleImmutableEntry);
   pragma Import (Java, GetKey, "getKey");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, SetValue, "setValue");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, ToString, "toString");

end Jdk.Xml.Internal.AbstractMap.SimpleImmutableEntry;
pragma Import (Java, Jdk.Xml.Internal.AbstractMap.SimpleImmutableEntry, "jdk.xml.internal.AbstractMap$SimpleImmutableEntry");
pragma Extensions_Allowed (Off);
