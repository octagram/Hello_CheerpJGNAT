pragma Extensions_Allowed (On);
limited with Java.Util.Collection;
limited with Java.Util.Set;
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Util.Map;
with Jdk.Xml.Internal.AbstractMap;

package Jdk.Xml.Internal.HashMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Jdk.Xml.Internal.AbstractMap.Typ(Map_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HashMap (P1_Int : Java.Int;
                         P2_Float : Java.Float; 
                         This : Ref := null)
                         return Ref;

   function New_HashMap (P1_Int : Java.Int; 
                         This : Ref := null)
                         return Ref;

   function New_HashMap (This : Ref := null)
                         return Ref;

   function New_HashMap (P1_Map : access Standard.Java.Util.Map.Typ'Class; 
                         This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Size (This : access Typ)
                  return Java.Int;

   function IsEmpty (This : access Typ)
                     return Java.Boolean;

   function Get (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   function ContainsKey (This : access Typ;
                         P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                         return Java.Boolean;

   function Put (This : access Typ;
                 P1_Object : access Standard.Java.Lang.Object.Typ'Class;
                 P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                 return access Java.Lang.Object.Typ'Class;

   procedure PutAll (This : access Typ;
                     P1_Map : access Standard.Java.Util.Map.Typ'Class);

   function Remove (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return access Java.Lang.Object.Typ'Class;

   procedure Clear (This : access Typ);

   function ContainsValue (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                           return Java.Boolean;

   function KeySet (This : access Typ)
                    return access Java.Util.Set.Typ'Class;

   function Values (This : access Typ)
                    return access Java.Util.Collection.Typ'Class;

   function EntrySet (This : access Typ)
                      return access Java.Util.Set.Typ'Class;

   function Clone (This : access Typ)
                   return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HashMap);
   pragma Import (Java, Size, "size");
   pragma Import (Java, IsEmpty, "isEmpty");
   pragma Import (Java, Get, "get");
   pragma Import (Java, ContainsKey, "containsKey");
   pragma Import (Java, Put, "put");
   pragma Import (Java, PutAll, "putAll");
   pragma Import (Java, Remove, "remove");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, ContainsValue, "containsValue");
   pragma Import (Java, KeySet, "keySet");
   pragma Import (Java, Values, "values");
   pragma Import (Java, EntrySet, "entrySet");
   pragma Import (Java, Clone, "clone");

end Jdk.Xml.Internal.HashMap;
pragma Import (Java, Jdk.Xml.Internal.HashMap, "jdk.xml.internal.HashMap");
pragma Extensions_Allowed (Off);
