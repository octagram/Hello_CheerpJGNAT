pragma Extensions_Allowed (On);
package Jdk.Xml.Internal is
   pragma Preelaborate;
end Jdk.Xml.Internal;
pragma Import (Java, Jdk.Xml.Internal, "jdk.xml.internal");
pragma Extensions_Allowed (Off);
