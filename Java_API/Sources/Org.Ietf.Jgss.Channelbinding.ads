pragma Extensions_Allowed (On);
limited with Java.Net.InetAddress;
with Java.Lang.Object;

package Org.Ietf.Jgss.ChannelBinding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ChannelBinding (P1_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                P2_InetAddress : access Standard.Java.Net.InetAddress.Typ'Class;
                                P3_Byte_Arr : Java.Byte_Arr; 
                                This : Ref := null)
                                return Ref;

   function New_ChannelBinding (P1_Byte_Arr : Java.Byte_Arr; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInitiatorAddress (This : access Typ)
                                 return access Java.Net.InetAddress.Typ'Class;

   function GetAcceptorAddress (This : access Typ)
                                return access Java.Net.InetAddress.Typ'Class;

   function GetApplicationData (This : access Typ)
                                return Java.Byte_Arr;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ChannelBinding);
   pragma Import (Java, GetInitiatorAddress, "getInitiatorAddress");
   pragma Import (Java, GetAcceptorAddress, "getAcceptorAddress");
   pragma Import (Java, GetApplicationData, "getApplicationData");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, HashCode, "hashCode");

end Org.Ietf.Jgss.ChannelBinding;
pragma Import (Java, Org.Ietf.Jgss.ChannelBinding, "org.ietf.jgss.ChannelBinding");
pragma Extensions_Allowed (Off);
