pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.OutputStream;
limited with Org.Ietf.Jgss.ChannelBinding;
limited with Org.Ietf.Jgss.GSSCredential;
limited with Org.Ietf.Jgss.GSSName;
limited with Org.Ietf.Jgss.MessageProp;
limited with Org.Ietf.Jgss.Oid;
with Java.Lang.Object;

package Org.Ietf.Jgss.GSSContext is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function InitSecContext (This : access Typ;
                            P1_Byte_Arr : Java.Byte_Arr;
                            P2_Int : Java.Int;
                            P3_Int : Java.Int)
                            return Java.Byte_Arr is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function InitSecContext (This : access Typ;
                            P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                            P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                            return Java.Int is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function AcceptSecContext (This : access Typ;
                              P1_Byte_Arr : Java.Byte_Arr;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int)
                              return Java.Byte_Arr is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure AcceptSecContext (This : access Typ;
                               P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                               P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function IsEstablished (This : access Typ)
                           return Java.Boolean is abstract;

   procedure Dispose (This : access Typ) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetWrapSizeLimit (This : access Typ;
                              P1_Int : Java.Int;
                              P2_Boolean : Java.Boolean;
                              P3_Int : Java.Int)
                              return Java.Int is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function Wrap (This : access Typ;
                  P1_Byte_Arr : Java.Byte_Arr;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int;
                  P4_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class)
                  return Java.Byte_Arr is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure Wrap (This : access Typ;
                   P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                   P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                   P3_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function Unwrap (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class)
                    return Java.Byte_Arr is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure Unwrap (This : access Typ;
                     P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                     P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                     P3_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetMIC (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int;
                    P4_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class)
                    return Java.Byte_Arr is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure GetMIC (This : access Typ;
                     P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                     P2_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class;
                     P3_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure VerifyMIC (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Int : Java.Int;
                        P3_Int : Java.Int;
                        P4_Byte_Arr : Java.Byte_Arr;
                        P5_Int : Java.Int;
                        P6_Int : Java.Int;
                        P7_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure VerifyMIC (This : access Typ;
                        P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                        P2_InputStream : access Standard.Java.Io.InputStream.Typ'Class;
                        P3_MessageProp : access Standard.Org.Ietf.Jgss.MessageProp.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function Export (This : access Typ)
                    return Java.Byte_Arr is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestMutualAuth (This : access Typ;
                                P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestReplayDet (This : access Typ;
                               P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestSequenceDet (This : access Typ;
                                 P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestCredDeleg (This : access Typ;
                               P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestAnonymity (This : access Typ;
                               P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestConf (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestInteg (This : access Typ;
                           P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure RequestLifetime (This : access Typ;
                              P1_Int : Java.Int) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure SetChannelBinding (This : access Typ;
                                P1_ChannelBinding : access Standard.Org.Ietf.Jgss.ChannelBinding.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetCredDelegState (This : access Typ)
                               return Java.Boolean is abstract;

   function GetMutualAuthState (This : access Typ)
                                return Java.Boolean is abstract;

   function GetReplayDetState (This : access Typ)
                               return Java.Boolean is abstract;

   function GetSequenceDetState (This : access Typ)
                                 return Java.Boolean is abstract;

   function GetAnonymityState (This : access Typ)
                               return Java.Boolean is abstract;

   function IsTransferable (This : access Typ)
                            return Java.Boolean is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function IsProtReady (This : access Typ)
                         return Java.Boolean is abstract;

   function GetConfState (This : access Typ)
                          return Java.Boolean is abstract;

   function GetIntegState (This : access Typ)
                           return Java.Boolean is abstract;

   function GetLifetime (This : access Typ)
                         return Java.Int is abstract;

   function GetSrcName (This : access Typ)
                        return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetTargName (This : access Typ)
                         return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetMech (This : access Typ)
                     return access Org.Ietf.Jgss.Oid.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetDelegCred (This : access Typ)
                          return access Org.Ietf.Jgss.GSSCredential.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function IsInitiator (This : access Typ)
                         return Java.Boolean is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DEFAULT_LIFETIME : constant Java.Int;

   --  final
   INDEFINITE_LIFETIME : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, InitSecContext, "initSecContext");
   pragma Export (Java, AcceptSecContext, "acceptSecContext");
   pragma Export (Java, IsEstablished, "isEstablished");
   pragma Export (Java, Dispose, "dispose");
   pragma Export (Java, GetWrapSizeLimit, "getWrapSizeLimit");
   pragma Export (Java, Wrap, "wrap");
   pragma Export (Java, Unwrap, "unwrap");
   pragma Export (Java, GetMIC, "getMIC");
   pragma Export (Java, VerifyMIC, "verifyMIC");
   pragma Export (Java, Export, "export");
   pragma Export (Java, RequestMutualAuth, "requestMutualAuth");
   pragma Export (Java, RequestReplayDet, "requestReplayDet");
   pragma Export (Java, RequestSequenceDet, "requestSequenceDet");
   pragma Export (Java, RequestCredDeleg, "requestCredDeleg");
   pragma Export (Java, RequestAnonymity, "requestAnonymity");
   pragma Export (Java, RequestConf, "requestConf");
   pragma Export (Java, RequestInteg, "requestInteg");
   pragma Export (Java, RequestLifetime, "requestLifetime");
   pragma Export (Java, SetChannelBinding, "setChannelBinding");
   pragma Export (Java, GetCredDelegState, "getCredDelegState");
   pragma Export (Java, GetMutualAuthState, "getMutualAuthState");
   pragma Export (Java, GetReplayDetState, "getReplayDetState");
   pragma Export (Java, GetSequenceDetState, "getSequenceDetState");
   pragma Export (Java, GetAnonymityState, "getAnonymityState");
   pragma Export (Java, IsTransferable, "isTransferable");
   pragma Export (Java, IsProtReady, "isProtReady");
   pragma Export (Java, GetConfState, "getConfState");
   pragma Export (Java, GetIntegState, "getIntegState");
   pragma Export (Java, GetLifetime, "getLifetime");
   pragma Export (Java, GetSrcName, "getSrcName");
   pragma Export (Java, GetTargName, "getTargName");
   pragma Export (Java, GetMech, "getMech");
   pragma Export (Java, GetDelegCred, "getDelegCred");
   pragma Export (Java, IsInitiator, "isInitiator");
   pragma Import (Java, DEFAULT_LIFETIME, "DEFAULT_LIFETIME");
   pragma Import (Java, INDEFINITE_LIFETIME, "INDEFINITE_LIFETIME");

end Org.Ietf.Jgss.GSSContext;
pragma Import (Java, Org.Ietf.Jgss.GSSContext, "org.ietf.jgss.GSSContext");
pragma Extensions_Allowed (Off);
