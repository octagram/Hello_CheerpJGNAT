pragma Extensions_Allowed (On);
limited with Org.Ietf.Jgss.GSSName;
limited with Org.Ietf.Jgss.Oid;
with Java.Lang.Cloneable;
with Java.Lang.Object;

package Org.Ietf.Jgss.GSSCredential is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Cloneable_I : Java.Lang.Cloneable.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Dispose (This : access Typ) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetName (This : access Typ)
                     return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetName (This : access Typ;
                     P1_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                     return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetRemainingLifetime (This : access Typ)
                                  return Java.Int is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetRemainingInitLifetime (This : access Typ;
                                      P1_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                                      return Java.Int is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetRemainingAcceptLifetime (This : access Typ;
                                        P1_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                                        return Java.Int is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetUsage (This : access Typ)
                      return Java.Int is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetUsage (This : access Typ;
                      P1_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                      return Java.Int is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetMechs (This : access Typ)
                      return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure Add (This : access Typ;
                  P1_GSSName : access Standard.Org.Ietf.Jgss.GSSName.Typ'Class;
                  P2_Int : Java.Int;
                  P3_Int : Java.Int;
                  P4_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class;
                  P5_Int : Java.Int) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INITIATE_AND_ACCEPT : constant Java.Int;

   --  final
   INITIATE_ONLY : constant Java.Int;

   --  final
   ACCEPT_ONLY : constant Java.Int;

   --  final
   DEFAULT_LIFETIME : constant Java.Int;

   --  final
   INDEFINITE_LIFETIME : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Dispose, "dispose");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetRemainingLifetime, "getRemainingLifetime");
   pragma Export (Java, GetRemainingInitLifetime, "getRemainingInitLifetime");
   pragma Export (Java, GetRemainingAcceptLifetime, "getRemainingAcceptLifetime");
   pragma Export (Java, GetUsage, "getUsage");
   pragma Export (Java, GetMechs, "getMechs");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Import (Java, INITIATE_AND_ACCEPT, "INITIATE_AND_ACCEPT");
   pragma Import (Java, INITIATE_ONLY, "INITIATE_ONLY");
   pragma Import (Java, ACCEPT_ONLY, "ACCEPT_ONLY");
   pragma Import (Java, DEFAULT_LIFETIME, "DEFAULT_LIFETIME");
   pragma Import (Java, INDEFINITE_LIFETIME, "INDEFINITE_LIFETIME");

end Org.Ietf.Jgss.GSSCredential;
pragma Import (Java, Org.Ietf.Jgss.GSSCredential, "org.ietf.jgss.GSSCredential");
pragma Extensions_Allowed (Off);
