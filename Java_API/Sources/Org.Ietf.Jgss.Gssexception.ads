pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Org.Ietf.Jgss.GSSException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_GSSException (P1_Int : Java.Int; 
                              This : Ref := null)
                              return Ref;

   function New_GSSException (P1_Int : Java.Int;
                              P2_Int : Java.Int;
                              P3_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMajor (This : access Typ)
                      return Java.Int;

   function GetMinor (This : access Typ)
                      return Java.Int;

   function GetMajorString (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function GetMinorString (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetMinor (This : access Typ;
                       P1_Int : Java.Int;
                       P2_String : access Standard.Java.Lang.String.Typ'Class);

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   BAD_BINDINGS : constant Java.Int;

   --  final
   BAD_MECH : constant Java.Int;

   --  final
   BAD_NAME : constant Java.Int;

   --  final
   BAD_NAMETYPE : constant Java.Int;

   --  final
   BAD_STATUS : constant Java.Int;

   --  final
   BAD_MIC : constant Java.Int;

   --  final
   CONTEXT_EXPIRED : constant Java.Int;

   --  final
   CREDENTIALS_EXPIRED : constant Java.Int;

   --  final
   DEFECTIVE_CREDENTIAL : constant Java.Int;

   --  final
   DEFECTIVE_TOKEN : constant Java.Int;

   --  final
   FAILURE : constant Java.Int;

   --  final
   NO_CONTEXT : constant Java.Int;

   --  final
   NO_CRED : constant Java.Int;

   --  final
   BAD_QOP : constant Java.Int;

   --  final
   UNAUTHORIZED : constant Java.Int;

   --  final
   UNAVAILABLE : constant Java.Int;

   --  final
   DUPLICATE_ELEMENT : constant Java.Int;

   --  final
   NAME_NOT_MN : constant Java.Int;

   --  final
   DUPLICATE_TOKEN : constant Java.Int;

   --  final
   OLD_TOKEN : constant Java.Int;

   --  final
   UNSEQ_TOKEN : constant Java.Int;

   --  final
   GAP_TOKEN : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.ietf.jgss.GSSException");
   pragma Java_Constructor (New_GSSException);
   pragma Import (Java, GetMajor, "getMajor");
   pragma Import (Java, GetMinor, "getMinor");
   pragma Import (Java, GetMajorString, "getMajorString");
   pragma Import (Java, GetMinorString, "getMinorString");
   pragma Import (Java, SetMinor, "setMinor");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, BAD_BINDINGS, "BAD_BINDINGS");
   pragma Import (Java, BAD_MECH, "BAD_MECH");
   pragma Import (Java, BAD_NAME, "BAD_NAME");
   pragma Import (Java, BAD_NAMETYPE, "BAD_NAMETYPE");
   pragma Import (Java, BAD_STATUS, "BAD_STATUS");
   pragma Import (Java, BAD_MIC, "BAD_MIC");
   pragma Import (Java, CONTEXT_EXPIRED, "CONTEXT_EXPIRED");
   pragma Import (Java, CREDENTIALS_EXPIRED, "CREDENTIALS_EXPIRED");
   pragma Import (Java, DEFECTIVE_CREDENTIAL, "DEFECTIVE_CREDENTIAL");
   pragma Import (Java, DEFECTIVE_TOKEN, "DEFECTIVE_TOKEN");
   pragma Import (Java, FAILURE, "FAILURE");
   pragma Import (Java, NO_CONTEXT, "NO_CONTEXT");
   pragma Import (Java, NO_CRED, "NO_CRED");
   pragma Import (Java, BAD_QOP, "BAD_QOP");
   pragma Import (Java, UNAUTHORIZED, "UNAUTHORIZED");
   pragma Import (Java, UNAVAILABLE, "UNAVAILABLE");
   pragma Import (Java, DUPLICATE_ELEMENT, "DUPLICATE_ELEMENT");
   pragma Import (Java, NAME_NOT_MN, "NAME_NOT_MN");
   pragma Import (Java, DUPLICATE_TOKEN, "DUPLICATE_TOKEN");
   pragma Import (Java, OLD_TOKEN, "OLD_TOKEN");
   pragma Import (Java, UNSEQ_TOKEN, "UNSEQ_TOKEN");
   pragma Import (Java, GAP_TOKEN, "GAP_TOKEN");

end Org.Ietf.Jgss.GSSException;
pragma Import (Java, Org.Ietf.Jgss.GSSException, "org.ietf.jgss.GSSException");
pragma Extensions_Allowed (Off);
