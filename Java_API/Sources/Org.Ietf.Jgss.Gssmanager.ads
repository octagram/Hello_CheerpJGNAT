pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Org.Ietf.Jgss.GSSContext;
limited with Org.Ietf.Jgss.GSSCredential;
limited with Org.Ietf.Jgss.GSSName;
limited with Org.Ietf.Jgss.Oid;
with Java.Lang.Object;

package Org.Ietf.Jgss.GSSManager is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_GSSManager (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInstance return access Org.Ietf.Jgss.GSSManager.Typ'Class;

   function GetMechs (This : access Typ)
                      return Standard.Java.Lang.Object.Ref is abstract;

   function GetNamesForMech (This : access Typ;
                             P1_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                             return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function GetMechsForName (This : access Typ;
                             P1_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                             return Standard.Java.Lang.Object.Ref is abstract;

   function CreateName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                        return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateName (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                        return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class;
                        P3_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                        return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateName (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class;
                        P3_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                        return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateCredential (This : access Typ;
                              P1_Int : Java.Int)
                              return access Org.Ietf.Jgss.GSSCredential.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateCredential (This : access Typ;
                              P1_GSSName : access Standard.Org.Ietf.Jgss.GSSName.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class;
                              P4_Int : Java.Int)
                              return access Org.Ietf.Jgss.GSSCredential.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateCredential (This : access Typ;
                              P1_GSSName : access Standard.Org.Ietf.Jgss.GSSName.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Oid_Arr : access Org.Ietf.Jgss.Oid.Arr_Obj;
                              P4_Int : Java.Int)
                              return access Org.Ietf.Jgss.GSSCredential.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateContext (This : access Typ;
                           P1_GSSName : access Standard.Org.Ietf.Jgss.GSSName.Typ'Class;
                           P2_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class;
                           P3_GSSCredential : access Standard.Org.Ietf.Jgss.GSSCredential.Typ'Class;
                           P4_Int : Java.Int)
                           return access Org.Ietf.Jgss.GSSContext.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateContext (This : access Typ;
                           P1_GSSCredential : access Standard.Org.Ietf.Jgss.GSSCredential.Typ'Class)
                           return access Org.Ietf.Jgss.GSSContext.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function CreateContext (This : access Typ;
                           P1_Byte_Arr : Java.Byte_Arr)
                           return access Org.Ietf.Jgss.GSSContext.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure AddProviderAtFront (This : access Typ;
                                 P1_Provider : access Standard.Java.Security.Provider.Typ'Class;
                                 P2_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   procedure AddProviderAtEnd (This : access Typ;
                               P1_Provider : access Standard.Java.Security.Provider.Typ'Class;
                               P2_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class) is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_GSSManager);
   pragma Import (Java, GetInstance, "getInstance");
   pragma Export (Java, GetMechs, "getMechs");
   pragma Export (Java, GetNamesForMech, "getNamesForMech");
   pragma Export (Java, GetMechsForName, "getMechsForName");
   pragma Export (Java, CreateName, "createName");
   pragma Export (Java, CreateCredential, "createCredential");
   pragma Export (Java, CreateContext, "createContext");
   pragma Export (Java, AddProviderAtFront, "addProviderAtFront");
   pragma Export (Java, AddProviderAtEnd, "addProviderAtEnd");

end Org.Ietf.Jgss.GSSManager;
pragma Import (Java, Org.Ietf.Jgss.GSSManager, "org.ietf.jgss.GSSManager");
pragma Extensions_Allowed (Off);
