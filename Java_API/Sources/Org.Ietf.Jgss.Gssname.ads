pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Ietf.Jgss.Oid;
with Java.Lang.Object;

package Org.Ietf.Jgss.GSSName is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equals (This : access Typ;
                    P1_GSSName : access Standard.Org.Ietf.Jgss.GSSName.Typ'Class)
                    return Java.Boolean is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean is abstract;

   function HashCode (This : access Typ)
                      return Java.Int is abstract;

   function Canonicalize (This : access Typ;
                          P1_Oid : access Standard.Org.Ietf.Jgss.Oid.Typ'Class)
                          return access Org.Ietf.Jgss.GSSName.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function Export (This : access Typ)
                    return Java.Byte_Arr is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function GetStringNameType (This : access Typ)
                               return access Org.Ietf.Jgss.Oid.Typ'Class is abstract;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function IsAnonymous (This : access Typ)
                         return Java.Boolean is abstract;

   function IsMN (This : access Typ)
                  return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NT_HOSTBASED_SERVICE : access Org.Ietf.Jgss.Oid.Typ'Class;

   --  final
   NT_USER_NAME : access Org.Ietf.Jgss.Oid.Typ'Class;

   --  final
   NT_MACHINE_UID_NAME : access Org.Ietf.Jgss.Oid.Typ'Class;

   --  final
   NT_STRING_UID_NAME : access Org.Ietf.Jgss.Oid.Typ'Class;

   --  final
   NT_ANONYMOUS : access Org.Ietf.Jgss.Oid.Typ'Class;

   --  final
   NT_EXPORT_NAME : access Org.Ietf.Jgss.Oid.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Equals, "equals");
   pragma Export (Java, HashCode, "hashCode");
   pragma Export (Java, Canonicalize, "canonicalize");
   pragma Export (Java, Export, "export");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, GetStringNameType, "getStringNameType");
   pragma Export (Java, IsAnonymous, "isAnonymous");
   pragma Export (Java, IsMN, "isMN");
   pragma Import (Java, NT_HOSTBASED_SERVICE, "NT_HOSTBASED_SERVICE");
   pragma Import (Java, NT_USER_NAME, "NT_USER_NAME");
   pragma Import (Java, NT_MACHINE_UID_NAME, "NT_MACHINE_UID_NAME");
   pragma Import (Java, NT_STRING_UID_NAME, "NT_STRING_UID_NAME");
   pragma Import (Java, NT_ANONYMOUS, "NT_ANONYMOUS");
   pragma Import (Java, NT_EXPORT_NAME, "NT_EXPORT_NAME");

end Org.Ietf.Jgss.GSSName;
pragma Import (Java, Org.Ietf.Jgss.GSSName, "org.ietf.jgss.GSSName");
pragma Extensions_Allowed (Off);
