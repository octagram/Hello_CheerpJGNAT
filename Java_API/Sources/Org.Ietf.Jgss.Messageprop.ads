pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Ietf.Jgss.MessageProp is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_MessageProp (P1_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   function New_MessageProp (P1_Int : Java.Int;
                             P2_Boolean : Java.Boolean; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetQOP (This : access Typ)
                    return Java.Int;

   function GetPrivacy (This : access Typ)
                        return Java.Boolean;

   procedure SetQOP (This : access Typ;
                     P1_Int : Java.Int);

   procedure SetPrivacy (This : access Typ;
                         P1_Boolean : Java.Boolean);

   function IsDuplicateToken (This : access Typ)
                              return Java.Boolean;

   function IsOldToken (This : access Typ)
                        return Java.Boolean;

   function IsUnseqToken (This : access Typ)
                          return Java.Boolean;

   function IsGapToken (This : access Typ)
                        return Java.Boolean;

   function GetMinorStatus (This : access Typ)
                            return Java.Int;

   function GetMinorString (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   procedure SetSupplementaryStates (This : access Typ;
                                     P1_Boolean : Java.Boolean;
                                     P2_Boolean : Java.Boolean;
                                     P3_Boolean : Java.Boolean;
                                     P4_Boolean : Java.Boolean;
                                     P5_Int : Java.Int;
                                     P6_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_MessageProp);
   pragma Import (Java, GetQOP, "getQOP");
   pragma Import (Java, GetPrivacy, "getPrivacy");
   pragma Import (Java, SetQOP, "setQOP");
   pragma Import (Java, SetPrivacy, "setPrivacy");
   pragma Import (Java, IsDuplicateToken, "isDuplicateToken");
   pragma Import (Java, IsOldToken, "isOldToken");
   pragma Import (Java, IsUnseqToken, "isUnseqToken");
   pragma Import (Java, IsGapToken, "isGapToken");
   pragma Import (Java, GetMinorStatus, "getMinorStatus");
   pragma Import (Java, GetMinorString, "getMinorString");
   pragma Import (Java, SetSupplementaryStates, "setSupplementaryStates");

end Org.Ietf.Jgss.MessageProp;
pragma Import (Java, Org.Ietf.Jgss.MessageProp, "org.ietf.jgss.MessageProp");
pragma Extensions_Allowed (Off);
