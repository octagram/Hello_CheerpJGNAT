pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Ietf.Jgss.Oid is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Oid (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function New_Oid (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                     This : Ref := null)
                     return Ref;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function New_Oid (P1_Byte_Arr : Java.Byte_Arr; 
                     This : Ref := null)
                     return Ref;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;

   function GetDER (This : access Typ)
                    return Java.Byte_Arr;
   --  can raise Org.Ietf.Jgss.GSSException.Except

   function ContainedIn (This : access Typ;
                         P1_Oid_Arr : access Org.Ietf.Jgss.Oid.Arr_Obj)
                         return Java.Boolean;

   function HashCode (This : access Typ)
                      return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Oid);
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, Equals, "equals");
   pragma Import (Java, GetDER, "getDER");
   pragma Import (Java, ContainedIn, "containedIn");
   pragma Import (Java, HashCode, "hashCode");

end Org.Ietf.Jgss.Oid;
pragma Import (Java, Org.Ietf.Jgss.Oid, "org.ietf.jgss.Oid");
pragma Extensions_Allowed (Off);
