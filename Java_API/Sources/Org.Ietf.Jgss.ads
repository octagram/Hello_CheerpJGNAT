pragma Extensions_Allowed (On);
package Org.Ietf.Jgss is
   pragma Preelaborate;
end Org.Ietf.Jgss;
pragma Import (Java, Org.Ietf.Jgss, "org.ietf.jgss");
pragma Extensions_Allowed (Off);
