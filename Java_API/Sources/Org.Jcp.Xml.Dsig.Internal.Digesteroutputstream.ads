pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Security.MessageDigest;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.OutputStream;
with Java.Lang.Object;

package Org.Jcp.Xml.Dsig.Internal.DigesterOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.OutputStream.Typ(Closeable_I,
                                    Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DigesterOutputStream (P1_MessageDigest : access Standard.Java.Security.MessageDigest.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   function New_DigesterOutputStream (P1_MessageDigest : access Standard.Java.Security.MessageDigest.Typ'Class;
                                      P2_Boolean : Java.Boolean; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr);

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);

   function GetDigestValue (This : access Typ)
                            return Java.Byte_Arr;

   function GetInputStream (This : access Typ)
                            return access Java.Io.InputStream.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DigesterOutputStream);
   pragma Import (Java, Write, "write");
   pragma Import (Java, GetDigestValue, "getDigestValue");
   pragma Import (Java, GetInputStream, "getInputStream");

end Org.Jcp.Xml.Dsig.Internal.DigesterOutputStream;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.DigesterOutputStream, "org.jcp.xml.dsig.internal.DigesterOutputStream");
pragma Extensions_Allowed (Off);
