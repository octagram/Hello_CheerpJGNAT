pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
with Java.Lang.Object;
with Javax.Xml.Crypto.NodeSetData;
with Org.Jcp.Xml.Dsig.Internal.Dom.ApacheData;

package Org.Jcp.Xml.Dsig.Internal.Dom.ApacheNodeSetData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(NodeSetData_I : Javax.Xml.Crypto.NodeSetData.Ref;
            ApacheData_I : Org.Jcp.Xml.Dsig.Internal.Dom.ApacheData.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Iterator, "iterator");

end Org.Jcp.Xml.Dsig.Internal.Dom.ApacheNodeSetData;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.ApacheNodeSetData, "org.jcp.xml.dsig.internal.dom.ApacheNodeSetData");
pragma Extensions_Allowed (Off);
