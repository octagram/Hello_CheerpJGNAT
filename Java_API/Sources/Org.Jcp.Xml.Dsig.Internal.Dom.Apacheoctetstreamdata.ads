pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Xml.Crypto.Data;
with Javax.Xml.Crypto.OctetStreamData;
with Org.Jcp.Xml.Dsig.Internal.Dom.ApacheData;

package Org.Jcp.Xml.Dsig.Internal.Dom.ApacheOctetStreamData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Data_I : Javax.Xml.Crypto.Data.Ref;
            ApacheData_I : Org.Jcp.Xml.Dsig.Internal.Dom.ApacheData.Ref)
    is new Javax.Xml.Crypto.OctetStreamData.Typ(Data_I)
      with null record;
private
   pragma Convention (Java, Typ);

end Org.Jcp.Xml.Dsig.Internal.Dom.ApacheOctetStreamData;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.ApacheOctetStreamData, "org.jcp.xml.dsig.internal.dom.ApacheOctetStreamData");
pragma Extensions_Allowed (Off);
