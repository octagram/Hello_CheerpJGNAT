pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Javax.Xml.Crypto.XMLStructure;
limited with Org.W3c.Dom.Document;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Transform;
with Javax.Xml.Crypto.Dsig.TransformService;

package Org.Jcp.Xml.Dsig.Internal.Dom.ApacheTransform is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transform_I : Javax.Xml.Crypto.Dsig.Transform.Ref)
    is abstract new Javax.Xml.Crypto.Dsig.TransformService.Typ(Transform_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      OwnerDoc : access Org.W3c.Dom.Document.Typ'Class;
      pragma Import (Java, OwnerDoc, "ownerDoc");

      --  protected
      TransformElem : access Org.W3c.Dom.Element.Typ'Class;
      pragma Import (Java, TransformElem, "transformElem");

      --  protected
      Params : access Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec.Typ'Class;
      pragma Import (Java, Params, "params");

   end record;

   function New_ApacheTransform (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;

   procedure Init (This : access Typ;
                   P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                   P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   procedure MarshalParams (This : access Typ;
                            P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                            P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Transform (This : access Typ;
                       P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                       P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                       return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   function Transform (This : access Typ;
                       P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                       P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                       P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                       return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   --  final
   function IsFeatureSupported (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ApacheTransform);
   pragma Import (Java, GetParameterSpec, "getParameterSpec");
   pragma Import (Java, Init, "init");
   pragma Import (Java, MarshalParams, "marshalParams");
   pragma Import (Java, Transform, "transform");
   pragma Import (Java, IsFeatureSupported, "isFeatureSupported");

end Org.Jcp.Xml.Dsig.Internal.Dom.ApacheTransform;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.ApacheTransform, "org.jcp.xml.dsig.internal.dom.ApacheTransform");
pragma Extensions_Allowed (Off);
