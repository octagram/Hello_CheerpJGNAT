pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Security.Provider;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dsig.TransformService;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.CanonicalizationMethod;
with Javax.Xml.Crypto.Dsig.Transform;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMTransform;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMCanonicalizationMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            CanonicalizationMethod_I : Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Ref;
            Transform_I : Javax.Xml.Crypto.Dsig.Transform.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMTransform.Typ(XMLStructure_I,
                                                          Transform_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMCanonicalizationMethod (P1_TransformService : access Standard.Javax.Xml.Crypto.Dsig.TransformService.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   function New_DOMCanonicalizationMethod (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                                           P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                                           P3_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                                           This : Ref := null)
                                           return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function Canonicalize (This : access Typ;
                          P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                          P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                          return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   function Canonicalize (This : access Typ;
                          P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                          P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                          P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                          return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMCanonicalizationMethod);
   pragma Import (Java, Canonicalize, "canonicalize");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMCanonicalizationMethod;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMCanonicalizationMethod, "org.jcp.xml.dsig.internal.dom.DOMCanonicalizationMethod");
pragma Extensions_Allowed (Off);
