pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMCryptoBinary is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMCryptoBinary (P1_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   function New_DOMCryptoBinary (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetBigNum (This : access Typ)
                       return access Java.Math.BigInteger.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMCryptoBinary);
   pragma Import (Java, GetBigNum, "getBigNum");
   pragma Import (Java, Marshal, "marshal");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMCryptoBinary;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMCryptoBinary, "org.jcp.xml.dsig.internal.dom.DOMCryptoBinary");
pragma Extensions_Allowed (Off);
