pragma Extensions_Allowed (On);
limited with Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Transform;
with Org.Jcp.Xml.Dsig.Internal.Dom.ApacheTransform;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMEnvelopedTransform is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transform_I : Javax.Xml.Crypto.Dsig.Transform.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.ApacheTransform.Typ(Transform_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMEnvelopedTransform (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Init (This : access Typ;
                   P1_TransformParameterSpec : access Standard.Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMEnvelopedTransform);
   pragma Import (Java, Init, "init");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMEnvelopedTransform;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMEnvelopedTransform, "org.jcp.xml.dsig.internal.dom.DOMEnvelopedTransform");
pragma Extensions_Allowed (Off);
