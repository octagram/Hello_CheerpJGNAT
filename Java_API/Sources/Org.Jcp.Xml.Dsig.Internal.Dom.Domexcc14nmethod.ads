pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Javax.Xml.Crypto.XMLStructure;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Transform;
with Org.Jcp.Xml.Dsig.Internal.Dom.ApacheCanonicalizer;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMExcC14NMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Transform_I : Javax.Xml.Crypto.Dsig.Transform.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.ApacheCanonicalizer.Typ(Transform_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMExcC14NMethod (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Init (This : access Typ;
                   P1_TransformParameterSpec : access Standard.Javax.Xml.Crypto.Dsig.Spec.TransformParameterSpec.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   procedure Init (This : access Typ;
                   P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                   P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class);
   --  can raise Java.Security.InvalidAlgorithmParameterException.Except

   procedure MarshalParams (This : access Typ;
                            P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                            P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function GetParamsNSURI (This : access Typ)
                            return access Java.Lang.String.Typ'Class;

   function Transform (This : access Typ;
                       P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                       P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                       return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMExcC14NMethod);
   pragma Import (Java, Init, "init");
   pragma Import (Java, MarshalParams, "marshalParams");
   pragma Import (Java, GetParamsNSURI, "getParamsNSURI");
   pragma Import (Java, Transform, "transform");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMExcC14NMethod;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMExcC14NMethod, "org.jcp.xml.dsig.internal.dom.DOMExcC14NMethod");
pragma Extensions_Allowed (Off);
