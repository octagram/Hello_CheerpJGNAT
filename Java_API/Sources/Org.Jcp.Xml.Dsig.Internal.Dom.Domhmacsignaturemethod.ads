pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.SignatureMethod;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignatureMethod;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMHMACSignatureMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            SignatureMethod_I : Javax.Xml.Crypto.Dsig.SignatureMethod.Ref)
    is abstract new Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignatureMethod.Typ(XMLStructure_I,
                                                                         SignatureMethod_I)
      with null record;
private
   pragma Convention (Java, Typ);

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMHMACSignatureMethod;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMHMACSignatureMethod, "org.jcp.xml.dsig.internal.dom.DOMHMACSignatureMethod");
pragma Extensions_Allowed (Off);
