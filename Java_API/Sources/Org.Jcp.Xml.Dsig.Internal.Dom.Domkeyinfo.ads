pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            KeyInfo_I : Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMKeyInfo (P1_List : access Standard.Java.Util.List.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_DOMKeyInfo (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                            P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                            P3_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetContent (This : access Typ)
                        return access Java.Util.List.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class;
                      P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P3_String : access Standard.Java.Lang.String.Typ'Class;
                      P4_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMKeyInfo);
   pragma Import (Java, GetId, "getId");
   pragma Import (Java, GetContent, "getContent");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyInfo;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyInfo, "org.jcp.xml.dsig.internal.dom.DOMKeyInfo");
pragma Extensions_Allowed (Off);
