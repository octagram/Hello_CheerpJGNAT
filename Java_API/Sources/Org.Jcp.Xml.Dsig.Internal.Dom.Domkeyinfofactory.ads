pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Java.Security.PublicKey;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyName;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.PGPData;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.X509Data;
limited with Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial;
limited with Javax.Xml.Crypto.URIDereferencer;
limited with Javax.Xml.Crypto.XMLStructure;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyInfoFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfoFactory.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMKeyInfoFactory (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewKeyInfo (This : access Typ;
                        P1_List : access Standard.Java.Util.List.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class;

   function NewKeyInfo (This : access Typ;
                        P1_List : access Standard.Java.Util.List.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class;

   function NewKeyName (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyName.Typ'Class;

   function NewKeyValue (This : access Typ;
                         P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue.Typ'Class;
   --  can raise Java.Security.KeyException.Except

   function NewPGPData (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.PGPData.Typ'Class;

   function NewPGPData (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_Byte_Arr : Java.Byte_Arr;
                        P3_List : access Standard.Java.Util.List.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.PGPData.Typ'Class;

   function NewPGPData (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr;
                        P2_List : access Standard.Java.Util.List.Typ'Class)
                        return access Javax.Xml.Crypto.Dsig.Keyinfo.PGPData.Typ'Class;

   function NewRetrievalMethod (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod.Typ'Class;

   function NewRetrievalMethod (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_List : access Standard.Java.Util.List.Typ'Class)
                                return access Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod.Typ'Class;

   function NewX509Data (This : access Typ;
                         P1_List : access Standard.Java.Util.List.Typ'Class)
                         return access Javax.Xml.Crypto.Dsig.Keyinfo.X509Data.Typ'Class;

   function NewX509IssuerSerial (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class)
                                 return access Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial.Typ'Class;

   function IsFeatureSupported (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean;

   function GetURIDereferencer (This : access Typ)
                                return access Javax.Xml.Crypto.URIDereferencer.Typ'Class;

   function UnmarshalKeyInfo (This : access Typ;
                              P1_XMLStructure : access Standard.Javax.Xml.Crypto.XMLStructure.Typ'Class)
                              return access Javax.Xml.Crypto.Dsig.Keyinfo.KeyInfo.Typ'Class;
   --  can raise Javax.Xml.Crypto.MarshalException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMKeyInfoFactory);
   pragma Import (Java, NewKeyInfo, "newKeyInfo");
   pragma Import (Java, NewKeyName, "newKeyName");
   pragma Import (Java, NewKeyValue, "newKeyValue");
   pragma Import (Java, NewPGPData, "newPGPData");
   pragma Import (Java, NewRetrievalMethod, "newRetrievalMethod");
   pragma Import (Java, NewX509Data, "newX509Data");
   pragma Import (Java, NewX509IssuerSerial, "newX509IssuerSerial");
   pragma Import (Java, IsFeatureSupported, "isFeatureSupported");
   pragma Import (Java, GetURIDereferencer, "getURIDereferencer");
   pragma Import (Java, UnmarshalKeyInfo, "unmarshalKeyInfo");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyInfoFactory;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyInfoFactory, "org.jcp.xml.dsig.internal.dom.DOMKeyInfoFactory");
pragma Extensions_Allowed (Off);
