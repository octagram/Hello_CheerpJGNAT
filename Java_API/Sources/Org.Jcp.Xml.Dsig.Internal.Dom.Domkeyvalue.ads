pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.PublicKey;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            KeyValue_I : Javax.Xml.Crypto.Dsig.Keyinfo.KeyValue.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMKeyValue (P1_PublicKey : access Standard.Java.Security.PublicKey.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Java.Security.KeyException.Except

   function New_DOMKeyValue (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class; 
                             This : Ref := null)
                             return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublicKey (This : access Typ)
                          return access Java.Security.PublicKey.Typ'Class;
   --  can raise Java.Security.KeyException.Except

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMKeyValue);
   pragma Import (Java, GetPublicKey, "getPublicKey");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyValue;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMKeyValue, "org.jcp.xml.dsig.internal.dom.DOMKeyValue");
pragma Extensions_Allowed (Off);
