pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Keyinfo.PGPData;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMPGPData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            PGPData_I : Javax.Xml.Crypto.Dsig.Keyinfo.PGPData.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMPGPData (P1_Byte_Arr : Java.Byte_Arr;
                            P2_List : access Standard.Java.Util.List.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_DOMPGPData (P1_Byte_Arr : Java.Byte_Arr;
                            P2_Byte_Arr : Java.Byte_Arr;
                            P3_List : access Standard.Java.Util.List.Typ'Class; 
                            This : Ref := null)
                            return Ref;

   function New_DOMPGPData (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class; 
                            This : Ref := null)
                            return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetKeyId (This : access Typ)
                      return Java.Byte_Arr;

   function GetKeyPacket (This : access Typ)
                          return Java.Byte_Arr;

   function GetExternalElements (This : access Typ)
                                 return access Java.Util.List.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMPGPData);
   pragma Import (Java, GetKeyId, "getKeyId");
   pragma Import (Java, GetKeyPacket, "getKeyPacket");
   pragma Import (Java, GetExternalElements, "getExternalElements");
   pragma Import (Java, Marshal, "marshal");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMPGPData;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMPGPData, "org.jcp.xml.dsig.internal.dom.DOMPGPData");
pragma Extensions_Allowed (Off);
