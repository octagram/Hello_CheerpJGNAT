pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Javax.Xml.Crypto.Dsig.DigestMethod;
limited with Javax.Xml.Crypto.Dsig.XMLSignContext;
limited with Javax.Xml.Crypto.Dsig.XMLValidateContext;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dom.DOMURIReference;
with Javax.Xml.Crypto.Dsig.Reference;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMReference is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            DOMURIReference_I : Javax.Xml.Crypto.Dom.DOMURIReference.Ref;
            Reference_I : Javax.Xml.Crypto.Dsig.Reference.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMReference (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_DigestMethod : access Standard.Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class;
                              P4_List : access Standard.Java.Util.List.Typ'Class;
                              P5_String : access Standard.Java.Lang.String.Typ'Class;
                              P6_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_DOMReference (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_DigestMethod : access Standard.Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class;
                              P4_List : access Standard.Java.Util.List.Typ'Class;
                              P5_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                              P6_List : access Standard.Java.Util.List.Typ'Class;
                              P7_String : access Standard.Java.Lang.String.Typ'Class;
                              P8_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_DOMReference (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_DigestMethod : access Standard.Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class;
                              P4_List : access Standard.Java.Util.List.Typ'Class;
                              P5_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                              P6_List : access Standard.Java.Util.List.Typ'Class;
                              P7_String : access Standard.Java.Lang.String.Typ'Class;
                              P8_Byte_Arr : Java.Byte_Arr;
                              P9_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_DOMReference (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                              P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                              P3_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDigestMethod (This : access Typ)
                             return access Javax.Xml.Crypto.Dsig.DigestMethod.Typ'Class;

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetURI (This : access Typ)
                    return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetTransforms (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   function GetDigestValue (This : access Typ)
                            return Java.Byte_Arr;

   function GetCalculatedDigestValue (This : access Typ)
                                      return Java.Byte_Arr;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   procedure Digest (This : access Typ;
                     P1_XMLSignContext : access Standard.Javax.Xml.Crypto.Dsig.XMLSignContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.Dsig.XMLSignatureException.Except

   function Validate (This : access Typ;
                      P1_XMLValidateContext : access Standard.Javax.Xml.Crypto.Dsig.XMLValidateContext.Typ'Class)
                      return Java.Boolean;
   --  can raise Javax.Xml.Crypto.Dsig.XMLSignatureException.Except

   function GetDereferencedData (This : access Typ)
                                 return access Javax.Xml.Crypto.Data.Typ'Class;

   function GetDigestInputStream (This : access Typ)
                                  return access Java.Io.InputStream.Typ'Class;

   function GetHere (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMReference);
   pragma Import (Java, GetDigestMethod, "getDigestMethod");
   pragma Import (Java, GetId, "getId");
   pragma Import (Java, GetURI, "getURI");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetTransforms, "getTransforms");
   pragma Import (Java, GetDigestValue, "getDigestValue");
   pragma Import (Java, GetCalculatedDigestValue, "getCalculatedDigestValue");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Digest, "digest");
   pragma Import (Java, Validate, "validate");
   pragma Import (Java, GetDereferencedData, "getDereferencedData");
   pragma Import (Java, GetDigestInputStream, "getDigestInputStream");
   pragma Import (Java, GetHere, "getHere");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMReference;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMReference, "org.jcp.xml.dsig.internal.dom.DOMReference");
pragma Extensions_Allowed (Off);
