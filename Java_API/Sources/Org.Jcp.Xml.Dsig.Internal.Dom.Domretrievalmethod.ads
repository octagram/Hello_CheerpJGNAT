pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dom.DOMURIReference;
with Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMRetrievalMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            DOMURIReference_I : Javax.Xml.Crypto.Dom.DOMURIReference.Ref;
            RetrievalMethod_I : Javax.Xml.Crypto.Dsig.Keyinfo.RetrievalMethod.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMRetrievalMethod (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class;
                                    P3_List : access Standard.Java.Util.List.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   function New_DOMRetrievalMethod (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                                    P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                                    P3_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetURI (This : access Typ)
                    return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class;

   function GetTransforms (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function GetHere (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class;

   function Dereference (This : access Typ;
                         P1_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                         return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.URIReferenceException.Except

   function DereferenceAsXMLStructure (This : access Typ;
                                       P1_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                                       return access Javax.Xml.Crypto.XMLStructure.Typ'Class;
   --  can raise Javax.Xml.Crypto.URIReferenceException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMRetrievalMethod);
   pragma Import (Java, GetURI, "getURI");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetTransforms, "getTransforms");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, GetHere, "getHere");
   pragma Import (Java, Dereference, "dereference");
   pragma Import (Java, DereferenceAsXMLStructure, "dereferenceAsXMLStructure");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMRetrievalMethod;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMRetrievalMethod, "org.jcp.xml.dsig.internal.dom.DOMRetrievalMethod");
pragma Extensions_Allowed (Off);
