pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.SignatureMethod;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignatureMethod is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            SignatureMethod_I : Javax.Xml.Crypto.Dsig.SignatureMethod.Ref)
    is abstract new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetParameterSpec, "getParameterSpec");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignatureMethod;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignatureMethod, "org.jcp.xml.dsig.internal.dom.DOMSignatureMethod");
pragma Extensions_Allowed (Off);
