pragma Extensions_Allowed (On);
limited with Java.Io.ByteArrayOutputStream;
limited with Java.Io.InputStream;
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Javax.Xml.Crypto.Dsig.CanonicalizationMethod;
limited with Javax.Xml.Crypto.Dsig.SignatureMethod;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.SignedInfo;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignedInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            SignedInfo_I : Javax.Xml.Crypto.Dsig.SignedInfo.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMSignedInfo (P1_CanonicalizationMethod : access Standard.Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class;
                               P2_SignatureMethod : access Standard.Javax.Xml.Crypto.Dsig.SignatureMethod.Typ'Class;
                               P3_List : access Standard.Java.Util.List.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_DOMSignedInfo (P1_CanonicalizationMethod : access Standard.Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class;
                               P2_SignatureMethod : access Standard.Javax.Xml.Crypto.Dsig.SignatureMethod.Typ'Class;
                               P3_List : access Standard.Java.Util.List.Typ'Class;
                               P4_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_DOMSignedInfo (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                               P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                               P3_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                               This : Ref := null)
                               return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCanonicalizationMethod (This : access Typ)
                                       return access Javax.Xml.Crypto.Dsig.CanonicalizationMethod.Typ'Class;

   function GetSignatureMethod (This : access Typ)
                                return access Javax.Xml.Crypto.Dsig.SignatureMethod.Typ'Class;

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetReferences (This : access Typ)
                           return access Java.Util.List.Typ'Class;

   function GetCanonicalizedData (This : access Typ)
                                  return access Java.Io.InputStream.Typ'Class;

   procedure Canonicalize (This : access Typ;
                           P1_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                           P2_ByteArrayOutputStream : access Standard.Java.Io.ByteArrayOutputStream.Typ'Class);
   --  can raise Javax.Xml.Crypto.Dsig.XMLSignatureException.Except

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMSignedInfo);
   pragma Import (Java, GetCanonicalizationMethod, "getCanonicalizationMethod");
   pragma Import (Java, GetSignatureMethod, "getSignatureMethod");
   pragma Import (Java, GetId, "getId");
   pragma Import (Java, GetReferences, "getReferences");
   pragma Import (Java, GetCanonicalizedData, "getCanonicalizedData");
   pragma Import (Java, Canonicalize, "canonicalize");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignedInfo;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMSignedInfo, "org.jcp.xml.dsig.internal.dom.DOMSignedInfo");
pragma Extensions_Allowed (Off);
