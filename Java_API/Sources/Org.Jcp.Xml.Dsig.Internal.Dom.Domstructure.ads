pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.XMLStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_DOMStructure (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function IsFeatureSupported (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class) is abstract;
   --  can raise Javax.Xml.Crypto.MarshalException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMStructure);
   pragma Import (Java, IsFeatureSupported, "isFeatureSupported");
   pragma Export (Java, Marshal, "marshal");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure, "org.jcp.xml.dsig.internal.dom.DOMStructure");
pragma Extensions_Allowed (Off);
