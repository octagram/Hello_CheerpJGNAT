pragma Extensions_Allowed (On);
limited with Java.Util.Iterator;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.NodeSetData;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMSubTreeData is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(NodeSetData_I : Javax.Xml.Crypto.NodeSetData.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMSubTreeData (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                                P2_Boolean : Java.Boolean; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Iterator (This : access Typ)
                      return access Java.Util.Iterator.Typ'Class;

   function GetRoot (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class;

   function ExcludeComments (This : access Typ)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMSubTreeData);
   pragma Import (Java, Iterator, "iterator");
   pragma Import (Java, GetRoot, "getRoot");
   pragma Import (Java, ExcludeComments, "excludeComments");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMSubTreeData;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMSubTreeData, "org.jcp.xml.dsig.internal.dom.DOMSubTreeData");
pragma Extensions_Allowed (Off);
