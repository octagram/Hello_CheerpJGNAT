pragma Extensions_Allowed (On);
limited with Java.Io.OutputStream;
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Security.Spec.AlgorithmParameterSpec;
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Javax.Xml.Crypto.Dsig.TransformService;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Transform;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMTransform is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            Transform_I : Javax.Xml.Crypto.Dsig.Transform.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      --  protected
      Spi : access Javax.Xml.Crypto.Dsig.TransformService.Typ'Class;
      pragma Import (Java, Spi, "spi");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMTransform (P1_TransformService : access Standard.Javax.Xml.Crypto.Dsig.TransformService.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_DOMTransform (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                              P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                              P3_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function GetParameterSpec (This : access Typ)
                              return access Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;

   --  final
   function GetAlgorithm (This : access Typ)
                          return access Java.Lang.String.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Transform (This : access Typ;
                       P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                       P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                       return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   function Transform (This : access Typ;
                       P1_Data : access Standard.Javax.Xml.Crypto.Data.Typ'Class;
                       P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                       P3_OutputStream : access Standard.Java.Io.OutputStream.Typ'Class)
                       return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.Dsig.TransformException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMTransform);
   pragma Import (Java, GetParameterSpec, "getParameterSpec");
   pragma Import (Java, GetAlgorithm, "getAlgorithm");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Transform, "transform");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMTransform;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMTransform, "org.jcp.xml.dsig.internal.dom.DOMTransform");
pragma Extensions_Allowed (Off);
