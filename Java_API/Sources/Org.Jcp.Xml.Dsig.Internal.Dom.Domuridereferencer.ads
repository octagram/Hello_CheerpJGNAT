pragma Extensions_Allowed (On);
limited with Javax.Xml.Crypto.Data;
limited with Javax.Xml.Crypto.URIReference;
limited with Javax.Xml.Crypto.XMLCryptoContext;
with Java.Lang.Object;
with Javax.Xml.Crypto.URIDereferencer;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMURIDereferencer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(URIDereferencer_I : Javax.Xml.Crypto.URIDereferencer.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Dereference (This : access Typ;
                         P1_URIReference : access Standard.Javax.Xml.Crypto.URIReference.Typ'Class;
                         P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                         return access Javax.Xml.Crypto.Data.Typ'Class;
   --  can raise Javax.Xml.Crypto.URIReferenceException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Dereference, "dereference");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMURIDereferencer;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMURIDereferencer, "org.jcp.xml.dsig.internal.dom.DOMURIDereferencer");
pragma Extensions_Allowed (Off);
