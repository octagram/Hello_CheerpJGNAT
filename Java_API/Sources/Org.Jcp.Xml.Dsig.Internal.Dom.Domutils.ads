pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Spec.AlgorithmParameterSpec;
limited with Java.Util.Set;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Document;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
limited with Org.W3c.Dom.NodeList;
with Java.Lang.Object;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMUtils is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOwnerDocument (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                              return access Org.W3c.Dom.Document.Typ'Class;

   function CreateElement (P1_Document : access Standard.Org.W3c.Dom.Document.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.W3c.Dom.Element.Typ'Class;

   procedure SetAttribute (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetAttributeID (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class);

   function GetFirstChildElement (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                                  return access Org.W3c.Dom.Element.Typ'Class;

   function GetLastChildElement (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                                 return access Org.W3c.Dom.Element.Typ'Class;

   function GetNextSiblingElement (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                                   return access Org.W3c.Dom.Element.Typ'Class;

   function GetAttributeValue (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Java.Lang.String.Typ'Class;

   function NodeSet (P1_NodeList : access Standard.Org.W3c.Dom.NodeList.Typ'Class)
                     return access Java.Util.Set.Typ'Class;

   function GetNSPrefix (P1_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.String.Typ'Class;

   function GetSignaturePrefix (P1_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class)
                                return access Java.Lang.String.Typ'Class;

   procedure RemoveAllChildren (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);

   function NodesEqual (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                        P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                        return Java.Boolean;

   procedure AppendChild (P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                          P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class);

   function ParamsEqual (P1_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class;
                         P2_AlgorithmParameterSpec : access Standard.Java.Security.Spec.AlgorithmParameterSpec.Typ'Class)
                         return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, GetOwnerDocument, "getOwnerDocument");
   pragma Import (Java, CreateElement, "createElement");
   pragma Import (Java, SetAttribute, "setAttribute");
   pragma Import (Java, SetAttributeID, "setAttributeID");
   pragma Import (Java, GetFirstChildElement, "getFirstChildElement");
   pragma Import (Java, GetLastChildElement, "getLastChildElement");
   pragma Import (Java, GetNextSiblingElement, "getNextSiblingElement");
   pragma Import (Java, GetAttributeValue, "getAttributeValue");
   pragma Import (Java, NodeSet, "nodeSet");
   pragma Import (Java, GetNSPrefix, "getNSPrefix");
   pragma Import (Java, GetSignaturePrefix, "getSignaturePrefix");
   pragma Import (Java, RemoveAllChildren, "removeAllChildren");
   pragma Import (Java, NodesEqual, "nodesEqual");
   pragma Import (Java, AppendChild, "appendChild");
   pragma Import (Java, ParamsEqual, "paramsEqual");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMUtils;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMUtils, "org.jcp.xml.dsig.internal.dom.DOMUtils");
pragma Extensions_Allowed (Off);
