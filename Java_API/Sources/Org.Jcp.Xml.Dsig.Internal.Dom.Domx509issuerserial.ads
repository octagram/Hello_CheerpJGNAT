pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigInteger;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMX509IssuerSerial is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            X509IssuerSerial_I : Javax.Xml.Crypto.Dsig.Keyinfo.X509IssuerSerial.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMX509IssuerSerial (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                     P2_BigInteger : access Standard.Java.Math.BigInteger.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   function New_DOMX509IssuerSerial (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class; 
                                     This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetIssuerName (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetSerialNumber (This : access Typ)
                             return access Java.Math.BigInteger.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMX509IssuerSerial);
   pragma Import (Java, GetIssuerName, "getIssuerName");
   pragma Import (Java, GetSerialNumber, "getSerialNumber");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMX509IssuerSerial;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMX509IssuerSerial, "org.jcp.xml.dsig.internal.dom.DOMX509IssuerSerial");
pragma Extensions_Allowed (Off);
