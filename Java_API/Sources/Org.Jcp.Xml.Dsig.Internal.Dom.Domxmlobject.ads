pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Security.Provider;
limited with Java.Util.List;
limited with Javax.Xml.Crypto.Dom.DOMCryptoContext;
limited with Javax.Xml.Crypto.XMLCryptoContext;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Javax.Xml.Crypto.Dsig.XMLObject;
with Javax.Xml.Crypto.XMLStructure;
with Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure;

package Org.Jcp.Xml.Dsig.Internal.Dom.DOMXMLObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(XMLStructure_I : Javax.Xml.Crypto.XMLStructure.Ref;
            XMLObject_I : Javax.Xml.Crypto.Dsig.XMLObject.Ref)
    is new Org.Jcp.Xml.Dsig.Internal.Dom.DOMStructure.Typ(XMLStructure_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMXMLObject (P1_List : access Standard.Java.Util.List.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_String : access Standard.Java.Lang.String.Typ'Class;
                              P4_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_DOMXMLObject (P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                              P2_XMLCryptoContext : access Standard.Javax.Xml.Crypto.XMLCryptoContext.Typ'Class;
                              P3_Provider : access Standard.Java.Security.Provider.Typ'Class; 
                              This : Ref := null)
                              return Ref;
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetContent (This : access Typ)
                        return access Java.Util.List.Typ'Class;

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetMimeType (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure Marshal (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class;
                      P3_DOMCryptoContext : access Standard.Javax.Xml.Crypto.Dom.DOMCryptoContext.Typ'Class);
   --  can raise Javax.Xml.Crypto.MarshalException.Except

   function Equals (This : access Typ;
                    P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DOMXMLObject);
   pragma Import (Java, GetContent, "getContent");
   pragma Import (Java, GetId, "getId");
   pragma Import (Java, GetMimeType, "getMimeType");
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, Marshal, "marshal");
   pragma Import (Java, Equals, "equals");

end Org.Jcp.Xml.Dsig.Internal.Dom.DOMXMLObject;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.DOMXMLObject, "org.jcp.xml.dsig.internal.dom.DOMXMLObject");
pragma Extensions_Allowed (Off);
