pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Set;
with Java.Lang.Object;

package Org.Jcp.Xml.Dsig.Internal.Dom.Policy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function RestrictAlg (P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   function RestrictNumTransforms (P1_Int : Java.Int)
                                   return Java.Boolean;

   function RestrictNumReferences (P1_Int : Java.Int)
                                   return Java.Boolean;

   function RestrictReferenceUriScheme (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return Java.Boolean;

   function RestrictKey (P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Int : Java.Int)
                         return Java.Boolean;

   function RestrictDuplicateIds return Java.Boolean;

   function RestrictRetrievalMethodLoops return Java.Boolean;

   function DisabledAlgs return access Java.Util.Set.Typ'Class;

   function MaxTransforms return Java.Int;

   function MaxReferences return Java.Int;

   function DisabledReferenceUriSchemes return access Java.Util.Set.Typ'Class;

   function MinKeySize (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, RestrictAlg, "restrictAlg");
   pragma Import (Java, RestrictNumTransforms, "restrictNumTransforms");
   pragma Import (Java, RestrictNumReferences, "restrictNumReferences");
   pragma Import (Java, RestrictReferenceUriScheme, "restrictReferenceUriScheme");
   pragma Import (Java, RestrictKey, "restrictKey");
   pragma Import (Java, RestrictDuplicateIds, "restrictDuplicateIds");
   pragma Import (Java, RestrictRetrievalMethodLoops, "restrictRetrievalMethodLoops");
   pragma Import (Java, DisabledAlgs, "disabledAlgs");
   pragma Import (Java, MaxTransforms, "maxTransforms");
   pragma Import (Java, MaxReferences, "maxReferences");
   pragma Import (Java, DisabledReferenceUriSchemes, "disabledReferenceUriSchemes");
   pragma Import (Java, MinKeySize, "minKeySize");

end Org.Jcp.Xml.Dsig.Internal.Dom.Policy;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.Policy, "org.jcp.xml.dsig.internal.dom.Policy");
pragma Extensions_Allowed (Off);
