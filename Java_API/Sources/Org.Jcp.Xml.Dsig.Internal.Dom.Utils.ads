pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Jcp.Xml.Dsig.Internal.Dom.Utils is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ReadBytesFromStream (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class)
                                 return Java.Byte_Arr;
   --  can raise Java.Io.IOException.Except

   function ParseIdFromSameDocumentURI (P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return access Java.Lang.String.Typ'Class;

   function SameDocumentURI (P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, ReadBytesFromStream, "readBytesFromStream");
   pragma Import (Java, ParseIdFromSameDocumentURI, "parseIdFromSameDocumentURI");
   pragma Import (Java, SameDocumentURI, "sameDocumentURI");

end Org.Jcp.Xml.Dsig.Internal.Dom.Utils;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.Utils, "org.jcp.xml.dsig.internal.dom.Utils");
pragma Extensions_Allowed (Off);
