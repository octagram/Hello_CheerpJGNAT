pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Cloneable;
with Java.Lang.Object;
with Java.Security.Provider;
with Java.Util.Map;

package Org.Jcp.Xml.Dsig.Internal.Dom.XMLDSigRI is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            Cloneable_I : Java.Lang.Cloneable.Ref;
            Map_I : Java.Util.Map.Ref)
    is new Java.Security.Provider.Typ(Serializable_I,
                                      Cloneable_I,
                                      Map_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XMLDSigRI (This : Ref := null)
                           return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLDSigRI);

end Org.Jcp.Xml.Dsig.Internal.Dom.XMLDSigRI;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom.XMLDSigRI, "org.jcp.xml.dsig.internal.dom.XMLDSigRI");
pragma Extensions_Allowed (Off);
