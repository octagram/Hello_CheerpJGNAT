pragma Extensions_Allowed (On);
package Org.Jcp.Xml.Dsig.Internal.Dom is
   pragma Preelaborate;
end Org.Jcp.Xml.Dsig.Internal.Dom;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.Dom, "org.jcp.xml.dsig.internal.dom");
pragma Extensions_Allowed (Off);
