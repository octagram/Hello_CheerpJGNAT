pragma Extensions_Allowed (On);
limited with Java.Security.Signature;
with Java.Io.ByteArrayOutputStream;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Object;

package Org.Jcp.Xml.Dsig.Internal.SignerOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is new Java.Io.ByteArrayOutputStream.Typ(Closeable_I,
                                             Flushable_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SignerOutputStream (P1_Signature : access Standard.Java.Security.Signature.Typ'Class; 
                                    This : Ref := null)
                                    return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr);

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);

   procedure Write (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr;
                    P2_Int : Java.Int;
                    P3_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_SignerOutputStream);
   pragma Import (Java, Write, "write");

end Org.Jcp.Xml.Dsig.Internal.SignerOutputStream;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal.SignerOutputStream, "org.jcp.xml.dsig.internal.SignerOutputStream");
pragma Extensions_Allowed (Off);
