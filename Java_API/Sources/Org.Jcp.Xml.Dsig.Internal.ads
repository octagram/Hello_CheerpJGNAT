pragma Extensions_Allowed (On);
package Org.Jcp.Xml.Dsig.Internal is
   pragma Preelaborate;
end Org.Jcp.Xml.Dsig.Internal;
pragma Import (Java, Org.Jcp.Xml.Dsig.Internal, "org.jcp.xml.dsig.internal");
pragma Extensions_Allowed (Off);
