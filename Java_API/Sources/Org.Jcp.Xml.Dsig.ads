pragma Extensions_Allowed (On);
package Org.Jcp.Xml.Dsig is
   pragma Preelaborate;
end Org.Jcp.Xml.Dsig;
pragma Import (Java, Org.Jcp.Xml.Dsig, "org.jcp.xml.dsig");
pragma Extensions_Allowed (Off);
